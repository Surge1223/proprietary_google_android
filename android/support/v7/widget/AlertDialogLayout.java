package android.support.v7.widget;

import android.graphics.drawable.Drawable;
import android.support.v4.view.GravityCompat;
import android.support.v7.appcompat.R;
import android.view.ViewGroup;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;

public class AlertDialogLayout extends LinearLayoutCompat
{
    public AlertDialogLayout(final Context context) {
        super(context);
    }
    
    public AlertDialogLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    private void forceUniformWidth(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.getMeasuredWidth(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                if (layoutParams.width == -1) {
                    final int height = layoutParams.height;
                    layoutParams.height = child.getMeasuredHeight();
                    this.measureChildWithMargins(child, measureSpec, 0, n2, 0);
                    layoutParams.height = height;
                }
            }
        }
    }
    
    private static int resolveMinimumHeight(final View view) {
        final int minimumHeight = ViewCompat.getMinimumHeight(view);
        if (minimumHeight > 0) {
            return minimumHeight;
        }
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            if (viewGroup.getChildCount() == 1) {
                return resolveMinimumHeight(viewGroup.getChildAt(0));
            }
        }
        return 0;
    }
    
    private void setChildFrame(final View view, final int n, final int n2, final int n3, final int n4) {
        view.layout(n, n2, n + n3, n2 + n4);
    }
    
    private boolean tryOnMeasure(final int n, final int n2) {
        final int childCount = this.getChildCount();
        View view = null;
        View view2 = null;
        View view3 = null;
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final int id = child.getId();
                if (id == R.id.topPanel) {
                    view3 = child;
                }
                else if (id == R.id.buttonPanel) {
                    view2 = child;
                }
                else {
                    if (id != R.id.contentPanel && id != R.id.customPanel) {
                        return false;
                    }
                    if (view != null) {
                        return false;
                    }
                    view = child;
                }
            }
        }
        final int mode = View$MeasureSpec.getMode(n2);
        final int size = View$MeasureSpec.getSize(n2);
        final int mode2 = View$MeasureSpec.getMode(n);
        int combineMeasuredStates = 0;
        int n4;
        final int n3 = n4 = this.getPaddingTop() + this.getPaddingBottom();
        if (view3 != null) {
            view3.measure(n, 0);
            n4 = n3 + view3.getMeasuredHeight();
            combineMeasuredStates = View.combineMeasuredStates(0, view3.getMeasuredState());
        }
        int resolveMinimumHeight = 0;
        int n5 = 0;
        int n6 = combineMeasuredStates;
        int n7 = n4;
        if (view2 != null) {
            view2.measure(n, 0);
            resolveMinimumHeight = resolveMinimumHeight(view2);
            n5 = view2.getMeasuredHeight() - resolveMinimumHeight;
            n7 = n4 + resolveMinimumHeight;
            n6 = View.combineMeasuredStates(combineMeasuredStates, view2.getMeasuredState());
        }
        int measuredHeight = 0;
        if (view != null) {
            int measureSpec;
            if (mode == 0) {
                measureSpec = 0;
            }
            else {
                measureSpec = View$MeasureSpec.makeMeasureSpec(Math.max(0, size - n7), mode);
            }
            view.measure(n, measureSpec);
            measuredHeight = view.getMeasuredHeight();
            n7 += measuredHeight;
            n6 = View.combineMeasuredStates(n6, view.getMeasuredState());
        }
        int n9;
        final int n8 = n9 = size - n7;
        int n10 = n6;
        int n11 = n7;
        if (view2 != null) {
            final int min = Math.min(n8, n5);
            n9 = n8;
            int n12 = resolveMinimumHeight;
            if (min > 0) {
                n9 = n8 - min;
                n12 = resolveMinimumHeight + min;
            }
            view2.measure(n, View$MeasureSpec.makeMeasureSpec(n12, 1073741824));
            n11 = n7 - resolveMinimumHeight + view2.getMeasuredHeight();
            n10 = View.combineMeasuredStates(n6, view2.getMeasuredState());
        }
        if (view != null && n9 > 0) {
            view.measure(n, View$MeasureSpec.makeMeasureSpec(measuredHeight + n9, mode));
            n11 = n11 - measuredHeight + view.getMeasuredHeight();
            n10 = View.combineMeasuredStates(n10, view.getMeasuredState());
        }
        int n13 = 0;
        int max;
        for (int j = 0; j < childCount; ++j, n13 = max) {
            final View child2 = this.getChildAt(j);
            max = n13;
            if (child2.getVisibility() != 8) {
                max = Math.max(n13, child2.getMeasuredWidth());
            }
        }
        this.setMeasuredDimension(View.resolveSizeAndState(n13 + (this.getPaddingLeft() + this.getPaddingRight()), n, n10), View.resolveSizeAndState(n11, n2, 0));
        if (mode2 != 1073741824) {
            this.forceUniformWidth(childCount, n2);
        }
        return true;
    }
    
    @Override
    protected void onLayout(final boolean b, int n, int gravity, int intrinsicHeight, int n2) {
        final int paddingLeft = this.getPaddingLeft();
        final int n3 = intrinsicHeight - n;
        final int paddingRight = this.getPaddingRight();
        final int paddingRight2 = this.getPaddingRight();
        n = this.getMeasuredHeight();
        final int childCount = this.getChildCount();
        final int gravity2 = this.getGravity();
        final int n4 = gravity2 & 0x70;
        if (n4 != 16) {
            if (n4 != 80) {
                n = this.getPaddingTop();
            }
            else {
                n = this.getPaddingTop() + n2 - gravity - n;
            }
        }
        else {
            n = this.getPaddingTop() + (n2 - gravity - n) / 2;
        }
        final Drawable dividerDrawable = this.getDividerDrawable();
        gravity = 0;
        if (dividerDrawable == null) {
            intrinsicHeight = 0;
        }
        else {
            intrinsicHeight = dividerDrawable.getIntrinsicHeight();
        }
        n2 = childCount;
        for (int i = gravity; i < n2; ++i) {
            final View child = this.getChildAt(i);
            if (child != null) {
                if (child.getVisibility() != 8) {
                    final int measuredWidth = child.getMeasuredWidth();
                    final int measuredHeight = child.getMeasuredHeight();
                    final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                    if ((gravity = layoutParams.gravity) < 0) {
                        gravity = (gravity2 & 0x800007);
                    }
                    gravity = (GravityCompat.getAbsoluteGravity(gravity, ViewCompat.getLayoutDirection((View)this)) & 0x7);
                    if (gravity != 1) {
                        if (gravity != 5) {
                            gravity = layoutParams.leftMargin + paddingLeft;
                        }
                        else {
                            gravity = n3 - paddingRight - measuredWidth - layoutParams.rightMargin;
                        }
                    }
                    else {
                        gravity = (n3 - paddingLeft - paddingRight2 - measuredWidth) / 2 + paddingLeft + layoutParams.leftMargin - layoutParams.rightMargin;
                    }
                    int n5 = n;
                    if (this.hasDividerBeforeChildAt(i)) {
                        n5 = n + intrinsicHeight;
                    }
                    n = n5 + layoutParams.topMargin;
                    this.setChildFrame(child, gravity, n, measuredWidth, measuredHeight);
                    n += measuredHeight + layoutParams.bottomMargin;
                }
            }
        }
    }
    
    @Override
    protected void onMeasure(final int n, final int n2) {
        if (!this.tryOnMeasure(n, n2)) {
            super.onMeasure(n, n2);
        }
    }
}
