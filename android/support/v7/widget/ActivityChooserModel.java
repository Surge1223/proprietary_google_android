package android.support.v7.widget;

import java.math.BigDecimal;
import android.content.ComponentName;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.content.pm.ResolveInfo;
import java.util.HashMap;
import android.content.Intent;
import android.content.Context;
import java.util.List;
import java.util.Map;
import android.database.DataSetObservable;

class ActivityChooserModel extends DataSetObservable
{
    static final String LOG_TAG;
    private static final Map<String, ActivityChooserModel> sDataModelRegistry;
    private static final Object sRegistryLock;
    private final List<ActivityResolveInfo> mActivities;
    private OnChooseActivityListener mActivityChoserModelPolicy;
    private ActivitySorter mActivitySorter;
    boolean mCanReadHistoricalData;
    final Context mContext;
    private final List<HistoricalRecord> mHistoricalRecords;
    private boolean mHistoricalRecordsChanged;
    final String mHistoryFileName;
    private int mHistoryMaxSize;
    private final Object mInstanceLock;
    private Intent mIntent;
    private boolean mReadShareHistoryCalled;
    private boolean mReloadActivities;
    
    static {
        LOG_TAG = ActivityChooserModel.class.getSimpleName();
        sRegistryLock = new Object();
        sDataModelRegistry = new HashMap<String, ActivityChooserModel>();
    }
    
    private boolean addHistoricalRecord(final HistoricalRecord historicalRecord) {
        final boolean add = this.mHistoricalRecords.add(historicalRecord);
        if (add) {
            this.mHistoricalRecordsChanged = true;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            this.persistHistoricalDataIfNeeded();
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
        return add;
    }
    
    private void ensureConsistentState() {
        final boolean loadActivitiesIfNeeded = this.loadActivitiesIfNeeded();
        final boolean historicalDataIfNeeded = this.readHistoricalDataIfNeeded();
        this.pruneExcessiveHistoricalRecordsIfNeeded();
        if (loadActivitiesIfNeeded | historicalDataIfNeeded) {
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
    }
    
    private boolean loadActivitiesIfNeeded() {
        final boolean mReloadActivities = this.mReloadActivities;
        int i = 0;
        if (mReloadActivities && this.mIntent != null) {
            this.mReloadActivities = false;
            this.mActivities.clear();
            for (List queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0); i < queryIntentActivities.size(); ++i) {
                this.mActivities.add(new ActivityResolveInfo(queryIntentActivities.get(i)));
            }
            return true;
        }
        return false;
    }
    
    private void persistHistoricalDataIfNeeded() {
        if (!this.mReadShareHistoryCalled) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        }
        if (!this.mHistoricalRecordsChanged) {
            return;
        }
        this.mHistoricalRecordsChanged = false;
        if (!TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            new PersistHistoryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[] { new ArrayList(this.mHistoricalRecords), this.mHistoryFileName });
        }
    }
    
    private void pruneExcessiveHistoricalRecordsIfNeeded() {
        final int n = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
        if (n <= 0) {
            return;
        }
        this.mHistoricalRecordsChanged = true;
        for (int i = 0; i < n; ++i) {
            final HistoricalRecord historicalRecord = this.mHistoricalRecords.remove(0);
        }
    }
    
    private boolean readHistoricalDataIfNeeded() {
        if (this.mCanReadHistoricalData && this.mHistoricalRecordsChanged && !TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            this.mCanReadHistoricalData = false;
            this.mReadShareHistoryCalled = true;
            this.readHistoricalDataImpl();
            return true;
        }
        return false;
    }
    
    private void readHistoricalDataImpl() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        android/support/v7/widget/ActivityChooserModel.mContext:Landroid/content/Context;
        //     4: aload_0        
        //     5: getfield        android/support/v7/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //     8: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    11: astore_1       
        //    12: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    15: astore_2       
        //    16: aload_2        
        //    17: aload_1        
        //    18: ldc             "UTF-8"
        //    20: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    25: iconst_0       
        //    26: istore_3       
        //    27: iload_3        
        //    28: iconst_1       
        //    29: if_icmpeq       47
        //    32: iload_3        
        //    33: iconst_2       
        //    34: if_icmpeq       47
        //    37: aload_2        
        //    38: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    43: istore_3       
        //    44: goto            27
        //    47: ldc             "historical-records"
        //    49: aload_2        
        //    50: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    55: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    58: ifeq            204
        //    61: aload_0        
        //    62: getfield        android/support/v7/widget/ActivityChooserModel.mHistoricalRecords:Ljava/util/List;
        //    65: astore          4
        //    67: aload           4
        //    69: invokeinterface java/util/List.clear:()V
        //    74: aload_2        
        //    75: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    80: istore_3       
        //    81: iload_3        
        //    82: iconst_1       
        //    83: if_icmpne       97
        //    86: aload_1        
        //    87: ifnull          335
        //    90: aload_1        
        //    91: invokevirtual   java/io/FileInputStream.close:()V
        //    94: goto            328
        //    97: iload_3        
        //    98: iconst_3       
        //    99: if_icmpeq       74
        //   102: iload_3        
        //   103: iconst_4       
        //   104: if_icmpne       110
        //   107: goto            74
        //   110: ldc             "historical-record"
        //   112: aload_2        
        //   113: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //   118: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   121: ifeq            192
        //   124: aload_2        
        //   125: aconst_null    
        //   126: ldc             "activity"
        //   128: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   133: astore          5
        //   135: aload_2        
        //   136: aconst_null    
        //   137: ldc             "time"
        //   139: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   144: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   147: lstore          6
        //   149: aload_2        
        //   150: aconst_null    
        //   151: ldc             "weight"
        //   153: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   158: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   161: fstore          8
        //   163: new             Landroid/support/v7/widget/ActivityChooserModel$HistoricalRecord;
        //   166: astore          9
        //   168: aload           9
        //   170: aload           5
        //   172: lload           6
        //   174: fload           8
        //   176: invokespecial   android/support/v7/widget/ActivityChooserModel$HistoricalRecord.<init>:(Ljava/lang/String;JF)V
        //   179: aload           4
        //   181: aload           9
        //   183: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   188: pop            
        //   189: goto            74
        //   192: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   195: astore_2       
        //   196: aload_2        
        //   197: ldc             "Share records file not well-formed."
        //   199: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   202: aload_2        
        //   203: athrow         
        //   204: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   207: astore_2       
        //   208: aload_2        
        //   209: ldc_w           "Share records file does not start with historical-records tag."
        //   212: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   215: aload_2        
        //   216: athrow         
        //   217: astore_2       
        //   218: goto            336
        //   221: astore          5
        //   223: getstatic       android/support/v7/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
        //   226: astore          9
        //   228: new             Ljava/lang/StringBuilder;
        //   231: astore_2       
        //   232: aload_2        
        //   233: invokespecial   java/lang/StringBuilder.<init>:()V
        //   236: aload_2        
        //   237: ldc_w           "Error reading historical recrod file: "
        //   240: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   243: pop            
        //   244: aload_2        
        //   245: aload_0        
        //   246: getfield        android/support/v7/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   249: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   252: pop            
        //   253: aload           9
        //   255: aload_2        
        //   256: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   259: aload           5
        //   261: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   264: pop            
        //   265: aload_1        
        //   266: ifnull          335
        //   269: aload_1        
        //   270: invokevirtual   java/io/FileInputStream.close:()V
        //   273: goto            328
        //   276: astore          5
        //   278: getstatic       android/support/v7/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
        //   281: astore          9
        //   283: new             Ljava/lang/StringBuilder;
        //   286: astore_2       
        //   287: aload_2        
        //   288: invokespecial   java/lang/StringBuilder.<init>:()V
        //   291: aload_2        
        //   292: ldc_w           "Error reading historical recrod file: "
        //   295: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   298: pop            
        //   299: aload_2        
        //   300: aload_0        
        //   301: getfield        android/support/v7/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   304: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   307: pop            
        //   308: aload           9
        //   310: aload_2        
        //   311: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   314: aload           5
        //   316: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   319: pop            
        //   320: aload_1        
        //   321: ifnull          335
        //   324: aload_1        
        //   325: invokevirtual   java/io/FileInputStream.close:()V
        //   328: goto            335
        //   331: astore_1       
        //   332: goto            328
        //   335: return         
        //   336: aload_1        
        //   337: ifnull          348
        //   340: aload_1        
        //   341: invokevirtual   java/io/FileInputStream.close:()V
        //   344: goto            348
        //   347: astore_1       
        //   348: aload_2        
        //   349: athrow         
        //   350: astore_1       
        //   351: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  0      12     350    352    Ljava/io/FileNotFoundException;
        //  12     25     276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  12     25     221    276    Ljava/io/IOException;
        //  12     25     217    350    Any
        //  37     44     276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  37     44     221    276    Ljava/io/IOException;
        //  37     44     217    350    Any
        //  47     74     276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  47     74     221    276    Ljava/io/IOException;
        //  47     74     217    350    Any
        //  74     81     276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  74     81     221    276    Ljava/io/IOException;
        //  74     81     217    350    Any
        //  90     94     331    335    Ljava/io/IOException;
        //  110    189    276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  110    189    221    276    Ljava/io/IOException;
        //  110    189    217    350    Any
        //  192    204    276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  192    204    221    276    Ljava/io/IOException;
        //  192    204    217    350    Any
        //  204    217    276    328    Lorg/xmlpull/v1/XmlPullParserException;
        //  204    217    221    276    Ljava/io/IOException;
        //  204    217    217    350    Any
        //  223    265    217    350    Any
        //  269    273    331    335    Ljava/io/IOException;
        //  278    320    217    350    Any
        //  324    328    331    335    Ljava/io/IOException;
        //  340    344    347    348    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0097:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Thread.java:745)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean sortActivitiesIfNeeded() {
        if (this.mActivitySorter != null && this.mIntent != null && !this.mActivities.isEmpty() && !this.mHistoricalRecords.isEmpty()) {
            this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList((List<? extends HistoricalRecord>)this.mHistoricalRecords));
            return true;
        }
        return false;
    }
    
    public Intent chooseActivity(final int n) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == null) {
                return null;
            }
            this.ensureConsistentState();
            final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
            final ComponentName component = new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
            final Intent intent = new Intent(this.mIntent);
            intent.setComponent(component);
            if (this.mActivityChoserModelPolicy != null && this.mActivityChoserModelPolicy.onChooseActivity(this, new Intent(intent))) {
                return null;
            }
            this.addHistoricalRecord(new HistoricalRecord(component, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }
    
    public ResolveInfo getActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.get(n).resolveInfo;
        }
    }
    
    public int getActivityCount() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.size();
        }
    }
    
    public int getActivityIndex(final ResolveInfo resolveInfo) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            final List<ActivityResolveInfo> mActivities = this.mActivities;
            for (int size = mActivities.size(), i = 0; i < size; ++i) {
                if (mActivities.get(i).resolveInfo == resolveInfo) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    public ResolveInfo getDefaultActivity() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            if (!this.mActivities.isEmpty()) {
                return this.mActivities.get(0).resolveInfo;
            }
            return null;
        }
    }
    
    public int getHistorySize() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mHistoricalRecords.size();
        }
    }
    
    public void setDefaultActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
            final ActivityResolveInfo activityResolveInfo2 = this.mActivities.get(0);
            float n2;
            if (activityResolveInfo2 != null) {
                n2 = activityResolveInfo2.weight - activityResolveInfo.weight + 5.0f;
            }
            else {
                n2 = 1.0f;
            }
            this.addHistoricalRecord(new HistoricalRecord(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), System.currentTimeMillis(), n2));
        }
    }
    
    public static final class ActivityResolveInfo implements Comparable<ActivityResolveInfo>
    {
        public final ResolveInfo resolveInfo;
        public float weight;
        
        public ActivityResolveInfo(final ResolveInfo resolveInfo) {
            this.resolveInfo = resolveInfo;
        }
        
        @Override
        public int compareTo(final ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && this.getClass() == o.getClass() && Float.floatToIntBits(this.weight) == Float.floatToIntBits(((ActivityResolveInfo)o).weight));
        }
        
        @Override
        public int hashCode() {
            return 31 + Float.floatToIntBits(this.weight);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:");
            sb.append(this.resolveInfo.toString());
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface ActivitySorter
    {
        void sort(final Intent p0, final List<ActivityResolveInfo> p1, final List<HistoricalRecord> p2);
    }
    
    public static final class HistoricalRecord
    {
        public final ComponentName activity;
        public final long time;
        public final float weight;
        
        public HistoricalRecord(final ComponentName activity, final long time, final float weight) {
            this.activity = activity;
            this.time = time;
            this.weight = weight;
        }
        
        public HistoricalRecord(final String s, final long n, final float n2) {
            this(ComponentName.unflattenFromString(s), n, n2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final HistoricalRecord historicalRecord = (HistoricalRecord)o;
            if (this.activity == null) {
                if (historicalRecord.activity != null) {
                    return false;
                }
            }
            else if (!this.activity.equals((Object)historicalRecord.activity)) {
                return false;
            }
            return this.time == historicalRecord.time && Float.floatToIntBits(this.weight) == Float.floatToIntBits(historicalRecord.weight);
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.activity == null) {
                hashCode = 0;
            }
            else {
                hashCode = this.activity.hashCode();
            }
            return 31 * (31 * (31 * 1 + hashCode) + (int)(this.time ^ this.time >>> 32)) + Float.floatToIntBits(this.weight);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:");
            sb.append(this.activity);
            sb.append("; time:");
            sb.append(this.time);
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface OnChooseActivityListener
    {
        boolean onChooseActivity(final ActivityChooserModel p0, final Intent p1);
    }
    
    private final class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void>
    {
        public Void doInBackground(final Object... p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: iconst_0       
            //     2: aaload         
            //     3: checkcast       Ljava/util/List;
            //     6: astore_2       
            //     7: aload_1        
            //     8: iconst_1       
            //     9: aaload         
            //    10: checkcast       Ljava/lang/String;
            //    13: astore_3       
            //    14: aload_0        
            //    15: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //    18: getfield        android/support/v7/widget/ActivityChooserModel.mContext:Landroid/content/Context;
            //    21: aload_3        
            //    22: iconst_0       
            //    23: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
            //    26: astore_1       
            //    27: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
            //    30: astore_3       
            //    31: aload_3        
            //    32: aload_1        
            //    33: aconst_null    
            //    34: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
            //    39: aload_3        
            //    40: ldc             "UTF-8"
            //    42: iconst_1       
            //    43: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
            //    46: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
            //    51: aload_3        
            //    52: aconst_null    
            //    53: ldc             "historical-records"
            //    55: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //    60: pop            
            //    61: aload_2        
            //    62: invokeinterface java/util/List.size:()I
            //    67: istore          4
            //    69: iconst_0       
            //    70: istore          5
            //    72: iload           5
            //    74: iload           4
            //    76: if_icmpge       171
            //    79: aload_2        
            //    80: iconst_0       
            //    81: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
            //    86: checkcast       Landroid/support/v7/widget/ActivityChooserModel$HistoricalRecord;
            //    89: astore          6
            //    91: aload_3        
            //    92: aconst_null    
            //    93: ldc             "historical-record"
            //    95: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   100: pop            
            //   101: aload_3        
            //   102: aconst_null    
            //   103: ldc             "activity"
            //   105: aload           6
            //   107: getfield        android/support/v7/widget/ActivityChooserModel$HistoricalRecord.activity:Landroid/content/ComponentName;
            //   110: invokevirtual   android/content/ComponentName.flattenToString:()Ljava/lang/String;
            //   113: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   118: pop            
            //   119: aload_3        
            //   120: aconst_null    
            //   121: ldc             "time"
            //   123: aload           6
            //   125: getfield        android/support/v7/widget/ActivityChooserModel$HistoricalRecord.time:J
            //   128: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
            //   131: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   136: pop            
            //   137: aload_3        
            //   138: aconst_null    
            //   139: ldc             "weight"
            //   141: aload           6
            //   143: getfield        android/support/v7/widget/ActivityChooserModel$HistoricalRecord.weight:F
            //   146: invokestatic    java/lang/String.valueOf:(F)Ljava/lang/String;
            //   149: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   154: pop            
            //   155: aload_3        
            //   156: aconst_null    
            //   157: ldc             "historical-record"
            //   159: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   164: pop            
            //   165: iinc            5, 1
            //   168: goto            72
            //   171: aload_3        
            //   172: aconst_null    
            //   173: ldc             "historical-records"
            //   175: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   180: pop            
            //   181: aload_3        
            //   182: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
            //   187: aload_0        
            //   188: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   191: iconst_1       
            //   192: putfield        android/support/v7/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   195: aload_1        
            //   196: ifnull          409
            //   199: aload_1        
            //   200: invokevirtual   java/io/FileOutputStream.close:()V
            //   203: goto            402
            //   206: astore_2       
            //   207: goto            411
            //   210: astore_3       
            //   211: getstatic       android/support/v7/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   214: astore          6
            //   216: new             Ljava/lang/StringBuilder;
            //   219: astore_2       
            //   220: aload_2        
            //   221: invokespecial   java/lang/StringBuilder.<init>:()V
            //   224: aload_2        
            //   225: ldc             "Error writing historical record file: "
            //   227: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   230: pop            
            //   231: aload_2        
            //   232: aload_0        
            //   233: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   236: getfield        android/support/v7/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   239: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   242: pop            
            //   243: aload           6
            //   245: aload_2        
            //   246: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   249: aload_3        
            //   250: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   253: pop            
            //   254: aload_0        
            //   255: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   258: iconst_1       
            //   259: putfield        android/support/v7/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   262: aload_1        
            //   263: ifnull          409
            //   266: aload_1        
            //   267: invokevirtual   java/io/FileOutputStream.close:()V
            //   270: goto            402
            //   273: astore_2       
            //   274: getstatic       android/support/v7/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   277: astore_3       
            //   278: new             Ljava/lang/StringBuilder;
            //   281: astore          6
            //   283: aload           6
            //   285: invokespecial   java/lang/StringBuilder.<init>:()V
            //   288: aload           6
            //   290: ldc             "Error writing historical record file: "
            //   292: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   295: pop            
            //   296: aload           6
            //   298: aload_0        
            //   299: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   302: getfield        android/support/v7/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   305: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   308: pop            
            //   309: aload_3        
            //   310: aload           6
            //   312: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   315: aload_2        
            //   316: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   319: pop            
            //   320: aload_0        
            //   321: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   324: iconst_1       
            //   325: putfield        android/support/v7/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   328: aload_1        
            //   329: ifnull          409
            //   332: aload_1        
            //   333: invokevirtual   java/io/FileOutputStream.close:()V
            //   336: goto            402
            //   339: astore_3       
            //   340: getstatic       android/support/v7/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   343: astore_2       
            //   344: new             Ljava/lang/StringBuilder;
            //   347: astore          6
            //   349: aload           6
            //   351: invokespecial   java/lang/StringBuilder.<init>:()V
            //   354: aload           6
            //   356: ldc             "Error writing historical record file: "
            //   358: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   361: pop            
            //   362: aload           6
            //   364: aload_0        
            //   365: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   368: getfield        android/support/v7/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   371: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   374: pop            
            //   375: aload_2        
            //   376: aload           6
            //   378: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   381: aload_3        
            //   382: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   385: pop            
            //   386: aload_0        
            //   387: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   390: iconst_1       
            //   391: putfield        android/support/v7/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   394: aload_1        
            //   395: ifnull          409
            //   398: aload_1        
            //   399: invokevirtual   java/io/FileOutputStream.close:()V
            //   402: goto            409
            //   405: astore_1       
            //   406: goto            402
            //   409: aconst_null    
            //   410: areturn        
            //   411: aload_0        
            //   412: getfield        android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroid/support/v7/widget/ActivityChooserModel;
            //   415: iconst_1       
            //   416: putfield        android/support/v7/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   419: aload_1        
            //   420: ifnull          431
            //   423: aload_1        
            //   424: invokevirtual   java/io/FileOutputStream.close:()V
            //   427: goto            431
            //   430: astore_1       
            //   431: aload_2        
            //   432: athrow         
            //   433: astore          6
            //   435: getstatic       android/support/v7/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   438: astore_1       
            //   439: new             Ljava/lang/StringBuilder;
            //   442: dup            
            //   443: invokespecial   java/lang/StringBuilder.<init>:()V
            //   446: astore_2       
            //   447: aload_2        
            //   448: ldc             "Error writing historical record file: "
            //   450: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   453: pop            
            //   454: aload_2        
            //   455: aload_3        
            //   456: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   459: pop            
            //   460: aload_1        
            //   461: aload_2        
            //   462: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   465: aload           6
            //   467: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   470: pop            
            //   471: aconst_null    
            //   472: areturn        
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                
            //  -----  -----  -----  -----  ------------------------------------
            //  14     27     433    473    Ljava/io/FileNotFoundException;
            //  31     69     339    402    Ljava/lang/IllegalArgumentException;
            //  31     69     273    339    Ljava/lang/IllegalStateException;
            //  31     69     210    273    Ljava/io/IOException;
            //  31     69     206    433    Any
            //  79     165    339    402    Ljava/lang/IllegalArgumentException;
            //  79     165    273    339    Ljava/lang/IllegalStateException;
            //  79     165    210    273    Ljava/io/IOException;
            //  79     165    206    433    Any
            //  171    187    339    402    Ljava/lang/IllegalArgumentException;
            //  171    187    273    339    Ljava/lang/IllegalStateException;
            //  171    187    210    273    Ljava/io/IOException;
            //  171    187    206    433    Any
            //  199    203    405    409    Ljava/io/IOException;
            //  211    254    206    433    Any
            //  266    270    405    409    Ljava/io/IOException;
            //  274    320    206    433    Any
            //  332    336    405    409    Ljava/io/IOException;
            //  340    386    206    433    Any
            //  398    402    405    409    Ljava/io/IOException;
            //  423    427    430    431    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index: 235, Size: 235
            //     at java.util.ArrayList.rangeCheck(ArrayList.java:653)
            //     at java.util.ArrayList.get(ArrayList.java:429)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Thread.java:745)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}
