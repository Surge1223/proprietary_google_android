package android.support.v7.widget;

import android.view.ViewDebug$ExportedProperty;
import android.view.ContextThemeWrapper;
import android.content.res.Configuration;
import android.view.MenuItem;
import android.support.v7.view.menu.MenuItemImpl;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.accessibility.AccessibilityEvent;
import android.view.ViewGroup.LayoutParams;
import android.support.v7.view.menu.ActionMenuItemView;
import android.view.View$MeasureSpec;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.MenuBuilder;

public class ActionMenuView extends LinearLayoutCompat implements ItemInvoker, MenuView
{
    private MenuPresenter.Callback mActionMenuPresenterCallback;
    private boolean mFormatItems;
    private int mFormatItemsWidth;
    private int mGeneratedItemPadding;
    private MenuBuilder mMenu;
    Callback mMenuBuilderCallback;
    private int mMinCellSize;
    OnMenuItemClickListener mOnMenuItemClickListener;
    private Context mPopupContext;
    private int mPopupTheme;
    private ActionMenuPresenter mPresenter;
    private boolean mReserveOverflow;
    
    public ActionMenuView(final Context context) {
        this(context, null);
    }
    
    public ActionMenuView(final Context mPopupContext, final AttributeSet set) {
        super(mPopupContext, set);
        this.setBaselineAligned(false);
        final float density = mPopupContext.getResources().getDisplayMetrics().density;
        this.mMinCellSize = (int)(56.0f * density);
        this.mGeneratedItemPadding = (int)(4.0f * density);
        this.mPopupContext = mPopupContext;
        this.mPopupTheme = 0;
    }
    
    static int measureChildForCells(final View view, final int n, int n2, int cellsUsed, int n3) {
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(View$MeasureSpec.getSize(cellsUsed) - n3, View$MeasureSpec.getMode(cellsUsed));
        ActionMenuItemView actionMenuItemView;
        if (view instanceof ActionMenuItemView) {
            actionMenuItemView = (ActionMenuItemView)view;
        }
        else {
            actionMenuItemView = null;
        }
        final boolean b = false;
        if (actionMenuItemView != null && actionMenuItemView.hasText()) {
            n3 = 1;
        }
        else {
            n3 = 0;
        }
        final int n4 = cellsUsed = 0;
        Label_0146: {
            if (n2 > 0) {
                if (n3 != 0) {
                    cellsUsed = n4;
                    if (n2 < 2) {
                        break Label_0146;
                    }
                }
                view.measure(View$MeasureSpec.makeMeasureSpec(n * n2, Integer.MIN_VALUE), measureSpec);
                final int measuredWidth = view.getMeasuredWidth();
                cellsUsed = (n2 = measuredWidth / n);
                if (measuredWidth % n != 0) {
                    n2 = cellsUsed + 1;
                }
                cellsUsed = n2;
                if (n3 != 0 && (cellsUsed = n2) < 2) {
                    cellsUsed = 2;
                }
            }
        }
        boolean expandable = b;
        if (!layoutParams.isOverflowButton) {
            expandable = b;
            if (n3 != 0) {
                expandable = true;
            }
        }
        layoutParams.expandable = expandable;
        layoutParams.cellsUsed = cellsUsed;
        view.measure(View$MeasureSpec.makeMeasureSpec(cellsUsed * n, 1073741824), measureSpec);
        return cellsUsed;
    }
    
    private void onMeasureExactFormat(int i, int max) {
        final int mode = View$MeasureSpec.getMode(max);
        i = View$MeasureSpec.getSize(i);
        final int size = View$MeasureSpec.getSize(max);
        final int n = this.getPaddingLeft() + this.getPaddingRight();
        final int n2 = this.getPaddingTop() + this.getPaddingBottom();
        final int childMeasureSpec = getChildMeasureSpec(max, n2, -2);
        final int n3 = i - n;
        final int n4 = n3 / this.mMinCellSize;
        final int n5 = n3 % this.mMinCellSize;
        if (n4 == 0) {
            this.setMeasuredDimension(n3, 0);
            return;
        }
        final int n6 = this.mMinCellSize + n5 / n4;
        boolean b = false;
        long n7 = 0L;
        final int childCount;
        final int n8 = childCount = this.getChildCount();
        max = 0;
        int n9 = 0;
        int n10 = 0;
        int max2 = 0;
        i = n4;
        for (int j = 0; j < childCount; ++j) {
            final View child = this.getChildAt(j);
            if (child.getVisibility() != 8) {
                final boolean b2 = child instanceof ActionMenuItemView;
                ++n9;
                if (b2) {
                    child.setPadding(this.mGeneratedItemPadding, 0, this.mGeneratedItemPadding, 0);
                }
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                layoutParams.expanded = false;
                layoutParams.extraPixels = 0;
                layoutParams.cellsUsed = 0;
                layoutParams.expandable = false;
                layoutParams.leftMargin = 0;
                layoutParams.rightMargin = 0;
                layoutParams.preventEdgeOffset = (b2 && ((ActionMenuItemView)child).hasText());
                int n11;
                if (layoutParams.isOverflowButton) {
                    n11 = 1;
                }
                else {
                    n11 = i;
                }
                final int measureChildForCells = measureChildForCells(child, n6, n11, childMeasureSpec, n2);
                max2 = Math.max(max2, measureChildForCells);
                int n12 = n10;
                if (layoutParams.expandable) {
                    n12 = n10 + 1;
                }
                if (layoutParams.isOverflowButton) {
                    b = true;
                }
                i -= measureChildForCells;
                max = Math.max(max, child.getMeasuredHeight());
                if (measureChildForCells == 1) {
                    n7 |= 1 << j;
                    n10 = n12;
                }
                else {
                    n10 = n12;
                }
            }
        }
        final boolean b3 = b && n9 == 2;
        final int n13 = 0;
        int n14 = i;
        i = n13;
        final boolean b4 = b3;
        final int n15 = n3;
        while (n10 > 0 && n14 > 0) {
            long n16 = 0L;
            int n17 = Integer.MAX_VALUE;
            final int n18 = 0;
            int k = 0;
            final int n19 = i;
            i = n18;
            while (k < childCount) {
                final LayoutParams layoutParams2 = (LayoutParams)this.getChildAt(k).getLayoutParams();
                int n20;
                int cellsUsed;
                long n21;
                if (!layoutParams2.expandable) {
                    n20 = i;
                    cellsUsed = n17;
                    n21 = n16;
                }
                else if (layoutParams2.cellsUsed < n17) {
                    cellsUsed = layoutParams2.cellsUsed;
                    n21 = 1L << k;
                    n20 = 1;
                }
                else {
                    n20 = i;
                    cellsUsed = n17;
                    n21 = n16;
                    if (layoutParams2.cellsUsed == n17) {
                        n21 = (n16 | 1L << k);
                        n20 = i + 1;
                        cellsUsed = n17;
                    }
                }
                ++k;
                i = n20;
                n17 = cellsUsed;
                n16 = n21;
            }
            n7 |= n16;
            if (i > n14) {
                i = n19;
                break;
            }
            long n22;
            for (int l = 0; l < childCount; ++l, n14 = i, n7 = n22) {
                final View child2 = this.getChildAt(l);
                final LayoutParams layoutParams3 = (LayoutParams)child2.getLayoutParams();
                if ((n16 & 1 << l) == 0x0L) {
                    i = n14;
                    n22 = n7;
                    if (layoutParams3.cellsUsed == n17 + 1) {
                        n22 = (n7 | 1 << l);
                        i = n14;
                    }
                }
                else {
                    if (b4 && layoutParams3.preventEdgeOffset && n14 == 1) {
                        child2.setPadding(this.mGeneratedItemPadding + n6, 0, this.mGeneratedItemPadding, 0);
                    }
                    ++layoutParams3.cellsUsed;
                    layoutParams3.expanded = true;
                    i = n14 - 1;
                    n22 = n7;
                }
            }
            i = 1;
        }
        final boolean b5 = !b && n9 == 1;
        int n23;
        if (n14 > 0 && n7 != 0L) {
            if (n14 >= n9 - 1 && !b5 && max2 <= 1) {
                n23 = childCount;
            }
            else {
                float n24 = Long.bitCount(n7);
                if (!b5) {
                    float n25 = n24;
                    if ((0x1L & n7) != 0x0L) {
                        n25 = n24;
                        if (!((LayoutParams)this.getChildAt(0).getLayoutParams()).preventEdgeOffset) {
                            n25 = n24 - 0.5f;
                        }
                    }
                    n24 = n25;
                    if ((n7 & 1 << childCount - 1) != 0x0L) {
                        n24 = n25;
                        if (!((LayoutParams)this.getChildAt(childCount - 1).getLayoutParams()).preventEdgeOffset) {
                            n24 = n25 - 0.5f;
                        }
                    }
                }
                int n26 = n8;
                int n27;
                if (n24 > 0.0f) {
                    n27 = (int)(n14 * n6 / n24);
                }
                else {
                    n27 = 0;
                }
                int n28 = 0;
                while (true) {
                    n23 = n26;
                    if (n28 >= n23) {
                        break;
                    }
                    int n29 = 0;
                    Label_1142: {
                        if ((1 << n28 & n7) != 0x0L) {
                            final View child3 = this.getChildAt(n28);
                            final LayoutParams layoutParams4 = (LayoutParams)child3.getLayoutParams();
                            if (child3 instanceof ActionMenuItemView) {
                                layoutParams4.extraPixels = n27;
                                layoutParams4.expanded = true;
                                if (n28 == 0 && !layoutParams4.preventEdgeOffset) {
                                    layoutParams4.leftMargin = -n27 / 2;
                                }
                                i = 1;
                            }
                            else {
                                if (layoutParams4.isOverflowButton) {
                                    layoutParams4.extraPixels = n27;
                                    layoutParams4.expanded = true;
                                    layoutParams4.rightMargin = -n27 / 2;
                                    n29 = 1;
                                    break Label_1142;
                                }
                                if (n28 != 0) {
                                    layoutParams4.leftMargin = n27 / 2;
                                }
                                n29 = i;
                                if (n28 != n23 - 1) {
                                    layoutParams4.rightMargin = n27 / 2;
                                    n29 = i;
                                }
                                break Label_1142;
                            }
                        }
                        n29 = i;
                    }
                    ++n28;
                    i = n29;
                    n26 = n23;
                }
            }
        }
        else {
            n23 = childCount;
        }
        if (i != 0) {
            View child4;
            LayoutParams layoutParams5;
            for (i = 0; i < n23; ++i) {
                child4 = this.getChildAt(i);
                layoutParams5 = (LayoutParams)child4.getLayoutParams();
                if (layoutParams5.expanded) {
                    child4.measure(View$MeasureSpec.makeMeasureSpec(layoutParams5.cellsUsed * n6 + layoutParams5.extraPixels, 1073741824), childMeasureSpec);
                }
            }
        }
        if (mode != 1073741824) {
            i = max;
        }
        else {
            i = size;
        }
        this.setMeasuredDimension(n15, i);
    }
    
    @Override
    protected boolean checkLayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        return viewGroup.LayoutParams != null && viewGroup.LayoutParams instanceof LayoutParams;
    }
    
    public void dismissPopupMenus() {
        if (this.mPresenter != null) {
            this.mPresenter.dismissPopupMenus();
        }
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        return false;
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        final LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 16;
        return layoutParams;
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    protected LayoutParams generateLayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        if (viewGroup.LayoutParams != null) {
            LayoutParams layoutParams;
            if (viewGroup.LayoutParams instanceof LayoutParams) {
                layoutParams = new LayoutParams((LayoutParams)viewGroup.LayoutParams);
            }
            else {
                layoutParams = new LayoutParams(viewGroup.LayoutParams);
            }
            if (layoutParams.gravity <= 0) {
                layoutParams.gravity = 16;
            }
            return layoutParams;
        }
        return this.generateDefaultLayoutParams();
    }
    
    public LayoutParams generateOverflowButtonLayoutParams() {
        final LayoutParams generateDefaultLayoutParams = this.generateDefaultLayoutParams();
        generateDefaultLayoutParams.isOverflowButton = true;
        return generateDefaultLayoutParams;
    }
    
    public Menu getMenu() {
        if (this.mMenu == null) {
            final Context context = this.getContext();
            (this.mMenu = new MenuBuilder(context)).setCallback((MenuBuilder.Callback)new MenuBuilderCallback());
            (this.mPresenter = new ActionMenuPresenter(context)).setReserveOverflow(true);
            final ActionMenuPresenter mPresenter = this.mPresenter;
            MenuPresenter.Callback mActionMenuPresenterCallback;
            if (this.mActionMenuPresenterCallback != null) {
                mActionMenuPresenterCallback = this.mActionMenuPresenterCallback;
            }
            else {
                mActionMenuPresenterCallback = new ActionMenuPresenterCallback();
            }
            mPresenter.setCallback(mActionMenuPresenterCallback);
            this.mMenu.addMenuPresenter(this.mPresenter, this.mPopupContext);
            this.mPresenter.setMenuView(this);
        }
        return (Menu)this.mMenu;
    }
    
    public Drawable getOverflowIcon() {
        this.getMenu();
        return this.mPresenter.getOverflowIcon();
    }
    
    public int getPopupTheme() {
        return this.mPopupTheme;
    }
    
    public int getWindowAnimations() {
        return 0;
    }
    
    protected boolean hasSupportDividerBeforeChildAt(final int n) {
        if (n == 0) {
            return false;
        }
        final View child = this.getChildAt(n - 1);
        final View child2 = this.getChildAt(n);
        boolean b2;
        final boolean b = b2 = false;
        if (n < this.getChildCount()) {
            b2 = b;
            if (child instanceof ActionMenuChildView) {
                b2 = (false | ((ActionMenuChildView)child).needsDividerAfter());
            }
        }
        boolean b3 = b2;
        if (n > 0) {
            b3 = b2;
            if (child2 instanceof ActionMenuChildView) {
                b3 = (b2 | ((ActionMenuChildView)child2).needsDividerBefore());
            }
        }
        return b3;
    }
    
    public boolean hideOverflowMenu() {
        return this.mPresenter != null && this.mPresenter.hideOverflowMenu();
    }
    
    @Override
    public void initialize(final MenuBuilder mMenu) {
        this.mMenu = mMenu;
    }
    
    @Override
    public boolean invokeItem(final MenuItemImpl menuItemImpl) {
        return this.mMenu.performItemAction((MenuItem)menuItemImpl, 0);
    }
    
    public boolean isOverflowMenuShowPending() {
        return this.mPresenter != null && this.mPresenter.isOverflowMenuShowPending();
    }
    
    public boolean isOverflowMenuShowing() {
        return this.mPresenter != null && this.mPresenter.isOverflowMenuShowing();
    }
    
    public boolean isOverflowReserved() {
        return this.mReserveOverflow;
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.mPresenter != null) {
            this.mPresenter.updateMenuView(false);
            if (this.mPresenter.isOverflowMenuShowing()) {
                this.mPresenter.hideOverflowMenu();
                this.mPresenter.showOverflowMenu();
            }
        }
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.dismissPopupMenus();
    }
    
    @Override
    protected void onLayout(final boolean b, int i, int n, int paddingLeft, int j) {
        if (!this.mFormatItems) {
            super.onLayout(b, i, n, paddingLeft, j);
            return;
        }
        final int childCount = this.getChildCount();
        final int n2 = (j - n) / 2;
        final int dividerWidth = this.getDividerWidth();
        int n3 = 0;
        j = this.getPaddingRight();
        n = this.getPaddingLeft();
        boolean b2 = false;
        final boolean layoutRtl = ViewUtils.isLayoutRtl((View)this);
        j = paddingLeft - i - j - n;
        int n4 = 0;
        n = 0;
        for (int k = 0; k < childCount; ++k) {
            final View child = this.getChildAt(k);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                if (layoutParams.isOverflowButton) {
                    final int n5 = n = child.getMeasuredWidth();
                    if (this.hasSupportDividerBeforeChildAt(k)) {
                        n = n5 + dividerWidth;
                    }
                    final int measuredHeight = child.getMeasuredHeight();
                    int n6;
                    int n7;
                    if (layoutRtl) {
                        n6 = this.getPaddingLeft() + layoutParams.leftMargin;
                        n7 = n6 + n;
                    }
                    else {
                        n7 = this.getWidth() - this.getPaddingRight() - layoutParams.rightMargin;
                        n6 = n7 - n;
                    }
                    final int n8 = n2 - measuredHeight / 2;
                    child.layout(n6, n8, n7, n8 + measuredHeight);
                    j -= n;
                    b2 = true;
                }
                else {
                    final int n9 = child.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin;
                    final int n10 = n4 + n9;
                    j -= n9;
                    n4 = n10;
                    if (this.hasSupportDividerBeforeChildAt(k)) {
                        n4 = n10 + dividerWidth;
                    }
                    ++n3;
                }
            }
        }
        final int n11 = 1;
        if (childCount == 1 && !b2) {
            final View child2 = this.getChildAt(0);
            j = child2.getMeasuredWidth();
            n = child2.getMeasuredHeight();
            i = (paddingLeft - i) / 2 - j / 2;
            paddingLeft = n2 - n / 2;
            child2.layout(i, paddingLeft, i + j, paddingLeft + n);
            return;
        }
        i = n11;
        if (b2) {
            i = 0;
        }
        paddingLeft = n3 - i;
        if (paddingLeft > 0) {
            i = j / paddingLeft;
        }
        else {
            i = 0;
        }
        final int n12 = 0;
        j = 0;
        final int max = Math.max(0, i);
        if (layoutRtl) {
            int n13 = this.getWidth() - this.getPaddingRight();
            i = dividerWidth;
            while (j < childCount) {
                final View child3 = this.getChildAt(j);
                final LayoutParams layoutParams2 = (LayoutParams)child3.getLayoutParams();
                if (child3.getVisibility() != 8) {
                    if (!layoutParams2.isOverflowButton) {
                        final int n14 = n13 - layoutParams2.rightMargin;
                        final int measuredWidth = child3.getMeasuredWidth();
                        final int measuredHeight2 = child3.getMeasuredHeight();
                        final int n15 = n2 - measuredHeight2 / 2;
                        child3.layout(n14 - measuredWidth, n15, n14, n15 + measuredHeight2);
                        n13 = n14 - (layoutParams2.leftMargin + measuredWidth + max);
                    }
                }
                ++j;
            }
        }
        else {
            paddingLeft = this.getPaddingLeft();
            View child4;
            LayoutParams layoutParams3;
            int n16;
            for (i = n12; i < childCount; ++i, paddingLeft = n) {
                child4 = this.getChildAt(i);
                layoutParams3 = (LayoutParams)child4.getLayoutParams();
                n = paddingLeft;
                if (child4.getVisibility() != 8) {
                    if (layoutParams3.isOverflowButton) {
                        n = paddingLeft;
                    }
                    else {
                        n16 = paddingLeft + layoutParams3.leftMargin;
                        j = child4.getMeasuredWidth();
                        n = child4.getMeasuredHeight();
                        paddingLeft = n2 - n / 2;
                        child4.layout(n16, paddingLeft, n16 + j, paddingLeft + n);
                        n = n16 + (layoutParams3.rightMargin + j + max);
                    }
                }
            }
        }
    }
    
    @Override
    protected void onMeasure(final int n, final int n2) {
        final boolean mFormatItems = this.mFormatItems;
        this.mFormatItems = (View$MeasureSpec.getMode(n) == 1073741824);
        if (mFormatItems != this.mFormatItems) {
            this.mFormatItemsWidth = 0;
        }
        final int size = View$MeasureSpec.getSize(n);
        if (this.mFormatItems && this.mMenu != null && size != this.mFormatItemsWidth) {
            this.mFormatItemsWidth = size;
            this.mMenu.onItemsChanged(true);
        }
        final int childCount = this.getChildCount();
        if (this.mFormatItems && childCount > 0) {
            this.onMeasureExactFormat(n, n2);
        }
        else {
            for (int i = 0; i < childCount; ++i) {
                final LayoutParams layoutParams = (LayoutParams)this.getChildAt(i).getLayoutParams();
                layoutParams.rightMargin = 0;
                layoutParams.leftMargin = 0;
            }
            super.onMeasure(n, n2);
        }
    }
    
    public MenuBuilder peekMenu() {
        return this.mMenu;
    }
    
    public void setExpandedActionViewsExclusive(final boolean expandedActionViewsExclusive) {
        this.mPresenter.setExpandedActionViewsExclusive(expandedActionViewsExclusive);
    }
    
    public void setMenuCallbacks(final MenuPresenter.Callback mActionMenuPresenterCallback, final Callback mMenuBuilderCallback) {
        this.mActionMenuPresenterCallback = mActionMenuPresenterCallback;
        this.mMenuBuilderCallback = mMenuBuilderCallback;
    }
    
    public void setOnMenuItemClickListener(final OnMenuItemClickListener mOnMenuItemClickListener) {
        this.mOnMenuItemClickListener = mOnMenuItemClickListener;
    }
    
    public void setOverflowIcon(final Drawable overflowIcon) {
        this.getMenu();
        this.mPresenter.setOverflowIcon(overflowIcon);
    }
    
    public void setOverflowReserved(final boolean mReserveOverflow) {
        this.mReserveOverflow = mReserveOverflow;
    }
    
    public void setPopupTheme(final int mPopupTheme) {
        if (this.mPopupTheme != mPopupTheme) {
            if ((this.mPopupTheme = mPopupTheme) == 0) {
                this.mPopupContext = this.getContext();
            }
            else {
                this.mPopupContext = (Context)new ContextThemeWrapper(this.getContext(), mPopupTheme);
            }
        }
    }
    
    public void setPresenter(final ActionMenuPresenter mPresenter) {
        (this.mPresenter = mPresenter).setMenuView(this);
    }
    
    public boolean showOverflowMenu() {
        return this.mPresenter != null && this.mPresenter.showOverflowMenu();
    }
    
    public interface ActionMenuChildView
    {
        boolean needsDividerAfter();
        
        boolean needsDividerBefore();
    }
    
    private static class ActionMenuPresenterCallback implements MenuPresenter.Callback
    {
        @Override
        public void onCloseMenu(final MenuBuilder menuBuilder, final boolean b) {
        }
        
        @Override
        public boolean onOpenSubMenu(final MenuBuilder menuBuilder) {
            return false;
        }
    }
    
    public static class LayoutParams extends LinearLayoutCompat.LayoutParams
    {
        @ViewDebug$ExportedProperty
        public int cellsUsed;
        @ViewDebug$ExportedProperty
        public boolean expandable;
        boolean expanded;
        @ViewDebug$ExportedProperty
        public int extraPixels;
        @ViewDebug$ExportedProperty
        public boolean isOverflowButton;
        @ViewDebug$ExportedProperty
        public boolean preventEdgeOffset;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.isOverflowButton = false;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup.LayoutParams)layoutParams);
            this.isOverflowButton = layoutParams.isOverflowButton;
        }
        
        public LayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
            super(viewGroup.LayoutParams);
        }
    }
    
    private class MenuBuilderCallback implements Callback
    {
        @Override
        public boolean onMenuItemSelected(final MenuBuilder menuBuilder, final MenuItem menuItem) {
            return ActionMenuView.this.mOnMenuItemClickListener != null && ActionMenuView.this.mOnMenuItemClickListener.onMenuItemClick(menuItem);
        }
        
        @Override
        public void onMenuModeChange(final MenuBuilder menuBuilder) {
            if (ActionMenuView.this.mMenuBuilderCallback != null) {
                ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(menuBuilder);
            }
        }
    }
    
    public interface OnMenuItemClickListener
    {
        boolean onMenuItemClick(final MenuItem p0);
    }
}
