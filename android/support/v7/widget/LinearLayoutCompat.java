package android.support.v7.widget;

import android.content.res.TypedArray;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityEvent;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.graphics.Canvas;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.view.View$MeasureSpec;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;

public class LinearLayoutCompat extends ViewGroup
{
    private boolean mBaselineAligned;
    private int mBaselineAlignedChildIndex;
    private int mBaselineChildTop;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;
    private int mGravity;
    private int[] mMaxAscent;
    private int[] mMaxDescent;
    private int mOrientation;
    private int mShowDividers;
    private int mTotalLength;
    private boolean mUseLargestChild;
    private float mWeightSum;
    
    public LinearLayoutCompat(final Context context) {
        this(context, null);
    }
    
    public LinearLayoutCompat(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public LinearLayoutCompat(final Context context, final AttributeSet set, int n) {
        super(context, set, n);
        this.mBaselineAligned = true;
        this.mBaselineAlignedChildIndex = -1;
        this.mBaselineChildTop = 0;
        this.mGravity = 8388659;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.LinearLayoutCompat, n, 0);
        n = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_orientation, -1);
        if (n >= 0) {
            this.setOrientation(n);
        }
        n = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_gravity, -1);
        if (n >= 0) {
            this.setGravity(n);
        }
        final boolean boolean1 = obtainStyledAttributes.getBoolean(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
        if (!boolean1) {
            this.setBaselineAligned(boolean1);
        }
        this.mWeightSum = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_android_weightSum, -1.0f);
        this.mBaselineAlignedChildIndex = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.mUseLargestChild = obtainStyledAttributes.getBoolean(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
        this.setDividerDrawable(obtainStyledAttributes.getDrawable(R.styleable.LinearLayoutCompat_divider));
        this.mShowDividers = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_showDividers, 0);
        this.mDividerPadding = obtainStyledAttributes.getDimensionPixelSize(R.styleable.LinearLayoutCompat_dividerPadding, 0);
        obtainStyledAttributes.recycle();
    }
    
    private void forceUniformHeight(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.getMeasuredHeight(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                if (layoutParams.height == -1) {
                    final int width = layoutParams.width;
                    layoutParams.width = virtualChild.getMeasuredWidth();
                    this.measureChildWithMargins(virtualChild, n2, 0, measureSpec, 0);
                    layoutParams.width = width;
                }
            }
        }
    }
    
    private void forceUniformWidth(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.getMeasuredWidth(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                if (layoutParams.width == -1) {
                    final int height = layoutParams.height;
                    layoutParams.height = virtualChild.getMeasuredHeight();
                    this.measureChildWithMargins(virtualChild, measureSpec, 0, n2, 0);
                    layoutParams.height = height;
                }
            }
        }
    }
    
    private void setChildFrame(final View view, final int n, final int n2, final int n3, final int n4) {
        view.layout(n, n2, n + n3, n2 + n4);
    }
    
    protected boolean checkLayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        return viewGroup.LayoutParams instanceof LayoutParams;
    }
    
    void drawDividersHorizontal(final Canvas canvas) {
        final int virtualChildCount = this.getVirtualChildCount();
        final boolean layoutRtl = ViewUtils.isLayoutRtl((View)this);
        for (int i = 0; i < virtualChildCount; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild != null && virtualChild.getVisibility() != 8 && this.hasDividerBeforeChildAt(i)) {
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                int n;
                if (layoutRtl) {
                    n = virtualChild.getRight() + layoutParams.rightMargin;
                }
                else {
                    n = virtualChild.getLeft() - layoutParams.leftMargin - this.mDividerWidth;
                }
                this.drawVerticalDivider(canvas, n);
            }
        }
        if (this.hasDividerBeforeChildAt(virtualChildCount)) {
            final View virtualChild2 = this.getVirtualChildAt(virtualChildCount - 1);
            int paddingLeft;
            if (virtualChild2 == null) {
                if (layoutRtl) {
                    paddingLeft = this.getPaddingLeft();
                }
                else {
                    paddingLeft = this.getWidth() - this.getPaddingRight() - this.mDividerWidth;
                }
            }
            else {
                final LayoutParams layoutParams2 = (LayoutParams)virtualChild2.getLayoutParams();
                if (layoutRtl) {
                    paddingLeft = virtualChild2.getLeft() - layoutParams2.leftMargin - this.mDividerWidth;
                }
                else {
                    paddingLeft = virtualChild2.getRight() + layoutParams2.rightMargin;
                }
            }
            this.drawVerticalDivider(canvas, paddingLeft);
        }
    }
    
    void drawDividersVertical(final Canvas canvas) {
        final int virtualChildCount = this.getVirtualChildCount();
        for (int i = 0; i < virtualChildCount; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild != null && virtualChild.getVisibility() != 8 && this.hasDividerBeforeChildAt(i)) {
                this.drawHorizontalDivider(canvas, virtualChild.getTop() - ((LayoutParams)virtualChild.getLayoutParams()).topMargin - this.mDividerHeight);
            }
        }
        if (this.hasDividerBeforeChildAt(virtualChildCount)) {
            final View virtualChild2 = this.getVirtualChildAt(virtualChildCount - 1);
            int n;
            if (virtualChild2 == null) {
                n = this.getHeight() - this.getPaddingBottom() - this.mDividerHeight;
            }
            else {
                n = virtualChild2.getBottom() + ((LayoutParams)virtualChild2.getLayoutParams()).bottomMargin;
            }
            this.drawHorizontalDivider(canvas, n);
        }
    }
    
    void drawHorizontalDivider(final Canvas canvas, final int n) {
        this.mDivider.setBounds(this.getPaddingLeft() + this.mDividerPadding, n, this.getWidth() - this.getPaddingRight() - this.mDividerPadding, this.mDividerHeight + n);
        this.mDivider.draw(canvas);
    }
    
    void drawVerticalDivider(final Canvas canvas, final int n) {
        this.mDivider.setBounds(n, this.getPaddingTop() + this.mDividerPadding, this.mDividerWidth + n, this.getHeight() - this.getPaddingBottom() - this.mDividerPadding);
        this.mDivider.draw(canvas);
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        if (this.mOrientation == 0) {
            return new LayoutParams(-2, -2);
        }
        if (this.mOrientation == 1) {
            return new LayoutParams(-1, -2);
        }
        return null;
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    protected LayoutParams generateLayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        return new LayoutParams(viewGroup.LayoutParams);
    }
    
    public int getBaseline() {
        if (this.mBaselineAlignedChildIndex < 0) {
            return super.getBaseline();
        }
        if (this.getChildCount() <= this.mBaselineAlignedChildIndex) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        final View child = this.getChildAt(this.mBaselineAlignedChildIndex);
        final int baseline = child.getBaseline();
        if (baseline != -1) {
            int mBaselineChildTop;
            final int n = mBaselineChildTop = this.mBaselineChildTop;
            if (this.mOrientation == 1) {
                final int n2 = this.mGravity & 0x70;
                mBaselineChildTop = n;
                if (n2 != 48) {
                    if (n2 != 16) {
                        if (n2 != 80) {
                            mBaselineChildTop = n;
                        }
                        else {
                            mBaselineChildTop = this.getBottom() - this.getTop() - this.getPaddingBottom() - this.mTotalLength;
                        }
                    }
                    else {
                        mBaselineChildTop = n + (this.getBottom() - this.getTop() - this.getPaddingTop() - this.getPaddingBottom() - this.mTotalLength) / 2;
                    }
                }
            }
            return ((LayoutParams)child.getLayoutParams()).topMargin + mBaselineChildTop + baseline;
        }
        if (this.mBaselineAlignedChildIndex == 0) {
            return -1;
        }
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
    }
    
    public int getBaselineAlignedChildIndex() {
        return this.mBaselineAlignedChildIndex;
    }
    
    int getChildrenSkipCount(final View view, final int n) {
        return 0;
    }
    
    public Drawable getDividerDrawable() {
        return this.mDivider;
    }
    
    public int getDividerPadding() {
        return this.mDividerPadding;
    }
    
    public int getDividerWidth() {
        return this.mDividerWidth;
    }
    
    public int getGravity() {
        return this.mGravity;
    }
    
    int getLocationOffset(final View view) {
        return 0;
    }
    
    int getNextLocationOffset(final View view) {
        return 0;
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public int getShowDividers() {
        return this.mShowDividers;
    }
    
    View getVirtualChildAt(final int n) {
        return this.getChildAt(n);
    }
    
    int getVirtualChildCount() {
        return this.getChildCount();
    }
    
    public float getWeightSum() {
        return this.mWeightSum;
    }
    
    protected boolean hasDividerBeforeChildAt(int n) {
        final boolean b = false;
        boolean b2 = false;
        if (n == 0) {
            if ((this.mShowDividers & 0x1) != 0x0) {
                b2 = true;
            }
            return b2;
        }
        if (n == this.getChildCount()) {
            boolean b3 = b;
            if ((this.mShowDividers & 0x4) != 0x0) {
                b3 = true;
            }
            return b3;
        }
        if ((this.mShowDividers & 0x2) != 0x0) {
            final boolean b4 = false;
            --n;
            boolean b5;
            while (true) {
                b5 = b4;
                if (n < 0) {
                    break;
                }
                if (this.getChildAt(n).getVisibility() != 8) {
                    b5 = true;
                    break;
                }
                --n;
            }
            return b5;
        }
        return false;
    }
    
    void layoutHorizontal(int n, int absoluteGravity, int i, int n2) {
        final boolean layoutRtl = ViewUtils.isLayoutRtl((View)this);
        final int paddingTop = this.getPaddingTop();
        final int n3 = n2 - absoluteGravity;
        final int paddingBottom = this.getPaddingBottom();
        final int paddingBottom2 = this.getPaddingBottom();
        final int virtualChildCount = this.getVirtualChildCount();
        final int n4 = this.mGravity & 0x800007;
        final int mGravity = this.mGravity;
        final boolean mBaselineAligned = this.mBaselineAligned;
        final int[] mMaxAscent = this.mMaxAscent;
        final int[] mMaxDescent = this.mMaxDescent;
        absoluteGravity = GravityCompat.getAbsoluteGravity(n4, ViewCompat.getLayoutDirection((View)this));
        if (absoluteGravity != 1) {
            if (absoluteGravity != 5) {
                n = this.getPaddingLeft();
            }
            else {
                n = this.getPaddingLeft() + i - n - this.mTotalLength;
            }
        }
        else {
            n = this.getPaddingLeft() + (i - n - this.mTotalLength) / 2;
        }
        int n5 = 0;
        n2 = 1;
        if (layoutRtl) {
            n5 = virtualChildCount - 1;
            n2 = -1;
        }
        i = 0;
        absoluteGravity = n;
        while (i < virtualChildCount) {
            final int n6 = n5 + n2 * i;
            final View virtualChild = this.getVirtualChildAt(n6);
            if (virtualChild == null) {
                absoluteGravity += this.measureNullChild(n6);
            }
            else if (virtualChild.getVisibility() != 8) {
                final int measuredWidth = virtualChild.getMeasuredWidth();
                final int measuredHeight = virtualChild.getMeasuredHeight();
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                int baseline;
                if (mBaselineAligned && layoutParams.height != -1) {
                    baseline = virtualChild.getBaseline();
                }
                else {
                    baseline = -1;
                }
                if ((n = layoutParams.gravity) < 0) {
                    n = (mGravity & 0x70);
                }
                n &= 0x70;
                if (n != 16) {
                    if (n != 48) {
                        if (n != 80) {
                            n = paddingTop;
                        }
                        else {
                            final int n7 = n = n3 - paddingBottom - measuredHeight - layoutParams.bottomMargin;
                            if (baseline != -1) {
                                n = virtualChild.getMeasuredHeight();
                                n = n7 - (mMaxDescent[2] - (n - baseline));
                            }
                        }
                    }
                    else {
                        final int n8 = n = layoutParams.topMargin + paddingTop;
                        if (baseline != -1) {
                            n = n8 + (mMaxAscent[1] - baseline);
                        }
                    }
                }
                else {
                    n = (n3 - paddingTop - paddingBottom2 - measuredHeight) / 2 + paddingTop + layoutParams.topMargin - layoutParams.bottomMargin;
                }
                int n9 = absoluteGravity;
                if (this.hasDividerBeforeChildAt(n6)) {
                    n9 = absoluteGravity + this.mDividerWidth;
                }
                absoluteGravity = n9 + layoutParams.leftMargin;
                this.setChildFrame(virtualChild, absoluteGravity + this.getLocationOffset(virtualChild), n, measuredWidth, measuredHeight);
                absoluteGravity += measuredWidth + layoutParams.rightMargin + this.getNextLocationOffset(virtualChild);
                i += this.getChildrenSkipCount(virtualChild, n6);
            }
            ++i;
        }
    }
    
    void layoutVertical(int paddingTop, int n, int n2, int n3) {
        final int paddingLeft = this.getPaddingLeft();
        final int n4 = n2 - paddingTop;
        final int paddingRight = this.getPaddingRight();
        final int paddingRight2 = this.getPaddingRight();
        final int virtualChildCount = this.getVirtualChildCount();
        final int n5 = this.mGravity & 0x70;
        final int mGravity = this.mGravity;
        if (n5 != 16) {
            if (n5 != 80) {
                paddingTop = this.getPaddingTop();
            }
            else {
                paddingTop = this.getPaddingTop() + n3 - n - this.mTotalLength;
            }
        }
        else {
            paddingTop = this.getPaddingTop() + (n3 - n - this.mTotalLength) / 2;
        }
        n = 0;
        n2 = paddingLeft;
        n3 = n5;
        while (true) {
            int n6 = n;
            if (n6 >= virtualChildCount) {
                break;
            }
            final View virtualChild = this.getVirtualChildAt(n6);
            if (virtualChild == null) {
                paddingTop += this.measureNullChild(n6);
            }
            else if (virtualChild.getVisibility() != 8) {
                final int measuredWidth = virtualChild.getMeasuredWidth();
                final int measuredHeight = virtualChild.getMeasuredHeight();
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                if ((n = layoutParams.gravity) < 0) {
                    n = (mGravity & 0x800007);
                }
                n = (GravityCompat.getAbsoluteGravity(n, ViewCompat.getLayoutDirection((View)this)) & 0x7);
                if (n != 1) {
                    if (n != 5) {
                        n = layoutParams.leftMargin + n2;
                    }
                    else {
                        n = n4 - paddingRight - measuredWidth - layoutParams.rightMargin;
                    }
                }
                else {
                    n = (n4 - paddingLeft - paddingRight2 - measuredWidth) / 2 + n2 + layoutParams.leftMargin - layoutParams.rightMargin;
                }
                int n7 = paddingTop;
                if (this.hasDividerBeforeChildAt(n6)) {
                    n7 = paddingTop + this.mDividerHeight;
                }
                paddingTop = n7 + layoutParams.topMargin;
                this.setChildFrame(virtualChild, n, paddingTop + this.getLocationOffset(virtualChild), measuredWidth, measuredHeight);
                final int bottomMargin = layoutParams.bottomMargin;
                n = this.getNextLocationOffset(virtualChild);
                n6 += this.getChildrenSkipCount(virtualChild, n6);
                paddingTop += measuredHeight + bottomMargin + n;
            }
            n = n6 + 1;
        }
    }
    
    void measureChildBeforeLayout(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        this.measureChildWithMargins(view, n2, n3, n4, n5);
    }
    
    void measureHorizontal(final int n, final int n2) {
        this.mTotalLength = 0;
        float n3 = 0.0f;
        int virtualChildCount = this.getVirtualChildCount();
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        if (this.mMaxAscent == null || this.mMaxDescent == null) {
            this.mMaxAscent = new int[4];
            this.mMaxDescent = new int[4];
        }
        final int[] mMaxAscent = this.mMaxAscent;
        final int[] mMaxDescent = this.mMaxDescent;
        mMaxAscent[2] = (mMaxAscent[3] = -1);
        mMaxAscent[0] = (mMaxAscent[1] = -1);
        mMaxDescent[2] = (mMaxDescent[3] = -1);
        mMaxDescent[0] = (mMaxDescent[1] = -1);
        final boolean mBaselineAligned = this.mBaselineAligned;
        boolean b = false;
        final boolean mUseLargestChild = this.mUseLargestChild;
        final boolean b2 = mode == 1073741824;
        int combineMeasuredStates = 0;
        int max = 0;
        int n4 = 0;
        int n5 = 1;
        int max2 = 0;
        int i = 0;
        int n6 = 0;
        int n7 = 0;
        while (i < virtualChildCount) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild == null) {
                this.mTotalLength += this.measureNullChild(i);
            }
            else {
                final int n8 = max;
                if (virtualChild.getVisibility() == 8) {
                    i += this.getChildrenSkipCount(virtualChild, i);
                    max = n8;
                }
                else {
                    if (this.hasDividerBeforeChildAt(i)) {
                        this.mTotalLength += this.mDividerWidth;
                    }
                    final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                    n3 += layoutParams.weight;
                    if (mode == 1073741824 && layoutParams.width == 0 && layoutParams.weight > 0.0f) {
                        if (b2) {
                            this.mTotalLength += layoutParams.leftMargin + layoutParams.rightMargin;
                        }
                        else {
                            final int mTotalLength = this.mTotalLength;
                            this.mTotalLength = Math.max(mTotalLength, layoutParams.leftMargin + mTotalLength + layoutParams.rightMargin);
                        }
                        if (mBaselineAligned) {
                            final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
                            virtualChild.measure(measureSpec, measureSpec);
                        }
                        else {
                            b = true;
                            max = n8;
                        }
                    }
                    else {
                        int width;
                        final int n9 = width = Integer.MIN_VALUE;
                        if (layoutParams.width == 0) {
                            width = n9;
                            if (layoutParams.weight > 0.0f) {
                                width = 0;
                                layoutParams.width = -2;
                            }
                        }
                        int mTotalLength2;
                        if (n3 == 0.0f) {
                            mTotalLength2 = this.mTotalLength;
                        }
                        else {
                            mTotalLength2 = 0;
                        }
                        this.measureChildBeforeLayout(virtualChild, i, n, mTotalLength2, n2, 0);
                        if (width != Integer.MIN_VALUE) {
                            layoutParams.width = width;
                        }
                        final LayoutParams layoutParams2 = layoutParams;
                        final int measuredWidth = virtualChild.getMeasuredWidth();
                        if (b2) {
                            this.mTotalLength += layoutParams2.leftMargin + measuredWidth + layoutParams2.rightMargin + this.getNextLocationOffset(virtualChild);
                        }
                        else {
                            final int mTotalLength3 = this.mTotalLength;
                            this.mTotalLength = Math.max(mTotalLength3, mTotalLength3 + measuredWidth + layoutParams2.leftMargin + layoutParams2.rightMargin + this.getNextLocationOffset(virtualChild));
                        }
                        if (mUseLargestChild) {
                            max = Math.max(measuredWidth, n8);
                        }
                    }
                    final int n10 = n6;
                    final int n11 = n7;
                    boolean b4;
                    final boolean b3 = b4 = false;
                    int n12 = n4;
                    if (mode2 != 1073741824) {
                        b4 = b3;
                        n12 = n4;
                        if (layoutParams.height == -1) {
                            n12 = 1;
                            b4 = true;
                        }
                    }
                    final int n13 = layoutParams.topMargin + layoutParams.bottomMargin;
                    final int n14 = virtualChild.getMeasuredHeight() + n13;
                    combineMeasuredStates = View.combineMeasuredStates(combineMeasuredStates, virtualChild.getMeasuredState());
                    if (mBaselineAligned) {
                        final int baseline = virtualChild.getBaseline();
                        if (baseline != -1) {
                            int n15;
                            if (layoutParams.gravity < 0) {
                                n15 = this.mGravity;
                            }
                            else {
                                n15 = layoutParams.gravity;
                            }
                            final int n16 = ((n15 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
                            mMaxAscent[n16] = Math.max(mMaxAscent[n16], baseline);
                            mMaxDescent[n16] = Math.max(mMaxDescent[n16], n14 - baseline);
                        }
                    }
                    int n17 = n13;
                    max2 = Math.max(max2, n14);
                    if (n5 != 0 && layoutParams.height == -1) {
                        n5 = 1;
                    }
                    else {
                        n5 = 0;
                    }
                    int max3;
                    int max4;
                    if (layoutParams.weight > 0.0f) {
                        if (!b4) {
                            n17 = n14;
                        }
                        max3 = Math.max(n11, n17);
                        max4 = n10;
                    }
                    else {
                        if (!b4) {
                            n17 = n14;
                        }
                        max4 = Math.max(n10, n17);
                        max3 = n11;
                    }
                    final int n18 = i + this.getChildrenSkipCount(virtualChild, i);
                    final int n19 = max4;
                    final int n20 = max3;
                    n4 = n12;
                    n6 = n19;
                    n7 = n20;
                    i = n18;
                }
            }
            ++i;
        }
        final int n21 = n7;
        final int n22 = max;
        if (this.mTotalLength > 0 && this.hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerWidth;
        }
        int max5;
        if (mMaxAscent[1] == -1 && mMaxAscent[0] == -1 && mMaxAscent[2] == -1 && mMaxAscent[3] == -1) {
            max5 = max2;
        }
        else {
            max5 = Math.max(max2, Math.max(mMaxAscent[3], Math.max(mMaxAscent[0], Math.max(mMaxAscent[1], mMaxAscent[2]))) + Math.max(mMaxDescent[3], Math.max(mMaxDescent[0], Math.max(mMaxDescent[1], mMaxDescent[2]))));
        }
        if (mUseLargestChild && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.mTotalLength = 0;
            for (int j = 0; j < virtualChildCount; ++j) {
                final View virtualChild2 = this.getVirtualChildAt(j);
                if (virtualChild2 == null) {
                    this.mTotalLength += this.measureNullChild(j);
                }
                else if (virtualChild2.getVisibility() == 8) {
                    j += this.getChildrenSkipCount(virtualChild2, j);
                }
                else {
                    final LayoutParams layoutParams3 = (LayoutParams)virtualChild2.getLayoutParams();
                    if (b2) {
                        this.mTotalLength += layoutParams3.leftMargin + n22 + layoutParams3.rightMargin + this.getNextLocationOffset(virtualChild2);
                    }
                    else {
                        final int mTotalLength4 = this.mTotalLength;
                        this.mTotalLength = Math.max(mTotalLength4, mTotalLength4 + n22 + layoutParams3.leftMargin + layoutParams3.rightMargin + this.getNextLocationOffset(virtualChild2));
                    }
                }
            }
        }
        this.mTotalLength += this.getPaddingLeft() + this.getPaddingRight();
        final int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.mTotalLength, this.getSuggestedMinimumWidth()), n, 0);
        final int n23 = (resolveSizeAndState & 0xFFFFFF) - this.mTotalLength;
        int n27;
        int n28;
        if (!b && (n23 == 0 || n3 <= 0.0f)) {
            int max6;
            final int n24 = max6 = Math.max(n6, n21);
            int n25 = max5;
            if (mUseLargestChild) {
                max6 = n24;
                n25 = max5;
                if (mode != 1073741824) {
                    int n26 = 0;
                    while (true) {
                        max6 = n24;
                        n25 = max5;
                        if (n26 >= virtualChildCount) {
                            break;
                        }
                        final View virtualChild3 = this.getVirtualChildAt(n26);
                        if (virtualChild3 != null) {
                            if (virtualChild3.getVisibility() != 8) {
                                if (((LayoutParams)virtualChild3.getLayoutParams()).weight > 0.0f) {
                                    virtualChild3.measure(View$MeasureSpec.makeMeasureSpec(n22, 1073741824), View$MeasureSpec.makeMeasureSpec(virtualChild3.getMeasuredHeight(), 1073741824));
                                }
                            }
                        }
                        ++n26;
                    }
                }
            }
            n27 = n25;
            n28 = max6;
        }
        else {
            float mWeightSum;
            if (this.mWeightSum > 0.0f) {
                mWeightSum = this.mWeightSum;
            }
            else {
                mWeightSum = n3;
            }
            mMaxAscent[2] = (mMaxAscent[3] = -1);
            mMaxAscent[0] = (mMaxAscent[1] = -1);
            mMaxDescent[2] = (mMaxDescent[3] = -1);
            mMaxDescent[0] = (mMaxDescent[1] = -1);
            final int n29 = -1;
            this.mTotalLength = 0;
            n28 = n6;
            int combineMeasuredStates2 = combineMeasuredStates;
            final float n30 = mWeightSum;
            int k = 0;
            int n31 = n29;
            final int n32 = virtualChildCount;
            int n33 = n23;
            float n34 = n30;
            while (k < n32) {
                final View virtualChild4 = this.getVirtualChildAt(k);
                if (virtualChild4 != null) {
                    if (virtualChild4.getVisibility() != 8) {
                        final LayoutParams layoutParams4 = (LayoutParams)virtualChild4.getLayoutParams();
                        final float weight = layoutParams4.weight;
                        if (weight > 0.0f) {
                            final int n35 = (int)(n33 * weight / n34);
                            final int paddingTop = this.getPaddingTop();
                            final int paddingBottom = this.getPaddingBottom();
                            n34 -= weight;
                            final int topMargin = layoutParams4.topMargin;
                            final int bottomMargin = layoutParams4.bottomMargin;
                            final int n36 = n33 - n35;
                            final int childMeasureSpec = getChildMeasureSpec(n2, paddingTop + paddingBottom + topMargin + bottomMargin, layoutParams4.height);
                            if (layoutParams4.width == 0 && mode == 1073741824) {
                                int n37;
                                if (n35 > 0) {
                                    n37 = n35;
                                }
                                else {
                                    n37 = 0;
                                }
                                virtualChild4.measure(View$MeasureSpec.makeMeasureSpec(n37, 1073741824), childMeasureSpec);
                            }
                            else {
                                int n38;
                                if ((n38 = virtualChild4.getMeasuredWidth() + n35) < 0) {
                                    n38 = 0;
                                }
                                virtualChild4.measure(View$MeasureSpec.makeMeasureSpec(n38, 1073741824), childMeasureSpec);
                            }
                            combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates2, virtualChild4.getMeasuredState() & 0xFF000000);
                            n33 = n36;
                        }
                        if (b2) {
                            this.mTotalLength += virtualChild4.getMeasuredWidth() + layoutParams4.leftMargin + layoutParams4.rightMargin + this.getNextLocationOffset(virtualChild4);
                        }
                        else {
                            final int mTotalLength5 = this.mTotalLength;
                            this.mTotalLength = Math.max(mTotalLength5, virtualChild4.getMeasuredWidth() + mTotalLength5 + layoutParams4.leftMargin + layoutParams4.rightMargin + this.getNextLocationOffset(virtualChild4));
                        }
                        final boolean b5 = mode2 != 1073741824 && layoutParams4.height == -1;
                        final int n39 = layoutParams4.topMargin + layoutParams4.bottomMargin;
                        final int n40 = virtualChild4.getMeasuredHeight() + n39;
                        final int max7 = Math.max(n31, n40);
                        int n41;
                        if (b5) {
                            n41 = n39;
                        }
                        else {
                            n41 = n40;
                        }
                        final int max8 = Math.max(n28, n41);
                        final boolean b6 = n5 != 0 && layoutParams4.height == -1;
                        if (mBaselineAligned) {
                            final int baseline2 = virtualChild4.getBaseline();
                            if (baseline2 != -1) {
                                int n42;
                                if (layoutParams4.gravity < 0) {
                                    n42 = this.mGravity;
                                }
                                else {
                                    n42 = layoutParams4.gravity;
                                }
                                final int n43 = ((n42 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
                                mMaxAscent[n43] = Math.max(mMaxAscent[n43], baseline2);
                                mMaxDescent[n43] = Math.max(mMaxDescent[n43], n40 - baseline2);
                            }
                        }
                        n31 = max7;
                        n5 = (b6 ? 1 : 0);
                        n28 = max8;
                    }
                }
                ++k;
            }
            virtualChildCount = n32;
            this.mTotalLength += this.getPaddingLeft() + this.getPaddingRight();
            int max9 = 0;
            Label_2226: {
                if (mMaxAscent[1] == -1 && mMaxAscent[0] == -1 && mMaxAscent[2] == -1) {
                    max9 = n31;
                    if (mMaxAscent[3] == -1) {
                        break Label_2226;
                    }
                }
                max9 = Math.max(n31, Math.max(mMaxAscent[3], Math.max(mMaxAscent[0], Math.max(mMaxAscent[1], mMaxAscent[2]))) + Math.max(mMaxDescent[3], Math.max(mMaxDescent[0], Math.max(mMaxDescent[1], mMaxDescent[2]))));
            }
            combineMeasuredStates = combineMeasuredStates2;
            n27 = max9;
        }
        int n44 = n27;
        if (n5 == 0) {
            n44 = n27;
            if (mode2 != 1073741824) {
                n44 = n28;
            }
        }
        this.setMeasuredDimension((combineMeasuredStates & 0xFF000000) | resolveSizeAndState, View.resolveSizeAndState(Math.max(n44 + (this.getPaddingTop() + this.getPaddingBottom()), this.getSuggestedMinimumHeight()), n2, combineMeasuredStates << 16));
        if (n4 != 0) {
            this.forceUniformHeight(virtualChildCount, n);
        }
    }
    
    int measureNullChild(final int n) {
        return 0;
    }
    
    void measureVertical(final int n, final int n2) {
        this.mTotalLength = 0;
        int n3 = 0;
        float mWeightSum = 0.0f;
        final int virtualChildCount = this.getVirtualChildCount();
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        boolean b = false;
        final int mBaselineAlignedChildIndex = this.mBaselineAlignedChildIndex;
        final boolean mUseLargestChild = this.mUseLargestChild;
        boolean b2 = false;
        int n4 = 0;
        int n5 = 0;
        int i = 0;
        int n6 = 0;
        int max = 0;
        int n7 = 1;
        while (i < virtualChildCount) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild == null) {
                this.mTotalLength += this.measureNullChild(i);
            }
            else if (virtualChild.getVisibility() == 8) {
                i += this.getChildrenSkipCount(virtualChild, i);
            }
            else {
                if (this.hasDividerBeforeChildAt(i)) {
                    this.mTotalLength += this.mDividerHeight;
                }
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                mWeightSum += layoutParams.weight;
                if (mode2 == 1073741824 && layoutParams.height == 0 && layoutParams.weight > 0.0f) {
                    final int mTotalLength = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength, layoutParams.topMargin + mTotalLength + layoutParams.bottomMargin);
                    b = true;
                }
                else {
                    int height;
                    final int n8 = height = Integer.MIN_VALUE;
                    if (layoutParams.height == 0) {
                        height = n8;
                        if (layoutParams.weight > 0.0f) {
                            height = 0;
                            layoutParams.height = -2;
                        }
                    }
                    int mTotalLength2;
                    if (mWeightSum == 0.0f) {
                        mTotalLength2 = this.mTotalLength;
                    }
                    else {
                        mTotalLength2 = 0;
                    }
                    final View view = virtualChild;
                    this.measureChildBeforeLayout(virtualChild, i, n, 0, n2, mTotalLength2);
                    if (height != Integer.MIN_VALUE) {
                        layoutParams.height = height;
                    }
                    final LayoutParams layoutParams2 = layoutParams;
                    final int measuredHeight = view.getMeasuredHeight();
                    final int mTotalLength3 = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength3, mTotalLength3 + measuredHeight + layoutParams2.topMargin + layoutParams2.bottomMargin + this.getNextLocationOffset(view));
                    if (mUseLargestChild) {
                        max = Math.max(measuredHeight, max);
                    }
                }
                final int n9 = n6;
                if (mBaselineAlignedChildIndex >= 0 && mBaselineAlignedChildIndex == i + 1) {
                    this.mBaselineChildTop = this.mTotalLength;
                }
                final int n10 = i;
                if (n10 < mBaselineAlignedChildIndex && layoutParams.weight > 0.0f) {
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                }
                boolean b3 = false;
                if (mode != 1073741824 && layoutParams.width == -1) {
                    b2 = true;
                    b3 = true;
                }
                final int n11 = layoutParams.leftMargin + layoutParams.rightMargin;
                int n12 = virtualChild.getMeasuredWidth() + n11;
                final int max2 = Math.max(n5, n12);
                final int combineMeasuredStates = View.combineMeasuredStates(n3, virtualChild.getMeasuredState());
                final boolean b4 = n7 != 0 && layoutParams.width == -1;
                int max3;
                int max4;
                if (layoutParams.weight > 0.0f) {
                    if (b3) {
                        n12 = n11;
                    }
                    max3 = Math.max(n9, n12);
                    max4 = n4;
                }
                else {
                    max3 = n9;
                    if (b3) {
                        n12 = n11;
                    }
                    max4 = Math.max(n4, n12);
                }
                final int n13 = n10 + this.getChildrenSkipCount(virtualChild, n10);
                final int n14 = max3;
                n4 = max4;
                final int n15 = max2;
                n3 = combineMeasuredStates;
                n7 = (b4 ? 1 : 0);
                n6 = n14;
                n5 = n15;
                i = n13;
            }
            ++i;
        }
        final int n16 = n4;
        final int n17 = n6;
        final int n18 = max;
        if (this.mTotalLength > 0 && this.hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerHeight;
        }
        final int n19 = virtualChildCount;
        if (mUseLargestChild) {
            final int n20 = mode2;
            if (n20 == Integer.MIN_VALUE || n20 == 0) {
                this.mTotalLength = 0;
                for (int j = 0; j < n19; ++j) {
                    final View virtualChild2 = this.getVirtualChildAt(j);
                    if (virtualChild2 == null) {
                        this.mTotalLength += this.measureNullChild(j);
                    }
                    else if (virtualChild2.getVisibility() == 8) {
                        j += this.getChildrenSkipCount(virtualChild2, j);
                    }
                    else {
                        final LayoutParams layoutParams3 = (LayoutParams)virtualChild2.getLayoutParams();
                        final int mTotalLength4 = this.mTotalLength;
                        this.mTotalLength = Math.max(mTotalLength4, mTotalLength4 + n18 + layoutParams3.topMargin + layoutParams3.bottomMargin + this.getNextLocationOffset(virtualChild2));
                    }
                }
            }
        }
        final int n21 = mode2;
        this.mTotalLength += this.getPaddingTop() + this.getPaddingBottom();
        final int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.mTotalLength, this.getSuggestedMinimumHeight()), n2, 0);
        final int n22 = (resolveSizeAndState & 0xFFFFFF) - this.mTotalLength;
        int n26;
        int n27;
        if (!b && (n22 == 0 || mWeightSum <= 0.0f)) {
            final int max5 = Math.max(n16, n17);
            int n25;
            if (mUseLargestChild) {
                int n23 = max5;
                if (n21 != 1073741824) {
                    int n24 = 0;
                    while (true) {
                        n23 = max5;
                        if (n24 >= n19) {
                            break;
                        }
                        final View virtualChild3 = this.getVirtualChildAt(n24);
                        if (virtualChild3 != null) {
                            if (virtualChild3.getVisibility() != 8) {
                                if (((LayoutParams)virtualChild3.getLayoutParams()).weight > 0.0f) {
                                    virtualChild3.measure(View$MeasureSpec.makeMeasureSpec(virtualChild3.getMeasuredWidth(), 1073741824), View$MeasureSpec.makeMeasureSpec(n18, 1073741824));
                                }
                            }
                        }
                        ++n24;
                    }
                }
                n25 = n23;
            }
            else {
                n25 = max5;
            }
            n26 = n3;
            n27 = n25;
        }
        else {
            if (this.mWeightSum > 0.0f) {
                mWeightSum = this.mWeightSum;
            }
            this.mTotalLength = 0;
            final int n28 = 0;
            int n29 = n22;
            int combineMeasuredStates2 = n3;
            n27 = n16;
            final int n30 = n21;
            for (int k = n28; k < n19; ++k) {
                final View virtualChild4 = this.getVirtualChildAt(k);
                if (virtualChild4.getVisibility() != 8) {
                    final LayoutParams layoutParams4 = (LayoutParams)virtualChild4.getLayoutParams();
                    final float weight = layoutParams4.weight;
                    if (weight > 0.0f) {
                        final int n31 = (int)(n29 * weight / mWeightSum);
                        final int paddingLeft = this.getPaddingLeft();
                        final int paddingRight = this.getPaddingRight();
                        final int leftMargin = layoutParams4.leftMargin;
                        final int rightMargin = layoutParams4.rightMargin;
                        final int n32 = n29 - n31;
                        final int childMeasureSpec = getChildMeasureSpec(n, paddingLeft + paddingRight + leftMargin + rightMargin, layoutParams4.width);
                        if (layoutParams4.height == 0 && n30 == 1073741824) {
                            int n33;
                            if (n31 > 0) {
                                n33 = n31;
                            }
                            else {
                                n33 = 0;
                            }
                            virtualChild4.measure(childMeasureSpec, View$MeasureSpec.makeMeasureSpec(n33, 1073741824));
                        }
                        else {
                            int n34;
                            if ((n34 = virtualChild4.getMeasuredHeight() + n31) < 0) {
                                n34 = 0;
                            }
                            virtualChild4.measure(childMeasureSpec, View$MeasureSpec.makeMeasureSpec(n34, 1073741824));
                        }
                        combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates2, virtualChild4.getMeasuredState() & 0xFFFFFF00);
                        mWeightSum -= weight;
                        n29 = n32;
                    }
                    final int n35 = layoutParams4.leftMargin + layoutParams4.rightMargin;
                    final int n36 = virtualChild4.getMeasuredWidth() + n35;
                    final int max6 = Math.max(n5, n36);
                    int n37;
                    if (mode != 1073741824 && layoutParams4.width == -1) {
                        n37 = n35;
                    }
                    else {
                        n37 = n36;
                    }
                    final int max7 = Math.max(n27, n37);
                    final boolean b5 = n7 != 0 && layoutParams4.width == -1;
                    final int mTotalLength5 = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength5, mTotalLength5 + virtualChild4.getMeasuredHeight() + layoutParams4.topMargin + layoutParams4.bottomMargin + this.getNextLocationOffset(virtualChild4));
                    n7 = (b5 ? 1 : 0);
                    n5 = max6;
                    n27 = max7;
                }
            }
            this.mTotalLength += this.getPaddingTop() + this.getPaddingBottom();
            n26 = combineMeasuredStates2;
        }
        int n38 = n5;
        if (n7 == 0) {
            n38 = n5;
            if (mode != 1073741824) {
                n38 = n27;
            }
        }
        this.setMeasuredDimension(View.resolveSizeAndState(Math.max(n38 + (this.getPaddingLeft() + this.getPaddingRight()), this.getSuggestedMinimumWidth()), n, n26), resolveSizeAndState);
        if (b2) {
            this.forceUniformWidth(n19, n2);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        if (this.mDivider == null) {
            return;
        }
        if (this.mOrientation == 1) {
            this.drawDividersVertical(canvas);
        }
        else {
            this.drawDividersHorizontal(canvas);
        }
    }
    
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName((CharSequence)LinearLayoutCompat.class.getName());
    }
    
    public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName((CharSequence)LinearLayoutCompat.class.getName());
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        if (this.mOrientation == 1) {
            this.layoutVertical(n, n2, n3, n4);
        }
        else {
            this.layoutHorizontal(n, n2, n3, n4);
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        if (this.mOrientation == 1) {
            this.measureVertical(n, n2);
        }
        else {
            this.measureHorizontal(n, n2);
        }
    }
    
    public void setBaselineAligned(final boolean mBaselineAligned) {
        this.mBaselineAligned = mBaselineAligned;
    }
    
    public void setBaselineAlignedChildIndex(final int mBaselineAlignedChildIndex) {
        if (mBaselineAlignedChildIndex >= 0 && mBaselineAlignedChildIndex < this.getChildCount()) {
            this.mBaselineAlignedChildIndex = mBaselineAlignedChildIndex;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("base aligned child index out of range (0, ");
        sb.append(this.getChildCount());
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void setDividerDrawable(final Drawable mDivider) {
        if (mDivider == this.mDivider) {
            return;
        }
        this.mDivider = mDivider;
        boolean willNotDraw = false;
        if (mDivider != null) {
            this.mDividerWidth = mDivider.getIntrinsicWidth();
            this.mDividerHeight = mDivider.getIntrinsicHeight();
        }
        else {
            this.mDividerWidth = 0;
            this.mDividerHeight = 0;
        }
        if (mDivider == null) {
            willNotDraw = true;
        }
        this.setWillNotDraw(willNotDraw);
        this.requestLayout();
    }
    
    public void setDividerPadding(final int mDividerPadding) {
        this.mDividerPadding = mDividerPadding;
    }
    
    public void setGravity(int mGravity) {
        if (this.mGravity != mGravity) {
            int n = mGravity;
            if ((0x800007 & mGravity) == 0x0) {
                n = (mGravity | 0x800003);
            }
            mGravity = n;
            if ((n & 0x70) == 0x0) {
                mGravity = (n | 0x30);
            }
            this.mGravity = mGravity;
            this.requestLayout();
        }
    }
    
    public void setHorizontalGravity(int n) {
        n &= 0x800007;
        if ((0x800007 & this.mGravity) != n) {
            this.mGravity = ((this.mGravity & 0xFF7FFFF8) | n);
            this.requestLayout();
        }
    }
    
    public void setMeasureWithLargestChildEnabled(final boolean mUseLargestChild) {
        this.mUseLargestChild = mUseLargestChild;
    }
    
    public void setOrientation(final int mOrientation) {
        if (this.mOrientation != mOrientation) {
            this.mOrientation = mOrientation;
            this.requestLayout();
        }
    }
    
    public void setShowDividers(final int mShowDividers) {
        if (mShowDividers != this.mShowDividers) {
            this.requestLayout();
        }
        this.mShowDividers = mShowDividers;
    }
    
    public void setVerticalGravity(int n) {
        n &= 0x70;
        if ((this.mGravity & 0x70) != n) {
            this.mGravity = ((this.mGravity & 0xFFFFFF8F) | n);
            this.requestLayout();
        }
    }
    
    public void setWeightSum(final float n) {
        this.mWeightSum = Math.max(0.0f, n);
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        public int gravity;
        public float weight;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.gravity = -1;
            this.weight = 0.0f;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.gravity = -1;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.LinearLayoutCompat_Layout);
            this.weight = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0f);
            this.gravity = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
            obtainStyledAttributes.recycle();
        }
        
        public LayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
            super(viewGroup.LayoutParams);
            this.gravity = -1;
        }
    }
}
