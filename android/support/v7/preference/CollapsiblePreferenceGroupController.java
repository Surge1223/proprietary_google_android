package android.support.v7.preference;

import android.text.TextUtils;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;

final class CollapsiblePreferenceGroupController
{
    private final Context mContext;
    private boolean mHasExpandablePreference;
    private final PreferenceGroupAdapter mPreferenceGroupAdapter;
    
    CollapsiblePreferenceGroupController(final PreferenceGroup preferenceGroup, final PreferenceGroupAdapter mPreferenceGroupAdapter) {
        this.mHasExpandablePreference = false;
        this.mPreferenceGroupAdapter = mPreferenceGroupAdapter;
        this.mContext = preferenceGroup.getContext();
    }
    
    private ExpandButton createExpandButton(final PreferenceGroup preferenceGroup, final List<Preference> list) {
        final ExpandButton expandButton = new ExpandButton(this.mContext, list, preferenceGroup.getId());
        expandButton.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                preferenceGroup.setInitialExpandedChildrenCount(Integer.MAX_VALUE);
                CollapsiblePreferenceGroupController.this.mPreferenceGroupAdapter.onPreferenceHierarchyChange(preference);
                return true;
            }
        });
        return expandButton;
    }
    
    private List<Preference> createInnerVisiblePreferencesList(final PreferenceGroup preferenceGroup) {
        int i = 0;
        this.mHasExpandablePreference = false;
        int n = 0;
        final boolean b = preferenceGroup.getInitialExpandedChildrenCount() != Integer.MAX_VALUE;
        final ArrayList<ExpandButton> list = new ArrayList<ExpandButton>();
        final ArrayList<Preference> list2 = new ArrayList<Preference>();
        while (i < preferenceGroup.getPreferenceCount()) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (preference.isVisible()) {
                if (b && n >= preferenceGroup.getInitialExpandedChildrenCount()) {
                    list2.add(preference);
                }
                else {
                    list.add((ExpandButton)preference);
                }
                if (!(preference instanceof PreferenceGroup)) {
                    ++n;
                }
                else {
                    final PreferenceGroup preferenceGroup2 = (PreferenceGroup)preference;
                    if (preferenceGroup2.isOnSameScreenAsChildren()) {
                        final List<Preference> innerVisiblePreferencesList = this.createInnerVisiblePreferencesList(preferenceGroup2);
                        if (b && this.mHasExpandablePreference) {
                            throw new IllegalArgumentException("Nested expand buttons are not supported!");
                        }
                        final Iterator<Preference> iterator = innerVisiblePreferencesList.iterator();
                        int n2 = n;
                        while (true) {
                            n = n2;
                            if (!iterator.hasNext()) {
                                break;
                            }
                            final Preference preference2 = iterator.next();
                            if (b && n2 >= preferenceGroup.getInitialExpandedChildrenCount()) {
                                list2.add(preference2);
                            }
                            else {
                                list.add((ExpandButton)preference2);
                            }
                            ++n2;
                        }
                    }
                }
            }
            ++i;
        }
        if (b && n > preferenceGroup.getInitialExpandedChildrenCount()) {
            list.add(this.createExpandButton(preferenceGroup, list2));
        }
        this.mHasExpandablePreference |= b;
        return (List<Preference>)list;
    }
    
    public List<Preference> createVisiblePreferencesList(final PreferenceGroup preferenceGroup) {
        return this.createInnerVisiblePreferencesList(preferenceGroup);
    }
    
    public boolean onPreferenceVisibilityChange(final Preference preference) {
        if (this.mHasExpandablePreference) {
            this.mPreferenceGroupAdapter.onPreferenceHierarchyChange(preference);
            return true;
        }
        return false;
    }
    
    static class ExpandButton extends Preference
    {
        private long mId;
        
        ExpandButton(final Context context, final List<Preference> summary, final long n) {
            super(context);
            this.initLayout();
            this.setSummary(summary);
            this.mId = 1000000L + n;
        }
        
        private void initLayout() {
            this.setLayoutResource(R.layout.expand_button);
            this.setIcon(R.drawable.ic_arrow_down_24dp);
            this.setTitle(R.string.expand_button_title);
            this.setOrder(999);
        }
        
        private void setSummary(final List<Preference> list) {
            CharSequence summary = null;
            final ArrayList<PreferenceGroup> list2 = new ArrayList<PreferenceGroup>();
            for (final Preference preference : list) {
                final CharSequence title = preference.getTitle();
                if (preference instanceof PreferenceGroup && !TextUtils.isEmpty(title)) {
                    list2.add((PreferenceGroup)preference);
                }
                if (list2.contains(preference.getParent())) {
                    if (!(preference instanceof PreferenceGroup)) {
                        continue;
                    }
                    list2.add((PreferenceGroup)preference);
                }
                else {
                    CharSequence string = summary;
                    if (!TextUtils.isEmpty(title)) {
                        if (summary == null) {
                            string = title;
                        }
                        else {
                            string = this.getContext().getString(R.string.summary_collapsed_preference_list, new Object[] { summary, title });
                        }
                    }
                    summary = string;
                }
            }
            this.setSummary(summary);
        }
        
        public long getId() {
            return this.mId;
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            super.onBindViewHolder(preferenceViewHolder);
            preferenceViewHolder.setDividerAllowedAbove(false);
        }
    }
}
