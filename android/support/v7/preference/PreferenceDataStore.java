package android.support.v7.preference;

import java.util.Set;

public abstract class PreferenceDataStore
{
    public boolean getBoolean(final String s, final boolean b) {
        return b;
    }
    
    public int getInt(final String s, final int n) {
        return n;
    }
    
    public String getString(final String s, final String s2) {
        return s2;
    }
    
    public Set<String> getStringSet(final String s, final Set<String> set) {
        return set;
    }
    
    public void putBoolean(final String s, final boolean b) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }
    
    public void putInt(final String s, final int n) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }
    
    public void putString(final String s, final String s2) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }
    
    public void putStringSet(final String s, final Set<String> set) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }
}
