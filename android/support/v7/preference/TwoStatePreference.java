package android.support.v7.preference;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.widget.TextView;
import android.view.View;
import android.os.Parcelable;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;

public abstract class TwoStatePreference extends Preference
{
    protected boolean mChecked;
    private boolean mCheckedSet;
    private boolean mDisableDependentsState;
    private CharSequence mSummaryOff;
    private CharSequence mSummaryOn;
    
    public TwoStatePreference(final Context context) {
        this(context, null);
    }
    
    public TwoStatePreference(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public TwoStatePreference(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public TwoStatePreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    public boolean isChecked() {
        return this.mChecked;
    }
    
    @Override
    protected void onClick() {
        super.onClick();
        final boolean checked = this.isChecked() ^ true;
        if (this.callChangeListener(checked)) {
            this.setChecked(checked);
        }
    }
    
    @Override
    protected Object onGetDefaultValue(final TypedArray typedArray, final int n) {
        return typedArray.getBoolean(n, false);
    }
    
    @Override
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable != null && parcelable.getClass().equals(SavedState.class)) {
            final SavedState savedState = (SavedState)parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            this.setChecked(savedState.checked);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }
    
    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (this.isPersistent()) {
            return onSaveInstanceState;
        }
        final SavedState savedState = new SavedState(onSaveInstanceState);
        savedState.checked = this.isChecked();
        return (Parcelable)savedState;
    }
    
    @Override
    protected void onSetInitialValue(final boolean b, final Object o) {
        boolean checked;
        if (b) {
            checked = this.getPersistedBoolean(this.mChecked);
        }
        else {
            checked = (boolean)o;
        }
        this.setChecked(checked);
    }
    
    public void setChecked(final boolean mChecked) {
        final boolean b = this.mChecked != mChecked;
        if (b || !this.mCheckedSet) {
            this.mChecked = mChecked;
            this.mCheckedSet = true;
            this.persistBoolean(mChecked);
            if (b) {
                this.notifyDependencyChange(this.shouldDisableDependents());
                this.notifyChanged();
            }
        }
    }
    
    public void setDisableDependentsState(final boolean mDisableDependentsState) {
        this.mDisableDependentsState = mDisableDependentsState;
    }
    
    public void setSummaryOff(final CharSequence mSummaryOff) {
        this.mSummaryOff = mSummaryOff;
        if (!this.isChecked()) {
            this.notifyChanged();
        }
    }
    
    public void setSummaryOn(final CharSequence mSummaryOn) {
        this.mSummaryOn = mSummaryOn;
        if (this.isChecked()) {
            this.notifyChanged();
        }
    }
    
    @Override
    public boolean shouldDisableDependents() {
        final boolean mDisableDependentsState = this.mDisableDependentsState;
        final boolean b = false;
        boolean mChecked;
        if (mDisableDependentsState) {
            mChecked = this.mChecked;
        }
        else {
            mChecked = !this.mChecked;
        }
        return mChecked || super.shouldDisableDependents() || b;
    }
    
    protected void syncSummaryView(final PreferenceViewHolder preferenceViewHolder) {
        this.syncSummaryView(preferenceViewHolder.findViewById(16908304));
    }
    
    protected void syncSummaryView(final View view) {
        if (!(view instanceof TextView)) {
            return;
        }
        final TextView textView = (TextView)view;
        final boolean b = true;
        boolean b2;
        if (this.mChecked && !TextUtils.isEmpty(this.mSummaryOn)) {
            textView.setText(this.mSummaryOn);
            b2 = false;
        }
        else {
            b2 = b;
            if (!this.mChecked) {
                b2 = b;
                if (!TextUtils.isEmpty(this.mSummaryOff)) {
                    textView.setText(this.mSummaryOff);
                    b2 = false;
                }
            }
        }
        boolean b3;
        if (b3 = b2) {
            final CharSequence summary = this.getSummary();
            b3 = b2;
            if (!TextUtils.isEmpty(summary)) {
                textView.setText(summary);
                b3 = false;
            }
        }
        int visibility = 8;
        if (!b3) {
            visibility = 0;
        }
        if (visibility != textView.getVisibility()) {
            textView.setVisibility(visibility);
        }
    }
    
    static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        boolean checked;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel) {
            super(parcel);
            final int int1 = parcel.readInt();
            boolean checked = true;
            if (int1 != 1) {
                checked = false;
            }
            this.checked = checked;
        }
        
        public SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt((int)(this.checked ? 1 : 0));
        }
    }
}
