package android.support.v7.preference;

import android.view.View;
import android.util.SparseArray;
import android.support.v7.widget.RecyclerView;

public class PreferenceViewHolder extends ViewHolder
{
    private final SparseArray<View> mCachedViews;
    private boolean mDividerAllowedAbove;
    private boolean mDividerAllowedBelow;
    
    PreferenceViewHolder(final View view) {
        super(view);
        (this.mCachedViews = (SparseArray<View>)new SparseArray(4)).put(16908310, (Object)view.findViewById(16908310));
        this.mCachedViews.put(16908304, (Object)view.findViewById(16908304));
        this.mCachedViews.put(16908294, (Object)view.findViewById(16908294));
        this.mCachedViews.put(R.id.icon_frame, (Object)view.findViewById(R.id.icon_frame));
        this.mCachedViews.put(16908350, (Object)view.findViewById(16908350));
    }
    
    public View findViewById(final int n) {
        final View view = (View)this.mCachedViews.get(n);
        if (view != null) {
            return view;
        }
        final View viewById = this.itemView.findViewById(n);
        if (viewById != null) {
            this.mCachedViews.put(n, (Object)viewById);
        }
        return viewById;
    }
    
    public boolean isDividerAllowedAbove() {
        return this.mDividerAllowedAbove;
    }
    
    public boolean isDividerAllowedBelow() {
        return this.mDividerAllowedBelow;
    }
    
    public void setDividerAllowedAbove(final boolean mDividerAllowedAbove) {
        this.mDividerAllowedAbove = mDividerAllowedAbove;
    }
    
    public void setDividerAllowedBelow(final boolean mDividerAllowedBelow) {
        this.mDividerAllowedBelow = mDividerAllowedBelow;
    }
}
