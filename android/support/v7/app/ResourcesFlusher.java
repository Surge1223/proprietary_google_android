package android.support.v7.app;

import android.util.LongSparseArray;
import java.util.Map;
import android.util.Log;
import android.os.Build.VERSION;
import android.support.v4.os.BuildCompat;
import android.content.res.Resources;
import java.lang.reflect.Field;

class ResourcesFlusher
{
    private static Field sDrawableCacheField;
    private static boolean sDrawableCacheFieldFetched;
    private static Field sResourcesImplField;
    private static boolean sResourcesImplFieldFetched;
    private static Class sThemedResourceCacheClazz;
    private static boolean sThemedResourceCacheClazzFetched;
    private static Field sThemedResourceCache_mUnthemedEntriesField;
    private static boolean sThemedResourceCache_mUnthemedEntriesFieldFetched;
    
    static void flush(final Resources resources) {
        if (BuildCompat.isAtLeastP()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 24) {
            flushNougats(resources);
        }
        else if (Build.VERSION.SDK_INT >= 23) {
            flushMarshmallows(resources);
        }
        else if (Build.VERSION.SDK_INT >= 21) {
            flushLollipops(resources);
        }
    }
    
    private static void flushLollipops(final Resources resources) {
        if (!ResourcesFlusher.sDrawableCacheFieldFetched) {
            try {
                (ResourcesFlusher.sDrawableCacheField = Resources.class.getDeclaredField("mDrawableCache")).setAccessible(true);
            }
            catch (NoSuchFieldException ex) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", (Throwable)ex);
            }
            ResourcesFlusher.sDrawableCacheFieldFetched = true;
        }
        if (ResourcesFlusher.sDrawableCacheField != null) {
            final Map map = null;
            Map map2;
            try {
                map2 = (Map)ResourcesFlusher.sDrawableCacheField.get(resources);
            }
            catch (IllegalAccessException ex2) {
                Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", (Throwable)ex2);
                map2 = map;
            }
            if (map2 != null) {
                map2.clear();
            }
        }
    }
    
    private static void flushMarshmallows(final Resources resources) {
        if (!ResourcesFlusher.sDrawableCacheFieldFetched) {
            try {
                (ResourcesFlusher.sDrawableCacheField = Resources.class.getDeclaredField("mDrawableCache")).setAccessible(true);
            }
            catch (NoSuchFieldException ex) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", (Throwable)ex);
            }
            ResourcesFlusher.sDrawableCacheFieldFetched = true;
        }
        Object value;
        final Object o = value = null;
        if (ResourcesFlusher.sDrawableCacheField != null) {
            try {
                value = ResourcesFlusher.sDrawableCacheField.get(resources);
            }
            catch (IllegalAccessException ex2) {
                Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", (Throwable)ex2);
                value = o;
            }
        }
        if (value == null) {
            return;
        }
        flushThemedResourcesCache(value);
    }
    
    private static void flushNougats(Resources value) {
        if (!ResourcesFlusher.sResourcesImplFieldFetched) {
            try {
                (ResourcesFlusher.sResourcesImplField = Resources.class.getDeclaredField("mResourcesImpl")).setAccessible(true);
            }
            catch (NoSuchFieldException ex) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", (Throwable)ex);
            }
            ResourcesFlusher.sResourcesImplFieldFetched = true;
        }
        if (ResourcesFlusher.sResourcesImplField == null) {
            return;
        }
        final Object o = null;
        try {
            value = ResourcesFlusher.sResourcesImplField.get(value);
        }
        catch (IllegalAccessException ex2) {
            Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", (Throwable)ex2);
            value = o;
        }
        if (value == null) {
            return;
        }
        if (!ResourcesFlusher.sDrawableCacheFieldFetched) {
            try {
                (ResourcesFlusher.sDrawableCacheField = value.getClass().getDeclaredField("mDrawableCache")).setAccessible(true);
            }
            catch (NoSuchFieldException ex3) {
                Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", (Throwable)ex3);
            }
            ResourcesFlusher.sDrawableCacheFieldFetched = true;
        }
        Object value2;
        final Object o2 = value2 = null;
        if (ResourcesFlusher.sDrawableCacheField != null) {
            try {
                value2 = ResourcesFlusher.sDrawableCacheField.get(value);
            }
            catch (IllegalAccessException ex4) {
                Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", (Throwable)ex4);
                value2 = o2;
            }
        }
        if (value2 != null) {
            flushThemedResourcesCache(value2);
        }
    }
    
    private static void flushThemedResourcesCache(final Object o) {
        if (!ResourcesFlusher.sThemedResourceCacheClazzFetched) {
            try {
                ResourcesFlusher.sThemedResourceCacheClazz = Class.forName("android.content.res.ThemedResourceCache");
            }
            catch (ClassNotFoundException ex) {
                Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", (Throwable)ex);
            }
            ResourcesFlusher.sThemedResourceCacheClazzFetched = true;
        }
        if (ResourcesFlusher.sThemedResourceCacheClazz == null) {
            return;
        }
        if (!ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesFieldFetched) {
            try {
                (ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesField = ResourcesFlusher.sThemedResourceCacheClazz.getDeclaredField("mUnthemedEntries")).setAccessible(true);
            }
            catch (NoSuchFieldException ex2) {
                Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", (Throwable)ex2);
            }
            ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesFieldFetched = true;
        }
        if (ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesField == null) {
            return;
        }
        final LongSparseArray longSparseArray = null;
        LongSparseArray longSparseArray2;
        try {
            longSparseArray2 = (LongSparseArray)ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesField.get(o);
        }
        catch (IllegalAccessException ex3) {
            Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", (Throwable)ex3);
            longSparseArray2 = longSparseArray;
        }
        if (longSparseArray2 != null) {
            longSparseArray2.clear();
        }
    }
}
