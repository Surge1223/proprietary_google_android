package android.support.v13.app;

import android.app.Fragment;
import android.os.Build.VERSION;

@Deprecated
public class FragmentCompat
{
    static final FragmentCompatImpl IMPL;
    
    static {
        if (Build.VERSION.SDK_INT >= 24) {
            IMPL = (FragmentCompatImpl)new FragmentCompatApi24Impl();
        }
        else if (Build.VERSION.SDK_INT >= 23) {
            IMPL = (FragmentCompatImpl)new FragmentCompatApi23Impl();
        }
        else if (Build.VERSION.SDK_INT >= 15) {
            IMPL = (FragmentCompatImpl)new FragmentCompatApi15Impl();
        }
        else {
            IMPL = (FragmentCompatImpl)new FragmentCompatBaseImpl();
        }
    }
    
    @Deprecated
    public static void setUserVisibleHint(final Fragment fragment, final boolean b) {
        FragmentCompat.IMPL.setUserVisibleHint(fragment, b);
    }
    
    static class FragmentCompatApi15Impl extends FragmentCompatBaseImpl
    {
        @Override
        public void setUserVisibleHint(final Fragment fragment, final boolean userVisibleHint) {
            fragment.setUserVisibleHint(userVisibleHint);
        }
    }
    
    static class FragmentCompatApi23Impl extends FragmentCompatApi15Impl
    {
    }
    
    static class FragmentCompatApi24Impl extends FragmentCompatApi23Impl
    {
        @Override
        public void setUserVisibleHint(final Fragment fragment, final boolean userVisibleHint) {
            fragment.setUserVisibleHint(userVisibleHint);
        }
    }
    
    static class FragmentCompatBaseImpl implements FragmentCompatImpl
    {
        @Override
        public void setUserVisibleHint(final Fragment fragment, final boolean b) {
        }
    }
    
    interface FragmentCompatImpl
    {
        void setUserVisibleHint(final Fragment p0, final boolean p1);
    }
}
