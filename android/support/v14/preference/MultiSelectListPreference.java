package android.support.v14.preference;

import java.util.Collections;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v7.preference.Preference;
import java.util.Collection;
import android.os.Parcelable;
import android.content.res.TypedArray;
import java.util.HashSet;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.preference.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.Set;
import android.support.v7.preference.internal.AbstractMultiSelectListPreference;

public class MultiSelectListPreference extends AbstractMultiSelectListPreference
{
    private CharSequence[] mEntries;
    private CharSequence[] mEntryValues;
    private Set<String> mValues;
    
    public MultiSelectListPreference(final Context context) {
        this(context, null);
    }
    
    public MultiSelectListPreference(final Context context, final AttributeSet set) {
        this(context, set, TypedArrayUtils.getAttr(context, R.attr.dialogPreferenceStyle, 16842897));
    }
    
    public MultiSelectListPreference(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public MultiSelectListPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mValues = new HashSet<String>();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.MultiSelectListPreference, n, n2);
        this.mEntries = TypedArrayUtils.getTextArray(obtainStyledAttributes, R.styleable.MultiSelectListPreference_entries, R.styleable.MultiSelectListPreference_android_entries);
        this.mEntryValues = TypedArrayUtils.getTextArray(obtainStyledAttributes, R.styleable.MultiSelectListPreference_entryValues, R.styleable.MultiSelectListPreference_android_entryValues);
        obtainStyledAttributes.recycle();
    }
    
    public int findIndexOfValue(final String s) {
        if (s != null && this.mEntryValues != null) {
            for (int i = this.mEntryValues.length - 1; i >= 0; --i) {
                if (this.mEntryValues[i].equals(s)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    @Override
    public CharSequence[] getEntries() {
        return this.mEntries;
    }
    
    @Override
    public CharSequence[] getEntryValues() {
        return this.mEntryValues;
    }
    
    @Override
    public Set<String> getValues() {
        return this.mValues;
    }
    
    @Override
    protected Object onGetDefaultValue(final TypedArray typedArray, int i) {
        final CharSequence[] textArray = typedArray.getTextArray(i);
        final HashSet<String> set = new HashSet<String>();
        int length;
        for (length = textArray.length, i = 0; i < length; ++i) {
            set.add(textArray[i].toString());
        }
        return set;
    }
    
    @Override
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable != null && parcelable.getClass().equals(SavedState.class)) {
            final SavedState savedState = (SavedState)parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            this.setValues(savedState.mValues);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }
    
    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (this.isPersistent()) {
            return onSaveInstanceState;
        }
        final SavedState savedState = new SavedState(onSaveInstanceState);
        savedState.mValues = this.getValues();
        return (Parcelable)savedState;
    }
    
    @Override
    protected void onSetInitialValue(final boolean b, final Object o) {
        Set<String> persistedStringSet;
        if (b) {
            persistedStringSet = this.getPersistedStringSet(this.mValues);
        }
        else {
            persistedStringSet = (Set<String>)o;
        }
        this.setValues(persistedStringSet);
    }
    
    public void setEntries(final CharSequence[] mEntries) {
        this.mEntries = mEntries;
    }
    
    public void setEntryValues(final CharSequence[] mEntryValues) {
        this.mEntryValues = mEntryValues;
    }
    
    @Override
    public void setValues(final Set<String> set) {
        this.mValues.clear();
        this.mValues.addAll(set);
        this.persistStringSet(set);
    }
    
    private static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        Set<String> mValues;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel) {
            super(parcel);
            final int int1 = parcel.readInt();
            this.mValues = new HashSet<String>();
            final String[] array = new String[int1];
            parcel.readStringArray(array);
            Collections.addAll(this.mValues, array);
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.mValues.size());
            parcel.writeStringArray((String[])this.mValues.toArray(new String[this.mValues.size()]));
        }
    }
}
