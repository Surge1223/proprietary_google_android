package android.support.customtabs;

public final class R
{
    public static final class dimen
    {
        public static final int browser_actions_context_menu_max_width = 2131165321;
        public static final int browser_actions_context_menu_min_padding = 2131165322;
    }
    
    public static final class id
    {
        public static final int browser_actions_header_text = 2131361925;
        public static final int browser_actions_menu_item_icon = 2131361926;
        public static final int browser_actions_menu_item_text = 2131361927;
        public static final int browser_actions_menu_items = 2131361928;
        public static final int browser_actions_menu_view = 2131361929;
    }
    
    public static final class layout
    {
        public static final int browser_actions_context_menu_page = 2131558476;
        public static final int browser_actions_context_menu_row = 2131558477;
    }
}
