package android.support.v4.app;

import android.arch.lifecycle.ReportFragment;
import android.os.Bundle;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleRegistry;
import android.support.v4.util.SimpleArrayMap;
import android.arch.lifecycle.LifecycleOwner;
import android.app.Activity;

public class SupportActivity extends Activity implements LifecycleOwner
{
    private SimpleArrayMap<Class<?>, Object> mExtraDataMap;
    private LifecycleRegistry mLifecycleRegistry;
    
    public SupportActivity() {
        this.mExtraDataMap = new SimpleArrayMap<Class<?>, Object>();
        this.mLifecycleRegistry = new LifecycleRegistry(this);
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ReportFragment.injectIfNeededIn(this);
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        this.mLifecycleRegistry.markState(Lifecycle.State.CREATED);
        super.onSaveInstanceState(bundle);
    }
}
