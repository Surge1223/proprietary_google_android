package android.support.v4.app;

import android.app.Notification$Builder;

public interface NotificationBuilderWithBuilderAccessor
{
    Notification$Builder getBuilder();
}
