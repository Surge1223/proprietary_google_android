package android.support.v4.app;

import android.view.View;
import java.util.Map;
import java.util.List;

public abstract class SharedElementCallback
{
    public void onMapSharedElements(final List<String> list, final Map<String, View> map) {
    }
    
    public void onSharedElementEnd(final List<String> list, final List<View> list2, final List<View> list3) {
    }
    
    public void onSharedElementStart(final List<String> list, final List<View> list2, final List<View> list3) {
    }
}
