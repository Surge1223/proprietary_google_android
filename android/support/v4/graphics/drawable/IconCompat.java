package android.support.v4.graphics.drawable;

import android.os.Parcelable;
import android.os.Build.VERSION;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import java.io.InputStream;
import android.content.pm.PackageManager;
import android.support.v4.content.res.ResourcesCompat;
import android.content.res.Resources;
import android.text.TextUtils;
import android.graphics.BitmapFactory;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.File;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.lang.reflect.InvocationTargetException;
import android.support.v4.os.BuildCompat;
import android.content.Context;
import android.net.Uri;
import android.graphics.Shader;
import android.graphics.Matrix;
import android.graphics.BitmapShader;
import android.graphics.Shader$TileMode;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.support.v4.util.Preconditions;
import android.graphics.drawable.Icon;
import android.util.Log;
import android.os.Bundle;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import androidx.versionedparcelable.CustomVersionedParcelable;

public class IconCompat extends CustomVersionedParcelable
{
    static final PorterDuff.Mode DEFAULT_TINT_MODE;
    public int mInt1;
    public int mInt2;
    Object mObj1;
    public ColorStateList mTintList;
    PorterDuff.Mode mTintMode;
    public int mType;
    
    static {
        DEFAULT_TINT_MODE = PorterDuff.Mode.SRC_IN;
    }
    
    public IconCompat() {
        this.mTintList = null;
        this.mTintMode = IconCompat.DEFAULT_TINT_MODE;
    }
    
    private IconCompat(final int mType) {
        this.mTintList = null;
        this.mTintMode = IconCompat.DEFAULT_TINT_MODE;
        this.mType = mType;
    }
    
    public static IconCompat createFromBundle(final Bundle bundle) {
        final int int1 = bundle.getInt("type");
        final IconCompat iconCompat = new IconCompat(int1);
        iconCompat.mInt1 = bundle.getInt("int1");
        iconCompat.mInt2 = bundle.getInt("int2");
        if (bundle.containsKey("tint_list")) {
            iconCompat.mTintList = (ColorStateList)bundle.getParcelable("tint_list");
        }
        if (bundle.containsKey("tint_mode")) {
            iconCompat.mTintMode = PorterDuff.Mode.valueOf(bundle.getString("tint_mode"));
        }
        if (int1 != -1) {
            switch (int1) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown type ");
                    sb.append(int1);
                    Log.w("IconCompat", sb.toString());
                    return null;
                }
                case 3: {
                    iconCompat.mObj1 = bundle.getByteArray("obj");
                    return iconCompat;
                }
                case 2:
                case 4: {
                    iconCompat.mObj1 = bundle.getString("obj");
                    return iconCompat;
                }
                case 1:
                case 5: {
                    break;
                }
            }
        }
        iconCompat.mObj1 = bundle.getParcelable("obj");
        return iconCompat;
    }
    
    public static IconCompat createFromIcon(final Icon mObj1) {
        Preconditions.checkNotNull(mObj1);
        final int type = getType(mObj1);
        if (type == 2) {
            return createWithResource(getResPackage(mObj1), getResId(mObj1));
        }
        if (type != 4) {
            final IconCompat iconCompat = new IconCompat(-1);
            iconCompat.mObj1 = mObj1;
            return iconCompat;
        }
        return createWithContentUri(getUri(mObj1));
    }
    
    static Bitmap createLegacyIconFromAdaptiveIcon(final Bitmap bitmap, final boolean b) {
        final int n = (int)(0.6666667f * Math.min(bitmap.getWidth(), bitmap.getHeight()));
        final Bitmap bitmap2 = Bitmap.createBitmap(n, n, Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint(3);
        final float n2 = n * 0.5f;
        final float n3 = 0.9166667f * n2;
        if (b) {
            final float n4 = 0.010416667f * n;
            paint.setColor(0);
            paint.setShadowLayer(n4, 0.0f, 0.020833334f * n, 1023410176);
            canvas.drawCircle(n2, n2, n3, paint);
            paint.setShadowLayer(n4, 0.0f, 0.0f, 503316480);
            canvas.drawCircle(n2, n2, n3, paint);
            paint.clearShadowLayer();
        }
        paint.setColor(-16777216);
        final BitmapShader shader = new BitmapShader(bitmap, Shader$TileMode.CLAMP, Shader$TileMode.CLAMP);
        final Matrix localMatrix = new Matrix();
        localMatrix.setTranslate((float)(-(bitmap.getWidth() - n) / 2), (float)(-(bitmap.getHeight() - n) / 2));
        shader.setLocalMatrix(localMatrix);
        paint.setShader((Shader)shader);
        canvas.drawCircle(n2, n2, n3, paint);
        canvas.setBitmap((Bitmap)null);
        return bitmap2;
    }
    
    public static IconCompat createWithBitmap(final Bitmap mObj1) {
        if (mObj1 != null) {
            final IconCompat iconCompat = new IconCompat(1);
            iconCompat.mObj1 = mObj1;
            return iconCompat;
        }
        throw new IllegalArgumentException("Bitmap must not be null.");
    }
    
    public static IconCompat createWithContentUri(final Uri uri) {
        if (uri != null) {
            return createWithContentUri(uri.toString());
        }
        throw new IllegalArgumentException("Uri must not be null.");
    }
    
    public static IconCompat createWithContentUri(final String mObj1) {
        if (mObj1 != null) {
            final IconCompat iconCompat = new IconCompat(4);
            iconCompat.mObj1 = mObj1;
            return iconCompat;
        }
        throw new IllegalArgumentException("Uri must not be null.");
    }
    
    public static IconCompat createWithResource(final Context context, final int mInt1) {
        if (context != null) {
            final IconCompat iconCompat = new IconCompat(2);
            iconCompat.mInt1 = mInt1;
            iconCompat.mObj1 = context.getPackageName();
            return iconCompat;
        }
        throw new IllegalArgumentException("Context must not be null.");
    }
    
    public static IconCompat createWithResource(final String mObj1, final int mInt1) {
        if (mObj1 != null) {
            final IconCompat iconCompat = new IconCompat(2);
            iconCompat.mInt1 = mInt1;
            iconCompat.mObj1 = mObj1;
            return iconCompat;
        }
        throw new IllegalArgumentException("Package must not be null.");
    }
    
    public static int getResId(final Icon icon) {
        if (BuildCompat.isAtLeastP()) {
            return icon.getResId();
        }
        try {
            return (int)icon.getClass().getMethod("getResId", (Class<?>[])new Class[0]).invoke(icon, new Object[0]);
        }
        catch (NoSuchMethodException ex) {
            Log.e("IconCompat", "Unable to get icon resource", (Throwable)ex);
            return 0;
        }
        catch (InvocationTargetException ex2) {
            Log.e("IconCompat", "Unable to get icon resource", (Throwable)ex2);
            return 0;
        }
        catch (IllegalAccessException ex3) {
            Log.e("IconCompat", "Unable to get icon resource", (Throwable)ex3);
            return 0;
        }
    }
    
    public static String getResPackage(final Icon icon) {
        if (BuildCompat.isAtLeastP()) {
            return icon.getResPackage();
        }
        try {
            return (String)icon.getClass().getMethod("getResPackage", (Class<?>[])new Class[0]).invoke(icon, new Object[0]);
        }
        catch (NoSuchMethodException ex) {
            Log.e("IconCompat", "Unable to get icon package", (Throwable)ex);
            return null;
        }
        catch (InvocationTargetException ex2) {
            Log.e("IconCompat", "Unable to get icon package", (Throwable)ex2);
            return null;
        }
        catch (IllegalAccessException ex3) {
            Log.e("IconCompat", "Unable to get icon package", (Throwable)ex3);
            return null;
        }
    }
    
    public static int getType(final Icon icon) {
        if (BuildCompat.isAtLeastP()) {
            return icon.getType();
        }
        try {
            return (int)icon.getClass().getMethod("getType", (Class<?>[])new Class[0]).invoke(icon, new Object[0]);
        }
        catch (NoSuchMethodException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to get icon type ");
            sb.append(icon);
            Log.e("IconCompat", sb.toString(), (Throwable)ex);
            return -1;
        }
        catch (InvocationTargetException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to get icon type ");
            sb2.append(icon);
            Log.e("IconCompat", sb2.toString(), (Throwable)ex2);
            return -1;
        }
        catch (IllegalAccessException ex3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to get icon type ");
            sb3.append(icon);
            Log.e("IconCompat", sb3.toString(), (Throwable)ex3);
            return -1;
        }
    }
    
    public static Uri getUri(final Icon icon) {
        if (BuildCompat.isAtLeastP()) {
            return icon.getUri();
        }
        try {
            return (Uri)icon.getClass().getMethod("getUri", (Class<?>[])new Class[0]).invoke(icon, new Object[0]);
        }
        catch (NoSuchMethodException ex) {
            Log.e("IconCompat", "Unable to get icon uri", (Throwable)ex);
            return null;
        }
        catch (InvocationTargetException ex2) {
            Log.e("IconCompat", "Unable to get icon uri", (Throwable)ex2);
            return null;
        }
        catch (IllegalAccessException ex3) {
            Log.e("IconCompat", "Unable to get icon uri", (Throwable)ex3);
            return null;
        }
    }
    
    private Drawable loadDrawableInner(final Context context) {
        switch (this.mType) {
            case 5: {
                return (Drawable)new BitmapDrawable(context.getResources(), createLegacyIconFromAdaptiveIcon((Bitmap)this.mObj1, false));
            }
            case 4: {
                final Uri parse = Uri.parse((String)this.mObj1);
                final String scheme = parse.getScheme();
                InputStream openInputStream = null;
                final InputStream inputStream = null;
                Label_0241: {
                    if (!"content".equals(scheme)) {
                        if (!"file".equals(scheme)) {
                            try {
                                openInputStream = new FileInputStream(new File((String)this.mObj1));
                            }
                            catch (FileNotFoundException ex) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Unable to load image from path: ");
                                sb.append(parse);
                                Log.w("IconCompat", sb.toString(), (Throwable)ex);
                            }
                            break Label_0241;
                        }
                    }
                    try {
                        openInputStream = context.getContentResolver().openInputStream(parse);
                    }
                    catch (Exception ex2) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unable to load image from URI: ");
                        sb2.append(parse);
                        Log.w("IconCompat", sb2.toString(), (Throwable)ex2);
                        openInputStream = inputStream;
                    }
                }
                if (openInputStream != null) {
                    return (Drawable)new BitmapDrawable(context.getResources(), BitmapFactory.decodeStream(openInputStream));
                }
                break;
            }
            case 3: {
                return (Drawable)new BitmapDrawable(context.getResources(), BitmapFactory.decodeByteArray((byte[])this.mObj1, this.mInt1, this.mInt2));
            }
            case 2: {
                Object o;
                if (TextUtils.isEmpty((CharSequence)(o = this.mObj1))) {
                    o = context.getPackageName();
                }
                Label_0372: {
                    if ("android".equals(o)) {
                        o = Resources.getSystem();
                        break Label_0372;
                    }
                    final PackageManager packageManager = context.getPackageManager();
                    try {
                        final ApplicationInfo applicationInfo = packageManager.getApplicationInfo((String)o, 8192);
                        if (applicationInfo != null) {
                            o = packageManager.getResourcesForApplication(applicationInfo);
                            try {
                                return ResourcesCompat.getDrawable((Resources)o, this.mInt1, context.getTheme());
                            }
                            catch (RuntimeException ex3) {
                                Log.e("IconCompat", String.format("Unable to load resource 0x%08x from pkg=%s", this.mInt1, this.mObj1), (Throwable)ex3);
                            }
                        }
                    }
                    catch (PackageManager$NameNotFoundException ex4) {
                        Log.e("IconCompat", String.format("Unable to find pkg=%s for icon %s", o, this), (Throwable)ex4);
                    }
                }
                break;
            }
            case 1: {
                return (Drawable)new BitmapDrawable(context.getResources(), (Bitmap)this.mObj1);
            }
        }
        return null;
    }
    
    private static String typeToString(final int n) {
        switch (n) {
            default: {
                return "UNKNOWN";
            }
            case 5: {
                return "BITMAP_MASKABLE";
            }
            case 4: {
                return "URI";
            }
            case 3: {
                return "DATA";
            }
            case 2: {
                return "RESOURCE";
            }
            case 1: {
                return "BITMAP";
            }
        }
    }
    
    public int getResId() {
        if (this.mType == -1 && Build.VERSION.SDK_INT >= 23) {
            return getResId((Icon)this.mObj1);
        }
        if (this.mType == 2) {
            return this.mInt1;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("called getResId() on ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    public String getResPackage() {
        if (this.mType == -1 && Build.VERSION.SDK_INT >= 23) {
            return getResPackage((Icon)this.mObj1);
        }
        if (this.mType == 2) {
            return (String)this.mObj1;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("called getResPackage() on ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    public Drawable loadDrawable(final Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return this.toIcon().loadDrawable(context);
        }
        final Drawable loadDrawableInner = this.loadDrawableInner(context);
        if (loadDrawableInner != null && (this.mTintList != null || this.mTintMode != IconCompat.DEFAULT_TINT_MODE)) {
            loadDrawableInner.mutate();
            DrawableCompat.setTintList(loadDrawableInner, this.mTintList);
            DrawableCompat.setTintMode(loadDrawableInner, this.mTintMode);
        }
        return loadDrawableInner;
    }
    
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        final int mType = this.mType;
        if (mType != -1) {
            switch (mType) {
                default: {
                    throw new IllegalArgumentException("Invalid icon");
                }
                case 3: {
                    bundle.putByteArray("obj", (byte[])this.mObj1);
                    break;
                }
                case 2:
                case 4: {
                    bundle.putString("obj", (String)this.mObj1);
                    break;
                }
                case 1:
                case 5: {
                    bundle.putParcelable("obj", (Parcelable)this.mObj1);
                    break;
                }
            }
        }
        else {
            bundle.putParcelable("obj", (Parcelable)this.mObj1);
        }
        bundle.putInt("type", this.mType);
        bundle.putInt("int1", this.mInt1);
        bundle.putInt("int2", this.mInt2);
        if (this.mTintList != null) {
            bundle.putParcelable("tint_list", (Parcelable)this.mTintList);
        }
        if (this.mTintMode != IconCompat.DEFAULT_TINT_MODE) {
            bundle.putString("tint_mode", this.mTintMode.name());
        }
        return bundle;
    }
    
    public Icon toIcon() {
        final int mType = this.mType;
        if (mType != -1) {
            Icon icon = null;
            switch (mType) {
                default: {
                    throw new IllegalArgumentException("Unknown type");
                }
                case 5: {
                    if (Build.VERSION.SDK_INT >= 26) {
                        icon = Icon.createWithAdaptiveBitmap((Bitmap)this.mObj1);
                        break;
                    }
                    icon = Icon.createWithBitmap(createLegacyIconFromAdaptiveIcon((Bitmap)this.mObj1, false));
                    break;
                }
                case 4: {
                    icon = Icon.createWithContentUri((String)this.mObj1);
                    break;
                }
                case 3: {
                    icon = Icon.createWithData((byte[])this.mObj1, this.mInt1, this.mInt2);
                    break;
                }
                case 2: {
                    icon = Icon.createWithResource((String)this.mObj1, this.mInt1);
                    break;
                }
                case 1: {
                    icon = Icon.createWithBitmap((Bitmap)this.mObj1);
                    break;
                }
            }
            if (this.mTintList != null) {
                icon.setTintList(this.mTintList);
            }
            if (this.mTintMode != IconCompat.DEFAULT_TINT_MODE) {
                icon.setTintMode(this.mTintMode);
            }
            return icon;
        }
        return (Icon)this.mObj1;
    }
    
    @Override
    public String toString() {
        if (this.mType == -1) {
            return String.valueOf(this.mObj1);
        }
        final StringBuilder append = new StringBuilder("Icon(typ=").append(typeToString(this.mType));
        switch (this.mType) {
            case 4: {
                append.append(" uri=");
                append.append(this.mObj1);
                break;
            }
            case 3: {
                append.append(" len=");
                append.append(this.mInt1);
                if (this.mInt2 != 0) {
                    append.append(" off=");
                    append.append(this.mInt2);
                    break;
                }
                break;
            }
            case 2: {
                append.append(" pkg=");
                append.append(this.getResPackage());
                append.append(" id=");
                append.append(String.format("0x%08x", this.getResId()));
                break;
            }
            case 1:
            case 5: {
                append.append(" size=");
                append.append(((Bitmap)this.mObj1).getWidth());
                append.append("x");
                append.append(((Bitmap)this.mObj1).getHeight());
                break;
            }
        }
        if (this.mTintList != null) {
            append.append(" tint=");
            append.append(this.mTintList);
        }
        if (this.mTintMode != IconCompat.DEFAULT_TINT_MODE) {
            append.append(" mode=");
            append.append(this.mTintMode);
        }
        append.append(")");
        return append.toString();
    }
}
