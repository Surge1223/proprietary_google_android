package android.support.v4.widget;

import android.os.Build.VERSION;

public interface AutoSizeableTextView
{
    public static final boolean PLATFORM_SUPPORTS_AUTOSIZE = Build.VERSION.SDK_INT >= 27;
}
