package android.support.v4.widget;

import android.graphics.PorterDuff.Mode;
import android.content.res.ColorStateList;

public interface TintableCompoundButton
{
    void setSupportButtonTintList(final ColorStateList p0);
    
    void setSupportButtonTintMode(final PorterDuff.Mode p0);
}
