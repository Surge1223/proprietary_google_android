package android.support.v4.widget;

import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.util.Log;
import android.os.Build.VERSION;
import android.widget.PopupWindow;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public final class PopupWindowCompat
{
    private static Field sOverlapAnchorField;
    private static boolean sOverlapAnchorFieldAttempted;
    private static Method sSetWindowLayoutTypeMethod;
    private static boolean sSetWindowLayoutTypeMethodAttempted;
    
    public static void setOverlapAnchor(final PopupWindow popupWindow, final boolean overlapAnchor) {
        if (Build.VERSION.SDK_INT >= 23) {
            popupWindow.setOverlapAnchor(overlapAnchor);
        }
        else if (Build.VERSION.SDK_INT >= 21) {
            if (!PopupWindowCompat.sOverlapAnchorFieldAttempted) {
                try {
                    (PopupWindowCompat.sOverlapAnchorField = PopupWindow.class.getDeclaredField("mOverlapAnchor")).setAccessible(true);
                }
                catch (NoSuchFieldException ex) {
                    Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", (Throwable)ex);
                }
                PopupWindowCompat.sOverlapAnchorFieldAttempted = true;
            }
            if (PopupWindowCompat.sOverlapAnchorField != null) {
                try {
                    PopupWindowCompat.sOverlapAnchorField.set(popupWindow, overlapAnchor);
                }
                catch (IllegalAccessException ex2) {
                    Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", (Throwable)ex2);
                }
            }
        }
    }
    
    public static void setWindowLayoutType(final PopupWindow popupWindow, final int windowLayoutType) {
        if (Build.VERSION.SDK_INT >= 23) {
            popupWindow.setWindowLayoutType(windowLayoutType);
            return;
        }
        if (!PopupWindowCompat.sSetWindowLayoutTypeMethodAttempted) {
            try {
                (PopupWindowCompat.sSetWindowLayoutTypeMethod = PopupWindow.class.getDeclaredMethod("setWindowLayoutType", Integer.TYPE)).setAccessible(true);
            }
            catch (Exception ex) {}
            PopupWindowCompat.sSetWindowLayoutTypeMethodAttempted = true;
        }
        if (PopupWindowCompat.sSetWindowLayoutTypeMethod != null) {
            try {
                PopupWindowCompat.sSetWindowLayoutTypeMethod.invoke(popupWindow, windowLayoutType);
            }
            catch (Exception ex2) {}
        }
    }
    
    public static void showAsDropDown(final PopupWindow popupWindow, final View view, final int n, final int n2, final int n3) {
        if (Build.VERSION.SDK_INT >= 19) {
            popupWindow.showAsDropDown(view, n, n2, n3);
        }
        else {
            int n4 = n;
            if ((GravityCompat.getAbsoluteGravity(n3, ViewCompat.getLayoutDirection(view)) & 0x7) == 0x5) {
                n4 = n - (popupWindow.getWidth() - view.getWidth());
            }
            popupWindow.showAsDropDown(view, n4, n2);
        }
    }
}
