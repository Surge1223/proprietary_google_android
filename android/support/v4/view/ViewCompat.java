package android.support.v4.view;

import android.view.ViewGroup;
import android.support.compat.R;
import android.util.SparseBooleanArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import android.view.View$OnApplyWindowInsetsListener;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View$AccessibilityDelegate;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.annotation.SuppressLint;
import android.view.WindowManager;
import android.view.Display;
import android.graphics.PorterDuff.Mode;
import android.content.res.ColorStateList;
import android.support.v4.os.BuildCompat;
import android.view.KeyEvent;
import android.view.WindowInsets;
import android.os.Build.VERSION;
import android.view.ViewParent;
import android.view.View;
import java.util.WeakHashMap;
import android.graphics.Rect;
import java.util.concurrent.atomic.AtomicInteger;
import java.lang.reflect.Field;

public class ViewCompat
{
    private static boolean sAccessibilityDelegateCheckFailed;
    private static Field sAccessibilityDelegateField;
    private static Field sMinHeightField;
    private static boolean sMinHeightFieldFetched;
    private static Field sMinWidthField;
    private static boolean sMinWidthFieldFetched;
    private static final AtomicInteger sNextGeneratedId;
    private static ThreadLocal<Rect> sThreadLocalRect;
    private static WeakHashMap<View, String> sTransitionNameMap;
    private static WeakHashMap<View, ViewPropertyAnimatorCompat> sViewPropertyAnimatorMap;
    
    static {
        sNextGeneratedId = new AtomicInteger(1);
        ViewCompat.sViewPropertyAnimatorMap = null;
        ViewCompat.sAccessibilityDelegateCheckFailed = false;
    }
    
    public static ViewPropertyAnimatorCompat animate(final View view) {
        if (ViewCompat.sViewPropertyAnimatorMap == null) {
            ViewCompat.sViewPropertyAnimatorMap = new WeakHashMap<View, ViewPropertyAnimatorCompat>();
        }
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat;
        if ((viewPropertyAnimatorCompat = ViewCompat.sViewPropertyAnimatorMap.get(view)) == null) {
            viewPropertyAnimatorCompat = new ViewPropertyAnimatorCompat(view);
            ViewCompat.sViewPropertyAnimatorMap.put(view, viewPropertyAnimatorCompat);
        }
        return viewPropertyAnimatorCompat;
    }
    
    private static void compatOffsetLeftAndRight(final View view, final int n) {
        view.offsetLeftAndRight(n);
        if (view.getVisibility() == 0) {
            tickleInvalidationFlag(view);
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                tickleInvalidationFlag((View)parent);
            }
        }
    }
    
    private static void compatOffsetTopAndBottom(final View view, final int n) {
        view.offsetTopAndBottom(n);
        if (view.getVisibility() == 0) {
            tickleInvalidationFlag(view);
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                tickleInvalidationFlag((View)parent);
            }
        }
    }
    
    public static WindowInsetsCompat dispatchApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
        if (Build.VERSION.SDK_INT >= 21) {
            final WindowInsets windowInsets = (WindowInsets)WindowInsetsCompat.unwrap(windowInsetsCompat);
            final WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
            WindowInsets windowInsets2;
            if (dispatchApplyWindowInsets != (windowInsets2 = windowInsets)) {
                windowInsets2 = new WindowInsets(dispatchApplyWindowInsets);
            }
            return WindowInsetsCompat.wrap(windowInsets2);
        }
        return windowInsetsCompat;
    }
    
    public static boolean dispatchUnhandledKeyEventPost(final View view, final KeyEvent keyEvent) {
        return !BuildCompat.isAtLeastP() && UnhandledKeyEventManager.at(view).dispatch(view, keyEvent);
    }
    
    public static boolean dispatchUnhandledKeyEventPre(final View view, final KeyEvent keyEvent) {
        final boolean atLeastP = BuildCompat.isAtLeastP();
        final boolean b = false;
        if (atLeastP) {
            return false;
        }
        boolean b2 = b;
        if (UnhandledKeyEventManager.at(view).hasFocus()) {
            b2 = b;
            if (UnhandledKeyEventManager.at(view).dispatch(view, keyEvent)) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static ColorStateList getBackgroundTintList(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintList();
        }
        ColorStateList supportBackgroundTintList;
        if (view instanceof TintableBackgroundView) {
            supportBackgroundTintList = ((TintableBackgroundView)view).getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    public static PorterDuff.Mode getBackgroundTintMode(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintMode();
        }
        PorterDuff.Mode supportBackgroundTintMode;
        if (view instanceof TintableBackgroundView) {
            supportBackgroundTintMode = ((TintableBackgroundView)view).getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    public static Display getDisplay(final View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getDisplay();
        }
        if (isAttachedToWindow(view)) {
            return ((WindowManager)view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }
    
    public static float getElevation(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getElevation();
        }
        return 0.0f;
    }
    
    private static Rect getEmptyTempRect() {
        if (ViewCompat.sThreadLocalRect == null) {
            ViewCompat.sThreadLocalRect = new ThreadLocal<Rect>();
        }
        Rect rect;
        if ((rect = ViewCompat.sThreadLocalRect.get()) == null) {
            rect = new Rect();
            ViewCompat.sThreadLocalRect.set(rect);
        }
        rect.setEmpty();
        return rect;
    }
    
    public static boolean getFitsSystemWindows(final View view) {
        return Build.VERSION.SDK_INT >= 16 && view.getFitsSystemWindows();
    }
    
    public static int getImportantForAccessibility(final View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getImportantForAccessibility();
        }
        return 0;
    }
    
    @SuppressLint({ "InlinedApi" })
    public static int getImportantForAutofill(final View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return view.getImportantForAutofill();
        }
        return 0;
    }
    
    public static int getLayoutDirection(final View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getLayoutDirection();
        }
        return 0;
    }
    
    public static int getMinimumHeight(final View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumHeight();
        }
        if (!ViewCompat.sMinHeightFieldFetched) {
            try {
                (ViewCompat.sMinHeightField = View.class.getDeclaredField("mMinHeight")).setAccessible(true);
            }
            catch (NoSuchFieldException ex) {}
            ViewCompat.sMinHeightFieldFetched = true;
        }
        if (ViewCompat.sMinHeightField != null) {
            try {
                return (int)ViewCompat.sMinHeightField.get(view);
            }
            catch (Exception ex2) {}
        }
        return 0;
    }
    
    public static int getMinimumWidth(final View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumWidth();
        }
        if (!ViewCompat.sMinWidthFieldFetched) {
            try {
                (ViewCompat.sMinWidthField = View.class.getDeclaredField("mMinWidth")).setAccessible(true);
            }
            catch (NoSuchFieldException ex) {}
            ViewCompat.sMinWidthFieldFetched = true;
        }
        if (ViewCompat.sMinWidthField != null) {
            try {
                return (int)ViewCompat.sMinWidthField.get(view);
            }
            catch (Exception ex2) {}
        }
        return 0;
    }
    
    public static ViewParent getParentForAccessibility(final View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getParentForAccessibility();
        }
        return view.getParent();
    }
    
    public static String getTransitionName(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getTransitionName();
        }
        if (ViewCompat.sTransitionNameMap == null) {
            return null;
        }
        return ViewCompat.sTransitionNameMap.get(view);
    }
    
    @Deprecated
    public static float getTranslationX(final View view) {
        return view.getTranslationX();
    }
    
    @Deprecated
    public static float getTranslationY(final View view) {
        return view.getTranslationY();
    }
    
    public static int getWindowSystemUiVisibility(final View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getWindowSystemUiVisibility();
        }
        return 0;
    }
    
    @Deprecated
    public static float getY(final View view) {
        return view.getY();
    }
    
    public static float getZ(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getZ();
        }
        return 0.0f;
    }
    
    public static boolean hasAccessibilityDelegate(final View view) {
        final boolean sAccessibilityDelegateCheckFailed = ViewCompat.sAccessibilityDelegateCheckFailed;
        boolean b = false;
        if (sAccessibilityDelegateCheckFailed) {
            return false;
        }
        if (ViewCompat.sAccessibilityDelegateField == null) {
            try {
                (ViewCompat.sAccessibilityDelegateField = View.class.getDeclaredField("mAccessibilityDelegate")).setAccessible(true);
            }
            catch (Throwable t) {
                ViewCompat.sAccessibilityDelegateCheckFailed = true;
                return false;
            }
        }
        try {
            if (ViewCompat.sAccessibilityDelegateField.get(view) != null) {
                b = true;
            }
            return b;
        }
        catch (Throwable t2) {
            ViewCompat.sAccessibilityDelegateCheckFailed = true;
            return false;
        }
    }
    
    public static boolean hasOnClickListeners(final View view) {
        return Build.VERSION.SDK_INT >= 15 && view.hasOnClickListeners();
    }
    
    public static boolean hasOverlappingRendering(final View view) {
        return Build.VERSION.SDK_INT < 16 || view.hasOverlappingRendering();
    }
    
    public static boolean hasTransientState(final View view) {
        return Build.VERSION.SDK_INT >= 16 && view.hasTransientState();
    }
    
    public static boolean isAttachedToWindow(final View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isAttachedToWindow();
        }
        return view.getWindowToken() != null;
    }
    
    public static boolean isLaidOut(final View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isLaidOut();
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }
    
    public static boolean isNestedScrollingEnabled(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.isNestedScrollingEnabled();
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).isNestedScrollingEnabled();
    }
    
    public static void offsetLeftAndRight(final View view, final int n) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.offsetLeftAndRight(n);
        }
        else if (Build.VERSION.SDK_INT >= 21) {
            final Rect emptyTempRect = getEmptyTempRect();
            boolean b = false;
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                final View view2 = (View)parent;
                emptyTempRect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                b = (emptyTempRect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()) ^ true);
            }
            compatOffsetLeftAndRight(view, n);
            if (b && emptyTempRect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View)parent).invalidate(emptyTempRect);
            }
        }
        else {
            compatOffsetLeftAndRight(view, n);
        }
    }
    
    public static void offsetTopAndBottom(final View view, final int n) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.offsetTopAndBottom(n);
        }
        else if (Build.VERSION.SDK_INT >= 21) {
            final Rect emptyTempRect = getEmptyTempRect();
            boolean b = false;
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                final View view2 = (View)parent;
                emptyTempRect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                b = (emptyTempRect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()) ^ true);
            }
            compatOffsetTopAndBottom(view, n);
            if (b && emptyTempRect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View)parent).invalidate(emptyTempRect);
            }
        }
        else {
            compatOffsetTopAndBottom(view, n);
        }
    }
    
    public static WindowInsetsCompat onApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
        if (Build.VERSION.SDK_INT >= 21) {
            final WindowInsets windowInsets = (WindowInsets)WindowInsetsCompat.unwrap(windowInsetsCompat);
            final WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
            WindowInsets windowInsets2;
            if (onApplyWindowInsets != (windowInsets2 = windowInsets)) {
                windowInsets2 = new WindowInsets(onApplyWindowInsets);
            }
            return WindowInsetsCompat.wrap(windowInsets2);
        }
        return windowInsetsCompat;
    }
    
    public static void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        view.onInitializeAccessibilityNodeInfo(accessibilityNodeInfoCompat.unwrap());
    }
    
    public static boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
        return Build.VERSION.SDK_INT >= 16 && view.performAccessibilityAction(n, bundle);
    }
    
    public static void postInvalidateOnAnimation(final View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation();
        }
        else {
            view.postInvalidate();
        }
    }
    
    public static void postInvalidateOnAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation(n, n2, n3, n4);
        }
        else {
            view.postInvalidate(n, n2, n3, n4);
        }
    }
    
    public static void postOnAnimation(final View view, final Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        }
        else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay());
        }
    }
    
    public static void postOnAnimationDelayed(final View view, final Runnable runnable, final long n) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimationDelayed(runnable, n);
        }
        else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay() + n);
        }
    }
    
    public static void requestApplyInsets(final View view) {
        if (Build.VERSION.SDK_INT >= 20) {
            view.requestApplyInsets();
        }
        else if (Build.VERSION.SDK_INT >= 16) {
            view.requestFitSystemWindows();
        }
    }
    
    public static void setAccessibilityDelegate(final View view, final AccessibilityDelegateCompat accessibilityDelegateCompat) {
        View$AccessibilityDelegate bridge;
        if (accessibilityDelegateCompat == null) {
            bridge = null;
        }
        else {
            bridge = accessibilityDelegateCompat.getBridge();
        }
        view.setAccessibilityDelegate(bridge);
    }
    
    public static void setBackground(final View view, final Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        }
        else {
            view.setBackgroundDrawable(drawable);
        }
    }
    
    public static void setBackgroundTintList(final View view, final ColorStateList list) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintList(list);
            if (Build.VERSION.SDK_INT == 21) {
                final Drawable background = view.getBackground();
                final boolean b = view.getBackgroundTintList() != null || view.getBackgroundTintMode() != null;
                if (background != null && b) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        }
        else if (view instanceof TintableBackgroundView) {
            ((TintableBackgroundView)view).setSupportBackgroundTintList(list);
        }
    }
    
    public static void setBackgroundTintMode(final View view, final PorterDuff.Mode porterDuff$Mode) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintMode(porterDuff$Mode);
            if (Build.VERSION.SDK_INT == 21) {
                final Drawable background = view.getBackground();
                final boolean b = view.getBackgroundTintList() != null || view.getBackgroundTintMode() != null;
                if (background != null && b) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        }
        else if (view instanceof TintableBackgroundView) {
            ((TintableBackgroundView)view).setSupportBackgroundTintMode(porterDuff$Mode);
        }
    }
    
    public static void setElevation(final View view, final float elevation) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setElevation(elevation);
        }
    }
    
    public static void setImportantForAccessibility(final View view, final int importantForAccessibility) {
        if (Build.VERSION.SDK_INT >= 19) {
            view.setImportantForAccessibility(importantForAccessibility);
        }
        else if (Build.VERSION.SDK_INT >= 16) {
            int importantForAccessibility2;
            if ((importantForAccessibility2 = importantForAccessibility) == 4) {
                importantForAccessibility2 = 2;
            }
            view.setImportantForAccessibility(importantForAccessibility2);
        }
    }
    
    public static void setImportantForAutofill(final View view, final int importantForAutofill) {
        if (Build.VERSION.SDK_INT >= 26) {
            view.setImportantForAutofill(importantForAutofill);
        }
    }
    
    public static void setLayerPaint(final View view, final Paint layerPaint) {
        if (Build.VERSION.SDK_INT >= 17) {
            view.setLayerPaint(layerPaint);
        }
        else {
            view.setLayerType(view.getLayerType(), layerPaint);
            view.invalidate();
        }
    }
    
    public static void setOnApplyWindowInsetsListener(final View view, final OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (onApplyWindowInsetsListener == null) {
                view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)null);
                return;
            }
            view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)new View$OnApplyWindowInsetsListener() {
                public WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
                    return (WindowInsets)WindowInsetsCompat.unwrap(onApplyWindowInsetsListener.onApplyWindowInsets(view, WindowInsetsCompat.wrap(windowInsets)));
                }
            });
        }
    }
    
    public static void setScrollIndicators(final View view, final int n, final int n2) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.setScrollIndicators(n, n2);
        }
    }
    
    public static void setTransitionName(final View view, final String transitionName) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setTransitionName(transitionName);
        }
        else {
            if (ViewCompat.sTransitionNameMap == null) {
                ViewCompat.sTransitionNameMap = new WeakHashMap<View, String>();
            }
            ViewCompat.sTransitionNameMap.put(view, transitionName);
        }
    }
    
    public static void stopNestedScroll(final View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.stopNestedScroll();
        }
        else if (view instanceof NestedScrollingChild) {
            ((NestedScrollingChild)view).stopNestedScroll();
        }
    }
    
    private static void tickleInvalidationFlag(final View view) {
        final float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }
    
    public interface OnUnhandledKeyEventListenerCompat
    {
        boolean onUnhandledKeyEvent(final View p0, final KeyEvent p1);
    }
    
    static class UnhandledKeyEventManager
    {
        private static final ArrayList<WeakReference<View>> sViewsWithListeners;
        private SparseBooleanArray mCapturedKeys;
        private WeakReference<View> mCurrentReceiver;
        private WeakHashMap<View, Boolean> mViewsContainingListeners;
        
        static {
            sViewsWithListeners = new ArrayList<WeakReference<View>>();
        }
        
        UnhandledKeyEventManager() {
            this.mViewsContainingListeners = null;
            this.mCapturedKeys = null;
            this.mCurrentReceiver = null;
        }
        
        static UnhandledKeyEventManager at(final View view) {
            UnhandledKeyEventManager unhandledKeyEventManager;
            if ((unhandledKeyEventManager = (UnhandledKeyEventManager)view.getTag(R.id.tag_unhandled_key_event_manager)) == null) {
                unhandledKeyEventManager = new UnhandledKeyEventManager();
                view.setTag(R.id.tag_unhandled_key_event_manager, (Object)unhandledKeyEventManager);
            }
            return unhandledKeyEventManager;
        }
        
        private View dispatchInOrder(final View view, final KeyEvent keyEvent) {
            if (this.mViewsContainingListeners == null || !this.mViewsContainingListeners.containsKey(view)) {
                return null;
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                    final View dispatchInOrder = this.dispatchInOrder(viewGroup.getChildAt(i), keyEvent);
                    if (dispatchInOrder != null) {
                        return dispatchInOrder;
                    }
                }
            }
            if (this.onUnhandledKeyEvent(view, keyEvent)) {
                return view;
            }
            return null;
        }
        
        private SparseBooleanArray getCapturedKeys() {
            if (this.mCapturedKeys == null) {
                this.mCapturedKeys = new SparseBooleanArray();
            }
            return this.mCapturedKeys;
        }
        
        private void recalcViewsWithUnhandled() {
            if (this.mViewsContainingListeners != null) {
                this.mViewsContainingListeners.clear();
            }
            if (UnhandledKeyEventManager.sViewsWithListeners.isEmpty()) {
                return;
            }
            synchronized (UnhandledKeyEventManager.sViewsWithListeners) {
                if (this.mViewsContainingListeners == null) {
                    this.mViewsContainingListeners = new WeakHashMap<View, Boolean>();
                }
                for (int i = UnhandledKeyEventManager.sViewsWithListeners.size() - 1; i >= 0; --i) {
                    final View view = UnhandledKeyEventManager.sViewsWithListeners.get(i).get();
                    if (view == null) {
                        UnhandledKeyEventManager.sViewsWithListeners.remove(i);
                    }
                    else {
                        this.mViewsContainingListeners.put(view, Boolean.TRUE);
                        for (ViewParent viewParent = view.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
                            this.mViewsContainingListeners.put((View)viewParent, Boolean.TRUE);
                        }
                    }
                }
            }
        }
        
        private void updateCaptureState(final KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                this.getCapturedKeys().append(keyEvent.getKeyCode(), true);
            }
            else if (keyEvent.getAction() == 1) {
                this.getCapturedKeys().delete(keyEvent.getKeyCode());
            }
        }
        
        boolean dispatch(View dispatchInOrder, final KeyEvent keyEvent) {
            this.updateCaptureState(keyEvent);
            final WeakReference<View> mCurrentReceiver = this.mCurrentReceiver;
            boolean b = true;
            if (mCurrentReceiver != null) {
                dispatchInOrder = this.mCurrentReceiver.get();
                if (this.getCapturedKeys().size() == 0) {
                    this.mCurrentReceiver = null;
                }
                return dispatchInOrder == null || !ViewCompat.isAttachedToWindow(dispatchInOrder) || this.onUnhandledKeyEvent(dispatchInOrder, keyEvent);
            }
            if (keyEvent.getAction() == 0 && this.getCapturedKeys().size() == 1) {
                this.recalcViewsWithUnhandled();
            }
            dispatchInOrder = this.dispatchInOrder(dispatchInOrder, keyEvent);
            if (dispatchInOrder != null) {
                this.mCurrentReceiver = new WeakReference<View>(dispatchInOrder);
            }
            if (dispatchInOrder == null) {
                b = false;
            }
            return b;
        }
        
        boolean hasFocus() {
            return this.mCurrentReceiver != null;
        }
        
        boolean onUnhandledKeyEvent(final View view, final KeyEvent keyEvent) {
            final ArrayList list = (ArrayList)view.getTag(R.id.tag_unhandled_key_listeners);
            if (list != null) {
                for (int i = list.size() - 1; i >= 0; --i) {
                    if (list.get(i).onUnhandledKeyEvent(view, keyEvent)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
