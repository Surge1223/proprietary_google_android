package android.support.v4.content.res;

import android.os.Looper;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.util.Log;
import org.xmlpull.v1.XmlPullParser;
import android.support.v4.graphics.TypefaceCompat;
import android.os.Handler;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.content.Context;
import android.content.res.Resources$NotFoundException;
import android.os.Build.VERSION;
import android.graphics.drawable.Drawable;
import android.content.res.Resources.Theme;
import android.content.res.Resources;

public final class ResourcesCompat
{
    public static Drawable getDrawable(final Resources resources, final int n, final Resources.Theme resources$Theme) throws Resources$NotFoundException {
        if (Build.VERSION.SDK_INT >= 21) {
            return resources.getDrawable(n, resources$Theme);
        }
        return resources.getDrawable(n);
    }
    
    public static Typeface getFont(final Context context, final int n, final TypedValue typedValue, final int n2, final FontCallback fontCallback) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, typedValue, n2, fontCallback, null, true);
    }
    
    private static Typeface loadFont(final Context context, final int n, final TypedValue typedValue, final int n2, final FontCallback fontCallback, final Handler handler, final boolean b) {
        final Resources resources = context.getResources();
        resources.getValue(n, typedValue, true);
        final Typeface loadFont = loadFont(context, resources, typedValue, n, n2, fontCallback, handler, b);
        if (loadFont == null && fontCallback == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Font resource ID #0x");
            sb.append(Integer.toHexString(n));
            sb.append(" could not be retrieved.");
            throw new Resources$NotFoundException(sb.toString());
        }
        return loadFont;
    }
    
    private static Typeface loadFont(final Context ex, final Resources resources, TypedValue string, final int n, final int n2, final FontCallback fontCallback, final Handler handler, final boolean b) {
        if (string.string == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Resource \"");
            sb.append(resources.getResourceName(n));
            sb.append("\" (");
            sb.append(Integer.toHexString(n));
            sb.append(") is not a Font: ");
            sb.append(string);
            throw new Resources$NotFoundException(sb.toString());
        }
        string = (TypedValue)string.string.toString();
        if (!((String)string).startsWith("res/")) {
            if (fontCallback != null) {
                fontCallback.callbackFailAsync(-3, handler);
            }
            return null;
        }
        final Typeface fromCache = TypefaceCompat.findFromCache(resources, n, n2);
        if (fromCache != null) {
            if (fontCallback != null) {
                fontCallback.callbackSuccessAsync(fromCache, handler);
            }
            return fromCache;
        }
        Label_0270: {
            try {
                if (((String)string).toLowerCase().endsWith(".xml")) {
                    try {
                        final FontResourcesParserCompat.FamilyResourceEntry parse = FontResourcesParserCompat.parse((XmlPullParser)resources.getXml(n), resources);
                        if (parse == null) {
                            try {
                                Log.e("ResourcesCompat", "Failed to find font-family tag");
                                if (fontCallback != null) {
                                    fontCallback.callbackFailAsync(-3, handler);
                                }
                                return null;
                            }
                            catch (IOException ex) {
                                goto Label_0234;
                            }
                            catch (XmlPullParserException ex) {
                                break Label_0270;
                            }
                        }
                        try {
                            return TypefaceCompat.createFromResourcesFamilyXml((Context)ex, parse, resources, n, n2, fontCallback, handler, b);
                        }
                        catch (IOException ex) {}
                        catch (XmlPullParserException ex) {}
                    }
                    catch (IOException ex) {
                        goto Label_0234;
                    }
                    catch (XmlPullParserException ex) {
                        break Label_0270;
                    }
                }
                try {
                    final Typeface fromResourcesFontFile = TypefaceCompat.createFromResourcesFontFile((Context)ex, resources, n, (String)string, n2);
                    if (fontCallback != null) {
                        if (fromResourcesFontFile != null) {
                            try {
                                fontCallback.callbackSuccessAsync(fromResourcesFontFile, handler);
                                return fromResourcesFontFile;
                            }
                            catch (IOException ex) {
                                goto Label_0234;
                            }
                            catch (XmlPullParserException ex) {
                                break Label_0270;
                            }
                        }
                        fontCallback.callbackFailAsync(-3, handler);
                    }
                    return fromResourcesFontFile;
                }
                catch (IOException ex) {}
                catch (XmlPullParserException ex) {}
            }
            catch (IOException ex2) {}
            catch (XmlPullParserException ex3) {}
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Failed to parse xml resource ");
        sb2.append((String)string);
        Log.e("ResourcesCompat", sb2.toString(), (Throwable)ex);
        if (fontCallback != null) {
            fontCallback.callbackFailAsync(-3, handler);
        }
        return null;
    }
    
    public abstract static class FontCallback
    {
        public final void callbackFailAsync(final int n, final Handler handler) {
            Handler handler2 = handler;
            if (handler == null) {
                handler2 = new Handler(Looper.getMainLooper());
            }
            handler2.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    FontCallback.this.onFontRetrievalFailed(n);
                }
            });
        }
        
        public final void callbackSuccessAsync(final Typeface typeface, final Handler handler) {
            Handler handler2 = handler;
            if (handler == null) {
                handler2 = new Handler(Looper.getMainLooper());
            }
            handler2.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    FontCallback.this.onFontRetrieved(typeface);
                }
            });
        }
        
        public abstract void onFontRetrievalFailed(final int p0);
        
        public abstract void onFontRetrieved(final Typeface p0);
    }
}
