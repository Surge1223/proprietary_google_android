package android.support.v4.content;

import java.util.Set;
import android.net.Uri;
import android.util.Log;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.Looper;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.content.Context;
import java.util.ArrayList;
import java.util.HashMap;

public final class LocalBroadcastManager
{
    private static LocalBroadcastManager mInstance;
    private static final Object mLock;
    private final HashMap<String, ArrayList<ReceiverRecord>> mActions;
    private final Context mAppContext;
    private final Handler mHandler;
    private final ArrayList<BroadcastRecord> mPendingBroadcasts;
    private final HashMap<BroadcastReceiver, ArrayList<ReceiverRecord>> mReceivers;
    
    static {
        mLock = new Object();
    }
    
    private LocalBroadcastManager(final Context mAppContext) {
        this.mReceivers = new HashMap<BroadcastReceiver, ArrayList<ReceiverRecord>>();
        this.mActions = new HashMap<String, ArrayList<ReceiverRecord>>();
        this.mPendingBroadcasts = new ArrayList<BroadcastRecord>();
        this.mAppContext = mAppContext;
        this.mHandler = new Handler(mAppContext.getMainLooper()) {
            public void handleMessage(final Message message) {
                if (message.what != 1) {
                    super.handleMessage(message);
                }
                else {
                    LocalBroadcastManager.this.executePendingBroadcasts();
                }
            }
        };
    }
    
    private void executePendingBroadcasts() {
        while (true) {
            Object mReceivers = this.mReceivers;
            synchronized (mReceivers) {
                final int size = this.mPendingBroadcasts.size();
                if (size <= 0) {
                    return;
                }
                final BroadcastRecord[] array = new BroadcastRecord[size];
                this.mPendingBroadcasts.toArray(array);
                this.mPendingBroadcasts.clear();
                // monitorexit(mReceivers)
                for (int i = 0; i < array.length; ++i) {
                    mReceivers = array[i];
                    for (int size2 = ((BroadcastRecord)mReceivers).receivers.size(), j = 0; j < size2; ++j) {
                        final ReceiverRecord receiverRecord = ((BroadcastRecord)mReceivers).receivers.get(j);
                        if (!receiverRecord.dead) {
                            receiverRecord.receiver.onReceive(this.mAppContext, ((BroadcastRecord)mReceivers).intent);
                        }
                    }
                }
            }
        }
    }
    
    public static LocalBroadcastManager getInstance(final Context context) {
        synchronized (LocalBroadcastManager.mLock) {
            if (LocalBroadcastManager.mInstance == null) {
                LocalBroadcastManager.mInstance = new LocalBroadcastManager(context.getApplicationContext());
            }
            return LocalBroadcastManager.mInstance;
        }
    }
    
    public void registerReceiver(final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter) {
        synchronized (this.mReceivers) {
            final ReceiverRecord receiverRecord = new ReceiverRecord(intentFilter, broadcastReceiver);
            ArrayList<ReceiverRecord> list;
            if ((list = this.mReceivers.get(broadcastReceiver)) == null) {
                list = new ArrayList<ReceiverRecord>(1);
                this.mReceivers.put(broadcastReceiver, list);
            }
            list.add(receiverRecord);
            for (int i = 0; i < intentFilter.countActions(); ++i) {
                final String action = intentFilter.getAction(i);
                ArrayList<ReceiverRecord> list2;
                if ((list2 = this.mActions.get(action)) == null) {
                    list2 = new ArrayList<ReceiverRecord>(1);
                    this.mActions.put(action, list2);
                }
                list2.add(receiverRecord);
            }
        }
    }
    
    public boolean sendBroadcast(final Intent intent) {
        synchronized (this.mReceivers) {
            final String action = intent.getAction();
            final String resolveTypeIfNeeded = intent.resolveTypeIfNeeded(this.mAppContext.getContentResolver());
            final Uri data = intent.getData();
            final String scheme = intent.getScheme();
            final Set categories = intent.getCategories();
            final boolean b = (intent.getFlags() & 0x8) != 0x0;
            if (b) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Resolving type ");
                sb.append(resolveTypeIfNeeded);
                sb.append(" scheme ");
                sb.append(scheme);
                sb.append(" of intent ");
                sb.append(intent);
                Log.v("LocalBroadcastManager", sb.toString());
            }
            final ArrayList<ReceiverRecord> list = this.mActions.get(intent.getAction());
            if (list != null) {
                if (b) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Action list: ");
                    sb2.append(list);
                    Log.v("LocalBroadcastManager", sb2.toString());
                }
                ArrayList<ReceiverRecord> list2 = null;
                for (int i = 0; i < list.size(); ++i) {
                    final ReceiverRecord receiverRecord = list.get(i);
                    if (b) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Matching against filter ");
                        sb3.append(receiverRecord.filter);
                        Log.v("LocalBroadcastManager", sb3.toString());
                    }
                    if (receiverRecord.broadcasting) {
                        if (b) {
                            Log.v("LocalBroadcastManager", "  Filter's target already added");
                        }
                    }
                    else {
                        final IntentFilter filter = receiverRecord.filter;
                        final ArrayList<ReceiverRecord> list3 = list2;
                        final int match = filter.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                        if (match >= 0) {
                            if (b) {
                                final StringBuilder sb4 = new StringBuilder();
                                sb4.append("  Filter matched!  match=0x");
                                sb4.append(Integer.toHexString(match));
                                Log.v("LocalBroadcastManager", sb4.toString());
                            }
                            ArrayList<ReceiverRecord> list4;
                            if ((list4 = list3) == null) {
                                list4 = new ArrayList<ReceiverRecord>();
                            }
                            list4.add(receiverRecord);
                            receiverRecord.broadcasting = true;
                            list2 = list4;
                        }
                        else if (b) {
                            String s = null;
                            switch (match) {
                                default: {
                                    s = "unknown reason";
                                    break;
                                }
                                case -1: {
                                    s = "type";
                                    break;
                                }
                                case -2: {
                                    s = "data";
                                    break;
                                }
                                case -3: {
                                    s = "action";
                                    break;
                                }
                                case -4: {
                                    s = "category";
                                    break;
                                }
                            }
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("  Filter did not match: ");
                            sb5.append(s);
                            Log.v("LocalBroadcastManager", sb5.toString());
                        }
                    }
                }
                if (list2 != null) {
                    for (int j = 0; j < list2.size(); ++j) {
                        list2.get(j).broadcasting = false;
                    }
                    this.mPendingBroadcasts.add(new BroadcastRecord(intent, list2));
                    if (!this.mHandler.hasMessages(1)) {
                        this.mHandler.sendEmptyMessage(1);
                    }
                    return true;
                }
            }
            return false;
        }
    }
    
    public void sendBroadcastSync(final Intent intent) {
        if (this.sendBroadcast(intent)) {
            this.executePendingBroadcasts();
        }
    }
    
    public void unregisterReceiver(final BroadcastReceiver broadcastReceiver) {
        synchronized (this.mReceivers) {
            final ArrayList<ReceiverRecord> list = this.mReceivers.remove(broadcastReceiver);
            if (list == null) {
                return;
            }
            for (int i = list.size() - 1; i >= 0; --i) {
                final ReceiverRecord receiverRecord = list.get(i);
                receiverRecord.dead = true;
                for (int j = 0; j < receiverRecord.filter.countActions(); ++j) {
                    final String action = receiverRecord.filter.getAction(j);
                    final ArrayList<ReceiverRecord> list2 = this.mActions.get(action);
                    if (list2 != null) {
                        for (int k = list2.size() - 1; k >= 0; --k) {
                            final ReceiverRecord receiverRecord2 = list2.get(k);
                            if (receiverRecord2.receiver == broadcastReceiver) {
                                receiverRecord2.dead = true;
                                list2.remove(k);
                            }
                        }
                        if (list2.size() <= 0) {
                            this.mActions.remove(action);
                        }
                    }
                }
            }
        }
    }
    
    private static final class BroadcastRecord
    {
        final Intent intent;
        final ArrayList<ReceiverRecord> receivers;
        
        BroadcastRecord(final Intent intent, final ArrayList<ReceiverRecord> receivers) {
            this.intent = intent;
            this.receivers = receivers;
        }
    }
    
    private static final class ReceiverRecord
    {
        boolean broadcasting;
        boolean dead;
        final IntentFilter filter;
        final BroadcastReceiver receiver;
        
        ReceiverRecord(final IntentFilter filter, final BroadcastReceiver receiver) {
            this.filter = filter;
            this.receiver = receiver;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.receiver);
            sb.append(" filter=");
            sb.append(this.filter);
            if (this.dead) {
                sb.append(" DEAD");
            }
            sb.append("}");
            return sb.toString();
        }
    }
}
