package android.support.v4.util;

public interface Consumer<T>
{
    void accept(final T p0);
}
