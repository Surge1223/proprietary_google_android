package android.support.v4.media;

import android.media.session.MediaSessionManager$RemoteUserInfo;

class MediaSessionManagerImplApi28 extends MediaSessionManagerImplApi21
{
    static final class RemoteUserInfo implements RemoteUserInfoImpl
    {
        MediaSessionManager$RemoteUserInfo mObject;
        
        RemoteUserInfo(final String s, final int n, final int n2) {
            this.mObject = new MediaSessionManager$RemoteUserInfo(s, n, n2);
        }
    }
}
