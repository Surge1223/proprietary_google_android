package android.support.v4.media;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcelable;
import android.os.Bundle;

class MediaUtils2
{
    static final MediaBrowserServiceCompat.BrowserRoot sDefaultBrowserRoot;
    
    static {
        sDefaultBrowserRoot = new MediaBrowserServiceCompat.BrowserRoot("android.media.MediaLibraryService2", null);
    }
    
    static List<Bundle> convertToBundleList(final Parcelable[] array) {
        if (array == null) {
            return null;
        }
        final ArrayList<Bundle> list = new ArrayList<Bundle>();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add((Bundle)array[i]);
        }
        return list;
    }
    
    static List<MediaSession2.CommandButton> convertToCommandButtonList(final Parcelable[] array) {
        final ArrayList<MediaSession2.CommandButton> list = new ArrayList<MediaSession2.CommandButton>();
        for (int i = 0; i < array.length; ++i) {
            if (array[i] instanceof Bundle) {
                final MediaSession2.CommandButton fromBundle = MediaSession2.CommandButton.fromBundle((Bundle)array[i]);
                if (fromBundle != null) {
                    list.add(fromBundle);
                }
            }
        }
        return list;
    }
    
    static List<MediaItem2> convertToMediaItem2List(final Parcelable[] array) {
        final ArrayList<MediaItem2> list = new ArrayList<MediaItem2>();
        if (array != null) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] instanceof Bundle) {
                    final MediaItem2 fromBundle = MediaItem2.fromBundle((Bundle)array[i]);
                    if (fromBundle != null) {
                        list.add(fromBundle);
                    }
                }
            }
        }
        return list;
    }
}
