package android.support.v4.media;

import android.text.TextUtils;
import android.support.v4.app.BundleCompat;
import android.os.Bundle;
import android.content.ComponentName;

final class SessionToken2ImplBase implements SupportLibraryImpl
{
    private final ComponentName mComponentName;
    private final IMediaSession2 mISession2;
    private final String mPackageName;
    private final String mServiceName;
    private final String mSessionId;
    private final int mType;
    private final int mUid;
    
    SessionToken2ImplBase(final int mUid, final int mType, final String mPackageName, final String mServiceName, final String mSessionId, final IMediaSession2 miSession2) {
        this.mUid = mUid;
        this.mType = mType;
        this.mPackageName = mPackageName;
        this.mServiceName = mServiceName;
        ComponentName mComponentName;
        if (this.mType == 0) {
            mComponentName = null;
        }
        else {
            mComponentName = new ComponentName(mPackageName, mServiceName);
        }
        this.mComponentName = mComponentName;
        this.mSessionId = mSessionId;
        this.mISession2 = miSession2;
    }
    
    public static SessionToken2ImplBase fromBundle(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        final int int1 = bundle.getInt("android.media.token.uid");
        final int int2 = bundle.getInt("android.media.token.type", -1);
        final String string = bundle.getString("android.media.token.package_name");
        final String string2 = bundle.getString("android.media.token.service_name");
        final String string3 = bundle.getString("android.media.token.session_id");
        final IMediaSession2 interface1 = IMediaSession2.Stub.asInterface(BundleCompat.getBinder(bundle, "android.media.token.session_binder"));
        switch (int2) {
            default: {
                throw new IllegalArgumentException("Invalid type");
            }
            case 1:
            case 2: {
                if (!TextUtils.isEmpty((CharSequence)string2)) {
                    break;
                }
                throw new IllegalArgumentException("Session service needs service name");
            }
            case 0: {
                if (interface1 != null) {
                    break;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected token for session, binder=");
                sb.append(interface1);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (!TextUtils.isEmpty((CharSequence)string) && string3 != null) {
            return new SessionToken2ImplBase(int1, int2, string, string2, string3, interface1);
        }
        throw new IllegalArgumentException("Package name nor ID cannot be null.");
    }
    
    private boolean sessionBinderEquals(final IMediaSession2 mediaSession2, final IMediaSession2 mediaSession3) {
        if (mediaSession2 != null && mediaSession3 != null) {
            return mediaSession2.asBinder().equals(mediaSession3.asBinder());
        }
        return mediaSession2 == mediaSession3;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof SessionToken2ImplBase;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final SessionToken2ImplBase sessionToken2ImplBase = (SessionToken2ImplBase)o;
        boolean b3 = b2;
        if (this.mUid == sessionToken2ImplBase.mUid) {
            b3 = b2;
            if (TextUtils.equals((CharSequence)this.mPackageName, (CharSequence)sessionToken2ImplBase.mPackageName)) {
                b3 = b2;
                if (TextUtils.equals((CharSequence)this.mServiceName, (CharSequence)sessionToken2ImplBase.mServiceName)) {
                    b3 = b2;
                    if (TextUtils.equals((CharSequence)this.mSessionId, (CharSequence)sessionToken2ImplBase.mSessionId)) {
                        b3 = b2;
                        if (this.mType == sessionToken2ImplBase.mType) {
                            b3 = b2;
                            if (this.sessionBinderEquals(this.mISession2, sessionToken2ImplBase.mISession2)) {
                                b3 = true;
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        final int mType = this.mType;
        final int mUid = this.mUid;
        final int hashCode = this.mPackageName.hashCode();
        final int hashCode2 = this.mSessionId.hashCode();
        int hashCode3;
        if (this.mServiceName != null) {
            hashCode3 = this.mServiceName.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        return mType + 31 * (mUid + (hashCode + (hashCode2 + hashCode3 * 31) * 31) * 31);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SessionToken {pkg=");
        sb.append(this.mPackageName);
        sb.append(" id=");
        sb.append(this.mSessionId);
        sb.append(" type=");
        sb.append(this.mType);
        sb.append(" service=");
        sb.append(this.mServiceName);
        sb.append(" IMediaSession2=");
        sb.append(this.mISession2);
        sb.append("}");
        return sb.toString();
    }
}
