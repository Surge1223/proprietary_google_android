package android.support.v4.media;

import android.os.Build.VERSION;
import android.util.Log;

public final class MediaSessionManager
{
    static final boolean DEBUG;
    private static final Object sLock;
    
    static {
        DEBUG = Log.isLoggable("MediaSessionManager", 3);
        sLock = new Object();
    }
    
    public static final class RemoteUserInfo
    {
        RemoteUserInfoImpl mImpl;
        
        public RemoteUserInfo(final String s, final int n, final int n2) {
            if (Build.VERSION.SDK_INT >= 28) {
                this.mImpl = new MediaSessionManagerImplApi28.RemoteUserInfo(s, n, n2);
            }
            else {
                this.mImpl = new MediaSessionManagerImplBase.RemoteUserInfo(s, n, n2);
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return this.mImpl.equals(o);
        }
        
        @Override
        public int hashCode() {
            return this.mImpl.hashCode();
        }
    }
    
    interface RemoteUserInfoImpl
    {
    }
}
