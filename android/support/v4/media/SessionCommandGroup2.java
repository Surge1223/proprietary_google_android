package android.support.v4.media;

import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

public final class SessionCommandGroup2
{
    private Set<SessionCommand2> mCommands;
    
    public SessionCommandGroup2() {
        this.mCommands = new HashSet<SessionCommand2>();
    }
    
    public static SessionCommandGroup2 fromBundle(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        final ArrayList parcelableArrayList = bundle.getParcelableArrayList("android.media.mediasession2.commandgroup.commands");
        if (parcelableArrayList == null) {
            return null;
        }
        final SessionCommandGroup2 sessionCommandGroup2 = new SessionCommandGroup2();
        for (int i = 0; i < parcelableArrayList.size(); ++i) {
            final Parcelable parcelable = parcelableArrayList.get(i);
            if (parcelable instanceof Bundle) {
                final SessionCommand2 fromBundle = SessionCommand2.fromBundle((Bundle)parcelable);
                if (fromBundle != null) {
                    sessionCommandGroup2.addCommand(fromBundle);
                }
            }
        }
        return sessionCommandGroup2;
    }
    
    public void addCommand(final SessionCommand2 sessionCommand2) {
        if (sessionCommand2 != null) {
            this.mCommands.add(sessionCommand2);
            return;
        }
        throw new IllegalArgumentException("command shouldn't be null");
    }
}
