package android.support.v4.media;

import java.util.Arrays;
import android.media.AudioAttributes;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.SparseIntArray;

public class AudioAttributesCompat
{
    private static final int[] SDK_USAGES;
    private static final SparseIntArray SUPPRESSIBLE_USAGES;
    private static boolean sForceLegacyBehavior;
    private AudioAttributesCompatApi21.Wrapper mAudioAttributesWrapper;
    int mContentType;
    int mFlags;
    Integer mLegacyStream;
    int mUsage;
    
    static {
        (SUPPRESSIBLE_USAGES = new SparseIntArray()).put(5, 1);
        AudioAttributesCompat.SUPPRESSIBLE_USAGES.put(6, 2);
        AudioAttributesCompat.SUPPRESSIBLE_USAGES.put(7, 2);
        AudioAttributesCompat.SUPPRESSIBLE_USAGES.put(8, 1);
        AudioAttributesCompat.SUPPRESSIBLE_USAGES.put(9, 1);
        AudioAttributesCompat.SUPPRESSIBLE_USAGES.put(10, 1);
        SDK_USAGES = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16 };
    }
    
    private AudioAttributesCompat() {
        this.mUsage = 0;
        this.mContentType = 0;
        this.mFlags = 0;
    }
    
    public static AudioAttributesCompat fromBundle(final Bundle bundle) {
        final Integer n = null;
        final AudioAttributesCompat audioAttributesCompat = null;
        if (bundle == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            final AudioAttributes audioAttributes = (AudioAttributes)bundle.getParcelable("android.support.v4.media.audio_attrs.FRAMEWORKS");
            AudioAttributesCompat wrap;
            if (audioAttributes == null) {
                wrap = audioAttributesCompat;
            }
            else {
                wrap = wrap(audioAttributes);
            }
            return wrap;
        }
        final int int1 = bundle.getInt("android.support.v4.media.audio_attrs.USAGE", 0);
        final int int2 = bundle.getInt("android.support.v4.media.audio_attrs.CONTENT_TYPE", 0);
        final int int3 = bundle.getInt("android.support.v4.media.audio_attrs.FLAGS", 0);
        final AudioAttributesCompat audioAttributesCompat2 = new AudioAttributesCompat();
        audioAttributesCompat2.mUsage = int1;
        audioAttributesCompat2.mContentType = int2;
        audioAttributesCompat2.mFlags = int3;
        Integer value = n;
        if (bundle.containsKey("android.support.v4.media.audio_attrs.LEGACY_STREAM_TYPE")) {
            value = bundle.getInt("android.support.v4.media.audio_attrs.LEGACY_STREAM_TYPE");
        }
        audioAttributesCompat2.mLegacyStream = value;
        return audioAttributesCompat2;
    }
    
    static int toVolumeStreamType(final boolean b, int n, final int n2) {
        final int n3 = 1;
        if ((n & 0x1) == 0x1) {
            if (b) {
                n = n3;
            }
            else {
                n = 7;
            }
            return n;
        }
        final int n4 = 0;
        final int n5 = 0;
        if ((n & 0x4) == 0x4) {
            if (b) {
                n = n5;
            }
            else {
                n = 6;
            }
            return n;
        }
        n = 3;
        switch (n2) {
            default: {
                if (!b) {
                    return 3;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown usage value ");
                sb.append(n2);
                sb.append(" in audio attributes");
                throw new IllegalArgumentException(sb.toString());
            }
            case 13: {
                return 1;
            }
            case 11: {
                return 10;
            }
            case 6: {
                return 2;
            }
            case 5:
            case 7:
            case 8:
            case 9:
            case 10: {
                return 5;
            }
            case 4: {
                return 4;
            }
            case 3: {
                if (b) {
                    n = n4;
                }
                else {
                    n = 8;
                }
                return n;
            }
            case 2: {
                return 0;
            }
            case 1:
            case 12:
            case 14:
            case 16: {
                return 3;
            }
            case 0: {
                if (b) {
                    n = Integer.MIN_VALUE;
                }
                return n;
            }
        }
    }
    
    static String usageToString(final int n) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown usage ");
                sb.append(n);
                return new String(sb.toString());
            }
            case 16: {
                return new String("USAGE_ASSISTANT");
            }
            case 14: {
                return new String("USAGE_GAME");
            }
            case 13: {
                return new String("USAGE_ASSISTANCE_SONIFICATION");
            }
            case 12: {
                return new String("USAGE_ASSISTANCE_NAVIGATION_GUIDANCE");
            }
            case 11: {
                return new String("USAGE_ASSISTANCE_ACCESSIBILITY");
            }
            case 10: {
                return new String("USAGE_NOTIFICATION_EVENT");
            }
            case 9: {
                return new String("USAGE_NOTIFICATION_COMMUNICATION_DELAYED");
            }
            case 8: {
                return new String("USAGE_NOTIFICATION_COMMUNICATION_INSTANT");
            }
            case 7: {
                return new String("USAGE_NOTIFICATION_COMMUNICATION_REQUEST");
            }
            case 6: {
                return new String("USAGE_NOTIFICATION_RINGTONE");
            }
            case 5: {
                return new String("USAGE_NOTIFICATION");
            }
            case 4: {
                return new String("USAGE_ALARM");
            }
            case 3: {
                return new String("USAGE_VOICE_COMMUNICATION_SIGNALLING");
            }
            case 2: {
                return new String("USAGE_VOICE_COMMUNICATION");
            }
            case 1: {
                return new String("USAGE_MEDIA");
            }
            case 0: {
                return new String("USAGE_UNKNOWN");
            }
        }
    }
    
    public static AudioAttributesCompat wrap(final Object o) {
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior) {
            final AudioAttributesCompat audioAttributesCompat = new AudioAttributesCompat();
            audioAttributesCompat.mAudioAttributesWrapper = AudioAttributesCompatApi21.Wrapper.wrap((AudioAttributes)o);
            return audioAttributesCompat;
        }
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final AudioAttributesCompat audioAttributesCompat = (AudioAttributesCompat)o;
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior && this.mAudioAttributesWrapper != null) {
            return this.mAudioAttributesWrapper.unwrap().equals(audioAttributesCompat.unwrap());
        }
        if (this.mContentType == audioAttributesCompat.getContentType() && this.mFlags == audioAttributesCompat.getFlags() && this.mUsage == audioAttributesCompat.getUsage()) {
            if (this.mLegacyStream != null) {
                if (!this.mLegacyStream.equals(audioAttributesCompat.mLegacyStream)) {
                    return false;
                }
            }
            else if (audioAttributesCompat.mLegacyStream != null) {
                return false;
            }
            return b;
        }
        b = false;
        return b;
    }
    
    public int getContentType() {
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior && this.mAudioAttributesWrapper != null) {
            return this.mAudioAttributesWrapper.unwrap().getContentType();
        }
        return this.mContentType;
    }
    
    public int getFlags() {
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior && this.mAudioAttributesWrapper != null) {
            return this.mAudioAttributesWrapper.unwrap().getFlags();
        }
        final int mFlags = this.mFlags;
        final int legacyStreamType = this.getLegacyStreamType();
        int n;
        if (legacyStreamType == 6) {
            n = (mFlags | 0x4);
        }
        else {
            n = mFlags;
            if (legacyStreamType == 7) {
                n = (mFlags | 0x1);
            }
        }
        return n & 0x111;
    }
    
    public int getLegacyStreamType() {
        if (this.mLegacyStream != null) {
            return this.mLegacyStream;
        }
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior) {
            return AudioAttributesCompatApi21.toLegacyStreamType(this.mAudioAttributesWrapper);
        }
        return toVolumeStreamType(false, this.mFlags, this.mUsage);
    }
    
    public int getUsage() {
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior && this.mAudioAttributesWrapper != null) {
            return this.mAudioAttributesWrapper.unwrap().getUsage();
        }
        return this.mUsage;
    }
    
    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= 21 && !AudioAttributesCompat.sForceLegacyBehavior && this.mAudioAttributesWrapper != null) {
            return this.mAudioAttributesWrapper.unwrap().hashCode();
        }
        return Arrays.hashCode(new Object[] { this.mContentType, this.mFlags, this.mUsage, this.mLegacyStream });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AudioAttributesCompat:");
        if (this.unwrap() != null) {
            sb.append(" audioattributes=");
            sb.append(this.unwrap());
        }
        else {
            if (this.mLegacyStream != null) {
                sb.append(" stream=");
                sb.append(this.mLegacyStream);
                sb.append(" derived");
            }
            sb.append(" usage=");
            sb.append(this.usageToString());
            sb.append(" content=");
            sb.append(this.mContentType);
            sb.append(" flags=0x");
            sb.append(Integer.toHexString(this.mFlags).toUpperCase());
        }
        return sb.toString();
    }
    
    public Object unwrap() {
        if (this.mAudioAttributesWrapper != null) {
            return this.mAudioAttributesWrapper.unwrap();
        }
        return null;
    }
    
    String usageToString() {
        return usageToString(this.mUsage);
    }
}
