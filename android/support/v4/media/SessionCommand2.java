package android.support.v4.media;

import android.text.TextUtils;
import android.os.Bundle;

public final class SessionCommand2
{
    private final int mCommandCode;
    private final String mCustomCommand;
    private final Bundle mExtras;
    
    public SessionCommand2(final int mCommandCode) {
        if (mCommandCode != 0) {
            this.mCommandCode = mCommandCode;
            this.mCustomCommand = null;
            this.mExtras = null;
            return;
        }
        throw new IllegalArgumentException("commandCode shouldn't be COMMAND_CODE_CUSTOM");
    }
    
    public SessionCommand2(final String mCustomCommand, final Bundle mExtras) {
        if (mCustomCommand != null) {
            this.mCommandCode = 0;
            this.mCustomCommand = mCustomCommand;
            this.mExtras = mExtras;
            return;
        }
        throw new IllegalArgumentException("action shouldn't be null");
    }
    
    public static SessionCommand2 fromBundle(final Bundle bundle) {
        if (bundle == null) {
            throw new IllegalArgumentException("command shouldn't be null");
        }
        final int int1 = bundle.getInt("android.media.media_session2.command.command_code");
        if (int1 != 0) {
            return new SessionCommand2(int1);
        }
        final String string = bundle.getString("android.media.media_session2.command.custom_command");
        if (string == null) {
            return null;
        }
        return new SessionCommand2(string, bundle.getBundle("android.media.media_session2.command.extras"));
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof SessionCommand2;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final SessionCommand2 sessionCommand2 = (SessionCommand2)o;
        boolean b3 = b2;
        if (this.mCommandCode == sessionCommand2.mCommandCode) {
            b3 = b2;
            if (TextUtils.equals((CharSequence)this.mCustomCommand, (CharSequence)sessionCommand2.mCustomCommand)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if (this.mCustomCommand != null) {
            hashCode = this.mCustomCommand.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode * 31 + this.mCommandCode;
    }
}
