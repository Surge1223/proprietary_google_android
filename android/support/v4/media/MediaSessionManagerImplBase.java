package android.support.v4.media;

import android.support.v4.util.ObjectsCompat;
import android.text.TextUtils;

class MediaSessionManagerImplBase
{
    private static final boolean DEBUG;
    
    static {
        DEBUG = MediaSessionManager.DEBUG;
    }
    
    static class RemoteUserInfo implements RemoteUserInfoImpl
    {
        private String mPackageName;
        private int mPid;
        private int mUid;
        
        RemoteUserInfo(final String mPackageName, final int mPid, final int mUid) {
            this.mPackageName = mPackageName;
            this.mPid = mPid;
            this.mUid = mUid;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof RemoteUserInfo;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final RemoteUserInfo remoteUserInfo = (RemoteUserInfo)o;
            boolean b3 = b2;
            if (TextUtils.equals((CharSequence)this.mPackageName, (CharSequence)remoteUserInfo.mPackageName)) {
                b3 = b2;
                if (this.mPid == remoteUserInfo.mPid) {
                    b3 = b2;
                    if (this.mUid == remoteUserInfo.mUid) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.mPackageName, this.mPid, this.mUid);
        }
    }
}
