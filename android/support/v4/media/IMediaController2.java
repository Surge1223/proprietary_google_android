package android.support.v4.media;

import java.util.ArrayList;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.ResultReceiver;
import android.app.PendingIntent;
import java.util.List;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface IMediaController2 extends IInterface
{
    void onAllowedCommandsChanged(final Bundle p0) throws RemoteException;
    
    void onBufferingStateChanged(final Bundle p0, final int p1, final long p2) throws RemoteException;
    
    void onChildrenChanged(final String p0, final int p1, final Bundle p2) throws RemoteException;
    
    void onConnected(final IMediaSession2 p0, final Bundle p1, final int p2, final Bundle p3, final long p4, final long p5, final float p6, final long p7, final Bundle p8, final int p9, final int p10, final List<Bundle> p11, final PendingIntent p12) throws RemoteException;
    
    void onCurrentMediaItemChanged(final Bundle p0) throws RemoteException;
    
    void onCustomCommand(final Bundle p0, final Bundle p1, final ResultReceiver p2) throws RemoteException;
    
    void onCustomLayoutChanged(final List<Bundle> p0) throws RemoteException;
    
    void onDisconnected() throws RemoteException;
    
    void onError(final int p0, final Bundle p1) throws RemoteException;
    
    void onGetChildrenDone(final String p0, final int p1, final int p2, final List<Bundle> p3, final Bundle p4) throws RemoteException;
    
    void onGetItemDone(final String p0, final Bundle p1) throws RemoteException;
    
    void onGetLibraryRootDone(final Bundle p0, final String p1, final Bundle p2) throws RemoteException;
    
    void onGetSearchResultDone(final String p0, final int p1, final int p2, final List<Bundle> p3, final Bundle p4) throws RemoteException;
    
    void onPlaybackInfoChanged(final Bundle p0) throws RemoteException;
    
    void onPlaybackSpeedChanged(final long p0, final long p1, final float p2) throws RemoteException;
    
    void onPlayerStateChanged(final long p0, final long p1, final int p2) throws RemoteException;
    
    void onPlaylistChanged(final List<Bundle> p0, final Bundle p1) throws RemoteException;
    
    void onPlaylistMetadataChanged(final Bundle p0) throws RemoteException;
    
    void onRepeatModeChanged(final int p0) throws RemoteException;
    
    void onRoutesInfoChanged(final List<Bundle> p0) throws RemoteException;
    
    void onSearchResultChanged(final String p0, final int p1, final Bundle p2) throws RemoteException;
    
    void onSeekCompleted(final long p0, final long p1, final long p2) throws RemoteException;
    
    void onShuffleModeChanged(final int p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IMediaController2
    {
        public static IMediaController2 asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.v4.media.IMediaController2");
            if (queryLocalInterface != null && queryLocalInterface instanceof IMediaController2) {
                return (IMediaController2)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, int n2) throws RemoteException {
            if (n == 1598968902) {
                parcel2.writeString("android.support.v4.media.IMediaController2");
                return true;
            }
            final Bundle bundle = null;
            final Bundle bundle2 = null;
            final Bundle bundle3 = null;
            final Bundle bundle4 = null;
            final Bundle bundle5 = null;
            final ResultReceiver resultReceiver = null;
            final Bundle bundle6 = null;
            final PendingIntent pendingIntent = null;
            final Bundle bundle7 = null;
            final Bundle bundle8 = null;
            final Bundle bundle9 = null;
            final Bundle bundle10 = null;
            final Bundle bundle11 = null;
            final Bundle bundle12 = null;
            final Bundle bundle13 = null;
            switch (n) {
                default: {
                    return super.onTransact(n, parcel, parcel2, n2);
                }
                case 23: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final String string = parcel.readString();
                    n = parcel.readInt();
                    n2 = parcel.readInt();
                    final ArrayList typedArrayList = parcel.createTypedArrayList(Bundle.CREATOR);
                    Bundle bundle14;
                    if (parcel.readInt() != 0) {
                        bundle14 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle14 = bundle13;
                    }
                    this.onGetSearchResultDone(string, n, n2, typedArrayList, bundle14);
                    return true;
                }
                case 22: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final String string2 = parcel.readString();
                    n = parcel.readInt();
                    Bundle bundle15;
                    if (parcel.readInt() != 0) {
                        bundle15 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle15 = bundle;
                    }
                    this.onSearchResultChanged(string2, n, bundle15);
                    return true;
                }
                case 21: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final String string3 = parcel.readString();
                    n = parcel.readInt();
                    n2 = parcel.readInt();
                    final ArrayList typedArrayList2 = parcel.createTypedArrayList(Bundle.CREATOR);
                    Bundle bundle16;
                    if (parcel.readInt() != 0) {
                        bundle16 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle16 = bundle2;
                    }
                    this.onGetChildrenDone(string3, n, n2, typedArrayList2, bundle16);
                    return true;
                }
                case 20: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final String string4 = parcel.readString();
                    n = parcel.readInt();
                    Bundle bundle17;
                    if (parcel.readInt() != 0) {
                        bundle17 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle17 = bundle3;
                    }
                    this.onChildrenChanged(string4, n, bundle17);
                    return true;
                }
                case 19: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final String string5 = parcel.readString();
                    Bundle bundle18;
                    if (parcel.readInt() != 0) {
                        bundle18 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle18 = bundle4;
                    }
                    this.onGetItemDone(string5, bundle18);
                    return true;
                }
                case 18: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle19;
                    if (parcel.readInt() != 0) {
                        bundle19 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle19 = null;
                    }
                    final String string6 = parcel.readString();
                    Bundle bundle20;
                    if (parcel.readInt() != 0) {
                        bundle20 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle20 = bundle5;
                    }
                    this.onGetLibraryRootDone(bundle19, string6, bundle20);
                    return true;
                }
                case 17: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle21;
                    if (parcel.readInt() != 0) {
                        bundle21 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle21 = null;
                    }
                    Bundle bundle22;
                    if (parcel.readInt() != 0) {
                        bundle22 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle22 = null;
                    }
                    ResultReceiver resultReceiver2;
                    if (parcel.readInt() != 0) {
                        resultReceiver2 = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        resultReceiver2 = resultReceiver;
                    }
                    this.onCustomCommand(bundle21, bundle22, resultReceiver2);
                    return true;
                }
                case 16: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle23;
                    if (parcel.readInt() != 0) {
                        bundle23 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle23 = bundle6;
                    }
                    this.onAllowedCommandsChanged(bundle23);
                    return true;
                }
                case 15: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onCustomLayoutChanged(parcel.createTypedArrayList(Bundle.CREATOR));
                    return true;
                }
                case 14: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onDisconnected();
                    return true;
                }
                case 13: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final IMediaSession2 interface1 = IMediaSession2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle24;
                    if (parcel.readInt() != 0) {
                        bundle24 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle24 = null;
                    }
                    n = parcel.readInt();
                    Bundle bundle25;
                    if (parcel.readInt() != 0) {
                        bundle25 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle25 = null;
                    }
                    final long long1 = parcel.readLong();
                    final long long2 = parcel.readLong();
                    final float float1 = parcel.readFloat();
                    final long long3 = parcel.readLong();
                    Bundle bundle26;
                    if (parcel.readInt() != 0) {
                        bundle26 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle26 = null;
                    }
                    n2 = parcel.readInt();
                    final int int1 = parcel.readInt();
                    final ArrayList typedArrayList3 = parcel.createTypedArrayList(Bundle.CREATOR);
                    PendingIntent pendingIntent2;
                    if (parcel.readInt() != 0) {
                        pendingIntent2 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        pendingIntent2 = pendingIntent;
                    }
                    this.onConnected(interface1, bundle24, n, bundle25, long1, long2, float1, long3, bundle26, n2, int1, typedArrayList3, pendingIntent2);
                    return true;
                }
                case 12: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onRoutesInfoChanged(parcel.createTypedArrayList(Bundle.CREATOR));
                    return true;
                }
                case 11: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    n = parcel.readInt();
                    Bundle bundle27;
                    if (parcel.readInt() != 0) {
                        bundle27 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle27 = bundle7;
                    }
                    this.onError(n, bundle27);
                    return true;
                }
                case 10: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onSeekCompleted(parcel.readLong(), parcel.readLong(), parcel.readLong());
                    return true;
                }
                case 9: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onShuffleModeChanged(parcel.readInt());
                    return true;
                }
                case 8: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onRepeatModeChanged(parcel.readInt());
                    return true;
                }
                case 7: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle28;
                    if (parcel.readInt() != 0) {
                        bundle28 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle28 = bundle8;
                    }
                    this.onPlaybackInfoChanged(bundle28);
                    return true;
                }
                case 6: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle29;
                    if (parcel.readInt() != 0) {
                        bundle29 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle29 = bundle9;
                    }
                    this.onPlaylistMetadataChanged(bundle29);
                    return true;
                }
                case 5: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    final ArrayList typedArrayList4 = parcel.createTypedArrayList(Bundle.CREATOR);
                    Bundle bundle30;
                    if (parcel.readInt() != 0) {
                        bundle30 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle30 = bundle10;
                    }
                    this.onPlaylistChanged(typedArrayList4, bundle30);
                    return true;
                }
                case 4: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle31;
                    if (parcel.readInt() != 0) {
                        bundle31 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle31 = bundle11;
                    }
                    this.onBufferingStateChanged(bundle31, parcel.readInt(), parcel.readLong());
                    return true;
                }
                case 3: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onPlaybackSpeedChanged(parcel.readLong(), parcel.readLong(), parcel.readFloat());
                    return true;
                }
                case 2: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    this.onPlayerStateChanged(parcel.readLong(), parcel.readLong(), parcel.readInt());
                    return true;
                }
                case 1: {
                    parcel.enforceInterface("android.support.v4.media.IMediaController2");
                    Bundle bundle32;
                    if (parcel.readInt() != 0) {
                        bundle32 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle32 = bundle12;
                    }
                    this.onCurrentMediaItemChanged(bundle32);
                    return true;
                }
            }
        }
        
        private static class Proxy implements IMediaController2
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void onAllowedCommandsChanged(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(16, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onBufferingStateChanged(final Bundle bundle, final int n, final long n2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(n);
                    obtain.writeLong(n2);
                    this.mRemote.transact(4, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onChildrenChanged(final String s, final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(20, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onConnected(final IMediaSession2 mediaSession2, final Bundle bundle, final int n, final Bundle bundle2, final long n2, final long n3, final float n4, final long n5, final Bundle bundle3, final int n6, final int n7, final List<Bundle> list, final PendingIntent pendingIntent) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    IBinder binder;
                    if (mediaSession2 != null) {
                        binder = mediaSession2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(n);
                    if (bundle2 != null) {
                        obtain.writeInt(1);
                        bundle2.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    try {
                        obtain.writeLong(n2);
                        try {
                            obtain.writeLong(n3);
                            try {
                                obtain.writeFloat(n4);
                                try {
                                    obtain.writeLong(n5);
                                    if (bundle3 != null) {
                                        obtain.writeInt(1);
                                        bundle3.writeToParcel(obtain, 0);
                                    }
                                    else {
                                        obtain.writeInt(0);
                                    }
                                    try {
                                        obtain.writeInt(n6);
                                        try {
                                            obtain.writeInt(n7);
                                            obtain.writeTypedList((List)list);
                                            if (pendingIntent != null) {
                                                obtain.writeInt(1);
                                                pendingIntent.writeToParcel(obtain, 0);
                                            }
                                            else {
                                                obtain.writeInt(0);
                                            }
                                            this.mRemote.transact(13, obtain, (Parcel)null, 1);
                                            obtain.recycle();
                                            return;
                                        }
                                        finally {}
                                    }
                                    finally {}
                                }
                                finally {}
                            }
                            finally {}
                        }
                        finally {}
                    }
                    finally {}
                }
                finally {}
                obtain.recycle();
            }
            
            @Override
            public void onCurrentMediaItemChanged(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onCustomCommand(final Bundle bundle, final Bundle bundle2, final ResultReceiver resultReceiver) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle2 != null) {
                        obtain.writeInt(1);
                        bundle2.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (resultReceiver != null) {
                        obtain.writeInt(1);
                        resultReceiver.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(17, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onCustomLayoutChanged(final List<Bundle> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(15, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onDisconnected() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    this.mRemote.transact(14, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onError(final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeInt(n);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(11, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onGetChildrenDone(final String s, final int n, final int n2, final List<Bundle> list, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    obtain.writeTypedList((List)list);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(21, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onGetItemDone(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(19, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onGetLibraryRootDone(final Bundle bundle, final String s, final Bundle bundle2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(s);
                    if (bundle2 != null) {
                        obtain.writeInt(1);
                        bundle2.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(18, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onGetSearchResultDone(final String s, final int n, final int n2, final List<Bundle> list, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    obtain.writeTypedList((List)list);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(23, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPlaybackInfoChanged(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(7, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPlaybackSpeedChanged(final long n, final long n2, final float n3) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeLong(n);
                    obtain.writeLong(n2);
                    obtain.writeFloat(n3);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPlayerStateChanged(final long n, final long n2, final int n3) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeLong(n);
                    obtain.writeLong(n2);
                    obtain.writeInt(n3);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPlaylistChanged(final List<Bundle> list, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeTypedList((List)list);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(5, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPlaylistMetadataChanged(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(6, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onRepeatModeChanged(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeInt(n);
                    this.mRemote.transact(8, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onRoutesInfoChanged(final List<Bundle> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(12, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onSearchResultChanged(final String s, final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(22, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onSeekCompleted(final long n, final long n2, final long n3) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeLong(n);
                    obtain.writeLong(n2);
                    obtain.writeLong(n3);
                    this.mRemote.transact(10, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onShuffleModeChanged(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaController2");
                    obtain.writeInt(n);
                    this.mRemote.transact(9, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}
