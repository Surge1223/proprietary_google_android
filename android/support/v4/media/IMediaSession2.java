package android.support.v4.media;

import java.util.ArrayList;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import java.util.List;
import android.os.ResultReceiver;
import android.net.Uri;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface IMediaSession2 extends IInterface
{
    void addPlaylistItem(final IMediaController2 p0, final int p1, final Bundle p2) throws RemoteException;
    
    void adjustVolume(final IMediaController2 p0, final int p1, final int p2) throws RemoteException;
    
    void connect(final IMediaController2 p0, final String p1) throws RemoteException;
    
    void fastForward(final IMediaController2 p0) throws RemoteException;
    
    void getChildren(final IMediaController2 p0, final String p1, final int p2, final int p3, final Bundle p4) throws RemoteException;
    
    void getItem(final IMediaController2 p0, final String p1) throws RemoteException;
    
    void getLibraryRoot(final IMediaController2 p0, final Bundle p1) throws RemoteException;
    
    void getSearchResult(final IMediaController2 p0, final String p1, final int p2, final int p3, final Bundle p4) throws RemoteException;
    
    void pause(final IMediaController2 p0) throws RemoteException;
    
    void play(final IMediaController2 p0) throws RemoteException;
    
    void playFromMediaId(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void playFromSearch(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void playFromUri(final IMediaController2 p0, final Uri p1, final Bundle p2) throws RemoteException;
    
    void prepare(final IMediaController2 p0) throws RemoteException;
    
    void prepareFromMediaId(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void prepareFromSearch(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void prepareFromUri(final IMediaController2 p0, final Uri p1, final Bundle p2) throws RemoteException;
    
    void release(final IMediaController2 p0) throws RemoteException;
    
    void removePlaylistItem(final IMediaController2 p0, final Bundle p1) throws RemoteException;
    
    void replacePlaylistItem(final IMediaController2 p0, final int p1, final Bundle p2) throws RemoteException;
    
    void reset(final IMediaController2 p0) throws RemoteException;
    
    void rewind(final IMediaController2 p0) throws RemoteException;
    
    void search(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void seekTo(final IMediaController2 p0, final long p1) throws RemoteException;
    
    void selectRoute(final IMediaController2 p0, final Bundle p1) throws RemoteException;
    
    void sendCustomCommand(final IMediaController2 p0, final Bundle p1, final Bundle p2, final ResultReceiver p3) throws RemoteException;
    
    void setPlaybackSpeed(final IMediaController2 p0, final float p1) throws RemoteException;
    
    void setPlaylist(final IMediaController2 p0, final List<Bundle> p1, final Bundle p2) throws RemoteException;
    
    void setRating(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void setRepeatMode(final IMediaController2 p0, final int p1) throws RemoteException;
    
    void setShuffleMode(final IMediaController2 p0, final int p1) throws RemoteException;
    
    void setVolumeTo(final IMediaController2 p0, final int p1, final int p2) throws RemoteException;
    
    void skipToNextItem(final IMediaController2 p0) throws RemoteException;
    
    void skipToPlaylistItem(final IMediaController2 p0, final Bundle p1) throws RemoteException;
    
    void skipToPreviousItem(final IMediaController2 p0) throws RemoteException;
    
    void subscribe(final IMediaController2 p0, final String p1, final Bundle p2) throws RemoteException;
    
    void subscribeRoutesInfo(final IMediaController2 p0) throws RemoteException;
    
    void unsubscribe(final IMediaController2 p0, final String p1) throws RemoteException;
    
    void unsubscribeRoutesInfo(final IMediaController2 p0) throws RemoteException;
    
    void updatePlaylistMetadata(final IMediaController2 p0, final Bundle p1) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IMediaSession2
    {
        public static IMediaSession2 asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.v4.media.IMediaSession2");
            if (queryLocalInterface != null && queryLocalInterface instanceof IMediaSession2) {
                return (IMediaSession2)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, int n2) throws RemoteException {
            if (n == 1598968902) {
                parcel2.writeString("android.support.v4.media.IMediaSession2");
                return true;
            }
            final Bundle bundle = null;
            final Bundle bundle2 = null;
            final Bundle bundle3 = null;
            final Bundle bundle4 = null;
            final Bundle bundle5 = null;
            final Bundle bundle6 = null;
            final Bundle bundle7 = null;
            final Bundle bundle8 = null;
            final Bundle bundle9 = null;
            final Bundle bundle10 = null;
            final Bundle bundle11 = null;
            final Bundle bundle12 = null;
            final Bundle bundle13 = null;
            final Bundle bundle14 = null;
            final Bundle bundle15 = null;
            final Bundle bundle16 = null;
            final Bundle bundle17 = null;
            final Bundle bundle18 = null;
            final ResultReceiver resultReceiver = null;
            final Bundle bundle19 = null;
            switch (n) {
                default: {
                    return super.onTransact(n, parcel, parcel2, n2);
                }
                case 40: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.unsubscribe(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readString());
                    return true;
                }
                case 39: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface1 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string = parcel.readString();
                    Bundle bundle20;
                    if (parcel.readInt() != 0) {
                        bundle20 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle20 = bundle19;
                    }
                    this.subscribe(interface1, string, bundle20);
                    return true;
                }
                case 38: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface2 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string2 = parcel.readString();
                    n2 = parcel.readInt();
                    n = parcel.readInt();
                    Bundle bundle21;
                    if (parcel.readInt() != 0) {
                        bundle21 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle21 = bundle;
                    }
                    this.getSearchResult(interface2, string2, n2, n, bundle21);
                    return true;
                }
                case 37: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface3 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string3 = parcel.readString();
                    Bundle bundle22;
                    if (parcel.readInt() != 0) {
                        bundle22 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle22 = bundle2;
                    }
                    this.search(interface3, string3, bundle22);
                    return true;
                }
                case 36: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface4 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string4 = parcel.readString();
                    n2 = parcel.readInt();
                    n = parcel.readInt();
                    Bundle bundle23;
                    if (parcel.readInt() != 0) {
                        bundle23 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle23 = bundle3;
                    }
                    this.getChildren(interface4, string4, n2, n, bundle23);
                    return true;
                }
                case 35: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.getItem(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readString());
                    return true;
                }
                case 34: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface5 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle24;
                    if (parcel.readInt() != 0) {
                        bundle24 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle24 = bundle4;
                    }
                    this.getLibraryRoot(interface5, bundle24);
                    return true;
                }
                case 33: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface6 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle25;
                    if (parcel.readInt() != 0) {
                        bundle25 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle25 = bundle5;
                    }
                    this.selectRoute(interface6, bundle25);
                    return true;
                }
                case 32: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.unsubscribeRoutesInfo(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 31: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.subscribeRoutesInfo(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 30: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.setShuffleMode(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readInt());
                    return true;
                }
                case 29: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.setRepeatMode(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readInt());
                    return true;
                }
                case 28: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.skipToNextItem(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 27: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.skipToPreviousItem(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 26: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface7 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle26;
                    if (parcel.readInt() != 0) {
                        bundle26 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle26 = bundle6;
                    }
                    this.skipToPlaylistItem(interface7, bundle26);
                    return true;
                }
                case 25: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface8 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    n = parcel.readInt();
                    Bundle bundle27;
                    if (parcel.readInt() != 0) {
                        bundle27 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle27 = bundle7;
                    }
                    this.replacePlaylistItem(interface8, n, bundle27);
                    return true;
                }
                case 24: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface9 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle28;
                    if (parcel.readInt() != 0) {
                        bundle28 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle28 = bundle8;
                    }
                    this.removePlaylistItem(interface9, bundle28);
                    return true;
                }
                case 23: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface10 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    n = parcel.readInt();
                    Bundle bundle29;
                    if (parcel.readInt() != 0) {
                        bundle29 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle29 = bundle9;
                    }
                    this.addPlaylistItem(interface10, n, bundle29);
                    return true;
                }
                case 22: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface11 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle30;
                    if (parcel.readInt() != 0) {
                        bundle30 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle30 = bundle10;
                    }
                    this.updatePlaylistMetadata(interface11, bundle30);
                    return true;
                }
                case 21: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface12 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final ArrayList typedArrayList = parcel.createTypedArrayList(Bundle.CREATOR);
                    Bundle bundle31;
                    if (parcel.readInt() != 0) {
                        bundle31 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle31 = bundle11;
                    }
                    this.setPlaylist(interface12, typedArrayList, bundle31);
                    return true;
                }
                case 20: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.setPlaybackSpeed(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readFloat());
                    return true;
                }
                case 19: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface13 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string5 = parcel.readString();
                    Bundle bundle32;
                    if (parcel.readInt() != 0) {
                        bundle32 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle32 = bundle12;
                    }
                    this.setRating(interface13, string5, bundle32);
                    return true;
                }
                case 18: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface14 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string6 = parcel.readString();
                    Bundle bundle33;
                    if (parcel.readInt() != 0) {
                        bundle33 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle33 = bundle13;
                    }
                    this.playFromMediaId(interface14, string6, bundle33);
                    return true;
                }
                case 17: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface15 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string7 = parcel.readString();
                    Bundle bundle34;
                    if (parcel.readInt() != 0) {
                        bundle34 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle34 = bundle14;
                    }
                    this.playFromSearch(interface15, string7, bundle34);
                    return true;
                }
                case 16: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface16 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Uri uri;
                    if (parcel.readInt() != 0) {
                        uri = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        uri = null;
                    }
                    Bundle bundle35;
                    if (parcel.readInt() != 0) {
                        bundle35 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle35 = bundle15;
                    }
                    this.playFromUri(interface16, uri, bundle35);
                    return true;
                }
                case 15: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface17 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string8 = parcel.readString();
                    Bundle bundle36;
                    if (parcel.readInt() != 0) {
                        bundle36 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle36 = bundle16;
                    }
                    this.prepareFromMediaId(interface17, string8, bundle36);
                    return true;
                }
                case 14: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface18 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    final String string9 = parcel.readString();
                    Bundle bundle37;
                    if (parcel.readInt() != 0) {
                        bundle37 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle37 = bundle17;
                    }
                    this.prepareFromSearch(interface18, string9, bundle37);
                    return true;
                }
                case 13: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface19 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Uri uri2;
                    if (parcel.readInt() != 0) {
                        uri2 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        uri2 = null;
                    }
                    Bundle bundle38;
                    if (parcel.readInt() != 0) {
                        bundle38 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle38 = bundle18;
                    }
                    this.prepareFromUri(interface19, uri2, bundle38);
                    return true;
                }
                case 12: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    final IMediaController2 interface20 = IMediaController2.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle39;
                    if (parcel.readInt() != 0) {
                        bundle39 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle39 = null;
                    }
                    Bundle bundle40;
                    if (parcel.readInt() != 0) {
                        bundle40 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        bundle40 = null;
                    }
                    ResultReceiver resultReceiver2;
                    if (parcel.readInt() != 0) {
                        resultReceiver2 = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        resultReceiver2 = resultReceiver;
                    }
                    this.sendCustomCommand(interface20, bundle39, bundle40, resultReceiver2);
                    return true;
                }
                case 11: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.seekTo(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readLong());
                    return true;
                }
                case 10: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.rewind(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 9: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.fastForward(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 8: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.prepare(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 7: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.reset(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 6: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.pause(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 5: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.play(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 4: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.adjustVolume(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readInt(), parcel.readInt());
                    return true;
                }
                case 3: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.setVolumeTo(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readInt(), parcel.readInt());
                    return true;
                }
                case 2: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.release(IMediaController2.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 1: {
                    parcel.enforceInterface("android.support.v4.media.IMediaSession2");
                    this.connect(IMediaController2.Stub.asInterface(parcel.readStrongBinder()), parcel.readString());
                    return true;
                }
            }
        }
        
        private static class Proxy implements IMediaSession2
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public void addPlaylistItem(final IMediaController2 mediaController2, final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(n);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(23, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void adjustVolume(final IMediaController2 mediaController2, final int n, final int n2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    this.mRemote.transact(4, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void connect(final IMediaController2 mediaController2, final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void fastForward(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(9, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void getChildren(final IMediaController2 mediaController2, final String s, final int n, final int n2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(36, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void getItem(final IMediaController2 mediaController2, final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    this.mRemote.transact(35, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void getLibraryRoot(final IMediaController2 mediaController2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(34, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void getSearchResult(final IMediaController2 mediaController2, final String s, final int n, final int n2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(38, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void pause(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(6, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void play(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(5, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void playFromMediaId(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(18, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void playFromSearch(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(17, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void playFromUri(final IMediaController2 mediaController2, final Uri uri, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(16, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void prepare(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(8, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void prepareFromMediaId(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(15, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void prepareFromSearch(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(14, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void prepareFromUri(final IMediaController2 mediaController2, final Uri uri, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(13, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void release(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void removePlaylistItem(final IMediaController2 mediaController2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(24, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void replacePlaylistItem(final IMediaController2 mediaController2, final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(n);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(25, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void reset(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(7, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void rewind(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(10, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void search(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(37, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void seekTo(final IMediaController2 mediaController2, final long n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeLong(n);
                    this.mRemote.transact(11, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void selectRoute(final IMediaController2 mediaController2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(33, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void sendCustomCommand(final IMediaController2 mediaController2, final Bundle bundle, final Bundle bundle2, final ResultReceiver resultReceiver) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle2 != null) {
                        obtain.writeInt(1);
                        bundle2.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (resultReceiver != null) {
                        obtain.writeInt(1);
                        resultReceiver.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(12, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setPlaybackSpeed(final IMediaController2 mediaController2, final float n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeFloat(n);
                    this.mRemote.transact(20, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setPlaylist(final IMediaController2 mediaController2, final List<Bundle> list, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeTypedList((List)list);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(21, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setRating(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(19, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setRepeatMode(final IMediaController2 mediaController2, final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(n);
                    this.mRemote.transact(29, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setShuffleMode(final IMediaController2 mediaController2, final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(n);
                    this.mRemote.transact(30, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setVolumeTo(final IMediaController2 mediaController2, final int n, final int n2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void skipToNextItem(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(28, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void skipToPlaylistItem(final IMediaController2 mediaController2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(26, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void skipToPreviousItem(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(27, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void subscribe(final IMediaController2 mediaController2, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(39, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void subscribeRoutesInfo(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(31, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void unsubscribe(final IMediaController2 mediaController2, final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    this.mRemote.transact(40, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void unsubscribeRoutesInfo(final IMediaController2 mediaController2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(32, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void updatePlaylistMetadata(final IMediaController2 mediaController2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.IMediaSession2");
                    IBinder binder;
                    if (mediaController2 != null) {
                        binder = mediaController2.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(22, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}
