package android.support.v4.media;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.UUID;

public class MediaItem2
{
    private DataSourceDesc mDataSourceDesc;
    private final int mFlags;
    private final String mId;
    private MediaMetadata2 mMetadata;
    private final UUID mUUID;
    
    private MediaItem2(final String mId, final DataSourceDesc mDataSourceDesc, final MediaMetadata2 mMetadata, final int mFlags, UUID randomUUID) {
        if (mId == null) {
            throw new IllegalArgumentException("mediaId shouldn't be null");
        }
        if (mMetadata != null && !TextUtils.equals((CharSequence)mId, (CharSequence)mMetadata.getMediaId())) {
            throw new IllegalArgumentException("metadata's id should be matched with the mediaid");
        }
        this.mId = mId;
        this.mDataSourceDesc = mDataSourceDesc;
        this.mMetadata = mMetadata;
        this.mFlags = mFlags;
        if (randomUUID == null) {
            randomUUID = UUID.randomUUID();
        }
        this.mUUID = randomUUID;
    }
    
    public static MediaItem2 fromBundle(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        return fromBundle(bundle, UUID.fromString(bundle.getString("android.media.mediaitem2.uuid")));
    }
    
    static MediaItem2 fromBundle(final Bundle bundle, final UUID uuid) {
        MediaMetadata2 fromBundle = null;
        if (bundle == null) {
            return null;
        }
        final String string = bundle.getString("android.media.mediaitem2.id");
        final Bundle bundle2 = bundle.getBundle("android.media.mediaitem2.metadata");
        if (bundle2 != null) {
            fromBundle = MediaMetadata2.fromBundle(bundle2);
        }
        return new MediaItem2(string, null, fromBundle, bundle.getInt("android.media.mediaitem2.flags"), uuid);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof MediaItem2 && this.mUUID.equals(((MediaItem2)o).mUUID);
    }
    
    @Override
    public int hashCode() {
        return this.mUUID.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MediaItem2{");
        sb.append("mFlags=");
        sb.append(this.mFlags);
        sb.append(", mMetadata=");
        sb.append(this.mMetadata);
        sb.append('}');
        return sb.toString();
    }
}
