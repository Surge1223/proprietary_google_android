package android.support.v4.media;

import android.os.Bundle;
import android.annotation.TargetApi;

@TargetApi(19)
public class MediaSession2 implements AutoCloseable
{
    private final SupportLibraryImpl mImpl;
    
    @Override
    public void close() {
        try {
            this.mImpl.close();
        }
        catch (Exception ex) {}
    }
    
    public static final class CommandButton
    {
        private SessionCommand2 mCommand;
        private String mDisplayName;
        private boolean mEnabled;
        private Bundle mExtras;
        private int mIconResId;
        
        private CommandButton(final SessionCommand2 mCommand, final int mIconResId, final String mDisplayName, final Bundle mExtras, final boolean mEnabled) {
            this.mCommand = mCommand;
            this.mIconResId = mIconResId;
            this.mDisplayName = mDisplayName;
            this.mExtras = mExtras;
            this.mEnabled = mEnabled;
        }
        
        public static CommandButton fromBundle(final Bundle bundle) {
            if (bundle == null) {
                return null;
            }
            final Builder builder = new Builder();
            builder.setCommand(SessionCommand2.fromBundle(bundle.getBundle("android.media.media_session2.command_button.command")));
            builder.setIconResId(bundle.getInt("android.media.media_session2.command_button.icon_res_id", 0));
            builder.setDisplayName(bundle.getString("android.media.media_session2.command_button.display_name"));
            builder.setExtras(bundle.getBundle("android.media.media_session2.command_button.extras"));
            builder.setEnabled(bundle.getBoolean("android.media.media_session2.command_button.enabled"));
            try {
                return builder.build();
            }
            catch (IllegalStateException ex) {
                return null;
            }
        }
        
        public static final class Builder
        {
            private SessionCommand2 mCommand;
            private String mDisplayName;
            private boolean mEnabled;
            private Bundle mExtras;
            private int mIconResId;
            
            public CommandButton build() {
                return new CommandButton(this.mCommand, this.mIconResId, this.mDisplayName, this.mExtras, this.mEnabled);
            }
            
            public Builder setCommand(final SessionCommand2 mCommand) {
                this.mCommand = mCommand;
                return this;
            }
            
            public Builder setDisplayName(final String mDisplayName) {
                this.mDisplayName = mDisplayName;
                return this;
            }
            
            public Builder setEnabled(final boolean mEnabled) {
                this.mEnabled = mEnabled;
                return this;
            }
            
            public Builder setExtras(final Bundle mExtras) {
                this.mExtras = mExtras;
                return this;
            }
            
            public Builder setIconResId(final int mIconResId) {
                this.mIconResId = mIconResId;
                return this;
            }
        }
    }
    
    interface SupportLibraryImpl extends AutoCloseable
    {
    }
}
