package android.support.v4.media;

import java.util.List;
import android.os.ResultReceiver;
import android.os.Bundle;
import android.annotation.TargetApi;

@TargetApi(19)
public class MediaController2 implements AutoCloseable
{
    private final SupportLibraryImpl mImpl;
    
    @Override
    public void close() {
        try {
            this.mImpl.close();
        }
        catch (Exception ex) {}
    }
    
    public abstract static class ControllerCallback
    {
        public void onAllowedCommandsChanged(final MediaController2 mediaController2, final SessionCommandGroup2 sessionCommandGroup2) {
        }
        
        public void onBufferingStateChanged(final MediaController2 mediaController2, final MediaItem2 mediaItem2, final int n) {
        }
        
        public void onConnected(final MediaController2 mediaController2, final SessionCommandGroup2 sessionCommandGroup2) {
        }
        
        public void onCurrentMediaItemChanged(final MediaController2 mediaController2, final MediaItem2 mediaItem2) {
        }
        
        public void onCustomCommand(final MediaController2 mediaController2, final SessionCommand2 sessionCommand2, final Bundle bundle, final ResultReceiver resultReceiver) {
        }
        
        public void onCustomLayoutChanged(final MediaController2 mediaController2, final List<MediaSession2.CommandButton> list) {
        }
        
        public void onDisconnected(final MediaController2 mediaController2) {
        }
        
        public void onError(final MediaController2 mediaController2, final int n, final Bundle bundle) {
        }
        
        public void onPlaybackInfoChanged(final MediaController2 mediaController2, final PlaybackInfo playbackInfo) {
        }
        
        public void onPlaybackSpeedChanged(final MediaController2 mediaController2, final float n) {
        }
        
        public void onPlayerStateChanged(final MediaController2 mediaController2, final int n) {
        }
        
        public void onPlaylistChanged(final MediaController2 mediaController2, final List<MediaItem2> list, final MediaMetadata2 mediaMetadata2) {
        }
        
        public void onPlaylistMetadataChanged(final MediaController2 mediaController2, final MediaMetadata2 mediaMetadata2) {
        }
        
        public void onRepeatModeChanged(final MediaController2 mediaController2, final int n) {
        }
        
        public void onRoutesInfoChanged(final MediaController2 mediaController2, final List<Bundle> list) {
        }
        
        public void onSeekCompleted(final MediaController2 mediaController2, final long n) {
        }
        
        public void onShuffleModeChanged(final MediaController2 mediaController2, final int n) {
        }
    }
    
    public static final class PlaybackInfo
    {
        private final AudioAttributesCompat mAudioAttrsCompat;
        private final int mControlType;
        private final int mCurrentVolume;
        private final int mMaxVolume;
        private final int mPlaybackType;
        
        PlaybackInfo(final int mPlaybackType, final AudioAttributesCompat mAudioAttrsCompat, final int mControlType, final int mMaxVolume, final int mCurrentVolume) {
            this.mPlaybackType = mPlaybackType;
            this.mAudioAttrsCompat = mAudioAttrsCompat;
            this.mControlType = mControlType;
            this.mMaxVolume = mMaxVolume;
            this.mCurrentVolume = mCurrentVolume;
        }
        
        static PlaybackInfo createPlaybackInfo(final int n, final AudioAttributesCompat audioAttributesCompat, final int n2, final int n3, final int n4) {
            return new PlaybackInfo(n, audioAttributesCompat, n2, n3, n4);
        }
        
        static PlaybackInfo fromBundle(final Bundle bundle) {
            if (bundle == null) {
                return null;
            }
            return createPlaybackInfo(bundle.getInt("android.media.audio_info.playback_type"), AudioAttributesCompat.fromBundle(bundle.getBundle("android.media.audio_info.audio_attrs")), bundle.getInt("android.media.audio_info.control_type"), bundle.getInt("android.media.audio_info.max_volume"), bundle.getInt("android.media.audio_info.current_volume"));
        }
    }
    
    interface SupportLibraryImpl extends AutoCloseable
    {
    }
}
