package android.support.v4.media;

import android.os.Bundle;

public final class SessionToken2
{
    private final SupportLibraryImpl mImpl;
    
    SessionToken2(final SupportLibraryImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    public static SessionToken2 fromBundle(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (bundle.getInt("android.media.token.type", -1) == 100) {
            return new SessionToken2((SupportLibraryImpl)SessionToken2ImplLegacy.fromBundle(bundle));
        }
        return new SessionToken2((SupportLibraryImpl)SessionToken2ImplBase.fromBundle(bundle));
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SessionToken2 && this.mImpl.equals(((SessionToken2)o).mImpl);
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    @Override
    public String toString() {
        return this.mImpl.toString();
    }
    
    interface SupportLibraryImpl
    {
    }
}
