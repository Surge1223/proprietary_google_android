package android.support.v4.media;

import android.os.Build.VERSION;
import android.os.Process;
import android.support.v4.app.BundleCompat;
import android.os.ResultReceiver;
import android.util.Log;
import java.util.List;
import android.support.v4.media.session.PlaybackStateCompat;
import android.os.HandlerThread;
import android.os.Handler;
import android.support.v4.media.session.MediaControllerCompat;
import android.content.Context;
import java.util.concurrent.Executor;
import android.os.Bundle;
import android.annotation.TargetApi;

@TargetApi(16)
class MediaController2ImplLegacy implements SupportLibraryImpl
{
    private static final boolean DEBUG;
    static final Bundle sDefaultRootExtras;
    private SessionCommandGroup2 mAllowedCommands;
    private MediaBrowserCompat mBrowserCompat;
    private int mBufferingState;
    private final ControllerCallback mCallback;
    private final Executor mCallbackExecutor;
    private volatile boolean mConnected;
    private final Context mContext;
    private MediaControllerCompat mControllerCompat;
    private ControllerCompatCallback mControllerCompatCallback;
    private MediaItem2 mCurrentMediaItem;
    private final Handler mHandler;
    private final HandlerThread mHandlerThread;
    private MediaController2 mInstance;
    private boolean mIsReleased;
    final Object mLock;
    private MediaMetadataCompat mMediaMetadataCompat;
    private PlaybackInfo mPlaybackInfo;
    private PlaybackStateCompat mPlaybackStateCompat;
    private int mPlayerState;
    private List<MediaItem2> mPlaylist;
    private MediaMetadata2 mPlaylistMetadata;
    private int mRepeatMode;
    private int mShuffleMode;
    private final SessionToken2 mToken;
    
    static {
        DEBUG = Log.isLoggable("MC2ImplLegacy", 3);
        (sDefaultRootExtras = new Bundle()).putBoolean("android.support.v4.media.root_default_root", true);
    }
    
    private void sendCommand(final String s) {
        this.sendCommand(s, null, null);
    }
    
    private void sendCommand(final String s, final Bundle bundle, final ResultReceiver resultReceiver) {
        Bundle bundle2 = bundle;
        if (bundle == null) {
            bundle2 = new Bundle();
        }
        final Object mLock = this.mLock;
        // monitorenter(mLock)
        try {
            final MediaControllerCompat mControllerCompat = this.mControllerCompat;
            try {
                final ControllerCompatCallback mControllerCompatCallback = this.mControllerCompatCallback;
                // monitorexit(mLock)
                BundleCompat.putBinder(bundle2, "android.support.v4.media.argument.ICONTROLLER_CALLBACK", ((MediaControllerCompat.Callback)mControllerCompatCallback).getIControllerCallback().asBinder());
                bundle2.putString("android.support.v4.media.argument.PACKAGE_NAME", this.mContext.getPackageName());
                bundle2.putInt("android.support.v4.media.argument.UID", Process.myUid());
                bundle2.putInt("android.support.v4.media.argument.PID", Process.myPid());
                mControllerCompat.sendCommand(s, bundle2, resultReceiver);
                return;
            }
            finally {}
        }
        finally {}
        while (true) {
            try {
                // monitorexit(mLock)
                throw;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    private void sendCommand(final String s, final ResultReceiver resultReceiver) {
        this.sendCommand(s, null, resultReceiver);
    }
    
    @Override
    public void close() {
        if (MediaController2ImplLegacy.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("release from ");
            sb.append(this.mToken);
            Log.d("MC2ImplLegacy", sb.toString());
        }
        synchronized (this.mLock) {
            if (this.mIsReleased) {
                return;
            }
            this.mHandler.removeCallbacksAndMessages((Object)null);
            if (Build.VERSION.SDK_INT >= 18) {
                this.mHandlerThread.quitSafely();
            }
            else {
                this.mHandlerThread.quit();
            }
            this.mIsReleased = true;
            this.sendCommand("android.support.v4.media.controller.command.DISCONNECT");
            if (this.mControllerCompat != null) {
                this.mControllerCompat.unregisterCallback((MediaControllerCompat.Callback)this.mControllerCompatCallback);
            }
            if (this.mBrowserCompat != null) {
                this.mBrowserCompat.disconnect();
                this.mBrowserCompat = null;
            }
            if (this.mControllerCompat != null) {
                this.mControllerCompat.unregisterCallback((MediaControllerCompat.Callback)this.mControllerCompatCallback);
                this.mControllerCompat = null;
            }
            this.mConnected = false;
            // monitorexit(this.mLock)
            this.mCallbackExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    MediaController2ImplLegacy.this.mCallback.onDisconnected(MediaController2ImplLegacy.this.mInstance);
                }
            });
        }
    }
    
    void onConnectedNotLocked(final Bundle bundle) {
        bundle.setClassLoader(MediaSession2.class.getClassLoader());
        final SessionCommandGroup2 fromBundle = SessionCommandGroup2.fromBundle(bundle.getBundle("android.support.v4.media.argument.ALLOWED_COMMANDS"));
        final int int1 = bundle.getInt("android.support.v4.media.argument.PLAYER_STATE");
        final MediaItem2 fromBundle2 = MediaItem2.fromBundle(bundle.getBundle("android.support.v4.media.argument.MEDIA_ITEM"));
        final int int2 = bundle.getInt("android.support.v4.media.argument.BUFFERING_STATE");
        final PlaybackStateCompat mPlaybackStateCompat = (PlaybackStateCompat)bundle.getParcelable("android.support.v4.media.argument.PLAYBACK_STATE_COMPAT");
        final int int3 = bundle.getInt("android.support.v4.media.argument.REPEAT_MODE");
        final int int4 = bundle.getInt("android.support.v4.media.argument.SHUFFLE_MODE");
        final List<MediaItem2> convertToMediaItem2List = MediaUtils2.convertToMediaItem2List(bundle.getParcelableArray("android.support.v4.media.argument.PLAYLIST"));
        final MediaController2.PlaybackInfo fromBundle3 = MediaController2.PlaybackInfo.fromBundle(bundle.getBundle("android.support.v4.media.argument.PLAYBACK_INFO"));
        final MediaMetadata2 fromBundle4 = MediaMetadata2.fromBundle(bundle.getBundle("android.support.v4.media.argument.PLAYLIST_METADATA"));
        if (MediaController2ImplLegacy.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onConnectedNotLocked token=");
            sb.append(this.mToken);
            sb.append(", allowedCommands=");
            sb.append(fromBundle);
            Log.d("MC2ImplLegacy", sb.toString());
        }
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = b;
        try {
            final Object mLock = this.mLock;
            b3 = b;
            // monitorenter(mLock)
            b3 = b2;
            try {
                if (this.mIsReleased) {
                    b3 = b2;
                    // monitorexit(mLock)
                    if (false) {
                        this.close();
                    }
                    return;
                }
                b3 = b2;
                if (this.mConnected) {
                    b3 = b2;
                    Log.e("MC2ImplLegacy", "Cannot be notified about the connection result many times. Probably a bug or malicious app.");
                    b3 = true;
                    // monitorexit(mLock)
                    if (true) {
                        this.close();
                    }
                    return;
                }
                b3 = b2;
                this.mAllowedCommands = fromBundle;
                b3 = b2;
                this.mPlayerState = int1;
                b3 = b2;
                this.mCurrentMediaItem = fromBundle2;
                b3 = b2;
                this.mBufferingState = int2;
                b3 = b2;
                this.mPlaybackStateCompat = mPlaybackStateCompat;
                b3 = b2;
                this.mRepeatMode = int3;
                b3 = b2;
                this.mShuffleMode = int4;
                b3 = b2;
                this.mPlaylist = convertToMediaItem2List;
                b3 = b2;
                this.mPlaylistMetadata = fromBundle4;
                b3 = b2;
                this.mConnected = true;
                b3 = b2;
                this.mPlaybackInfo = fromBundle3;
                b3 = b2;
                // monitorexit(mLock)
                b3 = b;
                final Executor mCallbackExecutor = this.mCallbackExecutor;
                b3 = b;
                b3 = b;
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        MediaController2ImplLegacy.this.mCallback.onConnected(MediaController2ImplLegacy.this.mInstance, fromBundle);
                    }
                };
                b3 = b;
                mCallbackExecutor.execute(runnable);
                if (false) {
                    this.close();
                }
            }
            finally {
            }
            // monitorexit(mLock)
        }
        finally {
            if (b3) {
                this.close();
            }
        }
    }
    
    private final class ControllerCompatCallback extends Callback
    {
        final /* synthetic */ MediaController2ImplLegacy this$0;
        
        @Override
        public void onMetadataChanged(final MediaMetadataCompat mediaMetadataCompat) {
            synchronized (this.this$0.mLock) {
                this.this$0.mMediaMetadataCompat = mediaMetadataCompat;
            }
        }
        
        @Override
        public void onPlaybackStateChanged(final PlaybackStateCompat playbackStateCompat) {
            synchronized (this.this$0.mLock) {
                this.this$0.mPlaybackStateCompat = playbackStateCompat;
            }
        }
        
        @Override
        public void onSessionDestroyed() {
            this.this$0.close();
        }
        
        @Override
        public void onSessionEvent(final String s, final Bundle bundle) {
            if (bundle != null) {
                bundle.setClassLoader(MediaSession2.class.getClassLoader());
            }
            switch (s) {
                case "android.support.v4.media.session.event.ON_SEEK_COMPLETED": {
                    final long long1 = bundle.getLong("android.support.v4.media.argument.SEEK_POSITION");
                    final PlaybackStateCompat playbackStateCompat = (PlaybackStateCompat)bundle.getParcelable("android.support.v4.media.argument.PLAYBACK_STATE_COMPAT");
                    if (playbackStateCompat == null) {
                        return;
                    }
                    synchronized (this.this$0.mLock) {
                        this.this$0.mPlaybackStateCompat = playbackStateCompat;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onSeekCompleted(ControllerCompatCallback.this.this$0.mInstance, long1);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_BUFFERING_STATE_CHANGED": {
                    final MediaItem2 fromBundle = MediaItem2.fromBundle(bundle.getBundle("android.support.v4.media.argument.MEDIA_ITEM"));
                    final int int1 = bundle.getInt("android.support.v4.media.argument.BUFFERING_STATE");
                    final PlaybackStateCompat playbackStateCompat2 = (PlaybackStateCompat)bundle.getParcelable("android.support.v4.media.argument.PLAYBACK_STATE_COMPAT");
                    if (fromBundle != null) {
                        if (playbackStateCompat2 != null) {
                            synchronized (this.this$0.mLock) {
                                this.this$0.mBufferingState = int1;
                                this.this$0.mPlaybackStateCompat = playbackStateCompat2;
                                // monitorexit(this.this$0.mLock)
                                this.this$0.mCallbackExecutor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        ControllerCompatCallback.this.this$0.mCallback.onBufferingStateChanged(ControllerCompatCallback.this.this$0.mInstance, fromBundle, int1);
                                    }
                                });
                                break;
                            }
                        }
                    }
                }
                case "android.support.v4.media.session.event.ON_PLAYBACK_SPEED_CHANGED": {
                    final PlaybackStateCompat playbackStateCompat3 = (PlaybackStateCompat)bundle.getParcelable("android.support.v4.media.argument.PLAYBACK_STATE_COMPAT");
                    if (playbackStateCompat3 == null) {
                        return;
                    }
                    synchronized (this.this$0.mLock) {
                        this.this$0.mPlaybackStateCompat = playbackStateCompat3;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onPlaybackSpeedChanged(ControllerCompatCallback.this.this$0.mInstance, playbackStateCompat3.getPlaybackSpeed());
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_PLAYBACK_INFO_CHANGED": {
                    final MediaController2.PlaybackInfo fromBundle2 = MediaController2.PlaybackInfo.fromBundle(bundle.getBundle("android.support.v4.media.argument.PLAYBACK_INFO"));
                    if (fromBundle2 == null) {
                        return;
                    }
                    synchronized (this.this$0.mLock) {
                        this.this$0.mPlaybackInfo = fromBundle2;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onPlaybackInfoChanged(ControllerCompatCallback.this.this$0.mInstance, fromBundle2);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.SET_CUSTOM_LAYOUT": {
                    final List<MediaSession2.CommandButton> convertToCommandButtonList = MediaUtils2.convertToCommandButtonList(bundle.getParcelableArray("android.support.v4.media.argument.COMMAND_BUTTONS"));
                    if (convertToCommandButtonList == null) {
                        return;
                    }
                    this.this$0.mCallbackExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            ControllerCompatCallback.this.this$0.mCallback.onCustomLayoutChanged(ControllerCompatCallback.this.this$0.mInstance, convertToCommandButtonList);
                        }
                    });
                    break;
                }
                case "android.support.v4.media.session.event.SEND_CUSTOM_COMMAND": {
                    final Bundle bundle2 = bundle.getBundle("android.support.v4.media.argument.CUSTOM_COMMAND");
                    if (bundle2 == null) {
                        return;
                    }
                    this.this$0.mCallbackExecutor.execute(new Runnable() {
                        final /* synthetic */ Bundle val$args = bundle.getBundle("android.support.v4.media.argument.ARGUMENTS");
                        final /* synthetic */ SessionCommand2 val$command = SessionCommand2.fromBundle(bundle2);
                        final /* synthetic */ ResultReceiver val$receiver = (ResultReceiver)bundle.getParcelable("android.support.v4.media.argument.RESULT_RECEIVER");
                        
                        @Override
                        public void run() {
                            ControllerCompatCallback.this.this$0.mCallback.onCustomCommand(ControllerCompatCallback.this.this$0.mInstance, this.val$command, this.val$args, this.val$receiver);
                        }
                    });
                    break;
                }
                case "android.support.v4.media.session.event.ON_SHUFFLE_MODE_CHANGED": {
                    final int int2 = bundle.getInt("android.support.v4.media.argument.SHUFFLE_MODE");
                    synchronized (this.this$0.mLock) {
                        this.this$0.mShuffleMode = int2;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onShuffleModeChanged(ControllerCompatCallback.this.this$0.mInstance, int2);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_REPEAT_MODE_CHANGED": {
                    final int int3 = bundle.getInt("android.support.v4.media.argument.REPEAT_MODE");
                    synchronized (this.this$0.mLock) {
                        this.this$0.mRepeatMode = int3;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onRepeatModeChanged(ControllerCompatCallback.this.this$0.mInstance, int3);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_PLAYLIST_METADATA_CHANGED": {
                    final MediaMetadata2 fromBundle3 = MediaMetadata2.fromBundle(bundle.getBundle("android.support.v4.media.argument.PLAYLIST_METADATA"));
                    synchronized (this.this$0.mLock) {
                        this.this$0.mPlaylistMetadata = fromBundle3;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onPlaylistMetadataChanged(ControllerCompatCallback.this.this$0.mInstance, fromBundle3);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_PLAYLIST_CHANGED": {
                    final MediaMetadata2 fromBundle4 = MediaMetadata2.fromBundle(bundle.getBundle("android.support.v4.media.argument.PLAYLIST_METADATA"));
                    final List<MediaItem2> convertToMediaItem2List = MediaUtils2.convertToMediaItem2List(bundle.getParcelableArray("android.support.v4.media.argument.PLAYLIST"));
                    synchronized (this.this$0.mLock) {
                        this.this$0.mPlaylist = convertToMediaItem2List;
                        this.this$0.mPlaylistMetadata = fromBundle4;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onPlaylistChanged(ControllerCompatCallback.this.this$0.mInstance, convertToMediaItem2List, fromBundle4);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_ROUTES_INFO_CHANGED": {
                    this.this$0.mCallbackExecutor.execute(new Runnable() {
                        final /* synthetic */ List val$routes = MediaUtils2.convertToBundleList(bundle.getParcelableArray("android.support.v4.media.argument.ROUTE_BUNDLE"));
                        
                        @Override
                        public void run() {
                            ControllerCompatCallback.this.this$0.mCallback.onRoutesInfoChanged(ControllerCompatCallback.this.this$0.mInstance, this.val$routes);
                        }
                    });
                    break;
                }
                case "android.support.v4.media.session.event.ON_ERROR": {
                    this.this$0.mCallbackExecutor.execute(new Runnable() {
                        final /* synthetic */ int val$errorCode = bundle.getInt("android.support.v4.media.argument.ERROR_CODE");
                        final /* synthetic */ Bundle val$errorExtras = bundle.getBundle("android.support.v4.media.argument.EXTRAS");
                        
                        @Override
                        public void run() {
                            ControllerCompatCallback.this.this$0.mCallback.onError(ControllerCompatCallback.this.this$0.mInstance, this.val$errorCode, this.val$errorExtras);
                        }
                    });
                    break;
                }
                case "android.support.v4.media.session.event.ON_CURRENT_MEDIA_ITEM_CHANGED": {
                    final MediaItem2 fromBundle5 = MediaItem2.fromBundle(bundle.getBundle("android.support.v4.media.argument.MEDIA_ITEM"));
                    synchronized (this.this$0.mLock) {
                        this.this$0.mCurrentMediaItem = fromBundle5;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onCurrentMediaItemChanged(ControllerCompatCallback.this.this$0.mInstance, fromBundle5);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_PLAYER_STATE_CHANGED": {
                    final int int4 = bundle.getInt("android.support.v4.media.argument.PLAYER_STATE");
                    final PlaybackStateCompat playbackStateCompat4 = (PlaybackStateCompat)bundle.getParcelable("android.support.v4.media.argument.PLAYBACK_STATE_COMPAT");
                    if (playbackStateCompat4 == null) {
                        return;
                    }
                    synchronized (this.this$0.mLock) {
                        this.this$0.mPlayerState = int4;
                        this.this$0.mPlaybackStateCompat = playbackStateCompat4;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onPlayerStateChanged(ControllerCompatCallback.this.this$0.mInstance, int4);
                            }
                        });
                        break;
                    }
                }
                case "android.support.v4.media.session.event.ON_ALLOWED_COMMANDS_CHANGED": {
                    final SessionCommandGroup2 fromBundle6 = SessionCommandGroup2.fromBundle(bundle.getBundle("android.support.v4.media.argument.ALLOWED_COMMANDS"));
                    synchronized (this.this$0.mLock) {
                        this.this$0.mAllowedCommands = fromBundle6;
                        // monitorexit(this.this$0.mLock)
                        this.this$0.mCallbackExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ControllerCompatCallback.this.this$0.mCallback.onAllowedCommandsChanged(ControllerCompatCallback.this.this$0.mInstance, fromBundle6);
                            }
                        });
                    }
                    break;
                }
            }
        }
        
        @Override
        public void onSessionReady() {
            MediaController2ImplLegacy.this.sendCommand("android.support.v4.media.controller.command.CONNECT", new ResultReceiver(this.this$0.mHandler) {
                protected void onReceiveResult(final int n, final Bundle bundle) {
                    if (!ControllerCompatCallback.this.this$0.mHandlerThread.isAlive()) {
                        return;
                    }
                    switch (n) {
                        case 0: {
                            ControllerCompatCallback.this.this$0.onConnectedNotLocked(bundle);
                            break;
                        }
                        case -1: {
                            ControllerCompatCallback.this.this$0.mCallbackExecutor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    ControllerCompatCallback.this.this$0.mCallback.onDisconnected(ControllerCompatCallback.this.this$0.mInstance);
                                }
                            });
                            ControllerCompatCallback.this.this$0.close();
                            break;
                        }
                    }
                }
            });
        }
    }
}
