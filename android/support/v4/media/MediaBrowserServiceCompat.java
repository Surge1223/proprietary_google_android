package android.support.v4.media;

import android.os.Binder;
import java.util.Collection;
import android.os.Message;
import android.text.TextUtils;
import android.os.Parcel;
import android.support.v4.media.session.IMediaSession;
import android.support.v4.app.BundleCompat;
import android.os.Handler;
import android.content.Context;
import android.os.Messenger;
import java.util.HashMap;
import android.os.IBinder$DeathRecipient;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.os.ResultReceiver;
import android.os.Build.VERSION;
import android.content.Intent;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import android.support.v4.util.Pair;
import java.util.List;
import android.os.Bundle;
import android.util.Log;
import android.support.v4.media.session.MediaSessionCompat;
import android.os.IBinder;
import android.support.v4.util.ArrayMap;
import android.app.Service;

public abstract class MediaBrowserServiceCompat extends Service
{
    static final boolean DEBUG;
    final ArrayMap<IBinder, ConnectionRecord> mConnections;
    ConnectionRecord mCurConnection;
    final ServiceHandler mHandler;
    private MediaBrowserServiceImpl mImpl;
    MediaSessionCompat.Token mSession;
    
    static {
        DEBUG = Log.isLoggable("MBServiceCompat", 3);
    }
    
    void addSubscription(final String s, final ConnectionRecord connectionRecord, final IBinder binder, final Bundle bundle) {
        List<Pair<IBinder, Bundle>> list;
        if ((list = connectionRecord.subscriptions.get(s)) == null) {
            list = new ArrayList<Pair<IBinder, Bundle>>();
        }
        for (final Pair<IBinder, Bundle> pair : list) {
            if (binder == pair.first && MediaBrowserCompatUtils.areSameOptions(bundle, pair.second)) {
                return;
            }
        }
        list.add(new Pair<IBinder, Bundle>(binder, bundle));
        connectionRecord.subscriptions.put(s, list);
        this.performLoadChildren(s, connectionRecord, bundle, null);
    }
    
    List<MediaBrowserCompat.MediaItem> applyOptions(final List<MediaBrowserCompat.MediaItem> list, final Bundle bundle) {
        if (list == null) {
            return null;
        }
        final int int1 = bundle.getInt("android.media.browse.extra.PAGE", -1);
        final int int2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (int1 == -1 && int2 == -1) {
            return list;
        }
        final int n = int2 * int1;
        final int n2 = n + int2;
        if (int1 >= 0 && int2 >= 1 && n < list.size()) {
            int size;
            if ((size = n2) > list.size()) {
                size = list.size();
            }
            return list.subList(n, size);
        }
        return (List<MediaBrowserCompat.MediaItem>)Collections.EMPTY_LIST;
    }
    
    public void dump(final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
    }
    
    boolean isValidPackage(final String s, int i) {
        if (s == null) {
            return false;
        }
        final String[] packagesForUid = this.getPackageManager().getPackagesForUid(i);
        int length;
        for (length = packagesForUid.length, i = 0; i < length; ++i) {
            if (packagesForUid[i].equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    public IBinder onBind(final Intent intent) {
        return this.mImpl.onBind(intent);
    }
    
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 28) {
            this.mImpl = (MediaBrowserServiceImpl)new MediaBrowserServiceImplApi28();
        }
        else if (Build.VERSION.SDK_INT >= 26) {
            this.mImpl = (MediaBrowserServiceImpl)new MediaBrowserServiceImplApi26();
        }
        else if (Build.VERSION.SDK_INT >= 23) {
            this.mImpl = (MediaBrowserServiceImpl)new MediaBrowserServiceImplApi23();
        }
        else if (Build.VERSION.SDK_INT >= 21) {
            this.mImpl = (MediaBrowserServiceImpl)new MediaBrowserServiceImplApi21();
        }
        else {
            this.mImpl = (MediaBrowserServiceImpl)new MediaBrowserServiceImplBase();
        }
        this.mImpl.onCreate();
    }
    
    public void onCustomAction(final String s, final Bundle bundle, final Result<Bundle> result) {
        result.sendError(null);
    }
    
    public abstract BrowserRoot onGetRoot(final String p0, final int p1, final Bundle p2);
    
    public abstract void onLoadChildren(final String p0, final Result<List<MediaBrowserCompat.MediaItem>> p1);
    
    public void onLoadChildren(final String s, final Result<List<MediaBrowserCompat.MediaItem>> result, final Bundle bundle) {
        result.setFlags(1);
        this.onLoadChildren(s, result);
    }
    
    public void onLoadItem(final String s, final Result<MediaBrowserCompat.MediaItem> result) {
        result.setFlags(2);
        result.sendResult(null);
    }
    
    public void onSearch(final String s, final Bundle bundle, final Result<List<MediaBrowserCompat.MediaItem>> result) {
        result.setFlags(4);
        result.sendResult(null);
    }
    
    void performCustomAction(final String s, final Bundle bundle, final ConnectionRecord mCurConnection, final ResultReceiver resultReceiver) {
        final Result<Bundle> result = new Result<Bundle>(s) {
            @Override
            void onErrorSent(final Bundle bundle) {
                resultReceiver.send(-1, bundle);
            }
            
            void onResultSent(final Bundle bundle) {
                resultReceiver.send(0, bundle);
            }
        };
        this.mCurConnection = mCurConnection;
        this.onCustomAction(s, bundle, (Result<Bundle>)result);
        this.mCurConnection = null;
        if (((Result)result).isDone()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("onCustomAction must call detach() or sendResult() or sendError() before returning for action=");
        sb.append(s);
        sb.append(" extras=");
        sb.append(bundle);
        throw new IllegalStateException(sb.toString());
    }
    
    void performLoadChildren(final String s, final ConnectionRecord mCurConnection, final Bundle bundle, final Bundle bundle2) {
        final Result<List<MediaBrowserCompat.MediaItem>> result = new Result<List<MediaBrowserCompat.MediaItem>>(s) {
            void onResultSent(List<MediaBrowserCompat.MediaItem> applyOptions) {
                if (MediaBrowserServiceCompat.this.mConnections.get(mCurConnection.callbacks.asBinder()) != mCurConnection) {
                    if (MediaBrowserServiceCompat.DEBUG) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Not sending onLoadChildren result for connection that has been disconnected. pkg=");
                        sb.append(mCurConnection.pkg);
                        sb.append(" id=");
                        sb.append(s);
                        Log.d("MBServiceCompat", sb.toString());
                    }
                    return;
                }
                if ((((Result)this).getFlags() & 0x1) != 0x0) {
                    applyOptions = MediaBrowserServiceCompat.this.applyOptions(applyOptions, bundle);
                }
                try {
                    mCurConnection.callbacks.onLoadChildren(s, applyOptions, bundle, bundle2);
                }
                catch (RemoteException ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Calling onLoadChildren() failed for id=");
                    sb2.append(s);
                    sb2.append(" package=");
                    sb2.append(mCurConnection.pkg);
                    Log.w("MBServiceCompat", sb2.toString());
                }
            }
        };
        this.mCurConnection = mCurConnection;
        if (bundle == null) {
            this.onLoadChildren(s, (Result<List<MediaBrowserCompat.MediaItem>>)result);
        }
        else {
            this.onLoadChildren(s, (Result<List<MediaBrowserCompat.MediaItem>>)result, bundle);
        }
        this.mCurConnection = null;
        if (((Result)result).isDone()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("onLoadChildren must call detach() or sendResult() before returning for package=");
        sb.append(mCurConnection.pkg);
        sb.append(" id=");
        sb.append(s);
        throw new IllegalStateException(sb.toString());
    }
    
    void performLoadItem(final String s, final ConnectionRecord mCurConnection, final ResultReceiver resultReceiver) {
        final Result<MediaBrowserCompat.MediaItem> result = new Result<MediaBrowserCompat.MediaItem>(s) {
            void onResultSent(final MediaBrowserCompat.MediaItem mediaItem) {
                if ((((Result)this).getFlags() & 0x2) != 0x0) {
                    resultReceiver.send(-1, null);
                    return;
                }
                final Bundle bundle = new Bundle();
                bundle.putParcelable("media_item", (Parcelable)mediaItem);
                resultReceiver.send(0, bundle);
            }
        };
        this.mCurConnection = mCurConnection;
        this.onLoadItem(s, (Result<MediaBrowserCompat.MediaItem>)result);
        this.mCurConnection = null;
        if (((Result)result).isDone()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("onLoadItem must call detach() or sendResult() before returning for id=");
        sb.append(s);
        throw new IllegalStateException(sb.toString());
    }
    
    void performSearch(final String s, final Bundle bundle, final ConnectionRecord mCurConnection, final ResultReceiver resultReceiver) {
        final Result<List<MediaBrowserCompat.MediaItem>> result = new Result<List<MediaBrowserCompat.MediaItem>>(s) {
            void onResultSent(final List<MediaBrowserCompat.MediaItem> list) {
                if ((((Result)this).getFlags() & 0x4) == 0x0 && list != null) {
                    final Bundle bundle = new Bundle();
                    bundle.putParcelableArray("search_results", (Parcelable[])list.toArray((Parcelable[])new MediaBrowserCompat.MediaItem[0]));
                    resultReceiver.send(0, bundle);
                    return;
                }
                resultReceiver.send(-1, null);
            }
        };
        this.mCurConnection = mCurConnection;
        this.onSearch(s, bundle, (Result<List<MediaBrowserCompat.MediaItem>>)result);
        this.mCurConnection = null;
        if (((Result)result).isDone()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("onSearch must call detach() or sendResult() before returning for query=");
        sb.append(s);
        throw new IllegalStateException(sb.toString());
    }
    
    boolean removeSubscription(final String s, final ConnectionRecord connectionRecord, final IBinder binder) {
        if (binder == null) {
            return connectionRecord.subscriptions.remove(s) != null;
        }
        boolean b = false;
        boolean b2 = false;
        final List<Pair<IBinder, Bundle>> list = connectionRecord.subscriptions.get(s);
        if (list != null) {
            final Iterator<Pair<IBinder, Bundle>> iterator = list.iterator();
            while (iterator.hasNext()) {
                if (binder == iterator.next().first) {
                    b2 = true;
                    iterator.remove();
                }
            }
            b = b2;
            if (list.size() == 0) {
                connectionRecord.subscriptions.remove(s);
                b = b2;
            }
        }
        return b;
    }
    
    public static final class BrowserRoot
    {
        private final Bundle mExtras;
        private final String mRootId;
        
        public BrowserRoot(final String mRootId, final Bundle mExtras) {
            if (mRootId != null) {
                this.mRootId = mRootId;
                this.mExtras = mExtras;
                return;
            }
            throw new IllegalArgumentException("The root id in BrowserRoot cannot be null. Use null for BrowserRoot instead.");
        }
        
        public Bundle getExtras() {
            return this.mExtras;
        }
        
        public String getRootId() {
            return this.mRootId;
        }
    }
    
    private class ConnectionRecord implements IBinder$DeathRecipient
    {
        public final MediaSessionManager.RemoteUserInfo browserInfo;
        public final ServiceCallbacks callbacks;
        public final int pid;
        public final String pkg;
        public BrowserRoot root;
        public final Bundle rootHints;
        public final HashMap<String, List<Pair<IBinder, Bundle>>> subscriptions;
        public final int uid;
        
        ConnectionRecord(final String pkg, final int pid, final int uid, final Bundle rootHints, final ServiceCallbacks callbacks) {
            this.subscriptions = new HashMap<String, List<Pair<IBinder, Bundle>>>();
            this.pkg = pkg;
            this.pid = pid;
            this.uid = uid;
            this.browserInfo = new MediaSessionManager.RemoteUserInfo(pkg, pid, uid);
            this.rootHints = rootHints;
            this.callbacks = callbacks;
        }
        
        public void binderDied() {
            MediaBrowserServiceCompat.this.mHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    MediaBrowserServiceCompat.this.mConnections.remove(ConnectionRecord.this.callbacks.asBinder());
                }
            });
        }
    }
    
    interface MediaBrowserServiceImpl
    {
        IBinder onBind(final Intent p0);
        
        void onCreate();
    }
    
    class MediaBrowserServiceImplApi21 implements MediaBrowserServiceImpl, ServiceCompatProxy
    {
        Messenger mMessenger;
        final List<Bundle> mRootExtrasList;
        Object mServiceObj;
        
        MediaBrowserServiceImplApi21() {
            this.mRootExtrasList = new ArrayList<Bundle>();
        }
        
        @Override
        public IBinder onBind(final Intent intent) {
            return MediaBrowserServiceCompatApi21.onBind(this.mServiceObj, intent);
        }
        
        @Override
        public void onCreate() {
            MediaBrowserServiceCompatApi21.onCreate(this.mServiceObj = MediaBrowserServiceCompatApi21.createService((Context)MediaBrowserServiceCompat.this, (MediaBrowserServiceCompatApi21.ServiceCompatProxy)this));
        }
        
        @Override
        public MediaBrowserServiceCompatApi21.BrowserRoot onGetRoot(final String s, final int n, final Bundle bundle) {
            Bundle bundle3;
            final Bundle bundle2 = bundle3 = null;
            if (bundle != null) {
                bundle3 = bundle2;
                if (bundle.getInt("extra_client_version", 0) != 0) {
                    bundle.remove("extra_client_version");
                    this.mMessenger = new Messenger((Handler)MediaBrowserServiceCompat.this.mHandler);
                    final Bundle bundle4 = new Bundle();
                    bundle4.putInt("extra_service_version", 2);
                    BundleCompat.putBinder(bundle4, "extra_messenger", this.mMessenger.getBinder());
                    if (MediaBrowserServiceCompat.this.mSession != null) {
                        final IMediaSession extraBinder = MediaBrowserServiceCompat.this.mSession.getExtraBinder();
                        IBinder binder;
                        if (extraBinder == null) {
                            binder = null;
                        }
                        else {
                            binder = extraBinder.asBinder();
                        }
                        BundleCompat.putBinder(bundle4, "extra_session_binder", binder);
                        bundle3 = bundle4;
                    }
                    else {
                        this.mRootExtrasList.add(bundle4);
                        bundle3 = bundle4;
                    }
                }
            }
            MediaBrowserServiceCompat.this.mCurConnection = new ConnectionRecord(s, -1, n, bundle, null);
            final MediaBrowserServiceCompat.BrowserRoot onGetRoot = MediaBrowserServiceCompat.this.onGetRoot(s, n, bundle);
            MediaBrowserServiceCompat.this.mCurConnection = null;
            if (onGetRoot == null) {
                return null;
            }
            Bundle extras;
            if (bundle3 == null) {
                extras = onGetRoot.getExtras();
            }
            else {
                extras = bundle3;
                if (onGetRoot.getExtras() != null) {
                    bundle3.putAll(onGetRoot.getExtras());
                    extras = bundle3;
                }
            }
            return new MediaBrowserServiceCompatApi21.BrowserRoot(onGetRoot.getRootId(), extras);
        }
        
        @Override
        public void onLoadChildren(final String s, final ResultWrapper<List<Parcel>> resultWrapper) {
            MediaBrowserServiceCompat.this.onLoadChildren(s, (Result<List<MediaBrowserCompat.MediaItem>>)new Result<List<MediaBrowserCompat.MediaItem>>(s) {
                void onResultSent(final List<MediaBrowserCompat.MediaItem> list) {
                    ArrayList<Parcel> list2 = null;
                    if (list != null) {
                        final ArrayList<Parcel> list3 = new ArrayList<Parcel>();
                        final Iterator<MediaBrowserCompat.MediaItem> iterator = list.iterator();
                        while (true) {
                            list2 = list3;
                            if (!iterator.hasNext()) {
                                break;
                            }
                            final MediaBrowserCompat.MediaItem mediaItem = iterator.next();
                            final Parcel obtain = Parcel.obtain();
                            mediaItem.writeToParcel(obtain, 0);
                            list3.add(obtain);
                        }
                    }
                    resultWrapper.sendResult(list2);
                }
            });
        }
    }
    
    class MediaBrowserServiceImplApi23 extends MediaBrowserServiceImplApi21 implements MediaBrowserServiceCompatApi23.ServiceCompatProxy
    {
        @Override
        public void onCreate() {
            MediaBrowserServiceCompatApi21.onCreate(this.mServiceObj = MediaBrowserServiceCompatApi23.createService((Context)MediaBrowserServiceCompat.this, (MediaBrowserServiceCompatApi23.ServiceCompatProxy)this));
        }
        
        @Override
        public void onLoadItem(final String s, final ResultWrapper<Parcel> resultWrapper) {
            MediaBrowserServiceCompat.this.onLoadItem(s, (Result<MediaBrowserCompat.MediaItem>)new Result<MediaBrowserCompat.MediaItem>(s) {
                void onResultSent(final MediaBrowserCompat.MediaItem mediaItem) {
                    if (mediaItem == null) {
                        resultWrapper.sendResult(null);
                    }
                    else {
                        final Parcel obtain = Parcel.obtain();
                        mediaItem.writeToParcel(obtain, 0);
                        resultWrapper.sendResult(obtain);
                    }
                }
            });
        }
    }
    
    class MediaBrowserServiceImplApi26 extends MediaBrowserServiceImplApi23 implements MediaBrowserServiceCompatApi26.ServiceCompatProxy
    {
        @Override
        public void onCreate() {
            MediaBrowserServiceCompatApi21.onCreate(this.mServiceObj = MediaBrowserServiceCompatApi26.createService((Context)MediaBrowserServiceCompat.this, (MediaBrowserServiceCompatApi26.ServiceCompatProxy)this));
        }
        
        @Override
        public void onLoadChildren(final String s, final MediaBrowserServiceCompatApi26.ResultWrapper resultWrapper, final Bundle bundle) {
            MediaBrowserServiceCompat.this.onLoadChildren(s, (Result<List<MediaBrowserCompat.MediaItem>>)new Result<List<MediaBrowserCompat.MediaItem>>(s) {
                void onResultSent(final List<MediaBrowserCompat.MediaItem> list) {
                    List<Parcel> list2 = null;
                    if (list != null) {
                        final ArrayList<Parcel> list3 = new ArrayList<Parcel>();
                        final Iterator<MediaBrowserCompat.MediaItem> iterator = list.iterator();
                        while (true) {
                            list2 = list3;
                            if (!iterator.hasNext()) {
                                break;
                            }
                            final MediaBrowserCompat.MediaItem mediaItem = iterator.next();
                            final Parcel obtain = Parcel.obtain();
                            mediaItem.writeToParcel(obtain, 0);
                            list3.add(obtain);
                        }
                    }
                    resultWrapper.sendResult(list2, ((Result)this).getFlags());
                }
            }, bundle);
        }
    }
    
    class MediaBrowserServiceImplApi28 extends MediaBrowserServiceImplApi26
    {
    }
    
    class MediaBrowserServiceImplBase implements MediaBrowserServiceImpl
    {
        private Messenger mMessenger;
        
        @Override
        public IBinder onBind(final Intent intent) {
            if ("android.media.browse.MediaBrowserService".equals(intent.getAction())) {
                return this.mMessenger.getBinder();
            }
            return null;
        }
        
        @Override
        public void onCreate() {
            this.mMessenger = new Messenger((Handler)MediaBrowserServiceCompat.this.mHandler);
        }
    }
    
    public static class Result<T>
    {
        private final Object mDebug;
        private boolean mDetachCalled;
        private int mFlags;
        private boolean mSendErrorCalled;
        private boolean mSendResultCalled;
        
        Result(final Object mDebug) {
            this.mDebug = mDebug;
        }
        
        int getFlags() {
            return this.mFlags;
        }
        
        boolean isDone() {
            return this.mDetachCalled || this.mSendResultCalled || this.mSendErrorCalled;
        }
        
        void onErrorSent(final Bundle bundle) {
            final StringBuilder sb = new StringBuilder();
            sb.append("It is not supported to send an error for ");
            sb.append(this.mDebug);
            throw new UnsupportedOperationException(sb.toString());
        }
        
        void onResultSent(final T t) {
        }
        
        public void sendError(final Bundle bundle) {
            if (!this.mSendResultCalled && !this.mSendErrorCalled) {
                this.mSendErrorCalled = true;
                this.onErrorSent(bundle);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("sendError() called when either sendResult() or sendError() had already been called for: ");
            sb.append(this.mDebug);
            throw new IllegalStateException(sb.toString());
        }
        
        public void sendResult(final T t) {
            if (!this.mSendResultCalled && !this.mSendErrorCalled) {
                this.mSendResultCalled = true;
                this.onResultSent(t);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("sendResult() called when either sendResult() or sendError() had already been called for: ");
            sb.append(this.mDebug);
            throw new IllegalStateException(sb.toString());
        }
        
        void setFlags(final int mFlags) {
            this.mFlags = mFlags;
        }
    }
    
    private class ServiceBinderImpl
    {
        final /* synthetic */ MediaBrowserServiceCompat this$0;
        
        public void addSubscription(final String s, final IBinder binder, final Bundle bundle, final ServiceCallbacks serviceCallbacks) {
            this.this$0.mHandler.postOrRun(new Runnable() {
                @Override
                public void run() {
                    final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.get(serviceCallbacks.asBinder());
                    if (connectionRecord == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("addSubscription for callback that isn't registered id=");
                        sb.append(s);
                        Log.w("MBServiceCompat", sb.toString());
                        return;
                    }
                    ServiceBinderImpl.this.this$0.addSubscription(s, connectionRecord, binder, bundle);
                }
            });
        }
        
        public void connect(final String s, final int n, final int n2, final Bundle bundle, final ServiceCallbacks serviceCallbacks) {
            if (this.this$0.isValidPackage(s, n2)) {
                this.this$0.mHandler.postOrRun(new Runnable() {
                    @Override
                    public void run() {
                        final IBinder binder = serviceCallbacks.asBinder();
                        ServiceBinderImpl.this.this$0.mConnections.remove(binder);
                        final ConnectionRecord mCurConnection = ServiceBinderImpl.this.this$0.new ConnectionRecord(s, n, n2, bundle, serviceCallbacks);
                        ServiceBinderImpl.this.this$0.mCurConnection = mCurConnection;
                        mCurConnection.root = ServiceBinderImpl.this.this$0.onGetRoot(s, n2, bundle);
                        ServiceBinderImpl.this.this$0.mCurConnection = null;
                        if (mCurConnection.root == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No root for client ");
                            sb.append(s);
                            sb.append(" from service ");
                            sb.append(this.getClass().getName());
                            Log.i("MBServiceCompat", sb.toString());
                            try {
                                serviceCallbacks.onConnectFailed();
                            }
                            catch (RemoteException ex) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Calling onConnectFailed() failed. Ignoring. pkg=");
                                sb2.append(s);
                                Log.w("MBServiceCompat", sb2.toString());
                            }
                        }
                        else {
                            try {
                                ServiceBinderImpl.this.this$0.mConnections.put(binder, mCurConnection);
                                binder.linkToDeath((IBinder$DeathRecipient)mCurConnection, 0);
                                if (ServiceBinderImpl.this.this$0.mSession != null) {
                                    serviceCallbacks.onConnect(mCurConnection.root.getRootId(), ServiceBinderImpl.this.this$0.mSession, mCurConnection.root.getExtras());
                                }
                            }
                            catch (RemoteException ex2) {
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("Calling onConnect() failed. Dropping client. pkg=");
                                sb3.append(s);
                                Log.w("MBServiceCompat", sb3.toString());
                                ServiceBinderImpl.this.this$0.mConnections.remove(binder);
                            }
                        }
                    }
                });
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Package/uid mismatch: uid=");
            sb.append(n2);
            sb.append(" package=");
            sb.append(s);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public void disconnect(final ServiceCallbacks serviceCallbacks) {
            this.this$0.mHandler.postOrRun(new Runnable() {
                @Override
                public void run() {
                    final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.remove(serviceCallbacks.asBinder());
                    if (connectionRecord != null) {
                        connectionRecord.callbacks.asBinder().unlinkToDeath((IBinder$DeathRecipient)connectionRecord, 0);
                    }
                }
            });
        }
        
        public void getMediaItem(final String s, final ResultReceiver resultReceiver, final ServiceCallbacks serviceCallbacks) {
            if (!TextUtils.isEmpty((CharSequence)s) && resultReceiver != null) {
                this.this$0.mHandler.postOrRun(new Runnable() {
                    @Override
                    public void run() {
                        final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.get(serviceCallbacks.asBinder());
                        if (connectionRecord == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("getMediaItem for callback that isn't registered id=");
                            sb.append(s);
                            Log.w("MBServiceCompat", sb.toString());
                            return;
                        }
                        ServiceBinderImpl.this.this$0.performLoadItem(s, connectionRecord, resultReceiver);
                    }
                });
            }
        }
        
        public void registerCallbacks(final ServiceCallbacks serviceCallbacks, final String s, final int n, final int n2, final Bundle bundle) {
            this.this$0.mHandler.postOrRun(new Runnable() {
                @Override
                public void run() {
                    final IBinder binder = serviceCallbacks.asBinder();
                    ServiceBinderImpl.this.this$0.mConnections.remove(binder);
                    final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.new ConnectionRecord(s, n, n2, bundle, serviceCallbacks);
                    ServiceBinderImpl.this.this$0.mConnections.put(binder, connectionRecord);
                    try {
                        binder.linkToDeath((IBinder$DeathRecipient)connectionRecord, 0);
                    }
                    catch (RemoteException ex) {
                        Log.w("MBServiceCompat", "IBinder is already dead.");
                    }
                }
            });
        }
        
        public void removeSubscription(final String s, final IBinder binder, final ServiceCallbacks serviceCallbacks) {
            this.this$0.mHandler.postOrRun(new Runnable() {
                @Override
                public void run() {
                    final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.get(serviceCallbacks.asBinder());
                    if (connectionRecord == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("removeSubscription for callback that isn't registered id=");
                        sb.append(s);
                        Log.w("MBServiceCompat", sb.toString());
                        return;
                    }
                    if (!ServiceBinderImpl.this.this$0.removeSubscription(s, connectionRecord, binder)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("removeSubscription called for ");
                        sb2.append(s);
                        sb2.append(" which is not subscribed");
                        Log.w("MBServiceCompat", sb2.toString());
                    }
                }
            });
        }
        
        public void search(final String s, final Bundle bundle, final ResultReceiver resultReceiver, final ServiceCallbacks serviceCallbacks) {
            if (!TextUtils.isEmpty((CharSequence)s) && resultReceiver != null) {
                this.this$0.mHandler.postOrRun(new Runnable() {
                    @Override
                    public void run() {
                        final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.get(serviceCallbacks.asBinder());
                        if (connectionRecord == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("search for callback that isn't registered query=");
                            sb.append(s);
                            Log.w("MBServiceCompat", sb.toString());
                            return;
                        }
                        ServiceBinderImpl.this.this$0.performSearch(s, bundle, connectionRecord, resultReceiver);
                    }
                });
            }
        }
        
        public void sendCustomAction(final String s, final Bundle bundle, final ResultReceiver resultReceiver, final ServiceCallbacks serviceCallbacks) {
            if (!TextUtils.isEmpty((CharSequence)s) && resultReceiver != null) {
                this.this$0.mHandler.postOrRun(new Runnable() {
                    @Override
                    public void run() {
                        final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.get(serviceCallbacks.asBinder());
                        if (connectionRecord == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("sendCustomAction for callback that isn't registered action=");
                            sb.append(s);
                            sb.append(", extras=");
                            sb.append(bundle);
                            Log.w("MBServiceCompat", sb.toString());
                            return;
                        }
                        ServiceBinderImpl.this.this$0.performCustomAction(s, bundle, connectionRecord, resultReceiver);
                    }
                });
            }
        }
        
        public void unregisterCallbacks(final ServiceCallbacks serviceCallbacks) {
            this.this$0.mHandler.postOrRun(new Runnable() {
                @Override
                public void run() {
                    final IBinder binder = serviceCallbacks.asBinder();
                    final ConnectionRecord connectionRecord = ServiceBinderImpl.this.this$0.mConnections.remove(binder);
                    if (connectionRecord != null) {
                        binder.unlinkToDeath((IBinder$DeathRecipient)connectionRecord, 0);
                    }
                }
            });
        }
    }
    
    private interface ServiceCallbacks
    {
        IBinder asBinder();
        
        void onConnect(final String p0, final MediaSessionCompat.Token p1, final Bundle p2) throws RemoteException;
        
        void onConnectFailed() throws RemoteException;
        
        void onLoadChildren(final String p0, final List<MediaBrowserCompat.MediaItem> p1, final Bundle p2, final Bundle p3) throws RemoteException;
    }
    
    private static class ServiceCallbacksCompat implements ServiceCallbacks
    {
        final Messenger mCallbacks;
        
        ServiceCallbacksCompat(final Messenger mCallbacks) {
            this.mCallbacks = mCallbacks;
        }
        
        private void sendRequest(final int what, final Bundle data) throws RemoteException {
            final Message obtain = Message.obtain();
            obtain.what = what;
            obtain.arg1 = 2;
            obtain.setData(data);
            this.mCallbacks.send(obtain);
        }
        
        @Override
        public IBinder asBinder() {
            return this.mCallbacks.getBinder();
        }
        
        @Override
        public void onConnect(final String s, final MediaSessionCompat.Token token, final Bundle bundle) throws RemoteException {
            Bundle bundle2 = bundle;
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            bundle2.putInt("extra_service_version", 2);
            final Bundle bundle3 = new Bundle();
            bundle3.putString("data_media_item_id", s);
            bundle3.putParcelable("data_media_session_token", (Parcelable)token);
            bundle3.putBundle("data_root_hints", bundle2);
            this.sendRequest(1, bundle3);
        }
        
        @Override
        public void onConnectFailed() throws RemoteException {
            this.sendRequest(2, null);
        }
        
        @Override
        public void onLoadChildren(final String s, final List<MediaBrowserCompat.MediaItem> list, final Bundle bundle, final Bundle bundle2) throws RemoteException {
            final Bundle bundle3 = new Bundle();
            bundle3.putString("data_media_item_id", s);
            bundle3.putBundle("data_options", bundle);
            bundle3.putBundle("data_notify_children_changed_options", bundle2);
            if (list != null) {
                ArrayList list2;
                if (list instanceof ArrayList) {
                    list2 = (ArrayList<? extends E>)list;
                }
                else {
                    list2 = new ArrayList((Collection<? extends E>)list);
                }
                bundle3.putParcelableArrayList("data_media_item_list", list2);
            }
            this.sendRequest(3, bundle3);
        }
    }
    
    private final class ServiceHandler extends Handler
    {
        private final ServiceBinderImpl mServiceBinderImpl;
        
        public void handleMessage(final Message message) {
            final Bundle data = message.getData();
            switch (message.what) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unhandled message: ");
                    sb.append(message);
                    sb.append("\n  Service version: ");
                    sb.append(2);
                    sb.append("\n  Client version: ");
                    sb.append(message.arg1);
                    Log.w("MBServiceCompat", sb.toString());
                    break;
                }
                case 9: {
                    this.mServiceBinderImpl.sendCustomAction(data.getString("data_custom_action"), data.getBundle("data_custom_action_extras"), (ResultReceiver)data.getParcelable("data_result_receiver"), new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 8: {
                    this.mServiceBinderImpl.search(data.getString("data_search_query"), data.getBundle("data_search_extras"), (ResultReceiver)data.getParcelable("data_result_receiver"), new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 7: {
                    this.mServiceBinderImpl.unregisterCallbacks(new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 6: {
                    this.mServiceBinderImpl.registerCallbacks(new ServiceCallbacksCompat(message.replyTo), data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), data.getBundle("data_root_hints"));
                    break;
                }
                case 5: {
                    this.mServiceBinderImpl.getMediaItem(data.getString("data_media_item_id"), (ResultReceiver)data.getParcelable("data_result_receiver"), new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 4: {
                    this.mServiceBinderImpl.removeSubscription(data.getString("data_media_item_id"), BundleCompat.getBinder(data, "data_callback_token"), new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 3: {
                    this.mServiceBinderImpl.addSubscription(data.getString("data_media_item_id"), BundleCompat.getBinder(data, "data_callback_token"), data.getBundle("data_options"), new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 2: {
                    this.mServiceBinderImpl.disconnect(new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
                case 1: {
                    this.mServiceBinderImpl.connect(data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), data.getBundle("data_root_hints"), new ServiceCallbacksCompat(message.replyTo));
                    break;
                }
            }
        }
        
        public void postOrRun(final Runnable runnable) {
            if (Thread.currentThread() == this.getLooper().getThread()) {
                runnable.run();
            }
            else {
                this.post(runnable);
            }
        }
        
        public boolean sendMessageAtTime(final Message message, final long n) {
            final Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            data.putInt("data_calling_pid", Binder.getCallingPid());
            return super.sendMessageAtTime(message, n);
        }
    }
}
