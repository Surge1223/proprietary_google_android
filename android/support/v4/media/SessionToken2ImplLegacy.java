package android.support.v4.media;

import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;

final class SessionToken2ImplLegacy implements SupportLibraryImpl
{
    private final MediaSessionCompat.Token mLegacyToken;
    
    SessionToken2ImplLegacy(final MediaSessionCompat.Token mLegacyToken) {
        this.mLegacyToken = mLegacyToken;
    }
    
    public static SessionToken2ImplLegacy fromBundle(final Bundle bundle) {
        return new SessionToken2ImplLegacy(MediaSessionCompat.Token.fromBundle(bundle.getBundle("android.media.token.LEGACY")));
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SessionToken2ImplLegacy && this.mLegacyToken.equals(((SessionToken2ImplLegacy)o).mLegacyToken);
    }
    
    @Override
    public int hashCode() {
        return this.mLegacyToken.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SessionToken2 {legacyToken=");
        sb.append(this.mLegacyToken);
        sb.append("}");
        return sb.toString();
    }
}
