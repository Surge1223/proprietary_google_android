package android.arch.lifecycle;

public interface GeneratedAdapter
{
    void callMethods(final LifecycleOwner p0, final Lifecycle.Event p1, final boolean p2, final MethodCallsLogger p3);
}
