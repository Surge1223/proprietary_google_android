package android.arch.lifecycle;

class FullLifecycleObserverAdapter implements GenericLifecycleObserver
{
    private final FullLifecycleObserver mObserver;
    
    FullLifecycleObserverAdapter(final FullLifecycleObserver mObserver) {
        this.mObserver = mObserver;
    }
    
    @Override
    public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        switch (event) {
            case ON_ANY: {
                throw new IllegalArgumentException("ON_ANY must not been send by anybody");
            }
            case ON_DESTROY: {
                this.mObserver.onDestroy(lifecycleOwner);
                break;
            }
            case ON_STOP: {
                this.mObserver.onStop(lifecycleOwner);
                break;
            }
            case ON_PAUSE: {
                this.mObserver.onPause(lifecycleOwner);
                break;
            }
            case ON_RESUME: {
                this.mObserver.onResume(lifecycleOwner);
                break;
            }
            case ON_START: {
                this.mObserver.onStart(lifecycleOwner);
                break;
            }
            case ON_CREATE: {
                this.mObserver.onCreate(lifecycleOwner);
                break;
            }
        }
    }
}
