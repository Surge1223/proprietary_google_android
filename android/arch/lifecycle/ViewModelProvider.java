package android.arch.lifecycle;

public class ViewModelProvider
{
    private final Factory mFactory;
    private final ViewModelStore mViewModelStore;
    
    public ViewModelProvider(final ViewModelStore mViewModelStore, final Factory mFactory) {
        this.mFactory = mFactory;
        this.mViewModelStore = mViewModelStore;
    }
    
    public <T extends ViewModel> T get(final Class<T> clazz) {
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("android.arch.lifecycle.ViewModelProvider.DefaultKey:");
            sb.append(canonicalName);
            return this.get(sb.toString(), clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    public <T extends ViewModel> T get(final String s, final Class<T> clazz) {
        final ViewModel value = this.mViewModelStore.get(s);
        if (clazz.isInstance(value)) {
            return (T)value;
        }
        final ViewModel create = this.mFactory.create(clazz);
        this.mViewModelStore.put(s, create);
        return (T)create;
    }
    
    public interface Factory
    {
         <T extends ViewModel> T create(final Class<T> p0);
    }
}
