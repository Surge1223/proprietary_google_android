package android.arch.lifecycle;

import android.os.Bundle;
import android.app.Activity;
import java.util.concurrent.atomic.AtomicBoolean;

class LifecycleDispatcher
{
    private static AtomicBoolean sInitialized;
    
    static {
        LifecycleDispatcher.sInitialized = new AtomicBoolean(false);
    }
    
    static class DispatcherActivityCallback extends EmptyActivityLifecycleCallbacks
    {
        @Override
        public void onActivityCreated(final Activity activity, final Bundle bundle) {
            ReportFragment.injectIfNeededIn(activity);
        }
        
        @Override
        public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
        }
        
        @Override
        public void onActivityStopped(final Activity activity) {
        }
    }
}
