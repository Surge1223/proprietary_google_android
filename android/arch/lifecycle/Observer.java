package android.arch.lifecycle;

public interface Observer<T>
{
    void onChanged(final T p0);
}
