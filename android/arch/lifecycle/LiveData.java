package android.arch.lifecycle;

import java.util.Iterator;
import java.util.Map;
import android.arch.core.executor.ArchTaskExecutor;
import android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$ObserverWrapper;
import android.arch.core.internal.SafeIterableMap;

public abstract class LiveData<T>
{
    private static final Object NOT_SET;
    private int mActiveCount;
    private volatile Object mData;
    private final Object mDataLock;
    private boolean mDispatchInvalidated;
    private boolean mDispatchingValue;
    private SafeIterableMap<Observer<? super T>, LiveData$ObserverWrapper> mObservers;
    private volatile Object mPendingData;
    private final Runnable mPostValueRunnable;
    private int mVersion;
    
    static {
        NOT_SET = new Object();
    }
    
    public LiveData() {
        this.mDataLock = new Object();
        this.mObservers = new SafeIterableMap<Observer<? super T>, LiveData$ObserverWrapper>();
        this.mActiveCount = 0;
        this.mData = LiveData.NOT_SET;
        this.mPendingData = LiveData.NOT_SET;
        this.mVersion = -1;
        this.mPostValueRunnable = new Runnable() {
            @Override
            public void run() {
                synchronized (LiveData.this.mDataLock) {
                    final Object access$100 = LiveData.this.mPendingData;
                    LiveData.this.mPendingData = LiveData.NOT_SET;
                    // monitorexit(LiveData.access$000(this.this$0))
                    LiveData.this.setValue(access$100);
                }
            }
        };
    }
    
    static /* synthetic */ void access$400(final LiveData liveData, final ObserverWrapper observerWrapper) {
        liveData.dispatchingValue(observerWrapper);
    }
    
    private static void assertMainThread(final String s) {
        if (ArchTaskExecutor.getInstance().isMainThread()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot invoke ");
        sb.append(s);
        sb.append(" on a background");
        sb.append(" thread");
        throw new IllegalStateException(sb.toString());
    }
    
    private void considerNotify(final android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$ObserverWrapper liveData$ObserverWrapper) {
        if (!((ObserverWrapper)liveData$ObserverWrapper).mActive) {
            return;
        }
        if (!((ObserverWrapper)liveData$ObserverWrapper).shouldBeActive()) {
            ((ObserverWrapper)liveData$ObserverWrapper).activeStateChanged(false);
            return;
        }
        if (((ObserverWrapper)liveData$ObserverWrapper).mLastVersion >= this.mVersion) {
            return;
        }
        ((ObserverWrapper)liveData$ObserverWrapper).mLastVersion = this.mVersion;
        ((ObserverWrapper)liveData$ObserverWrapper).mObserver.onChanged((Object)this.mData);
    }
    
    private void dispatchingValue(android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$ObserverWrapper liveData$ObserverWrapper) {
        if (this.mDispatchingValue) {
            this.mDispatchInvalidated = true;
            return;
        }
        this.mDispatchingValue = true;
        do {
            this.mDispatchInvalidated = false;
            android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$android.arch.lifecycle.LiveData$ObserverWrapper liveData$ObserverWrapper2 = null;
            Label_0086: {
                if (liveData$ObserverWrapper != null) {
                    this.considerNotify((ObserverWrapper)liveData$ObserverWrapper);
                    liveData$ObserverWrapper2 = null;
                }
                else {
                    final SafeIterableMap.IteratorWithAdditions iteratorWithAdditions = this.mObservers.iteratorWithAdditions();
                    do {
                        liveData$ObserverWrapper2 = liveData$ObserverWrapper;
                        if (!iteratorWithAdditions.hasNext()) {
                            break Label_0086;
                        }
                        this.considerNotify((ObserverWrapper)((Iterator<Map.Entry<K, ObserverWrapper>>)iteratorWithAdditions).next().getValue());
                    } while (!this.mDispatchInvalidated);
                    liveData$ObserverWrapper2 = liveData$ObserverWrapper;
                }
            }
            liveData$ObserverWrapper = liveData$ObserverWrapper2;
        } while (this.mDispatchInvalidated);
        this.mDispatchingValue = false;
    }
    
    public T getValue() {
        final Object mData = this.mData;
        if (mData != LiveData.NOT_SET) {
            return (T)mData;
        }
        return null;
    }
    
    public boolean hasActiveObservers() {
        return this.mActiveCount > 0;
    }
    
    public void observe(final LifecycleOwner lifecycleOwner, final Observer<? super T> observer) {
        assertMainThread("observe");
        if (lifecycleOwner.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
            return;
        }
        final LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(lifecycleOwner, observer);
        final ObserverWrapper observerWrapper = (ObserverWrapper)this.mObservers.putIfAbsent(observer, lifecycleBoundObserver);
        if (observerWrapper != null && !observerWrapper.isAttachedTo(lifecycleOwner)) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (observerWrapper != null) {
            return;
        }
        lifecycleOwner.getLifecycle().addObserver(lifecycleBoundObserver);
    }
    
    protected void onActive() {
    }
    
    protected void onInactive() {
    }
    
    public void removeObserver(final Observer<? super T> observer) {
        assertMainThread("removeObserver");
        final ObserverWrapper observerWrapper = (ObserverWrapper)this.mObservers.remove(observer);
        if (observerWrapper == null) {
            return;
        }
        observerWrapper.detachObserver();
        observerWrapper.activeStateChanged(false);
    }
    
    protected void setValue(final T mData) {
        assertMainThread("setValue");
        ++this.mVersion;
        this.mData = mData;
        this.dispatchingValue((ObserverWrapper)null);
    }
    
    class LifecycleBoundObserver extends LiveData$ObserverWrapper implements GenericLifecycleObserver
    {
        final LifecycleOwner mOwner;
        
        LifecycleBoundObserver(final LifecycleOwner mOwner, final Observer<? super T> observer) {
            super(observer);
            this.mOwner = mOwner;
        }
        
        void detachObserver() {
            this.mOwner.getLifecycle().removeObserver(this);
        }
        
        boolean isAttachedTo(final LifecycleOwner lifecycleOwner) {
            return this.mOwner == lifecycleOwner;
        }
        
        public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
            if (this.mOwner.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
                LiveData.this.removeObserver(this.mObserver);
                return;
            }
            this.activeStateChanged(this.shouldBeActive());
        }
        
        boolean shouldBeActive() {
            return this.mOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED);
        }
    }
    
    private abstract class ObserverWrapper
    {
        boolean mActive;
        int mLastVersion;
        final Observer<? super T> mObserver;
        
        ObserverWrapper(final Observer<? super T> mObserver) {
            this.mLastVersion = -1;
            this.mObserver = mObserver;
        }
        
        void activeStateChanged(final boolean mActive) {
            if (mActive == this.mActive) {
                return;
            }
            this.mActive = mActive;
            final int access$300 = LiveData.this.mActiveCount;
            int n = 1;
            final boolean b = access$300 == 0;
            final LiveData this$0 = LiveData.this;
            final int access$301 = this$0.mActiveCount;
            if (!this.mActive) {
                n = -1;
            }
            this$0.mActiveCount = access$301 + n;
            if (b && this.mActive) {
                LiveData.this.onActive();
            }
            if (LiveData.this.mActiveCount == 0 && !this.mActive) {
                LiveData.this.onInactive();
            }
            if (this.mActive) {
                LiveData.access$400(LiveData.this, this);
            }
        }
        
        void detachObserver() {
        }
        
        boolean isAttachedTo(final LifecycleOwner lifecycleOwner) {
            return false;
        }
        
        abstract boolean shouldBeActive();
    }
}
