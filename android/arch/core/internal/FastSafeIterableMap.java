package android.arch.core.internal;

import java.util.Map;
import java.util.HashMap;

public class FastSafeIterableMap<K, V> extends SafeIterableMap<K, V>
{
    private HashMap<K, Entry<K, V>> mHashMap;
    
    public FastSafeIterableMap() {
        this.mHashMap = new HashMap<K, Entry<K, V>>();
    }
    
    public Map.Entry<K, V> ceil(final K k) {
        if (this.contains(k)) {
            return this.mHashMap.get(k).mPrevious;
        }
        return null;
    }
    
    public boolean contains(final K k) {
        return this.mHashMap.containsKey(k);
    }
    
    @Override
    protected Entry<K, V> get(final K k) {
        return this.mHashMap.get(k);
    }
    
    @Override
    public V putIfAbsent(final K k, final V v) {
        final Entry<K, V> value = this.get(k);
        if (value != null) {
            return value.mValue;
        }
        this.mHashMap.put(k, this.put(k, v));
        return null;
    }
    
    @Override
    public V remove(final K k) {
        final V remove = super.remove(k);
        this.mHashMap.remove(k);
        return remove;
    }
}
