package android.arch.core.executor;

public abstract class TaskExecutor
{
    public abstract void executeOnDiskIO(final Runnable p0);
    
    public abstract boolean isMainThread();
    
    public abstract void postToMainThread(final Runnable p0);
}
