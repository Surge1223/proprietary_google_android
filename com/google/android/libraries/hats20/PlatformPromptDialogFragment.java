package com.google.android.libraries.hats20;

import android.view.View;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.libraries.hats20.answer.AnswerBeacon;
import com.google.android.libraries.hats20.model.SurveyController;
import android.app.DialogFragment;

public final class PlatformPromptDialogFragment extends DialogFragment implements DialogFragmentInterface
{
    private final PromptDialogDelegate delegate;
    
    public PlatformPromptDialogFragment() {
        this.delegate = new PromptDialogDelegate((PromptDialogDelegate.DialogFragmentInterface)this);
    }
    
    public static PlatformPromptDialogFragment newInstance(final String s, final SurveyController surveyController, final AnswerBeacon answerBeacon, final Integer n, final Integer n2, final boolean b) {
        final PlatformPromptDialogFragment platformPromptDialogFragment = new PlatformPromptDialogFragment();
        platformPromptDialogFragment.setArguments(PromptDialogDelegate.createArgs(s, surveyController, answerBeacon, n, n2, b));
        return platformPromptDialogFragment;
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        return this.delegate.onCreateView(layoutInflater, viewGroup, bundle);
    }
    
    public void onDestroy() {
        this.delegate.onDestroy();
        super.onDestroy();
    }
    
    public void onPause() {
        super.onPause();
        this.delegate.onPause();
    }
    
    public void onResume() {
        super.onResume();
        this.delegate.onResume();
    }
    
    public void onStart() {
        super.onStart();
        this.delegate.onStart();
    }
}
