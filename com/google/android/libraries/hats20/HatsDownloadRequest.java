package com.google.android.libraries.hats20;

import android.util.Log;
import android.net.Uri.Builder;
import android.net.Uri;
import android.content.Context;

public class HatsDownloadRequest
{
    private final String advertisingId;
    private final String baseDownloadUrl;
    private final Context context;
    private final String siteContext;
    private final String siteId;
    
    private HatsDownloadRequest(final Builder builder) {
        this.context = builder.context;
        this.siteContext = builder.siteContext;
        this.siteId = builder.siteId;
        this.advertisingId = builder.advertisingId;
        this.baseDownloadUrl = builder.baseDownloadUrl;
    }
    
    public static Builder builder(final Context context) {
        return new Builder(context);
    }
    
    Uri computeDownloadUri() {
        final Uri.Builder appendQueryParameter = Uri.parse(this.baseDownloadUrl).buildUpon().appendQueryParameter("lang", "EN").appendQueryParameter("site", this.siteId).appendQueryParameter("adid", this.advertisingId);
        if (this.siteContext != null) {
            appendQueryParameter.appendQueryParameter("sc", this.siteContext);
        }
        return appendQueryParameter.build();
    }
    
    Context getContext() {
        return this.context;
    }
    
    String getSiteId() {
        return this.siteId;
    }
    
    public static class Builder
    {
        private String advertisingId;
        private boolean alreadyBuilt;
        private String baseDownloadUrl;
        private final Context context;
        private String siteContext;
        private String siteId;
        
        private Builder(final Context context) {
            this.baseDownloadUrl = "https://clients4.google.com/insights/consumersurveys/gk/prompt";
            this.alreadyBuilt = false;
            if (context != null) {
                this.context = context;
                return;
            }
            throw new NullPointerException("Context was missing.");
        }
        
        public HatsDownloadRequest build() {
            if (this.alreadyBuilt) {
                throw new IllegalStateException("Cannot reuse Builder instance once instantiated");
            }
            this.alreadyBuilt = true;
            if (this.siteId == null) {
                Log.d("HatsLibDownloadRequest", "Site ID was not set, no survey will be downloaded.");
                this.siteId = "-1";
            }
            if (this.advertisingId != null) {
                return new HatsDownloadRequest(this, null);
            }
            throw new NullPointerException("Advertising ID was missing.");
        }
        
        public Builder forSiteId(final String siteId) {
            if (this.siteId != null) {
                throw new UnsupportedOperationException("Currently don't support multiple site IDs.");
            }
            if (siteId != null) {
                this.siteId = siteId;
                return this;
            }
            throw new NullPointerException("Site ID cannot be set to null.");
        }
        
        public Builder setBaseDownloadUrlForTesting(final String baseDownloadUrl) {
            if (baseDownloadUrl != null) {
                this.baseDownloadUrl = baseDownloadUrl;
                return this;
            }
            throw new NullPointerException("Base download URL was missing.");
        }
        
        public Builder withAdvertisingId(final String advertisingId) {
            if (advertisingId != null) {
                this.advertisingId = advertisingId;
                return this;
            }
            throw new NullPointerException("Advertising ID was missing.");
        }
        
        public Builder withSiteContext(final String siteContext) {
            if (siteContext != null) {
                if (siteContext.length() > 1000) {
                    Log.w("HatsLibDownloadRequest", "Site context was longer than 1000 chars, please trim it down.");
                }
                this.siteContext = siteContext;
                return this;
            }
            throw new NullPointerException("Site context was missing.");
        }
    }
}
