package com.google.android.libraries.hats20.model;

import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class QuestionOpenText extends Question
{
    public static final Parcelable.Creator<QuestionOpenText> CREATOR;
    private boolean singleLine;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<QuestionOpenText>() {
            public QuestionOpenText createFromParcel(final Parcel parcel) {
                return new QuestionOpenText(parcel, null);
            }
            
            public QuestionOpenText[] newArray(final int n) {
                return new QuestionOpenText[n];
            }
        };
    }
    
    private QuestionOpenText(final Parcel parcel) {
        this.singleLine = (parcel.readByte() != 0);
        this.questionText = parcel.readString();
    }
    
    QuestionOpenText(final JSONObject jsonObject) throws JSONException {
        this.questionText = jsonObject.optString("question");
        this.singleLine = jsonObject.optBoolean("single_line");
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public int getType() {
        return 3;
    }
    
    public boolean isSingleLine() {
        return this.singleLine;
    }
    
    @Override
    public String toString() {
        final String questionText = this.questionText;
        final boolean singleLine = this.singleLine;
        final StringBuilder sb = new StringBuilder(49 + String.valueOf(questionText).length());
        sb.append("QuestionOpenText{questionText=");
        sb.append(questionText);
        sb.append(", singleLine=");
        sb.append(singleLine);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeByte((byte)(byte)(this.singleLine ? 1 : 0));
        parcel.writeString(this.questionText);
    }
}
