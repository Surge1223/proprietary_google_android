package com.google.android.libraries.hats20.model;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import java.util.Collections;
import com.google.android.libraries.hats20.R;
import android.support.v4.util.ArrayMap;
import java.util.Map;
import android.os.Parcelable.Creator;

public class QuestionRating extends Question
{
    public static final Parcelable.Creator<QuestionRating> CREATOR;
    public static final Map<Integer, Integer> READONLY_SURVEY_RATING_ICON_RESOURCE_MAP;
    private final String highValueText;
    private final String lowValueText;
    private final int numIcons;
    private final Sprite sprite;
    
    static {
        final ArrayMap<Integer, Integer> arrayMap = new ArrayMap<Integer, Integer>();
        arrayMap.put(0, R.drawable.hats_smiley_1);
        arrayMap.put(1, R.drawable.hats_smiley_2);
        arrayMap.put(2, R.drawable.hats_smiley_3);
        arrayMap.put(3, R.drawable.hats_smiley_4);
        arrayMap.put(4, R.drawable.hats_smiley_5);
        READONLY_SURVEY_RATING_ICON_RESOURCE_MAP = Collections.unmodifiableMap((Map<?, ?>)arrayMap);
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<QuestionRating>() {
            public QuestionRating createFromParcel(final Parcel parcel) {
                return new QuestionRating(parcel, null);
            }
            
            public QuestionRating[] newArray(final int n) {
                return new QuestionRating[n];
            }
        };
    }
    
    private QuestionRating(final Parcel parcel) {
        this.lowValueText = parcel.readString();
        this.highValueText = parcel.readString();
        this.numIcons = parcel.readInt();
        this.questionText = parcel.readString();
        this.sprite = (Sprite)parcel.readSerializable();
    }
    
    QuestionRating(final JSONObject jsonObject) throws JSONException {
        this.questionText = jsonObject.optString("question");
        this.lowValueText = jsonObject.optString("low_value");
        this.highValueText = jsonObject.optString("high_value");
        this.numIcons = jsonObject.getInt("num_stars");
        Sprite sprite;
        if (this.numIcons == 5) {
            sprite = Sprite.SMILEYS;
        }
        else {
            sprite = Sprite.STARS;
        }
        this.sprite = sprite;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public String getHighValueText() {
        return this.highValueText;
    }
    
    public String getLowValueText() {
        return this.lowValueText;
    }
    
    public int getNumIcons() {
        return this.numIcons;
    }
    
    public Sprite getSprite() {
        return this.sprite;
    }
    
    @Override
    public int getType() {
        return 4;
    }
    
    @Override
    public String toString() {
        final String questionText = this.questionText;
        final String lowValueText = this.lowValueText;
        final String highValueText = this.highValueText;
        final int numIcons = this.numIcons;
        final String value = String.valueOf(this.sprite);
        final StringBuilder sb = new StringBuilder(97 + String.valueOf(questionText).length() + String.valueOf(lowValueText).length() + String.valueOf(highValueText).length() + String.valueOf(value).length());
        sb.append("QuestionRating{questionText='");
        sb.append(questionText);
        sb.append("'");
        sb.append(", lowValueText='");
        sb.append(lowValueText);
        sb.append("'");
        sb.append(", highValueText='");
        sb.append(highValueText);
        sb.append("'");
        sb.append(", numIcons=");
        sb.append(numIcons);
        sb.append(", sprite=");
        sb.append(value);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.lowValueText);
        parcel.writeString(this.highValueText);
        parcel.writeInt(this.numIcons);
        parcel.writeString(this.questionText);
        parcel.writeSerializable((Serializable)this.sprite);
    }
    
    public enum Sprite
    {
        SMILEYS, 
        STARS;
    }
}
