package com.google.android.libraries.hats20.model;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;
import android.os.Parcel;
import java.util.Collections;
import com.google.android.libraries.hats20.R;
import android.support.v4.util.ArrayMap;
import java.util.ArrayList;
import java.util.Map;
import android.os.Parcelable.Creator;

public class QuestionMultipleChoice extends Question implements QuestionWithSelectableAnswers
{
    public static final Parcelable.Creator<QuestionMultipleChoice> CREATOR;
    public static final Map<Integer, Integer> READONLY_SURVEY_RATING_ICON_RESOURCE_MAP;
    private ArrayList<String> answers;
    private ArrayList<Integer> ordering;
    private String spriteName;
    
    static {
        final ArrayMap<Integer, Integer> arrayMap = new ArrayMap<Integer, Integer>();
        arrayMap.put(0, R.drawable.hats_smiley_5);
        arrayMap.put(1, R.drawable.hats_smiley_4);
        arrayMap.put(2, R.drawable.hats_smiley_3);
        arrayMap.put(3, R.drawable.hats_smiley_2);
        arrayMap.put(4, R.drawable.hats_smiley_1);
        READONLY_SURVEY_RATING_ICON_RESOURCE_MAP = Collections.unmodifiableMap((Map<?, ?>)arrayMap);
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<QuestionMultipleChoice>() {
            public QuestionMultipleChoice createFromParcel(final Parcel parcel) {
                return new QuestionMultipleChoice(parcel, null);
            }
            
            public QuestionMultipleChoice[] newArray(final int n) {
                return new QuestionMultipleChoice[n];
            }
        };
    }
    
    private QuestionMultipleChoice(final Parcel parcel) {
        this.answers = new ArrayList<String>();
        this.ordering = new ArrayList<Integer>();
        parcel.readStringList((List)this.answers);
        parcel.readList((List)this.ordering, Integer.class.getClassLoader());
        this.questionText = parcel.readString();
        this.spriteName = parcel.readString();
    }
    
    QuestionMultipleChoice(final JSONObject jsonObject) throws JSONException {
        this.answers = new ArrayList<String>();
        this.ordering = new ArrayList<Integer>();
        this.questionText = jsonObject.optString("question");
        final JSONArray emptyArrayIfNull = Question.toEmptyArrayIfNull(jsonObject.optJSONArray("ordering"));
        final JSONArray emptyArrayIfNull2 = Question.toEmptyArrayIfNull(jsonObject.optJSONArray("answers"));
        final int n = 0;
        for (int i = 0; i < emptyArrayIfNull2.length(); ++i) {
            this.answers.add(emptyArrayIfNull2.getString(i));
        }
        for (int j = n; j < emptyArrayIfNull.length(); ++j) {
            this.ordering.add(emptyArrayIfNull.getInt(j));
        }
        this.spriteName = jsonObject.optString("sprite_name");
    }
    
    public int describeContents() {
        return 0;
    }
    
    public ArrayList<String> getAnswers() {
        return this.answers;
    }
    
    public ArrayList<Integer> getOrdering() {
        return this.ordering;
    }
    
    public String getSpriteName() {
        return this.spriteName;
    }
    
    @Override
    public int getType() {
        return 1;
    }
    
    @Override
    public String toString() {
        final String questionText = this.questionText;
        final String value = String.valueOf(this.answers);
        final String value2 = String.valueOf(this.ordering);
        final String spriteName = this.spriteName;
        final StringBuilder sb = new StringBuilder(71 + String.valueOf(questionText).length() + String.valueOf(value).length() + String.valueOf(value2).length() + String.valueOf(spriteName).length());
        sb.append("QuestionMultipleChoice{questionText=");
        sb.append(questionText);
        sb.append(", answers=");
        sb.append(value);
        sb.append(", ordering=");
        sb.append(value2);
        sb.append(", spriteName=");
        sb.append(spriteName);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeStringList((List)this.answers);
        parcel.writeList((List)this.ordering);
        parcel.writeString(this.questionText);
        parcel.writeString(this.spriteName);
    }
}
