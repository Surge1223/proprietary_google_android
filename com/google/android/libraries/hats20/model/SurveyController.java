package com.google.android.libraries.hats20.model;

import com.google.android.libraries.hats20.R;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.res.Resources;
import java.util.List;
import android.text.TextUtils;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class SurveyController implements Parcelable
{
    public static final Parcelable.Creator<SurveyController> CREATOR;
    private String answerUrl;
    private String promptMessage;
    private String promptParams;
    private Question[] questions;
    private boolean showInvitation;
    private String thankYouMessage;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<SurveyController>() {
            public SurveyController createFromParcel(final Parcel parcel) {
                return new SurveyController(parcel, null);
            }
            
            public SurveyController[] newArray(final int n) {
                return new SurveyController[n];
            }
        };
    }
    
    private SurveyController() {
        this.showInvitation = true;
    }
    
    private SurveyController(final Parcel parcel) {
        boolean showInvitation = true;
        this.showInvitation = true;
        final byte byte1 = parcel.readByte();
        int i = 0;
        if (byte1 == 0) {
            showInvitation = false;
        }
        this.showInvitation = showInvitation;
        final int int1 = parcel.readInt();
        this.questions = new Question[int1];
        while (i < int1) {
            this.questions[i] = (Question)parcel.readParcelable(Question.class.getClassLoader());
            ++i;
        }
        this.promptMessage = parcel.readString();
        this.thankYouMessage = parcel.readString();
        this.promptParams = parcel.readString();
        this.answerUrl = parcel.readString();
    }
    
    private static void assertSurveyIsValid(final SurveyController surveyController) throws MalformedSurveyException {
        if (surveyController.getQuestions().length == 0) {
            throw new MalformedSurveyException("Survey has no questions.");
        }
        if (TextUtils.isEmpty((CharSequence)surveyController.getAnswerUrl())) {
            throw new MalformedSurveyException("Survey did not have an AnswerUrl, this is a GCS issue.");
        }
        if (!TextUtils.isEmpty((CharSequence)surveyController.getPromptParams())) {
            for (int i = 0; i < surveyController.getQuestions().length; ++i) {
                final Question question = surveyController.questions[i];
                if (TextUtils.isEmpty((CharSequence)question.questionText)) {
                    final StringBuilder sb = new StringBuilder(43);
                    sb.append("Question #");
                    sb.append(i + 1);
                    sb.append(" had no question text.");
                    throw new MalformedSurveyException(sb.toString());
                }
                if (question instanceof Question.QuestionWithSelectableAnswers) {
                    final List<String> answers = ((Question.QuestionWithSelectableAnswers)question).getAnswers();
                    final List<Integer> ordering = ((Question.QuestionWithSelectableAnswers)question).getOrdering();
                    if (answers.isEmpty()) {
                        final StringBuilder sb2 = new StringBuilder(42);
                        sb2.append("Question #");
                        sb2.append(i + 1);
                        sb2.append(" was missing answers.");
                        throw new MalformedSurveyException(sb2.toString());
                    }
                    if (ordering.isEmpty()) {
                        final StringBuilder sb3 = new StringBuilder(74);
                        sb3.append("Question #");
                        sb3.append(i + 1);
                        sb3.append(" was missing an ordering, this likely is a GCS issue.");
                        throw new MalformedSurveyException(sb3.toString());
                    }
                }
                if (question.getType() == 4) {
                    final QuestionRating questionRating = (QuestionRating)question;
                    if (TextUtils.isEmpty((CharSequence)questionRating.getLowValueText()) || TextUtils.isEmpty((CharSequence)questionRating.getHighValueText())) {
                        throw new MalformedSurveyException("A rating question was missing its high/low text.");
                    }
                    if (questionRating.getSprite() == QuestionRating.Sprite.SMILEYS && questionRating.getNumIcons() != 5) {
                        throw new MalformedSurveyException("Smiley surveys must have 5 options.");
                    }
                    final QuestionRating.Sprite sprite = questionRating.getSprite();
                    if (sprite != QuestionRating.Sprite.STARS) {
                        if (sprite != QuestionRating.Sprite.SMILEYS) {
                            final String value = String.valueOf(sprite);
                            final StringBuilder sb4 = new StringBuilder(40 + String.valueOf(value).length());
                            sb4.append("Rating question has unsupported sprite: ");
                            sb4.append(value);
                            throw new MalformedSurveyException(sb4.toString());
                        }
                    }
                }
            }
            return;
        }
        throw new MalformedSurveyException("Survey did not have prompt params, this is a GCS issue.");
    }
    
    public static SurveyController initWithSurveyFromJson(final String s, final Resources resources) throws JSONException, MalformedSurveyException {
        final JSONObject jsonObject = new JSONObject(s).getJSONObject("params");
        final SurveyController surveyController = new SurveyController();
        retrieveTagDataFromJson(surveyController, jsonObject.getJSONArray("tags"), resources);
        surveyController.questions = Question.getQuestionsFromSurveyDefinition(jsonObject);
        surveyController.promptParams = jsonObject.optString("promptParams");
        surveyController.answerUrl = jsonObject.optString("answerUrl");
        assertSurveyIsValid(surveyController);
        return surveyController;
    }
    
    private static void retrieveTagDataFromJson(final SurveyController surveyController, final JSONArray jsonArray, final Resources resources) throws JSONException {
        for (int i = 0; i < jsonArray.length(); ++i) {
            final String[] split = jsonArray.getString(i).split("=");
            final int length = split.length;
            int n = 2;
            if (length != 2) {
                Log.e("HatsLibSurveyController", String.format("Tag couldn't be split: %s", jsonArray.getString(i)));
            }
            else {
                final String s = split[0];
                final String s2 = split[1];
                Label_0266: {
                    switch (s.hashCode()) {
                        case -453401085: {
                            if (s.equals("promptMessage")) {
                                n = 1;
                                break Label_0266;
                            }
                            break;
                        }
                        case -1179592925: {
                            if (s.equals("hatsClient")) {
                                n = 4;
                                break Label_0266;
                            }
                            break;
                        }
                        case -1224386186: {
                            if (s.equals("hats20")) {
                                n = 5;
                                break Label_0266;
                            }
                            break;
                        }
                        case -1268779017: {
                            if (s.equals("format")) {
                                n = 3;
                                break Label_0266;
                            }
                            break;
                        }
                        case -1336354446: {
                            if (s.equals("thankYouMessage")) {
                                break Label_0266;
                            }
                            break;
                        }
                        case -1505536394: {
                            if (s.equals("showInvitation")) {
                                n = 0;
                                break Label_0266;
                            }
                            break;
                        }
                        case -1765207296: {
                            if (s.equals("hatsNoRateLimiting")) {
                                n = 6;
                                break Label_0266;
                            }
                            break;
                        }
                    }
                    n = -1;
                }
                switch (n) {
                    default: {
                        Log.w("HatsLibSurveyController", String.format("Skipping unknown tag '%s'", s));
                        break;
                    }
                    case 3:
                    case 4:
                    case 5:
                    case 6: {
                        break;
                    }
                    case 2: {
                        surveyController.thankYouMessage = s2;
                        break;
                    }
                    case 1: {
                        surveyController.promptMessage = s2;
                        break;
                    }
                    case 0: {
                        surveyController.showInvitation = Boolean.valueOf(s2);
                        break;
                    }
                }
            }
        }
        if (!surveyController.showInvitation && !TextUtils.isEmpty((CharSequence)surveyController.promptMessage)) {
            Log.w("HatsLibSurveyController", String.format("Survey is promptless but a prompt message was parsed: %s", surveyController.promptMessage));
        }
        if (surveyController.showInvitation && TextUtils.isEmpty((CharSequence)surveyController.promptMessage)) {
            surveyController.promptMessage = resources.getString(R.string.hats_lib_default_prompt_title);
        }
        if (TextUtils.isEmpty((CharSequence)surveyController.thankYouMessage)) {
            surveyController.thankYouMessage = resources.getString(R.string.hats_lib_default_thank_you);
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public String getAnswerUrl() {
        return this.answerUrl;
    }
    
    public String getPromptMessage() {
        return this.promptMessage;
    }
    
    public String getPromptParams() {
        return this.promptParams;
    }
    
    public Question[] getQuestions() {
        return this.questions;
    }
    
    public String getThankYouMessage() {
        return this.thankYouMessage;
    }
    
    public boolean shouldIncludeSurveyControls() {
        final int length = this.questions.length;
        boolean b = true;
        if (length == 1 && this.questions[0].getType() == 4) {
            if (((QuestionRating)this.questions[0]).getSprite() == QuestionRating.Sprite.SMILEYS) {
                b = false;
            }
            return b;
        }
        return true;
    }
    
    public boolean showInvitation() {
        return this.showInvitation;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeByte((byte)(byte)(this.showInvitation ? 1 : 0));
        parcel.writeInt(this.questions.length);
        final Question[] questions = this.questions;
        for (int length = questions.length, i = 0; i < length; ++i) {
            parcel.writeParcelable((Parcelable)questions[i], n);
        }
        parcel.writeString(this.promptMessage);
        parcel.writeString(this.thankYouMessage);
        parcel.writeString(this.promptParams);
        parcel.writeString(this.answerUrl);
    }
    
    public static class MalformedSurveyException extends Exception
    {
        public MalformedSurveyException(final String s) {
            super(s);
        }
    }
}
