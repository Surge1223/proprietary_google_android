package com.google.android.libraries.hats20.model;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;
import android.os.Parcel;
import java.util.ArrayList;
import android.os.Parcelable.Creator;

public class QuestionMultipleSelect extends Question implements QuestionWithSelectableAnswers
{
    public static final Parcelable.Creator<QuestionMultipleSelect> CREATOR;
    private ArrayList<String> answers;
    private ArrayList<Integer> ordering;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<QuestionMultipleSelect>() {
            public QuestionMultipleSelect createFromParcel(final Parcel parcel) {
                return new QuestionMultipleSelect(parcel, null);
            }
            
            public QuestionMultipleSelect[] newArray(final int n) {
                return new QuestionMultipleSelect[n];
            }
        };
    }
    
    private QuestionMultipleSelect(final Parcel parcel) {
        this.answers = new ArrayList<String>();
        this.ordering = new ArrayList<Integer>();
        parcel.readStringList((List)this.answers);
        parcel.readList((List)this.ordering, Integer.class.getClassLoader());
        this.questionText = parcel.readString();
    }
    
    QuestionMultipleSelect(final JSONObject jsonObject) throws JSONException {
        this.answers = new ArrayList<String>();
        this.ordering = new ArrayList<Integer>();
        this.questionText = jsonObject.optString("question");
        final JSONArray emptyArrayIfNull = Question.toEmptyArrayIfNull(jsonObject.optJSONArray("ordering"));
        final JSONArray emptyArrayIfNull2 = Question.toEmptyArrayIfNull(jsonObject.optJSONArray("answers"));
        final int n = 0;
        for (int i = 0; i < emptyArrayIfNull2.length(); ++i) {
            this.answers.add(emptyArrayIfNull2.getString(i));
        }
        for (int j = n; j < emptyArrayIfNull.length(); ++j) {
            this.ordering.add(emptyArrayIfNull.getInt(j));
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public ArrayList<String> getAnswers() {
        return this.answers;
    }
    
    public ArrayList<Integer> getOrdering() {
        return this.ordering;
    }
    
    @Override
    public int getType() {
        return 2;
    }
    
    @Override
    public String toString() {
        final String questionText = this.questionText;
        final String value = String.valueOf(this.answers);
        final String value2 = String.valueOf(this.ordering);
        final StringBuilder sb = new StringBuilder(58 + String.valueOf(questionText).length() + String.valueOf(value).length() + String.valueOf(value2).length());
        sb.append("QuestionMultipleSelect{questionText=");
        sb.append(questionText);
        sb.append(", answers=");
        sb.append(value);
        sb.append(", ordering=");
        sb.append(value2);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeStringList((List)this.answers);
        parcel.writeList((List)this.ordering);
        parcel.writeString(this.questionText);
    }
}
