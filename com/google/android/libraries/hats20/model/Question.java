package com.google.android.libraries.hats20.model;

import java.util.List;
import org.json.JSONException;
import org.json.JSONArray;
import android.util.Log;
import java.util.Locale;
import org.json.JSONObject;
import java.util.Collections;
import android.support.v4.util.ArrayMap;
import java.util.Map;
import android.os.Parcelable;

public abstract class Question implements Parcelable
{
    private static final Map<String, Integer> READONLY_JSON_KEY_TO_QUESTION_TYPE;
    protected String questionText;
    
    static {
        final ArrayMap<String, Integer> arrayMap = new ArrayMap<String, Integer>();
        arrayMap.put("multi", 1);
        arrayMap.put("multi-select", 2);
        arrayMap.put("open-text", 3);
        arrayMap.put("rating", 4);
        READONLY_JSON_KEY_TO_QUESTION_TYPE = Collections.unmodifiableMap((Map<?, ?>)arrayMap);
    }
    
    private static int getQuestionTypeFromString(final String s) {
        final Integer n = Question.READONLY_JSON_KEY_TO_QUESTION_TYPE.get(s);
        if (n != null) {
            return n;
        }
        throw new IllegalArgumentException(String.format("Question string %s was not found in the json to QuestionType map", s));
    }
    
    public static Question[] getQuestionsFromSurveyDefinition(JSONObject jsonObject) throws JSONException {
        final JSONArray jsonArray = jsonObject.getJSONObject("payload").getJSONArray("longform_questions");
        final Question[] array = new Question[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); ++i) {
            jsonObject = jsonArray.getJSONObject(i);
            final String string = jsonObject.getString("type");
            Question question = null;
            switch (getQuestionTypeFromString(string)) {
                default: {
                    throw new UnsupportedOperationException(String.format("Attempted to deserialize an unsupported question type.  Unsupported type was: %s", string));
                }
                case 4: {
                    question = new QuestionRating(jsonObject);
                    break;
                }
                case 3: {
                    question = new QuestionOpenText(jsonObject);
                    break;
                }
                case 2: {
                    question = new QuestionMultipleSelect(jsonObject);
                    break;
                }
                case 1: {
                    question = new QuestionMultipleChoice(jsonObject);
                    break;
                }
            }
            array[i] = question;
            Log.d("HatsLibQuestionClass", String.format(Locale.US, "Parsed question %d of %d with content %s", i + 1, jsonArray.length(), question.toString()));
        }
        return array;
    }
    
    public static JSONArray toEmptyArrayIfNull(JSONArray jsonArray) {
        if (jsonArray == null) {
            jsonArray = new JSONArray();
        }
        return jsonArray;
    }
    
    public String getQuestionText() {
        return this.questionText;
    }
    
    public abstract int getType();
    
    public interface QuestionWithSelectableAnswers
    {
        List<String> getAnswers();
        
        List<Integer> getOrdering();
    }
}
