package com.google.android.libraries.hats20.storage;

import java.util.Iterator;
import java.util.HashSet;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.net.URI;
import java.util.Collections;
import android.util.Log;
import java.net.CookieManager;
import java.net.CookieHandler;
import java.util.Set;
import android.net.Uri;
import java.util.Map;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.content.SharedPreferences;

public class HatsDataStore
{
    static final int MAX_DAYS_UNTIL_EXPIRATION = 7;
    private static final long MILLIS_TO_CACHE_FAILED_DOWNLOAD;
    static final String SHARED_PREF_SET_COOKIE_URI = "SET_COOKIE_URI";
    static final String SHARED_PREF_SET_COOKIE_VALUE = "SET_COOKIE_VALUE";
    private final SharedPreferences sharedPreferences;
    
    static {
        MILLIS_TO_CACHE_FAILED_DOWNLOAD = TimeUnit.HOURS.toMillis(24L);
    }
    
    private HatsDataStore(final SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }
    
    public static HatsDataStore buildFromContext(final Context context) {
        return new HatsDataStore(getSharedPreferences(context.getApplicationContext()));
    }
    
    private static String getKeyForPrefSuffix(final String s, final String s2) {
        final StringBuilder sb = new StringBuilder(1 + String.valueOf(s).length() + String.valueOf(s2).length());
        sb.append(s);
        sb.append("_");
        sb.append(s2);
        return sb.toString();
    }
    
    static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences("com.google.android.libraries.hats20", 0);
    }
    
    private long getSurveyExpirationDate(final String s) {
        return this.sharedPreferences.getLong(getKeyForPrefSuffix(s, "EXPIRATION_DATE"), -1L);
    }
    
    private static boolean isSetCookieHeader(final Map.Entry<String, ?> entry) {
        return "Set-Cookie".equalsIgnoreCase(entry.getKey());
    }
    
    private void storeSetCookieHeaderValueSet(final Uri uri, final Set<String> set) {
        this.sharedPreferences.edit().putString("SET_COOKIE_URI", uri.toString()).putStringSet("SET_COOKIE_VALUE", (Set)set).apply();
    }
    
    public void forTestingClearAllData() {
        this.sharedPreferences.edit().clear().apply();
        final CookieHandler default1 = CookieHandler.getDefault();
        if (default1 instanceof CookieManager) {
            ((CookieManager)default1).getCookieStore().removeAll();
        }
        else {
            final String value = String.valueOf(((CookieManager)default1).getClass().getName());
            String concat;
            if (value.length() != 0) {
                concat = "Unknown cookie manager type, could not clear cookies: ".concat(value);
            }
            else {
                concat = new String("Unknown cookie manager type, could not clear cookies: ");
            }
            Log.e("HatsLibDataStore", concat);
        }
    }
    
    public void forTestingInjectSurveyIntoStorage(final String s, final String s2, final int n, final long n2) {
        this.sharedPreferences.edit().putInt(getKeyForPrefSuffix(s, "RESPONSE_CODE"), n).putLong(getKeyForPrefSuffix(s, "EXPIRATION_DATE"), n2).putString(getKeyForPrefSuffix(s, "CONTENT"), s2).apply();
    }
    
    public long getSurveyExpirationDate(final String s, final int n) {
        long surveyExpirationDate;
        if (n == this.sharedPreferences.getInt(getKeyForPrefSuffix(s, "RESPONSE_CODE"), -1)) {
            surveyExpirationDate = this.getSurveyExpirationDate(s);
        }
        else {
            surveyExpirationDate = -1L;
        }
        return surveyExpirationDate;
    }
    
    public String getSurveyJson(final String s) {
        return this.sharedPreferences.getString(getKeyForPrefSuffix(s, "CONTENT"), (String)null);
    }
    
    public void removeSurvey(final String s) {
        this.sharedPreferences.edit().remove(getKeyForPrefSuffix(s, "EXPIRATION_DATE")).remove(getKeyForPrefSuffix(s, "RESPONSE_CODE")).remove(getKeyForPrefSuffix(s, "CONTENT")).apply();
    }
    
    public void removeSurveyIfExpired(final String s) {
        final long surveyExpirationDate = this.getSurveyExpirationDate(s);
        if (surveyExpirationDate == -1L) {
            this.sharedPreferences.edit().remove(getKeyForPrefSuffix(s, "RESPONSE_CODE")).remove(getKeyForPrefSuffix(s, "CONTENT")).apply();
        }
        else if (surveyExpirationDate < System.currentTimeMillis() / 1000L) {
            this.removeSurvey(s);
        }
    }
    
    public void restoreCookiesFromPersistence() {
        if (CookieHandler.getDefault() == null) {
            Log.e("HatsLibDataStore", "Cannot restore cookies: Application does not have a cookie jar installed.");
            return;
        }
        final String string = this.sharedPreferences.getString("SET_COOKIE_URI", "");
        final Set stringSet = this.sharedPreferences.getStringSet("SET_COOKIE_VALUE", (Set)Collections.emptySet());
        if (!string.isEmpty() && !stringSet.isEmpty()) {
            try {
                CookieHandler.getDefault().put(new URI(string), (Map<String, List<String>>)Collections.singletonMap("Set-Cookie", new ArrayList<String>(stringSet)));
            }
            catch (URISyntaxException | IOException ex) {
                final Throwable t;
                Log.e("HatsLibDataStore", "Failed to restore cookies from persistence.", t);
            }
            return;
        }
        Log.d("HatsLibDataStore", "No cookies found in persistence.");
    }
    
    public void saveFailedDownload(final String s) {
        this.sharedPreferences.edit().putInt(getKeyForPrefSuffix(s, "RESPONSE_CODE"), 4).putLong(getKeyForPrefSuffix(s, "EXPIRATION_DATE"), (System.currentTimeMillis() + HatsDataStore.MILLIS_TO_CACHE_FAILED_DOWNLOAD) / 1000L).putString(getKeyForPrefSuffix(s, "CONTENT"), "").apply();
    }
    
    public void saveSuccessfulDownload(final int n, final long n2, final String s, final String s2) {
        int n3 = n;
        if (n != 0 && (n3 = n) != 1 && (n3 = n) != 2 && (n3 = n) != 3) {
            n3 = 5;
        }
        this.sharedPreferences.edit().putInt(getKeyForPrefSuffix(s2, "RESPONSE_CODE"), n3).putLong(getKeyForPrefSuffix(s2, "EXPIRATION_DATE"), Math.min((System.currentTimeMillis() + 604800000L) / 1000L, n2)).putString(getKeyForPrefSuffix(s2, "CONTENT"), s).apply();
    }
    
    public void storeSetCookieHeaders(final Uri uri, final Map<String, List<String>> map) {
        for (final Map.Entry<String, List<String>> entry : map.entrySet()) {
            if (isSetCookieHeader((Map.Entry<String, ?>)entry)) {
                this.storeSetCookieHeaderValueSet(uri, new HashSet<String>(entry.getValue()));
                break;
            }
        }
    }
    
    public boolean surveyExists(final String s) {
        final int int1 = this.sharedPreferences.getInt(getKeyForPrefSuffix(s, "RESPONSE_CODE"), -1);
        boolean b = false;
        if (int1 == -1) {
            Log.d("HatsLibDataStore", String.format("Checking if survey exists, Site ID %s was not in shared preferences.", s));
        }
        else {
            Log.d("HatsLibDataStore", String.format("Checking if survey exists, Site ID %s has response code %d in shared preferences.", s, int1));
        }
        if (int1 != -1) {
            b = true;
        }
        return b;
    }
    
    public boolean validSurveyExists(final String s) {
        final int int1 = this.sharedPreferences.getInt(getKeyForPrefSuffix(s, "RESPONSE_CODE"), -1);
        boolean b = false;
        if (int1 == -1) {
            Log.d("HatsLibDataStore", String.format("Checking for survey to show, Site ID %s was not in shared preferences.", s));
        }
        else {
            Log.d("HatsLibDataStore", String.format("Checking for survey to show, Site ID %s has response code %d in shared preferences.", s, int1));
        }
        if (int1 == 0) {
            b = true;
        }
        return b;
    }
}
