package com.google.android.libraries.hats20;

import android.view.WindowManager.LayoutParams;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup$MarginLayoutParams;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.content.res.Resources;
import android.content.Context;
import com.google.android.libraries.hats20.util.LayoutUtils;
import android.os.Build.VERSION;
import android.graphics.RectF;
import android.support.v7.widget.CardView;
import com.google.android.libraries.hats20.util.LayoutDimensions;
import android.app.Dialog;

final class DimensionConfigurationHelper
{
    private final boolean bottomSheet;
    private final int containerPadding;
    private final Dialog dialog;
    private final LayoutDimensions layoutDimensions;
    private final int maxPromptWidth;
    private final CardView promptCard;
    private final boolean twoLinePrompt;
    
    DimensionConfigurationHelper(final CardView promptCard, final Dialog dialog, final LayoutDimensions layoutDimensions, final boolean bottomSheet, final boolean twoLinePrompt) {
        this.promptCard = promptCard;
        this.dialog = dialog;
        this.layoutDimensions = layoutDimensions;
        this.bottomSheet = bottomSheet;
        this.twoLinePrompt = twoLinePrompt;
        this.containerPadding = this.getResources().getDimensionPixelSize(R.dimen.hats_lib_container_padding);
        this.maxPromptWidth = layoutDimensions.getPromptMaxWidth();
    }
    
    private RectF getBannerPadding(float n) {
        final float n2 = 0.0f;
        float n3 = 0.0f;
        final float n4 = 0.0f;
        final float n5 = 0.0f;
        float n6 = n2;
        float n7 = n4;
        float n8 = n5;
        if (this.dialog != null) {
            final boolean bottomSheet = this.bottomSheet;
            final boolean b = false;
            boolean b2 = false;
            if (bottomSheet) {
                if (Build.VERSION.SDK_INT < 21) {
                    b2 = true;
                }
                if (!b2) {
                    n = this.containerPadding;
                }
                n6 = n2;
                n3 = n;
                n7 = n4;
                n8 = n5;
            }
            else {
                boolean b3 = b;
                if (getPromptWidthPx(this.getContext(), this.maxPromptWidth) == LayoutUtils.getUsableContentDimensions(this.getContext()).x) {
                    b3 = true;
                }
                if (b3) {
                    n = this.getResources().getDimension(R.dimen.hats_lib_container_padding);
                }
                else {
                    n = this.getResources().getDimension(R.dimen.hats_lib_container_padding_left);
                }
                n3 = this.containerPadding;
                n7 = this.containerPadding;
                n8 = this.containerPadding;
                n6 = n;
            }
        }
        return new RectF(n6, n3, n7, n8);
    }
    
    private Context getContext() {
        return this.promptCard.getContext();
    }
    
    static int getPromptWidthPx(final Context context, final int n) {
        return Math.min(LayoutUtils.getUsableContentDimensions(context).x, n);
    }
    
    private Resources getResources() {
        return this.getContext().getResources();
    }
    
    void configureDimensions() {
        final boolean b = this.dialog != null;
        int promptWidthPx;
        if (this.bottomSheet) {
            promptWidthPx = -1;
        }
        else {
            promptWidthPx = getPromptWidthPx(this.getContext(), this.maxPromptWidth);
        }
        final int promptBannerHeight = this.layoutDimensions.getPromptBannerHeight(this.twoLinePrompt);
        final CardView promptCard = this.promptCard;
        float cardElevation;
        if (this.bottomSheet) {
            cardElevation = this.getResources().getDimension(R.dimen.hats_lib_prompt_banner_elevation_sheet);
        }
        else {
            cardElevation = this.getResources().getDimension(R.dimen.hats_lib_prompt_banner_elevation_card);
        }
        promptCard.setCardElevation(cardElevation);
        final float n = this.promptCard.getMaxCardElevation() * 1.5f;
        final float maxCardElevation = this.promptCard.getMaxCardElevation();
        final RectF bannerPadding = this.getBannerPadding(n);
        if (b) {
            final Window window = this.dialog.getWindow();
            window.setBackgroundDrawable((Drawable)new ColorDrawable(0));
            window.addFlags(32);
            window.clearFlags(2);
            final WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.x = 0;
            attributes.y = 0;
            attributes.width = promptWidthPx;
            attributes.height = Math.round(promptBannerHeight + bannerPadding.top + bannerPadding.bottom);
            attributes.gravity = 85;
            window.setAttributes(attributes);
        }
        try {
            final ViewGroup$MarginLayoutParams layoutParams = (ViewGroup$MarginLayoutParams)this.promptCard.getLayoutParams();
            layoutParams.height = Math.round(promptBannerHeight + 2.0f * n);
            layoutParams.setMargins(Math.round(bannerPadding.left - maxCardElevation), Math.round(bannerPadding.top - n), Math.round(bannerPadding.right - maxCardElevation), Math.round(bannerPadding.bottom - n));
            this.promptCard.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        }
        catch (ClassCastException ex) {
            throw new RuntimeException("HatsShowRequest.insertIntoParent can only be called with a ViewGroup whose LayoutParams extend MarginLayoutParams", ex);
        }
    }
}
