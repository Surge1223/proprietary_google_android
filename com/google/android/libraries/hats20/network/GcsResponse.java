package com.google.android.libraries.hats20.network;

public class GcsResponse
{
    private final long expirationDateUnix;
    private final int responseCode;
    private final String surveyJson;
    
    public GcsResponse(final int responseCode, final long expirationDateUnix, final String surveyJson) {
        this.responseCode = responseCode;
        this.expirationDateUnix = expirationDateUnix;
        this.surveyJson = surveyJson;
    }
    
    public long expirationDateUnix() {
        return this.expirationDateUnix;
    }
    
    public int getResponseCode() {
        return this.responseCode;
    }
    
    public String getSurveyJson() {
        return this.surveyJson;
    }
}
