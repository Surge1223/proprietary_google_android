package com.google.android.libraries.hats20.network;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import android.util.Log;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.os.Build;
import android.os.Build.VERSION;
import java.util.Locale;
import android.net.Uri;
import com.google.android.libraries.hats20.storage.HatsDataStore;

public class GcsRequest
{
    public static final String USER_AGENT;
    private final HatsDataStore hatsDataStore;
    private final String postData;
    private final Uri requestUriWithNoParams;
    private final ResponseListener responseListener;
    
    static {
        USER_AGENT = String.format(Locale.US, "Mozilla/5.0; Hats App/v%d (Android %s; SDK %d; %s; %s; %s)", 2, Build.VERSION.RELEASE, Build.VERSION.SDK_INT, Build.ID, Build.MODEL, Build.TAGS);
    }
    
    public GcsRequest(final ResponseListener responseListener, final Uri uri, final HatsDataStore hatsDataStore) {
        this.responseListener = responseListener;
        this.postData = uri.getEncodedQuery();
        this.requestUriWithNoParams = uri.buildUpon().clearQuery().build();
        this.hatsDataStore = hatsDataStore;
    }
    
    public void send() {
        HttpURLConnection httpURLConnection2;
        final HttpURLConnection httpURLConnection = httpURLConnection2 = null;
        try {
            while (true) {
                try {
                    final long currentTimeMillis = System.currentTimeMillis();
                    httpURLConnection2 = httpURLConnection;
                    httpURLConnection2 = httpURLConnection;
                    final URL url = new URL(this.requestUriWithNoParams.toString());
                    httpURLConnection2 = httpURLConnection;
                    final HttpURLConnection httpURLConnection3 = httpURLConnection2 = (HttpURLConnection)url.openConnection();
                    httpURLConnection3.setDoOutput(true);
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setInstanceFollowRedirects(false);
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setRequestMethod("POST");
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection2 = httpURLConnection3;
                    final byte[] bytes = this.postData.getBytes("utf-8");
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setRequestProperty("Content-Length", Integer.toString(bytes.length));
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setRequestProperty("charset", "utf-8");
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setRequestProperty("Connection", "close");
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setRequestProperty("User-Agent", GcsRequest.USER_AGENT);
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection3.setUseCaches(false);
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    final DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection3.getOutputStream());
                    httpURLConnection2 = httpURLConnection3;
                    dataOutputStream.write(bytes);
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    final InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection3.getInputStream());
                    httpURLConnection2 = httpURLConnection3;
                    final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    final StringBuffer sb = new StringBuffer();
                    while (true) {
                        httpURLConnection2 = httpURLConnection3;
                        final String line = bufferedReader.readLine();
                        if (line == null) {
                            break;
                        }
                        httpURLConnection2 = httpURLConnection3;
                        sb.append(line);
                    }
                    httpURLConnection2 = httpURLConnection3;
                    bufferedReader.close();
                    httpURLConnection2 = httpURLConnection3;
                    final long currentTimeMillis2 = System.currentTimeMillis();
                    httpURLConnection2 = httpURLConnection3;
                    String string = sb.toString();
                    httpURLConnection2 = httpURLConnection3;
                    final int length = string.length();
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    final StringBuilder sb2 = new StringBuilder(55);
                    httpURLConnection2 = httpURLConnection3;
                    sb2.append("Downloaded ");
                    httpURLConnection2 = httpURLConnection3;
                    sb2.append(length);
                    httpURLConnection2 = httpURLConnection3;
                    sb2.append(" bytes in ");
                    httpURLConnection2 = httpURLConnection3;
                    sb2.append(currentTimeMillis2 - currentTimeMillis);
                    httpURLConnection2 = httpURLConnection3;
                    sb2.append(" ms");
                    httpURLConnection2 = httpURLConnection3;
                    Log.d("HatsLibGcsRequest", sb2.toString());
                    httpURLConnection2 = httpURLConnection3;
                    if (string.isEmpty()) {
                        httpURLConnection2 = httpURLConnection3;
                        final ResponseListener responseListener = this.responseListener;
                        httpURLConnection2 = httpURLConnection3;
                        httpURLConnection2 = httpURLConnection3;
                        final IOException ex = new IOException("GCS responded with no data. The site's publishing state may not be Enabled. Check Site > Advanced settings > Publishing state. For more info, see go/get-hats");
                        httpURLConnection2 = httpURLConnection3;
                        responseListener.onError(ex);
                    }
                    httpURLConnection2 = httpURLConnection3;
                    this.hatsDataStore.storeSetCookieHeaders(this.requestUriWithNoParams, httpURLConnection3.getHeaderFields());
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    final JSONObject jsonObject = new JSONObject(string);
                    httpURLConnection2 = httpURLConnection3;
                    final JSONObject jsonObject2 = jsonObject.getJSONObject("params");
                    httpURLConnection2 = httpURLConnection3;
                    final int int1 = jsonObject2.getInt("responseCode");
                    httpURLConnection2 = httpURLConnection3;
                    final long long1 = jsonObject2.getLong("expirationDate");
                    httpURLConnection2 = httpURLConnection3;
                    if (int1 != 0) {
                        string = "";
                    }
                    httpURLConnection2 = httpURLConnection3;
                    final GcsResponse gcsResponse = new GcsResponse(int1, long1, string);
                    httpURLConnection2 = httpURLConnection3;
                    this.responseListener.onSuccess(gcsResponse);
                    if (httpURLConnection3 != null) {
                        final HttpURLConnection httpURLConnection4 = httpURLConnection3;
                        httpURLConnection4.disconnect();
                    }
                }
                finally {
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.disconnect();
                    }
                    continue;
                }
                break;
            }
        }
        catch (IOException ex2) {}
        catch (JSONException ex3) {}
    }
    
    public interface ResponseListener
    {
        void onError(final Exception p0);
        
        void onSuccess(final GcsResponse p0);
    }
}
