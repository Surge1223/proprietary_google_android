package com.google.android.libraries.hats20.util;

import com.google.android.libraries.hats20.R;
import android.content.res.Resources;

public final class LayoutDimensions
{
    private final Resources resources;
    
    public LayoutDimensions(final Resources resources) {
        this.resources = resources;
    }
    
    public int getPromptBannerHeight(final boolean b) {
        if (b) {
            return this.resources.getDimensionPixelSize(R.dimen.hats_lib_prompt_banner_tall_height);
        }
        return this.resources.getDimensionPixelSize(R.dimen.hats_lib_prompt_banner_height);
    }
    
    public int getPromptMaxWidth() {
        return this.resources.getDimensionPixelSize(R.dimen.hats_lib_prompt_max_width);
    }
    
    public int getSurveyMaxHeight() {
        return this.resources.getDimensionPixelSize(R.dimen.hats_lib_survey_max_height);
    }
    
    public int getSurveyMaxWidth() {
        return this.resources.getDimensionPixelSize(R.dimen.hats_lib_survey_max_width);
    }
    
    public boolean isSurveyFullBleed() {
        return this.resources.getBoolean(R.bool.hats_lib_survey_is_full_bleed);
    }
    
    public boolean shouldDisplayPrompt() {
        return this.resources.getBoolean(R.bool.hats_lib_prompt_should_display);
    }
    
    public boolean shouldSurveyDisplayCloseButton() {
        return this.resources.getBoolean(R.bool.hats_lib_survey_should_display_close_button);
    }
    
    public boolean shouldSurveyDisplayScrim() {
        return true;
    }
}
