package com.google.android.libraries.hats20.util;

import android.view.Display;
import android.view.WindowManager;
import android.util.DisplayMetrics;
import android.graphics.Point;
import android.app.Activity;
import android.widget.TextView;
import android.content.Context;
import com.google.android.libraries.material.autoresizetext.AutoResizeTextView;

public final class LayoutUtils
{
    public static void fitTextInTextViewWrapIfNeeded(final float n, final int n2, final int n3, final String s, final AutoResizeTextView autoResizeTextView) {
        final Integer fontSizeGivenSpace = getFontSizeGivenSpace(n, n2, n3, s, autoResizeTextView.getContext());
        if (fontSizeGivenSpace == null) {
            autoResizeTextView.setMinTextSize(1, n3);
            autoResizeTextView.setTextSize(2, n3);
            autoResizeTextView.setLines(2);
            autoResizeTextView.setMaxLines(2);
        }
        else {
            autoResizeTextView.setTextSize(1, fontSizeGivenSpace);
            autoResizeTextView.setLines(1);
            autoResizeTextView.setMaxLines(1);
        }
    }
    
    private static Integer getFontSizeGivenSpace(final float n, int round, final int n2, final String s, final Context context) {
        TextView textView;
        float n3;
        for (textView = new TextView(context), round = Math.round(round * context.getResources().getConfiguration().fontScale), n3 = measureTextAtFontSize(round, s, textView); n3 > n && round > n2; n3 = measureTextAtFontSize(--round, s, textView)) {}
        Integer value;
        if (n3 > n) {
            value = null;
        }
        else {
            value = round;
        }
        return value;
    }
    
    public static Point getNavigationBarDimensionPixelSize(final Activity activity) {
        final Point realScreenDimensions = getRealScreenDimensions(activity);
        final Point usableContentDimensions = getUsableContentDimensions((Context)activity);
        if (usableContentDimensions.x < realScreenDimensions.x) {
            return new Point(realScreenDimensions.x - usableContentDimensions.x, usableContentDimensions.y);
        }
        if (usableContentDimensions.y < realScreenDimensions.y) {
            return new Point(usableContentDimensions.x, realScreenDimensions.y - usableContentDimensions.y);
        }
        return new Point();
    }
    
    public static Point getRealScreenDimensions(final Activity activity) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }
    
    public static Point getUsableContentDimensions(final Context context) {
        final Display defaultDisplay = ((WindowManager)context.getSystemService("window")).getDefaultDisplay();
        final Point point = new Point();
        defaultDisplay.getSize(point);
        return point;
    }
    
    public static boolean isNavigationBarOnRight(final Activity activity) {
        return getUsableContentDimensions((Context)activity).x < getRealScreenDimensions(activity).x;
    }
    
    private static float measureTextAtFontSize(final int n, final String s, final TextView textView) {
        textView.setTextSize(1, (float)n);
        return textView.getPaint().measureText(s);
    }
}
