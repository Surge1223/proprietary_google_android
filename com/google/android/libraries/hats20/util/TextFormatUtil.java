package com.google.android.libraries.hats20.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.Queue;
import java.util.Collection;
import java.util.PriorityQueue;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.text.SpannableStringBuilder;
import java.util.regex.Pattern;

public class TextFormatUtil
{
    private static Pattern boldRegexPattern;
    private static Pattern italicRegexPattern;
    
    static {
        TextFormatUtil.italicRegexPattern = Pattern.compile("(?<=(['\"]|\\s|^))(_(\\w|[.!?,'\"#$*])+_)(?=([.!?,'\"]|\\s|$))");
        TextFormatUtil.boldRegexPattern = Pattern.compile("(?<=(['\"]|\\s|^))(\\*(\\w|[.!?,'\"#$*])+\\*)(?=([.!?,'\"]|\\s|$))");
    }
    
    private static void addAndFormatTextToOutput(final SpannableStringBuilder spannableStringBuilder, final WordMatch wordMatch) {
        final char char1 = wordMatch.word.charAt(0);
        if (char1 != '*' && char1 != '_') {
            spannableStringBuilder.append((CharSequence)wordMatch.word);
            return;
        }
        spannableStringBuilder.append((CharSequence)wordMatch.word.substring(1, wordMatch.word.length() - 1));
        final int end = wordMatch.end;
        final int start = wordMatch.start;
        int n = 2;
        if (char1 == '*') {
            n = 1;
        }
        spannableStringBuilder.setSpan((Object)new StyleSpan(n), spannableStringBuilder.length() - (end - start - 2), spannableStringBuilder.length(), 33);
    }
    
    public static Spannable format(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return (Spannable)new SpannableString((CharSequence)"");
        }
        if (!s.contains(Character.toString('*')) && !s.contains(Character.toString('_'))) {
            return (Spannable)new SpannableString((CharSequence)s);
        }
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        final Matcher matcher = TextFormatUtil.italicRegexPattern.matcher(s);
        final Matcher matcher2 = TextFormatUtil.boldRegexPattern.matcher(s);
        final PriorityQueue<WordMatch> priorityQueue = new PriorityQueue<WordMatch>();
        priorityQueue.addAll((Collection<?>)parseMatches(matcher));
        priorityQueue.addAll((Collection<?>)parseMatches(matcher2));
        for (int i = 0; i < s.length(); ++i) {
            if (isThereAFormattedItemAtStartPosition(i, priorityQueue)) {
                final WordMatch wordMatch = priorityQueue.remove();
                addAndFormatTextToOutput(spannableStringBuilder, wordMatch);
                i = wordMatch.end - 1;
            }
            else {
                spannableStringBuilder.append(s.charAt(i));
            }
        }
        return (Spannable)spannableStringBuilder;
    }
    
    private static boolean isThereAFormattedItemAtStartPosition(final int n, final Queue<WordMatch> queue) {
        return !queue.isEmpty() && n == queue.peek().start;
    }
    
    private static class WordMatch implements Comparable<WordMatch>
    {
        final int end;
        final int start;
        final String word;
        
        WordMatch(final Matcher matcher) {
            this.start = matcher.start();
            this.end = matcher.end();
            this.word = matcher.group();
        }
        
        private static List<WordMatch> parseMatches(final Matcher matcher) {
            final ArrayList<WordMatch> list = new ArrayList<WordMatch>();
            while (matcher.find()) {
                list.add(new WordMatch(matcher));
            }
            return list;
        }
        
        @Override
        public int compareTo(final WordMatch wordMatch) {
            return Integer.compare(this.start, wordMatch.start);
        }
    }
}
