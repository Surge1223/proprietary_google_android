package com.google.android.libraries.hats20.ui;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;
import android.view.MotionEvent;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Bitmap;
import android.view.accessibility.AccessibilityManager;
import android.view.View;

public final class StarRatingBar extends View
{
    private AccessibilityManager accessibilityManager;
    private Bitmap emptyStarBitmap;
    private int numStars;
    private OnRatingChangeListener onRatingChangeListener;
    private Paint paint;
    private int rating;
    private Bitmap starBitmap;
    
    public StarRatingBar(final Context context) {
        super(context);
        this.numStars = 11;
        this.init(context);
    }
    
    public StarRatingBar(final Context context, final AttributeSet set) {
        super(context, set);
        this.numStars = 11;
        this.init(context);
    }
    
    public StarRatingBar(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.numStars = 11;
        this.init(context);
    }
    
    public StarRatingBar(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.numStars = 11;
        this.init(context);
    }
    
    private float getDistanceBetweenStars() {
        return (this.getWidth() - this.getPaddingLeft() - this.getPaddingRight() - this.starBitmap.getWidth()) / (this.numStars - 1);
    }
    
    private int getRatingAtTouchPoint(final float n, float n2) {
        float distanceBetweenStars;
        int n3;
        for (distanceBetweenStars = this.getDistanceBetweenStars(), n2 = this.getPaddingLeft() + this.starBitmap.getWidth() / 2.0f + distanceBetweenStars / 2.0f, n3 = 1; n2 < n && n3 < this.numStars; ++n3, n2 += distanceBetweenStars) {}
        return n3;
    }
    
    private float getStarXCoord(final int n) {
        return this.getPaddingLeft() + n * this.getDistanceBetweenStars();
    }
    
    private void init(final Context context) {
        this.accessibilityManager = (AccessibilityManager)context.getSystemService("accessibility");
        this.starBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.quantum_ic_star_black_24);
        this.emptyStarBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.quantum_ic_star_border_grey600_24);
        (this.paint = new Paint(5)).setStyle(Paint.Style.FILL);
    }
    
    private void setRating(final int rating) {
        if (rating > 0 && rating <= this.numStars && rating != this.rating) {
            this.rating = rating;
            this.invalidate();
            if (this.onRatingChangeListener != null) {
                this.onRatingChangeListener.onRatingChanged(this.rating);
            }
            if (this.accessibilityManager.isEnabled()) {
                this.sendAccessibilityEvent(4);
            }
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.getWidth() != 0 && this.getHeight() != 0) {
            for (int i = 0; i < this.numStars; ++i) {
                Bitmap bitmap;
                if (i < this.rating) {
                    bitmap = this.starBitmap;
                }
                else {
                    bitmap = this.emptyStarBitmap;
                }
                canvas.drawBitmap(bitmap, this.getStarXCoord(i), (float)this.getPaddingTop(), this.paint);
            }
        }
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        if (n == 21) {
            this.setRating(this.rating - 1);
            return true;
        }
        if (n == 22) {
            this.setRating(this.rating + 1);
            return true;
        }
        return super.onKeyDown(n, keyEvent);
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.setMeasuredDimension(resolveSize(this.numStars * this.starBitmap.getWidth() + this.getPaddingLeft() + this.getPaddingRight(), n), resolveSize(this.starBitmap.getHeight() + this.getPaddingTop() + this.getPaddingBottom(), n2));
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.numStars = savedState.numStars;
        this.rating = savedState.rating;
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.numStars = this.numStars;
        savedState.rating = this.rating;
        return (Parcelable)savedState;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final int n = motionEvent.getAction() & 0xFF;
        if (n != 0 && n != 2) {
            return false;
        }
        this.setRating(this.getRatingAtTouchPoint(motionEvent.getX(), motionEvent.getY()));
        return true;
    }
    
    public void setNumStars(final int numStars) {
        if (numStars >= 3) {
            this.numStars = numStars;
            this.requestLayout();
            return;
        }
        throw new IllegalArgumentException("numStars must be at least 3");
    }
    
    public void setOnRatingChangeListener(final OnRatingChangeListener onRatingChangeListener) {
        this.onRatingChangeListener = onRatingChangeListener;
    }
    
    public interface OnRatingChangeListener
    {
        void onRatingChanged(final int p0);
    }
    
    private static final class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        int numStars;
        int rating;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        private SavedState(final Parcel parcel) {
            super(parcel);
            this.numStars = parcel.readInt();
            this.rating = parcel.readInt();
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.numStars);
            parcel.writeInt(this.rating);
        }
    }
}
