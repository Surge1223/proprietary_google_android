package com.google.android.libraries.hats20;

import android.util.Log;
import android.app.Activity;

public class HatsShowRequest
{
    private final boolean bottomSheet;
    private final Activity clientActivity;
    private final Integer maxPromptWidth;
    private final int parentResId;
    private final Integer requestCode;
    private final String siteId;
    
    private HatsShowRequest(final Builder builder) {
        this.clientActivity = builder.clientActivity;
        this.siteId = builder.siteId;
        this.requestCode = builder.requestCode;
        this.parentResId = builder.parentResId;
        this.maxPromptWidth = builder.maxPromptWidth;
        this.bottomSheet = builder.bottomSheet;
    }
    
    public static Builder builder(final Activity activity) {
        return new Builder(activity);
    }
    
    public Activity getClientActivity() {
        return this.clientActivity;
    }
    
    public Integer getMaxPromptWidth() {
        return this.maxPromptWidth;
    }
    
    public int getParentResId() {
        return this.parentResId;
    }
    
    public Integer getRequestCode() {
        return this.requestCode;
    }
    
    public String getSiteId() {
        return this.siteId;
    }
    
    public boolean isBottomSheet() {
        return this.bottomSheet;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.clientActivity.getLocalClassName());
        final String siteId = this.siteId;
        final String value2 = String.valueOf(this.requestCode);
        final int parentResId = this.parentResId;
        final String value3 = String.valueOf(this.maxPromptWidth);
        final boolean bottomSheet = this.bottomSheet;
        final StringBuilder sb = new StringBuilder(118 + String.valueOf(value).length() + String.valueOf(siteId).length() + String.valueOf(value2).length() + String.valueOf(value3).length());
        sb.append("HatsShowRequest{clientActivity=");
        sb.append(value);
        sb.append(", siteId='");
        sb.append(siteId);
        sb.append("'");
        sb.append(", requestCode=");
        sb.append(value2);
        sb.append(", parentResId=");
        sb.append(parentResId);
        sb.append(", maxPromptWidth=");
        sb.append(value3);
        sb.append(", bottomSheet=");
        sb.append(bottomSheet);
        sb.append("}");
        return sb.toString();
    }
    
    public static class Builder
    {
        private boolean bottomSheet;
        private final Activity clientActivity;
        private Integer maxPromptWidth;
        private int parentResId;
        private Integer requestCode;
        private String siteId;
        
        Builder(final Activity clientActivity) {
            if (clientActivity != null) {
                this.clientActivity = clientActivity;
                return;
            }
            throw new IllegalArgumentException("Client activity is missing.");
        }
        
        public HatsShowRequest build() {
            if (this.siteId == null) {
                Log.d("HatsLibShowRequest", "Site ID was not set, no survey will be shown.");
                this.siteId = "-1";
            }
            return new HatsShowRequest(this, null);
        }
        
        public Builder forSiteId(final String siteId) {
            if (siteId == null) {
                throw new NullPointerException("Site ID cannot be set to null.");
            }
            if (this.siteId == null) {
                this.siteId = siteId;
                return this;
            }
            throw new UnsupportedOperationException("Currently don't support multiple site IDs.");
        }
    }
}
