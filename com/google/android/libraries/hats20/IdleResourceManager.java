package com.google.android.libraries.hats20;

import com.google.android.apps.common.testing.ui.espresso.IdlingResource;

public class IdleResourceManager implements IdlingResource
{
    private ResourceCallback espressoIdlingCallback;
    private boolean isMultipleChoiceSelectionAnimating;
    private boolean isThankYouAnimating;
    
    private void transitionToIdle() {
        if (this.espressoIdlingCallback != null) {
            this.espressoIdlingCallback.onTransitionToIdle();
        }
    }
    
    public boolean isIdleNow() {
        return !this.isMultipleChoiceSelectionAnimating && !this.isThankYouAnimating;
    }
    
    public void setIsMultipleChoiceSelectionAnimating(final boolean isMultipleChoiceSelectionAnimating) {
        final boolean idleNow = this.isIdleNow();
        this.isMultipleChoiceSelectionAnimating = isMultipleChoiceSelectionAnimating;
        if (!idleNow && this.isIdleNow()) {
            this.transitionToIdle();
        }
    }
    
    public void setIsThankYouAnimating(final boolean isThankYouAnimating) {
        final boolean idleNow = this.isIdleNow();
        this.isThankYouAnimating = isThankYouAnimating;
        if (!idleNow && this.isIdleNow()) {
            this.transitionToIdle();
        }
    }
}
