package com.google.android.libraries.hats20;

import android.app.Dialog;
import android.app.Activity;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.res.Resources;
import com.google.android.libraries.hats20.util.LayoutUtils;
import com.google.android.libraries.material.autoresizetext.AutoResizeTextView;
import android.view.View;
import com.google.android.libraries.hats20.answer.AnswerBeaconTransmitter;
import com.google.android.libraries.hats20.storage.HatsDataStore;
import android.os.Parcelable;
import android.os.Bundle;
import com.google.android.libraries.hats20.model.SurveyController;
import com.google.android.libraries.hats20.util.LayoutDimensions;
import android.content.Context;
import com.google.android.libraries.hats20.answer.AnswerBeacon;

class PromptDialogDelegate
{
    private AnswerBeacon answerBeacon;
    private boolean areDimensionsValid;
    private DimensionConfigurationHelper configurationHelper;
    private Context context;
    private final DialogFragmentInterface dialogFragment;
    private boolean isBottomSheet;
    private boolean isStartingSurvey;
    private boolean isTwoLinePrompt;
    private LayoutDimensions layoutDimensions;
    private int maxPromptWidth;
    private SurveyController surveyController;
    
    public PromptDialogDelegate(final DialogFragmentInterface dialogFragment) {
        this.isTwoLinePrompt = false;
        this.isBottomSheet = false;
        this.isStartingSurvey = false;
        this.areDimensionsValid = false;
        this.dialogFragment = dialogFragment;
    }
    
    private void configureDimensions() {
        if (!this.areDimensionsValid) {
            this.configurationHelper.configureDimensions();
        }
        this.areDimensionsValid = true;
    }
    
    public static Bundle createArgs(final String s, final SurveyController surveyController, final AnswerBeacon answerBeacon, final Integer n, final Integer n2, final boolean b) {
        final Bundle bundle = new Bundle();
        bundle.putString("SiteId", s);
        bundle.putParcelable("SurveyController", (Parcelable)surveyController);
        bundle.putParcelable("AnswerBeacon", (Parcelable)answerBeacon);
        if (n != null) {
            bundle.putInt("RequestCode", (int)n);
        }
        if (n2 != null) {
            bundle.putInt("MaxPromptWidth", (int)n2);
        }
        bundle.putBoolean("BottomSheet", b);
        return bundle;
    }
    
    private void transmitOtherAccessBeacon() {
        this.answerBeacon.setBeaconType("o");
        new AnswerBeaconTransmitter(this.surveyController.getAnswerUrl(), HatsDataStore.buildFromContext(this.context)).transmit(this.answerBeacon);
    }
    
    private void updatePromptBannerText(final View view, final String text) {
        final AutoResizeTextView autoResizeTextView = (AutoResizeTextView)view.findViewById(R.id.hats_lib_prompt_title_text);
        final Resources resources = this.context.getResources();
        LayoutUtils.fitTextInTextViewWrapIfNeeded(DimensionConfigurationHelper.getPromptWidthPx(this.context, this.maxPromptWidth) - (resources.getDimensionPixelSize(R.dimen.hats_lib_prompt_banner_left_padding) + resources.getDimensionPixelSize(R.dimen.hats_lib_prompt_banner_right_padding)), 20, 16, text, autoResizeTextView);
        if (autoResizeTextView.getMaxLines() == 2) {
            this.isTwoLinePrompt = true;
        }
        autoResizeTextView.setText((CharSequence)text);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.context = (Context)this.dialogFragment.getActivity();
        this.layoutDimensions = new LayoutDimensions(this.context.getResources());
        final Bundle arguments = this.dialogFragment.getArguments();
        final String string = arguments.getString("SiteId");
        final int int1 = arguments.getInt("RequestCode", -1);
        this.maxPromptWidth = arguments.getInt("MaxPromptWidth", this.layoutDimensions.getPromptMaxWidth());
        this.surveyController = (SurveyController)arguments.getParcelable("SurveyController");
        this.answerBeacon = (AnswerBeacon)arguments.getParcelable("AnswerBeacon");
        this.isBottomSheet = arguments.getBoolean("BottomSheet");
        if (this.dialogFragment.getShowsDialog()) {
            this.dialogFragment.getDialog().requestWindowFeature(1);
        }
        HatsClient.markSurveyRunning();
        final View inflate = layoutInflater.inflate(R.layout.hats_prompt_banner, viewGroup, false);
        this.updatePromptBannerText(inflate, this.surveyController.getPromptMessage());
        inflate.findViewById(R.id.hats_lib_prompt_banner).setMinimumHeight(this.layoutDimensions.getPromptBannerHeight(this.isTwoLinePrompt));
        this.configurationHelper = new DimensionConfigurationHelper((CardView)inflate, this.dialogFragment.getDialog(), this.layoutDimensions, this.isBottomSheet, this.isTwoLinePrompt);
        final Button button = (Button)inflate.findViewById(R.id.hats_lib_prompt_no_thanks_button);
        final Button button2 = (Button)inflate.findViewById(R.id.hats_lib_prompt_take_survey_button);
        inflate.findViewById(R.id.hats_lib_prompt_no_thanks_wrapper).setOnTouchListener((View.OnTouchListener)new View.OnTouchListener(this) {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                button.onTouchEvent(motionEvent);
                return true;
            }
        });
        inflate.findViewById(R.id.hats_lib_prompt_take_survey_wrapper).setOnTouchListener((View.OnTouchListener)new View.OnTouchListener(this) {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                button2.onTouchEvent(motionEvent);
                return true;
            }
        });
        button2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                SurveyPromptActivity.startSurveyActivity(PromptDialogDelegate.this.dialogFragment.getActivity(), string, PromptDialogDelegate.this.surveyController, PromptDialogDelegate.this.answerBeacon, int1, PromptDialogDelegate.this.isBottomSheet);
                PromptDialogDelegate.this.isStartingSurvey = true;
                PromptDialogDelegate.this.dialogFragment.dismissAllowingStateLoss();
            }
        });
        button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                PromptDialogDelegate.this.transmitOtherAccessBeacon();
                PromptDialogDelegate.this.dialogFragment.dismissAllowingStateLoss();
            }
        });
        return inflate;
    }
    
    public void onDestroy() {
        if (!this.isStartingSurvey) {
            HatsClient.markSurveyFinished();
        }
    }
    
    public void onPause() {
        this.areDimensionsValid = false;
    }
    
    public void onResume() {
        this.configureDimensions();
    }
    
    public void onStart() {
        this.configureDimensions();
    }
    
    public interface DialogFragmentInterface
    {
        void dismissAllowingStateLoss();
        
        Activity getActivity();
        
        Bundle getArguments();
        
        Dialog getDialog();
        
        boolean getShowsDialog();
    }
}
