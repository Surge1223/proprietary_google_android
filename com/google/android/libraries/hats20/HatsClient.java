package com.google.android.libraries.hats20;

import android.support.v4.app.FragmentManager;
import org.json.JSONException;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.google.android.libraries.hats20.util.LayoutDimensions;
import com.google.android.libraries.hats20.answer.AnswerBeacon;
import com.google.android.libraries.hats20.model.SurveyController;
import android.os.Build.VERSION;
import android.support.v4.content.LocalBroadcastManager;
import android.content.Intent;
import java.net.CookieManager;
import android.content.Context;
import com.google.android.libraries.hats20.network.GcsResponse;
import com.google.android.libraries.hats20.network.GcsRequest;
import java.net.CookieHandler;
import com.google.android.libraries.hats20.storage.HatsDataStore;
import android.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

public class HatsClient
{
    private static final AtomicBoolean isSurveyRunning;
    
    static {
        isSurveyRunning = new AtomicBoolean(false);
    }
    
    public static void downloadSurvey(final HatsDownloadRequest hatsDownloadRequest) {
        if ("-1".equals(hatsDownloadRequest.getSiteId())) {
            Log.d("HatsLibClient", "No Site ID set, ignoring download request.");
            return;
        }
        synchronized (HatsClient.isSurveyRunning) {
            if (HatsClient.isSurveyRunning.get()) {
                return;
            }
            final HatsDataStore buildFromContext = HatsDataStore.buildFromContext(hatsDownloadRequest.getContext());
            buildFromContext.removeSurveyIfExpired(hatsDownloadRequest.getSiteId());
            if (buildFromContext.surveyExists(hatsDownloadRequest.getSiteId())) {
                return;
            }
            if (!hasInternetPermission(hatsDownloadRequest.getContext())) {
                Log.e("HatsLibClient", "Application does not have internet permission. Cannot make network request.");
                return;
            }
            if (CookieHandler.getDefault() == null) {
                Log.e("HatsLibClient", "Invalid configuration: Application does not have a cookie jar installed.");
                return;
            }
            buildFromContext.restoreCookiesFromPersistence();
            NetworkExecutor.getNetworkExecutor().execute(new Runnable() {
                final /* synthetic */ GcsRequest val$gcsRequest = new GcsRequest((GcsRequest.ResponseListener)new GcsRequest.ResponseListener(hatsDownloadRequest, buildFromContext) {
                    final /* synthetic */ HatsDownloadRequest val$downloadRequest;
                    final /* synthetic */ HatsDataStore val$hatsDataStore;
                    
                    @Override
                    public void onError(final Exception ex) {
                        Log.w("HatsLibClient", String.format("Site ID %s failed to download with error: %s", this.val$downloadRequest.getSiteId(), ex.toString()));
                        this.val$hatsDataStore.saveFailedDownload(this.val$downloadRequest.getSiteId());
                    }
                    
                    @Override
                    public void onSuccess(final GcsResponse gcsResponse) {
                        Log.d("HatsLibClient", String.format("Site ID %s downloaded with response code: %s", this.val$downloadRequest.getSiteId(), gcsResponse.getResponseCode()));
                        this.val$hatsDataStore.saveSuccessfulDownload(gcsResponse.getResponseCode(), gcsResponse.expirationDateUnix(), gcsResponse.getSurveyJson(), this.val$downloadRequest.getSiteId());
                        HatsClient.sendBroadcast(this.val$downloadRequest.getContext(), this.val$downloadRequest.getSiteId(), gcsResponse.getResponseCode());
                    }
                }, hatsDownloadRequest.computeDownloadUri(), buildFromContext);
                
                @Override
                public void run() {
                    this.val$gcsRequest.send();
                }
            });
        }
    }
    
    public static void forTestingClearAllData(final Context context) {
        HatsDataStore.buildFromContext(context).forTestingClearAllData();
    }
    
    public static void forTestingInjectSurveyIntoStorage(final Context context, final String s, final String s2, final int n, final long n2) {
        HatsDataStore.buildFromContext(context).forTestingInjectSurveyIntoStorage(s, s2, n, n2);
    }
    
    public static long getSurveyExpirationDate(final String s, final Context context) {
        final HatsDataStore buildFromContext = HatsDataStore.buildFromContext(context);
        buildFromContext.removeSurveyIfExpired(s);
        return buildFromContext.getSurveyExpirationDate(s, 0);
    }
    
    private static boolean hasInternetPermission(final Context context) {
        return context.checkCallingOrSelfPermission("android.permission.INTERNET") == 0;
    }
    
    public static void installCookieHandlerIfNeeded() {
        if (CookieHandler.getDefault() == null) {
            CookieHandler.setDefault(new CookieManager());
            Log.d("HatsLibClient", "Installed cookie handler.");
        }
        else {
            Log.d("HatsLibClient", "Attempted to install cookie handler but one was already installed; skipping the install.");
        }
    }
    
    static void markSurveyFinished() {
        synchronized (HatsClient.isSurveyRunning) {
            if (!HatsClient.isSurveyRunning.get()) {
                Log.e("HatsLibClient", "Notified that survey was destroyed when it wasn't marked as running.");
            }
            HatsClient.isSurveyRunning.set(false);
        }
    }
    
    static void markSurveyRunning() {
        synchronized (HatsClient.isSurveyRunning) {
            HatsClient.isSurveyRunning.set(true);
        }
    }
    
    static void sendBroadcast(final Context context, final String s, final int n) {
        if (Log.isLoggable("HatsLibClient", 3)) {
            Log.d("HatsLibClient", "Hats survey is downloaded. Sending broadcast with action ACTION_BROADCAST_SURVEY_DOWNLOADED");
        }
        final Intent intent = new Intent("com.google.android.libraries.hats20.SURVEY_DOWNLOADED");
        intent.putExtra("SiteId", s);
        intent.putExtra("ResponseCode", n);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
    
    public static boolean showSurveyIfAvailable(final HatsShowRequest hatsShowRequest) {
        if ("-1".equals(hatsShowRequest.getSiteId())) {
            Log.d("HatsLibClient", "No Site ID set, ignoring show request.");
            return false;
        }
        synchronized (HatsClient.isSurveyRunning) {
            if (HatsClient.isSurveyRunning.get()) {
                Log.d("HatsLibClient", "Attempted to show a survey while another one was already running, bailing out.");
                return false;
            }
            final Activity clientActivity = hatsShowRequest.getClientActivity();
            final boolean b = Build.VERSION.SDK_INT >= 17 && clientActivity.isDestroyed();
            if (clientActivity == null || clientActivity.isFinishing() || b) {
                Log.w("HatsLibClient", "Cancelling show request, activity was null, destroyed or finishing.");
                return false;
            }
            Object o = hatsShowRequest.getSiteId();
            final Integer requestCode = hatsShowRequest.getRequestCode();
            final HatsDataStore buildFromContext = HatsDataStore.buildFromContext((Context)hatsShowRequest.getClientActivity());
            buildFromContext.removeSurveyIfExpired((String)o);
            if (!buildFromContext.validSurveyExists((String)o)) {
                return false;
            }
            final String surveyJson = buildFromContext.getSurveyJson((String)o);
            if (surveyJson != null) {
                if (!surveyJson.isEmpty()) {
                    try {
                        final SurveyController initWithSurveyFromJson = SurveyController.initWithSurveyFromJson(surveyJson, clientActivity.getResources());
                        markSurveyRunning();
                        buildFromContext.removeSurvey((String)o);
                        final AnswerBeacon setPromptParams = new AnswerBeacon().setPromptParams(initWithSurveyFromJson.getPromptParams());
                        if (initWithSurveyFromJson.showInvitation() && new LayoutDimensions(clientActivity.getResources()).shouldDisplayPrompt()) {
                            if (clientActivity instanceof FragmentActivity) {
                                final FragmentManager supportFragmentManager = ((FragmentActivity)clientActivity).getSupportFragmentManager();
                                if (supportFragmentManager.findFragmentByTag("com.google.android.libraries.hats20.PromptDialogFragment") == null) {
                                    o = PromptDialogFragment.newInstance((String)o, initWithSurveyFromJson, setPromptParams, requestCode, hatsShowRequest.getMaxPromptWidth(), hatsShowRequest.isBottomSheet());
                                    supportFragmentManager.beginTransaction().add(hatsShowRequest.getParentResId(), (Fragment)o, "com.google.android.libraries.hats20.PromptDialogFragment").commitAllowingStateLoss();
                                }
                                else {
                                    Log.w("HatsLibClient", "PromptDialog was already open, bailing out.");
                                }
                            }
                            else if (clientActivity instanceof Activity) {
                                final android.app.FragmentManager fragmentManager = clientActivity.getFragmentManager();
                                if (fragmentManager.findFragmentByTag("com.google.android.libraries.hats20.PromptDialogFragment") == null) {
                                    o = PlatformPromptDialogFragment.newInstance((String)o, initWithSurveyFromJson, setPromptParams, requestCode, hatsShowRequest.getMaxPromptWidth(), hatsShowRequest.isBottomSheet());
                                    fragmentManager.beginTransaction().add(hatsShowRequest.getParentResId(), (android.app.Fragment)o, "com.google.android.libraries.hats20.PromptDialogFragment").commitAllowingStateLoss();
                                }
                                else {
                                    Log.w("HatsLibClient", "PromptDialog was already open, bailing out.");
                                }
                            }
                            return true;
                        }
                        SurveyPromptActivity.startSurveyActivity(clientActivity, (String)o, initWithSurveyFromJson, setPromptParams, requestCode, hatsShowRequest.isBottomSheet());
                        return true;
                    }
                    catch (JSONException ex) {
                        final StringBuilder sb = new StringBuilder(46 + String.valueOf(o).length());
                        sb.append("Failed to parse JSON for survey with site ID ");
                        sb.append((String)o);
                        sb.append(".");
                        Log.e("HatsLibClient", sb.toString(), (Throwable)ex);
                        return false;
                    }
                    catch (SurveyController.MalformedSurveyException ex2) {
                        Log.e("HatsLibClient", ex2.getMessage());
                        return false;
                    }
                }
            }
            Log.e("HatsLibClient", String.format("Attempted to start survey with site ID %s, but the json in the shared preferences was not found or was empty.", o));
            return false;
        }
    }
}
