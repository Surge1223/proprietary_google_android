package com.google.android.libraries.hats20;

import android.view.View;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.app.Activity;
import com.google.android.libraries.hats20.answer.AnswerBeacon;
import com.google.android.libraries.hats20.model.SurveyController;
import android.support.v4.app.DialogFragment;

public final class PromptDialogFragment extends DialogFragment implements DialogFragmentInterface
{
    private final PromptDialogDelegate delegate;
    
    public PromptDialogFragment() {
        this.delegate = new PromptDialogDelegate((PromptDialogDelegate.DialogFragmentInterface)this);
    }
    
    public static PromptDialogFragment newInstance(final String s, final SurveyController surveyController, final AnswerBeacon answerBeacon, final Integer n, final Integer n2, final boolean b) {
        final PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
        promptDialogFragment.setArguments(PromptDialogDelegate.createArgs(s, surveyController, answerBeacon, n, n2, b));
        return promptDialogFragment;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        return this.delegate.onCreateView(layoutInflater, viewGroup, bundle);
    }
    
    @Override
    public void onDestroy() {
        this.delegate.onDestroy();
        super.onDestroy();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.delegate.onPause();
    }
    
    @Override
    public void onResume() {
        this.delegate.onResume();
        super.onResume();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.delegate.onStart();
    }
}
