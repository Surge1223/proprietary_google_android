package com.google.android.libraries.hats20.adapter;

import com.google.android.libraries.hats20.view.MultipleChoiceFragment;
import com.google.android.libraries.hats20.view.MultipleSelectFragment;
import com.google.android.libraries.hats20.view.OpenTextFragment;
import com.google.android.libraries.hats20.view.RatingFragment;
import com.google.android.libraries.hats20.model.QuestionRating;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.google.android.libraries.hats20.model.Question;
import android.support.v4.app.FragmentPagerAdapter;

public class SurveyViewPagerAdapter extends FragmentPagerAdapter
{
    private final Question[] questions;
    
    public SurveyViewPagerAdapter(final FragmentManager fragmentManager, final Question[] questions) {
        super(fragmentManager);
        if (questions != null) {
            this.questions = questions;
            return;
        }
        throw new NullPointerException("Questions were missing!");
    }
    
    private Fragment buildFragment(final Question question) {
        final int type = question.getType();
        switch (type) {
            default: {
                throw new AssertionError((Object)String.format("Attempted to build fragment for unsupported Question type %s.  Note this should never happen as it's invalid to create a Question type that does not have a matching fragment.", type));
            }
            case 4: {
                return RatingFragment.newInstance((QuestionRating)question);
            }
            case 3: {
                return OpenTextFragment.newInstance(question);
            }
            case 2: {
                return MultipleSelectFragment.newInstance(question);
            }
            case 1: {
                return MultipleChoiceFragment.newInstance(question);
            }
        }
    }
    
    public static int getQuestionIndex(final Fragment fragment) {
        return fragment.getArguments().getInt("QuestionIndex", -1);
    }
    
    @Override
    public int getCount() {
        return this.questions.length;
    }
    
    @Override
    public Fragment getItem(final int n) {
        final Fragment buildFragment = this.buildFragment(this.questions[n]);
        buildFragment.getArguments().putInt("QuestionIndex", n);
        return buildFragment;
    }
}
