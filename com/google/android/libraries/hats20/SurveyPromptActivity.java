package com.google.android.libraries.hats20;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.support.v4.app.Fragment;
import com.google.android.libraries.hats20.storage.HatsDataStore;
import com.google.android.libraries.hats20.view.OpenTextFragment;
import android.view.ViewGroup;
import android.view.View$MeasureSpec;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.util.Log;
import android.os.Parcelable;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.os.Bundle;
import com.google.android.libraries.hats20.model.Question;
import android.widget.Button;
import android.view.WindowManager.LayoutParams;
import android.view.Window;
import android.app.Activity;
import com.google.android.libraries.hats20.util.LayoutUtils;
import com.google.android.libraries.hats20.answer.QuestionResponse;
import android.widget.TextView;
import com.google.android.libraries.hats20.adapter.SurveyViewPagerAdapter;
import com.google.android.libraries.hats20.view.SurveyViewPager;
import android.graphics.Point;
import com.google.android.libraries.hats20.model.SurveyController;
import android.widget.LinearLayout;
import android.widget.FrameLayout;
import com.google.android.libraries.hats20.util.LayoutDimensions;
import com.google.android.libraries.hats20.answer.AnswerBeaconTransmitter;
import com.google.android.libraries.hats20.answer.AnswerBeacon;
import android.os.Handler;
import com.google.android.libraries.hats20.view.OnQuestionProgressableChangeListener;
import com.google.android.libraries.hats20.view.FragmentViewDelegate;
import android.support.v7.app.AppCompatActivity;

public class SurveyPromptActivity extends AppCompatActivity implements MeasurementSurrogate, OnQuestionProgressableChangeListener
{
    private final Handler activityFinishHandler;
    private AnswerBeacon answerBeacon;
    private AnswerBeaconTransmitter answerBeaconTransmitter;
    private IdleResourceManager idleResourceManager;
    private boolean isFullWidth;
    private boolean isSubmitting;
    private int itemMeasureCount;
    private LayoutDimensions layoutDimensions;
    private FrameLayout overallContainer;
    private String siteId;
    private LinearLayout surveyContainer;
    private SurveyController surveyController;
    private final Point surveyPreDrawMeasurements;
    private SurveyViewPager surveyViewPager;
    private SurveyViewPagerAdapter surveyViewPagerAdapter;
    private TextView thankYouTextView;
    
    public SurveyPromptActivity() {
        this.surveyPreDrawMeasurements = new Point(0, 0);
        this.itemMeasureCount = 0;
        this.activityFinishHandler = new Handler();
    }
    
    private void addCurrentItemResponseToAnswerBeacon() {
        final QuestionResponse currentItemQuestionResponse = this.surveyViewPager.getCurrentItemQuestionResponse();
        if (currentItemQuestionResponse != null) {
            this.answerBeacon.setQuestionResponse(this.surveyViewPager.getCurrentItem(), currentItemQuestionResponse);
        }
    }
    
    private void configureSurveyWindowParameters() {
        final Window window = this.getWindow();
        final WindowManager.LayoutParams attributes = window.getAttributes();
        final Point point = new Point(0, 0);
        this.getWindowManager().getDefaultDisplay().getSize(point);
        attributes.gravity = 85;
        attributes.width = this.getFinalizedSurveyDimensions().x;
        attributes.height = point.y;
        if (LayoutUtils.isNavigationBarOnRight(this)) {
            attributes.x = LayoutUtils.getNavigationBarDimensionPixelSize(this).x;
        }
        else {
            attributes.y = LayoutUtils.getNavigationBarDimensionPixelSize(this).y;
        }
        if (this.layoutDimensions.shouldSurveyDisplayScrim()) {
            this.showWindowScrim();
        }
        window.setAttributes(attributes);
    }
    
    private void sendWindowStateChangeAccessibilityEvent() {
        this.surveyViewPager.getCurrentItemFragment().getView().sendAccessibilityEvent(32);
    }
    
    private void setBeaconTypeAndTransmit(final String beaconType) {
        this.answerBeacon.setBeaconType(beaconType);
        this.answerBeaconTransmitter.transmit(this.answerBeacon);
    }
    
    private void setNextButtonEnabled(final boolean enabled) {
        final Button button = this.findViewById(R.id.hats_lib_next);
        if (button != null && button.isEnabled() != enabled) {
            float alpha;
            if (enabled) {
                alpha = 1.0f;
            }
            else {
                alpha = 0.3f;
            }
            button.setAlpha(alpha);
            button.setEnabled(enabled);
        }
    }
    
    private void setUpSurveyPager(final Question[] array, final Bundle bundle) {
        this.surveyViewPagerAdapter = new SurveyViewPagerAdapter(this.getSupportFragmentManager(), array);
        (this.surveyViewPager = this.findViewById(R.id.hats_lib_survey_viewpager)).setAdapter(this.surveyViewPagerAdapter);
        this.surveyViewPager.setImportantForAccessibility(2);
        if (bundle != null) {
            this.surveyViewPager.setCurrentItem(bundle.getInt("CurrentQuestionIndex"));
        }
        if (this.surveyController.shouldIncludeSurveyControls()) {
            this.switchNextTextToSubmitIfNeeded();
        }
    }
    
    private void showWindowScrim() {
        final Window window = this.getWindow();
        window.addFlags(2);
        window.clearFlags(32);
        window.addFlags(262144);
        window.setDimAmount(0.4f);
    }
    
    private void signalSurveyBegun() {
        this.answerBeacon.setShown(this.surveyViewPager.getCurrentItem());
        this.surveyContainer.setVisibility(0);
        this.surveyContainer.forceLayout();
    }
    
    static void startSurveyActivity(final Activity activity, final String s, final SurveyController surveyController, final AnswerBeacon answerBeacon, final Integer n, final boolean b) {
        final Intent intent = new Intent((Context)activity, (Class)SurveyPromptActivity.class);
        intent.putExtra("SiteId", s);
        intent.putExtra("SurveyController", (Parcelable)surveyController);
        intent.putExtra("AnswerBeacon", (Parcelable)answerBeacon);
        intent.putExtra("IsFullWidth", b);
        Log.d("HatsLibSurveyActivity", String.format("Starting survey for client activity: %s", activity.getClass().getCanonicalName()));
        if (n == null) {
            activity.startActivity(intent);
        }
        else {
            activity.startActivityForResult(intent, (int)n);
        }
    }
    
    private void submit() {
        this.isSubmitting = true;
        this.idleResourceManager.setIsThankYouAnimating(true);
        this.findViewById(R.id.hats_lib_close_button).setVisibility(8);
        final AnimatorSet set = new AnimatorSet();
        final ObjectAnimator setDuration = ObjectAnimator.ofFloat((Object)this.surveyContainer, "alpha", new float[] { 0.0f }).setDuration(350L);
        setDuration.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            public void onAnimationEnd(final Animator animator) {
                SurveyPromptActivity.this.surveyContainer.setVisibility(8);
            }
        });
        final ValueAnimator setDuration2 = ValueAnimator.ofInt(new int[] { this.overallContainer.getHeight(), this.getResources().getDimensionPixelSize(R.dimen.hats_lib_thank_you_height) }).setDuration(350L);
        setDuration2.setStartDelay(350L);
        setDuration2.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                SurveyPromptActivity.this.overallContainer.getLayoutParams().height = (int)valueAnimator.getAnimatedValue();
                SurveyPromptActivity.this.overallContainer.requestLayout();
            }
        });
        final ObjectAnimator setDuration3 = ObjectAnimator.ofFloat((Object)this.thankYouTextView, "alpha", new float[] { 1.0f }).setDuration(350L);
        setDuration3.setStartDelay(700L);
        this.thankYouTextView.setVisibility(0);
        this.thankYouTextView.announceForAccessibility(this.thankYouTextView.getContentDescription());
        this.activityFinishHandler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                SurveyPromptActivity.this.idleResourceManager.setIsThankYouAnimating(false);
                SurveyPromptActivity.this.finish();
            }
        }, 2400L);
        set.playTogether(new Animator[] { setDuration, setDuration2, setDuration3 });
        set.start();
    }
    
    private void switchNextTextToSubmitIfNeeded() {
        final Button button = this.findViewById(R.id.hats_lib_next);
        if (button != null && this.surveyViewPager.isLastQuestion()) {
            button.setText(R.string.hats_lib_submit);
        }
    }
    
    private void transitionToSurveyMode() {
        this.surveyViewPager.fireOnPageScrolledIntoViewListener();
        if (!this.answerBeacon.hasBeaconType()) {
            this.setBeaconTypeAndTransmit("sv");
        }
        this.configureSurveyWindowParameters();
        this.surveyContainer.setAlpha(1.0f);
        this.updateSurveyLayoutParameters();
        this.updateSurveyFullBleed();
        if (this.layoutDimensions.shouldSurveyDisplayCloseButton()) {
            this.findViewById(R.id.hats_lib_close_button).setVisibility(0);
        }
        this.sendWindowStateChangeAccessibilityEvent();
    }
    
    private void updateSurveyLayoutParameters() {
        final FrameLayout$LayoutParams layoutParams = (FrameLayout$LayoutParams)this.overallContainer.getLayoutParams();
        final Point finalizedSurveyDimensions = this.getFinalizedSurveyDimensions();
        layoutParams.width = finalizedSurveyDimensions.x;
        layoutParams.height = finalizedSurveyDimensions.y;
        this.overallContainer.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    }
    
    private void wireUpCloseButton() {
        this.findViewById(R.id.hats_lib_close_button).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                SurveyPromptActivity.this.setBeaconTypeAndTransmit("o");
                SurveyPromptActivity.this.finish();
            }
        });
    }
    
    private void wireUpSurveyControls() {
        this.findViewById(R.id.hats_lib_next).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                SurveyPromptActivity.this.nextPageOrSubmit();
            }
        });
    }
    
    public Point getFinalizedSurveyDimensions() {
        int n = LayoutUtils.getUsableContentDimensions((Context)this).x;
        if (!this.isFullWidth) {
            n = Math.min(n, this.layoutDimensions.getSurveyMaxWidth());
        }
        return new Point(n, Math.min(this.layoutDimensions.getSurveyMaxHeight(), this.surveyPreDrawMeasurements.y));
    }
    
    public IdleResourceManager getIdleResourceManager() {
        return this.idleResourceManager;
    }
    
    @Override
    public Point getMeasureSpecs() {
        final Point usableContentDimensions = LayoutUtils.getUsableContentDimensions((Context)this);
        usableContentDimensions.y = (int)Math.min(usableContentDimensions.y * 0.8f, this.layoutDimensions.getSurveyMaxHeight());
        usableContentDimensions.x = Math.min(usableContentDimensions.x, this.layoutDimensions.getSurveyMaxWidth());
        return new Point(View$MeasureSpec.makeMeasureSpec(usableContentDimensions.x, Integer.MIN_VALUE), View$MeasureSpec.makeMeasureSpec(usableContentDimensions.y, Integer.MIN_VALUE));
    }
    
    public ViewGroup getSurveyContainer() {
        return (ViewGroup)this.surveyContainer;
    }
    
    public void nextPageOrSubmit() {
        if (this.surveyViewPager.getCurrentItemFragment() instanceof OpenTextFragment) {
            ((OpenTextFragment)this.surveyViewPager.getCurrentItemFragment()).closeKeyboard();
        }
        this.addCurrentItemResponseToAnswerBeacon();
        if (this.surveyViewPager.isLastQuestion()) {
            Log.d("HatsLibSurveyActivity", "Survey completed, submitting.");
            this.setBeaconTypeAndTransmit("a");
            this.submit();
        }
        else {
            this.setBeaconTypeAndTransmit("pa");
            this.surveyViewPager.navigateToNextPage();
            this.answerBeacon.setShown(this.surveyViewPager.getCurrentItem());
            this.switchNextTextToSubmitIfNeeded();
            this.sendWindowStateChangeAccessibilityEvent();
            Log.d("HatsLibSurveyActivity", String.format("Showing question: %d", this.surveyViewPager.getCurrentItem() + 1));
        }
    }
    
    @Override
    public void onBackPressed() {
        this.setBeaconTypeAndTransmit("o");
        super.onBackPressed();
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setTitle((CharSequence)"");
        this.layoutDimensions = new LayoutDimensions(this.getResources());
        this.siteId = this.getIntent().getStringExtra("SiteId");
        this.surveyController = (SurveyController)this.getIntent().getParcelableExtra("SurveyController");
        AnswerBeacon answerBeacon;
        if (bundle == null) {
            answerBeacon = (AnswerBeacon)this.getIntent().getParcelableExtra("AnswerBeacon");
        }
        else {
            answerBeacon = (AnswerBeacon)bundle.getParcelable("AnswerBeacon");
        }
        this.answerBeacon = answerBeacon;
        this.isSubmitting = (bundle != null && bundle.getBoolean("IsSubmitting"));
        this.isFullWidth = this.getIntent().getBooleanExtra("IsFullWidth", false);
        if (this.siteId != null && this.surveyController != null && this.answerBeacon != null) {
            HatsClient.markSurveyRunning();
            String s;
            if (bundle != null) {
                s = "created with savedInstanceState";
            }
            else {
                s = "created anew";
            }
            Log.d("HatsLibSurveyActivity", String.format("Activity %s with site ID: %s", s, this.siteId));
            this.answerBeaconTransmitter = new AnswerBeaconTransmitter(this.surveyController.getAnswerUrl(), HatsDataStore.buildFromContext((Context)this));
            this.setContentView(R.layout.hats_container);
            this.surveyContainer = this.findViewById(R.id.hats_lib_survey_container);
            this.overallContainer = this.findViewById(R.id.hats_lib_overall_container);
            this.wireUpCloseButton();
            (this.thankYouTextView = (TextView)this.overallContainer.findViewById(R.id.hats_lib_thank_you)).setText((CharSequence)this.surveyController.getThankYouMessage());
            this.thankYouTextView.setContentDescription((CharSequence)this.surveyController.getThankYouMessage());
            if (this.surveyController.shouldIncludeSurveyControls()) {
                this.getLayoutInflater().inflate(R.layout.hats_survey_controls, (ViewGroup)this.surveyContainer);
            }
            this.setUpSurveyPager(this.surveyController.getQuestions(), bundle);
            this.signalSurveyBegun();
            if (this.surveyController.shouldIncludeSurveyControls()) {
                this.wireUpSurveyControls();
            }
            this.idleResourceManager = new IdleResourceManager();
            return;
        }
        Log.e("HatsLibSurveyActivity", "Required EXTRAS not found in the intent, bailing out.");
        this.finish();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.isFinishing()) {
            HatsClient.markSurveyFinished();
        }
        this.activityFinishHandler.removeCallbacks((Runnable)null);
    }
    
    @Override
    public void onFragmentContentMeasurement(final int n, final int n2) {
        ++this.itemMeasureCount;
        this.surveyPreDrawMeasurements.x = Math.max(this.surveyPreDrawMeasurements.x, n);
        this.surveyPreDrawMeasurements.y = Math.max(this.surveyPreDrawMeasurements.y, n2);
        if (this.itemMeasureCount == this.surveyViewPagerAdapter.getCount()) {
            this.itemMeasureCount = 0;
            final FrameLayout frameLayout = this.findViewById(R.id.hats_lib_survey_controls_container);
            if (frameLayout != null) {
                final Point surveyPreDrawMeasurements = this.surveyPreDrawMeasurements;
                surveyPreDrawMeasurements.y += frameLayout.getMeasuredHeight();
            }
            this.transitionToSurveyMode();
        }
    }
    
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (this.isSubmitting) {
            this.finish();
        }
    }
    
    @Override
    public void onQuestionProgressableChanged(final boolean nextButtonEnabled, final Fragment fragment) {
        if (SurveyViewPagerAdapter.getQuestionIndex(fragment) == this.surveyViewPager.getCurrentItem()) {
            this.setNextButtonEnabled(nextButtonEnabled);
        }
    }
    
    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("CurrentQuestionIndex", this.surveyViewPager.getCurrentItem());
        bundle.putBoolean("IsSubmitting", this.isSubmitting);
        bundle.putParcelable("AnswerBeacon", (Parcelable)this.answerBeacon);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            final Rect rect = new Rect();
            this.overallContainer.getGlobalVisibleRect(rect);
            if (!rect.contains((int)motionEvent.getX(), (int)motionEvent.getY())) {
                Log.d("HatsLibSurveyActivity", "User clicked outside of survey root container. Closing.");
                if (!this.answerBeacon.hasBeaconTypeFullAnswer()) {
                    this.setBeaconTypeAndTransmit("o");
                }
                this.finish();
                return true;
            }
        }
        return super.onTouchEvent(motionEvent);
    }
    
    public void setIsMultipleChoiceSelectionAnimating(final boolean isMultipleChoiceSelectionAnimating) {
        this.idleResourceManager.setIsMultipleChoiceSelectionAnimating(isMultipleChoiceSelectionAnimating);
    }
    
    public void updateSurveyFullBleed() {
        if (this.layoutDimensions.isSurveyFullBleed()) {
            this.overallContainer.setPadding(0, 0, 0, 0);
        }
        else {
            final int dimensionPixelSize = this.getResources().getDimensionPixelSize(R.dimen.hats_lib_container_padding);
            this.overallContainer.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        }
    }
}
