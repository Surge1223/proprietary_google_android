package com.google.android.libraries.hats20.view;

import android.os.SystemClock;
import android.util.Log;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class QuestionMetrics implements Parcelable
{
    public static final Parcelable.Creator<QuestionMetrics> CREATOR;
    private long delayEndMs;
    private long delayStartMs;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<QuestionMetrics>() {
            public QuestionMetrics createFromParcel(final Parcel parcel) {
                return new QuestionMetrics(parcel, null);
            }
            
            public QuestionMetrics[] newArray(final int n) {
                return new QuestionMetrics[n];
            }
        };
    }
    
    QuestionMetrics() {
        this.delayStartMs = -1L;
        this.delayEndMs = -1L;
    }
    
    private QuestionMetrics(final Parcel parcel) {
        this.delayStartMs = parcel.readLong();
        this.delayEndMs = parcel.readLong();
    }
    
    public int describeContents() {
        return 0;
    }
    
    long getDelayMs() {
        long n;
        if (this.isAnswered()) {
            n = this.delayEndMs - this.delayStartMs;
        }
        else {
            n = -1L;
        }
        return n;
    }
    
    boolean isAnswered() {
        return this.delayEndMs >= 0L;
    }
    
    boolean isShown() {
        return this.delayStartMs >= 0L;
    }
    
    void markAsAnswered() {
        if (!this.isShown()) {
            Log.e("HatsLibQuestionMetrics", "Question was marked as answered but was never marked as shown.");
            return;
        }
        if (this.isAnswered()) {
            Log.d("HatsLibQuestionMetrics", "Question was already marked as answered.");
            return;
        }
        this.delayEndMs = SystemClock.elapsedRealtime();
    }
    
    void markAsShown() {
        if (this.isShown()) {
            return;
        }
        this.delayStartMs = SystemClock.elapsedRealtime();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.delayStartMs);
        parcel.writeLong(this.delayEndMs);
    }
}
