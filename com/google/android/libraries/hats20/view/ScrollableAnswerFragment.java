package com.google.android.libraries.hats20.view;

import android.os.Build.VERSION;
import com.google.android.libraries.hats20.SurveyPromptActivity;
import com.google.android.libraries.hats20.util.TextFormatUtil;
import com.google.android.libraries.hats20.R;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver$OnScrollChangedListener;
import android.view.View;
import android.widget.TextView;

public abstract class ScrollableAnswerFragment extends BaseFragment
{
    private boolean isOnScrollChangedListenerAttached;
    private TextView questionTextView;
    private ScrollShadowHandler scrollShadowHandler;
    private ScrollViewWithSizeCallback scrollView;
    private View scrollViewContents;
    private View surveyControlsContainer;
    
    public ScrollableAnswerFragment() {
        this.scrollShadowHandler = new ScrollShadowHandler();
        this.isOnScrollChangedListenerAttached = false;
    }
    
    private void startRespondingToScrollChanges() {
        if (!this.isOnScrollChangedListenerAttached && this.scrollView != null) {
            this.scrollView.getViewTreeObserver().addOnScrollChangedListener((ViewTreeObserver$OnScrollChangedListener)this.scrollShadowHandler);
            this.isOnScrollChangedListenerAttached = true;
        }
    }
    
    private void stopRespondingToScrollChanges() {
        if (this.isOnScrollChangedListenerAttached && this.scrollView != null) {
            this.scrollView.getViewTreeObserver().removeOnScrollChangedListener((ViewTreeObserver$OnScrollChangedListener)this.scrollShadowHandler);
            this.isOnScrollChangedListenerAttached = false;
        }
    }
    
    abstract View createScrollViewContents();
    
    abstract String getQuestionText();
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(R.layout.hats_survey_question_with_scrollable_content, viewGroup, false);
        (this.questionTextView = (TextView)inflate.findViewById(R.id.hats_lib_survey_question_text)).setText((CharSequence)TextFormatUtil.format(this.getQuestionText()));
        this.questionTextView.setContentDescription((CharSequence)this.getQuestionText());
        this.scrollViewContents = this.createScrollViewContents();
        (this.scrollView = (ScrollViewWithSizeCallback)inflate.findViewById(R.id.hats_survey_question_scroll_view)).addView(this.scrollViewContents);
        this.scrollView.setOnHeightChangedListener((ScrollViewWithSizeCallback.OnHeightChangedListener)this.scrollShadowHandler);
        this.startRespondingToScrollChanges();
        this.surveyControlsContainer = ((SurveyPromptActivity)viewGroup.getContext()).getSurveyContainer().findViewById(R.id.hats_lib_survey_controls_container);
        return inflate;
    }
    
    @Override
    public void onDetach() {
        this.stopRespondingToScrollChanges();
        super.onDetach();
    }
    
    private class ScrollShadowHandler implements ViewTreeObserver$OnScrollChangedListener, OnHeightChangedListener
    {
        private void hideBottomShadow() {
            this.setElevation(ScrollableAnswerFragment.this.surveyControlsContainer, 0.0f);
        }
        
        private void hideTopShadow() {
            this.setElevation((View)ScrollableAnswerFragment.this.questionTextView, 0.0f);
        }
        
        private void setElevation(final View view, final float elevation) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setElevation(elevation);
            }
        }
        
        private void showBottomShadow() {
            this.setElevation(ScrollableAnswerFragment.this.surveyControlsContainer, ScrollableAnswerFragment.this.getResources().getDimensionPixelSize(R.dimen.hats_lib_survey_controls_view_elevation));
        }
        
        private void showTopShadow() {
            this.setElevation((View)ScrollableAnswerFragment.this.questionTextView, ScrollableAnswerFragment.this.getResources().getDimensionPixelSize(R.dimen.hats_lib_question_view_elevation));
        }
        
        private void updateShadowVisibility(final int n) {
            if (!ScrollableAnswerFragment.this.getUserVisibleHint()) {
                return;
            }
            final int scrollY = ScrollableAnswerFragment.this.scrollView.getScrollY();
            boolean b = false;
            final boolean b2 = scrollY == 0;
            final boolean b3 = ScrollableAnswerFragment.this.scrollViewContents.getBottom() == ScrollableAnswerFragment.this.scrollView.getScrollY() + n;
            if (ScrollableAnswerFragment.this.scrollViewContents.getBottom() > n) {
                b = true;
            }
            if (b && !b2) {
                this.showTopShadow();
            }
            else {
                this.hideTopShadow();
            }
            if (b && !b3) {
                this.showBottomShadow();
            }
            else {
                this.hideBottomShadow();
            }
        }
        
        public void onHeightChanged(final int n) {
            if (n != 0) {
                this.updateShadowVisibility(n);
            }
        }
        
        public void onScrollChanged() {
            this.updateShadowVisibility(ScrollableAnswerFragment.this.scrollView.getHeight());
        }
    }
}
