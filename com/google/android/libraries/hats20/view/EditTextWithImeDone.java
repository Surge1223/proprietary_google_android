package com.google.android.libraries.hats20.view;

import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.EditText;

public class EditTextWithImeDone extends EditText
{
    public EditTextWithImeDone(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        editorInfo.imeOptions &= 0xFFFFFF00;
        editorInfo.imeOptions |= 0x6;
        editorInfo.imeOptions &= 0xBFFFFFFF;
        editorInfo.actionId = 6;
        return onCreateInputConnection;
    }
}
