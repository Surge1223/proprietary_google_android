package com.google.android.libraries.hats20.view;

import android.widget.CompoundButton;
import android.os.Parcelable;
import android.util.Log;
import android.support.v4.app.Fragment;
import android.widget.LinearLayout;
import java.util.List;
import com.google.android.libraries.hats20.answer.QuestionResponse;
import android.os.Bundle;
import com.google.android.libraries.hats20.model.QuestionMultipleSelect;
import com.google.android.libraries.hats20.model.Question;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import com.google.android.libraries.hats20.R;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.ArrayList;

public class MultipleSelectFragment extends ScrollableAnswerFragment
{
    private ArrayList<String> answers;
    private ViewGroup answersContainer;
    private FragmentViewDelegate fragmentViewDelegate;
    private boolean isNoneOfTheAboveChecked;
    private ArrayList<Integer> ordering;
    private QuestionMetrics questionMetrics;
    private String questionText;
    private boolean[] responses;
    
    public MultipleSelectFragment() {
        this.fragmentViewDelegate = new FragmentViewDelegate();
    }
    
    private void addCheckboxToAnswersContainer(final String s, final boolean checked, final int n, final String tag) {
        LayoutInflater.from(this.getContext()).inflate(R.layout.hats_survey_question_multiple_select_item, this.answersContainer, true);
        final FrameLayout frameLayout = (FrameLayout)this.answersContainer.getChildAt(n);
        final CheckBox checkBox = (CheckBox)frameLayout.findViewById(R.id.hats_lib_multiple_select_checkbox);
        checkBox.setText((CharSequence)s);
        checkBox.setContentDescription((CharSequence)s);
        checkBox.setChecked(checked);
        checkBox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CheckboxChangeListener(n));
        frameLayout.setOnClickListener((View.OnClickListener)new View.OnClickListener(this) {
            public void onClick(final View view) {
                checkBox.performClick();
            }
        });
        if (tag != null) {
            checkBox.setTag((Object)tag);
        }
    }
    
    public static MultipleSelectFragment newInstance(final Question question) {
        final MultipleSelectFragment multipleSelectFragment = new MultipleSelectFragment();
        final QuestionMultipleSelect questionMultipleSelect = (QuestionMultipleSelect)question;
        final Bundle arguments = new Bundle();
        arguments.putString("QuestionText", question.getQuestionText());
        arguments.putStringArrayList("AnswersAsArray", (ArrayList)questionMultipleSelect.getAnswers());
        arguments.putIntegerArrayList("OrderingAsArray", (ArrayList)questionMultipleSelect.getOrdering());
        multipleSelectFragment.setArguments(arguments);
        return multipleSelectFragment;
    }
    
    @Override
    public QuestionResponse computeQuestionResponse() {
        final QuestionResponse.Builder builder = QuestionResponse.builder();
        if (this.questionMetrics.isShown()) {
            if (this.ordering != null) {
                builder.setOrdering(this.ordering);
            }
            if (this.isNoneOfTheAboveChecked) {
                this.questionMetrics.markAsAnswered();
            }
            else {
                for (int i = 0; i < this.responses.length; ++i) {
                    if (this.responses[i]) {
                        builder.addResponse(this.answers.get(i));
                        this.questionMetrics.markAsAnswered();
                    }
                }
            }
            builder.setDelayMs(this.questionMetrics.getDelayMs());
        }
        return builder.build();
    }
    
    public View createScrollViewContents() {
        this.answersContainer = (ViewGroup)LayoutInflater.from(this.getContext()).inflate(R.layout.hats_survey_scrollable_answer_content_container, (ViewGroup)null).findViewById(R.id.hats_lib_survey_answers_container);
        for (int i = 0; i < this.answers.size(); ++i) {
            this.addCheckboxToAnswersContainer(this.answers.get(i), this.responses[i], i, null);
        }
        this.addCheckboxToAnswersContainer(this.getResources().getString(R.string.hats_lib_none_of_the_above), this.isNoneOfTheAboveChecked, this.answers.size(), "NoneOfTheAbove");
        return (View)this.answersContainer;
    }
    
    @Override
    String getQuestionText() {
        return this.questionText;
    }
    
    public boolean isResponseSatisfactory() {
        if (this.isNoneOfTheAboveChecked) {
            return true;
        }
        final boolean[] responses = this.responses;
        for (int length = responses.length, i = 0; i < length; ++i) {
            if (responses[i]) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        ((OnQuestionProgressableChangeListener)this.getActivity()).onQuestionProgressableChanged(this.isResponseSatisfactory(), this);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Bundle arguments = this.getArguments();
        this.questionText = arguments.getString("QuestionText");
        this.answers = (ArrayList<String>)arguments.getStringArrayList("AnswersAsArray");
        this.ordering = (ArrayList<Integer>)arguments.getIntegerArrayList("OrderingAsArray");
        if (bundle != null) {
            this.isNoneOfTheAboveChecked = bundle.getBoolean("NoneOfTheAboveAsBoolean", false);
            this.questionMetrics = (QuestionMetrics)bundle.getParcelable("QuestionMetrics");
            this.responses = bundle.getBooleanArray("ResponsesAsArray");
        }
        if (this.questionMetrics == null) {
            this.questionMetrics = new QuestionMetrics();
        }
        if (this.responses == null) {
            this.responses = new boolean[this.answers.size()];
        }
        else if (this.responses.length != this.answers.size()) {
            final int length = this.responses.length;
            final StringBuilder sb = new StringBuilder(64);
            sb.append("Saved instance state responses had incorrect length: ");
            sb.append(length);
            Log.e("HatsLibMultiSelectFrag", sb.toString());
            this.responses = new boolean[this.answers.size()];
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        onCreateView.setContentDescription((CharSequence)this.questionText);
        if (!this.isDetached()) {
            this.fragmentViewDelegate.watch((FragmentViewDelegate.MeasurementSurrogate)this.getActivity(), onCreateView);
        }
        return onCreateView;
    }
    
    @Override
    public void onDetach() {
        this.fragmentViewDelegate.cleanUp();
        super.onDetach();
    }
    
    @Override
    public void onPageScrolledIntoView() {
        this.questionMetrics.markAsShown();
        ((OnQuestionProgressableChangeListener)this.getActivity()).onQuestionProgressableChanged(this.isResponseSatisfactory(), this);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("NoneOfTheAboveAsBoolean", this.isNoneOfTheAboveChecked);
        bundle.putParcelable("QuestionMetrics", (Parcelable)this.questionMetrics);
        bundle.putBooleanArray("ResponsesAsArray", this.responses);
    }
    
    private class CheckboxChangeListener implements CompoundButton$OnCheckedChangeListener
    {
        private final int index;
        
        CheckboxChangeListener(final int index) {
            this.index = index;
        }
        
        private void uncheckAllButNoneOfAbove() {
            if (MultipleSelectFragment.this.answersContainer.getChildCount() != MultipleSelectFragment.this.responses.length + 1) {
                Log.e("HatsLibMultiSelectFrag", "Number of children (checkboxes) contained in the answers container was not equal to the number of possible responses including \"None of the Above\". Note this is not expected to happen in prod.");
            }
            for (int i = 0; i < MultipleSelectFragment.this.answersContainer.getChildCount(); ++i) {
                final CheckBox checkBox = (CheckBox)MultipleSelectFragment.this.answersContainer.getChildAt(i).findViewById(R.id.hats_lib_multiple_select_checkbox);
                if (!"NoneOfTheAbove".equals(checkBox.getTag())) {
                    checkBox.setChecked(false);
                }
            }
        }
        
        public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
            if ("NoneOfTheAbove".equals(compoundButton.getTag())) {
                MultipleSelectFragment.this.isNoneOfTheAboveChecked = b;
                if (b) {
                    this.uncheckAllButNoneOfAbove();
                }
            }
            else {
                MultipleSelectFragment.this.responses[this.index] = b;
                if (b) {
                    ((CheckBox)MultipleSelectFragment.this.answersContainer.findViewWithTag((Object)"NoneOfTheAbove")).setChecked(false);
                }
            }
            final OnQuestionProgressableChangeListener onQuestionProgressableChangeListener = (OnQuestionProgressableChangeListener)MultipleSelectFragment.this.getActivity();
            if (onQuestionProgressableChangeListener != null) {
                onQuestionProgressableChangeListener.onQuestionProgressableChanged(MultipleSelectFragment.this.isResponseSatisfactory(), MultipleSelectFragment.this);
            }
        }
    }
}
