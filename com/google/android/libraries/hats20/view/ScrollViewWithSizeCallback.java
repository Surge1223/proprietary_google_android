package com.google.android.libraries.hats20.view;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.ScrollView;

public class ScrollViewWithSizeCallback extends ScrollView
{
    private OnHeightChangedListener onHeightChangedListener;
    
    public ScrollViewWithSizeCallback(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        if (this.onHeightChangedListener != null && n4 != n2) {
            this.onHeightChangedListener.onHeightChanged(n2);
        }
    }
    
    public void setOnHeightChangedListener(final OnHeightChangedListener onHeightChangedListener) {
        this.onHeightChangedListener = onHeightChangedListener;
    }
    
    interface OnHeightChangedListener
    {
        void onHeightChanged(final int p0);
    }
}
