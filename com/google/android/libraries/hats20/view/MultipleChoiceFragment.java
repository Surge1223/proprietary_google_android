package com.google.android.libraries.hats20.view;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.os.Build.VERSION;
import com.google.android.libraries.hats20.SurveyPromptActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.view.ViewGroup;
import com.google.android.libraries.hats20.R;
import android.view.LayoutInflater;
import java.util.List;
import com.google.android.libraries.hats20.answer.QuestionResponse;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import com.google.android.libraries.hats20.model.QuestionMultipleChoice;
import com.google.android.libraries.hats20.model.Question;
import android.view.View;
import java.util.ArrayList;

public class MultipleChoiceFragment extends ScrollableAnswerFragment
{
    private ArrayList<String> answers;
    private FragmentViewDelegate fragmentViewDelegate;
    private boolean hasSmileys;
    private ArrayList<Integer> ordering;
    private QuestionMetrics questionMetrics;
    private String questionText;
    private String selectedResponse;
    
    public MultipleChoiceFragment() {
        this.hasSmileys = false;
        this.fragmentViewDelegate = new FragmentViewDelegate();
    }
    
    public static MultipleChoiceFragment newInstance(final Question question) {
        final MultipleChoiceFragment multipleChoiceFragment = new MultipleChoiceFragment();
        final String spriteName = ((QuestionMultipleChoice)question).getSpriteName();
        boolean b2;
        final boolean b = b2 = (spriteName != null && spriteName.equals("smileys"));
        if (b) {
            b2 = b;
            if (((QuestionMultipleChoice)question).getAnswers().size() != 5) {
                b2 = false;
                Log.e("HatsLibMultiChoiceFrag", "Multiple choice with smileys survey must have exactly five answers.");
            }
        }
        final Bundle arguments = new Bundle();
        arguments.putString("QuestionText", question.getQuestionText());
        arguments.putStringArrayList("AnswersAsArray", (ArrayList)((QuestionMultipleChoice)question).getAnswers());
        arguments.putIntegerArrayList("OrderingAsArray", (ArrayList)((QuestionMultipleChoice)question).getOrdering());
        arguments.putBoolean("Smileys", b2);
        multipleChoiceFragment.setArguments(arguments);
        return multipleChoiceFragment;
    }
    
    private void removeOnClickListenersAndDisableClickEvents(final View[] array) {
        for (final View view : array) {
            view.setOnClickListener((View.OnClickListener)null);
            view.setClickable(false);
        }
    }
    
    @Override
    public QuestionResponse computeQuestionResponse() {
        final QuestionResponse.Builder builder = QuestionResponse.builder();
        if (this.questionMetrics.isShown()) {
            if (this.selectedResponse != null) {
                builder.addResponse(this.selectedResponse);
            }
            builder.setDelayMs(this.questionMetrics.getDelayMs());
            if (this.ordering != null) {
                builder.setOrdering(this.ordering);
            }
        }
        return builder.build();
    }
    
    public View createScrollViewContents() {
        final LayoutInflater from = LayoutInflater.from(this.getContext());
        final View inflate = from.inflate(R.layout.hats_survey_scrollable_answer_content_container, (ViewGroup)null);
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(R.id.hats_lib_survey_answers_container);
        final View[] array = new View[this.answers.size()];
        for (int i = 0; i < this.answers.size(); ++i) {
            if (this.hasSmileys) {
                from.inflate(R.layout.hats_survey_question_multiple_choice_with_smileys_item, (ViewGroup)linearLayout, true);
                array[i] = linearLayout.getChildAt(linearLayout.getChildCount() - 1);
                final TextView textView = (TextView)array[i].findViewById(R.id.hats_lib_survey_multiple_choice_text);
                textView.setText((CharSequence)this.answers.get(i));
                textView.setContentDescription((CharSequence)this.answers.get(i));
                ((ImageView)array[i].findViewById(R.id.hats_lib_survey_multiple_choice_icon)).setImageResource((int)QuestionMultipleChoice.READONLY_SURVEY_RATING_ICON_RESOURCE_MAP.get(i));
            }
            else {
                from.inflate(R.layout.hats_survey_question_multiple_choice_item, (ViewGroup)linearLayout, true);
                array[i] = linearLayout.getChildAt(linearLayout.getChildCount() - 1);
                ((Button)array[i]).setText((CharSequence)this.answers.get(i));
                ((Button)array[i]).setContentDescription((CharSequence)this.answers.get(i));
            }
            array[i].setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    MultipleChoiceFragment.this.removeOnClickListenersAndDisableClickEvents(array);
                    ((SurveyPromptActivity)MultipleChoiceFragment.this.getActivity()).setIsMultipleChoiceSelectionAnimating(true);
                    view.postOnAnimationDelayed((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            final SurveyPromptActivity surveyPromptActivity = (SurveyPromptActivity)MultipleChoiceFragment.this.getActivity();
                            final boolean b = Build.VERSION.SDK_INT >= 17 && surveyPromptActivity.isDestroyed();
                            if (surveyPromptActivity != null && !surveyPromptActivity.isFinishing() && !b) {
                                MultipleChoiceFragment.this.selectedResponse = MultipleChoiceFragment.this.answers.get(i);
                                MultipleChoiceFragment.this.questionMetrics.markAsAnswered();
                                final String value = String.valueOf(MultipleChoiceFragment.this.answers.get(i));
                                String concat;
                                if (value.length() != 0) {
                                    concat = "User selected response: ".concat(value);
                                }
                                else {
                                    concat = new String("User selected response: ");
                                }
                                Log.d("HatsLibMultiChoiceFrag", concat);
                                surveyPromptActivity.nextPageOrSubmit();
                                surveyPromptActivity.setIsMultipleChoiceSelectionAnimating(false);
                                return;
                            }
                            Log.w("HatsLibMultiChoiceFrag", "Activity was null, finishing or destroyed while attempting to navigate to the next next page. Likely the user rotated the device before the Runnable executed.");
                        }
                    }, 200L);
                }
            });
        }
        return inflate;
    }
    
    @Override
    String getQuestionText() {
        return this.questionText;
    }
    
    public boolean isResponseSatisfactory() {
        return false;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Bundle arguments = this.getArguments();
        this.questionText = arguments.getString("QuestionText");
        this.answers = (ArrayList<String>)arguments.getStringArrayList("AnswersAsArray");
        this.ordering = (ArrayList<Integer>)arguments.getIntegerArrayList("OrderingAsArray");
        this.hasSmileys = arguments.getBoolean("Smileys");
        if (bundle != null) {
            this.selectedResponse = bundle.getString("SelectedResponse", (String)null);
            this.questionMetrics = (QuestionMetrics)bundle.getParcelable("QuestionMetrics");
        }
        if (this.questionMetrics == null) {
            this.questionMetrics = new QuestionMetrics();
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        onCreateView.setContentDescription((CharSequence)this.questionText);
        if (!this.isDetached()) {
            this.fragmentViewDelegate.watch((FragmentViewDelegate.MeasurementSurrogate)this.getActivity(), onCreateView);
        }
        return onCreateView;
    }
    
    @Override
    public void onDetach() {
        this.fragmentViewDelegate.cleanUp();
        super.onDetach();
    }
    
    @Override
    public void onPageScrolledIntoView() {
        this.questionMetrics.markAsShown();
        ((OnQuestionProgressableChangeListener)this.getActivity()).onQuestionProgressableChanged(this.isResponseSatisfactory(), this);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("SelectedResponse", this.selectedResponse);
        bundle.putParcelable("QuestionMetrics", (Parcelable)this.questionMetrics);
    }
}
