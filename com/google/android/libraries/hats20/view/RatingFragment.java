package com.google.android.libraries.hats20.view;

import android.content.res.Resources;
import com.google.android.libraries.hats20.util.LayoutUtils;
import android.util.TypedValue;
import android.view.View$MeasureSpec;
import com.google.android.libraries.material.autoresizetext.AutoResizeTextView;
import com.google.android.libraries.hats20.SurveyPromptActivity;
import android.widget.ImageView;
import android.support.v4.app.Fragment;
import com.google.android.libraries.hats20.ui.StarRatingBar;
import com.google.android.libraries.hats20.util.TextFormatUtil;
import com.google.android.libraries.hats20.R;
import android.view.LayoutInflater;
import android.util.Log;
import com.google.android.libraries.hats20.answer.QuestionResponse;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.os.Parcelable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.libraries.hats20.model.QuestionRating;

public class RatingFragment extends BaseFragment
{
    private FragmentViewDelegate fragmentViewDelegate;
    private QuestionRating question;
    private QuestionMetrics questionMetrics;
    private String selectedResponse;
    
    public RatingFragment() {
        this.fragmentViewDelegate = new FragmentViewDelegate();
    }
    
    public static RatingFragment newInstance(final QuestionRating questionRating) {
        final RatingFragment ratingFragment = new RatingFragment();
        final Bundle arguments = new Bundle();
        arguments.putParcelable("Question", (Parcelable)questionRating);
        ratingFragment.setArguments(arguments);
        return ratingFragment;
    }
    
    private void removeMarginIfNeeded(final View view, final int n, final int n2) {
        if (n != 0 && n != n2 - 1) {
            return;
        }
        final LinearLayout$LayoutParams layoutParams = (LinearLayout$LayoutParams)view.getLayoutParams();
        if (n == 0) {
            layoutParams.setMargins(0, layoutParams.topMargin, layoutParams.rightMargin, layoutParams.bottomMargin);
        }
        else if (n == n2 - 1) {
            layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, 0, layoutParams.bottomMargin);
        }
        view.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    }
    
    private void removeOnClickListenersAndDisableClickEvents(final ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); ++i) {
            viewGroup.getChildAt(i).setOnClickListener((View.OnClickListener)null);
            viewGroup.getChildAt(i).setClickable(false);
        }
    }
    
    private void setDescriptionForTalkBack(final View view, final int n, final int n2) {
        final String format = String.format("%d of %d", n, n2);
        String contentDescription;
        if (n == 1) {
            final String value = String.valueOf(format);
            final String value2 = String.valueOf(this.question.getLowValueText());
            final StringBuilder sb = new StringBuilder(1 + String.valueOf(value).length() + String.valueOf(value2).length());
            sb.append(value);
            sb.append(" ");
            sb.append(value2);
            contentDescription = sb.toString();
        }
        else {
            contentDescription = format;
            if (n == n2) {
                final String value3 = String.valueOf(format);
                final String value4 = String.valueOf(this.question.getHighValueText());
                final StringBuilder sb2 = new StringBuilder(1 + String.valueOf(value3).length() + String.valueOf(value4).length());
                sb2.append(value3);
                sb2.append(" ");
                sb2.append(value4);
                contentDescription = sb2.toString();
            }
        }
        view.setContentDescription((CharSequence)contentDescription);
    }
    
    private void setTextAndContentDescription(final TextView textView, final String s) {
        textView.setText((CharSequence)s);
        textView.setContentDescription((CharSequence)s);
    }
    
    @Override
    public QuestionResponse computeQuestionResponse() {
        final QuestionResponse.Builder builder = QuestionResponse.builder();
        if (this.questionMetrics.isShown()) {
            builder.setDelayMs(this.questionMetrics.getDelayMs());
            if (this.selectedResponse != null) {
                builder.addResponse(this.selectedResponse);
                final String value = String.valueOf(this.selectedResponse);
                String concat;
                if (value.length() != 0) {
                    concat = "Selected response: ".concat(value);
                }
                else {
                    concat = new String("Selected response: ");
                }
                Log.d("HatsLibRatingFragment", concat);
            }
        }
        return builder.build();
    }
    
    public boolean isResponseSatisfactory() {
        return this.selectedResponse != null;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.question = (QuestionRating)this.getArguments().getParcelable("Question");
        if (bundle != null) {
            this.selectedResponse = bundle.getString("SelectedResponse", (String)null);
            this.questionMetrics = (QuestionMetrics)bundle.getParcelable("QuestionMetrics");
        }
        if (this.questionMetrics == null) {
            this.questionMetrics = new QuestionMetrics();
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(R.layout.hats_survey_question_rating, viewGroup, false);
        inflate.setContentDescription((CharSequence)this.question.getQuestionText());
        final TextView textView = (TextView)inflate.findViewById(R.id.hats_lib_survey_question_text);
        textView.setText((CharSequence)TextFormatUtil.format(this.question.getQuestionText()));
        textView.setContentDescription((CharSequence)this.question.getQuestionText());
        this.setTextAndContentDescription((TextView)inflate.findViewById(R.id.hats_lib_survey_rating_low_value_text), this.question.getLowValueText());
        this.setTextAndContentDescription((TextView)inflate.findViewById(R.id.hats_lib_survey_rating_high_value_text), this.question.getHighValueText());
        final ViewGroup viewGroup2 = (ViewGroup)inflate.findViewById(R.id.hats_lib_survey_rating_images_container);
        final StarRatingBar starRatingBar = (StarRatingBar)inflate.findViewById(R.id.hats_lib_star_rating_bar);
        switch (this.question.getSprite()) {
            default: {
                final String value = String.valueOf(this.question.getSprite());
                final StringBuilder sb = new StringBuilder(15 + String.valueOf(value).length());
                sb.append("Unknown sprite ");
                sb.append(value);
                throw new IllegalStateException(sb.toString());
            }
            case STARS: {
                starRatingBar.setVisibility(0);
                starRatingBar.setNumStars(this.question.getNumIcons());
                starRatingBar.setOnRatingChangeListener((StarRatingBar.OnRatingChangeListener)new StarRatingBar.OnRatingChangeListener() {
                    @Override
                    public void onRatingChanged(final int n) {
                        RatingFragment.this.setDescriptionForTalkBack(starRatingBar, n, RatingFragment.this.question.getNumIcons());
                        RatingFragment.this.questionMetrics.markAsAnswered();
                        RatingFragment.this.selectedResponse = Integer.toString(n);
                        ((OnQuestionProgressableChangeListener)RatingFragment.this.getActivity()).onQuestionProgressableChanged(RatingFragment.this.isResponseSatisfactory(), RatingFragment.this);
                    }
                });
                break;
            }
            case SMILEYS: {
                viewGroup2.setVisibility(0);
                for (int i = 0; i < 5; ++i) {
                    final View inflate2 = layoutInflater.inflate(R.layout.hats_survey_question_rating_item, viewGroup2, false);
                    ((ImageView)inflate2.findViewById(R.id.hats_lib_survey_rating_icon)).setImageResource((int)QuestionRating.READONLY_SURVEY_RATING_ICON_RESOURCE_MAP.get(i));
                    final int n = i + 1;
                    inflate2.setTag((Object)n);
                    this.setDescriptionForTalkBack(inflate2, n, 5);
                    inflate2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                        public void onClick(final View view) {
                            RatingFragment.this.removeOnClickListenersAndDisableClickEvents(viewGroup2);
                            final int val$rating = n;
                            final StringBuilder sb = new StringBuilder(35);
                            sb.append("Rating selected, value: ");
                            sb.append(val$rating);
                            Log.d("HatsLibRatingFragment", sb.toString());
                            RatingFragment.this.questionMetrics.markAsAnswered();
                            RatingFragment.this.selectedResponse = Integer.toString(n);
                            ((SurveyPromptActivity)RatingFragment.this.getActivity()).nextPageOrSubmit();
                        }
                    });
                    this.removeMarginIfNeeded(inflate2, i, 5);
                    viewGroup2.addView(inflate2);
                }
                break;
            }
        }
        this.updateRatingQuestionTextSize((AutoResizeTextView)inflate.findViewById(R.id.hats_lib_survey_question_text));
        if (!this.isDetached()) {
            this.fragmentViewDelegate.watch((FragmentViewDelegate.MeasurementSurrogate)this.getActivity(), inflate);
        }
        return inflate;
    }
    
    @Override
    public void onDetach() {
        this.fragmentViewDelegate.cleanUp();
        super.onDetach();
    }
    
    @Override
    public void onPageScrolledIntoView() {
        this.questionMetrics.markAsShown();
        ((OnQuestionProgressableChangeListener)this.getActivity()).onQuestionProgressableChanged(this.isResponseSatisfactory(), this);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("SelectedResponse", this.selectedResponse);
        bundle.putParcelable("QuestionMetrics", (Parcelable)this.questionMetrics);
    }
    
    public void updateRatingQuestionTextSize(final AutoResizeTextView autoResizeTextView) {
        final Resources resources = this.getResources();
        LayoutUtils.fitTextInTextViewWrapIfNeeded(View$MeasureSpec.getSize(((FragmentViewDelegate.MeasurementSurrogate)this.getActivity()).getMeasureSpecs().x) - (resources.getDimensionPixelSize(R.dimen.hats_lib_rating_container_padding) * 2 + TypedValue.applyDimension(1, 24.0f, resources.getDisplayMetrics()) + TypedValue.applyDimension(1, 40.0f, resources.getDisplayMetrics())), 20, 16, this.question.getQuestionText(), autoResizeTextView);
    }
}
