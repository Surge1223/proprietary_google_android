package com.google.android.libraries.hats20.view;

import com.google.android.libraries.hats20.answer.QuestionResponse;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment
{
    public abstract QuestionResponse computeQuestionResponse();
    
    public abstract void onPageScrolledIntoView();
}
