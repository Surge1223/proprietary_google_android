package com.google.android.libraries.hats20.view;

import android.view.MotionEvent;
import com.google.android.libraries.hats20.answer.QuestionResponse;
import java.util.Iterator;
import com.google.android.libraries.hats20.adapter.SurveyViewPagerAdapter;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.google.android.libraries.hats20.SurveyPromptActivity;
import com.google.android.libraries.hats20.R;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v4.view.ViewPager;

public class SurveyViewPager extends ViewPager
{
    public SurveyViewPager(final Context context) {
        super(context);
        this.setUpSurveyViewPager();
    }
    
    public SurveyViewPager(final Context context, final AttributeSet set) {
        super(context, set);
        this.setUpSurveyViewPager();
    }
    
    private void setUpSurveyViewPager() {
        this.setPageMargin(this.getResources().getDimensionPixelSize(R.dimen.hats_lib_survey_page_margin));
        this.setOffscreenPageLimit(Integer.MAX_VALUE);
        this.addOnPageChangeListener((OnPageChangeListener)new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int n) {
                super.onPageSelected(n);
                SurveyViewPager.this.fireOnPageScrolledIntoViewListener();
            }
        });
    }
    
    public void fireOnPageScrolledIntoViewListener() {
        this.getCurrentItemFragment().onPageScrolledIntoView();
    }
    
    public BaseFragment getCurrentItemFragment() {
        if (!(this.getContext() instanceof SurveyPromptActivity)) {
            Log.e("HatsLibSurveyViewPager", "Context is not a SurveyPromptActivity, something is very wrong.");
            return null;
        }
        final int currentItem = this.getCurrentItem();
        for (final Fragment fragment : ((SurveyPromptActivity)this.getContext()).getSupportFragmentManager().getFragments()) {
            if (SurveyViewPagerAdapter.getQuestionIndex(fragment) == currentItem && fragment instanceof BaseFragment) {
                return (BaseFragment)fragment;
            }
        }
        Log.e("HatsLibSurveyViewPager", "No Fragment found for the current item, something is very wrong.");
        return null;
    }
    
    public QuestionResponse getCurrentItemQuestionResponse() {
        QuestionResponse computeQuestionResponse;
        if (this.getCurrentItemFragment() == null) {
            computeQuestionResponse = null;
        }
        else {
            computeQuestionResponse = this.getCurrentItemFragment().computeQuestionResponse();
        }
        return computeQuestionResponse;
    }
    
    public boolean isLastQuestion() {
        final int currentItem = this.getCurrentItem();
        final int count = this.getAdapter().getCount();
        boolean b = true;
        if (currentItem != count - 1) {
            b = false;
        }
        return b;
    }
    
    public void navigateToNextPage() {
        this.setCurrentItem(this.getCurrentItem() + 1, true);
    }
    
    @Override
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        return false;
    }
    
    @Override
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        return false;
    }
}
