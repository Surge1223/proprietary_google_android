package com.google.android.libraries.hats20.view;

import android.support.v4.app.Fragment;

public interface OnQuestionProgressableChangeListener
{
    void onQuestionProgressableChanged(final boolean p0, final Fragment p1);
}
