package com.google.android.libraries.hats20.view;

import android.graphics.Point;
import android.view.View;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;

public class FragmentViewDelegate implements ViewTreeObserver$OnGlobalLayoutListener
{
    private View fragmentView;
    private MeasurementSurrogate measurementSurrogate;
    
    public void cleanUp() {
        if (this.fragmentView != null) {
            this.fragmentView.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        this.fragmentView = null;
        this.measurementSurrogate = null;
    }
    
    public void onGlobalLayout() {
        final Point measureSpecs = this.measurementSurrogate.getMeasureSpecs();
        this.fragmentView.measure(measureSpecs.x, measureSpecs.y);
        this.measurementSurrogate.onFragmentContentMeasurement(this.fragmentView.getMeasuredWidth(), this.fragmentView.getMeasuredHeight());
        this.cleanUp();
    }
    
    public void watch(final MeasurementSurrogate measurementSurrogate, final View fragmentView) {
        this.measurementSurrogate = measurementSurrogate;
        this.fragmentView = fragmentView;
        fragmentView.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
    }
    
    public interface MeasurementSurrogate
    {
        Point getMeasureSpecs();
        
        void onFragmentContentMeasurement(final int p0, final int p1);
    }
}
