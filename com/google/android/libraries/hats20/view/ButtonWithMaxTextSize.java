package com.google.android.libraries.hats20.view;

import android.content.res.TypedArray;
import com.google.android.libraries.hats20.R;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.widget.AppCompatButton;

public class ButtonWithMaxTextSize extends AppCompatButton
{
    public ButtonWithMaxTextSize(final Context context) {
        super(context);
    }
    
    public ButtonWithMaxTextSize(final Context context, final AttributeSet set) {
        super(context, set);
        this.capTextSize(context, set);
    }
    
    public ButtonWithMaxTextSize(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.capTextSize(context, set);
    }
    
    private void capTextSize(final Context context, final AttributeSet set) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ButtonWithMaxTextSize);
        this.setTextSize(0, Math.min(this.getTextSize(), obtainStyledAttributes.getDimensionPixelSize(R.styleable.ButtonWithMaxTextSize_textSizeMaxDp, Integer.MAX_VALUE)));
        obtainStyledAttributes.recycle();
    }
}
