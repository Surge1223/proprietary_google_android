package com.google.android.libraries.hats20.view;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.widget.LinearLayout;
import android.view.ViewGroup;
import com.google.android.libraries.hats20.R;
import android.view.LayoutInflater;
import android.view.View;
import com.google.android.libraries.hats20.answer.QuestionResponse;
import android.view.inputmethod.InputMethodManager;
import android.os.Bundle;
import com.google.android.libraries.hats20.model.QuestionOpenText;
import com.google.android.libraries.hats20.model.Question;
import android.widget.EditText;

public class OpenTextFragment extends ScrollableAnswerFragment
{
    private EditText editTextField;
    private FragmentViewDelegate fragmentViewDelegate;
    private boolean isSingleLine;
    private QuestionMetrics questionMetrics;
    private String questionText;
    
    public OpenTextFragment() {
        this.fragmentViewDelegate = new FragmentViewDelegate();
    }
    
    public static OpenTextFragment newInstance(final Question question) {
        final OpenTextFragment openTextFragment = new OpenTextFragment();
        final QuestionOpenText questionOpenText = (QuestionOpenText)question;
        final Bundle arguments = new Bundle();
        arguments.putString("QuestionText", questionOpenText.getQuestionText());
        arguments.putBoolean("IsSingleLine", questionOpenText.isSingleLine());
        openTextFragment.setArguments(arguments);
        return openTextFragment;
    }
    
    public void closeKeyboard() {
        ((InputMethodManager)this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.editTextField.getWindowToken(), 0);
    }
    
    @Override
    public QuestionResponse computeQuestionResponse() {
        final QuestionResponse.Builder builder = QuestionResponse.builder();
        if (this.questionMetrics.isShown()) {
            this.questionMetrics.markAsAnswered();
            builder.setDelayMs(this.questionMetrics.getDelayMs());
            final String string = this.editTextField.getText().toString();
            if (string.trim().isEmpty()) {
                builder.addResponse("skipped");
            }
            else {
                builder.addResponse(string);
            }
        }
        return builder.build();
    }
    
    @Override
    View createScrollViewContents() {
        final LayoutInflater from = LayoutInflater.from(this.getContext());
        final View inflate = from.inflate(R.layout.hats_survey_scrollable_answer_content_container, (ViewGroup)null);
        inflate.setMinimumHeight(this.getResources().getDimensionPixelSize(R.dimen.hats_lib_open_text_question_min_height));
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(R.id.hats_lib_survey_answers_container);
        from.inflate(R.layout.hats_survey_question_open_text_item, (ViewGroup)linearLayout, true);
        (this.editTextField = (EditText)linearLayout.findViewById(R.id.hats_lib_survey_open_text)).setSingleLine(this.isSingleLine);
        this.editTextField.setHint((CharSequence)this.getResources().getString(R.string.hats_lib_open_text_hint));
        return (View)linearLayout;
    }
    
    @Override
    String getQuestionText() {
        return this.questionText;
    }
    
    public boolean isResponseSatisfactory() {
        return true;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        ((OnQuestionProgressableChangeListener)this.getActivity()).onQuestionProgressableChanged(this.isResponseSatisfactory(), this);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Bundle arguments = this.getArguments();
        this.questionText = arguments.getString("QuestionText");
        this.isSingleLine = arguments.getBoolean("IsSingleLine");
        if (bundle == null) {
            this.questionMetrics = new QuestionMetrics();
        }
        else {
            this.questionMetrics = (QuestionMetrics)bundle.getParcelable("QuestionMetrics");
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        onCreateView.setContentDescription((CharSequence)this.questionText);
        if (!this.isDetached()) {
            this.fragmentViewDelegate.watch((FragmentViewDelegate.MeasurementSurrogate)this.getActivity(), onCreateView);
        }
        return onCreateView;
    }
    
    @Override
    public void onDetach() {
        this.fragmentViewDelegate.cleanUp();
        super.onDetach();
    }
    
    @Override
    public void onPageScrolledIntoView() {
        this.questionMetrics.markAsShown();
        ((OnQuestionProgressableChangeListener)this.getActivity()).onQuestionProgressableChanged(this.isResponseSatisfactory(), this);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("QuestionMetrics", (Parcelable)this.questionMetrics);
    }
}
