package com.google.android.libraries.hats20.answer;

import android.net.Uri.Builder;
import android.net.Uri;
import android.util.Log;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class AnswerBeacon implements Parcelable
{
    public static final Parcelable.Creator<AnswerBeacon> CREATOR;
    private final Bundle parameterBundle;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<AnswerBeacon>() {
            public AnswerBeacon createFromParcel(final Parcel parcel) {
                return new AnswerBeacon(parcel, null);
            }
            
            public AnswerBeacon[] newArray(final int n) {
                return new AnswerBeacon[n];
            }
        };
    }
    
    public AnswerBeacon() {
        (this.parameterBundle = new Bundle()).putString("m.v", "3");
    }
    
    private AnswerBeacon(final Parcel parcel) {
        this.parameterBundle = parcel.readBundle(this.getClass().getClassLoader());
        if (this.parameterBundle != null) {
            return;
        }
        throw new NullPointerException("Parcel did not contain required Bundle while unparceling an AnswerBeacon.");
    }
    
    private void addResponses(final int n, final List<String> list) {
        final Bundle parameterBundle = this.parameterBundle;
        final String value = String.valueOf("r.r-");
        final StringBuilder sb = new StringBuilder(11 + String.valueOf(value).length());
        sb.append(value);
        sb.append(n);
        parameterBundle.putStringArrayList(sb.toString(), new ArrayList((Collection<? extends E>)list));
    }
    
    private static boolean areBundlesEqual(final Bundle bundle, final Bundle bundle2) {
        if (bundle.size() != bundle2.size()) {
            return false;
        }
        final Set keySet = bundle.keySet();
        if (!keySet.containsAll(bundle2.keySet())) {
            return false;
        }
        for (final String s : keySet) {
            final Object value = bundle.get(s);
            final Object value2 = bundle2.get(s);
            if (value == null) {
                if (value2 != null) {
                    return false;
                }
                continue;
            }
            else {
                if (!value.equals(value2)) {
                    return false;
                }
                continue;
            }
        }
        return true;
    }
    
    private void hasWriteIn(final int n, final boolean b) {
        final String value = String.valueOf("r.t-");
        final StringBuilder sb = new StringBuilder(11 + String.valueOf(value).length());
        sb.append(value);
        sb.append(n);
        this.setBooleanParameter(sb.toString(), b);
    }
    
    private AnswerBeacon setBooleanParameter(final String s, final boolean b) {
        if (b) {
            this.parameterBundle.putString(s, "1");
        }
        else {
            this.parameterBundle.remove(s);
        }
        return this;
    }
    
    private void setDelayMsForQuestion(final int n, final long n2) {
        final Bundle parameterBundle = this.parameterBundle;
        final String value = String.valueOf("m.sc-");
        final StringBuilder sb = new StringBuilder(String.valueOf(value).length() + 11);
        sb.append(value);
        sb.append(n);
        parameterBundle.remove(sb.toString());
        final Bundle parameterBundle2 = this.parameterBundle;
        final String value2 = String.valueOf("m.d-");
        final StringBuilder sb2 = new StringBuilder(String.valueOf(value2).length() + 11);
        sb2.append(value2);
        sb2.append(n);
        parameterBundle2.remove(sb2.toString());
        if (n == 0 && n2 < 1500L) {
            final StringBuilder sb3 = new StringBuilder(63);
            sb3.append("First question delay ");
            sb3.append(n2);
            sb3.append(" is considered spammy.");
            Log.d("HatsLibAnswerBeacon", sb3.toString());
            final String value3 = String.valueOf("m.sc-");
            final StringBuilder sb4 = new StringBuilder(11 + String.valueOf(value3).length());
            sb4.append(value3);
            sb4.append(n);
            this.setLongParameter(sb4.toString(), n2);
        }
        else {
            final String value4 = String.valueOf("m.d-");
            final StringBuilder sb5 = new StringBuilder(11 + String.valueOf(value4).length());
            sb5.append(value4);
            sb5.append(n);
            this.setLongParameter(sb5.toString(), n2);
        }
    }
    
    private AnswerBeacon setLongParameter(final String s, final long n) {
        if (n < 0L) {
            this.parameterBundle.remove(s);
        }
        else {
            this.parameterBundle.putLong(s, n);
        }
        return this;
    }
    
    private void setOrdering(final int n, final String s) {
        final String value = String.valueOf("r.o-");
        final StringBuilder sb = new StringBuilder(11 + String.valueOf(value).length());
        sb.append(value);
        sb.append(n);
        this.setStringParameter(sb.toString(), s);
    }
    
    private AnswerBeacon setStringParameter(final String s, final String s2) {
        if (s2 == null) {
            this.parameterBundle.remove(s);
        }
        else {
            this.parameterBundle.putString(s, s2);
        }
        return this;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AnswerBeacon && areBundlesEqual(this.parameterBundle, ((AnswerBeacon)o).parameterBundle);
    }
    
    public Uri exportAllParametersInQueryString() {
        final Uri.Builder uri$Builder = new Uri.Builder();
        this.setLongParameter("m.lt", System.currentTimeMillis() / 1000L);
        for (final String s : this.parameterBundle.keySet()) {
            final Object value = this.parameterBundle.get(s);
            if (value instanceof List) {
                final Iterator<Object> iterator2 = ((List<Object>)value).iterator();
                while (iterator2.hasNext()) {
                    uri$Builder.appendQueryParameter(s, String.valueOf(iterator2.next()));
                }
            }
            else {
                if (value == null) {
                    continue;
                }
                uri$Builder.appendQueryParameter(s, String.valueOf(value));
            }
        }
        if (this.hasBeaconTypeOtherAccess()) {
            uri$Builder.appendQueryParameter("m.sh", "close");
        }
        uri$Builder.appendQueryParameter("d", "1");
        return uri$Builder.build();
    }
    
    public boolean hasBeaconType() {
        return this.parameterBundle.getString("t") != null;
    }
    
    public boolean hasBeaconTypeFullAnswer() {
        return "a".equals(this.parameterBundle.getString("t"));
    }
    
    public boolean hasBeaconTypeOtherAccess() {
        return "o".equals(this.parameterBundle.getString("t"));
    }
    
    @Override
    public int hashCode() {
        return this.parameterBundle.keySet().hashCode();
    }
    
    public AnswerBeacon setBeaconType(final String s) {
        if (s != null) {
            return this.setStringParameter("t", s);
        }
        throw new NullPointerException("Beacon type cannot be null.");
    }
    
    public AnswerBeacon setPromptParams(final String s) {
        return this.setStringParameter("p", s);
    }
    
    public AnswerBeacon setQuestionResponse(final int n, final QuestionResponse questionResponse) {
        this.setDelayMsForQuestion(n, questionResponse.getDelayMs());
        this.setOrdering(n, questionResponse.getOrdering());
        this.hasWriteIn(n, questionResponse.hasWriteIn());
        this.addResponses(n, questionResponse.getResponses());
        return this;
    }
    
    public AnswerBeacon setShown(final int n) {
        final String value = String.valueOf("r.s-");
        final StringBuilder sb = new StringBuilder(11 + String.valueOf(value).length());
        sb.append(value);
        sb.append(n);
        this.setBooleanParameter(sb.toString(), true);
        return this;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.exportAllParametersInQueryString().toString().replace("&", "\n"));
        final StringBuilder sb = new StringBuilder(14 + String.valueOf(value).length());
        sb.append("AnswerBeacon{");
        sb.append(value);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeBundle(this.parameterBundle);
    }
}
