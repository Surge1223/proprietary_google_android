package com.google.android.libraries.hats20.answer;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class QuestionResponse implements Parcelable
{
    public static final Parcelable.Creator<QuestionResponse> CREATOR;
    private final long delayMs;
    private final boolean hasWriteIn;
    private final String ordering;
    private final List<String> responses;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<QuestionResponse>() {
            public QuestionResponse createFromParcel(final Parcel parcel) {
                return new QuestionResponse(parcel, null);
            }
            
            public QuestionResponse[] newArray(final int n) {
                return new QuestionResponse[n];
            }
        };
    }
    
    private QuestionResponse(final Parcel parcel) {
        this.delayMs = parcel.readLong();
        this.responses = Collections.unmodifiableList((List<? extends String>)parcel.createStringArrayList());
        this.ordering = parcel.readString();
        this.hasWriteIn = (parcel.readByte() != 0);
    }
    
    private QuestionResponse(final Builder builder) {
        this.delayMs = builder.delayMs;
        this.responses = Collections.unmodifiableList((List<? extends String>)builder.responses);
        this.ordering = builder.ordering;
        this.hasWriteIn = builder.hasWriteIn;
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final QuestionResponse questionResponse = (QuestionResponse)o;
            return this.delayMs == questionResponse.delayMs && this.hasWriteIn == questionResponse.hasWriteIn && this.responses.equals(questionResponse.responses) && this.ordering.equals(questionResponse.ordering);
        }
        return false;
    }
    
    long getDelayMs() {
        return this.delayMs;
    }
    
    String getOrdering() {
        return this.ordering;
    }
    
    List<String> getResponses() {
        return this.responses;
    }
    
    boolean hasWriteIn() {
        return this.hasWriteIn;
    }
    
    @Override
    public int hashCode() {
        return 31 * (31 * (31 * (int)(this.delayMs ^ this.delayMs >>> 32) + this.responses.hashCode()) + this.ordering.hashCode()) + (this.hasWriteIn ? 1 : 0);
    }
    
    @Override
    public String toString() {
        final long delayMs = this.delayMs;
        final String value = String.valueOf(this.responses);
        final String ordering = this.ordering;
        final boolean hasWriteIn = this.hasWriteIn;
        final StringBuilder sb = new StringBuilder(89 + String.valueOf(value).length() + String.valueOf(ordering).length());
        sb.append("QuestionResponse{delayMs=");
        sb.append(delayMs);
        sb.append(", responses=");
        sb.append(value);
        sb.append(", ordering='");
        sb.append(ordering);
        sb.append("'");
        sb.append(", hasWriteIn=");
        sb.append(hasWriteIn);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.delayMs);
        parcel.writeStringList((List)this.responses);
        parcel.writeString(this.ordering);
        parcel.writeByte((byte)(byte)(this.hasWriteIn ? 1 : 0));
    }
    
    public static class Builder
    {
        private long delayMs;
        private boolean hasWriteIn;
        private String ordering;
        private ArrayList<String> responses;
        
        private Builder() {
            this.delayMs = -1L;
            this.responses = new ArrayList<String>(1);
            this.ordering = null;
            this.hasWriteIn = false;
        }
        
        public Builder addResponse(final String s) {
            this.responses.add(s);
            return this;
        }
        
        public QuestionResponse build() {
            return new QuestionResponse(this, null);
        }
        
        public Builder setDelayMs(final long delayMs) {
            this.delayMs = delayMs;
            return this;
        }
        
        public Builder setOrdering(final List<Integer> list) {
            if (!list.isEmpty()) {
                final Iterator<Integer> iterator = list.iterator();
                this.ordering = iterator.next().toString();
                while (iterator.hasNext()) {
                    final String value = String.valueOf(this.ordering);
                    final String value2 = String.valueOf(iterator.next());
                    final StringBuilder sb = new StringBuilder(1 + String.valueOf(value).length() + String.valueOf(value2).length());
                    sb.append(value);
                    sb.append(".");
                    sb.append(value2);
                    this.ordering = sb.toString();
                }
                return this;
            }
            throw new IllegalArgumentException("Must specify at least one ordering entry.");
        }
    }
}
