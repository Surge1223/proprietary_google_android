package com.google.android.libraries.hats20.answer;

import java.net.URISyntaxException;
import java.io.IOException;
import android.util.Log;
import java.io.DataOutputStream;
import com.google.android.libraries.hats20.network.GcsRequest;
import java.net.HttpURLConnection;
import java.net.URL;
import android.net.Uri;
import android.os.AsyncTask;
import com.google.android.libraries.hats20.storage.HatsDataStore;
import java.util.concurrent.Executor;

public class AnswerBeaconTransmitter
{
    private final String answerUrl;
    private final Executor executor;
    private final HatsDataStore hatsDataStore;
    
    public AnswerBeaconTransmitter(final String s, final HatsDataStore hatsDataStore) {
        this(s, hatsDataStore, AsyncTask.THREAD_POOL_EXECUTOR);
    }
    
    AnswerBeaconTransmitter(final String answerUrl, final HatsDataStore hatsDataStore, final Executor executor) {
        if (answerUrl == null) {
            throw new NullPointerException("Answer URL was missing");
        }
        if (hatsDataStore == null) {
            throw new NullPointerException("HaTS cookie store was missing");
        }
        if (executor != null) {
            this.answerUrl = answerUrl;
            this.hatsDataStore = hatsDataStore;
            this.executor = executor;
            return;
        }
        throw new NullPointerException("Executor was missing");
    }
    
    public void transmit(final AnswerBeacon answerBeacon) {
        this.executor.execute(new TransmitTask(answerBeacon.exportAllParametersInQueryString()));
    }
    
    private class TransmitTask implements Runnable
    {
        private final Uri uri;
        
        TransmitTask(final Uri uri) {
            this.uri = uri;
        }
        
        private void transmit() throws IOException, URISyntaxException {
            AnswerBeaconTransmitter.this.hatsDataStore.restoreCookiesFromPersistence();
            final String queryParameter = this.uri.getQueryParameter("t");
            final URL url = new URL(AnswerBeaconTransmitter.this.answerUrl);
            final byte[] bytes = this.uri.getEncodedQuery().getBytes("UTF-8");
            final HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            try {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setRequestProperty("Content-Length", Integer.toString(bytes.length));
                httpURLConnection.setRequestProperty("charset", "utf-8");
                httpURLConnection.setRequestProperty("Connection", "close");
                httpURLConnection.setRequestProperty("User-Agent", GcsRequest.USER_AGENT);
                httpURLConnection.setUseCaches(false);
                new DataOutputStream(httpURLConnection.getOutputStream()).write(bytes);
                final int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    final String value = String.valueOf(queryParameter);
                    String concat;
                    if (value.length() != 0) {
                        concat = "Successfully transmitted answer beacon of type: ".concat(value);
                    }
                    else {
                        concat = new String("Successfully transmitted answer beacon of type: ");
                    }
                    Log.d("HatsLibTransmitter", concat);
                    AnswerBeaconTransmitter.this.hatsDataStore.storeSetCookieHeaders(Uri.parse(AnswerBeaconTransmitter.this.answerUrl), httpURLConnection.getHeaderFields());
                }
                else {
                    final StringBuilder sb = new StringBuilder(74 + String.valueOf(queryParameter).length());
                    sb.append("Failed to transmit answer beacon of type: ");
                    sb.append(queryParameter);
                    sb.append("; response code was: ");
                    sb.append(responseCode);
                    Log.e("HatsLibTransmitter", sb.toString());
                }
            }
            finally {
                httpURLConnection.disconnect();
            }
        }
        
        @Override
        public void run() {
            try {
                if (AnswerBeaconTransmitter.this.answerUrl.equals("/")) {
                    Log.d("HatsLibTransmitter", "Skipping transmission of beacon since answerUrl was \"/\", this should only happen during debugging.");
                }
                else {
                    this.transmit();
                }
            }
            catch (Exception ex) {
                Log.e("HatsLibTransmitter", "Transmission of answer beacon failed.", (Throwable)ex);
            }
        }
    }
}
