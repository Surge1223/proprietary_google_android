package com.google.android.libraries.hats20;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executor;

public final class NetworkExecutor
{
    private static volatile Executor networkExecutor;
    private static final Object networkExecutorLock;
    
    static {
        networkExecutorLock = new Object();
    }
    
    public static Executor getNetworkExecutor() {
        if (NetworkExecutor.networkExecutor == null) {
            synchronized (NetworkExecutor.networkExecutorLock) {
                if (NetworkExecutor.networkExecutor == null) {
                    NetworkExecutor.networkExecutor = new ThreadPoolExecutor(1, 3, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
                        private final AtomicInteger threadCount = new AtomicInteger(1);
                        
                        @Override
                        public Thread newThread(final Runnable runnable) {
                            final int andIncrement = this.threadCount.getAndIncrement();
                            final StringBuilder sb = new StringBuilder(17);
                            sb.append("HaTS #");
                            sb.append(andIncrement);
                            return new Thread(runnable, sb.toString());
                        }
                    });
                    ((ThreadPoolExecutor)NetworkExecutor.networkExecutor).allowCoreThreadTimeOut(true);
                }
            }
        }
        return NetworkExecutor.networkExecutor;
    }
}
