package com.google.android.settings.phenotype;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.phenotype.Phenotype;
import com.google.android.gms.phenotype.PhenotypeFlagCommitter.Callback;
import com.google.android.gms.phenotype.PhenotypeFlagSharedPrefsCommitter;

public final class PhenotypeBroadcastReceiver extends BroadcastReceiver {

    private static final class CommitCallback implements Callback {
        private final Context mContext;

        public CommitCallback(Context context) {
            this.mContext = context;
        }

        public void onFinish(boolean z) {
        }
    }

    public void onReceive(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("com.google.android.gms.phenotype.PACKAGE_NAME");
        if ("com.google.android.settings".equals(stringExtra)) {
            Context applicationContext = context.getApplicationContext();
            new PhenotypeFlagSharedPrefsCommitter(Phenotype.getInstance(applicationContext), stringExtra, context.getSharedPreferences("SettingsGoogleSharedPrefFile", 0)).commitForUserAsync("", new CommitCallback(applicationContext));
        }
    }
}
