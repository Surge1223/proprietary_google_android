package com.google.android.settings.survey;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.android.internal.annotations.VisibleForTesting;
import android.content.ContentResolver;
import com.google.android.settings.support.PsdValuesLoader;
import com.android.settingslib.utils.AsyncLoader;
import com.google.android.libraries.hats20.HatsShowRequest;
import android.content.Loader;
import com.google.android.gsf.Gservices;
import android.os.Bundle;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.content.BroadcastReceiver;
import android.app.Activity;
import com.google.android.libraries.hats20.HatsClient;
import android.content.Context;
import com.android.settings.overlay.SurveyFeatureProvider;
import com.google.android.libraries.hats20.HatsDownloadRequest;
import android.app.LoaderManager;

public class SurveyFeatureProviderImpl implements LoaderManager.LoaderCallbacks<HatsDownloadRequest>, SurveyFeatureProvider
{
    private Context mContext;
    
    public SurveyFeatureProviderImpl(final Context context) {
        HatsClient.installCookieHandlerIfNeeded();
        this.mContext = context.getApplicationContext();
    }
    
    public BroadcastReceiver createAndRegisterReceiver(final Activity activity) {
        if (activity != null) {
            final SurveyBroadcastReceiver surveyBroadcastReceiver = new SurveyBroadcastReceiver();
            surveyBroadcastReceiver.setActivity(activity);
            LocalBroadcastManager.getInstance((Context)activity).registerReceiver(surveyBroadcastReceiver, new IntentFilter("com.google.android.libraries.hats20.SURVEY_DOWNLOADED"));
            return surveyBroadcastReceiver;
        }
        throw new IllegalStateException("Cannot register receiver if activity is null.");
    }
    
    public void downloadSurvey(final Activity activity, final String s, final String s2) {
        final Bundle bundle = new Bundle(2);
        bundle.putString("survey_id", s);
        bundle.putString("data", s2);
        if (activity != null && s != null) {
            activity.getLoaderManager().initLoader(20, bundle, (LoaderManager.LoaderCallbacks)this);
        }
    }
    
    public long getSurveyExpirationDate(final Context context, final String s) {
        return HatsClient.getSurveyExpirationDate(s, context);
    }
    
    public String getSurveyId(final Context context, final String s) {
        return Gservices.getString(context.getContentResolver(), String.format("settingsgoogle:%s_site_id", s), null);
    }
    
    public Loader<HatsDownloadRequest> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<HatsDownloadRequest>)new SurveyProviderLoader(this.mContext, bundle);
    }
    
    public void onLoadFinished(final Loader<HatsDownloadRequest> loader, final HatsDownloadRequest hatsDownloadRequest) {
        if (hatsDownloadRequest != null) {
            HatsClient.downloadSurvey(hatsDownloadRequest);
        }
    }
    
    public void onLoaderReset(final Loader<HatsDownloadRequest> loader) {
    }
    
    public boolean showSurveyIfAvailable(final Activity activity, final String s) {
        return activity != null && HatsClient.showSurveyIfAvailable(HatsShowRequest.builder(activity).forSiteId(s).build());
    }
    
    public static class SurveyProviderLoader extends AsyncLoader<HatsDownloadRequest>
    {
        private String mData;
        private String mSurveyId;
        
        public SurveyProviderLoader(final Context context, final Bundle bundle) {
            super(context);
            this.mSurveyId = bundle.getString("survey_id", (String)null);
            this.mData = bundle.getString("data", (String)null);
        }
        
        @VisibleForTesting
        String getPayload() {
            final StringBuilder sb = new StringBuilder();
            final Context context = this.getContext();
            final ContentResolver contentResolver = context.getContentResolver();
            int i = 0;
            if (Gservices.getBoolean(contentResolver, "settingsgoogle:survey_payloads_enabled", false)) {
                for (String[] values = PsdValuesLoader.makePsdBundle(context, 1).getValues(); i < values.length; ++i) {
                    sb.append(values[i]);
                    sb.append(",");
                }
                if (this.mData != null) {
                    sb.append(this.mData);
                }
                if (sb.length() > 1000) {
                    sb.setLength(1000);
                }
            }
            return sb.toString();
        }
        
        public HatsDownloadRequest loadInBackground() {
            String id;
            try {
                id = AdvertisingIdClient.getAdvertisingIdInfo(this.getContext()).getId();
            }
            catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException ex) {
                id = null;
            }
            if (id != null && this.mSurveyId != null) {
                return HatsDownloadRequest.builder(this.getContext().getApplicationContext()).forSiteId(this.mSurveyId).withAdvertisingId(id).withSiteContext(this.getPayload()).build();
            }
            return null;
        }
        
        @Override
        protected void onDiscardResult(final HatsDownloadRequest hatsDownloadRequest) {
        }
    }
}
