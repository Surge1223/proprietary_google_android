package com.google.android.settings.search;

import com.android.internal.annotations.VisibleForTesting;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.Indexable;
import androidx.slice.SliceManager;
import android.net.Uri;
import android.content.Context;
import com.android.settings.search.DeviceIndexFeatureProvider;

public class DeviceIndexFeatureProviderGoogleImpl implements DeviceIndexFeatureProvider
{
    private Indexable.Metadata.Builder createMetadataBuilderWithSliceUri(final Context context, final Uri uri, final SliceManager sliceManager) {
        final Indexable.Metadata.Builder builder = new Indexable.Metadata.Builder();
        if (uri == null) {
            return builder;
        }
        sliceManager.grantSlicePermission("com.google.android.gms", uri);
        sliceManager.grantSlicePermission("com.google.android.googlequicksearchbox", uri);
        builder.put("sliceUri", uri.toString());
        return builder;
    }
    
    @Override
    public void clearIndex(final Context context) {
        FirebaseAppIndex.getInstance(context).removeAll();
    }
    
    @Override
    public void index(final Context context, final CharSequence charSequence, final Uri uri, final Uri uri2, final List<String> list) {
        this.index(context, charSequence, uri, uri2, list, SliceManager.getInstance(context));
    }
    
    @VisibleForTesting
    protected void index(final Context context, final CharSequence charSequence, final Uri uri, final Uri uri2, final List<String> list, final SliceManager sliceManager) {
        ArrayList<String> list2;
        if (list != null) {
            list2 = new ArrayList<String>(list);
        }
        else {
            list2 = new ArrayList<String>();
        }
        list2.add(charSequence.toString());
        FirebaseAppIndex.getInstance(context).update(((Indexable.Builder)new Indexable.Builder().setName(charSequence.toString()).setUrl(uri2.toString()).setMetadata(this.createMetadataBuilderWithSliceUri(context, uri, sliceManager).setWorksOffline(true)).put("keywords", (String[])list2.toArray(new String[list2.size()]))).build());
    }
    
    @Override
    public boolean isIndexingEnabled() {
        return true;
    }
}
