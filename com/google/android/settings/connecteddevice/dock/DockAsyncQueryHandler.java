package com.google.android.settings.connecteddevice.dock;

import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;
import android.content.ContentResolver;
import android.content.AsyncQueryHandler;

public class DockAsyncQueryHandler extends AsyncQueryHandler {
    private OnQueryListener mListener;
    
    public DockAsyncQueryHandler(final ContentResolver contentResolver) {
        super(contentResolver);
        this.mListener = null;
    }
    
    public static List<DockDevice> parseCursorToDockDevice(final Cursor cursor) {
        final ArrayList<DockDevice> list = new ArrayList<DockDevice>();
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                list.add(new DockDevice(cursor.getString(cursor.getColumnIndex("dockId")), cursor.getString(cursor.getColumnIndex("dockName"))));
            }
        }
        return list;
    }
    
    protected void onQueryComplete(final int n, final Object o, final Cursor cursor) {
        super.onQueryComplete(n, o, cursor);
        if (this.mListener == null) {
            return;
        }
        this.mListener.onQueryComplete(n, parseCursorToDockDevice(cursor));
    }
    
    public void setOnQueryListener(final OnQueryListener mListener) {
        this.mListener = mListener;
    }
    
    public interface OnQueryListener
    {
        void onQueryComplete(final int p0, final List<DockDevice> p1);
    }
}
