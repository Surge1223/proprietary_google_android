package com.google.android.settings.connecteddevice.dock;

public class DockDevice
{
    private String mId;
    private String mName;
    
    private DockDevice() {
    }
    
    DockDevice(String mId, String mName) {
        this.mId = mId;
        this.mName = mName;
    }
    
    public String getId() {
        return this.mId;
    }
    
    public String getName() {
        return this.mName;
    }
}
