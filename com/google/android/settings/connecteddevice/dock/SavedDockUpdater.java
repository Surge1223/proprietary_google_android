package com.google.android.settings.connecteddevice.dock;

import android.content.ContentProviderClient;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.util.Log;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.support.v7.preference.Preference;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.ArrayMap;
import java.util.Map;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.List;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settings.connecteddevice.dock.DockUpdater;
import com.android.settings.widget.GearPreference;
import com.android.settings.widget.GearPreference.OnGearClickListener;
import com.google.android.settings.connecteddevice.dock.DockAsyncQueryHandler.OnQueryListener;

public class SavedDockUpdater implements DockUpdater, OnGearClickListener, OnQueryListener {
    private DockAsyncQueryHandler mAsyncQueryHandler;
    private String mConnectedDockId;
    private DockObserver mConnectedDockObserver;
    private Context mContext;
    private DevicePreferenceCallback mDevicePreferenceCallback;
    @VisibleForTesting
    boolean mIsObserverRegistered;
    @VisibleForTesting
    Map<String, GearPreference> mPreferenceMap;
    private Map<String, String> mSavedDevices;
    private DockObserver mSavedDockObserver;
    
    public class DockObserver extends ContentObserver {
        private int mToken;
        private Uri mUri;

        DockObserver(Handler handler, int token, Uri uri) {
            super(handler);
            mToken = token;
            mUri = uri;
        }

        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            startQuery(mToken, mUri);
        }
    }
    
    
    public SavedDockUpdater(Context mContext,DevicePreferenceCallback mDevicePreferenceCallback) {
        mSavedDevices = null;
        mConnectedDockId = null;
        mContext = mContext;
        mDevicePreferenceCallback = mDevicePreferenceCallback;
        mPreferenceMap = (Map<String, GearPreference>)new ArrayMap();
        Handler handler = new Handler(Looper.getMainLooper());
        mConnectedDockObserver = new DockObserver(handler, 1, DockContract.DOCK_PROVIDER_CONNECTED_URI);
        mSavedDockObserver = new DockObserver(handler, 2, DockContract.DOCK_PROVIDER_SAVED_URI);
        if (isRunningOnMainThread()) {
            (mAsyncQueryHandler = new DockAsyncQueryHandler(mContext.getContentResolver())).setOnQueryListener((DockAsyncQueryHandler.OnQueryListener)this);
        }
        else {
            mAsyncQueryHandler = null;
        }
    }
    
    public boolean hasDeviceBeenRemoved(String s) {
        if (!mSavedDevices.containsKey(s)) {
            mDevicePreferenceCallback.onDeviceRemoved(mPreferenceMap.get(s));
            return true;
        }
        return false;
    }
    
    private GearPreference initPreference(String key,String title) {
        GearPreference gearPreference = new GearPreference(mContext, null);
        gearPreference.setIcon(2131231003);
        gearPreference.setSelectable(false);
        gearPreference.setTitle(title);
        if (!TextUtils.isEmpty((CharSequence)key)) {
            gearPreference.setOnGearClickListener((GearPreference.OnGearClickListener)this);
            gearPreference.setKey(key);
        }
        return gearPreference;
    }
    
    private boolean isRunningOnMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
    

    public void startQuery(int n, Uri uri) {
        if (this.isRunningOnMainThread()) {
            this.mAsyncQueryHandler.startQuery(n, (Object)this.mContext, uri, DockContract.DOCK_PROJECTION, (String)null, (String[])null, (String)null);
        }
        else {
            try {
                Cursor query = this.mContext.getApplicationContext().getContentResolver().query(uri, DockContract.DOCK_PROJECTION, (String)null, (String[])null, (String)null);
                uri = null;
                try {
                    try {
                        this.onQueryComplete(n, DockAsyncQueryHandler.parseCursorToDockDevice(query));
                        if (query != null) {
                            query.close();
                        }
                    }
                    finally {
                        if (query != null) {
                            if (uri != null) {
                                Cursor cursor = query;
                                cursor.close();
                            }
                            else {
                                query.close();
                            }
                        }
                    }
                }
                catch (Throwable t) {}
                try {
                    Cursor cursor = query;
                    cursor.close();
                }
                catch (Throwable t2) {}
            }
            catch (Exception ex) {
                Log.w("SavedDockUpdater", "Query dockProvider fail", (Throwable)ex);
            }
        }
    }
    
    private void updateConnectedDevice(List<DockDevice> list) {
        if (list.isEmpty()) {
            mConnectedDockId = null;
            updateDevices();
        }
        else {
            mConnectedDockId = list.get(0).getId();
            if (mPreferenceMap.containsKey(mConnectedDockId)) {
                mDevicePreferenceCallback.onDeviceRemoved(mPreferenceMap.get(mConnectedDockId));
                mPreferenceMap.remove(mConnectedDockId);
            }
        }
    }
    
    private void updateDevices() {
        if (mSavedDevices == null) {
            return;
        }
        for (String s : mSavedDevices.keySet()) {
            if (TextUtils.equals((CharSequence)s, (CharSequence)mConnectedDockId)) {
                continue;
            }
           String title = mSavedDevices.get(s);
            if (mPreferenceMap.containsKey(s)) {
                mPreferenceMap.get(s).setTitle(title);
            }
            else {
                mPreferenceMap.put(s, initPreference(s, title));
                mDevicePreferenceCallback.onDeviceAdded(mPreferenceMap.get(s));
            }
        }
            mPreferenceMap.keySet().removeIf(new Predicate() {
                public boolean test(Object obj) {
                    return hasDeviceBeenRemoved((String) obj);
                }
            });
    }
    
    private void updateSavedDevicesList(List<DockDevice> list) {
        if (mSavedDevices == null) {
            mSavedDevices = (Map<String, String>)new ArrayMap();
        }
        mSavedDevices.clear();
        for (DockDevice dockDevice : list) {
           String name = dockDevice.getName();
            if (!TextUtils.isEmpty((CharSequence)name)) {
                mSavedDevices.put(dockDevice.getId(), name);
            }
        }
        updateDevices();
    }
    
    @Override
    public void forceUpdate() {
        startQuery(1, DockContract.DOCK_PROVIDER_CONNECTED_URI);
        startQuery(2, DockContract.DOCK_PROVIDER_SAVED_URI);
    }
    
    @Override
    public void onGearClick(GearPreference gearPreference) {
        mContext.startActivity(DockContract.buildDockSettingIntent(gearPreference.getKey()));
    }
    
    @Override
    public void onQueryComplete(int n,List<DockDevice> list) {
        if (list != null) {
            if (n == 2) {
                updateSavedDevicesList(list);
            }
            else if (n == 1) {
                updateConnectedDevice(list);
            }
        }
    }
    
    @Override
    public void registerCallback() {
       ContentProviderClient acquireContentProviderClient = mContext.getContentResolver().acquireContentProviderClient(DockContract.DOCK_PROVIDER_SAVED_URI);
        if (acquireContentProviderClient != null) {
            acquireContentProviderClient.release();
            mContext.getContentResolver().registerContentObserver(DockContract.DOCK_PROVIDER_CONNECTED_URI, false, (ContentObserver)mConnectedDockObserver);
            mContext.getContentResolver().registerContentObserver(DockContract.DOCK_PROVIDER_SAVED_URI, false, (ContentObserver)mSavedDockObserver);
            mIsObserverRegistered = true;
            forceUpdate();
        }
    }
    
    @Override
    public void unregisterCallback() {
        if (mIsObserverRegistered) {
            mContext.getContentResolver().unregisterContentObserver((ContentObserver)mConnectedDockObserver);
            mContext.getContentResolver().unregisterContentObserver((ContentObserver)mSavedDockObserver);
            mIsObserverRegistered = false;
        }
    }

}
