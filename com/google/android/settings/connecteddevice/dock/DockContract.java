package com.google.android.settings.connecteddevice.dock;

import android.content.Intent;
import android.net.Uri.Builder;
import android.net.Uri;
import com.android.internal.annotations.VisibleForTesting;
import android.content.ComponentName;

public class DockContract
{
    private static final ComponentName DOCK_COMPONENT;
    public static final String[] DOCK_PROJECTION;
    @VisibleForTesting
    static final int DOCK_PROVIDER_CONNECTED_TOKEN = 1;
    public static final Uri DOCK_PROVIDER_CONNECTED_URI;
    @VisibleForTesting
    static final int DOCK_PROVIDER_SAVED_TOKEN = 2;
    public static final Uri DOCK_PROVIDER_SAVED_URI;
    
    static {
        DOCK_PROJECTION = new String[] { "dockId", "dockName" };
        DOCK_COMPONENT = new ComponentName("com.google.android.apps.dreamliner", "com.google.android.apps.dreamliner.settings.DockDetailSettings");
        DOCK_PROVIDER_CONNECTED_URI = new Uri.Builder().scheme("content").authority("com.google.android.apps.dreamliner.provider").appendPath("connected").build();
        DOCK_PROVIDER_SAVED_URI = new Uri.Builder().scheme("content").authority("com.google.android.apps.dreamliner.provider").appendPath("saved").build();
    }
    
    public static Intent buildDockSettingIntent(final String s) {
        return new Intent().setComponent(DockContract.DOCK_COMPONENT).putExtra("dockId", s);
    }
}
