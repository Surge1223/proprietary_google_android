package com.google.android.settings.dashboard;

import android.content.Context;
import com.android.settings.dashboard.DashboardFeatureProviderImpl;

public class DashboardFeatureProviderGoogleImpl extends DashboardFeatureProviderImpl
{
    public DashboardFeatureProviderGoogleImpl(final Context context) {
        super(context);
    }
}
