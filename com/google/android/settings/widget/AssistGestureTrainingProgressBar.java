package com.google.android.settings.widget;

import android.content.res.TypedArray;
import com.android.settings.R;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.widget.ProgressBar;
import android.view.View;
import android.widget.FrameLayout;

public class AssistGestureTrainingProgressBar extends FrameLayout
{
    private View mDoneView;
    private ProgressBar mProgressBar;
    private int mState;
    private TextView mTextView;
    
    public AssistGestureTrainingProgressBar(final Context context) {
        this(context, null);
    }
    
    public AssistGestureTrainingProgressBar(final Context context, final AttributeSet set) {
        this(context, set, 0, 2131951626);
    }
    
    public AssistGestureTrainingProgressBar(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set);
        this.mState = 0;
        LayoutInflater.from(context).inflate(2131558463, (ViewGroup)this, true);
        this.mTextView = (TextView)this.findViewById(2131362311);
        this.mProgressBar = (ProgressBar)this.findViewById(2131362483);
        this.mDoneView = this.findViewById(2131362088);
        this.refreshViews();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.AssistGestureTrainingProgressBar, n, n2);
        this.mTextView.setText(obtainStyledAttributes.getText(0));
        obtainStyledAttributes.recycle();
    }
    
    private void refreshViews() {
        final TextView mTextView = this.mTextView;
        final int mState = this.mState;
        final int n = 8;
        int visibility;
        if (mState != 2) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        mTextView.setVisibility(visibility);
        final ProgressBar mProgressBar = this.mProgressBar;
        int visibility2;
        if (this.mState == 1) {
            visibility2 = 0;
        }
        else {
            visibility2 = 8;
        }
        mProgressBar.setVisibility(visibility2);
        final View mDoneView = this.mDoneView;
        int visibility3 = n;
        if (this.mState == 2) {
            visibility3 = 0;
        }
        mDoneView.setVisibility(visibility3);
    }
    
    public void setState(final int mState) {
        this.mState = mState;
        this.refreshViews();
    }
    
    public void setText(final CharSequence text) {
        this.mTextView.setText(text);
    }
}
