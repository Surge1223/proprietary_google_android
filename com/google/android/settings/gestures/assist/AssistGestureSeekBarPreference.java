package com.google.android.settings.gestures.assist;

import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.widget.SeekBarPreference;

public class AssistGestureSeekBarPreference extends SeekBarPreference
{
    public AssistGestureSeekBarPreference(final Context context, final AttributeSet set) {
        this(context, set, TypedArrayUtils.getAttr(context, R.attr.seekBarPreferenceStyle, 17891515), 0);
    }
    
    public AssistGestureSeekBarPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setLayoutResource(2131558646);
    }
}
