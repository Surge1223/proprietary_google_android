package com.google.android.settings.gestures.assist;

import android.content.ContentResolver;
import android.net.Uri;
import android.database.ContentObserver;
import android.os.UserHandle;
import android.util.Log;
import android.content.Intent;
import com.google.android.settings.gestures.assist.bubble.AssistGestureBubbleActivity;
import android.os.SystemClock;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import com.android.settings.overlay.FeatureFactory;
import android.os.Message;
import android.os.Looper;
import android.content.Context;
import android.os.UserManager;
import com.android.settings.widget.SeekBarPreference;
import android.os.Handler;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.gestures.AssistGestureFeatureProvider;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.SliderPreferenceController;

public class AssistGestureSensitivityPreferenceController extends SliderPreferenceController implements LifecycleObserver, OnPause, OnResume
{
    public static final float DEFAULT_SENSITIVITY = 0.5f;
    private static final int MSG_GESTURE_DETECTED = 1;
    private static final String PREF_KEY_VIDEO = "gesture_assist_video";
    private static final String PREF_KEY_VIDEO_SILENCE = "gesture_assist_video_silence";
    private static final String TAG = "AssistGesSensePrefCtrl";
    private AssistGestureHelper mAssistGestureHelper;
    private final AssistGestureFeatureProvider mFeatureProvider;
    private RestrictedLockUtils.EnforcedAdmin mFunDisallowedAdmin;
    private boolean mFunDisallowedBySystem;
    private final AssistGestureHelper.GestureListener mGestureListener;
    private final Handler mHandler;
    private long[] mHits;
    private SeekBarPreference mPreference;
    private final SettingObserver mSettingObserver;
    private final UserManager mUserManager;
    private boolean mWasListening;
    
    public AssistGestureSensitivityPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mHits = new long[3];
        this.mGestureListener = new AssistGestureHelper.GestureListener() {
            @Override
            public void onGestureDetected() {
                AssistGestureSensitivityPreferenceController.this.mHandler.obtainMessage(1).sendToTarget();
            }
            
            @Override
            public void onGestureProgress(final float n, final int n2) {
            }
        };
        this.mHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(final Message message) {
                if (message.what == 1) {
                    AssistGestureSensitivityPreferenceController.this.mPreference.setShouldBlink(true);
                }
            }
        };
        this.mFeatureProvider = FeatureFactory.getFactory(context).getAssistGestureFeatureProvider();
        this.mSettingObserver = new SettingObserver();
        this.mAssistGestureHelper = new AssistGestureHelper(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
    }
    
    public static int convertSensitivityFloatToInt(final Context context, final float n) {
        return Math.round(getMaxSensitivityResourceInteger(context) * n);
    }
    
    public static float convertSensitivityIntToFloat(final Context context, final int n) {
        return 1.0f - n / getMaxSensitivityResourceInteger(context);
    }
    
    public static int getMaxSensitivityResourceInteger(final Context context) {
        return context.getResources().getInteger(2131427336);
    }
    
    public static float getSensitivity(final Context context) {
        final float float1 = Settings.Secure.getFloat(context.getContentResolver(), "assist_gesture_sensitivity", 0.5f);
        if (float1 >= 0.0f) {
            final float n = float1;
            if (float1 <= 1.0f) {
                return 1.0f - n;
            }
        }
        final float n = 0.5f;
        return 1.0f - n;
    }
    
    public static int getSensitivityInt(final Context context) {
        return convertSensitivityFloatToInt(context, getSensitivity(context));
    }
    
    public static boolean isAvailable(final Context context, final AssistGestureFeatureProvider assistGestureFeatureProvider) {
        return assistGestureFeatureProvider.isSensorAvailable(context);
    }
    
    private void updateGestureListenerState(final boolean mWasListening) {
        if (mWasListening == this.mWasListening) {
            return;
        }
        if (mWasListening) {
            this.mAssistGestureHelper.setListener(this.mGestureListener);
        }
        else {
            this.mAssistGestureHelper.setListener(null);
        }
        this.mWasListening = mWasListening;
    }
    
    private void updatePreference() {
        if (this.mPreference == null) {
            return;
        }
        this.mPreference.setProgress(this.getSliderPosition());
        final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), "assist_gesture_enabled", 1);
        boolean b = false;
        final boolean b2 = int1 != 0;
        final boolean b3 = Settings.Secure.getInt(this.mContext.getContentResolver(), "assist_gesture_silence_alerts_enabled", 1) != 0;
        if (this.mFeatureProvider.isSupported(this.mContext) && (b2 || b3)) {
            this.mPreference.setEnabled(true);
        }
        else if (this.mFeatureProvider.isSensorAvailable(this.mContext) && b3) {
            this.mPreference.setEnabled(true);
        }
        else {
            this.mPreference.setEnabled(false);
        }
        if ((b2 && this.mFeatureProvider.isSupported(this.mContext)) || b3) {
            b = true;
        }
        this.updateGestureListenerState(b);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mPreference = (SeekBarPreference)preferenceScreen.findPreference(this.getPreferenceKey());
        if (!this.mFeatureProvider.isSupported(this.mContext)) {
            this.setVisible(preferenceScreen, "gesture_assist_video", false);
        }
        else {
            this.setVisible(preferenceScreen, "gesture_assist_video_silence", false);
        }
        super.displayPreference(preferenceScreen);
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (isAvailable(this.mContext, this.mFeatureProvider)) {
            return 0;
        }
        return 2;
    }
    
    @Override
    public int getMaxSteps() {
        return this.mContext.getResources().getInteger(2131427336);
    }
    
    @Override
    public int getSliderPosition() {
        return getSensitivityInt(this.mContext);
    }
    
    @Override
    public boolean handlePreferenceTreeClick(Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return false;
        }
        System.arraycopy(this.mHits, 1, this.mHits, 0, this.mHits.length - 1);
        this.mHits[this.mHits.length - 1] = SystemClock.uptimeMillis();
        if (this.mHits[0] >= SystemClock.uptimeMillis() - 500L) {
            if (this.mUserManager.hasUserRestriction("no_fun")) {
                if (this.mFunDisallowedAdmin != null && !this.mFunDisallowedBySystem) {
                    RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mFunDisallowedAdmin);
                }
                return false;
            }
            preference = (Preference)new Intent(this.mContext, (Class)AssistGestureBubbleActivity.class);
            try {
                this.mContext.startActivity((Intent)preference);
                return true;
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to start activity ");
                sb.append(((Intent)preference).toString());
                Log.e("AssistGesSensePrefCtrl", sb.toString());
            }
        }
        return false;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_assist_sensitivity");
    }
    
    @Override
    public void onPause() {
        this.updateGestureListenerState(false);
        this.mAssistGestureHelper.unbindFromElmyraServiceProxy();
        this.mSettingObserver.unregister();
    }
    
    @Override
    public void onResume() {
        this.mAssistGestureHelper.bindToElmyraServiceProxy();
        this.mSettingObserver.register();
        this.updatePreference();
        this.mFunDisallowedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_fun", UserHandle.myUserId());
        this.mFunDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_fun", UserHandle.myUserId());
    }
    
    @Override
    public boolean setSliderPosition(final int n) {
        return Settings.Secure.putFloat(this.mContext.getContentResolver(), "assist_gesture_sensitivity", convertSensitivityIntToFloat(this.mContext, n));
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updatePreference();
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri ASSIST_GESTURE_ENABLED_URI;
        private final Uri ASSIST_GESTURE_SENSITIVITY_URI;
        private final Uri ASSIST_GESTURE_SILENCE_PHONE_ENABLED_URI;
        
        SettingObserver() {
            super(AssistGestureSensitivityPreferenceController.this.mHandler);
            this.ASSIST_GESTURE_ENABLED_URI = Settings.Secure.getUriFor("assist_gesture_enabled");
            this.ASSIST_GESTURE_SILENCE_PHONE_ENABLED_URI = Settings.Secure.getUriFor("assist_gesture_silence_alerts_enabled");
            this.ASSIST_GESTURE_SENSITIVITY_URI = Settings.Secure.getUriFor("assist_gesture_sensitivity");
        }
        
        public void onChange(final boolean b) {
            AssistGestureSensitivityPreferenceController.this.updatePreference();
        }
        
        public void register() {
            final ContentResolver contentResolver = AssistGestureSensitivityPreferenceController.this.mContext.getContentResolver();
            contentResolver.registerContentObserver(this.ASSIST_GESTURE_ENABLED_URI, false, (ContentObserver)this);
            contentResolver.registerContentObserver(this.ASSIST_GESTURE_SILENCE_PHONE_ENABLED_URI, false, (ContentObserver)this);
            contentResolver.registerContentObserver(this.ASSIST_GESTURE_SENSITIVITY_URI, false, (ContentObserver)this);
        }
        
        public void unregister() {
            AssistGestureSensitivityPreferenceController.this.mContext.getContentResolver().unregisterContentObserver((ContentObserver)this);
        }
    }
}
