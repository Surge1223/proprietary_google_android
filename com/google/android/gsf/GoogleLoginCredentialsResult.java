package com.google.android.gsf;

import android.os.Parcel;
import android.content.Intent;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class GoogleLoginCredentialsResult implements Parcelable
{
    public static final Parcelable.Creator<GoogleLoginCredentialsResult> CREATOR;
    private String mAccount;
    private Intent mCredentialsIntent;
    private String mCredentialsString;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<GoogleLoginCredentialsResult>() {
            public GoogleLoginCredentialsResult createFromParcel(final Parcel parcel) {
                return new GoogleLoginCredentialsResult(parcel, null);
            }
            
            public GoogleLoginCredentialsResult[] newArray(final int n) {
                return new GoogleLoginCredentialsResult[n];
            }
        };
    }
    
    public GoogleLoginCredentialsResult() {
        this.mCredentialsString = null;
        this.mCredentialsIntent = null;
        this.mAccount = null;
    }
    
    private GoogleLoginCredentialsResult(final Parcel parcel) {
        this.readFromParcel(parcel);
    }
    
    public int describeContents() {
        int describeContents;
        if (this.mCredentialsIntent != null) {
            describeContents = this.mCredentialsIntent.describeContents();
        }
        else {
            describeContents = 0;
        }
        return describeContents;
    }
    
    public void readFromParcel(final Parcel parcel) {
        this.mAccount = parcel.readString();
        this.mCredentialsString = parcel.readString();
        final int int1 = parcel.readInt();
        this.mCredentialsIntent = null;
        if (int1 == 1) {
            (this.mCredentialsIntent = new Intent()).readFromParcel(parcel);
            this.mCredentialsIntent.setExtrasClassLoader(this.getClass().getClassLoader());
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.mAccount);
        parcel.writeString(this.mCredentialsString);
        if (this.mCredentialsIntent != null) {
            parcel.writeInt(1);
            this.mCredentialsIntent.writeToParcel(parcel, 0);
        }
        else {
            parcel.writeInt(0);
        }
    }
}
