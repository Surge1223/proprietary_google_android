package com.google.android.gsf;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class LoginData implements Parcelable
{
    public static final Parcelable.Creator<LoginData> CREATOR;
    public String mAuthtoken;
    public String mCaptchaAnswer;
    public byte[] mCaptchaData;
    public String mCaptchaMimeType;
    public String mCaptchaToken;
    public String mEncryptedPassword;
    public int mFlags;
    public String mJsonString;
    public String mOAuthAccessToken;
    public String mPassword;
    public String mService;
    public String mSid;
    public Status mStatus;
    public String mUsername;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<LoginData>() {
            public LoginData createFromParcel(final Parcel parcel) {
                return new LoginData(parcel, null);
            }
            
            public LoginData[] newArray(final int n) {
                return new LoginData[n];
            }
        };
    }
    
    public LoginData() {
        this.mUsername = null;
        this.mEncryptedPassword = null;
        this.mPassword = null;
        this.mService = null;
        this.mCaptchaToken = null;
        this.mCaptchaData = null;
        this.mCaptchaMimeType = null;
        this.mCaptchaAnswer = null;
        this.mFlags = 0;
        this.mStatus = null;
        this.mJsonString = null;
        this.mSid = null;
        this.mAuthtoken = null;
        this.mOAuthAccessToken = null;
    }
    
    private LoginData(final Parcel parcel) {
        this.mUsername = null;
        this.mEncryptedPassword = null;
        this.mPassword = null;
        this.mService = null;
        this.mCaptchaToken = null;
        this.mCaptchaData = null;
        this.mCaptchaMimeType = null;
        this.mCaptchaAnswer = null;
        this.mFlags = 0;
        this.mStatus = null;
        this.mJsonString = null;
        this.mSid = null;
        this.mAuthtoken = null;
        this.mOAuthAccessToken = null;
        this.readFromParcel(parcel);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void readFromParcel(final Parcel parcel) {
        this.mUsername = parcel.readString();
        this.mEncryptedPassword = parcel.readString();
        this.mPassword = parcel.readString();
        this.mService = parcel.readString();
        this.mCaptchaToken = parcel.readString();
        final int int1 = parcel.readInt();
        if (int1 == -1) {
            this.mCaptchaData = null;
        }
        else {
            parcel.readByteArray(this.mCaptchaData = new byte[int1]);
        }
        this.mCaptchaMimeType = parcel.readString();
        this.mCaptchaAnswer = parcel.readString();
        this.mFlags = parcel.readInt();
        final String string = parcel.readString();
        if (string == null) {
            this.mStatus = null;
        }
        else {
            this.mStatus = Status.valueOf(string);
        }
        this.mJsonString = parcel.readString();
        this.mSid = parcel.readString();
        this.mAuthtoken = parcel.readString();
        this.mOAuthAccessToken = parcel.readString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.mUsername);
        parcel.writeString(this.mEncryptedPassword);
        parcel.writeString(this.mPassword);
        parcel.writeString(this.mService);
        parcel.writeString(this.mCaptchaToken);
        if (this.mCaptchaData == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(this.mCaptchaData.length);
            parcel.writeByteArray(this.mCaptchaData);
        }
        parcel.writeString(this.mCaptchaMimeType);
        parcel.writeString(this.mCaptchaAnswer);
        parcel.writeInt(this.mFlags);
        if (this.mStatus == null) {
            parcel.writeString((String)null);
        }
        else {
            parcel.writeString(this.mStatus.name());
        }
        parcel.writeString(this.mJsonString);
        parcel.writeString(this.mSid);
        parcel.writeString(this.mAuthtoken);
        parcel.writeString(this.mOAuthAccessToken);
    }
    
    public enum Status
    {
        ACCOUNT_DISABLED, 
        BAD_REQUEST, 
        BAD_USERNAME, 
        CANCELLED, 
        CAPTCHA, 
        DELETED_GMAIL, 
        DMAGENT, 
        LOGIN_FAIL, 
        MISSING_APPS, 
        NETWORK_ERROR, 
        NO_GMAIL, 
        OAUTH_MIGRATION_REQUIRED, 
        SERVER_ERROR, 
        SUCCESS;
    }
}
