package com.google.android.gms.auth.api.signin.internal;

public final class zzq
{
    private static int zza;
    private int zzb;
    
    static {
        zzq.zza = 31;
    }
    
    public zzq() {
        this.zzb = 1;
    }
    
    public final int zza() {
        return this.zzb;
    }
    
    public final zzq zza(final Object o) {
        final int zza = zzq.zza;
        final int zzb = this.zzb;
        int hashCode;
        if (o == null) {
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
        }
        this.zzb = zza * zzb + hashCode;
        return this;
    }
    
    public final zzq zza(final boolean b) {
        this.zzb = zzq.zza * this.zzb + (b ? 1 : 0);
        return this;
    }
}
