package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzo extends zzbid
{
    public static final Parcelable.Creator<zzo> CREATOR;
    private final int zza;
    private int zzb;
    private Bundle zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzn();
    }
    
    zzo(final int zza, final int zzb, final Bundle zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
    
    public final int zza() {
        return this.zzb;
    }
}
