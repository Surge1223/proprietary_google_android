package com.google.android.gms.auth.api.signin.internal;

import org.json.JSONException;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.zzau;
import android.content.Context;
import java.util.concurrent.locks.ReentrantLock;
import android.content.SharedPreferences;
import java.util.concurrent.locks.Lock;

public final class zzaa
{
    private static final Lock zza;
    private static zzaa zzb;
    private final Lock zzc;
    private final SharedPreferences zzd;
    
    static {
        zza = new ReentrantLock();
    }
    
    private zzaa(final Context context) {
        this.zzc = new ReentrantLock();
        this.zzd = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }
    
    public static zzaa zza(final Context context) {
        zzau.zza(context);
        zzaa.zza.lock();
        try {
            if (zzaa.zzb == null) {
                zzaa.zzb = new zzaa(context.getApplicationContext());
            }
            return zzaa.zzb;
        }
        finally {
            zzaa.zza.unlock();
        }
    }
    
    private final GoogleSignInAccount zzb(String zza) {
        if (TextUtils.isEmpty((CharSequence)zza)) {
            return null;
        }
        zza = this.zza(zzb("googleSignInAccount", zza));
        if (zza != null) {
            try {
                return GoogleSignInAccount.zza(zza);
            }
            catch (JSONException ex) {
                return null;
            }
        }
        return null;
    }
    
    private static String zzb(final String s, final String s2) {
        final StringBuilder sb = new StringBuilder(1 + String.valueOf(s).length() + String.valueOf(s2).length());
        sb.append(s);
        sb.append(":");
        sb.append(s2);
        return sb.toString();
    }
    
    public final GoogleSignInAccount zza() {
        return this.zzb(this.zza("defaultGoogleSignInAccount"));
    }
    
    protected final String zza(String string) {
        this.zzc.lock();
        try {
            string = this.zzd.getString(string, (String)null);
            return string;
        }
        finally {
            this.zzc.unlock();
        }
    }
}
