package com.google.android.gms.auth.api.signin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.auth.api.signin.internal.zzq;
import java.util.Collections;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import com.google.android.gms.auth.api.signin.internal.zzo;
import android.accounts.Account;
import java.util.ArrayList;
import java.util.Comparator;
import com.google.android.gms.common.api.Scope;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.zzbid;

public class GoogleSignInOptions extends zzbid implements Optional, ReflectedParcelable
{
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR;
    public static final GoogleSignInOptions DEFAULT_GAMES_SIGN_IN;
    public static final GoogleSignInOptions DEFAULT_SIGN_IN;
    public static final Scope zza;
    public static final Scope zzb;
    public static final Scope zzc;
    public static final Scope zzd;
    public static final Scope zze;
    private static Comparator<Scope> zzp;
    private final int zzf;
    private final ArrayList<Scope> zzg;
    private Account zzh;
    private boolean zzi;
    private final boolean zzj;
    private final boolean zzk;
    private String zzl;
    private String zzm;
    private ArrayList<zzo> zzn;
    private Map<Integer, zzo> zzo;
    
    static {
        zza = new Scope("profile");
        zzb = new Scope("email");
        zzc = new Scope("openid");
        zzd = new Scope("https://www.googleapis.com/auth/games_lite");
        zze = new Scope("https://www.googleapis.com/auth/games");
        DEFAULT_SIGN_IN = new Builder().requestId().requestProfile().build();
        DEFAULT_GAMES_SIGN_IN = new Builder().requestScopes(GoogleSignInOptions.zzd, new Scope[0]).build();
        CREATOR = (Parcelable.Creator)new com.google.android.gms.auth.api.signin.zze();
        GoogleSignInOptions.zzp = new com.google.android.gms.auth.api.signin.zzd();
    }
    
    GoogleSignInOptions(final int n, final ArrayList<Scope> list, final Account account, final boolean b, final boolean b2, final boolean b3, final String s, final String s2, final ArrayList<zzo> list2) {
        this(n, list, account, b, b2, b3, s, s2, zzb(list2));
    }
    
    private GoogleSignInOptions(final int zzf, final ArrayList<Scope> zzg, final Account zzh, final boolean zzi, final boolean zzj, final boolean zzk, final String zzl, final String zzm, final Map<Integer, zzo> zzo) {
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = new ArrayList<zzo>(zzo.values());
        this.zzo = zzo;
    }
    
    private static Map<Integer, zzo> zzb(final List<zzo> list) {
        final HashMap<Integer, zzo> hashMap = new HashMap<Integer, zzo>();
        if (list == null) {
            return hashMap;
        }
        for (final zzo zzo : list) {
            hashMap.put(zzo.zza(), zzo);
        }
        return hashMap;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        try {
            final GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions)o;
            if (this.zzn.size() > 0 || googleSignInOptions.zzn.size() > 0) {
                return false;
            }
            if (this.zzg.size() == googleSignInOptions.zza().size() && this.zzg.containsAll(googleSignInOptions.zza())) {
                if (this.zzh == null) {
                    if (googleSignInOptions.zzh != null) {
                        return false;
                    }
                }
                else if (!this.zzh.equals((Object)googleSignInOptions.zzh)) {
                    return false;
                }
                if (TextUtils.isEmpty((CharSequence)this.zzl)) {
                    if (!TextUtils.isEmpty((CharSequence)googleSignInOptions.zzl)) {
                        return false;
                    }
                }
                else if (!this.zzl.equals(googleSignInOptions.zzl)) {
                    return false;
                }
                if (this.zzk == googleSignInOptions.zzk && this.zzi == googleSignInOptions.zzi && this.zzj == googleSignInOptions.zzj) {
                    return true;
                }
                return false;
            }
            return false;
        }
        catch (ClassCastException ex) {
            return false;
        }
    }
    
    @Override
    public int hashCode() {
        final ArrayList<Comparable> list = new ArrayList<Comparable>();
        final ArrayList<Scope> list2 = this.zzg;
        final int size = list2.size();
        int i = 0;
        while (i < size) {
            final Scope value = list2.get(i);
            ++i;
            list.add(value.zza());
        }
        Collections.sort(list);
        return new zzq().zza(list).zza(this.zzh).zza(this.zzl).zza(this.zzk).zza(this.zzi).zza(this.zzj).zza();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzf);
        zzbig.zzc(parcel, 2, this.zza(), false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzh, n, false);
        zzbig.zza(parcel, 4, this.zzi);
        zzbig.zza(parcel, 5, this.zzj);
        zzbig.zza(parcel, 6, this.zzk);
        zzbig.zza(parcel, 7, this.zzl, false);
        zzbig.zza(parcel, 8, this.zzm, false);
        zzbig.zzc(parcel, 9, (List<Parcelable>)this.zzn, false);
        zzbig.zza(parcel, zza);
    }
    
    public final ArrayList<Scope> zza() {
        return new ArrayList<Scope>(this.zzg);
    }
    
    public static final class Builder
    {
        private Set<Scope> zza;
        private boolean zzb;
        private boolean zzc;
        private boolean zzd;
        private String zze;
        private Account zzf;
        private String zzg;
        private Map<Integer, zzo> zzh;
        
        public Builder() {
            this.zza = new HashSet<Scope>();
            this.zzh = new HashMap<Integer, zzo>();
        }
        
        public final GoogleSignInOptions build() {
            if (this.zza.contains(GoogleSignInOptions.zze) && this.zza.contains(GoogleSignInOptions.zzd)) {
                this.zza.remove(GoogleSignInOptions.zzd);
            }
            if (this.zzd && (this.zzf == null || !this.zza.isEmpty())) {
                this.requestId();
            }
            return new GoogleSignInOptions(3, new ArrayList((Collection<? extends E>)this.zza), this.zzf, this.zzd, this.zzb, this.zzc, this.zze, this.zzg, this.zzh, null);
        }
        
        public final Builder requestId() {
            this.zza.add(GoogleSignInOptions.zzc);
            return this;
        }
        
        public final Builder requestProfile() {
            this.zza.add(GoogleSignInOptions.zza);
            return this;
        }
        
        public final Builder requestScopes(final Scope scope, final Scope... array) {
            this.zza.add(scope);
            this.zza.addAll(Arrays.asList(array));
            return this;
        }
    }
}
