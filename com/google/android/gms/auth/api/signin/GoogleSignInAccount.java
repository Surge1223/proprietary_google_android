package com.google.android.gms.auth.api.signin;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.accounts.Account;
import java.util.Collection;
import java.util.ArrayList;
import com.google.android.gms.common.internal.zzau;
import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import android.text.TextUtils;
import java.util.HashSet;
import com.google.android.gms.common.util.zzh;
import java.util.Set;
import com.google.android.gms.common.api.Scope;
import java.util.List;
import android.net.Uri;
import com.google.android.gms.common.util.Clock;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public class GoogleSignInAccount extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<GoogleSignInAccount> CREATOR;
    private static Clock zza;
    private final int zzb;
    private String zzc;
    private String zzd;
    private String zze;
    private String zzf;
    private Uri zzg;
    private String zzh;
    private long zzi;
    private String zzj;
    private List<Scope> zzk;
    private String zzl;
    private String zzm;
    private Set<Scope> zzn;
    
    static {
        CREATOR = (Parcelable.Creator)new zzb();
        GoogleSignInAccount.zza = zzh.zza();
    }
    
    GoogleSignInAccount(final int zzb, final String zzc, final String zzd, final String zze, final String zzf, final Uri zzg, final String zzh, final long zzi, final String zzj, final List<Scope> zzk, final String zzl, final String zzm) {
        this.zzn = new HashSet<Scope>();
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
    }
    
    public static GoogleSignInAccount zza(String optString) throws JSONException {
        if (TextUtils.isEmpty((CharSequence)optString)) {
            return null;
        }
        final JSONObject jsonObject = new JSONObject(optString);
        optString = jsonObject.optString("photoUrl", (String)null);
        Uri parse;
        if (!TextUtils.isEmpty((CharSequence)optString)) {
            parse = Uri.parse(optString);
        }
        else {
            parse = null;
        }
        final long long1 = Long.parseLong(jsonObject.getString("expirationTime"));
        final HashSet<Scope> set = new HashSet<Scope>();
        final JSONArray jsonArray = jsonObject.getJSONArray("grantedScopes");
        for (int length = jsonArray.length(), i = 0; i < length; ++i) {
            set.add(new Scope(jsonArray.getString(i)));
        }
        final GoogleSignInAccount zza = zza(jsonObject.optString("id"), jsonObject.optString("tokenId", (String)null), jsonObject.optString("email", (String)null), jsonObject.optString("displayName", (String)null), jsonObject.optString("givenName", (String)null), jsonObject.optString("familyName", (String)null), parse, long1, jsonObject.getString("obfuscatedIdentifier"), set);
        zza.zzh = jsonObject.optString("serverAuthCode", (String)null);
        return zza;
    }
    
    private static GoogleSignInAccount zza(final String s, final String s2, final String s3, final String s4, final String s5, final String s6, final Uri uri, Long value, final String s7, final Set<Scope> set) {
        if (value == null) {
            value = GoogleSignInAccount.zza.currentTimeMillis() / 1000L;
        }
        return new GoogleSignInAccount(3, s, s2, s3, s4, uri, null, value, zzau.zza(s7), new ArrayList<Scope>(zzau.zza(set)), s5, s6);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof GoogleSignInAccount)) {
            return false;
        }
        final GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount)o;
        return googleSignInAccount.zzj.equals(this.zzj) && googleSignInAccount.zzd().equals(this.zzd());
    }
    
    public Account getAccount() {
        if (this.zze == null) {
            return null;
        }
        return new Account(this.zze, "com.google");
    }
    
    public String getDisplayName() {
        return this.zzf;
    }
    
    public String getEmail() {
        return this.zze;
    }
    
    public String getFamilyName() {
        return this.zzm;
    }
    
    public String getGivenName() {
        return this.zzl;
    }
    
    public String getId() {
        return this.zzc;
    }
    
    public String getIdToken() {
        return this.zzd;
    }
    
    public Uri getPhotoUrl() {
        return this.zzg;
    }
    
    public String getServerAuthCode() {
        return this.zzh;
    }
    
    @Override
    public int hashCode() {
        return (527 + this.zzj.hashCode()) * 31 + this.zzd().hashCode();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzb);
        zzbig.zza(parcel, 2, this.getId(), false);
        zzbig.zza(parcel, 3, this.getIdToken(), false);
        zzbig.zza(parcel, 4, this.getEmail(), false);
        zzbig.zza(parcel, 5, this.getDisplayName(), false);
        zzbig.zza(parcel, 6, (Parcelable)this.getPhotoUrl(), n, false);
        zzbig.zza(parcel, 7, this.getServerAuthCode(), false);
        zzbig.zza(parcel, 8, this.zzi);
        zzbig.zza(parcel, 9, this.zzj, false);
        zzbig.zzc(parcel, 10, this.zzk, false);
        zzbig.zza(parcel, 11, this.getGivenName(), false);
        zzbig.zza(parcel, 12, this.getFamilyName(), false);
        zzbig.zza(parcel, zza);
    }
    
    public final Set<Scope> zzd() {
        final HashSet<Object> set = (HashSet<Object>)new HashSet<Scope>(this.zzk);
        set.addAll(this.zzn);
        return (Set<Scope>)set;
    }
}
