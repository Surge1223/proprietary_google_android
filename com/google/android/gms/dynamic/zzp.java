package com.google.android.gms.dynamic;

import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.internal.zzau;
import android.content.Context;
import android.os.IBinder;

public abstract class zzp<T>
{
    private final String zza;
    private T zzb;
    
    protected zzp(final String zza) {
        this.zza = zza;
    }
    
    protected abstract T zza(final IBinder p0);
    
    protected final T zzb(Context remoteContext) throws zzq {
        if (this.zzb == null) {
            zzau.zza(remoteContext);
            remoteContext = GooglePlayServicesUtilLight.getRemoteContext(remoteContext);
            if (remoteContext != null) {
                final ClassLoader classLoader = remoteContext.getClassLoader();
                try {
                    this.zzb = this.zza((IBinder)classLoader.loadClass(this.zza).newInstance());
                    return this.zzb;
                }
                catch (IllegalAccessException ex) {
                    throw new zzq("Could not access creator.", ex);
                }
                catch (InstantiationException ex2) {
                    throw new zzq("Could not instantiate creator.", ex2);
                }
                catch (ClassNotFoundException ex3) {
                    throw new zzq("Could not load creator class.", ex3);
                }
            }
            throw new zzq("Could not get remote context.");
        }
        return this.zzb;
    }
}
