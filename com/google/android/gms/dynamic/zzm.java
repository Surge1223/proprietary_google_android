package com.google.android.gms.dynamic;

import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzm extends zzey implements IObjectWrapper
{
    zzm(final IBinder binder) {
        super(binder, "com.google.android.gms.dynamic.IObjectWrapper");
    }
}
