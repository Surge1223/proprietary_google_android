package com.google.android.gms.dynamic;

import android.os.IBinder;
import com.google.android.gms.internal.zzez;
import android.os.IInterface;

public interface IObjectWrapper extends IInterface
{
    public static class zza extends zzez implements IObjectWrapper
    {
        public zza() {
            this.attachInterface((IInterface)this, "com.google.android.gms.dynamic.IObjectWrapper");
        }
        
        public static IObjectWrapper zza(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            if (queryLocalInterface instanceof IObjectWrapper) {
                return (IObjectWrapper)queryLocalInterface;
            }
            return new zzm(binder);
        }
    }
}
