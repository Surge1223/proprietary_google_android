package com.google.android.gms.dynamic;

public final class zzq extends Exception
{
    public zzq(final String s) {
        super(s);
    }
    
    public zzq(final String s, final Throwable t) {
        super(s, t);
    }
}
