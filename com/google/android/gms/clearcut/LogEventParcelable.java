package com.google.android.gms.clearcut;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import com.google.android.gms.clearcut.internal.PlayLoggerContext;
import com.google.android.gms.internal.zzgnq;
import com.google.android.gms.phenotype.ExperimentTokens;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class LogEventParcelable extends zzbid
{
    public static final Parcelable.Creator<LogEventParcelable> CREATOR;
    public boolean addPhenotypeExperimentTokens;
    public final ClearcutLogger.MessageProducer clientVisualElementsProducer;
    public int[] experimentIds;
    public byte[][] experimentTokens;
    public ExperimentTokens[] experimentTokensParcelables;
    public final ClearcutLogger.MessageProducer extensionProducer;
    public final zzgnq logEvent;
    public byte[] logEventBytes;
    public String[] mendelPackages;
    public PlayLoggerContext playLoggerContext;
    public int[] testCodes;
    
    static {
        CREATOR = (Parcelable.Creator)new zzq();
    }
    
    LogEventParcelable(final PlayLoggerContext playLoggerContext, final byte[] logEventBytes, final int[] testCodes, final String[] mendelPackages, final int[] experimentIds, final byte[][] experimentTokens, final boolean addPhenotypeExperimentTokens, final ExperimentTokens[] experimentTokensParcelables) {
        this.playLoggerContext = playLoggerContext;
        this.logEventBytes = logEventBytes;
        this.testCodes = testCodes;
        this.mendelPackages = mendelPackages;
        this.logEvent = null;
        this.extensionProducer = null;
        this.clientVisualElementsProducer = null;
        this.experimentIds = experimentIds;
        this.experimentTokens = experimentTokens;
        this.experimentTokensParcelables = experimentTokensParcelables;
        this.addPhenotypeExperimentTokens = addPhenotypeExperimentTokens;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof LogEventParcelable) {
            final LogEventParcelable logEventParcelable = (LogEventParcelable)o;
            return zzak.zza(this.playLoggerContext, logEventParcelable.playLoggerContext) && Arrays.equals(this.logEventBytes, logEventParcelable.logEventBytes) && Arrays.equals(this.testCodes, logEventParcelable.testCodes) && Arrays.equals(this.mendelPackages, logEventParcelable.mendelPackages) && zzak.zza(this.logEvent, logEventParcelable.logEvent) && zzak.zza(this.extensionProducer, logEventParcelable.extensionProducer) && zzak.zza(this.clientVisualElementsProducer, logEventParcelable.clientVisualElementsProducer) && Arrays.equals(this.experimentIds, logEventParcelable.experimentIds) && Arrays.deepEquals(this.experimentTokens, logEventParcelable.experimentTokens) && Arrays.equals(this.experimentTokensParcelables, logEventParcelable.experimentTokensParcelables) && this.addPhenotypeExperimentTokens == logEventParcelable.addPhenotypeExperimentTokens;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.playLoggerContext, this.logEventBytes, this.testCodes, this.mendelPackages, this.logEvent, this.extensionProducer, this.clientVisualElementsProducer, this.experimentIds, this.experimentTokens, this.experimentTokensParcelables, this.addPhenotypeExperimentTokens });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LogEventParcelable[");
        sb.append(this.playLoggerContext);
        sb.append(", LogEventBytes: ");
        String s;
        if (this.logEventBytes == null) {
            s = null;
        }
        else {
            s = new String(this.logEventBytes);
        }
        sb.append(s);
        sb.append(", TestCodes: ");
        sb.append(Arrays.toString(this.testCodes));
        sb.append(", MendelPackages: ");
        sb.append(Arrays.toString(this.mendelPackages));
        sb.append(", LogEvent: ");
        sb.append(this.logEvent);
        sb.append(", ExtensionProducer: ");
        sb.append(this.extensionProducer);
        sb.append(", VeProducer: ");
        sb.append(this.clientVisualElementsProducer);
        sb.append(", ExperimentIDs: ");
        sb.append(Arrays.toString(this.experimentIds));
        sb.append(", ExperimentTokens: ");
        sb.append(Arrays.toString(this.experimentTokens));
        sb.append(", ExperimentTokensParcelables: ");
        sb.append(Arrays.toString(this.experimentTokensParcelables));
        sb.append(", AddPhenotypeExperimentTokens: ");
        sb.append(this.addPhenotypeExperimentTokens);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, (Parcelable)this.playLoggerContext, n, false);
        zzbig.zza(parcel, 3, this.logEventBytes, false);
        zzbig.zza(parcel, 4, this.testCodes, false);
        zzbig.zza(parcel, 5, this.mendelPackages, false);
        zzbig.zza(parcel, 6, this.experimentIds, false);
        zzbig.zza(parcel, 7, this.experimentTokens, false);
        zzbig.zza(parcel, 8, this.addPhenotypeExperimentTokens);
        zzbig.zza(parcel, 9, this.experimentTokensParcelables, n, false);
        zzbig.zza(parcel, zza);
    }
}
