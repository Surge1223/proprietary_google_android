package com.google.android.gms.clearcut;

import com.google.android.gms.phenotype.ExperimentTokens;
import com.google.android.gms.clearcut.internal.zzi;
import com.google.android.gms.common.api.Api;

public final class ClearcutLogger
{
    @Deprecated
    public static final Api<Object> API;
    private static final Api.zzf<zzi> zza;
    private static final Api.zza<zzi, Object> zzb;
    private static final ExperimentTokens[] zzc;
    private static final String[] zzd;
    private static final byte[][] zze;
    
    static {
        zza = new Api.zzf();
        zzb = new zzd();
        API = new Api<Object>("ClearcutLogger.API", (Api.zza<C, Object>)ClearcutLogger.zzb, (Api.zzf<C>)ClearcutLogger.zza);
        zzc = new ExperimentTokens[0];
        zzd = new String[0];
        zze = new byte[0][];
    }
    
    public interface MessageProducer
    {
    }
}
