package com.google.android.gms.clearcut.internal;

import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzr extends zzey implements zzq
{
    zzr(final IBinder binder) {
        super(binder, "com.google.android.gms.clearcut.internal.IClearcutLoggerService");
    }
}
