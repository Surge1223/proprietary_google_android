package com.google.android.gms.clearcut.internal;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class PlayLoggerContext extends zzbid
{
    public static final Parcelable.Creator<PlayLoggerContext> CREATOR;
    public final boolean isAnonymous;
    public final boolean logAndroidId;
    public final int logSource;
    public final String logSourceName;
    public final String loggingId;
    public final String packageName;
    public final int packageVersionCode;
    public final int qosTier;
    public final String uploadAccountName;
    
    static {
        CREATOR = (Parcelable.Creator)new zzu();
    }
    
    public PlayLoggerContext(final String packageName, final int packageVersionCode, final int logSource, final String uploadAccountName, final String loggingId, final boolean logAndroidId, final String logSourceName, final boolean isAnonymous, final int qosTier) {
        this.packageName = packageName;
        this.packageVersionCode = packageVersionCode;
        this.logSource = logSource;
        this.uploadAccountName = uploadAccountName;
        this.loggingId = loggingId;
        this.logAndroidId = logAndroidId;
        this.logSourceName = logSourceName;
        this.isAnonymous = isAnonymous;
        this.qosTier = qosTier;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof PlayLoggerContext) {
            final PlayLoggerContext playLoggerContext = (PlayLoggerContext)o;
            return zzak.zza(this.packageName, playLoggerContext.packageName) && this.packageVersionCode == playLoggerContext.packageVersionCode && this.logSource == playLoggerContext.logSource && zzak.zza(this.logSourceName, playLoggerContext.logSourceName) && zzak.zza(this.uploadAccountName, playLoggerContext.uploadAccountName) && zzak.zza(this.loggingId, playLoggerContext.loggingId) && this.logAndroidId == playLoggerContext.logAndroidId && this.isAnonymous == playLoggerContext.isAnonymous && this.qosTier == playLoggerContext.qosTier;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.packageName, this.packageVersionCode, this.logSource, this.logSourceName, this.uploadAccountName, this.loggingId, this.logAndroidId, this.isAnonymous, this.qosTier });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PlayLoggerContext[");
        sb.append("package=");
        sb.append(this.packageName);
        sb.append(',');
        sb.append("packageVersionCode=");
        sb.append(this.packageVersionCode);
        sb.append(',');
        sb.append("logSource=");
        sb.append(this.logSource);
        sb.append(',');
        sb.append("logSourceName=");
        sb.append(this.logSourceName);
        sb.append(',');
        sb.append("uploadAccount=");
        sb.append(this.uploadAccountName);
        sb.append(',');
        sb.append("loggingId=");
        sb.append(this.loggingId);
        sb.append(',');
        sb.append("logAndroidId=");
        sb.append(this.logAndroidId);
        sb.append(',');
        sb.append("isAnonymous=");
        sb.append(this.isAnonymous);
        sb.append(',');
        sb.append("qosTier=");
        sb.append(this.qosTier);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.packageName, false);
        zzbig.zza(parcel, 3, this.packageVersionCode);
        zzbig.zza(parcel, 4, this.logSource);
        zzbig.zza(parcel, 5, this.uploadAccountName, false);
        zzbig.zza(parcel, 6, this.loggingId, false);
        zzbig.zza(parcel, 7, this.logAndroidId);
        zzbig.zza(parcel, 8, this.logSourceName, false);
        zzbig.zza(parcel, 9, this.isAnonymous);
        zzbig.zza(parcel, 10, this.qosTier);
        zzbig.zza(parcel, zza);
    }
}
