package com.google.android.gms.clearcut.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.internal.zzl;

public final class zzi extends zzl<zzq>
{
    public zzi(final Context context, final Looper looper, final ClientSettings clientSettings, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 40, clientSettings, connectionCallbacks, onConnectionFailedListener);
    }
    
    @Override
    protected final String getServiceDescriptor() {
        return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
    }
    
    @Override
    protected final String getStartServiceAction() {
        return "com.google.android.gms.clearcut.service.START";
    }
    
    @Override
    public final int zza() {
        return 12438000;
    }
}
