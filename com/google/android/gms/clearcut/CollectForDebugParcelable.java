package com.google.android.gms.clearcut;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class CollectForDebugParcelable extends zzbid
{
    public static final Parcelable.Creator<CollectForDebugParcelable> CREATOR;
    public final long collectForDebugExpiryTimeMillis;
    public final long collectForDebugStartTimeMillis;
    public final boolean skipPersistentStorage;
    
    static {
        CREATOR = (Parcelable.Creator)new zzn();
    }
    
    public CollectForDebugParcelable(final boolean skipPersistentStorage, final long collectForDebugStartTimeMillis, final long collectForDebugExpiryTimeMillis) {
        this.skipPersistentStorage = skipPersistentStorage;
        this.collectForDebugStartTimeMillis = collectForDebugStartTimeMillis;
        this.collectForDebugExpiryTimeMillis = collectForDebugExpiryTimeMillis;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof CollectForDebugParcelable) {
            final CollectForDebugParcelable collectForDebugParcelable = (CollectForDebugParcelable)o;
            return this.skipPersistentStorage == collectForDebugParcelable.skipPersistentStorage && this.collectForDebugStartTimeMillis == collectForDebugParcelable.collectForDebugStartTimeMillis && this.collectForDebugExpiryTimeMillis == collectForDebugParcelable.collectForDebugExpiryTimeMillis;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.skipPersistentStorage, this.collectForDebugStartTimeMillis, this.collectForDebugExpiryTimeMillis });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CollectForDebugParcelable[skipPersistentStorage: ");
        sb.append(this.skipPersistentStorage);
        sb.append(",collectForDebugStartTimeMillis: ");
        sb.append(this.collectForDebugStartTimeMillis);
        sb.append(",collectForDebugExpiryTimeMillis: ");
        sb.append(this.collectForDebugExpiryTimeMillis);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.skipPersistentStorage);
        zzbig.zza(parcel, 2, this.collectForDebugExpiryTimeMillis);
        zzbig.zza(parcel, 3, this.collectForDebugStartTimeMillis);
        zzbig.zza(parcel, zza);
    }
}
