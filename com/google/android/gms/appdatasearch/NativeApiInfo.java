package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class NativeApiInfo extends zzbid
{
    public static final Parcelable.Creator<NativeApiInfo> CREATOR;
    public final String downloadManagerFilename;
    public final String sharedLibAbsoluteFilename;
    public final String sharedLibExtensionAbsoluteFilename;
    
    static {
        CREATOR = (Parcelable.Creator)new zzt();
    }
    
    public NativeApiInfo(final String sharedLibAbsoluteFilename, final String sharedLibExtensionAbsoluteFilename, final String downloadManagerFilename) {
        this.sharedLibAbsoluteFilename = sharedLibAbsoluteFilename;
        this.sharedLibExtensionAbsoluteFilename = sharedLibExtensionAbsoluteFilename;
        this.downloadManagerFilename = downloadManagerFilename;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.sharedLibAbsoluteFilename, false);
        zzbig.zza(parcel, 2, this.sharedLibExtensionAbsoluteFilename, false);
        zzbig.zza(parcel, 3, this.downloadManagerFilename, false);
        zzbig.zza(parcel, zza);
    }
}
