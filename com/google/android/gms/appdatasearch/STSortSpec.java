package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class STSortSpec extends zzbid
{
    public static final Parcelable.Creator<STSortSpec> CREATOR;
    private final String zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzag();
    }
    
    public STSortSpec(final String zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return this == o || (o instanceof STSortSpec && zzak.zza(this.zza, ((STSortSpec)o).zza));
    }
    
    public final String getSortExpr() {
        return this.zza;
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza });
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getSortExpr(), false);
        zzbig.zza(parcel, zza);
    }
}
