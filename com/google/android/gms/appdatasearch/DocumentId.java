package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class DocumentId extends zzbid
{
    public static final Parcelable.Creator<DocumentId> CREATOR;
    private final String zza;
    private final String zzb;
    private final String zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    public DocumentId(final String zza, final String zzb, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public String toString() {
        return String.format("DocumentId[packageName=%s, corpusName=%s, uri=%s]", this.zza, this.zzb, this.zzc);
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
}
