package com.google.android.gms.appdatasearch;

import java.util.NoSuchElementException;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SuggestionResults extends zzbid implements Iterable<Result>
{
    public static final Parcelable.Creator<SuggestionResults> CREATOR;
    final String[] zza;
    private final String zzb;
    private final String[] zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzao();
    }
    
    SuggestionResults(final String zzb, final String[] zza, final String[] zzc) {
        this.zzb = zzb;
        this.zza = zza;
        this.zzc = zzc;
    }
    
    public boolean hasError() {
        return this.zzb != null;
    }
    
    @Override
    public Iterator<Result> iterator() {
        if (this.hasError()) {
            return null;
        }
        return new ResultIterator();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzb, false);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
    
    public class Result
    {
        private final int zza;
        
        Result(final int zza) {
            this.zza = zza;
        }
    }
    
    public final class ResultIterator implements Iterator<Result>
    {
        private int zza;
        
        public ResultIterator() {
            this.zza = 0;
        }
        
        @Override
        public final boolean hasNext() {
            return this.zza < SuggestionResults.this.zza.length;
        }
        
        @Override
        public final Result next() {
            if (this.hasNext()) {
                return new Result(this.zza++);
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public final void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
