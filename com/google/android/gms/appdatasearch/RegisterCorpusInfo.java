package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.net.Uri;
import android.accounts.Account;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RegisterCorpusInfo extends zzbid
{
    public static final Parcelable.Creator<RegisterCorpusInfo> CREATOR;
    public final Account account;
    public final Uri contentProviderUri;
    public final int documentMaxAgeSecs;
    public final GlobalSearchCorpusConfig globalSearchConfig;
    public final RegisterCorpusIMEInfo imeConfig;
    public final String name;
    public final String schemaOrgType;
    public final RegisterSectionInfo[] sections;
    public final boolean semanticallySearchable;
    public final boolean trimmable;
    public final String version;
    
    static {
        CREATOR = (Parcelable.Creator)new zzac();
    }
    
    public RegisterCorpusInfo(final String name, final String version, final Uri contentProviderUri, final RegisterSectionInfo[] sections, final GlobalSearchCorpusConfig globalSearchConfig, final boolean trimmable, final Account account, final RegisterCorpusIMEInfo imeConfig, final String schemaOrgType, final boolean semanticallySearchable, final int documentMaxAgeSecs) {
        this.name = name;
        this.version = version;
        this.contentProviderUri = contentProviderUri;
        this.sections = sections;
        this.globalSearchConfig = globalSearchConfig;
        this.trimmable = trimmable;
        this.account = account;
        this.imeConfig = imeConfig;
        this.schemaOrgType = schemaOrgType;
        this.semanticallySearchable = semanticallySearchable;
        this.documentMaxAgeSecs = documentMaxAgeSecs;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.name, false);
        zzbig.zza(parcel, 2, this.version, false);
        zzbig.zza(parcel, 3, (Parcelable)this.contentProviderUri, n, false);
        zzbig.zza(parcel, 4, this.sections, n, false);
        zzbig.zza(parcel, 7, (Parcelable)this.globalSearchConfig, n, false);
        zzbig.zza(parcel, 8, this.trimmable);
        zzbig.zza(parcel, 9, (Parcelable)this.account, n, false);
        zzbig.zza(parcel, 10, (Parcelable)this.imeConfig, n, false);
        zzbig.zza(parcel, 11, this.schemaOrgType, false);
        zzbig.zza(parcel, 12, this.semanticallySearchable);
        zzbig.zza(parcel, 13, this.documentMaxAgeSecs);
        zzbig.zza(parcel, zza);
    }
}
