package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import android.accounts.Account;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class PIMEUpdate extends zzbid
{
    public static final Parcelable.Creator<PIMEUpdate> CREATOR;
    public final Account account;
    public final long createdTimestamp;
    public final boolean inputByUser;
    public final long score;
    public final int sourceClass;
    public final String sourceCorpusHandle;
    public final String sourcePackageName;
    final byte[] zza;
    final byte[] zzb;
    private final Bundle zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzw();
    }
    
    PIMEUpdate(final byte[] zza, final byte[] zzb, final int sourceClass, final String sourcePackageName, final String sourceCorpusHandle, final boolean inputByUser, final Bundle zzc, final long score, final long createdTimestamp, final Account account) {
        this.zza = zza;
        this.zzb = zzb;
        this.sourceClass = sourceClass;
        this.sourcePackageName = sourcePackageName;
        this.sourceCorpusHandle = sourceCorpusHandle;
        this.inputByUser = inputByUser;
        this.zzc = zzc;
        this.score = score;
        this.createdTimestamp = createdTimestamp;
        this.account = account;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.sourceClass);
        zzbig.zza(parcel, 4, this.sourcePackageName, false);
        zzbig.zza(parcel, 5, this.sourceCorpusHandle, false);
        zzbig.zza(parcel, 6, this.inputByUser);
        zzbig.zza(parcel, 8, this.zzc, false);
        zzbig.zza(parcel, 9, this.score);
        zzbig.zza(parcel, 10, this.createdTimestamp);
        zzbig.zza(parcel, 11, (Parcelable)this.account, n, false);
        zzbig.zza(parcel, zza);
    }
}
