package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Locale;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class UsageInfo extends zzbid
{
    public static final Parcelable.Creator<UsageInfo> CREATOR;
    public final String query;
    private final DocumentId zza;
    private final long zzb;
    private int zzc;
    private final DocumentContents zzd;
    private final boolean zze;
    private int zzf;
    private int zzg;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaq();
    }
    
    UsageInfo(final DocumentId zza, final long zzb, final int zzc, final String query, final DocumentContents zzd, final boolean zze, final int zzf, final int zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.query = query;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    @Override
    public String toString() {
        return String.format(Locale.US, "UsageInfo[documentId=%s, timestamp=%d, usageType=%d, status=%d]", this.zza, this.zzb, this.zzc, this.zzg);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, this.query, false);
        zzbig.zza(parcel, 5, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, 6, this.zze);
        zzbig.zza(parcel, 7, this.zzf);
        zzbig.zza(parcel, 8, this.zzg);
        zzbig.zza(parcel, zza);
    }
}
