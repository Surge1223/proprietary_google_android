package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class PIMEUpdateResponse extends zzbid
{
    public static final Parcelable.Creator<PIMEUpdateResponse> CREATOR;
    public final byte[] nextIterToken;
    public final PIMEUpdate[] updates;
    private final String zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzx();
    }
    
    PIMEUpdateResponse(final String zza, final byte[] nextIterToken, final PIMEUpdate[] updates) {
        this.zza = zza;
        this.nextIterToken = nextIterToken;
        this.updates = updates;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.nextIterToken, false);
        zzbig.zza(parcel, 3, this.updates, n, false);
        zzbig.zza(parcel, zza);
    }
}
