package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class StorageStats extends zzbid
{
    public static final Parcelable.Creator<StorageStats> CREATOR;
    public final long allServicesDiskUsageBytes;
    public final long otherReclaimableBytes;
    public final RegisteredPackageInfo[] packageStats;
    public final long searchDiskUsageBytes;
    
    static {
        CREATOR = (Parcelable.Creator)new zzam();
    }
    
    public StorageStats(final RegisteredPackageInfo[] packageStats, final long otherReclaimableBytes, final long searchDiskUsageBytes, final long allServicesDiskUsageBytes) {
        this.packageStats = packageStats;
        this.otherReclaimableBytes = otherReclaimableBytes;
        this.searchDiskUsageBytes = searchDiskUsageBytes;
        this.allServicesDiskUsageBytes = allServicesDiskUsageBytes;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.packageStats, n, false);
        zzbig.zza(parcel, 2, this.otherReclaimableBytes);
        zzbig.zza(parcel, 3, this.searchDiskUsageBytes);
        zzbig.zza(parcel, 4, this.allServicesDiskUsageBytes);
        zzbig.zza(parcel, zza);
    }
}
