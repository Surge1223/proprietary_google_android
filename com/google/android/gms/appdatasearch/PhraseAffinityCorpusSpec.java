package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class PhraseAffinityCorpusSpec extends zzbid
{
    public static final Parcelable.Creator<PhraseAffinityCorpusSpec> CREATOR;
    public final CorpusId corpus;
    private final Bundle zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzy();
    }
    
    PhraseAffinityCorpusSpec(final CorpusId corpus, final Bundle zza) {
        this.corpus = corpus;
        this.zza = zza;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.corpus, n, false);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
