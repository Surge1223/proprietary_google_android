package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import android.text.TextUtils;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

@Deprecated
public class GlobalSearchApplicationInfo extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<GlobalSearchApplicationInfo> CREATOR;
    public final String defaultIntentAction;
    public final String defaultIntentActivity;
    public final String defaultIntentData;
    public final int iconId;
    public final int labelId;
    public final int settingsDescriptionId;
    public final String sourceName;
    private final String zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzp();
    }
    
    public GlobalSearchApplicationInfo(final String zza, final String sourceName, final int labelId, final int settingsDescriptionId, final int iconId, final String defaultIntentAction, final String defaultIntentData, final String defaultIntentActivity) {
        this.zza = zza;
        this.sourceName = sourceName;
        this.labelId = labelId;
        this.settingsDescriptionId = settingsDescriptionId;
        this.iconId = iconId;
        this.defaultIntentAction = defaultIntentAction;
        this.defaultIntentData = defaultIntentData;
        this.defaultIntentActivity = defaultIntentActivity;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof GlobalSearchApplicationInfo) {
            final GlobalSearchApplicationInfo globalSearchApplicationInfo = (GlobalSearchApplicationInfo)o;
            return TextUtils.equals((CharSequence)this.zza, (CharSequence)globalSearchApplicationInfo.zza) && TextUtils.equals((CharSequence)this.sourceName, (CharSequence)globalSearchApplicationInfo.sourceName) && this.labelId == globalSearchApplicationInfo.labelId && this.settingsDescriptionId == globalSearchApplicationInfo.settingsDescriptionId && this.iconId == globalSearchApplicationInfo.iconId && TextUtils.equals((CharSequence)this.defaultIntentAction, (CharSequence)globalSearchApplicationInfo.defaultIntentAction) && TextUtils.equals((CharSequence)this.defaultIntentData, (CharSequence)globalSearchApplicationInfo.defaultIntentData) && TextUtils.equals((CharSequence)this.defaultIntentActivity, (CharSequence)globalSearchApplicationInfo.defaultIntentActivity);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.sourceName, this.labelId, this.settingsDescriptionId, this.iconId, this.defaultIntentAction, this.defaultIntentData, this.defaultIntentActivity });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{");
        sb.append(this.zza);
        sb.append(";sourceName=");
        sb.append(this.sourceName);
        sb.append(";labelId=");
        sb.append(Integer.toHexString(this.labelId));
        sb.append(";settingsDescriptionId=");
        sb.append(Integer.toHexString(this.settingsDescriptionId));
        sb.append(";iconId=");
        sb.append(Integer.toHexString(this.iconId));
        sb.append(";defaultIntentAction=");
        sb.append(this.defaultIntentAction);
        sb.append(";defaultIntentData=");
        sb.append(this.defaultIntentData);
        sb.append(";defaultIntentActivity=");
        sb.append(this.defaultIntentActivity);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.labelId);
        zzbig.zza(parcel, 3, this.settingsDescriptionId);
        zzbig.zza(parcel, 4, this.iconId);
        zzbig.zza(parcel, 5, this.defaultIntentAction, false);
        zzbig.zza(parcel, 6, this.defaultIntentData, false);
        zzbig.zza(parcel, 7, this.defaultIntentActivity, false);
        zzbig.zza(parcel, 8, this.sourceName, false);
        zzbig.zza(parcel, zza);
    }
}
