package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GlobalSearchApplication extends zzbid
{
    public static final Parcelable.Creator<GlobalSearchApplication> CREATOR;
    public final GlobalSearchApplicationInfo appInfo;
    public final boolean enabled;
    private final zzm[] zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzo();
    }
    
    GlobalSearchApplication(final GlobalSearchApplicationInfo appInfo, final zzm[] zza, final boolean enabled) {
        this.appInfo = appInfo;
        this.zza = zza;
        this.enabled = enabled;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof GlobalSearchApplication) {
            final GlobalSearchApplication globalSearchApplication = (GlobalSearchApplication)o;
            return zzak.zza(this.appInfo, globalSearchApplication.appInfo) && zzak.zza(this.enabled, globalSearchApplication.enabled) && Arrays.equals(this.zza, globalSearchApplication.zza);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.appInfo, this.enabled, Arrays.hashCode(this.zza) });
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.appInfo, n, false);
        zzbig.zza(parcel, 2, this.zza, n, false);
        zzbig.zza(parcel, 3, this.enabled);
        zzbig.zza(parcel, zza);
    }
}
