package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzau;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class DocumentSection extends zzbid
{
    public static final Parcelable.Creator<DocumentSection> CREATOR;
    public static final int INVALID_GLOBAL_SEARCH_SECTION_ID;
    private static final RegisterSectionInfo zza;
    public final byte[] blobContent;
    public final String content;
    public final int globalSearchSectionType;
    private final RegisterSectionInfo zzb;
    
    static {
        INVALID_GLOBAL_SEARCH_SECTION_ID = Integer.parseInt("-1");
        CREATOR = (Parcelable.Creator)new zzh();
        zza = new RegisterSectionInfo.Builder("SsbContext").noIndex(true).format("blob").build();
    }
    
    DocumentSection(String string, final RegisterSectionInfo zzb, int globalSearchSectionType, final byte[] blobContent) {
        final boolean b = globalSearchSectionType == DocumentSection.INVALID_GLOBAL_SEARCH_SECTION_ID || GlobalSearchSections.getSectionName(globalSearchSectionType) != null;
        final StringBuilder sb = new StringBuilder(32);
        sb.append("Invalid section type ");
        sb.append(globalSearchSectionType);
        zzau.zzb(b, sb.toString());
        this.content = string;
        this.zzb = zzb;
        this.globalSearchSectionType = globalSearchSectionType;
        this.blobContent = blobContent;
        if (this.globalSearchSectionType != DocumentSection.INVALID_GLOBAL_SEARCH_SECTION_ID && GlobalSearchSections.getSectionName(this.globalSearchSectionType) == null) {
            globalSearchSectionType = this.globalSearchSectionType;
            final StringBuilder sb2 = new StringBuilder(32);
            sb2.append("Invalid section type ");
            sb2.append(globalSearchSectionType);
            string = sb2.toString();
        }
        else if (this.content != null && this.blobContent != null) {
            string = "Both content and blobContent set";
        }
        else {
            string = null;
        }
        if (string == null) {
            return;
        }
        throw new IllegalArgumentException(string);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.content, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, 4, this.globalSearchSectionType);
        zzbig.zza(parcel, 5, this.blobContent, false);
        zzbig.zza(parcel, zza);
    }
}
