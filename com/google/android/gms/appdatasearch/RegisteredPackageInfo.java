package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RegisteredPackageInfo extends zzbid
{
    public static final Parcelable.Creator<RegisteredPackageInfo> CREATOR;
    public final boolean blocked;
    public final String packageName;
    public final long reclaimableDiskBytes;
    public final long usedDiskBytes;
    
    static {
        CREATOR = (Parcelable.Creator)new zzae();
    }
    
    public RegisteredPackageInfo(final String packageName, final long usedDiskBytes, final boolean blocked, final long reclaimableDiskBytes) {
        this.packageName = packageName;
        this.usedDiskBytes = usedDiskBytes;
        this.blocked = blocked;
        this.reclaimableDiskBytes = reclaimableDiskBytes;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.packageName, false);
        zzbig.zza(parcel, 2, this.usedDiskBytes);
        zzbig.zza(parcel, 3, this.blocked);
        zzbig.zza(parcel, 4, this.reclaimableDiskBytes);
        zzbig.zza(parcel, zza);
    }
}
