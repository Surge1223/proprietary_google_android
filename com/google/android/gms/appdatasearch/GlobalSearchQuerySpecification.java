package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GlobalSearchQuerySpecification extends zzbid
{
    public static final Parcelable.Creator<GlobalSearchQuerySpecification> CREATOR;
    public final String context;
    public final byte[] experimentOverrideConfig;
    public final String origin;
    public final boolean prefixMatch;
    public final boolean prefixMatchAllTokens;
    public final int queryTokenizer;
    public final int rankingStrategy;
    public final boolean returnPerCorpusResults;
    public final int scoringVerbosityLevel;
    public final STSortSpec stSortSpec;
    public final boolean useSectionRestricts;
    public final int[] wantedAnnotationTypes;
    private final CorpusId[] zza;
    private final CorpusScoringInfo[] zzb;
    private final int zzc;
    private final byte[] zzd;
    private final transient Map<String, Set<String>> zze;
    private final transient Map<CorpusId, CorpusScoringInfo> zzf;
    
    static {
        CREATOR = (Parcelable.Creator)new zzr();
    }
    
    GlobalSearchQuerySpecification(final CorpusId[] zza, int i, final CorpusScoringInfo[] zzb, int zzc, final int rankingStrategy, final int queryTokenizer, final String context, final boolean returnPerCorpusResults, final byte[] experimentOverrideConfig, final boolean prefixMatch, final boolean prefixMatchAllTokens, final boolean useSectionRestricts, final int[] wantedAnnotationTypes, final byte[] zzd, final STSortSpec stSortSpec, final String origin) {
        this.zza = zza;
        this.scoringVerbosityLevel = i;
        this.zzc = zzc;
        this.rankingStrategy = rankingStrategy;
        this.queryTokenizer = queryTokenizer;
        this.context = context;
        this.returnPerCorpusResults = returnPerCorpusResults;
        this.experimentOverrideConfig = experimentOverrideConfig;
        this.prefixMatch = prefixMatch;
        this.prefixMatchAllTokens = prefixMatchAllTokens;
        this.useSectionRestricts = useSectionRestricts;
        this.wantedAnnotationTypes = wantedAnnotationTypes;
        this.zzd = zzd;
        this.stSortSpec = stSortSpec;
        this.origin = origin;
        this.zzb = zzb;
        zzc = 0;
        if (zza != null && zza.length != 0) {
            this.zze = new HashMap<String, Set<String>>();
            Set<String> set;
            for (i = 0; i < zza.length; ++i) {
                if ((set = this.zze.get(zza[i].packageName)) == null) {
                    set = new HashSet<String>();
                    this.zze.put(zza[i].packageName, set);
                }
                if (zza[i].corpusName != null) {
                    set.add(zza[i].corpusName);
                }
            }
        }
        else {
            this.zze = null;
        }
        if (zzb != null && zzb.length != 0) {
            this.zzf = new HashMap<CorpusId, CorpusScoringInfo>(zzb.length);
            for (i = zzc; i < zzb.length; ++i) {
                this.zzf.put(zzb[i].corpus, zzb[i]);
            }
            return;
        }
        this.zzf = null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof GlobalSearchQuerySpecification) {
            final GlobalSearchQuerySpecification globalSearchQuerySpecification = (GlobalSearchQuerySpecification)o;
            return zzak.zza(this.scoringVerbosityLevel, globalSearchQuerySpecification.scoringVerbosityLevel) && zzak.zza(this.zzc, globalSearchQuerySpecification.zzc) && zzak.zza(this.rankingStrategy, globalSearchQuerySpecification.rankingStrategy) && zzak.zza(this.queryTokenizer, globalSearchQuerySpecification.queryTokenizer) && zzak.zza(this.context, globalSearchQuerySpecification.context) && zzak.zza(this.returnPerCorpusResults, globalSearchQuerySpecification.returnPerCorpusResults) && zzak.zza(this.prefixMatch, globalSearchQuerySpecification.prefixMatch) && zzak.zza(this.prefixMatchAllTokens, globalSearchQuerySpecification.prefixMatchAllTokens) && zzak.zza(this.useSectionRestricts, globalSearchQuerySpecification.useSectionRestricts) && zzak.zza(this.zzf, globalSearchQuerySpecification.zzf) && zzak.zza(this.stSortSpec, globalSearchQuerySpecification.stSortSpec) && zzak.zza(this.origin, globalSearchQuerySpecification.origin) && Arrays.equals(this.zza, globalSearchQuerySpecification.zza) && Arrays.equals(this.experimentOverrideConfig, globalSearchQuerySpecification.experimentOverrideConfig) && Arrays.equals(this.zzb, globalSearchQuerySpecification.zzb) && Arrays.equals(this.wantedAnnotationTypes, globalSearchQuerySpecification.wantedAnnotationTypes) && Arrays.equals(this.zzd, globalSearchQuerySpecification.zzd);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.scoringVerbosityLevel, this.zzc, this.rankingStrategy, this.queryTokenizer, this.context, this.returnPerCorpusResults, this.prefixMatch, this.prefixMatchAllTokens, this.useSectionRestricts, this.zzf, this.stSortSpec, this.origin, Arrays.hashCode(this.zza), Arrays.hashCode(this.experimentOverrideConfig), Arrays.hashCode(this.zzb), Arrays.hashCode(this.zzd) });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (this.zze != null) {
            sb.append("mFilter\n");
            for (final String s : this.zze.keySet()) {
                final Iterator<String> iterator2 = this.zze.get(s).iterator();
                String string = "";
                while (iterator2.hasNext()) {
                    final String value = String.valueOf(string);
                    final String s2 = iterator2.next();
                    final StringBuilder sb2 = new StringBuilder(1 + String.valueOf(value).length() + String.valueOf(s2).length());
                    sb2.append(value);
                    sb2.append(s2);
                    sb2.append(",");
                    string = sb2.toString();
                }
                sb.append("key:");
                sb.append(s);
                sb.append(", values:");
                sb.append(string);
                sb.append("\n");
            }
        }
        if (this.zzf != null) {
            sb.append("mCorpusScoringInfoMap\n");
            final Iterator<CorpusId> iterator3 = this.zzf.keySet().iterator();
            while (iterator3.hasNext()) {
                sb.append(String.valueOf(iterator3.next().toString()).concat("\n"));
            }
        }
        if (this.stSortSpec != null) {
            sb.append("STSortSpec: ");
            sb.append(this.stSortSpec.toString());
            sb.append("\n");
        }
        if (this.origin != null) {
            sb.append("Origin: ");
            sb.append(this.origin);
            sb.append("\n");
        }
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, n, false);
        zzbig.zza(parcel, 2, this.scoringVerbosityLevel);
        zzbig.zza(parcel, 3, this.zzb, n, false);
        zzbig.zza(parcel, 4, this.zzc);
        zzbig.zza(parcel, 5, this.rankingStrategy);
        zzbig.zza(parcel, 6, this.queryTokenizer);
        zzbig.zza(parcel, 7, this.context, false);
        zzbig.zza(parcel, 8, this.returnPerCorpusResults);
        zzbig.zza(parcel, 9, this.experimentOverrideConfig, false);
        zzbig.zza(parcel, 10, this.prefixMatch);
        zzbig.zza(parcel, 11, this.prefixMatchAllTokens);
        zzbig.zza(parcel, 12, this.useSectionRestricts);
        zzbig.zza(parcel, 13, this.wantedAnnotationTypes, false);
        zzbig.zza(parcel, 14, this.zzd, false);
        zzbig.zza(parcel, 15, (Parcelable)this.stSortSpec, n, false);
        zzbig.zza(parcel, 16, this.origin, false);
        zzbig.zza(parcel, zza);
    }
}
