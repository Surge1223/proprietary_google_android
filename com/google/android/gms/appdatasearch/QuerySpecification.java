package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class QuerySpecification extends zzbid
{
    public static final Parcelable.Creator<QuerySpecification> CREATOR;
    public final String origin;
    public final boolean prefixMatch;
    public final boolean prefixMatchAllTokens;
    public final int queryTokenizer;
    public final int rankingStrategy;
    public final boolean semanticSectionNames;
    public final STSortSpec stSortSpec;
    public final boolean wantUris;
    public final int[] wantedAnnotationTypes;
    public final List<Section> wantedSections;
    public final List<String> wantedTags;
    private final int zza;
    private final byte[] zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaa();
    }
    
    QuerySpecification(final boolean wantUris, final List<String> wantedTags, final List<Section> wantedSections, final boolean prefixMatch, final int zza, final int rankingStrategy, final boolean semanticSectionNames, final int queryTokenizer, final boolean prefixMatchAllTokens, final int[] wantedAnnotationTypes, final byte[] zzb, final STSortSpec stSortSpec, final String origin) {
        this.wantUris = wantUris;
        this.wantedTags = wantedTags;
        this.wantedSections = wantedSections;
        this.prefixMatch = prefixMatch;
        this.zza = zza;
        this.rankingStrategy = rankingStrategy;
        this.semanticSectionNames = semanticSectionNames;
        this.queryTokenizer = queryTokenizer;
        this.prefixMatchAllTokens = prefixMatchAllTokens;
        this.wantedAnnotationTypes = wantedAnnotationTypes;
        this.zzb = zzb;
        this.stSortSpec = stSortSpec;
        this.origin = origin;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.wantUris);
        zzbig.zzb(parcel, 2, this.wantedTags, false);
        zzbig.zzc(parcel, 3, this.wantedSections, false);
        zzbig.zza(parcel, 4, this.prefixMatch);
        zzbig.zza(parcel, 5, this.zza);
        zzbig.zza(parcel, 6, this.rankingStrategy);
        zzbig.zza(parcel, 7, this.semanticSectionNames);
        zzbig.zza(parcel, 8, this.queryTokenizer);
        zzbig.zza(parcel, 9, this.prefixMatchAllTokens);
        zzbig.zza(parcel, 10, this.wantedAnnotationTypes, false);
        zzbig.zza(parcel, 11, this.zzb, false);
        zzbig.zza(parcel, 12, (Parcelable)this.stSortSpec, n, false);
        zzbig.zza(parcel, 13, this.origin, false);
        zzbig.zza(parcel, zza);
    }
}
