package com.google.android.gms.appdatasearch;

import java.util.HashMap;
import java.util.Map;

public class GlobalSearchSections
{
    static final String[] zza;
    private static final Map<String, Integer> zzb;
    
    static {
        zza = new String[] { "text1", "text2", "icon", "intent_action", "intent_data", "intent_data_id", "intent_extra_data", "suggest_large_icon", "intent_activity", "thing_proto" };
        zzb = new HashMap<String, Integer>(GlobalSearchSections.zza.length);
        for (int i = 0; i < GlobalSearchSections.zza.length; ++i) {
            GlobalSearchSections.zzb.put(GlobalSearchSections.zza[i], i);
        }
    }
    
    public static String getSectionName(final int n) {
        if (n >= 0 && n < GlobalSearchSections.zza.length) {
            return GlobalSearchSections.zza[n];
        }
        return null;
    }
}
