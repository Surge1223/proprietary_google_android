package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import com.google.android.gms.common.internal.zzak;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class CorpusStatus extends zzbid
{
    public static final Parcelable.Creator<CorpusStatus> CREATOR;
    private final boolean zza;
    private final long zzb;
    private final long zzc;
    private final long zzd;
    private final Bundle zze;
    private final String zzf;
    private final String zzg;
    
    static {
        CREATOR = (Parcelable.Creator)new zzd();
    }
    
    CorpusStatus() {
        this(false, 0L, 0L, 0L, null, null, null);
    }
    
    CorpusStatus(final boolean zza, final long zzb, final long zzc, final long zzd, final Bundle bundle, final String zzf, final String zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        Bundle zze = bundle;
        if (bundle == null) {
            zze = new Bundle();
        }
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof CorpusStatus) {
            final CorpusStatus corpusStatus = (CorpusStatus)o;
            return zzak.zza(this.zza, corpusStatus.zza) && zzak.zza(this.zzb, corpusStatus.zzb) && zzak.zza(this.zzc, corpusStatus.zzc) && zzak.zza(this.zzd, corpusStatus.zzd) && zzak.zza(this.getCounters(), corpusStatus.getCounters()) && zzak.zza(this.zzg, corpusStatus.zzg);
        }
        return false;
    }
    
    public Map<String, Integer> getCounters() {
        final HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        for (final String s : this.zze.keySet()) {
            final int int1 = this.zze.getInt(s, -1);
            if (int1 != -1) {
                hashMap.put(s, int1);
            }
        }
        return hashMap;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc, this.zzd, this.getCounters(), this.zzg });
    }
    
    @Override
    public String toString() {
        final boolean zza = this.zza;
        final String zzg = this.zzg;
        final long zzb = this.zzb;
        final long zzc = this.zzc;
        final long zzd = this.zzd;
        final String value = String.valueOf(this.zze);
        final StringBuilder sb = new StringBuilder(181 + String.valueOf(zzg).length() + String.valueOf(value).length());
        sb.append("CorpusStatus{found=");
        sb.append(zza);
        sb.append(", contentIncarnation=");
        sb.append(zzg);
        sb.append(", lastIndexedSeqno=");
        sb.append(zzb);
        sb.append(", lastCommittedSeqno=");
        sb.append(zzc);
        sb.append(", committedNumDocuments=");
        sb.append(zzd);
        sb.append(", counters=");
        sb.append(value);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, this.zzd);
        zzbig.zza(parcel, 5, this.zze, false);
        zzbig.zza(parcel, 6, this.zzf, false);
        zzbig.zza(parcel, 7, this.zzg, false);
        zzbig.zza(parcel, zza);
    }
}
