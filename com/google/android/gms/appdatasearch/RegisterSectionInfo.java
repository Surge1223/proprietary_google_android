package com.google.android.gms.appdatasearch;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RegisterSectionInfo extends zzbid
{
    public static final Parcelable.Creator<RegisterSectionInfo> CREATOR;
    public final Feature[] features;
    public final String format;
    public final boolean indexPrefixes;
    public final String name;
    public final boolean noIndex;
    public final String schemaOrgProperty;
    public final String subsectionSeparator;
    public final int weight;
    private final zzah zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzad();
    }
    
    RegisterSectionInfo(final String name, final String format, final boolean noIndex, final int weight, final boolean indexPrefixes, final String subsectionSeparator, final Feature[] features, final String schemaOrgProperty, final zzah zza) {
        this.name = name;
        this.format = format;
        this.noIndex = noIndex;
        this.weight = weight;
        this.indexPrefixes = indexPrefixes;
        this.subsectionSeparator = subsectionSeparator;
        this.features = features;
        this.schemaOrgProperty = schemaOrgProperty;
        this.zza = zza;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.name, false);
        zzbig.zza(parcel, 2, this.format, false);
        zzbig.zza(parcel, 3, this.noIndex);
        zzbig.zza(parcel, 4, this.weight);
        zzbig.zza(parcel, 5, this.indexPrefixes);
        zzbig.zza(parcel, 6, this.subsectionSeparator, false);
        zzbig.zza(parcel, 7, this.features, n, false);
        zzbig.zza(parcel, 11, this.schemaOrgProperty, false);
        zzbig.zza(parcel, 12, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, zza);
    }
    
    public static final class Builder
    {
        private final String zza;
        private String zzb;
        private boolean zzc;
        private int zzd;
        private boolean zze;
        private String zzf;
        private final List<Feature> zzg;
        private String zzh;
        
        public Builder(final String zza) {
            this.zza = zza;
            this.zzd = 1;
            this.zzg = new ArrayList<Feature>();
        }
        
        public final RegisterSectionInfo build() {
            return new RegisterSectionInfo(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg.toArray(new Feature[this.zzg.size()]), this.zzh, null);
        }
        
        public final Builder format(final String zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public final Builder noIndex(final boolean zzc) {
            this.zzc = zzc;
            return this;
        }
    }
}
