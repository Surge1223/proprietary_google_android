package com.google.android.gms.appdatasearch;

import com.google.android.gms.common.api.Status;
import java.util.List;
import com.google.android.gms.common.api.Result;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.accounts.Account;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GetRecentContextCall
{
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        public final Account filterAccount;
        public final String filterPackageName;
        public final boolean includeDeviceOnlyData;
        public final boolean includeThirdPartyContext;
        public final boolean includeUsageEnded;
        
        static {
            CREATOR = (Parcelable.Creator)new zzk();
        }
        
        public Request() {
            this(null, false, false, false, null);
        }
        
        public Request(final Account filterAccount, final boolean includeDeviceOnlyData, final boolean includeThirdPartyContext, final boolean includeUsageEnded, final String filterPackageName) {
            this.filterAccount = filterAccount;
            this.includeDeviceOnlyData = includeDeviceOnlyData;
            this.includeThirdPartyContext = includeThirdPartyContext;
            this.includeUsageEnded = includeUsageEnded;
            this.filterPackageName = filterPackageName;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.filterAccount, n, false);
            zzbig.zza(parcel, 2, this.includeDeviceOnlyData);
            zzbig.zza(parcel, 3, this.includeThirdPartyContext);
            zzbig.zza(parcel, 4, this.includeUsageEnded);
            zzbig.zza(parcel, 5, this.filterPackageName, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public List<UsageInfo> context;
        public Status status;
        @Deprecated
        public String[] topRunningPackages;
        
        static {
            CREATOR = (Parcelable.Creator)new zzl();
        }
        
        public Response() {
        }
        
        Response(final Status status, final List<UsageInfo> context, final String[] topRunningPackages) {
            this.status = status;
            this.context = context;
            this.topRunningPackages = topRunningPackages;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zzc(parcel, 2, this.context, false);
            zzbig.zza(parcel, 3, this.topRunningPackages, false);
            zzbig.zza(parcel, zza);
        }
    }
}
