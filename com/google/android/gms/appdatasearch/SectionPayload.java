package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.util.SparseArray;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SectionPayload extends zzbid
{
    public static final Parcelable.Creator<SectionPayload> CREATOR;
    final SparseArray<byte[]> zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzal();
    }
    
    SectionPayload(final SparseArray<byte[]> zza) {
        this.zza = zza;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
