package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Section extends zzbid
{
    public static final Parcelable.Creator<Section> CREATOR;
    public final String name;
    public final int snippetLength;
    public final boolean snippeted;
    
    static {
        CREATOR = (Parcelable.Creator)new zzak();
    }
    
    public Section(final String name, final boolean snippeted, final int snippetLength) {
        this.name = name;
        this.snippeted = snippeted;
        this.snippetLength = snippetLength;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.name, false);
        zzbig.zza(parcel, 2, this.snippeted);
        zzbig.zza(parcel, 3, this.snippetLength);
        zzbig.zza(parcel, zza);
    }
}
