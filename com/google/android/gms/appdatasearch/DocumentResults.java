package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class DocumentResults extends zzbid
{
    public static final Parcelable.Creator<DocumentResults> CREATOR;
    private final String zza;
    private final Bundle zzb;
    private final Bundle zzc;
    private final Bundle zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zzg();
    }
    
    DocumentResults(final String zza, final Bundle zzb, final Bundle zzc, final Bundle zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, 4, this.zzd, false);
        zzbig.zza(parcel, zza);
    }
}
