package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class CorpusId extends zzbid
{
    public static final Parcelable.Creator<CorpusId> CREATOR;
    public final String corpusName;
    public final String packageName;
    private final Bundle zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzb();
    }
    
    public CorpusId(final String packageName, final String corpusName, final Bundle zza) {
        this.packageName = packageName;
        this.corpusName = corpusName;
        this.zza = zza;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof CorpusId) {
            final CorpusId corpusId = (CorpusId)o;
            return zzak.zza(this.packageName, corpusId.packageName) && zzak.zza(this.corpusName, corpusId.corpusName) && zzak.zza(this.zza, corpusId.zza);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.packageName, this.corpusName, this.zza });
    }
    
    @Override
    public String toString() {
        final String packageName = this.packageName;
        final String corpusName = this.corpusName;
        String string;
        if (this.zza != null) {
            string = this.zza.toString();
        }
        else {
            string = "null";
        }
        final StringBuilder sb = new StringBuilder(38 + String.valueOf(packageName).length() + String.valueOf(corpusName).length() + String.valueOf(string).length());
        sb.append("CorpusId[package=");
        sb.append(packageName);
        sb.append(", corpus=");
        sb.append(corpusName);
        sb.append("userHandle=");
        sb.append(string);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.packageName, false);
        zzbig.zza(parcel, 2, this.corpusName, false);
        zzbig.zza(parcel, 3, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
