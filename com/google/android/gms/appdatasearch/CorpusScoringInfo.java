package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class CorpusScoringInfo extends zzbid
{
    public static final Parcelable.Creator<CorpusScoringInfo> CREATOR;
    public final CorpusId corpus;
    public final int maxResults;
    public final int weight;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    public CorpusScoringInfo(final CorpusId corpus, final int weight, final int maxResults) {
        this.corpus = corpus;
        this.weight = weight;
        this.maxResults = maxResults;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.corpus, n, false);
        zzbig.zza(parcel, 2, this.weight);
        zzbig.zza(parcel, 3, this.maxResults);
        zzbig.zza(parcel, zza);
    }
}
