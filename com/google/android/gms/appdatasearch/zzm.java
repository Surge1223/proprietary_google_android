package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzm extends zzbid
{
    public static final Parcelable.Creator<zzm> CREATOR;
    final String zza;
    final Feature[] zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzn();
    }
    
    zzm(final String zza, final Feature[] zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, n, false);
        zzbig.zza(parcel, zza);
    }
}
