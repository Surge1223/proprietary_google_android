package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RegisterCorpusIMEInfo extends zzbid
{
    public static final Parcelable.Creator<RegisterCorpusIMEInfo> CREATOR;
    public final String[] sectionNames;
    public final int sourceClass;
    public final String toAddressesSectionName;
    public final String userInputSectionName;
    public final String[] userInputSectionValues;
    public final String userInputTag;
    
    static {
        CREATOR = (Parcelable.Creator)new zzab();
    }
    
    public RegisterCorpusIMEInfo(final int sourceClass, final String[] sectionNames, final String userInputTag, final String userInputSectionName, final String[] userInputSectionValues, final String toAddressesSectionName) {
        this.sourceClass = sourceClass;
        this.sectionNames = sectionNames;
        this.userInputTag = userInputTag;
        this.userInputSectionName = userInputSectionName;
        this.userInputSectionValues = userInputSectionValues;
        this.toAddressesSectionName = toAddressesSectionName;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.sourceClass);
        zzbig.zza(parcel, 2, this.sectionNames, false);
        zzbig.zza(parcel, 3, this.userInputTag, false);
        zzbig.zza(parcel, 4, this.userInputSectionName, false);
        zzbig.zza(parcel, 6, this.toAddressesSectionName, false);
        zzbig.zza(parcel, 7, this.userInputSectionValues, false);
        zzbig.zza(parcel, zza);
    }
}
