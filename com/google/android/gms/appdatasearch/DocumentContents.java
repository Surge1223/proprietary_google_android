package com.google.android.gms.appdatasearch;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.accounts.Account;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class DocumentContents extends zzbid
{
    public static final Parcelable.Creator<DocumentContents> CREATOR;
    public final Account account;
    public final boolean globalSearchEnabled;
    public final String schemaOrgType;
    private final DocumentSection[] zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    DocumentContents(final DocumentSection[] zza, final String schemaOrgType, final boolean globalSearchEnabled, final Account account) {
        this.zza = zza;
        this.schemaOrgType = schemaOrgType;
        this.globalSearchEnabled = globalSearchEnabled;
        this.account = account;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof DocumentContents) {
            final DocumentContents documentContents = (DocumentContents)o;
            return zzak.zza(this.schemaOrgType, documentContents.schemaOrgType) && zzak.zza(this.globalSearchEnabled, documentContents.globalSearchEnabled) && zzak.zza(this.account, documentContents.account) && Arrays.equals(this.getDocumentSectionContents(), documentContents.getDocumentSectionContents());
        }
        return false;
    }
    
    public DocumentSection[] getDocumentSectionContents() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.schemaOrgType, this.globalSearchEnabled, this.account, Arrays.hashCode(this.zza) });
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, n, false);
        zzbig.zza(parcel, 2, this.schemaOrgType, false);
        zzbig.zza(parcel, 3, this.globalSearchEnabled);
        zzbig.zza(parcel, 4, (Parcelable)this.account, n, false);
        zzbig.zza(parcel, zza);
    }
}
