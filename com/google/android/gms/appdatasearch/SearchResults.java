package com.google.android.gms.appdatasearch;

import java.util.NoSuchElementException;
import java.util.Map;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SearchResults extends zzbid implements Iterable<Result>
{
    public static final Parcelable.Creator<SearchResults> CREATOR;
    final int[] zza;
    final byte[] zzb;
    final Bundle[] zzc;
    final Bundle[] zzd;
    final Bundle[] zze;
    final int[] zzf;
    final String[] zzg;
    final double[] zzh;
    final Bundle zzi;
    final long[] zzj;
    final long[] zzk;
    final Bundle[] zzl;
    final int[] zzm;
    final byte[] zzn;
    private final String zzo;
    private final int zzp;
    private final byte[] zzq;
    private final int zzr;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaj();
    }
    
    SearchResults(final String zzo, final int[] zza, final byte[] zzb, final Bundle[] zzc, final Bundle[] zzd, final Bundle[] zze, final int zzp, final int[] zzf, final String[] zzg, final byte[] zzq, final double[] zzh, final Bundle zzi, final int zzr, final long[] zzj, final long[] zzk, final Bundle[] zzl, final int[] zzm, final byte[] zzn) {
        this.zzo = zzo;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzp = zzp;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzq = zzq;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzr = zzr;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = zzn;
    }
    
    public int getNumResults() {
        return this.zzp;
    }
    
    public boolean hasError() {
        return this.zzo != null;
    }
    
    @Override
    public ResultIterator iterator() {
        return new ResultIterator();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzo, false);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb, false);
        zzbig.zza(parcel, 4, this.zzc, n, false);
        zzbig.zza(parcel, 5, this.zzd, n, false);
        zzbig.zza(parcel, 6, this.zze, n, false);
        zzbig.zza(parcel, 7, this.zzp);
        zzbig.zza(parcel, 8, this.zzf, false);
        zzbig.zza(parcel, 9, this.zzg, false);
        zzbig.zza(parcel, 10, this.zzq, false);
        zzbig.zza(parcel, 11, this.zzh, false);
        zzbig.zza(parcel, 12, this.zzi, false);
        zzbig.zza(parcel, 13, this.zzr);
        zzbig.zza(parcel, 14, this.zzj, false);
        zzbig.zza(parcel, 15, this.zzk, false);
        zzbig.zza(parcel, 16, this.zzl, n, false);
        zzbig.zza(parcel, 17, this.zzm, false);
        zzbig.zza(parcel, 18, this.zzn, false);
        zzbig.zza(parcel, zza);
    }
    
    public class Result
    {
        private final ResultIterator zza;
        private final int zzb;
        
        Result(final int zzb, final ResultIterator zza) {
            this.zza = zza;
            this.zzb = zzb;
        }
    }
    
    public class ResultIterator implements Iterator<Result>
    {
        protected int mCurIdx;
        private final Map<String, Object>[] zzc;
        
        ResultIterator() {
            Map<String, Object>[] zzc;
            if (SearchResults.this.hasError()) {
                zzc = null;
            }
            else {
                zzc = (Map<String, Object>[])new Map[SearchResults.this.zzg.length];
            }
            this.zzc = zzc;
        }
        
        @Override
        public boolean hasNext() {
            return !SearchResults.this.hasError() && this.mCurIdx < SearchResults.this.getNumResults();
        }
        
        protected void moveToNext() {
            ++this.mCurIdx;
        }
        
        @Override
        public Result next() {
            if (this.hasNext()) {
                final Result result = new Result(this.mCurIdx, this);
                this.moveToNext();
                return result;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
