package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzah extends zzbid
{
    public static final Parcelable.Creator<zzah> CREATOR;
    private final boolean zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzai();
    }
    
    public zzah(final boolean zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, zza);
    }
}
