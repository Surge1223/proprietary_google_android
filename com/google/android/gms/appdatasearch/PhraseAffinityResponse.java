package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class PhraseAffinityResponse extends zzbid
{
    public static final Parcelable.Creator<PhraseAffinityResponse> CREATOR;
    private final String zza;
    private final CorpusId[] zzb;
    private final int[] zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzz();
    }
    
    PhraseAffinityResponse(final String zza, final CorpusId[] zzb, final int[] zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, n, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
}
