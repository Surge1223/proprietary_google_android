package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RequestIndexingSpecification extends zzbid
{
    public static final Parcelable.Creator<RequestIndexingSpecification> CREATOR;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaf();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        zzbig.zza(parcel, zzbig.zza(parcel));
    }
}
