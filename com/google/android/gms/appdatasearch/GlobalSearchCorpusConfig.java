package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GlobalSearchCorpusConfig extends zzbid
{
    public static final Parcelable.Creator<GlobalSearchCorpusConfig> CREATOR;
    public final Feature[] features;
    public final int[] globalSearchSectionMappings;
    
    static {
        CREATOR = (Parcelable.Creator)new zzq();
    }
    
    public GlobalSearchCorpusConfig(final int[] globalSearchSectionMappings, final Feature[] features) {
        this.globalSearchSectionMappings = globalSearchSectionMappings;
        this.features = features;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof GlobalSearchCorpusConfig) {
            final GlobalSearchCorpusConfig globalSearchCorpusConfig = (GlobalSearchCorpusConfig)o;
            return Arrays.equals(this.features, globalSearchCorpusConfig.features) && Arrays.equals(this.globalSearchSectionMappings, globalSearchCorpusConfig.globalSearchSectionMappings);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { Arrays.hashCode(this.globalSearchSectionMappings), Arrays.hashCode(this.features) });
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.globalSearchSectionMappings, false);
        zzbig.zza(parcel, 2, this.features, n, false);
        zzbig.zza(parcel, zza);
    }
}
