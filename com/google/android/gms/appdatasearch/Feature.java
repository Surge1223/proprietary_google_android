package com.google.android.gms.appdatasearch;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Feature extends zzbid
{
    public static final Parcelable.Creator<Feature> CREATOR;
    public final int id;
    private final Bundle zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzi();
    }
    
    Feature(final int id, final Bundle zza) {
        this.id = id;
        this.zza = zza;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Feature) {
            final Feature feature = (Feature)o;
            return zzak.zza(feature.id, this.id) && zzak.zza(feature.zza, this.zza);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.id, this.zza });
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.id);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
