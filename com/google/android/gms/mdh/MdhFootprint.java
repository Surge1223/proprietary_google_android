package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Locale;
import java.util.Arrays;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class MdhFootprint extends zzbid
{
    public static final Parcelable.Creator<MdhFootprint> CREATOR;
    private final byte[] zza;
    private final byte[] zzb;
    private final long zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    public MdhFootprint(final byte[] zza, final byte[] zzb, final long zzc) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzb = zzb;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final MdhFootprint mdhFootprint = (MdhFootprint)o;
            return this.zzc == mdhFootprint.zzc && Arrays.equals(this.zza, mdhFootprint.zza) && Arrays.equals(this.zzb, mdhFootprint.zzb);
        }
        return false;
    }
    
    public byte[] getData() {
        return this.zza;
    }
    
    public byte[] getSecondaryId() {
        return this.zzb;
    }
    
    public long getServerEventIdTimestamp() {
        return this.zzc;
    }
    
    @Override
    public int hashCode() {
        byte[] array;
        if (this.zza.length < 64) {
            array = this.zza;
        }
        else {
            array = Arrays.copyOf(this.zza, 64);
        }
        return Arrays.hashCode(new Object[] { Arrays.toString(array), Arrays.toString(this.zzb), this.zzc });
    }
    
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "MdhFootprint{sizeOf(data)=%d, secondaryId=%s, serverEventIdTimestamp=%d}", this.zza.length, Arrays.toString(this.zzb), this.zzc);
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getData(), false);
        zzbig.zza(parcel, 2, this.getSecondaryId(), false);
        zzbig.zza(parcel, 3, this.getServerEventIdTimestamp());
        zzbig.zza(parcel, zza);
    }
}
