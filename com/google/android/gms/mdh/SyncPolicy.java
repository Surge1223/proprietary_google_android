package com.google.android.gms.mdh;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SyncPolicy extends zzbid
{
    public static final Parcelable.Creator<SyncPolicy> CREATOR;
    public static final SyncPolicy DEFAULT;
    public static final SyncPolicy NO_SYNC;
    private int zza;
    private SyncSubPolicy zzb;
    private SyncSubPolicy zzc;
    private SyncSubPolicy zzd;
    private SyncSubPolicy zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzh();
        NO_SYNC = newBuilder().build();
        DEFAULT = newBuilder().zza().build();
    }
    
    public SyncPolicy(final int zza, final SyncSubPolicy zzb, final SyncSubPolicy zzc, final SyncSubPolicy zzd, final SyncSubPolicy zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final SyncPolicy syncPolicy = (SyncPolicy)o;
            return this.zza == syncPolicy.zza && this.zzb.equals(syncPolicy.zzb) && this.zzc.equals(syncPolicy.zzc) && this.zzd.equals(syncPolicy.zzd) && this.zze.equals(syncPolicy.zze);
        }
        return false;
    }
    
    public SyncSubPolicy getDownSyncPolicy() {
        return this.zzd;
    }
    
    public SyncSubPolicy getDownSyncWithListenerPolicy() {
        return this.zze;
    }
    
    public int getSyncType() {
        return this.zza;
    }
    
    public SyncSubPolicy getUpSyncPolicy() {
        return this.zzb;
    }
    
    public SyncSubPolicy getUpSyncWithListenerPolicy() {
        return this.zzc;
    }
    
    @Override
    public int hashCode() {
        return (((this.zza * 31 + this.zzb.hashCode()) * 31 + this.zzc.hashCode()) * 31 + this.zzd.hashCode()) * 31 + this.zze.hashCode();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getSyncType());
        zzbig.zza(parcel, 2, (Parcelable)this.getUpSyncPolicy(), n, false);
        zzbig.zza(parcel, 3, (Parcelable)this.getUpSyncWithListenerPolicy(), n, false);
        zzbig.zza(parcel, 4, (Parcelable)this.getDownSyncPolicy(), n, false);
        zzbig.zza(parcel, 5, (Parcelable)this.getDownSyncWithListenerPolicy(), n, false);
        zzbig.zza(parcel, zza);
    }
    
    public static class Builder
    {
        private int zza;
        private SyncSubPolicy zzb;
        private SyncSubPolicy zzc;
        private SyncSubPolicy zzd;
        private SyncSubPolicy zze;
        
        private Builder() {
            this.zza = 1;
            this.zzb = SyncSubPolicy.newBuilder().build();
            this.zzc = null;
            this.zzd = SyncSubPolicy.newBuilder().build();
            this.zze = null;
        }
        
        private final Builder zza() {
            this.zza = 0;
            return this;
        }
        
        public SyncPolicy build() {
            final int zza = this.zza;
            final SyncSubPolicy zzb = this.zzb;
            SyncSubPolicy syncSubPolicy;
            if (this.zzc != null) {
                syncSubPolicy = this.zzc;
            }
            else {
                syncSubPolicy = this.zzb;
            }
            final SyncSubPolicy zzd = this.zzd;
            SyncSubPolicy syncSubPolicy2;
            if (this.zze != null) {
                syncSubPolicy2 = this.zze;
            }
            else {
                syncSubPolicy2 = this.zzd;
            }
            return new SyncPolicy(zza, zzb, syncSubPolicy, zzd, syncSubPolicy2);
        }
    }
}
