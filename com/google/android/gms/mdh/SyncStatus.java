package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SyncStatus extends zzbid
{
    public static final Parcelable.Creator<SyncStatus> CREATOR;
    private final long zza;
    private final long zzb;
    private final long zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzi();
    }
    
    public SyncStatus(final long zza, final long zzb, final long zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final SyncStatus syncStatus = (SyncStatus)o;
            return zzak.zza(this.zza, syncStatus.zza) && zzak.zza(this.zzb, syncStatus.zzb) && zzak.zza(this.zzc, syncStatus.zzc);
        }
        return false;
    }
    
    public long getCurrentTimeMillisAtLastSync() {
        return this.zzb;
    }
    
    public long getElapsedRealtimeMillisAtLastSync() {
        return this.zza;
    }
    
    public long getServerTimeMillisAtLastSync() {
        return this.zzc;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc });
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getElapsedRealtimeMillisAtLastSync());
        zzbig.zza(parcel, 2, this.getCurrentTimeMillisAtLastSync());
        zzbig.zza(parcel, 3, this.getServerTimeMillisAtLastSync());
        zzbig.zza(parcel, zza);
    }
}
