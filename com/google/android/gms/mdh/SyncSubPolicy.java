package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SyncSubPolicy extends zzbid
{
    public static final Parcelable.Creator<SyncSubPolicy> CREATOR;
    private boolean zza;
    private int zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzk();
    }
    
    SyncSubPolicy(final boolean zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final SyncSubPolicy syncSubPolicy = (SyncSubPolicy)o;
            return this.zza == syncSubPolicy.zza && this.zzb == syncSubPolicy.zzb;
        }
        return false;
    }
    
    public int getThrottleDelaySeconds() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return (this.zza ? 1 : 0) * 31 + this.zzb;
    }
    
    public boolean isEnabled() {
        return this.zza;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.isEnabled());
        zzbig.zza(parcel, 2, this.getThrottleDelaySeconds());
        zzbig.zza(parcel, zza);
    }
    
    public static class Builder
    {
        private boolean zza;
        private int zzb;
        
        public SyncSubPolicy build() {
            return new SyncSubPolicy(this.zza, this.zzb);
        }
    }
}
