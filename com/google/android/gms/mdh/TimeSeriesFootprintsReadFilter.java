package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class TimeSeriesFootprintsReadFilter extends zzbid
{
    public static final Parcelable.Creator<TimeSeriesFootprintsReadFilter> CREATOR;
    private final int zza;
    private final Long zzb;
    private final Long zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzl();
    }
    
    public TimeSeriesFootprintsReadFilter(final int zza, final Long zzb, final Long zzc) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzb = zzb;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final TimeSeriesFootprintsReadFilter timeSeriesFootprintsReadFilter = (TimeSeriesFootprintsReadFilter)o;
            return zzak.zza(this.zza, timeSeriesFootprintsReadFilter.zza) && zzak.zza(this.zzb, timeSeriesFootprintsReadFilter.zzb) && zzak.zza(this.zzc, timeSeriesFootprintsReadFilter.zzc);
        }
        return false;
    }
    
    public Long getAbsoluteMaxTimestampMicros() {
        return this.zzc;
    }
    
    public Long getAbsoluteMinTimestampMicros() {
        return this.zzb;
    }
    
    public int getNumFootprints() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc });
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getNumFootprints());
        zzbig.zza(parcel, 2, this.getAbsoluteMinTimestampMicros(), false);
        zzbig.zza(parcel, 3, this.getAbsoluteMaxTimestampMicros(), false);
        zzbig.zza(parcel, zza);
    }
}
