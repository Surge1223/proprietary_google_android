package com.google.android.gms.mdh;

import java.util.ArrayList;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import java.util.List;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class LatestFootprintFilter extends zzbid
{
    public static final LatestFootprintFilter ALL;
    public static final Parcelable.Creator<LatestFootprintFilter> CREATOR;
    public static final LatestFootprintFilter NONE;
    private static final byte[] zza;
    private final List<SecondaryIdMatcher> zzb;
    
    static {
        zza = new byte[0];
        ALL = new Builder().addSecondaryIdMatching(LatestFootprintFilter.zza, 2).build();
        NONE = new Builder().build();
        CREATOR = (Parcelable.Creator)new zzb();
    }
    
    LatestFootprintFilter(final List<SecondaryIdMatcher> zzb) {
        this.zzb = zzb;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && zzak.zza(this.zzb, ((LatestFootprintFilter)o).zzb));
    }
    
    public List<SecondaryIdMatcher> getSecondaryIdMatchers() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zzb });
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zzc(parcel, 1, this.getSecondaryIdMatchers(), false);
        zzbig.zza(parcel, zza);
    }
    
    public static class Builder
    {
        private final List<SecondaryIdMatcher> zza;
        
        protected Builder() {
            this.zza = new ArrayList<SecondaryIdMatcher>();
        }
        
        public Builder addSecondaryIdMatching(final byte[] array, final int n) {
            this.zza.add(new SecondaryIdMatcher(array.clone(), n));
            return this;
        }
        
        public LatestFootprintFilter build() {
            return new LatestFootprintFilter(this.zza);
        }
    }
}
