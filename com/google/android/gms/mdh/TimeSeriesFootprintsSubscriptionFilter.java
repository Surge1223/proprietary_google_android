package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class TimeSeriesFootprintsSubscriptionFilter extends zzbid
{
    public static final Parcelable.Creator<TimeSeriesFootprintsSubscriptionFilter> CREATOR;
    public static final TimeSeriesFootprintsSubscriptionFilter NONE;
    private final int zza;
    private final Long zzb;
    private final Long zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzm();
        NONE = new TimeSeriesFootprintsSubscriptionFilter(0, null, null);
    }
    
    public TimeSeriesFootprintsSubscriptionFilter(final int zza, final Long zzb, final Long zzc) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzb = zzb;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final TimeSeriesFootprintsSubscriptionFilter timeSeriesFootprintsSubscriptionFilter = (TimeSeriesFootprintsSubscriptionFilter)o;
            return zzak.zza(this.zza, timeSeriesFootprintsSubscriptionFilter.zza) && zzak.zza(this.zzb, timeSeriesFootprintsSubscriptionFilter.zzb) && zzak.zza(this.zzc, timeSeriesFootprintsSubscriptionFilter.zzc);
        }
        return false;
    }
    
    public int getNumFootprints() {
        return this.zza;
    }
    
    public Long getRelativeNowMaxTimestampMicros() {
        return this.zzc;
    }
    
    public Long getRelativeNowMinTimestampMicros() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc });
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getNumFootprints());
        zzbig.zza(parcel, 2, this.getRelativeNowMinTimestampMicros(), false);
        zzbig.zza(parcel, 3, this.getRelativeNowMaxTimestampMicros(), false);
        zzbig.zza(parcel, zza);
    }
}
