package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzau;
import java.nio.charset.Charset;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SecondaryIdMatcher extends zzbid
{
    public static final Parcelable.Creator<SecondaryIdMatcher> CREATOR;
    private static final Charset zza;
    private final byte[] zzb;
    private final int zzc;
    
    static {
        zza = Charset.forName("UTF-8");
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    SecondaryIdMatcher(final byte[] zzb, final int zzc) {
        zzau.zza(zzb);
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final SecondaryIdMatcher secondaryIdMatcher = (SecondaryIdMatcher)o;
            return this.zzc == secondaryIdMatcher.zzc && Arrays.equals(this.zzb, secondaryIdMatcher.zzb);
        }
        return false;
    }
    
    public int getMatchingType() {
        return this.zzc;
    }
    
    public byte[] getSecondaryId() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.zzb) * 31 + this.zzc;
    }
    
    @Override
    public String toString() {
        String s = null;
        switch (this.zzc) {
            default: {
                s = "__unknown__";
                break;
            }
            case 2: {
                s = "MATCHING_TYPE_PREFIX";
                break;
            }
            case 1: {
                s = "MATCHING_TYPE_EXACT";
                break;
            }
        }
        final String s2 = new String(this.zzb, SecondaryIdMatcher.zza);
        final StringBuilder sb = new StringBuilder(2 + String.valueOf(s).length() + String.valueOf(s2).length());
        sb.append(s);
        sb.append(": ");
        sb.append(s2);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getSecondaryId(), false);
        zzbig.zza(parcel, 2, this.getMatchingType());
        zzbig.zza(parcel, zza);
    }
}
