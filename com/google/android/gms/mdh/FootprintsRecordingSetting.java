package com.google.android.gms.mdh;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FootprintsRecordingSetting extends zzbid
{
    public static final Parcelable.Creator<FootprintsRecordingSetting> CREATOR;
    private final int zza;
    private final boolean zzb;
    private final boolean zzc;
    private final long zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zza();
    }
    
    public FootprintsRecordingSetting(final int zza, final boolean zzb, final boolean zzc, final long zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof FootprintsRecordingSetting) {
            final FootprintsRecordingSetting footprintsRecordingSetting = (FootprintsRecordingSetting)o;
            return this.zza == footprintsRecordingSetting.zza && this.zzb == footprintsRecordingSetting.zzb && this.zzc == footprintsRecordingSetting.zzc && this.zzd == footprintsRecordingSetting.zzd;
        }
        return false;
    }
    
    public int getCorpusGroup() {
        return this.zza;
    }
    
    public long getLastModifiedTimeMicros() {
        return this.zzd;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc, this.zzd });
    }
    
    public boolean isEnabled() {
        return this.zzb;
    }
    
    public boolean isUnset() {
        return this.zzc;
    }
    
    @Override
    public String toString() {
        final int zza = this.zza;
        final boolean zzb = this.zzb;
        final boolean zzc = this.zzc;
        final long zzd = this.zzd;
        final StringBuilder sb = new StringBuilder(124);
        sb.append("FootprintsRecordingSetting{corpusGroup=");
        sb.append(zza);
        sb.append(", enabled=");
        sb.append(zzb);
        sb.append(", unset=");
        sb.append(zzc);
        sb.append(", lastModifiedTimeMicros=");
        sb.append(zzd);
        sb.append('}');
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getCorpusGroup());
        zzbig.zza(parcel, 2, this.isEnabled());
        zzbig.zza(parcel, 3, this.isUnset());
        zzbig.zza(parcel, 4, this.getLastModifiedTimeMicros());
        zzbig.zza(parcel, zza);
    }
}
