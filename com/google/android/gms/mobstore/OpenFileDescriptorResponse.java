package com.google.android.gms.mobstore;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class OpenFileDescriptorResponse extends zzbid
{
    public static final Parcelable.Creator<OpenFileDescriptorResponse> CREATOR;
    public final ParcelFileDescriptor descriptor;
    
    static {
        CREATOR = (Parcelable.Creator)new zzk();
    }
    
    public OpenFileDescriptorResponse(final ParcelFileDescriptor descriptor) {
        this.descriptor = descriptor;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.descriptor, n, false);
        zzbig.zza(parcel, zza);
    }
}
