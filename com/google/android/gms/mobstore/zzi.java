package com.google.android.gms.mobstore;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.net.Uri;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzi extends zzbid
{
    public static final Parcelable.Creator<zzi> CREATOR;
    private final Uri zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzj();
    }
    
    public zzi(final Uri zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, zza);
    }
}
