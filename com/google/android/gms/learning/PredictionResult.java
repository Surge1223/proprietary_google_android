package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class PredictionResult extends zzbid
{
    public static final Parcelable.Creator<PredictionResult> CREATOR;
    private Features zza;
    private int zzb;
    private DiagnosisInfo zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzg();
    }
    
    public PredictionResult(final Features zza, final int zzb, final DiagnosisInfo zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, zza);
    }
}
