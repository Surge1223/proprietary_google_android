package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class Solution extends BaseOptions
{
    public static final Parcelable.Creator<Solution> CREATOR;
    private ExampleCollectionOptions zza;
    private TrainerOptions zzb;
    private PredictorOptions zzc;
    private ContextOptions zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new com.google.android.gms.learning.zzi();
    }
    
    Solution(final ExampleCollectionOptions zza, final TrainerOptions zzb, final PredictorOptions zzc, final ContextOptions zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, 4, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, zza);
    }
}
