package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzau;
import android.os.Parcelable.Creator;

public final class ExampleCollectionOptions extends BaseOptions
{
    public static final Parcelable.Creator<ExampleCollectionOptions> CREATOR;
    private String zza;
    private long zzb;
    private long zzc;
    private ContextOptions zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new com.google.android.gms.learning.zzd();
    }
    
    ExampleCollectionOptions(final String zza, final long zzb, final long zzc, final ContextOptions zzd) {
        zzau.zza(zza);
        final boolean b = false;
        zzau.zzb(zzb > 0L);
        boolean b2 = b;
        if (zzc > 0L) {
            b2 = true;
        }
        zzau.zzb(b2);
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final ExampleCollectionOptions exampleCollectionOptions = (ExampleCollectionOptions)o;
        if (this.zzb != exampleCollectionOptions.zzb) {
            return false;
        }
        if (this.zzc != exampleCollectionOptions.zzc) {
            return false;
        }
        if (!this.zza.equals(exampleCollectionOptions.zza)) {
            return false;
        }
        if (this.zzd != null) {
            return this.zzd.equals(exampleCollectionOptions.zzd);
        }
        return exampleCollectionOptions.zzd == null;
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.zza.hashCode();
        final int n = (int)(this.zzb ^ this.zzb >>> 32);
        final int n2 = (int)(this.zzc ^ this.zzc >>> 32);
        int hashCode2;
        if (this.zzd != null) {
            hashCode2 = this.zzd.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return ((hashCode * 31 + n) * 31 + n2) * 31 + hashCode2;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, zza);
    }
}
