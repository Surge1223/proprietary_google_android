package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.accounts.Account;
import java.util.List;
import android.os.Parcelable.Creator;

public final class ContextOptions extends BaseOptions
{
    public static final Parcelable.Creator<ContextOptions> CREATOR;
    private List<Integer> zza;
    private Account zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new com.google.android.gms.learning.zza();
    }
    
    ContextOptions(final List<Integer> zza, final Account zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, zza);
    }
}
