package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzau;
import android.net.Uri;
import android.os.Parcelable.Creator;

public final class PredictorOptions extends BaseOptions
{
    public static final Parcelable.Creator<PredictorOptions> CREATOR;
    private String zza;
    private Uri zzb;
    private Uri zzc;
    private ContextOptions zzd;
    private Uri zze;
    private Uri zzf;
    private boolean zzg;
    
    static {
        CREATOR = (Parcelable.Creator)new com.google.android.gms.learning.zzh();
    }
    
    PredictorOptions(final String zza, final Uri zzb, final Uri zzc, final ContextOptions zzd, final Uri zze, final Uri zzf, final boolean zzg) {
        zzau.zza(zza);
        zzau.zza(zzb);
        zzau.zza(zzc);
        boolean b = false;
        Label_0038: {
            if (zze != null) {
                if (zzf == null) {
                    break Label_0038;
                }
            }
            else if (zzf != null) {
                break Label_0038;
            }
            b = true;
        }
        zzau.zzb(b);
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zze = zze;
        this.zzf = zzf;
        this.zzd = zzd;
        this.zzg = zzg;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final PredictorOptions predictorOptions = (PredictorOptions)o;
        if (!this.zza.equals(predictorOptions.zza)) {
            return false;
        }
        if (!this.zzb.equals((Object)predictorOptions.zzb)) {
            return false;
        }
        if (!this.zzc.equals((Object)predictorOptions.zzc)) {
            return false;
        }
        Label_0111: {
            if (this.zze != null) {
                if (this.zze.equals((Object)predictorOptions.zze)) {
                    break Label_0111;
                }
            }
            else if (predictorOptions.zze == null) {
                break Label_0111;
            }
            return false;
        }
        Label_0144: {
            if (this.zzf != null) {
                if (this.zzf.equals((Object)predictorOptions.zzf)) {
                    break Label_0144;
                }
            }
            else if (predictorOptions.zzf == null) {
                break Label_0144;
            }
            return false;
        }
        if (this.zzg != predictorOptions.zzg) {
            return false;
        }
        if (this.zzd != null) {
            return this.zzd.equals(predictorOptions.zzd);
        }
        return predictorOptions.zzd == null;
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.zza.hashCode();
        final int hashCode2 = this.zzb.hashCode();
        final int hashCode3 = this.zzc.hashCode();
        final Uri zze = this.zze;
        int hashCode4 = 0;
        int hashCode5;
        if (zze != null) {
            hashCode5 = this.zze.hashCode();
        }
        else {
            hashCode5 = 0;
        }
        int hashCode6;
        if (this.zzf != null) {
            hashCode6 = this.zzf.hashCode();
        }
        else {
            hashCode6 = 0;
        }
        if (this.zzd != null) {
            hashCode4 = this.zzd.hashCode();
        }
        return (((((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode4) * 31 + (this.zzg ? 1 : 0);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, 4, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, 5, (Parcelable)this.zze, n, false);
        zzbig.zza(parcel, 6, (Parcelable)this.zzf, n, false);
        zzbig.zza(parcel, 7, this.zzg);
        zzbig.zza(parcel, zza);
    }
}
