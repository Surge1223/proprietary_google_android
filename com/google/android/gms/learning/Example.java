package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.nio.charset.Charset;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Example extends zzbid
{
    public static final Parcelable.Creator<Example> CREATOR;
    private static final Charset zza;
    private Features zzb;
    
    static {
        zza = Charset.forName("UTF-8");
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    Example(final Features zzb) {
        this.zzb = zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, zza);
    }
}
