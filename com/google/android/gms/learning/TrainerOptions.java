package com.google.android.gms.learning;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzau;
import android.net.Uri;
import android.os.Parcelable.Creator;

public final class TrainerOptions extends BaseOptions
{
    public static final Parcelable.Creator<TrainerOptions> CREATOR;
    private String zza;
    private String zzb;
    private Uri zzc;
    private Uri zzd;
    private Uri zze;
    private String zzf;
    
    static {
        CREATOR = (Parcelable.Creator)new zzj();
    }
    
    TrainerOptions(final String zza, final String zzb, final Uri zzc, final Uri zzd, final Uri zze, final String zzf) {
        zzau.zza(zza);
        zzau.zza(zzb);
        if (zzc != null && zzf == null) {
            zzau.zza(zzd);
            zzau.zza(zze);
        }
        else if (zzc == null && zzf != null) {
            zzau.zza(zzf);
        }
        else {
            if (zzc == null) {
                throw new IllegalArgumentException("no federation or personalization is set up.");
            }
            throw new IllegalArgumentException("cannot set options both for federation and personalization");
        }
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final TrainerOptions trainerOptions = (TrainerOptions)o;
        if (!this.zza.equals(trainerOptions.zza)) {
            return false;
        }
        if (!this.zzb.equals(trainerOptions.zzb)) {
            return false;
        }
        Label_0095: {
            if (this.zzc != null) {
                if (this.zzc.equals((Object)trainerOptions.zzc)) {
                    break Label_0095;
                }
            }
            else if (trainerOptions.zzc == null) {
                break Label_0095;
            }
            return false;
        }
        Label_0128: {
            if (this.zzd != null) {
                if (this.zzd.equals((Object)trainerOptions.zzd)) {
                    break Label_0128;
                }
            }
            else if (trainerOptions.zzd == null) {
                break Label_0128;
            }
            return false;
        }
        Label_0161: {
            if (this.zze != null) {
                if (this.zze.equals((Object)trainerOptions.zze)) {
                    break Label_0161;
                }
            }
            else if (trainerOptions.zze == null) {
                break Label_0161;
            }
            return false;
        }
        if (this.zzf != null) {
            return this.zzf.equals(trainerOptions.zzf);
        }
        return trainerOptions.zzf == null;
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.zza.hashCode();
        final int hashCode2 = this.zzb.hashCode();
        final Uri zzc = this.zzc;
        int hashCode3 = 0;
        int hashCode4;
        if (zzc != null) {
            hashCode4 = this.zzc.hashCode();
        }
        else {
            hashCode4 = 0;
        }
        int hashCode5;
        if (this.zzd != null) {
            hashCode5 = this.zzd.hashCode();
        }
        else {
            hashCode5 = 0;
        }
        int hashCode6;
        if (this.zze != null) {
            hashCode6 = this.zze.hashCode();
        }
        else {
            hashCode6 = 0;
        }
        if (this.zzf != null) {
            hashCode3 = this.zzf.hashCode();
        }
        return ((((hashCode * 31 + hashCode2) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode3;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, 4, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, 5, (Parcelable)this.zze, n, false);
        zzbig.zza(parcel, 6, this.zzf, false);
        zzbig.zza(parcel, zza);
    }
}
