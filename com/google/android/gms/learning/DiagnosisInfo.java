package com.google.android.gms.learning;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class DiagnosisInfo extends zzbid
{
    public static final Parcelable.Creator<DiagnosisInfo> CREATOR;
    private final int zza;
    private final int zzb;
    private final int zzc;
    private final int zzd;
    private final long zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzb();
    }
    
    public DiagnosisInfo(final int zza, final int zzb, final int zzc, final int zzd, final long zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, this.zzd);
        zzbig.zza(parcel, 5, this.zze);
        zzbig.zza(parcel, zza);
    }
}
