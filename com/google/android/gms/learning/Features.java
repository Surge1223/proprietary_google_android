package com.google.android.gms.learning;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Set;
import java.nio.ByteBuffer;
import android.os.Bundle;
import java.nio.charset.Charset;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Features extends zzbid
{
    public static final Parcelable.Creator<Features> CREATOR;
    private static final Charset zza;
    private Bundle zzb;
    private Bundle zzc;
    
    static {
        zza = Charset.forName("UTF-8");
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    public Features() {
        this(new Bundle(), new Bundle());
    }
    
    Features(final Bundle zzb, final Bundle zzc) {
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public byte[][] getBytes(final String s) {
        final ByteBuffer[] bytesAsByteBuffer = this.getBytesAsByteBuffer(s);
        final byte[][] array = new byte[bytesAsByteBuffer.length][];
        for (int i = 0; i < bytesAsByteBuffer.length; ++i) {
            array[i] = new byte[bytesAsByteBuffer[i].remaining()];
            bytesAsByteBuffer[i].get(array[i]);
        }
        return array;
    }
    
    public ByteBuffer[] getBytesAsByteBuffer(final String s) {
        final byte[] byteArray = this.zzb.getByteArray(s);
        final int[] intArray = this.zzc.getIntArray(s);
        final ByteBuffer[] array = new ByteBuffer[intArray.length];
        int i = 0;
        int n = 0;
        while (i < array.length) {
            array[i] = ByteBuffer.wrap(byteArray, n, intArray[i] - n).asReadOnlyBuffer();
            n = intArray[i];
            ++i;
        }
        return array;
    }
    
    public Set<String> getFeatureNames() {
        return (Set<String>)this.zzb.keySet();
    }
    
    public int getFeatureType(String s) {
        final Object value = this.zzb.get(s);
        if (value == null) {
            return 0;
        }
        if (value instanceof float[]) {
            return 1;
        }
        if (value instanceof long[]) {
            return 2;
        }
        if (value instanceof byte[]) {
            return 3;
        }
        s = String.valueOf(value.getClass().getSimpleName());
        if (s.length() != 0) {
            s = "inconsistent example with feature of type: ".concat(s);
        }
        else {
            s = new String("inconsistent example with feature of type: ");
        }
        throw new IllegalStateException(s);
    }
    
    public float[] getFloats(final String s) {
        return this.zzb.getFloatArray(s);
    }
    
    public long[] getLongs(final String s) {
        return this.zzb.getLongArray(s);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Features{");
        final Iterator<String> iterator = this.getFeatureNames().iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(",");
            }
            sb.append(s);
            sb.append("=");
            switch (this.getFeatureType(s)) {
                default: {
                    continue;
                }
                case 3: {
                    sb.append("[");
                    final byte[][] bytes = this.getBytes(s);
                    final int length = bytes.length;
                    int n2 = 1;
                    for (int i = 0; i < length; ++i, n2 = 0) {
                        final byte[] array = bytes[i];
                        if (n2 == 0) {
                            sb.append(",");
                        }
                        sb.append(new String(array, Features.zza));
                    }
                    sb.append("]");
                    continue;
                }
                case 2: {
                    sb.append(Arrays.toString(this.getLongs(s)));
                    continue;
                }
                case 1: {
                    sb.append(Arrays.toString(this.getFloats(s)));
                    continue;
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzb, false);
        zzbig.zza(parcel, 2, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
}
