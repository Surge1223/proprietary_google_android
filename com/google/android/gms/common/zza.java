package com.google.android.gms.common;

import java.util.concurrent.TimeoutException;
import com.google.android.gms.common.internal.zzau;
import java.util.concurrent.TimeUnit;
import android.content.ComponentName;
import java.util.concurrent.LinkedBlockingQueue;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import android.content.ServiceConnection;

public final class zza implements ServiceConnection
{
    private boolean zza;
    private final BlockingQueue<IBinder> zzb;
    
    public zza() {
        this.zza = false;
        this.zzb = new LinkedBlockingQueue<IBinder>();
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        this.zzb.add(binder);
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
    }
    
    public final IBinder zza(final long n, final TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        zzau.zzc("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (this.zza) {
            throw new IllegalStateException("Cannot call get on this connection more than once");
        }
        this.zza = true;
        final IBinder binder = this.zzb.poll(10000L, timeUnit);
        if (binder != null) {
            return binder;
        }
        throw new TimeoutException("Timed out waiting for the service connection");
    }
}
