package com.google.android.gms.common;

import android.content.pm.Signature;
import android.util.Log;
import android.content.pm.PackageInfo;
import com.google.android.gms.common.internal.zzau;
import android.content.Context;

public class GoogleSignatureVerifier
{
    private static GoogleSignatureVerifier zza;
    private final Context zzb;
    
    private GoogleSignatureVerifier(final Context context) {
        this.zzb = context.getApplicationContext();
    }
    
    public static GoogleSignatureVerifier getInstance(final Context context) {
        zzau.zza(context);
        synchronized (GoogleSignatureVerifier.class) {
            if (GoogleSignatureVerifier.zza == null) {
                zze.zza(context);
                GoogleSignatureVerifier.zza = new GoogleSignatureVerifier(context);
            }
            return GoogleSignatureVerifier.zza;
        }
    }
    
    private static zzf zza(final PackageInfo packageInfo, final zzf... array) {
        if (packageInfo.signatures == null) {
            return null;
        }
        if (packageInfo.signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        final Signature[] signatures = packageInfo.signatures;
        int i = 0;
        final zzg zzg = new zzg(signatures[0].toByteArray());
        while (i < array.length) {
            if (array[i].equals(zzg)) {
                return array[i];
            }
            ++i;
        }
        return null;
    }
    
    public static boolean zza(final PackageInfo packageInfo, final boolean b) {
        if (packageInfo != null && packageInfo.signatures != null) {
            zzf zzf;
            if (b) {
                zzf = zza(packageInfo, zzi.zza);
            }
            else {
                zzf = zza(packageInfo, zzi.zza[0]);
            }
            if (zzf != null) {
                return true;
            }
        }
        return false;
    }
}
