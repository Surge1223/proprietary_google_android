package com.google.android.gms.common;

import android.util.Log;
import android.content.Context;

final class zze
{
    private static final Object zzb;
    private static Context zzc;
    
    static {
        zzb = new Object();
    }
    
    static void zza(final Context context) {
        synchronized (zze.class) {
            if (zze.zzc == null) {
                if (context != null) {
                    zze.zzc = context.getApplicationContext();
                }
            }
            else {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            }
        }
    }
}
