package com.google.android.gms.common.api;

public class ResolvableApiException extends ApiException
{
    public ResolvableApiException(final Status status) {
        super(status);
    }
}
