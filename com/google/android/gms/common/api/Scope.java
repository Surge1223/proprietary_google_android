package com.google.android.gms.common.api;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzau;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public final class Scope extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<Scope> CREATOR;
    private final int zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    Scope(final int zza, final String zzb) {
        zzau.zza(zzb, (Object)"scopeUri must not be null or empty");
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public Scope(final String s) {
        this(1, s);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return this == o || (o instanceof Scope && this.zzb.equals(((Scope)o).zzb));
    }
    
    @Override
    public final int hashCode() {
        return this.zzb.hashCode();
    }
    
    @Override
    public final String toString() {
        return this.zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, zza);
    }
    
    public final String zza() {
        return this.zzb;
    }
}
