package com.google.android.gms.common.api;

import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.api.internal.zzdq;
import com.google.android.gms.common.api.internal.zzn;
import java.util.Map;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.Set;

public abstract class GoogleApiClient
{
    private static final Set<GoogleApiClient> zza;
    
    static {
        zza = Collections.newSetFromMap(new WeakHashMap<GoogleApiClient, Boolean>());
    }
    
    public <A extends Api.zzb, R extends Result, T extends zzn<R, A>> T zza(final T t) {
        throw new UnsupportedOperationException();
    }
    
    public void zza(final zzdq zzdq) {
        throw new UnsupportedOperationException();
    }
    
    public void zzb(final zzdq zzdq) {
        throw new UnsupportedOperationException();
    }
    
    public Looper zzc() {
        throw new UnsupportedOperationException();
    }
    
    public interface ConnectionCallbacks
    {
        void onConnected(final Bundle p0);
        
        void onConnectionSuspended(final int p0);
    }
    
    public interface OnConnectionFailedListener
    {
        void onConnectionFailed(final ConnectionResult p0);
    }
}
