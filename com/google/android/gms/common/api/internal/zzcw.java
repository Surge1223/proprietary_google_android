package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApi;

public final class zzcw
{
    public final zzb zza;
    public final int zzb;
    public final GoogleApi<?> zzc;
    
    public zzcw(final zzb zza, final int zzb, final GoogleApi<?> zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}
