package com.google.android.gms.common.api.internal;

import java.util.Collection;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import java.util.Iterator;
import com.google.android.gms.common.ConnectionResult;
import android.app.Application;
import android.util.Log;
import android.os.Message;
import com.google.android.gms.common.api.GoogleApi;
import android.os.HandlerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.support.v4.util.ArraySet;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Looper;
import android.os.Handler;
import java.util.Set;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import com.google.android.gms.common.api.Status;
import android.os.Handler$Callback;

public final class zzbn implements Handler$Callback
{
    public static final Status zza;
    private static final Status zzb;
    private static final Object zzf;
    private static zzbn zzg;
    private long zzc;
    private long zzd;
    private long zze;
    private final Context zzh;
    private final GoogleApiAvailability zzi;
    private final zzv zzj;
    private final AtomicInteger zzk;
    private final AtomicInteger zzl;
    private final Map<zzi<?>, zzbp<?>> zzm;
    private zzai zzn;
    private final Set<zzi<?>> zzo;
    private final Set<zzi<?>> zzp;
    private final Handler zzq;
    
    static {
        zza = new Status(4, "Sign-out occurred while this API call was in progress.");
        zzb = new Status(4, "The user must be signed in to make this API call.");
        zzf = new Object();
    }
    
    private zzbn(final Context zzh, final Looper looper, final GoogleApiAvailability zzi) {
        this.zzc = 5000L;
        this.zzd = 120000L;
        this.zze = 10000L;
        this.zzk = new AtomicInteger(1);
        this.zzl = new AtomicInteger(0);
        this.zzm = new ConcurrentHashMap<zzi<?>, zzbp<?>>(5, 0.75f, 1);
        this.zzn = null;
        this.zzo = new ArraySet<zzi<?>>();
        this.zzp = new ArraySet<zzi<?>>();
        this.zzh = zzh;
        this.zzq = new Handler(looper, (Handler$Callback)this);
        this.zzi = zzi;
        this.zzj = new zzv(zzi);
        this.zzq.sendMessage(this.zzq.obtainMessage(6));
    }
    
    public static zzbn zza(final Context context) {
        synchronized (zzbn.zzf) {
            if (zzbn.zzg == null) {
                final HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                zzbn.zzg = new zzbn(context.getApplicationContext(), handlerThread.getLooper(), GoogleApiAvailability.getInstance());
            }
            return zzbn.zzg;
        }
    }
    
    private final void zzb(final GoogleApi<?> googleApi) {
        final zzi<O> zzd = googleApi.zzd();
        zzbp<?> zzbp;
        if ((zzbp = this.zzm.get(zzd)) == null) {
            zzbp = new zzbp<Object>(googleApi);
            this.zzm.put(zzd, zzbp);
        }
        if (zzbp.zzk()) {
            this.zzp.add(zzd);
        }
        zzbp.zzi();
    }
    
    public final boolean handleMessage(final Message message) {
        final int what = message.what;
        long zze = 300000L;
        Label_1010: {
            switch (what) {
                default: {
                    final int what2 = message.what;
                    final StringBuilder sb = new StringBuilder(31);
                    sb.append("Unknown message id: ");
                    sb.append(what2);
                    Log.w("GoogleApiManager", sb.toString());
                    return false;
                }
                case 12: {
                    if (this.zzm.containsKey(message.obj)) {
                        this.zzm.get(message.obj).zzh();
                        break;
                    }
                    break;
                }
                case 11: {
                    if (this.zzm.containsKey(message.obj)) {
                        this.zzm.get(message.obj).zzg();
                        break;
                    }
                    break;
                }
                case 10: {
                    final Iterator<zzi<?>> iterator = this.zzp.iterator();
                    while (iterator.hasNext()) {
                        this.zzm.remove(iterator.next()).zza();
                    }
                    this.zzp.clear();
                    break;
                }
                case 9: {
                    if (this.zzm.containsKey(message.obj)) {
                        this.zzm.get(message.obj).zzf();
                        break;
                    }
                    break;
                }
                case 7: {
                    this.zzb((GoogleApi<?>)message.obj);
                    break;
                }
                case 6: {
                    if (this.zzh.getApplicationContext() instanceof Application) {
                        com.google.android.gms.common.api.internal.zzl.zza((Application)this.zzh.getApplicationContext());
                        com.google.android.gms.common.api.internal.zzl.zza().zza(new zzbo(this));
                        if (!com.google.android.gms.common.api.internal.zzl.zza().zza(true)) {
                            this.zze = 300000L;
                        }
                    }
                    break;
                }
                case 5: {
                    final int arg1 = message.arg1;
                    final ConnectionResult connectionResult = (ConnectionResult)message.obj;
                    while (true) {
                        for (final zzbp<?> zzbp : this.zzm.values()) {
                            if (zzbp.zzl() == arg1) {
                                if (zzbp != null) {
                                    final String errorString = this.zzi.getErrorString(connectionResult.getErrorCode());
                                    final String errorMessage = connectionResult.getErrorMessage();
                                    final StringBuilder sb2 = new StringBuilder(69 + String.valueOf(errorString).length() + String.valueOf(errorMessage).length());
                                    sb2.append("Error resolution was canceled by the user, original error message: ");
                                    sb2.append(errorString);
                                    sb2.append(": ");
                                    sb2.append(errorMessage);
                                    zzbp.zza(new Status(17, sb2.toString()));
                                    break Label_1010;
                                }
                                final StringBuilder sb3 = new StringBuilder(76);
                                sb3.append("Could not find API instance ");
                                sb3.append(arg1);
                                sb3.append(" while trying to fail enqueued calls.");
                                Log.wtf("GoogleApiManager", sb3.toString(), (Throwable)new Exception());
                                break Label_1010;
                            }
                        }
                        zzbp<?> zzbp = null;
                        continue;
                    }
                }
                case 4:
                case 8:
                case 13: {
                    final zzcw zzcw = (zzcw)message.obj;
                    zzbp<?> zzbp2;
                    if ((zzbp2 = this.zzm.get(zzcw.zzc.zzd())) == null) {
                        this.zzb(zzcw.zzc);
                        zzbp2 = this.zzm.get(zzcw.zzc.zzd());
                    }
                    if (zzbp2.zzk() && this.zzl.get() != zzcw.zzb) {
                        zzcw.zza.zza(zzbn.zza);
                        zzbp2.zza();
                        break;
                    }
                    zzbp2.zza(zzcw.zza);
                    break;
                }
                case 3: {
                    for (final zzbp<?> zzbp3 : this.zzm.values()) {
                        zzbp3.zzd();
                        zzbp3.zzi();
                    }
                    break;
                }
                case 2: {
                    final zzk zzk = (zzk)message.obj;
                    for (final zzi<?> zzi : zzk.zza()) {
                        final zzbp<?> zzbp4 = this.zzm.get(zzi);
                        if (zzbp4 == null) {
                            zzk.zza(zzi, new ConnectionResult(13), null);
                            break;
                        }
                        if (zzbp4.zzj()) {
                            zzk.zza(zzi, ConnectionResult.zza, zzbp4.zzb().zzab());
                        }
                        else if (zzbp4.zze() != null) {
                            zzk.zza(zzi, zzbp4.zze(), null);
                        }
                        else {
                            zzbp4.zza(zzk);
                        }
                    }
                    break;
                }
                case 1: {
                    if (message.obj) {
                        zze = 10000L;
                    }
                    this.zze = zze;
                    this.zzq.removeMessages(12);
                    final Iterator<zzi<?>> iterator5 = this.zzm.keySet().iterator();
                    while (iterator5.hasNext()) {
                        this.zzq.sendMessageDelayed(this.zzq.obtainMessage(12, (Object)iterator5.next()), this.zze);
                    }
                    break;
                }
            }
        }
        return true;
    }
    
    public final void zza(final GoogleApi<?> googleApi) {
        this.zzq.sendMessage(this.zzq.obtainMessage(7, (Object)googleApi));
    }
    
    public final <O extends Api.ApiOptions, TResult> void zza(final GoogleApi<O> googleApi, final int n, final zzdn<Api.zzb, TResult> zzdn, final TaskCompletionSource<TResult> taskCompletionSource, final zzdi zzdi) {
        this.zzq.sendMessage(this.zzq.obtainMessage(4, (Object)new zzcw(new zzf<Object>(n, zzdn, taskCompletionSource, zzdi), this.zzl.get(), googleApi)));
    }
    
    public final <O extends Api.ApiOptions> void zza(final GoogleApi<O> googleApi, final int n, final zzn<? extends Result, Api.zzb> zzn) {
        this.zzq.sendMessage(this.zzq.obtainMessage(4, (Object)new zzcw(new zzd<Object>(n, zzn), this.zzl.get(), googleApi)));
    }
    
    public final void zza(final zzai zzn) {
        synchronized (zzbn.zzf) {
            if (this.zzn != zzn) {
                this.zzn = zzn;
                this.zzo.clear();
                this.zzo.addAll(zzn.zzf());
            }
        }
    }
    
    final boolean zza(final ConnectionResult connectionResult, final int n) {
        return this.zzi.zza(this.zzh, connectionResult, n);
    }
    
    public final void zzb(final ConnectionResult connectionResult, final int n) {
        if (!this.zza(connectionResult, n)) {
            this.zzq.sendMessage(this.zzq.obtainMessage(5, n, 0, (Object)connectionResult));
        }
    }
    
    final void zzb(final zzai zzai) {
        synchronized (zzbn.zzf) {
            if (this.zzn == zzai) {
                this.zzn = null;
                this.zzo.clear();
            }
        }
    }
    
    public final int zzc() {
        return this.zzk.getAndIncrement();
    }
    
    public final void zzd() {
        this.zzq.sendMessage(this.zzq.obtainMessage(3));
    }
}
