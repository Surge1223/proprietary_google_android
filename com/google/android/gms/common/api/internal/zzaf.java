package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ApiException;
import java.util.HashMap;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.WeakHashMap;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Map;

public final class zzaf
{
    private final Map<BasePendingResult<?>, Boolean> zza;
    private final Map<TaskCompletionSource<?>, Boolean> zzb;
    
    public zzaf() {
        this.zza = Collections.synchronizedMap(new WeakHashMap<BasePendingResult<?>, Boolean>());
        this.zzb = Collections.synchronizedMap(new WeakHashMap<TaskCompletionSource<?>, Boolean>());
    }
    
    private final void zza(final boolean b, final Status status) {
        Object o = this.zza;
        synchronized (o) {
            final HashMap<Object, Boolean> hashMap = new HashMap<Object, Boolean>(this.zza);
            // monitorexit(o)
            Object o2 = this.zzb;
            synchronized (o2) {
                o = new HashMap<Object, Object>(this.zzb);
                // monitorexit(o2)
                o2 = hashMap.entrySet().iterator();
                while (((Iterator)o2).hasNext()) {
                    final Map.Entry<TaskCompletionSource, Boolean> entry = ((Iterator<Map.Entry<TaskCompletionSource, Boolean>>)o2).next();
                    if (b || entry.getValue()) {
                        ((BasePendingResult)entry.getKey()).zzd(status);
                    }
                }
                o = ((Map<Object, Object>)o).entrySet().iterator();
                while (((Iterator)o).hasNext()) {
                    o2 = ((Iterator<Map.Entry<K, Boolean>>)o).next();
                    if (b || ((Map.Entry<TaskCompletionSource, Boolean>)o2).getValue()) {
                        ((Map.Entry<TaskCompletionSource, Boolean>)o2).getKey().trySetException(new ApiException(status));
                    }
                }
            }
        }
    }
    
    final void zza(final BasePendingResult<? extends Result> basePendingResult, final boolean b) {
        this.zza.put(basePendingResult, b);
        basePendingResult.zza((PendingResult.zza)new zzag(this, basePendingResult));
    }
    
    final <TResult> void zza(final TaskCompletionSource<TResult> taskCompletionSource, final boolean b) {
        this.zzb.put(taskCompletionSource, b);
        taskCompletionSource.getTask().addOnCompleteListener((OnCompleteListener<TResult>)new zzah(this, taskCompletionSource));
    }
    
    final boolean zza() {
        return !this.zza.isEmpty() || !this.zzb.isEmpty();
    }
    
    public final void zzb() {
        this.zza(false, zzbn.zza);
    }
    
    public final void zzc() {
        this.zza(true, zzdt.zza);
    }
}
