package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzau;
import android.app.Activity;
import android.support.v4.util.ArraySet;

public class zzai extends zzp
{
    private final ArraySet<zzi<?>> zze;
    private zzbn zzf;
    
    private zzai(final zzch zzch) {
        super(zzch);
        this.zze = new ArraySet<zzi<?>>();
        this.zzd.zza("ConnectionlessLifecycleHelper", this);
    }
    
    public static void zza(final Activity activity, final zzbn zzf, final zzi<?> zzi) {
        final zzch zzb = LifecycleCallback.zzb(activity);
        zzai zzai;
        if ((zzai = zzb.zza("ConnectionlessLifecycleHelper", zzai.class)) == null) {
            zzai = new zzai(zzb);
        }
        zzai.zzf = zzf;
        zzau.zza(zzi, "ApiKey cannot be null");
        zzai.zze.add(zzi);
        zzf.zza(zzai);
    }
    
    private final void zzi() {
        if (!this.zze.isEmpty()) {
            this.zzf.zza(this);
        }
    }
    
    @Override
    public final void zza() {
        super.zza();
        this.zzf.zzb(this);
    }
    
    @Override
    protected final void zza(final ConnectionResult connectionResult, final int n) {
        this.zzf.zzb(connectionResult, n);
    }
    
    @Override
    public final void zzb() {
        super.zzb();
        this.zzi();
    }
    
    @Override
    protected final void zzc() {
        this.zzf.zzd();
    }
    
    @Override
    public final void zze() {
        super.zze();
        this.zzi();
    }
    
    final ArraySet<zzi<?>> zzf() {
        return this.zze;
    }
}
