package com.google.android.gms.common.api.internal;

import java.util.Collection;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Iterator;
import android.app.Application;
import android.os.Message;
import com.google.android.gms.common.api.GoogleApi;
import android.os.HandlerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.support.v4.util.ArraySet;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Looper;
import android.os.Handler;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import com.google.android.gms.common.api.Status;
import android.os.Handler$Callback;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzbv implements zzdg, ConnectionProgressReportCallbacks
{
    final /* synthetic */ zzbn zza;
    private final Api.zze zzb;
    private final zzi<?> zzc;
    private IAccountAccessor zzd;
    private Set<Scope> zze;
    private boolean zzf;
    
    public zzbv(final zzbn zza, final Api.zze zzb, final zzi<?> zzc) {
        this.zza = zza;
        this.zzd = null;
        this.zze = null;
        this.zzf = false;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    private final void zza() {
        if (this.zzf && this.zzd != null) {
            this.zzb.zza(this.zzd, this.zze);
        }
    }
    
    @Override
    public final void onReportServiceBinding(final ConnectionResult connectionResult) {
        this.zza.zzq.post((Runnable)new zzbw(this, connectionResult));
    }
    
    @Override
    public final void zza(final ConnectionResult connectionResult) {
        this.zza.zzm.get(this.zzc).zza(connectionResult);
    }
    
    @Override
    public final void zza(final IAccountAccessor zzd, final Set<Scope> zze) {
        if (zzd != null && zze != null) {
            this.zzd = zzd;
            this.zze = zze;
            this.zza();
            return;
        }
        Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", (Throwable)new Exception());
        this.zza(new ConnectionResult(4));
    }
}
