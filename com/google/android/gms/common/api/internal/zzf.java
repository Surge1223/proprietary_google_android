package com.google.android.gms.common.api.internal;

import android.os.TransactionTooLargeException;
import com.google.android.gms.common.util.zzo;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;

public final class zzf<TResult> extends zzb
{
    private final zzdn<Api.zzb, TResult> zza;
    private final TaskCompletionSource<TResult> zzb;
    private final zzdi zzc;
    
    public zzf(final int n, final zzdn<Api.zzb, TResult> zza, final TaskCompletionSource<TResult> zzb, final zzdi zzc) {
        super(n);
        this.zzb = zzb;
        this.zza = zza;
        this.zzc = zzc;
    }
    
    @Override
    public final void zza(final Status status) {
        this.zzb.trySetException(this.zzc.zza(status));
    }
    
    @Override
    public final void zza(final zzaf zzaf, final boolean b) {
        zzaf.zza(this.zzb, b);
    }
    
    @Override
    public final void zza(final zzbp<?> zzbp) throws DeadObjectException {
        try {
            this.zza.zza(zzbp.zzb(), this.zzb);
        }
        catch (RuntimeException ex) {
            this.zzb.trySetException(ex);
        }
        catch (RemoteException ex2) {
            this.zza(zzb(ex2));
        }
        catch (DeadObjectException ex3) {
            throw ex3;
        }
    }
}
