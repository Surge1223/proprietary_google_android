package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.app.Activity;
import android.content.Intent;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.support.v4.util.ArrayMap;
import java.util.Map;
import java.lang.ref.WeakReference;
import android.support.v4.app.FragmentActivity;
import java.util.WeakHashMap;
import android.support.v4.app.Fragment;
import android.os.Bundle;

final class zzdl implements Runnable
{
    private final /* synthetic */ LifecycleCallback zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ zzdk zzc;
    
    zzdl(final zzdk zzc, final LifecycleCallback zza, final String zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void run() {
        if (this.zzc.zzc > 0) {
            final LifecycleCallback zza = this.zza;
            Bundle bundle;
            if (this.zzc.zzd != null) {
                bundle = this.zzc.zzd.getBundle(this.zzb);
            }
            else {
                bundle = null;
            }
            zza.zza(bundle);
        }
        if (this.zzc.zzc >= 2) {
            this.zza.zzb();
        }
        if (this.zzc.zzc >= 3) {
            this.zza.zze();
        }
        if (this.zzc.zzc >= 4) {
            this.zza.zza();
        }
        if (this.zzc.zzc >= 5) {
            this.zza.zzh();
        }
    }
}
