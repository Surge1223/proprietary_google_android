package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzego;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzax;
import android.util.Log;
import com.google.android.gms.internal.zzegv;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.signin.zza;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import android.os.Handler;
import android.content.Context;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zzd;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzegn;
import com.google.android.gms.common.ConnectionResult;

final class zzde implements Runnable
{
    private final /* synthetic */ zzdd zza;
    
    zzde(final zzdd zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        this.zza.zzh.zza(new ConnectionResult(4));
    }
}
