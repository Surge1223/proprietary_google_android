package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;

public abstract class zzcx<A extends Api.zzb, L>
{
    private final zzcl<L> zza;
    
    protected abstract void zza(final A p0, final TaskCompletionSource<Void> p1) throws RemoteException;
    
    public final void zzb() {
        this.zza.zzb();
    }
}
