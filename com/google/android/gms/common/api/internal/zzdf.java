package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzego;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzax;
import com.google.android.gms.common.ConnectionResult;
import android.util.Log;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.signin.zza;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import android.os.Handler;
import android.content.Context;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zzd;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzegn;
import com.google.android.gms.internal.zzegv;

final class zzdf implements Runnable
{
    private final /* synthetic */ zzegv zza;
    private final /* synthetic */ zzdd zzb;
    
    zzdf(final zzdd zzb, final zzegv zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        this.zzb.zzb(this.zza);
    }
}
