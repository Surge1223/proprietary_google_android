package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.internal.zzbd;
import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;

public abstract class zzn<R extends Result, A extends Api.zzb> extends BasePendingResult<R>
{
    private final Api.zzc<A> zza;
    private final Api<?> zzb;
    
    protected zzn(final Api<?> zzb, final GoogleApiClient googleApiClient) {
        super(zzau.zza(googleApiClient, "GoogleApiClient must not be null"));
        zzau.zza(zzb, "Api must not be null");
        this.zza = (Api.zzc<A>)zzb.zzc();
        this.zzb = zzb;
    }
    
    private final void zza(final RemoteException ex) {
        this.zzc(new Status(8, ex.getLocalizedMessage(), null));
    }
    
    protected abstract void zza(final A p0) throws RemoteException;
    
    public final void zzb(final A a) throws DeadObjectException {
        Api.zzb zzc = (Api.zzb)a;
        if (a instanceof zzbd) {
            zzc = (Api.zzb)zzbd.zzc();
        }
        try {
            this.zza((A)zzc);
        }
        catch (RemoteException ex) {
            this.zza(ex);
        }
        catch (DeadObjectException ex2) {
            this.zza((RemoteException)ex2);
            throw ex2;
        }
    }
    
    public final void zzc(final Status status) {
        zzau.zzb(status.isSuccess() ^ true, "Failed result must not be success");
        this.zza(this.zza(status));
    }
}
