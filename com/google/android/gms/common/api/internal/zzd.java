package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;

public final class zzd<A extends zzn<? extends Result, Api.zzb>> extends zzb
{
    private final A zza;
    
    public zzd(final int n, final A zza) {
        super(n);
        this.zza = zza;
    }
    
    @Override
    public final void zza(final Status status) {
        this.zza.zzc(status);
    }
    
    @Override
    public final void zza(final zzaf zzaf, final boolean b) {
        zzaf.zza(this.zza, b);
    }
    
    @Override
    public final void zza(final zzbp<?> zzbp) throws DeadObjectException {
        try {
            ((zzn<R, Api.zze>)this.zza).zzb(zzbp.zzb());
        }
        catch (RuntimeException ex) {
            final String simpleName = ex.getClass().getSimpleName();
            final String localizedMessage = ex.getLocalizedMessage();
            final StringBuilder sb = new StringBuilder(2 + String.valueOf(simpleName).length() + String.valueOf(localizedMessage).length());
            sb.append(simpleName);
            sb.append(": ");
            sb.append(localizedMessage);
            this.zza.zzc(new Status(10, sb.toString()));
        }
    }
}
