package com.google.android.gms.common.api.internal;

public final class zzcn<L>
{
    private final L zza;
    private final String zzb;
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzcn)) {
            return false;
        }
        final zzcn zzcn = (zzcn)o;
        return this.zza == zzcn.zza && this.zzb.equals(zzcn.zzb);
    }
    
    @Override
    public final int hashCode() {
        return System.identityHashCode(this.zza) * 31 + this.zzb.hashCode();
    }
}
