package com.google.android.gms.common.api.internal;

import android.os.TransactionTooLargeException;
import com.google.android.gms.common.util.zzo;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zzc<T> extends zzb
{
    protected final TaskCompletionSource<T> zza;
    
    public zzc(final int n, final TaskCompletionSource<T> zza) {
        super(n);
        this.zza = zza;
    }
    
    @Override
    public void zza(final Status status) {
        this.zza.trySetException(new ApiException(status));
    }
    
    @Override
    public void zza(final zzaf zzaf, final boolean b) {
    }
    
    @Override
    public final void zza(final zzbp<?> zzbp) throws DeadObjectException {
        try {
            this.zzb(zzbp);
        }
        catch (RuntimeException ex) {
            this.zza(ex);
        }
        catch (RemoteException ex2) {
            this.zza(zzb(ex2));
        }
        catch (DeadObjectException ex3) {
            this.zza(zzb((RemoteException)ex3));
            throw ex3;
        }
    }
    
    public void zza(final RuntimeException ex) {
        this.zza.trySetException(ex);
    }
    
    protected abstract void zzb(final zzbp<?> p0) throws RemoteException;
}
