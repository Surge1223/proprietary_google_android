package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;

public abstract class zzdx<A extends Api.zzb, L>
{
    protected abstract void zza(final A p0, final TaskCompletionSource<Boolean> p1) throws RemoteException;
}
