package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.AvailabilityException;
import java.util.Set;
import java.util.Map;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.ConnectionResult;
import android.support.v4.util.ArrayMap;

public final class zzk
{
    private final ArrayMap<zzi<?>, ConnectionResult> zza;
    private final ArrayMap<zzi<?>, String> zzb;
    private final TaskCompletionSource<Map<zzi<?>, String>> zzc;
    private int zzd;
    private boolean zze;
    
    public final Set<zzi<?>> zza() {
        return this.zza.keySet();
    }
    
    public final void zza(final zzi<?> zzi, final ConnectionResult connectionResult, final String s) {
        this.zza.put(zzi, connectionResult);
        this.zzb.put(zzi, s);
        --this.zzd;
        if (!connectionResult.isSuccess()) {
            this.zze = true;
        }
        if (this.zzd == 0) {
            if (this.zze) {
                this.zzc.setException(new AvailabilityException(this.zza));
                return;
            }
            this.zzc.setResult(this.zzb);
        }
    }
}
