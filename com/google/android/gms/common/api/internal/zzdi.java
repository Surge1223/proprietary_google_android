package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;

public interface zzdi
{
    Exception zza(final Status p0);
}
