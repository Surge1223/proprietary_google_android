package com.google.android.gms.common.api.internal;

import java.util.Collection;
import com.google.android.gms.common.api.Result;
import android.app.Application;
import android.util.Log;
import android.os.HandlerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.support.v4.util.ArraySet;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Handler;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import android.os.Handler$Callback;
import android.app.PendingIntent;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzau;
import android.os.Looper;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.os.DeadObjectException;
import java.util.Iterator;
import com.google.android.gms.common.internal.zzbd;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.ConnectionResult;
import java.util.Map;
import java.util.Set;
import java.util.Queue;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Api;

public final class zzbp<O extends Api.ApiOptions> implements ConnectionCallbacks, OnConnectionFailedListener, zzv
{
    final /* synthetic */ zzbn zza;
    private final Queue<zzb> zzb;
    private final Api.zze zzc;
    private final Api.zzb zzd;
    private final zzi<O> zze;
    private final zzaf zzf;
    private final Set<zzk> zzg;
    private final Map<zzcn<?>, zzcy> zzh;
    private final int zzi;
    private final zzdd zzj;
    private boolean zzk;
    private int zzl;
    private ConnectionResult zzm;
    
    public zzbp(final zzbn zza, final GoogleApi<O> googleApi) {
        this.zza = zza;
        this.zzb = new LinkedList<zzb>();
        this.zzg = new HashSet<zzk>();
        this.zzh = new HashMap<zzcn<?>, zzcy>();
        this.zzl = -1;
        this.zzm = null;
        this.zzc = googleApi.zza(zza.zzq.getLooper(), this);
        if (this.zzc instanceof zzbd) {
            this.zzd = zzbd.zzc();
        }
        else {
            this.zzd = this.zzc;
        }
        this.zze = googleApi.zzd();
        this.zzf = new zzaf();
        this.zzi = googleApi.zze();
        if (this.zzc.requiresSignIn()) {
            this.zzj = googleApi.zza(zza.zzh, zza.zzq);
            return;
        }
        this.zzj = null;
    }
    
    private final void zzb(final ConnectionResult connectionResult) {
        for (final zzk zzk : this.zzg) {
            String zzab = null;
            if (connectionResult == ConnectionResult.zza) {
                zzab = this.zzc.zzab();
            }
            zzk.zza(this.zze, connectionResult, zzab);
        }
        this.zzg.clear();
    }
    
    private final void zzb(final zzb zzb) {
        zzb.zza(this.zzf, this.zzk());
        try {
            zzb.zza(this);
        }
        catch (DeadObjectException ex) {
            this.onConnectionSuspended(1);
            this.zzc.disconnect();
        }
    }
    
    private final void zzn() {
        this.zzd();
        this.zzb(ConnectionResult.zza);
        this.zzp();
        for (final zzcy zzcy : this.zzh.values()) {
            try {
                zzcy.zza.zza(this.zzd, new TaskCompletionSource<Void>());
                continue;
            }
            catch (RemoteException ex) {
                continue;
            }
            catch (DeadObjectException ex2) {
                this.onConnectionSuspended(1);
                this.zzc.disconnect();
            }
            break;
        }
        while (this.zzc.isConnected() && !this.zzb.isEmpty()) {
            this.zzb(this.zzb.remove());
        }
        this.zzq();
    }
    
    private final void zzo() {
        this.zzd();
        this.zzk = true;
        this.zzf.zzc();
        this.zza.zzq.sendMessageDelayed(Message.obtain(this.zza.zzq, 9, (Object)this.zze), this.zza.zzc);
        this.zza.zzq.sendMessageDelayed(Message.obtain(this.zza.zzq, 11, (Object)this.zze), this.zza.zzd);
        this.zza.zzj.zza();
    }
    
    private final void zzp() {
        if (this.zzk) {
            this.zza.zzq.removeMessages(11, (Object)this.zze);
            this.zza.zzq.removeMessages(9, (Object)this.zze);
            this.zzk = false;
        }
    }
    
    private final void zzq() {
        this.zza.zzq.removeMessages(12, (Object)this.zze);
        this.zza.zzq.sendMessageDelayed(this.zza.zzq.obtainMessage(12, (Object)this.zze), this.zza.zze);
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        if (Looper.myLooper() == this.zza.zzq.getLooper()) {
            this.zzn();
            return;
        }
        this.zza.zzq.post((Runnable)new zzbq(this));
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult zzm) {
        zzau.zza(this.zza.zzq);
        if (this.zzj != null) {
            this.zzj.zzb();
        }
        this.zzd();
        this.zza.zzj.zza();
        this.zzb(zzm);
        if (zzm.getErrorCode() == 4) {
            this.zza(zzbn.zzb);
            return;
        }
        if (this.zzb.isEmpty()) {
            this.zzm = zzm;
            return;
        }
        Object o = zzbn.zzf;
        synchronized (o) {
            if (this.zza.zzn != null && this.zza.zzo.contains(this.zze)) {
                this.zza.zzn.zzb(zzm, this.zzi);
                return;
            }
            // monitorexit(o)
            if (!this.zza.zza(zzm, this.zzi)) {
                if (zzm.getErrorCode() == 18) {
                    this.zzk = true;
                }
                if (this.zzk) {
                    this.zza.zzq.sendMessageDelayed(Message.obtain(this.zza.zzq, 9, (Object)this.zze), this.zza.zzc);
                    return;
                }
                o = this.zze.zza();
                final StringBuilder sb = new StringBuilder(38 + String.valueOf(o).length());
                sb.append("API: ");
                sb.append((String)o);
                sb.append(" is not available on this device.");
                this.zza(new Status(17, sb.toString()));
            }
        }
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        if (Looper.myLooper() == this.zza.zzq.getLooper()) {
            this.zzo();
            return;
        }
        this.zza.zzq.post((Runnable)new zzbr(this));
    }
    
    public final void zza() {
        zzau.zza(this.zza.zzq);
        this.zza(zzbn.zza);
        this.zzf.zzb();
        final zzcn[] array = this.zzh.keySet().toArray(new zzcn[this.zzh.size()]);
        for (int length = array.length, i = 0; i < length; ++i) {
            this.zza(new zzg(array[i], new TaskCompletionSource<Boolean>()));
        }
        this.zzb(new ConnectionResult(4));
        if (this.zzc.isConnected()) {
            this.zzc.onUserSignOut(new zzbt(this));
        }
    }
    
    public final void zza(final ConnectionResult connectionResult) {
        zzau.zza(this.zza.zzq);
        this.zzc.disconnect();
        this.onConnectionFailed(connectionResult);
    }
    
    public final void zza(final Status status) {
        zzau.zza(this.zza.zzq);
        final Iterator<zzb> iterator = this.zzb.iterator();
        while (iterator.hasNext()) {
            iterator.next().zza(status);
        }
        this.zzb.clear();
    }
    
    public final void zza(final zzb zzb) {
        zzau.zza(this.zza.zzq);
        if (this.zzc.isConnected()) {
            this.zzb(zzb);
            this.zzq();
            return;
        }
        this.zzb.add(zzb);
        if (this.zzm != null && this.zzm.hasResolution()) {
            this.onConnectionFailed(this.zzm);
            return;
        }
        this.zzi();
    }
    
    public final void zza(final zzk zzk) {
        zzau.zza(this.zza.zzq);
        this.zzg.add(zzk);
    }
    
    public final Api.zze zzb() {
        return this.zzc;
    }
    
    public final Map<zzcn<?>, zzcy> zzc() {
        return this.zzh;
    }
    
    public final void zzd() {
        zzau.zza(this.zza.zzq);
        this.zzm = null;
    }
    
    public final ConnectionResult zze() {
        zzau.zza(this.zza.zzq);
        return this.zzm;
    }
    
    public final void zzf() {
        zzau.zza(this.zza.zzq);
        if (this.zzk) {
            this.zzi();
        }
    }
    
    public final void zzg() {
        zzau.zza(this.zza.zzq);
        if (this.zzk) {
            this.zzp();
            Status status;
            if (this.zza.zzi.isGooglePlayServicesAvailable(this.zza.zzh) == 18) {
                status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
            }
            else {
                status = new Status(8, "API failed to connect while resuming due to an unknown error.");
            }
            this.zza(status);
            this.zzc.disconnect();
        }
    }
    
    public final void zzh() {
        zzau.zza(this.zza.zzq);
        if (this.zzc.isConnected() && this.zzh.size() == 0) {
            if (this.zzf.zza()) {
                this.zzq();
                return;
            }
            this.zzc.disconnect();
        }
    }
    
    public final void zzi() {
        zzau.zza(this.zza.zzq);
        if (this.zzc.isConnected() || this.zzc.isConnecting()) {
            return;
        }
        final int zza = this.zza.zzj.zza(this.zza.zzh, this.zzc);
        if (zza != 0) {
            this.onConnectionFailed(new ConnectionResult(zza, null));
            return;
        }
        final zzbv zzbv = new zzbv(this.zzc, this.zze);
        if (this.zzc.requiresSignIn()) {
            this.zzj.zza(zzbv);
        }
        this.zzc.connect(zzbv);
    }
    
    final boolean zzj() {
        return this.zzc.isConnected();
    }
    
    public final boolean zzk() {
        return this.zzc.requiresSignIn();
    }
    
    public final int zzl() {
        return this.zzi;
    }
}
