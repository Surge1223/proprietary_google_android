package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzau;
import android.util.Log;
import com.google.android.gms.common.api.Releasable;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;

final class zzdr implements Runnable
{
    private final /* synthetic */ Result zza;
    private final /* synthetic */ zzdq zzb;
    
    zzdr(final zzdq zzb, final Result zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        try {
            try {
                BasePendingResult.zzc.set(true);
                this.zzb.zzh.sendMessage(this.zzb.zzh.obtainMessage(0, (Object)this.zzb.zza.onSuccess(this.zza)));
                BasePendingResult.zzc.set(false);
                zzdq.zza(this.zzb, this.zza);
                final GoogleApiClient googleApiClient = (GoogleApiClient)this.zzb.zzg.get();
                if (googleApiClient != null) {
                    googleApiClient.zzb(this.zzb);
                }
            }
            finally {
                BasePendingResult.zzc.set(false);
                zzdq.zza(this.zzb, this.zza);
                final GoogleApiClient googleApiClient2 = (GoogleApiClient)this.zzb.zzg.get();
                if (googleApiClient2 != null) {
                    googleApiClient2.zzb(this.zzb);
                }
                final GoogleApiClient googleApiClient3;
                googleApiClient3.zzb(this.zzb);
            }
        }
        catch (RuntimeException ex) {}
    }
}
