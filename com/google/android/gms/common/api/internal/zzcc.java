package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.common.api.Status;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzcc extends zzez implements zzcb
{
    public zzcc() {
        this.attachInterface((IInterface)this, "com.google.android.gms.common.api.internal.IStatusCallback");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            this.zza(zzfa.zza(parcel, Status.CREATOR));
            return true;
        }
        return false;
    }
}
