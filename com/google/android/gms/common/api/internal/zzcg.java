package com.google.android.gms.common.api.internal;

import android.support.v4.app.FragmentActivity;
import com.google.android.gms.common.internal.zzau;
import android.app.Activity;

public final class zzcg
{
    private final Object zza;
    
    public zzcg(final Activity zza) {
        zzau.zza(zza, "Activity must not be null");
        this.zza = zza;
    }
    
    public final boolean zza() {
        return this.zza instanceof FragmentActivity;
    }
    
    public final boolean zzb() {
        return this.zza instanceof Activity;
    }
    
    public final Activity zzc() {
        return (Activity)this.zza;
    }
    
    public final FragmentActivity zzd() {
        return (FragmentActivity)this.zza;
    }
}
