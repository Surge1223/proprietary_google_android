package com.google.android.gms.common.api.internal;

import android.app.Dialog;

final class zzs extends zzbz
{
    private final /* synthetic */ Dialog zza;
    private final /* synthetic */ zzr zzb;
    
    zzs(final zzr zzb, final Dialog zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        this.zzb.zza.zzd();
        if (this.zza.isShowing()) {
            this.zza.dismiss();
        }
    }
}
