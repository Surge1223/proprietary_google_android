package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.app.Activity;
import android.content.Intent;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.support.v4.util.ArrayMap;
import android.os.Bundle;
import java.util.Map;
import java.lang.ref.WeakReference;
import android.support.v4.app.FragmentActivity;
import java.util.WeakHashMap;
import android.support.v4.app.Fragment;

public final class zzdk extends Fragment implements zzch
{
    private static WeakHashMap<FragmentActivity, WeakReference<zzdk>> zza;
    private Map<String, LifecycleCallback> zzb;
    private int zzc;
    private Bundle zzd;
    
    static {
        zzdk.zza = new WeakHashMap<FragmentActivity, WeakReference<zzdk>>();
    }
    
    public zzdk() {
        this.zzb = new ArrayMap<String, LifecycleCallback>();
        this.zzc = 0;
    }
    
    public static zzdk zza(final FragmentActivity fragmentActivity) {
        final WeakReference<zzdk> weakReference = zzdk.zza.get(fragmentActivity);
        if (weakReference != null) {
            final zzdk zzdk = weakReference.get();
            if (zzdk != null) {
                return zzdk;
            }
        }
        try {
            final zzdk zzdk2 = (zzdk)fragmentActivity.getSupportFragmentManager().findFragmentByTag("SupportLifecycleFragmentImpl");
            zzdk zzdk3 = null;
            Label_0080: {
                if (zzdk2 != null) {
                    zzdk3 = zzdk2;
                    if (!zzdk2.isRemoving()) {
                        break Label_0080;
                    }
                }
                zzdk3 = new zzdk();
                fragmentActivity.getSupportFragmentManager().beginTransaction().add(zzdk3, "SupportLifecycleFragmentImpl").commitAllowingStateLoss();
            }
            zzdk.zza.put(fragmentActivity, new WeakReference<zzdk>(zzdk3));
            return zzdk3;
        }
        catch (ClassCastException ex) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", ex);
        }
    }
    
    @Override
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zza(s, fileDescriptor, printWriter, array);
        }
    }
    
    @Override
    public final void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zza(n, n2, intent);
        }
    }
    
    @Override
    public final void onCreate(final Bundle zzd) {
        super.onCreate(zzd);
        this.zzc = 1;
        this.zzd = zzd;
        for (final Map.Entry<String, LifecycleCallback> entry : this.zzb.entrySet()) {
            final LifecycleCallback lifecycleCallback = entry.getValue();
            Bundle bundle;
            if (zzd != null) {
                bundle = zzd.getBundle((String)entry.getKey());
            }
            else {
                bundle = null;
            }
            lifecycleCallback.zza(bundle);
        }
    }
    
    @Override
    public final void onDestroy() {
        super.onDestroy();
        this.zzc = 5;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zzh();
        }
    }
    
    @Override
    public final void onResume() {
        super.onResume();
        this.zzc = 3;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zze();
        }
    }
    
    @Override
    public final void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle == null) {
            return;
        }
        for (final Map.Entry<String, LifecycleCallback> entry : this.zzb.entrySet()) {
            final Bundle bundle2 = new Bundle();
            entry.getValue().zzb(bundle2);
            bundle.putBundle((String)entry.getKey(), bundle2);
        }
    }
    
    @Override
    public final void onStart() {
        super.onStart();
        this.zzc = 2;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zzb();
        }
    }
    
    @Override
    public final void onStop() {
        super.onStop();
        this.zzc = 4;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zza();
        }
    }
    
    @Override
    public final <T extends LifecycleCallback> T zza(final String s, final Class<T> clazz) {
        return clazz.cast(this.zzb.get(s));
    }
    
    @Override
    public final void zza(final String s, final LifecycleCallback lifecycleCallback) {
        if (!this.zzb.containsKey(s)) {
            this.zzb.put(s, lifecycleCallback);
            if (this.zzc > 0) {
                new Handler(Looper.getMainLooper()).post((Runnable)new zzdl(this, lifecycleCallback, s));
            }
            return;
        }
        final StringBuilder sb = new StringBuilder(59 + String.valueOf(s).length());
        sb.append("LifecycleCallback with tag ");
        sb.append(s);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }
}
