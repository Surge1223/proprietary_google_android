package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import android.os.IInterface;

public interface zzcb extends IInterface
{
    void zza(final Status p0) throws RemoteException;
}
