package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.ConnectionResult;

public interface zzdg
{
    void zza(final ConnectionResult p0);
    
    void zza(final IAccountAccessor p0, final Set<Scope> p1);
}
