package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ApiException;
import java.util.HashMap;
import java.util.Collections;
import java.util.WeakHashMap;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Map;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;

final class zzag implements zza
{
    private final /* synthetic */ BasePendingResult zza;
    private final /* synthetic */ zzaf zzb;
    
    zzag(final zzaf zzb, final BasePendingResult zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void zza(final Status status) {
        this.zzb.zza.remove(this.zza);
    }
}
