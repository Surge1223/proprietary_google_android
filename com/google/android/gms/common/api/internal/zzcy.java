package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;

public final class zzcy
{
    public final zzcx<Api.zzb, ?> zza;
    public final zzdx<Api.zzb, ?> zzb;
}
