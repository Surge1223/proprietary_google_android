package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzau;
import android.os.Looper;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.os.DeadObjectException;
import java.util.Iterator;
import com.google.android.gms.common.internal.zzbd;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.ConnectionResult;
import java.util.Map;
import java.util.Set;
import java.util.Queue;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Api;

final class zzbu implements Runnable
{
    private final /* synthetic */ zzbt zza;
    
    zzbu(final zzbt zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        this.zza.zza.zzc.disconnect();
    }
}
