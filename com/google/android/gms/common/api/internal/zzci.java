package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.content.Intent;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.support.v4.util.ArrayMap;
import android.os.Bundle;
import java.util.Map;
import java.lang.ref.WeakReference;
import android.app.Activity;
import java.util.WeakHashMap;
import android.app.Fragment;

public final class zzci extends Fragment implements zzch
{
    private static WeakHashMap<Activity, WeakReference<zzci>> zza;
    private Map<String, LifecycleCallback> zzb;
    private int zzc;
    private Bundle zzd;
    
    static {
        zzci.zza = new WeakHashMap<Activity, WeakReference<zzci>>();
    }
    
    public zzci() {
        this.zzb = new ArrayMap<String, LifecycleCallback>();
        this.zzc = 0;
    }
    
    public static zzci zza(final Activity activity) {
        final WeakReference<zzci> weakReference = zzci.zza.get(activity);
        if (weakReference != null) {
            final zzci zzci = weakReference.get();
            if (zzci != null) {
                return zzci;
            }
        }
        try {
            final zzci zzci2 = (zzci)activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
            zzci zzci3 = null;
            Label_0080: {
                if (zzci2 != null) {
                    zzci3 = zzci2;
                    if (!zzci2.isRemoving()) {
                        break Label_0080;
                    }
                }
                zzci3 = new zzci();
                activity.getFragmentManager().beginTransaction().add((Fragment)zzci3, "LifecycleFragmentImpl").commitAllowingStateLoss();
            }
            zzci.zza.put(activity, new WeakReference<zzci>(zzci3));
            return zzci3;
        }
        catch (ClassCastException ex) {
            throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", ex);
        }
    }
    
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zza(s, fileDescriptor, printWriter, array);
        }
    }
    
    public final void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zza(n, n2, intent);
        }
    }
    
    public final void onCreate(final Bundle zzd) {
        super.onCreate(zzd);
        this.zzc = 1;
        this.zzd = zzd;
        for (final Map.Entry<String, LifecycleCallback> entry : this.zzb.entrySet()) {
            final LifecycleCallback lifecycleCallback = entry.getValue();
            Bundle bundle;
            if (zzd != null) {
                bundle = zzd.getBundle((String)entry.getKey());
            }
            else {
                bundle = null;
            }
            lifecycleCallback.zza(bundle);
        }
    }
    
    public final void onDestroy() {
        super.onDestroy();
        this.zzc = 5;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zzh();
        }
    }
    
    public final void onResume() {
        super.onResume();
        this.zzc = 3;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zze();
        }
    }
    
    public final void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle == null) {
            return;
        }
        for (final Map.Entry<String, LifecycleCallback> entry : this.zzb.entrySet()) {
            final Bundle bundle2 = new Bundle();
            entry.getValue().zzb(bundle2);
            bundle.putBundle((String)entry.getKey(), bundle2);
        }
    }
    
    public final void onStart() {
        super.onStart();
        this.zzc = 2;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zzb();
        }
    }
    
    public final void onStop() {
        super.onStop();
        this.zzc = 4;
        final Iterator<LifecycleCallback> iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zza();
        }
    }
    
    public final Activity zza() {
        return this.getActivity();
    }
    
    public final <T extends LifecycleCallback> T zza(final String s, final Class<T> clazz) {
        return clazz.cast(this.zzb.get(s));
    }
    
    public final void zza(final String s, final LifecycleCallback lifecycleCallback) {
        if (!this.zzb.containsKey(s)) {
            this.zzb.put(s, lifecycleCallback);
            if (this.zzc > 0) {
                new Handler(Looper.getMainLooper()).post((Runnable)new zzcj(this, lifecycleCallback, s));
            }
            return;
        }
        final StringBuilder sb = new StringBuilder(59 + String.valueOf(s).length());
        sb.append("LifecycleCallback with tag ");
        sb.append(s);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }
}
