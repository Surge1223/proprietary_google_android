package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.DialogInterface$OnCancelListener;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApiActivity;

final class zzr implements Runnable
{
    final /* synthetic */ zzp zza;
    private final zzq zzb;
    
    zzr(final zzp zza, final zzq zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void run() {
        if (!this.zza.zza) {
            return;
        }
        final ConnectionResult zzb = this.zzb.zzb();
        if (zzb.hasResolution()) {
            this.zza.zzd.startActivityForResult(GoogleApiActivity.zza((Context)this.zza.zzg(), zzb.getResolution(), this.zzb.zza(), false), 1);
            return;
        }
        if (this.zza.zzc.isUserResolvableError(zzb.getErrorCode())) {
            this.zza.zzc.zza(this.zza.zzg(), this.zza.zzd, zzb.getErrorCode(), 2, (DialogInterface$OnCancelListener)this.zza);
            return;
        }
        if (zzb.getErrorCode() == 18) {
            GoogleApiAvailability.zza(this.zza.zzg().getApplicationContext(), new zzs(this, GoogleApiAvailability.zza(this.zza.zzg(), (DialogInterface$OnCancelListener)this.zza)));
            return;
        }
        this.zza.zza(zzb, this.zzb.zza());
    }
}
