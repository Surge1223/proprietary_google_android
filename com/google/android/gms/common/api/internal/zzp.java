package com.google.android.gms.common.api.internal;

import android.os.Parcelable;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import android.content.DialogInterface;
import android.os.Looper;
import android.os.Handler;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.concurrent.atomic.AtomicReference;
import android.content.DialogInterface$OnCancelListener;

public abstract class zzp extends LifecycleCallback implements DialogInterface$OnCancelListener
{
    protected volatile boolean zza;
    protected final AtomicReference<zzq> zzb;
    protected final GoogleApiAvailability zzc;
    private final Handler zze;
    
    protected zzp(final zzch zzch) {
        this(zzch, GoogleApiAvailability.getInstance());
    }
    
    private zzp(final zzch zzch, final GoogleApiAvailability zzc) {
        super(zzch);
        this.zzb = new AtomicReference<zzq>(null);
        this.zze = new Handler(Looper.getMainLooper());
        this.zzc = zzc;
    }
    
    private static int zza(final zzq zzq) {
        if (zzq == null) {
            return -1;
        }
        return zzq.zza();
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        this.zza(new ConnectionResult(13, null), zza(this.zzb.get()));
        this.zzd();
    }
    
    @Override
    public void zza() {
        super.zza();
        this.zza = false;
    }
    
    @Override
    public final void zza(int intExtra, int n, final Intent intent) {
        final zzq zzq = this.zzb.get();
        final int n2 = 1;
        final int n3 = 1;
        zzq zzq2 = null;
        Label_0185: {
            switch (intExtra) {
                default: {
                    zzq2 = zzq;
                    break;
                }
                case 2: {
                    final int googlePlayServicesAvailable = this.zzc.isGooglePlayServicesAvailable((Context)this.zzg());
                    if (googlePlayServicesAvailable == 0) {
                        intExtra = n3;
                    }
                    else {
                        intExtra = 0;
                    }
                    if (zzq == null) {
                        return;
                    }
                    zzq2 = zzq;
                    n = intExtra;
                    if (zzq.zzb().getErrorCode() != 18) {
                        break Label_0185;
                    }
                    zzq2 = zzq;
                    n = intExtra;
                    if (googlePlayServicesAvailable == 18) {
                        return;
                    }
                    break Label_0185;
                }
                case 1: {
                    if (n == -1) {
                        zzq2 = zzq;
                        n = n2;
                        break Label_0185;
                    }
                    zzq2 = zzq;
                    if (n == 0) {
                        intExtra = 13;
                        if (intent != null) {
                            intExtra = intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
                        }
                        zzq2 = new zzq(new ConnectionResult(intExtra, null), zza(zzq));
                        this.zzb.set(zzq2);
                        break;
                    }
                    break;
                }
            }
            n = 0;
        }
        if (n != 0) {
            this.zzd();
            return;
        }
        if (zzq2 != null) {
            this.zza(zzq2.zzb(), zzq2.zza());
        }
    }
    
    @Override
    public final void zza(final Bundle bundle) {
        super.zza(bundle);
        if (bundle != null) {
            final AtomicReference<zzq> zzb = this.zzb;
            zzq zzq;
            if (bundle.getBoolean("resolving_error", false)) {
                zzq = new zzq(new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent)bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1));
            }
            else {
                zzq = null;
            }
            zzb.set(zzq);
        }
    }
    
    protected abstract void zza(final ConnectionResult p0, final int p1);
    
    @Override
    public void zzb() {
        super.zzb();
        this.zza = true;
    }
    
    @Override
    public final void zzb(final Bundle bundle) {
        super.zzb(bundle);
        final zzq zzq = this.zzb.get();
        if (zzq != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", zzq.zza());
            bundle.putInt("failed_status", zzq.zzb().getErrorCode());
            bundle.putParcelable("failed_resolution", (Parcelable)zzq.zzb().getResolution());
        }
    }
    
    public final void zzb(final ConnectionResult connectionResult, final int n) {
        final zzq zzq = new zzq(connectionResult, n);
        if (this.zzb.compareAndSet(null, zzq)) {
            this.zze.post((Runnable)new zzr(this, zzq));
        }
    }
    
    protected abstract void zzc();
    
    protected final void zzd() {
        this.zzb.set(null);
        this.zzc();
    }
}
