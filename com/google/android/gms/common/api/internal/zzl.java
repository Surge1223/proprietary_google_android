package com.google.android.gms.common.api.internal;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager$RunningAppProcessInfo;
import com.google.android.gms.common.util.zzo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.app.Activity;
import android.content.ComponentCallbacks;
import android.app.Application;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.ComponentCallbacks2;
import android.app.Application$ActivityLifecycleCallbacks;

public final class zzl implements Application$ActivityLifecycleCallbacks, ComponentCallbacks2
{
    private static final zzl zza;
    private final AtomicBoolean zzb;
    private final AtomicBoolean zzc;
    private final ArrayList<zzm> zzd;
    private boolean zze;
    
    static {
        zza = new zzl();
    }
    
    private zzl() {
        this.zzb = new AtomicBoolean();
        this.zzc = new AtomicBoolean();
        this.zzd = new ArrayList<zzm>();
        this.zze = false;
    }
    
    public static zzl zza() {
        return zzl.zza;
    }
    
    public static void zza(final Application application) {
        synchronized (zzl.zza) {
            if (!zzl.zza.zze) {
                application.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)zzl.zza);
                application.registerComponentCallbacks((ComponentCallbacks)zzl.zza);
                zzl.zza.zze = true;
            }
        }
    }
    
    private final void zzb(final boolean b) {
        synchronized (zzl.zza) {
            final ArrayList<zzm> list = this.zzd;
            final int size = list.size();
            int i = 0;
            while (i < size) {
                final zzm value = list.get(i);
                ++i;
                value.zza(b);
            }
        }
    }
    
    public final void onActivityCreated(final Activity activity, final Bundle bundle) {
        final boolean compareAndSet = this.zzb.compareAndSet(true, false);
        this.zzc.set(true);
        if (compareAndSet) {
            this.zzb(false);
        }
    }
    
    public final void onActivityDestroyed(final Activity activity) {
    }
    
    public final void onActivityPaused(final Activity activity) {
    }
    
    public final void onActivityResumed(final Activity activity) {
        final boolean compareAndSet = this.zzb.compareAndSet(true, false);
        this.zzc.set(true);
        if (compareAndSet) {
            this.zzb(false);
        }
    }
    
    public final void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
    }
    
    public final void onActivityStarted(final Activity activity) {
    }
    
    public final void onActivityStopped(final Activity activity) {
    }
    
    public final void onConfigurationChanged(final Configuration configuration) {
    }
    
    public final void onLowMemory() {
    }
    
    public final void onTrimMemory(final int n) {
        if (n == 20 && this.zzb.compareAndSet(false, true)) {
            this.zzc.set(true);
            this.zzb(true);
        }
    }
    
    public final void zza(final zzm zzm) {
        synchronized (zzl.zza) {
            this.zzd.add(zzm);
        }
    }
    
    @TargetApi(16)
    public final boolean zza(final boolean b) {
        if (!this.zzc.get()) {
            if (!zzo.zzb()) {
                return true;
            }
            final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo = new ActivityManager$RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(activityManager$RunningAppProcessInfo);
            if (!this.zzc.getAndSet(true) && activityManager$RunningAppProcessInfo.importance > 100) {
                this.zzb.set(true);
            }
        }
        return this.zzb.get();
    }
}
