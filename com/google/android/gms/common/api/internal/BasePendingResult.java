package com.google.android.gms.common.api.internal;

import android.util.Pair;
import android.os.Message;
import android.os.Handler;
import java.util.concurrent.TimeUnit;
import android.util.Log;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.internal.zzau;
import android.os.Looper;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.atomic.AtomicReference;
import com.google.android.gms.common.api.ResultCallback;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import com.google.android.gms.common.api.GoogleApiClient;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;

@KeepName
public abstract class BasePendingResult<R extends Result> extends PendingResult<R>
{
    static final ThreadLocal<Boolean> zzc;
    @KeepName
    private zzb mResultGuardian;
    private final Object zza;
    private final zza<R> zzb;
    private final WeakReference<GoogleApiClient> zzd;
    private final CountDownLatch zze;
    private final ArrayList<PendingResult.zza> zzf;
    private ResultCallback<? super R> zzg;
    private final AtomicReference<zzdw> zzh;
    private R zzi;
    private Status zzj;
    private volatile boolean zzk;
    private boolean zzl;
    private boolean zzm;
    private zzx zzn;
    private volatile zzdq<R> zzp;
    private boolean zzq;
    
    static {
        zzc = new zzt();
    }
    
    BasePendingResult() {
        this.zza = new Object();
        this.zze = new CountDownLatch(1);
        this.zzf = new ArrayList<PendingResult.zza>();
        this.zzh = new AtomicReference<zzdw>();
        this.zzq = false;
        this.zzb = new zza<R>(Looper.getMainLooper());
        this.zzd = new WeakReference<GoogleApiClient>(null);
    }
    
    protected BasePendingResult(final GoogleApiClient googleApiClient) {
        this.zza = new Object();
        this.zze = new CountDownLatch(1);
        this.zzf = new ArrayList<PendingResult.zza>();
        this.zzh = new AtomicReference<zzdw>();
        this.zzq = false;
        Looper looper;
        if (googleApiClient != null) {
            looper = googleApiClient.zzc();
        }
        else {
            looper = Looper.getMainLooper();
        }
        this.zzb = new zza<R>(looper);
        this.zzd = new WeakReference<GoogleApiClient>(googleApiClient);
    }
    
    private final R zza() {
        Object zza = this.zza;
        synchronized (zza) {
            zzau.zza(this.zzk ^ true, (Object)"Result has already been consumed.");
            zzau.zza(this.zze(), (Object)"Result is not ready.");
            final Result zzi = this.zzi;
            this.zzi = null;
            this.zzg = null;
            this.zzk = true;
            // monitorexit(zza)
            zza = this.zzh.getAndSet(null);
            if (zza != null) {
                ((zzdw)zza).zza(this);
            }
            return (R)zzi;
        }
    }
    
    public static void zzb(final Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable)result).release();
            }
            catch (RuntimeException ex) {
                final String value = String.valueOf(result);
                final StringBuilder sb = new StringBuilder(18 + String.valueOf(value).length());
                sb.append("Unable to release ");
                sb.append(value);
                Log.w("BasePendingResult", sb.toString(), (Throwable)ex);
            }
        }
    }
    
    private final void zzc(final R zzi) {
        this.zzi = zzi;
        this.zzn = null;
        this.zze.countDown();
        this.zzj = this.zzi.getStatus();
        if (this.zzl) {
            this.zzg = null;
        }
        else if (this.zzg == null) {
            if (this.zzi instanceof Releasable) {
                this.mResultGuardian = new zzb((zzt)null);
            }
        }
        else {
            this.zzb.removeMessages(2);
            this.zzb.zza(this.zzg, this.zza());
        }
        final ArrayList<PendingResult.zza> list = this.zzf;
        final int size = list.size();
        int i = 0;
        while (i < size) {
            final PendingResult.zza value = list.get(i);
            ++i;
            value.zza(this.zzj);
        }
        this.zzf.clear();
    }
    
    @Override
    public final R await(final long n, final TimeUnit timeUnit) {
        if (n > 0L) {
            zzau.zzc("await must not be called on the UI thread when time is greater than zero.");
        }
        final boolean zzk = this.zzk;
        boolean b = true;
        zzau.zza(zzk ^ true, (Object)"Result has already been consumed.");
        if (this.zzp != null) {
            b = false;
        }
        zzau.zza(b, (Object)"Cannot await if then() has been called.");
        try {
            if (!this.zze.await(n, timeUnit)) {
                this.zzd(Status.zzd);
            }
        }
        catch (InterruptedException ex) {
            this.zzd(Status.zzb);
        }
        zzau.zza(this.zze(), (Object)"Result is not ready.");
        return this.zza();
    }
    
    @Override
    public boolean isCanceled() {
        synchronized (this.zza) {
            return this.zzl;
        }
    }
    
    @Override
    public final void setResultCallback(final ResultCallback<? super R> zzg) {
        final Object zza = this.zza;
        // monitorenter(zza)
        Label_0023: {
            if (zzg != null) {
                break Label_0023;
            }
            try {
                this.zzg = null;
            }
            // monitorexit(zza)
            finally {
                // monitorexit(zza)
                // monitorexit(zza)
                // iftrue(Label_0091:, !this.zze())
                // monitorexit(zza)
                // iftrue(Label_0069:, !this.isCanceled())
                // iftrue(Label_0049:, this.zzp != null)
            Label_0052_Outer:
                while (true) {
                    Block_6: {
                        while (true) {
                            while (true) {
                                return;
                                Label_0069: {
                                    break Block_6;
                                }
                                return;
                                final boolean b;
                                zzau.zza(b, (Object)"Cannot set callbacks if then() has been called.");
                                continue Label_0052_Outer;
                            }
                            Label_0049: {
                                final boolean b = false;
                            }
                            continue;
                            final boolean zzk = this.zzk;
                            final boolean b = true;
                            zzau.zza(zzk ^ true, (Object)"Result has already been consumed.");
                            continue;
                        }
                    }
                    this.zzb.zza(zzg, this.zza());
                    continue;
                    Label_0091: {
                        this.zzg = zzg;
                    }
                    continue;
                }
            }
        }
    }
    
    protected abstract R zza(final Status p0);
    
    @Override
    public final void zza(final PendingResult.zza zza) {
        zzau.zzb(zza != null, "Callback cannot be null.");
        synchronized (this.zza) {
            if (this.zze()) {
                zza.zza(this.zzj);
            }
            else {
                this.zzf.add(zza);
            }
        }
    }
    
    public final void zza(final R r) {
        synchronized (this.zza) {
            if (!this.zzm && !this.zzl) {
                this.zze();
                zzau.zza(this.zze() ^ true, (Object)"Results have already been set");
                zzau.zza(this.zzk ^ true, (Object)"Result has already been consumed");
                this.zzc(r);
                return;
            }
            zzb(r);
        }
    }
    
    public final void zzd(final Status status) {
        synchronized (this.zza) {
            if (!this.zze()) {
                this.zza(this.zza(status));
                this.zzm = true;
            }
        }
    }
    
    public final boolean zze() {
        return this.zze.getCount() == 0L;
    }
    
    public final void zzg() {
        this.zzq = (this.zzq || BasePendingResult.zzc.get());
    }
    
    public static final class zza<R extends Result> extends Handler
    {
        public zza() {
            this(Looper.getMainLooper());
        }
        
        public zza(final Looper looper) {
            super(looper);
        }
        
        public final void handleMessage(final Message message) {
            switch (message.what) {
                default: {
                    final int what = message.what;
                    final StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(what);
                    Log.wtf("BasePendingResult", sb.toString(), (Throwable)new Exception());
                }
                case 2: {
                    ((BasePendingResult)message.obj).zzd(Status.zzd);
                }
                case 1: {
                    final Pair pair = (Pair)message.obj;
                    final ResultCallback resultCallback = (ResultCallback)pair.first;
                    final Result result = (Result)pair.second;
                    try {
                        resultCallback.onResult(result);
                        return;
                    }
                    catch (RuntimeException ex) {
                        BasePendingResult.zzb(result);
                        throw ex;
                    }
                    break;
                }
            }
        }
        
        public final void zza(final ResultCallback<? super R> resultCallback, final R r) {
            this.sendMessage(this.obtainMessage(1, (Object)new Pair((Object)resultCallback, (Object)r)));
        }
    }
    
    final class zzb
    {
        @Override
        protected final void finalize() throws Throwable {
            BasePendingResult.zzb(BasePendingResult.this.zzi);
            super.finalize();
        }
    }
}
