package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.ConnectionResult;

final class zzq
{
    private final int zza;
    private final ConnectionResult zzb;
    
    zzq(final ConnectionResult zzb, final int zza) {
        zzau.zza(zzb);
        this.zzb = zzb;
        this.zza = zza;
    }
    
    final int zza() {
        return this.zza;
    }
    
    final ConnectionResult zzb() {
        return this.zzb;
    }
}
