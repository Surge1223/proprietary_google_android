package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.lang.ref.WeakReference;

public final class zza extends ActivityLifecycleObserver
{
    private final WeakReference<zza> zza;
    
    private zza(final zza zza) {
        this.zza = new WeakReference<zza>(zza);
    }
    
    static class zza extends LifecycleCallback
    {
        private List<Runnable> zza;
        
        @Override
        public final void zza() {
            synchronized (this) {
                final List<Runnable> zza = this.zza;
                this.zza = new ArrayList<Runnable>();
                // monitorexit(this)
                final Iterator<Runnable> iterator = zza.iterator();
                while (iterator.hasNext()) {
                    iterator.next().run();
                }
            }
        }
    }
}
