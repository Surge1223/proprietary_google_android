package com.google.android.gms.common.api.internal;

import android.net.Uri;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public final class zzby extends BroadcastReceiver
{
    private Context zza;
    private final zzbz zzb;
    
    public zzby(final zzbz zzb) {
        this.zzb = zzb;
    }
    
    public final void onReceive(final Context context, final Intent intent) {
        final Uri data = intent.getData();
        String schemeSpecificPart;
        if (data != null) {
            schemeSpecificPart = data.getSchemeSpecificPart();
        }
        else {
            schemeSpecificPart = null;
        }
        if ("com.google.android.gms".equals(schemeSpecificPart)) {
            this.zzb.zza();
            this.zza();
        }
    }
    
    public final void zza() {
        synchronized (this) {
            if (this.zza != null) {
                this.zza.unregisterReceiver((BroadcastReceiver)this);
            }
            this.zza = null;
        }
    }
    
    public final void zza(final Context zza) {
        this.zza = zza;
    }
}
