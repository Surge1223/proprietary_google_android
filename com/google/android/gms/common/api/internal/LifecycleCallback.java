package com.google.android.gms.common.api.internal;

import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.os.Bundle;
import android.content.Intent;
import android.app.Activity;
import android.support.annotation.Keep;

public class LifecycleCallback
{
    protected final zzch zzd;
    
    protected LifecycleCallback(final zzch zzd) {
        this.zzd = zzd;
    }
    
    @Keep
    private static zzch getChimeraLifecycleFragmentImpl(final zzcg zzcg) {
        throw new IllegalStateException("Method not available in SDK.");
    }
    
    public static zzch zzb(final Activity activity) {
        return zzb(new zzcg(activity));
    }
    
    protected static zzch zzb(final zzcg zzcg) {
        if (zzcg.zza()) {
            return zzdk.zza(zzcg.zzd());
        }
        if (zzcg.zzb()) {
            return zzci.zza(zzcg.zzc());
        }
        throw new IllegalArgumentException("Can't get fragment for unexpected activity.");
    }
    
    public void zza() {
    }
    
    public void zza(final int n, final int n2, final Intent intent) {
    }
    
    public void zza(final Bundle bundle) {
    }
    
    public void zza(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
    }
    
    public void zzb() {
    }
    
    public void zzb(final Bundle bundle) {
    }
    
    public void zze() {
    }
    
    public final Activity zzg() {
        return this.zzd.zza();
    }
    
    public void zzh() {
    }
}
