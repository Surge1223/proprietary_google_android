package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.TransactionTooLargeException;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.common.api.Status;
import android.os.RemoteException;

public abstract class zzb
{
    private final int zza;
    
    public zzb(final int zza) {
        this.zza = zza;
    }
    
    private static Status zzb(final RemoteException ex) {
        final StringBuilder sb = new StringBuilder();
        if (zzo.zza() && ex instanceof TransactionTooLargeException) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(ex.getLocalizedMessage());
        return new Status(8, sb.toString());
    }
    
    public abstract void zza(final Status p0);
    
    public abstract void zza(final zzaf p0, final boolean p1);
    
    public abstract void zza(final zzbp<?> p0) throws DeadObjectException;
}
