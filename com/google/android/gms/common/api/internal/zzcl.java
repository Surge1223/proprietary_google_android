package com.google.android.gms.common.api.internal;

public final class zzcl<L>
{
    private volatile L zzb;
    
    public final void zzb() {
        this.zzb = null;
    }
}
