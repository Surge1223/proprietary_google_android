package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;

public final class zzdt
{
    public static final Status zza;
    private static final BasePendingResult<?>[] zzc;
    
    static {
        zza = new Status(8, "The connection to Google Play services was lost");
        zzc = new BasePendingResult[0];
    }
}
