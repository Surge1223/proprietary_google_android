package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.GoogleApiClient;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.PendingResult;
import android.util.Log;
import android.os.Message;
import android.os.Handler;

final class zzds extends Handler
{
    private final /* synthetic */ zzdq zza;
    
    public final void handleMessage(Message zzd) {
        switch (zzd.what) {
            default: {
                final int what = zzd.what;
                final StringBuilder sb = new StringBuilder(70);
                sb.append("TransformationResultHandler received unknown message type: ");
                sb.append(what);
                Log.e("TransformedResultImpl", sb.toString());
            }
            case 1: {
                final RuntimeException ex = (RuntimeException)zzd.obj;
                final String value = String.valueOf(ex.getMessage());
                String concat;
                if (value.length() != 0) {
                    concat = "Runtime exception on the transformation worker thread: ".concat(value);
                }
                else {
                    concat = new String("Runtime exception on the transformation worker thread: ");
                }
                Log.e("TransformedResultImpl", concat);
                throw ex;
            }
            case 0: {
                final PendingResult pendingResult = (PendingResult)zzd.obj;
                zzd = (Message)this.zza.zze;
                // monitorenter(zzd)
                Label_0175: {
                    if (pendingResult != null) {
                        break Label_0175;
                    }
                Block_5_Outer:
                    while (true) {
                        try {
                            this.zza.zzb.zza(new Status(13, "Transform returned null"));
                            break Label_0213;
                        }
                        finally {
                            // monitorexit(zzd)
                            // monitorexit(zzd)
                            // iftrue(Label_0202:, !pendingResult instanceof zzdb)
                            while (true) {
                                this.zza.zzb.zza(((zzdb<?>)pendingResult).zza());
                                return;
                                continue;
                            }
                            Label_0202: {
                                this.zza.zzb.zza(pendingResult);
                            }
                            continue Block_5_Outer;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
}
