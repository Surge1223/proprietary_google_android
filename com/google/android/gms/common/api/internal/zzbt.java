package com.google.android.gms.common.api.internal;

import java.util.Collection;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import java.util.Iterator;
import com.google.android.gms.common.ConnectionResult;
import android.app.Application;
import android.util.Log;
import android.os.Message;
import com.google.android.gms.common.api.GoogleApi;
import android.os.HandlerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.support.v4.util.ArraySet;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Looper;
import android.os.Handler;
import java.util.Set;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import com.google.android.gms.common.api.Status;
import android.os.Handler$Callback;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzbt implements SignOutCallbacks
{
    final /* synthetic */ zzbp zza;
    
    zzbt(final zzbp zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onSignOutComplete() {
        this.zza.zza.zzq.post((Runnable)new zzbu(this));
    }
}
