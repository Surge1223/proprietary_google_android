package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ApiException;
import java.util.HashMap;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.Map;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.OnCompleteListener;

final class zzah implements OnCompleteListener<Object>
{
    private final /* synthetic */ TaskCompletionSource zza;
    private final /* synthetic */ zzaf zzb;
    
    zzah(final zzaf zzb, final TaskCompletionSource zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void onComplete(final Task<Object> task) {
        this.zzb.zzb.remove(this.zza);
    }
}
