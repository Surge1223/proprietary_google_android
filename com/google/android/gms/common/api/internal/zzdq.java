package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzau;
import android.util.Log;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.GoogleApiClient;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.Result;

public final class zzdq<R extends Result> extends TransformedResult<R> implements ResultCallback<R>
{
    private ResultTransform<? super R, ? extends Result> zza;
    private zzdq<? extends Result> zzb;
    private volatile ResultCallbacks<? super R> zzc;
    private PendingResult<R> zzd;
    private final Object zze;
    private Status zzf;
    private final WeakReference<GoogleApiClient> zzg;
    private final zzds zzh;
    private boolean zzi;
    
    private static void zza(final Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable)result).release();
            }
            catch (RuntimeException ex) {
                final String value = String.valueOf(result);
                final StringBuilder sb = new StringBuilder(18 + String.valueOf(value).length());
                sb.append("Unable to release ");
                sb.append(value);
                Log.w("TransformedResultImpl", sb.toString(), (Throwable)ex);
            }
        }
    }
    
    private final void zza(final Status zzf) {
        synchronized (this.zze) {
            this.zzb(this.zzf = zzf);
        }
    }
    
    private final void zzb() {
        if (this.zza == null && this.zzc == null) {
            return;
        }
        final GoogleApiClient googleApiClient = this.zzg.get();
        if (!this.zzi && this.zza != null && googleApiClient != null) {
            googleApiClient.zza(this);
            this.zzi = true;
        }
        if (this.zzf != null) {
            this.zzb(this.zzf);
            return;
        }
        if (this.zzd != null) {
            this.zzd.setResultCallback(this);
        }
    }
    
    private final void zzb(Status onFailure) {
        synchronized (this.zze) {
            if (this.zza != null) {
                onFailure = this.zza.onFailure(onFailure);
                zzau.zza(onFailure, "onFailure must not return null");
                this.zzb.zza(onFailure);
            }
            else if (this.zzc()) {
                this.zzc.onFailure(onFailure);
            }
        }
    }
    
    private final boolean zzc() {
        final GoogleApiClient googleApiClient = this.zzg.get();
        return this.zzc != null && googleApiClient != null;
    }
    
    @Override
    public final void onResult(final R r) {
        synchronized (this.zze) {
            if (r.getStatus().isSuccess()) {
                if (this.zza != null) {
                    zzda.zza().submit(new zzdr(this, r));
                }
                else if (this.zzc()) {
                    this.zzc.onSuccess((Object)r);
                }
            }
            else {
                this.zza(r.getStatus());
                zza(r);
            }
        }
    }
    
    public final void zza(final PendingResult<?> zzd) {
        synchronized (this.zze) {
            this.zzd = (PendingResult<R>)zzd;
            this.zzb();
        }
    }
}
