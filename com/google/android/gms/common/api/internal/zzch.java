package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;

public interface zzch
{
    void startActivityForResult(final Intent p0, final int p1);
    
    Activity zza();
    
     <T extends LifecycleCallback> T zza(final String p0, final Class<T> p1);
    
    void zza(final String p0, final LifecycleCallback p1);
}
