package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzego;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzax;
import com.google.android.gms.common.ConnectionResult;
import android.util.Log;
import com.google.android.gms.internal.zzegv;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.signin.zza;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import android.os.Handler;
import android.content.Context;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zzd;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzegn;

public final class zzdd extends zzegn implements ConnectionCallbacks, OnConnectionFailedListener
{
    private static Api.zza<? extends zzd, SignInOptions> zza;
    private final Context zzb;
    private final Handler zzc;
    private final Api.zza<? extends zzd, SignInOptions> zzd;
    private Set<Scope> zze;
    private ClientSettings zzf;
    private zzd zzg;
    private zzdg zzh;
    
    static {
        zzdd.zza = com.google.android.gms.signin.zza.zza;
    }
    
    public zzdd(final Context context, final Handler handler, final ClientSettings clientSettings) {
        this(context, handler, clientSettings, zzdd.zza);
    }
    
    public zzdd(final Context zzb, final Handler zzc, final ClientSettings clientSettings, final Api.zza<? extends zzd, SignInOptions> zzd) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzf = zzau.zza(clientSettings, "ClientSettings must not be null");
        this.zze = clientSettings.getRequiredScopes();
        this.zzd = zzd;
    }
    
    private final void zzb(final zzegv zzegv) {
        final ConnectionResult zza = zzegv.zza();
        if (zza.isSuccess()) {
            final zzax zzb = zzegv.zzb();
            final ConnectionResult zzb2 = zzb.zzb();
            if (!zzb2.isSuccess()) {
                final String value = String.valueOf(zzb2);
                final StringBuilder sb = new StringBuilder(48 + String.valueOf(value).length());
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(value);
                Log.wtf("SignInCoordinator", sb.toString(), (Throwable)new Exception());
                this.zzh.zza(zzb2);
                ((Api.zze)this.zzg).disconnect();
                return;
            }
            this.zzh.zza(zzb.zza(), this.zze);
        }
        else {
            this.zzh.zza(zza);
        }
        ((Api.zze)this.zzg).disconnect();
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        this.zzg.zza(this);
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zzh.zza(connectionResult);
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        ((Api.zze)this.zzg).disconnect();
    }
    
    public final void zza(final zzdg zzh) {
        if (this.zzg != null) {
            ((Api.zze)this.zzg).disconnect();
        }
        this.zzf.setClientSessionId(System.identityHashCode(this));
        this.zzg = (zzd)this.zzd.zza(this.zzb, this.zzc.getLooper(), this.zzf, this.zzf.getSignInOptions(), this, this);
        this.zzh = zzh;
        if (this.zze != null && !this.zze.isEmpty()) {
            this.zzg.zzd();
            return;
        }
        this.zzc.post((Runnable)new zzde(this));
    }
    
    @Override
    public final void zza(final zzegv zzegv) {
        this.zzc.post((Runnable)new zzdf(this, zzegv));
    }
    
    public final void zzb() {
        if (this.zzg != null) {
            ((Api.zze)this.zzg).disconnect();
        }
    }
}
