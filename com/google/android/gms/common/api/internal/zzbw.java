package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.BaseGmsClient;
import java.util.Collection;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import java.util.Iterator;
import android.app.Application;
import android.util.Log;
import android.os.Message;
import com.google.android.gms.common.api.GoogleApi;
import android.os.HandlerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.support.v4.util.ArraySet;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Looper;
import android.os.Handler;
import java.util.Set;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import com.google.android.gms.common.api.Status;
import android.os.Handler$Callback;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.Collections;
import com.google.android.gms.common.ConnectionResult;

final class zzbw implements Runnable
{
    private final /* synthetic */ ConnectionResult zza;
    private final /* synthetic */ zzbv zzb;
    
    zzbw(final zzbv zzb, final ConnectionResult zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        if (!this.zza.isSuccess()) {
            this.zzb.zza.zzm.get(this.zzb.zzc).onConnectionFailed(this.zza);
            return;
        }
        zzbv.zza(this.zzb, true);
        if (this.zzb.zzb.requiresSignIn()) {
            this.zzb.zza();
            return;
        }
        this.zzb.zzb.zza(null, Collections.emptySet());
    }
}
