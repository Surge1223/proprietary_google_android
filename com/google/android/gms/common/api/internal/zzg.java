package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzg extends zzc<Boolean>
{
    private final zzcn<?> zzb;
    
    public zzg(final zzcn<?> zzb, final TaskCompletionSource<Boolean> taskCompletionSource) {
        super(4, taskCompletionSource);
        this.zzb = zzb;
    }
    
    public final void zzb(final zzbp<?> zzbp) throws RemoteException {
        final zzcy zzcy = (zzcy)zzbp.zzc().remove(this.zzb);
        if (zzcy != null) {
            zzcy.zzb.zza(zzbp.zzb(), (TaskCompletionSource<Boolean>)this.zza);
            zzcy.zza.zzb();
            return;
        }
        this.zza.trySetResult((T)false);
    }
}
