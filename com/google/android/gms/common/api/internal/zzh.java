package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.api.Status;

public final class zzh implements zzdi
{
    @Override
    public final Exception zza(final Status status) {
        return zzb.zza(status);
    }
}
