package com.google.android.gms.common.api.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import com.google.android.gms.internal.zzbjn;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;

public final class zzda
{
    private static final ExecutorService zza;
    
    static {
        zza = new ThreadPoolExecutor(0, 4, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new zzbjn("GAC_Transform"));
    }
    
    public static ExecutorService zza() {
        return zzda.zza;
    }
}
