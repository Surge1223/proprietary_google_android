package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Api;

public final class zzbx<O extends Api.ApiOptions> extends zzal
{
    private final GoogleApi<O> zza;
    
    public zzbx(final GoogleApi<O> zza) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.zza = zza;
    }
    
    @Override
    public final <A extends Api.zzb, R extends Result, T extends zzn<R, A>> T zza(final T t) {
        return this.zza.zza(t);
    }
    
    @Override
    public final void zza(final zzdq zzdq) {
    }
    
    @Override
    public final void zzb(final zzdq zzdq) {
    }
    
    @Override
    public final Looper zzc() {
        return this.zza.zzf();
    }
}
