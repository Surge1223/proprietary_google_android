package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzak;
import java.util.Arrays;
import com.google.android.gms.common.api.Api;

public final class zzi<O extends Api.ApiOptions>
{
    private final boolean zza;
    private final int zzb;
    private final Api<O> zzc;
    private final O zzd;
    
    private zzi(final Api<O> zzc, final O zzd) {
        this.zza = false;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzb = Arrays.hashCode(new Object[] { this.zzc, this.zzd });
    }
    
    public static <O extends Api.ApiOptions> zzi<O> zza(final Api<O> api, final O o) {
        return new zzi<O>(api, o);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzi)) {
            return false;
        }
        final zzi zzi = (zzi)o;
        return !this.zza && !zzi.zza && zzak.zza(this.zzc, zzi.zzc) && zzak.zza(this.zzd, zzi.zzd);
    }
    
    @Override
    public final int hashCode() {
        return this.zzb;
    }
    
    public final String zza() {
        return this.zzc.zzd();
    }
}
