package com.google.android.gms.common.api;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.app.PendingIntent;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public final class Status extends zzbid implements Result, ReflectedParcelable
{
    public static final Parcelable.Creator<Status> CREATOR;
    public static final Status zza;
    public static final Status zzb;
    public static final Status zzc;
    public static final Status zzd;
    public static final Status zze;
    private static final Status zzf;
    private static final Status zzg;
    private final int zzh;
    private final int zzi;
    private final String zzj;
    private final PendingIntent zzk;
    
    static {
        zza = new Status(0);
        zzb = new Status(14);
        zzc = new Status(8);
        zzd = new Status(15);
        zze = new Status(16);
        zzf = new Status(17);
        zzg = new Status(18);
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    public Status(final int n) {
        this(n, null);
    }
    
    Status(final int zzh, final int zzi, final String zzj, final PendingIntent zzk) {
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
    }
    
    public Status(final int n, final String s) {
        this(1, n, s, null);
    }
    
    public Status(final int n, final String s, final PendingIntent pendingIntent) {
        this(1, n, s, pendingIntent);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof Status)) {
            return false;
        }
        final Status status = (Status)o;
        return this.zzh == status.zzh && this.zzi == status.zzi && zzak.zza(this.zzj, status.zzj) && zzak.zza(this.zzk, status.zzk);
    }
    
    @Override
    public final Status getStatus() {
        return this;
    }
    
    public final int getStatusCode() {
        return this.zzi;
    }
    
    public final String getStatusMessage() {
        return this.zzj;
    }
    
    public final boolean hasResolution() {
        return this.zzk != null;
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zzh, this.zzi, this.zzj, this.zzk });
    }
    
    public final boolean isSuccess() {
        return this.zzi <= 0;
    }
    
    @Override
    public final String toString() {
        return zzak.zza(this).zza("statusCode", this.zza()).zza("resolution", this.zzk).toString();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getStatusCode());
        zzbig.zza(parcel, 2, this.getStatusMessage(), false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzk, n, false);
        zzbig.zza(parcel, 1000, this.zzh);
        zzbig.zza(parcel, zza);
    }
    
    public final String zza() {
        if (this.zzj != null) {
            return this.zzj;
        }
        return CommonStatusCodes.getStatusCodeString(this.zzi);
    }
}
