package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.zzdd;
import android.os.Handler;
import com.google.android.gms.common.api.internal.zzbp;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.internal.zzdn;
import java.util.Set;
import android.accounts.Account;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import java.util.Collections;
import java.util.Collection;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.common.api.internal.zzai;
import com.google.android.gms.common.api.internal.zzbx;
import com.google.android.gms.common.internal.zzau;
import android.app.Activity;
import com.google.android.gms.common.api.internal.zzdi;
import android.os.Looper;
import com.google.android.gms.common.api.internal.zzi;
import android.content.Context;
import com.google.android.gms.common.api.internal.zzbn;

public class GoogleApi<O extends Api.ApiOptions>
{
    protected final zzbn zza;
    private final Context zzb;
    private final Api<O> zzc;
    private final O zzd;
    private final zzi<O> zze;
    private final Looper zzf;
    private final int zzg;
    private final GoogleApiClient zzh;
    private final zzdi zzi;
    
    public GoogleApi(final Activity activity, final Api<O> zzc, final O zzd, final zza zza) {
        zzau.zza(activity, "Null activity is not permitted.");
        zzau.zza(zzc, "Api must not be null.");
        zzau.zza(zza, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.zzb = activity.getApplicationContext();
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzf = zza.zzc;
        this.zze = com.google.android.gms.common.api.internal.zzi.zza(this.zzc, this.zzd);
        this.zzh = new zzbx<Object>(this);
        this.zza = zzbn.zza(this.zzb);
        this.zzg = this.zza.zzc();
        this.zzi = zza.zzb;
        zzai.zza(activity, this.zza, this.zze);
        this.zza.zza(this);
    }
    
    public GoogleApi(final Context context, final Api<O> api, final O o, final Looper looper, final zzdi zzdi) {
        this(context, (Api<Api.ApiOptions>)api, o, new zzd().zza(looper).zza(zzdi).zza());
    }
    
    public GoogleApi(final Context context, final Api<O> zzc, final O zzd, final zza zza) {
        zzau.zza(context, "Null context is not permitted.");
        zzau.zza(zzc, "Api must not be null.");
        zzau.zza(zza, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.zzb = context.getApplicationContext();
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzf = zza.zzc;
        this.zze = com.google.android.gms.common.api.internal.zzi.zza(this.zzc, this.zzd);
        this.zzh = new zzbx<Object>(this);
        this.zza = zzbn.zza(this.zzb);
        this.zzg = this.zza.zzc();
        this.zzi = zza.zzb;
        this.zza.zza(this);
    }
    
    private final <A extends Api.zzb, T extends zzn<? extends Result, A>> T zza(final int n, final T t) {
        t.zzg();
        this.zza.zza((GoogleApi<Api.ApiOptions>)this, n, (zzn<? extends Result, Api.zzb>)t);
        return t;
    }
    
    private final ClientSettings.zza zza() {
        final ClientSettings.zza zza = new ClientSettings.zza();
        Account account = null;
        Label_0071: {
            if (this.zzd instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) {
                final GoogleSignInAccount googleSignInAccount = ((Api.ApiOptions.HasGoogleSignInAccountOptions)this.zzd).getGoogleSignInAccount();
                if (googleSignInAccount != null) {
                    account = googleSignInAccount.getAccount();
                    break Label_0071;
                }
            }
            if (this.zzd instanceof Api.ApiOptions.HasAccountOptions) {
                account = ((Api.ApiOptions.HasAccountOptions)this.zzd).getAccount();
            }
            else {
                account = null;
            }
        }
        final ClientSettings.zza zza2 = zza.zza(account);
        if (this.zzd instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) {
            final GoogleSignInAccount googleSignInAccount2 = ((Api.ApiOptions.HasGoogleSignInAccountOptions)this.zzd).getGoogleSignInAccount();
            if (googleSignInAccount2 != null) {
                final Set<Scope> set = googleSignInAccount2.zzd();
                return zza2.zza(set).zzb(this.zzb.getClass().getName()).zza(this.zzb.getPackageName());
            }
        }
        final Set<Scope> set = Collections.emptySet();
        return zza2.zza(set).zzb(this.zzb.getClass().getName()).zza(this.zzb.getPackageName());
    }
    
    private final <TResult, A extends Api.zzb> Task<TResult> zza(final int n, final zzdn<A, TResult> zzdn) {
        final TaskCompletionSource<TResult> taskCompletionSource = new TaskCompletionSource<TResult>();
        this.zza.zza((GoogleApi<Api.ApiOptions>)this, n, (zzdn<Api.zzb, TResult>)zzdn, taskCompletionSource, this.zzi);
        return taskCompletionSource.getTask();
    }
    
    public GoogleApiClient asGoogleApiClient() {
        return this.zzh;
    }
    
    public Api.zze zza(final Looper looper, final zzbp<O> zzbp) {
        return (Api.zze)this.zzc.zzb().zza(this.zzb, looper, this.zza().zza(), this.zzd, zzbp, zzbp);
    }
    
    public zzdd zza(final Context context, final Handler handler) {
        return new zzdd(context, handler, this.zza().zza());
    }
    
    public final <A extends Api.zzb, T extends zzn<? extends Result, A>> T zza(final T t) {
        return this.zza(0, t);
    }
    
    public final <TResult, A extends Api.zzb> Task<TResult> zza(final zzdn<A, TResult> zzdn) {
        return this.zza(0, zzdn);
    }
    
    public final <TResult, A extends Api.zzb> Task<TResult> zzb(final zzdn<A, TResult> zzdn) {
        return this.zza(1, zzdn);
    }
    
    public final zzi<O> zzd() {
        return this.zze;
    }
    
    public final int zze() {
        return this.zzg;
    }
    
    public final Looper zzf() {
        return this.zzf;
    }
    
    public static final class zza
    {
        public static final zza zza;
        public final zzdi zzb;
        public final Looper zzc;
        
        static {
            zza = new zzd().zza();
        }
        
        private zza(final zzdi zzb, final Account account, final Looper zzc) {
            this.zzb = zzb;
            this.zzc = zzc;
        }
    }
}
