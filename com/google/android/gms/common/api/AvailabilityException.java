package com.google.android.gms.common.api;

import java.util.Iterator;
import android.text.TextUtils;
import java.util.ArrayList;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.zzi;
import android.support.v4.util.ArrayMap;

public class AvailabilityException extends Exception
{
    private final ArrayMap<zzi<?>, ConnectionResult> zza;
    
    public AvailabilityException(final ArrayMap<zzi<?>, ConnectionResult> zza) {
        this.zza = zza;
    }
    
    @Override
    public String getMessage() {
        final ArrayList<String> list = new ArrayList<String>();
        final Iterator<zzi<?>> iterator = this.zza.keySet().iterator();
        boolean b = true;
        while (iterator.hasNext()) {
            final zzi<?> zzi = iterator.next();
            final ConnectionResult connectionResult = this.zza.get(zzi);
            if (connectionResult.isSuccess()) {
                b = false;
            }
            final String zza = zzi.zza();
            final String value = String.valueOf(connectionResult);
            final StringBuilder sb = new StringBuilder(2 + String.valueOf(zza).length() + String.valueOf(value).length());
            sb.append(zza);
            sb.append(": ");
            sb.append(value);
            list.add(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        if (b) {
            sb2.append("None of the queried APIs are available. ");
        }
        else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join((CharSequence)"; ", (Iterable)list));
        return sb2.toString();
    }
}
