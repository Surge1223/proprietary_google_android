package com.google.android.gms.common.api;

import android.os.IInterface;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import android.accounts.Account;
import com.google.android.gms.common.internal.zzau;

public final class Api<O extends ApiOptions>
{
    private final zza<?, O> zza;
    private final zzh<?, O> zzb;
    private final zzf<?> zzc;
    private final zzi<?> zzd;
    private final String zze;
    
    public Api(final String zze, final zza<C, O> zza, final zzf<C> zzc) {
        zzau.zza(zza, "Cannot construct an Api with a null ClientBuilder");
        zzau.zza(zzc, "Cannot construct an Api with a null ClientKey");
        this.zze = zze;
        this.zza = zza;
        this.zzb = null;
        this.zzc = zzc;
        this.zzd = null;
    }
    
    public final zza<?, O> zzb() {
        zzau.zza(this.zza != null, (Object)"This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.zza;
    }
    
    public final zzc<?> zzc() {
        if (this.zzc != null) {
            return (zzc<?>)this.zzc;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }
    
    public final String zzd() {
        return this.zze;
    }
    
    public interface ApiOptions
    {
        public interface HasAccountOptions extends HasOptions, NotRequiredOptions
        {
            Account getAccount();
        }
        
        public interface HasGoogleSignInAccountOptions extends HasOptions
        {
            GoogleSignInAccount getGoogleSignInAccount();
        }
        
        public interface HasOptions extends ApiOptions
        {
        }
        
        public interface NotRequiredOptions extends ApiOptions
        {
        }
        
        public interface Optional extends HasOptions, NotRequiredOptions
        {
        }
    }
    
    public abstract static class zza<T extends zze, O> extends zzd<T, O>
    {
        public abstract T zza(final Context p0, final Looper p1, final ClientSettings p2, final O p3, final GoogleApiClient.ConnectionCallbacks p4, final GoogleApiClient.OnConnectionFailedListener p5);
    }
    
    public interface zzb
    {
    }
    
    public static class zzc<C extends zzb>
    {
    }
    
    public static class zzd<T extends zzb, O>
    {
    }
    
    public interface zze extends zzb
    {
        void connect(final BaseGmsClient.ConnectionProgressReportCallbacks p0);
        
        void disconnect();
        
        boolean isConnected();
        
        boolean isConnecting();
        
        void onUserSignOut(final BaseGmsClient.SignOutCallbacks p0);
        
        boolean requiresGooglePlayServices();
        
        boolean requiresSignIn();
        
        int zza();
        
        void zza(final IAccountAccessor p0, final Set<Scope> p1);
        
        String zzab();
    }
    
    public static final class zzf<C extends zze> extends zzc<C>
    {
    }
    
    public interface zzg<T extends IInterface> extends zzb
    {
    }
    
    public static class zzh<T extends zzg, O> extends zzd<T, O>
    {
    }
    
    public static final class zzi<C extends zzg> extends zzc<C>
    {
    }
}
