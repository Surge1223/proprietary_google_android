package com.google.android.gms.common.api;

import com.google.android.gms.common.internal.zzau;
import android.accounts.Account;
import com.google.android.gms.common.api.internal.zzh;
import android.os.Looper;
import com.google.android.gms.common.api.internal.zzdi;

public final class zzd
{
    private zzdi zza;
    private Looper zzb;
    
    public final GoogleApi.zza zza() {
        if (this.zza == null) {
            this.zza = new zzh();
        }
        if (this.zzb == null) {
            this.zzb = Looper.getMainLooper();
        }
        return new GoogleApi.zza(this.zza, null, this.zzb, null);
    }
    
    public final zzd zza(final Looper zzb) {
        zzau.zza(zzb, "Looper must not be null.");
        this.zzb = zzb;
        return this;
    }
    
    public final zzd zza(final zzdi zza) {
        zzau.zza(zza, "StatusExceptionMapper must not be null.");
        this.zza = zza;
        return this;
    }
}
