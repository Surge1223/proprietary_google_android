package com.google.android.gms.common.api;

import java.util.concurrent.TimeUnit;

public abstract class PendingResult<R extends Result>
{
    public abstract R await(final long p0, final TimeUnit p1);
    
    public abstract boolean isCanceled();
    
    public abstract void setResultCallback(final ResultCallback<? super R> p0);
    
    public void zza(final zza zza) {
        throw new UnsupportedOperationException();
    }
    
    public interface zza
    {
        void zza(final Status p0);
    }
}
