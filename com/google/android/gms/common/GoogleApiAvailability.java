package com.google.android.gms.common;

import android.os.Message;
import android.os.Looper;
import android.annotation.SuppressLint;
import android.os.Handler;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.zzch;
import android.content.Intent;
import android.app.Notification;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.base.R;
import android.app.Notification$Style;
import android.app.Notification$BigTextStyle;
import android.app.Notification$Builder;
import com.google.android.gms.common.util.zzi;
import android.util.Log;
import android.app.PendingIntent;
import android.support.v4.app.FragmentActivity;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.util.zzo;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.google.android.gms.common.api.internal.zzby;
import com.google.android.gms.common.api.internal.zzbz;
import android.util.TypedValue;
import com.google.android.gms.common.internal.zzg;
import android.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import com.google.android.gms.common.internal.zzf;
import android.view.View;
import android.app.AlertDialog$Builder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ProgressBar;
import android.app.Dialog;
import android.content.DialogInterface$OnCancelListener;
import android.app.Activity;

public class GoogleApiAvailability extends GoogleApiAvailabilityLight
{
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE;
    private static final Object zza;
    private static final GoogleApiAvailability zzb;
    private String zzc;
    
    static {
        zza = new Object();
        zzb = new GoogleApiAvailability();
        GOOGLE_PLAY_SERVICES_VERSION_CODE = GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }
    
    public static GoogleApiAvailability getInstance() {
        return GoogleApiAvailability.zzb;
    }
    
    public static Dialog zza(final Activity activity, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        final ProgressBar view = new ProgressBar((Context)activity, (AttributeSet)null, 16842874);
        view.setIndeterminate(true);
        view.setVisibility(0);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
        alertDialog$Builder.setView((View)view);
        alertDialog$Builder.setMessage((CharSequence)zzf.zzc((Context)activity, 18));
        alertDialog$Builder.setPositiveButton((CharSequence)"", (DialogInterface$OnClickListener)null);
        final AlertDialog create = alertDialog$Builder.create();
        zza(activity, (Dialog)create, "GooglePlayServicesUpdatingDialog", dialogInterface$OnCancelListener);
        return (Dialog)create;
    }
    
    static Dialog zza(final Context context, final int n, final zzg zzg, final DialogInterface$OnCancelListener onCancelListener) {
        AlertDialog$Builder alertDialog$Builder = null;
        if (n == 0) {
            return null;
        }
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            alertDialog$Builder = new AlertDialog$Builder(context, 5);
        }
        AlertDialog$Builder alertDialog$Builder2;
        if ((alertDialog$Builder2 = alertDialog$Builder) == null) {
            alertDialog$Builder2 = new AlertDialog$Builder(context);
        }
        alertDialog$Builder2.setMessage((CharSequence)zzf.zzc(context, n));
        if (onCancelListener != null) {
            alertDialog$Builder2.setOnCancelListener(onCancelListener);
        }
        final String zze = zzf.zze(context, n);
        if (zze != null) {
            alertDialog$Builder2.setPositiveButton((CharSequence)zze, (DialogInterface$OnClickListener)zzg);
        }
        final String zza = zzf.zza(context, n);
        if (zza != null) {
            alertDialog$Builder2.setTitle((CharSequence)zza);
        }
        return (Dialog)alertDialog$Builder2.create();
    }
    
    public static zzby zza(final Context context, final zzbz zzbz) {
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        final zzby zzby = new zzby(zzbz);
        context.registerReceiver((BroadcastReceiver)zzby, intentFilter);
        zzby.zza(context);
        if (!GooglePlayServicesUtilLight.zza(context, "com.google.android.gms")) {
            zzbz.zza();
            zzby.zza();
            return null;
        }
        return zzby;
    }
    
    private final String zza() {
        synchronized (GoogleApiAvailability.zza) {
            return this.zzc;
        }
    }
    
    @TargetApi(26)
    private final String zza(final Context context, final NotificationManager notificationManager) {
        zzau.zza(zzo.zzk());
        String zza;
        if ((zza = this.zza()) == null) {
            final String s = "com.google.android.gms.availability";
            final NotificationChannel notificationChannel = notificationManager.getNotificationChannel("com.google.android.gms.availability");
            final String zza2 = zzf.zza(context);
            if (notificationChannel == null) {
                notificationManager.createNotificationChannel(new NotificationChannel("com.google.android.gms.availability", (CharSequence)zza2, 4));
                zza = s;
            }
            else {
                zza = s;
                if (!zza2.equals(notificationChannel.getName())) {
                    notificationChannel.setName((CharSequence)zza2);
                    notificationManager.createNotificationChannel(notificationChannel);
                    zza = s;
                }
            }
        }
        return zza;
    }
    
    static void zza(final Activity activity, final Dialog dialog, final String s, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        if (activity instanceof FragmentActivity) {
            SupportErrorDialogFragment.newInstance(dialog, dialogInterface$OnCancelListener).show(((FragmentActivity)activity).getSupportFragmentManager(), s);
            return;
        }
        ErrorDialogFragment.newInstance(dialog, dialogInterface$OnCancelListener).show(activity.getFragmentManager(), s);
    }
    
    @TargetApi(20)
    private final void zza(final Context context, int n, final String s, final PendingIntent contentIntent) {
        if (n == 18) {
            this.zza(context);
            return;
        }
        if (contentIntent == null) {
            if (n == 6) {
                Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
            }
            return;
        }
        final String zzb = zzf.zzb(context, n);
        final String zzd = zzf.zzd(context, n);
        final Resources resources = context.getResources();
        final NotificationManager notificationManager = (NotificationManager)context.getSystemService("notification");
        Notification notification;
        if (zzi.zzb(context)) {
            zzau.zza(zzo.zzf());
            final Notification$Builder addAction = new Notification$Builder(context).setSmallIcon(context.getApplicationInfo().icon).setPriority(2).setAutoCancel(true).setContentTitle((CharSequence)zzb).setStyle((Notification$Style)new Notification$BigTextStyle().bigText((CharSequence)zzd)).addAction(R.drawable.common_full_open_on_phone, (CharSequence)resources.getString(R.string.common_open_on_phone), contentIntent);
            if (zzo.zzk() && zzo.zzk()) {
                addAction.setChannelId(this.zza(context, notificationManager));
            }
            notification = addAction.build();
        }
        else {
            final NotificationCompat.Builder setStyle = new NotificationCompat.Builder(context).setSmallIcon(17301642).setTicker(resources.getString(R.string.common_google_play_services_notification_ticker)).setWhen(System.currentTimeMillis()).setAutoCancel(true).setContentIntent(contentIntent).setContentTitle(zzb).setContentText(zzd).setLocalOnly(true).setStyle(new NotificationCompat.BigTextStyle().bigText(zzd));
            if (zzo.zzk() && zzo.zzk()) {
                setStyle.setChannelId(this.zza(context, notificationManager));
            }
            notification = setStyle.build();
        }
        switch (n) {
            default: {
                n = 39789;
                break;
            }
            case 1:
            case 2:
            case 3: {
                n = 10436;
                GooglePlayServicesUtilLight.zza.set(false);
                break;
            }
        }
        notificationManager.notify(n, notification);
    }
    
    @Override
    public int getApkVersion(final Context context) {
        return super.getApkVersion(context);
    }
    
    public Dialog getErrorDialog(final Activity activity, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        return zza((Context)activity, n, zzg.zza(activity, this.getErrorResolutionIntent((Context)activity, n, "d"), n2), dialogInterface$OnCancelListener);
    }
    
    @Override
    public Intent getErrorResolutionIntent(final Context context, final int n, final String s) {
        return super.getErrorResolutionIntent(context, n, s);
    }
    
    @Override
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2) {
        return super.getErrorResolutionPendingIntent(context, n, n2);
    }
    
    @Override
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2, final String s) {
        return super.getErrorResolutionPendingIntent(context, n, n2, s);
    }
    
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            return connectionResult.getResolution();
        }
        return this.getErrorResolutionPendingIntent(context, connectionResult.getErrorCode(), 0);
    }
    
    @Override
    public final String getErrorString(final int n) {
        return super.getErrorString(n);
    }
    
    @Override
    public int isGooglePlayServicesAvailable(final Context context) {
        return super.isGooglePlayServicesAvailable(context);
    }
    
    @Override
    public final boolean isUserResolvableError(final int n) {
        return super.isUserResolvableError(n);
    }
    
    public boolean showErrorDialogFragment(final Activity activity, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        final Dialog errorDialog = this.getErrorDialog(activity, n, n2, dialogInterface$OnCancelListener);
        if (errorDialog == null) {
            return false;
        }
        zza(activity, errorDialog, "GooglePlayServicesErrorDialog", dialogInterface$OnCancelListener);
        return true;
    }
    
    public void showErrorNotification(final Context context, final int n) {
        this.zza(context, n, null, this.getErrorResolutionPendingIntent(context, n, 0, "n"));
    }
    
    final void zza(final Context context) {
        new zza(context).sendEmptyMessageDelayed(1, 120000L);
    }
    
    public final boolean zza(final Activity activity, final zzch zzch, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        final Dialog zza = zza((Context)activity, n, zzg.zza(zzch, this.getErrorResolutionIntent((Context)activity, n, "d"), 2), dialogInterface$OnCancelListener);
        if (zza == null) {
            return false;
        }
        zza(activity, zza, "GooglePlayServicesErrorDialog", dialogInterface$OnCancelListener);
        return true;
    }
    
    public final boolean zza(final Context context, final ConnectionResult connectionResult, final int n) {
        final PendingIntent errorResolutionPendingIntent = this.getErrorResolutionPendingIntent(context, connectionResult);
        if (errorResolutionPendingIntent != null) {
            this.zza(context, connectionResult.getErrorCode(), null, GoogleApiActivity.zza(context, errorResolutionPendingIntent, n));
            return true;
        }
        return false;
    }
    
    @SuppressLint({ "HandlerLeak" })
    final class zza extends Handler
    {
        private final Context zza;
        
        public zza(final Context context) {
            Looper looper;
            if (Looper.myLooper() == null) {
                looper = Looper.getMainLooper();
            }
            else {
                looper = Looper.myLooper();
            }
            super(looper);
            this.zza = context.getApplicationContext();
        }
        
        public final void handleMessage(final Message message) {
            if (message.what != 1) {
                final int what = message.what;
                final StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(what);
                Log.w("GoogleApiAvailability", sb.toString());
            }
            else {
                final int googlePlayServicesAvailable = GoogleApiAvailability.this.isGooglePlayServicesAvailable(this.zza);
                if (GoogleApiAvailability.this.isUserResolvableError(googlePlayServicesAvailable)) {
                    GoogleApiAvailability.this.showErrorNotification(this.zza, googlePlayServicesAvailable);
                }
            }
        }
    }
}
