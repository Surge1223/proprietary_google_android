package com.google.android.gms.common.server.response;

import android.os.Parcelable;
import java.util.Set;
import android.os.Bundle;
import java.util.Iterator;
import com.google.android.gms.internal.zzbif;
import com.google.android.gms.common.util.zzb;
import java.math.BigInteger;
import java.math.BigDecimal;
import com.google.android.gms.internal.zzbie;
import android.util.SparseArray;
import java.util.Map;
import java.util.ArrayList;
import com.google.android.gms.common.util.zzc;
import com.google.android.gms.common.util.zzn;
import java.util.HashMap;
import com.google.android.gms.internal.zzbig;
import com.google.android.gms.common.internal.zzau;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class zzl extends FastSafeParcelableJsonResponse
{
    public static final Parcelable.Creator<zzl> CREATOR;
    private final int zza;
    private final Parcel zzb;
    private final int zzc;
    private final FieldMappingDictionary zzd;
    private final String zze;
    private int zzf;
    private int zzg;
    
    static {
        CREATOR = (Parcelable.Creator)new zzm();
    }
    
    zzl(final int zza, final Parcel parcel, final FieldMappingDictionary zzd) {
        this.zza = zza;
        this.zzb = zzau.zza(parcel);
        this.zzc = 2;
        this.zzd = zzd;
        if (this.zzd == null) {
            this.zze = null;
        }
        else {
            this.zze = this.zzd.getRootClassName();
        }
        this.zzf = 2;
    }
    
    private final Parcel zza() {
        switch (this.zzf) {
            case 0: {
                this.zzg = zzbig.zza(this.zzb);
            }
            case 1: {
                zzbig.zza(this.zzb, this.zzg);
                this.zzf = 2;
                break;
            }
        }
        return this.zzb;
    }
    
    private static void zza(StringBuilder sb, final int n, final Object o) {
        switch (n) {
            default: {
                sb = new StringBuilder(26);
                sb.append("Unknown type = ");
                sb.append(n);
                throw new IllegalArgumentException(sb.toString());
            }
            case 11: {
                throw new IllegalArgumentException("Method does not accept concrete type.");
            }
            case 10: {
                zzn.zza(sb, (HashMap<String, String>)o);
            }
            case 9: {
                sb.append("\"");
                sb.append(zzc.zzb((byte[])o));
                sb.append("\"");
            }
            case 8: {
                sb.append("\"");
                sb.append(zzc.zza((byte[])o));
                sb.append("\"");
            }
            case 7: {
                sb.append("\"");
                sb.append(com.google.android.gms.common.util.zzm.zzb(o.toString()));
                sb.append("\"");
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6: {
                sb.append(o);
            }
        }
    }
    
    private final void zza(final StringBuilder sb, final Field<?, ?> field, final Object o) {
        if (field.isTypeInArray()) {
            final ArrayList list = (ArrayList)o;
            sb.append("[");
            for (int size = list.size(), i = 0; i < size; ++i) {
                if (i != 0) {
                    sb.append(",");
                }
                zza(sb, field.getTypeIn(), list.get(i));
            }
            sb.append("]");
            return;
        }
        zza(sb, field.getTypeIn(), o);
    }
    
    private final void zza(final StringBuilder sb, final Map<String, Field<?, ?>> map, final Parcel parcel) {
        final SparseArray sparseArray = new SparseArray();
        for (final Map.Entry<String, Field<?, ?>> entry : map.entrySet()) {
            sparseArray.put(((Field)entry.getValue()).getSafeParcelableFieldId(), (Object)entry);
        }
        sb.append('{');
        final int zza = zzbie.zza(parcel);
        int n = 0;
        while (parcel.dataPosition() < zza) {
            final int int1 = parcel.readInt();
            final Map.Entry entry2 = (Map.Entry)sparseArray.get(0xFFFF & int1);
            if (entry2 != null) {
                if (n != 0) {
                    sb.append(",");
                }
                final String s = entry2.getKey();
                final Field<Object, Object> field = entry2.getValue();
                sb.append("\"");
                sb.append(s);
                sb.append("\":");
                if (field.hasConverter()) {
                    switch (field.getTypeOut()) {
                        default: {
                            final int typeOut = field.getTypeOut();
                            final StringBuilder sb2 = new StringBuilder(36);
                            sb2.append("Unknown field out type = ");
                            sb2.append(typeOut);
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        case 11: {
                            throw new IllegalArgumentException("Method does not accept concrete type.");
                        }
                        case 10: {
                            final Bundle zzs = zzbie.zzs(parcel, int1);
                            final HashMap<String, String> hashMap = new HashMap<String, String>();
                            for (final String s2 : zzs.keySet()) {
                                hashMap.put(s2, zzs.getString(s2));
                            }
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, hashMap));
                            break;
                        }
                        case 8:
                        case 9: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzt(parcel, int1)));
                            break;
                        }
                        case 7: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzq(parcel, int1)));
                            break;
                        }
                        case 6: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzc(parcel, int1)));
                            break;
                        }
                        case 5: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzp(parcel, int1)));
                            break;
                        }
                        case 4: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzn(parcel, int1)));
                            break;
                        }
                        case 3: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzl(parcel, int1)));
                            break;
                        }
                        case 2: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzi(parcel, int1)));
                            break;
                        }
                        case 1: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzk(parcel, int1)));
                            break;
                        }
                        case 0: {
                            this.zza(sb, (Field<?, ?>)field, this.getOriginalValue(field, zzbie.zzg(parcel, int1)));
                            break;
                        }
                    }
                }
                else if (field.isTypeOutArray()) {
                    sb.append("[");
                    final int typeOut2 = field.getTypeOut();
                    final BigDecimal[] array = null;
                    final BigInteger[] array2 = null;
                    Parcel[] array3 = null;
                    switch (typeOut2) {
                        default: {
                            throw new IllegalStateException("Unknown field type out.");
                        }
                        case 11: {
                            final int zza2 = zzbie.zza(parcel, int1);
                            final int dataPosition = parcel.dataPosition();
                            if (zza2 != 0) {
                                final int int2 = parcel.readInt();
                                array3 = new Parcel[int2];
                                for (int i = 0; i < int2; ++i) {
                                    final int int3 = parcel.readInt();
                                    if (int3 != 0) {
                                        final int dataPosition2 = parcel.dataPosition();
                                        final Parcel obtain = Parcel.obtain();
                                        obtain.appendFrom(parcel, dataPosition2, int3);
                                        array3[i] = obtain;
                                        parcel.setDataPosition(dataPosition2 + int3);
                                    }
                                    else {
                                        array3[i] = null;
                                    }
                                }
                                parcel.setDataPosition(dataPosition + zza2);
                            }
                            for (int length = array3.length, j = 0; j < length; ++j) {
                                if (j > 0) {
                                    sb.append(",");
                                }
                                array3[j].setDataPosition(0);
                                this.zza(sb, field.getConcreteTypeFieldMappingFromDictionary(), array3[j]);
                            }
                            break;
                        }
                        case 8:
                        case 9:
                        case 10: {
                            throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                        }
                        case 7: {
                            final String[] zzaa = zzbie.zzaa(parcel, int1);
                            for (int length2 = zzaa.length, k = 0; k < length2; ++k) {
                                if (k != 0) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(zzaa[k]);
                                sb.append("\"");
                            }
                            break;
                        }
                        case 6: {
                            final boolean[] zzv = zzbie.zzv(parcel, int1);
                            for (int length3 = zzv.length, l = 0; l < length3; ++l) {
                                if (l != 0) {
                                    sb.append(",");
                                }
                                sb.append(Boolean.toString(zzv[l]));
                            }
                            break;
                        }
                        case 5: {
                            final int zza3 = zzbie.zza(parcel, int1);
                            final int dataPosition3 = parcel.dataPosition();
                            BigDecimal[] array4;
                            if (zza3 == 0) {
                                array4 = array;
                            }
                            else {
                                final int int4 = parcel.readInt();
                                array4 = new BigDecimal[int4];
                                for (int n2 = 0; n2 < int4; ++n2) {
                                    array4[n2] = new BigDecimal(new BigInteger(parcel.createByteArray()), parcel.readInt());
                                }
                                parcel.setDataPosition(dataPosition3 + zza3);
                            }
                            com.google.android.gms.common.util.zzb.zza(sb, array4);
                            break;
                        }
                        case 4: {
                            final double[] zzz = zzbie.zzz(parcel, int1);
                            for (int length4 = zzz.length, n3 = 0; n3 < length4; ++n3) {
                                if (n3 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Double.toString(zzz[n3]));
                            }
                            break;
                        }
                        case 3: {
                            final float[] zzy = zzbie.zzy(parcel, int1);
                            for (int length5 = zzy.length, n4 = 0; n4 < length5; ++n4) {
                                if (n4 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Float.toString(zzy[n4]));
                            }
                            break;
                        }
                        case 2: {
                            final long[] zzx = zzbie.zzx(parcel, int1);
                            for (int length6 = zzx.length, n5 = 0; n5 < length6; ++n5) {
                                if (n5 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Long.toString(zzx[n5]));
                            }
                            break;
                        }
                        case 1: {
                            final int zza4 = zzbie.zza(parcel, int1);
                            final int dataPosition4 = parcel.dataPosition();
                            BigInteger[] array5;
                            if (zza4 == 0) {
                                array5 = array2;
                            }
                            else {
                                final int int5 = parcel.readInt();
                                array5 = new BigInteger[int5];
                                for (int n6 = 0; n6 < int5; ++n6) {
                                    array5[n6] = new BigInteger(parcel.createByteArray());
                                }
                                parcel.setDataPosition(dataPosition4 + zza4);
                            }
                            com.google.android.gms.common.util.zzb.zza(sb, array5);
                            break;
                        }
                        case 0: {
                            final int[] zzw = zzbie.zzw(parcel, int1);
                            for (int length7 = zzw.length, n7 = 0; n7 < length7; ++n7) {
                                if (n7 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Integer.toString(zzw[n7]));
                            }
                            break;
                        }
                    }
                    sb.append("]");
                }
                else {
                    switch (field.getTypeOut()) {
                        default: {
                            throw new IllegalStateException("Unknown field type out");
                        }
                        case 11: {
                            final Parcel zzad = zzbie.zzad(parcel, int1);
                            zzad.setDataPosition(0);
                            this.zza(sb, field.getConcreteTypeFieldMappingFromDictionary(), zzad);
                            break;
                        }
                        case 10: {
                            final Bundle zzs2 = zzbie.zzs(parcel, int1);
                            final Set keySet = zzs2.keySet();
                            keySet.size();
                            sb.append("{");
                            final Iterator<String> iterator3 = keySet.iterator();
                            int n8 = 1;
                            while (iterator3.hasNext()) {
                                final String s3 = iterator3.next();
                                if (n8 == 0) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(s3);
                                sb.append("\"");
                                sb.append(":");
                                sb.append("\"");
                                sb.append(com.google.android.gms.common.util.zzm.zzb(zzs2.getString(s3)));
                                sb.append("\"");
                                n8 = 0;
                            }
                            sb.append("}");
                            break;
                        }
                        case 9: {
                            final byte[] zzt = zzbie.zzt(parcel, int1);
                            sb.append("\"");
                            sb.append(com.google.android.gms.common.util.zzc.zzb(zzt));
                            sb.append("\"");
                            break;
                        }
                        case 8: {
                            final byte[] zzt2 = zzbie.zzt(parcel, int1);
                            sb.append("\"");
                            sb.append(com.google.android.gms.common.util.zzc.zza(zzt2));
                            sb.append("\"");
                            break;
                        }
                        case 7: {
                            final String zzq = zzbie.zzq(parcel, int1);
                            sb.append("\"");
                            sb.append(com.google.android.gms.common.util.zzm.zzb(zzq));
                            sb.append("\"");
                            break;
                        }
                        case 6: {
                            sb.append(zzbie.zzc(parcel, int1));
                            break;
                        }
                        case 5: {
                            sb.append(zzbie.zzp(parcel, int1));
                            break;
                        }
                        case 4: {
                            sb.append(zzbie.zzn(parcel, int1));
                            break;
                        }
                        case 3: {
                            sb.append(zzbie.zzl(parcel, int1));
                            break;
                        }
                        case 2: {
                            sb.append(zzbie.zzi(parcel, int1));
                            break;
                        }
                        case 1: {
                            sb.append(zzbie.zzk(parcel, int1));
                            break;
                        }
                        case 0: {
                            sb.append(zzbie.zzg(parcel, int1));
                            break;
                        }
                    }
                }
                n = 1;
            }
        }
        if (parcel.dataPosition() == zza) {
            sb.append('}');
            return;
        }
        final StringBuilder sb3 = new StringBuilder(37);
        sb3.append("Overread allowed size end=");
        sb3.append(zza);
        throw new zzbif(sb3.toString(), parcel);
    }
    
    @Override
    public Map<String, Field<?, ?>> getFieldMappings() {
        if (this.zzd == null) {
            return null;
        }
        return this.zzd.getFieldMapping(this.zze);
    }
    
    @Override
    public Object getValueObject(final String s) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }
    
    @Override
    public boolean isPrimitiveFieldSet(final String s) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }
    
    @Override
    public String toString() {
        zzau.zza(this.zzd, "Cannot convert to JSON on client side.");
        final Parcel zza = this.zza();
        zza.setDataPosition(0);
        final StringBuilder sb = new StringBuilder(100);
        this.zza(sb, this.zzd.getFieldMapping(this.zze), zza);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zzc) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zza(), false);
        Object o = null;
        switch (this.zzc) {
            default: {
                zzc = this.zzc;
                final StringBuilder sb = new StringBuilder(34);
                sb.append("Invalid creation type: ");
                sb.append(zzc);
                throw new IllegalStateException(sb.toString());
            }
            case 2: {
                o = this.zzd;
                break;
            }
            case 1: {
                o = this.zzd;
                break;
            }
            case 0: {
                o = null;
                break;
            }
        }
        zzbig.zza(parcel, 3, (Parcelable)o, zzc, false);
        zzbig.zza(parcel, zza);
    }
}
