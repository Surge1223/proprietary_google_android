package com.google.android.gms.common.server.response;

import com.google.android.gms.internal.zzbjg;
import com.google.android.gms.internal.zzbie;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class FieldCreator implements Parcelable.Creator<FastJsonResponse.Field>
{
    public FastJsonResponse.Field createFromParcel(final Parcel parcel) {
        final int zza = zzbie.zza(parcel);
        Object zzq = null;
        Object o;
        String zzq2 = (String)(o = zzq);
        int zzg2;
        int zzg = zzg2 = 0;
        int zzg3;
        int zzc = zzg3 = zzg2;
        int zzg4;
        int zzc2 = zzg4 = zzg3;
        while (parcel.dataPosition() < zza) {
            final int int1 = parcel.readInt();
            switch (0xFFFF & int1) {
                default: {
                    zzbie.zzb(parcel, int1);
                    continue;
                }
                case 9: {
                    o = zzbie.zza(parcel, int1, zzbjg.CREATOR);
                    continue;
                }
                case 8: {
                    zzq2 = zzbie.zzq(parcel, int1);
                    continue;
                }
                case 7: {
                    zzg4 = zzbie.zzg(parcel, int1);
                    continue;
                }
                case 6: {
                    zzq = zzbie.zzq(parcel, int1);
                    continue;
                }
                case 5: {
                    zzc2 = (zzbie.zzc(parcel, int1) ? 1 : 0);
                    continue;
                }
                case 4: {
                    zzg3 = zzbie.zzg(parcel, int1);
                    continue;
                }
                case 3: {
                    zzc = (zzbie.zzc(parcel, int1) ? 1 : 0);
                    continue;
                }
                case 2: {
                    zzg2 = zzbie.zzg(parcel, int1);
                    continue;
                }
                case 1: {
                    zzg = zzbie.zzg(parcel, int1);
                    continue;
                }
            }
        }
        zzbie.zzae(parcel, zza);
        return new FastJsonResponse.Field(zzg, zzg2, zzc != 0, zzg3, zzc2 != 0, (String)zzq, zzg4, zzq2, (zzbjg)o);
    }
    
    public FastJsonResponse.Field[] newArray(final int n) {
        return new FastJsonResponse.Field[n];
    }
}
