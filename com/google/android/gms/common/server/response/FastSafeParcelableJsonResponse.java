package com.google.android.gms.common.server.response;

import java.util.Iterator;
import com.google.android.gms.internal.zzbih;

public abstract class FastSafeParcelableJsonResponse extends FastJsonResponse implements zzbih
{
    public final int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!this.getClass().isInstance(o)) {
            return false;
        }
        final FastJsonResponse fastJsonResponse = (FastJsonResponse)o;
        for (final Field<?, ?> field : this.getFieldMappings().values()) {
            if (this.isFieldSet(field)) {
                if (!fastJsonResponse.isFieldSet(field)) {
                    return false;
                }
                if (!this.getFieldValue(field).equals(fastJsonResponse.getFieldValue(field))) {
                    return false;
                }
                continue;
            }
            else {
                if (fastJsonResponse.isFieldSet(field)) {
                    return false;
                }
                continue;
            }
        }
        return true;
    }
    
    public Object getValueObject(final String s) {
        return null;
    }
    
    @Override
    public int hashCode() {
        final Iterator<Field<?, ?>> iterator = this.getFieldMappings().values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Field<?, ?> field = iterator.next();
            int n2 = n;
            if (this.isFieldSet(field)) {
                n2 = n * 31 + this.getFieldValue(field).hashCode();
            }
            n = n2;
        }
        return n;
    }
    
    public boolean isPrimitiveFieldSet(final String s) {
        return false;
    }
}
