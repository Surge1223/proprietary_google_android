package com.google.android.gms.common.server.response;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzam;
import com.google.android.gms.common.internal.zzak;
import com.google.android.gms.internal.zzbjg;
import com.google.android.gms.internal.zzbid;
import java.util.Iterator;
import com.google.android.gms.common.util.zzc;
import com.google.android.gms.common.util.zzn;
import java.util.ArrayList;
import com.google.android.gms.common.internal.zzau;
import java.util.Map;
import java.util.HashMap;
import com.google.android.gms.common.util.zzm;

public abstract class FastJsonResponse
{
    private static void zza(final StringBuilder sb, final Field field, final Object o) {
        if (field.getTypeIn() == 11) {
            sb.append(((FastJsonResponse)field.getConcreteType().cast(o)).toString());
            return;
        }
        if (field.getTypeIn() == 7) {
            sb.append("\"");
            sb.append(zzm.zzb((String)o));
            sb.append("\"");
            return;
        }
        sb.append(o);
    }
    
    public HashMap<String, Object> getConcreteTypeArrays() {
        return null;
    }
    
    public HashMap<String, Object> getConcreteTypes() {
        return null;
    }
    
    public abstract Map<String, Field<?, ?>> getFieldMappings();
    
    protected Object getFieldValue(final Field field) {
        final String outputFieldName = field.getOutputFieldName();
        if (field.getConcreteType() != null) {
            zzau.zza(this.getValueObject(field.getOutputFieldName()) == null, "Concrete field shouldn't be value object: %s", field.getOutputFieldName());
            HashMap<String, Object> hashMap;
            if (field.isTypeOutArray()) {
                hashMap = this.getConcreteTypeArrays();
            }
            else {
                hashMap = this.getConcreteTypes();
            }
            if (hashMap != null) {
                return hashMap.get(outputFieldName);
            }
            try {
                final char upperCase = Character.toUpperCase(outputFieldName.charAt(0));
                final String substring = outputFieldName.substring(1);
                final StringBuilder sb = new StringBuilder(4 + String.valueOf(substring).length());
                sb.append("get");
                sb.append(upperCase);
                sb.append(substring);
                return this.getClass().getMethod(sb.toString(), (Class<?>[])new Class[0]).invoke(this, new Object[0]);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        return this.getValueObject(field.getOutputFieldName());
    }
    
    protected <O, I> I getOriginalValue(final Field<I, O> field, final Object o) {
        if (((Field<Object, Object>)field).zzc != null) {
            return field.convertBack(o);
        }
        return (I)o;
    }
    
    protected abstract Object getValueObject(final String p0);
    
    protected boolean isConcreteTypeArrayFieldSet(final String s) {
        throw new UnsupportedOperationException("Concrete type arrays not supported");
    }
    
    protected boolean isConcreteTypeFieldSet(final String s) {
        throw new UnsupportedOperationException("Concrete types not supported");
    }
    
    protected boolean isFieldSet(final Field field) {
        if (field.getTypeOut() != 11) {
            return this.isPrimitiveFieldSet(field.getOutputFieldName());
        }
        if (field.isTypeOutArray()) {
            return this.isConcreteTypeArrayFieldSet(field.getOutputFieldName());
        }
        return this.isConcreteTypeFieldSet(field.getOutputFieldName());
    }
    
    protected abstract boolean isPrimitiveFieldSet(final String p0);
    
    @Override
    public String toString() {
        final Map<String, Field<?, ?>> fieldMappings = this.getFieldMappings();
        final StringBuilder sb = new StringBuilder(100);
        for (final String s : fieldMappings.keySet()) {
            final Field<ArrayList<Object>, Object> field = fieldMappings.get(s);
            if (this.isFieldSet(field)) {
                final ArrayList<Object> originalValue = this.getOriginalValue(field, this.getFieldValue(field));
                if (sb.length() == 0) {
                    sb.append("{");
                }
                else {
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(s);
                sb.append("\":");
                if (originalValue == null) {
                    sb.append("null");
                }
                else {
                    switch (field.getTypeOut()) {
                        default: {
                            if (field.isTypeInArray()) {
                                final ArrayList<Object> list = originalValue;
                                sb.append("[");
                                for (int i = 0; i < list.size(); ++i) {
                                    if (i > 0) {
                                        sb.append(",");
                                    }
                                    final Object value = list.get(i);
                                    if (value != null) {
                                        zza(sb, field, value);
                                    }
                                }
                                sb.append("]");
                                continue;
                            }
                            zza(sb, field, originalValue);
                            continue;
                        }
                        case 10: {
                            zzn.zza(sb, (HashMap<String, String>)originalValue);
                            continue;
                        }
                        case 9: {
                            sb.append("\"");
                            sb.append(zzc.zzb((byte[])(Object)originalValue));
                            sb.append("\"");
                            continue;
                        }
                        case 8: {
                            sb.append("\"");
                            sb.append(zzc.zza((byte[])(Object)originalValue));
                            sb.append("\"");
                            continue;
                        }
                    }
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        }
        else {
            sb.append("{}");
        }
        return sb.toString();
    }
    
    public static class Field<I, O> extends zzbid
    {
        public static final FieldCreator CREATOR;
        protected final Class<? extends FastJsonResponse> mConcreteType;
        protected final String mConcreteTypeName;
        protected final String mOutputFieldName;
        protected final int mSafeParcelableFieldId;
        protected final int mTypeIn;
        protected final boolean mTypeInArray;
        protected final int mTypeOut;
        protected final boolean mTypeOutArray;
        private final int zza;
        private FieldMappingDictionary zzb;
        private FieldConverter<I, O> zzc;
        
        static {
            CREATOR = new FieldCreator();
        }
        
        Field(final int zza, final int mTypeIn, final boolean mTypeInArray, final int mTypeOut, final boolean mTypeOutArray, final String mOutputFieldName, final int mSafeParcelableFieldId, final String mConcreteTypeName, final zzbjg zzbjg) {
            this.zza = zza;
            this.mTypeIn = mTypeIn;
            this.mTypeInArray = mTypeInArray;
            this.mTypeOut = mTypeOut;
            this.mTypeOutArray = mTypeOutArray;
            this.mOutputFieldName = mOutputFieldName;
            this.mSafeParcelableFieldId = mSafeParcelableFieldId;
            if (mConcreteTypeName == null) {
                this.mConcreteType = null;
                this.mConcreteTypeName = null;
            }
            else {
                this.mConcreteType = zzl.class;
                this.mConcreteTypeName = mConcreteTypeName;
            }
            if (zzbjg == null) {
                this.zzc = null;
                return;
            }
            this.zzc = (FieldConverter<I, O>)zzbjg.zza();
        }
        
        private final String zza() {
            if (this.mConcreteTypeName == null) {
                return null;
            }
            return this.mConcreteTypeName;
        }
        
        private final zzbjg zzb() {
            if (this.zzc == null) {
                return null;
            }
            return zzbjg.zza(this.zzc);
        }
        
        public I convertBack(final O o) {
            return this.zzc.convertBack(o);
        }
        
        public Class<? extends FastJsonResponse> getConcreteType() {
            return this.mConcreteType;
        }
        
        public Map<String, Field<?, ?>> getConcreteTypeFieldMappingFromDictionary() {
            zzau.zza(this.mConcreteTypeName);
            zzau.zza(this.zzb);
            return this.zzb.getFieldMapping(this.mConcreteTypeName);
        }
        
        public String getOutputFieldName() {
            return this.mOutputFieldName;
        }
        
        public int getSafeParcelableFieldId() {
            return this.mSafeParcelableFieldId;
        }
        
        public int getTypeIn() {
            return this.mTypeIn;
        }
        
        public int getTypeOut() {
            return this.mTypeOut;
        }
        
        public int getVersionCode() {
            return this.zza;
        }
        
        public boolean hasConverter() {
            return this.zzc != null;
        }
        
        public boolean isTypeInArray() {
            return this.mTypeInArray;
        }
        
        public boolean isTypeOutArray() {
            return this.mTypeOutArray;
        }
        
        public void setFieldMappingDictionary(final FieldMappingDictionary zzb) {
            this.zzb = zzb;
        }
        
        @Override
        public String toString() {
            final zzam zza = zzak.zza(this).zza("versionCode", this.zza).zza("typeIn", this.mTypeIn).zza("typeInArray", this.mTypeInArray).zza("typeOut", this.mTypeOut).zza("typeOutArray", this.mTypeOutArray).zza("outputFieldName", this.mOutputFieldName).zza("safeParcelFieldId", this.mSafeParcelableFieldId).zza("concreteTypeName", this.zza());
            final Class<? extends FastJsonResponse> concreteType = this.getConcreteType();
            if (concreteType != null) {
                zza.zza("concreteType.class", concreteType.getCanonicalName());
            }
            if (this.zzc != null) {
                zza.zza("converterName", this.zzc.getClass().getCanonicalName());
            }
            return zza.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.getVersionCode());
            zzbig.zza(parcel, 2, this.getTypeIn());
            zzbig.zza(parcel, 3, this.isTypeInArray());
            zzbig.zza(parcel, 4, this.getTypeOut());
            zzbig.zza(parcel, 5, this.isTypeOutArray());
            zzbig.zza(parcel, 6, this.getOutputFieldName(), false);
            zzbig.zza(parcel, 7, this.getSafeParcelableFieldId());
            zzbig.zza(parcel, 8, this.zza(), false);
            zzbig.zza(parcel, 9, (Parcelable)this.zzb(), n, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public interface FieldConverter<I, O>
    {
        I convertBack(final O p0);
    }
}
