package com.google.android.gms.common.server.response;

import java.util.List;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import com.google.android.gms.common.internal.zzau;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FieldMappingDictionary extends zzbid
{
    public static final Parcelable.Creator<FieldMappingDictionary> CREATOR;
    private final int zza;
    private final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> zzb;
    private final ArrayList<Entry> zzc;
    private final String zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zzj();
    }
    
    FieldMappingDictionary(int i, final ArrayList<Entry> list, final String s) {
        this.zza = i;
        this.zzc = null;
        final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> zzb = new HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>>();
        int size;
        Entry entry;
        String zza;
        HashMap<String, FastJsonResponse.Field<?, ?>> hashMap;
        int size2;
        int j;
        FieldMapPair fieldMapPair;
        for (size = list.size(), i = 0; i < size; ++i) {
            entry = list.get(i);
            zza = entry.zza;
            hashMap = new HashMap<String, FastJsonResponse.Field<?, ?>>();
            for (size2 = entry.zzb.size(), j = 0; j < size2; ++j) {
                fieldMapPair = (FieldMapPair)entry.zzb.get(j);
                hashMap.put(fieldMapPair.zza, fieldMapPair.zzb);
            }
            zzb.put(zza, hashMap);
        }
        this.zzb = zzb;
        this.zzd = zzau.zza(s);
        this.linkFields();
    }
    
    public Map<String, FastJsonResponse.Field<?, ?>> getFieldMapping(final String s) {
        return this.zzb.get(s);
    }
    
    public String getRootClassName() {
        return this.zzd;
    }
    
    public void linkFields() {
        final Iterator<String> iterator = this.zzb.keySet().iterator();
        while (iterator.hasNext()) {
            final Map<String, FastJsonResponse.Field<?, ?>> map = this.zzb.get(iterator.next());
            final Iterator<String> iterator2 = map.keySet().iterator();
            while (iterator2.hasNext()) {
                ((FastJsonResponse.Field)map.get(iterator2.next())).setFieldMappingDictionary(this);
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (final String s : this.zzb.keySet()) {
            sb.append(s);
            sb.append(":\n");
            final Map<String, FastJsonResponse.Field<?, ?>> map = this.zzb.get(s);
            for (final String s2 : map.keySet()) {
                sb.append("  ");
                sb.append(s2);
                sb.append(": ");
                sb.append(map.get(s2));
            }
        }
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        final ArrayList<Parcelable> list = new ArrayList<Parcelable>();
        for (final String s : this.zzb.keySet()) {
            list.add((Parcelable)new Entry(s, this.zzb.get(s)));
        }
        zzbig.zzc(parcel, 2, list, false);
        zzbig.zza(parcel, 3, this.getRootClassName(), false);
        zzbig.zza(parcel, zza);
    }
    
    public static class Entry extends zzbid
    {
        public static final Parcelable.Creator<Entry> CREATOR;
        final String zza;
        final ArrayList<FieldMapPair> zzb;
        private final int zzc;
        
        static {
            CREATOR = (Parcelable.Creator)new zzk();
        }
        
        Entry(final int zzc, final String zza, final ArrayList<FieldMapPair> zzb) {
            this.zzc = zzc;
            this.zza = zza;
            this.zzb = zzb;
        }
        
        Entry(final String zza, final Map<String, FastJsonResponse.Field<?, ?>> map) {
            this.zzc = 1;
            this.zza = zza;
            ArrayList<FieldMapPair> zzb;
            if (map == null) {
                zzb = null;
            }
            else {
                zzb = new ArrayList<FieldMapPair>();
                for (final String s : map.keySet()) {
                    zzb.add(new FieldMapPair(s, map.get(s)));
                }
            }
            this.zzb = zzb;
        }
        
        public void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zzc);
            zzbig.zza(parcel, 2, this.zza, false);
            zzbig.zzc(parcel, 3, this.zzb, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class FieldMapPair extends zzbid
    {
        public static final Parcelable.Creator<FieldMapPair> CREATOR;
        final String zza;
        final FastJsonResponse.Field<?, ?> zzb;
        private final int zzc;
        
        static {
            CREATOR = (Parcelable.Creator)new zzi();
        }
        
        FieldMapPair(final int zzc, final String zza, final FastJsonResponse.Field<?, ?> zzb) {
            this.zzc = zzc;
            this.zza = zza;
            this.zzb = zzb;
        }
        
        FieldMapPair(final String zza, final FastJsonResponse.Field<?, ?> zzb) {
            this.zzc = 1;
            this.zza = zza;
            this.zzb = zzb;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zzc);
            zzbig.zza(parcel, 2, this.zza, false);
            zzbig.zza(parcel, 3, (Parcelable)this.zzb, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
