package com.google.android.gms.common.server;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public class FavaDiagnosticsEntity extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<FavaDiagnosticsEntity> CREATOR;
    private final int zza;
    private final String zzb;
    private final int zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zza();
    }
    
    public FavaDiagnosticsEntity(final int zza, final String zzb, final int zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, zza);
    }
}
