package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;

final class zzao implements zzat
{
    @Override
    public final ApiException zza(final Status status) {
        return zzb.zza(status);
    }
}
