package com.google.android.gms.common.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzfa;
import android.accounts.Account;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzw extends zzey implements IAccountAccessor
{
    zzw(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.IAccountAccessor");
    }
    
    @Override
    public final Account getAccount() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final Account account = zzfa.zza(zza, (android.os.Parcelable.Creator<Account>)Account.CREATOR);
        zza.recycle();
        return account;
    }
}
