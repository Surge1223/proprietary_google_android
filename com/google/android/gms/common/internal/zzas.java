package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.Result;

public interface zzas<R extends Result, T>
{
    T zza(final R p0);
}
