package com.google.android.gms.common.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.PendingResult;

public final class zzan
{
    private static final zzat zza;
    
    static {
        zza = new zzao();
    }
    
    public static <R extends Result> Task<Void> zza(final PendingResult<R> pendingResult) {
        return zza(pendingResult, (zzas<R, Void>)new zzar());
    }
    
    public static <R extends Result, T> Task<T> zza(final PendingResult<R> pendingResult, final zzas<R, T> zzas) {
        final zzat zza = zzan.zza;
        final TaskCompletionSource<T> taskCompletionSource = new TaskCompletionSource<T>();
        pendingResult.zza((PendingResult.zza)new zzap(pendingResult, taskCompletionSource, zzas, zza));
        return taskCompletionSource.getTask();
    }
}
