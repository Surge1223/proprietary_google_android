package com.google.android.gms.common.internal;

public final class zzu
{
    private final String zza;
    private final String zzb;
    private final int zzc;
    private final boolean zzd;
    
    public zzu(final String zzb, final String zza, final boolean zzd, final int zzc) {
        this.zzb = zzb;
        this.zza = zza;
        this.zzd = zzd;
        this.zzc = zzc;
    }
    
    final String zza() {
        return this.zza;
    }
    
    final String zzb() {
        return this.zzb;
    }
    
    final int zzc() {
        return this.zzc;
    }
}
