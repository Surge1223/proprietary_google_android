package com.google.android.gms.common.internal;

import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzfa;
import android.os.Parcel;
import com.google.android.gms.internal.zzez;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface IGmsCallbacks extends IInterface
{
    void onAccountValidationComplete(final int p0, final Bundle p1) throws RemoteException;
    
    void onPostInitComplete(final int p0, final IBinder p1, final Bundle p2) throws RemoteException;
    
    public abstract static class zza extends zzez implements IGmsCallbacks
    {
        public zza() {
            this.attachInterface((IInterface)this, "com.google.android.gms.common.internal.IGmsCallbacks");
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (this.zza(n, parcel, parcel2, n2)) {
                return true;
            }
            switch (n) {
                default: {
                    return false;
                }
                case 2: {
                    this.onAccountValidationComplete(parcel.readInt(), zzfa.zza(parcel, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR));
                    break;
                }
                case 1: {
                    this.onPostInitComplete(parcel.readInt(), parcel.readStrongBinder(), zzfa.zza(parcel, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR));
                    break;
                }
            }
            parcel2.writeNoException();
            return true;
        }
    }
}
