package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzn implements BaseOnConnectionFailedListener
{
    private final /* synthetic */ GoogleApiClient.OnConnectionFailedListener zza;
    
    zzn(final GoogleApiClient.OnConnectionFailedListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zza.onConnectionFailed(connectionResult);
    }
}
