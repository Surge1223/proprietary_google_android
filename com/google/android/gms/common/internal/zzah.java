package com.google.android.gms.common.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzah extends IInterface
{
    IObjectWrapper zza(final IObjectWrapper p0, final zzaz p1) throws RemoteException;
}
