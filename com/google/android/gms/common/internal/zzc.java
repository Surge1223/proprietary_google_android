package com.google.android.gms.common.internal;

import android.util.Log;
import android.os.Looper;

public final class zzc
{
    public static void zza(final Object o) {
        if (o != null) {
            return;
        }
        throw new IllegalArgumentException("null reference");
    }
    
    public static void zza(final String s) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            return;
        }
        final String value = String.valueOf(Thread.currentThread());
        final String value2 = String.valueOf(Looper.getMainLooper().getThread());
        final StringBuilder sb = new StringBuilder(57 + String.valueOf(value).length() + String.valueOf(value2).length());
        sb.append("checkMainThread: current thread ");
        sb.append(value);
        sb.append(" IS NOT the main thread ");
        sb.append(value2);
        sb.append("!");
        Log.e("Asserts", sb.toString());
        throw new IllegalStateException(s);
    }
}
