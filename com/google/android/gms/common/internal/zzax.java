package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.ConnectionResult;
import android.os.IBinder;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzax extends zzbid
{
    public static final Parcelable.Creator<zzax> CREATOR;
    private final int zza;
    private IBinder zzb;
    private ConnectionResult zzc;
    private boolean zzd;
    private boolean zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzay();
    }
    
    zzax(final int zza, final IBinder zzb, final ConnectionResult zzc, final boolean zzd, final boolean zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzax)) {
            return false;
        }
        final zzax zzax = (zzax)o;
        return this.zzc.equals(zzax.zzc) && this.zza().equals(zzax.zza());
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, 4, this.zzd);
        zzbig.zza(parcel, 5, this.zze);
        zzbig.zza(parcel, zza);
    }
    
    public final IAccountAccessor zza() {
        final IBinder zzb = this.zzb;
        if (zzb == null) {
            return null;
        }
        final IInterface queryLocalInterface = zzb.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
        if (queryLocalInterface instanceof IAccountAccessor) {
            return (IAccountAccessor)queryLocalInterface;
        }
        return new zzw(zzb);
    }
    
    public final ConnectionResult zzb() {
        return this.zzc;
    }
}
