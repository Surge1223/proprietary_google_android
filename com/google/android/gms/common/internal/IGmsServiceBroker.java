package com.google.android.gms.common.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface IGmsServiceBroker extends IInterface
{
    void getService(final IGmsCallbacks p0, final GetServiceRequest p1) throws RemoteException;
}
