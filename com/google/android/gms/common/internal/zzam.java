package com.google.android.gms.common.internal;

import java.util.ArrayList;
import java.util.List;

public final class zzam
{
    private final List<String> zza;
    private final Object zzb;
    
    private zzam(final Object o) {
        this.zzb = zzau.zza(o);
        this.zza = new ArrayList<String>();
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append(this.zzb.getClass().getSimpleName());
        sb.append('{');
        for (int size = this.zza.size(), i = 0; i < size; ++i) {
            sb.append(this.zza.get(i));
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        sb.append('}');
        return sb.toString();
    }
    
    public final zzam zza(String s, final Object o) {
        final List<String> zza = this.zza;
        s = zzau.zza(s);
        final String value = String.valueOf(o);
        final StringBuilder sb = new StringBuilder(1 + String.valueOf(s).length() + String.valueOf(value).length());
        sb.append(s);
        sb.append("=");
        sb.append(value);
        zza.add(sb.toString());
        return this;
    }
}
