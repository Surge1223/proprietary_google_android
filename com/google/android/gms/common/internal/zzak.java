package com.google.android.gms.common.internal;

public final class zzak
{
    public static zzam zza(final Object o) {
        return new zzam(o, null);
    }
    
    public static boolean zza(final Object o, final Object o2) {
        return o == o2 || (o != null && o.equals(o2));
    }
}
