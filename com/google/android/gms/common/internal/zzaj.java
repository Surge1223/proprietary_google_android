package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.internal.zzbjq;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.internal.zzbjr;
import android.content.Context;

public final class zzaj
{
    private static Object zza;
    private static boolean zzb;
    private static String zzc;
    private static int zzd;
    
    static {
        zzaj.zza = new Object();
    }
    
    public static int zzb(final Context context) {
        zzc(context);
        return zzaj.zzd;
    }
    
    private static void zzc(final Context context) {
        synchronized (zzaj.zza) {
            if (zzaj.zzb) {
                return;
            }
            zzaj.zzb = true;
            final String packageName = context.getPackageName();
            final zzbjq zza = zzbjr.zza(context);
            try {
                final Bundle metaData = zza.zza(packageName, 128).metaData;
                if (metaData == null) {
                    return;
                }
                zzaj.zzc = metaData.getString("com.google.app.id");
                zzaj.zzd = metaData.getInt("com.google.android.gms.version");
            }
            catch (PackageManager$NameNotFoundException ex) {
                Log.wtf("MetadataValueReader", "This should never happen.", (Throwable)ex);
            }
        }
    }
}
