package com.google.android.gms.common.internal;

import android.os.Parcel;
import com.google.android.gms.internal.zzez;
import android.os.RemoteException;
import android.accounts.Account;
import android.os.IInterface;

public interface IAccountAccessor extends IInterface
{
    Account getAccount() throws RemoteException;
    
    public abstract static class zza extends zzez implements IAccountAccessor
    {
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }
}
