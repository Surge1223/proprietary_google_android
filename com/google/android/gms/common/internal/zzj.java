package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.internal.zzch;
import android.content.Intent;

final class zzj extends zzg
{
    private final /* synthetic */ Intent zza;
    private final /* synthetic */ zzch zzb;
    private final /* synthetic */ int zzc;
    
    zzj(final Intent zza, final zzch zzb, final int zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void zza() {
        if (this.zza != null) {
            this.zzb.startActivityForResult(this.zza, this.zzc);
        }
    }
}
