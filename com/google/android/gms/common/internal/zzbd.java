package com.google.android.gms.common.internal;

import android.os.IBinder;
import com.google.android.gms.common.api.Api;
import android.os.IInterface;

public final class zzbd<T extends IInterface> extends zzl<T>
{
    public static Api.zzg<T> zzc() {
        throw new NoSuchMethodError();
    }
    
    @Override
    protected final String getServiceDescriptor() {
        throw new NoSuchMethodError();
    }
    
    @Override
    protected final String getStartServiceAction() {
        throw new NoSuchMethodError();
    }
    
    @Override
    protected final T zza(final IBinder binder) {
        throw new NoSuchMethodError();
    }
    
    protected final void zza(final int n, final T t) {
        throw new NoSuchMethodError();
    }
}
