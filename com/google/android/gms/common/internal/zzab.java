package com.google.android.gms.common.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzfa;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzab extends zzez implements zzaa
{
    public zzab() {
        this.attachInterface((IInterface)this, "com.google.android.gms.common.internal.ICertData");
    }
    
    public static zzaa zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.internal.ICertData");
        if (queryLocalInterface instanceof zzaa) {
            return (zzaa)queryLocalInterface;
        }
        return new zzac(binder);
    }
    
    public boolean onTransact(int zzc, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
        if (this.zza(zzc, parcel, parcel2, n)) {
            return true;
        }
        switch (zzc) {
            default: {
                return false;
            }
            case 2: {
                zzc = this.zzc();
                parcel2.writeNoException();
                parcel2.writeInt(zzc);
                break;
            }
            case 1: {
                final IObjectWrapper zzb = this.zzb();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzb);
                break;
            }
        }
        return true;
    }
}
