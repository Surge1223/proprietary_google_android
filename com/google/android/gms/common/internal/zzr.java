package com.google.android.gms.common.internal;

import android.util.Log;
import android.os.Message;
import com.google.android.gms.common.stats.zza;
import android.os.Handler;
import android.content.Context;
import java.util.HashMap;
import android.os.Handler$Callback;
import java.util.Iterator;
import java.util.HashSet;
import android.content.ComponentName;
import android.os.IBinder;
import java.util.Set;
import android.content.ServiceConnection;

final class zzr implements ServiceConnection
{
    private final Set<ServiceConnection> zza;
    private int zzb;
    private boolean zzc;
    private IBinder zzd;
    private final GmsClientSupervisor.ConnectionStatusConfig zze;
    private ComponentName zzf;
    private final /* synthetic */ zzq zzg;
    
    public zzr(final zzq zzg, final GmsClientSupervisor.ConnectionStatusConfig zze) {
        this.zzg = zzg;
        this.zze = zze;
        this.zza = new HashSet<ServiceConnection>();
        this.zzb = 2;
    }
    
    public final void onServiceConnected(final ComponentName zzf, final IBinder zzd) {
        synchronized (this.zzg.zza) {
            this.zzg.zzc.removeMessages(1, (Object)this.zze);
            this.zzd = zzd;
            this.zzf = zzf;
            final Iterator<ServiceConnection> iterator = this.zza.iterator();
            while (iterator.hasNext()) {
                iterator.next().onServiceConnected(zzf, zzd);
            }
            this.zzb = 1;
        }
    }
    
    public final void onServiceDisconnected(final ComponentName zzf) {
        synchronized (this.zzg.zza) {
            this.zzg.zzc.removeMessages(1, (Object)this.zze);
            this.zzd = null;
            this.zzf = zzf;
            final Iterator<ServiceConnection> iterator = this.zza.iterator();
            while (iterator.hasNext()) {
                iterator.next().onServiceDisconnected(zzf);
            }
            this.zzb = 2;
        }
    }
    
    public final void zza(final ServiceConnection serviceConnection, final String s) {
        this.zzg.zzd;
        this.zzg.zzb;
        this.zze.getStartServiceIntent(this.zzg.zzb);
        this.zza.add(serviceConnection);
    }
    
    public final void zza(final String s) {
        this.zzb = 3;
        this.zzc = this.zzg.zzd.zza(this.zzg.zzb, s, this.zze.getStartServiceIntent(this.zzg.zzb), (ServiceConnection)this, this.zze.getBindFlags());
        if (this.zzc) {
            this.zzg.zzc.sendMessageDelayed(this.zzg.zzc.obtainMessage(1, (Object)this.zze), this.zzg.zzf);
            return;
        }
        this.zzb = 2;
        try {
            this.zzg.zzd;
            this.zzg.zzb.unbindService((ServiceConnection)this);
        }
        catch (IllegalArgumentException ex) {}
    }
    
    public final boolean zza() {
        return this.zzc;
    }
    
    public final boolean zza(final ServiceConnection serviceConnection) {
        return this.zza.contains(serviceConnection);
    }
    
    public final int zzb() {
        return this.zzb;
    }
    
    public final void zzb(final ServiceConnection serviceConnection, final String s) {
        this.zzg.zzd;
        this.zzg.zzb;
        this.zza.remove(serviceConnection);
    }
    
    public final void zzb(final String s) {
        this.zzg.zzc.removeMessages(1, (Object)this.zze);
        this.zzg.zzd;
        this.zzg.zzb.unbindService((ServiceConnection)this);
        this.zzc = false;
        this.zzb = 2;
    }
    
    public final boolean zzc() {
        return this.zza.isEmpty();
    }
    
    public final IBinder zzd() {
        return this.zzd;
    }
    
    public final ComponentName zze() {
        return this.zzf;
    }
}
