package com.google.android.gms.common.internal;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.api.Scope;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzaz extends zzbid
{
    public static final Parcelable.Creator<zzaz> CREATOR;
    private final int zza;
    private final int zzb;
    private final int zzc;
    @Deprecated
    private final Scope[] zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zzba();
    }
    
    zzaz(final int zza, final int zzb, final int zzc, final Scope[] zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public zzaz(final int n, final int n2, final Scope[] array) {
        this(1, n, n2, null);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, this.zzd, n, false);
        zzbig.zza(parcel, zza);
    }
}
