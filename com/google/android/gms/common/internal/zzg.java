package com.google.android.gms.common.internal;

import android.content.ActivityNotFoundException;
import android.util.Log;
import android.content.DialogInterface;
import com.google.android.gms.common.api.internal.zzch;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.app.Activity;
import android.content.DialogInterface$OnClickListener;

public abstract class zzg implements DialogInterface$OnClickListener
{
    public static zzg zza(final Activity activity, final Intent intent, final int n) {
        return new zzh(intent, activity, n);
    }
    
    public static zzg zza(final Fragment fragment, final Intent intent, final int n) {
        return new zzi(intent, fragment, n);
    }
    
    public static zzg zza(final zzch zzch, final Intent intent, final int n) {
        return new zzj(intent, zzch, 2);
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        try {
            try {
                this.zza();
                dialogInterface.dismiss();
                return;
            }
            finally {}
        }
        catch (ActivityNotFoundException ex) {
            Log.e("DialogRedirect", "Failed to start resolution intent", (Throwable)ex);
            dialogInterface.dismiss();
            return;
        }
        dialogInterface.dismiss();
    }
    
    protected abstract void zza();
}
