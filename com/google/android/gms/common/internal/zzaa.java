package com.google.android.gms.common.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzaa extends IInterface
{
    IObjectWrapper zzb() throws RemoteException;
    
    int zzc() throws RemoteException;
}
