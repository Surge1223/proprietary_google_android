package com.google.android.gms.common.internal;

import android.support.v4.util.ArraySet;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Collections;
import com.google.android.gms.signin.SignInOptions;
import android.view.View;
import com.google.android.gms.common.api.Api;
import java.util.Map;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import android.accounts.Account;

public final class ClientSettings
{
    private final Account zza;
    private final Set<Scope> zzb;
    private final Set<Scope> zzc;
    private final Map<Api<?>, OptionalApiSettings> zzd;
    private final int zze;
    private final View zzf;
    private final String zzg;
    private final String zzh;
    private final SignInOptions zzi;
    private Integer zzj;
    
    public ClientSettings(final Account zza, final Set<Scope> set, final Map<Api<?>, OptionalApiSettings> map, final int zze, final View zzf, final String zzg, final String zzh, final SignInOptions zzi) {
        this.zza = zza;
        Set<Scope> zzb;
        if (set == null) {
            zzb = (Set<Scope>)Collections.EMPTY_SET;
        }
        else {
            zzb = Collections.unmodifiableSet((Set<? extends Scope>)set);
        }
        this.zzb = zzb;
        Map<Api<?>, OptionalApiSettings> empty_MAP = map;
        if (map == null) {
            empty_MAP = (Map<Api<?>, OptionalApiSettings>)Collections.EMPTY_MAP;
        }
        this.zzd = empty_MAP;
        this.zzf = zzf;
        this.zze = zze;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        final HashSet<Scope> set2 = new HashSet<Scope>(this.zzb);
        final Iterator<OptionalApiSettings> iterator = this.zzd.values().iterator();
        while (iterator.hasNext()) {
            set2.addAll((Collection<?>)iterator.next().mScopes);
        }
        this.zzc = (Set<Scope>)Collections.unmodifiableSet((Set<?>)set2);
    }
    
    public final Account getAccount() {
        return this.zza;
    }
    
    public final Account getAccountOrDefault() {
        if (this.zza != null) {
            return this.zza;
        }
        return new Account("<<default account>>", "com.google");
    }
    
    public final Set<Scope> getAllRequestedScopes() {
        return this.zzc;
    }
    
    public final Integer getClientSessionId() {
        return this.zzj;
    }
    
    public final String getRealClientClassName() {
        return this.zzh;
    }
    
    public final String getRealClientPackageName() {
        return this.zzg;
    }
    
    public final Set<Scope> getRequiredScopes() {
        return this.zzb;
    }
    
    public final SignInOptions getSignInOptions() {
        return this.zzi;
    }
    
    public final void setClientSessionId(final Integer zzj) {
        this.zzj = zzj;
    }
    
    public static final class OptionalApiSettings
    {
        public final Set<Scope> mScopes;
    }
    
    public static final class zza
    {
        private Account zza;
        private ArraySet<Scope> zzb;
        private int zzc;
        private String zzd;
        private String zze;
        private SignInOptions zzf;
        
        public zza() {
            this.zzc = 0;
            this.zzf = SignInOptions.DEFAULT;
        }
        
        public final zza zza(final Account zza) {
            this.zza = zza;
            return this;
        }
        
        public final zza zza(final String zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public final zza zza(final Collection<Scope> collection) {
            if (this.zzb == null) {
                this.zzb = new ArraySet<Scope>();
            }
            this.zzb.addAll(collection);
            return this;
        }
        
        public final ClientSettings zza() {
            return new ClientSettings(this.zza, this.zzb, null, 0, null, this.zzd, this.zze, this.zzf);
        }
        
        public final zza zzb(final String zze) {
            this.zze = zze;
            return this;
        }
    }
}
