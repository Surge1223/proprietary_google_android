package com.google.android.gms.common.internal;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class ClientIdentity extends zzbid
{
    public static final Parcelable.Creator<ClientIdentity> CREATOR;
    public final String packageName;
    public final int uid;
    
    static {
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    public ClientIdentity(final int uid, final String packageName) {
        this.uid = uid;
        this.packageName = packageName;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (o != null && o instanceof ClientIdentity) {
            final ClientIdentity clientIdentity = (ClientIdentity)o;
            return clientIdentity.uid == this.uid && zzak.zza(clientIdentity.packageName, this.packageName);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.uid;
    }
    
    @Override
    public String toString() {
        final int uid = this.uid;
        final String packageName = this.packageName;
        final StringBuilder sb = new StringBuilder(12 + String.valueOf(packageName).length());
        sb.append(uid);
        sb.append(":");
        sb.append(packageName);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.uid);
        zzbig.zza(parcel, 2, this.packageName, false);
        zzbig.zza(parcel, zza);
    }
}
