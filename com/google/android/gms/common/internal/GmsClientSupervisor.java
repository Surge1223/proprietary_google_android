package com.google.android.gms.common.internal;

import java.util.Arrays;
import android.content.Intent;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.Context;

public abstract class GmsClientSupervisor
{
    private static final Object zza;
    private static GmsClientSupervisor zzb;
    
    static {
        zza = new Object();
    }
    
    public static GmsClientSupervisor getInstance(final Context context) {
        synchronized (GmsClientSupervisor.zza) {
            if (GmsClientSupervisor.zzb == null) {
                GmsClientSupervisor.zzb = new zzq(context.getApplicationContext());
            }
            return GmsClientSupervisor.zzb;
        }
    }
    
    protected abstract boolean bindService(final ConnectionStatusConfig p0, final ServiceConnection p1, final String p2);
    
    public boolean bindService(final String s, final String s2, final int n, final ServiceConnection serviceConnection, final String s3) {
        return this.bindService(new ConnectionStatusConfig(s, s2, n), serviceConnection, s3);
    }
    
    protected abstract void unbindService(final ConnectionStatusConfig p0, final ServiceConnection p1, final String p2);
    
    public void unbindService(final String s, final String s2, final int n, final ServiceConnection serviceConnection, final String s3) {
        this.unbindService(new ConnectionStatusConfig(s, s2, n), serviceConnection, s3);
    }
    
    public static final class ConnectionStatusConfig
    {
        private final String zza;
        private final String zzb;
        private final ComponentName zzc;
        private final int zzd;
        
        public ConnectionStatusConfig(final String s, final String s2, final int zzd) {
            this.zza = zzau.zza(s);
            this.zzb = zzau.zza(s2);
            this.zzc = null;
            this.zzd = zzd;
        }
        
        @Override
        public final boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ConnectionStatusConfig)) {
                return false;
            }
            final ConnectionStatusConfig connectionStatusConfig = (ConnectionStatusConfig)o;
            return zzak.zza(this.zza, connectionStatusConfig.zza) && zzak.zza(this.zzb, connectionStatusConfig.zzb) && zzak.zza(this.zzc, connectionStatusConfig.zzc) && this.zzd == connectionStatusConfig.zzd;
        }
        
        public final int getBindFlags() {
            return this.zzd;
        }
        
        public final ComponentName getComponentName() {
            return this.zzc;
        }
        
        public final String getPackage() {
            return this.zzb;
        }
        
        public final Intent getStartServiceIntent(final Context context) {
            Intent intent;
            if (this.zza != null) {
                intent = new Intent(this.zza).setPackage(this.zzb);
            }
            else {
                intent = new Intent().setComponent(this.zzc);
            }
            return intent;
        }
        
        @Override
        public final int hashCode() {
            return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc, this.zzd });
        }
        
        @Override
        public final String toString() {
            if (this.zza == null) {
                return this.zzc.flattenToString();
            }
            return this.zza;
        }
    }
}
