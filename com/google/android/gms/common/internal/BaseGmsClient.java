package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.os.Message;
import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import java.util.Collection;
import android.os.IBinder;
import java.util.Collections;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.Feature;
import android.accounts.Account;
import android.text.TextUtils;
import android.os.Bundle;
import android.content.ServiceConnection;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import java.util.ArrayList;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.os.Looper;
import android.content.Context;
import android.os.Handler;
import java.util.concurrent.atomic.AtomicInteger;
import android.os.IInterface;

public abstract class BaseGmsClient<T extends IInterface>
{
    public static final String[] zzb;
    protected ConnectionProgressReportCallbacks mConnectionProgressReportCallbacks;
    protected AtomicInteger mDisconnectCount;
    final Handler zza;
    private int zzc;
    private long zzd;
    private long zze;
    private int zzf;
    private long zzg;
    private zzu zzh;
    private final Context zzi;
    private final Looper zzj;
    private final GmsClientSupervisor zzk;
    private final GoogleApiAvailabilityLight zzl;
    private final Object zzm;
    private final Object zzn;
    private IGmsServiceBroker zzo;
    private T zzp;
    private final ArrayList<zzc<?>> zzq;
    private zze zzr;
    private int zzs;
    private final BaseConnectionCallbacks zzt;
    private final BaseOnConnectionFailedListener zzu;
    private final int zzv;
    private final String zzw;
    private ConnectionResult zzx;
    private boolean zzy;
    
    static {
        zzb = new String[] { "service_esmobile", "service_googleme" };
    }
    
    protected BaseGmsClient(final Context context, final Looper looper, final GmsClientSupervisor gmsClientSupervisor, final GoogleApiAvailabilityLight googleApiAvailabilityLight, final int zzv, final BaseConnectionCallbacks zzt, final BaseOnConnectionFailedListener zzu, final String zzw) {
        this.zzm = new Object();
        this.zzn = new Object();
        this.zzq = new ArrayList<zzc<?>>();
        this.zzs = 1;
        this.zzx = null;
        this.zzy = false;
        this.mDisconnectCount = new AtomicInteger(0);
        this.zzi = zzau.zza(context, "Context must not be null");
        this.zzj = zzau.zza(looper, "Looper must not be null");
        this.zzk = zzau.zza(gmsClientSupervisor, "Supervisor must not be null");
        this.zzl = zzau.zza(googleApiAvailabilityLight, "API availability must not be null");
        this.zza = new zzb(looper);
        this.zzv = zzv;
        this.zzt = zzt;
        this.zzu = zzu;
        this.zzw = zzw;
    }
    
    static /* synthetic */ void zza(final BaseGmsClient baseGmsClient, final int n) {
        baseGmsClient.zzb(16);
    }
    
    static /* synthetic */ void zza(final BaseGmsClient baseGmsClient, final int n, final IInterface interface1) {
        baseGmsClient.zzb(n, null);
    }
    
    private final boolean zza(final int n, final int n2, final T t) {
        synchronized (this.zzm) {
            if (this.zzs != n) {
                return false;
            }
            this.zzb(n2, t);
            return true;
        }
    }
    
    private final void zzb(int n) {
        if (this.zzd()) {
            n = 5;
            this.zzy = true;
        }
        else {
            n = 4;
        }
        this.zza.sendMessage(this.zza.obtainMessage(n, this.mDisconnectCount.get(), 16));
    }
    
    private final void zzb(int zzs, final T zzp) {
        zzau.zzb(zzs == 4 == (zzp != null));
        synchronized (this.zzm) {
            this.zza(this.zzs = zzs, this.zzp = zzp);
            switch (zzs) {
                case 4: {
                    this.onConnectedLocked(zzp);
                    break;
                }
                case 2:
                case 3: {
                    if (this.zzr != null && this.zzh != null) {
                        final String zza = this.zzh.zza();
                        final String zzb = this.zzh.zzb();
                        zzs = String.valueOf(zza).length();
                        final StringBuilder sb = new StringBuilder(70 + zzs + String.valueOf(zzb).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(zza);
                        sb.append(" on ");
                        sb.append(zzb);
                        Log.e("GmsClient", sb.toString());
                        this.zzk.unbindService(this.zzh.zza(), this.zzh.zzb(), this.zzh.zzc(), (ServiceConnection)this.zzr, this.zzc());
                        this.mDisconnectCount.incrementAndGet();
                    }
                    this.zzr = new zze(this.mDisconnectCount.get());
                    zzu zzh;
                    if (this.zzs == 3 && this.getLocalStartServiceAction() != null) {
                        zzh = new zzu(this.zzi.getPackageName(), this.getLocalStartServiceAction(), true, this.zzaa());
                    }
                    else {
                        zzh = new zzu(this.zzac(), this.getStartServiceAction(), false, this.zzaa());
                    }
                    this.zzh = zzh;
                    if (!this.zzk.bindService(this.zzh.zza(), this.zzh.zzb(), this.zzh.zzc(), (ServiceConnection)this.zzr, this.zzc())) {
                        final String zza2 = this.zzh.zza();
                        final String zzb2 = this.zzh.zzb();
                        final int length = String.valueOf(zza2).length();
                        zzs = String.valueOf(zzb2).length();
                        final StringBuilder sb2 = new StringBuilder(34 + length + zzs);
                        sb2.append("unable to connect to service: ");
                        sb2.append(zza2);
                        sb2.append(" on ");
                        sb2.append(zzb2);
                        Log.e("GmsClient", sb2.toString());
                        this.zza(16, null, this.mDisconnectCount.get());
                    }
                    break;
                }
                case 1: {
                    if (this.zzr != null) {
                        this.zzk.unbindService(this.getStartServiceAction(), this.zzac(), this.zzaa(), (ServiceConnection)this.zzr, this.zzc());
                        this.zzr = null;
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    private final String zzc() {
        if (this.zzw == null) {
            return this.zzi.getClass().getName();
        }
        return this.zzw;
    }
    
    private final boolean zzd() {
        synchronized (this.zzm) {
            return this.zzs == 3;
        }
    }
    
    private final boolean zze() {
        if (this.zzy) {
            return false;
        }
        if (TextUtils.isEmpty((CharSequence)this.getServiceDescriptor())) {
            return false;
        }
        if (TextUtils.isEmpty((CharSequence)this.getLocalStartServiceAction())) {
            return false;
        }
        try {
            Class.forName(this.getServiceDescriptor());
            return true;
        }
        catch (ClassNotFoundException ex) {
            return false;
        }
    }
    
    public void connect(final ConnectionProgressReportCallbacks connectionProgressReportCallbacks) {
        this.mConnectionProgressReportCallbacks = zzau.zza(connectionProgressReportCallbacks, "Connection progress callbacks cannot be null.");
        this.zzb(2, null);
    }
    
    public void disconnect() {
        this.mDisconnectCount.incrementAndGet();
        synchronized (this.zzq) {
            for (int size = this.zzq.size(), i = 0; i < size; ++i) {
                this.zzq.get(i).zze();
            }
            this.zzq.clear();
            // monitorexit(this.zzq)
            synchronized (this.zzn) {
                this.zzo = null;
                // monitorexit(this.zzn)
                this.zzb(1, null);
            }
        }
    }
    
    public Account getAccount() {
        return null;
    }
    
    public final Account getAccountOrDefault() {
        if (this.getAccount() != null) {
            return this.getAccount();
        }
        return new Account("<<default account>>", "com.google");
    }
    
    public Bundle getConnectionHint() {
        return null;
    }
    
    protected String getLocalStartServiceAction() {
        return null;
    }
    
    public Feature[] getRequiredFeatures() {
        return new Feature[0];
    }
    
    protected Set<Scope> getScopes() {
        return (Set<Scope>)Collections.EMPTY_SET;
    }
    
    protected abstract String getServiceDescriptor();
    
    protected abstract String getStartServiceAction();
    
    public boolean isConnected() {
        synchronized (this.zzm) {
            return this.zzs == 4;
        }
    }
    
    public boolean isConnecting() {
        synchronized (this.zzm) {
            return this.zzs == 2 || this.zzs == 3;
        }
    }
    
    protected void onConnectedLocked(final T t) {
        this.zze = System.currentTimeMillis();
    }
    
    protected void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zzf = connectionResult.getErrorCode();
        this.zzg = System.currentTimeMillis();
    }
    
    protected void onConnectionSuspended(final int zzc) {
        this.zzc = zzc;
        this.zzd = System.currentTimeMillis();
    }
    
    protected void onPostInitHandler(final int n, final IBinder binder, final Bundle bundle, final int n2) {
        this.zza.sendMessage(this.zza.obtainMessage(1, n2, -1, (Object)new zzg(n, binder, bundle)));
    }
    
    public void onUserSignOut(final SignOutCallbacks signOutCallbacks) {
        signOutCallbacks.onSignOutComplete();
    }
    
    public boolean requiresAccount() {
        return false;
    }
    
    public boolean requiresGooglePlayServices() {
        return true;
    }
    
    public boolean requiresSignIn() {
        return false;
    }
    
    protected abstract T zza(final IBinder p0);
    
    public final void zza(final int n) {
        this.zza.sendMessage(this.zza.obtainMessage(6, this.mDisconnectCount.get(), n));
    }
    
    protected final void zza(final int n, final Bundle bundle, final int n2) {
        this.zza.sendMessage(this.zza.obtainMessage(7, n2, -1, (Object)new zzh(n, null)));
    }
    
    void zza(final int n, final T t) {
    }
    
    public final void zza(final IAccountAccessor authenticatedAccount, final Set<Scope> scopes) {
        final GetServiceRequest setExtraArgs = new GetServiceRequest(this.zzv).setCallingPackage(this.zzi.getPackageName()).setExtraArgs(this.zzb());
        if (scopes != null) {
            setExtraArgs.setScopes(scopes);
        }
        if (this.requiresSignIn()) {
            setExtraArgs.setClientRequestedAccount(this.getAccountOrDefault()).setAuthenticatedAccount(authenticatedAccount);
        }
        else if (this.requiresAccount()) {
            setExtraArgs.setClientRequestedAccount(this.getAccount());
        }
        setExtraArgs.setClientRequiredFeatures(this.getRequiredFeatures());
        try {
            synchronized (this.zzn) {
                if (this.zzo != null) {
                    this.zzo.getService(new zzd(this, this.mDisconnectCount.get()), setExtraArgs);
                }
                else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        }
        catch (RemoteException | RuntimeException ex3) {
            final Throwable t;
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", t);
            this.onPostInitHandler(8, null, null, this.mDisconnectCount.get());
        }
        catch (SecurityException ex) {
            throw ex;
        }
        catch (DeadObjectException ex2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", (Throwable)ex2);
            this.zza(1);
        }
    }
    
    protected int zzaa() {
        return 129;
    }
    
    public final String zzab() {
        if (this.isConnected() && this.zzh != null) {
            return this.zzh.zzb();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }
    
    protected String zzac() {
        return "com.google.android.gms";
    }
    
    public final Context zzad() {
        return this.zzi;
    }
    
    protected final void zzaf() {
        if (this.isConnected()) {
            return;
        }
        throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
    }
    
    public final T zzag() throws DeadObjectException {
        synchronized (this.zzm) {
            if (this.zzs != 5) {
                this.zzaf();
                zzau.zza(this.zzp != null, (Object)"Client is connected but service is null");
                return this.zzp;
            }
            throw new DeadObjectException();
        }
    }
    
    protected Bundle zzb() {
        return new Bundle();
    }
    
    public interface BaseConnectionCallbacks
    {
        void onConnected(final Bundle p0);
        
        void onConnectionSuspended(final int p0);
    }
    
    public interface BaseOnConnectionFailedListener
    {
        void onConnectionFailed(final ConnectionResult p0);
    }
    
    public interface ConnectionProgressReportCallbacks
    {
        void onReportServiceBinding(final ConnectionResult p0);
    }
    
    public interface SignOutCallbacks
    {
        void onSignOutComplete();
    }
    
    abstract class zza extends zzc<Boolean>
    {
        private final int zza;
        private final Bundle zzb;
        
        protected zza(final int zza, final Bundle zzb) {
            super(true);
            this.zza = zza;
            this.zzb = zzb;
        }
        
        protected abstract void zza(final ConnectionResult p0);
        
        protected abstract boolean zza();
        
        @Override
        protected final void zzb() {
        }
    }
    
    final class zzb extends Handler
    {
        public zzb(final Looper looper) {
            super(looper);
        }
        
        private static void zza(final Message message) {
            final zzc zzc = (zzc)message.obj;
            zzc.zzb();
            zzc.zzd();
        }
        
        private static boolean zzb(final Message message) {
            return message.what == 2 || message.what == 1 || message.what == 7;
        }
        
        public final void handleMessage(final Message message) {
            if (BaseGmsClient.this.mDisconnectCount.get() != message.arg1) {
                if (zzb(message)) {
                    zza(message);
                }
                return;
            }
            if ((message.what == 1 || message.what == 7 || message.what == 4 || message.what == 5) && !BaseGmsClient.this.isConnecting()) {
                zza(message);
                return;
            }
            final int what = message.what;
            PendingIntent pendingIntent = null;
            if (what == 4) {
                BaseGmsClient.this.zzx = new ConnectionResult(message.arg2);
                if (BaseGmsClient.this.zze() && !BaseGmsClient.this.zzy) {
                    BaseGmsClient.zza(BaseGmsClient.this, 3, null);
                    return;
                }
                ConnectionResult zzd;
                if (BaseGmsClient.this.zzx != null) {
                    zzd = BaseGmsClient.this.zzx;
                }
                else {
                    zzd = new ConnectionResult(8);
                }
                BaseGmsClient.this.mConnectionProgressReportCallbacks.onReportServiceBinding(zzd);
                BaseGmsClient.this.onConnectionFailed(zzd);
            }
            else {
                if (message.what == 5) {
                    ConnectionResult zzd2;
                    if (BaseGmsClient.this.zzx != null) {
                        zzd2 = BaseGmsClient.this.zzx;
                    }
                    else {
                        zzd2 = new ConnectionResult(8);
                    }
                    BaseGmsClient.this.mConnectionProgressReportCallbacks.onReportServiceBinding(zzd2);
                    BaseGmsClient.this.onConnectionFailed(zzd2);
                    return;
                }
                if (message.what == 3) {
                    if (message.obj instanceof PendingIntent) {
                        pendingIntent = (PendingIntent)message.obj;
                    }
                    final ConnectionResult connectionResult = new ConnectionResult(message.arg2, pendingIntent);
                    BaseGmsClient.this.mConnectionProgressReportCallbacks.onReportServiceBinding(connectionResult);
                    BaseGmsClient.this.onConnectionFailed(connectionResult);
                    return;
                }
                if (message.what == 6) {
                    BaseGmsClient.zza(BaseGmsClient.this, 5, null);
                    if (BaseGmsClient.this.zzt != null) {
                        BaseGmsClient.this.zzt.onConnectionSuspended(message.arg2);
                    }
                    BaseGmsClient.this.onConnectionSuspended(message.arg2);
                    BaseGmsClient.this.zza(5, 1, null);
                    return;
                }
                if (message.what == 2 && !BaseGmsClient.this.isConnected()) {
                    zza(message);
                    return;
                }
                if (zzb(message)) {
                    ((zzc)message.obj).zzc();
                    return;
                }
                final int what2 = message.what;
                final StringBuilder sb = new StringBuilder(45);
                sb.append("Don't know how to handle message: ");
                sb.append(what2);
                Log.wtf("GmsClient", sb.toString(), (Throwable)new Exception());
            }
        }
    }
    
    public abstract class zzc<TListener>
    {
        private TListener zza;
        private boolean zzb;
        
        public zzc(final TListener zza) {
            this.zza = zza;
            this.zzb = false;
        }
        
        protected abstract void zza(final TListener p0);
        
        protected abstract void zzb();
        
        public final void zzc() {
            synchronized (this) {
                final TListener zza = this.zza;
                if (this.zzb) {
                    final String value = String.valueOf(this);
                    final StringBuilder sb = new StringBuilder(47 + String.valueOf(value).length());
                    sb.append("Callback proxy ");
                    sb.append(value);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
                // monitorexit(this)
                Label_0100: {
                    if (zza != null) {
                        try {
                            this.zza(zza);
                            break Label_0100;
                        }
                        catch (RuntimeException ex) {
                            this.zzb();
                            throw ex;
                        }
                    }
                    this.zzb();
                }
                synchronized (this) {
                    this.zzb = true;
                    // monitorexit(this)
                    this.zzd();
                }
            }
        }
        
        public final void zzd() {
            this.zze();
            synchronized (BaseGmsClient.this.zzq) {
                BaseGmsClient.this.zzq.remove(this);
            }
        }
        
        public final void zze() {
            synchronized (this) {
                this.zza = null;
            }
        }
    }
    
    public static final class zzd extends IGmsCallbacks.zza
    {
        private BaseGmsClient zza;
        private final int zzb;
        
        public zzd(final BaseGmsClient zza, final int zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        @Override
        public final void onAccountValidationComplete(final int n, final Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", (Throwable)new Exception());
        }
        
        @Override
        public final void onPostInitComplete(final int n, final IBinder binder, final Bundle bundle) {
            zzau.zza(this.zza, "onPostInitComplete can be called only once per call to getRemoteService");
            this.zza.onPostInitHandler(n, binder, bundle, this.zzb);
            this.zza = null;
        }
    }
    
    public final class zze implements ServiceConnection
    {
        private final int zza;
        
        public zze(final int zza) {
            this.zza = zza;
        }
        
        public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
            if (binder == null) {
                BaseGmsClient.zza(BaseGmsClient.this, 16);
                return;
            }
            synchronized (BaseGmsClient.this.zzn) {
                final BaseGmsClient zzb = BaseGmsClient.this;
                IGmsServiceBroker gmsServiceBroker;
                if (binder == null) {
                    gmsServiceBroker = null;
                }
                else {
                    final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    if (queryLocalInterface != null && queryLocalInterface instanceof IGmsServiceBroker) {
                        gmsServiceBroker = (IGmsServiceBroker)queryLocalInterface;
                    }
                    else {
                        gmsServiceBroker = new zzad(binder);
                    }
                }
                zzb.zzo = gmsServiceBroker;
                // monitorexit(BaseGmsClient.zza(this.zzb))
                BaseGmsClient.this.zza(0, null, this.zza);
            }
        }
        
        public final void onServiceDisconnected(final ComponentName componentName) {
            synchronized (BaseGmsClient.this.zzn) {
                BaseGmsClient.this.zzo = null;
                // monitorexit(BaseGmsClient.zza(this.zzb))
                BaseGmsClient.this.zza.sendMessage(BaseGmsClient.this.zza.obtainMessage(6, this.zza, 1));
            }
        }
    }
    
    public final class zzf implements ConnectionProgressReportCallbacks
    {
        @Override
        public final void onReportServiceBinding(final ConnectionResult connectionResult) {
            if (connectionResult.isSuccess()) {
                BaseGmsClient.this.zza(null, BaseGmsClient.this.getScopes());
                return;
            }
            if (BaseGmsClient.this.zzu != null) {
                BaseGmsClient.this.zzu.onConnectionFailed(connectionResult);
            }
        }
    }
    
    public final class zzg extends zza
    {
        private final IBinder zza;
        
        public zzg(final int n, final IBinder zza, final Bundle bundle) {
            super(n, bundle);
            this.zza = zza;
        }
        
        @Override
        protected final void zza(final ConnectionResult connectionResult) {
            if (BaseGmsClient.this.zzu != null) {
                BaseGmsClient.this.zzu.onConnectionFailed(connectionResult);
            }
            BaseGmsClient.this.onConnectionFailed(connectionResult);
        }
        
        @Override
        protected final boolean zza() {
            try {
                final String interfaceDescriptor = this.zza.getInterfaceDescriptor();
                if (!BaseGmsClient.this.getServiceDescriptor().equals(interfaceDescriptor)) {
                    final String serviceDescriptor = BaseGmsClient.this.getServiceDescriptor();
                    final StringBuilder sb = new StringBuilder(34 + String.valueOf(serviceDescriptor).length() + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(serviceDescriptor);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                final IInterface zza = BaseGmsClient.this.zza(this.zza);
                if (zza != null && (BaseGmsClient.this.zza(2, 4, zza) || BaseGmsClient.this.zza(3, 4, zza))) {
                    BaseGmsClient.this.zzx = null;
                    final Bundle connectionHint = BaseGmsClient.this.getConnectionHint();
                    if (BaseGmsClient.this.zzt != null) {
                        BaseGmsClient.this.zzt.onConnected(connectionHint);
                    }
                    return true;
                }
                return false;
            }
            catch (RemoteException ex) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }
    
    public final class zzh extends zza
    {
        public zzh(final int n, final Bundle bundle) {
            super(n, null);
        }
        
        @Override
        protected final void zza(final ConnectionResult connectionResult) {
            BaseGmsClient.this.mConnectionProgressReportCallbacks.onReportServiceBinding(connectionResult);
            BaseGmsClient.this.onConnectionFailed(connectionResult);
        }
        
        @Override
        protected final boolean zza() {
            BaseGmsClient.this.mConnectionProgressReportCallbacks.onReportServiceBinding(ConnectionResult.zza);
            return true;
        }
    }
}
