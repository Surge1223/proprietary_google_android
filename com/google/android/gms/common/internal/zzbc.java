package com.google.android.gms.common.internal;

import android.graphics.drawable.Drawable;
import com.google.android.gms.common.util.zzi;
import android.text.method.TransformationMethod;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.support.v4.graphics.drawable.DrawableCompat;
import com.google.android.gms.base.R;
import android.graphics.Typeface;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Button;

public final class zzbc extends Button
{
    public zzbc(final Context context) {
        this(context, null);
    }
    
    private zzbc(final Context context, final AttributeSet set) {
        super(context, (AttributeSet)null, 16842824);
    }
    
    private static int zza(final int n, final int n2, final int n3, final int n4) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder(33);
                sb.append("Unknown color scheme: ");
                sb.append(n);
                throw new IllegalStateException(sb.toString());
            }
            case 2: {
                return n4;
            }
            case 1: {
                return n3;
            }
            case 0: {
                return n2;
            }
        }
    }
    
    public final void zza(final Resources resources, final int n, final int n2) {
        this.setTypeface(Typeface.DEFAULT_BOLD);
        this.setTextSize(14.0f);
        final int n3 = (int)(resources.getDisplayMetrics().density * 48.0f + 0.5f);
        this.setMinHeight(n3);
        this.setMinWidth(n3);
        int zza = zza(n2, R.drawable.common_google_signin_btn_icon_dark, R.drawable.common_google_signin_btn_icon_light, R.drawable.common_google_signin_btn_icon_light);
        final int zza2 = zza(n2, R.drawable.common_google_signin_btn_text_dark, R.drawable.common_google_signin_btn_text_light, R.drawable.common_google_signin_btn_text_light);
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder(32);
                sb.append("Unknown button size: ");
                sb.append(n);
                throw new IllegalStateException(sb.toString());
            }
            case 2: {
                break;
            }
            case 0:
            case 1: {
                zza = zza2;
                break;
            }
        }
        final Drawable wrap = DrawableCompat.wrap(resources.getDrawable(zza));
        DrawableCompat.setTintList(wrap, resources.getColorStateList(R.color.common_google_signin_btn_tint));
        DrawableCompat.setTintMode(wrap, PorterDuff.Mode.SRC_ATOP);
        this.setBackgroundDrawable(wrap);
        this.setTextColor((ColorStateList)zzau.zza(resources.getColorStateList(zza(n2, R.color.common_google_signin_btn_text_dark, R.color.common_google_signin_btn_text_light, R.color.common_google_signin_btn_text_light))));
        switch (n) {
            default: {
                final StringBuilder sb2 = new StringBuilder(32);
                sb2.append("Unknown button size: ");
                sb2.append(n);
                throw new IllegalStateException(sb2.toString());
            }
            case 2: {
                this.setText((CharSequence)null);
                break;
            }
            case 1: {
                this.setText((CharSequence)resources.getString(R.string.common_signin_button_text_long));
                break;
            }
            case 0: {
                this.setText((CharSequence)resources.getString(R.string.common_signin_button_text));
                break;
            }
        }
        this.setTransformationMethod((TransformationMethod)null);
        if (zzi.zza(this.getContext())) {
            this.setGravity(19);
        }
    }
}
