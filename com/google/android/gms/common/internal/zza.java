package com.google.android.gms.common.internal;

import android.os.RemoteException;
import android.util.Log;
import android.os.Binder;
import android.accounts.Account;

public final class zza extends IAccountAccessor.zza
{
    public static Account zza(final IAccountAccessor accountAccessor) {
        if (accountAccessor != null) {
            final long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                try {
                    accountAccessor.getAccount();
                    Binder.restoreCallingIdentity(clearCallingIdentity);
                }
                finally {}
            }
            catch (RemoteException ex) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
                Binder.restoreCallingIdentity(clearCallingIdentity);
                return null;
            }
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
        return null;
    }
    
    public final boolean equals(final Object o) {
        throw new NoSuchMethodError();
    }
    
    @Override
    public final Account getAccount() {
        throw new NoSuchMethodError();
    }
}
