package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;

public interface zzat
{
    ApiException zza(final Status p0);
}
