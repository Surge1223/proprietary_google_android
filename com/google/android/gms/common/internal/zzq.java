package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.util.Log;
import android.os.Message;
import android.content.ServiceConnection;
import com.google.android.gms.common.stats.zza;
import android.os.Handler;
import android.content.Context;
import java.util.HashMap;
import android.os.Handler$Callback;

final class zzq extends GmsClientSupervisor implements Handler$Callback
{
    private final HashMap<ConnectionStatusConfig, zzr> zza;
    private final Context zzb;
    private final Handler zzc;
    private final zza zzd;
    private final long zze;
    private final long zzf;
    
    zzq(final Context context) {
        this.zza = new HashMap<ConnectionStatusConfig, zzr>();
        this.zzb = context.getApplicationContext();
        this.zzc = new Handler(context.getMainLooper(), (Handler$Callback)this);
        this.zzd = com.google.android.gms.common.stats.zza.zza();
        this.zze = 5000L;
        this.zzf = 300000L;
    }
    
    @Override
    protected final boolean bindService(final ConnectionStatusConfig connectionStatusConfig, final ServiceConnection serviceConnection, final String s) {
        zzau.zza(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.zza) {
            final zzr zzr = this.zza.get(connectionStatusConfig);
            zzr zzr3 = null;
            if (zzr == null) {
                final zzr zzr2 = new zzr(this, connectionStatusConfig);
                zzr2.zza(serviceConnection, s);
                zzr2.zza(s);
                this.zza.put(connectionStatusConfig, zzr2);
                zzr3 = zzr2;
            }
            else {
                this.zzc.removeMessages(0, (Object)connectionStatusConfig);
                if (zzr.zza(serviceConnection)) {
                    final String value = String.valueOf(connectionStatusConfig);
                    final StringBuilder sb = new StringBuilder(81 + String.valueOf(value).length());
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(value);
                    throw new IllegalStateException(sb.toString());
                }
                zzr.zza(serviceConnection, s);
                switch (zzr.zzb()) {
                    default: {
                        zzr3 = zzr;
                        break;
                    }
                    case 2: {
                        zzr.zza(s);
                        zzr3 = zzr;
                        break;
                    }
                    case 1: {
                        serviceConnection.onServiceConnected(zzr.zze(), zzr.zzd());
                        zzr3 = zzr;
                        break;
                    }
                }
            }
            return zzr3.zza();
        }
    }
    
    public final boolean handleMessage(final Message message) {
        switch (message.what) {
            default: {
                return false;
            }
            case 1: {
                synchronized (this.zza) {
                    final ConnectionStatusConfig connectionStatusConfig = (ConnectionStatusConfig)message.obj;
                    final zzr zzr = this.zza.get(connectionStatusConfig);
                    if (zzr != null && zzr.zzb() == 3) {
                        final String value = String.valueOf(connectionStatusConfig);
                        final StringBuilder sb = new StringBuilder(47 + String.valueOf(value).length());
                        sb.append("Timeout waiting for ServiceConnection callback ");
                        sb.append(value);
                        Log.wtf("GmsClientSupervisor", sb.toString(), (Throwable)new Exception());
                        ComponentName componentName;
                        if ((componentName = zzr.zze()) == null) {
                            componentName = connectionStatusConfig.getComponentName();
                        }
                        ComponentName componentName2;
                        if ((componentName2 = componentName) == null) {
                            componentName2 = new ComponentName(connectionStatusConfig.getPackage(), "unknown");
                        }
                        zzr.onServiceDisconnected(componentName2);
                    }
                    return true;
                }
            }
            case 0: {
                synchronized (this.zza) {
                    final ConnectionStatusConfig connectionStatusConfig2 = (ConnectionStatusConfig)message.obj;
                    final zzr zzr2 = this.zza.get(connectionStatusConfig2);
                    if (zzr2 != null && zzr2.zzc()) {
                        if (zzr2.zza()) {
                            zzr2.zzb("GmsClientSupervisor");
                        }
                        this.zza.remove(connectionStatusConfig2);
                    }
                    return true;
                }
                break;
            }
        }
    }
    
    @Override
    protected final void unbindService(final ConnectionStatusConfig connectionStatusConfig, final ServiceConnection serviceConnection, String s) {
        zzau.zza(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.zza) {
            final zzr zzr = this.zza.get(connectionStatusConfig);
            if (zzr == null) {
                s = String.valueOf(connectionStatusConfig);
                final StringBuilder sb = new StringBuilder(50 + String.valueOf(s).length());
                sb.append("Nonexistent connection status for service config: ");
                sb.append(s);
                throw new IllegalStateException(sb.toString());
            }
            if (zzr.zza(serviceConnection)) {
                zzr.zzb(serviceConnection, s);
                if (zzr.zzc()) {
                    this.zzc.sendMessageDelayed(this.zzc.obtainMessage(0, (Object)connectionStatusConfig), this.zze);
                }
                return;
            }
            s = String.valueOf(connectionStatusConfig);
            final StringBuilder sb2 = new StringBuilder(76 + String.valueOf(s).length());
            sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
            sb2.append(s);
            throw new IllegalStateException(sb2.toString());
        }
    }
}
