package com.google.android.gms.common.internal;

import android.support.v4.app.Fragment;
import android.content.Intent;

final class zzi extends zzg
{
    private final /* synthetic */ Intent zza;
    private final /* synthetic */ Fragment zzb;
    private final /* synthetic */ int zzc;
    
    zzi(final Intent zza, final Fragment zzb, final int zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void zza() {
        if (this.zza != null) {
            this.zzb.startActivityForResult(this.zza, this.zzc);
        }
    }
}
