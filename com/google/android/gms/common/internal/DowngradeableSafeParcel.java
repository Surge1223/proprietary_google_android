package com.google.android.gms.common.internal;

import com.google.android.gms.internal.zzbid;

public abstract class DowngradeableSafeParcel extends zzbid implements ReflectedParcelable
{
    private static final Object zza;
    private static ClassLoader zzb;
    private static Integer zzc;
    private boolean zzd;
    
    static {
        zza = new Object();
        DowngradeableSafeParcel.zzb = null;
        DowngradeableSafeParcel.zzc = null;
    }
    
    public DowngradeableSafeParcel() {
        this.zzd = false;
    }
}
