package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.Api;
import android.content.Context;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.util.SparseIntArray;

public final class zzv
{
    private final SparseIntArray zza;
    private GoogleApiAvailabilityLight zzb;
    
    public zzv() {
        this(GoogleApiAvailability.getInstance());
    }
    
    public zzv(final GoogleApiAvailabilityLight zzb) {
        this.zza = new SparseIntArray();
        zzau.zza(zzb);
        this.zzb = zzb;
    }
    
    public final int zza(final Context context, final Api.zze zze) {
        zzau.zza(context);
        zzau.zza(zze);
        if (!zze.requiresGooglePlayServices()) {
            return 0;
        }
        final int zza = zze.zza();
        final int value = this.zza.get(zza, -1);
        if (value != -1) {
            return value;
        }
        int n = 0;
        int n2;
        while (true) {
            n2 = value;
            if (n >= this.zza.size()) {
                break;
            }
            final int key = this.zza.keyAt(n);
            if (key > zza && this.zza.get(key) == 0) {
                n2 = 0;
                break;
            }
            ++n;
        }
        int googlePlayServicesAvailable;
        if ((googlePlayServicesAvailable = n2) == -1) {
            googlePlayServicesAvailable = this.zzb.isGooglePlayServicesAvailable(context, zza);
        }
        this.zza.put(zza, googlePlayServicesAvailable);
        return googlePlayServicesAvailable;
    }
    
    public final void zza() {
        this.zza.clear();
    }
}
