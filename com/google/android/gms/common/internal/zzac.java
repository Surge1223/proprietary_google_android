package com.google.android.gms.common.internal;

import android.os.RemoteException;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzac extends zzey implements zzaa
{
    zzac(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.ICertData");
    }
    
    @Override
    public final IObjectWrapper zzb() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final int zzc() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final int int1 = zza.readInt();
        zza.recycle();
        return int1;
    }
}
