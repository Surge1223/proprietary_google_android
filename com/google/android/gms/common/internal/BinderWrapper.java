package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import android.os.Parcelable;

@KeepName
public final class BinderWrapper implements Parcelable
{
    public static final Parcelable.Creator<BinderWrapper> CREATOR;
    private IBinder zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzd();
    }
    
    public BinderWrapper() {
        this.zza = null;
    }
    
    private BinderWrapper(final Parcel parcel) {
        this.zza = null;
        this.zza = parcel.readStrongBinder();
    }
    
    public final int describeContents() {
        return 0;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeStrongBinder(this.zza);
    }
}
