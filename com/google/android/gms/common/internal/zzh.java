package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class zzh extends zzg
{
    private final /* synthetic */ Intent zza;
    private final /* synthetic */ Activity zzb;
    private final /* synthetic */ int zzc;
    
    zzh(final Intent zza, final Activity zzb, final int zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void zza() {
        if (this.zza != null) {
            this.zzb.startActivityForResult(this.zza, this.zzc);
        }
    }
}
