package com.google.android.gms.common.internal;

import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.PendingResult;

final class zzap implements zza
{
    private final /* synthetic */ PendingResult zza;
    private final /* synthetic */ TaskCompletionSource zzb;
    private final /* synthetic */ zzas zzc;
    private final /* synthetic */ zzat zzd;
    
    zzap(final PendingResult zza, final TaskCompletionSource zzb, final zzas zzc, final zzat zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final void zza(final Status status) {
        if (status.isSuccess()) {
            this.zzb.setResult(this.zzc.zza(this.zza.await(0L, TimeUnit.MILLISECONDS)));
            return;
        }
        this.zzb.setException(this.zzd.zza(status));
    }
}
