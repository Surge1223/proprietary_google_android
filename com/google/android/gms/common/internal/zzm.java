package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzm implements BaseConnectionCallbacks
{
    private final /* synthetic */ GoogleApiClient.ConnectionCallbacks zza;
    
    zzm(final GoogleApiClient.ConnectionCallbacks zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        this.zza.onConnected(bundle);
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        this.zza.onConnectionSuspended(n);
    }
}
