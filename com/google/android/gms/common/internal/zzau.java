package com.google.android.gms.common.internal;

import com.google.android.gms.common.util.zzs;
import android.os.Looper;
import android.os.Handler;
import android.text.TextUtils;

public final class zzau
{
    public static <T> T zza(final T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException("null reference");
    }
    
    public static <T> T zza(final T t, final Object o) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(o));
    }
    
    public static String zza(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            return s;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }
    
    public static String zza(final String s, final Object o) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            return s;
        }
        throw new IllegalArgumentException(String.valueOf(o));
    }
    
    public static void zza(final Handler handler) {
        if (Looper.myLooper() == handler.getLooper()) {
            return;
        }
        throw new IllegalStateException("Must be called on the handler thread");
    }
    
    public static void zza(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public static void zza(final boolean b, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.valueOf(o));
    }
    
    public static void zza(final boolean b, final String s, final Object... array) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.format(s, array));
    }
    
    public static void zzb(final String s) {
        if (zzs.zza()) {
            return;
        }
        throw new IllegalStateException(s);
    }
    
    public static void zzb(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static void zzb(final boolean b, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.valueOf(o));
    }
    
    public static void zzc(final String s) {
        if (!zzs.zza()) {
            return;
        }
        throw new IllegalStateException(s);
    }
}
