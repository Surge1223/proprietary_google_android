package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzq;
import android.view.View;
import android.content.Context;
import com.google.android.gms.dynamic.zzp;

public final class zzbb extends zzp<zzah>
{
    private static final zzbb zza;
    
    static {
        zza = new zzbb();
    }
    
    private zzbb() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }
    
    public static View zza(final Context context, final int n, final int n2) throws zzq {
        return zzbb.zza.zzb(context, n, n2);
    }
    
    private final View zzb(final Context context, final int n, final int n2) throws zzq {
        try {
            return zzn.zza(this.zzb(context).zza(zzn.zza(context), new zzaz(n, n2, null)));
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(n);
            sb.append(" and color ");
            sb.append(n2);
            throw new zzq(sb.toString(), ex);
        }
    }
}
