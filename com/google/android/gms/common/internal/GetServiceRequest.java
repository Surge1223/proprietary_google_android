package com.google.android.gms.common.internal;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Collection;
import android.os.IInterface;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.Feature;
import android.accounts.Account;
import android.os.Bundle;
import com.google.android.gms.common.api.Scope;
import android.os.IBinder;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GetServiceRequest extends zzbid
{
    public static final Parcelable.Creator<GetServiceRequest> CREATOR;
    private final int zza;
    private final int zzb;
    private int zzc;
    private String zzd;
    private IBinder zze;
    private Scope[] zzf;
    private Bundle zzg;
    private Account zzh;
    private Feature[] zzi;
    
    static {
        CREATOR = (Parcelable.Creator)new zzk();
    }
    
    public GetServiceRequest(final int zzb) {
        this.zza = 3;
        this.zzc = GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        this.zzb = zzb;
    }
    
    GetServiceRequest(final int zza, final int zzb, final int zzc, final String zzd, final IBinder zze, final Scope[] zzf, final Bundle zzg, final Account zzh, final Feature[] zzi) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        if ("com.google.android.gms".equals(zzd)) {
            this.zzd = "com.google.android.gms";
        }
        else {
            this.zzd = zzd;
        }
        if (zza < 2) {
            this.zzh = zza(zze);
        }
        else {
            this.zze = zze;
            this.zzh = zzh;
        }
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzi = zzi;
    }
    
    private static Account zza(final IBinder binder) {
        Account zza = null;
        final IAccountAccessor accountAccessor = null;
        if (binder != null) {
            IAccountAccessor accountAccessor2;
            if (binder == null) {
                accountAccessor2 = accountAccessor;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
                if (queryLocalInterface instanceof IAccountAccessor) {
                    accountAccessor2 = (IAccountAccessor)queryLocalInterface;
                }
                else {
                    accountAccessor2 = new zzw(binder);
                }
            }
            zza = com.google.android.gms.common.internal.zza.zza(accountAccessor2);
        }
        return zza;
    }
    
    public GetServiceRequest setAuthenticatedAccount(final IAccountAccessor accountAccessor) {
        if (accountAccessor != null) {
            this.zze = accountAccessor.asBinder();
        }
        return this;
    }
    
    public GetServiceRequest setCallingPackage(final String zzd) {
        this.zzd = zzd;
        return this;
    }
    
    public GetServiceRequest setClientRequestedAccount(final Account zzh) {
        this.zzh = zzh;
        return this;
    }
    
    public GetServiceRequest setClientRequiredFeatures(final Feature[] zzi) {
        this.zzi = zzi;
        return this;
    }
    
    public GetServiceRequest setExtraArgs(final Bundle zzg) {
        this.zzg = zzg;
        return this;
    }
    
    public GetServiceRequest setScopes(final Collection<Scope> collection) {
        this.zzf = collection.toArray(new Scope[collection.size()]);
        return this;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, this.zzd, false);
        zzbig.zza(parcel, 5, this.zze, false);
        zzbig.zza(parcel, 6, this.zzf, n, false);
        zzbig.zza(parcel, 7, this.zzg, false);
        zzbig.zza(parcel, 8, (Parcelable)this.zzh, n, false);
        zzbig.zza(parcel, 10, this.zzi, n, false);
        zzbig.zza(parcel, zza);
    }
}
