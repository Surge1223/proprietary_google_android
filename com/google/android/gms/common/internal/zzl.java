package com.google.android.gms.common.internal;

import com.google.android.gms.common.Feature;
import java.util.Iterator;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import android.os.Looper;
import android.content.Context;
import android.accounts.Account;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.api.Api;
import android.os.IInterface;

public abstract class zzl<T extends IInterface> extends BaseGmsClient<T> implements Api.zze
{
    private final ClientSettings zzc;
    private final Set<Scope> zzd;
    private final Account zze;
    
    protected zzl(final Context context, final Looper looper, final int n, final ClientSettings clientSettings, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, GmsClientSupervisor.getInstance(context), GoogleApiAvailability.getInstance(), n, clientSettings, zzau.zza(connectionCallbacks), zzau.zza(onConnectionFailedListener));
    }
    
    private zzl(final Context context, final Looper looper, final GmsClientSupervisor gmsClientSupervisor, final GoogleApiAvailability googleApiAvailability, final int n, final ClientSettings zzc, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Object o;
        if (connectionCallbacks == null) {
            o = null;
        }
        else {
            o = new zzm(connectionCallbacks);
        }
        Object o2;
        if (onConnectionFailedListener == null) {
            o2 = null;
        }
        else {
            o2 = new zzn(onConnectionFailedListener);
        }
        super(context, looper, gmsClientSupervisor, googleApiAvailability, n, (BaseConnectionCallbacks)o, (BaseOnConnectionFailedListener)o2, zzc.getRealClientClassName());
        this.zzc = zzc;
        this.zze = zzc.getAccount();
        final Set<Scope> allRequestedScopes = zzc.getAllRequestedScopes();
        final Set<Scope> zza = this.zza(allRequestedScopes);
        final Iterator<Scope> iterator = zza.iterator();
        while (iterator.hasNext()) {
            if (allRequestedScopes.contains(iterator.next())) {
                continue;
            }
            throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
        }
        this.zzd = zza;
    }
    
    @Override
    public final Account getAccount() {
        return this.zze;
    }
    
    @Override
    public Feature[] getRequiredFeatures() {
        return new Feature[0];
    }
    
    @Override
    protected final Set<Scope> getScopes() {
        return this.zzd;
    }
    
    @Override
    public int zza() {
        return GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }
    
    protected Set<Scope> zza(final Set<Scope> set) {
        return set;
    }
}
