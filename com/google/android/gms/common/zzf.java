package com.google.android.gms.common;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.common.internal.zzaa;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.internal.zzab;

abstract class zzf extends zzab
{
    private int zza;
    
    protected zzf(final byte[] array) {
        zzau.zzb(array.length == 25);
        this.zza = Arrays.hashCode(array);
    }
    
    protected static byte[] zza(final String s) {
        try {
            return s.getBytes("ISO-8859-1");
        }
        catch (UnsupportedEncodingException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    public boolean equals(final Object o) {
        if (o != null) {
            if (o instanceof zzaa) {
                try {
                    final zzaa zzaa = (zzaa)o;
                    if (zzaa.zzc() != this.hashCode()) {
                        return false;
                    }
                    final IObjectWrapper zzb = zzaa.zzb();
                    return zzb != null && Arrays.equals(this.zza(), (byte[])zzn.zza(zzb));
                }
                catch (RemoteException ex) {
                    Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
                    return false;
                }
            }
        }
        return false;
    }
    
    public int hashCode() {
        return this.zza;
    }
    
    abstract byte[] zza();
    
    @Override
    public final IObjectWrapper zzb() {
        return zzn.zza(this.zza());
    }
    
    @Override
    public final int zzc() {
        return this.hashCode();
    }
}
