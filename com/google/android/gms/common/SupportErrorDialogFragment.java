package com.google.android.gms.common;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import com.google.android.gms.common.internal.zzau;
import android.content.DialogInterface$OnCancelListener;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;

public class SupportErrorDialogFragment extends DialogFragment
{
    private Dialog zza;
    private DialogInterface$OnCancelListener zzb;
    
    public SupportErrorDialogFragment() {
        this.zza = null;
        this.zzb = null;
    }
    
    public static SupportErrorDialogFragment newInstance(Dialog zza, final DialogInterface$OnCancelListener zzb) {
        final SupportErrorDialogFragment supportErrorDialogFragment = new SupportErrorDialogFragment();
        zza = zzau.zza(zza, "Cannot display null dialog");
        zza.setOnCancelListener((DialogInterface$OnCancelListener)null);
        zza.setOnDismissListener((DialogInterface$OnDismissListener)null);
        supportErrorDialogFragment.zza = zza;
        if (zzb != null) {
            supportErrorDialogFragment.zzb = zzb;
        }
        return supportErrorDialogFragment;
    }
    
    @Override
    public void onCancel(final DialogInterface dialogInterface) {
        if (this.zzb != null) {
            this.zzb.onCancel(dialogInterface);
        }
    }
    
    @Override
    public Dialog onCreateDialog(final Bundle bundle) {
        if (this.zza == null) {
            this.setShowsDialog(false);
        }
        return this.zza;
    }
    
    @Override
    public void show(final FragmentManager fragmentManager, final String s) {
        super.show(fragmentManager, s);
    }
}
