package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class zzh extends zzf
{
    private static final WeakReference<byte[]> zzb;
    private WeakReference<byte[]> zza;
    
    static {
        zzb = new WeakReference<byte[]>(null);
    }
    
    zzh(final byte[] array) {
        super(array);
        this.zza = zzh.zzb;
    }
    
    @Override
    final byte[] zza() {
        synchronized (this) {
            byte[] zzd;
            if ((zzd = this.zza.get()) == null) {
                zzd = this.zzd();
                this.zza = new WeakReference<byte[]>(zzd);
            }
            return zzd;
        }
    }
    
    protected abstract byte[] zzd();
}
