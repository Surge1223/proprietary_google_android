package com.google.android.gms.common;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.common.internal.zzab;
import android.os.IBinder;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzl extends zzbid
{
    public static final Parcelable.Creator<zzl> CREATOR;
    private final String zza;
    private final zzf zzb;
    private final boolean zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzm();
    }
    
    zzl(final String zza, final IBinder binder, final boolean zzc) {
        this.zza = zza;
        this.zzb = zza(binder);
        this.zzc = zzc;
    }
    
    private static zzf zza(final IBinder binder) {
        final zzf zzf = null;
        if (binder == null) {
            return null;
        }
        try {
            final IObjectWrapper zzb = zzab.zza(binder).zzb();
            byte[] array;
            if (zzb == null) {
                array = null;
            }
            else {
                array = zzn.zza(zzb);
            }
            zzf zzf2;
            if (array != null) {
                zzf2 = new zzg(array);
            }
            else {
                Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
                zzf2 = zzf;
            }
            return zzf2;
        }
        catch (RemoteException ex) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", (Throwable)ex);
            return null;
        }
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        IBinder binder;
        if (this.zzb == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            binder = null;
        }
        else {
            binder = this.zzb.asBinder();
        }
        zzbig.zza(parcel, 2, binder, false);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, zza);
    }
}
