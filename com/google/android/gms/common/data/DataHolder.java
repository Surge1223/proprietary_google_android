package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.zzau;
import java.util.HashMap;
import java.util.ArrayList;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.util.Log;
import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import java.io.Closeable;
import com.google.android.gms.internal.zzbid;

@KeepName
public final class DataHolder extends zzbid implements Closeable
{
    public static final Parcelable.Creator<DataHolder> CREATOR;
    private static final Builder zzk;
    private final int zza;
    private final String[] zzb;
    private Bundle zzc;
    private final CursorWindow[] zzd;
    private final int zze;
    private final Bundle zzf;
    private int[] zzg;
    private int zzh;
    private boolean zzi;
    private boolean zzj;
    
    static {
        CREATOR = (Parcelable.Creator)new zzf();
        zzk = (Builder)new zze(new String[0], null);
    }
    
    DataHolder(final int zza, final String[] zzb, final CursorWindow[] zzd, final int zze, final Bundle zzf) {
        this.zzi = false;
        this.zzj = true;
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    @Override
    public final void close() {
        synchronized (this) {
            if (!this.zzi) {
                this.zzi = true;
                for (int i = 0; i < this.zzd.length; ++i) {
                    this.zzd[i].close();
                }
            }
        }
    }
    
    @Override
    protected final void finalize() throws Throwable {
        try {
            if (this.zzj && this.zzd.length > 0 && !this.isClosed()) {
                this.close();
                final String string = this.toString();
                final StringBuilder sb = new StringBuilder(178 + String.valueOf(string).length());
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(string);
                sb.append(")");
                Log.e("DataBuffer", sb.toString());
            }
        }
        finally {
            super.finalize();
        }
    }
    
    public final int getStatusCode() {
        return this.zze;
    }
    
    public final boolean isClosed() {
        synchronized (this) {
            return this.zzi;
        }
    }
    
    public final void validateContents() {
        this.zzc = new Bundle();
        final int n = 0;
        for (int i = 0; i < this.zzb.length; ++i) {
            this.zzc.putInt(this.zzb[i], i);
        }
        this.zzg = new int[this.zzd.length];
        int zzh = 0;
        for (int j = n; j < this.zzd.length; ++j) {
            this.zzg[j] = zzh;
            zzh += this.zzd[j].getNumRows() - (zzh - this.zzd[j].getStartPosition());
        }
        this.zzh = zzh;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzb, false);
        zzbig.zza(parcel, 2, this.zzd, n, false);
        zzbig.zza(parcel, 3, this.getStatusCode());
        zzbig.zza(parcel, 4, this.zzf, false);
        zzbig.zza(parcel, 1000, this.zza);
        zzbig.zza(parcel, zza);
        if ((n & 0x1) != 0x0) {
            this.close();
        }
    }
    
    public static class Builder
    {
        private final String[] zza;
        private final ArrayList<HashMap<String, Object>> zzb;
        private final String zzc;
        private final HashMap<Object, Integer> zzd;
        private boolean zze;
        private String zzf;
        
        private Builder(final String[] array, final String zzc) {
            this.zza = zzau.zza(array);
            this.zzb = new ArrayList<HashMap<String, Object>>();
            this.zzc = zzc;
            this.zzd = new HashMap<Object, Integer>();
            this.zze = false;
            this.zzf = null;
        }
    }
}
