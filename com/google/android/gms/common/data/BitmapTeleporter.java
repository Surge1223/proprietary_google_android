package com.google.android.gms.common.data;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import android.os.Parcel;
import android.util.Log;
import java.io.Closeable;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public class BitmapTeleporter extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<BitmapTeleporter> CREATOR;
    private final int zza;
    private ParcelFileDescriptor zzb;
    private final int zzc;
    private Bitmap zzd;
    private boolean zze;
    private File zzf;
    
    static {
        CREATOR = (Parcelable.Creator)new zza();
    }
    
    BitmapTeleporter(final int zza, final ParcelFileDescriptor zzb, final int zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = null;
        this.zze = false;
    }
    
    public BitmapTeleporter(final Bitmap zzd) {
        this.zza = 1;
        this.zzb = null;
        this.zzc = 0;
        this.zzd = zzd;
        this.zze = true;
    }
    
    private final FileOutputStream zza() {
        if (this.zzf != null) {
            try {
                final File tempFile = File.createTempFile("teleporter", ".tmp", this.zzf);
                try {
                    final FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
                    this.zzb = ParcelFileDescriptor.open(tempFile, 268435456);
                    tempFile.delete();
                    return fileOutputStream;
                }
                catch (FileNotFoundException ex2) {
                    throw new IllegalStateException("Temporary file is somehow already deleted");
                }
            }
            catch (IOException ex) {
                throw new IllegalStateException("Could not create temporary file", ex);
            }
        }
        throw new IllegalStateException("setTempDir() must be called before writing this object to a parcel");
    }
    
    private static void zza(final Closeable closeable) {
        try {
            closeable.close();
        }
        catch (IOException ex) {
            Log.w("BitmapTeleporter", "Could not close stream", (Throwable)ex);
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        if (this.zzb == null) {
            final Bitmap zzd = this.zzd;
            final ByteBuffer allocate = ByteBuffer.allocate(zzd.getRowBytes() * zzd.getHeight());
            zzd.copyPixelsToBuffer((Buffer)allocate);
            final byte[] array = allocate.array();
            final DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(this.zza()));
            try {
                try {
                    dataOutputStream.writeInt(array.length);
                    dataOutputStream.writeInt(zzd.getWidth());
                    dataOutputStream.writeInt(zzd.getHeight());
                    dataOutputStream.writeUTF(zzd.getConfig().toString());
                    dataOutputStream.write(array);
                    zza(dataOutputStream);
                }
                finally {}
            }
            catch (IOException ex) {
                throw new IllegalStateException("Could not write into unlinked file", ex);
            }
            zza(dataOutputStream);
        }
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n | 0x1, false);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, zza);
        this.zzb = null;
    }
}
