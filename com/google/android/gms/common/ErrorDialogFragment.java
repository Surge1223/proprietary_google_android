package com.google.android.gms.common;

import android.app.FragmentManager;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import com.google.android.gms.common.internal.zzau;
import android.content.DialogInterface$OnCancelListener;
import android.app.Dialog;
import android.app.DialogFragment;

public class ErrorDialogFragment extends DialogFragment
{
    private Dialog zza;
    private DialogInterface$OnCancelListener zzb;
    
    public ErrorDialogFragment() {
        this.zza = null;
        this.zzb = null;
    }
    
    public static ErrorDialogFragment newInstance(Dialog zza, final DialogInterface$OnCancelListener zzb) {
        final ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        zza = zzau.zza(zza, "Cannot display null dialog");
        zza.setOnCancelListener((DialogInterface$OnCancelListener)null);
        zza.setOnDismissListener((DialogInterface$OnDismissListener)null);
        errorDialogFragment.zza = zza;
        if (zzb != null) {
            errorDialogFragment.zzb = zzb;
        }
        return errorDialogFragment;
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        if (this.zzb != null) {
            this.zzb.onCancel(dialogInterface);
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        if (this.zza == null) {
            this.setShowsDialog(false);
        }
        return this.zza;
    }
    
    public void show(final FragmentManager fragmentManager, final String s) {
        super.show(fragmentManager, s);
    }
}
