package com.google.android.gms.common.stats;

import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public abstract class StatsEvent extends zzbid implements ReflectedParcelable
{
    @Override
    public String toString() {
        final long zza = this.zza();
        final int zzb = this.zzb();
        final long zzc = this.zzc();
        final String zzd = this.zzd();
        final StringBuilder sb = new StringBuilder(53 + String.valueOf(zzd).length());
        sb.append(zza);
        sb.append("\t");
        sb.append(zzb);
        sb.append("\t");
        sb.append(zzc);
        sb.append(zzd);
        return sb.toString();
    }
    
    public abstract long zza();
    
    public abstract int zzb();
    
    public abstract long zzc();
    
    public abstract String zzd();
}
