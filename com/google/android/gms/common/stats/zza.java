package com.google.android.gms.common.stats;

import android.content.ComponentName;
import android.util.Log;
import com.google.android.gms.common.util.zzd;
import android.content.ServiceConnection;
import android.content.Intent;
import android.content.Context;
import java.util.Collections;
import java.util.List;

public final class zza
{
    private static final Object zza;
    private static volatile zza zzb;
    private static boolean zzc;
    private final List<String> zzd;
    private final List<String> zze;
    private final List<String> zzf;
    private final List<String> zzg;
    
    static {
        zza = new Object();
        com.google.android.gms.common.stats.zza.zzc = false;
    }
    
    private zza() {
        this.zzd = (List<String>)Collections.EMPTY_LIST;
        this.zze = (List<String>)Collections.EMPTY_LIST;
        this.zzf = (List<String>)Collections.EMPTY_LIST;
        this.zzg = (List<String>)Collections.EMPTY_LIST;
    }
    
    public static zza zza() {
        if (com.google.android.gms.common.stats.zza.zzb == null) {
            synchronized (com.google.android.gms.common.stats.zza.zza) {
                if (com.google.android.gms.common.stats.zza.zzb == null) {
                    com.google.android.gms.common.stats.zza.zzb = new zza();
                }
            }
        }
        return com.google.android.gms.common.stats.zza.zzb;
    }
    
    public final boolean zza(final Context context, final Intent intent, final ServiceConnection serviceConnection, final int n) {
        return this.zza(context, context.getClass().getName(), intent, serviceConnection, n);
    }
    
    public final boolean zza(final Context context, final String s, final Intent intent, final ServiceConnection serviceConnection, final int n) {
        final ComponentName component = intent.getComponent();
        if (component != null && com.google.android.gms.common.util.zzd.zzb(context, component.getPackageName())) {
            Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
            return false;
        }
        return context.bindService(intent, serviceConnection, n);
    }
}
