package com.google.android.gms.common.stats;

import android.text.TextUtils;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable.Creator;

public final class WakeLockEvent extends StatsEvent
{
    public static final Parcelable.Creator<WakeLockEvent> CREATOR;
    private final int zza;
    private final long zzb;
    private int zzc;
    private final String zzd;
    private final String zze;
    private final String zzf;
    private final int zzg;
    private final List<String> zzh;
    private final String zzi;
    private final long zzj;
    private int zzk;
    private final String zzl;
    private final float zzm;
    private final long zzn;
    private long zzo;
    
    static {
        CREATOR = (Parcelable.Creator)new zzd();
    }
    
    WakeLockEvent(final int zza, final long zzb, final int zzc, final String zzd, final int zzg, final List<String> zzh, final String zzi, final long zzj, final int zzk, final String zze, final String zzl, final float zzm, final long zzn, final String zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzo = -1L;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = zzn;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zza());
        zzbig.zza(parcel, 4, this.zzd, false);
        zzbig.zza(parcel, 5, this.zzg);
        zzbig.zzb(parcel, 6, this.zzh, false);
        zzbig.zza(parcel, 8, this.zzj);
        zzbig.zza(parcel, 10, this.zze, false);
        zzbig.zza(parcel, 11, this.zzb());
        zzbig.zza(parcel, 12, this.zzi, false);
        zzbig.zza(parcel, 13, this.zzl, false);
        zzbig.zza(parcel, 14, this.zzk);
        zzbig.zza(parcel, 15, this.zzm);
        zzbig.zza(parcel, 16, this.zzn);
        zzbig.zza(parcel, 17, this.zzf, false);
        zzbig.zza(parcel, zza);
    }
    
    @Override
    public final long zza() {
        return this.zzb;
    }
    
    @Override
    public final int zzb() {
        return this.zzc;
    }
    
    @Override
    public final long zzc() {
        return this.zzo;
    }
    
    @Override
    public final String zzd() {
        final String zzd = this.zzd;
        final int zzg = this.zzg;
        String join;
        if (this.zzh == null) {
            join = "";
        }
        else {
            join = TextUtils.join((CharSequence)",", (Iterable)this.zzh);
        }
        final int zzk = this.zzk;
        String zze;
        if (this.zze == null) {
            zze = "";
        }
        else {
            zze = this.zze;
        }
        String zzl;
        if (this.zzl == null) {
            zzl = "";
        }
        else {
            zzl = this.zzl;
        }
        final float zzm = this.zzm;
        String zzf;
        if (this.zzf == null) {
            zzf = "";
        }
        else {
            zzf = this.zzf;
        }
        final StringBuilder sb = new StringBuilder(45 + String.valueOf(zzd).length() + String.valueOf(join).length() + String.valueOf(zze).length() + String.valueOf(zzl).length() + String.valueOf(zzf).length());
        sb.append("\t");
        sb.append(zzd);
        sb.append("\t");
        sb.append(zzg);
        sb.append("\t");
        sb.append(join);
        sb.append("\t");
        sb.append(zzk);
        sb.append("\t");
        sb.append(zze);
        sb.append("\t");
        sb.append(zzl);
        sb.append("\t");
        sb.append(zzm);
        sb.append("\t");
        sb.append(zzf);
        return sb.toString();
    }
}
