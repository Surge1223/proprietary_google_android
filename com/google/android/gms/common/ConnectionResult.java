package com.google.android.gms.common;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.app.PendingIntent;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class ConnectionResult extends zzbid
{
    public static final Parcelable.Creator<ConnectionResult> CREATOR;
    public static final ConnectionResult zza;
    private final int zzb;
    private final int zzc;
    private final PendingIntent zzd;
    private final String zze;
    
    static {
        zza = new ConnectionResult(0);
        CREATOR = (Parcelable.Creator)new zzb();
    }
    
    public ConnectionResult(final int n) {
        this(n, null, null);
    }
    
    ConnectionResult(final int zzb, final int zzc, final PendingIntent zzd, final String zze) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public ConnectionResult(final int n, final PendingIntent pendingIntent) {
        this(n, pendingIntent, null);
    }
    
    public ConnectionResult(final int n, final PendingIntent pendingIntent, final String s) {
        this(1, n, pendingIntent, s);
    }
    
    static String zza(final int n) {
        if (n == 99) {
            return "UNFINISHED";
        }
        if (n == 1500) {
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        final StringBuilder sb = new StringBuilder(31);
                        sb.append("UNKNOWN_ERROR_CODE(");
                        sb.append(n);
                        sb.append(")");
                        return sb.toString();
                    }
                    case 21: {
                        return "API_VERSION_UPDATE_REQUIRED";
                    }
                    case 20: {
                        return "RESTRICTED_PROFILE";
                    }
                    case 19: {
                        return "SERVICE_MISSING_PERMISSION";
                    }
                    case 18: {
                        return "SERVICE_UPDATING";
                    }
                    case 17: {
                        return "SIGN_IN_FAILED";
                    }
                    case 16: {
                        return "API_UNAVAILABLE";
                    }
                    case 15: {
                        return "INTERRUPTED";
                    }
                    case 14: {
                        return "TIMEOUT";
                    }
                    case 13: {
                        return "CANCELED";
                    }
                }
                break;
            }
            case 11: {
                return "LICENSE_CHECK_FAILED";
            }
            case 10: {
                return "DEVELOPER_ERROR";
            }
            case 9: {
                return "SERVICE_INVALID";
            }
            case 8: {
                return "INTERNAL_ERROR";
            }
            case 7: {
                return "NETWORK_ERROR";
            }
            case 6: {
                return "RESOLUTION_REQUIRED";
            }
            case 5: {
                return "INVALID_ACCOUNT";
            }
            case 4: {
                return "SIGN_IN_REQUIRED";
            }
            case 3: {
                return "SERVICE_DISABLED";
            }
            case 2: {
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            }
            case 1: {
                return "SERVICE_MISSING";
            }
            case 0: {
                return "SUCCESS";
            }
            case -1: {
                return "UNKNOWN";
            }
        }
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ConnectionResult)) {
            return false;
        }
        final ConnectionResult connectionResult = (ConnectionResult)o;
        return this.zzc == connectionResult.zzc && zzak.zza(this.zzd, connectionResult.zzd) && zzak.zza(this.zze, connectionResult.zze);
    }
    
    public final int getErrorCode() {
        return this.zzc;
    }
    
    public final String getErrorMessage() {
        return this.zze;
    }
    
    public final PendingIntent getResolution() {
        return this.zzd;
    }
    
    public final boolean hasResolution() {
        return this.zzc != 0 && this.zzd != null;
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zzc, this.zzd, this.zze });
    }
    
    public final boolean isSuccess() {
        return this.zzc == 0;
    }
    
    @Override
    public final String toString() {
        return zzak.zza(this).zza("statusCode", zza(this.zzc)).zza("resolution", this.zzd).zza("message", this.zze).toString();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzb);
        zzbig.zza(parcel, 2, this.getErrorCode());
        zzbig.zza(parcel, 3, (Parcelable)this.getResolution(), n, false);
        zzbig.zza(parcel, 4, this.getErrorMessage(), false);
        zzbig.zza(parcel, zza);
    }
}
