package com.google.android.gms.common;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Feature extends zzbid
{
    public static final Parcelable.Creator<Feature> CREATOR;
    public final String name;
    public final int version;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    public Feature(final String name, final int version) {
        this.name = name;
        this.version = version;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.name, false);
        zzbig.zza(parcel, 2, this.version);
        zzbig.zza(parcel, zza);
    }
}
