package com.google.android.gms.common.util;

import java.util.Iterator;
import java.util.HashMap;

public final class zzn
{
    public static void zza(final StringBuilder sb, final HashMap<String, String> hashMap) {
        sb.append("{");
        final Iterator<String> iterator = hashMap.keySet().iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = iterator.next();
            if (n == 0) {
                sb.append(",");
            }
            else {
                n = 0;
            }
            final String s2 = hashMap.get(s);
            sb.append("\"");
            sb.append(s);
            sb.append("\":");
            if (s2 == null) {
                sb.append("null");
            }
            else {
                sb.append("\"");
                sb.append(s2);
                sb.append("\"");
            }
        }
        sb.append("}");
    }
}
