package com.google.android.gms.common.util;

import android.content.pm.PackageManager;
import com.google.android.gms.internal.zzbjr;
import android.content.Context;

public final class zzd
{
    public static boolean zzb(final Context context, final String s) {
        "com.google.android.gms".equals(s);
        try {
            return (zzbjr.zza(context).zza(s, 0).flags & 0x200000) != 0x0;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
}
