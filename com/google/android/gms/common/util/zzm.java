package com.google.android.gms.common.util;

import java.util.regex.Matcher;
import android.text.TextUtils;
import java.util.regex.Pattern;

public final class zzm
{
    private static final Pattern zza;
    private static final Pattern zzb;
    
    static {
        zza = Pattern.compile("\\\\.");
        zzb = Pattern.compile("[\\\\\"/\b\f\n\r\t]");
    }
    
    public static String zzb(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return s;
        }
        final Matcher matcher = zzm.zzb.matcher(s);
        StringBuffer sb = null;
        while (matcher.find()) {
            StringBuffer sb2;
            if ((sb2 = sb) == null) {
                sb2 = new StringBuffer();
            }
            final char char1 = matcher.group().charAt(0);
            if (char1 != '\"') {
                if (char1 != '/') {
                    if (char1 != '\\') {
                        Label_0170: {
                            switch (char1) {
                                default: {
                                    switch (char1) {
                                        default: {
                                            break Label_0170;
                                        }
                                        case 13: {
                                            matcher.appendReplacement(sb2, "\\\\r");
                                            sb = sb2;
                                            continue;
                                        }
                                        case 12: {
                                            matcher.appendReplacement(sb2, "\\\\f");
                                            sb = sb2;
                                            continue;
                                        }
                                    }
                                    break;
                                }
                                case 10: {
                                    matcher.appendReplacement(sb2, "\\\\n");
                                    sb = sb2;
                                    continue;
                                }
                                case 9: {
                                    matcher.appendReplacement(sb2, "\\\\t");
                                    break;
                                }
                                case 8: {
                                    matcher.appendReplacement(sb2, "\\\\b");
                                    sb = sb2;
                                    continue;
                                }
                            }
                        }
                        sb = sb2;
                    }
                    else {
                        matcher.appendReplacement(sb2, "\\\\\\\\");
                        sb = sb2;
                    }
                }
                else {
                    matcher.appendReplacement(sb2, "\\\\/");
                    sb = sb2;
                }
            }
            else {
                matcher.appendReplacement(sb2, "\\\\\\\"");
                sb = sb2;
            }
        }
        if (sb == null) {
            return s;
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}
