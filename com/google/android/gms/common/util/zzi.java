package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;

public final class zzi
{
    private static Boolean zzc;
    private static Boolean zzd;
    private static Boolean zze;
    
    @TargetApi(20)
    public static boolean zza(final Context context) {
        if (zzi.zzc == null) {
            zzi.zzc = (zzo.zzf() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return zzi.zzc;
    }
    
    @TargetApi(24)
    public static boolean zzb(final Context context) {
        return (!zzo.zzj() || zzc(context)) && zza(context);
    }
    
    @TargetApi(21)
    public static boolean zzc(final Context context) {
        if (zzi.zzd == null) {
            zzi.zzd = (zzo.zzg() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return zzi.zzd;
    }
    
    public static boolean zzd(final Context context) {
        if (zzi.zze == null) {
            zzi.zze = (context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return zzi.zze;
    }
}
