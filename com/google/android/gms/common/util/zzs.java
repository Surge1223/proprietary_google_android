package com.google.android.gms.common.util;

import android.os.Looper;

public final class zzs
{
    public static boolean zza() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
