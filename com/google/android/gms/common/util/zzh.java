package com.google.android.gms.common.util;

public final class zzh implements Clock
{
    private static zzh zza;
    
    static {
        zzh.zza = new zzh();
    }
    
    public static Clock zza() {
        return zzh.zza;
    }
    
    @Override
    public final long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
