package com.google.android.gms.common.util;

import java.util.ArrayList;
import java.util.List;

public final class zze
{
    public static <T> List<T> zza(final int n, final T t) {
        final ArrayList<T> list = new ArrayList<T>(Math.max(1, 1));
        list.add(t);
        return list;
    }
}
