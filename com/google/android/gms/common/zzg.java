package com.google.android.gms.common;

import java.util.Arrays;

final class zzg extends zzf
{
    private final byte[] zza;
    
    zzg(final byte[] zza) {
        super(Arrays.copyOfRange(zza, 0, 25));
        this.zza = zza;
    }
    
    @Override
    final byte[] zza() {
        return this.zza;
    }
}
