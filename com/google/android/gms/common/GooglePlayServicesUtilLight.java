package com.google.android.gms.common;

import android.annotation.TargetApi;
import android.os.Bundle;
import java.util.Iterator;
import android.os.UserManager;
import android.content.pm.PackageInstaller$SessionInfo;
import com.google.android.gms.common.util.zzo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.common.internal.zzaj;
import android.content.res.Resources;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.Context;
import java.util.concurrent.atomic.AtomicBoolean;

public class GooglePlayServicesUtilLight
{
    @Deprecated
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE;
    static final AtomicBoolean zza;
    private static boolean zzb;
    private static boolean zzc;
    private static boolean zzd;
    private static boolean zze;
    private static final AtomicBoolean zzf;
    
    static {
        GOOGLE_PLAY_SERVICES_VERSION_CODE = 12438000;
        GooglePlayServicesUtilLight.zzb = false;
        GooglePlayServicesUtilLight.zzc = false;
        GooglePlayServicesUtilLight.zzd = false;
        GooglePlayServicesUtilLight.zze = false;
        zza = new AtomicBoolean();
        zzf = new AtomicBoolean();
    }
    
    @Deprecated
    public static int getApkVersion(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }
    
    @Deprecated
    public static String getErrorString(final int n) {
        return ConnectionResult.zza(n);
    }
    
    public static Context getRemoteContext(Context packageContext) {
        try {
            packageContext = packageContext.createPackageContext("com.google.android.gms", 3);
            return packageContext;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    public static Resources getRemoteResource(final Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @Deprecated
    public static int isGooglePlayServicesAvailable(final Context context) {
        return isGooglePlayServicesAvailable(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }
    
    @Deprecated
    public static int isGooglePlayServicesAvailable(final Context context, int google_PLAY_SERVICES_VERSION_CODE) {
        try {
            context.getResources().getString(R.string.common_google_play_services_unknown_issue);
        }
        catch (Throwable t) {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName()) && !GooglePlayServicesUtilLight.zzf.get()) {
            final int zzb = zzaj.zzb(context);
            if (zzb == 0) {
                throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            }
            if (zzb != GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE) {
                google_PLAY_SERVICES_VERSION_CODE = GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
                final StringBuilder sb = new StringBuilder(320);
                sb.append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
                sb.append(google_PLAY_SERVICES_VERSION_CODE);
                sb.append(" but found ");
                sb.append(zzb);
                sb.append(".  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
                throw new IllegalStateException(sb.toString());
            }
        }
        return zza(context, !zzi.zzb(context) && !zzi.zzd(context), google_PLAY_SERVICES_VERSION_CODE);
    }
    
    @Deprecated
    public static boolean isPlayServicesPossiblyUpdating(final Context context, final int n) {
        return n == 18 || (n == 1 && zza(context, "com.google.android.gms"));
    }
    
    @Deprecated
    public static boolean isUserRecoverableError(final int n) {
        if (n != 9) {
            switch (n) {
                default: {
                    return false;
                }
                case 1:
                case 2:
                case 3: {
                    break;
                }
            }
        }
        return true;
    }
    
    private static int zza(final Context context, final boolean b, final int n) {
        zzau.zzb(n >= 0);
        final PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        if (b) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            }
            catch (PackageManager$NameNotFoundException ex2) {
                Log.w("GooglePlayServicesUtil", "Google Play Store is missing.");
                return 9;
            }
        }
        try {
            final PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            GoogleSignatureVerifier.getInstance(context);
            if (!GoogleSignatureVerifier.zza(packageInfo2, true)) {
                Log.w("GooglePlayServicesUtil", "Google Play services signature invalid.");
                return 9;
            }
            if (b && (!GoogleSignatureVerifier.zza(packageInfo, true) || !packageInfo.signatures[0].equals((Object)packageInfo2.signatures[0]))) {
                Log.w("GooglePlayServicesUtil", "Google Play Store signature invalid.");
                return 9;
            }
            if (packageInfo2.versionCode / 1000 < n / 1000) {
                final int versionCode = packageInfo2.versionCode;
                final StringBuilder sb = new StringBuilder(77);
                sb.append("Google Play services out of date.  Requires ");
                sb.append(n);
                sb.append(" but found ");
                sb.append(versionCode);
                Log.w("GooglePlayServicesUtil", sb.toString());
                return 2;
            }
            ApplicationInfo applicationInfo;
            if ((applicationInfo = packageInfo2.applicationInfo) == null) {
                try {
                    applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                }
                catch (PackageManager$NameNotFoundException ex) {
                    Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", (Throwable)ex);
                    return 1;
                }
            }
            if (!applicationInfo.enabled) {
                return 3;
            }
            return 0;
        }
        catch (PackageManager$NameNotFoundException ex3) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 1;
        }
    }
    
    @TargetApi(21)
    static boolean zza(final Context context, final String s) {
        final boolean equals = s.equals("com.google.android.gms");
        if (zzo.zzg()) {
            try {
                final Iterator<PackageInstaller$SessionInfo> iterator = (Iterator<PackageInstaller$SessionInfo>)context.getPackageManager().getPackageInstaller().getAllSessions().iterator();
                while (iterator.hasNext()) {
                    if (s.equals(iterator.next().getAppPackageName())) {
                        return true;
                    }
                }
            }
            catch (Exception ex) {
                return false;
            }
        }
        final PackageManager packageManager = context.getPackageManager();
        try {
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(s, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            if (applicationInfo.enabled) {
                boolean b = false;
                Label_0152: {
                    if (zzo.zzd()) {
                        final Bundle applicationRestrictions = ((UserManager)context.getSystemService("user")).getApplicationRestrictions(context.getPackageName());
                        if (applicationRestrictions != null && "true".equals(applicationRestrictions.getString("restricted_profile"))) {
                            b = true;
                            break Label_0152;
                        }
                    }
                    b = false;
                }
                if (!b) {
                    return true;
                }
            }
            return false;
        }
        catch (PackageManager$NameNotFoundException ex2) {
            return false;
        }
    }
}
