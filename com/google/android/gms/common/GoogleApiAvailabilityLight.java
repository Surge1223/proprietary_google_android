package com.google.android.gms.common;

import android.app.PendingIntent;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.common.internal.zzs;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.google.android.gms.internal.zzbjr;
import android.text.TextUtils;
import android.content.Context;

public class GoogleApiAvailabilityLight
{
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE;
    private static final GoogleApiAvailabilityLight zza;
    
    static {
        GOOGLE_PLAY_SERVICES_VERSION_CODE = GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        zza = new GoogleApiAvailabilityLight();
    }
    
    public static GoogleApiAvailabilityLight getInstance() {
        return GoogleApiAvailabilityLight.zza;
    }
    
    private static String zza(final Context context, final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
        sb.append("-");
        if (!TextUtils.isEmpty((CharSequence)s)) {
            sb.append(s);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(zzbjr.zza(context).zzb(context.getPackageName(), 0).versionCode);
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        return sb.toString();
    }
    
    public int getApkVersion(final Context context) {
        return GooglePlayServicesUtilLight.getApkVersion(context);
    }
    
    public Intent getErrorResolutionIntent(final Context context, final int n, final String s) {
        switch (n) {
            default: {
                return null;
            }
            case 3: {
                return zzs.zza("com.google.android.gms");
            }
            case 1:
            case 2: {
                if (context != null && zzi.zzb(context)) {
                    return zzs.zza();
                }
                return zzs.zza("com.google.android.gms", zza(context, s));
            }
        }
    }
    
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2) {
        return this.getErrorResolutionPendingIntent(context, n, n2, null);
    }
    
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2, final String s) {
        final Intent errorResolutionIntent = this.getErrorResolutionIntent(context, n, s);
        if (errorResolutionIntent == null) {
            return null;
        }
        return PendingIntent.getActivity(context, n2, errorResolutionIntent, 134217728);
    }
    
    public String getErrorString(final int n) {
        return GooglePlayServicesUtilLight.getErrorString(n);
    }
    
    public int isGooglePlayServicesAvailable(final Context context) {
        return this.isGooglePlayServicesAvailable(context, GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }
    
    public int isGooglePlayServicesAvailable(final Context context, int googlePlayServicesAvailable) {
        if (GooglePlayServicesUtilLight.isPlayServicesPossiblyUpdating(context, googlePlayServicesAvailable = GooglePlayServicesUtilLight.isGooglePlayServicesAvailable(context, googlePlayServicesAvailable))) {
            googlePlayServicesAvailable = 18;
        }
        return googlePlayServicesAvailable;
    }
    
    public boolean isUserResolvableError(final int n) {
        return GooglePlayServicesUtilLight.isUserRecoverableError(n);
    }
}
