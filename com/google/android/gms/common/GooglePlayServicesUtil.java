package com.google.android.gms.common;

import android.app.Dialog;
import com.google.android.gms.common.internal.zzg;
import android.support.v4.app.Fragment;
import android.content.DialogInterface$OnCancelListener;
import android.app.Activity;
import android.content.res.Resources;
import android.content.Context;

public final class GooglePlayServicesUtil extends GooglePlayServicesUtilLight
{
    @Deprecated
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE;
    
    static {
        GOOGLE_PLAY_SERVICES_VERSION_CODE = GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }
    
    public static Resources getRemoteResource(final Context context) {
        return GooglePlayServicesUtilLight.getRemoteResource(context);
    }
    
    @Deprecated
    public static int isGooglePlayServicesAvailable(final Context context) {
        return GooglePlayServicesUtilLight.isGooglePlayServicesAvailable(context);
    }
    
    @Deprecated
    public static boolean isPlayServicesPossiblyUpdating(final Context context, final int n) {
        return GooglePlayServicesUtilLight.isPlayServicesPossiblyUpdating(context, n);
    }
    
    @Deprecated
    public static boolean showErrorDialogFragment(final int n, final Activity activity, final int n2) {
        return showErrorDialogFragment(n, activity, n2, null);
    }
    
    @Deprecated
    public static boolean showErrorDialogFragment(final int n, final Activity activity, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        return showErrorDialogFragment(n, activity, null, n2, dialogInterface$OnCancelListener);
    }
    
    public static boolean showErrorDialogFragment(final int n, final Activity activity, final Fragment fragment, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        int n3 = n;
        if (isPlayServicesPossiblyUpdating((Context)activity, n)) {
            n3 = 18;
        }
        final GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        if (fragment == null) {
            return instance.showErrorDialogFragment(activity, n3, n2, dialogInterface$OnCancelListener);
        }
        final Dialog zza = GoogleApiAvailability.zza((Context)activity, n3, zzg.zza(fragment, GoogleApiAvailability.getInstance().getErrorResolutionIntent((Context)activity, n3, "d"), n2), dialogInterface$OnCancelListener);
        if (zza == null) {
            return false;
        }
        GoogleApiAvailability.zza(activity, zza, "GooglePlayServicesErrorDialog", dialogInterface$OnCancelListener);
        return true;
    }
}
