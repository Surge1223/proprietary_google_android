package com.google.android.gms.common.images;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Locale;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.net.Uri;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class WebImage extends zzbid
{
    public static final Parcelable.Creator<WebImage> CREATOR;
    private final int zza;
    private final Uri zzb;
    private final int zzc;
    private final int zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    WebImage(final int zza, final Uri zzb, final int zzc, final int zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && o instanceof WebImage) {
            final WebImage webImage = (WebImage)o;
            return zzak.zza(this.zzb, webImage.zzb) && this.zzc == webImage.zzc && this.zzd == webImage.zzd;
        }
        return false;
    }
    
    public final int getHeight() {
        return this.zzd;
    }
    
    public final Uri getUrl() {
        return this.zzb;
    }
    
    public final int getWidth() {
        return this.zzc;
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zzb, this.zzc, this.zzd });
    }
    
    @Override
    public final String toString() {
        return String.format(Locale.US, "Image %dx%d %s", this.zzc, this.zzd, this.zzb.toString());
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, (Parcelable)this.getUrl(), n, false);
        zzbig.zza(parcel, 3, this.getWidth());
        zzbig.zza(parcel, 4, this.getHeight());
        zzbig.zza(parcel, zza);
    }
}
