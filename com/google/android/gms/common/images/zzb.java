package com.google.android.gms.common.images;

import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.net.Uri;

final class zzb
{
    public final Uri zza;
    
    public zzb(final Uri zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof zzb && (this == o || zzak.zza(((zzb)o).zza, this.zza));
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza });
    }
}
