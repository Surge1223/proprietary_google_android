package com.google.android.gms.common.images;

import android.os.SystemClock;
import com.google.android.gms.common.internal.zzc;
import java.util.concurrent.CountDownLatch;
import java.io.IOException;
import android.util.Log;
import android.graphics.BitmapFactory;
import android.os.Looper;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.graphics.drawable.Drawable;
import android.os.ParcelFileDescriptor;
import android.os.Bundle;
import java.util.ArrayList;
import com.google.android.gms.common.annotation.KeepName;
import android.os.ResultReceiver;
import java.util.Map;
import com.google.android.gms.internal.zzbic;
import java.util.concurrent.ExecutorService;
import android.os.Handler;
import android.content.Context;
import android.net.Uri;
import java.util.HashSet;

public final class ImageManager
{
    private static final Object zza;
    private static HashSet<Uri> zzb;
    private final Context zzd;
    private final Handler zze;
    private final ExecutorService zzf;
    private final zza zzg;
    private final zzbic zzh;
    private final Map<com.google.android.gms.common.images.zza, ImageReceiver> zzi;
    private final Map<Uri, ImageReceiver> zzj;
    private final Map<Uri, Long> zzk;
    
    static {
        zza = new Object();
        ImageManager.zzb = new HashSet<Uri>();
    }
    
    @KeepName
    final class ImageReceiver extends ResultReceiver
    {
        private final Uri zza;
        private final ArrayList<com.google.android.gms.common.images.zza> zzb;
        private final /* synthetic */ ImageManager zzc;
        
        public final void onReceiveResult(final int n, final Bundle bundle) {
            this.zzc.zzf.execute(this.zzc.new zzb(this.zza, (ParcelFileDescriptor)bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }
    }
    
    public interface OnImageLoadedListener
    {
        void onImageLoaded(final Uri p0, final Drawable p1, final boolean p2);
    }
    
    static final class zza extends LruCache<com.google.android.gms.common.images.zzb, Bitmap>
    {
    }
    
    final class zzb implements Runnable
    {
        private final Uri zza;
        private final ParcelFileDescriptor zzb;
        
        public zzb(final Uri zza, final ParcelFileDescriptor zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        @Override
        public final void run() {
            if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
                boolean b = false;
                final boolean b2 = false;
                Bitmap decodeFileDescriptor = null;
                final Bitmap bitmap = null;
                if (this.zzb != null) {
                    try {
                        decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(this.zzb.getFileDescriptor());
                        b = b2;
                    }
                    catch (OutOfMemoryError outOfMemoryError) {
                        final String value = String.valueOf(this.zza);
                        final StringBuilder sb = new StringBuilder(34 + String.valueOf(value).length());
                        sb.append("OOM while loading bitmap for uri: ");
                        sb.append(value);
                        Log.e("ImageManager", sb.toString(), (Throwable)outOfMemoryError);
                        b = true;
                        decodeFileDescriptor = bitmap;
                    }
                    try {
                        this.zzb.close();
                    }
                    catch (IOException ex) {
                        Log.e("ImageManager", "closed failed", (Throwable)ex);
                    }
                }
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                ImageManager.this.zze.post((Runnable)new zzd(this.zza, decodeFileDescriptor, b, countDownLatch));
                try {
                    countDownLatch.await();
                    return;
                }
                catch (InterruptedException ex2) {
                    final String value2 = String.valueOf(this.zza);
                    final StringBuilder sb2 = new StringBuilder(32 + String.valueOf(value2).length());
                    sb2.append("Latch interrupted while posting ");
                    sb2.append(value2);
                    Log.w("ImageManager", sb2.toString());
                    return;
                }
            }
            final String value3 = String.valueOf(Thread.currentThread());
            final String value4 = String.valueOf(Looper.getMainLooper().getThread());
            final StringBuilder sb3 = new StringBuilder(56 + String.valueOf(value3).length() + String.valueOf(value4).length());
            sb3.append("checkNotMainThread: current thread ");
            sb3.append(value3);
            sb3.append(" IS the main thread ");
            sb3.append(value4);
            sb3.append("!");
            Log.e("Asserts", sb3.toString());
            throw new IllegalStateException("LoadBitmapFromDiskRunnable can't be executed in the main thread");
        }
    }
    
    final class zzd implements Runnable
    {
        private final Uri zza;
        private final Bitmap zzb;
        private final CountDownLatch zzc;
        private boolean zzd;
        
        public zzd(final Uri zza, final Bitmap zzb, final boolean zzd, final CountDownLatch zzc) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzd = zzd;
            this.zzc = zzc;
        }
        
        @Override
        public final void run() {
            com.google.android.gms.common.internal.zzc.zza("OnBitmapLoadedRunnable must be executed in the main thread");
            final boolean b = this.zzb != null;
            if (ImageManager.this.zzg != null) {
                if (this.zzd) {
                    ImageManager.this.zzg.evictAll();
                    System.gc();
                    this.zzd = false;
                    ImageManager.this.zze.post((Runnable)this);
                    return;
                }
                if (b) {
                    ImageManager.this.zzg.put(new com.google.android.gms.common.images.zzb(this.zza), this.zzb);
                }
            }
            final ImageReceiver imageReceiver = ImageManager.this.zzj.remove(this.zza);
            if (imageReceiver != null) {
                final ArrayList zza = imageReceiver.zzb;
                for (int size = zza.size(), i = 0; i < size; ++i) {
                    final com.google.android.gms.common.images.zza zza2 = zza.get(i);
                    if (b) {
                        zza2.zza(ImageManager.this.zzd, this.zzb, false);
                    }
                    else {
                        ImageManager.this.zzk.put(this.zza, SystemClock.elapsedRealtime());
                        zza2.zza(ImageManager.this.zzd, ImageManager.this.zzh, false);
                    }
                    if (!(zza2 instanceof com.google.android.gms.common.images.zzd)) {
                        ImageManager.this.zzi.remove(zza2);
                    }
                }
            }
            this.zzc.countDown();
            synchronized (ImageManager.zza) {
                ImageManager.zzb.remove(this.zza);
            }
        }
    }
}
