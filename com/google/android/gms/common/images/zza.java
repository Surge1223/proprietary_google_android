package com.google.android.gms.common.images;

import com.google.android.gms.internal.zzbic;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import com.google.android.gms.common.internal.zzc;
import android.graphics.Bitmap;
import android.content.Context;

public abstract class zza
{
    final zzb zza;
    protected int zzb;
    
    final void zza(final Context context, final Bitmap bitmap, final boolean b) {
        zzc.zza(bitmap);
        this.zza((Drawable)new BitmapDrawable(context.getResources(), bitmap), b, false, true);
    }
    
    final void zza(final Context context, final zzbic zzbic, final boolean b) {
        Drawable drawable;
        if (this.zzb != 0) {
            drawable = context.getResources().getDrawable(this.zzb);
        }
        else {
            drawable = null;
        }
        this.zza(drawable, b, false, false);
    }
    
    protected abstract void zza(final Drawable p0, final boolean p1, final boolean p2, final boolean p3);
}
