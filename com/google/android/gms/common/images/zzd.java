package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import java.lang.ref.WeakReference;

public final class zzd extends zza
{
    private WeakReference<ImageManager.OnImageLoadedListener> zzc;
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzd)) {
            return false;
        }
        if (this == o) {
            return true;
        }
        final zzd zzd = (zzd)o;
        final ImageManager.OnImageLoadedListener onImageLoadedListener = this.zzc.get();
        final ImageManager.OnImageLoadedListener onImageLoadedListener2 = zzd.zzc.get();
        return onImageLoadedListener2 != null && onImageLoadedListener != null && zzak.zza(onImageLoadedListener2, onImageLoadedListener) && zzak.zza(zzd.zza, this.zza);
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza });
    }
    
    @Override
    protected final void zza(final Drawable drawable, final boolean b, final boolean b2, final boolean b3) {
        if (!b2) {
            final ImageManager.OnImageLoadedListener onImageLoadedListener = this.zzc.get();
            if (onImageLoadedListener != null) {
                onImageLoadedListener.onImageLoaded(this.zza.zza, drawable, b3);
            }
        }
    }
}
