package com.google.android.gms.common;

import android.content.res.TypedArray;
import com.google.android.gms.dynamic.zzq;
import com.google.android.gms.common.internal.zzbc;
import android.util.Log;
import com.google.android.gms.common.internal.zzbb;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.base.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

public final class SignInButton extends FrameLayout implements View.OnClickListener
{
    private int zza;
    private int zzb;
    private View zzc;
    private View.OnClickListener zzd;
    
    public SignInButton(final Context context) {
        this(context, null);
    }
    
    public SignInButton(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public SignInButton(Context obtainStyledAttributes, final AttributeSet set, final int n) {
        super(obtainStyledAttributes, set, n);
        this.zzd = null;
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.SignInButton, 0, 0);
        try {
            this.zza = ((TypedArray)obtainStyledAttributes).getInt(R.styleable.SignInButton_buttonSize, 0);
            this.zzb = ((TypedArray)obtainStyledAttributes).getInt(R.styleable.SignInButton_colorScheme, 2);
            ((TypedArray)obtainStyledAttributes).recycle();
            this.setStyle(this.zza, this.zzb);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    public final void onClick(final View view) {
        if (this.zzd != null && view == this.zzc) {
            this.zzd.onClick((View)this);
        }
    }
    
    public final void setColorScheme(final int n) {
        this.setStyle(this.zza, n);
    }
    
    public final void setEnabled(final boolean b) {
        super.setEnabled(b);
        this.zzc.setEnabled(b);
    }
    
    public final void setOnClickListener(final View.OnClickListener zzd) {
        this.zzd = zzd;
        if (this.zzc != null) {
            this.zzc.setOnClickListener((View.OnClickListener)this);
        }
    }
    
    @Deprecated
    public final void setScopes(final Scope[] array) {
        this.setStyle(this.zza, this.zzb);
    }
    
    public final void setSize(final int n) {
        this.setStyle(n, this.zzb);
    }
    
    public final void setStyle(int zza, int zzb) {
        this.zza = zza;
        this.zzb = zzb;
        final Context context = this.getContext();
        if (this.zzc != null) {
            this.removeView(this.zzc);
        }
        try {
            this.zzc = zzbb.zza(context, this.zza, this.zzb);
        }
        catch (zzq zzq) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            zza = this.zza;
            zzb = this.zzb;
            final zzbc zzc = new zzbc(context);
            zzc.zza(context.getResources(), zza, zzb);
            this.zzc = (View)zzc;
        }
        this.addView(this.zzc);
        this.zzc.setEnabled(this.isEnabled());
        this.zzc.setOnClickListener((View.OnClickListener)this);
    }
}
