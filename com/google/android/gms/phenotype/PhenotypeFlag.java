package com.google.android.gms.phenotype;

import android.net.Uri;
import android.support.v4.content.PermissionChecker;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.util.Log;
import android.os.UserManager;
import com.google.android.gms.internal.zzfcx;
import android.os.Binder;
import android.os.Build.VERSION;
import com.google.android.gms.internal.zzfdo;
import android.annotation.SuppressLint;
import android.content.Context;

@Deprecated
public abstract class PhenotypeFlag<T>
{
    private static final Object zzb;
    @SuppressLint({ "StaticFieldLeak" })
    private static Context zzc;
    private static boolean zzd;
    private static Boolean zzf;
    final String zza;
    private final Factory zzg;
    private final String zzh;
    private final T zzi;
    private T zzj;
    
    static {
        zzb = new Object();
        PhenotypeFlag.zzc = null;
        PhenotypeFlag.zzd = false;
        PhenotypeFlag.zzf = null;
    }
    
    private PhenotypeFlag(final Factory zzg, String value, final T zzi) {
        this.zzj = null;
        if (zzg.zza == null && zzg.zzb == null) {
            throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
        }
        if (zzg.zza != null && zzg.zzb != null) {
            throw new IllegalArgumentException("Must pass one of SharedPreferences file name or ContentProvider URI");
        }
        this.zzg = zzg;
        final String value2 = String.valueOf(zzg.zzc);
        final String value3 = String.valueOf(value);
        String concat;
        if (value3.length() != 0) {
            concat = value2.concat(value3);
        }
        else {
            concat = new String(value2);
        }
        this.zzh = concat;
        final String value4 = String.valueOf(zzg.zzd);
        value = String.valueOf(value);
        String concat2;
        if (value.length() != 0) {
            concat2 = value4.concat(value);
        }
        else {
            concat2 = new String(value4);
        }
        this.zza = concat2;
        this.zzi = zzi;
    }
    
    public static void init(Context zzc) {
        zzfdo.zza(zzc);
        synchronized (PhenotypeFlag.zzb) {
            if (Build.VERSION.SDK_INT < 24 || !zzc.isDeviceProtectedStorage()) {
                final Context applicationContext = zzc.getApplicationContext();
                if (applicationContext != null) {
                    zzc = applicationContext;
                }
            }
            if (PhenotypeFlag.zzc != zzc) {
                PhenotypeFlag.zzf = null;
            }
            PhenotypeFlag.zzc = zzc;
            // monitorexit(PhenotypeFlag.zzb)
            PhenotypeFlag.zzd = false;
        }
    }
    
    private static <V> V zza(zza<V> zza) {
        long clearCallingIdentity;
        try {
            zza = zza.zza();
            return (V)zza;
        }
        catch (SecurityException ex) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            final Object o = zza;
            final zza<Object> zza2 = zza = ((zza<zza<Object>>)o).zza();
        }
        try {
            final Object o = zza;
            zza = ((zza<zza<Object>>)o).zza();
            return (V)zza;
        }
        finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }
    
    static boolean zza(final String s, final boolean b) {
        return zzd() && zza((zza<Boolean>)new zzak(s, false));
    }
    
    private static PhenotypeFlag<Boolean> zzb(final Factory factory, final String s, final boolean b) {
        return new zzan(factory, s, Boolean.valueOf(b));
    }
    
    @TargetApi(24)
    private final T zzb() {
        if (!zza("gms:phenotype:phenotype_flag:debug_bypass_phenotype", false)) {
            if (this.zzg.zzb != null) {
                final String s = zza((zza<String>)new zzai(this, ConfigurationContentLoader.getLoader(PhenotypeFlag.zzc.getContentResolver(), this.zzg.zzb)));
                if (s != null) {
                    return this.fromString(s);
                }
            }
            else if (this.zzg.zza != null) {
                if (Build.VERSION.SDK_INT >= 24 && !PhenotypeFlag.zzc.isDeviceProtectedStorage() && !((UserManager)PhenotypeFlag.zzc.getSystemService((Class)UserManager.class)).isUserUnlocked()) {
                    return null;
                }
                final SharedPreferences sharedPreferences = PhenotypeFlag.zzc.getSharedPreferences(this.zzg.zza, 0);
                if (sharedPreferences.contains(this.zza)) {
                    return this.fromSharedPreferences(sharedPreferences);
                }
            }
        }
        else {
            final String value = String.valueOf(this.zza);
            String concat;
            if (value.length() != 0) {
                concat = "Bypass reading Phenotype values for flag: ".concat(value);
            }
            else {
                concat = new String("Bypass reading Phenotype values for flag: ");
            }
            Log.w("PhenotypeFlag", concat);
        }
        return null;
    }
    
    private final T zzc() {
        if (!this.zzg.zze && zzd()) {
            final String s = zza((zza<String>)new zzaj(this));
            if (s != null) {
                return this.fromString(s);
            }
        }
        return null;
    }
    
    private static boolean zzd() {
        if (PhenotypeFlag.zzf == null) {
            final Context zzc = PhenotypeFlag.zzc;
            boolean b = false;
            if (zzc == null) {
                return false;
            }
            if (PermissionChecker.checkCallingOrSelfPermission(PhenotypeFlag.zzc, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0) {
                b = true;
            }
            PhenotypeFlag.zzf = b;
        }
        return PhenotypeFlag.zzf;
    }
    
    public abstract T fromSharedPreferences(final SharedPreferences p0);
    
    public abstract T fromString(final String p0);
    
    public T get() {
        if (this.zzj != null) {
            return this.zzj;
        }
        if (PhenotypeFlag.zzd) {
            final String value = String.valueOf(this.zza);
            String concat;
            if (value.length() != 0) {
                concat = "Test mode, using default for flag: ".concat(value);
            }
            else {
                concat = new String("Test mode, using default for flag: ");
            }
            Log.w("PhenotypeFlag", concat);
            return this.zzi;
        }
        if (PhenotypeFlag.zzc != null) {
            if (this.zzg.zzf) {
                final T zzc = this.zzc();
                if (zzc != null) {
                    return zzc;
                }
                final T zzb = this.zzb();
                if (zzb != null) {
                    return zzb;
                }
            }
            else {
                final T zzb2 = this.zzb();
                if (zzb2 != null) {
                    return zzb2;
                }
                final T zzc2 = this.zzc();
                if (zzc2 != null) {
                    return zzc2;
                }
            }
            return this.zzi;
        }
        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
    }
    
    public static class Factory
    {
        private final String zza;
        private final Uri zzb;
        private final String zzc;
        private final String zzd;
        private final boolean zze;
        private final boolean zzf;
        
        public Factory(final Uri uri) {
            this(null, uri, "", "", false, false);
        }
        
        private Factory(final String zza, final Uri zzb, final String zzc, final String zzd, final boolean zze, final boolean zzf) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
            this.zzd = zzd;
            this.zze = zze;
            this.zzf = zzf;
        }
        
        public PhenotypeFlag<Boolean> createFlag(final String s, final boolean b) {
            return zzb(this, s, b);
        }
    }
    
    interface zza<V>
    {
        V zza();
    }
}
