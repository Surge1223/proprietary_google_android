package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzfcx;
import android.util.Log;
import android.content.SharedPreferences;

final class zzan extends PhenotypeFlag<Boolean>
{
    zzan(final Factory factory, final String s, final Boolean b) {
        super(factory, s, b, null);
    }
    
    private final Boolean zza(final SharedPreferences sharedPreferences) {
        try {
            return sharedPreferences.getBoolean(this.zza, false);
        }
        catch (ClassCastException ex) {
            final String value = String.valueOf(this.zza);
            String concat;
            if (value.length() != 0) {
                concat = "Invalid boolean value in SharedPreferences for ".concat(value);
            }
            else {
                concat = new String("Invalid boolean value in SharedPreferences for ");
            }
            Log.e("PhenotypeFlag", concat, (Throwable)ex);
            return null;
        }
    }
}
