package com.google.android.gms.phenotype;

import android.os.Handler;
import android.database.ContentObserver;

final class zza extends ContentObserver
{
    private final /* synthetic */ ConfigurationContentLoader zza;
    
    zza(final ConfigurationContentLoader zza, final Handler handler) {
        this.zza = zza;
        super((Handler)null);
    }
    
    public final void onChange(final boolean b) {
        this.zza.invalidateCache();
    }
}
