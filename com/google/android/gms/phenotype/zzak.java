package com.google.android.gms.phenotype;

import android.net.Uri;
import android.support.v4.content.PermissionChecker;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.util.Log;
import android.os.UserManager;
import com.google.android.gms.internal.zzfcx;
import android.os.Binder;
import android.os.Build.VERSION;
import com.google.android.gms.internal.zzfdo;
import android.annotation.SuppressLint;
import android.content.Context;

final class zzak implements zza
{
    private final String zza;
    private final boolean zzb;
    
    zzak(final String zza, final boolean b) {
        this.zza = zza;
        this.zzb = false;
    }
    
    @Override
    public final Object zza() {
        return PhenotypeFlag.zzb(this.zza, this.zzb);
    }
}
