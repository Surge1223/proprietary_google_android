package com.google.android.gms.phenotype;

import android.os.RemoteException;
import com.google.android.gms.internal.zzdtf;
import com.google.android.gms.internal.zzdth;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.zzdus;
import com.google.android.gms.common.api.internal.zzdn;

final class zzae extends zzdn<zzdus, Configurations>
{
    private final /* synthetic */ String zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ String zzc;
    
    zzae(final PhenotypeClient phenotypeClient, final String zza, final String zzb, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}
