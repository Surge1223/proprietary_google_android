package com.google.android.gms.phenotype;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FlagOverride extends zzbid
{
    public static final Parcelable.Creator<FlagOverride> CREATOR;
    public final boolean committed;
    public final String configurationName;
    public final Flag flag;
    public final String userName;
    
    static {
        CREATOR = (Parcelable.Creator)new zzl();
    }
    
    public FlagOverride(final String configurationName, final String userName, final Flag flag, final boolean committed) {
        this.configurationName = configurationName;
        this.userName = userName;
        this.flag = flag;
        this.committed = committed;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FlagOverride)) {
            return false;
        }
        final FlagOverride flagOverride = (FlagOverride)o;
        return PhenotypeCore.zza(this.configurationName, flagOverride.configurationName) && PhenotypeCore.zza(this.userName, flagOverride.userName) && PhenotypeCore.zza(this.flag, flagOverride.flag) && this.committed == flagOverride.committed;
    }
    
    @Override
    public String toString() {
        return this.toString(new StringBuilder());
    }
    
    public String toString(final StringBuilder sb) {
        sb.append("FlagOverride(");
        sb.append(this.configurationName);
        sb.append(", ");
        sb.append(this.userName);
        sb.append(", ");
        this.flag.toString(sb);
        sb.append(", ");
        sb.append(this.committed);
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.configurationName, false);
        zzbig.zza(parcel, 3, this.userName, false);
        zzbig.zza(parcel, 4, (Parcelable)this.flag, n, false);
        zzbig.zza(parcel, 5, this.committed);
        zzbig.zza(parcel, zza);
    }
}
