package com.google.android.gms.phenotype;

import android.util.Log;
import android.content.SharedPreferences;

final class zzal extends PhenotypeFlag<Long>
{
    private final Long zza(final SharedPreferences sharedPreferences) {
        try {
            return sharedPreferences.getLong(this.zza, 0L);
        }
        catch (ClassCastException ex) {
            final String value = String.valueOf(this.zza);
            String concat;
            if (value.length() != 0) {
                concat = "Invalid long value in SharedPreferences for ".concat(value);
            }
            else {
                concat = new String("Invalid long value in SharedPreferences for ");
            }
            Log.e("PhenotypeFlag", concat, (Throwable)ex);
            return null;
        }
    }
    
    private final Long zza(final String s) {
        try {
            return Long.parseLong(s);
        }
        catch (NumberFormatException ex) {
            final String zza = this.zza;
            final StringBuilder sb = new StringBuilder(25 + String.valueOf(zza).length() + String.valueOf(s).length());
            sb.append("Invalid long value for ");
            sb.append(zza);
            sb.append(": ");
            sb.append(s);
            Log.e("PhenotypeFlag", sb.toString());
            return null;
        }
    }
}
