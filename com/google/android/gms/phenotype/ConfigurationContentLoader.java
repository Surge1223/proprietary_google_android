package com.google.android.gms.phenotype;

import android.database.Cursor;
import java.util.HashMap;
import java.util.Iterator;
import android.os.Handler;
import java.util.Map;
import android.database.ContentObserver;
import android.content.ContentResolver;
import android.net.Uri;
import java.util.concurrent.ConcurrentHashMap;

public class ConfigurationContentLoader
{
    public static final String[] COLUMNS;
    private static final ConcurrentHashMap<Uri, ConfigurationContentLoader> zza;
    private final ContentResolver zzb;
    private final Uri zzc;
    private final ContentObserver zzd;
    private final Object zze;
    private volatile Map<String, String> zzf;
    
    static {
        zza = new ConcurrentHashMap<Uri, ConfigurationContentLoader>();
        COLUMNS = new String[] { "key", "value" };
    }
    
    private ConfigurationContentLoader(final ContentResolver zzb, final Uri zzc) {
        this.zze = new Object();
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = new zza(this, null);
    }
    
    public static ConfigurationContentLoader getLoader(final ContentResolver contentResolver, final Uri uri) {
        ConfigurationContentLoader configurationContentLoader;
        if ((configurationContentLoader = ConfigurationContentLoader.zza.get(uri)) == null) {
            configurationContentLoader = new ConfigurationContentLoader(contentResolver, uri);
            final ConfigurationContentLoader configurationContentLoader2 = ConfigurationContentLoader.zza.putIfAbsent(uri, configurationContentLoader);
            if (configurationContentLoader2 == null) {
                configurationContentLoader.zzb.registerContentObserver(configurationContentLoader.zzc, false, configurationContentLoader.zzd);
            }
            else {
                configurationContentLoader = configurationContentLoader2;
            }
        }
        return configurationContentLoader;
    }
    
    public static void invalidateAllCaches() {
        final Iterator<ConfigurationContentLoader> iterator = ConfigurationContentLoader.zza.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().invalidateCache();
        }
    }
    
    private final Map<String, String> zza() {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        final Cursor query = this.zzb.query(this.zzc, ConfigurationContentLoader.COLUMNS, (String)null, (String[])null, (String)null);
        if (query != null) {
            try {
                while (query.moveToNext()) {
                    hashMap.put(query.getString(0), query.getString(1));
                }
            }
            finally {
                query.close();
            }
        }
        return hashMap;
    }
    
    public Map<String, String> getFlags() {
        Map<String, String> map;
        if (PhenotypeFlag.zza("gms:phenotype:phenotype_flag:debug_disable_caching", false)) {
            map = this.zza();
        }
        else {
            map = this.zzf;
        }
        Map<String, String> map2 = map;
        if (map == null) {
            synchronized (this.zze) {
                Map<String, String> zzf;
                map2 = (zzf = this.zzf);
                if (map2 == null) {
                    zzf = this.zza();
                    this.zzf = zzf;
                }
                // monitorexit(this.zze)
                map2 = zzf;
            }
        }
        return map2;
    }
    
    public void invalidateCache() {
        synchronized (this.zze) {
            this.zzf = null;
        }
    }
}
