package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import android.util.Base64;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.Map;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Configurations extends zzbid
{
    public static final Parcelable.Creator<Configurations> CREATOR;
    public final Map<Integer, Configuration> configurationMap;
    public final long configurationVersion;
    public final Configuration[] configurations;
    public final byte[] experimentToken;
    public final boolean isDelta;
    public final String serverToken;
    public final String snapshotToken;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    public Configurations(final String snapshotToken, final String serverToken, final Configuration[] configurations, final boolean isDelta, final byte[] experimentToken, final long configurationVersion) {
        this.snapshotToken = snapshotToken;
        this.serverToken = serverToken;
        this.configurations = configurations;
        this.isDelta = isDelta;
        this.experimentToken = experimentToken;
        this.configurationVersion = configurationVersion;
        this.configurationMap = new TreeMap<Integer, Configuration>();
        for (final Configuration configuration : configurations) {
            this.configurationMap.put(configuration.flagType, configuration);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Configurations) {
            final Configurations configurations = (Configurations)o;
            return PhenotypeCore.zza(this.snapshotToken, configurations.snapshotToken) && PhenotypeCore.zza(this.serverToken, configurations.serverToken) && this.configurationMap.equals(configurations.configurationMap) && this.isDelta == configurations.isDelta && Arrays.equals(this.experimentToken, configurations.experimentToken) && this.configurationVersion == configurations.configurationVersion;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.snapshotToken, this.serverToken, this.configurationMap, this.isDelta, this.experimentToken, this.configurationVersion });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Configurations('");
        sb.append(this.snapshotToken);
        sb.append('\'');
        sb.append(", ");
        sb.append('\'');
        sb.append(this.serverToken);
        sb.append('\'');
        sb.append(", ");
        sb.append('(');
        final Iterator<Configuration> iterator = this.configurationMap.values().iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next());
            sb.append(", ");
        }
        sb.append(')');
        sb.append(", ");
        sb.append(this.isDelta);
        sb.append(", ");
        String encodeToString;
        if (this.experimentToken == null) {
            encodeToString = "null";
        }
        else {
            encodeToString = Base64.encodeToString(this.experimentToken, 3);
        }
        sb.append(encodeToString);
        sb.append(", ");
        sb.append(this.configurationVersion);
        sb.append(')');
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.snapshotToken, false);
        zzbig.zza(parcel, 3, this.serverToken, false);
        zzbig.zza(parcel, 4, this.configurations, n, false);
        zzbig.zza(parcel, 5, this.isDelta);
        zzbig.zza(parcel, 6, this.experimentToken, false);
        zzbig.zza(parcel, 7, this.configurationVersion);
        zzbig.zza(parcel, zza);
    }
}
