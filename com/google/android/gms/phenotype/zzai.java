package com.google.android.gms.phenotype;

final class zzai implements zza
{
    private final PhenotypeFlag zza;
    private final ConfigurationContentLoader zzb;
    
    zzai(final PhenotypeFlag zza, final ConfigurationContentLoader zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final Object zza() {
        return this.zzb.getFlags().get(this.zza.zza);
    }
}
