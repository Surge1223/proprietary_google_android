package com.google.android.gms.phenotype;

import com.google.android.gms.common.api.internal.zzdo;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.internal.zzdtg;
import com.google.android.gms.common.api.internal.zzdn;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.Api;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApi;

public class PhenotypeClient extends GoogleApi<Object>
{
    private static long zzb;
    
    static {
        PhenotypeClient.zzb = 0L;
    }
    
    PhenotypeClient(final Context context) {
        super(context, (Api<Api.ApiOptions>)Phenotype.API, null, GoogleApi.zza.zza);
    }
    
    public Task<Void> commitToConfiguration(final String s) {
        return this.zza((zzdn<Api.zzb, Void>)new zzaf(this, s));
    }
    
    public Task<Configurations> getConfigurationSnapshot(final String s, final String s2, final String s3) {
        return this.zza((zzdn<Api.zzb, Configurations>)new zzae(this, s, s2, s3));
    }
    
    static class zza extends zzdtg
    {
        private final TaskCompletionSource zza;
        
        private zza(final TaskCompletionSource zza) {
            this.zza = zza;
        }
        
        @Override
        public void zza(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zza(final Status status, final Configurations configurations) {
            zzdo.zza(status, configurations, this.zza);
        }
        
        @Override
        public final void zza(final Status status, final DogfoodsToken dogfoodsToken) {
            zzdo.zza(status, dogfoodsToken, this.zza);
        }
        
        @Override
        public final void zza(final Status status, final ExperimentTokens experimentTokens) {
            zzdo.zza(status, experimentTokens, this.zza);
        }
        
        @Override
        public final void zza(final Status status, final Flag flag) {
            zzdo.zza(status, flag, this.zza);
        }
        
        @Override
        public final void zza(final Status status, final FlagOverrides flagOverrides) {
            zzdo.zza(status, flagOverrides, this.zza);
        }
        
        @Override
        public final void zzb(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zzb(final Status status, final Configurations configurations) {
            zzdo.zza(status, configurations, this.zza);
        }
        
        @Override
        public final void zzc(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zzd(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zze(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zzf(final Status status) {
            if (status.isSuccess()) {
                ConfigurationContentLoader.invalidateAllCaches();
            }
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zzg(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
        
        @Override
        public final void zzh(final Status status) {
            zzdo.zza(status, null, (TaskCompletionSource<Object>)this.zza);
        }
    }
}
