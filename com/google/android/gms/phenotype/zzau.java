package com.google.android.gms.phenotype;

import com.google.android.gms.tasks.TaskExecutors;
import android.util.Base64;
import android.annotation.SuppressLint;
import android.content.SharedPreferences$Editor;
import android.util.Log;
import android.content.SharedPreferences;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.OnCompleteListener;

final class zzau implements OnCompleteListener
{
    private final PhenotypeFlagCommitter zza;
    private final int zzb;
    private final String zzc;
    private final Executor zzd;
    private final PhenotypeFlagCommitter.Callback zze;
    
    zzau(final PhenotypeFlagCommitter zza, final int zzb, final String zzc, final Executor zzd, final PhenotypeFlagCommitter.Callback zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    @Override
    public final void onComplete(final Task task) {
        this.zza.zza(this.zzb, this.zzc, this.zzd, this.zze, task);
    }
}
