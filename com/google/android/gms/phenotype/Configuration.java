package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.Map;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Configuration extends zzbid implements Comparable<Configuration>
{
    public static final Parcelable.Creator<Configuration> CREATOR;
    public final String[] deleteFlags;
    public final Map<String, Flag> flagMap;
    public final int flagType;
    public final Flag[] flags;
    
    static {
        CREATOR = (Parcelable.Creator)new zzb();
    }
    
    public Configuration(int i, final Flag[] flags, final String[] deleteFlags) {
        this.flagType = i;
        this.flags = flags;
        this.flagMap = new TreeMap<String, Flag>();
        int length;
        Flag flag;
        for (length = flags.length, i = 0; i < length; ++i) {
            flag = flags[i];
            this.flagMap.put(flag.name, flag);
        }
        this.deleteFlags = deleteFlags;
        if (this.deleteFlags != null) {
            Arrays.sort(this.deleteFlags);
        }
    }
    
    @Override
    public int compareTo(final Configuration configuration) {
        return this.flagType - configuration.flagType;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Configuration) {
            final Configuration configuration = (Configuration)o;
            return this.flagType == configuration.flagType && PhenotypeCore.zza(this.flagMap, configuration.flagMap) && Arrays.equals(this.deleteFlags, configuration.deleteFlags);
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Configuration(");
        sb.append(this.flagType);
        sb.append(", ");
        sb.append("(");
        final Iterator<Flag> iterator = this.flagMap.values().iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next());
            sb.append(", ");
        }
        sb.append(")");
        sb.append(", ");
        sb.append("(");
        if (this.deleteFlags != null) {
            final String[] deleteFlags = this.deleteFlags;
            for (int length = deleteFlags.length, i = 0; i < length; ++i) {
                sb.append(deleteFlags[i]);
                sb.append(", ");
            }
        }
        else {
            sb.append("null");
        }
        sb.append(")");
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.flagType);
        zzbig.zza(parcel, 3, this.flags, n, false);
        zzbig.zza(parcel, 4, this.deleteFlags, false);
        zzbig.zza(parcel, zza);
    }
}
