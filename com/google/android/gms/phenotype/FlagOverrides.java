package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import java.util.List;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FlagOverrides extends zzbid
{
    public static final Parcelable.Creator<FlagOverrides> CREATOR;
    public final List<FlagOverride> overrides;
    
    static {
        CREATOR = (Parcelable.Creator)new zzm();
    }
    
    public FlagOverrides(final List<FlagOverride> overrides) {
        this.overrides = overrides;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof FlagOverrides && this.overrides.equals(((FlagOverrides)o).overrides));
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FlagOverrides(");
        final Iterator<FlagOverride> iterator = this.overrides.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final FlagOverride flagOverride = iterator.next();
            if (n == 0) {
                sb.append(", ");
            }
            n = 0;
            flagOverride.toString(sb);
        }
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zzc(parcel, 2, this.overrides, false);
        zzbig.zza(parcel, zza);
    }
}
