package com.google.android.gms.phenotype;

import android.os.RemoteException;
import com.google.android.gms.internal.zzdtf;
import com.google.android.gms.internal.zzdth;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.zzdus;
import com.google.android.gms.common.api.internal.zzdn;

final class zzaf extends zzdn<zzdus, Void>
{
    private final /* synthetic */ String zza;
    
    zzaf(final PhenotypeClient phenotypeClient, final String zza) {
        this.zza = zza;
    }
}
