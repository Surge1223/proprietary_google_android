package com.google.android.gms.phenotype;

import android.content.SharedPreferences;

public class PhenotypeFlagSharedPrefsCommitter extends PhenotypeFlagCommitter
{
    private final SharedPreferences zza;
    
    public PhenotypeFlagSharedPrefsCommitter(final PhenotypeClient phenotypeClient, final String s, final SharedPreferences zza) {
        super(phenotypeClient, s);
        this.zza = zza;
    }
    
    @Override
    protected String getSnapshotToken() {
        return this.zza.getString("__phenotype_snapshot_token", (String)null);
    }
    
    @Override
    protected void handleConfigurations(final Configurations configurations) {
        PhenotypeFlagCommitter.writeToSharedPrefs(this.zza, configurations);
    }
}
