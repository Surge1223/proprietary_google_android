package com.google.android.gms.phenotype;

import android.net.Uri;
import android.support.v4.content.PermissionChecker;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.util.Log;
import android.os.UserManager;
import com.google.android.gms.internal.zzfcx;
import android.os.Binder;
import android.os.Build.VERSION;
import com.google.android.gms.internal.zzfdo;
import android.annotation.SuppressLint;
import android.content.Context;

final class zzaj implements zza
{
    private final PhenotypeFlag zza;
    
    zzaj(final PhenotypeFlag zza) {
        this.zza = zza;
    }
    
    @Override
    public final Object zza() {
        return this.zza.zza();
    }
}
