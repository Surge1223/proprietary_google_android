package com.google.android.gms.phenotype;

import android.util.Log;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.OnCompleteListener;

final class zzat implements OnCompleteListener
{
    private final PhenotypeFlagCommitter zza;
    private final PhenotypeFlagCommitter.Callback zzb;
    private final Executor zzc;
    private final int zzd;
    private final String zze;
    
    zzat(final PhenotypeFlagCommitter zza, final PhenotypeFlagCommitter.Callback zzb, final Executor zzc, final int zzd, final String zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    @Override
    public final void onComplete(final Task task) {
        final PhenotypeFlagCommitter zza = this.zza;
        final PhenotypeFlagCommitter.Callback zzb = this.zzb;
        final Executor zzc = this.zzc;
        final int zzd = this.zzd;
        final String zze = this.zze;
        if (!task.isSuccessful()) {
            final String packageName = zza.packageName;
            final StringBuilder sb = new StringBuilder(31 + String.valueOf(packageName).length());
            sb.append("Retrieving snapshot for ");
            sb.append(packageName);
            sb.append(" failed");
            Log.e("PhenotypeFlagCommitter", sb.toString());
            if (zzb != null) {
                zzb.onFinish(false);
            }
            return;
        }
        zza.handleConfigurations(task.getResult());
        zza.client.commitToConfiguration(task.getResult().snapshotToken).addOnCompleteListener(zzc, new zzau(zza, zzd, zze, zzc, zzb));
    }
}
