package com.google.android.gms.phenotype;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.gms.tasks.OnCompleteListener;
import java.util.concurrent.Executor;
import android.util.Base64;
import android.annotation.SuppressLint;
import android.content.SharedPreferences$Editor;
import android.util.Log;
import android.content.SharedPreferences;

public abstract class PhenotypeFlagCommitter
{
    protected final PhenotypeClient client;
    protected final String packageName;
    private long zza;
    
    public PhenotypeFlagCommitter(final PhenotypeClient client, final String packageName) {
        this.client = client;
        this.packageName = packageName;
        this.zza = 2000L;
    }
    
    @SuppressLint({ "ApplySharedPref" })
    public static void writeToSharedPrefs(final SharedPreferences sharedPreferences, final Configurations configurations) {
        final SharedPreferences$Editor edit = sharedPreferences.edit();
        if (!configurations.isDelta) {
            edit.clear();
        }
        final Configuration[] configurations2 = configurations.configurations;
        for (int length = configurations2.length, i = 0; i < length; ++i) {
            zza(edit, configurations2[i]);
        }
        edit.putString("__phenotype_server_token", configurations.serverToken);
        edit.putLong("__phenotype_configuration_version", configurations.configurationVersion);
        edit.putString("__phenotype_snapshot_token", configurations.snapshotToken);
        if (!edit.commit()) {
            Log.w("PhenotypeFlagCommitter", "Failed to commit Phenotype configs to SharedPreferences!");
        }
    }
    
    private static void zza(final SharedPreferences$Editor sharedPreferences$Editor, final Configuration configuration) {
        if (configuration == null) {
            return;
        }
        final String[] deleteFlags = configuration.deleteFlags;
        final int length = deleteFlags.length;
        final int n = 0;
        for (int i = 0; i < length; ++i) {
            sharedPreferences$Editor.remove(deleteFlags[i]);
        }
        final Flag[] flags = configuration.flags;
        for (int length2 = flags.length, j = n; j < length2; ++j) {
            final Flag flag = flags[j];
            switch (flag.flagValueType) {
                case 5: {
                    sharedPreferences$Editor.putString(flag.name, Base64.encodeToString(flag.getBytesValue(), 3));
                    break;
                }
                case 4: {
                    sharedPreferences$Editor.putString(flag.name, flag.getString());
                    break;
                }
                case 3: {
                    sharedPreferences$Editor.putFloat(flag.name, (float)flag.getDouble());
                    break;
                }
                case 2: {
                    sharedPreferences$Editor.putBoolean(flag.name, flag.getBoolean());
                    break;
                }
                case 1: {
                    sharedPreferences$Editor.putLong(flag.name, flag.getLong());
                    break;
                }
            }
        }
    }
    
    private final void zza(final String s, final Executor executor, final Callback callback, final int n) {
        this.client.getConfigurationSnapshot(this.packageName, s, this.getSnapshotToken()).addOnCompleteListener(executor, new zzat(this, callback, executor, n, s));
    }
    
    public final void commitForUserAsync(final String s, final Callback callback) {
        this.commitForUserAsync(s, TaskExecutors.MAIN_THREAD, callback);
    }
    
    public final void commitForUserAsync(final String s, final Executor executor, final Callback callback) {
        zzau.zza(s);
        this.zza(s, executor, callback, 3);
    }
    
    protected String getSnapshotToken() {
        return null;
    }
    
    protected abstract void handleConfigurations(final Configurations p0);
    
    public interface Callback
    {
        void onFinish(final boolean p0);
    }
}
