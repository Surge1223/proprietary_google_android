package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RegistrationInfo extends zzbid
{
    public static final Parcelable.Creator<RegistrationInfo> CREATOR;
    public final String androidPackage;
    public final String configPackage;
    public final String[] logSourceNames;
    public final byte[] params;
    public final int version;
    public final boolean weak;
    public final int[] weakExperimentIds;
    
    static {
        CREATOR = (Parcelable.Creator)new zzav();
    }
    
    public RegistrationInfo(final String configPackage, final int version, final String[] logSourceNames, final byte[] params, final boolean weak, final int[] weakExperimentIds, final String androidPackage) {
        this.configPackage = configPackage;
        this.version = version;
        this.logSourceNames = logSourceNames;
        this.params = params;
        this.weak = weak;
        this.weakExperimentIds = weakExperimentIds;
        this.androidPackage = androidPackage;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.configPackage, false);
        zzbig.zza(parcel, 2, this.version);
        zzbig.zza(parcel, 3, this.logSourceNames, false);
        zzbig.zza(parcel, 4, this.params, false);
        zzbig.zza(parcel, 5, this.weak);
        zzbig.zza(parcel, 6, this.weakExperimentIds, false);
        zzbig.zza(parcel, 7, this.androidPackage, false);
        zzbig.zza(parcel, zza);
    }
}
