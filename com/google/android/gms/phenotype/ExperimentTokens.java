package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import android.util.Base64;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class ExperimentTokens extends zzbid
{
    public static final Parcelable.Creator<ExperimentTokens> CREATOR;
    public static final ExperimentTokens EMPTY;
    public static final byte[][] EMPTY_BYTES;
    private static final zza zza;
    private static final zza zzb;
    private static final zza zzc;
    private static final zza zzd;
    public final byte[][] additionalDirectExperimentTokens;
    public final byte[][] alwaysCrossExperimentTokens;
    public final byte[] directExperimentToken;
    public final byte[][] gaiaCrossExperimentTokens;
    public final byte[][] otherCrossExperimentTokens;
    public final byte[][] pseudonymousCrossExperimentTokens;
    public final String user;
    public final int[] weakExperimentIds;
    
    static {
        CREATOR = (Parcelable.Creator)new zzi();
        EMPTY_BYTES = new byte[0][];
        EMPTY = new ExperimentTokens("", null, ExperimentTokens.EMPTY_BYTES, ExperimentTokens.EMPTY_BYTES, ExperimentTokens.EMPTY_BYTES, ExperimentTokens.EMPTY_BYTES, null, null);
        zza = (zza)new zze();
        zzb = (zza)new zzf();
        zzc = (zza)new zzg();
        zzd = (zza)new zzh();
    }
    
    public ExperimentTokens(final String user, final byte[] directExperimentToken, final byte[][] gaiaCrossExperimentTokens, final byte[][] pseudonymousCrossExperimentTokens, final byte[][] alwaysCrossExperimentTokens, final byte[][] otherCrossExperimentTokens, final int[] weakExperimentIds, final byte[][] additionalDirectExperimentTokens) {
        this.user = user;
        this.directExperimentToken = directExperimentToken;
        this.gaiaCrossExperimentTokens = gaiaCrossExperimentTokens;
        this.pseudonymousCrossExperimentTokens = pseudonymousCrossExperimentTokens;
        this.alwaysCrossExperimentTokens = alwaysCrossExperimentTokens;
        this.otherCrossExperimentTokens = otherCrossExperimentTokens;
        this.weakExperimentIds = weakExperimentIds;
        this.additionalDirectExperimentTokens = additionalDirectExperimentTokens;
    }
    
    private static List<Integer> zza(final int[] array) {
        if (array == null) {
            return Collections.emptyList();
        }
        final ArrayList<Comparable> list = new ArrayList<Comparable>(array.length);
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(array[i]);
        }
        Collections.sort(list);
        return (List<Integer>)list;
    }
    
    private static void zza(final StringBuilder sb, final String s, final byte[][] array) {
        sb.append(s);
        sb.append("=");
        if (array == null) {
            sb.append("null");
            return;
        }
        sb.append("(");
        final int length = array.length;
        int n = 1;
        for (int i = 0; i < length; ++i, n = 0) {
            final byte[] array2 = array[i];
            if (n == 0) {
                sb.append(", ");
            }
            sb.append("'");
            sb.append(Base64.encodeToString(array2, 3));
            sb.append("'");
        }
        sb.append(")");
    }
    
    private static List<String> zzb(final byte[][] array) {
        if (array == null) {
            return Collections.emptyList();
        }
        final ArrayList<Comparable> list = new ArrayList<Comparable>(array.length);
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(Base64.encodeToString(array[i], 3));
        }
        Collections.sort(list);
        return (List<String>)list;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof ExperimentTokens) {
            final ExperimentTokens experimentTokens = (ExperimentTokens)o;
            return PhenotypeCore.zza(this.user, experimentTokens.user) && Arrays.equals(this.directExperimentToken, experimentTokens.directExperimentToken) && PhenotypeCore.zza(zzb(this.gaiaCrossExperimentTokens), zzb(experimentTokens.gaiaCrossExperimentTokens)) && PhenotypeCore.zza(zzb(this.pseudonymousCrossExperimentTokens), zzb(experimentTokens.pseudonymousCrossExperimentTokens)) && PhenotypeCore.zza(zzb(this.alwaysCrossExperimentTokens), zzb(experimentTokens.alwaysCrossExperimentTokens)) && PhenotypeCore.zza(zzb(this.otherCrossExperimentTokens), zzb(experimentTokens.otherCrossExperimentTokens)) && PhenotypeCore.zza(zza(this.weakExperimentIds), zza(experimentTokens.weakExperimentIds)) && PhenotypeCore.zza(zzb(this.additionalDirectExperimentTokens), zzb(experimentTokens.additionalDirectExperimentTokens));
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExperimentTokens");
        sb.append("(");
        String string;
        if (this.user == null) {
            string = "null";
        }
        else {
            final String user = this.user;
            final StringBuilder sb2 = new StringBuilder(2 + String.valueOf(user).length());
            sb2.append("'");
            sb2.append(user);
            sb2.append("'");
            string = sb2.toString();
        }
        sb.append(string);
        sb.append(", ");
        final byte[] directExperimentToken = this.directExperimentToken;
        sb.append("direct");
        sb.append("=");
        if (directExperimentToken == null) {
            sb.append("null");
        }
        else {
            sb.append("'");
            sb.append(Base64.encodeToString(directExperimentToken, 3));
            sb.append("'");
        }
        sb.append(", ");
        zza(sb, "GAIA", this.gaiaCrossExperimentTokens);
        sb.append(", ");
        zza(sb, "PSEUDO", this.pseudonymousCrossExperimentTokens);
        sb.append(", ");
        zza(sb, "ALWAYS", this.alwaysCrossExperimentTokens);
        sb.append(", ");
        zza(sb, "OTHER", this.otherCrossExperimentTokens);
        sb.append(", ");
        final int[] weakExperimentIds = this.weakExperimentIds;
        sb.append("weak");
        sb.append("=");
        if (weakExperimentIds == null) {
            sb.append("null");
        }
        else {
            sb.append("(");
            final int length = weakExperimentIds.length;
            int n = 1;
            for (int i = 0; i < length; ++i, n = 0) {
                final int n2 = weakExperimentIds[i];
                if (n == 0) {
                    sb.append(", ");
                }
                sb.append(n2);
            }
            sb.append(")");
        }
        sb.append(", ");
        zza(sb, "directs", this.additionalDirectExperimentTokens);
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.user, false);
        zzbig.zza(parcel, 3, this.directExperimentToken, false);
        zzbig.zza(parcel, 4, this.gaiaCrossExperimentTokens, false);
        zzbig.zza(parcel, 5, this.pseudonymousCrossExperimentTokens, false);
        zzbig.zza(parcel, 6, this.alwaysCrossExperimentTokens, false);
        zzbig.zza(parcel, 7, this.otherCrossExperimentTokens, false);
        zzbig.zza(parcel, 8, this.weakExperimentIds, false);
        zzbig.zza(parcel, 9, this.additionalDirectExperimentTokens, false);
        zzbig.zza(parcel, zza);
    }
    
    interface zza
    {
    }
}
