package com.google.android.gms.phenotype;

public final class PhenotypeCore
{
    static boolean zza(final Object o, final Object o2) {
        return o == o2 || (o != null && o.equals(o2));
    }
}
