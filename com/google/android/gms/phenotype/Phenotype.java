package com.google.android.gms.phenotype;

import android.content.Context;
import android.net.Uri;
import com.google.android.gms.internal.zzdtj;
import com.google.android.gms.internal.zzdus;
import com.google.android.gms.common.api.Api;

public final class Phenotype
{
    @Deprecated
    public static final Api<Object> API;
    @Deprecated
    public static final PhenotypeApi PhenotypeApi;
    private static final Api.zzf<zzdus> zza;
    private static final Api.zza<zzdus, Object> zzb;
    
    static {
        zza = new Api.zzf();
        zzb = new zzn();
        API = new Api<Object>("Phenotype.API", (Api.zza<C, Object>)Phenotype.zzb, (Api.zzf<C>)Phenotype.zza);
        PhenotypeApi = new zzdtj();
    }
    
    public static Uri getContentProviderUri(String s) {
        s = String.valueOf(Uri.encode(s));
        if (s.length() != 0) {
            s = "content://com.google.android.gms.phenotype/".concat(s);
        }
        else {
            s = new String("content://com.google.android.gms.phenotype/");
        }
        return Uri.parse(s);
    }
    
    public static PhenotypeClient getInstance(final Context context) {
        return new PhenotypeClient(context);
    }
}
