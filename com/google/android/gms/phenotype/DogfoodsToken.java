package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class DogfoodsToken extends zzbid
{
    public static final Parcelable.Creator<DogfoodsToken> CREATOR;
    public final byte[] token;
    
    static {
        CREATOR = (Parcelable.Creator)new zzd();
    }
    
    public DogfoodsToken(final byte[] token) {
        this.token = token;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.token, false);
        zzbig.zza(parcel, zza);
    }
}
