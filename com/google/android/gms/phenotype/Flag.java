package com.google.android.gms.phenotype;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.util.Base64;
import java.util.Arrays;
import java.util.Comparator;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Flag extends zzbid implements Comparable<Flag>
{
    public static final Parcelable.Creator<Flag> CREATOR;
    public static final Comparator<Flag> NON_VALUE_COMPARATOR;
    public final int flagStorageType;
    public final int flagValueType;
    public final String name;
    private final long zza;
    private final boolean zzb;
    private final double zzc;
    private final String zzd;
    private final byte[] zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzk();
        NON_VALUE_COMPARATOR = new zzj();
    }
    
    public Flag(final String name, final long zza, final boolean zzb, final double zzc, final String zzd, final byte[] zze, final int flagValueType, final int flagStorageType) {
        this.name = name;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.flagValueType = flagValueType;
        this.flagStorageType = flagStorageType;
    }
    
    private static int zza(final int n, final int n2) {
        if (n < n2) {
            return -1;
        }
        if (n == n2) {
            return 0;
        }
        return 1;
    }
    
    @Override
    public int compareTo(final Flag flag) {
        final int compareTo = this.name.compareTo(flag.name);
        if (compareTo != 0) {
            return compareTo;
        }
        final int zza = zza(this.flagValueType, flag.flagValueType);
        if (zza != 0) {
            return zza;
        }
        final int flagValueType = this.flagValueType;
        int i = 0;
        switch (flagValueType) {
            default: {
                final int flagValueType2 = this.flagValueType;
                final StringBuilder sb = new StringBuilder(31);
                sb.append("Invalid enum value: ");
                sb.append(flagValueType2);
                throw new AssertionError((Object)sb.toString());
            }
            case 5: {
                if (this.zze == flag.zze) {
                    return 0;
                }
                if (this.zze == null) {
                    return -1;
                }
                if (flag.zze == null) {
                    return 1;
                }
                while (i < Math.min(this.zze.length, flag.zze.length)) {
                    final byte b = (byte)(this.zze[i] - flag.zze[i]);
                    if (b != 0) {
                        return b;
                    }
                    ++i;
                }
                return zza(this.zze.length, flag.zze.length);
            }
            case 4: {
                final String zzd = this.zzd;
                final String zzd2 = flag.zzd;
                if (zzd == zzd2) {
                    return 0;
                }
                if (zzd == null) {
                    return -1;
                }
                if (zzd2 == null) {
                    return 1;
                }
                return zzd.compareTo(zzd2);
            }
            case 3: {
                return Double.compare(this.zzc, flag.zzc);
            }
            case 2: {
                final boolean zzb = this.zzb;
                if (zzb == flag.zzb) {
                    return 0;
                }
                if (zzb) {
                    return 1;
                }
                return -1;
            }
            case 1: {
                final long n = lcmp(this.zza, flag.zza);
                if (n < 0) {
                    return -1;
                }
                if (n == 0) {
                    return 0;
                }
                return 1;
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Flag)) {
            return false;
        }
        final Flag flag = (Flag)o;
        if (!PhenotypeCore.zza(this.name, flag.name) || this.flagValueType != flag.flagValueType || this.flagStorageType != flag.flagStorageType) {
            return false;
        }
        switch (this.flagValueType) {
            default: {
                final int flagValueType = this.flagValueType;
                final StringBuilder sb = new StringBuilder(31);
                sb.append("Invalid enum value: ");
                sb.append(flagValueType);
                throw new AssertionError((Object)sb.toString());
            }
            case 5: {
                return Arrays.equals(this.zze, flag.zze);
            }
            case 4: {
                return PhenotypeCore.zza(this.zzd, flag.zzd);
            }
            case 3: {
                return this.zzc == flag.zzc;
            }
            case 2: {
                return this.zzb == flag.zzb;
            }
            case 1: {
                return this.zza == flag.zza;
            }
        }
    }
    
    public boolean getBoolean() {
        if (this.flagValueType == 2) {
            return this.zzb;
        }
        throw new IllegalArgumentException("Not a boolean type");
    }
    
    public byte[] getBytesValue() {
        if (this.flagValueType == 5) {
            return this.zze;
        }
        throw new IllegalArgumentException("Not a bytes type");
    }
    
    public double getDouble() {
        if (this.flagValueType == 3) {
            return this.zzc;
        }
        throw new IllegalArgumentException("Not a double type");
    }
    
    public long getLong() {
        if (this.flagValueType == 1) {
            return this.zza;
        }
        throw new IllegalArgumentException("Not a long type");
    }
    
    public String getString() {
        if (this.flagValueType == 4) {
            return this.zzd;
        }
        throw new IllegalArgumentException("Not a String type");
    }
    
    @Override
    public String toString() {
        return this.toString(new StringBuilder());
    }
    
    public String toString(StringBuilder sb) {
        sb.append("Flag(");
        sb.append(this.name);
        sb.append(", ");
        switch (this.flagValueType) {
            default: {
                final String name = this.name;
                final int flagValueType = this.flagValueType;
                sb = new StringBuilder(27 + String.valueOf(name).length());
                sb.append("Invalid type: ");
                sb.append(name);
                sb.append(", ");
                sb.append(flagValueType);
                throw new AssertionError((Object)sb.toString());
            }
            case 5: {
                if (this.zze == null) {
                    sb.append("null");
                    break;
                }
                sb.append("'");
                sb.append(Base64.encodeToString(this.zze, 3));
                sb.append("'");
                break;
            }
            case 4: {
                sb.append("'");
                sb.append(this.zzd);
                sb.append("'");
                break;
            }
            case 3: {
                sb.append(this.zzc);
                break;
            }
            case 2: {
                sb.append(this.zzb);
                break;
            }
            case 1: {
                sb.append(this.zza);
                break;
            }
        }
        sb.append(", ");
        sb.append(this.flagValueType);
        sb.append(", ");
        sb.append(this.flagStorageType);
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.name, false);
        zzbig.zza(parcel, 3, this.zza);
        zzbig.zza(parcel, 4, this.zzb);
        zzbig.zza(parcel, 5, this.zzc);
        zzbig.zza(parcel, 6, this.zzd, false);
        zzbig.zza(parcel, 7, this.zze, false);
        zzbig.zza(parcel, 8, this.flagValueType);
        zzbig.zza(parcel, 9, this.flagStorageType);
        zzbig.zza(parcel, zza);
    }
}
