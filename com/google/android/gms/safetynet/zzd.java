package com.google.android.gms.safetynet;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzd extends zzbid
{
    public static final Parcelable.Creator<zzd> CREATOR;
    public final long zza;
    public final HarmfulAppsData[] zzb;
    public final int zzc;
    private final boolean zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    public zzd(final long zza, final HarmfulAppsData[] zzb, final int zzc, final boolean zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        if (zzd) {
            this.zzc = zzc;
            return;
        }
        this.zzc = -1;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza);
        zzbig.zza(parcel, 3, this.zzb, n, false);
        zzbig.zza(parcel, 4, this.zzc);
        zzbig.zza(parcel, 5, this.zzd);
        zzbig.zza(parcel, zza);
    }
}
