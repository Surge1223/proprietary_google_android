package com.google.android.gms.safetynet;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzf extends zzbid
{
    public static final Parcelable.Creator<zzf> CREATOR;
    private final String zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzg();
    }
    
    public zzf(final String zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
