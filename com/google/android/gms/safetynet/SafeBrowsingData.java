package com.google.android.gms.safetynet;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import android.os.Parcel;
import java.io.Closeable;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.data.DataHolder;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SafeBrowsingData extends zzbid
{
    public static final Parcelable.Creator<SafeBrowsingData> CREATOR;
    private static final String zza;
    private String zzb;
    private DataHolder zzc;
    private ParcelFileDescriptor zzd;
    private long zze;
    private byte[] zzf;
    private byte[] zzg;
    private File zzh;
    
    static {
        zza = SafeBrowsingData.class.getSimpleName();
        CREATOR = (Parcelable.Creator)new zzi();
    }
    
    public SafeBrowsingData() {
        this(null, null, null, 0L, null);
    }
    
    public SafeBrowsingData(final String zzb, final DataHolder zzc, final ParcelFileDescriptor zzd, final long zze, final byte[] zzf) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    private final FileOutputStream zza() {
        final File zzh = this.zzh;
        final File file = null;
        if (zzh == null) {
            return null;
        }
        File file2 = null;
        try {
            final File tempFile = File.createTempFile("xlb", ".tmp", this.zzh);
            try {
                final FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
                this.zzd = ParcelFileDescriptor.open(tempFile, 268435456);
                if (tempFile != null) {
                    tempFile.delete();
                }
                return fileOutputStream;
            }
            catch (IOException ex) {}
        }
        catch (IOException ex2) {
            file2 = null;
        }
        finally {
            file2 = file;
        }
        if (file2 != null) {
            file2.delete();
        }
        return null;
    }
    
    private static void zza(final Closeable closeable) {
        try {
            closeable.close();
        }
        catch (IOException ex) {}
    }
    
    public DataHolder getBlacklistsDataHolder() {
        return this.zzc;
    }
    
    public ParcelFileDescriptor getFileDescriptor() {
        return this.zzd;
    }
    
    public long getLastUpdateTimeMs() {
        return this.zze;
    }
    
    public String getMetadata() {
        return this.zzb;
    }
    
    public byte[] getState() {
        return this.zzf;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        boolean b = false;
        Label_0082: {
            if (this.zzd == null && this.zzg != null) {
                final FileOutputStream zza = this.zza();
                if (zza != null) {
                    final DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(zza));
                    try {
                        dataOutputStream.writeInt(this.zzg.length);
                        dataOutputStream.write(this.zzg);
                        zza(dataOutputStream);
                        b = true;
                        break Label_0082;
                    }
                    catch (IOException ex) {
                        zza(dataOutputStream);
                    }
                    finally {
                        zza(dataOutputStream);
                    }
                }
            }
            b = false;
        }
        if (!b) {
            zzi.zza(this, parcel, n);
        }
        else {
            zzi.zza(this, parcel, n | 0x1);
        }
        this.zzd = null;
    }
}
