package com.google.android.gms.safetynet;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class HarmfulAppsData extends zzbid
{
    public static final Parcelable.Creator<HarmfulAppsData> CREATOR;
    public final int apkCategory;
    public final String apkPackageName;
    public final byte[] apkSha256;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    public HarmfulAppsData(final String apkPackageName, final byte[] apkSha256, final int apkCategory) {
        this.apkPackageName = apkPackageName;
        this.apkSha256 = apkSha256;
        this.apkCategory = apkCategory;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.apkPackageName, false);
        zzbig.zza(parcel, 3, this.apkSha256, false);
        zzbig.zza(parcel, 4, this.apkCategory);
        zzbig.zza(parcel, zza);
    }
}
