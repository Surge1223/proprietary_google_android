package com.google.android.gms.safetynet;

import com.google.android.gms.common.data.DataHolder;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.internal.zzbie;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzi implements Parcelable.Creator<SafeBrowsingData>
{
    static void zza(final SafeBrowsingData safeBrowsingData, final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, safeBrowsingData.getMetadata(), false);
        zzbig.zza(parcel, 3, (Parcelable)safeBrowsingData.getBlacklistsDataHolder(), n, false);
        zzbig.zza(parcel, 4, (Parcelable)safeBrowsingData.getFileDescriptor(), n, false);
        zzbig.zza(parcel, 5, safeBrowsingData.getLastUpdateTimeMs());
        zzbig.zza(parcel, 6, safeBrowsingData.getState(), false);
        zzbig.zza(parcel, zza);
    }
}
