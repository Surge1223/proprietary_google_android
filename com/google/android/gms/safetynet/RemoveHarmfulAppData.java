package com.google.android.gms.safetynet;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class RemoveHarmfulAppData extends zzbid
{
    public static final Parcelable.Creator<RemoveHarmfulAppData> CREATOR;
    public final int dialogNotShown;
    public final boolean pressedUninstall;
    
    static {
        CREATOR = (Parcelable.Creator)new zzh();
    }
    
    public RemoveHarmfulAppData(final int dialogNotShown, final boolean pressedUninstall) {
        this.dialogNotShown = dialogNotShown;
        this.pressedUninstall = pressedUninstall;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.dialogNotShown);
        zzbig.zza(parcel, 3, this.pressedUninstall);
        zzbig.zza(parcel, zza);
    }
}
