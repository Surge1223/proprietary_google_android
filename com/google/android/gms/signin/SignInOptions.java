package com.google.android.gms.signin;

import com.google.android.gms.common.api.Api;

public final class SignInOptions implements Optional
{
    public static final SignInOptions DEFAULT;
    private final boolean zza;
    private final boolean zzb;
    private final String zzc;
    private final boolean zzd;
    private final String zze;
    private final boolean zzf;
    private final Long zzg;
    private final Long zzh;
    
    static {
        new zza();
        DEFAULT = new SignInOptions(false, false, null, false, null, false, null, null);
    }
    
    private SignInOptions(final boolean b, final boolean b2, final String s, final boolean b3, final String s2, final boolean b4, final Long n, final Long n2) {
        this.zza = false;
        this.zzb = false;
        this.zzc = null;
        this.zzd = false;
        this.zzf = false;
        this.zze = null;
        this.zzg = null;
        this.zzh = null;
    }
    
    public final Long getAuthApiSignInModuleVersion() {
        return this.zzg;
    }
    
    public final String getHostedDomain() {
        return this.zze;
    }
    
    public final Long getRealClientLibraryVersion() {
        return this.zzh;
    }
    
    public final String getServerClientId() {
        return this.zzc;
    }
    
    public final boolean isForceCodeForRefreshToken() {
        return this.zzd;
    }
    
    public final boolean isIdTokenRequested() {
        return this.zzb;
    }
    
    public final boolean isOfflineAccessRequested() {
        return this.zza;
    }
    
    public final boolean waitForAccessTokenRefresh() {
        return this.zzf;
    }
    
    public static final class zza
    {
    }
}
