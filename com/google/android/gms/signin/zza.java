package com.google.android.gms.signin;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzegs;
import com.google.android.gms.common.api.Api;

public final class zza
{
    public static final Api.zza<zzegs, SignInOptions> zza;
    public static final Api<SignInOptions> zzb;
    private static final Api.zzf<zzegs> zzc;
    private static final Api.zzf<zzegs> zzd;
    private static final Api.zza<zzegs, Object> zze;
    private static final Scope zzf;
    private static final Scope zzg;
    private static final Api<Object> zzh;
    
    static {
        zzc = new Api.zzf();
        zzd = new Api.zzf();
        zza = new zzb();
        zze = new zzc();
        zzf = new Scope("profile");
        zzg = new Scope("email");
        zzb = new Api<SignInOptions>("SignIn.API", (Api.zza<C, SignInOptions>)com.google.android.gms.signin.zza.zza, (Api.zzf<C>)com.google.android.gms.signin.zza.zzc);
        zzh = new Api<Object>("SignIn.INTERNAL_API", (Api.zza<C, Object>)com.google.android.gms.signin.zza.zze, (Api.zzf<C>)com.google.android.gms.signin.zza.zzd);
    }
}
