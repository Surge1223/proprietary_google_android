package com.google.android.gms.search.corpora;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.CorpusStatus;
import com.google.android.gms.internal.zzbie;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzh implements Parcelable.Creator<GetCorpusStatusCall.Response>
{
}
