package com.google.android.gms.search.corpora;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public class DeleteUsageReportCall
{
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzd();
        }
        
        public Response() {
        }
        
        Response(final Status status) {
            this.status = status;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static final class zzb extends zzbid
    {
        public static final Parcelable.Creator<zzb> CREATOR;
        public String zza;
        public String zzb;
        public String zzc;
        
        static {
            CREATOR = (Parcelable.Creator)new zzc();
        }
        
        public zzb() {
        }
        
        zzb(final String zza, final String zzb, final String zzc) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
        }
        
        public final void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zza, false);
            zzbig.zza(parcel, 2, this.zzb, false);
            zzbig.zza(parcel, 3, this.zzc, false);
            zzbig.zza(parcel, zza);
        }
    }
}
