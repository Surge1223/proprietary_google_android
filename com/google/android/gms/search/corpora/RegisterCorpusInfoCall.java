package com.google.android.gms.search.corpora;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public class RegisterCorpusInfoCall
{
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzi();
        }
        
        public Response() {
        }
        
        Response(final Status status) {
            this.status = status;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
