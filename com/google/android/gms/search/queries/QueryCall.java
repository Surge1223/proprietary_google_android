package com.google.android.gms.search.queries;

import com.google.android.gms.appdatasearch.QuerySpecification;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.SearchResults;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public class QueryCall
{
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public SearchResults documents;
        public Status status;
        public Bundle zza;
        
        static {
            CREATOR = (Parcelable.Creator)new zzk();
        }
        
        public Response() {
        }
        
        Response(final Status status, final SearchResults documents, final Bundle zza) {
            this.status = status;
            this.documents = documents;
            this.zza = zza;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, (Parcelable)this.documents, n, false);
            zzbig.zza(parcel, 3, this.zza, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static final class zzb extends zzbid
    {
        public static final Parcelable.Creator<zzb> CREATOR;
        public String zza;
        public String zzb;
        public String[] zzc;
        public int zzd;
        public int zze;
        public QuerySpecification zzf;
        public Bundle zzg;
        
        static {
            CREATOR = (Parcelable.Creator)new zzj();
        }
        
        public zzb() {
        }
        
        zzb(final String zza, final String zzb, final String[] zzc, final int zzd, final int zze, final QuerySpecification zzf, final Bundle zzg) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
            this.zzd = zzd;
            this.zze = zze;
            this.zzf = zzf;
            this.zzg = zzg;
        }
        
        public final void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zza, false);
            zzbig.zza(parcel, 2, this.zzb, false);
            zzbig.zza(parcel, 3, this.zzc, false);
            zzbig.zza(parcel, 4, this.zzd);
            zzbig.zza(parcel, 5, this.zze);
            zzbig.zza(parcel, 6, (Parcelable)this.zzf, n, false);
            zzbig.zza(parcel, 7, this.zzg, false);
            zzbig.zza(parcel, zza);
        }
    }
}
