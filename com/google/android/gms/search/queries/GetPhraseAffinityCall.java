package com.google.android.gms.search.queries;

import com.google.android.gms.appdatasearch.PhraseAffinityCorpusSpec;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.PhraseAffinityResponse;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public class GetPhraseAffinityCall
{
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public PhraseAffinityResponse affinity;
        public Status status;
        public Bundle zza;
        
        static {
            CREATOR = (Parcelable.Creator)new zzg();
        }
        
        public Response() {
        }
        
        Response(final Status status, final PhraseAffinityResponse affinity, final Bundle zza) {
            this.status = status;
            this.affinity = affinity;
            this.zza = zza;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, (Parcelable)this.affinity, n, false);
            zzbig.zza(parcel, 3, this.zza, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static final class zzb extends zzbid
    {
        public static final Parcelable.Creator<zzb> CREATOR;
        public String[] zza;
        public PhraseAffinityCorpusSpec[] zzb;
        public Bundle zzc;
        
        static {
            CREATOR = (Parcelable.Creator)new zzf();
        }
        
        public zzb() {
        }
        
        zzb(final String[] zza, final PhraseAffinityCorpusSpec[] zzb, final Bundle zzc) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
        }
        
        public final void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zza, false);
            zzbig.zza(parcel, 2, this.zzb, n, false);
            zzbig.zza(parcel, 3, this.zzc, false);
            zzbig.zza(parcel, zza);
        }
    }
}
