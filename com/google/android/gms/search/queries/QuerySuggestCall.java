package com.google.android.gms.search.queries;

import com.google.android.gms.appdatasearch.SuggestSpecification;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.SuggestionResults;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public class QuerySuggestCall
{
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public SuggestionResults querySuggestions;
        public Status status;
        public Bundle zza;
        
        static {
            CREATOR = (Parcelable.Creator)new zzm();
        }
        
        public Response() {
        }
        
        Response(final Status status, final SuggestionResults querySuggestions, final Bundle zza) {
            this.status = status;
            this.querySuggestions = querySuggestions;
            this.zza = zza;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, (Parcelable)this.querySuggestions, n, false);
            zzbig.zza(parcel, 3, this.zza, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static final class zzb extends zzbid
    {
        public static final Parcelable.Creator<zzb> CREATOR;
        public String zza;
        public String zzb;
        public String[] zzc;
        public int zzd;
        public SuggestSpecification zze;
        public Bundle zzf;
        
        static {
            CREATOR = (Parcelable.Creator)new zzl();
        }
        
        public zzb() {
        }
        
        zzb(final String zza, final String zzb, final String[] zzc, final int zzd, final SuggestSpecification zze, final Bundle zzf) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
            this.zzd = zzd;
            this.zze = zze;
            this.zzf = zzf;
        }
        
        public final void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zza, false);
            zzbig.zza(parcel, 2, this.zzb, false);
            zzbig.zza(parcel, 3, this.zzc, false);
            zzbig.zza(parcel, 4, this.zzd);
            zzbig.zza(parcel, 5, (Parcelable)this.zze, n, false);
            zzbig.zza(parcel, 6, this.zzf, false);
            zzbig.zza(parcel, zza);
        }
    }
}
