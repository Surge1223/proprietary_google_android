package com.google.android.gms.search.queries;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import java.util.List;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public class AnnotateCall
{
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public List<Annotation> annotations;
        public Status status;
        public Bundle zza;
        
        static {
            CREATOR = (Parcelable.Creator)new com.google.android.gms.search.queries.zzb();
        }
        
        public Response() {
        }
        
        Response(final Status status, final List<Annotation> annotations, final Bundle zza) {
            this.status = status;
            this.annotations = annotations;
            this.zza = zza;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zzc(parcel, 2, this.annotations, false);
            zzbig.zza(parcel, 3, this.zza, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static final class zzb extends zzbid
    {
        public static final Parcelable.Creator<zzb> CREATOR;
        public Bundle zza;
        private final String zzb;
        private final String zzc;
        private final int[] zzd;
        
        static {
            CREATOR = (Parcelable.Creator)new zza();
        }
        
        public zzb(final String zzb, final String zzc, final int[] zzd, final Bundle zza) {
            this.zzb = zzb;
            this.zzc = zzc;
            this.zzd = zzd;
            this.zza = zza;
        }
        
        public final void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zzb, false);
            zzbig.zza(parcel, 2, this.zzc, false);
            zzbig.zza(parcel, 3, this.zzd, false);
            zzbig.zza(parcel, 4, this.zza, false);
            zzbig.zza(parcel, zza);
        }
    }
}
