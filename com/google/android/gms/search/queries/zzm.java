package com.google.android.gms.search.queries;

import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.SuggestionResults;
import com.google.android.gms.internal.zzbie;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzm implements Parcelable.Creator<QuerySuggestCall.Response>
{
}
