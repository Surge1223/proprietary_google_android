package com.google.android.gms.search.queries;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class Annotation extends zzbid
{
    public static final Parcelable.Creator<Annotation> CREATOR;
    public final int annotationType;
    public final byte[] payloadBytes;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    public Annotation(final int annotationType, final byte[] payloadBytes) {
        this.annotationType = annotationType;
        this.payloadBytes = payloadBytes;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.annotationType);
        zzbig.zza(parcel, 5, this.payloadBytes, false);
        zzbig.zza(parcel, zza);
    }
}
