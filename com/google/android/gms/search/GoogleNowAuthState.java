package com.google.android.gms.search;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GoogleNowAuthState extends zzbid
{
    public static final Parcelable.Creator<GoogleNowAuthState> CREATOR;
    private String zza;
    private String zzb;
    private long zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zza();
    }
    
    GoogleNowAuthState(final String zza, final String zzb, final long zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public String getAccessToken() {
        return this.zzb;
    }
    
    public String getAuthCode() {
        return this.zza;
    }
    
    public long getNextAllowedTimeMillis() {
        return this.zzc;
    }
    
    @Override
    public String toString() {
        final String zza = this.zza;
        final String zzb = this.zzb;
        final long zzc = this.zzc;
        final StringBuilder sb = new StringBuilder(74 + String.valueOf(zza).length() + String.valueOf(zzb).length());
        sb.append("mAuthCode = ");
        sb.append(zza);
        sb.append("\nmAccessToken = ");
        sb.append(zzb);
        sb.append("\nmNextAllowedTimeMillis = ");
        sb.append(zzc);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getAuthCode(), false);
        zzbig.zza(parcel, 2, this.getAccessToken(), false);
        zzbig.zza(parcel, 3, this.getNextAllowedTimeMillis());
        zzbig.zza(parcel, zza);
    }
}
