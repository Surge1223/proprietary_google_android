package com.google.android.gms.search.ime;

import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GetCorpusHandlesRegisteredForIMECall
{
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        
        static {
            CREATOR = (Parcelable.Creator)new zza();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            zzbig.zza(parcel, zzbig.zza(parcel));
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public String[] corpusHandlesRegisteredForIME;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzb();
        }
        
        public Response() {
        }
        
        Response(final Status status, final String[] corpusHandlesRegisteredForIME) {
            this.status = status;
            this.corpusHandlesRegisteredForIME = corpusHandlesRegisteredForIME;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, this.corpusHandlesRegisteredForIME, false);
            zzbig.zza(parcel, zza);
        }
    }
}
