package com.google.android.gms.search.ime;

import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.PIMEUpdateResponse;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GetIMEUpdatesCall
{
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        public byte[] iterToken;
        public int maxNumUpdates;
        
        static {
            CREATOR = (Parcelable.Creator)new zzc();
        }
        
        public Request() {
        }
        
        Request(final int maxNumUpdates, final byte[] iterToken) {
            this.maxNumUpdates = maxNumUpdates;
            this.iterToken = iterToken;
        }
        
        public void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.maxNumUpdates);
            zzbig.zza(parcel, 2, this.iterToken, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public PIMEUpdateResponse pimeUpdateResponse;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzd();
        }
        
        public Response() {
        }
        
        Response(final Status status, final PIMEUpdateResponse pimeUpdateResponse) {
            this.status = status;
            this.pimeUpdateResponse = pimeUpdateResponse;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, (Parcelable)this.pimeUpdateResponse, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
