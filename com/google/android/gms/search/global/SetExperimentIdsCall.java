package com.google.android.gms.search.global;

import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SetExperimentIdsCall
{
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        public boolean emergency;
        public byte[] serializedExperimentConfig;
        
        static {
            CREATOR = (Parcelable.Creator)new zzi();
        }
        
        public Request() {
        }
        
        Request(final byte[] serializedExperimentConfig, final boolean emergency) {
            this.serializedExperimentConfig = serializedExperimentConfig;
            this.emergency = emergency;
        }
        
        public void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.serializedExperimentConfig, false);
            zzbig.zza(parcel, 2, this.emergency);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzj();
        }
        
        public Response() {
        }
        
        Response(final Status status) {
            this.status = status;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
