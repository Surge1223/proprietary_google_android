package com.google.android.gms.search.global;

import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class SetIncludeInGlobalSearchCall
{
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        public boolean enabled;
        public String packageName;
        public String sourceName;
        
        static {
            CREATOR = (Parcelable.Creator)new zzk();
        }
        
        public Request() {
        }
        
        Request(final String packageName, final String sourceName, final boolean enabled) {
            this.packageName = packageName;
            this.sourceName = sourceName;
            this.enabled = enabled;
        }
        
        public void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.packageName, false);
            zzbig.zza(parcel, 2, this.enabled);
            zzbig.zza(parcel, 3, this.sourceName, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzl();
        }
        
        public Response() {
        }
        
        Response(final Status status) {
            this.status = status;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
