package com.google.android.gms.search.global;

import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Bundle;
import com.google.android.gms.appdatasearch.Feature;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GetGlobalSearchSourcesCall
{
    public static class CorpusInfo extends zzbid
    {
        public static final Parcelable.Creator<CorpusInfo> CREATOR;
        public String corpusName;
        public Feature[] features;
        public boolean isAppHistoryCorpus;
        public Bundle userHandle;
        
        static {
            CREATOR = (Parcelable.Creator)new zzc();
        }
        
        public CorpusInfo() {
        }
        
        CorpusInfo(final String corpusName, final Feature[] features, final boolean isAppHistoryCorpus, final Bundle userHandle) {
            this.corpusName = corpusName;
            this.features = features;
            this.isAppHistoryCorpus = isAppHistoryCorpus;
            this.userHandle = userHandle;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.corpusName, false);
            zzbig.zza(parcel, 2, this.features, n, false);
            zzbig.zza(parcel, 3, this.isAppHistoryCorpus);
            zzbig.zza(parcel, 4, this.userHandle, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class GlobalSearchSource extends zzbid
    {
        public static final Parcelable.Creator<GlobalSearchSource> CREATOR;
        public CorpusInfo[] corpora;
        public String defaultIntentAction;
        public String defaultIntentActivity;
        public String defaultIntentData;
        public boolean enabled;
        public int iconId;
        public int labelId;
        public String packageName;
        public int settingsDescriptionId;
        public String sourceName;
        
        static {
            CREATOR = (Parcelable.Creator)new zzd();
        }
        
        public GlobalSearchSource() {
        }
        
        GlobalSearchSource(final String packageName, final String sourceName, final int labelId, final int settingsDescriptionId, final int iconId, final String defaultIntentAction, final String defaultIntentData, final String defaultIntentActivity, final CorpusInfo[] corpora, final boolean enabled) {
            this.packageName = packageName;
            this.sourceName = sourceName;
            this.labelId = labelId;
            this.settingsDescriptionId = settingsDescriptionId;
            this.iconId = iconId;
            this.defaultIntentAction = defaultIntentAction;
            this.defaultIntentData = defaultIntentData;
            this.defaultIntentActivity = defaultIntentActivity;
            this.corpora = corpora;
            this.enabled = enabled;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.packageName, false);
            zzbig.zza(parcel, 2, this.labelId);
            zzbig.zza(parcel, 3, this.settingsDescriptionId);
            zzbig.zza(parcel, 4, this.iconId);
            zzbig.zza(parcel, 5, this.defaultIntentAction, false);
            zzbig.zza(parcel, 6, this.defaultIntentData, false);
            zzbig.zza(parcel, 7, this.defaultIntentActivity, false);
            zzbig.zza(parcel, 8, this.corpora, n, false);
            zzbig.zza(parcel, 9, this.enabled);
            zzbig.zza(parcel, 10, this.sourceName, false);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        public boolean wantDisabledSources;
        
        static {
            CREATOR = (Parcelable.Creator)new zze();
        }
        
        public Request() {
        }
        
        Request(final boolean wantDisabledSources) {
            this.wantDisabledSources = wantDisabledSources;
        }
        
        public void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.wantDisabledSources);
            zzbig.zza(parcel, zza);
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public GlobalSearchSource[] sources;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zzf();
        }
        
        public Response() {
        }
        
        Response(final Status status, final GlobalSearchSource[] sources) {
            this.status = status;
            this.sources = sources;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, this.sources, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
