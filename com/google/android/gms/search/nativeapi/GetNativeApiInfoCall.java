package com.google.android.gms.search.nativeapi;

import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.appdatasearch.NativeApiInfo;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class GetNativeApiInfoCall
{
    public static class Request extends zzbid
    {
        public static final Parcelable.Creator<Request> CREATOR;
        
        static {
            CREATOR = (Parcelable.Creator)new zzb();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            zzbig.zza(parcel, zzbig.zza(parcel));
        }
    }
    
    public static class Response extends zzbid implements Result
    {
        public static final Parcelable.Creator<Response> CREATOR;
        public NativeApiInfo nativeApiInfo;
        public Status status;
        
        static {
            CREATOR = (Parcelable.Creator)new zza();
        }
        
        public Response() {
        }
        
        Response(final Status status, final NativeApiInfo nativeApiInfo) {
            this.status = status;
            this.nativeApiInfo = nativeApiInfo;
        }
        
        @Override
        public Status getStatus() {
            return this.status;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            final int zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, (Parcelable)this.status, n, false);
            zzbig.zza(parcel, 2, (Parcelable)this.nativeApiInfo, n, false);
            zzbig.zza(parcel, zza);
        }
    }
}
