package com.google.android.gms.dynamite;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzk extends IInterface
{
    int zza(final IObjectWrapper p0, final String p1, final boolean p2) throws RemoteException;
}
