package com.google.android.gms.dynamite;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzl extends zzey implements zzk
{
    zzl(final IBinder binder) {
        super(binder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }
    
    @Override
    public final int zza(final IObjectWrapper objectWrapper, final String s, final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeString(s);
        zzfa.zza(a_, b);
        final Parcel zza = this.zza(3, a_);
        final int int1 = zza.readInt();
        zza.recycle();
        return int1;
    }
}
