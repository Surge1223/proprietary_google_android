package com.google.android.gms.dynamite;

import com.google.android.gms.common.util.DynamiteApi;
import android.database.Cursor;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.RemoteException;
import java.lang.reflect.InvocationTargetException;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import java.lang.reflect.Field;
import android.util.Log;
import android.content.Context;

public final class DynamiteModule
{
    public static final zzd zza;
    public static final zzd zzb;
    public static final zzd zzc;
    public static final zzd zzd;
    public static final zzd zze;
    private static Boolean zzf;
    private static zzk zzg;
    private static zzm zzh;
    private static String zzi;
    private static final ThreadLocal<zza> zzj;
    private static final zzi zzk;
    private static final zzd zzl;
    
    static {
        zzj = new ThreadLocal<zza>();
        zzk = new com.google.android.gms.dynamite.zza();
        zza = (zzd)new zzb();
        zzl = (zzd)new com.google.android.gms.dynamite.zzc();
        zzb = (zzd)new com.google.android.gms.dynamite.zzd();
        zzc = (zzd)new zze();
        zzd = (zzd)new zzf();
        zze = (zzd)new zzg();
    }
    
    public static int zza(final Context context, final String s) {
        try {
            final ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            final StringBuilder sb = new StringBuilder(61 + String.valueOf(s).length());
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(s);
            sb.append(".ModuleDescriptor");
            final Class<?> loadClass = classLoader.loadClass(sb.toString());
            final Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            final Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (!declaredField.get(null).equals(s)) {
                final String value = String.valueOf(declaredField.get(null));
                final StringBuilder sb2 = new StringBuilder(51 + String.valueOf(value).length() + String.valueOf(s).length());
                sb2.append("Module descriptor id '");
                sb2.append(value);
                sb2.append("' didn't match expected id '");
                sb2.append(s);
                sb2.append("'");
                Log.e("DynamiteModule", sb2.toString());
                return 0;
            }
            return declaredField2.getInt(null);
        }
        catch (Exception ex) {
            final String value2 = String.valueOf(ex.getMessage());
            String concat;
            if (value2.length() != 0) {
                concat = "Failed to load module descriptor class: ".concat(value2);
            }
            else {
                concat = new String("Failed to load module descriptor class: ");
            }
            Log.e("DynamiteModule", concat);
        }
        catch (ClassNotFoundException ex2) {
            final StringBuilder sb3 = new StringBuilder(45 + String.valueOf(s).length());
            sb3.append("Local module descriptor class for ");
            sb3.append(s);
            sb3.append(" not found.");
            Log.w("DynamiteModule", sb3.toString());
        }
        return 0;
    }
    
    public static int zza(final Context context, final String s, final boolean b) {
        synchronized (DynamiteModule.class) {
            Boolean zzf;
            if ((zzf = DynamiteModule.zzf) == null) {
                try {
                    final Class<?> loadClass = context.getApplicationContext().getClassLoader().loadClass(DynamiteLoaderClassLoader.class.getName());
                    final Field declaredField = loadClass.getDeclaredField("sClassLoader");
                    synchronized (loadClass) {
                        final ClassLoader classLoader = (ClassLoader)declaredField.get(null);
                        if (classLoader != null) {
                            if (classLoader == ClassLoader.getSystemClassLoader()) {
                                final Boolean false = Boolean.FALSE;
                            }
                            else {
                                try {
                                    zza(classLoader);
                                }
                                catch (zzc zzc3) {}
                                final Boolean true = Boolean.TRUE;
                            }
                        }
                        else if ("com.google.android.gms".equals(context.getApplicationContext().getPackageName())) {
                            declaredField.set(null, ClassLoader.getSystemClassLoader());
                            final Boolean false2 = Boolean.FALSE;
                        }
                        else {
                            try {
                                final int zzc = zzc(context, s, b);
                                if (DynamiteModule.zzi != null && !DynamiteModule.zzi.isEmpty()) {
                                    final zzh zzh = new zzh(DynamiteModule.zzi, ClassLoader.getSystemClassLoader());
                                    zza((ClassLoader)zzh);
                                    declaredField.set(null, zzh);
                                    DynamiteModule.zzf = Boolean.TRUE;
                                    return zzc;
                                }
                                return zzc;
                            }
                            catch (zzc zzc4) {
                                declaredField.set(null, ClassLoader.getSystemClassLoader());
                                final Boolean false3 = Boolean.FALSE;
                            }
                        }
                    }
                }
                catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException ex) {
                    final Object o;
                    final String value = String.valueOf(o);
                    final StringBuilder sb = new StringBuilder(30 + String.valueOf(value).length());
                    sb.append("Failed to load module via V2: ");
                    sb.append(value);
                    Log.w("DynamiteModule", sb.toString());
                    zzf = Boolean.FALSE;
                }
                DynamiteModule.zzf = zzf;
            }
            // monitorexit(DynamiteModule.class)
            if (zzf) {
                try {
                    return zzc(context, s, b);
                }
                catch (zzc zzc2) {
                    final String value2 = String.valueOf(zzc2.getMessage());
                    String concat;
                    if (value2.length() != 0) {
                        concat = "Failed to retrieve remote module version: ".concat(value2);
                    }
                    else {
                        concat = new String("Failed to retrieve remote module version: ");
                    }
                    Log.w("DynamiteModule", concat);
                    return 0;
                }
            }
            return zzb(context, s, b);
        }
    }
    
    private static zzk zza(final Context context) {
        synchronized (DynamiteModule.class) {
            if (DynamiteModule.zzg != null) {
                return DynamiteModule.zzg;
            }
            if (GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(context) != 0) {
                return null;
            }
            try {
                final IBinder binder = (IBinder)context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance();
                zzk zzg;
                if (binder == null) {
                    zzg = null;
                }
                else {
                    final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoader");
                    if (queryLocalInterface instanceof zzk) {
                        zzg = (zzk)queryLocalInterface;
                    }
                    else {
                        zzg = new zzl(binder);
                    }
                }
                if (zzg != null) {
                    return DynamiteModule.zzg = zzg;
                }
            }
            catch (Exception ex) {
                final String value = String.valueOf(ex.getMessage());
                String concat;
                if (value.length() != 0) {
                    concat = "Failed to load IDynamiteLoader from GmsCore: ".concat(value);
                }
                else {
                    concat = new String("Failed to load IDynamiteLoader from GmsCore: ");
                }
                Log.e("DynamiteModule", concat);
            }
            return null;
        }
    }
    
    private static void zza(final ClassLoader classLoader) throws zzc {
        try {
            final IBinder binder = (IBinder)classLoader.loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            Label_0068: {
                if (binder == null) {
                    final zzm zzh = null;
                    break Label_0068;
                }
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoaderV2");
                if (queryLocalInterface instanceof zzm) {
                    final zzm zzh = (zzm)queryLocalInterface;
                    break Label_0068;
                }
                try {
                    final zzm zzh = new zzn(binder);
                    DynamiteModule.zzh = zzh;
                }
                catch (IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                    final Object o;
                    throw new zzc("Failed to instantiate dynamite loader", (Throwable)o, null);
                }
            }
        }
        catch (ClassNotFoundException ex2) {}
        catch (IllegalAccessException ex3) {}
        catch (InstantiationException ex4) {}
        catch (InvocationTargetException ex5) {}
        catch (NoSuchMethodException ex6) {}
    }
    
    public static int zzb(final Context context, final String s) {
        return zza(context, s, false);
    }
    
    private static int zzb(final Context context, final String s, final boolean b) {
        final zzk zza = zza(context);
        if (zza == null) {
            return 0;
        }
        try {
            return zza.zza(com.google.android.gms.dynamic.zzn.zza(context), s, b);
        }
        catch (RemoteException ex) {
            final String value = String.valueOf(ex.getMessage());
            String concat;
            if (value.length() != 0) {
                concat = "Failed to retrieve remote module version: ".concat(value);
            }
            else {
                concat = new String("Failed to retrieve remote module version: ");
            }
            Log.w("DynamiteModule", concat);
            return 0;
        }
    }
    
    private static int zzc(Context context, final String s, final boolean b) throws zzc {
        zza zza = null;
        final Exception ex;
        Label_0248: {
            try {
                final ContentResolver contentResolver = context.getContentResolver();
                String s2;
                if (b) {
                    s2 = "api_force_staging";
                }
                else {
                    s2 = "api";
                }
                final StringBuilder sb = new StringBuilder(42 + String.valueOf(s2).length() + String.valueOf(s).length());
                sb.append("content://com.google.android.gms.chimera/");
                sb.append(s2);
                sb.append("/");
                sb.append(s);
                final Cursor query = contentResolver.query(Uri.parse(sb.toString()), (String[])null, (String)null, (String[])null, (String)null);
                if (query != null) {
                    try {
                        if (query.moveToFirst()) {
                            final int int1 = query.getInt(0);
                            final Cursor cursor = query;
                            if (int1 > 0) {
                                synchronized (DynamiteModule.class) {
                                    DynamiteModule.zzi = query.getString(2);
                                    // monitorexit(DynamiteModule.class)
                                    zza = DynamiteModule.zzj.get();
                                    if (zza != null && zza.zza == null) {
                                        zza.zza = query;
                                    }
                                }
                            }
                            if (cursor != null) {
                                cursor.close();
                            }
                            return int1;
                        }
                    }
                    catch (Exception ex2) {
                        break Label_0248;
                    }
                    finally {
                        break Label_0248;
                    }
                }
                Log.w("DynamiteModule", "Failed to retrieve remote module version.");
                throw new zzc("Failed to connect to dynamite module ContentResolver.", (com.google.android.gms.dynamite.zza)null);
            }
            catch (Exception ex) {
                context = null;
            }
            finally {
                context = (Context)zza;
                break Label_0248;
            }
            try {
                if (ex instanceof zzc) {
                    throw ex;
                }
                throw new zzc("V2 version check failed", ex, null);
            }
            finally {}
        }
        if (context != null) {
            ((Cursor)context).close();
        }
        throw ex;
    }
    
    @DynamiteApi
    public static class DynamiteLoaderClassLoader
    {
        public static ClassLoader sClassLoader;
    }
    
    static final class zza
    {
        public Cursor zza;
    }
    
    public static final class zzc extends Exception
    {
        private zzc(final String s) {
            super(s);
        }
        
        private zzc(final String s, final Throwable t) {
            super(s, t);
        }
    }
    
    public interface zzd
    {
    }
}
