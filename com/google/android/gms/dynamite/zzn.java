package com.google.android.gms.dynamite;

import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzn extends zzey implements zzm
{
    zzn(final IBinder binder) {
        super(binder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }
}
