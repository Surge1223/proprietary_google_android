package com.google.android.gms.ads.doubleclick;

public interface OnCustomRenderedAdLoadedListener
{
    void onCustomRenderedAdLoaded(final CustomRenderedAd p0);
}
