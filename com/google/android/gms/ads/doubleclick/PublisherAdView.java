package com.google.android.gms.ads.doubleclick;

import com.google.android.gms.ads.Correlator;
import com.google.android.gms.internal.zzaid;
import android.view.View;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.common.internal.zzau;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzbr;
import android.view.ViewGroup;

public final class PublisherAdView extends ViewGroup
{
    private final zzbr zza;
    
    public PublisherAdView(final Context context) {
        super(context);
        this.zza = new zzbr(this);
    }
    
    public PublisherAdView(final Context context, final AttributeSet set) {
        super(context, set);
        this.zza = new zzbr(this, set, true);
        zzau.zza(context, "Context cannot be null");
    }
    
    public PublisherAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.zza = new zzbr(this, set, true);
    }
    
    public final AdListener getAdListener() {
        return this.zza.zzb();
    }
    
    public final AdSize getAdSize() {
        return this.zza.zzc();
    }
    
    public final AdSize[] getAdSizes() {
        return this.zza.zzd();
    }
    
    public final String getAdUnitId() {
        return this.zza.zze();
    }
    
    public final AppEventListener getAppEventListener() {
        return this.zza.zzf();
    }
    
    public final String getMediationAdapterClassName() {
        return this.zza.zzk();
    }
    
    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.zza.zzg();
    }
    
    public final VideoController getVideoController() {
        return this.zza.zzm();
    }
    
    public final VideoOptions getVideoOptions() {
        return this.zza.zzo();
    }
    
    protected final void onLayout(final boolean b, int n, int n2, final int n3, final int n4) {
        final View child = this.getChildAt(0);
        if (child != null && child.getVisibility() != 8) {
            final int measuredWidth = child.getMeasuredWidth();
            final int measuredHeight = child.getMeasuredHeight();
            n = (n3 - n - measuredWidth) / 2;
            n2 = (n4 - n2 - measuredHeight) / 2;
            child.layout(n, n2, measuredWidth + n, measuredHeight + n2);
        }
    }
    
    protected final void onMeasure(final int n, final int n2) {
        int n3 = 0;
        final View child = this.getChildAt(0);
        int n4;
        if (child != null && child.getVisibility() != 8) {
            this.measureChild(child, n, n2);
            n3 = child.getMeasuredWidth();
            n4 = child.getMeasuredHeight();
        }
        else {
            AdSize adSize = null;
            try {
                adSize = this.getAdSize();
            }
            catch (NullPointerException ex) {
                zzaid.zzb("Unable to retrieve ad size.", ex);
            }
            if (adSize != null) {
                final Context context = this.getContext();
                n3 = adSize.getWidthInPixels(context);
                n4 = adSize.getHeightInPixels(context);
            }
            else {
                n4 = 0;
            }
        }
        this.setMeasuredDimension(View.resolveSize(Math.max(n3, this.getSuggestedMinimumWidth()), n), View.resolveSize(Math.max(n4, this.getSuggestedMinimumHeight()), n2));
    }
    
    public final void setAdListener(final AdListener adListener) {
        this.zza.zza(adListener);
    }
    
    public final void setAdSizes(final AdSize... array) {
        if (array != null && array.length > 0) {
            this.zza.zzb(array);
            return;
        }
        throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
    }
    
    public final void setAdUnitId(final String s) {
        this.zza.zza(s);
    }
    
    public final void setAppEventListener(final AppEventListener appEventListener) {
        this.zza.zza(appEventListener);
    }
    
    public final void setCorrelator(final Correlator correlator) {
        this.zza.zza(correlator);
    }
    
    public final void setManualImpressionsEnabled(final boolean b) {
        this.zza.zza(b);
    }
    
    public final void setOnCustomRenderedAdLoadedListener(final OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.zza.zza(onCustomRenderedAdLoadedListener);
    }
    
    public final void setVideoOptions(final VideoOptions videoOptions) {
        this.zza.zza(videoOptions);
    }
}
