package com.google.android.gms.ads.identifier;

import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import android.content.Context;
import android.content.SharedPreferences;

public final class zzd
{
    private SharedPreferences zza;
    
    public zzd(Context remoteContext) {
        try {
            remoteContext = GooglePlayServicesUtilLight.getRemoteContext(remoteContext);
            SharedPreferences sharedPreferences;
            if (remoteContext == null) {
                sharedPreferences = null;
            }
            else {
                sharedPreferences = remoteContext.getSharedPreferences("google_ads_flags", 0);
            }
            this.zza = sharedPreferences;
        }
        catch (Throwable t) {
            Log.w("GmscoreFlag", "Error while getting SharedPreferences ", t);
            this.zza = null;
        }
    }
    
    final float zza(final String s, float float1) {
        try {
            if (this.zza == null) {
                return 0.0f;
            }
            float1 = this.zza.getFloat(s, 0.0f);
            return float1;
        }
        catch (Throwable t) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", t);
            return 0.0f;
        }
    }
    
    final String zza(String string, final String s) {
        try {
            if (this.zza == null) {
                return s;
            }
            string = this.zza.getString(string, s);
            return string;
        }
        catch (Throwable t) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", t);
            return s;
        }
    }
    
    public final boolean zza(final String s, final boolean b) {
        try {
            return this.zza != null && this.zza.getBoolean(s, false);
        }
        catch (Throwable t) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", t);
            return false;
        }
    }
}
