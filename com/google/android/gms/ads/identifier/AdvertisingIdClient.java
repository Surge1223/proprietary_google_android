package com.google.android.gms.ads.identifier;

import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import android.os.RemoteException;
import android.util.Log;
import java.util.Map;
import java.util.HashMap;
import com.google.android.gms.internal.zzgk;
import java.util.concurrent.TimeUnit;
import android.content.pm.PackageManager;
import android.content.ServiceConnection;
import android.content.Intent;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import java.io.IOException;
import android.os.SystemClock;
import com.google.android.gms.common.internal.zzau;
import android.content.Context;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.common.zza;

public class AdvertisingIdClient
{
    private com.google.android.gms.common.zza zza;
    private zzgj zzb;
    private boolean zzc;
    private final Object zzd;
    private zza zze;
    private final Context zzf;
    private final boolean zzg;
    private final long zzh;
    
    private AdvertisingIdClient(Context context, final long zzh, final boolean b, final boolean zzg) {
        this.zzd = new Object();
        zzau.zza(context);
        if (b) {
            final Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            this.zzf = context;
        }
        else {
            this.zzf = context;
        }
        this.zzc = false;
        this.zzh = zzh;
        this.zzg = zzg;
    }
    
    public static Info getAdvertisingIdInfo(Context context) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        final zzd zzd = new zzd(context);
        final boolean zza = zzd.zza("gads:ad_id_app_context:enabled", false);
        final float zza2 = zzd.zza("gads:ad_id_app_context:ping_ratio", 0.0f);
        final String zza3 = zzd.zza("gads:ad_id_use_shared_preference:experiment_id", "");
        context = (Context)new AdvertisingIdClient(context, -1L, zza, zzd.zza("gads:ad_id_use_persistent_service:enabled", false));
        try {
            try {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                ((AdvertisingIdClient)context).zza(false);
                final Info info = ((AdvertisingIdClient)context).getInfo();
                ((AdvertisingIdClient)context).zza(info, zza, zza2, SystemClock.elapsedRealtime() - elapsedRealtime, zza3, null);
                ((AdvertisingIdClient)context).zza();
                return info;
            }
            finally {}
        }
        catch (Throwable t) {
            final String s;
            ((AdvertisingIdClient)context).zza(null, zza, zza2, -1L, s, t);
            throw t;
        }
        ((AdvertisingIdClient)context).zza();
    }
    
    private static com.google.android.gms.common.zza zza(final Context context, final boolean b) throws IOException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            final int googlePlayServicesAvailable = GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(context);
            if (googlePlayServicesAvailable != 0 && googlePlayServicesAvailable != 2) {
                throw new IOException("Google Play services not available");
            }
            String s;
            if (b) {
                s = "com.google.android.gms.ads.identifier.service.PERSISTENT_START";
            }
            else {
                s = "com.google.android.gms.ads.identifier.service.START";
            }
            final com.google.android.gms.common.zza zza = new com.google.android.gms.common.zza();
            final Intent intent = new Intent(s);
            intent.setPackage("com.google.android.gms");
            try {
                if (com.google.android.gms.common.stats.zza.zza().zza(context, intent, (ServiceConnection)zza, 1)) {
                    return zza;
                }
                throw new IOException("Connection failure");
            }
            catch (Throwable t) {
                throw new IOException(t);
            }
        }
        catch (PackageManager$NameNotFoundException ex) {
            throw new GooglePlayServicesNotAvailableException(9);
        }
    }
    
    private static zzgj zza(final Context context, final com.google.android.gms.common.zza zza) throws IOException {
        try {
            return zzgk.zza(zza.zza(10000L, TimeUnit.MILLISECONDS));
        }
        catch (Throwable t) {
            throw new IOException(t);
        }
        catch (InterruptedException ex) {
            throw new IOException("Interrupted exception");
        }
    }
    
    private final void zza(final boolean b) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        zzau.zzc("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.zzc) {
                this.zza();
            }
            this.zza = zza(this.zzf, this.zzg);
            this.zzb = zza(this.zzf, this.zza);
            this.zzc = true;
            if (b) {
                this.zzb();
            }
        }
    }
    
    private final boolean zza(final Info info, final boolean b, final float n, final long n2, final String s, final Throwable t) {
        if (Math.random() > n) {
            return false;
        }
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        String s2;
        if (b) {
            s2 = "1";
        }
        else {
            s2 = "0";
        }
        hashMap.put("app_context", s2);
        if (info != null) {
            String s3;
            if (info.isLimitAdTrackingEnabled()) {
                s3 = "1";
            }
            else {
                s3 = "0";
            }
            hashMap.put("limit_ad_tracking", s3);
        }
        if (info != null && info.getId() != null) {
            hashMap.put("ad_id_size", Integer.toString(info.getId().length()));
        }
        if (t != null) {
            hashMap.put("error", t.getClass().getName());
        }
        if (s != null && !s.isEmpty()) {
            hashMap.put("experiment_id", s);
        }
        hashMap.put("tag", "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(n2));
        new com.google.android.gms.ads.identifier.zza(this, hashMap).start();
        return true;
    }
    
    private final void zzb() {
        synchronized (this.zzd) {
            if (this.zze != null) {
                this.zze.zza.countDown();
                try {
                    this.zze.join();
                }
                catch (InterruptedException ex) {}
            }
            if (this.zzh > 0L) {
                this.zze = new zza(this, this.zzh);
            }
        }
    }
    
    @Override
    protected void finalize() throws Throwable {
        this.zza();
        super.finalize();
    }
    
    public Info getInfo() throws IOException {
        zzau.zzc("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            Label_0101: {
                if (!this.zzc) {
                    Object zzd = this.zzd;
                    synchronized (zzd) {
                        if (this.zze != null && this.zze.zzb) {
                            // monitorexit(zzd)
                            try {
                                this.zza(false);
                                if (this.zzc) {
                                    break Label_0101;
                                }
                                zzd = new IOException("AdvertisingIdClient cannot reconnect.");
                                throw zzd;
                            }
                            catch (Exception ex) {
                                zzd = new IOException("AdvertisingIdClient cannot reconnect.", ex);
                                throw zzd;
                            }
                        }
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
            }
            zzau.zza(this.zza);
            zzau.zza(this.zzb);
            try {
                final Info info = new Info(this.zzb.zza(), this.zzb.zza(true));
                // monitorexit(this)
                this.zzb();
                return info;
            }
            catch (RemoteException ex2) {
                Log.i("AdvertisingIdClient", "GMS remote exception ", (Throwable)ex2);
                throw new IOException("Remote exception");
            }
        }
    }
    
    public final void zza() {
        zzau.zzc("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.zzf != null && this.zza != null) {
                try {
                    if (this.zzc) {
                        com.google.android.gms.common.stats.zza.zza();
                        this.zzf.unbindService((ServiceConnection)this.zza);
                    }
                }
                catch (Throwable t) {
                    Log.i("AdvertisingIdClient", "AdvertisingIdClient unbindService failed.", t);
                }
                this.zzc = false;
                this.zzb = null;
                this.zza = null;
            }
        }
    }
    
    public static final class Info
    {
        private final String zza;
        private final boolean zzb;
        
        public Info(final String zza, final boolean zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        public final String getId() {
            return this.zza;
        }
        
        public final boolean isLimitAdTrackingEnabled() {
            return this.zzb;
        }
        
        @Override
        public final String toString() {
            final String zza = this.zza;
            final boolean zzb = this.zzb;
            final StringBuilder sb = new StringBuilder(7 + String.valueOf(zza).length());
            sb.append("{");
            sb.append(zza);
            sb.append("}");
            sb.append(zzb);
            return sb.toString();
        }
    }
    
    static final class zza extends Thread
    {
        CountDownLatch zza;
        boolean zzb;
        private WeakReference<AdvertisingIdClient> zzc;
        private long zzd;
        
        public zza(final AdvertisingIdClient advertisingIdClient, final long zzd) {
            this.zzc = new WeakReference<AdvertisingIdClient>(advertisingIdClient);
            this.zzd = zzd;
            this.zza = new CountDownLatch(1);
            this.zzb = false;
            this.start();
        }
        
        private final void zza() {
            final AdvertisingIdClient advertisingIdClient = this.zzc.get();
            if (advertisingIdClient != null) {
                advertisingIdClient.zza();
                this.zzb = true;
            }
        }
        
        @Override
        public final void run() {
            try {
                if (!this.zza.await(this.zzd, TimeUnit.MILLISECONDS)) {
                    this.zza();
                }
            }
            catch (InterruptedException ex) {
                this.zza();
            }
        }
    }
}
