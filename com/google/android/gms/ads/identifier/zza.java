package com.google.android.gms.ads.identifier;

import java.util.Iterator;
import android.net.Uri.Builder;
import java.io.IOException;
import android.util.Log;
import java.net.URL;
import java.net.HttpURLConnection;
import android.net.Uri;
import java.util.Map;

final class zza extends Thread
{
    private final /* synthetic */ Map zza;
    
    zza(final AdvertisingIdClient advertisingIdClient, final Map zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        new zze();
        final Map zza = this.zza;
        final Uri.Builder buildUpon = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204?id=gmob-apps").buildUpon();
        for (final String s : zza.keySet()) {
            buildUpon.appendQueryParameter(s, (String)zza.get(s));
        }
        final String string = buildUpon.build().toString();
        try {
            final HttpURLConnection httpURLConnection = (HttpURLConnection)new URL(string).openConnection();
            try {
                final int responseCode = httpURLConnection.getResponseCode();
                if (responseCode < 200 || responseCode >= 300) {
                    final StringBuilder sb = new StringBuilder(65 + String.valueOf(string).length());
                    sb.append("Received non-success response code ");
                    sb.append(responseCode);
                    sb.append(" from pinging URL: ");
                    sb.append(string);
                    Log.w("HttpUrlPinger", sb.toString());
                }
            }
            finally {
                httpURLConnection.disconnect();
            }
        }
        catch (IOException | RuntimeException ex4) {
            final RuntimeException ex2;
            final RuntimeException ex = ex2;
            final String message = ex.getMessage();
            final StringBuilder sb2 = new StringBuilder(27 + String.valueOf(string).length() + String.valueOf(message).length());
            sb2.append("Error while pinging URL: ");
            sb2.append(string);
            sb2.append(". ");
            sb2.append(message);
            Log.w("HttpUrlPinger", sb2.toString(), (Throwable)ex);
        }
        catch (IndexOutOfBoundsException ex3) {
            final String message2 = ex3.getMessage();
            final StringBuilder sb3 = new StringBuilder(32 + String.valueOf(string).length() + String.valueOf(message2).length());
            sb3.append("Error while parsing ping URL: ");
            sb3.append(string);
            sb3.append(". ");
            sb3.append(message2);
            Log.w("HttpUrlPinger", sb3.toString(), (Throwable)ex3);
        }
    }
}
