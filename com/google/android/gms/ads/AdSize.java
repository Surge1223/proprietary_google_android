package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzj;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.ads.internal.client.zzx;
import android.content.Context;

public final class AdSize
{
    public static final AdSize BANNER;
    public static final AdSize FLUID;
    public static final AdSize FULL_BANNER;
    public static final AdSize LARGE_BANNER;
    public static final AdSize LEADERBOARD;
    public static final AdSize MEDIUM_RECTANGLE;
    public static final AdSize SEARCH;
    public static final AdSize SMART_BANNER;
    public static final AdSize WIDE_SKYSCRAPER;
    public static final AdSize zza;
    private final int zzb;
    private final int zzc;
    private final String zzd;
    
    static {
        BANNER = new AdSize(320, 50, "320x50_mb");
        FULL_BANNER = new AdSize(468, 60, "468x60_as");
        LARGE_BANNER = new AdSize(320, 100, "320x100_as");
        LEADERBOARD = new AdSize(728, 90, "728x90_as");
        MEDIUM_RECTANGLE = new AdSize(300, 250, "300x250_as");
        WIDE_SKYSCRAPER = new AdSize(160, 600, "160x600_as");
        SMART_BANNER = new AdSize(-1, -2, "smart_banner");
        FLUID = new AdSize(-3, -4, "fluid");
        zza = new AdSize(50, 50, "50x50_mb");
        SEARCH = new AdSize(-3, 0, "search_v2");
    }
    
    public AdSize(final int n, final int n2) {
        String value;
        if (n == -1) {
            value = "FULL";
        }
        else {
            value = String.valueOf(n);
        }
        String value2;
        if (n2 == -2) {
            value2 = "AUTO";
        }
        else {
            value2 = String.valueOf(n2);
        }
        final StringBuilder sb = new StringBuilder(4 + String.valueOf(value).length() + String.valueOf(value2).length());
        sb.append(value);
        sb.append("x");
        sb.append(value2);
        sb.append("_as");
        this(n, n2, sb.toString());
    }
    
    AdSize(final int zzb, final int zzc, final String zzd) {
        if (zzb < 0 && zzb != -1 && zzb != -3) {
            final StringBuilder sb = new StringBuilder(37);
            sb.append("Invalid width for AdSize: ");
            sb.append(zzb);
            throw new IllegalArgumentException(sb.toString());
        }
        if (zzc < 0 && zzc != -2 && zzc != -4) {
            final StringBuilder sb2 = new StringBuilder(38);
            sb2.append("Invalid height for AdSize: ");
            sb2.append(zzc);
            throw new IllegalArgumentException(sb2.toString());
        }
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AdSize)) {
            return false;
        }
        final AdSize adSize = (AdSize)o;
        return this.zzb == adSize.zzb && this.zzc == adSize.zzc && this.zzd.equals(adSize.zzd);
    }
    
    public final int getHeight() {
        return this.zzc;
    }
    
    public final int getHeightInPixels(final Context context) {
        switch (this.zzc) {
            default: {
                zzx.zza();
                return zzaht.zza(context, this.zzc);
            }
            case -2: {
                return zzj.zzb(context.getResources().getDisplayMetrics());
            }
            case -4:
            case -3: {
                return -1;
            }
        }
    }
    
    public final int getWidth() {
        return this.zzb;
    }
    
    public final int getWidthInPixels(final Context context) {
        final int zzb = this.zzb;
        if (zzb == -1) {
            return zzj.zza(context.getResources().getDisplayMetrics());
        }
        switch (zzb) {
            default: {
                zzx.zza();
                return zzaht.zza(context, this.zzb);
            }
            case -4:
            case -3: {
                return -1;
            }
        }
    }
    
    @Override
    public final int hashCode() {
        return this.zzd.hashCode();
    }
    
    public final boolean isFluid() {
        return this.zzb == -3 && this.zzc == -4;
    }
    
    @Override
    public final String toString() {
        return this.zzd;
    }
}
