package com.google.android.gms.ads;

import android.location.Location;
import java.util.Date;
import com.google.ads.mediation.admob.AdMobAdapter;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.internal.client.zzbq;
import com.google.android.gms.ads.internal.client.zzbp;

public final class AdRequest
{
    private final zzbp zza;
    
    private AdRequest(final Builder builder) {
        this.zza = new zzbp(builder.zza);
    }
    
    public final zzbp zza() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private final zzbq zza;
        
        public Builder() {
            (this.zza = new zzbq()).zzb("B3EEABB8EE11C2BE770B684D95219ECB");
        }
        
        public final Builder addKeyword(final String s) {
            this.zza.zza(s);
            return this;
        }
        
        public final Builder addNetworkExtrasBundle(final Class<? extends MediationAdapter> clazz, final Bundle bundle) {
            this.zza.zza(clazz, bundle);
            if (clazz.equals(AdMobAdapter.class) && bundle.getBoolean("_emulatorLiveAds")) {
                this.zza.zzc("B3EEABB8EE11C2BE770B684D95219ECB");
            }
            return this;
        }
        
        public final Builder addTestDevice(final String s) {
            this.zza.zzb(s);
            return this;
        }
        
        public final AdRequest build() {
            return new AdRequest(this, null);
        }
        
        public final Builder setBirthday(final Date date) {
            this.zza.zza(date);
            return this;
        }
        
        public final Builder setGender(final int n) {
            this.zza.zza(n);
            return this;
        }
        
        public final Builder setIsDesignedForFamilies(final boolean b) {
            this.zza.zzc(b);
            return this;
        }
        
        public final Builder setLocation(final Location location) {
            this.zza.zza(location);
            return this;
        }
        
        public final Builder tagForChildDirectedTreatment(final boolean b) {
            this.zza.zzb(b);
            return this;
        }
    }
}
