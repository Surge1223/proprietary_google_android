package com.google.android.gms.ads;

import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.common.internal.zzau;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzbt;

public final class InterstitialAd
{
    private final zzbt zza;
    
    public InterstitialAd(final Context context) {
        this.zza = new zzbt(context);
        zzau.zza(context, "Context cannot be null");
    }
    
    public final void loadAd(final AdRequest adRequest) {
        this.zza.zza(adRequest.zza());
    }
    
    public final void setAdListener(final AdListener adListener) {
        this.zza.zza(adListener);
        if (adListener != null && adListener instanceof zza) {
            this.zza.zza((zza)adListener);
            return;
        }
        if (adListener == null) {
            this.zza.zza((zza)null);
        }
    }
    
    public final void setAdUnitId(final String s) {
        this.zza.zza(s);
    }
    
    public final void setImmersiveMode(final boolean b) {
        this.zza.zzb(b);
    }
    
    public final void show() {
        this.zza.zzh();
    }
    
    public final void zza(final RewardedVideoAdListener rewardedVideoAdListener) {
        this.zza.zza(rewardedVideoAdListener);
    }
    
    public final void zza(final boolean b) {
        this.zza.zza(true);
    }
}
