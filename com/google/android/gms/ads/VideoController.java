package com.google.android.gms.ads;

import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.ads.internal.client.zzbk;
import com.google.android.gms.ads.internal.client.zzcm;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.ads.internal.client.zzbh;

public final class VideoController
{
    private final Object zza;
    private zzbh zzb;
    private VideoLifecycleCallbacks zzc;
    
    public VideoController() {
        this.zza = new Object();
    }
    
    public final void setVideoLifecycleCallbacks(final VideoLifecycleCallbacks zzc) {
        zzau.zza(zzc, "VideoLifecycleCallbacks may not be null.");
        synchronized (this.zza) {
            this.zzc = zzc;
            if (this.zzb == null) {
                return;
            }
            try {
                this.zzb.zza(new zzcm(zzc));
            }
            catch (RemoteException ex) {
                zzaid.zzb("Unable to call setVideoLifecycleCallbacks on video controller.", (Throwable)ex);
            }
        }
    }
    
    public final zzbh zza() {
        synchronized (this.zza) {
            return this.zzb;
        }
    }
    
    public final void zza(final zzbh zzb) {
        synchronized (this.zza) {
            this.zzb = zzb;
            if (this.zzc != null) {
                this.setVideoLifecycleCallbacks(this.zzc);
            }
        }
    }
    
    public static class VideoLifecycleCallbacks
    {
        public void onVideoEnd() {
        }
        
        public void onVideoMute(final boolean b) {
        }
        
        public void onVideoPause() {
        }
        
        public void onVideoPlay() {
        }
        
        public void onVideoStart() {
        }
    }
}
