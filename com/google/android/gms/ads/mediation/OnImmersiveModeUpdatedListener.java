package com.google.android.gms.ads.mediation;

public interface OnImmersiveModeUpdatedListener
{
    void onImmersiveModeUpdated(final boolean p0);
}
