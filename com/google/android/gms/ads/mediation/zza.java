package com.google.android.gms.ads.mediation;

import java.util.Map;
import android.view.View;
import android.os.Bundle;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;

public class zza
{
    private String zza;
    private List<NativeAd.Image> zzb;
    private String zzc;
    private NativeAd.Image zzd;
    private String zze;
    private String zzf;
    private Double zzg;
    private String zzh;
    private String zzi;
    private VideoController zzj;
    private Object zzk;
    private Bundle zzl;
    private boolean zzm;
    private boolean zzn;
    
    public zza() {
        this.zzl = new Bundle();
    }
    
    public final String zza() {
        return this.zza;
    }
    
    public void zza(final View view, final Map<String, View> map, final Map<String, View> map2) {
    }
    
    public final void zza(final VideoController zzj) {
        this.zzj = zzj;
    }
    
    public final void zza(final NativeAd.Image zzd) {
        this.zzd = zzd;
    }
    
    public final void zza(final Double zzg) {
        this.zzg = zzg;
    }
    
    public final void zza(final Object zzk) {
        this.zzk = zzk;
    }
    
    public final void zza(final String zza) {
        this.zza = zza;
    }
    
    public final void zza(final List<NativeAd.Image> zzb) {
        this.zzb = zzb;
    }
    
    public final void zza(final boolean b) {
        this.zzm = true;
    }
    
    public final List<NativeAd.Image> zzb() {
        return this.zzb;
    }
    
    public final void zzb(final String zzc) {
        this.zzc = zzc;
    }
    
    public final void zzb(final boolean b) {
        this.zzn = true;
    }
    
    public final String zzc() {
        return this.zzc;
    }
    
    public final void zzc(final String zze) {
        this.zze = zze;
    }
    
    public final NativeAd.Image zzd() {
        return this.zzd;
    }
    
    public final void zzd(final String zzf) {
        this.zzf = zzf;
    }
    
    public final String zze() {
        return this.zze;
    }
    
    public final void zze(final String zzh) {
        this.zzh = zzh;
    }
    
    public final String zzf() {
        return this.zzf;
    }
    
    public final void zzf(final String zzi) {
        this.zzi = zzi;
    }
    
    public final Double zzg() {
        return this.zzg;
    }
    
    public final String zzh() {
        return this.zzh;
    }
    
    public final String zzi() {
        return this.zzi;
    }
    
    public final VideoController zzj() {
        return this.zzj;
    }
    
    public final Object zzk() {
        return this.zzk;
    }
    
    public final Bundle zzl() {
        return this.zzl;
    }
    
    public final boolean zzm() {
        return this.zzm;
    }
    
    public final boolean zzn() {
        return this.zzn;
    }
}
