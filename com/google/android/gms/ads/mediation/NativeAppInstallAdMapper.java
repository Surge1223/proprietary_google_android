package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;

public class NativeAppInstallAdMapper extends NativeAdMapper
{
    private String zza;
    private List<NativeAd.Image> zzb;
    private String zzc;
    private NativeAd.Image zzd;
    private String zze;
    private double zzf;
    private String zzg;
    private String zzh;
    
    public final String getBody() {
        return this.zzc;
    }
    
    public final String getCallToAction() {
        return this.zze;
    }
    
    public final String getHeadline() {
        return this.zza;
    }
    
    public final NativeAd.Image getIcon() {
        return this.zzd;
    }
    
    public final List<NativeAd.Image> getImages() {
        return this.zzb;
    }
    
    public final String getPrice() {
        return this.zzh;
    }
    
    public final double getStarRating() {
        return this.zzf;
    }
    
    public final String getStore() {
        return this.zzg;
    }
    
    public final void setBody(final String zzc) {
        this.zzc = zzc;
    }
    
    public final void setCallToAction(final String zze) {
        this.zze = zze;
    }
    
    public final void setHeadline(final String zza) {
        this.zza = zza;
    }
    
    public final void setIcon(final NativeAd.Image zzd) {
        this.zzd = zzd;
    }
    
    public final void setImages(final List<NativeAd.Image> zzb) {
        this.zzb = zzb;
    }
    
    public final void setPrice(final String zzh) {
        this.zzh = zzh;
    }
    
    public final void setStarRating(final double zzf) {
        this.zzf = zzf;
    }
    
    public final void setStore(final String zzg) {
        this.zzg = zzg;
    }
}
