package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;

public class NativeContentAdMapper extends NativeAdMapper
{
    private String zza;
    private List<NativeAd.Image> zzb;
    private String zzc;
    private NativeAd.Image zzd;
    private String zze;
    private String zzf;
    
    public final String getAdvertiser() {
        return this.zzf;
    }
    
    public final String getBody() {
        return this.zzc;
    }
    
    public final String getCallToAction() {
        return this.zze;
    }
    
    public final String getHeadline() {
        return this.zza;
    }
    
    public final List<NativeAd.Image> getImages() {
        return this.zzb;
    }
    
    public final NativeAd.Image getLogo() {
        return this.zzd;
    }
    
    public final void setAdvertiser(final String zzf) {
        this.zzf = zzf;
    }
    
    public final void setBody(final String zzc) {
        this.zzc = zzc;
    }
    
    public final void setCallToAction(final String zze) {
        this.zze = zze;
    }
    
    public final void setHeadline(final String zza) {
        this.zza = zza;
    }
    
    public final void setImages(final List<NativeAd.Image> zzb) {
        this.zzb = zzb;
    }
    
    public final void setLogo(final NativeAd.Image zzd) {
        this.zzd = zzd;
    }
}
