package com.google.android.gms.ads.mediation;

import java.util.Map;
import com.google.android.gms.ads.VideoController;
import android.os.Bundle;
import android.view.View;

public class NativeAdMapper
{
    protected View mAdChoicesContent;
    protected Bundle mExtras;
    protected boolean mOverrideClickHandling;
    protected boolean mOverrideImpressionRecording;
    private View zza;
    private VideoController zzb;
    private boolean zzc;
    
    public NativeAdMapper() {
        this.mExtras = new Bundle();
    }
    
    public View getAdChoicesContent() {
        return this.mAdChoicesContent;
    }
    
    public final Bundle getExtras() {
        return this.mExtras;
    }
    
    public final boolean getOverrideClickHandling() {
        return this.mOverrideClickHandling;
    }
    
    public final boolean getOverrideImpressionRecording() {
        return this.mOverrideImpressionRecording;
    }
    
    public void handleClick(final View view) {
    }
    
    public boolean hasVideoContent() {
        return this.zzc;
    }
    
    public void recordImpression() {
    }
    
    public final void setOverrideClickHandling(final boolean mOverrideClickHandling) {
        this.mOverrideClickHandling = mOverrideClickHandling;
    }
    
    public final void setOverrideImpressionRecording(final boolean mOverrideImpressionRecording) {
        this.mOverrideImpressionRecording = mOverrideImpressionRecording;
    }
    
    @Deprecated
    public void trackView(final View view) {
    }
    
    public void trackViews(final View view, final Map<String, View> map, final Map<String, View> map2) {
    }
    
    public void untrackView(final View view) {
    }
    
    public final View zza() {
        return this.zza;
    }
    
    public final void zza(final VideoController zzb) {
        this.zzb = zzb;
    }
    
    public final VideoController zzb() {
        return this.zzb;
    }
}
