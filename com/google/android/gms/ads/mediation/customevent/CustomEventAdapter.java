package com.google.android.gms.ads.mediation.customevent;

import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.AdSize;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import android.content.Context;
import com.google.android.gms.internal.zzaid;
import android.view.View;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter
{
    private View zza;
    private CustomEventBanner zzb;
    private CustomEventInterstitial zzc;
    private CustomEventNative zzd;
    
    private static <T> T zza(final String s) {
        try {
            return (T)Class.forName(s).newInstance();
        }
        catch (Throwable t) {
            final String message = t.getMessage();
            final StringBuilder sb = new StringBuilder(46 + String.valueOf(s).length() + String.valueOf(message).length());
            sb.append("Could not instantiate custom event adapter: ");
            sb.append(s);
            sb.append(". ");
            sb.append(message);
            zzaid.zze(sb.toString());
            return null;
        }
    }
    
    @Override
    public final View getBannerView() {
        return this.zza;
    }
    
    @Override
    public final void onDestroy() {
        if (this.zzb != null) {
            this.zzb.onDestroy();
        }
        if (this.zzc != null) {
            this.zzc.onDestroy();
        }
        if (this.zzd != null) {
            this.zzd.onDestroy();
        }
    }
    
    @Override
    public final void onPause() {
        if (this.zzb != null) {
            this.zzb.onPause();
        }
        if (this.zzc != null) {
            this.zzc.onPause();
        }
        if (this.zzd != null) {
            this.zzd.onPause();
        }
    }
    
    @Override
    public final void onResume() {
        if (this.zzb != null) {
            this.zzb.onResume();
        }
        if (this.zzc != null) {
            this.zzc.onResume();
        }
        if (this.zzd != null) {
            this.zzd.onResume();
        }
    }
    
    @Override
    public final void requestBannerAd(final Context context, final MediationBannerListener mediationBannerListener, final Bundle bundle, final AdSize adSize, final MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.zzb = zza(bundle.getString("class_name"));
        if (this.zzb == null) {
            mediationBannerListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 == null) {
            bundle2 = null;
        }
        else {
            bundle2 = bundle2.getBundle(bundle.getString("class_name"));
        }
        this.zzb.requestBannerAd(context, new zza(this, mediationBannerListener), bundle.getString("parameter"), adSize, mediationAdRequest, bundle2);
    }
    
    @Override
    public final void requestInterstitialAd(final Context context, final MediationInterstitialListener mediationInterstitialListener, final Bundle bundle, final MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.zzc = zza(bundle.getString("class_name"));
        if (this.zzc == null) {
            mediationInterstitialListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 == null) {
            bundle2 = null;
        }
        else {
            bundle2 = bundle2.getBundle(bundle.getString("class_name"));
        }
        this.zzc.requestInterstitialAd(context, new zzb(this, mediationInterstitialListener), bundle.getString("parameter"), mediationAdRequest, bundle2);
    }
    
    @Override
    public final void requestNativeAd(final Context context, final MediationNativeListener mediationNativeListener, final Bundle bundle, final NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle2) {
        this.zzd = zza(bundle.getString("class_name"));
        if (this.zzd == null) {
            mediationNativeListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 == null) {
            bundle2 = null;
        }
        else {
            bundle2 = bundle2.getBundle(bundle.getString("class_name"));
        }
        this.zzd.requestNativeAd(context, new zzc(this, mediationNativeListener), bundle.getString("parameter"), nativeMediationAdRequest, bundle2);
    }
    
    @Override
    public final void showInterstitial() {
        this.zzc.showInterstitial();
    }
    
    static final class zza implements CustomEventBannerListener
    {
        private final CustomEventAdapter zza;
        private final MediationBannerListener zzb;
        
        public zza(final CustomEventAdapter zza, final MediationBannerListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
    }
    
    final class zzb implements CustomEventInterstitialListener
    {
        private final CustomEventAdapter zza;
        private final MediationInterstitialListener zzb;
        
        public zzb(final CustomEventAdapter zza, final MediationInterstitialListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
    }
    
    static final class zzc implements CustomEventNativeListener
    {
        private final CustomEventAdapter zza;
        private final MediationNativeListener zzb;
        
        public zzc(final CustomEventAdapter zza, final MediationNativeListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
    }
}
