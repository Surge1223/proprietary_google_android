package com.google.android.gms.ads.mediation.customevent;

import java.util.HashMap;
import com.google.ads.mediation.NetworkExtras;

@Deprecated
public final class CustomEventExtras implements NetworkExtras
{
    private final HashMap<String, Object> zza;
    
    public CustomEventExtras() {
        this.zza = new HashMap<String, Object>();
    }
    
    public final Object getExtra(final String s) {
        return this.zza.get(s);
    }
}
