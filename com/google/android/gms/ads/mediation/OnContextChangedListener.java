package com.google.android.gms.ads.mediation;

import android.content.Context;

public interface OnContextChangedListener
{
    void onContextChanged(final Context p0);
}
