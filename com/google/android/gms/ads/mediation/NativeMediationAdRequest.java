package com.google.android.gms.ads.mediation;

import java.util.Map;
import com.google.android.gms.ads.formats.NativeAdOptions;

public interface NativeMediationAdRequest extends MediationAdRequest
{
    NativeAdOptions getNativeAdOptions();
    
    boolean isAppInstallAdRequested();
    
    boolean isContentAdRequested();
    
    boolean zza();
    
    boolean zzb();
    
    Map<String, Boolean> zzc();
}
