package com.google.android.gms.ads.mediation;

import android.os.Bundle;

public interface MediationAdapter
{
    void onDestroy();
    
    void onPause();
    
    void onResume();
    
    public static final class zza
    {
        private int zza;
        
        public final Bundle zza() {
            final Bundle bundle = new Bundle();
            bundle.putInt("capabilities", this.zza);
            return bundle;
        }
        
        public final zza zza(final int n) {
            this.zza = 1;
            return this;
        }
    }
}
