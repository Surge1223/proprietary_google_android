package com.google.android.gms.ads.mediation;

import android.location.Location;
import java.util.Set;
import java.util.Date;

public interface MediationAdRequest
{
    Date getBirthday();
    
    int getGender();
    
    Set<String> getKeywords();
    
    Location getLocation();
    
    boolean isDesignedForFamilies();
    
    boolean isTesting();
    
    int taggedForChildDirectedTreatment();
}
