package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.internal.client.zzbh;

public interface zzb
{
    zzbh getVideoController();
}
