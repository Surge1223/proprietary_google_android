package com.google.android.gms.ads.reward.mediation;

import com.google.android.gms.ads.reward.RewardItem;

public interface MediationRewardedVideoAdListener
{
    void onAdClosed(final MediationRewardedVideoAdAdapter p0);
    
    void onAdFailedToLoad(final MediationRewardedVideoAdAdapter p0, final int p1);
    
    void onAdLeftApplication(final MediationRewardedVideoAdAdapter p0);
    
    void onAdLoaded(final MediationRewardedVideoAdAdapter p0);
    
    void onAdOpened(final MediationRewardedVideoAdAdapter p0);
    
    void onInitializationSucceeded(final MediationRewardedVideoAdAdapter p0);
    
    void onRewarded(final MediationRewardedVideoAdAdapter p0, final RewardItem p1);
    
    void onVideoCompleted(final MediationRewardedVideoAdAdapter p0);
    
    void onVideoStarted(final MediationRewardedVideoAdAdapter p0);
}
