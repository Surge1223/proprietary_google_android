package com.google.android.gms.ads;

import android.util.AttributeSet;
import android.content.Context;

public final class NativeExpressAdView extends BaseAdView
{
    public NativeExpressAdView(final Context context) {
        super(context, 1);
    }
    
    public NativeExpressAdView(final Context context, final AttributeSet set) {
        super(context, set, 1);
    }
    
    public NativeExpressAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n, 1);
    }
    
    public final VideoController getVideoController() {
        return this.zza.zzm();
    }
    
    public final VideoOptions getVideoOptions() {
        return this.zza.zzo();
    }
    
    public final void setVideoOptions(final VideoOptions videoOptions) {
        this.zza.zza(videoOptions);
    }
}
