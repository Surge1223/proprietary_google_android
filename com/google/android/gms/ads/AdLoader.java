package com.google.android.gms.ads;

import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzqb;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.internal.client.zzad;
import com.google.android.gms.ads.internal.client.zzc;
import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpx;
import com.google.android.gms.internal.zzpy;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.internal.zzpa;
import com.google.android.gms.internal.zzpw;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zzpv;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.internal.zztj;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.ads.internal.client.zzaj;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.ads.internal.client.zzbp;
import com.google.android.gms.ads.internal.client.zzag;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzi;

public class AdLoader
{
    private final zzi zza;
    private final Context zzb;
    private final zzag zzc;
    
    AdLoader(final Context context, final zzag zzag) {
        this(context, zzag, zzi.zza);
    }
    
    private AdLoader(final Context zzb, final zzag zzc, final zzi zza) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zza = zza;
    }
    
    private final void zza(final zzbp zzbp) {
        try {
            this.zzc.zza(zzi.zza(this.zzb, zzbp));
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to load ad.", (Throwable)ex);
        }
    }
    
    public void loadAd(final AdRequest adRequest) {
        this.zza(adRequest.zza());
    }
    
    public static class Builder
    {
        private final Context zza;
        private final zzaj zzb;
        
        private Builder(final Context zza, final zzaj zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        public Builder(final Context context, final String s) {
            this(zzau.zza(context, "context cannot be null"), zzx.zzb().zza(context, s, new zztj()));
        }
        
        public AdLoader build() {
            try {
                return new AdLoader(this.zza, this.zzb.zza());
            }
            catch (RemoteException ex) {
                zzaid.zzb("Failed to build AdLoader.", (Throwable)ex);
                return null;
            }
        }
        
        public Builder forAppInstallAd(final NativeAppInstallAd.OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
            try {
                this.zzb.zza(new zzpv(onAppInstallAdLoadedListener));
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to add app install ad listener", (Throwable)ex);
            }
            return this;
        }
        
        public Builder forContentAd(final NativeContentAd.OnContentAdLoadedListener onContentAdLoadedListener) {
            try {
                this.zzb.zza(new zzpw(onContentAdLoadedListener));
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to add content ad listener", (Throwable)ex);
            }
            return this;
        }
        
        public Builder forCustomTemplateAd(final String s, final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener, final NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
            try {
                final zzaj zzb = this.zzb;
                final zzpy zzpy = new zzpy(onCustomTemplateAdLoadedListener);
                zzpd zzpd;
                if (onCustomClickListener == null) {
                    zzpd = null;
                }
                else {
                    zzpd = new zzpx(onCustomClickListener);
                }
                zzb.zza(s, zzpy, zzpd);
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to add custom template ad listener", (Throwable)ex);
            }
            return this;
        }
        
        public Builder withAdListener(final AdListener adListener) {
            try {
                this.zzb.zza(new zzc(adListener));
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to set AdListener.", (Throwable)ex);
            }
            return this;
        }
        
        public Builder withNativeAdOptions(final NativeAdOptions nativeAdOptions) {
            try {
                this.zzb.zza(new zznm(nativeAdOptions));
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to specify native ad options", (Throwable)ex);
            }
            return this;
        }
        
        public final Builder zza(final UnifiedNativeAd.zza zza) {
            try {
                this.zzb.zza(new zzqb(zza));
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to add google native ad listener", (Throwable)ex);
            }
            return this;
        }
    }
}
