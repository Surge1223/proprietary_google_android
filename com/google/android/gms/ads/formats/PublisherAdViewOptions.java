package com.google.android.gms.ads.formats;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.zzau;
import android.os.IBinder;
import com.google.android.gms.ads.internal.client.zzat;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class PublisherAdViewOptions extends zzbid
{
    public static final Parcelable.Creator<PublisherAdViewOptions> CREATOR;
    private final boolean zza;
    private final zzat zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    PublisherAdViewOptions(final boolean zza, final IBinder binder) {
        this.zza = zza;
        zzat zza2;
        if (binder != null) {
            zza2 = zzau.zza(binder);
        }
        else {
            zza2 = null;
        }
        this.zzb = zza2;
    }
    
    public final boolean getManualImpressionsEnabled() {
        return this.zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.getManualImpressionsEnabled());
        IBinder binder;
        if (this.zzb == null) {
            binder = null;
        }
        else {
            binder = this.zzb.asBinder();
        }
        zzbig.zza(parcel, 2, binder, false);
        zzbig.zza(parcel, zza);
    }
}
