package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.VideoController;
import java.util.List;

public abstract class NativeContentAd extends NativeAd
{
    public abstract CharSequence getAdvertiser();
    
    public abstract CharSequence getBody();
    
    public abstract CharSequence getCallToAction();
    
    public abstract CharSequence getHeadline();
    
    public abstract List<Image> getImages();
    
    public abstract Image getLogo();
    
    public abstract VideoController getVideoController();
    
    public interface OnContentAdLoadedListener
    {
        void onContentAdLoaded(final NativeContentAd p0);
    }
}
