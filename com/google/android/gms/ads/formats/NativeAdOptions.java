package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.VideoOptions;

public final class NativeAdOptions
{
    private final boolean zza;
    private final int zzb;
    private final boolean zzc;
    private final int zzd;
    private final VideoOptions zze;
    
    private NativeAdOptions(final Builder builder) {
        this.zza = builder.zza;
        this.zzb = builder.zzb;
        this.zzc = builder.zzc;
        this.zzd = builder.zze;
        this.zze = builder.zzd;
    }
    
    public final int getAdChoicesPlacement() {
        return this.zzd;
    }
    
    public final int getImageOrientation() {
        return this.zzb;
    }
    
    public final VideoOptions getVideoOptions() {
        return this.zze;
    }
    
    public final boolean shouldRequestMultipleImages() {
        return this.zzc;
    }
    
    public final boolean shouldReturnUrlsForImageAssets() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private boolean zza;
        private int zzb;
        private boolean zzc;
        private VideoOptions zzd;
        private int zze;
        
        public Builder() {
            this.zza = false;
            this.zzb = -1;
            this.zzc = false;
            this.zze = 1;
        }
        
        public final NativeAdOptions build() {
            return new NativeAdOptions(this, null);
        }
        
        public final Builder setAdChoicesPlacement(final int zze) {
            this.zze = zze;
            return this;
        }
        
        public final Builder setImageOrientation(final int zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public final Builder setRequestMultipleImages(final boolean zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public final Builder setReturnUrlsForImageAssets(final boolean zza) {
            this.zza = zza;
            return this;
        }
        
        public final Builder setVideoOptions(final VideoOptions zzd) {
            this.zzd = zzd;
            return this;
        }
    }
}
