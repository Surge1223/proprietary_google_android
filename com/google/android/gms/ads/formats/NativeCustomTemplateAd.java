package com.google.android.gms.ads.formats;

public interface NativeCustomTemplateAd
{
    String getCustomTemplateId();
    
    public interface OnCustomClickListener
    {
        void onCustomClick(final NativeCustomTemplateAd p0, final String p1);
    }
    
    public interface OnCustomTemplateAdLoadedListener
    {
        void onCustomTemplateAdLoaded(final NativeCustomTemplateAd p0);
    }
}
