package com.google.android.gms.ads.formats;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzaid;
import java.lang.ref.WeakReference;
import com.google.android.gms.internal.zzog;
import android.view.View;
import java.util.WeakHashMap;

public final class NativeAdViewHolder
{
    public static WeakHashMap<View, NativeAdViewHolder> zza;
    private zzog zzb;
    private WeakReference<View> zzc;
    
    static {
        NativeAdViewHolder.zza = new WeakHashMap<View, NativeAdViewHolder>();
    }
    
    public final void setNativeAd(final NativeAd nativeAd) {
        View view;
        if (this.zzc != null) {
            view = this.zzc.get();
        }
        else {
            view = null;
        }
        if (view == null) {
            zzaid.zze("NativeAdViewHolder.setNativeAd containerView doesn't exist, returning");
            return;
        }
        if (!NativeAdViewHolder.zza.containsKey(view)) {
            NativeAdViewHolder.zza.put(view, this);
        }
        if (this.zzb != null) {
            try {
                this.zzb.zza((IObjectWrapper)nativeAd.zza());
            }
            catch (RemoteException ex) {
                zzaid.zzb("Unable to call setNativeAd on delegate", (Throwable)ex);
            }
        }
    }
}
