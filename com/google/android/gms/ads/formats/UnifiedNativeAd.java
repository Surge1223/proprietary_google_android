package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.VideoController;
import java.util.List;

public abstract class UnifiedNativeAd
{
    public abstract String zza();
    
    public abstract List<NativeAd.Image> zzb();
    
    public abstract String zzc();
    
    public abstract NativeAd.Image zzd();
    
    public abstract String zze();
    
    public abstract String zzf();
    
    public abstract Double zzg();
    
    public abstract String zzh();
    
    public abstract String zzi();
    
    public abstract VideoController zzj();
    
    public abstract Object zzk();
    
    public interface zza
    {
        void zza(final UnifiedNativeAd p0);
    }
}
