package com.google.android.gms.ads.formats;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.common.internal.zzau;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.gms.internal.zzob;
import android.widget.FrameLayout;

public class NativeAdView extends FrameLayout
{
    private final FrameLayout zza;
    private final zzob zzb;
    
    public NativeAdView(final Context context) {
        super(context);
        this.zza = this.zza(context);
        this.zzb = this.zza();
    }
    
    public NativeAdView(final Context context, final AttributeSet set) {
        super(context, set);
        this.zza = this.zza(context);
        this.zzb = this.zza();
    }
    
    public NativeAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.zza = this.zza(context);
        this.zzb = this.zza();
    }
    
    public NativeAdView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.zza = this.zza(context);
        this.zzb = this.zza();
    }
    
    private final FrameLayout zza(final Context context) {
        final FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams((ViewGroup.LayoutParams)new FrameLayout$LayoutParams(-1, -1));
        this.addView((View)frameLayout);
        return frameLayout;
    }
    
    private final zzob zza() {
        zzau.zza(this.zza, "createDelegate must be called after mOverlayFrame has been created");
        if (this.isInEditMode()) {
            return null;
        }
        return zzx.zzb().zza(this.zza.getContext(), this, this.zza);
    }
    
    public void addView(final View view, final int n, final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        super.addView(view, n, viewGroup.LayoutParams);
        super.bringChildToFront((View)this.zza);
    }
    
    public void bringChildToFront(final View view) {
        super.bringChildToFront(view);
        if (this.zza != view) {
            super.bringChildToFront((View)this.zza);
        }
    }
    
    public AdChoicesView getAdChoicesView() {
        final View zza = this.zza("1098");
        if (zza instanceof AdChoicesView) {
            return (AdChoicesView)zza;
        }
        return null;
    }
    
    public void onVisibilityChanged(final View view, final int n) {
        super.onVisibilityChanged(view, n);
        if (this.zzb != null) {
            try {
                this.zzb.zza(zzn.zza(view), n);
            }
            catch (RemoteException ex) {
                zzaid.zzb("Unable to call onVisibilityChanged on delegate", (Throwable)ex);
            }
        }
    }
    
    public void removeAllViews() {
        super.removeAllViews();
        super.addView((View)this.zza);
    }
    
    public void removeView(final View view) {
        if (this.zza == view) {
            return;
        }
        super.removeView(view);
    }
    
    public void setAdChoicesView(final AdChoicesView adChoicesView) {
        this.zza("1098", (View)adChoicesView);
    }
    
    public void setNativeAd(final NativeAd nativeAd) {
        try {
            this.zzb.zza((IObjectWrapper)nativeAd.zza());
        }
        catch (RemoteException ex) {
            zzaid.zzb("Unable to call setNativeAd on delegate", (Throwable)ex);
        }
    }
    
    protected final View zza(final String s) {
        try {
            final IObjectWrapper zza = this.zzb.zza(s);
            if (zza != null) {
                return (View)zzn.zza(zza);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Unable to call getAssetView on delegate", (Throwable)ex);
        }
        return null;
    }
    
    protected final void zza(final String s, final View view) {
        try {
            this.zzb.zza(s, zzn.zza(view));
        }
        catch (RemoteException ex) {
            zzaid.zzb("Unable to call setAssetView on delegate", (Throwable)ex);
        }
    }
}
