package com.google.android.gms.ads.formats;

import android.net.Uri;
import android.graphics.drawable.Drawable;

public abstract class NativeAd
{
    protected abstract Object zza();
    
    public abstract static class AdChoicesInfo
    {
    }
    
    public abstract static class Image
    {
        public abstract Drawable getDrawable();
        
        public abstract double getScale();
        
        public abstract Uri getUri();
    }
}
