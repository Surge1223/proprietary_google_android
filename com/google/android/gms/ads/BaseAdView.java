package com.google.android.gms.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.internal.zzaid;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzbr;
import android.view.ViewGroup;

class BaseAdView extends ViewGroup
{
    protected final zzbr zza;
    
    public BaseAdView(final Context context, final int n) {
        super(context);
        this.zza = new zzbr(this, n);
    }
    
    public BaseAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set);
        this.zza = new zzbr(this, set, false, n);
    }
    
    public BaseAdView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n);
        this.zza = new zzbr(this, set, false, n2);
    }
    
    public void destroy() {
        this.zza.zza();
    }
    
    public AdListener getAdListener() {
        return this.zza.zzb();
    }
    
    public AdSize getAdSize() {
        return this.zza.zzc();
    }
    
    public String getAdUnitId() {
        return this.zza.zze();
    }
    
    public String getMediationAdapterClassName() {
        return this.zza.zzk();
    }
    
    public void loadAd(final AdRequest adRequest) {
        this.zza.zza(adRequest.zza());
    }
    
    protected void onLayout(final boolean b, int n, int n2, final int n3, final int n4) {
        final View child = this.getChildAt(0);
        if (child != null && child.getVisibility() != 8) {
            final int measuredWidth = child.getMeasuredWidth();
            final int measuredHeight = child.getMeasuredHeight();
            n = (n3 - n - measuredWidth) / 2;
            n2 = (n4 - n2 - measuredHeight) / 2;
            child.layout(n, n2, measuredWidth + n, measuredHeight + n2);
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        int n3 = 0;
        final View child = this.getChildAt(0);
        int n4;
        if (child != null && child.getVisibility() != 8) {
            this.measureChild(child, n, n2);
            n3 = child.getMeasuredWidth();
            n4 = child.getMeasuredHeight();
        }
        else {
            AdSize adSize = null;
            try {
                adSize = this.getAdSize();
            }
            catch (NullPointerException ex) {
                zzaid.zzb("Unable to retrieve ad size.", ex);
            }
            if (adSize != null) {
                final Context context = this.getContext();
                n3 = adSize.getWidthInPixels(context);
                n4 = adSize.getHeightInPixels(context);
            }
            else {
                n4 = 0;
            }
        }
        this.setMeasuredDimension(View.resolveSize(Math.max(n3, this.getSuggestedMinimumWidth()), n), View.resolveSize(Math.max(n4, this.getSuggestedMinimumHeight()), n2));
    }
    
    public void pause() {
        this.zza.zzh();
    }
    
    public void resume() {
        this.zza.zzj();
    }
    
    public void setAdListener(final AdListener adListener) {
        this.zza.zza(adListener);
        if (adListener == null) {
            this.zza.zza((zza)null);
            this.zza.zza((AppEventListener)null);
            return;
        }
        if (adListener instanceof zza) {
            this.zza.zza((zza)adListener);
        }
        if (adListener instanceof AppEventListener) {
            this.zza.zza((AppEventListener)adListener);
        }
    }
    
    public void setAdSize(final AdSize adSize) {
        this.zza.zza(adSize);
    }
    
    public void setAdUnitId(final String s) {
        this.zza.zza(s);
    }
}
