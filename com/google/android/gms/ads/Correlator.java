package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzy;

public final class Correlator
{
    private zzy zza;
    
    public final zzy zza() {
        return this.zza;
    }
}
