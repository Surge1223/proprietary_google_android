package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzcn;

public final class VideoOptions
{
    private final boolean zza;
    private final boolean zzb;
    private final boolean zzc;
    
    public VideoOptions(final zzcn zzcn) {
        this.zza = zzcn.zza;
        this.zzb = zzcn.zzb;
        this.zzc = zzcn.zzc;
    }
    
    public final boolean getClickToExpandRequested() {
        return this.zzc;
    }
    
    public final boolean getCustomControlsRequested() {
        return this.zzb;
    }
    
    public final boolean getStartMuted() {
        return this.zza;
    }
}
