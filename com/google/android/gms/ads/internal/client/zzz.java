package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;

public class zzz extends AdListener
{
    private final Object zza;
    private AdListener zzb;
    
    public zzz() {
        this.zza = new Object();
    }
    
    @Override
    public void onAdClosed() {
        synchronized (this.zza) {
            if (this.zzb != null) {
                this.zzb.onAdClosed();
            }
        }
    }
    
    @Override
    public void onAdFailedToLoad(final int n) {
        synchronized (this.zza) {
            if (this.zzb != null) {
                this.zzb.onAdFailedToLoad(n);
            }
        }
    }
    
    @Override
    public void onAdLeftApplication() {
        synchronized (this.zza) {
            if (this.zzb != null) {
                this.zzb.onAdLeftApplication();
            }
        }
    }
    
    @Override
    public void onAdLoaded() {
        synchronized (this.zza) {
            if (this.zzb != null) {
                this.zzb.onAdLoaded();
            }
        }
    }
    
    @Override
    public void onAdOpened() {
        synchronized (this.zza) {
            if (this.zzb != null) {
                this.zzb.onAdOpened();
            }
        }
    }
    
    public final void zza(final AdListener zzb) {
        synchronized (this.zza) {
            this.zzb = zzb;
        }
    }
}
