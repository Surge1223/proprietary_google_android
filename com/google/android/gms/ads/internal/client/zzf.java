package com.google.android.gms.ads.internal.client;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.location.Location;
import java.util.List;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzf extends zzbid
{
    public static final Parcelable.Creator<zzf> CREATOR;
    public final int zza;
    public final long zzb;
    public final Bundle zzc;
    public final int zzd;
    public final List<String> zze;
    public final boolean zzf;
    public final int zzg;
    public final boolean zzh;
    public final String zzi;
    public final zzcj zzj;
    public final Location zzk;
    public final String zzl;
    public final Bundle zzm;
    public final Bundle zzn;
    public final List<String> zzo;
    public final String zzp;
    public final String zzq;
    public final boolean zzr;
    
    static {
        CREATOR = (Parcelable.Creator)new zzh();
    }
    
    public zzf(final int zza, final long zzb, Bundle bundle, final int zzd, final List<String> zze, final boolean zzf, final int zzg, final boolean zzh, final String zzi, final zzcj zzj, final Location zzk, final String zzl, final Bundle bundle2, final Bundle zzn, final List<String> zzo, final String zzp, final String zzq, final boolean zzr) {
        this.zza = zza;
        this.zzb = zzb;
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.zzc = bundle;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        if (bundle2 == null) {
            bundle = new Bundle();
        }
        else {
            bundle = bundle2;
        }
        this.zzm = bundle;
        this.zzn = zzn;
        this.zzo = zzo;
        this.zzp = zzp;
        this.zzq = zzq;
        this.zzr = zzr;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzf)) {
            return false;
        }
        final zzf zzf = (zzf)o;
        return this.zza == zzf.zza && this.zzb == zzf.zzb && zzak.zza(this.zzc, zzf.zzc) && this.zzd == zzf.zzd && zzak.zza(this.zze, zzf.zze) && this.zzf == zzf.zzf && this.zzg == zzf.zzg && this.zzh == zzf.zzh && zzak.zza(this.zzi, zzf.zzi) && zzak.zza(this.zzj, zzf.zzj) && zzak.zza(this.zzk, zzf.zzk) && zzak.zza(this.zzl, zzf.zzl) && zzak.zza(this.zzm, zzf.zzm) && zzak.zza(this.zzn, zzf.zzn) && zzak.zza(this.zzo, zzf.zzo) && zzak.zza(this.zzp, zzf.zzp) && zzak.zza(this.zzq, zzf.zzq) && this.zzr == zzf.zzr;
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg, this.zzh, this.zzi, this.zzj, this.zzk, this.zzl, this.zzm, this.zzn, this.zzo, this.zzp, this.zzq, this.zzr });
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, 4, this.zzd);
        zzbig.zzb(parcel, 5, this.zze, false);
        zzbig.zza(parcel, 6, this.zzf);
        zzbig.zza(parcel, 7, this.zzg);
        zzbig.zza(parcel, 8, this.zzh);
        zzbig.zza(parcel, 9, this.zzi, false);
        zzbig.zza(parcel, 10, (Parcelable)this.zzj, n, false);
        zzbig.zza(parcel, 11, (Parcelable)this.zzk, n, false);
        zzbig.zza(parcel, 12, this.zzl, false);
        zzbig.zza(parcel, 13, this.zzm, false);
        zzbig.zza(parcel, 14, this.zzn, false);
        zzbig.zzb(parcel, 15, this.zzo, false);
        zzbig.zza(parcel, 16, this.zzp, false);
        zzbig.zza(parcel, 17, this.zzq, false);
        zzbig.zza(parcel, 18, this.zzr);
        zzbig.zza(parcel, zza);
    }
}
