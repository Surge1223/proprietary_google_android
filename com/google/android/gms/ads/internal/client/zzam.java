package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzam extends IInterface
{
    IBinder zza(final IObjectWrapper p0, final String p1, final zztk p2, final int p3) throws RemoteException;
}
