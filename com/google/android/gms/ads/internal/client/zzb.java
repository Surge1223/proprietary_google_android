package com.google.android.gms.ads.internal.client;

public final class zzb extends zzab
{
    private final zza zza;
    
    public zzb(final zza zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        this.zza.onAdClicked();
    }
}
