package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzwi;
import com.google.android.gms.internal.zzwc;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzach;

public final class zzcc extends zzap
{
    private zzad zza;
    
    @Override
    public final void zza(final zzaa zzaa) {
    }
    
    @Override
    public final void zza(final zzad zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzat zzat) {
    }
    
    @Override
    public final void zza(final zzaz zzaz) {
    }
    
    @Override
    public final void zza(final zzbn zzbn) {
    }
    
    @Override
    public final void zza(final zzcn zzcn) {
    }
    
    @Override
    public final void zza(final zzj zzj) {
    }
    
    @Override
    public final void zza(final zzach zzach) {
    }
    
    @Override
    public final void zza(final zzmg zzmg) {
    }
    
    @Override
    public final void zza(final zzwc zzwc) {
    }
    
    @Override
    public final void zza(final zzwi zzwi, final String s) {
    }
    
    @Override
    public final void zza(final String s) {
    }
    
    @Override
    public final void zza(final boolean b) {
    }
    
    @Override
    public final String zzal() {
        return null;
    }
    
    @Override
    public final String zzam() {
        return null;
    }
    
    @Override
    public final void zzan() {
    }
    
    @Override
    public final void zzb() {
    }
    
    @Override
    public final void zzb(final boolean b) {
    }
    
    @Override
    public final boolean zzb(final zzf zzf) {
        zzaid.zzc("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzaht.zza.post((Runnable)new zzcd(this));
        return false;
    }
    
    @Override
    public final IObjectWrapper zzc() {
        return null;
    }
    
    @Override
    public final zzj zzd() {
        return null;
    }
    
    @Override
    public final boolean zze() {
        return false;
    }
    
    @Override
    public final void zzg() {
    }
    
    @Override
    public final void zzh() {
    }
    
    @Override
    public final void zzi() {
    }
    
    @Override
    public final void zzj() {
    }
    
    @Override
    public final boolean zzk() {
        return false;
    }
    
    @Override
    public final zzbh zzl() {
        return null;
    }
    
    @Override
    public final String zzv() {
        return null;
    }
    
    @Override
    public final zzat zzw() {
        return null;
    }
    
    @Override
    public final zzad zzx() {
        return null;
    }
}
