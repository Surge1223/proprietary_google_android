package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.view.View;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzmj;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzaht;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.VideoController;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.internal.zztj;

public final class zzbr
{
    private final zztj zza;
    private final zzi zzb;
    private final AtomicBoolean zzc;
    private final VideoController zzd;
    private final zzz zze;
    private zza zzf;
    private AdListener zzg;
    private AdSize[] zzh;
    private AppEventListener zzi;
    private Correlator zzj;
    private zzao zzk;
    private OnCustomRenderedAdLoadedListener zzl;
    private VideoOptions zzm;
    private String zzn;
    private ViewGroup zzo;
    private int zzp;
    private boolean zzq;
    
    public zzbr(final ViewGroup viewGroup) {
        this(viewGroup, null, false, com.google.android.gms.ads.internal.client.zzi.zza, 0);
    }
    
    public zzbr(final ViewGroup viewGroup, final int n) {
        this(viewGroup, null, false, com.google.android.gms.ads.internal.client.zzi.zza, n);
    }
    
    public zzbr(final ViewGroup viewGroup, final AttributeSet set, final boolean b) {
        this(viewGroup, set, b, com.google.android.gms.ads.internal.client.zzi.zza, 0);
    }
    
    public zzbr(final ViewGroup viewGroup, final AttributeSet set, final boolean b, final int n) {
        this(viewGroup, set, false, com.google.android.gms.ads.internal.client.zzi.zza, n);
    }
    
    private zzbr(final ViewGroup viewGroup, final AttributeSet set, final boolean b, final zzi zzi, final int n) {
        this(viewGroup, set, b, zzi, null, n);
    }
    
    private zzbr(final ViewGroup zzo, final AttributeSet set, final boolean b, zzi context, final zzao zzao, int zzp) {
        this.zza = new zztj();
        this.zzd = new VideoController();
        this.zze = new zzbs(this);
        this.zzo = zzo;
        this.zzb = context;
        this.zzk = null;
        this.zzc = new AtomicBoolean(false);
        this.zzp = zzp;
        if (set != null) {
            context = (zzi)zzo.getContext();
            try {
                final zzm zzm = new zzm((Context)context, set);
                this.zzh = zzm.zza(b);
                this.zzn = zzm.zza();
                if (zzo.isInEditMode()) {
                    final zzaht zza = zzx.zza();
                    final AdSize adSize = this.zzh[0];
                    zzp = this.zzp;
                    context = (zzi)new zzj((Context)context, adSize);
                    ((zzj)context).zzj = zza(zzp);
                    zza.zza(zzo, (zzj)context, "Ads by Google");
                }
            }
            catch (IllegalArgumentException ex) {
                zzx.zza().zza(zzo, new zzj((Context)context, AdSize.BANNER), ex.getMessage(), ex.getMessage());
            }
        }
    }
    
    private static zzj zza(final Context context, final AdSize[] array, final int n) {
        final zzj zzj = new zzj(context, array);
        zzj.zzj = zza(n);
        return zzj;
    }
    
    private static boolean zza(final int n) {
        return n == 1;
    }
    
    public final void zza() {
        try {
            if (this.zzk != null) {
                this.zzk.zzb();
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to destroy AdView.", (Throwable)ex);
        }
    }
    
    public final void zza(final AdListener zzg) {
        this.zzg = zzg;
        this.zze.zza(zzg);
    }
    
    public final void zza(final Correlator zzj) {
        this.zzj = zzj;
        try {
            if (this.zzk != null) {
                final zzao zzk = this.zzk;
                zzaz zza;
                if (this.zzj == null) {
                    zza = null;
                }
                else {
                    zza = this.zzj.zza();
                }
                zzk.zza(zza);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set correlator.", (Throwable)ex);
        }
    }
    
    public final void zza(final VideoOptions zzm) {
        this.zzm = zzm;
        try {
            if (this.zzk != null) {
                final zzao zzk = this.zzk;
                zzcn zzcn;
                if (zzm == null) {
                    zzcn = null;
                }
                else {
                    zzcn = new zzcn(zzm);
                }
                zzk.zza(zzcn);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set video options.", (Throwable)ex);
        }
    }
    
    public final void zza(final AppEventListener zzi) {
        try {
            this.zzi = zzi;
            if (this.zzk != null) {
                final zzao zzk = this.zzk;
                zzat zzat;
                if (zzi != null) {
                    zzat = new zzl(zzi);
                }
                else {
                    zzat = null;
                }
                zzk.zza(zzat);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the AppEventListener.", (Throwable)ex);
        }
    }
    
    public final void zza(final OnCustomRenderedAdLoadedListener zzl) {
        this.zzl = zzl;
        try {
            if (this.zzk != null) {
                final zzao zzk = this.zzk;
                zzmg zzmg;
                if (zzl != null) {
                    zzmg = new zzmj(zzl);
                }
                else {
                    zzmg = null;
                }
                zzk.zza(zzmg);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the onCustomRenderedAdLoadedListener.", (Throwable)ex);
        }
    }
    
    public final void zza(final zza zzf) {
        try {
            this.zzf = zzf;
            if (this.zzk != null) {
                final zzao zzk = this.zzk;
                zzaa zzaa;
                if (zzf != null) {
                    zzaa = new zzb(zzf);
                }
                else {
                    zzaa = null;
                }
                zzk.zza(zzaa);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the AdClickListener.", (Throwable)ex);
        }
    }
    
    public final void zza(final zzbp zzbp) {
        try {
            if (this.zzk == null) {
                if ((this.zzh == null || this.zzn == null) && this.zzk == null) {
                    throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
                }
                final Context context = this.zzo.getContext();
                final zzj zza = zza(context, this.zzh, this.zzp);
                zzao zzk;
                if ("search_v2".equals(zza.zza)) {
                    zzk = com.google.android.gms.ads.internal.client.zzn.zza(context, false, (zzn.zza<zzao>)new zzp(zzx.zzb(), context, zza, this.zzn));
                }
                else {
                    zzk = com.google.android.gms.ads.internal.client.zzn.zza(context, false, (zzn.zza<zzao>)new zzo(zzx.zzb(), context, zza, this.zzn, this.zza));
                }
                (this.zzk = zzk).zza(new zzc(this.zze));
                if (this.zzf != null) {
                    this.zzk.zza(new zzb(this.zzf));
                }
                if (this.zzi != null) {
                    this.zzk.zza(new zzl(this.zzi));
                }
                if (this.zzl != null) {
                    this.zzk.zza(new zzmj(this.zzl));
                }
                if (this.zzj != null) {
                    this.zzk.zza(this.zzj.zza());
                }
                if (this.zzm != null) {
                    this.zzk.zza(new zzcn(this.zzm));
                }
                this.zzk.zza(this.zzq);
                try {
                    final IObjectWrapper zzc = this.zzk.zzc();
                    if (zzc != null) {
                        this.zzo.addView((View)com.google.android.gms.dynamic.zzn.zza(zzc));
                    }
                }
                catch (RemoteException ex) {
                    zzaid.zzc("Failed to get an ad frame.", (Throwable)ex);
                }
            }
            if (this.zzk.zzb(com.google.android.gms.ads.internal.client.zzi.zza(this.zzo.getContext(), zzbp))) {
                this.zza.zza(zzbp.zzj());
            }
        }
        catch (RemoteException ex2) {
            zzaid.zzc("Failed to load ad.", (Throwable)ex2);
        }
    }
    
    public final void zza(final String zzn) {
        if (this.zzn == null) {
            this.zzn = zzn;
            return;
        }
        throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
    }
    
    public final void zza(final boolean zzq) {
        this.zzq = zzq;
        try {
            if (this.zzk != null) {
                this.zzk.zza(this.zzq);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set manual impressions.", (Throwable)ex);
        }
    }
    
    public final void zza(final AdSize... array) {
        if (this.zzh == null) {
            this.zzb(array);
            return;
        }
        throw new IllegalStateException("The ad size can only be set once on AdView.");
    }
    
    public final AdListener zzb() {
        return this.zzg;
    }
    
    public final void zzb(final AdSize... zzh) {
        this.zzh = zzh;
        try {
            if (this.zzk != null) {
                this.zzk.zza(zza(this.zzo.getContext(), this.zzh, this.zzp));
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the ad size.", (Throwable)ex);
        }
        this.zzo.requestLayout();
    }
    
    public final AdSize zzc() {
        try {
            if (this.zzk != null) {
                final zzj zzd = this.zzk.zzd();
                if (zzd != null) {
                    return zzd.zzb();
                }
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to get the current AdSize.", (Throwable)ex);
        }
        if (this.zzh != null) {
            return this.zzh[0];
        }
        return null;
    }
    
    public final AdSize[] zzd() {
        return this.zzh;
    }
    
    public final String zze() {
        if (this.zzn == null && this.zzk != null) {
            try {
                this.zzn = this.zzk.zzv();
            }
            catch (RemoteException ex) {
                zzaid.zzc("Failed to get ad unit id.", (Throwable)ex);
            }
        }
        return this.zzn;
    }
    
    public final AppEventListener zzf() {
        return this.zzi;
    }
    
    public final OnCustomRenderedAdLoadedListener zzg() {
        return this.zzl;
    }
    
    public final void zzh() {
        try {
            if (this.zzk != null) {
                this.zzk.zzh();
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to call pause.", (Throwable)ex);
        }
    }
    
    public final void zzj() {
        try {
            if (this.zzk != null) {
                this.zzk.zzi();
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to call resume.", (Throwable)ex);
        }
    }
    
    public final String zzk() {
        try {
            if (this.zzk != null) {
                return this.zzk.zzam();
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to get the mediation adapter class name.", (Throwable)ex);
        }
        return null;
    }
    
    public final VideoController zzm() {
        return this.zzd;
    }
    
    public final zzbh zzn() {
        if (this.zzk == null) {
            return null;
        }
        try {
            return this.zzk.zzl();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to retrieve VideoController.", (Throwable)ex);
            return null;
        }
    }
    
    public final VideoOptions zzo() {
        return this.zzm;
    }
}
