package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzaht;
import com.google.android.gms.internal.zzaid;
import android.os.RemoteException;

final class zzca extends zzah
{
    final /* synthetic */ zzby zza;
    
    private zzca(final zzby zza) {
        this.zza = zza;
    }
    
    @Override
    public final String zza() throws RemoteException {
        return null;
    }
    
    @Override
    public final void zza(final zzf zzf) throws RemoteException {
        this.zza(zzf, 1);
    }
    
    @Override
    public final void zza(final zzf zzf, final int n) throws RemoteException {
        zzaid.zzc("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzaht.zza.post((Runnable)new zzcb(this));
    }
    
    @Override
    public final String zzb() throws RemoteException {
        return null;
    }
    
    @Override
    public final boolean zzc() throws RemoteException {
        return false;
    }
}
