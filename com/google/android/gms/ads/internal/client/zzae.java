package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzae extends zzez implements zzad
{
    public zzae() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IAdListener");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 7: {
                this.zzf();
                break;
            }
            case 6: {
                this.zze();
                break;
            }
            case 5: {
                this.zzd();
                break;
            }
            case 4: {
                this.zzc();
                break;
            }
            case 3: {
                this.zzb();
                break;
            }
            case 2: {
                this.zza(parcel.readInt());
                break;
            }
            case 1: {
                this.zza();
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}
