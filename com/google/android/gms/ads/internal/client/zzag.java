package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzag extends IInterface
{
    String zza() throws RemoteException;
    
    void zza(final zzf p0) throws RemoteException;
    
    void zza(final zzf p0, final int p1) throws RemoteException;
    
    String zzb() throws RemoteException;
    
    boolean zzc() throws RemoteException;
}
