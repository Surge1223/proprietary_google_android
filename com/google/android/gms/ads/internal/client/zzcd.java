package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.internal.zzwi;
import com.google.android.gms.internal.zzwc;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzach;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;

final class zzcd implements Runnable
{
    private final /* synthetic */ zzcc zza;
    
    zzcd(final zzcc zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        if (this.zza.zza != null) {
            try {
                this.zza.zza.zza(1);
            }
            catch (RemoteException ex) {
                zzaid.zzc("Could not notify onAdFailedToLoad event.", (Throwable)ex);
            }
        }
    }
}
