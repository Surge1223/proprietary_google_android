package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzwi;
import com.google.android.gms.internal.zzwc;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzach;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzao extends IInterface
{
    void zza(final zzaa p0) throws RemoteException;
    
    void zza(final zzad p0) throws RemoteException;
    
    void zza(final zzat p0) throws RemoteException;
    
    void zza(final zzaz p0) throws RemoteException;
    
    void zza(final zzbn p0) throws RemoteException;
    
    void zza(final zzcn p0) throws RemoteException;
    
    void zza(final zzj p0) throws RemoteException;
    
    void zza(final zzach p0) throws RemoteException;
    
    void zza(final zzmg p0) throws RemoteException;
    
    void zza(final zzwc p0) throws RemoteException;
    
    void zza(final zzwi p0, final String p1) throws RemoteException;
    
    void zza(final String p0) throws RemoteException;
    
    void zza(final boolean p0) throws RemoteException;
    
    String zzal() throws RemoteException;
    
    String zzam() throws RemoteException;
    
    void zzan() throws RemoteException;
    
    void zzb() throws RemoteException;
    
    void zzb(final boolean p0) throws RemoteException;
    
    boolean zzb(final zzf p0) throws RemoteException;
    
    IObjectWrapper zzc() throws RemoteException;
    
    zzj zzd() throws RemoteException;
    
    boolean zze() throws RemoteException;
    
    void zzg() throws RemoteException;
    
    void zzh() throws RemoteException;
    
    void zzi() throws RemoteException;
    
    void zzj() throws RemoteException;
    
    boolean zzk() throws RemoteException;
    
    zzbh zzl() throws RemoteException;
    
    String zzv() throws RemoteException;
    
    zzat zzw() throws RemoteException;
    
    zzad zzx() throws RemoteException;
}
