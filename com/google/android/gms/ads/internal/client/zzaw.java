package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzacc;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzob;
import com.google.android.gms.internal.zzwf;
import com.google.android.gms.internal.zzvv;
import android.os.RemoteException;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzaw extends IInterface
{
    zzaj createAdLoaderBuilder(final IObjectWrapper p0, final String p1, final zztk p2, final int p3) throws RemoteException;
    
    zzvv createAdOverlay(final IObjectWrapper p0) throws RemoteException;
    
    zzao createBannerAdManager(final IObjectWrapper p0, final zzj p1, final String p2, final zztk p3, final int p4) throws RemoteException;
    
    zzwf createInAppPurchaseManager(final IObjectWrapper p0) throws RemoteException;
    
    zzao createInterstitialAdManager(final IObjectWrapper p0, final zzj p1, final String p2, final zztk p3, final int p4) throws RemoteException;
    
    zzob createNativeAdViewDelegate(final IObjectWrapper p0, final IObjectWrapper p1) throws RemoteException;
    
    zzog createNativeAdViewHolderDelegate(final IObjectWrapper p0, final IObjectWrapper p1, final IObjectWrapper p2) throws RemoteException;
    
    zzacc createRewardedVideoAd(final IObjectWrapper p0, final zztk p1, final int p2) throws RemoteException;
    
    zzao createSearchAdManager(final IObjectWrapper p0, final zzj p1, final String p2, final int p3) throws RemoteException;
    
    zzbc getMobileAdsSettingsManager(final IObjectWrapper p0) throws RemoteException;
    
    zzbc getMobileAdsSettingsManagerWithClientJarVersion(final IObjectWrapper p0, final int p1) throws RemoteException;
}
