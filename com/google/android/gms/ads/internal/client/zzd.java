package com.google.android.gms.ads.internal.client;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzq;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zztk;
import android.content.Context;
import com.google.android.gms.dynamic.zzp;

public final class zzd extends zzp<zzam>
{
    public zzd() {
        super("com.google.android.gms.ads.AdLoaderBuilderCreatorImpl");
    }
    
    public final zzaj zza(final Context context, final String s, final zztk zztk) {
        try {
            final IBinder zza = this.zzb(context).zza(zzn.zza(context), s, zztk, 12438000);
            if (zza == null) {
                return null;
            }
            final IInterface queryLocalInterface = zza.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            if (queryLocalInterface instanceof zzaj) {
                return (zzaj)queryLocalInterface;
            }
            return new zzal(zza);
        }
        catch (RemoteException | zzq ex) {
            final Throwable t;
            zzaid.zzc("Could not create remote builder for AdLoader.", t);
            return null;
        }
    }
}
