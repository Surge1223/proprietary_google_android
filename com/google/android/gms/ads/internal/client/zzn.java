package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zzob;
import android.widget.FrameLayout;
import com.google.android.gms.internal.zztk;
import android.os.Bundle;
import com.google.android.gms.internal.zzaht;
import android.content.Context;
import com.google.android.gms.internal.zzaid;
import android.os.IBinder;
import com.google.android.gms.internal.zzpu;
import com.google.android.gms.internal.zzvu;
import com.google.android.gms.internal.zzacl;
import com.google.android.gms.internal.zzpt;

public class zzn
{
    private zzaw zza;
    private final Object zzb;
    private final zze zzc;
    private final zzd zzd;
    private final zzbx zze;
    private final zzpt zzf;
    private final zzacl zzg;
    private final zzvu zzh;
    private final zzpu zzi;
    
    public zzn(final zze zzc, final zzd zzd, final zzbx zze, final zzpt zzf, final zzacl zzg, final zzvu zzh, final zzpu zzi) {
        this.zzb = new Object();
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
    }
    
    private static zzaw zza() {
        try {
            final Object instance = zzn.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").newInstance();
            if (!(instance instanceof IBinder)) {
                zzaid.zze("ClientApi class is not an instance of IBinder");
                return null;
            }
            return zzax.asInterface((IBinder)instance);
        }
        catch (Exception ex) {
            zzaid.zzc("Failed to instantiate ClientApi class.", ex);
            return null;
        }
    }
    
    static <T> T zza(final Context context, final boolean b, final zza<T> zza) {
        boolean b2 = b;
        if (!b) {
            zzx.zza();
            b2 = b;
            if (!zzaht.zzc(context)) {
                zzaid.zzb("Google Play Services is not available");
                b2 = true;
            }
        }
        zzx.zza();
        final int zze = zzaht.zze(context);
        zzx.zza();
        if (zze > zzaht.zzd(context)) {
            b2 = true;
        }
        T t;
        if (b2) {
            if ((t = zza.zzb()) == null) {
                t = zza.zzc();
            }
        }
        else if ((t = zza.zzc()) == null) {
            t = zza.zzb();
        }
        return t;
    }
    
    private static void zza(final Context context, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("action", "no_ads_fallback");
        bundle.putString("flow", s);
        zzx.zza().zza(context, null, "gmob-apps", bundle, true);
    }
    
    private final zzaw zzb() {
        synchronized (this.zzb) {
            if (this.zza == null) {
                this.zza = zza();
            }
            return this.zza;
        }
    }
    
    public final zzaj zza(final Context context, final String s, final zztk zztk) {
        return zza(context, false, (zza<zzaj>)new zzr(this, context, s, zztk));
    }
    
    public final zzob zza(final Context context, final FrameLayout frameLayout, final FrameLayout frameLayout2) {
        return zza(context, false, (zza<zzob>)new zzt(this, frameLayout, frameLayout2, context));
    }
    
    abstract class zza<T>
    {
        protected abstract T zza() throws RemoteException;
        
        protected abstract T zza(final zzaw p0) throws RemoteException;
        
        protected final T zzb() {
            final zzaw zza = zzn.this.zzb();
            if (zza == null) {
                zzaid.zze("ClientApi class cannot be loaded.");
                return null;
            }
            try {
                return this.zza(zza);
            }
            catch (RemoteException ex) {
                zzaid.zzc("Cannot invoke local loader using ClientApi class", (Throwable)ex);
                return null;
            }
        }
        
        protected final T zzc() {
            try {
                return this.zza();
            }
            catch (RemoteException ex) {
                zzaid.zzc("Cannot invoke remote loader", (Throwable)ex);
                return null;
            }
        }
    }
}
