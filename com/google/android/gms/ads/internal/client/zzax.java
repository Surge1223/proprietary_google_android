package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zzob;
import com.google.android.gms.internal.zzacc;
import com.google.android.gms.internal.zzwf;
import com.google.android.gms.internal.zzvv;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zztl;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.zzez;

public abstract class zzax extends zzez implements zzaw
{
    public static zzaw asInterface(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IClientApi");
        if (queryLocalInterface instanceof zzaw) {
            return (zzaw)queryLocalInterface;
        }
        return new zzay(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 11: {
                final zzog nativeAdViewHolderDelegate = this.createNativeAdViewHolderDelegate(IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)nativeAdViewHolderDelegate);
                break;
            }
            case 10: {
                final zzao searchAdManager = this.createSearchAdManager(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zzfa.zza(parcel, zzj.CREATOR), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)searchAdManager);
                break;
            }
            case 9: {
                final zzbc mobileAdsSettingsManagerWithClientJarVersion = this.getMobileAdsSettingsManagerWithClientJarVersion(IObjectWrapper.zza.zza(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)mobileAdsSettingsManagerWithClientJarVersion);
                break;
            }
            case 8: {
                final zzvv adOverlay = this.createAdOverlay(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)adOverlay);
                break;
            }
            case 7: {
                final zzwf inAppPurchaseManager = this.createInAppPurchaseManager(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)inAppPurchaseManager);
                break;
            }
            case 6: {
                final zzacc rewardedVideoAd = this.createRewardedVideoAd(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zztl.zza(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)rewardedVideoAd);
                break;
            }
            case 5: {
                final zzob nativeAdViewDelegate = this.createNativeAdViewDelegate(IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)nativeAdViewDelegate);
                break;
            }
            case 4: {
                final zzbc mobileAdsSettingsManager = this.getMobileAdsSettingsManager(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)mobileAdsSettingsManager);
                break;
            }
            case 3: {
                final zzaj adLoaderBuilder = this.createAdLoaderBuilder(IObjectWrapper.zza.zza(parcel.readStrongBinder()), parcel.readString(), zztl.zza(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)adLoaderBuilder);
                break;
            }
            case 2: {
                final zzao interstitialAdManager = this.createInterstitialAdManager(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zzfa.zza(parcel, zzj.CREATOR), parcel.readString(), zztl.zza(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)interstitialAdManager);
                break;
            }
            case 1: {
                final zzao bannerAdManager = this.createBannerAdManager(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zzfa.zza(parcel, zzj.CREATOR), parcel.readString(), zztl.zza(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)bannerAdManager);
                break;
            }
        }
        return true;
    }
}
