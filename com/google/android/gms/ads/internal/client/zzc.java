package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;

public final class zzc extends zzae
{
    private final AdListener zza;
    
    public zzc(final AdListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        this.zza.onAdClosed();
    }
    
    @Override
    public final void zza(final int n) {
        this.zza.onAdFailedToLoad(n);
    }
    
    @Override
    public final void zzb() {
        this.zza.onAdLeftApplication();
    }
    
    @Override
    public final void zzc() {
        this.zza.onAdLoaded();
    }
    
    @Override
    public final void zzd() {
        this.zza.onAdOpened();
    }
    
    @Override
    public final void zze() {
        this.zza.onAdClicked();
    }
    
    @Override
    public final void zzf() {
        this.zza.onAdImpression();
    }
}
