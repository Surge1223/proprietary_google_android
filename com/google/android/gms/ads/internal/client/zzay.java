package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzacd;
import com.google.android.gms.internal.zzacc;
import com.google.android.gms.internal.zzoh;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzoc;
import com.google.android.gms.internal.zzob;
import com.google.android.gms.internal.zzwg;
import com.google.android.gms.internal.zzwf;
import android.os.Parcelable;
import com.google.android.gms.internal.zzvw;
import com.google.android.gms.internal.zzvv;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzay extends zzey implements zzaw
{
    zzay(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IClientApi");
    }
    
    @Override
    public final zzaj createAdLoaderBuilder(final IObjectWrapper objectWrapper, final String s, final zztk zztk, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztk);
        a_.writeInt(n);
        final Parcel zza = this.zza(3, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zzaj zzaj;
        if (strongBinder == null) {
            zzaj = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            if (queryLocalInterface instanceof zzaj) {
                zzaj = (zzaj)queryLocalInterface;
            }
            else {
                zzaj = new zzal(strongBinder);
            }
        }
        zza.recycle();
        return zzaj;
    }
    
    @Override
    public final zzvv createAdOverlay(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        final Parcel zza = this.zza(8, a_);
        final zzvv zza2 = zzvw.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzao createBannerAdManager(final IObjectWrapper objectWrapper, final zzj zzj, final String s, final zztk zztk, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzj);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztk);
        a_.writeInt(n);
        final Parcel zza = this.zza(1, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zzao zzao;
        if (strongBinder == null) {
            zzao = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzao) {
                zzao = (zzao)queryLocalInterface;
            }
            else {
                zzao = new zzaq(strongBinder);
            }
        }
        zza.recycle();
        return zzao;
    }
    
    @Override
    public final zzwf createInAppPurchaseManager(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        final Parcel zza = this.zza(7, a_);
        final zzwf zza2 = zzwg.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzao createInterstitialAdManager(final IObjectWrapper objectWrapper, final zzj zzj, final String s, final zztk zztk, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzj);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztk);
        a_.writeInt(n);
        final Parcel zza = this.zza(2, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zzao zzao;
        if (strongBinder == null) {
            zzao = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzao) {
                zzao = (zzao)queryLocalInterface;
            }
            else {
                zzao = new zzaq(strongBinder);
            }
        }
        zza.recycle();
        return zzao;
    }
    
    @Override
    public final zzob createNativeAdViewDelegate(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)objectWrapper2);
        final Parcel zza = this.zza(5, a_);
        final zzob zza2 = zzoc.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzog createNativeAdViewHolderDelegate(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)objectWrapper2);
        zzfa.zza(a_, (IInterface)objectWrapper3);
        final Parcel zza = this.zza(11, a_);
        final zzog zza2 = zzoh.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzacc createRewardedVideoAd(final IObjectWrapper objectWrapper, final zztk zztk, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)zztk);
        a_.writeInt(n);
        final Parcel zza = this.zza(6, a_);
        final zzacc zza2 = zzacd.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzao createSearchAdManager(final IObjectWrapper objectWrapper, final zzj zzj, final String s, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzj);
        a_.writeString(s);
        a_.writeInt(n);
        final Parcel zza = this.zza(10, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zzao zzao;
        if (strongBinder == null) {
            zzao = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzao) {
                zzao = (zzao)queryLocalInterface;
            }
            else {
                zzao = new zzaq(strongBinder);
            }
        }
        zza.recycle();
        return zzao;
    }
    
    @Override
    public final zzbc getMobileAdsSettingsManager(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        final Parcel zza = this.zza(4, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zzbc zzbc;
        if (strongBinder == null) {
            zzbc = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            if (queryLocalInterface instanceof zzbc) {
                zzbc = (zzbc)queryLocalInterface;
            }
            else {
                zzbc = new zzbe(strongBinder);
            }
        }
        zza.recycle();
        return zzbc;
    }
    
    @Override
    public final zzbc getMobileAdsSettingsManagerWithClientJarVersion(final IObjectWrapper objectWrapper, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeInt(n);
        final Parcel zza = this.zza(9, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zzbc zzbc;
        if (strongBinder == null) {
            zzbc = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            if (queryLocalInterface instanceof zzbc) {
                zzbc = (zzbc)queryLocalInterface;
            }
            else {
                zzbc = new zzbe(strongBinder);
            }
        }
        zza.recycle();
        return zzbc;
    }
}
