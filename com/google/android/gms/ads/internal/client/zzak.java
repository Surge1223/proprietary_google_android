package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zzoy;
import com.google.android.gms.internal.zzpb;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzph;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.internal.zzpk;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.internal.zzpn;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzak extends zzez implements zzaj
{
    public zzak() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        final zzad zzad = null;
        final zzaz zzaz = null;
        switch (n) {
            default: {
                return false;
            }
            case 10: {
                this.zza(zzpn.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 9: {
                this.zza(zzfa.zza(parcel, PublisherAdViewOptions.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 8: {
                this.zza(zzpk.zza(parcel.readStrongBinder()), zzfa.zza(parcel, zzj.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 7: {
                final IBinder strongBinder = parcel.readStrongBinder();
                zzaz zzaz2;
                if (strongBinder == null) {
                    zzaz2 = zzaz;
                }
                else {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    if (queryLocalInterface instanceof zzaz) {
                        zzaz2 = (zzaz)queryLocalInterface;
                    }
                    else {
                        zzaz2 = new zzbb(strongBinder);
                    }
                }
                this.zza(zzaz2);
                parcel2.writeNoException();
                break;
            }
            case 6: {
                this.zza(zzfa.zza(parcel, zznm.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 5: {
                this.zza(parcel.readString(), zzph.zza(parcel.readStrongBinder()), zzpe.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 4: {
                this.zza(zzpb.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 3: {
                this.zza(zzoy.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 2: {
                final IBinder strongBinder2 = parcel.readStrongBinder();
                zzad zzad2;
                if (strongBinder2 == null) {
                    zzad2 = zzad;
                }
                else {
                    final IInterface queryLocalInterface2 = strongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    if (queryLocalInterface2 instanceof zzad) {
                        zzad2 = (zzad)queryLocalInterface2;
                    }
                    else {
                        zzad2 = new zzaf(strongBinder2);
                    }
                }
                this.zza(zzad2);
                parcel2.writeNoException();
                break;
            }
            case 1: {
                final zzag zza = this.zza();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zza);
                break;
            }
        }
        return true;
    }
}
