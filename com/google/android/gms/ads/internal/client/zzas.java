package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzas extends zzey implements zzar
{
    zzas(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdManagerCreator");
    }
    
    @Override
    public final IBinder zza(final IObjectWrapper objectWrapper, final zzj zzj, final String s, final zztk zztk, final int n, final int n2) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzj);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztk);
        a_.writeInt(12438000);
        a_.writeInt(n2);
        final Parcel zza = this.zza(2, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zza.recycle();
        return strongBinder;
    }
}
