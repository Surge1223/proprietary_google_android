package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.doubleclick.AppEventListener;

public final class zzl extends zzau
{
    private final AppEventListener zza;
    
    public zzl(final AppEventListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final String s, final String s2) {
        this.zza.onAppEvent(s, s2);
    }
}
