package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzbh extends IInterface
{
    void zza() throws RemoteException;
    
    void zza(final zzbk p0) throws RemoteException;
    
    void zza(final boolean p0) throws RemoteException;
    
    void zzb() throws RemoteException;
    
    boolean zzc() throws RemoteException;
    
    int zzd() throws RemoteException;
    
    float zze() throws RemoteException;
    
    float zzf() throws RemoteException;
    
    float zzg() throws RemoteException;
    
    boolean zzh() throws RemoteException;
    
    zzbk zzi() throws RemoteException;
    
    boolean zzj() throws RemoteException;
}
