package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zzfa;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzbl extends zzez implements zzbk
{
    public zzbl() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 5: {
                this.zza(zzfa.zza(parcel));
                break;
            }
            case 4: {
                this.zzd();
                break;
            }
            case 3: {
                this.zzc();
                break;
            }
            case 2: {
                this.zzb();
                break;
            }
            case 1: {
                this.zza();
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}
