package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zzfa;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzah extends zzez implements zzag
{
    public zzah() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IAdLoader");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 5: {
                this.zza(zzfa.zza(parcel, zzf.CREATOR), parcel.readInt());
                parcel2.writeNoException();
                break;
            }
            case 4: {
                final String zzb = this.zzb();
                parcel2.writeNoException();
                parcel2.writeString(zzb);
                break;
            }
            case 3: {
                final boolean zzc = this.zzc();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzc);
                break;
            }
            case 2: {
                final String zza = this.zza();
                parcel2.writeNoException();
                parcel2.writeString(zza);
                break;
            }
            case 1: {
                this.zza(zzfa.zza(parcel, zzf.CREATOR));
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
