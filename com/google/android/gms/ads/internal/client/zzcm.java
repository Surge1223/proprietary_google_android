package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.VideoController;

public final class zzcm extends zzbl
{
    private final VideoController.VideoLifecycleCallbacks zza;
    
    public zzcm(final VideoController.VideoLifecycleCallbacks zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        this.zza.onVideoStart();
    }
    
    @Override
    public final void zza(final boolean b) {
        this.zza.onVideoMute(b);
    }
    
    @Override
    public final void zzb() {
        this.zza.onVideoPlay();
    }
    
    @Override
    public final void zzc() {
        this.zza.onVideoPause();
    }
    
    @Override
    public final void zzd() {
        this.zza.onVideoEnd();
    }
}
