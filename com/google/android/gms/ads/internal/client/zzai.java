package com.google.android.gms.ads.internal.client;

import android.os.Parcelable;
import com.google.android.gms.internal.zzfa;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzai extends zzey implements zzag
{
    zzai(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoader");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zza(final zzf zzf) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzf);
        this.zzb(1, a_);
    }
    
    @Override
    public final void zza(final zzf zzf, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeInt(n);
        this.zzb(5, a_);
    }
    
    @Override
    public final String zzb() throws RemoteException {
        final Parcel zza = this.zza(4, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final boolean zzc() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
}
