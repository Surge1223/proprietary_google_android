package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;

public final class zzcl extends zzj
{
    @Override
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, 6, this.zze);
        zzbig.zza(parcel, zza);
    }
}
