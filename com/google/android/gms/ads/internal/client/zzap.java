package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcelable;
import com.google.android.gms.internal.zzwd;
import com.google.android.gms.internal.zzwj;
import com.google.android.gms.internal.zzmh;
import com.google.android.gms.internal.zzaci;
import com.google.android.gms.internal.zzfa;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzap extends zzez implements zzao
{
    public zzap() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IAdManager");
    }
    
    public static zzao zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        if (queryLocalInterface instanceof zzao) {
            return (zzao)queryLocalInterface;
        }
        return new zzaq(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        final zzaa zzaa = null;
        final zzat zzat = null;
        final zzad zzad = null;
        final zzaz zzaz = null;
        switch (n) {
            default: {
                return false;
            }
            case 35: {
                final String zzam = this.zzam();
                parcel2.writeNoException();
                parcel2.writeString(zzam);
                break;
            }
            case 34: {
                this.zzb(zzfa.zza(parcel));
                parcel2.writeNoException();
                break;
            }
            case 33: {
                final zzad zzx = this.zzx();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzx);
                break;
            }
            case 32: {
                final zzat zzw = this.zzw();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzw);
                break;
            }
            case 31: {
                final String zzv = this.zzv();
                parcel2.writeNoException();
                parcel2.writeString(zzv);
                break;
            }
            case 30: {
                this.zza(zzfa.zza(parcel, zzbn.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 29: {
                this.zza(zzfa.zza(parcel, zzcn.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 26: {
                final zzbh zzl = this.zzl();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzl);
                break;
            }
            case 25: {
                this.zza(parcel.readString());
                parcel2.writeNoException();
                break;
            }
            case 24: {
                this.zza(zzaci.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 23: {
                final boolean zzk = this.zzk();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzk);
                break;
            }
            case 22: {
                this.zza(zzfa.zza(parcel));
                parcel2.writeNoException();
                break;
            }
            case 21: {
                final IBinder strongBinder = parcel.readStrongBinder();
                zzaz zzaz2;
                if (strongBinder == null) {
                    zzaz2 = zzaz;
                }
                else {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    if (queryLocalInterface instanceof zzaz) {
                        zzaz2 = (zzaz)queryLocalInterface;
                    }
                    else {
                        zzaz2 = new zzbb(strongBinder);
                    }
                }
                this.zza(zzaz2);
                parcel2.writeNoException();
                break;
            }
            case 20: {
                final IBinder strongBinder2 = parcel.readStrongBinder();
                zzaa zzaa2;
                if (strongBinder2 == null) {
                    zzaa2 = zzaa;
                }
                else {
                    final IInterface queryLocalInterface2 = strongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdClickListener");
                    if (queryLocalInterface2 instanceof zzaa) {
                        zzaa2 = (zzaa)queryLocalInterface2;
                    }
                    else {
                        zzaa2 = new zzac(strongBinder2);
                    }
                }
                this.zza(zzaa2);
                parcel2.writeNoException();
                break;
            }
            case 19: {
                this.zza(zzmh.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 18: {
                final String zzal = this.zzal();
                parcel2.writeNoException();
                parcel2.writeString(zzal);
                break;
            }
            case 15: {
                this.zza(zzwj.zza(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            }
            case 14: {
                this.zza(zzwd.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 13: {
                this.zza(zzfa.zza(parcel, zzj.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 12: {
                final zzj zzd = this.zzd();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzd);
                break;
            }
            case 11: {
                this.zzg();
                parcel2.writeNoException();
                break;
            }
            case 10: {
                this.zzj();
                parcel2.writeNoException();
                break;
            }
            case 9: {
                this.zzan();
                parcel2.writeNoException();
                break;
            }
            case 8: {
                final IBinder strongBinder3 = parcel.readStrongBinder();
                zzat zzat2;
                if (strongBinder3 == null) {
                    zzat2 = zzat;
                }
                else {
                    final IInterface queryLocalInterface3 = strongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
                    if (queryLocalInterface3 instanceof zzat) {
                        zzat2 = (zzat)queryLocalInterface3;
                    }
                    else {
                        zzat2 = new zzav(strongBinder3);
                    }
                }
                this.zza(zzat2);
                parcel2.writeNoException();
                break;
            }
            case 7: {
                final IBinder strongBinder4 = parcel.readStrongBinder();
                zzad zzad2;
                if (strongBinder4 == null) {
                    zzad2 = zzad;
                }
                else {
                    final IInterface queryLocalInterface4 = strongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    if (queryLocalInterface4 instanceof zzad) {
                        zzad2 = (zzad)queryLocalInterface4;
                    }
                    else {
                        zzad2 = new zzaf(strongBinder4);
                    }
                }
                this.zza(zzad2);
                parcel2.writeNoException();
                break;
            }
            case 6: {
                this.zzi();
                parcel2.writeNoException();
                break;
            }
            case 5: {
                this.zzh();
                parcel2.writeNoException();
                break;
            }
            case 4: {
                final boolean zzb = this.zzb(zzfa.zza(parcel, zzf.CREATOR));
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzb);
                break;
            }
            case 3: {
                final boolean zze = this.zze();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zze);
                break;
            }
            case 2: {
                this.zzb();
                parcel2.writeNoException();
                break;
            }
            case 1: {
                final IObjectWrapper zzc = this.zzc();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzc);
                break;
            }
        }
        return true;
    }
}
