package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzar extends IInterface
{
    IBinder zza(final IObjectWrapper p0, final zzj p1, final String p2, final zztk p3, final int p4, final int p5) throws RemoteException;
}
