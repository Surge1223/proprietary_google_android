package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.content.Context;
import android.widget.FrameLayout;
import com.google.android.gms.internal.zzob;

final class zzt extends zza<zzob>
{
    private final /* synthetic */ FrameLayout zza;
    private final /* synthetic */ FrameLayout zzb;
    private final /* synthetic */ Context zzc;
    private final /* synthetic */ zzn zzd;
    
    zzt(final zzn zzd, final FrameLayout zza, final FrameLayout zzb, final Context zzc) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        zzd.super();
    }
}
