package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpa;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzaj extends IInterface
{
    zzag zza() throws RemoteException;
    
    void zza(final PublisherAdViewOptions p0) throws RemoteException;
    
    void zza(final zzad p0) throws RemoteException;
    
    void zza(final zzaz p0) throws RemoteException;
    
    void zza(final zznm p0) throws RemoteException;
    
    void zza(final zzox p0) throws RemoteException;
    
    void zza(final zzpa p0) throws RemoteException;
    
    void zza(final zzpj p0, final zzj p1) throws RemoteException;
    
    void zza(final zzpm p0) throws RemoteException;
    
    void zza(final String p0, final zzpg p1, final zzpd p2) throws RemoteException;
}
