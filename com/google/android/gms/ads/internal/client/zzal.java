package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpa;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zznm;
import android.os.Parcelable;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import android.os.RemoteException;
import android.os.IInterface;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzal extends zzey implements zzaj
{
    zzal(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }
    
    @Override
    public final zzag zza() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zzag zzag;
        if (strongBinder == null) {
            zzag = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoader");
            if (queryLocalInterface instanceof zzag) {
                zzag = (zzag)queryLocalInterface;
            }
            else {
                zzag = new zzai(strongBinder);
            }
        }
        zza.recycle();
        return zzag;
    }
    
    @Override
    public final void zza(final PublisherAdViewOptions publisherAdViewOptions) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)publisherAdViewOptions);
        this.zzb(9, a_);
    }
    
    @Override
    public final void zza(final zzad zzad) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzad);
        this.zzb(2, a_);
    }
    
    @Override
    public final void zza(final zzaz zzaz) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzaz);
        this.zzb(7, a_);
    }
    
    @Override
    public final void zza(final zznm zznm) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zznm);
        this.zzb(6, a_);
    }
    
    @Override
    public final void zza(final zzox zzox) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzox);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zza(final zzpa zzpa) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzpa);
        this.zzb(4, a_);
    }
    
    @Override
    public final void zza(final zzpj zzpj, final zzj zzj) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzpj);
        zzfa.zza(a_, (Parcelable)zzj);
        this.zzb(8, a_);
    }
    
    @Override
    public final void zza(final zzpm zzpm) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzpm);
        this.zzb(10, a_);
    }
    
    @Override
    public final void zza(final String s, final zzpg zzpg, final zzpd zzpd) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zzpg);
        zzfa.zza(a_, (IInterface)zzpd);
        this.zzb(5, a_);
    }
}
