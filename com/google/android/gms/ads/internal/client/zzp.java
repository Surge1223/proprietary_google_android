package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zztk;
import android.content.Context;

final class zzp extends zza<zzao>
{
    private final /* synthetic */ Context zza;
    private final /* synthetic */ zzj zzb;
    private final /* synthetic */ String zzc;
    private final /* synthetic */ zzn zzd;
    
    zzp(final zzn zzd, final Context zza, final zzj zzb, final String zzc) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        zzd.super();
    }
}
