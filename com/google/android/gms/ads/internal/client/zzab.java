package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzab extends zzez implements zzaa
{
    public zzab() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IAdClickListener");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            this.zza();
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}
