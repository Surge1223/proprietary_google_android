package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzwi;
import com.google.android.gms.internal.zzwc;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzach;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzaq extends zzey implements zzao
{
    zzaq(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdManager");
    }
    
    @Override
    public final void zza(final zzaa zzaa) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzaa);
        this.zzb(20, a_);
    }
    
    @Override
    public final void zza(final zzad zzad) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzad);
        this.zzb(7, a_);
    }
    
    @Override
    public final void zza(final zzat zzat) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzat);
        this.zzb(8, a_);
    }
    
    @Override
    public final void zza(final zzaz zzaz) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzaz);
        this.zzb(21, a_);
    }
    
    @Override
    public final void zza(final zzbn zzbn) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzbn);
        this.zzb(30, a_);
    }
    
    @Override
    public final void zza(final zzcn zzcn) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzcn);
        this.zzb(29, a_);
    }
    
    @Override
    public final void zza(final zzj zzj) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzj);
        this.zzb(13, a_);
    }
    
    @Override
    public final void zza(final zzach zzach) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzach);
        this.zzb(24, a_);
    }
    
    @Override
    public final void zza(final zzmg zzmg) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzmg);
        this.zzb(19, a_);
    }
    
    @Override
    public final void zza(final zzwc zzwc) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzwc);
        this.zzb(14, a_);
    }
    
    @Override
    public final void zza(final zzwi zzwi, final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzwi);
        a_.writeString(s);
        this.zzb(15, a_);
    }
    
    @Override
    public final void zza(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        this.zzb(25, a_);
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, b);
        this.zzb(22, a_);
    }
    
    @Override
    public final String zzal() throws RemoteException {
        final Parcel zza = this.zza(18, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final String zzam() throws RemoteException {
        final Parcel zza = this.zza(35, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zzan() throws RemoteException {
        this.zzb(9, this.a_());
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zzb(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, b);
        this.zzb(34, a_);
    }
    
    @Override
    public final boolean zzb(final zzf zzf) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzf);
        final Parcel zza = this.zza(4, a_);
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzc() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzj zzd() throws RemoteException {
        final Parcel zza = this.zza(12, this.a_());
        final zzj zzj = zzfa.zza(zza, com.google.android.gms.ads.internal.client.zzj.CREATOR);
        zza.recycle();
        return zzj;
    }
    
    @Override
    public final boolean zze() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zzg() throws RemoteException {
        this.zzb(11, this.a_());
    }
    
    @Override
    public final void zzh() throws RemoteException {
        this.zzb(5, this.a_());
    }
    
    @Override
    public final void zzi() throws RemoteException {
        this.zzb(6, this.a_());
    }
    
    @Override
    public final void zzj() throws RemoteException {
        this.zzb(10, this.a_());
    }
    
    @Override
    public final boolean zzk() throws RemoteException {
        final Parcel zza = this.zza(23, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzbh zzl() throws RemoteException {
        final Parcel zza = this.zza(26, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zzbh zzbh;
        if (strongBinder == null) {
            zzbh = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
            if (queryLocalInterface instanceof zzbh) {
                zzbh = (zzbh)queryLocalInterface;
            }
            else {
                zzbh = new zzbj(strongBinder);
            }
        }
        zza.recycle();
        return zzbh;
    }
    
    @Override
    public final String zzv() throws RemoteException {
        final Parcel zza = this.zza(31, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final zzat zzw() throws RemoteException {
        final Parcel zza = this.zza(32, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zzat zzat;
        if (strongBinder == null) {
            zzat = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
            if (queryLocalInterface instanceof zzat) {
                zzat = (zzat)queryLocalInterface;
            }
            else {
                zzat = new zzav(strongBinder);
            }
        }
        zza.recycle();
        return zzat;
    }
    
    @Override
    public final zzad zzx() throws RemoteException {
        final Parcel zza = this.zza(33, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zzad zzad;
        if (strongBinder == null) {
            zzad = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
            if (queryLocalInterface instanceof zzad) {
                zzad = (zzad)queryLocalInterface;
            }
            else {
                zzad = new zzaf(strongBinder);
            }
        }
        zza.recycle();
        return zzad;
    }
}
