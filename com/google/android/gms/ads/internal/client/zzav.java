package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzav extends zzey implements zzat
{
    zzav(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAppEventListener");
    }
    
    @Override
    public final void zza(final String s, final String s2) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        a_.writeString(s2);
        this.zzb(1, a_);
    }
}
