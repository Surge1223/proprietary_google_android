package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.search.SearchAdRequest;
import android.os.Bundle;
import android.location.Location;
import java.util.Set;
import java.util.Date;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import android.content.Context;

public final class zzi
{
    public static final zzi zza;
    
    static {
        zza = new zzi();
    }
    
    public static zzf zza(Context applicationContext, final zzbp zzbp) {
        final Date zza = zzbp.zza();
        long time;
        if (zza != null) {
            time = zza.getTime();
        }
        else {
            time = -1L;
        }
        final String zzb = zzbp.zzb();
        final int zzc = zzbp.zzc();
        final Set<String> zzd = zzbp.zzd();
        Object unmodifiableList;
        if (!zzd.isEmpty()) {
            unmodifiableList = Collections.unmodifiableList((List<?>)new ArrayList<Object>(zzd));
        }
        else {
            unmodifiableList = null;
        }
        final boolean zza2 = zzbp.zza(applicationContext);
        final int zzl = zzbp.zzl();
        final Location zze = zzbp.zze();
        final Bundle zzb2 = zzbp.zzb(AdMobAdapter.class);
        final boolean zzf = zzbp.zzf();
        final String zzg = zzbp.zzg();
        final SearchAdRequest zzi = zzbp.zzi();
        zzcj zzcj;
        if (zzi != null) {
            zzcj = new zzcj(zzi);
        }
        else {
            zzcj = null;
        }
        applicationContext = applicationContext.getApplicationContext();
        String zza3;
        if (applicationContext != null) {
            final String packageName = applicationContext.getPackageName();
            zzx.zza();
            zza3 = zzaht.zza(Thread.currentThread().getStackTrace(), packageName);
        }
        else {
            zza3 = null;
        }
        return new zzf(7, time, zzb2, zzc, (List<String>)unmodifiableList, zza2, zzl, zzf, zzg, zzcj, zze, zzb, zzbp.zzk(), zzbp.zzm(), Collections.unmodifiableList((List<? extends String>)new ArrayList<String>(zzbp.zzn())), zzbp.zzh(), zza3, zzbp.zzo());
    }
}
