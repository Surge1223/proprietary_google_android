package com.google.android.gms.ads.internal.client;

import java.util.HashMap;
import java.util.HashSet;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.internal.zzaht;
import android.content.Context;
import java.util.Collections;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.Map;
import android.os.Bundle;
import android.location.Location;
import java.util.Set;
import java.util.Date;

public final class zzbp
{
    private final Date zza;
    private final String zzb;
    private final int zzc;
    private final Set<String> zzd;
    private final Location zze;
    private final boolean zzf;
    private final Bundle zzg;
    private final Map<Class<?>, Object> zzh;
    private final String zzi;
    private final String zzj;
    private final SearchAdRequest zzk;
    private final int zzl;
    private final Set<String> zzm;
    private final Bundle zzn;
    private final Set<String> zzo;
    private final boolean zzp;
    
    public zzbp(final zzbq zzbq) {
        this(zzbq, null);
    }
    
    public zzbp(final zzbq zzbq, final SearchAdRequest zzk) {
        this.zza = zzbq.zzg;
        this.zzb = zzbq.zzh;
        this.zzc = zzbq.zzi;
        this.zzd = Collections.unmodifiableSet((Set<? extends String>)zzbq.zza);
        this.zze = zzbq.zzj;
        this.zzf = zzbq.zzk;
        this.zzg = zzbq.zzb;
        this.zzh = Collections.unmodifiableMap((Map<? extends Class<?>, ?>)zzbq.zzc);
        this.zzi = zzbq.zzl;
        this.zzj = zzbq.zzm;
        this.zzk = zzk;
        this.zzl = zzbq.zzn;
        this.zzm = Collections.unmodifiableSet((Set<? extends String>)zzbq.zzd);
        this.zzn = zzbq.zze;
        this.zzo = Collections.unmodifiableSet((Set<? extends String>)zzbq.zzf);
        this.zzp = zzbq.zzo;
    }
    
    public final Date zza() {
        return this.zza;
    }
    
    public final boolean zza(final Context context) {
        final Set<String> zzm = this.zzm;
        zzx.zza();
        return zzm.contains(zzaht.zza(context));
    }
    
    public final Bundle zzb(final Class<? extends MediationAdapter> clazz) {
        return this.zzg.getBundle(clazz.getName());
    }
    
    public final String zzb() {
        return this.zzb;
    }
    
    public final int zzc() {
        return this.zzc;
    }
    
    public final Set<String> zzd() {
        return this.zzd;
    }
    
    public final Location zze() {
        return this.zze;
    }
    
    public final boolean zzf() {
        return this.zzf;
    }
    
    public final String zzg() {
        return this.zzi;
    }
    
    public final String zzh() {
        return this.zzj;
    }
    
    public final SearchAdRequest zzi() {
        return this.zzk;
    }
    
    public final Map<Class<?>, Object> zzj() {
        return this.zzh;
    }
    
    public final Bundle zzk() {
        return this.zzg;
    }
    
    public final int zzl() {
        return this.zzl;
    }
    
    public final Bundle zzm() {
        return this.zzn;
    }
    
    public final Set<String> zzn() {
        return this.zzo;
    }
    
    public final boolean zzo() {
        return this.zzp;
    }
}
