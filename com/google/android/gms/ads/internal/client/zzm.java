package com.google.android.gms.ads.internal.client;

import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.gms.ads.AdSize;

public final class zzm
{
    private final AdSize[] zza;
    private final String zzb;
    
    public zzm(final Context context, final AttributeSet set) {
        final TypedArray obtainAttributes = context.getResources().obtainAttributes(set, R.styleable.AdsAttrs);
        final String string = obtainAttributes.getString(R.styleable.AdsAttrs_adSize);
        final String string2 = obtainAttributes.getString(R.styleable.AdsAttrs_adSizes);
        final boolean b = TextUtils.isEmpty((CharSequence)string) ^ true;
        final boolean b2 = TextUtils.isEmpty((CharSequence)string2) ^ true;
        if (b && !b2) {
            this.zza = zza(string);
        }
        else if (!b && b2) {
            this.zza = zza(string2);
        }
        else {
            if (b) {
                throw new IllegalArgumentException("Either XML attribute \"adSize\" or XML attribute \"supportedAdSizes\" should be specified, but not both.");
            }
            throw new IllegalArgumentException("Required XML attribute \"adSize\" was missing.");
        }
        this.zzb = obtainAttributes.getString(R.styleable.AdsAttrs_adUnitId);
        if (!TextUtils.isEmpty((CharSequence)this.zzb)) {
            return;
        }
        throw new IllegalArgumentException("Required XML attribute \"adUnitId\" was missing.");
    }
    
    private static AdSize[] zza(String s) {
        final String[] split = s.split("\\s*,\\s*");
        final AdSize[] array = new AdSize[split.length];
        for (int i = 0; i < split.length; ++i) {
            final String trim = split[i].trim();
            if (trim.matches("^(\\d+|FULL_WIDTH)\\s*[xX]\\s*(\\d+|AUTO_HEIGHT)$")) {
                final String[] split2 = trim.split("[xX]");
                split2[0] = split2[0].trim();
                split2[1] = split2[1].trim();
                try {
                    int int1;
                    if ("FULL_WIDTH".equals(split2[0])) {
                        int1 = -1;
                    }
                    else {
                        int1 = Integer.parseInt(split2[0]);
                    }
                    int int2;
                    if ("AUTO_HEIGHT".equals(split2[1])) {
                        int2 = -2;
                    }
                    else {
                        int2 = Integer.parseInt(split2[1]);
                    }
                    array[i] = new AdSize(int1, int2);
                    continue;
                }
                catch (NumberFormatException ex) {
                    s = String.valueOf(trim);
                    if (s.length() != 0) {
                        s = "Could not parse XML attribute \"adSize\": ".concat(s);
                    }
                    else {
                        s = new String("Could not parse XML attribute \"adSize\": ");
                    }
                    throw new IllegalArgumentException(s);
                }
            }
            if ("BANNER".equals(trim)) {
                array[i] = AdSize.BANNER;
            }
            else if ("LARGE_BANNER".equals(trim)) {
                array[i] = AdSize.LARGE_BANNER;
            }
            else if ("FULL_BANNER".equals(trim)) {
                array[i] = AdSize.FULL_BANNER;
            }
            else if ("LEADERBOARD".equals(trim)) {
                array[i] = AdSize.LEADERBOARD;
            }
            else if ("MEDIUM_RECTANGLE".equals(trim)) {
                array[i] = AdSize.MEDIUM_RECTANGLE;
            }
            else if ("SMART_BANNER".equals(trim)) {
                array[i] = AdSize.SMART_BANNER;
            }
            else if ("WIDE_SKYSCRAPER".equals(trim)) {
                array[i] = AdSize.WIDE_SKYSCRAPER;
            }
            else if ("FLUID".equals(trim)) {
                array[i] = AdSize.FLUID;
            }
            else {
                if (!"ICON".equals(trim)) {
                    s = String.valueOf(trim);
                    if (s.length() != 0) {
                        s = "Could not parse XML attribute \"adSize\": ".concat(s);
                    }
                    else {
                        s = new String("Could not parse XML attribute \"adSize\": ");
                    }
                    throw new IllegalArgumentException(s);
                }
                array[i] = AdSize.zza;
            }
        }
        if (array.length == 0) {
            s = String.valueOf(s);
            if (s.length() != 0) {
                s = "Could not parse XML attribute \"adSize\": ".concat(s);
            }
            else {
                s = new String("Could not parse XML attribute \"adSize\": ");
            }
            throw new IllegalArgumentException(s);
        }
        return array;
    }
    
    public final String zza() {
        return this.zzb;
    }
    
    public final AdSize[] zza(final boolean b) {
        if (!b && this.zza.length != 1) {
            throw new IllegalArgumentException("The adSizes XML attribute is only allowed on PublisherAdViews.");
        }
        return this.zza;
    }
}
