package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.mediation.MediationAdapter;
import android.location.Location;
import java.util.Date;
import java.util.HashMap;
import android.os.Bundle;
import java.util.HashSet;

public final class zzbq
{
    private final HashSet<String> zza;
    private final Bundle zzb;
    private final HashMap<Class<?>, Object> zzc;
    private final HashSet<String> zzd;
    private final Bundle zze;
    private final HashSet<String> zzf;
    private Date zzg;
    private String zzh;
    private int zzi;
    private Location zzj;
    private boolean zzk;
    private String zzl;
    private String zzm;
    private int zzn;
    private boolean zzo;
    
    public zzbq() {
        this.zza = new HashSet<String>();
        this.zzb = new Bundle();
        this.zzc = new HashMap<Class<?>, Object>();
        this.zzd = new HashSet<String>();
        this.zze = new Bundle();
        this.zzf = new HashSet<String>();
        this.zzi = -1;
        this.zzk = false;
        this.zzn = -1;
    }
    
    public final void zza(final int zzi) {
        this.zzi = zzi;
    }
    
    public final void zza(final Location zzj) {
        this.zzj = zzj;
    }
    
    public final void zza(final Class<? extends MediationAdapter> clazz, final Bundle bundle) {
        this.zzb.putBundle(clazz.getName(), bundle);
    }
    
    public final void zza(final String s) {
        this.zza.add(s);
    }
    
    public final void zza(final Date zzg) {
        this.zzg = zzg;
    }
    
    public final void zzb(final String s) {
        this.zzd.add(s);
    }
    
    public final void zzb(final boolean zzn) {
        this.zzn = (zzn ? 1 : 0);
    }
    
    public final void zzc(final String s) {
        this.zzd.remove(s);
    }
    
    public final void zzc(final boolean zzo) {
        this.zzo = zzo;
    }
}
