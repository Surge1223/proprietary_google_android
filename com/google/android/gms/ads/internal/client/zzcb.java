package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpa;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;

final class zzcb implements Runnable
{
    private final /* synthetic */ zzca zza;
    
    zzcb(final zzca zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        if (this.zza.zza.zza != null) {
            try {
                this.zza.zza.zza.zza(1);
            }
            catch (RemoteException ex) {
                zzaid.zzc("Could not notify onAdFailedToLoad event.", (Throwable)ex);
            }
        }
    }
}
