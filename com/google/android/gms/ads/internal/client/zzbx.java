package com.google.android.gms.ads.internal.client;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzp;

public final class zzbx extends zzp<zzbf>
{
    public zzbx() {
        super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
    }
}
