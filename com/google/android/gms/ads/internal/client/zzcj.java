package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.ads.search.SearchAdRequest;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzcj extends zzbid
{
    public static final Parcelable.Creator<zzcj> CREATOR;
    public final String zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzck();
    }
    
    public zzcj(final SearchAdRequest searchAdRequest) {
        this.zza = searchAdRequest.getQuery();
    }
    
    zzcj(final String zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 15, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
