package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import com.google.android.gms.internal.zzfa;
import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzbm extends zzey implements zzbk
{
    zzbm(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(1, this.a_());
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, b);
        this.zzb(5, a_);
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zzc() throws RemoteException {
        this.zzb(3, this.a_());
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(4, this.a_());
    }
}
