package com.google.android.gms.ads.internal.client;

import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzbe extends zzey implements zzbc
{
    zzbe(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }
}
