package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzan extends zzey implements zzam
{
    zzan(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilderCreator");
    }
    
    @Override
    public final IBinder zza(final IObjectWrapper objectWrapper, final String s, final zztk zztk, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztk);
        a_.writeInt(12438000);
        final Parcel zza = this.zza(1, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zza.recycle();
        return strongBinder;
    }
}
