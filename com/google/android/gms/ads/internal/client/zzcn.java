package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.ads.VideoOptions;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzcn extends zzbid
{
    public static final Parcelable.Creator<zzcn> CREATOR;
    public final boolean zza;
    public final boolean zzb;
    public final boolean zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzco();
    }
    
    public zzcn(final VideoOptions videoOptions) {
        this(videoOptions.getStartMuted(), videoOptions.getCustomControlsRequested(), videoOptions.getClickToExpandRequested());
    }
    
    public zzcn(final boolean zza, final boolean zzb, final boolean zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, 4, this.zzc);
        zzbig.zza(parcel, zza);
    }
}
