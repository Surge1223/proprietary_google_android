package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzbn extends zzbid
{
    public static final Parcelable.Creator<zzbn> CREATOR;
    public final int zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzbo();
    }
    
    public zzbn(final int zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza);
        zzbig.zza(parcel, zza);
    }
}
