package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpa;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import android.os.RemoteException;

public final class zzby extends zzak
{
    private zzad zza;
    
    @Override
    public final zzag zza() throws RemoteException {
        return new zzca(this, null);
    }
    
    @Override
    public final void zza(final PublisherAdViewOptions publisherAdViewOptions) throws RemoteException {
    }
    
    @Override
    public final void zza(final zzad zza) throws RemoteException {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzaz zzaz) throws RemoteException {
    }
    
    @Override
    public final void zza(final zznm zznm) throws RemoteException {
    }
    
    @Override
    public final void zza(final zzox zzox) throws RemoteException {
    }
    
    @Override
    public final void zza(final zzpa zzpa) throws RemoteException {
    }
    
    @Override
    public final void zza(final zzpj zzpj, final zzj zzj) throws RemoteException {
    }
    
    @Override
    public final void zza(final zzpm zzpm) throws RemoteException {
    }
    
    @Override
    public final void zza(final String s, final zzpg zzpg, final zzpd zzpd) throws RemoteException {
    }
}
