package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zztk;
import android.content.Context;

final class zzo extends zza<zzao>
{
    private final /* synthetic */ Context zza;
    private final /* synthetic */ zzj zzb;
    private final /* synthetic */ String zzc;
    private final /* synthetic */ zztk zzd;
    private final /* synthetic */ zzn zze;
    
    zzo(final zzn zze, final Context zza, final zzj zzb, final String zzc, final zztk zzd) {
        this.zze = zze;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        zze.super();
    }
}
