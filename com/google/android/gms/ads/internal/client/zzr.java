package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zztk;
import android.content.Context;

final class zzr extends zza<zzaj>
{
    private final /* synthetic */ Context zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ zztk zzc;
    private final /* synthetic */ zzn zzd;
    
    zzr(final zzn zzd, final Context zza, final String zzb, final zztk zzc) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        zzd.super();
    }
}
