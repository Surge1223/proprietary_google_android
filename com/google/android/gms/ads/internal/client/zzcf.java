package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzoc;

public final class zzcf extends zzoc
{
    @Override
    public final IObjectWrapper zza(final String s) throws RemoteException {
        return null;
    }
    
    @Override
    public final void zza() throws RemoteException {
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final int n) {
    }
    
    @Override
    public final void zza(final String s, final IObjectWrapper objectWrapper) throws RemoteException {
    }
}
