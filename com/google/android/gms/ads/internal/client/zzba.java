package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import com.google.android.gms.internal.zzez;

public abstract class zzba extends zzez implements zzaz
{
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            final long zzb = this.zzb();
            parcel2.writeNoException();
            parcel2.writeLong(zzb);
            return true;
        }
        return false;
    }
}
