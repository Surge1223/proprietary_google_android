package com.google.android.gms.ads.internal.client;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzq;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zztk;
import android.content.Context;
import com.google.android.gms.dynamic.zzp;

public final class zze extends zzp<zzar>
{
    public zze() {
        super("com.google.android.gms.ads.AdManagerCreatorImpl");
    }
    
    public final zzao zza(final Context context, final zzj zzj, final String s, final zztk zztk, final int n) {
        try {
            final IBinder zza = this.zzb(context).zza(zzn.zza(context), zzj, s, zztk, 12438000, n);
            if (zza == null) {
                return null;
            }
            final IInterface queryLocalInterface = zza.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzao) {
                return (zzao)queryLocalInterface;
            }
            return new zzaq(zza);
        }
        catch (RemoteException | zzq ex) {
            final Throwable t;
            zzaid.zza("Could not create remote AdManager.", t);
            return null;
        }
    }
}
