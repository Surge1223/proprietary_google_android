package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzaf extends zzey implements zzad
{
    zzaf(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdListener");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(1, this.a_());
    }
    
    @Override
    public final void zza(final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeInt(n);
        this.zzb(2, a_);
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(3, this.a_());
    }
    
    @Override
    public final void zzc() throws RemoteException {
        this.zzb(4, this.a_());
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(5, this.a_());
    }
    
    @Override
    public final void zze() throws RemoteException {
        this.zzb(6, this.a_());
    }
    
    @Override
    public final void zzf() throws RemoteException {
        this.zzb(7, this.a_());
    }
}
