package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.view.View;
import com.google.android.gms.internal.zztk;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzmj;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzaht;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.VideoController;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.internal.zztj;

final class zzbs extends zzz
{
    private final /* synthetic */ zzbr zza;
    
    zzbs(final zzbr zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onAdFailedToLoad(final int n) {
        this.zza.zzd.zza(this.zza.zzn());
        super.onAdFailedToLoad(n);
    }
    
    @Override
    public final void onAdLoaded() {
        this.zza.zzd.zza(this.zza.zzn());
        super.onAdLoaded();
    }
}
