package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzach;
import com.google.android.gms.internal.zzacm;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzmj;
import com.google.android.gms.internal.zztk;
import android.os.RemoteException;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.AdListener;
import android.content.Context;
import com.google.android.gms.internal.zztj;

public final class zzbt
{
    private final zztj zza;
    private final Context zzb;
    private final zzi zzc;
    private AdListener zzd;
    private zza zze;
    private zzao zzf;
    private String zzg;
    private AppEventListener zzh;
    private PublisherInterstitialAd zzi;
    private OnCustomRenderedAdLoadedListener zzj;
    private Correlator zzk;
    private RewardedVideoAdListener zzl;
    private boolean zzm;
    private boolean zzn;
    
    public zzbt(final Context context) {
        this(context, com.google.android.gms.ads.internal.client.zzi.zza, null);
    }
    
    private zzbt(final Context zzb, final zzi zzc, final PublisherInterstitialAd zzi) {
        this.zza = new zztj();
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzi = zzi;
    }
    
    private final void zzb(final String s) {
        if (this.zzf != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder(63 + String.valueOf(s).length());
        sb.append("The ad unit ID must be set on InterstitialAd before ");
        sb.append(s);
        sb.append(" is called.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final void zza(final AdListener zzd) {
        try {
            this.zzd = zzd;
            if (this.zzf != null) {
                final zzao zzf = this.zzf;
                zzad zzad;
                if (zzd != null) {
                    zzad = new zzc(zzd);
                }
                else {
                    zzad = null;
                }
                zzf.zza(zzad);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the AdListener.", (Throwable)ex);
        }
    }
    
    public final void zza(final zza zze) {
        try {
            this.zze = zze;
            if (this.zzf != null) {
                final zzao zzf = this.zzf;
                zzaa zzaa;
                if (zze != null) {
                    zzaa = new zzb(zze);
                }
                else {
                    zzaa = null;
                }
                zzf.zza(zzaa);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the AdClickListener.", (Throwable)ex);
        }
    }
    
    public final void zza(final zzbp zzbp) {
        try {
            if (this.zzf == null) {
                if (this.zzg == null) {
                    this.zzb("loadAd");
                }
                zzj zza;
                if (this.zzm) {
                    zza = com.google.android.gms.ads.internal.client.zzj.zza();
                }
                else {
                    zza = new zzj();
                }
                final zzn zzb = zzx.zzb();
                final Context zzb2 = this.zzb;
                this.zzf = com.google.android.gms.ads.internal.client.zzn.zza(zzb2, false, (zzn.zza<zzao>)new zzq(zzb, zzb2, zza, this.zzg, this.zza));
                if (this.zzd != null) {
                    this.zzf.zza(new zzc(this.zzd));
                }
                if (this.zze != null) {
                    this.zzf.zza(new zzb(this.zze));
                }
                if (this.zzh != null) {
                    this.zzf.zza(new zzl(this.zzh));
                }
                if (this.zzj != null) {
                    this.zzf.zza(new zzmj(this.zzj));
                }
                if (this.zzk != null) {
                    this.zzf.zza(this.zzk.zza());
                }
                if (this.zzl != null) {
                    this.zzf.zza(new zzacm(this.zzl));
                }
                this.zzf.zzb(this.zzn);
            }
            if (this.zzf.zzb(com.google.android.gms.ads.internal.client.zzi.zza(this.zzb, zzbp))) {
                this.zza.zza(zzbp.zzj());
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to load ad.", (Throwable)ex);
        }
    }
    
    public final void zza(final RewardedVideoAdListener zzl) {
        try {
            this.zzl = zzl;
            if (this.zzf != null) {
                final zzao zzf = this.zzf;
                zzach zzach;
                if (zzl != null) {
                    zzach = new zzacm(zzl);
                }
                else {
                    zzach = null;
                }
                zzf.zza(zzach);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set the AdListener.", (Throwable)ex);
        }
    }
    
    public final void zza(final String zzg) {
        if (this.zzg == null) {
            this.zzg = zzg;
            return;
        }
        throw new IllegalStateException("The ad unit ID can only be set once on InterstitialAd.");
    }
    
    public final void zza(final boolean b) {
        this.zzm = true;
    }
    
    public final void zzb(final boolean zzn) {
        try {
            this.zzn = zzn;
            if (this.zzf != null) {
                this.zzf.zzb(zzn);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to set immersive mode", (Throwable)ex);
        }
    }
    
    public final void zzh() {
        try {
            this.zzb("show");
            this.zzf.zzan();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Failed to show interstitial.", (Throwable)ex);
        }
    }
}
