package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzpu;
import com.google.android.gms.internal.zzvu;
import com.google.android.gms.internal.zzacl;
import com.google.android.gms.internal.zzpt;
import com.google.android.gms.internal.zzll;
import com.google.android.gms.internal.zzlk;
import com.google.android.gms.internal.zzlj;
import com.google.android.gms.internal.zzaht;

public final class zzx
{
    private static final Object zza;
    private static zzx zzb;
    private final zzaht zzc;
    private final zzn zzd;
    private final String zze;
    private final zzlj zzf;
    private final zzlk zzg;
    private final zzll zzh;
    
    static {
        zza = new Object();
        final zzx zzb = new zzx();
        synchronized (zzx.zza) {
            zzx.zzb = zzb;
        }
    }
    
    protected zzx() {
        this.zzc = new zzaht();
        this.zzd = new zzn(new zze(), new zzd(), new zzbx(), new zzpt(), new zzacl(), new zzvu(), new zzpu());
        this.zze = zzaht.zzc();
        this.zzf = new zzlj();
        this.zzg = new zzlk();
        this.zzh = new zzll();
    }
    
    public static zzaht zza() {
        return zzg().zzc;
    }
    
    public static zzn zzb() {
        return zzg().zzd;
    }
    
    private static zzx zzg() {
        synchronized (zzx.zza) {
            return zzx.zzb;
        }
    }
}
