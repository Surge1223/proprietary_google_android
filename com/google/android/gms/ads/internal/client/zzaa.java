package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzaa extends IInterface
{
    void zza() throws RemoteException;
}
