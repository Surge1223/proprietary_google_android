package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.zzfa;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzbi extends zzez implements zzbh
{
    public zzbi() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.client.IVideoController");
    }
    
    public static zzbh zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
        if (queryLocalInterface instanceof zzbh) {
            return (zzbh)queryLocalInterface;
        }
        return new zzbj(binder);
    }
    
    public boolean onTransact(int zzd, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
        if (this.zza(zzd, parcel, parcel2, n)) {
            return true;
        }
        switch (zzd) {
            default: {
                return false;
            }
            case 12: {
                final boolean zzj = this.zzj();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzj);
                break;
            }
            case 11: {
                final zzbk zzi = this.zzi();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzi);
                break;
            }
            case 10: {
                final boolean zzh = this.zzh();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzh);
                break;
            }
            case 9: {
                final float zzg = this.zzg();
                parcel2.writeNoException();
                parcel2.writeFloat(zzg);
                break;
            }
            case 8: {
                final IBinder strongBinder = parcel.readStrongBinder();
                zzbk zzbk;
                if (strongBinder == null) {
                    zzbk = null;
                }
                else {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
                    if (queryLocalInterface instanceof zzbk) {
                        zzbk = (zzbk)queryLocalInterface;
                    }
                    else {
                        zzbk = new zzbm(strongBinder);
                    }
                }
                this.zza(zzbk);
                parcel2.writeNoException();
                break;
            }
            case 7: {
                final float zzf = this.zzf();
                parcel2.writeNoException();
                parcel2.writeFloat(zzf);
                break;
            }
            case 6: {
                final float zze = this.zze();
                parcel2.writeNoException();
                parcel2.writeFloat(zze);
                break;
            }
            case 5: {
                zzd = this.zzd();
                parcel2.writeNoException();
                parcel2.writeInt(zzd);
                break;
            }
            case 4: {
                final boolean zzc = this.zzc();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzc);
                break;
            }
            case 3: {
                this.zza(zzfa.zza(parcel));
                parcel2.writeNoException();
                break;
            }
            case 2: {
                this.zzb();
                parcel2.writeNoException();
                break;
            }
            case 1: {
                this.zza();
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
