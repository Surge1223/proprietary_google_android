package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzbj extends zzey implements zzbh
{
    zzbj(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IVideoController");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(1, this.a_());
    }
    
    @Override
    public final void zza(final zzbk zzbk) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzbk);
        this.zzb(8, a_);
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, b);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final boolean zzc() throws RemoteException {
        final Parcel zza = this.zza(4, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final int zzd() throws RemoteException {
        final Parcel zza = this.zza(5, this.a_());
        final int int1 = zza.readInt();
        zza.recycle();
        return int1;
    }
    
    @Override
    public final float zze() throws RemoteException {
        final Parcel zza = this.zza(6, this.a_());
        final float float1 = zza.readFloat();
        zza.recycle();
        return float1;
    }
    
    @Override
    public final float zzf() throws RemoteException {
        final Parcel zza = this.zza(7, this.a_());
        final float float1 = zza.readFloat();
        zza.recycle();
        return float1;
    }
    
    @Override
    public final float zzg() throws RemoteException {
        final Parcel zza = this.zza(9, this.a_());
        final float float1 = zza.readFloat();
        zza.recycle();
        return float1;
    }
    
    @Override
    public final boolean zzh() throws RemoteException {
        final Parcel zza = this.zza(10, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzbk zzi() throws RemoteException {
        final Parcel zza = this.zza(11, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zzbk zzbk;
        if (strongBinder == null) {
            zzbk = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
            if (queryLocalInterface instanceof zzbk) {
                zzbk = (zzbk)queryLocalInterface;
            }
            else {
                zzbk = new zzbm(strongBinder);
            }
        }
        zza.recycle();
        return zzbk;
    }
    
    @Override
    public final boolean zzj() throws RemoteException {
        final Parcel zza = this.zza(12, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
}
