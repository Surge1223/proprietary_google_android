package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzac extends zzey implements zzaa
{
    zzac(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdClickListener");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(1, this.a_());
    }
}
