package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzbk extends IInterface
{
    void zza() throws RemoteException;
    
    void zza(final boolean p0) throws RemoteException;
    
    void zzb() throws RemoteException;
    
    void zzc() throws RemoteException;
    
    void zzd() throws RemoteException;
}
