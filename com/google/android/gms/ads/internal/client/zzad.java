package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzad extends IInterface
{
    void zza() throws RemoteException;
    
    void zza(final int p0) throws RemoteException;
    
    void zzb() throws RemoteException;
    
    void zzc() throws RemoteException;
    
    void zzd() throws RemoteException;
    
    void zze() throws RemoteException;
    
    void zzf() throws RemoteException;
}
