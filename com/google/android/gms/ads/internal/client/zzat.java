package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzat extends IInterface
{
    void zza(final String p0, final String p1) throws RemoteException;
}
