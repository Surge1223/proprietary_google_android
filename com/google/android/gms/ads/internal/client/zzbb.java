package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzbb extends zzey implements zzaz
{
    zzbb(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
    }
    
    @Override
    public final long zzb() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final long long1 = zza.readLong();
        zza.recycle();
        return long1;
    }
}
