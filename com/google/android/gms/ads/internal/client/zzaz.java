package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzaz extends IInterface
{
    long zzb() throws RemoteException;
}
