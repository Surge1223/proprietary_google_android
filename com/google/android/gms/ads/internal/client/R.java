package com.google.android.gms.ads.internal.client;

public final class R
{
    public static final class styleable
    {
        public static int[] AdsAttrs;
        public static int AdsAttrs_adSize;
        public static int AdsAttrs_adSizes;
        public static int AdsAttrs_adUnitId;
        
        static {
            styleable.AdsAttrs = new int[] { 2130771968, 2130771969, 2130771970 };
            styleable.AdsAttrs_adSize = 0;
            styleable.AdsAttrs_adSizes = 1;
            styleable.AdsAttrs_adUnitId = 2;
        }
    }
}
