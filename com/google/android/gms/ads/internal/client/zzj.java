package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.zzb;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.util.DisplayMetrics;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.ads.AdSize;
import android.content.Context;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class zzj extends zzbid
{
    public static final Parcelable.Creator<zzj> CREATOR;
    public final String zza;
    public final int zzb;
    public final int zzc;
    public final boolean zzd;
    public final int zze;
    public final int zzf;
    public final zzj[] zzg;
    public final boolean zzh;
    public final boolean zzi;
    public boolean zzj;
    
    static {
        CREATOR = (Parcelable.Creator)new zzk();
    }
    
    public zzj() {
        this("interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
    }
    
    public zzj(final Context context, final AdSize adSize) {
        this(context, new AdSize[] { adSize });
    }
    
    public zzj(final Context context, final AdSize[] array) {
        final AdSize adSize = array[0];
        this.zzd = false;
        this.zzi = adSize.isFluid();
        if (this.zzi) {
            this.zze = AdSize.BANNER.getWidth();
            this.zzb = AdSize.BANNER.getHeight();
        }
        else {
            this.zze = adSize.getWidth();
            this.zzb = adSize.getHeight();
        }
        final boolean b = this.zze == -1;
        final boolean b2 = this.zzb == -2;
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int zze;
        if (b) {
            zzx.zza();
            Label_0172: {
                if (zzaht.zzg(context)) {
                    zzx.zza();
                    if (zzaht.zzh(context)) {
                        final int widthPixels = displayMetrics.widthPixels;
                        zzx.zza();
                        this.zzf = widthPixels - zzaht.zzi(context);
                        break Label_0172;
                    }
                }
                this.zzf = displayMetrics.widthPixels;
            }
            final double n = this.zzf / displayMetrics.density;
            final int n2 = zze = (int)n;
            if (n - n2 >= 0.01) {
                zze = n2 + 1;
            }
        }
        else {
            zze = this.zze;
            zzx.zza();
            this.zzf = zzaht.zza(displayMetrics, this.zze);
        }
        int n3;
        if (b2) {
            n3 = zzc(displayMetrics);
        }
        else {
            n3 = this.zzb;
        }
        zzx.zza();
        this.zzc = zzaht.zza(displayMetrics, n3);
        if (!b && !b2) {
            if (this.zzi) {
                this.zza = "320x50_mb";
            }
            else {
                this.zza = adSize.toString();
            }
        }
        else {
            final StringBuilder sb = new StringBuilder(26);
            sb.append(zze);
            sb.append("x");
            sb.append(n3);
            sb.append("_as");
            this.zza = sb.toString();
        }
        if (array.length > 1) {
            this.zzg = new zzj[array.length];
            for (int i = 0; i < array.length; ++i) {
                this.zzg[i] = new zzj(context, array[i]);
            }
        }
        else {
            this.zzg = null;
        }
        this.zzh = false;
        this.zzj = false;
    }
    
    zzj(final String zza, final int zzb, final int zzc, final boolean zzd, final int zze, final int zzf, final zzj[] zzg, final boolean zzh, final boolean zzi, final boolean zzj) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
    }
    
    public static int zza(final DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }
    
    public static zzj zza() {
        return new zzj("reward_mb", 0, 0, true, 0, 0, null, false, false, false);
    }
    
    public static int zzb(final DisplayMetrics displayMetrics) {
        return (int)(zzc(displayMetrics) * displayMetrics.density);
    }
    
    private static int zzc(final DisplayMetrics displayMetrics) {
        final int n = (int)(displayMetrics.heightPixels / displayMetrics.density);
        if (n <= 400) {
            return 32;
        }
        if (n <= 720) {
            return 50;
        }
        return 90;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, 4, this.zzc);
        zzbig.zza(parcel, 5, this.zzd);
        zzbig.zza(parcel, 6, this.zze);
        zzbig.zza(parcel, 7, this.zzf);
        zzbig.zza(parcel, 8, this.zzg, n, false);
        zzbig.zza(parcel, 9, this.zzh);
        zzbig.zza(parcel, 10, this.zzi);
        zzbig.zza(parcel, 11, this.zzj);
        zzbig.zza(parcel, zza);
    }
    
    public final AdSize zzb() {
        return com.google.android.gms.ads.zzb.zza(this.zze, this.zzb, this.zza);
    }
}
