package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzox extends IInterface
{
    void zza(final zzol p0) throws RemoteException;
}
