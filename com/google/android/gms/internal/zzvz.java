package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzvz extends zzey implements zzvy
{
    zzvz(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.overlay.client.IAdOverlayCreator");
    }
}
