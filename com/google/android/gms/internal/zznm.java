package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.internal.client.zzcn;
import android.os.Parcelable.Creator;

public final class zznm extends zzbid
{
    public static final Parcelable.Creator<zznm> CREATOR;
    public final int zza;
    public final boolean zzb;
    public final int zzc;
    public final boolean zzd;
    public final int zze;
    public final zzcn zzf;
    
    static {
        CREATOR = (Parcelable.Creator)new zznn();
    }
    
    public zznm(final int zza, final boolean zzb, final int zzc, final boolean zzd, final int zze, final zzcn zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    public zznm(final NativeAdOptions nativeAdOptions) {
        final boolean shouldReturnUrlsForImageAssets = nativeAdOptions.shouldReturnUrlsForImageAssets();
        final int imageOrientation = nativeAdOptions.getImageOrientation();
        final boolean shouldRequestMultipleImages = nativeAdOptions.shouldRequestMultipleImages();
        final int adChoicesPlacement = nativeAdOptions.getAdChoicesPlacement();
        zzcn zzcn;
        if (nativeAdOptions.getVideoOptions() != null) {
            zzcn = new zzcn(nativeAdOptions.getVideoOptions());
        }
        else {
            zzcn = null;
        }
        this(3, shouldReturnUrlsForImageAssets, imageOrientation, shouldRequestMultipleImages, adChoicesPlacement, zzcn);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc);
        zzbig.zza(parcel, 4, this.zzd);
        zzbig.zza(parcel, 5, this.zze);
        zzbig.zza(parcel, 6, (Parcelable)this.zzf, n, false);
        zzbig.zza(parcel, zza);
    }
}
