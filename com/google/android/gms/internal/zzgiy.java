package com.google.android.gms.internal;

final class zzgiy<V>
{
    private static final Object zza;
    private static final zzgiy<Object> zzb;
    private int zzc;
    private final float zzd;
    private int[] zze;
    private V[] zzf;
    private int zzg;
    private int zzh;
    
    static {
        zza = new Object();
        zzb = new zzgiy<Object>(true);
    }
    
    public zzgiy() {
        this(8, 0.5f);
    }
    
    private zzgiy(int n, final float n2) {
        this.zzd = 0.5f;
        n = 1 << 32 - Integer.numberOfLeadingZeros(7);
        this.zzh = n - 1;
        this.zze = new int[n];
        this.zzf = new Object[n];
        this.zzc = this.zzc(n);
    }
    
    private zzgiy(final boolean b) {
        this.zzd = 0.5f;
        this.zze = null;
        this.zzf = null;
    }
    
    static <V> zzgiy<V> zza() {
        return (zzgiy<V>)zzgiy.zzb;
    }
    
    private static <T> T zza(final T t) {
        T t2 = t;
        if (t == zzgiy.zza) {
            t2 = null;
        }
        return t2;
    }
    
    private final int zzb(final int n) {
        return n + 1 & this.zzh;
    }
    
    private static <T> T zzb(final T t) {
        Object zza = t;
        if (t == null) {
            zza = zzgiy.zza;
        }
        return (T)zza;
    }
    
    private final int zzc(final int n) {
        return Math.min(n - 1, (int)(n * this.zzd));
    }
    
    @Override
    public final String toString() {
        if (this.zzb()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(4 * this.zzg);
        sb.append('{');
        int n = 1;
        int n2;
        for (int i = 0; i < this.zzf.length; ++i, n = n2) {
            final Object o = this.zzf[i];
            n2 = n;
            if (o != null) {
                if (n == 0) {
                    sb.append(", ");
                }
                sb.append(Integer.toString(this.zze[i]));
                sb.append('=');
                Object zza;
                if (o == this) {
                    zza = "(this Map)";
                }
                else {
                    zza = zza(o);
                }
                sb.append(zza);
                n2 = 0;
            }
        }
        sb.append('}');
        return sb.toString();
    }
    
    public final V zza(final int n) {
        if (this.zze == null) {
            return null;
        }
        int zzb;
        final int n2 = zzb = (this.zzh & n);
        while (true) {
            while (this.zzf[zzb] != null) {
                if (n == this.zze[zzb]) {
                    if (zzb == -1) {
                        return null;
                    }
                    return zza(this.zzf[zzb]);
                }
                else {
                    if ((zzb = this.zzb(zzb)) == n2) {
                        break;
                    }
                    continue;
                }
            }
            zzb = -1;
            continue;
        }
    }
    
    public final V zza(int i, final V v) {
        if (this.zze != null) {
            int zzb;
            final int n = zzb = (this.zzh & i);
            while (this.zzf[zzb] != null) {
                if (this.zze[zzb] == i) {
                    final Object o = this.zzf[zzb];
                    this.zzf[zzb] = zzb(v);
                    return zza(o);
                }
                zzb = this.zzb(zzb);
                if (zzb != n) {
                    continue;
                }
                throw new IllegalStateException("Unable to insert");
            }
            this.zze[zzb] = i;
            this.zzf[zzb] = zzb(v);
            ++this.zzg;
            if (this.zzg > this.zzc) {
                if (this.zze.length == Integer.MAX_VALUE) {
                    i = this.zzg;
                    final StringBuilder sb = new StringBuilder(40);
                    sb.append("Max capacity reached at size=");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString());
                }
                i = this.zze.length << 1;
                final int[] zze = this.zze;
                final Object[] zzf = this.zzf;
                this.zze = new int[i];
                this.zzf = new Object[i];
                this.zzc = this.zzc(i);
                this.zzh = i - 1;
                Object o2;
                int n2;
                int zzb2;
                for (i = 0; i < zzf.length; ++i) {
                    o2 = zzf[i];
                    if (o2 != null) {
                        n2 = zze[i];
                        for (zzb2 = (this.zzh & n2); this.zzf[zzb2] != null; zzb2 = this.zzb(zzb2)) {}
                        this.zze[zzb2] = n2;
                        this.zzf[zzb2] = o2;
                    }
                }
            }
            return null;
        }
        throw new IllegalStateException("Trying to modify an immutable map.");
    }
    
    public final boolean zzb() {
        return this.zzg == 0;
    }
}
