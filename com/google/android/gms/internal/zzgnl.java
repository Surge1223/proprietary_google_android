package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgnl extends zzgmo<zzgnl>
{
    private static volatile zzgnl[] zza;
    private String zzb;
    private String zzc;
    private zzgnk[] zzd;
    private zzgnj zze;
    
    public zzgnl() {
        this.zzb = "";
        this.zzc = "";
        this.zzd = zzgnk.zza();
        this.zze = null;
        this.zzax = null;
        this.zzay = -1;
    }
    
    public static zzgnl[] zza() {
        if (zzgnl.zza == null) {
            synchronized (zzgms.zzb) {
                if (zzgnl.zza == null) {
                    zzgnl.zza = new zzgnl[0];
                }
            }
        }
        return zzgnl.zza;
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zzb != null) {
            computeSerializedSize = n;
            if (!this.zzb.equals("")) {
                computeSerializedSize = n + zzgmm.zzb(1, this.zzb);
            }
        }
        int n2 = computeSerializedSize;
        if (this.zzc != null) {
            n2 = computeSerializedSize;
            if (!this.zzc.equals("")) {
                n2 = computeSerializedSize + zzgmm.zzb(2, this.zzc);
            }
        }
        int n3 = n2;
        if (this.zzd != null) {
            n3 = n2;
            if (this.zzd.length > 0) {
                int n4 = 0;
                while (true) {
                    n3 = n2;
                    if (n4 >= this.zzd.length) {
                        break;
                    }
                    final zzgnk zzgnk = this.zzd[n4];
                    int n5 = n2;
                    if (zzgnk != null) {
                        n5 = n2 + zzgmm.zzb(3, zzgnk);
                    }
                    ++n4;
                    n2 = n5;
                }
            }
        }
        int n6 = n3;
        if (this.zze != null) {
            n6 = n3 + zzgmm.zzb(4, this.zze);
        }
        return n6;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgnl)) {
            return false;
        }
        final zzgnl zzgnl = (zzgnl)o;
        if (this.zzb == null) {
            if (zzgnl.zzb != null) {
                return false;
            }
        }
        else if (!this.zzb.equals(zzgnl.zzb)) {
            return false;
        }
        if (this.zzc == null) {
            if (zzgnl.zzc != null) {
                return false;
            }
        }
        else if (!this.zzc.equals(zzgnl.zzc)) {
            return false;
        }
        if (!zzgms.zza(this.zzd, zzgnl.zzd)) {
            return false;
        }
        if (this.zze == null) {
            if (zzgnl.zze != null) {
                return false;
            }
        }
        else if (!this.zze.equals(zzgnl.zze)) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgnl.zzax);
        }
        return zzgnl.zzax == null || zzgnl.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final String zzb = this.zzb;
        int hashCode2 = 0;
        int hashCode3;
        if (zzb == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = this.zzb.hashCode();
        }
        int hashCode4;
        if (this.zzc == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = this.zzc.hashCode();
        }
        final int zza = zzgms.zza(this.zzd);
        final zzgnj zze = this.zze;
        int hashCode5;
        if (zze == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = zze.hashCode();
        }
        if (this.zzax != null) {
            if (!this.zzax.zzb()) {
                hashCode2 = this.zzax.hashCode();
            }
        }
        return (((((527 + hashCode) * 31 + hashCode3) * 31 + hashCode4) * 31 + zza) * 31 + hashCode5) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zzb != null && !this.zzb.equals("")) {
            zzgmm.zza(1, this.zzb);
        }
        if (this.zzc != null && !this.zzc.equals("")) {
            zzgmm.zza(2, this.zzc);
        }
        if (this.zzd != null && this.zzd.length > 0) {
            for (int i = 0; i < this.zzd.length; ++i) {
                final zzgnk zzgnk = this.zzd[i];
                if (zzgnk != null) {
                    zzgmm.zza(3, zzgnk);
                }
            }
        }
        if (this.zze != null) {
            zzgmm.zza(4, this.zze);
        }
        super.writeTo(zzgmm);
    }
}
