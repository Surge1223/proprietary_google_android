package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgnj extends zzgmo<zzgnj>
{
    public boolean zza;
    public int zzb;
    public String zzc;
    private zzgnk[] zzd;
    
    public zzgnj() {
        this.zza = false;
        this.zzb = 0;
        this.zzc = "";
        this.zzd = zzgnk.zza();
        this.zzax = null;
        this.zzay = -1;
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza) {
            computeSerializedSize = n + (zzgmm.zzb(1) + 1);
        }
        int n2 = computeSerializedSize;
        if (this.zzb != 0) {
            n2 = computeSerializedSize + zzgmm.zzb(2, this.zzb);
        }
        int n3 = n2;
        if (this.zzc != null) {
            n3 = n2;
            if (!this.zzc.equals("")) {
                n3 = n2 + zzgmm.zzb(3, this.zzc);
            }
        }
        int n4 = n3;
        if (this.zzd != null) {
            n4 = n3;
            if (this.zzd.length > 0) {
                int n5 = 0;
                while (true) {
                    n4 = n3;
                    if (n5 >= this.zzd.length) {
                        break;
                    }
                    final zzgnk zzgnk = this.zzd[n5];
                    int n6 = n3;
                    if (zzgnk != null) {
                        n6 = n3 + zzgmm.zzb(4, zzgnk);
                    }
                    ++n5;
                    n3 = n6;
                }
            }
        }
        return n4;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgnj)) {
            return false;
        }
        final zzgnj zzgnj = (zzgnj)o;
        if (this.zza != zzgnj.zza) {
            return false;
        }
        if (this.zzb != zzgnj.zzb) {
            return false;
        }
        if (this.zzc == null) {
            if (zzgnj.zzc != null) {
                return false;
            }
        }
        else if (!this.zzc.equals(zzgnj.zzc)) {
            return false;
        }
        if (!zzgms.zza(this.zzd, zzgnj.zzd)) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgnj.zzax);
        }
        return zzgnj.zzax == null || zzgnj.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        int n;
        if (this.zza) {
            n = 1231;
        }
        else {
            n = 1237;
        }
        final int zzb = this.zzb;
        final String zzc = this.zzc;
        int hashCode2 = 0;
        int hashCode3;
        if (zzc == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = this.zzc.hashCode();
        }
        final int zza = zzgms.zza(this.zzd);
        if (this.zzax != null) {
            if (!this.zzax.zzb()) {
                hashCode2 = this.zzax.hashCode();
            }
        }
        return (((((527 + hashCode) * 31 + n) * 31 + zzb) * 31 + hashCode3) * 31 + zza) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza) {
            zzgmm.zza(1, this.zza);
        }
        if (this.zzb != 0) {
            zzgmm.zza(2, this.zzb);
        }
        if (this.zzc != null && !this.zzc.equals("")) {
            zzgmm.zza(3, this.zzc);
        }
        if (this.zzd != null && this.zzd.length > 0) {
            for (int i = 0; i < this.zzd.length; ++i) {
                final zzgnk zzgnk = this.zzd[i];
                if (zzgnk != null) {
                    zzgmm.zza(4, zzgnk);
                }
            }
        }
        super.writeTo(zzgmm);
    }
}
