package com.google.android.gms.internal;

import java.util.Iterator;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class zzgmq implements Cloneable
{
    private static final zzgmr zza;
    private boolean zzb;
    private int[] zzc;
    private zzgmr[] zzd;
    private int zze;
    
    static {
        zza = new zzgmr();
    }
    
    zzgmq() {
        this(10);
    }
    
    private zzgmq(int zzd) {
        this.zzb = false;
        zzd = zzd(zzd);
        this.zzc = new int[zzd];
        this.zzd = new zzgmr[zzd];
        this.zze = 0;
    }
    
    private final void zzc() {
        final int zze = this.zze;
        final int[] zzc = this.zzc;
        final zzgmr[] zzd = this.zzd;
        int zze2;
        int n;
        for (int i = zze2 = 0; i < zze; ++i, zze2 = n) {
            final zzgmr zzgmr = zzd[i];
            n = zze2;
            if (zzgmr != zzgmq.zza) {
                if (i != zze2) {
                    zzc[zze2] = zzc[i];
                    zzd[zze2] = zzgmr;
                    zzd[i] = null;
                }
                n = zze2 + 1;
            }
        }
        this.zzb = false;
        this.zze = zze2;
    }
    
    private static int zzd(int i) {
        final int n = i << 2;
        int n2;
        for (i = 4; i < 32; ++i) {
            n2 = (1 << i) - 12;
            if (n <= n2) {
                i = n2;
                return i / 4;
            }
        }
        i = n;
        return i / 4;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgmq)) {
            return false;
        }
        final zzgmq zzgmq = (zzgmq)o;
        if (this.zza() != zzgmq.zza()) {
            return false;
        }
        final int[] zzc = this.zzc;
        final int[] zzc2 = zzgmq.zzc;
        while (true) {
            for (int zze = this.zze, i = 0; i < zze; ++i) {
                if (zzc[i] != zzc2[i]) {
                    final boolean b = false;
                    if (b) {
                        final zzgmr[] zzd = this.zzd;
                        final zzgmr[] zzd2 = zzgmq.zzd;
                        final int zze2 = this.zze;
                        int j = 0;
                        while (true) {
                            while (j < zze2) {
                                if (!zzd[j].equals(zzd2[j])) {
                                    final boolean b2 = false;
                                    if (b2) {
                                        return true;
                                    }
                                    return false;
                                }
                                else {
                                    ++j;
                                }
                            }
                            final boolean b2 = true;
                            continue;
                        }
                    }
                    return false;
                }
            }
            final boolean b = true;
            continue;
        }
    }
    
    @Override
    public final int hashCode() {
        if (this.zzb) {
            this.zzc();
        }
        int n = 17;
        for (int i = 0; i < this.zze; ++i) {
            n = (n * 31 + this.zzc[i]) * 31 + this.zzd[i].hashCode();
        }
        return n;
    }
    
    final int zza() {
        if (this.zzb) {
            this.zzc();
        }
        return this.zze;
    }
    
    public final boolean zzb() {
        return this.zza() == 0;
    }
    
    final zzgmr zzc(final int n) {
        if (this.zzb) {
            this.zzc();
        }
        return this.zzd[n];
    }
}
