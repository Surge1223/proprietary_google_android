package com.google.android.gms.internal;

abstract class zzglq<T, B>
{
    abstract void zza(final T p0, final zzgmk p1);
    
    abstract void zza(final Object p0, final T p1);
    
    abstract T zzb(final Object p0);
    
    abstract void zzb(final T p0, final zzgmk p1);
    
    abstract T zzc(final T p0, final T p1);
    
    abstract int zze(final T p0);
    
    abstract int zzf(final T p0);
}
