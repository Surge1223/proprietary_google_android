package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzpm extends IInterface
{
    void zza(final zzpp p0) throws RemoteException;
}
