package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzcjd extends zzez implements zzcjc
{
    public zzcjd() {
        this.attachInterface((IInterface)this, "com.google.android.gms.flags.IFlagProvider");
    }
    
    public static zzcjc asInterface(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.flags.IFlagProvider");
        if (queryLocalInterface instanceof zzcjc) {
            return (zzcjc)queryLocalInterface;
        }
        return new zzcje(binder);
    }
    
    public boolean onTransact(int intFlagValue, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
        if (this.zza(intFlagValue, parcel, parcel2, n)) {
            return true;
        }
        switch (intFlagValue) {
            default: {
                return false;
            }
            case 5: {
                final String stringFlagValue = this.getStringFlagValue(parcel.readString(), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeString(stringFlagValue);
                break;
            }
            case 4: {
                final long longFlagValue = this.getLongFlagValue(parcel.readString(), parcel.readLong(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeLong(longFlagValue);
                break;
            }
            case 3: {
                intFlagValue = this.getIntFlagValue(parcel.readString(), parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeInt(intFlagValue);
                break;
            }
            case 2: {
                final boolean booleanFlagValue = this.getBooleanFlagValue(parcel.readString(), zzfa.zza(parcel), parcel.readInt());
                parcel2.writeNoException();
                zzfa.zza(parcel2, booleanFlagValue);
                break;
            }
            case 1: {
                this.init(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
