package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzaci extends zzez implements zzach
{
    public zzaci() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
    }
    
    public static zzach zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
        if (queryLocalInterface instanceof zzach) {
            return (zzach)queryLocalInterface;
        }
        return new zzacj(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 8: {
                this.zzf();
                break;
            }
            case 7: {
                this.zza(parcel.readInt());
                break;
            }
            case 6: {
                this.zze();
                break;
            }
            case 5: {
                final IBinder strongBinder = parcel.readStrongBinder();
                zzabz zzabz;
                if (strongBinder == null) {
                    zzabz = null;
                }
                else {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardItem");
                    if (queryLocalInterface instanceof zzabz) {
                        zzabz = (zzabz)queryLocalInterface;
                    }
                    else {
                        zzabz = new zzacb(strongBinder);
                    }
                }
                this.zza(zzabz);
                break;
            }
            case 4: {
                this.zzd();
                break;
            }
            case 3: {
                this.zzc();
                break;
            }
            case 2: {
                this.zzb();
                break;
            }
            case 1: {
                this.zza();
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}
