package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgjy<K, V>
{
    static <K, V> int zza(final zzgjz<K, V> zzgjz, final K k, final V v) {
        return zzgin.zza(zzgjz.zza, 1, k) + zzgin.zza(zzgjz.zzc, 2, v);
    }
    
    static <K, V> void zza(final zzgic zzgic, final zzgjz<K, V> zzgjz, final K k, final V v) throws IOException {
        zzgin.zza(zzgic, zzgjz.zza, 1, k);
        zzgin.zza(zzgic, zzgjz.zzc, 2, v);
    }
}
