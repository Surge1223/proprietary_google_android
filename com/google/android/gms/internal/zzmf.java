package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzmf extends zzey implements zzmd
{
    zzmf(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
    }
}
