package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzpb extends zzez implements zzpa
{
    public zzpb() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.formats.client.IOnContentAdLoadedListener");
    }
    
    public static zzpa zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IOnContentAdLoadedListener");
        if (queryLocalInterface instanceof zzpa) {
            return (zzpa)queryLocalInterface;
        }
        return new zzpc(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            final IBinder strongBinder = parcel.readStrongBinder();
            zzop zzop;
            if (strongBinder == null) {
                zzop = null;
            }
            else {
                final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeContentAd");
                if (queryLocalInterface instanceof zzop) {
                    zzop = (zzop)queryLocalInterface;
                }
                else {
                    zzop = new zzor(strongBinder);
                }
            }
            this.zza(zzop);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}
