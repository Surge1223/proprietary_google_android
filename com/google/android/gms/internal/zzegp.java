package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import android.os.Parcel;
import android.os.IInterface;

public abstract class zzegp extends zzez implements zzego
{
    public zzegp() {
        this.attachInterface((IInterface)this, "com.google.android.gms.signin.internal.ISignInCallbacks");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 8: {
                this.zza(zzfa.zza(parcel, zzegv.CREATOR));
                break;
            }
            case 7: {
                this.zza(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, GoogleSignInAccount.CREATOR));
                break;
            }
            case 6: {
                this.zzb(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 4: {
                this.zza(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 3: {
                this.zza(zzfa.zza(parcel, ConnectionResult.CREATOR), zzfa.zza(parcel, zzegl.CREATOR));
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}
