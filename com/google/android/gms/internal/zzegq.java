package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzegq extends IInterface
{
    void zza(final zzegt p0, final zzego p1) throws RemoteException;
}
