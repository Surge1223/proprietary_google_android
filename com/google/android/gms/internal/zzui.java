package com.google.android.gms.internal;

import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.internal.client.zzbh;
import com.google.android.gms.ads.VideoController;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationBannerListener;

public final class zzui implements MediationBannerListener, MediationInterstitialListener, MediationNativeListener
{
    private final zztq zza;
    private NativeAdMapper zzb;
    private zza zzc;
    private NativeCustomTemplateAd zzd;
    
    public zzui(final zztq zza) {
        this.zza = zza;
    }
    
    private static void zza(final MediationNativeAdapter mediationNativeAdapter, final zza zza, final NativeAdMapper nativeAdMapper) {
        if (mediationNativeAdapter instanceof AdMobAdapter) {
            return;
        }
        final VideoController videoController = new VideoController();
        videoController.zza(new zzuf());
        if (nativeAdMapper != null && nativeAdMapper.hasVideoContent()) {
            nativeAdMapper.zza(videoController);
        }
    }
    
    @Override
    public final void onAdClicked(final MediationBannerAdapter mediationBannerAdapter) {
        zzau.zzb("onAdClicked must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdClicked.");
        try {
            this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClicked.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdClicked(final MediationInterstitialAdapter mediationInterstitialAdapter) {
        zzau.zzb("onAdClicked must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdClicked.");
        try {
            this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClicked.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdClicked(final MediationNativeAdapter mediationNativeAdapter) {
        zzau.zzb("onAdClicked must be called on the main UI thread.");
        final NativeAdMapper zzb = this.zzb;
        final zza zzc = this.zzc;
        if (this.zzd == null) {
            if (zzb == null && zzc == null) {
                zzaid.zze("Could not call onAdClicked since mapper is null.");
                return;
            }
            if (zzc != null && !zzc.zzn()) {
                zzaid.zzb("Could not call onAdClicked since setOverrideClickHandling is not set to true");
                return;
            }
            if (zzb != null && !zzb.getOverrideClickHandling()) {
                zzaid.zzb("Could not call onAdClicked since setOverrideClickHandling is not set to true");
                return;
            }
        }
        zzaid.zzb("Adapter called onAdClicked.");
        try {
            this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClicked.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdClosed(final MediationBannerAdapter mediationBannerAdapter) {
        zzau.zzb("onAdClosed must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdClosed.");
        try {
            this.zza.zzb();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClosed.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdClosed(final MediationInterstitialAdapter mediationInterstitialAdapter) {
        zzau.zzb("onAdClosed must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdClosed.");
        try {
            this.zza.zzb();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClosed.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdClosed(final MediationNativeAdapter mediationNativeAdapter) {
        zzau.zzb("onAdClosed must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdClosed.");
        try {
            this.zza.zzb();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClosed.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdFailedToLoad(final MediationBannerAdapter mediationBannerAdapter, final int n) {
        zzau.zzb("onAdFailedToLoad must be called on the main UI thread.");
        final StringBuilder sb = new StringBuilder(55);
        sb.append("Adapter called onAdFailedToLoad with error. ");
        sb.append(n);
        zzaid.zzb(sb.toString());
        try {
            this.zza.zza(n);
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdFailedToLoad(final MediationInterstitialAdapter mediationInterstitialAdapter, final int n) {
        zzau.zzb("onAdFailedToLoad must be called on the main UI thread.");
        final StringBuilder sb = new StringBuilder(55);
        sb.append("Adapter called onAdFailedToLoad with error ");
        sb.append(n);
        sb.append(".");
        zzaid.zzb(sb.toString());
        try {
            this.zza.zza(n);
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdFailedToLoad(final MediationNativeAdapter mediationNativeAdapter, final int n) {
        zzau.zzb("onAdFailedToLoad must be called on the main UI thread.");
        final StringBuilder sb = new StringBuilder(55);
        sb.append("Adapter called onAdFailedToLoad with error ");
        sb.append(n);
        sb.append(".");
        zzaid.zzb(sb.toString());
        try {
            this.zza.zza(n);
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdImpression(final MediationNativeAdapter mediationNativeAdapter) {
        zzau.zzb("onAdImpression must be called on the main UI thread.");
        final NativeAdMapper zzb = this.zzb;
        final zza zzc = this.zzc;
        if (this.zzd == null) {
            if (zzb == null && zzc == null) {
                zzaid.zze("Could not call onAdImpression since AdMapper is null. ");
                return;
            }
            if (zzc != null && !zzc.zzm()) {
                zzaid.zzb("Could not call onAdImpression since setOverrideImpressionRecording is not set to true");
                return;
            }
            if (zzb != null && !zzb.getOverrideImpressionRecording()) {
                zzaid.zzb("Could not call onAdImpression since setOverrideImpressionRecording is not set to true");
                return;
            }
        }
        zzaid.zzb("Adapter called onAdImpression.");
        try {
            this.zza.zzf();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdImpression.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLeftApplication(final MediationBannerAdapter mediationBannerAdapter) {
        zzau.zzb("onAdLeftApplication must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLeftApplication.");
        try {
            this.zza.zzc();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLeftApplication.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLeftApplication(final MediationInterstitialAdapter mediationInterstitialAdapter) {
        zzau.zzb("onAdLeftApplication must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLeftApplication.");
        try {
            this.zza.zzc();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLeftApplication.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLeftApplication(final MediationNativeAdapter mediationNativeAdapter) {
        zzau.zzb("onAdLeftApplication must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLeftApplication.");
        try {
            this.zza.zzc();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLeftApplication.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLoaded(final MediationBannerAdapter mediationBannerAdapter) {
        zzau.zzb("onAdLoaded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLoaded.");
        try {
            this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLoaded.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLoaded(final MediationInterstitialAdapter mediationInterstitialAdapter) {
        zzau.zzb("onAdLoaded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLoaded.");
        try {
            this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLoaded.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLoaded(final MediationNativeAdapter mediationNativeAdapter, final NativeAdMapper zzb) {
        zzau.zzb("onAdLoaded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLoaded.");
        this.zzb = zzb;
        zza(mediationNativeAdapter, this.zzc = null, this.zzb);
        try {
            this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLoaded.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdOpened(final MediationBannerAdapter mediationBannerAdapter) {
        zzau.zzb("onAdOpened must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdOpened.");
        try {
            this.zza.zzd();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdOpened.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdOpened(final MediationInterstitialAdapter mediationInterstitialAdapter) {
        zzau.zzb("onAdOpened must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdOpened.");
        try {
            this.zza.zzd();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdOpened.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdOpened(final MediationNativeAdapter mediationNativeAdapter) {
        zzau.zzb("onAdOpened must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdOpened.");
        try {
            this.zza.zzd();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdOpened.", (Throwable)ex);
        }
    }
    
    public final NativeAdMapper zza() {
        return this.zzb;
    }
    
    @Override
    public final void zza(final MediationBannerAdapter mediationBannerAdapter, final String s, final String s2) {
        zzau.zzb("onAppEvent must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAppEvent.");
        try {
            this.zza.zza(s, s2);
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAppEvent.", (Throwable)ex);
        }
    }
    
    @Override
    public final void zza(final MediationNativeAdapter mediationNativeAdapter, final NativeCustomTemplateAd zzd) {
        zzau.zzb("onAdLoaded must be called on the main UI thread.");
        final String value = String.valueOf(zzd.getCustomTemplateId());
        String concat;
        if (value.length() != 0) {
            concat = "Adapter called onAdLoaded with template id ".concat(value);
        }
        else {
            concat = new String("Adapter called onAdLoaded with template id ");
        }
        zzaid.zzb(concat);
        this.zzd = zzd;
        try {
            this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLoaded.", (Throwable)ex);
        }
    }
    
    @Override
    public final void zza(final MediationNativeAdapter mediationNativeAdapter, final NativeCustomTemplateAd nativeCustomTemplateAd, final String s) {
        if (nativeCustomTemplateAd instanceof zzow) {
            try {
                this.zza.zza(((zzow)nativeCustomTemplateAd).zza(), s);
                return;
            }
            catch (RemoteException ex) {
                zzaid.zzc("Could not call onCustomClick.", (Throwable)ex);
                return;
            }
        }
        zzaid.zze("Unexpected native custom template ad type.");
    }
    
    @Override
    public final void zza(final MediationNativeAdapter mediationNativeAdapter, final zza zzc) {
        zzau.zzb("onAdLoaded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLoaded.");
        this.zzc = zzc;
        this.zzb = null;
        zza(mediationNativeAdapter, this.zzc, this.zzb);
        try {
            this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLoaded.", (Throwable)ex);
        }
    }
    
    public final zza zzb() {
        return this.zzc;
    }
    
    public final NativeCustomTemplateAd zzc() {
        return this.zzd;
    }
}
