package com.google.android.gms.internal;

import java.util.Arrays;
import java.nio.charset.Charset;

public final class zzgms
{
    protected static final Charset zza;
    public static final Object zzb;
    private static final Charset zzc;
    
    static {
        zza = Charset.forName("UTF-8");
        zzc = Charset.forName("ISO-8859-1");
        zzb = new Object();
    }
    
    public static int zza(final double[] array) {
        if (array != null && array.length != 0) {
            return Arrays.hashCode(array);
        }
        return 0;
    }
    
    public static int zza(final int[] array) {
        if (array != null && array.length != 0) {
            return Arrays.hashCode(array);
        }
        return 0;
    }
    
    public static int zza(final long[] array) {
        if (array != null && array.length != 0) {
            return Arrays.hashCode(array);
        }
        return 0;
    }
    
    public static int zza(final Object[] array) {
        int i = 0;
        int length;
        if (array == null) {
            length = 0;
        }
        else {
            length = array.length;
        }
        int n = 0;
        while (i < length) {
            final Object o = array[i];
            int n2 = n;
            if (o != null) {
                n2 = n * 31 + o.hashCode();
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public static int zza(final boolean[] array) {
        if (array != null && array.length != 0) {
            return Arrays.hashCode(array);
        }
        return 0;
    }
    
    public static int zza(final byte[][] array) {
        int i = 0;
        int length;
        if (array == null) {
            length = 0;
        }
        else {
            length = array.length;
        }
        int n = 0;
        while (i < length) {
            final byte[] array2 = array[i];
            int n2 = n;
            if (array2 != null) {
                n2 = n * 31 + Arrays.hashCode(array2);
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public static void zza(final zzgmo zzgmo, final zzgmo zzgmo2) {
        if (zzgmo.zzax != null) {
            zzgmo2.zzax = (zzgmq)zzgmo.zzax.clone();
        }
    }
    
    public static boolean zza(final double[] array, final double[] array2) {
        if (array != null && array.length != 0) {
            return Arrays.equals(array, array2);
        }
        return array2 == null || array2.length == 0;
    }
    
    public static boolean zza(final int[] array, final int[] array2) {
        if (array != null && array.length != 0) {
            return Arrays.equals(array, array2);
        }
        return array2 == null || array2.length == 0;
    }
    
    public static boolean zza(final long[] array, final long[] array2) {
        if (array != null && array.length != 0) {
            return Arrays.equals(array, array2);
        }
        return array2 == null || array2.length == 0;
    }
    
    public static boolean zza(final Object[] array, final Object[] array2) {
        int length;
        if (array == null) {
            length = 0;
        }
        else {
            length = array.length;
        }
        int length2;
        if (array2 == null) {
            length2 = 0;
        }
        else {
            length2 = array2.length;
        }
        int n2;
        int n = n2 = 0;
        while (true) {
            int n3 = n2;
            if (n < length) {
                n3 = n2;
                if (array[n] == null) {
                    ++n;
                    continue;
                }
            }
            while (n3 < length2 && array2[n3] == null) {
                ++n3;
            }
            final boolean b = n >= length;
            final boolean b2 = n3 >= length2;
            if (b && b2) {
                return true;
            }
            if (b != b2) {
                return false;
            }
            if (!array[n].equals(array2[n3])) {
                return false;
            }
            ++n;
            n2 = n3 + 1;
        }
    }
    
    public static boolean zza(final boolean[] array, final boolean[] array2) {
        if (array != null && array.length != 0) {
            return Arrays.equals(array, array2);
        }
        return array2 == null || array2.length == 0;
    }
    
    public static boolean zza(final byte[][] array, final byte[][] array2) {
        int length;
        if (array == null) {
            length = 0;
        }
        else {
            length = array.length;
        }
        int length2;
        if (array2 == null) {
            length2 = 0;
        }
        else {
            length2 = array2.length;
        }
        int n2;
        int n = n2 = 0;
        while (true) {
            int n3 = n2;
            if (n < length) {
                n3 = n2;
                if (array[n] == null) {
                    ++n;
                    continue;
                }
            }
            while (n3 < length2 && array2[n3] == null) {
                ++n3;
            }
            final boolean b = n >= length;
            final boolean b2 = n3 >= length2;
            if (b && b2) {
                return true;
            }
            if (b != b2) {
                return false;
            }
            if (!Arrays.equals(array[n], array2[n3])) {
                return false;
            }
            ++n;
            n2 = n3 + 1;
        }
    }
}
