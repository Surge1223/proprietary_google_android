package com.google.android.gms.internal;

import com.google.android.gms.phenotype.FlagOverrides;
import com.google.android.gms.phenotype.Flag;
import com.google.android.gms.phenotype.ExperimentTokens;
import com.google.android.gms.phenotype.DogfoodsToken;
import com.google.android.gms.phenotype.Configurations;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import android.os.IInterface;

public interface zzdtf extends IInterface
{
    void zza(final Status p0) throws RemoteException;
    
    void zza(final Status p0, final Configurations p1) throws RemoteException;
    
    void zza(final Status p0, final DogfoodsToken p1) throws RemoteException;
    
    void zza(final Status p0, final ExperimentTokens p1) throws RemoteException;
    
    void zza(final Status p0, final Flag p1) throws RemoteException;
    
    void zza(final Status p0, final FlagOverrides p1) throws RemoteException;
    
    void zzb(final Status p0) throws RemoteException;
    
    void zzb(final Status p0, final Configurations p1) throws RemoteException;
    
    void zzc(final Status p0) throws RemoteException;
    
    void zzd(final Status p0) throws RemoteException;
    
    void zze(final Status p0) throws RemoteException;
    
    void zzf(final Status p0) throws RemoteException;
    
    void zzg(final Status p0) throws RemoteException;
    
    void zzh(final Status p0) throws RemoteException;
}
