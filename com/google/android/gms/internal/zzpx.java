package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

public final class zzpx extends zzpe
{
    private final NativeCustomTemplateAd.OnCustomClickListener zza;
    
    public zzpx(final NativeCustomTemplateAd.OnCustomClickListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzot zzot, final String s) {
        this.zza.onCustomClick(zzow.zza(zzot), s);
    }
}
