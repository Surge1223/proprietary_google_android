package com.google.android.gms.internal;

import java.util.RandomAccess;
import java.util.List;

public interface zzgjf<E> extends List<E>, RandomAccess
{
    zzgjf<E> zza(final int p0);
    
    boolean zza();
}
