package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzacb extends zzey implements zzabz
{
    zzacb(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.reward.client.IRewardItem");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final int zzb() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final int int1 = zza.readInt();
        zza.recycle();
        return int1;
    }
}
