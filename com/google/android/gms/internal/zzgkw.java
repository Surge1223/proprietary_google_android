package com.google.android.gms.internal;

final class zzgkw
{
    private final String zza;
    private int zzb;
    
    zzgkw(final String zza) {
        this.zza = zza;
        this.zzb = 0;
    }
    
    final boolean zza() {
        return this.zzb < this.zza.length();
    }
    
    final int zzb() {
        final char char1 = this.zza.charAt(this.zzb++);
        if (char1 < '\ud800') {
            return char1;
        }
        int n = char1 & '\u1fff';
        int n2 = 13;
        char char2;
        while (true) {
            char2 = this.zza.charAt(this.zzb++);
            if (char2 < '\ud800') {
                break;
            }
            n |= (char2 & '\u1fff') << n2;
            n2 += 13;
        }
        return n | char2 << n2;
    }
}
