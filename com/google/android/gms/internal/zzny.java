package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.net.Uri;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzny extends zzez implements zznx
{
    public zzny() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.formats.client.INativeAdImage");
    }
    
    public static zznx zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
        if (queryLocalInterface instanceof zznx) {
            return (zznx)queryLocalInterface;
        }
        return new zznz(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 3: {
                final double zzc = this.zzc();
                parcel2.writeNoException();
                parcel2.writeDouble(zzc);
                break;
            }
            case 2: {
                final Uri zzb = this.zzb();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzb);
                break;
            }
            case 1: {
                final IObjectWrapper zza = this.zza();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zza);
                break;
            }
        }
        return true;
    }
}
