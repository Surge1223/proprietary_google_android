package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzob extends IInterface
{
    IObjectWrapper zza(final String p0) throws RemoteException;
    
    void zza() throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final int p1) throws RemoteException;
    
    void zza(final String p0, final IObjectWrapper p1) throws RemoteException;
}
