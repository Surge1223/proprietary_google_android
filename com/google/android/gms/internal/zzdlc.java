package com.google.android.gms.internal;

public final class zzdlc extends RuntimeException
{
    zzdlc(final String s, final Throwable t) {
        super(s, t);
    }
}
