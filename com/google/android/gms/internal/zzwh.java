package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzwh extends zzey implements zzwf
{
    zzwh(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.purchase.client.IInAppPurchaseManager");
    }
}
