package com.google.android.gms.internal;

import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.io.IOException;

final class zzgie implements zzgmk
{
    private final zzgic zza;
    
    private zzgie(final zzgic zzgic) {
        this.zza = zzgja.zza(zzgic, "output");
        this.zza.zza = this;
    }
    
    public static zzgie zza(final zzgic zzgic) {
        if (zzgic.zza != null) {
            return zzgic.zza;
        }
        return new zzgie(zzgic);
    }
    
    @Override
    public final int zza() {
        return zzgiw.zzg.zzk;
    }
    
    @Override
    public final void zza(final int n) {
        try {
            this.zza.zza(n, 3);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final double n2) {
        try {
            this.zza.zza(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final float n2) {
        try {
            this.zza.zza(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final int n2) {
        try {
            this.zza.zze(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final long n2) {
        try {
            this.zza.zza(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final zzgho zzgho) {
        try {
            this.zza.zza(n, zzgho);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final <K, V> void zza(final int n, final zzgjz<K, V> zzgjz, final Map<K, V> map) {
        try {
            for (final Map.Entry<K, V> entry : map.entrySet()) {
                this.zza.zza(n, 2);
                this.zza.zzb(zzgjy.zza(zzgjz, entry.getKey(), entry.getValue()));
                zzgjy.zza(this.zza, (zzgjz<K, V>)zzgjz, (K)entry.getKey(), (V)entry.getValue());
            }
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final Object o) {
        try {
            this.zza.zza(n, (zzgkg)o);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final String s) {
        try {
            this.zza.zza(n, s);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(final int n, final List<String> list) {
        try {
            final boolean b = list instanceof zzgjp;
            int i = 0;
            final int n2 = 0;
            if (b) {
                final zzgjp zzgjp = (zzgjp)list;
                for (int j = n2; j < list.size(); ++j) {
                    final Object zzb = zzgjp.zzb(j);
                    if (zzb instanceof String) {
                        this.zza.zza(n, (String)zzb);
                    }
                    else {
                        this.zza.zza(n, (zzgho)zzb);
                    }
                }
                return;
            }
            while (i < list.size()) {
                this.zza.zza(n, list.get(i));
                ++i;
            }
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zza(int i, final List<Integer> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzf(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzb(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zza(final int n, final boolean b) {
        try {
            this.zza.zza(n, b);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzb(final int n) {
        try {
            this.zza.zza(n, 4);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzb(final int n, final int n2) {
        try {
            this.zza.zzb(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzb(final int n, final long n2) {
        try {
            this.zza.zzc(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzb(final int n, final Object o) {
        try {
            this.zza.zze(n, (zzgkg)o);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzb(final int n, final List<zzgho> list) {
        int i = 0;
        try {
            while (i < list.size()) {
                this.zza.zza(n, list.get(i));
                ++i;
            }
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzb(int i, final List<Integer> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzi(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzd(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zze(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzc(final int n, final int n2) {
        try {
            this.zza.zzb(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzc(final int n, final long n2) {
        try {
            this.zza.zza(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzc(final int n, final Object o) {
        try {
            if (o instanceof zzgho) {
                this.zza.zzb(n, (zzgho)o);
                return;
            }
            this.zza.zzb(n, (zzgkg)o);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzc(final int n, final List<?> list) {
        for (int i = 0; i < list.size(); ++i) {
            this.zza(n, list.get(i));
        }
    }
    
    @Override
    public final void zzc(int i, final List<Long> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzd(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zza(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzd(final int n, final int n2) {
        try {
            this.zza.zze(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzd(final int n, final long n2) {
        try {
            this.zza.zzc(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzd(final int n, final List<?> list) {
        for (int i = 0; i < list.size(); ++i) {
            this.zzb(n, list.get(i));
        }
    }
    
    @Override
    public final void zzd(int i, final List<Long> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                for (int k = i = 0; k < list.size(); ++k) {
                    i += zzgic.zze(list.get(k));
                }
                this.zza.zzb(i);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zza(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zze(final int n, final int n2) {
        try {
            this.zza.zzc(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zze(final int n, final long n2) {
        try {
            this.zza.zzb(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zze(int i, final List<Long> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzg(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzc(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzc(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzf(final int n, final int n2) {
        try {
            this.zza.zzd(n, n2);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public final void zzf(int i, final List<Float> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzb(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zza(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzg(int i, final List<Double> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzb(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zza(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzh(int i, final List<Integer> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                for (int k = i = 0; k < list.size(); ++k) {
                    i += zzgic.zzk(list.get(k));
                }
                this.zza.zzb(i);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzb(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzi(int i, final List<Boolean> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                for (int k = i = 0; k < list.size(); ++k) {
                    i += zzgic.zzb(list.get(k));
                }
                this.zza.zzb(i);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zza(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zza(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzj(int i, final List<Integer> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzg(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzb(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzc(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzk(int i, final List<Integer> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzj(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzd(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zze(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzl(int i, final List<Long> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzh(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzc(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzc(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzm(int i, final List<Integer> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                for (int k = i = 0; k < list.size(); ++k) {
                    i += zzgic.zzh(list.get(k));
                }
                this.zza.zzb(i);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzc(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzd(i, list.get(j));
            ++j;
        }
    }
    
    @Override
    public final void zzn(int i, final List<Long> list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            try {
                this.zza.zza(i, 2);
                int n2;
                for (i = (n2 = 0); i < list.size(); ++i) {
                    n2 += zzgic.zzf(list.get(i));
                }
                this.zza.zzb(n2);
                for (i = n; i < list.size(); ++i) {
                    this.zza.zzb(list.get(i));
                }
                return;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        while (j < list.size()) {
            this.zza.zzb(i, list.get(j));
            ++j;
        }
    }
}
