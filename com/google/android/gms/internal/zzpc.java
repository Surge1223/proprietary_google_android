package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzpc extends zzey implements zzpa
{
    zzpc(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IOnContentAdLoadedListener");
    }
    
    @Override
    public final void zza(final zzop zzop) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzop);
        this.zzb(1, a_);
    }
}
