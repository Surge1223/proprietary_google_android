package com.google.android.gms.internal;

import java.util.Map;

final class zzgjl<K> implements Entry<K, Object>
{
    private Entry<K, zzgjj> zza;
    
    private zzgjl(final Entry<K, zzgjj> zza) {
        this.zza = zza;
    }
    
    @Override
    public final K getKey() {
        return this.zza.getKey();
    }
    
    @Override
    public final Object getValue() {
        if (this.zza.getValue() == null) {
            return null;
        }
        return zzgjj.zza();
    }
    
    @Override
    public final Object setValue(final Object o) {
        if (o instanceof zzgkg) {
            return this.zza.getValue().zza((zzgkg)o);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
    
    public final zzgjj zza() {
        return this.zza.getValue();
    }
}
