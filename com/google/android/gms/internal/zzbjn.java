package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzau;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;

public final class zzbjn implements ThreadFactory
{
    private final String zza;
    private final int zzb;
    private final AtomicInteger zzc;
    private final ThreadFactory zzd;
    
    public zzbjn(final String s) {
        this(s, 0);
    }
    
    private zzbjn(final String s, final int n) {
        this.zzc = new AtomicInteger();
        this.zzd = Executors.defaultThreadFactory();
        this.zza = zzau.zza(s, (Object)"Name must not be null");
        this.zzb = 0;
    }
    
    @Override
    public final Thread newThread(final Runnable runnable) {
        final Thread thread = this.zzd.newThread(new zzbjo(runnable, 0));
        final String zza = this.zza;
        final int andIncrement = this.zzc.getAndIncrement();
        final StringBuilder sb = new StringBuilder(13 + String.valueOf(zza).length());
        sb.append(zza);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        thread.setName(sb.toString());
        return thread;
    }
}
