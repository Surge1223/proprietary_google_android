package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzav;
import android.os.Parcelable.Creator;

public final class zzegt extends zzbid
{
    public static final Parcelable.Creator<zzegt> CREATOR;
    private final int zza;
    private final zzav zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzegu();
    }
    
    zzegt(final int zza, final zzav zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public zzegt(final zzav zzav) {
        this(1, zzav);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, zza);
    }
}
