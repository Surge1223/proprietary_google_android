package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzmg extends IInterface
{
    void zza(final zzmd p0) throws RemoteException;
}
