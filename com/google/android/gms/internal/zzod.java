package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;

public final class zzod extends zzey implements zzob
{
    zzod(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
    }
    
    @Override
    public final IObjectWrapper zza(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        final Parcel zza = this.zza(2, a_);
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(4, this.a_());
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeInt(n);
        this.zzb(5, a_);
    }
    
    @Override
    public final void zza(final String s, final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(1, a_);
    }
}
