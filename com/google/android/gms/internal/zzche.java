package com.google.android.gms.internal;

import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.internal.zzau;

public final class zzche
{
    private long zza;
    private long zzb;
    
    public zzche() {
        this(-1L);
    }
    
    private zzche(final long n) {
        this.zza = -1L;
        this.zzb = -1L;
    }
    
    private final long zzc() {
        if (this.zza == -1L) {
            return System.nanoTime();
        }
        try {
            return this.zza;
        }
        finally {
            this.zza = -1L;
        }
    }
    
    public final zzche zza() {
        this.zzb = this.zzc();
        return this;
    }
    
    public final long zzb() {
        zzau.zzb(this.zzb != -1L);
        return TimeUnit.NANOSECONDS.toMillis(this.zzc() - this.zzb);
    }
}
