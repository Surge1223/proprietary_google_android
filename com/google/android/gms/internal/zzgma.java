package com.google.android.gms.internal;

import java.nio.ByteBuffer;

final class zzgma extends zzglz
{
    @Override
    final int zza(int i, final byte[] array, int n, final int n2) {
        while (n < n2 && array[n] >= 0) {
            ++n;
        }
        if ((i = n) >= n2) {
            return 0;
        }
        while (i < n2) {
            n = i + 1;
            i = array[i];
            if (i < 0) {
                if (i < -32) {
                    if (n >= n2) {
                        return i;
                    }
                    if (i >= -62) {
                        i = n + 1;
                        if (array[n] <= -65) {
                            continue;
                        }
                    }
                    return -1;
                }
                else if (i < -16) {
                    if (n >= n2 - 1) {
                        return zzc(array, n, n2);
                    }
                    final int n3 = n + 1;
                    n = array[n];
                    if (n <= -65 && (i != -32 || n >= -96) && (i != -19 || n < -96)) {
                        i = n3 + 1;
                        if (array[n3] <= -65) {
                            continue;
                        }
                    }
                    return -1;
                }
                else {
                    if (n >= n2 - 2) {
                        return zzc(array, n, n2);
                    }
                    final int n4 = n + 1;
                    n = array[n];
                    if (n <= -65 && (i << 28) + (n + 112) >> 30 == 0) {
                        i = n4 + 1;
                        if (array[n4] <= -65) {
                            if (array[i] <= -65) {
                                ++i;
                                continue;
                            }
                        }
                    }
                    return -1;
                }
            }
            else {
                i = n;
            }
        }
        return 0;
    }
    
    @Override
    final int zza(final CharSequence charSequence, final byte[] array, int i, int j) {
        final int length = charSequence.length();
        final int n = j + i;
        int n2;
        char char1;
        for (j = 0; j < length; ++j) {
            n2 = j + i;
            if (n2 >= n) {
                break;
            }
            char1 = charSequence.charAt(j);
            if (char1 >= '\u0080') {
                break;
            }
            array[n2] = (byte)char1;
        }
        if (j == length) {
            return i + length;
        }
        int n3 = i + j;
        char char2;
        int n4;
        int n5;
        char char3;
        int n6;
        int n7;
        int n8;
        int n9;
        StringBuilder sb;
        for (i = j; i < length; ++i, n3 = j) {
            char2 = charSequence.charAt(i);
            if (char2 < '\u0080' && n3 < n) {
                j = n3 + 1;
                array[n3] = (byte)char2;
            }
            else if (char2 < '\u0800' && n3 <= n - 2) {
                n4 = n3 + 1;
                array[n3] = (byte)('\u03c0' | char2 >>> 6);
                j = n4 + 1;
                array[n4] = (byte)((char2 & '?') | '\u0080');
            }
            else if ((char2 < '\ud800' || '\udfff' < char2) && n3 <= n - 3) {
                j = n3 + 1;
                array[n3] = (byte)('\u01e0' | char2 >>> 12);
                n5 = j + 1;
                array[j] = (byte)((char2 >>> 6 & '?') | '\u0080');
                j = n5 + 1;
                array[n5] = (byte)((char2 & '?') | '\u0080');
            }
            else {
                if (n3 <= n - 4) {
                    j = i + 1;
                    if (j != charSequence.length()) {
                        char3 = charSequence.charAt(j);
                        if (Character.isSurrogatePair(char2, char3)) {
                            i = Character.toCodePoint(char2, char3);
                            n6 = n3 + 1;
                            array[n3] = (byte)(0xF0 | i >>> 18);
                            n7 = n6 + 1;
                            array[n6] = (byte)((i >>> 12 & 0x3F) | 0x80);
                            n8 = n7 + 1;
                            array[n7] = (byte)((i >>> 6 & 0x3F) | 0x80);
                            n9 = n8 + 1;
                            array[n8] = (byte)((i & 0x3F) | 0x80);
                            i = j;
                            j = n9;
                            continue;
                        }
                        i = j;
                    }
                    throw new zzgmb(i - 1, length);
                }
                if ('\ud800' <= char2 && char2 <= '\udfff') {
                    j = i + 1;
                    if (j == charSequence.length() || !Character.isSurrogatePair(char2, charSequence.charAt(j))) {
                        throw new zzgmb(i, length);
                    }
                }
                sb = new StringBuilder(37);
                sb.append("Failed writing ");
                sb.append(char2);
                sb.append(" at index ");
                sb.append(n3);
                throw new ArrayIndexOutOfBoundsException(sb.toString());
            }
        }
        return n3;
    }
    
    @Override
    final void zza(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        zzglz.zzb(charSequence, byteBuffer);
    }
}
