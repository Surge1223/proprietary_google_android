package com.google.android.gms.internal;

import libcore.io.Memory;
import java.util.logging.Level;
import java.security.PrivilegedExceptionAction;
import java.security.AccessController;
import java.nio.ByteBuffer;
import java.lang.reflect.Field;
import java.nio.ByteOrder;
import java.nio.Buffer;
import sun.misc.Unsafe;
import java.util.logging.Logger;

final class zzglw
{
    private static final Logger zza;
    private static final Unsafe zzb;
    private static final Class<?> zzc;
    private static final boolean zzd;
    private static final boolean zze;
    private static final zzd zzf;
    private static final boolean zzg;
    private static final boolean zzh;
    private static final long zzi;
    private static final long zzj;
    private static final long zzk;
    private static final long zzl;
    private static final long zzm;
    private static final long zzn;
    private static final long zzo;
    private static final long zzp;
    private static final long zzq;
    private static final long zzr;
    private static final long zzs;
    private static final long zzt;
    private static final long zzu;
    private static final long zzv;
    private static final boolean zzw;
    
    static {
        zza = Logger.getLogger(zzglw.class.getName());
        zzb = zzc();
        zzc = zzghg.zzb();
        zzd = zzc(Long.TYPE);
        zze = zzc(Integer.TYPE);
        final Unsafe zzb2 = zzglw.zzb;
        Object zzf2 = null;
        if (zzb2 != null) {
            if (zzghg.zza()) {
                if (zzglw.zzd) {
                    zzf2 = new zzb(zzglw.zzb);
                }
                else if (zzglw.zze) {
                    zzf2 = new zza(zzglw.zzb);
                }
            }
            else {
                zzf2 = new zzc(zzglw.zzb);
            }
        }
        zzf = (zzd)zzf2;
        zzg = zzg();
        zzh = zzf();
        zzi = zza(byte[].class);
        zzj = zza(boolean[].class);
        zzk = zzb(boolean[].class);
        zzl = zza(int[].class);
        zzm = zzb(int[].class);
        zzn = zza(long[].class);
        zzo = zzb(long[].class);
        zzp = zza(float[].class);
        zzq = zzb(float[].class);
        zzr = zza(double[].class);
        zzs = zzb(double[].class);
        zzt = zza(Object[].class);
        zzu = zzb(Object[].class);
        Field field = null;
        Label_0276: {
            if (zzghg.zza()) {
                field = zza(Buffer.class, "effectiveDirectAddress");
                if (field != null) {
                    break Label_0276;
                }
            }
            field = zza(Buffer.class, "address");
        }
        long zza2;
        if (field != null && zzglw.zzf != null) {
            zza2 = zzglw.zzf.zza(field);
        }
        else {
            zza2 = -1L;
        }
        zzv = zza2;
        zzw = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);
    }
    
    static byte zza(final byte[] array, final long n) {
        return zzglw.zzf.zza(array, zzglw.zzi + n);
    }
    
    private static int zza(final Class<?> clazz) {
        if (zzglw.zzh) {
            return zzglw.zzf.zza.arrayBaseOffset(clazz);
        }
        return -1;
    }
    
    static int zza(final Object o, final long n) {
        return zzglw.zzf.zze(o, n);
    }
    
    static long zza(final Field field) {
        return zzglw.zzf.zza(field);
    }
    
    static long zza(final ByteBuffer byteBuffer) {
        return zzglw.zzf.zzf(byteBuffer, zzglw.zzv);
    }
    
    private static Field zza(final Class<?> clazz, final String s) {
        Field declaredField;
        try {
            declaredField = clazz.getDeclaredField(s);
            declaredField.setAccessible(true);
        }
        catch (Throwable t) {
            declaredField = null;
        }
        return declaredField;
    }
    
    static void zza(final long n, final byte b) {
        zzglw.zzf.zza(n, b);
    }
    
    static void zza(final Object o, final long n, final double n2) {
        zzglw.zzf.zza(o, n, n2);
    }
    
    static void zza(final Object o, final long n, final float n2) {
        zzglw.zzf.zza(o, n, n2);
    }
    
    static void zza(final Object o, final long n, final int n2) {
        zzglw.zzf.zza(o, n, n2);
    }
    
    static void zza(final Object o, final long n, final long n2) {
        zzglw.zzf.zza(o, n, n2);
    }
    
    static void zza(final Object o, final long n, final Object o2) {
        zzglw.zzf.zza.putObject(o, n, o2);
    }
    
    static void zza(final Object o, final long n, final boolean b) {
        zzglw.zzf.zza(o, n, b);
    }
    
    static void zza(final byte[] array, final long n, final byte b) {
        zzglw.zzf.zza(array, zzglw.zzi + n, b);
    }
    
    static void zza(final byte[] array, final long n, final long n2, final long n3) {
        zzglw.zzf.zza(array, n, n2, n3);
    }
    
    static boolean zza() {
        return zzglw.zzh;
    }
    
    private static int zzb(final Class<?> clazz) {
        if (zzglw.zzh) {
            return zzglw.zzf.zza.arrayIndexScale(clazz);
        }
        return -1;
    }
    
    static long zzb(final Object o, final long n) {
        return zzglw.zzf.zzf(o, n);
    }
    
    static boolean zzb() {
        return zzglw.zzg;
    }
    
    static Unsafe zzc() {
        Unsafe unsafe;
        try {
            unsafe = AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)new zzglx());
        }
        catch (Throwable t) {
            unsafe = null;
        }
        return unsafe;
    }
    
    private static void zzc(final Object o, final long n, final byte b) {
        final long n2 = 0xFFFFFFFFFFFFFFFCL & n;
        final int zza = zza(o, n2);
        final int n3 = ((int)n & 0x3) << 3;
        zza(o, n2, (0xFF & b) << n3 | (zza & 255 << n3));
    }
    
    private static boolean zzc(final Class<?> clazz) {
        if (!zzghg.zza()) {
            return false;
        }
        try {
            final Class<?> zzc = zzglw.zzc;
            zzc.getMethod("peekLong", clazz, Boolean.TYPE);
            zzc.getMethod("pokeLong", clazz, Long.TYPE, Boolean.TYPE);
            zzc.getMethod("pokeInt", clazz, Integer.TYPE, Boolean.TYPE);
            zzc.getMethod("peekInt", clazz, Boolean.TYPE);
            zzc.getMethod("pokeByte", clazz, Byte.TYPE);
            zzc.getMethod("peekByte", clazz);
            zzc.getMethod("pokeByteArray", clazz, byte[].class, Integer.TYPE, Integer.TYPE);
            zzc.getMethod("peekByteArray", clazz, byte[].class, Integer.TYPE, Integer.TYPE);
            return true;
        }
        catch (Throwable t) {
            return false;
        }
    }
    
    static boolean zzc(final Object o, final long n) {
        return zzglw.zzf.zzb(o, n);
    }
    
    static float zzd(final Object o, final long n) {
        return zzglw.zzf.zzc(o, n);
    }
    
    private static void zzd(final Object o, final long n, final byte b) {
        final long n2 = 0xFFFFFFFFFFFFFFFCL & n;
        final int zza = zza(o, n2);
        final int n3 = ((int)n & 0x3) << 3;
        zza(o, n2, (0xFF & b) << n3 | (zza & 255 << n3));
    }
    
    private static void zzd(final Object o, final long n, final boolean b) {
        zzc(o, n, (byte)(b ? 1 : 0));
    }
    
    static double zze(final Object o, final long n) {
        return zzglw.zzf.zzd(o, n);
    }
    
    private static void zze(final Object o, final long n, final boolean b) {
        zzd(o, n, (byte)(b ? 1 : 0));
    }
    
    static Object zzf(final Object o, final long n) {
        return zzglw.zzf.zza.getObject(o, n);
    }
    
    private static boolean zzf() {
        if (zzglw.zzb == null) {
            return false;
        }
        try {
            final Class<? extends Unsafe> class1 = zzglw.zzb.getClass();
            class1.getMethod("objectFieldOffset", Field.class);
            class1.getMethod("arrayBaseOffset", Class.class);
            class1.getMethod("arrayIndexScale", Class.class);
            class1.getMethod("getInt", Object.class, Long.TYPE);
            class1.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            class1.getMethod("getLong", Object.class, Long.TYPE);
            class1.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            class1.getMethod("getObject", Object.class, Long.TYPE);
            class1.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (zzghg.zza()) {
                return true;
            }
            class1.getMethod("getByte", Object.class, Long.TYPE);
            class1.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            class1.getMethod("getBoolean", Object.class, Long.TYPE);
            class1.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            class1.getMethod("getFloat", Object.class, Long.TYPE);
            class1.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            class1.getMethod("getDouble", Object.class, Long.TYPE);
            class1.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        }
        catch (Throwable t) {
            final Logger zza = zzglw.zza;
            final Level warning = Level.WARNING;
            final String value = String.valueOf(t);
            final StringBuilder sb = new StringBuilder(71 + String.valueOf(value).length());
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(value);
            zza.logp(warning, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }
    
    private static boolean zzg() {
        if (zzglw.zzb == null) {
            return false;
        }
        try {
            final Class<? extends Unsafe> class1 = zzglw.zzb.getClass();
            class1.getMethod("objectFieldOffset", Field.class);
            class1.getMethod("getLong", Object.class, Long.TYPE);
            if (zzghg.zza()) {
                return true;
            }
            class1.getMethod("getByte", Long.TYPE);
            class1.getMethod("putByte", Long.TYPE, Byte.TYPE);
            class1.getMethod("getInt", Long.TYPE);
            class1.getMethod("putInt", Long.TYPE, Integer.TYPE);
            class1.getMethod("getLong", Long.TYPE);
            class1.getMethod("putLong", Long.TYPE, Long.TYPE);
            class1.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            class1.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        }
        catch (Throwable t) {
            final Logger zza = zzglw.zza;
            final Level warning = Level.WARNING;
            final String value = String.valueOf(t);
            final StringBuilder sb = new StringBuilder(71 + String.valueOf(value).length());
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(value);
            zza.logp(warning, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }
    
    private static byte zzk(final Object o, final long n) {
        return (byte)(zza(o, 0xFFFFFFFFFFFFFFFCL & n) >>> (int)((n & 0x3L) << 3));
    }
    
    private static byte zzl(final Object o, final long n) {
        return (byte)(zza(o, 0xFFFFFFFFFFFFFFFCL & n) >>> (int)((n & 0x3L) << 3));
    }
    
    private static boolean zzm(final Object o, final long n) {
        return zzk(o, n) != 0;
    }
    
    private static boolean zzn(final Object o, final long n) {
        return zzl(o, n) != 0;
    }
    
    static final class zza extends zzd
    {
        zza(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public final byte zza(final Object o, final long n) {
            if (zzglw.zzw) {
                return zzk(o, n);
            }
            return zzl(o, n);
        }
        
        @Override
        public final void zza(final long n, final byte b) {
            Memory.pokeByte((int)(n & -1L), b);
        }
        
        @Override
        public final void zza(final Object o, final long n, final byte b) {
            if (zzglw.zzw) {
                zzc(o, n, b);
                return;
            }
            zzd(o, n, b);
        }
        
        @Override
        public final void zza(final Object o, final long n, final double n2) {
            ((zzd)this).zza(o, n, Double.doubleToLongBits(n2));
        }
        
        @Override
        public final void zza(final Object o, final long n, final float n2) {
            ((zzd)this).zza(o, n, Float.floatToIntBits(n2));
        }
        
        @Override
        public final void zza(final Object o, final long n, final boolean b) {
            if (zzglw.zzw) {
                zzd(o, n, b);
                return;
            }
            zze(o, n, b);
        }
        
        @Override
        public final void zza(final byte[] array, final long n, final long n2, final long n3) {
            Memory.pokeByteArray((int)(n2 & -1L), array, (int)n, (int)n3);
        }
        
        @Override
        public final boolean zzb(final Object o, final long n) {
            if (zzglw.zzw) {
                return zzm(o, n);
            }
            return zzn(o, n);
        }
        
        @Override
        public final float zzc(final Object o, final long n) {
            return Float.intBitsToFloat(((zzd)this).zze(o, n));
        }
        
        @Override
        public final double zzd(final Object o, final long n) {
            return Double.longBitsToDouble(((zzd)this).zzf(o, n));
        }
    }
    
    static final class zzb extends zzd
    {
        zzb(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public final byte zza(final Object o, final long n) {
            if (zzglw.zzw) {
                return zzk(o, n);
            }
            return zzl(o, n);
        }
        
        @Override
        public final void zza(final long n, final byte b) {
            Memory.pokeByte(n, b);
        }
        
        @Override
        public final void zza(final Object o, final long n, final byte b) {
            if (zzglw.zzw) {
                zzc(o, n, b);
                return;
            }
            zzd(o, n, b);
        }
        
        @Override
        public final void zza(final Object o, final long n, final double n2) {
            ((zzd)this).zza(o, n, Double.doubleToLongBits(n2));
        }
        
        @Override
        public final void zza(final Object o, final long n, final float n2) {
            ((zzd)this).zza(o, n, Float.floatToIntBits(n2));
        }
        
        @Override
        public final void zza(final Object o, final long n, final boolean b) {
            if (zzglw.zzw) {
                zzd(o, n, b);
                return;
            }
            zze(o, n, b);
        }
        
        @Override
        public final void zza(final byte[] array, final long n, final long n2, final long n3) {
            Memory.pokeByteArray(n2, array, (int)n, (int)n3);
        }
        
        @Override
        public final boolean zzb(final Object o, final long n) {
            if (zzglw.zzw) {
                return zzm(o, n);
            }
            return zzn(o, n);
        }
        
        @Override
        public final float zzc(final Object o, final long n) {
            return Float.intBitsToFloat(((zzd)this).zze(o, n));
        }
        
        @Override
        public final double zzd(final Object o, final long n) {
            return Double.longBitsToDouble(((zzd)this).zzf(o, n));
        }
    }
    
    static final class zzc extends zzd
    {
        zzc(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public final byte zza(final Object o, final long n) {
            return this.zza.getByte(o, n);
        }
        
        @Override
        public final void zza(final long n, final byte b) {
            this.zza.putByte(n, b);
        }
        
        @Override
        public final void zza(final Object o, final long n, final byte b) {
            this.zza.putByte(o, n, b);
        }
        
        @Override
        public final void zza(final Object o, final long n, final double n2) {
            this.zza.putDouble(o, n, n2);
        }
        
        @Override
        public final void zza(final Object o, final long n, final float n2) {
            this.zza.putFloat(o, n, n2);
        }
        
        @Override
        public final void zza(final Object o, final long n, final boolean b) {
            this.zza.putBoolean(o, n, b);
        }
        
        @Override
        public final void zza(final byte[] array, final long n, final long n2, final long n3) {
            this.zza.copyMemory(array, zzglw.zzi + n, null, n2, n3);
        }
        
        @Override
        public final boolean zzb(final Object o, final long n) {
            return this.zza.getBoolean(o, n);
        }
        
        @Override
        public final float zzc(final Object o, final long n) {
            return this.zza.getFloat(o, n);
        }
        
        @Override
        public final double zzd(final Object o, final long n) {
            return this.zza.getDouble(o, n);
        }
    }
    
    abstract static class zzd
    {
        Unsafe zza;
        
        zzd(final Unsafe zza) {
            this.zza = zza;
        }
        
        public abstract byte zza(final Object p0, final long p1);
        
        public final long zza(final Field field) {
            return this.zza.objectFieldOffset(field);
        }
        
        public abstract void zza(final long p0, final byte p1);
        
        public abstract void zza(final Object p0, final long p1, final byte p2);
        
        public abstract void zza(final Object p0, final long p1, final double p2);
        
        public abstract void zza(final Object p0, final long p1, final float p2);
        
        public final void zza(final Object o, final long n, final int n2) {
            this.zza.putInt(o, n, n2);
        }
        
        public final void zza(final Object o, final long n, final long n2) {
            this.zza.putLong(o, n, n2);
        }
        
        public abstract void zza(final Object p0, final long p1, final boolean p2);
        
        public abstract void zza(final byte[] p0, final long p1, final long p2, final long p3);
        
        public abstract boolean zzb(final Object p0, final long p1);
        
        public abstract float zzc(final Object p0, final long p1);
        
        public abstract double zzd(final Object p0, final long p1);
        
        public final int zze(final Object o, final long n) {
            return this.zza.getInt(o, n);
        }
        
        public final long zzf(final Object o, final long n) {
            return this.zza.getLong(o, n);
        }
    }
}
