package com.google.android.gms.internal;

import java.util.RandomAccess;
import java.util.List;
import java.util.Collection;
import java.util.AbstractList;

abstract class zzghf<E> extends AbstractList<E> implements zzgjf<E>
{
    private boolean zza;
    
    zzghf() {
        this.zza = true;
    }
    
    @Override
    public void add(final int n, final E e) {
        this.zzc();
        super.add(n, e);
    }
    
    @Override
    public boolean add(final E e) {
        this.zzc();
        return super.add(e);
    }
    
    @Override
    public boolean addAll(final int n, final Collection<? extends E> collection) {
        this.zzc();
        return super.addAll(n, collection);
    }
    
    @Override
    public boolean addAll(final Collection<? extends E> collection) {
        this.zzc();
        return super.addAll(collection);
    }
    
    @Override
    public void clear() {
        this.zzc();
        super.clear();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof List)) {
            return false;
        }
        if (!(o instanceof RandomAccess)) {
            return super.equals(o);
        }
        final List list = (List)o;
        final int size = this.size();
        if (size != list.size()) {
            return false;
        }
        for (int i = 0; i < size; ++i) {
            if (!this.get(i).equals(list.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final int size = this.size();
        int n = 1;
        for (int i = 0; i < size; ++i) {
            n = n * 31 + this.get(i).hashCode();
        }
        return n;
    }
    
    @Override
    public E remove(final int n) {
        this.zzc();
        return super.remove(n);
    }
    
    @Override
    public boolean remove(final Object o) {
        this.zzc();
        return super.remove(o);
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        this.zzc();
        return super.removeAll(collection);
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        this.zzc();
        return super.retainAll(collection);
    }
    
    @Override
    public E set(final int n, final E e) {
        this.zzc();
        return super.set(n, e);
    }
    
    @Override
    public boolean zza() {
        return this.zza;
    }
    
    public final void zzb() {
        this.zza = false;
    }
    
    protected final void zzc() {
        if (this.zza) {
            return;
        }
        throw new UnsupportedOperationException();
    }
}
