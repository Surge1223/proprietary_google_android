package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbb extends zzgmo<zzbb>
{
    public String zza;
    public Long zzaa;
    public Long zzab;
    public String zzac;
    public String zzad;
    public Integer zzae;
    public Integer zzaf;
    public Long zzag;
    public Long zzah;
    public Long zzai;
    public Integer zzaj;
    public zzbc zzak;
    public zzbc[] zzal;
    public zzbd zzam;
    public Long zzan;
    public Long zzao;
    public Long zzap;
    public Long zzaq;
    public Long zzar;
    public String zzas;
    public Integer zzat;
    public Boolean zzau;
    public Long zzav;
    public zzbh zzaw;
    private Long zzaz;
    public String zzb;
    private Long zzba;
    private Long zzbb;
    private Long zzbc;
    private Long zzbd;
    private Long zzbe;
    private String zzbf;
    private Long zzbg;
    private Long zzbh;
    private zzbe zzbi;
    private Long zzbj;
    private Long zzbk;
    private Long zzbl;
    private Long zzbm;
    private Long zzbn;
    private String zzbo;
    public Long zzc;
    public Long zzd;
    public Long zze;
    public Long zzf;
    public Long zzg;
    public Long zzh;
    public Long zzi;
    public Long zzj;
    public Long zzk;
    public Long zzl;
    public Long zzm;
    public String zzn;
    public String zzo;
    public Long zzp;
    public Long zzq;
    public Long zzr;
    public String zzs;
    public Long zzt;
    public Long zzu;
    public Long zzv;
    public Long zzw;
    public Long zzx;
    public Long zzy;
    public Long zzz;
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza != null) {
            computeSerializedSize = n + zzgmm.zzb(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzb != null) {
            n2 = computeSerializedSize + zzgmm.zzb(2, this.zzb);
        }
        int n3 = n2;
        if (this.zzc != null) {
            n3 = n2 + zzgmm.zzf(3, this.zzc);
        }
        int n4 = n3;
        if (this.zzaz != null) {
            n4 = n3 + zzgmm.zzf(4, this.zzaz);
        }
        int n5 = n4;
        if (this.zzd != null) {
            n5 = n4 + zzgmm.zzf(5, this.zzd);
        }
        int n6 = n5;
        if (this.zze != null) {
            n6 = n5 + zzgmm.zzf(6, this.zze);
        }
        int n7 = n6;
        if (this.zzba != null) {
            n7 = n6 + zzgmm.zzf(7, this.zzba);
        }
        int n8 = n7;
        if (this.zzbb != null) {
            n8 = n7 + zzgmm.zzf(8, this.zzbb);
        }
        int n9 = n8;
        if (this.zzbc != null) {
            n9 = n8 + zzgmm.zzf(9, this.zzbc);
        }
        int n10 = n9;
        if (this.zzbd != null) {
            n10 = n9 + zzgmm.zzf(10, this.zzbd);
        }
        int n11 = n10;
        if (this.zzbe != null) {
            n11 = n10 + zzgmm.zzf(11, this.zzbe);
        }
        int n12 = n11;
        if (this.zzf != null) {
            n12 = n11 + zzgmm.zzf(12, this.zzf);
        }
        int n13 = n12;
        if (this.zzbf != null) {
            n13 = n12 + zzgmm.zzb(13, this.zzbf);
        }
        int n14 = n13;
        if (this.zzg != null) {
            n14 = n13 + zzgmm.zzf(14, this.zzg);
        }
        int n15 = n14;
        if (this.zzh != null) {
            n15 = n14 + zzgmm.zzf(15, this.zzh);
        }
        int n16 = n15;
        if (this.zzi != null) {
            n16 = n15 + zzgmm.zzf(16, this.zzi);
        }
        int n17 = n16;
        if (this.zzj != null) {
            n17 = n16 + zzgmm.zzf(17, this.zzj);
        }
        int n18 = n17;
        if (this.zzbg != null) {
            n18 = n17 + zzgmm.zzf(18, this.zzbg);
        }
        int n19 = n18;
        if (this.zzbh != null) {
            n19 = n18 + zzgmm.zzf(19, this.zzbh);
        }
        int n20 = n19;
        if (this.zzk != null) {
            n20 = n19 + zzgmm.zzf(20, this.zzk);
        }
        int n21 = n20;
        if (this.zzbn != null) {
            n21 = n20 + zzgmm.zzf(21, this.zzbn);
        }
        int n22 = n21;
        if (this.zzl != null) {
            n22 = n21 + zzgmm.zzf(22, this.zzl);
        }
        int n23 = n22;
        if (this.zzm != null) {
            n23 = n22 + zzgmm.zzf(23, this.zzm);
        }
        int n24 = n23;
        if (this.zzas != null) {
            n24 = n23 + zzgmm.zzb(24, this.zzas);
        }
        int n25 = n24;
        if (this.zzav != null) {
            n25 = n24 + zzgmm.zzf(25, this.zzav);
        }
        int n26 = n25;
        if (this.zzat != null) {
            n26 = n25 + zzgmm.zzb(26, this.zzat);
        }
        int n27 = n26;
        if (this.zzn != null) {
            n27 = n26 + zzgmm.zzb(27, this.zzn);
        }
        int n28 = n27;
        if (this.zzau != null) {
            this.zzau;
            n28 = n27 + (zzgmm.zzb(28) + 1);
        }
        int n29 = n28;
        if (this.zzo != null) {
            n29 = n28 + zzgmm.zzb(29, this.zzo);
        }
        int n30 = n29;
        if (this.zzbo != null) {
            n30 = n29 + zzgmm.zzb(30, this.zzbo);
        }
        int n31 = n30;
        if (this.zzp != null) {
            n31 = n30 + zzgmm.zzf(31, this.zzp);
        }
        int n32 = n31;
        if (this.zzq != null) {
            n32 = n31 + zzgmm.zzf(32, this.zzq);
        }
        int n33 = n32;
        if (this.zzr != null) {
            n33 = n32 + zzgmm.zzf(33, this.zzr);
        }
        int n34 = n33;
        if (this.zzs != null) {
            n34 = n33 + zzgmm.zzb(34, this.zzs);
        }
        int n35 = n34;
        if (this.zzt != null) {
            n35 = n34 + zzgmm.zzf(35, this.zzt);
        }
        int n36 = n35;
        if (this.zzu != null) {
            n36 = n35 + zzgmm.zzf(36, this.zzu);
        }
        int n37 = n36;
        if (this.zzv != null) {
            n37 = n36 + zzgmm.zzf(37, this.zzv);
        }
        int n38 = n37;
        if (this.zzbi != null) {
            n38 = n37 + zzgmm.zzb(38, this.zzbi);
        }
        int n39 = n38;
        if (this.zzw != null) {
            n39 = n38 + zzgmm.zzf(39, this.zzw);
        }
        int n40 = n39;
        if (this.zzx != null) {
            n40 = n39 + zzgmm.zzf(40, this.zzx);
        }
        int n41 = n40;
        if (this.zzy != null) {
            n41 = n40 + zzgmm.zzf(41, this.zzy);
        }
        int n42 = n41;
        if (this.zzz != null) {
            n42 = n41 + zzgmm.zzf(42, this.zzz);
        }
        int n43 = n42;
        if (this.zzal != null) {
            n43 = n42;
            if (this.zzal.length > 0) {
                int n44 = 0;
                while (true) {
                    n43 = n42;
                    if (n44 >= this.zzal.length) {
                        break;
                    }
                    final zzbc zzbc = this.zzal[n44];
                    int n45 = n42;
                    if (zzbc != null) {
                        n45 = n42 + zzgmm.zzb(43, zzbc);
                    }
                    ++n44;
                    n42 = n45;
                }
            }
        }
        int n46 = n43;
        if (this.zzaa != null) {
            n46 = n43 + zzgmm.zzf(44, this.zzaa);
        }
        int n47 = n46;
        if (this.zzab != null) {
            n47 = n46 + zzgmm.zzf(45, this.zzab);
        }
        int n48 = n47;
        if (this.zzac != null) {
            n48 = n47 + zzgmm.zzb(46, this.zzac);
        }
        int n49 = n48;
        if (this.zzad != null) {
            n49 = n48 + zzgmm.zzb(47, this.zzad);
        }
        int n50 = n49;
        if (this.zzae != null) {
            n50 = n49 + zzgmm.zzb(48, this.zzae);
        }
        int n51 = n50;
        if (this.zzaf != null) {
            n51 = n50 + zzgmm.zzb(49, this.zzaf);
        }
        int n52 = n51;
        if (this.zzak != null) {
            n52 = n51 + zzgmm.zzb(50, this.zzak);
        }
        int n53 = n52;
        if (this.zzag != null) {
            n53 = n52 + zzgmm.zzf(51, this.zzag);
        }
        int n54 = n53;
        if (this.zzah != null) {
            n54 = n53 + zzgmm.zzf(52, this.zzah);
        }
        int n55 = n54;
        if (this.zzai != null) {
            n55 = n54 + zzgmm.zzf(53, this.zzai);
        }
        int n56 = n55;
        if (this.zzbj != null) {
            n56 = n55 + zzgmm.zzf(54, this.zzbj);
        }
        int n57 = n56;
        if (this.zzbk != null) {
            n57 = n56 + zzgmm.zzf(55, this.zzbk);
        }
        int n58 = n57;
        if (this.zzaj != null) {
            n58 = n57 + zzgmm.zzb(56, this.zzaj);
        }
        int n59 = n58;
        if (this.zzam != null) {
            n59 = n58 + zzgmm.zzb(57, this.zzam);
        }
        int n60 = n59;
        if (this.zzbl != null) {
            n60 = n59 + zzgmm.zzf(58, this.zzbl);
        }
        int n61 = n60;
        if (this.zzan != null) {
            n61 = n60 + zzgmm.zzf(59, this.zzan);
        }
        int n62 = n61;
        if (this.zzao != null) {
            n62 = n61 + zzgmm.zzf(60, this.zzao);
        }
        int n63 = n62;
        if (this.zzap != null) {
            n63 = n62 + zzgmm.zzf(61, this.zzap);
        }
        int n64 = n63;
        if (this.zzaq != null) {
            n64 = n63 + zzgmm.zzf(62, this.zzaq);
        }
        int n65 = n64;
        if (this.zzar != null) {
            n65 = n64 + zzgmm.zzf(63, this.zzar);
        }
        int n66 = n65;
        if (this.zzbm != null) {
            n66 = n65 + zzgmm.zzf(64, this.zzbm);
        }
        int n67 = n66;
        if (this.zzaw != null) {
            n67 = n66 + zzgmm.zzb(201, this.zzaw);
        }
        return n67;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza != null) {
            zzgmm.zza(1, this.zza);
        }
        if (this.zzb != null) {
            zzgmm.zza(2, this.zzb);
        }
        if (this.zzc != null) {
            zzgmm.zzb(3, this.zzc);
        }
        if (this.zzaz != null) {
            zzgmm.zzb(4, this.zzaz);
        }
        if (this.zzd != null) {
            zzgmm.zzb(5, this.zzd);
        }
        if (this.zze != null) {
            zzgmm.zzb(6, this.zze);
        }
        if (this.zzba != null) {
            zzgmm.zzb(7, this.zzba);
        }
        if (this.zzbb != null) {
            zzgmm.zzb(8, this.zzbb);
        }
        if (this.zzbc != null) {
            zzgmm.zzb(9, this.zzbc);
        }
        if (this.zzbd != null) {
            zzgmm.zzb(10, this.zzbd);
        }
        if (this.zzbe != null) {
            zzgmm.zzb(11, this.zzbe);
        }
        if (this.zzf != null) {
            zzgmm.zzb(12, this.zzf);
        }
        if (this.zzbf != null) {
            zzgmm.zza(13, this.zzbf);
        }
        if (this.zzg != null) {
            zzgmm.zzb(14, this.zzg);
        }
        if (this.zzh != null) {
            zzgmm.zzb(15, this.zzh);
        }
        if (this.zzi != null) {
            zzgmm.zzb(16, this.zzi);
        }
        if (this.zzj != null) {
            zzgmm.zzb(17, this.zzj);
        }
        if (this.zzbg != null) {
            zzgmm.zzb(18, this.zzbg);
        }
        if (this.zzbh != null) {
            zzgmm.zzb(19, this.zzbh);
        }
        if (this.zzk != null) {
            zzgmm.zzb(20, this.zzk);
        }
        if (this.zzbn != null) {
            zzgmm.zzb(21, this.zzbn);
        }
        if (this.zzl != null) {
            zzgmm.zzb(22, this.zzl);
        }
        if (this.zzm != null) {
            zzgmm.zzb(23, this.zzm);
        }
        if (this.zzas != null) {
            zzgmm.zza(24, this.zzas);
        }
        if (this.zzav != null) {
            zzgmm.zzb(25, this.zzav);
        }
        if (this.zzat != null) {
            zzgmm.zza(26, this.zzat);
        }
        if (this.zzn != null) {
            zzgmm.zza(27, this.zzn);
        }
        if (this.zzau != null) {
            zzgmm.zza(28, this.zzau);
        }
        if (this.zzo != null) {
            zzgmm.zza(29, this.zzo);
        }
        if (this.zzbo != null) {
            zzgmm.zza(30, this.zzbo);
        }
        if (this.zzp != null) {
            zzgmm.zzb(31, this.zzp);
        }
        if (this.zzq != null) {
            zzgmm.zzb(32, this.zzq);
        }
        if (this.zzr != null) {
            zzgmm.zzb(33, this.zzr);
        }
        if (this.zzs != null) {
            zzgmm.zza(34, this.zzs);
        }
        if (this.zzt != null) {
            zzgmm.zzb(35, this.zzt);
        }
        if (this.zzu != null) {
            zzgmm.zzb(36, this.zzu);
        }
        if (this.zzv != null) {
            zzgmm.zzb(37, this.zzv);
        }
        if (this.zzbi != null) {
            zzgmm.zza(38, this.zzbi);
        }
        if (this.zzw != null) {
            zzgmm.zzb(39, this.zzw);
        }
        if (this.zzx != null) {
            zzgmm.zzb(40, this.zzx);
        }
        if (this.zzy != null) {
            zzgmm.zzb(41, this.zzy);
        }
        if (this.zzz != null) {
            zzgmm.zzb(42, this.zzz);
        }
        if (this.zzal != null && this.zzal.length > 0) {
            for (int i = 0; i < this.zzal.length; ++i) {
                final zzbc zzbc = this.zzal[i];
                if (zzbc != null) {
                    zzgmm.zza(43, zzbc);
                }
            }
        }
        if (this.zzaa != null) {
            zzgmm.zzb(44, this.zzaa);
        }
        if (this.zzab != null) {
            zzgmm.zzb(45, this.zzab);
        }
        if (this.zzac != null) {
            zzgmm.zza(46, this.zzac);
        }
        if (this.zzad != null) {
            zzgmm.zza(47, this.zzad);
        }
        if (this.zzae != null) {
            zzgmm.zza(48, this.zzae);
        }
        if (this.zzaf != null) {
            zzgmm.zza(49, this.zzaf);
        }
        if (this.zzak != null) {
            zzgmm.zza(50, this.zzak);
        }
        if (this.zzag != null) {
            zzgmm.zzb(51, this.zzag);
        }
        if (this.zzah != null) {
            zzgmm.zzb(52, this.zzah);
        }
        if (this.zzai != null) {
            zzgmm.zzb(53, this.zzai);
        }
        if (this.zzbj != null) {
            zzgmm.zzb(54, this.zzbj);
        }
        if (this.zzbk != null) {
            zzgmm.zzb(55, this.zzbk);
        }
        if (this.zzaj != null) {
            zzgmm.zza(56, this.zzaj);
        }
        if (this.zzam != null) {
            zzgmm.zza(57, this.zzam);
        }
        if (this.zzbl != null) {
            zzgmm.zzb(58, this.zzbl);
        }
        if (this.zzan != null) {
            zzgmm.zzb(59, this.zzan);
        }
        if (this.zzao != null) {
            zzgmm.zzb(60, this.zzao);
        }
        if (this.zzap != null) {
            zzgmm.zzb(61, this.zzap);
        }
        if (this.zzaq != null) {
            zzgmm.zzb(62, this.zzaq);
        }
        if (this.zzar != null) {
            zzgmm.zzb(63, this.zzar);
        }
        if (this.zzbm != null) {
            zzgmm.zzb(64, this.zzbm);
        }
        if (this.zzaw != null) {
            zzgmm.zza(201, this.zzaw);
        }
        super.writeTo(zzgmm);
    }
}
