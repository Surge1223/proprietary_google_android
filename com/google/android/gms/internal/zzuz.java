package com.google.android.gms.internal;

import java.util.Set;
import java.util.Date;
import java.util.Collection;
import java.util.HashSet;
import com.google.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.internal.client.zzf;
import com.google.ads.AdRequest;

public final class zzuz
{
    public static int zza(final AdRequest.ErrorCode errorCode) {
        switch (zzva.zza[errorCode.ordinal()]) {
            default: {
                return 0;
            }
            case 4: {
                return 3;
            }
            case 3: {
                return 2;
            }
            case 2: {
                return 1;
            }
        }
    }
    
    public static MediationAdRequest zza(final zzf zzf, final boolean b) {
        HashSet<String> set;
        if (zzf.zze != null) {
            set = new HashSet<String>(zzf.zze);
        }
        else {
            set = null;
        }
        final Date date = new Date(zzf.zzb);
        AdRequest.Gender gender = null;
        switch (zzf.zzd) {
            default: {
                gender = AdRequest.Gender.UNKNOWN;
                break;
            }
            case 2: {
                gender = AdRequest.Gender.FEMALE;
                break;
            }
            case 1: {
                gender = AdRequest.Gender.MALE;
                break;
            }
        }
        return new MediationAdRequest(date, gender, set, b, zzf.zzk);
    }
}
