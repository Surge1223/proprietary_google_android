package com.google.android.gms.internal;

import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.RandomAccess;
import java.util.AbstractList;

public final class zzglt extends AbstractList<String> implements zzgjp, RandomAccess
{
    private final zzgjp zza;
    
    @Override
    public final Iterator<String> iterator() {
        return new zzglv(this);
    }
    
    @Override
    public final ListIterator<String> listIterator(final int n) {
        return new zzglu(this, n);
    }
    
    @Override
    public final int size() {
        return this.zza.size();
    }
    
    @Override
    public final Object zzb(final int n) {
        return this.zza.zzb(n);
    }
    
    @Override
    public final List<?> zzd() {
        return this.zza.zzd();
    }
}
