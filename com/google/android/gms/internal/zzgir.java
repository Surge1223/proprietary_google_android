package com.google.android.gms.internal;

final class zzgir
{
    static {
        zzb = new int[zzgji.values().length];
        try {
            zzgir.zzb[zzgji.zzh.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            zzgir.zzb[zzgji.zzj.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError2) {}
        try {
            zzgir.zzb[zzgji.zzg.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError3) {}
        zza = new int[zzgis.values().length];
        try {
            zzgir.zza[zzgis.zzd.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError4) {}
        try {
            zzgir.zza[zzgis.zzb.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError5) {}
        try {
            zzgir.zza[zzgis.zza.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError6) {}
    }
}
