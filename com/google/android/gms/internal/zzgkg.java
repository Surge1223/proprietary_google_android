package com.google.android.gms.internal;

import java.io.IOException;

public interface zzgkg extends zzgki
{
    int zza();
    
    void zza(final zzgic p0) throws IOException;
    
    zzgho zzj();
    
    zzgkh zzp();
}
