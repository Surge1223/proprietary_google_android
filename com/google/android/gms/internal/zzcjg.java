package com.google.android.gms.internal;

import android.os.StrictMode$ThreadPolicy;
import android.os.StrictMode;
import java.util.concurrent.Callable;

public final class zzcjg
{
    public static <T> T zza(final Callable<T> callable) throws Exception {
        final StrictMode$ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        try {
            StrictMode.setThreadPolicy(StrictMode$ThreadPolicy.LAX);
            return callable.call();
        }
        finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }
}
