package com.google.android.gms.internal;

abstract class zzgjq
{
    private static final zzgjq zza;
    private static final zzgjq zzb;
    
    static {
        zza = new zzgjs(null);
        zzb = new zzgjt(null);
    }
    
    static zzgjq zza() {
        return zzgjq.zza;
    }
    
    static zzgjq zzb() {
        return zzgjq.zzb;
    }
    
    abstract <L> void zza(final Object p0, final Object p1, final long p2);
}
