package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Bundle;
import android.os.RemoteException;
import android.content.Intent;
import android.os.IInterface;

public interface zzvv extends IInterface
{
    void zza(final int p0, final int p1, final Intent p2) throws RemoteException;
    
    void zza(final Bundle p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zzb(final Bundle p0) throws RemoteException;
    
    void zzd() throws RemoteException;
    
    boolean zze() throws RemoteException;
    
    void zzf() throws RemoteException;
    
    void zzg() throws RemoteException;
    
    void zzh() throws RemoteException;
    
    void zzi() throws RemoteException;
    
    void zzj() throws RemoteException;
    
    void zzk() throws RemoteException;
    
    void zzl() throws RemoteException;
}
