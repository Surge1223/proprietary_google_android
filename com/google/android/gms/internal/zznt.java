package com.google.android.gms.internal;

import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface zznt extends IInterface
{
    String zza() throws RemoteException;
    
    List<zznx> zzb() throws RemoteException;
}
