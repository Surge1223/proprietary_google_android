package com.google.android.gms.internal;

import java.util.NoSuchElementException;
import java.util.Iterator;

final class zzglg implements Iterator<Object>
{
    @Override
    public final boolean hasNext() {
        return false;
    }
    
    @Override
    public final Object next() {
        throw new NoSuchElementException();
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
