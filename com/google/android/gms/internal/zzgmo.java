package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzgmo<M extends zzgmo<M>> extends zzgmu
{
    protected zzgmq zzax;
    
    @Override
    public M clone() throws CloneNotSupportedException {
        final zzgmo zzgmo = (zzgmo)super.clone();
        zzgms.zza(this, zzgmo);
        return (M)zzgmo;
    }
    
    @Override
    protected int computeSerializedSize() {
        final zzgmq zzax = this.zzax;
        int n = 0;
        int n3;
        if (zzax != null) {
            int n2 = 0;
            while (true) {
                n3 = n2;
                if (n >= this.zzax.zza()) {
                    break;
                }
                n2 += this.zzax.zzc(n).zza();
                ++n;
            }
        }
        else {
            n3 = 0;
        }
        return n3;
    }
    
    @Override
    public void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zzax == null) {
            return;
        }
        for (int i = 0; i < this.zzax.zza(); ++i) {
            this.zzax.zzc(i).zza(zzgmm);
        }
    }
}
