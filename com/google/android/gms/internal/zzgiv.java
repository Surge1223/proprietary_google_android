package com.google.android.gms.internal;

final class zzgiv implements zzgkf
{
    private static final zzgiv zza;
    
    static {
        zza = new zzgiv();
    }
    
    public static zzgiv zza() {
        return zzgiv.zza;
    }
    
    @Override
    public final boolean zza(final Class<?> clazz) {
        return zzgiw.class.isAssignableFrom(clazz);
    }
    
    @Override
    public final zzgke zzb(final Class<?> clazz) {
        if (!zzgiw.class.isAssignableFrom(clazz)) {
            final String value = String.valueOf(clazz.getName());
            String concat;
            if (value.length() != 0) {
                concat = "Unsupported message type: ".concat(value);
            }
            else {
                concat = new String("Unsupported message type: ");
            }
            throw new IllegalArgumentException(concat);
        }
        try {
            return (zzgke)((zzgiw)zzgiw.zza(clazz.asSubclass(zzgiw.class))).zzb();
        }
        catch (Exception ex) {
            final String value2 = String.valueOf(clazz.getName());
            String concat2;
            if (value2.length() != 0) {
                concat2 = "Unable to get message info for ".concat(value2);
            }
            else {
                concat2 = new String("Unable to get message info for ");
            }
            throw new RuntimeException(concat2, ex);
        }
    }
}
