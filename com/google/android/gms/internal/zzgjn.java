package com.google.android.gms.internal;

public class zzgjn
{
    private static final zzgii zza;
    private zzgho zzb;
    private volatile zzgkg zzc;
    private volatile zzgho zzd;
    
    static {
        zza = zzgii.zza();
    }
    
    private final zzgkg zzb(final zzgkg zzgkg) {
        if (this.zzc == null) {
            synchronized (this) {
                if (this.zzc == null) {
                    try {
                        this.zzc = zzgkg;
                        this.zzd = zzgho.zza;
                    }
                    catch (zzgjg zzgjg) {
                        this.zzc = zzgkg;
                        this.zzd = zzgho.zza;
                    }
                }
            }
        }
        return this.zzc;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzgjn)) {
            return false;
        }
        final zzgjn zzgjn = (zzgjn)o;
        final zzgkg zzc = this.zzc;
        final zzgkg zzc2 = zzgjn.zzc;
        if (zzc == null && zzc2 == null) {
            return this.zzc().equals(zzgjn.zzc());
        }
        if (zzc != null && zzc2 != null) {
            return zzc.equals(zzc2);
        }
        if (zzc != null) {
            return zzc.equals(zzgjn.zzb(zzc.zzr()));
        }
        return this.zzb(zzc2.zzr()).equals(zzc2);
    }
    
    @Override
    public int hashCode() {
        return 1;
    }
    
    public final zzgkg zza(final zzgkg zzc) {
        final zzgkg zzc2 = this.zzc;
        this.zzb = null;
        this.zzd = null;
        this.zzc = zzc;
        return zzc2;
    }
    
    public final int zzb() {
        if (this.zzd != null) {
            return this.zzd.zza();
        }
        if (this.zzc != null) {
            return this.zzc.zza();
        }
        return 0;
    }
    
    public final zzgho zzc() {
        if (this.zzd != null) {
            return this.zzd;
        }
        synchronized (this) {
            if (this.zzd != null) {
                return this.zzd;
            }
            if (this.zzc == null) {
                this.zzd = zzgho.zza;
            }
            else {
                this.zzd = this.zzc.zzj();
            }
            return this.zzd;
        }
    }
}
