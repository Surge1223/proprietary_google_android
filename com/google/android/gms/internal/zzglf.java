package com.google.android.gms.internal;

import java.util.Iterator;

final class zzglf
{
    private static final Iterator<Object> zza;
    private static final Iterable<Object> zzb;
    
    static {
        zza = new zzglg();
        zzb = new zzglh();
    }
    
    static <T> Iterable<T> zza() {
        return (Iterable<T>)zzglf.zzb;
    }
}
