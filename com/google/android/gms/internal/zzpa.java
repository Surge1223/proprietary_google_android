package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzpa extends IInterface
{
    void zza(final zzop p0) throws RemoteException;
}
