package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzgl extends zzey implements zzgj
{
    zzgl(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final boolean zza(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, true);
        final Parcel zza = this.zza(2, a_);
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
}
