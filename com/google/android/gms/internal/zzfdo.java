package com.google.android.gms.internal;

import android.os.Build.VERSION;
import android.annotation.SuppressLint;
import android.content.Context;

public abstract class zzfdo<T>
{
    private static final Object zzb;
    @SuppressLint({ "StaticFieldLeak" })
    private static Context zzc;
    private static boolean zzd;
    private static volatile Boolean zze;
    private static volatile Boolean zzf;
    
    static {
        zzb = new Object();
        zzfdo.zzc = null;
        zzfdo.zzd = false;
        zzfdo.zze = null;
        zzfdo.zzf = null;
    }
    
    public static void zza(Context zzc) {
        synchronized (zzfdo.zzb) {
            if (Build.VERSION.SDK_INT < 24 || !zzc.isDeviceProtectedStorage()) {
                final Context applicationContext = zzc.getApplicationContext();
                if (applicationContext != null) {
                    zzc = applicationContext;
                }
            }
            if (zzfdo.zzc != zzc) {
                zzfdo.zze = null;
            }
            zzfdo.zzc = zzc;
            // monitorexit(zzfdo.zzb)
            zzfdo.zzd = false;
        }
    }
}
