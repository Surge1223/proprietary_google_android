package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public abstract class zzacd extends zzez implements zzacc
{
    public static zzacc zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
        if (queryLocalInterface instanceof zzacc) {
            return (zzacc)queryLocalInterface;
        }
        return new zzace(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        Label_0347: {
            if (n != 34) {
                switch (n) {
                    default: {
                        switch (n) {
                            default: {
                                return false;
                            }
                            case 13: {
                                this.zza(parcel.readString());
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 12: {
                                final String zzf = this.zzf();
                                parcel2.writeNoException();
                                parcel2.writeString(zzf);
                                break Label_0347;
                            }
                            case 11: {
                                this.zzc(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 10: {
                                this.zzb(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 9: {
                                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 8: {
                                this.zze();
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 7: {
                                this.zzd();
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 6: {
                                this.zzc();
                                parcel2.writeNoException();
                                break Label_0347;
                            }
                            case 5: {
                                final boolean zzb = this.zzb();
                                parcel2.writeNoException();
                                zzfa.zza(parcel2, zzb);
                                break Label_0347;
                            }
                        }
                        break;
                    }
                    case 3: {
                        final IBinder strongBinder = parcel.readStrongBinder();
                        zzach zzach;
                        if (strongBinder == null) {
                            zzach = null;
                        }
                        else {
                            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
                            if (queryLocalInterface instanceof zzach) {
                                zzach = (zzach)queryLocalInterface;
                            }
                            else {
                                zzach = new zzacj(strongBinder);
                            }
                        }
                        this.zza(zzach);
                        parcel2.writeNoException();
                        break;
                    }
                    case 2: {
                        this.zza();
                        parcel2.writeNoException();
                        break;
                    }
                    case 1: {
                        this.zza(zzfa.zza(parcel, zzacn.CREATOR));
                        parcel2.writeNoException();
                        break;
                    }
                }
            }
            else {
                this.zza(zzfa.zza(parcel));
                parcel2.writeNoException();
            }
        }
        return true;
    }
}
