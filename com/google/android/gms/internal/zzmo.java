package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.net.Uri;
import android.graphics.drawable.Drawable;

public final class zzmo extends zzny
{
    private final Drawable zza;
    private final Uri zzb;
    private final double zzc;
    
    public zzmo(final Drawable zza, final Uri zzb, final double zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public final IObjectWrapper zza() throws RemoteException {
        return zzn.zza(this.zza);
    }
    
    @Override
    public final Uri zzb() throws RemoteException {
        return this.zzb;
    }
    
    @Override
    public final double zzc() {
        return this.zzc;
    }
}
