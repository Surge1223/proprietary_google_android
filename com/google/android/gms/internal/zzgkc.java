package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzgkc implements zzgkb
{
    @Override
    public final int zza(final int n, final Object o, final Object o2) {
        final zzgka zzgka = (zzgka)o;
        if (zzgka.isEmpty()) {
            return 0;
        }
        final Iterator iterator = zzgka.entrySet().iterator();
        if (!iterator.hasNext()) {
            return 0;
        }
        final Map.Entry entry = iterator.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }
    
    @Override
    public final Object zza(final Object o, final Object o2) {
        final zzgka zzgka = (zzgka)o;
        final zzgka zzgka2 = (zzgka)o2;
        zzgka zzb = zzgka;
        if (!zzgka2.isEmpty()) {
            zzb = zzgka;
            if (!zzgka.zzd()) {
                zzb = zzgka.zzb();
            }
            zzb.zza(zzgka2);
        }
        return zzb;
    }
    
    @Override
    public final Map<?, ?> zzb(final Object o) {
        return (Map<?, ?>)o;
    }
    
    @Override
    public final zzgjz<?, ?> zzf(final Object o) {
        throw new NoSuchMethodError();
    }
}
