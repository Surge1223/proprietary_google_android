package com.google.android.gms.internal;

public interface zzgip<T extends zzgip<T>> extends Comparable<T>
{
    int zza();
    
    zzgkh zza(final zzgkh p0, final zzgkg p1);
    
    zzgkn zza(final zzgkn p0, final zzgkn p1);
    
    zzgme zzb();
    
    zzgmj zzc();
    
    boolean zzd();
    
    boolean zze();
}
