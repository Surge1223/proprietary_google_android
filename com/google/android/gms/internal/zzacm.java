package com.google.android.gms.internal;

import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public final class zzacm extends zzaci
{
    private final RewardedVideoAdListener zza;
    
    public zzacm(final RewardedVideoAdListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        if (this.zza != null) {
            this.zza.onRewardedVideoAdLoaded();
        }
    }
    
    @Override
    public final void zza(final int n) {
        if (this.zza != null) {
            this.zza.onRewardedVideoAdFailedToLoad(n);
        }
    }
    
    @Override
    public final void zza(final zzabz zzabz) {
        if (this.zza != null) {
            this.zza.onRewarded(new zzack(zzabz));
        }
    }
    
    @Override
    public final void zzb() {
        if (this.zza != null) {
            this.zza.onRewardedVideoAdOpened();
        }
    }
    
    @Override
    public final void zzc() {
        if (this.zza != null) {
            this.zza.onRewardedVideoStarted();
        }
    }
    
    @Override
    public final void zzd() {
        if (this.zza != null) {
            this.zza.onRewardedVideoAdClosed();
        }
    }
    
    @Override
    public final void zze() {
        if (this.zza != null) {
            this.zza.onRewardedVideoAdLeftApplication();
        }
    }
    
    @Override
    public final void zzf() {
        if (this.zza != null) {
            this.zza.onRewardedVideoCompleted();
        }
    }
}
