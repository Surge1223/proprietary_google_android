package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzglr
{
    private static final zzglr zza;
    private int zzb;
    private int[] zzc;
    private Object[] zzd;
    private int zze;
    private boolean zzf;
    
    static {
        zza = new zzglr(0, new int[0], new Object[0], false);
    }
    
    private zzglr() {
        this(0, new int[8], new Object[8], true);
    }
    
    private zzglr(final int zzb, final int[] zzc, final Object[] zzd, final boolean zzf) {
        this.zze = -1;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzf = zzf;
    }
    
    public static zzglr zza() {
        return zzglr.zza;
    }
    
    static zzglr zza(final zzglr zzglr, final zzglr zzglr2) {
        final int n = zzglr.zzb + zzglr2.zzb;
        final int[] copy = Arrays.copyOf(zzglr.zzc, n);
        System.arraycopy(zzglr2.zzc, 0, copy, zzglr.zzb, zzglr2.zzb);
        final Object[] copy2 = Arrays.copyOf(zzglr.zzd, n);
        System.arraycopy(zzglr2.zzd, 0, copy2, zzglr.zzb, zzglr2.zzb);
        return new zzglr(n, copy, copy2, true);
    }
    
    private static void zza(int n, final Object o, final zzgmk zzgmk) {
        final int n2 = n >>> 3;
        n &= 0x7;
        if (n == 5) {
            zzgmk.zzd(n2, (int)o);
            return;
        }
        switch (n) {
            default: {
                throw new RuntimeException(zzgjg.zzf());
            }
            case 3: {
                if (zzgmk.zza() == zzgiw.zzg.zzk) {
                    zzgmk.zza(n2);
                    ((zzglr)o).zzb(zzgmk);
                    zzgmk.zzb(n2);
                    return;
                }
                zzgmk.zzb(n2);
                ((zzglr)o).zzb(zzgmk);
                zzgmk.zza(n2);
            }
            case 2: {
                zzgmk.zza(n2, (zzgho)o);
            }
            case 1: {
                zzgmk.zzd(n2, (long)o);
            }
            case 0: {
                zzgmk.zza(n2, (long)o);
            }
        }
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof zzglr)) {
            return false;
        }
        final zzglr zzglr = (zzglr)o;
        if (this.zzb == zzglr.zzb) {
            final int[] zzc = this.zzc;
            final int[] zzc2 = zzglr.zzc;
            final int zzb = this.zzb;
            int i = 0;
            while (true) {
                while (i < zzb) {
                    if (zzc[i] != zzc2[i]) {
                        final boolean b = false;
                        if (b) {
                            final Object[] zzd = this.zzd;
                            final Object[] zzd2 = zzglr.zzd;
                            final int zzb2 = this.zzb;
                            int j = 0;
                            while (true) {
                                while (j < zzb2) {
                                    if (!zzd[j].equals(zzd2[j])) {
                                        final boolean b2 = false;
                                        if (!b2) {
                                            return false;
                                        }
                                        return true;
                                    }
                                    else {
                                        ++j;
                                    }
                                }
                                final boolean b2 = true;
                                continue;
                            }
                        }
                        return false;
                    }
                    else {
                        ++i;
                    }
                }
                final boolean b = true;
                continue;
            }
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        return ((527 + this.zzb) * 31 + Arrays.hashCode(this.zzc)) * 31 + Arrays.deepHashCode(this.zzd);
    }
    
    public final void zza(final zzgic zzgic) throws IOException {
        for (int i = 0; i < this.zzb; ++i) {
            final int n = this.zzc[i];
            final int n2 = n >>> 3;
            final int n3 = n & 0x7;
            if (n3 != 5) {
                switch (n3) {
                    default: {
                        throw zzgjg.zzf();
                    }
                    case 3: {
                        zzgic.zza(n2, 3);
                        ((zzglr)this.zzd[i]).zza(zzgic);
                        zzgic.zza(n2, 4);
                        break;
                    }
                    case 2: {
                        zzgic.zza(n2, (zzgho)this.zzd[i]);
                        break;
                    }
                    case 1: {
                        zzgic.zzc(n2, (long)this.zzd[i]);
                        break;
                    }
                    case 0: {
                        zzgic.zza(n2, (long)this.zzd[i]);
                        break;
                    }
                }
            }
            else {
                zzgic.zze(n2, (int)this.zzd[i]);
            }
        }
    }
    
    final void zza(final zzgmk zzgmk) {
        if (zzgmk.zza() == zzgiw.zzg.zzl) {
            for (int i = this.zzb - 1; i >= 0; --i) {
                zzgmk.zzc(this.zzc[i] >>> 3, this.zzd[i]);
            }
            return;
        }
        for (int j = 0; j < this.zzb; ++j) {
            zzgmk.zzc(this.zzc[j] >>> 3, this.zzd[j]);
        }
    }
    
    final void zza(final StringBuilder sb, final int n) {
        for (int i = 0; i < this.zzb; ++i) {
            zzgkj.zza(sb, n, String.valueOf(this.zzc[i] >>> 3), this.zzd[i]);
        }
    }
    
    public final void zzb(final zzgmk zzgmk) {
        if (this.zzb == 0) {
            return;
        }
        if (zzgmk.zza() == zzgiw.zzg.zzk) {
            for (int i = 0; i < this.zzb; ++i) {
                zza(this.zzc[i], this.zzd[i], zzgmk);
            }
            return;
        }
        for (int j = this.zzb - 1; j >= 0; --j) {
            zza(this.zzc[j], this.zzd[j], zzgmk);
        }
    }
    
    public final void zzc() {
        this.zzf = false;
    }
    
    public final int zzd() {
        final int zze = this.zze;
        if (zze != -1) {
            return zze;
        }
        int i = 0;
        int zze2 = 0;
        while (i < this.zzb) {
            zze2 += zzgic.zzd(this.zzc[i] >>> 3, (zzgho)this.zzd[i]);
            ++i;
        }
        return this.zze = zze2;
    }
    
    public final int zze() {
        final int zze = this.zze;
        if (zze != -1) {
            return zze;
        }
        int i = 0;
        int zze2 = 0;
        while (i < this.zzb) {
            final int n = this.zzc[i];
            final int n2 = n >>> 3;
            final int n3 = n & 0x7;
            if (n3 != 5) {
                switch (n3) {
                    default: {
                        throw new IllegalStateException(zzgjg.zzf());
                    }
                    case 3: {
                        zze2 += (zzgic.zze(n2) << 1) + ((zzglr)this.zzd[i]).zze();
                        break;
                    }
                    case 2: {
                        zze2 += zzgic.zzc(n2, (zzgho)this.zzd[i]);
                        break;
                    }
                    case 1: {
                        zze2 += zzgic.zzg(n2, (long)this.zzd[i]);
                        break;
                    }
                    case 0: {
                        zze2 += zzgic.zze(n2, (long)this.zzd[i]);
                        break;
                    }
                }
            }
            else {
                zze2 += zzgic.zzi(n2, (int)this.zzd[i]);
            }
            ++i;
        }
        return this.zze = zze2;
    }
}
