package com.google.android.gms.internal;

interface zzgkz
{
     <T> zzgky<T> zza(final Class<T> p0);
}
