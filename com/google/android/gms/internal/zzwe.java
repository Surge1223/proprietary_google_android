package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzwe extends zzey implements zzwc
{
    zzwe(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.purchase.client.IInAppPurchaseListener");
    }
}
