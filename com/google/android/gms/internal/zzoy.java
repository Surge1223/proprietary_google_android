package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzoy extends zzez implements zzox
{
    public zzoy() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.formats.client.IOnAppInstallAdLoadedListener");
    }
    
    public static zzox zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IOnAppInstallAdLoadedListener");
        if (queryLocalInterface instanceof zzox) {
            return (zzox)queryLocalInterface;
        }
        return new zzoz(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            final IBinder strongBinder = parcel.readStrongBinder();
            zzol zzol;
            if (strongBinder == null) {
                zzol = null;
            }
            else {
                final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAppInstallAd");
                if (queryLocalInterface instanceof zzol) {
                    zzol = (zzol)queryLocalInterface;
                }
                else {
                    zzol = new zzon(strongBinder);
                }
            }
            this.zza(zzol);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}
