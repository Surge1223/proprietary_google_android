package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.CustomRenderedAd;

public final class zzmc implements CustomRenderedAd
{
    private final zzmd zza;
    
    public zzmc(final zzmd zza) {
        this.zza = zza;
    }
}
