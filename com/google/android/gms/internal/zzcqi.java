package com.google.android.gms.internal;

import java.io.File;
import android.os.Parcelable.Creator;
import android.os.Parcel;

final class zzcqi implements zzdlb
{
    private final int zza;
    
    zzcqi(final int zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzbih zzbih, final Parcel parcel) {
        zzcqh.zza(this.zza, (zzcqh)zzbih, parcel);
    }
}
