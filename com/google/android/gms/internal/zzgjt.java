package com.google.android.gms.internal;

import java.util.Collection;

final class zzgjt extends zzgjq
{
    private zzgjt() {
        super(null);
    }
    
    private static <E> zzgjf<E> zzc(final Object o, final long n) {
        return (zzgjf<E>)zzglw.zzf(o, n);
    }
    
    @Override
    final <E> void zza(final Object o, final Object o2, final long n) {
        final zzgjf<Object> zzc = zzc(o, n);
        final zzgjf<Object> zzc2 = zzc(o2, n);
        final int size = zzc.size();
        final int size2 = zzc2.size();
        zzgjf<Object> zza = zzc;
        if (size > 0) {
            zza = zzc;
            if (size2 > 0) {
                zza = zzc;
                if (!zzc.zza()) {
                    zza = zzc.zza(size2 + size);
                }
                zza.addAll(zzc2);
            }
        }
        zzgjf<Object> zzgjf = zzc2;
        if (size > 0) {
            zzgjf = zza;
        }
        zzglw.zza(o, n, zzgjf);
    }
}
