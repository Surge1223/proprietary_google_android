package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzade extends IInterface
{
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final int p1) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzadi p1) throws RemoteException;
    
    void zzb(final IObjectWrapper p0) throws RemoteException;
    
    void zzb(final IObjectWrapper p0, final int p1) throws RemoteException;
    
    void zzc(final IObjectWrapper p0) throws RemoteException;
    
    void zzd(final IObjectWrapper p0) throws RemoteException;
    
    void zze(final IObjectWrapper p0) throws RemoteException;
    
    void zzf(final IObjectWrapper p0) throws RemoteException;
    
    void zzg(final IObjectWrapper p0) throws RemoteException;
    
    void zzh(final IObjectWrapper p0) throws RemoteException;
}
