package com.google.android.gms.internal;

import java.io.IOException;

public class zzgjg extends IOException
{
    private zzgkg zza;
    
    public zzgjg(final String s) {
        super(s);
        this.zza = null;
    }
    
    static zzgjg zza() {
        return new zzgjg("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }
    
    static zzgjg zzb() {
        return new zzgjg("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }
    
    static zzgjh zzf() {
        return new zzgjh("Protocol message tag had invalid wire type.");
    }
}
