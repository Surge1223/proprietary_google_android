package com.google.android.gms.internal;

import java.util.List;
import java.util.Iterator;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.ListIterator;

final class zzglu implements ListIterator<String>
{
    private ListIterator<String> zza;
    private final /* synthetic */ int zzb;
    private final /* synthetic */ zzglt zzc;
    
    zzglu(final zzglt zzc, final int zzb) {
        this.zzc = zzc;
        this.zzb = zzb;
        this.zza = this.zzc.zza.listIterator(this.zzb);
    }
    
    @Override
    public final boolean hasNext() {
        return this.zza.hasNext();
    }
    
    @Override
    public final boolean hasPrevious() {
        return this.zza.hasPrevious();
    }
    
    @Override
    public final int nextIndex() {
        return this.zza.nextIndex();
    }
    
    @Override
    public final int previousIndex() {
        return this.zza.previousIndex();
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
