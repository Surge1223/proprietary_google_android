package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import android.content.Context;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import android.os.IBinder;
import java.util.WeakHashMap;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

public final class zzow implements NativeCustomTemplateAd
{
    private static WeakHashMap<IBinder, zzow> zza;
    private final zzot zzb;
    private final MediaView zzc;
    private final VideoController zzd;
    
    static {
        zzow.zza = new WeakHashMap<IBinder, zzow>();
    }
    
    private zzow(final zzot zzb) {
        this.zzd = new VideoController();
        this.zzb = zzb;
        final MediaView mediaView = null;
        final MediaView mediaView2 = null;
        Context context;
        try {
            context = zzn.zza(zzb.zze());
        }
        catch (NullPointerException | RemoteException ex2) {
            final Throwable t;
            zzaid.zzb("Unable to inflate MediaView.", t);
            context = null;
        }
        MediaView zzc = mediaView;
        if (context != null) {
            MediaView mediaView3 = new MediaView(context);
            try {
                if (!this.zzb.zza(zzn.zza(mediaView3))) {
                    mediaView3 = mediaView2;
                }
                zzc = mediaView3;
            }
            catch (RemoteException ex) {
                zzaid.zzb("Unable to render video in MediaView.", (Throwable)ex);
                zzc = mediaView;
            }
        }
        this.zzc = zzc;
    }
    
    public static zzow zza(final zzot zzot) {
        synchronized (zzow.zza) {
            final zzow zzow = com.google.android.gms.internal.zzow.zza.get(zzot.asBinder());
            if (zzow != null) {
                return zzow;
            }
            final zzow zzow2 = new zzow(zzot);
            com.google.android.gms.internal.zzow.zza.put(zzot.asBinder(), zzow2);
            return zzow2;
        }
    }
    
    @Override
    public final String getCustomTemplateId() {
        try {
            return this.zzb.zzl();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get custom template id.", (Throwable)ex);
            return null;
        }
    }
    
    public final zzot zza() {
        return this.zzb;
    }
}
