package com.google.android.gms.internal;

import android.os.Parcel;

public final class zzbif extends RuntimeException
{
    public zzbif(final String s, final Parcel parcel) {
        final int dataPosition = parcel.dataPosition();
        final int dataSize = parcel.dataSize();
        final StringBuilder sb = new StringBuilder(41 + String.valueOf(s).length());
        sb.append(s);
        sb.append(" Parcel: pos=");
        sb.append(dataPosition);
        sb.append(" size=");
        sb.append(dataSize);
        super(sb.toString());
    }
}
