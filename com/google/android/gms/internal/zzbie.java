package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.Parcel;

public final class zzbie
{
    public static int zza(final Parcel parcel) {
        final int int1 = parcel.readInt();
        final int zza = zza(parcel, int1);
        final int dataPosition = parcel.dataPosition();
        if ((0xFFFF & int1) != 0x4F45) {
            final String value = String.valueOf(Integer.toHexString(int1));
            String concat;
            if (value.length() != 0) {
                concat = "Expected object header. Got 0x".concat(value);
            }
            else {
                concat = new String("Expected object header. Got 0x");
            }
            throw new zzbif(concat, parcel);
        }
        final int n = zza + dataPosition;
        if (n >= dataPosition && n <= parcel.dataSize()) {
            return n;
        }
        final StringBuilder sb = new StringBuilder(54);
        sb.append("Size read is invalid start=");
        sb.append(dataPosition);
        sb.append(" end=");
        sb.append(n);
        throw new zzbif(sb.toString(), parcel);
    }
    
    public static int zza(final Parcel parcel, final int n) {
        if ((n & 0xFFFF0000) != 0xFFFF0000) {
            return n >> 16 & 0xFFFF;
        }
        return parcel.readInt();
    }
    
    public static <T extends Parcelable> T zza(final Parcel parcel, int zza, final Parcelable.Creator<T> parcelable$Creator) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final Parcelable parcelable = (Parcelable)parcelable$Creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + zza);
        return (T)parcelable;
    }
    
    private static void zza(final Parcel parcel, int zza, final int n) {
        zza = zza(parcel, zza);
        if (zza == n) {
            return;
        }
        final String hexString = Integer.toHexString(zza);
        final StringBuilder sb = new StringBuilder(46 + String.valueOf(hexString).length());
        sb.append("Expected size ");
        sb.append(n);
        sb.append(" got ");
        sb.append(zza);
        sb.append(" (0x");
        sb.append(hexString);
        sb.append(")");
        throw new zzbif(sb.toString(), parcel);
    }
    
    private static void zza(final Parcel parcel, final int n, final int n2, final int n3) {
        if (n2 == n3) {
            return;
        }
        final String hexString = Integer.toHexString(n2);
        final StringBuilder sb = new StringBuilder(46 + String.valueOf(hexString).length());
        sb.append("Expected size ");
        sb.append(n3);
        sb.append(" got ");
        sb.append(n2);
        sb.append(" (0x");
        sb.append(hexString);
        sb.append(")");
        throw new zzbif(sb.toString(), parcel);
    }
    
    public static String[] zzaa(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final String[] stringArray = parcel.createStringArray();
        parcel.setDataPosition(dataPosition + zza);
        return stringArray;
    }
    
    public static ArrayList<Integer> zzab(final Parcel parcel, int i) {
        final int zza = zza(parcel, i);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final ArrayList<Integer> list = new ArrayList<Integer>();
        int int1;
        for (int1 = parcel.readInt(), i = 0; i < int1; ++i) {
            list.add(parcel.readInt());
        }
        parcel.setDataPosition(dataPosition + zza);
        return list;
    }
    
    public static ArrayList<String> zzac(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final ArrayList stringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(dataPosition + zza);
        return (ArrayList<String>)stringArrayList;
    }
    
    public static Parcel zzad(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final Parcel obtain = Parcel.obtain();
        obtain.appendFrom(parcel, dataPosition, zza);
        parcel.setDataPosition(dataPosition + zza);
        return obtain;
    }
    
    public static void zzae(final Parcel parcel, final int n) {
        if (parcel.dataPosition() == n) {
            return;
        }
        final StringBuilder sb = new StringBuilder(37);
        sb.append("Overread allowed size end=");
        sb.append(n);
        throw new zzbif(sb.toString(), parcel);
    }
    
    public static void zzb(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        parcel.setDataPosition(parcel.dataPosition() + zza);
    }
    
    public static <T> T[] zzb(final Parcel parcel, int dataPosition, final Parcelable.Creator<T> parcelable$Creator) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final Object[] typedArray = parcel.createTypedArray((Parcelable.Creator)parcelable$Creator);
        parcel.setDataPosition(dataPosition + zza);
        return (T[])typedArray;
    }
    
    public static <T> ArrayList<T> zzc(final Parcel parcel, int dataPosition, final Parcelable.Creator<T> parcelable$Creator) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final ArrayList typedArrayList = parcel.createTypedArrayList((Parcelable.Creator)parcelable$Creator);
        parcel.setDataPosition(dataPosition + zza);
        return (ArrayList<T>)typedArrayList;
    }
    
    public static boolean zzc(final Parcel parcel, final int n) {
        zza(parcel, n, 4);
        return parcel.readInt() != 0;
    }
    
    public static int zzg(final Parcel parcel, final int n) {
        zza(parcel, n, 4);
        return parcel.readInt();
    }
    
    public static long zzi(final Parcel parcel, final int n) {
        zza(parcel, n, 8);
        return parcel.readLong();
    }
    
    public static Long zzj(final Parcel parcel, final int n) {
        final int zza = zza(parcel, n);
        if (zza == 0) {
            return null;
        }
        zza(parcel, n, zza, 8);
        return parcel.readLong();
    }
    
    public static BigInteger zzk(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final byte[] byteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + zza);
        return new BigInteger(byteArray);
    }
    
    public static float zzl(final Parcel parcel, final int n) {
        zza(parcel, n, 4);
        return parcel.readFloat();
    }
    
    public static double zzn(final Parcel parcel, final int n) {
        zza(parcel, n, 8);
        return parcel.readDouble();
    }
    
    public static BigDecimal zzp(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final byte[] byteArray = parcel.createByteArray();
        final int int1 = parcel.readInt();
        parcel.setDataPosition(dataPosition + zza);
        return new BigDecimal(new BigInteger(byteArray), int1);
    }
    
    public static String zzq(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final String string = parcel.readString();
        parcel.setDataPosition(dataPosition + zza);
        return string;
    }
    
    public static IBinder zzr(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final IBinder strongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + zza);
        return strongBinder;
    }
    
    public static Bundle zzs(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final Bundle bundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + zza);
        return bundle;
    }
    
    public static byte[] zzt(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final byte[] byteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + zza);
        return byteArray;
    }
    
    public static byte[][] zzu(final Parcel parcel, int i) {
        final int zza = zza(parcel, i);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final int int1 = parcel.readInt();
        final byte[][] array = new byte[int1][];
        for (i = 0; i < int1; ++i) {
            array[i] = parcel.createByteArray();
        }
        parcel.setDataPosition(dataPosition + zza);
        return array;
    }
    
    public static boolean[] zzv(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final boolean[] booleanArray = parcel.createBooleanArray();
        parcel.setDataPosition(dataPosition + zza);
        return booleanArray;
    }
    
    public static int[] zzw(final Parcel parcel, int zza) {
        zza = zza(parcel, zza);
        final int dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final int[] intArray = parcel.createIntArray();
        parcel.setDataPosition(dataPosition + zza);
        return intArray;
    }
    
    public static long[] zzx(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final long[] longArray = parcel.createLongArray();
        parcel.setDataPosition(dataPosition + zza);
        return longArray;
    }
    
    public static float[] zzy(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final float[] floatArray = parcel.createFloatArray();
        parcel.setDataPosition(dataPosition + zza);
        return floatArray;
    }
    
    public static double[] zzz(final Parcel parcel, int dataPosition) {
        final int zza = zza(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        if (zza == 0) {
            return null;
        }
        final double[] doubleArray = parcel.createDoubleArray();
        parcel.setDataPosition(dataPosition + zza);
        return doubleArray;
    }
}
