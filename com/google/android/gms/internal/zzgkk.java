package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;
import java.util.List;
import sun.misc.Unsafe;

final class zzgkk<T> implements zzgky<T>
{
    private static final Unsafe zza;
    private final int[] zzb;
    private final int zzc;
    private final int zzd;
    private final zzgiy<Class<?>> zze;
    private final zzgiy<zzgjc<?>> zzf;
    private final zzgko zzg;
    private final zzgjq zzh;
    private final zzgkg zzi;
    private final zzglq<?, ?> zzj;
    private final boolean zzk;
    private final zzgij<?> zzl;
    private final boolean zzm;
    private final boolean zzn;
    private final zzgkb zzo;
    private final zzgiy<Object> zzp;
    private final int[] zzq;
    private final int[] zzr;
    private final int[] zzs;
    private final int zzt;
    private final Object[] zzu;
    
    static {
        zza = zzglw.zzc();
    }
    
    private zzgkk(final int[] zzb, final int zzc, final int zzd, final boolean zzn, final Class<T> clazz, final zzgiy<Class<?>> zze, final Object[] zzu, final zzgiy<zzgjc<?>> zzf, final zzgko zzg, final zzgjq zzh, final zzglq<?, ?> zzj, final zzgij<?> zzl, final zzgkb zzo, final zzgiy<Object> zzp, final int[] zzq, final zzgkg zzi, final int[] zzr, final int[] zzs, final int zzt) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzu = zzu;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzj = zzj;
        this.zzk = (zzl != null && zzl.zza(clazz));
        this.zzl = zzl;
        this.zzm = zzgiw.class.isAssignableFrom(clazz);
        this.zzn = zzn;
        this.zzi = zzi;
        this.zzo = zzo;
        this.zzp = zzp;
        this.zzq = zzq;
        this.zzr = zzr;
        this.zzs = zzs;
        this.zzt = zzt;
    }
    
    private final int zza(final int n) {
        return this.zzb[n + 1];
    }
    
    static <T> zzgkk<T> zza(final Class<T> clazz, final zzgke zzgke, final zzgko zzgko, final zzgjq zzgjq, final zzglq<?, ?> zzglq, final zzgij<?> zzgij, final zzgkb zzgkb) {
        if (zzgke instanceof zzgku) {
            final zzgku zzgku = (zzgku)zzgke;
            final boolean b = zzgku.zza() == zzgiw.zzg.zzj;
            int zzk;
            int zze;
            int zzf;
            if (zzgku.zzg() == 0) {
                zzk = 0;
                zzf = (zze = zzk);
            }
            else {
                zze = zzgku.zze();
                zzf = zzgku.zzf();
                zzk = zzgku.zzk();
            }
            final int[] array = new int[zzk << 2];
            final int zzh = zzgku.zzh();
            Object[] array2 = null;
            int[] array3;
            if (zzh > 0) {
                array3 = new int[zzgku.zzh()];
            }
            else {
                array3 = null;
            }
            int[] array4;
            if (zzgku.zzi() > 0) {
                array4 = new int[zzgku.zzi()];
            }
            else {
                array4 = null;
            }
            final zzgkv zzd = zzgku.zzd();
            if (zzd.zza()) {
                int zzb = zzd.zzb();
                int n = 0;
                int n3;
                int n2 = n3 = n;
                while (true) {
                    if (zzb < zzgku.zzl() && n < zzb - zze << 2) {
                        for (int i = 0; i < 4; ++i) {
                            array[n + i] = -1;
                        }
                    }
                    else {
                        int n4;
                        int n5;
                        int zzj;
                        if (zzd.zzd()) {
                            n4 = (int)zzglw.zza(zzd.zze());
                            n5 = (int)zzglw.zza(zzd.zzf());
                            zzj = 0;
                        }
                        else {
                            n4 = (int)zzglw.zza(zzd.zzg());
                            if (zzd.zzh()) {
                                n5 = (int)zzglw.zza(zzd.zzi());
                                zzj = zzd.zzj();
                            }
                            else {
                                n5 = (zzj = 0);
                            }
                        }
                        array[n] = zzd.zzb();
                        final int n6 = n + 1;
                        int n7;
                        if (zzd.zzl()) {
                            n7 = 536870912;
                        }
                        else {
                            n7 = 0;
                        }
                        int n8;
                        if (zzd.zzk()) {
                            n8 = 268435456;
                        }
                        else {
                            n8 = 0;
                        }
                        array[n6] = (n7 | n8 | zzd.zzc() << 20 | n4);
                        array[n + 2] = (n5 | zzj << 20);
                        final int zzc = zzd.zzc();
                        int n9;
                        int n10;
                        if (zzc == zzgiq.zzi.ordinal()) {
                            array3[n2] = n;
                            n9 = n2 + 1;
                            n10 = n3;
                        }
                        else {
                            n9 = n2;
                            n10 = n3;
                            if (zzc >= 18) {
                                n9 = n2;
                                n10 = n3;
                                if (zzc <= 49) {
                                    array4[n3] = (array[n6] & 0xFFFFF);
                                    n10 = n3 + 1;
                                    n9 = n2;
                                }
                            }
                        }
                        if (!zzd.zza()) {
                            break;
                        }
                        final int zzb2 = zzd.zzb();
                        n3 = n10;
                        n2 = n9;
                        zzb = zzb2;
                    }
                    n += 4;
                }
            }
            if (!zzd.zzm().zzb()) {
                array2 = new Object[zzk];
            }
            return new zzgkk<T>(array, zze, zzf, b, (Class<Object>)clazz, zzd.zzm(), array2, zzd.zzn(), zzgko, zzgjq, zzglq, zzgij, zzgkb, zzd.zzo(), zzgku.zzj(), zzgku.zzc(), array3, array4, zzgku.zzl());
        }
        ((zzgll)zzgke).zza();
        throw new NoSuchMethodError();
    }
    
    private static <E> List<E> zza(final Object o, final long n) {
        return (List<E>)zzglw.zzf(o, n);
    }
    
    private static void zza(final int n, final Object o, final zzgmk zzgmk) {
        if (o instanceof String) {
            zzgmk.zza(n, (String)o);
            return;
        }
        zzgmk.zza(n, (zzgho)o);
    }
    
    private static <UT, UB> void zza(final zzglq<UT, UB> zzglq, final T t, final zzgmk zzgmk) {
        zzglq.zza(zzglq.zzb(t), zzgmk);
    }
    
    private final <K, V> void zza(final zzgmk zzgmk, final int n, final Object o) {
        if (o != null) {
            zzgmk.zza(n, this.zzo.zzf(this.zzp.zza(n)), this.zzo.zzb(o));
        }
    }
    
    private final void zza(final T t, final T t2, final int n) {
        final long n2 = this.zza(n) & 0xFFFFF;
        if (!this.zza(t2, n)) {
            return;
        }
        final Object zzf = zzglw.zzf(t, n2);
        final Object zzf2 = zzglw.zzf(t2, n2);
        if (zzf != null && zzf2 != null) {
            zzglw.zza(t, n2, zzgja.zza(zzf, zzf2));
            this.zzb(t, n);
            return;
        }
        if (zzf2 != null) {
            zzglw.zza(t, n2, zzf2);
            this.zzb(t, n);
        }
    }
    
    private final boolean zza(final T t, int n) {
        if (!this.zzn) {
            n = this.zzb(n);
            return (zzglw.zza(t, n & 0xFFFFF) & 1 << (n >>> 20)) != 0x0;
        }
        n = this.zza(n);
        final long n2 = n & 0xFFFFF;
        switch ((n & 0xFF00000) >>> 20) {
            default: {
                throw new IllegalArgumentException();
            }
            case 17: {
                return zzglw.zzf(t, n2) != null;
            }
            case 16: {
                return zzglw.zzb(t, n2) != 0L;
            }
            case 15: {
                return zzglw.zza(t, n2) != 0;
            }
            case 14: {
                return zzglw.zzb(t, n2) != 0L;
            }
            case 13: {
                return zzglw.zza(t, n2) != 0;
            }
            case 12: {
                return zzglw.zza(t, n2) != 0;
            }
            case 11: {
                return zzglw.zza(t, n2) != 0;
            }
            case 10: {
                return !zzgho.zza.equals(zzglw.zzf(t, n2));
            }
            case 9: {
                return zzglw.zzf(t, n2) != null;
            }
            case 8: {
                final Object zzf = zzglw.zzf(t, n2);
                if (zzf instanceof String) {
                    return !((String)zzf).isEmpty();
                }
                if (zzf instanceof zzgho) {
                    return !zzgho.zza.equals(zzf);
                }
                throw new IllegalArgumentException();
            }
            case 7: {
                return zzglw.zzc(t, n2);
            }
            case 6: {
                return zzglw.zza(t, n2) != 0;
            }
            case 5: {
                return zzglw.zzb(t, n2) != 0L;
            }
            case 4: {
                return zzglw.zza(t, n2) != 0;
            }
            case 3: {
                return zzglw.zzb(t, n2) != 0L;
            }
            case 2: {
                return zzglw.zzb(t, n2) != 0L;
            }
            case 1: {
                return zzglw.zzd(t, n2) != 0.0f;
            }
            case 0: {
                return zzglw.zze(t, n2) != 0.0;
            }
        }
    }
    
    private final boolean zza(final T t, final int n, final int n2) {
        return zzglw.zza(t, this.zzb(n2) & 0xFFFFF) == n;
    }
    
    private static <T> double zzb(final T t, final long n) {
        return (double)zzglw.zzf(t, n);
    }
    
    private final int zzb(final int n) {
        return this.zzb[n + 2];
    }
    
    private final void zzb(final T t, int zzb) {
        if (this.zzn) {
            return;
        }
        zzb = this.zzb(zzb);
        final long n = zzb & 0xFFFFF;
        zzglw.zza(t, n, zzglw.zza(t, n) | 1 << (zzb >>> 20));
    }
    
    private final void zzb(final T t, final int n, final int n2) {
        zzglw.zza(t, this.zzb(n2) & 0xFFFFF, n);
    }
    
    private final void zzb(final T t, final T t2, final int n) {
        final int zza = this.zza(n);
        final int n2 = this.zzb[n];
        final long n3 = zza & 0xFFFFF;
        if (!this.zza(t2, n2, n)) {
            return;
        }
        final Object zzf = zzglw.zzf(t, n3);
        final Object zzf2 = zzglw.zzf(t2, n3);
        if (zzf != null && zzf2 != null) {
            zzglw.zza(t, n3, zzgja.zza(zzf, zzf2));
            this.zzb(t, n2, n);
            return;
        }
        if (zzf2 != null) {
            zzglw.zza(t, n3, zzf2);
            this.zzb(t, n2, n);
        }
    }
    
    private static <T> float zzc(final T t, final long n) {
        return (float)zzglw.zzf(t, n);
    }
    
    private final boolean zzc(final T t, final T t2, final int n) {
        return this.zza(t, n) == this.zza(t2, n);
    }
    
    private static <T> int zzd(final T t, final long n) {
        return (int)zzglw.zzf(t, n);
    }
    
    private static <T> long zze(final T t, final long n) {
        return (long)zzglw.zzf(t, n);
    }
    
    private static <T> boolean zzf(final T t, final long n) {
        return (boolean)zzglw.zzf(t, n);
    }
    
    @Override
    public final int zza(final T t) {
        final int length = this.zzb.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final int zza = this.zza(i);
            final int n2 = this.zzb[i];
            final long n3 = 0xFFFFF & zza;
            int n4 = 37;
            int n5 = 0;
            switch ((zza & 0xFF00000) >>> 20) {
                default: {
                    n5 = n;
                    break;
                }
                case 68: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzglw.zzf(t, n3).hashCode();
                        break;
                    }
                    break;
                }
                case 67: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(zze(t, n3));
                        break;
                    }
                    break;
                }
                case 66: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzd(t, n3);
                        break;
                    }
                    break;
                }
                case 65: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(zze(t, n3));
                        break;
                    }
                    break;
                }
                case 64: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzd(t, n3);
                        break;
                    }
                    break;
                }
                case 63: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzd(t, n3);
                        break;
                    }
                    break;
                }
                case 62: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzd(t, n3);
                        break;
                    }
                    break;
                }
                case 61: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzglw.zzf(t, n3).hashCode();
                        break;
                    }
                    break;
                }
                case 60: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzglw.zzf(t, n3).hashCode();
                        break;
                    }
                    break;
                }
                case 59: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + ((String)zzglw.zzf(t, n3)).hashCode();
                        break;
                    }
                    break;
                }
                case 58: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(zzf(t, n3));
                        break;
                    }
                    break;
                }
                case 57: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzd(t, n3);
                        break;
                    }
                    break;
                }
                case 56: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(zze(t, n3));
                        break;
                    }
                    break;
                }
                case 55: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzd(t, n3);
                        break;
                    }
                    break;
                }
                case 54: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(zze(t, n3));
                        break;
                    }
                    break;
                }
                case 53: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(zze(t, n3));
                        break;
                    }
                    break;
                }
                case 52: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + Float.floatToIntBits(zzc(t, n3));
                        break;
                    }
                    break;
                }
                case 51: {
                    n5 = n;
                    if (this.zza(t, n2, i)) {
                        n5 = n * 53 + zzgja.zza(Double.doubleToLongBits(zzb(t, n3)));
                        break;
                    }
                    break;
                }
                case 50: {
                    n5 = n * 53 + zzglw.zzf(t, n3).hashCode();
                    break;
                }
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49: {
                    n5 = n * 53 + zzglw.zzf(t, n3).hashCode();
                    break;
                }
                case 17: {
                    final Object zzf = zzglw.zzf(t, n3);
                    if (zzf != null) {
                        n4 = zzf.hashCode();
                    }
                    n5 = n * 53 + n4;
                    break;
                }
                case 16: {
                    n5 = n * 53 + zzgja.zza(zzglw.zzb(t, n3));
                    break;
                }
                case 15: {
                    n5 = n * 53 + zzglw.zza(t, n3);
                    break;
                }
                case 14: {
                    n5 = n * 53 + zzgja.zza(zzglw.zzb(t, n3));
                    break;
                }
                case 13: {
                    n5 = n * 53 + zzglw.zza(t, n3);
                    break;
                }
                case 12: {
                    n5 = n * 53 + zzglw.zza(t, n3);
                    break;
                }
                case 11: {
                    n5 = n * 53 + zzglw.zza(t, n3);
                    break;
                }
                case 10: {
                    n5 = n * 53 + zzglw.zzf(t, n3).hashCode();
                    break;
                }
                case 9: {
                    final Object zzf2 = zzglw.zzf(t, n3);
                    if (zzf2 != null) {
                        n4 = zzf2.hashCode();
                    }
                    n5 = n * 53 + n4;
                    break;
                }
                case 8: {
                    n5 = n * 53 + ((String)zzglw.zzf(t, n3)).hashCode();
                    break;
                }
                case 7: {
                    n5 = n * 53 + zzgja.zza(zzglw.zzc(t, n3));
                    break;
                }
                case 6: {
                    n5 = n * 53 + zzglw.zza(t, n3);
                    break;
                }
                case 5: {
                    n5 = n * 53 + zzgja.zza(zzglw.zzb(t, n3));
                    break;
                }
                case 4: {
                    n5 = n * 53 + zzglw.zza(t, n3);
                    break;
                }
                case 3: {
                    n5 = n * 53 + zzgja.zza(zzglw.zzb(t, n3));
                    break;
                }
                case 2: {
                    n5 = n * 53 + zzgja.zza(zzglw.zzb(t, n3));
                    break;
                }
                case 1: {
                    n5 = n * 53 + Float.floatToIntBits(zzglw.zzd(t, n3));
                    break;
                }
                case 0: {
                    n5 = n * 53 + zzgja.zza(Double.doubleToLongBits(zzglw.zze(t, n3)));
                    break;
                }
            }
            i += 4;
            n = n5;
        }
        int n6 = n * 53 + this.zzj.zzb(t).hashCode();
        if (this.zzk) {
            n6 = n6 * 53 + this.zzl.zza(t).hashCode();
        }
        return n6;
    }
    
    @Override
    public final void zza(final T t, final zzgmk zzgmk) {
        if (zzgmk.zza() == zzgiw.zzg.zzl) {
            zza(this.zzj, t, zzgmk);
            zzgin<?> zza;
            if (this.zzk) {
                zza = this.zzl.zza(t);
            }
            else {
                zza = null;
            }
            Object zzf;
            if (zza == null) {
                zzf = null;
            }
            else {
                zzf = zza.zzf();
            }
            Map.Entry<?, ?> entry;
            if (zzf != null && ((Iterator)zzf).hasNext()) {
                entry = (Map.Entry<?, ?>)((Iterator<Map.Entry>)zzf).next();
            }
            else {
                entry = null;
            }
            int n = this.zzb.length - 4;
            Map.Entry<?, ?> entry2;
            while (true) {
                entry2 = entry;
                if (n < 0) {
                    break;
                }
                final int zza2 = this.zza(n);
                final int n2 = this.zzb[n];
                while (entry != null && this.zzl.zza(entry) > n2) {
                    this.zzl.zza(zzgmk, entry);
                    if (((Iterator)zzf).hasNext()) {
                        entry = (Map.Entry<?, ?>)((Iterator<Map.Entry>)zzf).next();
                    }
                    else {
                        entry = null;
                    }
                }
                switch ((zza2 & 0xFF00000) >>> 20) {
                    case 68: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzb(n2, zzglw.zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 67: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zze(n2, zze(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 66: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzf(n2, zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 65: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzb(n2, zze(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 64: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 63: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzb(n2, zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 62: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zze(n2, zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 61: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, (zzgho)zzglw.zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 60: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, zzglw.zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 59: {
                        if (this.zza(t, n2, n)) {
                            zza(n2, zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk);
                            break;
                        }
                        break;
                    }
                    case 58: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 57: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzd(n2, zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 56: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzd(n2, zze(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 55: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzc(n2, zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 54: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zzc(n2, zze(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 53: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, zze(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 52: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, zzc(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 51: {
                        if (this.zza(t, n2, n)) {
                            zzgmk.zza(n2, zzb(t, (long)(zza2 & 0xFFFFF)));
                            break;
                        }
                        break;
                    }
                    case 50: {
                        this.zza(zzgmk, n2, zzglw.zzf(t, zza2 & 0xFFFFF));
                        break;
                    }
                    case 49: {
                        zzgla.zzd(this.zzb[n], (List<?>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk);
                        break;
                    }
                    case 48: {
                        zzgla.zze(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 47: {
                        zzgla.zzj(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 46: {
                        zzgla.zzg(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 45: {
                        zzgla.zzl(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 44: {
                        zzgla.zzm(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 43: {
                        zzgla.zzi(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 42: {
                        zzgla.zzn(this.zzb[n], (List<Boolean>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 41: {
                        zzgla.zzk(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 40: {
                        zzgla.zzf(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 39: {
                        zzgla.zzh(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 38: {
                        zzgla.zzd(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 37: {
                        zzgla.zzc(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 36: {
                        zzgla.zzb(this.zzb[n], (List<Float>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 35: {
                        zzgla.zza(this.zzb[n], (List<Double>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, true);
                        break;
                    }
                    case 34: {
                        zzgla.zze(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 33: {
                        zzgla.zzj(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 32: {
                        zzgla.zzg(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 31: {
                        zzgla.zzl(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 30: {
                        zzgla.zzm(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 29: {
                        zzgla.zzi(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 28: {
                        zzgla.zzb(this.zzb[n], (List<zzgho>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk);
                        break;
                    }
                    case 27: {
                        zzgla.zzc(this.zzb[n], (List<?>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk);
                        break;
                    }
                    case 26: {
                        zzgla.zza(this.zzb[n], (List<String>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk);
                        break;
                    }
                    case 25: {
                        zzgla.zzn(this.zzb[n], (List<Boolean>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 24: {
                        zzgla.zzk(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 23: {
                        zzgla.zzf(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 22: {
                        zzgla.zzh(this.zzb[n], (List<Integer>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 21: {
                        zzgla.zzd(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 20: {
                        zzgla.zzc(this.zzb[n], (List<Long>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 19: {
                        zzgla.zzb(this.zzb[n], (List<Float>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 18: {
                        zzgla.zza(this.zzb[n], (List<Double>)zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk, false);
                        break;
                    }
                    case 17: {
                        if (this.zza(t, n)) {
                            zzgmk.zzb(n2, zzglw.zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 16: {
                        if (this.zza(t, n)) {
                            zzgmk.zze(n2, zzglw.zzb(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 15: {
                        if (this.zza(t, n)) {
                            zzgmk.zzf(n2, zzglw.zza(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 14: {
                        if (this.zza(t, n)) {
                            zzgmk.zzb(n2, zzglw.zzb(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 13: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, zzglw.zza(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 12: {
                        if (this.zza(t, n)) {
                            zzgmk.zzb(n2, zzglw.zza(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 11: {
                        if (this.zza(t, n)) {
                            zzgmk.zze(n2, zzglw.zza(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 10: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, (zzgho)zzglw.zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 9: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, zzglw.zzf(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 8: {
                        if (this.zza(t, n)) {
                            zza(n2, zzglw.zzf(t, zza2 & 0xFFFFF), zzgmk);
                            break;
                        }
                        break;
                    }
                    case 7: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, zzglw.zzc(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 6: {
                        if (this.zza(t, n)) {
                            zzgmk.zzd(n2, zzglw.zza(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 5: {
                        if (this.zza(t, n)) {
                            zzgmk.zzd(n2, zzglw.zzb(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 4: {
                        if (this.zza(t, n)) {
                            zzgmk.zzc(n2, zzglw.zza(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 3: {
                        if (this.zza(t, n)) {
                            zzgmk.zzc(n2, zzglw.zzb(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 2: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, zzglw.zzb(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 1: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, zzglw.zzd(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                    case 0: {
                        if (this.zza(t, n)) {
                            zzgmk.zza(n2, zzglw.zze(t, zza2 & 0xFFFFF));
                            break;
                        }
                        break;
                    }
                }
                n -= 4;
            }
            while (entry2 != null) {
                this.zzl.zza(zzgmk, entry2);
                if (((Iterator)zzf).hasNext()) {
                    entry2 = (Map.Entry<?, ?>)((Iterator<Map.Entry>)zzf).next();
                }
                else {
                    entry2 = null;
                }
            }
            return;
        }
        zzgin<?> zza3;
        if (this.zzk) {
            zza3 = this.zzl.zza(t);
        }
        else {
            zza3 = null;
        }
        Object zze;
        if (zza3 == null) {
            zze = null;
        }
        else {
            zze = zza3.zze();
        }
        Map.Entry<?, ?> entry3;
        if (zze != null && ((Iterator)zze).hasNext()) {
            entry3 = (Map.Entry<?, ?>)((Iterator<Map.Entry>)zze).next();
        }
        else {
            entry3 = null;
        }
        final int length = this.zzb.length;
        int n3 = 0;
        Map.Entry<?, ?> entry4 = entry3;
        Map.Entry<?, ?> entry5;
        while (true) {
            entry5 = entry4;
            if (n3 >= length) {
                break;
            }
            final int zza4 = this.zza(n3);
            final int n4 = this.zzb[n3];
            while (entry4 != null && this.zzl.zza(entry4) <= n4) {
                this.zzl.zza(zzgmk, entry4);
                if (((Iterator)zze).hasNext()) {
                    entry4 = (Map.Entry<?, ?>)((Iterator<Map.Entry>)zze).next();
                }
                else {
                    entry4 = null;
                }
            }
            switch ((zza4 & 0xFF00000) >>> 20) {
                case 68: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzb(n4, zzglw.zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 67: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zze(n4, zze(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 66: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzf(n4, zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 65: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzb(n4, zze(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 64: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 63: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzb(n4, zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 62: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zze(n4, zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 61: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, (zzgho)zzglw.zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 60: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, zzglw.zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 59: {
                    if (this.zza(t, n4, n3)) {
                        zza(n4, zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk);
                        break;
                    }
                    break;
                }
                case 58: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 57: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzd(n4, zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 56: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzd(n4, zze(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 55: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzc(n4, zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 54: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zzc(n4, zze(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 53: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, zze(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 52: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, zzc(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 51: {
                    if (this.zza(t, n4, n3)) {
                        zzgmk.zza(n4, zzb(t, (long)(zza4 & 0xFFFFF)));
                        break;
                    }
                    break;
                }
                case 50: {
                    this.zza(zzgmk, n4, zzglw.zzf(t, zza4 & 0xFFFFF));
                    break;
                }
                case 49: {
                    zzgla.zzd(this.zzb[n3], (List<?>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk);
                    break;
                }
                case 48: {
                    zzgla.zze(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 47: {
                    zzgla.zzj(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 46: {
                    zzgla.zzg(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 45: {
                    zzgla.zzl(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 44: {
                    zzgla.zzm(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 43: {
                    zzgla.zzi(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 42: {
                    zzgla.zzn(this.zzb[n3], (List<Boolean>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 41: {
                    zzgla.zzk(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 40: {
                    zzgla.zzf(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 39: {
                    zzgla.zzh(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 38: {
                    zzgla.zzd(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 37: {
                    zzgla.zzc(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 36: {
                    zzgla.zzb(this.zzb[n3], (List<Float>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 35: {
                    zzgla.zza(this.zzb[n3], (List<Double>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, true);
                    break;
                }
                case 34: {
                    zzgla.zze(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 33: {
                    zzgla.zzj(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 32: {
                    zzgla.zzg(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 31: {
                    zzgla.zzl(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 30: {
                    zzgla.zzm(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 29: {
                    zzgla.zzi(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 28: {
                    zzgla.zzb(this.zzb[n3], (List<zzgho>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk);
                    break;
                }
                case 27: {
                    zzgla.zzc(this.zzb[n3], (List<?>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk);
                    break;
                }
                case 26: {
                    zzgla.zza(this.zzb[n3], (List<String>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk);
                    break;
                }
                case 25: {
                    zzgla.zzn(this.zzb[n3], (List<Boolean>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 24: {
                    zzgla.zzk(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 23: {
                    zzgla.zzf(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 22: {
                    zzgla.zzh(this.zzb[n3], (List<Integer>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 21: {
                    zzgla.zzd(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 20: {
                    zzgla.zzc(this.zzb[n3], (List<Long>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 19: {
                    zzgla.zzb(this.zzb[n3], (List<Float>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 18: {
                    zzgla.zza(this.zzb[n3], (List<Double>)zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk, false);
                    break;
                }
                case 17: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzb(n4, zzglw.zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 16: {
                    if (this.zza(t, n3)) {
                        zzgmk.zze(n4, zzglw.zzb(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 15: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzf(n4, zzglw.zza(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 14: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzb(n4, zzglw.zzb(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 13: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, zzglw.zza(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 12: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzb(n4, zzglw.zza(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 11: {
                    if (this.zza(t, n3)) {
                        zzgmk.zze(n4, zzglw.zza(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 10: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, (zzgho)zzglw.zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 9: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, zzglw.zzf(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 8: {
                    if (this.zza(t, n3)) {
                        zza(n4, zzglw.zzf(t, zza4 & 0xFFFFF), zzgmk);
                        break;
                    }
                    break;
                }
                case 7: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, zzglw.zzc(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 6: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzd(n4, zzglw.zza(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 5: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzd(n4, zzglw.zzb(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 4: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzc(n4, zzglw.zza(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 3: {
                    if (this.zza(t, n3)) {
                        zzgmk.zzc(n4, zzglw.zzb(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 2: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, zzglw.zzb(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 1: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, zzglw.zzd(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
                case 0: {
                    if (this.zza(t, n3)) {
                        zzgmk.zza(n4, zzglw.zze(t, zza4 & 0xFFFFF));
                        break;
                    }
                    break;
                }
            }
            n3 += 4;
        }
        while (entry5 != null) {
            this.zzl.zza(zzgmk, entry5);
            if (((Iterator)zze).hasNext()) {
                entry5 = (Map.Entry<?, ?>)((Iterator<Map.Entry>)zze).next();
            }
            else {
                entry5 = null;
            }
        }
        zza(this.zzj, t, zzgmk);
    }
    
    @Override
    public final boolean zza(final T t, final T t2) {
        final int length = this.zzb.length;
        int n = 0;
        while (true) {
            boolean b = true;
            if (n >= length) {
                return this.zzj.zzb(t).equals(this.zzj.zzb(t2)) && (!this.zzk || this.zzl.zza(t).equals(this.zzl.zza(t2)));
            }
            final int zza = this.zza(n);
            final long n2 = zza & 0xFFFFF;
            Label_0966: {
                switch ((zza & 0xFF00000) >>> 20) {
                    default: {
                        break Label_0966;
                    }
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68: {
                        final long n3 = this.zzb(n) & 0xFFFFF;
                        if (zzglw.zza(t, n3) != zzglw.zza(t2, n3) || !zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2))) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 50: {
                        b = zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2));
                        break Label_0966;
                    }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49: {
                        b = zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2));
                        break Label_0966;
                    }
                    case 17: {
                        if (!this.zzc(t, t2, n) || !zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2))) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 16: {
                        if (!this.zzc(t, t2, n) || zzglw.zzb(t, n2) != zzglw.zzb(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 15: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 14: {
                        if (!this.zzc(t, t2, n) || zzglw.zzb(t, n2) != zzglw.zzb(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 13: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 12: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 11: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 10: {
                        if (!this.zzc(t, t2, n) || !zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2))) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 9: {
                        if (!this.zzc(t, t2, n) || !zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2))) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 8: {
                        if (!this.zzc(t, t2, n) || !zzgla.zza(zzglw.zzf(t, n2), zzglw.zzf(t2, n2))) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 7: {
                        if (!this.zzc(t, t2, n) || zzglw.zzc(t, n2) != zzglw.zzc(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 6: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 5: {
                        if (!this.zzc(t, t2, n) || zzglw.zzb(t, n2) != zzglw.zzb(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 4: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 3: {
                        if (!this.zzc(t, t2, n) || zzglw.zzb(t, n2) != zzglw.zzb(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 2: {
                        if (!this.zzc(t, t2, n) || zzglw.zzb(t, n2) != zzglw.zzb(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 1: {
                        if (!this.zzc(t, t2, n) || zzglw.zza(t, n2) != zzglw.zza(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                    case 0: {
                        if (!this.zzc(t, t2, n) || zzglw.zzb(t, n2) != zzglw.zzb(t2, n2)) {
                            break;
                        }
                        break Label_0966;
                    }
                }
                b = false;
            }
            if (!b) {
                return false;
            }
            n += 4;
        }
    }
    
    @Override
    public final int zzb(final T t) {
        int n;
        for (int i = n = 0; i < this.zzb.length; i += 4) {
            final int zza = this.zza(i);
            final int n2 = this.zzb[i];
            final long n3 = 0xFFFFF & zza;
            int n4 = 0;
        Label_1854:
            while (true) {
                switch ((zza & 0xFF00000) >>> 20) {
                    default: {
                        n4 = 0;
                        break Label_1854;
                    }
                    case 68: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzf(n2, (zzgkg)zzglw.zzf(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 67: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzf(n2, zze(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 66: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzh(n2, zzd(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 65: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzh(n2, 0L);
                            break Label_1854;
                        }
                        break;
                    }
                    case 64: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzj(n2, 0);
                            break Label_1854;
                        }
                        break;
                    }
                    case 63: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzk(n2, zzd(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 62: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzg(n2, zzd(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 61: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzc(n2, (zzgho)zzglw.zzf(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 60: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgla.zza(n2, zzglw.zzf(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 59: {
                        if (!this.zza(t, n2, i)) {
                            break;
                        }
                        final Object zzf = zzglw.zzf(t, n3);
                        if (zzf instanceof zzgho) {
                            n4 = zzgic.zzc(n2, (zzgho)zzf);
                            break Label_1854;
                        }
                        n4 = zzgic.zzb(n2, (String)zzf);
                        break Label_1854;
                    }
                    case 58: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzb(n2, true);
                            break Label_1854;
                        }
                        break;
                    }
                    case 57: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzi(n2, 0);
                            break Label_1854;
                        }
                        break;
                    }
                    case 56: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzg(n2, 0L);
                            break Label_1854;
                        }
                        break;
                    }
                    case 55: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzf(n2, zzd(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 54: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zze(n2, zze(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 53: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzd(n2, zze(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 52: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzb(n2, 0.0f);
                            break Label_1854;
                        }
                        break;
                    }
                    case 51: {
                        if (this.zza(t, n2, i)) {
                            n4 = zzgic.zzb(n2, 0.0);
                            break Label_1854;
                        }
                        break;
                    }
                    case 50: {
                        n4 = this.zzo.zza(n2, zzglw.zzf(t, n3), this.zzp.zza(n2));
                        break Label_1854;
                    }
                    case 49: {
                        n4 = zzgla.zzd(n2, zza(t, n3));
                        break Label_1854;
                    }
                    case 48: {
                        n4 = zzgla.zzc(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 47: {
                        n4 = zzgla.zzg(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 46: {
                        n4 = zzgla.zzi(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 45: {
                        n4 = zzgla.zzh(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 44: {
                        n4 = zzgla.zzd(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 43: {
                        n4 = zzgla.zzf(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 42: {
                        n4 = zzgla.zzj(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 41: {
                        n4 = zzgla.zzh(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 40: {
                        n4 = zzgla.zzi(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 39: {
                        n4 = zzgla.zze(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 38: {
                        n4 = zzgla.zzb(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 37: {
                        n4 = zzgla.zza(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 36: {
                        n4 = zzgla.zzh(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 35: {
                        n4 = zzgla.zzi(n2, zza(t, n3), true);
                        break Label_1854;
                    }
                    case 34: {
                        n4 = zzgla.zzc(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 33: {
                        n4 = zzgla.zzg(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 32: {
                        n4 = zzgla.zzi(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 31: {
                        n4 = zzgla.zzh(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 30: {
                        n4 = zzgla.zzd(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 29: {
                        n4 = zzgla.zzf(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 28: {
                        n4 = zzgla.zzc(n2, zza(t, n3));
                        break Label_1854;
                    }
                    case 27: {
                        n4 = zzgla.zzb(n2, zza(t, n3));
                        break Label_1854;
                    }
                    case 26: {
                        n4 = zzgla.zza(n2, zza(t, n3));
                        break Label_1854;
                    }
                    case 25: {
                        n4 = zzgla.zzj(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 24: {
                        n4 = zzgla.zzh(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 23: {
                        n4 = zzgla.zzi(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 22: {
                        n4 = zzgla.zze(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 21: {
                        n4 = zzgla.zzb(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 20: {
                        n4 = zzgla.zza(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 19: {
                        n4 = zzgla.zzh(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 18: {
                        n4 = zzgla.zzi(n2, zza(t, n3), false);
                        break Label_1854;
                    }
                    case 17: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzf(n2, (zzgkg)zzglw.zzf(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 16: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzf(n2, zzglw.zzb(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 15: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzh(n2, zzglw.zza(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 14: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzh(n2, 0L);
                            break Label_1854;
                        }
                        break;
                    }
                    case 13: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzj(n2, 0);
                            break Label_1854;
                        }
                        break;
                    }
                    case 12: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzk(n2, zzglw.zza(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 11: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzg(n2, zzglw.zza(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 10: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzc(n2, (zzgho)zzglw.zzf(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 9: {
                        if (this.zza(t, i)) {
                            n4 = zzgla.zza(n2, zzglw.zzf(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 8: {
                        if (!this.zza(t, i)) {
                            break;
                        }
                        final Object zzf2 = zzglw.zzf(t, n3);
                        if (zzf2 instanceof zzgho) {
                            n4 = zzgic.zzc(n2, (zzgho)zzf2);
                            break Label_1854;
                        }
                        n4 = zzgic.zzb(n2, (String)zzf2);
                        break Label_1854;
                    }
                    case 7: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzb(n2, true);
                            break Label_1854;
                        }
                        break;
                    }
                    case 6: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzi(n2, 0);
                            break Label_1854;
                        }
                        break;
                    }
                    case 5: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzg(n2, 0L);
                            break Label_1854;
                        }
                        break;
                    }
                    case 4: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzf(n2, zzglw.zza(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 3: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zze(n2, zzglw.zzb(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 2: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzd(n2, zzglw.zzb(t, n3));
                            break Label_1854;
                        }
                        break;
                    }
                    case 1: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzb(n2, 0.0f);
                            break Label_1854;
                        }
                        break;
                    }
                    case 0: {
                        if (this.zza(t, i)) {
                            n4 = zzgic.zzb(n2, 0.0);
                            break Label_1854;
                        }
                        break;
                    }
                }
                continue;
            }
            n += n4;
        }
        final zzglq<?, ?> zzj = this.zzj;
        int n5 = n + zzj.zzf(zzj.zzb(t));
        if (this.zzk) {
            n5 += this.zzl.zza(t).zzh();
        }
        return n5;
    }
    
    @Override
    public final void zzb(final T t, final T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzb.length; i += 4) {
                final int zza = this.zza(i);
                final long n = 0xFFFFF & zza;
                final int n2 = this.zzb[i];
                switch ((zza & 0xFF00000) >>> 20) {
                    case 68: {
                        this.zzb(t, t2, i);
                        break;
                    }
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67: {
                        if (this.zza(t2, n2, i)) {
                            zzglw.zza(t, n, zzglw.zzf(t2, n));
                            this.zzb(t, n2, i);
                            break;
                        }
                        break;
                    }
                    case 60: {
                        this.zzb(t, t2, i);
                        break;
                    }
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59: {
                        if (this.zza(t2, n2, i)) {
                            zzglw.zza(t, n, zzglw.zzf(t2, n));
                            this.zzb(t, n2, i);
                            break;
                        }
                        break;
                    }
                    case 50: {
                        zzgla.zza(this.zzo, t, t2, n);
                        break;
                    }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49: {
                        this.zzh.zza(t, t2, n);
                        break;
                    }
                    case 17: {
                        this.zza(t, t2, i);
                        break;
                    }
                    case 16: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzb(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 15: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zza(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 14: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzb(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 13: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zza(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 12: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zza(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 11: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zza(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 10: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzf(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 9: {
                        this.zza(t, t2, i);
                        break;
                    }
                    case 8: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzf(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 7: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzc(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 6: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zza(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 5: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzb(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 4: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zza(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 3: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzb(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 2: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzb(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 1: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zzd(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                    case 0: {
                        if (this.zza(t2, i)) {
                            zzglw.zza(t, n, zzglw.zze(t2, n));
                            this.zzb(t, i);
                            break;
                        }
                        break;
                    }
                }
            }
            if (!this.zzn) {
                zzgla.zza(this.zzj, t, t2);
                if (this.zzk) {
                    zzgla.zza(this.zzl, t, t2);
                }
            }
            return;
        }
        throw new NullPointerException();
    }
}
