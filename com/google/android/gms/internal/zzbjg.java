package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import com.google.android.gms.common.server.response.FastJsonResponse;
import android.os.Parcelable.Creator;

public final class zzbjg extends zzbid
{
    public static final Parcelable.Creator<zzbjg> CREATOR;
    private final int zza;
    private final zzbji zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzbjh();
    }
    
    zzbjg(final int zza, final zzbji zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    private zzbjg(final zzbji zzb) {
        this.zza = 1;
        this.zzb = zzb;
    }
    
    public static zzbjg zza(final FastJsonResponse.FieldConverter<?, ?> fieldConverter) {
        if (fieldConverter instanceof zzbji) {
            return new zzbjg((zzbji)fieldConverter);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, zza);
    }
    
    public final FastJsonResponse.FieldConverter<?, ?> zza() {
        if (this.zzb != null) {
            return this.zzb;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }
}
