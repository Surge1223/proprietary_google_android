package com.google.android.gms.internal;

import java.lang.reflect.Field;

final class zzgku implements zzgke
{
    private final zzgkg zza;
    private final String zzb;
    private final zzgkv zzc;
    
    zzgku(final zzgkg zza, final String zzb, final Object[] array) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = new zzgkv(zza.getClass(), zzb, array);
    }
    
    @Override
    public final int zza() {
        if ((this.zzc.zzd & 0x1) == 0x1) {
            return zzgiw.zzg.zzi;
        }
        return zzgiw.zzg.zzj;
    }
    
    @Override
    public final boolean zzb() {
        return (this.zzc.zzd & 0x2) == 0x2;
    }
    
    @Override
    public final zzgkg zzc() {
        return this.zza;
    }
    
    final zzgkv zzd() {
        return this.zzc;
    }
    
    public final int zze() {
        return this.zzc.zzh;
    }
    
    public final int zzf() {
        return this.zzc.zzi;
    }
    
    public final int zzg() {
        return this.zzc.zze;
    }
    
    public final int zzh() {
        return this.zzc.zzj;
    }
    
    public final int zzi() {
        return this.zzc.zzm;
    }
    
    final int[] zzj() {
        return this.zzc.zzn;
    }
    
    public final int zzk() {
        return this.zzc.zzl;
    }
    
    public final int zzl() {
        return this.zzc.zzk;
    }
}
