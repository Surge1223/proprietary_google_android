package com.google.android.gms.internal;

final class zzgio
{
    static {
        zzb = new int[zzgme.values().length];
        try {
            zzgio.zzb[zzgme.zza.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            zzgio.zzb[zzgme.zzb.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError2) {}
        try {
            zzgio.zzb[zzgme.zzc.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError3) {}
        try {
            zzgio.zzb[zzgme.zzd.ordinal()] = 4;
        }
        catch (NoSuchFieldError noSuchFieldError4) {}
        try {
            zzgio.zzb[zzgme.zze.ordinal()] = 5;
        }
        catch (NoSuchFieldError noSuchFieldError5) {}
        try {
            zzgio.zzb[zzgme.zzf.ordinal()] = 6;
        }
        catch (NoSuchFieldError noSuchFieldError6) {}
        try {
            zzgio.zzb[zzgme.zzg.ordinal()] = 7;
        }
        catch (NoSuchFieldError noSuchFieldError7) {}
        try {
            zzgio.zzb[zzgme.zzh.ordinal()] = 8;
        }
        catch (NoSuchFieldError noSuchFieldError8) {}
        try {
            zzgio.zzb[zzgme.zzj.ordinal()] = 9;
        }
        catch (NoSuchFieldError noSuchFieldError9) {}
        try {
            zzgio.zzb[zzgme.zzk.ordinal()] = 10;
        }
        catch (NoSuchFieldError noSuchFieldError10) {}
        try {
            zzgio.zzb[zzgme.zzi.ordinal()] = 11;
        }
        catch (NoSuchFieldError noSuchFieldError11) {}
        try {
            zzgio.zzb[zzgme.zzl.ordinal()] = 12;
        }
        catch (NoSuchFieldError noSuchFieldError12) {}
        try {
            zzgio.zzb[zzgme.zzm.ordinal()] = 13;
        }
        catch (NoSuchFieldError noSuchFieldError13) {}
        try {
            zzgio.zzb[zzgme.zzo.ordinal()] = 14;
        }
        catch (NoSuchFieldError noSuchFieldError14) {}
        try {
            zzgio.zzb[zzgme.zzp.ordinal()] = 15;
        }
        catch (NoSuchFieldError noSuchFieldError15) {}
        try {
            zzgio.zzb[zzgme.zzq.ordinal()] = 16;
        }
        catch (NoSuchFieldError noSuchFieldError16) {}
        try {
            zzgio.zzb[zzgme.zzr.ordinal()] = 17;
        }
        catch (NoSuchFieldError noSuchFieldError17) {}
        try {
            zzgio.zzb[zzgme.zzn.ordinal()] = 18;
        }
        catch (NoSuchFieldError noSuchFieldError18) {}
        zza = new int[zzgmj.values().length];
        try {
            zzgio.zza[zzgmj.zza.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError19) {}
        try {
            zzgio.zza[zzgmj.zzb.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError20) {}
        try {
            zzgio.zza[zzgmj.zzc.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError21) {}
        try {
            zzgio.zza[zzgmj.zzd.ordinal()] = 4;
        }
        catch (NoSuchFieldError noSuchFieldError22) {}
        try {
            zzgio.zza[zzgmj.zze.ordinal()] = 5;
        }
        catch (NoSuchFieldError noSuchFieldError23) {}
        try {
            zzgio.zza[zzgmj.zzf.ordinal()] = 6;
        }
        catch (NoSuchFieldError noSuchFieldError24) {}
        try {
            zzgio.zza[zzgmj.zzg.ordinal()] = 7;
        }
        catch (NoSuchFieldError noSuchFieldError25) {}
        try {
            zzgio.zza[zzgmj.zzh.ordinal()] = 8;
        }
        catch (NoSuchFieldError noSuchFieldError26) {}
        try {
            zzgio.zza[zzgmj.zzi.ordinal()] = 9;
        }
        catch (NoSuchFieldError noSuchFieldError27) {}
    }
}
