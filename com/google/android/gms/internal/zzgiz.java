package com.google.android.gms.internal;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzgiz extends zzghf<Integer> implements zzgjd, RandomAccess
{
    private static final zzgiz zza;
    private int[] zzb;
    private int zzc;
    
    static {
        (zza = new zzgiz()).zzb();
    }
    
    zzgiz() {
        this(new int[10], 0);
    }
    
    private zzgiz(final int[] zzb, final int zzc) {
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    private final void zza(final int n, final int n2) {
        this.zzc();
        if (n >= 0 && n <= this.zzc) {
            if (this.zzc < this.zzb.length) {
                System.arraycopy(this.zzb, n, this.zzb, n + 1, this.zzc - n);
            }
            else {
                final int[] zzb = new int[this.zzc * 3 / 2 + 1];
                System.arraycopy(this.zzb, 0, zzb, 0, n);
                System.arraycopy(this.zzb, n, zzb, n + 1, this.zzc - n);
                this.zzb = zzb;
            }
            this.zzb[n] = n2;
            ++this.zzc;
            ++this.modCount;
            return;
        }
        throw new IndexOutOfBoundsException(this.zzf(n));
    }
    
    private final void zze(final int n) {
        if (n >= 0 && n < this.zzc) {
            return;
        }
        throw new IndexOutOfBoundsException(this.zzf(n));
    }
    
    private final String zzf(final int n) {
        final int zzc = this.zzc;
        final StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(n);
        sb.append(", Size:");
        sb.append(zzc);
        return sb.toString();
    }
    
    @Override
    public final boolean addAll(final Collection<? extends Integer> collection) {
        this.zzc();
        zzgja.zza(collection);
        if (!(collection instanceof zzgiz)) {
            return super.addAll(collection);
        }
        final zzgiz zzgiz = (zzgiz)collection;
        if (zzgiz.zzc == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.zzc >= zzgiz.zzc) {
            final int zzc = this.zzc + zzgiz.zzc;
            if (zzc > this.zzb.length) {
                this.zzb = Arrays.copyOf(this.zzb, zzc);
            }
            System.arraycopy(zzgiz.zzb, 0, this.zzb, this.zzc, zzgiz.zzc);
            this.zzc = zzc;
            ++this.modCount;
            return true;
        }
        throw new OutOfMemoryError();
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzgiz)) {
            return super.equals(o);
        }
        final zzgiz zzgiz = (zzgiz)o;
        if (this.zzc != zzgiz.zzc) {
            return false;
        }
        final int[] zzb = zzgiz.zzb;
        for (int i = 0; i < this.zzc; ++i) {
            if (this.zzb[i] != zzb[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final int hashCode() {
        int n = 1;
        for (int i = 0; i < this.zzc; ++i) {
            n = n * 31 + this.zzb[i];
        }
        return n;
    }
    
    @Override
    public final boolean remove(final Object o) {
        this.zzc();
        for (int i = 0; i < this.zzc; ++i) {
            if (o.equals(this.zzb[i])) {
                System.arraycopy(this.zzb, i + 1, this.zzb, i, this.zzc - i);
                --this.zzc;
                ++this.modCount;
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final int size() {
        return this.zzc;
    }
    
    public final zzgjd zzb(final int n) {
        if (n >= this.zzc) {
            return new zzgiz(Arrays.copyOf(this.zzb, n), this.zzc);
        }
        throw new IllegalArgumentException();
    }
    
    public final int zzc(final int n) {
        this.zze(n);
        return this.zzb[n];
    }
}
