package com.google.android.gms.internal;

final class zzglm
{
    static String zza(final zzgho zzgho) {
        final zzgln zzgln = new zzgln(zzgho);
        final StringBuilder sb = new StringBuilder(zzgln.zza());
        for (int i = 0; i < zzgln.zza(); ++i) {
            final byte zza = zzgln.zza(i);
            if (zza != 34) {
                if (zza != 39) {
                    if (zza != 92) {
                        switch (zza) {
                            default: {
                                if (zza >= 32 && zza <= 126) {
                                    sb.append((char)zza);
                                    break;
                                }
                                sb.append('\\');
                                sb.append((char)((zza >>> 6 & 0x3) + 48));
                                sb.append((char)((zza >>> 3 & 0x7) + 48));
                                sb.append((char)(48 + (zza & 0x7)));
                                break;
                            }
                            case 13: {
                                sb.append("\\r");
                                break;
                            }
                            case 12: {
                                sb.append("\\f");
                                break;
                            }
                            case 11: {
                                sb.append("\\v");
                                break;
                            }
                            case 10: {
                                sb.append("\\n");
                                break;
                            }
                            case 9: {
                                sb.append("\\t");
                                break;
                            }
                            case 8: {
                                sb.append("\\b");
                                break;
                            }
                            case 7: {
                                sb.append("\\a");
                                break;
                            }
                        }
                    }
                    else {
                        sb.append("\\\\");
                    }
                }
                else {
                    sb.append("\\'");
                }
            }
            else {
                sb.append("\\\"");
            }
        }
        return sb.toString();
    }
}
