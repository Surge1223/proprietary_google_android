package com.google.android.gms.internal;

import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEventAdapter;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import android.os.RemoteException;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import java.util.Map;

public final class zztj extends zztl
{
    private Map<Class<?>, Object> zza;
    
    private final <NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> zztn zzc(final String s) throws RemoteException {
        try {
            final Class<?> forName = Class.forName(s, false, zztj.class.getClassLoader());
            if (MediationAdapter.class.isAssignableFrom(forName)) {
                final MediationAdapter mediationAdapter = (MediationAdapter)forName.newInstance();
                return new zzum<Object, Object>(mediationAdapter, (NetworkExtras)this.zza.get(mediationAdapter.getAdditionalParametersType()));
            }
            if (com.google.android.gms.ads.mediation.MediationAdapter.class.isAssignableFrom(forName)) {
                return new zzuh((com.google.android.gms.ads.mediation.MediationAdapter)forName.newInstance());
            }
            final StringBuilder sb = new StringBuilder(64 + String.valueOf(s).length());
            sb.append("Could not instantiate mediation adapter: ");
            sb.append(s);
            sb.append(" (not a valid adapter).");
            zzaid.zze(sb.toString());
            throw new RemoteException();
        }
        catch (Throwable t) {
            return this.zzd(s);
        }
    }
    
    private final zztn zzd(final String s) throws RemoteException {
        try {
            zzaid.zzb("Reflection failed, retrying using direct instantiation");
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(s)) {
                return new zzuh(new AdMobAdapter());
            }
            if ("com.google.ads.mediation.AdUrlAdapter".equals(s)) {
                return new zzuh(new AdUrlAdapter());
            }
            if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(s)) {
                return new zzuh(new CustomEventAdapter());
            }
            if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(s)) {
                final com.google.ads.mediation.customevent.CustomEventAdapter customEventAdapter = new com.google.ads.mediation.customevent.CustomEventAdapter();
                return new zzum<Object, Object>((MediationAdapter<NetworkExtras, MediationServerParameters>)customEventAdapter, (NetworkExtras)this.zza.get(customEventAdapter.getAdditionalParametersType()));
            }
        }
        catch (Throwable t) {
            final StringBuilder sb = new StringBuilder(43 + String.valueOf(s).length());
            sb.append("Could not instantiate mediation adapter: ");
            sb.append(s);
            sb.append(". ");
            zzaid.zzc(sb.toString(), t);
        }
        throw new RemoteException();
    }
    
    @Override
    public final zztn zza(final String s) throws RemoteException {
        return this.zzc(s);
    }
    
    public final void zza(final Map<Class<?>, Object> zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean zzb(final String s) throws RemoteException {
        try {
            return CustomEvent.class.isAssignableFrom(Class.forName(s, false, zztj.class.getClassLoader()));
        }
        catch (Throwable t) {
            final StringBuilder sb = new StringBuilder(80 + String.valueOf(s).length());
            sb.append("Could not load custom event implementation class: ");
            sb.append(s);
            sb.append(", assuming old implementation.");
            zzaid.zze(sb.toString());
            return false;
        }
    }
}
