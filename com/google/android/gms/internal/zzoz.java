package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzoz extends zzey implements zzox
{
    zzoz(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IOnAppInstallAdLoadedListener");
    }
    
    @Override
    public final void zza(final zzol zzol) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzol);
        this.zzb(1, a_);
    }
}
