package com.google.android.gms.internal;

public interface zzgkh extends zzgki, Cloneable
{
    zzgkh zza(final zzgkg p0);
    
    zzgkg zze();
    
    zzgkg zzf();
}
