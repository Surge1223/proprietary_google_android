package com.google.android.gms.internal;

import com.google.android.gms.ads.reward.RewardItem;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;

public final class zzadh implements MediationRewardedVideoAdListener
{
    private final zzade zza;
    
    public zzadh(final zzade zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onAdClosed(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onAdClosed must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdClosed.");
        try {
            this.zza.zze(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdClosed.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdFailedToLoad(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, final int n) {
        zzau.zzb("onAdFailedToLoad must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdFailedToLoad.");
        try {
            this.zza.zzb(zzn.zza(mediationRewardedVideoAdAdapter), n);
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLeftApplication(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onAdLeftApplication must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLeftApplication.");
        try {
            this.zza.zzg(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLeftApplication.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdLoaded(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onAdLoaded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdLoaded.");
        try {
            this.zza.zzb(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdLoaded.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onAdOpened(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onAdOpened must be called on the main UI thread.");
        zzaid.zzb("Adapter called onAdOpened.");
        try {
            this.zza.zzc(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdOpened.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onInitializationSucceeded(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onInitializationSucceeded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onInitializationSucceeded.");
        try {
            this.zza.zza(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onInitializationSucceeded.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onRewarded(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, final RewardItem rewardItem) {
        zzau.zzb("onRewarded must be called on the main UI thread.");
        zzaid.zzb("Adapter called onRewarded.");
        Label_0049: {
            if (rewardItem != null) {
                Label_0079: {
                    try {
                        this.zza.zza(zzn.zza(mediationRewardedVideoAdAdapter), new zzadi(rewardItem));
                        return;
                    }
                    catch (RemoteException ex) {
                        break Label_0079;
                    }
                    break Label_0049;
                }
                final RemoteException ex;
                zzaid.zzc("Could not call onRewarded.", (Throwable)ex);
                return;
            }
        }
        this.zza.zza(zzn.zza(mediationRewardedVideoAdAdapter), new zzadi("", 1));
    }
    
    @Override
    public final void onVideoCompleted(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onVideoCompleted must be called on the main UI thread.");
        zzaid.zzb("Adapter called onVideoCompleted.");
        try {
            this.zza.zzh(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onVideoCompleted.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onVideoStarted(final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzau.zzb("onVideoStarted must be called on the main UI thread.");
        zzaid.zzb("Adapter called onVideoStarted.");
        try {
            this.zza.zzd(zzn.zza(mediationRewardedVideoAdAdapter));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onVideoStarted.", (Throwable)ex);
        }
    }
}
