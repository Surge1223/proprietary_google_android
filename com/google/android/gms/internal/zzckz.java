package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzckz extends zzbid
{
    public static final Parcelable.Creator<zzckz> CREATOR;
    private final int zza;
    private zzbb zzb;
    private byte[] zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzcla();
    }
    
    zzckz(final int zza, final byte[] zzc) {
        this.zza = zza;
        this.zzb = null;
        this.zzc = zzc;
        this.zzb();
    }
    
    private final void zzb() {
        if (this.zzb == null && this.zzc != null) {
            return;
        }
        if (this.zzb != null && this.zzc == null) {
            return;
        }
        if (this.zzb != null && this.zzc != null) {
            throw new IllegalStateException("Invalid internal representation - full");
        }
        if (this.zzb == null && this.zzc == null) {
            throw new IllegalStateException("Invalid internal representation - empty");
        }
        throw new IllegalStateException("Impossible");
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        byte[] array;
        if (this.zzc != null) {
            array = this.zzc;
        }
        else {
            array = zzgmu.toByteArray(this.zzb);
        }
        zzbig.zza(parcel, 2, array, false);
        zzbig.zza(parcel, zza);
    }
}
