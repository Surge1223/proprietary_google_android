package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Bundle;
import com.google.android.gms.ads.zzb;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.AdSize;
import com.google.android.gms.ads.internal.client.zzj;
import java.util.List;
import com.google.ads.mediation.MediationInterstitialListener;
import android.app.Activity;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.dynamic.zzn;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.internal.client.zzf;
import java.util.Iterator;
import java.util.Map;
import android.os.RemoteException;
import java.util.HashMap;
import org.json.JSONObject;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;

public final class zzum<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzto
{
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zza;
    private final NETWORK_EXTRAS zzb;
    
    public zzum(final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zza, final NETWORK_EXTRAS zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    private final SERVER_PARAMETERS zza(final String s, final int n, final String s2) throws RemoteException {
        HashMap<String, String> hashMap = null;
        Label_0087: {
            Label_0078: {
                if (s != null) {
                    Label_0121: {
                        try {
                            final JSONObject jsonObject = new JSONObject(s);
                            hashMap = new HashMap<String, String>(jsonObject.length());
                            final Iterator keys = jsonObject.keys();
                            while (keys.hasNext()) {
                                final String s3 = keys.next();
                                hashMap.put(s3, jsonObject.getString(s3));
                            }
                            break Label_0087;
                        }
                        catch (Throwable t) {
                            break Label_0121;
                        }
                        break Label_0078;
                    }
                    final Throwable t;
                    zzaid.zzc("Could not get MediationServerParameters.", t);
                    throw new RemoteException();
                }
            }
            hashMap = new HashMap<String, String>(0);
        }
        final Class<SERVER_PARAMETERS> serverParametersType = this.zza.getServerParametersType();
        MediationServerParameters mediationServerParameters = null;
        if (serverParametersType != null) {
            mediationServerParameters = serverParametersType.newInstance();
            mediationServerParameters.load(hashMap);
        }
        return (SERVER_PARAMETERS)mediationServerParameters;
    }
    
    private static boolean zza(final zzf zzf) {
        if (!zzf.zzf) {
            zzx.zza();
            if (!zzaht.zza()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final IObjectWrapper zza() throws RemoteException {
        if (!(this.zza instanceof MediationBannerAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationBannerAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationBannerAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        try {
            return zzn.zza(((MediationBannerAdapter)this.zza).getBannerView());
        }
        catch (Throwable t) {
            zzaid.zzc("Could not get banner view from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final zzf zzf, final String s) {
    }
    
    @Override
    public final void zza(final zzf zzf, final String s, final String s2) {
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final zzade zzade, final String s2) throws RemoteException {
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final zztq zztq) throws RemoteException {
        this.zza(objectWrapper, zzf, s, null, zztq);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final String s2, final zztq zztq) throws RemoteException {
        if (!(this.zza instanceof MediationInterstitialAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationInterstitialAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter)this.zza).requestInterstitialAd(new zzun<Object, Object>(zztq), zzn.zza(objectWrapper), this.zza(s, zzf.zzg, s2), zzuz.zza(zzf, zza(zzf)), this.zzb);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not request interstitial ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final String s2, final zztq zztq, final zznm zznm, final List<String> list) {
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzj zzj, final zzf zzf, final String s, final zztq zztq) throws RemoteException {
        this.zza(objectWrapper, zzj, zzf, s, null, zztq);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzj zzj, final zzf zzf, final String s, final String s2, final zztq zztq) throws RemoteException {
        if (!(this.zza instanceof MediationBannerAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationBannerAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationBannerAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Requesting banner ad from adapter.");
        try {
            final MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter)this.zza;
            final zzun<Object, Object> zzun = new zzun<Object, Object>(zztq);
            final Activity activity = zzn.zza(objectWrapper);
            final MediationServerParameters zza = this.zza(s, zzf.zzg, s2);
            final AdSize[] array = new AdSize[6];
            final AdSize smart_BANNER = AdSize.SMART_BANNER;
            int i = 0;
            array[0] = smart_BANNER;
            array[1] = AdSize.BANNER;
            array[2] = AdSize.IAB_MRECT;
            array[3] = AdSize.IAB_BANNER;
            array[4] = AdSize.IAB_LEADERBOARD;
            array[5] = AdSize.IAB_WIDE_SKYSCRAPER;
            while (true) {
                while (i < 6) {
                    if (array[i].getWidth() == zzj.zze && array[i].getHeight() == zzj.zzb) {
                        final AdSize adSize = array[i];
                        mediationBannerAdapter.requestBannerAd(zzun, activity, zza, adSize, zzuz.zza(zzf, zza(zzf)), this.zzb);
                        return;
                    }
                    ++i;
                }
                final AdSize adSize = new AdSize(com.google.android.gms.ads.zzb.zza(zzj.zze, zzj.zzb, zzj.zza));
                continue;
            }
        }
        catch (Throwable t) {
            zzaid.zzc("Could not request banner ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzade zzade, final List<String> list) {
    }
    
    @Override
    public final void zza(final boolean b) {
    }
    
    @Override
    public final void zzb() throws RemoteException {
        if (!(this.zza instanceof MediationInterstitialAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationInterstitialAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter)this.zza).showInterstitial();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not show interstitial from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zzc() throws RemoteException {
        try {
            this.zza.destroy();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not destroy adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zzd() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final void zze() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final void zzf() {
    }
    
    @Override
    public final boolean zzg() {
        return true;
    }
    
    @Override
    public final zztw zzh() {
        return null;
    }
    
    @Override
    public final zztz zzi() {
        return null;
    }
    
    @Override
    public final Bundle zzj() {
        return new Bundle();
    }
    
    @Override
    public final Bundle zzk() {
        return new Bundle();
    }
    
    @Override
    public final Bundle zzl() {
        return new Bundle();
    }
    
    @Override
    public final boolean zzm() {
        return false;
    }
    
    @Override
    public final zzot zzn() {
        return null;
    }
    
    @Override
    public final zzbh zzo() {
        return null;
    }
    
    @Override
    public final zzuc zzp() {
        return null;
    }
}
