package com.google.android.gms.internal;

public enum zzgmj
{
    zza((Object)0), 
    zzb((Object)0L), 
    zzc((Object)0.0f), 
    zzd((Object)0.0), 
    zze((Object)false), 
    zzf((Object)""), 
    zzg((Object)zzgho.zza), 
    zzh((Object)null), 
    zzi((Object)null);
    
    private final Object zzj;
    
    private zzgmj(final Object zzj) {
        this.zzj = zzj;
    }
}
