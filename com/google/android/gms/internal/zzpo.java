package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzpo extends zzey implements zzpm
{
    zzpo(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IOnUnifiedNativeAdLoadedListener");
    }
    
    @Override
    public final void zza(final zzpp zzpp) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzpp);
        this.zzb(1, a_);
    }
}
