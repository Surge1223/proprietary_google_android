package com.google.android.gms.internal;

import android.os.Bundle;
import android.util.Pair;
import java.util.List;

public final class zzchd
{
    public static Bundle zza(final List<Pair<String, String>> list) {
        if (list == null) {
            return null;
        }
        final int size = list.size();
        final Bundle bundle = new Bundle(size);
        for (int i = 0; i < size; ++i) {
            final Pair<String, String> pair = list.get(i);
            bundle.putString((String)pair.first, (String)pair.second);
        }
        return bundle;
    }
}
