package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Bundle;
import java.util.Iterator;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.google.android.gms.dynamic.zzn;
import android.view.View;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;

public final class zzuk extends zzua
{
    private final NativeContentAdMapper zza;
    
    public zzuk(final NativeContentAdMapper zza) {
        this.zza = zza;
    }
    
    @Override
    public final String zza() {
        return this.zza.getHeadline();
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) {
        this.zza.handleClick(zzn.zza(objectWrapper));
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) {
        this.zza.trackViews(zzn.zza(objectWrapper), zzn.zza(objectWrapper2), zzn.zza(objectWrapper3));
    }
    
    @Override
    public final List zzb() {
        final List<NativeAd.Image> images = this.zza.getImages();
        if (images != null) {
            final ArrayList<zzmo> list = new ArrayList<zzmo>();
            for (final NativeAd.Image image : images) {
                list.add(new zzmo(image.getDrawable(), image.getUri(), image.getScale()));
            }
            return list;
        }
        return null;
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper) {
        this.zza.trackView(zzn.zza(objectWrapper));
    }
    
    @Override
    public final String zzc() {
        return this.zza.getBody();
    }
    
    @Override
    public final void zzc(final IObjectWrapper objectWrapper) {
        this.zza.untrackView(zzn.zza(objectWrapper));
    }
    
    @Override
    public final zznx zzd() {
        final NativeAd.Image logo = this.zza.getLogo();
        if (logo != null) {
            return new zzmo(logo.getDrawable(), logo.getUri(), logo.getScale());
        }
        return null;
    }
    
    @Override
    public final String zze() {
        return this.zza.getCallToAction();
    }
    
    @Override
    public final String zzf() {
        return this.zza.getAdvertiser();
    }
    
    @Override
    public final void zzg() {
        this.zza.recordImpression();
    }
    
    @Override
    public final boolean zzh() {
        return this.zza.getOverrideImpressionRecording();
    }
    
    @Override
    public final boolean zzi() {
        return this.zza.getOverrideClickHandling();
    }
    
    @Override
    public final Bundle zzj() {
        return this.zza.getExtras();
    }
    
    @Override
    public final IObjectWrapper zzk() {
        final View adChoicesContent = this.zza.getAdChoicesContent();
        if (adChoicesContent == null) {
            return null;
        }
        return zzn.zza(adChoicesContent);
    }
    
    @Override
    public final zzbh zzl() {
        if (this.zza.zzb() != null) {
            return this.zza.zzb().zza();
        }
        return null;
    }
    
    @Override
    public final zznt zzm() {
        return null;
    }
    
    @Override
    public final IObjectWrapper zzn() {
        final View zza = this.zza.zza();
        if (zza == null) {
            return null;
        }
        return zzn.zza(zza);
    }
    
    @Override
    public final IObjectWrapper zzo() {
        return null;
    }
}
