package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzbh;
import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzpp extends IInterface
{
    String zza() throws RemoteException;
    
    List zzb() throws RemoteException;
    
    String zzc() throws RemoteException;
    
    zznx zzd() throws RemoteException;
    
    String zze() throws RemoteException;
    
    String zzf() throws RemoteException;
    
    double zzg() throws RemoteException;
    
    String zzh() throws RemoteException;
    
    String zzi() throws RemoteException;
    
    zzbh zzj() throws RemoteException;
    
    IObjectWrapper zzp() throws RemoteException;
    
    zznt zzr() throws RemoteException;
}
