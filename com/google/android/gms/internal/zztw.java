package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Bundle;
import java.util.List;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IInterface;

public interface zztw extends IInterface
{
    String zza() throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final IObjectWrapper p1, final IObjectWrapper p2) throws RemoteException;
    
    List zzb() throws RemoteException;
    
    void zzb(final IObjectWrapper p0) throws RemoteException;
    
    String zzc() throws RemoteException;
    
    void zzc(final IObjectWrapper p0) throws RemoteException;
    
    zznx zzd() throws RemoteException;
    
    String zze() throws RemoteException;
    
    double zzf() throws RemoteException;
    
    String zzg() throws RemoteException;
    
    String zzh() throws RemoteException;
    
    void zzi() throws RemoteException;
    
    boolean zzj() throws RemoteException;
    
    boolean zzk() throws RemoteException;
    
    Bundle zzl() throws RemoteException;
    
    zzbh zzm() throws RemoteException;
    
    IObjectWrapper zzn() throws RemoteException;
    
    zznt zzo() throws RemoteException;
    
    IObjectWrapper zzp() throws RemoteException;
    
    IObjectWrapper zzq() throws RemoteException;
}
