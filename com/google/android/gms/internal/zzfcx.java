package com.google.android.gms.internal;

import android.util.Log;
import android.database.ContentObserver;
import android.os.Handler;
import java.util.TreeMap;
import android.database.Cursor;
import java.util.Map;
import android.content.ContentResolver;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import android.net.Uri;
import java.util.regex.Pattern;

public class zzfcx
{
    public static final Pattern zza;
    public static final Pattern zzb;
    private static final Uri zzc;
    private static final Uri zzd;
    private static final AtomicBoolean zze;
    private static HashMap<String, String> zzf;
    private static final HashMap<String, Boolean> zzg;
    private static final HashMap<String, Integer> zzh;
    private static final HashMap<String, Long> zzi;
    private static final HashMap<String, Float> zzj;
    private static Object zzk;
    private static boolean zzl;
    private static String[] zzm;
    
    static {
        zzc = Uri.parse("content://com.google.android.gsf.gservices");
        zzd = Uri.parse("content://com.google.android.gsf.gservices/prefix");
        zza = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
        zzb = Pattern.compile("^(0|false|f|off|no|n)$", 2);
        zze = new AtomicBoolean();
        zzg = new HashMap<String, Boolean>();
        zzh = new HashMap<String, Integer>();
        zzi = new HashMap<String, Long>();
        zzj = new HashMap<String, Float>();
        zzfcx.zzm = new String[0];
    }
    
    private static <T> T zza(final HashMap<String, T> hashMap, final String s, T t) {
        synchronized (zzfcx.class) {
            if (hashMap.containsKey(s)) {
                final T value = hashMap.get(s);
                if (value != null) {
                    t = value;
                }
                return t;
            }
            return null;
        }
    }
    
    public static String zza(final ContentResolver contentResolver, final String s, String string) {
        synchronized (zzfcx.class) {
            zza(contentResolver);
            final Object zzk = zzfcx.zzk;
            if (zzfcx.zzf.containsKey(s)) {
                String s2 = zzfcx.zzf.get(s);
                if (s2 == null) {
                    s2 = null;
                }
                return s2;
            }
            final String[] zzm = zzfcx.zzm;
            for (int length = zzm.length, i = 0; i < length; ++i) {
                if (s.startsWith(zzm[i])) {
                    if (!zzfcx.zzl || zzfcx.zzf.isEmpty()) {
                        zzfcx.zzf.putAll(zza(contentResolver, zzfcx.zzm));
                        zzfcx.zzl = true;
                        if (zzfcx.zzf.containsKey(s)) {
                            String s3 = zzfcx.zzf.get(s);
                            if (s3 == null) {
                                s3 = null;
                            }
                            return s3;
                        }
                    }
                    return null;
                }
            }
            // monitorexit(zzfcx.class)
            final Cursor query = contentResolver.query(zzfcx.zzc, (String[])null, (String)null, new String[] { s }, (String)null);
            Label_0256: {
                if (query == null) {
                    break Label_0256;
                }
                try {
                    if (!query.moveToFirst()) {
                        break Label_0256;
                    }
                    string = query.getString(1);
                    String s4;
                    if ((s4 = string) != null) {
                        s4 = string;
                        if (string.equals(null)) {
                            s4 = null;
                        }
                    }
                    zza(zzk, s, s4);
                    if (s4 == null) {
                        s4 = null;
                    }
                    if (query != null) {
                        query.close();
                    }
                    return s4;
                }
                finally {
                    if (query != null) {
                        query.close();
                    }
                    Label_0274: {
                        return null;
                    }
                    zza(zzk, s, null);
                    // iftrue(Label_0274:, query == null)
                    query.close();
                }
            }
        }
    }
    
    private static Map<String, String> zza(ContentResolver query, final String... array) {
        query = (ContentResolver)query.query(zzfcx.zzd, (String[])null, (String)null, array, (String)null);
        final TreeMap<String, String> treeMap = new TreeMap<String, String>();
        if (query == null) {
            return treeMap;
        }
        try {
            while (((Cursor)query).moveToNext()) {
                treeMap.put(((Cursor)query).getString(0), ((Cursor)query).getString(1));
            }
            return treeMap;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static void zza(final ContentResolver contentResolver) {
        if (zzfcx.zzf == null) {
            zzfcx.zze.set(false);
            zzfcx.zzf = new HashMap<String, String>();
            zzfcx.zzk = new Object();
            zzfcx.zzl = false;
            contentResolver.registerContentObserver(zzfcx.zzc, true, (ContentObserver)new zzfcy(null));
            return;
        }
        if (zzfcx.zze.getAndSet(false)) {
            zzfcx.zzf.clear();
            zzfcx.zzg.clear();
            zzfcx.zzh.clear();
            zzfcx.zzi.clear();
            zzfcx.zzj.clear();
            zzfcx.zzk = new Object();
            zzfcx.zzl = false;
        }
    }
    
    private static void zza(final Object o, final String s, final String s2) {
        synchronized (zzfcx.class) {
            if (o == zzfcx.zzk) {
                zzfcx.zzf.put(s, s2);
            }
        }
    }
    
    private static <T> void zza(final Object o, final HashMap<String, T> hashMap, final String s, final T t) {
        synchronized (zzfcx.class) {
            if (o == zzfcx.zzk) {
                hashMap.put(s, t);
                zzfcx.zzf.remove(s);
            }
        }
    }
    
    public static boolean zza(final ContentResolver contentResolver, final String s, boolean b) {
        final Object zzb = zzb(contentResolver);
        final Boolean b2 = zza(zzfcx.zzg, s, Boolean.valueOf(b));
        if (b2 != null) {
            return b2;
        }
        final String zza = zza(contentResolver, s, null);
        Boolean b3;
        if (zza != null && !zza.equals("")) {
            if (zzfcx.zza.matcher(zza).matches()) {
                b3 = true;
                b = true;
            }
            else if (zzfcx.zzb.matcher(zza).matches()) {
                b3 = false;
                b = false;
            }
            else {
                final StringBuilder sb = new StringBuilder("attempt to read gservices key ");
                sb.append(s);
                sb.append(" (value \"");
                sb.append(zza);
                sb.append("\") as boolean");
                Log.w("Gservices", sb.toString());
                b3 = b2;
            }
        }
        else {
            b3 = b2;
        }
        zza(zzb, zzfcx.zzg, s, b3);
        return b;
    }
    
    private static Object zzb(final ContentResolver contentResolver) {
        synchronized (zzfcx.class) {
            zza(contentResolver);
            return zzfcx.zzk;
        }
    }
}
