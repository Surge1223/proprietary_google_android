package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzgmu
{
    protected volatile int zzay;
    
    public zzgmu() {
        this.zzay = -1;
    }
    
    public static final void toByteArray(final zzgmu zzgmu, final byte[] array, final int n, final int n2) {
        try {
            final zzgmm zza = zzgmm.zza(array, n, n2);
            zzgmu.writeTo(zza);
            zza.zza();
        }
        catch (IOException ex) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", ex);
        }
    }
    
    public static final byte[] toByteArray(final zzgmu zzgmu) {
        final byte[] array = new byte[zzgmu.getSerializedSize()];
        toByteArray(zzgmu, array, 0, array.length);
        return array;
    }
    
    public zzgmu clone() throws CloneNotSupportedException {
        return (zzgmu)super.clone();
    }
    
    protected int computeSerializedSize() {
        return 0;
    }
    
    public int getCachedSize() {
        if (this.zzay < 0) {
            this.getSerializedSize();
        }
        return this.zzay;
    }
    
    public int getSerializedSize() {
        return this.zzay = this.computeSerializedSize();
    }
    
    @Override
    public String toString() {
        return zzgmv.zza(this);
    }
    
    public void writeTo(final zzgmm zzgmm) throws IOException {
    }
}
