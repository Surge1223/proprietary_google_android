package com.google.android.gms.internal;

public enum zzgme
{
    zza(zzgmj.zzd, 1), 
    zzb(zzgmj.zzc, 5), 
    zzc(zzgmj.zzb, 0), 
    zzd(zzgmj.zzb, 0), 
    zze(zzgmj.zza, 0), 
    zzf(zzgmj.zzb, 1), 
    zzg(zzgmj.zza, 5), 
    zzh(zzgmj.zze, 0), 
    zzi("STRING", 8, zzgmj.zzf, 2), 
    zzj("GROUP", 9, zzgmj.zzi, 3), 
    zzk("MESSAGE", 10, zzgmj.zzi, 2), 
    zzl("BYTES", 11, zzgmj.zzg, 2), 
    zzm(zzgmj.zza, 0), 
    zzn(zzgmj.zzh, 0), 
    zzo(zzgmj.zza, 5), 
    zzp(zzgmj.zzb, 1), 
    zzq(zzgmj.zza, 0), 
    zzr(zzgmj.zzb, 0);
    
    private final zzgmj zzs;
    private final int zzt;
    
    private zzgme(final zzgmj zzs, final int zzt) {
        this.zzs = zzs;
        this.zzt = zzt;
    }
    
    public final zzgmj zza() {
        return this.zzs;
    }
    
    public final int zzb() {
        return this.zzt;
    }
}
