package com.google.android.gms.internal;

import java.nio.ByteBuffer;

final class zzgly
{
    private static final zzglz zza;
    
    static {
        zzglz zza2;
        if (zzglw.zza() && zzglw.zzb()) {
            zza2 = new zzgmc();
        }
        else {
            zza2 = new zzgma();
        }
        zza = zza2;
    }
    
    static int zza(final CharSequence charSequence) {
        final int length = charSequence.length();
        final char c = '\0';
        char c2;
        for (c2 = '\0'; c2 < length && charSequence.charAt(c2) < '\u0080'; ++c2) {}
        char c3 = (char)length;
        char c4;
        while (true) {
            c4 = c3;
            if (c2 >= length) {
                break;
            }
            final char char1 = charSequence.charAt(c2);
            if (char1 >= '\u0800') {
                final int length2 = charSequence.length();
                int n = c;
                while (c2 < length2) {
                    final char char2 = charSequence.charAt(c2);
                    char c5;
                    if (char2 < '\u0800') {
                        n += '\u007f' - char2 >>> 31;
                        c5 = c2;
                    }
                    else {
                        final char c6 = (char)(n += 2);
                        c5 = c2;
                        if ('\ud800' <= char2) {
                            n = c6;
                            c5 = c2;
                            if (char2 <= '\udfff') {
                                if (Character.codePointAt(charSequence, c2) < 65536) {
                                    throw new zzgmb(c2, length2);
                                }
                                c5 = (char)(c2 + '\u0001');
                                n = c6;
                            }
                        }
                    }
                    c2 = (char)(c5 + '\u0001');
                }
                c4 = (char)(c3 + n);
                break;
            }
            c3 += (char)('\u007f' - char1 >>> 31);
            ++c2;
        }
        if (c4 >= length) {
            return c4;
        }
        final long n2 = c4;
        final StringBuilder sb = new StringBuilder(54);
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(n2 + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int zza(final CharSequence charSequence, final byte[] array, final int n, final int n2) {
        return zzgly.zza.zza(charSequence, array, n, n2);
    }
    
    static void zza(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        final zzglz zza = zzgly.zza;
        if (byteBuffer.hasArray()) {
            final int arrayOffset = byteBuffer.arrayOffset();
            byteBuffer.position(zza(charSequence, byteBuffer.array(), byteBuffer.position() + arrayOffset, byteBuffer.remaining()) - arrayOffset);
            return;
        }
        if (byteBuffer.isDirect()) {
            zza.zza(charSequence, byteBuffer);
            return;
        }
        zzglz.zzb(charSequence, byteBuffer);
    }
    
    public static boolean zza(final byte[] array) {
        return zzgly.zza.zza(array, 0, array.length);
    }
    
    public static boolean zza(final byte[] array, final int n, final int n2) {
        return zzgly.zza.zza(array, n, n2);
    }
    
    private static int zzb(final int n) {
        if (n > -12) {
            return -1;
        }
        return n;
    }
    
    private static int zzb(final int n, final int n2) {
        if (n <= -12 && n2 <= -65) {
            return n ^ n2 << 8;
        }
        return -1;
    }
    
    private static int zzb(final int n, final int n2, final int n3) {
        if (n <= -12 && n2 <= -65 && n3 <= -65) {
            return n ^ n2 << 8 ^ n3 << 16;
        }
        return -1;
    }
    
    private static int zzc(final byte[] array, final int n, final int n2) {
        final byte b = array[n - 1];
        switch (n2 - n) {
            default: {
                throw new AssertionError();
            }
            case 2: {
                return zzb(b, array[n], array[n + 1]);
            }
            case 1: {
                return zzb(b, array[n]);
            }
            case 0: {
                return zzb(b);
            }
        }
    }
}
