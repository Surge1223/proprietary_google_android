package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzbk;
import com.google.android.gms.ads.internal.client.zzbi;

public final class zzuf extends zzbi
{
    private final Object zza;
    private volatile zzbk zzb;
    
    public zzuf() {
        this.zza = new Object();
    }
    
    @Override
    public final void zza() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final void zza(final zzbk zzb) throws RemoteException {
        synchronized (this.zza) {
            this.zzb = zzb;
        }
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final void zzb() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final boolean zzc() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final int zzd() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final float zze() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final float zzf() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final float zzg() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final boolean zzh() throws RemoteException {
        throw new RemoteException();
    }
    
    @Override
    public final zzbk zzi() throws RemoteException {
        synchronized (this.zza) {
            return this.zzb;
        }
    }
    
    @Override
    public final boolean zzj() throws RemoteException {
        throw new RemoteException();
    }
}
