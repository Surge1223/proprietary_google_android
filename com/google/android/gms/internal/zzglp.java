package com.google.android.gms.internal;

import java.util.List;

public final class zzglp extends RuntimeException
{
    private final List<String> zza;
    
    public zzglp(final zzgkg zzgkg) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
        this.zza = null;
    }
}
