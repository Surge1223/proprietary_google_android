package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public abstract class zzadf extends zzez implements zzade
{
    public static zzade zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
        if (queryLocalInterface instanceof zzade) {
            return (zzade)queryLocalInterface;
        }
        return new zzadg(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 11: {
                this.zzh(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 10: {
                this.zzg(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 9: {
                this.zzb(IObjectWrapper.zza.zza(parcel.readStrongBinder()), parcel.readInt());
                break;
            }
            case 8: {
                this.zzf(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 7: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zzfa.zza(parcel, zzadi.CREATOR));
                break;
            }
            case 6: {
                this.zze(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 5: {
                this.zzd(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 4: {
                this.zzc(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 3: {
                this.zzb(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
            case 2: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), parcel.readInt());
                break;
            }
            case 1: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}
