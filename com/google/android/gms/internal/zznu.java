package com.google.android.gms.internal;

import android.os.RemoteException;
import java.util.List;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public abstract class zznu extends zzez implements zznt
{
    public static zznt zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
        if (queryLocalInterface instanceof zznt) {
            return (zznt)queryLocalInterface;
        }
        return new zznv(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 3: {
                final List<zznx> zzb = this.zzb();
                parcel2.writeNoException();
                parcel2.writeList((List)zzb);
                break;
            }
            case 2: {
                final String zza = this.zza();
                parcel2.writeNoException();
                parcel2.writeString(zza);
                break;
            }
        }
        return true;
    }
}
