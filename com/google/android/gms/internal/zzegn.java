package com.google.android.gms.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;

public class zzegn extends zzegp
{
    @Override
    public final void zza(final ConnectionResult connectionResult, final zzegl zzegl) throws RemoteException {
    }
    
    @Override
    public final void zza(final Status status) throws RemoteException {
    }
    
    @Override
    public final void zza(final Status status, final GoogleSignInAccount googleSignInAccount) throws RemoteException {
    }
    
    @Override
    public void zza(final zzegv zzegv) throws RemoteException {
    }
    
    @Override
    public final void zzb(final Status status) throws RemoteException {
    }
}
