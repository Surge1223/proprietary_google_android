package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zznv extends zzey implements zznt
{
    zznv(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final List<zznx> zzb() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final ArrayList zzc = zzfa.zzc(zza);
        zza.recycle();
        return (List<zznx>)zzc;
    }
}
