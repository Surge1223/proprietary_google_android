package com.google.android.gms.internal;

import android.util.Log;
import java.util.TreeMap;
import android.database.Cursor;
import java.util.Map;
import android.content.ContentResolver;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import android.net.Uri;
import java.util.regex.Pattern;
import android.os.Handler;
import android.database.ContentObserver;

final class zzfcy extends ContentObserver
{
    zzfcy(final Handler handler) {
        super((Handler)null);
    }
    
    public final void onChange(final boolean b) {
        zzfcx.zze.set(true);
    }
}
