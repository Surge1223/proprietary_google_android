package com.google.android.gms.internal;

import java.util.Iterator;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

final class zzgmr implements Cloneable
{
    private zzgmp<?, ?> zza;
    private Object zzb;
    private List<zzgmw> zzc;
    
    zzgmr() {
        this.zzc = new ArrayList<zzgmw>();
    }
    
    private final byte[] zzb() throws IOException {
        final byte[] array = new byte[this.zza()];
        this.zza(zzgmm.zza(array));
        return array;
    }
    
    private final zzgmr zzc() {
        final zzgmr zzgmr = new zzgmr();
        try {
            zzgmr.zza = this.zza;
            if (this.zzc == null) {
                zzgmr.zzc = null;
            }
            else {
                zzgmr.zzc.addAll(this.zzc);
            }
            if (this.zzb != null) {
                if (this.zzb instanceof zzgmu) {
                    zzgmr.zzb = ((zzgmu)this.zzb).clone();
                }
                else if (this.zzb instanceof byte[]) {
                    zzgmr.zzb = ((byte[])this.zzb).clone();
                }
                else {
                    final boolean b = this.zzb instanceof byte[][];
                    final int n = 0;
                    int i = 0;
                    if (b) {
                        final byte[][] array = (byte[][])this.zzb;
                        final byte[][] zzb = new byte[array.length][];
                        zzgmr.zzb = zzb;
                        while (i < array.length) {
                            zzb[i] = array[i].clone();
                            ++i;
                        }
                    }
                    else if (this.zzb instanceof boolean[]) {
                        zzgmr.zzb = ((boolean[])this.zzb).clone();
                    }
                    else if (this.zzb instanceof int[]) {
                        zzgmr.zzb = ((int[])this.zzb).clone();
                    }
                    else if (this.zzb instanceof long[]) {
                        zzgmr.zzb = ((long[])this.zzb).clone();
                    }
                    else if (this.zzb instanceof float[]) {
                        zzgmr.zzb = ((float[])this.zzb).clone();
                    }
                    else if (this.zzb instanceof double[]) {
                        zzgmr.zzb = ((double[])this.zzb).clone();
                    }
                    else if (this.zzb instanceof zzgmu[]) {
                        final zzgmu[] array2 = (zzgmu[])this.zzb;
                        final zzgmu[] zzb2 = new zzgmu[array2.length];
                        zzgmr.zzb = zzb2;
                        for (int j = n; j < array2.length; ++j) {
                            zzb2[j] = (zzgmu)array2[j].clone();
                        }
                    }
                }
            }
            return zzgmr;
        }
        catch (CloneNotSupportedException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgmr)) {
            return false;
        }
        final zzgmr zzgmr = (zzgmr)o;
        if (this.zzb != null && zzgmr.zzb != null) {
            if (this.zza != zzgmr.zza) {
                return false;
            }
            if (!this.zza.zza.isArray()) {
                return this.zzb.equals(zzgmr.zzb);
            }
            if (this.zzb instanceof byte[]) {
                return Arrays.equals((byte[])this.zzb, (byte[])zzgmr.zzb);
            }
            if (this.zzb instanceof int[]) {
                return Arrays.equals((int[])this.zzb, (int[])zzgmr.zzb);
            }
            if (this.zzb instanceof long[]) {
                return Arrays.equals((long[])this.zzb, (long[])zzgmr.zzb);
            }
            if (this.zzb instanceof float[]) {
                return Arrays.equals((float[])this.zzb, (float[])zzgmr.zzb);
            }
            if (this.zzb instanceof double[]) {
                return Arrays.equals((double[])this.zzb, (double[])zzgmr.zzb);
            }
            if (this.zzb instanceof boolean[]) {
                return Arrays.equals((boolean[])this.zzb, (boolean[])zzgmr.zzb);
            }
            return Arrays.deepEquals((Object[])this.zzb, (Object[])zzgmr.zzb);
        }
        else {
            if (this.zzc != null && zzgmr.zzc != null) {
                return this.zzc.equals(zzgmr.zzc);
            }
            try {
                return Arrays.equals(this.zzb(), zzgmr.zzb());
            }
            catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
    }
    
    @Override
    public final int hashCode() {
        try {
            return 527 + Arrays.hashCode(this.zzb());
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
    
    final int zza() {
        final Object zzb = this.zzb;
        int i = 0;
        int n2;
        if (zzb != null) {
            final zzgmp<?, ?> zza = this.zza;
            final Object zzb2 = this.zzb;
            int zza2;
            if (zza.zzc) {
                final int length = Array.getLength(zzb2);
                zza2 = 0;
                while (i < length) {
                    int n = zza2;
                    if (Array.get(zzb2, i) != null) {
                        n = zza2 + zza.zza(Array.get(zzb2, i));
                    }
                    ++i;
                    zza2 = n;
                }
            }
            else {
                zza2 = zza.zza(zzb2);
            }
            n2 = zza2;
        }
        else {
            final Iterator<zzgmw> iterator = this.zzc.iterator();
            int n3 = 0;
            while (true) {
                n2 = n3;
                if (!iterator.hasNext()) {
                    break;
                }
                final zzgmw zzgmw = iterator.next();
                n3 += zzgmm.zzd(zzgmw.zza) + 0 + zzgmw.zzb.length;
            }
        }
        return n2;
    }
    
    final void zza(final zzgmm zzgmm) throws IOException {
        if (this.zzb == null) {
            for (final zzgmw zzgmw : this.zzc) {
                zzgmm.zzc(zzgmw.zza);
                zzgmm.zzd(zzgmw.zzb);
            }
            return;
        }
        final zzgmp<?, ?> zza = this.zza;
        final Object zzb = this.zzb;
        if (zza.zzc) {
            for (int length = Array.getLength(zzb), i = 0; i < length; ++i) {
                final Object value = Array.get(zzb, i);
                if (value != null) {
                    zza.zza(value, zzgmm);
                }
            }
            return;
        }
        zza.zza(zzb, zzgmm);
    }
}
