package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;
import java.util.SortedMap;
import java.util.Collections;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

final class zzgld implements Iterator<Map.Entry<Object, Object>>
{
    private int zza;
    private Iterator<Map.Entry<Object, Object>> zzb;
    private final /* synthetic */ zzglb zzc;
    
    private zzgld(final zzglb zzc) {
        this.zzc = zzc;
        this.zza = this.zzc.zzb.size();
    }
    
    private final Iterator<Map.Entry<Object, Object>> zza() {
        if (this.zzb == null) {
            this.zzb = this.zzc.zzf.entrySet().iterator();
        }
        return this.zzb;
    }
    
    @Override
    public final boolean hasNext() {
        return (this.zza > 0 && this.zza <= this.zzc.zzb.size()) || this.zza().hasNext();
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
