package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

public final class zzqb extends zzpn
{
    private final UnifiedNativeAd.zza zza;
    
    public zzqb(final UnifiedNativeAd.zza zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzpp zzpp) {
        this.zza.zza(new zzps(zzpp));
    }
}
