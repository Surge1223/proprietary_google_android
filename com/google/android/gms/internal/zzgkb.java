package com.google.android.gms.internal;

import java.util.Map;

interface zzgkb
{
    int zza(final int p0, final Object p1, final Object p2);
    
    Object zza(final Object p0, final Object p1);
    
    Map<?, ?> zzb(final Object p0);
    
    zzgjz<?, ?> zzf(final Object p0);
}
