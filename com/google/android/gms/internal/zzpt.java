package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzq;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import android.widget.FrameLayout;
import android.content.Context;
import com.google.android.gms.dynamic.zzp;

public final class zzpt extends zzp<zzoe>
{
    public zzpt() {
        super("com.google.android.gms.ads.NativeAdViewDelegateCreatorImpl");
    }
    
    public final zzob zza(final Context context, final FrameLayout frameLayout, final FrameLayout frameLayout2) {
        try {
            final IBinder zza = this.zzb(context).zza(zzn.zza(context), zzn.zza(frameLayout), zzn.zza(frameLayout2), 12438000);
            if (zza == null) {
                return null;
            }
            final IInterface queryLocalInterface = zza.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
            if (queryLocalInterface instanceof zzob) {
                return (zzob)queryLocalInterface;
            }
            return new zzod(zza);
        }
        catch (RemoteException | zzq ex) {
            final Throwable t;
            zzaid.zzc("Could not create remote NativeAdViewDelegate.", t);
            return null;
        }
    }
}
