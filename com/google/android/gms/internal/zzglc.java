package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.List;

final class zzglc extends zzglb<Object, Object>
{
    zzglc(final int n) {
        super(n, null);
    }
    
    @Override
    public final void zza() {
        if (!this.zzb()) {
            for (int i = 0; i < this.zzc(); ++i) {
                final Entry<Object, Object> zzb = (Entry<Object, Object>)((zzglb<Object, List<Object>>)this).zzb(i);
                if (zzb.getKey().zzd()) {
                    zzb.setValue(Collections.unmodifiableList((List<?>)zzb.getValue()));
                }
            }
            for (final Map.Entry<zzgip, V> entry : this.zzd()) {
                if (entry.getKey().zzd()) {
                    entry.setValue((V)Collections.unmodifiableList((List<?>)entry.getValue()));
                }
            }
        }
        super.zza();
    }
}
