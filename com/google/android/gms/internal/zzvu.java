package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzp;

public final class zzvu extends zzp<zzvy>
{
    public zzvu() {
        super("com.google.android.gms.ads.AdOverlayCreatorImpl");
    }
}
