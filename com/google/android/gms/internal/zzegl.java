package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;
import android.content.Intent;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;

public final class zzegl extends zzbid implements Result
{
    public static final Parcelable.Creator<zzegl> CREATOR;
    private final int zza;
    private int zzb;
    private Intent zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzegm();
    }
    
    public zzegl() {
        this(0, null);
    }
    
    zzegl(final int zza, final int zzb, final Intent zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    private zzegl(final int n, final Intent intent) {
        this(2, 0, null);
    }
    
    @Override
    public final Status getStatus() {
        if (this.zzb == 0) {
            return Status.zza;
        }
        return Status.zze;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, zza);
    }
}
