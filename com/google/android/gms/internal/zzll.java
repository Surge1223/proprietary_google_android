package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.os.ConditionVariable;

public final class zzll
{
    private final Object zza;
    private final ConditionVariable zzb;
    private volatile boolean zzc;
    private SharedPreferences zzd;
    
    public zzll() {
        this.zza = new Object();
        this.zzb = new ConditionVariable();
        this.zzc = false;
        this.zzd = null;
    }
}
