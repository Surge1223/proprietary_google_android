package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgns extends zzgmo<zzgns> implements Cloneable
{
    private int zza;
    private int zzb;
    
    public zzgns() {
        this.zza = -1;
        this.zzb = 0;
        this.zzax = null;
        this.zzay = -1;
    }
    
    private final zzgns zza() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza != -1) {
            computeSerializedSize = n + zzgmm.zzb(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzb != 0) {
            n2 = computeSerializedSize + zzgmm.zzb(2, this.zzb);
        }
        return n2;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgns)) {
            return false;
        }
        final zzgns zzgns = (zzgns)o;
        if (this.zza != zzgns.zza) {
            return false;
        }
        if (this.zzb != zzgns.zzb) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgns.zzax);
        }
        return zzgns.zzax == null || zzgns.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final int zza = this.zza;
        final int zzb = this.zzb;
        int hashCode2;
        if (this.zzax != null && !this.zzax.zzb()) {
            hashCode2 = this.zzax.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return (((527 + hashCode) * 31 + zza) * 31 + zzb) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza != -1) {
            zzgmm.zza(1, this.zza);
        }
        if (this.zzb != 0) {
            zzgmm.zza(2, this.zzb);
        }
        super.writeTo(zzgmm);
    }
}
