package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzao;
import android.os.IInterface;

public interface zzpj extends IInterface
{
    void zza(final zzao p0, final IObjectWrapper p1) throws RemoteException;
}
