package com.google.android.gms.internal;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

public final class zzgjo extends zzghf<String> implements zzgjp, RandomAccess
{
    private static final zzgjo zza;
    private static final zzgjp zzb;
    private final List<Object> zzc;
    
    static {
        (zza = new zzgjo()).zzb();
        zzb = zzgjo.zza;
    }
    
    public zzgjo() {
        this(10);
    }
    
    public zzgjo(final int n) {
        this(new ArrayList<Object>(n));
    }
    
    private zzgjo(final ArrayList<Object> zzc) {
        this.zzc = zzc;
    }
    
    private static String zza(final Object o) {
        if (o instanceof String) {
            return (String)o;
        }
        if (o instanceof zzgho) {
            return ((zzgho)o).zzd();
        }
        return zzgja.zzb((byte[])o);
    }
    
    @Override
    public final boolean addAll(final int n, final Collection<? extends String> collection) {
        this.zzc();
        Object zzd = collection;
        if (collection instanceof zzgjp) {
            zzd = ((zzgjp)collection).zzd();
        }
        final boolean addAll = this.zzc.addAll(n, (Collection<?>)zzd);
        ++this.modCount;
        return addAll;
    }
    
    @Override
    public final boolean addAll(final Collection<? extends String> collection) {
        return this.addAll(this.size(), collection);
    }
    
    @Override
    public final void clear() {
        this.zzc();
        this.zzc.clear();
        ++this.modCount;
    }
    
    @Override
    public final int size() {
        return this.zzc.size();
    }
    
    @Override
    public final Object zzb(final int n) {
        return this.zzc.get(n);
    }
    
    @Override
    public final List<?> zzd() {
        return Collections.unmodifiableList((List<?>)this.zzc);
    }
}
