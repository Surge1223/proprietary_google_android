package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbi;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Parcelable.Creator;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzub extends zzey implements zztz
{
    zzub(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(9, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)objectWrapper2);
        zzfa.zza(a_, (IInterface)objectWrapper3);
        this.zzb(22, a_);
    }
    
    @Override
    public final List zzb() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final ArrayList zzc = zzfa.zzc(zza);
        zza.recycle();
        return zzc;
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(10, a_);
    }
    
    @Override
    public final String zzc() throws RemoteException {
        final Parcel zza = this.zza(4, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zzc(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(14, a_);
    }
    
    @Override
    public final zznx zzd() throws RemoteException {
        final Parcel zza = this.zza(5, this.a_());
        final zznx zza2 = zzny.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final String zze() throws RemoteException {
        final Parcel zza = this.zza(6, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final String zzf() throws RemoteException {
        final Parcel zza = this.zza(7, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zzg() throws RemoteException {
        this.zzb(8, this.a_());
    }
    
    @Override
    public final boolean zzh() throws RemoteException {
        final Parcel zza = this.zza(11, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final boolean zzi() throws RemoteException {
        final Parcel zza = this.zza(12, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final Bundle zzj() throws RemoteException {
        final Parcel zza = this.zza(13, this.a_());
        final Bundle bundle = zzfa.zza(zza, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }
    
    @Override
    public final IObjectWrapper zzk() throws RemoteException {
        final Parcel zza = this.zza(15, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzbh zzl() throws RemoteException {
        final Parcel zza = this.zza(16, this.a_());
        final zzbh zza2 = zzbi.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zznt zzm() throws RemoteException {
        final Parcel zza = this.zza(19, this.a_());
        final zznt zza2 = zznu.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzn() throws RemoteException {
        final Parcel zza = this.zza(20, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzo() throws RemoteException {
        final Parcel zza = this.zza(21, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
}
