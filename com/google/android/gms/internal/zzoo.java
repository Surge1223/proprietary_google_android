package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;
import java.util.Iterator;
import android.os.RemoteException;
import android.os.IBinder;
import java.util.ArrayList;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;
import com.google.android.gms.ads.formats.NativeAppInstallAd;

public final class zzoo extends NativeAppInstallAd
{
    private final zzol zza;
    private final List<Image> zzb;
    private final zzoa zzc;
    private final VideoController zzd;
    private final AdChoicesInfo zze;
    
    public zzoo(final zzol zza) {
        this.zzb = new ArrayList<Image>();
        this.zzd = new VideoController();
        this.zza = zza;
        final AdChoicesInfo adChoicesInfo = null;
        final AdChoicesInfo adChoicesInfo2 = null;
        try {
            final List zzb = this.zza.zzb();
            if (zzb != null) {
                for (final IBinder next : zzb) {
                    zznx zznx = null;
                    Label_0133: {
                        if (next instanceof IBinder) {
                            final IBinder binder = next;
                            if (binder != null) {
                                final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                                if (queryLocalInterface instanceof zznx) {
                                    zznx = (zznx)queryLocalInterface;
                                    break Label_0133;
                                }
                                zznx = new zznz(binder);
                                break Label_0133;
                            }
                        }
                        zznx = null;
                    }
                    if (zznx != null) {
                        this.zzb.add(new zzoa(zznx));
                    }
                }
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get image.", (Throwable)ex);
        }
        zzoa zzc;
        try {
            final zznx zzd = this.zza.zzd();
            if (zzd != null) {
                zzc = new zzoa(zzd);
            }
            else {
                zzc = null;
            }
        }
        catch (RemoteException ex2) {
            zzaid.zzb("Failed to get image.", (Throwable)ex2);
            zzc = null;
        }
        this.zzc = zzc;
        AdChoicesInfo zze = adChoicesInfo2;
        try {
            if (this.zza.zzr() != null) {
                zze = new zznw(this.zza.zzr());
            }
        }
        catch (RemoteException ex3) {
            zzaid.zzb("Failed to get attribution info.", (Throwable)ex3);
            zze = adChoicesInfo;
        }
        this.zze = zze;
    }
    
    private final IObjectWrapper zzb() {
        try {
            return this.zza.zzj();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to retrieve native ad engine.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final CharSequence getBody() {
        try {
            return this.zza.zzc();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get body.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final CharSequence getCallToAction() {
        try {
            return this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get call to action.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final CharSequence getHeadline() {
        try {
            return this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get headline.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final Image getIcon() {
        return this.zzc;
    }
    
    @Override
    public final List<Image> getImages() {
        return this.zzb;
    }
    
    @Override
    public final CharSequence getPrice() {
        try {
            return this.zza.zzh();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get price.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final Double getStarRating() {
        try {
            final double zzf = this.zza.zzf();
            if (zzf == -1.0) {
                return null;
            }
            return zzf;
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get star rating.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final CharSequence getStore() {
        try {
            return this.zza.zzg();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get store", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final VideoController getVideoController() {
        try {
            if (this.zza.zzi() != null) {
                this.zzd.zza(this.zza.zzi());
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Exception occurred while getting video controller", (Throwable)ex);
        }
        return this.zzd;
    }
}
