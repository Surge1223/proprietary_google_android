package com.google.android.gms.internal;

public final class zzgmx
{
    public static final int[] zza;
    public static final long[] zzb;
    public static final float[] zzc;
    public static final double[] zzd;
    public static final boolean[] zze;
    public static final String[] zzf;
    public static final byte[][] zzg;
    public static final byte[] zzh;
    private static final int zzi;
    private static final int zzj;
    private static final int zzk;
    private static final int zzl;
    
    static {
        zzi = 11;
        zzj = 12;
        zzk = 16;
        zzl = 26;
        zza = new int[0];
        zzb = new long[0];
        zzc = new float[0];
        zzd = new double[0];
        zze = new boolean[0];
        zzf = new String[0];
        zzg = new byte[0][];
        zzh = new byte[0];
    }
}
