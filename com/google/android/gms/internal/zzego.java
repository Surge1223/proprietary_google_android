package com.google.android.gms.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import android.os.IInterface;

public interface zzego extends IInterface
{
    void zza(final ConnectionResult p0, final zzegl p1) throws RemoteException;
    
    void zza(final Status p0) throws RemoteException;
    
    void zza(final Status p0, final GoogleSignInAccount p1) throws RemoteException;
    
    void zza(final zzegv p0) throws RemoteException;
    
    void zzb(final Status p0) throws RemoteException;
}
