package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zztl extends zzez implements zztk
{
    public zztl() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }
    
    public static zztk zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
        if (queryLocalInterface instanceof zztk) {
            return (zztk)queryLocalInterface;
        }
        return new zztm(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 2: {
                final boolean zzb = this.zzb(parcel.readString());
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzb);
                break;
            }
            case 1: {
                final zztn zza = this.zza(parcel.readString());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zza);
                break;
            }
        }
        return true;
    }
}
