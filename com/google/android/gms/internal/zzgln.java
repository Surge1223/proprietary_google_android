package com.google.android.gms.internal;

final class zzgln implements zzglo
{
    private final /* synthetic */ zzgho zza;
    
    zzgln(final zzgho zza) {
        this.zza = zza;
    }
    
    @Override
    public final byte zza(final int n) {
        return this.zza.zza(n);
    }
    
    @Override
    public final int zza() {
        return this.zza.zza();
    }
}
