package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzmi extends zzey implements zzmg
{
    zzmi(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.customrenderedad.client.IOnCustomRenderedAdLoadedListener");
    }
    
    @Override
    public final void zza(final zzmd zzmd) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzmd);
        this.zzb(1, a_);
    }
}
