package com.google.android.gms.internal;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzgju extends zzghf<Long> implements zzgjf<Long>, RandomAccess
{
    private static final zzgju zza;
    private long[] zzb;
    private int zzc;
    
    static {
        (zza = new zzgju()).zzb();
    }
    
    zzgju() {
        this(new long[10], 0);
    }
    
    private zzgju(final long[] zzb, final int zzc) {
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    private final void zza(final int n, final long n2) {
        this.zzc();
        if (n >= 0 && n <= this.zzc) {
            if (this.zzc < this.zzb.length) {
                System.arraycopy(this.zzb, n, this.zzb, n + 1, this.zzc - n);
            }
            else {
                final long[] zzb = new long[this.zzc * 3 / 2 + 1];
                System.arraycopy(this.zzb, 0, zzb, 0, n);
                System.arraycopy(this.zzb, n, zzb, n + 1, this.zzc - n);
                this.zzb = zzb;
            }
            this.zzb[n] = n2;
            ++this.zzc;
            ++this.modCount;
            return;
        }
        throw new IndexOutOfBoundsException(this.zzd(n));
    }
    
    private final void zzc(final int n) {
        if (n >= 0 && n < this.zzc) {
            return;
        }
        throw new IndexOutOfBoundsException(this.zzd(n));
    }
    
    private final String zzd(final int n) {
        final int zzc = this.zzc;
        final StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(n);
        sb.append(", Size:");
        sb.append(zzc);
        return sb.toString();
    }
    
    @Override
    public final boolean addAll(final Collection<? extends Long> collection) {
        this.zzc();
        zzgja.zza(collection);
        if (!(collection instanceof zzgju)) {
            return super.addAll(collection);
        }
        final zzgju zzgju = (zzgju)collection;
        if (zzgju.zzc == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.zzc >= zzgju.zzc) {
            final int zzc = this.zzc + zzgju.zzc;
            if (zzc > this.zzb.length) {
                this.zzb = Arrays.copyOf(this.zzb, zzc);
            }
            System.arraycopy(zzgju.zzb, 0, this.zzb, this.zzc, zzgju.zzc);
            this.zzc = zzc;
            ++this.modCount;
            return true;
        }
        throw new OutOfMemoryError();
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzgju)) {
            return super.equals(o);
        }
        final zzgju zzgju = (zzgju)o;
        if (this.zzc != zzgju.zzc) {
            return false;
        }
        final long[] zzb = zzgju.zzb;
        for (int i = 0; i < this.zzc; ++i) {
            if (this.zzb[i] != zzb[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final int hashCode() {
        int n = 1;
        for (int i = 0; i < this.zzc; ++i) {
            n = n * 31 + zzgja.zza(this.zzb[i]);
        }
        return n;
    }
    
    @Override
    public final boolean remove(final Object o) {
        this.zzc();
        for (int i = 0; i < this.zzc; ++i) {
            if (o.equals(this.zzb[i])) {
                System.arraycopy(this.zzb, i + 1, this.zzb, i, this.zzc - i);
                --this.zzc;
                ++this.modCount;
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final int size() {
        return this.zzc;
    }
    
    public final long zzb(final int n) {
        this.zzc(n);
        return this.zzb[n];
    }
}
