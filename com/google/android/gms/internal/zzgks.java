package com.google.android.gms.internal;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzgks
{
    private static final zzgks zza;
    private final zzgkz zzb;
    private final ConcurrentMap<Class<?>, zzgky<?>> zzc;
    
    static {
        zza = new zzgks();
    }
    
    private zzgks() {
        this.zzc = new ConcurrentHashMap<Class<?>, zzgky<?>>();
        zzgkz zzgkz = null;
        zzgkz zza;
        for (int i = 0; i <= 0; ++i, zzgkz = zza) {
            zza = zza((new String[] { "com.google.protobuf.AndroidProto3SchemaFactory" })[0]);
            if ((zzgkz = zza) != null) {
                break;
            }
        }
        zzgkz zzb;
        if ((zzb = zzgkz) == null) {
            zzb = new zzgjv();
        }
        this.zzb = zzb;
    }
    
    public static zzgks zza() {
        return zzgks.zza;
    }
    
    private static zzgkz zza(final String s) {
        try {
            return (zzgkz)Class.forName(s).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    public final <T> zzgky<T> zza(final Class<T> clazz) {
        zzgja.zza(clazz, "messageType");
        zzgky<?> zza;
        if ((zza = this.zzc.get(clazz)) == null) {
            zza = this.zzb.zza(clazz);
            zzgja.zza(clazz, "messageType");
            zzgja.zza(zza, "schema");
            final zzgky<?> zzgky = this.zzc.putIfAbsent(clazz, zza);
            if (zzgky != null) {
                zza = zzgky;
            }
        }
        return (zzgky<T>)zza;
    }
    
    public final <T> zzgky<T> zza(final T t) {
        return this.zza((Class<T>)t.getClass());
    }
}
