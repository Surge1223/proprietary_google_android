package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzdld
{
    public static <T> T zza(final byte[] array, final Parcelable.Creator<T> parcelable$Creator) {
        final Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(array, 0, array.length);
            obtain.setDataPosition(0);
            return (T)parcelable$Creator.createFromParcel(obtain);
        }
        finally {
            obtain.recycle();
        }
    }
}
