package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzpn extends zzez implements zzpm
{
    public zzpn() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.formats.client.IOnUnifiedNativeAdLoadedListener");
    }
    
    public static zzpm zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IOnUnifiedNativeAdLoadedListener");
        if (queryLocalInterface instanceof zzpm) {
            return (zzpm)queryLocalInterface;
        }
        return new zzpo(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            final IBinder strongBinder = parcel.readStrongBinder();
            zzpp zzpp;
            if (strongBinder == null) {
                zzpp = null;
            }
            else {
                final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IUnifiedNativeAd");
                if (queryLocalInterface instanceof zzpp) {
                    zzpp = (zzpp)queryLocalInterface;
                }
                else {
                    zzpp = new zzpr(strongBinder);
                }
            }
            this.zza(zzpp);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}
