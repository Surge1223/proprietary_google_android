package com.google.android.gms.internal;

final class zzghz extends zzghx
{
    private final byte[] zzd;
    private final boolean zze;
    private int zzf;
    private int zzg;
    private int zzh;
    private int zzi;
    private int zzk;
    
    private zzghz(final byte[] zzd, final int zzh, final int n, final boolean zze) {
        super(null);
        this.zzk = Integer.MAX_VALUE;
        this.zzd = zzd;
        this.zzf = n + zzh;
        this.zzh = zzh;
        this.zzi = this.zzh;
        this.zze = zze;
    }
    
    private final void zzz() {
        this.zzf += this.zzg;
        final int n = this.zzf - this.zzi;
        if (n > this.zzk) {
            this.zzg = n - this.zzk;
            this.zzf -= this.zzg;
            return;
        }
        this.zzg = 0;
    }
    
    @Override
    public final int zzd(int zzk) throws zzgjg {
        if (zzk < 0) {
            throw zzgjg.zzb();
        }
        final int zzk2 = zzk + this.zzu();
        zzk = this.zzk;
        if (zzk2 <= zzk) {
            this.zzk = zzk2;
            this.zzz();
            return zzk;
        }
        throw zzgjg.zza();
    }
    
    @Override
    public final int zzu() {
        return this.zzh - this.zzi;
    }
}
