package com.google.android.gms.internal;

final class zzgkq
{
    private static final zzgko zza;
    private static final zzgko zzb;
    
    static {
        zza = zzc();
        zzb = new zzgkp();
    }
    
    static zzgko zza() {
        return zzgkq.zza;
    }
    
    static zzgko zzb() {
        return zzgkq.zzb;
    }
    
    private static zzgko zzc() {
        try {
            return (zzgko)Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (Exception ex) {
            return null;
        }
    }
}
