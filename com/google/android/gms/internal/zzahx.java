package com.google.android.gms.internal;

import java.util.HashMap;
import java.net.HttpURLConnection;
import com.google.android.gms.common.util.zzc;
import java.io.Writer;
import java.io.StringWriter;
import java.util.Iterator;
import java.io.IOException;
import android.util.JsonWriter;
import java.util.Map;
import java.util.UUID;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.google.android.gms.common.util.zzh;
import java.util.List;
import java.util.Set;
import com.google.android.gms.common.util.Clock;

public final class zzahx
{
    private static Object zza;
    private static boolean zzb;
    private static boolean zzc;
    private static Clock zzd;
    private static final Set<String> zze;
    private final List<String> zzf;
    
    static {
        zzahx.zza = new Object();
        zzahx.zzb = false;
        zzahx.zzc = false;
        zzahx.zzd = zzh.zza();
        zze = new HashSet<String>(Arrays.asList(new String[0]));
    }
    
    public zzahx() {
        this(null);
    }
    
    public zzahx(String s) {
        List<String> zzf;
        if (!zzc()) {
            zzf = new ArrayList<String>();
        }
        else {
            final String string = UUID.randomUUID().toString();
            if (s == null) {
                s = String.valueOf(string);
                if (s.length() != 0) {
                    s = "network_request_".concat(s);
                }
                else {
                    s = new String("network_request_");
                }
                zzf = Arrays.asList(s);
            }
            else {
                s = String.valueOf(s);
                if (s.length() != 0) {
                    s = "ad_request_".concat(s);
                }
                else {
                    s = new String("ad_request_");
                }
                final String value = String.valueOf(string);
                String concat;
                if (value.length() != 0) {
                    concat = "network_request_".concat(value);
                }
                else {
                    concat = new String("network_request_");
                }
                zzf = Arrays.asList(s, concat);
            }
        }
        this.zzf = zzf;
    }
    
    private static void zza(final JsonWriter jsonWriter, final Map<String, ?> map) throws IOException {
        if (map == null) {
            return;
        }
        jsonWriter.name("headers").beginArray();
        for (final Map.Entry<String, List<String>> entry : map.entrySet()) {
            final String s = entry.getKey();
            if (!zzahx.zze.contains(s)) {
                if (entry.getValue() instanceof List) {
                    for (final String s2 : entry.getValue()) {
                        jsonWriter.beginObject();
                        jsonWriter.name("name").value(s);
                        jsonWriter.name("value").value(s2);
                        jsonWriter.endObject();
                    }
                }
                else {
                    if (!(entry.getValue() instanceof String)) {
                        zzaid.zzc("Connection headers should be either Map<String, String> or Map<String, List<String>>");
                        break;
                    }
                    jsonWriter.beginObject();
                    jsonWriter.name("name").value(s);
                    jsonWriter.name("value").value((String)entry.getValue());
                    jsonWriter.endObject();
                }
            }
        }
        jsonWriter.endArray();
    }
    
    private final void zza(final String s, final zzaic zzaic) {
        final StringWriter stringWriter = new StringWriter();
        final JsonWriter jsonWriter = new JsonWriter((Writer)stringWriter);
        try {
            jsonWriter.beginObject();
            jsonWriter.name("timestamp").value(zzahx.zzd.currentTimeMillis());
            jsonWriter.name("event").value(s);
            jsonWriter.name("components").beginArray();
            final Iterator<String> iterator = this.zzf.iterator();
            while (iterator.hasNext()) {
                jsonWriter.value((String)iterator.next());
            }
            jsonWriter.endArray();
            zzaic.zza(jsonWriter);
            jsonWriter.endObject();
            jsonWriter.flush();
            jsonWriter.close();
        }
        catch (IOException ex) {
            zzaid.zzb("unable to log", ex);
        }
        zzc(stringWriter.toString());
    }
    
    private final void zzb(final String s) {
        this.zza("onNetworkRequestError", new zzaib(s));
    }
    
    private final void zzb(final String s, final String s2, final Map<String, ?> map, final byte[] array) {
        this.zza("onNetworkRequest", new zzahy(s, s2, map, array));
    }
    
    private final void zzb(final Map<String, ?> map, final int n) {
        this.zza("onNetworkResponse", new zzahz(n, map));
    }
    
    private static void zzc(final String s) {
        synchronized (zzahx.class) {
            zzaid.zzd("GMA Debug BEGIN");
            int n;
            for (int i = 0; i < s.length(); i = n) {
                n = i + 4000;
                final String value = String.valueOf(s.substring(i, Math.min(n, s.length())));
                String concat;
                if (value.length() != 0) {
                    concat = "GMA Debug CONTENT ".concat(value);
                }
                else {
                    concat = new String("GMA Debug CONTENT ");
                }
                zzaid.zzd(concat);
            }
            zzaid.zzd("GMA Debug FINISH");
        }
    }
    
    public static boolean zzc() {
        synchronized (zzahx.zza) {
            return zzahx.zzb && zzahx.zzc;
        }
    }
    
    public final void zza(final HttpURLConnection httpURLConnection, final int n) {
        if (!zzc()) {
            return;
        }
        Map<String, ?> map;
        if (httpURLConnection.getHeaderFields() == null) {
            map = null;
        }
        else {
            map = new HashMap<String, Object>(httpURLConnection.getHeaderFields());
        }
        this.zzb(map, n);
        if (n >= 200) {
            if (n < 300) {
                return;
            }
        }
        String responseMessage;
        try {
            responseMessage = httpURLConnection.getResponseMessage();
        }
        catch (IOException ex) {
            final String value = String.valueOf(ex.getMessage());
            String concat;
            if (value.length() != 0) {
                concat = "Can not get error message from error HttpURLConnection\n".concat(value);
            }
            else {
                concat = new String("Can not get error message from error HttpURLConnection\n");
            }
            zzaid.zze(concat);
            responseMessage = null;
        }
        this.zzb(responseMessage);
    }
    
    public final void zza(final HttpURLConnection httpURLConnection, final byte[] array) {
        if (!zzc()) {
            return;
        }
        Map<String, ?> map;
        if (httpURLConnection.getRequestProperties() == null) {
            map = null;
        }
        else {
            map = new HashMap<String, Object>(httpURLConnection.getRequestProperties());
        }
        this.zzb(new String(httpURLConnection.getURL().toString()), new String(httpURLConnection.getRequestMethod()), map, array);
    }
}
