package com.google.android.gms.internal;

import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.Iterator;

final class zzglv implements Iterator<String>
{
    private Iterator<String> zza;
    private final /* synthetic */ zzglt zzb;
    
    zzglv(final zzglt zzb) {
        this.zzb = zzb;
        this.zza = this.zzb.zza.iterator();
    }
    
    @Override
    public final boolean hasNext() {
        return this.zza.hasNext();
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
