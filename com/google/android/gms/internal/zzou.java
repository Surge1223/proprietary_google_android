package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzbh;
import java.util.List;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public abstract class zzou extends zzez implements zzot
{
    public static zzot zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
        if (queryLocalInterface instanceof zzot) {
            return (zzot)queryLocalInterface;
        }
        return new zzov(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 11: {
                final IObjectWrapper zzb = this.zzb();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzb);
                break;
            }
            case 10: {
                final boolean zza = this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzfa.zza(parcel2, zza);
                break;
            }
            case 9: {
                final IObjectWrapper zze = this.zze();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zze);
                break;
            }
            case 8: {
                this.zzf();
                parcel2.writeNoException();
                break;
            }
            case 7: {
                final zzbh zzc = this.zzc();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzc);
                break;
            }
            case 6: {
                this.zzd();
                parcel2.writeNoException();
                break;
            }
            case 5: {
                this.zzc(parcel.readString());
                parcel2.writeNoException();
                break;
            }
            case 4: {
                final String zzl = this.zzl();
                parcel2.writeNoException();
                parcel2.writeString(zzl);
                break;
            }
            case 3: {
                final List<String> zza2 = this.zza();
                parcel2.writeNoException();
                parcel2.writeStringList((List)zza2);
                break;
            }
            case 2: {
                final zznx zzb2 = this.zzb(parcel.readString());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzb2);
                break;
            }
            case 1: {
                final String zza3 = this.zza(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(zza3);
                break;
            }
        }
        return true;
    }
}
