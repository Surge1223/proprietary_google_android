package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzj;
import java.util.List;
import com.google.android.gms.ads.internal.client.zzf;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zztn extends IInterface
{
    IObjectWrapper zza() throws RemoteException;
    
    void zza(final zzf p0, final String p1) throws RemoteException;
    
    void zza(final zzf p0, final String p1, final String p2) throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzf p1, final String p2, final zzade p3, final String p4) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzf p1, final String p2, final zztq p3) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzf p1, final String p2, final String p3, final zztq p4) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzf p1, final String p2, final String p3, final zztq p4, final zznm p5, final List<String> p6) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzj p1, final zzf p2, final String p3, final zztq p4) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzj p1, final zzf p2, final String p3, final String p4, final zztq p5) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final zzade p1, final List<String> p2) throws RemoteException;
    
    void zza(final boolean p0) throws RemoteException;
    
    void zzb() throws RemoteException;
    
    void zzc() throws RemoteException;
    
    void zzd() throws RemoteException;
    
    void zze() throws RemoteException;
    
    void zzf() throws RemoteException;
    
    boolean zzg() throws RemoteException;
    
    zztw zzh() throws RemoteException;
    
    zztz zzi() throws RemoteException;
    
    Bundle zzj() throws RemoteException;
    
    Bundle zzk() throws RemoteException;
    
    Bundle zzl() throws RemoteException;
    
    boolean zzm() throws RemoteException;
    
    zzot zzn() throws RemoteException;
    
    zzbh zzo() throws RemoteException;
    
    zzuc zzp() throws RemoteException;
}
