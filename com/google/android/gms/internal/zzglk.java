package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;
import java.util.AbstractSet;

class zzglk extends AbstractSet<Map.Entry<Object, Object>>
{
    private final /* synthetic */ zzglb zza;
    
    private zzglk(final zzglb zza) {
        this.zza = zza;
    }
    
    @Override
    public void clear() {
        this.zza.clear();
    }
    
    @Override
    public boolean contains(Object value) {
        final Map.Entry entry = (Map.Entry)value;
        value = this.zza.get(entry.getKey());
        final Object value2 = entry.getValue();
        return value == value2 || (value != null && value.equals(value2));
    }
    
    @Override
    public Iterator<Map.Entry<Object, Object>> iterator() {
        return new zzglj(this.zza, null);
    }
    
    @Override
    public boolean remove(final Object o) {
        final Map.Entry entry = (Map.Entry)o;
        if (this.contains(entry)) {
            this.zza.remove(entry.getKey());
            return true;
        }
        return false;
    }
    
    @Override
    public int size() {
        return this.zza.size();
    }
}
