package com.google.android.gms.internal;

final class zzgim
{
    private static final zzgij<?> zza;
    private static final zzgij<?> zzb;
    
    static {
        zza = new zzgik();
        zzb = zzc();
    }
    
    static zzgij<?> zza() {
        return zzgim.zza;
    }
    
    static zzgij<?> zzb() {
        if (zzgim.zzb != null) {
            return zzgim.zzb;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
    
    private static zzgij<?> zzc() {
        try {
            return (zzgij<?>)Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (Exception ex) {
            return null;
        }
    }
}
