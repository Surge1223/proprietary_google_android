package com.google.android.gms.internal;

final class zzghg
{
    private static final Class<?> zza;
    private static final boolean zzb;
    
    static {
        zza = zza("libcore.io.Memory");
        zzb = (zza("org.robolectric.Robolectric") != null);
    }
    
    private static <T> Class<T> zza(final String s) {
        try {
            return (Class<T>)Class.forName(s);
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    static boolean zza() {
        return zzghg.zza != null && !zzghg.zzb;
    }
    
    static Class<?> zzb() {
        return zzghg.zza;
    }
}
