package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbc extends zzgmo<zzbc>
{
    public Long zza;
    public Long zzb;
    public Long zzc;
    public Long zzd;
    public Long zze;
    public Long zzf;
    public Integer zzg;
    public Long zzh;
    public Long zzi;
    public Long zzj;
    public Integer zzk;
    public Long zzl;
    public Long zzm;
    public Long zzn;
    public Long zzo;
    public Long zzp;
    public Long zzq;
    public Long zzr;
    public Long zzs;
    private Long zzu;
    private Long zzv;
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza != null) {
            computeSerializedSize = n + zzgmm.zzf(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzb != null) {
            n2 = computeSerializedSize + zzgmm.zzf(2, this.zzb);
        }
        int n3 = n2;
        if (this.zzc != null) {
            n3 = n2 + zzgmm.zzf(3, this.zzc);
        }
        int n4 = n3;
        if (this.zzd != null) {
            n4 = n3 + zzgmm.zzf(4, this.zzd);
        }
        int n5 = n4;
        if (this.zze != null) {
            n5 = n4 + zzgmm.zzf(5, this.zze);
        }
        int n6 = n5;
        if (this.zzf != null) {
            n6 = n5 + zzgmm.zzf(6, this.zzf);
        }
        int n7 = n6;
        if (this.zzg != null) {
            n7 = n6 + zzgmm.zzb(7, this.zzg);
        }
        int n8 = n7;
        if (this.zzh != null) {
            n8 = n7 + zzgmm.zzf(8, this.zzh);
        }
        int n9 = n8;
        if (this.zzi != null) {
            n9 = n8 + zzgmm.zzf(9, this.zzi);
        }
        int n10 = n9;
        if (this.zzj != null) {
            n10 = n9 + zzgmm.zzf(10, this.zzj);
        }
        int n11 = n10;
        if (this.zzk != null) {
            n11 = n10 + zzgmm.zzb(11, this.zzk);
        }
        int n12 = n11;
        if (this.zzl != null) {
            n12 = n11 + zzgmm.zzf(12, this.zzl);
        }
        int n13 = n12;
        if (this.zzm != null) {
            n13 = n12 + zzgmm.zzf(13, this.zzm);
        }
        int n14 = n13;
        if (this.zzn != null) {
            n14 = n13 + zzgmm.zzf(14, this.zzn);
        }
        int n15 = n14;
        if (this.zzo != null) {
            n15 = n14 + zzgmm.zzf(15, this.zzo);
        }
        int n16 = n15;
        if (this.zzp != null) {
            n16 = n15 + zzgmm.zzf(16, this.zzp);
        }
        int n17 = n16;
        if (this.zzq != null) {
            n17 = n16 + zzgmm.zzf(17, this.zzq);
        }
        int n18 = n17;
        if (this.zzr != null) {
            n18 = n17 + zzgmm.zzf(18, this.zzr);
        }
        int n19 = n18;
        if (this.zzs != null) {
            n19 = n18 + zzgmm.zzf(19, this.zzs);
        }
        int n20 = n19;
        if (this.zzu != null) {
            n20 = n19 + zzgmm.zzf(20, this.zzu);
        }
        int n21 = n20;
        if (this.zzv != null) {
            n21 = n20 + zzgmm.zzf(21, this.zzv);
        }
        return n21;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza != null) {
            zzgmm.zzb(1, this.zza);
        }
        if (this.zzb != null) {
            zzgmm.zzb(2, this.zzb);
        }
        if (this.zzc != null) {
            zzgmm.zzb(3, this.zzc);
        }
        if (this.zzd != null) {
            zzgmm.zzb(4, this.zzd);
        }
        if (this.zze != null) {
            zzgmm.zzb(5, this.zze);
        }
        if (this.zzf != null) {
            zzgmm.zzb(6, this.zzf);
        }
        if (this.zzg != null) {
            zzgmm.zza(7, this.zzg);
        }
        if (this.zzh != null) {
            zzgmm.zzb(8, this.zzh);
        }
        if (this.zzi != null) {
            zzgmm.zzb(9, this.zzi);
        }
        if (this.zzj != null) {
            zzgmm.zzb(10, this.zzj);
        }
        if (this.zzk != null) {
            zzgmm.zza(11, this.zzk);
        }
        if (this.zzl != null) {
            zzgmm.zzb(12, this.zzl);
        }
        if (this.zzm != null) {
            zzgmm.zzb(13, this.zzm);
        }
        if (this.zzn != null) {
            zzgmm.zzb(14, this.zzn);
        }
        if (this.zzo != null) {
            zzgmm.zzb(15, this.zzo);
        }
        if (this.zzp != null) {
            zzgmm.zzb(16, this.zzp);
        }
        if (this.zzq != null) {
            zzgmm.zzb(17, this.zzq);
        }
        if (this.zzr != null) {
            zzgmm.zzb(18, this.zzr);
        }
        if (this.zzs != null) {
            zzgmm.zzb(19, this.zzs);
        }
        if (this.zzu != null) {
            zzgmm.zzb(20, this.zzu);
        }
        if (this.zzv != null) {
            zzgmm.zzb(21, this.zzv);
        }
        super.writeTo(zzgmm);
    }
}
