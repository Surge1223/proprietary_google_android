package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.IBinder;

public final class zzacj extends zzey implements zzach
{
    zzacj(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(1, this.a_());
    }
    
    @Override
    public final void zza(final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeInt(n);
        this.zzb(7, a_);
    }
    
    @Override
    public final void zza(final zzabz zzabz) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzabz);
        this.zzb(5, a_);
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zzc() throws RemoteException {
        this.zzb(3, this.a_());
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(4, this.a_());
    }
    
    @Override
    public final void zze() throws RemoteException {
        this.zzb(6, this.a_());
    }
    
    @Override
    public final void zzf() throws RemoteException {
        this.zzb(8, this.a_());
    }
}
