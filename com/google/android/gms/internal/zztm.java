package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;
import android.os.Parcel;
import android.os.IBinder;

public final class zztm extends zzey implements zztk
{
    zztm(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }
    
    @Override
    public final zztn zza(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        final Parcel zza = this.zza(1, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zztn zztn;
        if (strongBinder == null) {
            zztn = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
            if (queryLocalInterface instanceof zztn) {
                zztn = (zztn)queryLocalInterface;
            }
            else {
                zztn = new zztp(strongBinder);
            }
        }
        zza.recycle();
        return zztn;
    }
    
    @Override
    public final boolean zzb(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        final Parcel zza = this.zza(2, a_);
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
}
