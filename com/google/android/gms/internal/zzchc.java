package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzchc extends zzey implements zzchb
{
    zzchc(final IBinder binder) {
        super(binder, "com.google.android.gms.feedback.internal.IFeedbackService");
    }
}
