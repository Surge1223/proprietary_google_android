package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;
import java.util.SortedMap;
import java.util.Collections;
import java.util.List;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Iterator;

final class zzglj implements Iterator<Map.Entry<Object, Object>>
{
    private int zza;
    private boolean zzb;
    private Iterator<Map.Entry<Object, Object>> zzc;
    private final /* synthetic */ zzglb zzd;
    
    private zzglj(final zzglb zzd) {
        this.zzd = zzd;
        this.zza = -1;
    }
    
    private final Iterator<Map.Entry<Object, Object>> zza() {
        if (this.zzc == null) {
            this.zzc = this.zzd.zzc.entrySet().iterator();
        }
        return this.zzc;
    }
    
    @Override
    public final boolean hasNext() {
        return this.zza + 1 < this.zzd.zzb.size() || (!this.zzd.zzc.isEmpty() && this.zza().hasNext());
    }
    
    @Override
    public final void remove() {
        if (!this.zzb) {
            throw new IllegalStateException("remove() was called before next()");
        }
        this.zzb = false;
        this.zzd.zzf();
        if (this.zza < this.zzd.zzb.size()) {
            this.zzd.zzc(this.zza--);
            return;
        }
        this.zza().remove();
    }
}
