package com.google.android.gms.internal;

import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzcje extends zzey implements zzcjc
{
    zzcje(final IBinder binder) {
        super(binder, "com.google.android.gms.flags.IFlagProvider");
    }
    
    @Override
    public final boolean getBooleanFlagValue(final String s, final boolean b, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        zzfa.zza(a_, b);
        a_.writeInt(n);
        final Parcel zza = this.zza(2, a_);
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final int getIntFlagValue(final String s, int int1, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        a_.writeInt(int1);
        a_.writeInt(n);
        final Parcel zza = this.zza(3, a_);
        int1 = zza.readInt();
        zza.recycle();
        return int1;
    }
    
    @Override
    public final long getLongFlagValue(final String s, long long1, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        a_.writeLong(long1);
        a_.writeInt(n);
        final Parcel zza = this.zza(4, a_);
        long1 = zza.readLong();
        zza.recycle();
        return long1;
    }
    
    @Override
    public final String getStringFlagValue(String string, final String s, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(string);
        a_.writeString(s);
        a_.writeInt(n);
        final Parcel zza = this.zza(5, a_);
        string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void init(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(1, a_);
    }
}
