package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgnm
{
    public static final class zza extends zzgiw<zzgnm.zza, zzgnm.zza.zza> implements zzgki
    {
        private static final zzgnm.zza zzh;
        private static volatile zzgkr<zzgnm.zza> zzi;
        private int zzd;
        private int zze;
        private String zzf;
        private String zzg;
        
        static {
            (zzh = new zzgnm.zza()).zzl();
            zzgiw.zza(zzgnm.zza.class, zzgnm.zza.zzh);
        }
        
        private zza() {
            this.zzf = "";
            this.zzg = "";
        }
        
        @Override
        public final int zza() {
            final int zzc = this.zzc;
            if (zzc != -1) {
                return zzc;
            }
            final int zzd = this.zzd;
            int n = 0;
            if ((zzd & 0x1) == 0x1) {
                n = 0 + zzgic.zzf(1, this.zze);
            }
            int n2 = n;
            if ((this.zzd & 0x2) == 0x2) {
                n2 = n + zzgic.zzb(2, this.zzf);
            }
            int n3 = n2;
            if ((this.zzd & 0x4) == 0x4) {
                n3 = n2 + zzgic.zzb(3, this.zzg);
            }
            return this.zzc = n3 + this.zzb.zze();
        }
        
        @Override
        protected final Object zza(final int n, Object o, final Object o2) {
            switch (zzgnn.zza[n - 1]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 8: {
                    return null;
                }
                case 7: {
                    return 1;
                }
                case 6: {
                    if (zzgnm.zza.zzi == null) {
                        synchronized (zzgnm.zza.class) {
                            if (zzgnm.zza.zzi == null) {
                                o = (zzgnm.zza.zzi = new zzb<zzgnm.zza>(zzgnm.zza.zzh));
                            }
                        }
                    }
                    return zzgnm.zza.zzi;
                }
                case 5: {
                    return zzgnm.zza.zzh;
                }
                case 4: {
                    return new zzgnm.zza.zza((zzgnn)null);
                }
                case 3: {
                    return null;
                }
                case 2: {
                    return zzgnm.zza.zzh;
                }
                case 1: {
                    return new zzgnm.zza();
                }
            }
        }
        
        @Override
        public final void zza(final zzgic zzgic) throws IOException {
            if ((this.zzd & 0x1) == 0x1) {
                zzgic.zzb(1, this.zze);
            }
            if ((this.zzd & 0x2) == 0x2) {
                zzgic.zza(2, this.zzf);
            }
            if ((this.zzd & 0x4) == 0x4) {
                zzgic.zza(3, this.zzg);
            }
            this.zzb.zza(zzgic);
        }
        
        @Override
        protected final Object zzb() throws Exception {
            return zzgiw.zza(zzgnm.zza.zzh, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\b\u0002", new Object[] { "zzd", "zze", "zzf", "zzg" });
        }
        
        public static final class zza extends zzgiw.zza<zzgnm.zza, zza> implements zzgki
        {
            private zza() {
                super(zzgnm.zza.zzh);
            }
        }
    }
}
