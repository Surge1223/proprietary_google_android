package com.google.android.gms.internal;

import android.util.Log;

public class zzaid
{
    public static void zza(final String s, final Throwable t) {
        if (zza(3)) {
            Log.d("Ads", s, t);
        }
    }
    
    public static boolean zza(final int n) {
        return n >= 5 || Log.isLoggable("Ads", n);
    }
    
    public static void zzb(final String s) {
        if (zza(3)) {
            Log.d("Ads", s);
        }
    }
    
    public static void zzb(final String s, final Throwable t) {
        if (zza(6)) {
            Log.e("Ads", s, t);
        }
    }
    
    public static void zzc(final String s) {
        if (zza(6)) {
            Log.e("Ads", s);
        }
    }
    
    public static void zzc(final String s, final Throwable t) {
        if (zza(5)) {
            Log.w("Ads", s, t);
        }
    }
    
    public static void zzd(final String s) {
        if (zza(4)) {
            Log.i("Ads", s);
        }
    }
    
    public static void zze(final String s) {
        if (zza(5)) {
            Log.w("Ads", s);
        }
    }
}
