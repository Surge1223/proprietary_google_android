package com.google.android.gms.internal;

import java.nio.ByteBuffer;

final class zzgmc extends zzglz
{
    private static int zza(final byte[] array, final int n, final long n2, final int n3) {
        switch (n3) {
            default: {
                throw new AssertionError();
            }
            case 2: {
                return zzb(n, zzglw.zza(array, n2), zzglw.zza(array, n2 + 1L));
            }
            case 1: {
                return zzb(n, zzglw.zza(array, n2));
            }
            case 0: {
                return zzb(n);
            }
        }
    }
    
    @Override
    final int zza(int i, final byte[] array, int zza, int n) {
        if ((zza | n | array.length - n) >= 0) {
            final long n2 = zza;
            zza = (int)(n - n2);
            Label_0073: {
                if (zza < 16) {
                    i = 0;
                }
                else {
                    i = 0;
                    for (long n3 = n2; i < zza; ++i, ++n3) {
                        if (zzglw.zza(array, n3) < 0) {
                            break Label_0073;
                        }
                    }
                    i = zza;
                }
            }
            zza -= i;
            long n4 = n2 + i;
            i = zza;
            while (true) {
                zza = 0;
                long n5;
                while (true) {
                    n5 = n4;
                    if (i <= 0) {
                        break;
                    }
                    n5 = n4 + 1L;
                    zza = zzglw.zza(array, n4);
                    if (zza < 0) {
                        break;
                    }
                    --i;
                    n4 = n5;
                }
                if (i == 0) {
                    return 0;
                }
                --i;
                if (zza < -32) {
                    if (i == 0) {
                        return zza;
                    }
                    --i;
                    if (zza < -62 || zzglw.zza(array, n5) > -65) {
                        return -1;
                    }
                    n4 = n5 + 1L;
                }
                else if (zza < -16) {
                    if (i < 2) {
                        return zza(array, zza, n5, i);
                    }
                    i -= 2;
                    final long n6 = n5 + 1L;
                    n = zzglw.zza(array, n5);
                    if (n > -65 || (zza == -32 && n < -96) || (zza == -19 && n >= -96)) {
                        break;
                    }
                    n4 = n6 + 1L;
                    if (zzglw.zza(array, n6) > -65) {
                        break;
                    }
                    continue;
                }
                else {
                    if (i < 3) {
                        return zza(array, zza, n5, i);
                    }
                    i -= 3;
                    final long n7 = n5 + 1L;
                    n = zzglw.zza(array, n5);
                    if (n > -65 || (zza << 28) + (n + 112) >> 30 != 0) {
                        return -1;
                    }
                    final long n8 = n7 + 1L;
                    if (zzglw.zza(array, n7) > -65) {
                        return -1;
                    }
                    n4 = n8 + 1L;
                    if (zzglw.zza(array, n8) > -65) {
                        return -1;
                    }
                    continue;
                }
            }
            return -1;
        }
        throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", array.length, zza, n));
    }
    
    @Override
    final int zza(final CharSequence charSequence, final byte[] array, int i, int j) {
        long n = i;
        final long n2 = j + n;
        final int length = charSequence.length();
        if (length > j || array.length - j < i) {
            final char char1 = charSequence.charAt(length - 1);
            final StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(char1);
            sb.append(" at index ");
            sb.append(i + j);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        for (j = 0; j < length; ++j, ++n) {
            i = charSequence.charAt(j);
            if (i >= 128) {
                break;
            }
            zzglw.zza(array, n, (byte)i);
        }
        i = j;
        long n3 = n;
        if (j == length) {
            return (int)n;
        }
        while (i < length) {
            final char char2 = charSequence.charAt(i);
            long n4 = 0L;
            Label_0462: {
                if (char2 < '\u0080' && n3 < n2) {
                    zzglw.zza(array, n3, (byte)char2);
                    n4 = n3 + 1L;
                }
                else if (char2 < '\u0800' && n3 <= n2 - 2L) {
                    final long n5 = n3 + 1L;
                    zzglw.zza(array, n3, (byte)('\u03c0' | char2 >>> 6));
                    n4 = n5 + 1L;
                    zzglw.zza(array, n5, (byte)((char2 & '?') | '\u0080'));
                }
                else if ((char2 < '\ud800' || '\udfff' < char2) && n3 <= n2 - 3L) {
                    final long n6 = n3 + 1L;
                    zzglw.zza(array, n3, (byte)('\u01e0' | char2 >>> 12));
                    final long n7 = n6 + 1L;
                    zzglw.zza(array, n6, (byte)((char2 >>> 6 & '?') | '\u0080'));
                    zzglw.zza(array, n7, (byte)((char2 & '?') | '\u0080'));
                    n4 = n7 + 1L;
                }
                else {
                    if (n3 <= n2 - 4L) {
                        j = i + 1;
                        if (j != length) {
                            final char char3 = charSequence.charAt(j);
                            i = j;
                            if (Character.isSurrogatePair(char2, char3)) {
                                i = Character.toCodePoint(char2, char3);
                                final long n8 = n3 + 1L;
                                zzglw.zza(array, n3, (byte)(0xF0 | i >>> 18));
                                final long n9 = n8 + 1L;
                                zzglw.zza(array, n8, (byte)((i >>> 12 & 0x3F) | 0x80));
                                final long n10 = n9 + 1L;
                                zzglw.zza(array, n9, (byte)((i >>> 6 & 0x3F) | 0x80));
                                n4 = n10 + 1L;
                                zzglw.zza(array, n10, (byte)((i & 0x3F) | 0x80));
                                i = j;
                                break Label_0462;
                            }
                        }
                        throw new zzgmb(i - 1, length);
                    }
                    if ('\ud800' <= char2 && char2 <= '\udfff') {
                        j = i + 1;
                        if (j == length || !Character.isSurrogatePair(char2, charSequence.charAt(j))) {
                            throw new zzgmb(i, length);
                        }
                    }
                    final StringBuilder sb2 = new StringBuilder(46);
                    sb2.append("Failed writing ");
                    sb2.append(char2);
                    sb2.append(" at index ");
                    sb2.append(n3);
                    throw new ArrayIndexOutOfBoundsException(sb2.toString());
                }
            }
            ++i;
            n3 = n4;
        }
        return (int)n3;
    }
    
    @Override
    final void zza(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        final long zza = zzglw.zza(byteBuffer);
        long n = byteBuffer.position() + zza;
        final long n2 = byteBuffer.limit() + zza;
        final int length = charSequence.length();
        if (length > n2 - n) {
            final char char1 = charSequence.charAt(length - 1);
            final int limit = byteBuffer.limit();
            final StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(char1);
            sb.append(" at index ");
            sb.append(limit);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i;
        for (i = 0; i < length; ++i, ++n) {
            final char char2 = charSequence.charAt(i);
            if (char2 >= '\u0080') {
                break;
            }
            zzglw.zza(n, (byte)char2);
        }
        final long n3 = zza;
        long n4 = n;
        int j;
        if ((j = i) == length) {
            byteBuffer.position((int)(n - zza));
            return;
        }
        while (j < length) {
            final char char3 = charSequence.charAt(j);
            long n5 = 0L;
            if (char3 < '\u0080' && n4 < n2) {
                zzglw.zza(n4, (byte)char3);
                n5 = n4 + 1L;
            }
            else {
                Label_0229: {
                    if (char3 < '\u0800' && n4 <= n2 - 2L) {
                        final long n6 = n4 + 1L;
                        zzglw.zza(n4, (byte)('\u03c0' | char3 >>> 6));
                        n5 = n6 + 1L;
                        zzglw.zza(n6, (byte)(('?' & char3) | '\u0080'));
                    }
                    else if ((char3 < '\ud800' || '\udfff' < char3) && n4 <= n2 - 3L) {
                        final long n7 = n4 + 1L;
                        zzglw.zza(n4, (byte)('\u01e0' | char3 >>> 12));
                        final long n8 = n7 + 1L;
                        zzglw.zza(n7, (byte)((char3 >>> 6 & '?') | '\u0080'));
                        zzglw.zza(n8, (byte)(('?' & char3) | '\u0080'));
                        n5 = n8 + 1L;
                    }
                    else {
                        if (n4 <= n2 - 4L) {
                            final int n9 = j + 1;
                            if (n9 != length) {
                                final char char4 = charSequence.charAt(n9);
                                j = n9;
                                if (Character.isSurrogatePair(char3, char4)) {
                                    final int codePoint = Character.toCodePoint(char3, char4);
                                    final long n10 = n4 + 1L;
                                    zzglw.zza(n4, (byte)(0xF0 | codePoint >>> 18));
                                    final long n11 = n10 + 1L;
                                    zzglw.zza(n10, (byte)((codePoint >>> 12 & 0x3F) | 0x80));
                                    final long n12 = n11 + 1L;
                                    zzglw.zza(n11, (byte)((codePoint >>> 6 & 0x3F) | 0x80));
                                    n5 = n12 + 1L;
                                    zzglw.zza(n12, (byte)((codePoint & 0x3F) | 0x80));
                                    j = n9;
                                    break Label_0229;
                                }
                            }
                            throw new zzgmb(j - 1, length);
                        }
                        if ('\ud800' <= char3 && char3 <= '\udfff') {
                            final int n13 = j + 1;
                            if (n13 == length || !Character.isSurrogatePair(char3, charSequence.charAt(n13))) {
                                throw new zzgmb(j, length);
                            }
                        }
                        final StringBuilder sb2 = new StringBuilder(46);
                        sb2.append("Failed writing ");
                        sb2.append(char3);
                        sb2.append(" at index ");
                        sb2.append(n4);
                        throw new ArrayIndexOutOfBoundsException(sb2.toString());
                    }
                }
            }
            ++j;
            n4 = n5;
        }
        byteBuffer.position((int)(n4 - n3));
    }
}
