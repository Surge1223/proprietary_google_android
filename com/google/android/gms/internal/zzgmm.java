package com.google.android.gms.internal;

import java.io.IOException;
import java.nio.ReadOnlyBufferException;
import java.nio.BufferOverflowException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public final class zzgmm
{
    private final ByteBuffer zza;
    private zzgic zzb;
    private int zzc;
    
    private zzgmm(final ByteBuffer zza) {
        (this.zza = zza).order(ByteOrder.LITTLE_ENDIAN);
    }
    
    private zzgmm(final byte[] array, final int n, final int n2) {
        this(ByteBuffer.wrap(array, n, n2));
    }
    
    public static int zza(final int n) {
        if (n >= 0) {
            return zzd(n);
        }
        return 10;
    }
    
    private static int zza(final CharSequence charSequence) {
        final int length = charSequence.length();
        final char c = '\0';
        char c2;
        for (c2 = '\0'; c2 < length && charSequence.charAt(c2) < '\u0080'; ++c2) {}
        char c3 = (char)length;
        char c4;
        while (true) {
            c4 = c3;
            if (c2 >= length) {
                break;
            }
            final char char1 = charSequence.charAt(c2);
            if (char1 >= '\u0800') {
                final int length2 = charSequence.length();
                int n = c;
                while (c2 < length2) {
                    final char char2 = charSequence.charAt(c2);
                    char c5;
                    if (char2 < '\u0800') {
                        n += '\u007f' - char2 >>> 31;
                        c5 = c2;
                    }
                    else {
                        final char c6 = (char)(n += 2);
                        c5 = c2;
                        if ('\ud800' <= char2) {
                            n = c6;
                            c5 = c2;
                            if (char2 <= '\udfff') {
                                if (Character.codePointAt(charSequence, c2) < 65536) {
                                    final StringBuilder sb = new StringBuilder(39);
                                    sb.append("Unpaired surrogate at index ");
                                    sb.append((int)c2);
                                    throw new IllegalArgumentException(sb.toString());
                                }
                                c5 = (char)(c2 + '\u0001');
                                n = c6;
                            }
                        }
                    }
                    c2 = (char)(c5 + '\u0001');
                }
                c4 = (char)(c3 + n);
                break;
            }
            c3 += (char)('\u007f' - char1 >>> 31);
            ++c2;
        }
        if (c4 >= length) {
            return c4;
        }
        final long n2 = c4;
        final StringBuilder sb2 = new StringBuilder(54);
        sb2.append("UTF-8 length does not fit in int: ");
        sb2.append(n2 + 4294967296L);
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public static int zza(final String s) {
        final int zza = zza((CharSequence)s);
        return zzd(zza) + zza;
    }
    
    public static zzgmm zza(final byte[] array) {
        return zza(array, 0, array.length);
    }
    
    public static zzgmm zza(final byte[] array, final int n, final int n2) {
        return new zzgmm(array, n, n2);
    }
    
    private static void zza(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        if (!byteBuffer.isReadOnly()) {
            final boolean hasArray = byteBuffer.hasArray();
            final int n = 0;
            int i = 0;
            if (hasArray) {
                try {
                    final byte[] array = byteBuffer.array();
                    final int n2 = byteBuffer.arrayOffset() + byteBuffer.position();
                    final int remaining = byteBuffer.remaining();
                    final int length = charSequence.length();
                    final int n3 = remaining + n2;
                    while (i < length) {
                        final int n4 = i + n2;
                        if (n4 >= n3) {
                            break;
                        }
                        final char char1 = charSequence.charAt(i);
                        if (char1 >= '\u0080') {
                            break;
                        }
                        array[n4] = (byte)char1;
                        ++i;
                    }
                    int n5;
                    if (i == length) {
                        n5 = n2 + length;
                    }
                    else {
                        n5 = n2 + i;
                        while (i < length) {
                            final char char2 = charSequence.charAt(i);
                            Label_0481: {
                                if (char2 < '\u0080' && n5 < n3) {
                                    final int n6 = n5 + 1;
                                    array[n5] = (byte)char2;
                                    n5 = n6;
                                }
                                else if (char2 < '\u0800' && n5 <= n3 - 2) {
                                    final int n7 = n5 + 1;
                                    array[n5] = (byte)('\u03c0' | char2 >>> 6);
                                    array[n7] = (byte)((char2 & '?') | '\u0080');
                                    n5 = n7 + 1;
                                }
                                else if ((char2 < '\ud800' || '\udfff' < char2) && n5 <= n3 - 3) {
                                    final int n8 = n5 + 1;
                                    array[n5] = (byte)(char2 >>> 12 | '\u01e0');
                                    final int n9 = n8 + 1;
                                    array[n8] = (byte)((char2 >>> 6 & '?') | '\u0080');
                                    n5 = n9 + 1;
                                    array[n9] = (byte)((char2 & '?') | '\u0080');
                                }
                                else {
                                    if (n5 <= n3 - 4) {
                                        final int n10 = i + 1;
                                        if (n10 != charSequence.length()) {
                                            final char char3 = charSequence.charAt(n10);
                                            i = n10;
                                            if (Character.isSurrogatePair(char2, char3)) {
                                                final int codePoint = Character.toCodePoint(char2, char3);
                                                final int n11 = n5 + 1;
                                                array[n5] = (byte)(0xF0 | codePoint >>> 18);
                                                final int n12 = n11 + 1;
                                                array[n11] = (byte)((codePoint >>> 12 & 0x3F) | 0x80);
                                                final int n13 = n12 + 1;
                                                array[n12] = (byte)((codePoint >>> 6 & 0x3F) | 0x80);
                                                array[n13] = (byte)((codePoint & 0x3F) | 0x80);
                                                n5 = n13 + 1;
                                                i = n10;
                                                break Label_0481;
                                            }
                                        }
                                        final StringBuilder sb = new StringBuilder(39);
                                        sb.append("Unpaired surrogate at index ");
                                        sb.append(i - 1);
                                        throw new IllegalArgumentException(sb.toString());
                                    }
                                    final StringBuilder sb2 = new StringBuilder(37);
                                    sb2.append("Failed writing ");
                                    sb2.append(char2);
                                    sb2.append(" at index ");
                                    sb2.append(n5);
                                    throw new ArrayIndexOutOfBoundsException(sb2.toString());
                                }
                            }
                            ++i;
                        }
                    }
                    byteBuffer.position(n5 - byteBuffer.arrayOffset());
                    return;
                }
                catch (ArrayIndexOutOfBoundsException ex2) {
                    final BufferOverflowException ex = new BufferOverflowException();
                    ex.initCause(ex2);
                    throw ex;
                }
            }
            for (int length2 = charSequence.length(), j = n; j < length2; ++j) {
                final char char4 = charSequence.charAt(j);
                if (char4 < '\u0080') {
                    byteBuffer.put((byte)char4);
                }
                else if (char4 < '\u0800') {
                    byteBuffer.put((byte)(char4 >>> 6 | '\u03c0'));
                    byteBuffer.put((byte)((char4 & '?') | '\u0080'));
                }
                else {
                    if (char4 >= '\ud800' && '\udfff' >= char4) {
                        final int n14 = j + 1;
                        if (n14 != charSequence.length()) {
                            final char char5 = charSequence.charAt(n14);
                            j = n14;
                            if (Character.isSurrogatePair(char4, char5)) {
                                final int codePoint2 = Character.toCodePoint(char4, char5);
                                byteBuffer.put((byte)(codePoint2 >>> 18 | 0xF0));
                                byteBuffer.put((byte)((codePoint2 >>> 12 & 0x3F) | 0x80));
                                byteBuffer.put((byte)((codePoint2 >>> 6 & 0x3F) | 0x80));
                                byteBuffer.put((byte)((codePoint2 & 0x3F) | 0x80));
                                j = n14;
                                continue;
                            }
                        }
                        final StringBuilder sb3 = new StringBuilder(39);
                        sb3.append("Unpaired surrogate at index ");
                        sb3.append(j - 1);
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    byteBuffer.put((byte)(char4 >>> 12 | '\u01e0'));
                    byteBuffer.put((byte)((char4 >>> 6 & '?') | '\u0080'));
                    byteBuffer.put((byte)((char4 & '?') | '\u0080'));
                }
            }
            return;
        }
        throw new ReadOnlyBufferException();
    }
    
    public static int zzb(final int n) {
        return zzd(n << 3);
    }
    
    public static int zzb(final int n, final int n2) {
        return zzb(n) + zza(n2);
    }
    
    public static int zzb(int serializedSize, final zzgmu zzgmu) {
        final int zzb = zzb(serializedSize);
        serializedSize = zzgmu.getSerializedSize();
        return zzb + (zzd(serializedSize) + serializedSize);
    }
    
    public static int zzb(final int n, final String s) {
        return zzb(n) + zza(s);
    }
    
    public static int zzb(final int n, final byte[] array) {
        return zzb(n) + zzc(array);
    }
    
    public static int zzb(final long n) {
        if ((0xFFFFFFFFFFFFFF80L & n) == 0x0L) {
            return 1;
        }
        if ((0xFFFFFFFFFFFFC000L & n) == 0x0L) {
            return 2;
        }
        if ((0xFFFFFFFFFFE00000L & n) == 0x0L) {
            return 3;
        }
        if ((0xFFFFFFFFF0000000L & n) == 0x0L) {
            return 4;
        }
        if ((0xFFFFFFF800000000L & n) == 0x0L) {
            return 5;
        }
        if ((0xFFFFFC0000000000L & n) == 0x0L) {
            return 6;
        }
        if ((0xFFFE000000000000L & n) == 0x0L) {
            return 7;
        }
        if ((0xFF00000000000000L & n) == 0x0L) {
            return 8;
        }
        if ((n & Long.MIN_VALUE) == 0x0L) {
            return 9;
        }
        return 10;
    }
    
    private final zzgic zzb() throws IOException {
        if (this.zzb == null) {
            this.zzb = zzgic.zza(this.zza);
            this.zzc = this.zza.position();
        }
        else if (this.zzc != this.zza.position()) {
            this.zzb.zzb(this.zza.array(), this.zzc, this.zza.position() - this.zzc);
            this.zzc = this.zza.position();
        }
        return this.zzb;
    }
    
    public static int zzc(final byte[] array) {
        return zzd(array.length) + array.length;
    }
    
    private final void zzc(final long n) throws IOException {
        if (this.zza.remaining() >= 8) {
            this.zza.putLong(n);
            return;
        }
        throw new zzgmn(this.zza.position(), this.zza.limit());
    }
    
    public static int zzd(final int n) {
        if ((n & 0xFFFFFF80) == 0x0) {
            return 1;
        }
        if ((n & 0xFFFFC000) == 0x0) {
            return 2;
        }
        if ((0xFFE00000 & n) == 0x0) {
            return 3;
        }
        if ((n & 0xF0000000) == 0x0) {
            return 4;
        }
        return 5;
    }
    
    private static long zzd(final long n) {
        return n >> 63 ^ n << 1;
    }
    
    public static int zze(final int n, final long n2) {
        return zzb(n) + zzb(n2);
    }
    
    public static int zzf(final int n, final long n2) {
        return zzb(n) + zzb(n2);
    }
    
    private final void zzf(final int n) throws IOException {
        final byte b = (byte)n;
        if (this.zza.hasRemaining()) {
            this.zza.put(b);
            return;
        }
        throw new zzgmn(this.zza.position(), this.zza.limit());
    }
    
    public static int zzg(final int n, final long n2) {
        return zzb(n) + zzb(zzd(n2));
    }
    
    public final void zza() {
        if (this.zza.remaining() == 0) {
            return;
        }
        throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", this.zza.remaining()));
    }
    
    public final void zza(final int n, final double n2) throws IOException {
        this.zzc(n, 1);
        this.zzc(Double.doubleToLongBits(n2));
    }
    
    public final void zza(final int n, final int n2) throws IOException {
        this.zzc(n, 0);
        if (n2 >= 0) {
            this.zzc(n2);
            return;
        }
        this.zza((long)n2);
    }
    
    public final void zza(final int n, final long n2) throws IOException {
        this.zzc(n, 0);
        this.zza(n2);
    }
    
    public final void zza(final int n, final zzgkg zzgkg) throws IOException {
        final zzgic zzb = this.zzb();
        zzb.zza(n, zzgkg);
        zzb.zza();
        this.zzc = this.zza.position();
    }
    
    public final void zza(final int n, final zzgmu zzgmu) throws IOException {
        this.zzc(n, 2);
        this.zza(zzgmu);
    }
    
    public final void zza(int position, final String s) throws IOException {
        this.zzc(position, 2);
        try {
            final int zzd = zzd(s.length());
            if (zzd != zzd(s.length() * 3)) {
                this.zzc(zza((CharSequence)s));
                zza(s, this.zza);
                return;
            }
            position = this.zza.position();
            if (this.zza.remaining() >= zzd) {
                this.zza.position(position + zzd);
                zza(s, this.zza);
                final int position2 = this.zza.position();
                this.zza.position(position);
                this.zzc(position2 - position - zzd);
                this.zza.position(position2);
                return;
            }
            throw new zzgmn(position + zzd, this.zza.limit());
        }
        catch (BufferOverflowException ex) {
            final zzgmn zzgmn = new zzgmn(this.zza.position(), this.zza.limit());
            zzgmn.initCause(ex);
            throw zzgmn;
        }
    }
    
    public final void zza(final int n, final boolean b) throws IOException {
        this.zzc(n, 0);
        final byte b2 = (byte)(b ? 1 : 0);
        if (this.zza.hasRemaining()) {
            this.zza.put(b2);
            return;
        }
        throw new zzgmn(this.zza.position(), this.zza.limit());
    }
    
    public final void zza(final int n, final byte[] array) throws IOException {
        this.zzc(n, 2);
        this.zzb(array);
    }
    
    public final void zza(long n) throws IOException {
        while ((0xFFFFFFFFFFFFFF80L & n) != 0x0L) {
            this.zzf(((int)n & 0x7F) | 0x80);
            n >>>= 7;
        }
        this.zzf((int)n);
    }
    
    public final void zza(final zzgmu zzgmu) throws IOException {
        this.zzc(zzgmu.getCachedSize());
        zzgmu.writeTo(this);
    }
    
    public final void zzb(final int n, final long n2) throws IOException {
        this.zzc(n, 0);
        this.zza(n2);
    }
    
    public final void zzb(final byte[] array) throws IOException {
        this.zzc(array.length);
        this.zzd(array);
    }
    
    public final void zzc(int n) throws IOException {
        while ((n & 0xFFFFFF80) != 0x0) {
            this.zzf((n & 0x7F) | 0x80);
            n >>>= 7;
        }
        this.zzf(n);
    }
    
    public final void zzc(final int n, final int n2) throws IOException {
        this.zzc(n << 3 | n2);
    }
    
    public final void zzd(final int n, final long n2) throws IOException {
        this.zzc(n, 0);
        this.zza(zzd(n2));
    }
    
    public final void zzd(final byte[] array) throws IOException {
        final int length = array.length;
        if (this.zza.remaining() >= length) {
            this.zza.put(array, 0, length);
            return;
        }
        throw new zzgmn(this.zza.position(), this.zza.limit());
    }
}
