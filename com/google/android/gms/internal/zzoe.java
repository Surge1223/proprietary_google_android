package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IBinder;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzoe extends IInterface
{
    IBinder zza(final IObjectWrapper p0, final IObjectWrapper p1, final IObjectWrapper p2, final int p3) throws RemoteException;
}
