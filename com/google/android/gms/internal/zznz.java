package com.google.android.gms.internal;

import android.os.Parcelable.Creator;
import android.net.Uri;
import android.os.RemoteException;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;

public final class zznz extends zzey implements zznx
{
    zznz(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeAdImage");
    }
    
    @Override
    public final IObjectWrapper zza() throws RemoteException {
        final Parcel zza = this.zza(1, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final Uri zzb() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final Uri uri = zzfa.zza(zza, (android.os.Parcelable.Creator<Uri>)Uri.CREATOR);
        zza.recycle();
        return uri;
    }
    
    @Override
    public final double zzc() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final double double1 = zza.readDouble();
        zza.recycle();
        return double1;
    }
}
