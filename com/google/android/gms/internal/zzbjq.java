package com.google.android.gms.internal;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.Context;

public final class zzbjq
{
    private final Context zza;
    
    public zzbjq(final Context zza) {
        this.zza = zza;
    }
    
    public final ApplicationInfo zza(final String s, final int n) throws PackageManager$NameNotFoundException {
        return this.zza.getPackageManager().getApplicationInfo(s, n);
    }
    
    public final PackageInfo zzb(final String s, final int n) throws PackageManager$NameNotFoundException {
        return this.zza.getPackageManager().getPackageInfo(s, n);
    }
    
    public final CharSequence zzb(final String s) throws PackageManager$NameNotFoundException {
        return this.zza.getPackageManager().getApplicationLabel(this.zza.getPackageManager().getApplicationInfo(s, 0));
    }
}
