package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgmp<M extends zzgmo<M>, T>
{
    protected final Class<T> zza;
    public final int zzb;
    protected final boolean zzc;
    private final int zzd;
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzgmp)) {
            return false;
        }
        final zzgmp zzgmp = (zzgmp)o;
        return this.zzd == zzgmp.zzd && this.zza == zzgmp.zza && this.zzb == zzgmp.zzb && this.zzc == zzgmp.zzc;
    }
    
    @Override
    public final int hashCode() {
        return (((1147 + this.zzd) * 31 + this.zza.hashCode()) * 31 + this.zzb) * 31 + (this.zzc ? 1 : 0);
    }
    
    protected final int zza(final Object o) {
        final int n = this.zzb >>> 3;
        switch (this.zzd) {
            default: {
                final int zzd = this.zzd;
                final StringBuilder sb = new StringBuilder(24);
                sb.append("Unknown type ");
                sb.append(zzd);
                throw new IllegalArgumentException(sb.toString());
            }
            case 11: {
                return zzgmm.zzb(n, (zzgmu)o);
            }
            case 10: {
                return (zzgmm.zzb(n) << 1) + ((zzgmu)o).getSerializedSize();
            }
        }
    }
    
    protected final void zza(Object o, final zzgmm zzgmm) {
        try {
            zzgmm.zzc(this.zzb);
            switch (this.zzd) {
                default: {
                    final int zzd = this.zzd;
                    o = new StringBuilder(24);
                    ((StringBuilder)o).append("Unknown type ");
                    ((StringBuilder)o).append(zzd);
                    throw new IllegalArgumentException(((StringBuilder)o).toString());
                }
                case 11: {
                    zzgmm.zza((zzgmu)o);
                }
                case 10: {
                    final int zzb = this.zzb;
                    ((zzgmu)o).writeTo(zzgmm);
                    zzgmm.zzc(zzb >>> 3, 4);
                }
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
}
