package com.google.android.gms.internal;

import java.io.File;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzcqh extends zzbid
{
    public static final Parcelable.Creator<zzcqh> CREATOR;
    private final byte[] zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzdla((android.os.Parcelable.Creator<zzbih>)new zzcqj());
    }
    
    public zzcqh(final byte[] zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        zzdla.zza(this, parcel, new zzcqi(n), null);
    }
}
