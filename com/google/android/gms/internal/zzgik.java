package com.google.android.gms.internal;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import java.util.Map;

final class zzgik extends zzgij<zzgiw.zze>
{
    @Override
    final int zza(final Map.Entry<?, ?> entry) {
        return ((zzgiw.zze)entry.getKey()).zzb;
    }
    
    @Override
    final zzgin<zzgiw.zze> zza(final Object o) {
        return ((zzgiw.zzd)o).zzd;
    }
    
    @Override
    final void zza(final zzgmk zzgmk, final Map.Entry<?, ?> entry) {
        final zzgiw.zze zze = (zzgiw.zze)entry.getKey();
        switch (zzgil.zza[zze.zzc.ordinal()]) {
            case 18: {
                zzgmk.zza(zze.zzb, entry.getValue());
                break;
            }
            case 17: {
                zzgmk.zzb(zze.zzb, entry.getValue());
            }
            case 16: {
                zzgmk.zza(zze.zzb, (String)entry.getValue());
            }
            case 15: {
                zzgmk.zza(zze.zzb, (zzgho)entry.getValue());
            }
            case 14: {
                zzgmk.zzc(zze.zzb, (int)entry.getValue());
            }
            case 13: {
                zzgmk.zze(zze.zzb, (long)entry.getValue());
            }
            case 12: {
                zzgmk.zzf(zze.zzb, (int)entry.getValue());
            }
            case 11: {
                zzgmk.zzb(zze.zzb, (long)entry.getValue());
            }
            case 10: {
                zzgmk.zza(zze.zzb, (int)entry.getValue());
            }
            case 9: {
                zzgmk.zze(zze.zzb, (int)entry.getValue());
            }
            case 8: {
                zzgmk.zza(zze.zzb, (boolean)entry.getValue());
            }
            case 7: {
                zzgmk.zzd(zze.zzb, (int)entry.getValue());
            }
            case 6: {
                zzgmk.zzd(zze.zzb, (long)entry.getValue());
            }
            case 5: {
                zzgmk.zzc(zze.zzb, (int)entry.getValue());
            }
            case 4: {
                zzgmk.zzc(zze.zzb, (long)entry.getValue());
            }
            case 3: {
                zzgmk.zza(zze.zzb, (long)entry.getValue());
            }
            case 2: {
                zzgmk.zza(zze.zzb, (float)entry.getValue());
            }
            case 1: {
                zzgmk.zza(zze.zzb, (double)entry.getValue());
            }
        }
    }
    
    @Override
    final void zza(final Object o, final zzgin<zzgiw.zze> zzd) {
        ((zzgiw.zzd)o).zzd = zzd;
    }
    
    @Override
    final boolean zza(final Class<?> clazz) {
        return zzgiw.zzd.class.isAssignableFrom(clazz);
    }
    
    @Override
    final zzgin<zzgiw.zze> zzb(final Object o) {
        zzgin<zzgiw.zze> zza;
        final zzgin<zzgiw.zze> zzgin = zza = this.zza(o);
        if (zzgin.zzd()) {
            zza = (zzgin<zzgiw.zze>)zzgin.clone();
            this.zza(o, zza);
        }
        return zza;
    }
}
