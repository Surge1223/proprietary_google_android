package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collection;

public final class zzlj
{
    private final Collection<Object<?>> zza;
    private final Collection<Object<String>> zzb;
    private final Collection<Object<String>> zzc;
    
    public zzlj() {
        this.zza = new ArrayList<Object<?>>();
        this.zzb = new ArrayList<Object<String>>();
        this.zzc = new ArrayList<Object<String>>();
    }
}
