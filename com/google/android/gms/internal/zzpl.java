package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzao;
import android.os.IBinder;

public final class zzpl extends zzey implements zzpj
{
    zzpl(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IOnPublisherAdViewLoadedListener");
    }
    
    @Override
    public final void zza(final zzao zzao, final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzao);
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(1, a_);
    }
}
