package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IBinder;

public final class zzoi extends zzey implements zzog
{
    zzoi(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegate");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(1, a_);
    }
}
