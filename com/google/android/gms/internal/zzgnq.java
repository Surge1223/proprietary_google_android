package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzgnq extends zzgmo<zzgnq> implements Cloneable
{
    public long zza;
    public long zzb;
    public String zzc;
    public int zzd;
    public int zze;
    public byte[] zzf;
    public long zzg;
    public byte[] zzh;
    private long zzi;
    private boolean zzj;
    private zzgnr[] zzk;
    private byte[] zzl;
    private zzgnm.zza zzm;
    private String zzn;
    private String zzo;
    private zzgno zzp;
    private String zzq;
    private zzgnp zzr;
    private String zzs;
    private int zzt;
    private int[] zzu;
    private long zzv;
    private zzgns zzw;
    private boolean zzx;
    
    public zzgnq() {
        this.zza = 0L;
        this.zzb = 0L;
        this.zzi = 0L;
        this.zzc = "";
        this.zzd = 0;
        this.zze = 0;
        this.zzj = false;
        this.zzk = zzgnr.zza();
        this.zzl = zzgmx.zzh;
        this.zzm = null;
        this.zzf = zzgmx.zzh;
        this.zzn = "";
        this.zzo = "";
        this.zzp = null;
        this.zzq = "";
        this.zzg = 180000L;
        this.zzr = null;
        this.zzh = zzgmx.zzh;
        this.zzs = "";
        this.zzt = 0;
        this.zzu = zzgmx.zza;
        this.zzv = 0L;
        this.zzw = null;
        this.zzx = false;
        this.zzax = null;
        this.zzay = -1;
    }
    
    private final zzgnq zza() {
        try {
            final zzgnq zzgnq = super.clone();
            if (this.zzk != null && this.zzk.length > 0) {
                zzgnq.zzk = new zzgnr[this.zzk.length];
                for (int i = 0; i < this.zzk.length; ++i) {
                    if (this.zzk[i] != null) {
                        zzgnq.zzk[i] = (zzgnr)this.zzk[i].clone();
                    }
                }
            }
            if (this.zzm != null) {
                zzgnq.zzm = this.zzm;
            }
            if (this.zzp != null) {
                zzgnq.zzp = (zzgno)this.zzp.clone();
            }
            if (this.zzr != null) {
                zzgnq.zzr = (zzgnp)this.zzr.clone();
            }
            if (this.zzu != null && this.zzu.length > 0) {
                zzgnq.zzu = this.zzu.clone();
            }
            if (this.zzw != null) {
                zzgnq.zzw = (zzgns)this.zzw.clone();
            }
            return zzgnq;
        }
        catch (CloneNotSupportedException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza != 0L) {
            computeSerializedSize = n + zzgmm.zzf(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzc != null) {
            n2 = computeSerializedSize;
            if (!this.zzc.equals("")) {
                n2 = computeSerializedSize + zzgmm.zzb(2, this.zzc);
            }
        }
        final zzgnr[] zzk = this.zzk;
        final int n3 = 0;
        int n4 = n2;
        if (zzk != null) {
            n4 = n2;
            if (this.zzk.length > 0) {
                int n5;
                for (int i = 0; i < this.zzk.length; ++i, n2 = n5) {
                    final zzgnr zzgnr = this.zzk[i];
                    n5 = n2;
                    if (zzgnr != null) {
                        n5 = n2 + zzgmm.zzb(3, zzgnr);
                    }
                }
                n4 = n2;
            }
        }
        int n6 = n4;
        if (!Arrays.equals(this.zzl, zzgmx.zzh)) {
            n6 = n4 + zzgmm.zzb(4, this.zzl);
        }
        int n7 = n6;
        if (!Arrays.equals(this.zzf, zzgmx.zzh)) {
            n7 = n6 + zzgmm.zzb(6, this.zzf);
        }
        int n8 = n7;
        if (this.zzp != null) {
            n8 = n7 + zzgmm.zzb(7, this.zzp);
        }
        int n9 = n8;
        if (this.zzn != null) {
            n9 = n8;
            if (!this.zzn.equals("")) {
                n9 = n8 + zzgmm.zzb(8, this.zzn);
            }
        }
        int n10 = n9;
        if (this.zzm != null) {
            n10 = n9 + zzgic.zzc(9, this.zzm);
        }
        int n11 = n10;
        if (this.zzj) {
            n11 = n10 + (zzgmm.zzb(10) + 1);
        }
        int n12 = n11;
        if (this.zzd != 0) {
            n12 = n11 + zzgmm.zzb(11, this.zzd);
        }
        int n13 = n12;
        if (this.zze != 0) {
            n13 = n12 + zzgmm.zzb(12, this.zze);
        }
        int n14 = n13;
        if (this.zzo != null) {
            n14 = n13;
            if (!this.zzo.equals("")) {
                n14 = n13 + zzgmm.zzb(13, this.zzo);
            }
        }
        int n15 = n14;
        if (this.zzq != null) {
            n15 = n14;
            if (!this.zzq.equals("")) {
                n15 = n14 + zzgmm.zzb(14, this.zzq);
            }
        }
        int n16 = n15;
        if (this.zzg != 180000L) {
            n16 = n15 + zzgmm.zzg(15, this.zzg);
        }
        int n17 = n16;
        if (this.zzr != null) {
            n17 = n16 + zzgmm.zzb(16, this.zzr);
        }
        int n18 = n17;
        if (this.zzb != 0L) {
            n18 = n17 + zzgmm.zzf(17, this.zzb);
        }
        int n19 = n18;
        if (!Arrays.equals(this.zzh, zzgmx.zzh)) {
            n19 = n18 + zzgmm.zzb(18, this.zzh);
        }
        int n20 = n19;
        if (this.zzt != 0) {
            n20 = n19 + zzgmm.zzb(19, this.zzt);
        }
        int n21 = n20;
        if (this.zzu != null) {
            n21 = n20;
            if (this.zzu.length > 0) {
                int n22 = 0;
                for (int j = n3; j < this.zzu.length; ++j) {
                    n22 += zzgmm.zza(this.zzu[j]);
                }
                n21 = n20 + n22 + 2 * this.zzu.length;
            }
        }
        int n23 = n21;
        if (this.zzi != 0L) {
            n23 = n21 + zzgmm.zzf(21, this.zzi);
        }
        int n24 = n23;
        if (this.zzv != 0L) {
            n24 = n23 + zzgmm.zzf(22, this.zzv);
        }
        int n25 = n24;
        if (this.zzw != null) {
            n25 = n24 + zzgmm.zzb(23, this.zzw);
        }
        int n26 = n25;
        if (this.zzs != null) {
            n26 = n25;
            if (!this.zzs.equals("")) {
                n26 = n25 + zzgmm.zzb(24, this.zzs);
            }
        }
        int n27 = n26;
        if (this.zzx) {
            n27 = n26 + (zzgmm.zzb(25) + 1);
        }
        return n27;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgnq)) {
            return false;
        }
        final zzgnq zzgnq = (zzgnq)o;
        if (this.zza != zzgnq.zza) {
            return false;
        }
        if (this.zzb != zzgnq.zzb) {
            return false;
        }
        if (this.zzi != zzgnq.zzi) {
            return false;
        }
        if (this.zzc == null) {
            if (zzgnq.zzc != null) {
                return false;
            }
        }
        else if (!this.zzc.equals(zzgnq.zzc)) {
            return false;
        }
        if (this.zzd != zzgnq.zzd) {
            return false;
        }
        if (this.zze != zzgnq.zze) {
            return false;
        }
        if (this.zzj != zzgnq.zzj) {
            return false;
        }
        if (!zzgms.zza(this.zzk, zzgnq.zzk)) {
            return false;
        }
        if (!Arrays.equals(this.zzl, zzgnq.zzl)) {
            return false;
        }
        if (this.zzm == null) {
            if (zzgnq.zzm != null) {
                return false;
            }
        }
        else if (!this.zzm.equals(zzgnq.zzm)) {
            return false;
        }
        if (!Arrays.equals(this.zzf, zzgnq.zzf)) {
            return false;
        }
        if (this.zzn == null) {
            if (zzgnq.zzn != null) {
                return false;
            }
        }
        else if (!this.zzn.equals(zzgnq.zzn)) {
            return false;
        }
        if (this.zzo == null) {
            if (zzgnq.zzo != null) {
                return false;
            }
        }
        else if (!this.zzo.equals(zzgnq.zzo)) {
            return false;
        }
        if (this.zzp == null) {
            if (zzgnq.zzp != null) {
                return false;
            }
        }
        else if (!this.zzp.equals(zzgnq.zzp)) {
            return false;
        }
        if (this.zzq == null) {
            if (zzgnq.zzq != null) {
                return false;
            }
        }
        else if (!this.zzq.equals(zzgnq.zzq)) {
            return false;
        }
        if (this.zzg != zzgnq.zzg) {
            return false;
        }
        if (this.zzr == null) {
            if (zzgnq.zzr != null) {
                return false;
            }
        }
        else if (!this.zzr.equals(zzgnq.zzr)) {
            return false;
        }
        if (!Arrays.equals(this.zzh, zzgnq.zzh)) {
            return false;
        }
        if (this.zzs == null) {
            if (zzgnq.zzs != null) {
                return false;
            }
        }
        else if (!this.zzs.equals(zzgnq.zzs)) {
            return false;
        }
        if (this.zzt != zzgnq.zzt) {
            return false;
        }
        if (!zzgms.zza(this.zzu, zzgnq.zzu)) {
            return false;
        }
        if (this.zzv != zzgnq.zzv) {
            return false;
        }
        if (this.zzw == null) {
            if (zzgnq.zzw != null) {
                return false;
            }
        }
        else if (!this.zzw.equals(zzgnq.zzw)) {
            return false;
        }
        if (this.zzx != zzgnq.zzx) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgnq.zzax);
        }
        return zzgnq.zzax == null || zzgnq.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final int n = (int)(this.zza ^ this.zza >>> 32);
        final int n2 = (int)(this.zzb ^ this.zzb >>> 32);
        final int n3 = (int)(this.zzi ^ this.zzi >>> 32);
        final String zzc = this.zzc;
        int hashCode2 = 0;
        int hashCode3;
        if (zzc == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = this.zzc.hashCode();
        }
        final int zzd = this.zzd;
        final int zze = this.zze;
        final boolean zzj = this.zzj;
        int n4 = 1237;
        int n5;
        if (zzj) {
            n5 = 1231;
        }
        else {
            n5 = 1237;
        }
        final int zza = zzgms.zza(this.zzk);
        final int hashCode4 = Arrays.hashCode(this.zzl);
        final zzgnm.zza zzm = this.zzm;
        int hashCode5;
        if (zzm == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = zzm.hashCode();
        }
        final int hashCode6 = Arrays.hashCode(this.zzf);
        int hashCode7;
        if (this.zzn == null) {
            hashCode7 = 0;
        }
        else {
            hashCode7 = this.zzn.hashCode();
        }
        int hashCode8;
        if (this.zzo == null) {
            hashCode8 = 0;
        }
        else {
            hashCode8 = this.zzo.hashCode();
        }
        final zzgno zzp = this.zzp;
        int hashCode9;
        if (zzp == null) {
            hashCode9 = 0;
        }
        else {
            hashCode9 = zzp.hashCode();
        }
        int hashCode10;
        if (this.zzq == null) {
            hashCode10 = 0;
        }
        else {
            hashCode10 = this.zzq.hashCode();
        }
        final int n6 = (int)(this.zzg ^ this.zzg >>> 32);
        final zzgnp zzr = this.zzr;
        int hashCode11;
        if (zzr == null) {
            hashCode11 = 0;
        }
        else {
            hashCode11 = zzr.hashCode();
        }
        final int hashCode12 = Arrays.hashCode(this.zzh);
        int hashCode13;
        if (this.zzs == null) {
            hashCode13 = 0;
        }
        else {
            hashCode13 = this.zzs.hashCode();
        }
        final int zzt = this.zzt;
        final int zza2 = zzgms.zza(this.zzu);
        final int n7 = (int)(this.zzv ^ this.zzv >>> 32);
        final zzgns zzw = this.zzw;
        int hashCode14;
        if (zzw == null) {
            hashCode14 = 0;
        }
        else {
            hashCode14 = zzw.hashCode();
        }
        if (this.zzx) {
            n4 = 1231;
        }
        if (this.zzax != null) {
            if (!this.zzax.zzb()) {
                hashCode2 = this.zzax.hashCode();
            }
        }
        return (((((((((((((((((((((((((527 + hashCode) * 31 + n) * 31 + n2) * 31 + n3) * 31 + hashCode3) * 31 + zzd) * 31 + zze) * 31 + n5) * 31 + zza) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode7) * 31 + hashCode8) * 31 + hashCode9) * 31 + hashCode10) * 31 + n6) * 31 + hashCode11) * 31 + hashCode12) * 31 + hashCode13) * 31 + zzt) * 31 + zza2) * 31 + n7) * 31 + hashCode14) * 31 + n4) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza != 0L) {
            zzgmm.zzb(1, this.zza);
        }
        if (this.zzc != null && !this.zzc.equals("")) {
            zzgmm.zza(2, this.zzc);
        }
        final zzgnr[] zzk = this.zzk;
        final int n = 0;
        if (zzk != null && this.zzk.length > 0) {
            for (int i = 0; i < this.zzk.length; ++i) {
                final zzgnr zzgnr = this.zzk[i];
                if (zzgnr != null) {
                    zzgmm.zza(3, zzgnr);
                }
            }
        }
        if (!Arrays.equals(this.zzl, zzgmx.zzh)) {
            zzgmm.zza(4, this.zzl);
        }
        if (!Arrays.equals(this.zzf, zzgmx.zzh)) {
            zzgmm.zza(6, this.zzf);
        }
        if (this.zzp != null) {
            zzgmm.zza(7, this.zzp);
        }
        if (this.zzn != null && !this.zzn.equals("")) {
            zzgmm.zza(8, this.zzn);
        }
        if (this.zzm != null) {
            zzgmm.zza(9, this.zzm);
        }
        if (this.zzj) {
            zzgmm.zza(10, this.zzj);
        }
        if (this.zzd != 0) {
            zzgmm.zza(11, this.zzd);
        }
        if (this.zze != 0) {
            zzgmm.zza(12, this.zze);
        }
        if (this.zzo != null && !this.zzo.equals("")) {
            zzgmm.zza(13, this.zzo);
        }
        if (this.zzq != null && !this.zzq.equals("")) {
            zzgmm.zza(14, this.zzq);
        }
        if (this.zzg != 180000L) {
            zzgmm.zzd(15, this.zzg);
        }
        if (this.zzr != null) {
            zzgmm.zza(16, this.zzr);
        }
        if (this.zzb != 0L) {
            zzgmm.zzb(17, this.zzb);
        }
        if (!Arrays.equals(this.zzh, zzgmx.zzh)) {
            zzgmm.zza(18, this.zzh);
        }
        if (this.zzt != 0) {
            zzgmm.zza(19, this.zzt);
        }
        if (this.zzu != null && this.zzu.length > 0) {
            for (int j = n; j < this.zzu.length; ++j) {
                zzgmm.zza(20, this.zzu[j]);
            }
        }
        if (this.zzi != 0L) {
            zzgmm.zzb(21, this.zzi);
        }
        if (this.zzv != 0L) {
            zzgmm.zzb(22, this.zzv);
        }
        if (this.zzw != null) {
            zzgmm.zza(23, this.zzw);
        }
        if (this.zzs != null && !this.zzs.equals("")) {
            zzgmm.zza(24, this.zzs);
        }
        if (this.zzx) {
            zzgmm.zza(25, this.zzx);
        }
        super.writeTo(zzgmm);
    }
}
