package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.List;
import android.os.Parcelable;
import android.os.Parcel;
import java.util.ArrayList;
import android.util.SparseArray;
import java.util.HashMap;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.server.response.FastJsonResponse;

public final class zzbji extends zzbid implements FieldConverter<String, Integer>
{
    public static final Parcelable.Creator<zzbji> CREATOR;
    private final int zza;
    private final HashMap<String, Integer> zzb;
    private final SparseArray<String> zzc;
    private final ArrayList<zzbjj> zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zzbjk();
    }
    
    public zzbji() {
        this.zza = 1;
        this.zzb = new HashMap<String, Integer>();
        this.zzc = (SparseArray<String>)new SparseArray();
        this.zzd = null;
    }
    
    zzbji(int i, final ArrayList<zzbjj> list) {
        this.zza = i;
        this.zzb = new HashMap<String, Integer>();
        this.zzc = (SparseArray<String>)new SparseArray();
        this.zzd = null;
        final ArrayList<zzbjj> list2 = list;
        final int size = list2.size();
        i = 0;
        while (i < size) {
            final zzbjj value = list2.get(i);
            ++i;
            final zzbjj zzbjj = value;
            this.zza(zzbjj.zza, zzbjj.zzb);
        }
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        final ArrayList<Parcelable> list = new ArrayList<Parcelable>();
        for (final String s : this.zzb.keySet()) {
            list.add((Parcelable)new zzbjj(s, this.zzb.get(s)));
        }
        zzbig.zzc(parcel, 2, list, false);
        zzbig.zza(parcel, zza);
    }
    
    public final zzbji zza(final String s, final int n) {
        this.zzb.put(s, n);
        this.zzc.put(n, (Object)s);
        return this;
    }
}
