package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.Parcelable;
import android.os.IBinder;

public final class zzegr extends zzey implements zzegq
{
    zzegr(final IBinder binder) {
        super(binder, "com.google.android.gms.signin.internal.ISignInService");
    }
    
    @Override
    public final void zza(final zzegt zzegt, final zzego zzego) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzegt);
        zzfa.zza(a_, (IInterface)zzego);
        this.zzb(12, a_);
    }
}
