package com.google.android.gms.internal;

interface zzgky<T>
{
    int zza(final T p0);
    
    void zza(final T p0, final zzgmk p1);
    
    boolean zza(final T p0, final T p1);
    
    int zzb(final T p0);
    
    void zzb(final T p0, final T p1);
}
