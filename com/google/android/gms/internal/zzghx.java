package com.google.android.gms.internal;

public abstract class zzghx
{
    private static volatile boolean zzf;
    int zzb;
    private int zzd;
    private boolean zze;
    
    static {
        zzghx.zzf = false;
        zzghx.zzf = true;
    }
    
    private zzghx() {
        this.zzb = 100;
        this.zzd = Integer.MAX_VALUE;
        this.zze = false;
    }
    
    static zzghx zza(final byte[] array, final int n, final int n2, final boolean b) {
        final zzghz zzghz = new zzghz(array, n, n2, b, null);
        try {
            zzghz.zzd(n2);
            return zzghz;
        }
        catch (zzgjg zzgjg) {
            throw new IllegalArgumentException(zzgjg);
        }
    }
    
    public abstract int zzd(final int p0) throws zzgjg;
    
    public abstract int zzu();
}
