package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzp;

public final class zzacl extends zzp<zzacf>
{
    public zzacl() {
        super("com.google.android.gms.ads.reward.RewardedVideoAdCreatorImpl");
    }
}
