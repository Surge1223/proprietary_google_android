package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public abstract class zzoh extends zzez implements zzog
{
    public static zzog zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegate");
        if (queryLocalInterface instanceof zzog) {
            return (zzog)queryLocalInterface;
        }
        return new zzoi(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 2: {
                this.zza();
                break;
            }
            case 1: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}
