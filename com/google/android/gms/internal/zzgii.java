package com.google.android.gms.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class zzgii
{
    static final zzgii zza;
    private static volatile boolean zzb;
    private static final Class<?> zzc;
    private final Map<Object, Object<?, ?>> zze;
    
    static {
        zzgii.zzb = false;
        zzc = zzd();
        zza = new zzgii(true);
    }
    
    zzgii() {
        this.zze = new HashMap<Object, Object<?, ?>>();
    }
    
    private zzgii(final boolean b) {
        this.zze = Collections.emptyMap();
    }
    
    public static zzgii zza() {
        return zzgih.zza();
    }
    
    private static Class<?> zzd() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        }
        catch (ClassNotFoundException ex) {
            return null;
        }
    }
}
