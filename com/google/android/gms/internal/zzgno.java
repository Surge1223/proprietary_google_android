package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgno extends zzgmo<zzgno> implements Cloneable
{
    private String[] zza;
    private String[] zzb;
    private int[] zzc;
    private long[] zzd;
    private long[] zze;
    
    public zzgno() {
        this.zza = zzgmx.zzf;
        this.zzb = zzgmx.zzf;
        this.zzc = zzgmx.zza;
        this.zzd = zzgmx.zzb;
        this.zze = zzgmx.zzb;
        this.zzax = null;
        this.zzay = -1;
    }
    
    private final zzgno zza() {
        try {
            final zzgno zzgno = super.clone();
            if (this.zza != null && this.zza.length > 0) {
                zzgno.zza = this.zza.clone();
            }
            if (this.zzb != null && this.zzb.length > 0) {
                zzgno.zzb = this.zzb.clone();
            }
            if (this.zzc != null && this.zzc.length > 0) {
                zzgno.zzc = this.zzc.clone();
            }
            if (this.zzd != null && this.zzd.length > 0) {
                zzgno.zzd = this.zzd.clone();
            }
            if (this.zze != null && this.zze.length > 0) {
                zzgno.zze = this.zze.clone();
            }
            return zzgno;
        }
        catch (CloneNotSupportedException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    @Override
    protected final int computeSerializedSize() {
        final int computeSerializedSize = super.computeSerializedSize();
        final String[] zza = this.zza;
        final int n = 0;
        int n2 = computeSerializedSize;
        if (zza != null) {
            n2 = computeSerializedSize;
            if (this.zza.length > 0) {
                int i = 0;
                int n4;
                int n3 = n4 = i;
                while (i < this.zza.length) {
                    final String s = this.zza[i];
                    int n5 = n4;
                    int n6 = n3;
                    if (s != null) {
                        n6 = n3 + 1;
                        n5 = n4 + zzgmm.zza(s);
                    }
                    ++i;
                    n4 = n5;
                    n3 = n6;
                }
                n2 = computeSerializedSize + n4 + n3 * 1;
            }
        }
        int n7 = n2;
        if (this.zzb != null) {
            n7 = n2;
            if (this.zzb.length > 0) {
                int j = 0;
                int n9;
                int n8 = n9 = j;
                while (j < this.zzb.length) {
                    final String s2 = this.zzb[j];
                    int n10 = n9;
                    int n11 = n8;
                    if (s2 != null) {
                        n11 = n8 + 1;
                        n10 = n9 + zzgmm.zza(s2);
                    }
                    ++j;
                    n9 = n10;
                    n8 = n11;
                }
                n7 = n2 + n9 + n8 * 1;
            }
        }
        int n12 = n7;
        if (this.zzc != null) {
            n12 = n7;
            if (this.zzc.length > 0) {
                int n13;
                for (int k = n13 = 0; k < this.zzc.length; ++k) {
                    n13 += zzgmm.zza(this.zzc[k]);
                }
                n12 = n7 + n13 + this.zzc.length * 1;
            }
        }
        int n14 = n12;
        if (this.zzd != null) {
            n14 = n12;
            if (this.zzd.length > 0) {
                int n15;
                for (int l = n15 = 0; l < this.zzd.length; ++l) {
                    n15 += zzgmm.zzb(this.zzd[l]);
                }
                n14 = n12 + n15 + this.zzd.length * 1;
            }
        }
        int n16 = n14;
        if (this.zze != null) {
            n16 = n14;
            if (this.zze.length > 0) {
                int n17 = 0;
                for (int n18 = n; n18 < this.zze.length; ++n18) {
                    n17 += zzgmm.zzb(this.zze[n18]);
                }
                n16 = n14 + n17 + 1 * this.zze.length;
            }
        }
        return n16;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgno)) {
            return false;
        }
        final zzgno zzgno = (zzgno)o;
        if (!zzgms.zza(this.zza, zzgno.zza)) {
            return false;
        }
        if (!zzgms.zza(this.zzb, zzgno.zzb)) {
            return false;
        }
        if (!zzgms.zza(this.zzc, zzgno.zzc)) {
            return false;
        }
        if (!zzgms.zza(this.zzd, zzgno.zzd)) {
            return false;
        }
        if (!zzgms.zza(this.zze, zzgno.zze)) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgno.zzax);
        }
        return zzgno.zzax == null || zzgno.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final int zza = zzgms.zza(this.zza);
        final int zza2 = zzgms.zza(this.zzb);
        final int zza3 = zzgms.zza(this.zzc);
        final int zza4 = zzgms.zza(this.zzd);
        final int zza5 = zzgms.zza(this.zze);
        int hashCode2;
        if (this.zzax != null && !this.zzax.zzb()) {
            hashCode2 = this.zzax.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return ((((((527 + hashCode) * 31 + zza) * 31 + zza2) * 31 + zza3) * 31 + zza4) * 31 + zza5) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        final String[] zza = this.zza;
        final int n = 0;
        if (zza != null && this.zza.length > 0) {
            for (int i = 0; i < this.zza.length; ++i) {
                final String s = this.zza[i];
                if (s != null) {
                    zzgmm.zza(1, s);
                }
            }
        }
        if (this.zzb != null && this.zzb.length > 0) {
            for (int j = 0; j < this.zzb.length; ++j) {
                final String s2 = this.zzb[j];
                if (s2 != null) {
                    zzgmm.zza(2, s2);
                }
            }
        }
        if (this.zzc != null && this.zzc.length > 0) {
            for (int k = 0; k < this.zzc.length; ++k) {
                zzgmm.zza(3, this.zzc[k]);
            }
        }
        if (this.zzd != null && this.zzd.length > 0) {
            for (int l = 0; l < this.zzd.length; ++l) {
                zzgmm.zzb(4, this.zzd[l]);
            }
        }
        if (this.zze != null && this.zze.length > 0) {
            for (int n2 = n; n2 < this.zze.length; ++n2) {
                zzgmm.zzb(5, this.zze[n2]);
            }
        }
        super.writeTo(zzgmm);
    }
}
