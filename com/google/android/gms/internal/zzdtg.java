package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.phenotype.ExperimentTokens;
import com.google.android.gms.phenotype.DogfoodsToken;
import com.google.android.gms.phenotype.Flag;
import com.google.android.gms.phenotype.Configurations;
import com.google.android.gms.phenotype.FlagOverrides;
import com.google.android.gms.common.api.Status;
import android.os.Parcel;
import android.os.IInterface;

public abstract class zzdtg extends zzez implements zzdtf
{
    public zzdtg() {
        this.attachInterface((IInterface)this, "com.google.android.gms.phenotype.internal.IPhenotypeCallbacks");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 14: {
                this.zzd(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 13: {
                this.zza(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, FlagOverrides.CREATOR));
                break;
            }
            case 12: {
                this.zzh(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 11: {
                this.zza(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 10: {
                this.zzb(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, Configurations.CREATOR));
                break;
            }
            case 9: {
                this.zza(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, Flag.CREATOR));
                break;
            }
            case 8: {
                this.zzg(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 7: {
                this.zza(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, DogfoodsToken.CREATOR));
                break;
            }
            case 6: {
                this.zza(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, ExperimentTokens.CREATOR));
                break;
            }
            case 5: {
                this.zzf(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 4: {
                this.zza(zzfa.zza(parcel, Status.CREATOR), zzfa.zza(parcel, Configurations.CREATOR));
                break;
            }
            case 3: {
                this.zze(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 2: {
                this.zzc(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
            case 1: {
                this.zzb(zzfa.zza(parcel, Status.CREATOR));
                break;
            }
        }
        return true;
    }
}
