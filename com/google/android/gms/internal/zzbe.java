package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbe extends zzgmo<zzbe>
{
    private Long zza;
    private Integer zzb;
    private Boolean zzc;
    private int[] zzd;
    private Long zze;
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza != null) {
            computeSerializedSize = n + zzgmm.zzf(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzb != null) {
            n2 = computeSerializedSize + zzgmm.zzb(2, this.zzb);
        }
        int n3 = n2;
        if (this.zzc != null) {
            this.zzc;
            n3 = n2 + (zzgmm.zzb(3) + 1);
        }
        int n4 = n3;
        if (this.zzd != null) {
            n4 = n3;
            if (this.zzd.length > 0) {
                int i = 0;
                int n5 = 0;
                while (i < this.zzd.length) {
                    n5 += zzgmm.zza(this.zzd[i]);
                    ++i;
                }
                n4 = n3 + n5 + 1 * this.zzd.length;
            }
        }
        int n6 = n4;
        if (this.zze != null) {
            n6 = n4 + zzgmm.zze(5, this.zze);
        }
        return n6;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza != null) {
            zzgmm.zzb(1, this.zza);
        }
        if (this.zzb != null) {
            zzgmm.zza(2, this.zzb);
        }
        if (this.zzc != null) {
            zzgmm.zza(3, this.zzc);
        }
        if (this.zzd != null && this.zzd.length > 0) {
            for (int i = 0; i < this.zzd.length; ++i) {
                zzgmm.zza(4, this.zzd[i]);
            }
        }
        if (this.zze != null) {
            zzgmm.zza(5, this.zze);
        }
        super.writeTo(zzgmm);
    }
}
