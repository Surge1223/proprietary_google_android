package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public abstract class zzgiw<MessageType extends zzgiw<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzghc<MessageType, BuilderType>
{
    private static Map<Object, zzgiw<?, ?>> zzd;
    protected zzglr zzb;
    protected int zzc;
    
    static {
        zzgiw.zzd = new ConcurrentHashMap<Object, zzgiw<?, ?>>();
    }
    
    public zzgiw() {
        this.zzb = zzglr.zza();
        this.zzc = -1;
    }
    
    static <T extends zzgiw<?, ?>> T zza(final Class<T> clazz) {
        zzgiw<?, ?> zzgiw;
        if ((zzgiw = com.google.android.gms.internal.zzgiw.zzd.get(clazz)) == null) {
            try {
                Class.forName(clazz.getName(), true, clazz.getClassLoader());
                zzgiw = com.google.android.gms.internal.zzgiw.zzd.get(clazz);
            }
            catch (ClassNotFoundException ex) {
                throw new IllegalStateException("Class initialization cannot fail.", ex);
            }
        }
        if (zzgiw == null) {
            final String value = String.valueOf(clazz.getName());
            String concat;
            if (value.length() != 0) {
                concat = "Unable to get default instance for: ".concat(value);
            }
            else {
                concat = new String("Unable to get default instance for: ");
            }
            throw new IllegalStateException(concat);
        }
        return (T)zzgiw;
    }
    
    protected static Object zza(final zzgkg zzgkg, final String s, final Object[] array) {
        return new zzgku(zzgkg, s, array);
    }
    
    static Object zza(final Method method, final Object o, final Object... array) {
        try {
            return method.invoke(o, array);
        }
        catch (InvocationTargetException ex) {
            final Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            if (cause instanceof Error) {
                throw (Error)cause;
            }
            throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
        }
        catch (IllegalAccessException ex2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", ex2);
        }
    }
    
    protected static <T extends zzgiw<?, ?>> void zza(final Class<T> clazz, final T t) {
        zzgiw.zzd.put(clazz, t);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (((zzgiw)this.zza(zzg.zzg, null, null)).getClass().isInstance(o) && zzgks.zza().zza(this).zza(this, (zzgiw)o));
    }
    
    @Override
    public int hashCode() {
        if (this.zza != 0) {
            return this.zza;
        }
        return this.zza = zzgks.zza().zza(this).zza(this);
    }
    
    @Override
    public String toString() {
        return zzgkj.zza(this, super.toString());
    }
    
    @Override
    public int zza() {
        if (this.zzc == -1) {
            this.zzc = zzgks.zza().zza(this).zzb(this);
        }
        return this.zzc;
    }
    
    protected abstract Object zza(final int p0, final Object p1, final Object p2);
    
    @Override
    public void zza(final zzgic zzgic) throws IOException {
        zzgks.zza().zza(this.getClass()).zza((zzgiw)this, (zzgmk)zzgie.zza(zzgic));
    }
    
    protected abstract Object zzb() throws Exception;
    
    protected void zzl() {
        this.zza(zzg.zzd, null, null);
        this.zzb.zzc();
    }
    
    public static class zza<MessageType extends zzgiw<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzghd<MessageType, BuilderType>
    {
        protected MessageType zza;
        protected boolean zzb;
        private final MessageType zzc;
        
        protected zza(final MessageType zzc) {
            this.zzc = zzc;
            this.zza = (MessageType)zzc.zza(zzg.zze, null, null);
            this.zzb = false;
        }
        
        private static void zza(final MessageType messageType, final MessageType messageType2) {
            zzgks.zza().zza(messageType).zzb(messageType, messageType2);
        }
        
        public final BuilderType zza(final MessageType messageType) {
            this.zzb();
            zza(this.zza, messageType);
            return (BuilderType)this;
        }
        
        protected void zzb() {
            if (this.zzb) {
                final zzgiw<MessageType, BuilderType> zza = (MessageType)this.zza.zza(zzg.zze, null, null);
                zza((MessageType)zza, this.zza);
                this.zza = (MessageType)zza;
                this.zzb = false;
            }
        }
        
        public MessageType zzc() {
            if (this.zzb) {
                return this.zza;
            }
            this.zza.zzl();
            this.zzb = true;
            return this.zza;
        }
    }
    
    public static final class zzb<T extends zzgiw<T, ?>> extends zzghe<T>
    {
        private T zza;
        
        public zzb(final T zza) {
            this.zza = zza;
        }
    }
    
    public abstract static class zzd<MessageType extends zzd<MessageType, BuilderType>, BuilderType> extends zzgiw<MessageType, BuilderType> implements zzgki
    {
        protected zzgin<zze> zzd;
        
        public zzd() {
            this.zzd = zzgin.zza();
        }
    }
    
    static final class zze implements zzgip<zze>
    {
        final int zzb;
        final zzgme zzc;
        
        @Override
        public final int zza() {
            return this.zzb;
        }
        
        @Override
        public final zzgkh zza(final zzgkh zzgkh, final zzgkg zzgkg) {
            return ((zza)zzgkh).zza((zzgiw)zzgkg);
        }
        
        @Override
        public final zzgkn zza(final zzgkn zzgkn, final zzgkn zzgkn2) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public final zzgme zzb() {
            return this.zzc;
        }
        
        @Override
        public final zzgmj zzc() {
            return this.zzc.zza();
        }
        
        @Override
        public final boolean zzd() {
            return false;
        }
        
        @Override
        public final boolean zze() {
            return false;
        }
    }
    
    public enum zzg
    {
        public static final int zza;
        public static final int zzb;
        public static final int zzc;
        public static final int zzd;
        public static final int zze;
        public static final int zzf;
        public static final int zzg;
        public static final int zzh;
        public static final int zzi;
        public static final int zzj;
        public static final int zzk;
        public static final int zzl;
        private static final /* synthetic */ int[] zzm;
        
        static {
            zza = 1;
            zzb = 2;
            zzc = 3;
            zzd = 4;
            zze = 5;
            zzf = 6;
            zzg = 7;
            zzh = 8;
            zzm = new int[] { zzgiw.zzg.zza, zzgiw.zzg.zzb, zzgiw.zzg.zzc, zzgiw.zzg.zzd, zzgiw.zzg.zze, zzgiw.zzg.zzf, zzgiw.zzg.zzg, zzgiw.zzg.zzh };
            zzi = 1;
            zzj = 2;
            zzn = new int[] { zzgiw.zzg.zzi, zzgiw.zzg.zzj };
            zzk = 1;
            zzl = 2;
            zzo = new int[] { zzgiw.zzg.zzk, zzgiw.zzg.zzl };
        }
        
        public static int[] values$50KLMJ33DTMIUPRFDTJMOP9FE1P6UT3FC9QMCBQ7CLN6ASJ1EHIM8JB5EDPM2PR59HKN8P949LIN8Q3FCHA6UIBEEPNMMP9R0() {
            return zzgiw.zzg.zzm.clone();
        }
    }
}
