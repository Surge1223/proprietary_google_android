package com.google.android.gms.internal;

import android.os.RemoteException;
import java.util.List;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Parcelable;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;

public abstract class zztx extends zzez implements zztw
{
    public zztx() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 22: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 21: {
                final IObjectWrapper zzq = this.zzq();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzq);
                break;
            }
            case 20: {
                final IObjectWrapper zzp = this.zzp();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzp);
                break;
            }
            case 19: {
                final zznt zzo = this.zzo();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzo);
                break;
            }
            case 18: {
                final IObjectWrapper zzn = this.zzn();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzn);
                break;
            }
            case 17: {
                final zzbh zzm = this.zzm();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzm);
                break;
            }
            case 16: {
                this.zzc(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 15: {
                final Bundle zzl = this.zzl();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzl);
                break;
            }
            case 14: {
                final boolean zzk = this.zzk();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzk);
                break;
            }
            case 13: {
                final boolean zzj = this.zzj();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzj);
                break;
            }
            case 12: {
                this.zzb(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 11: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 10: {
                this.zzi();
                parcel2.writeNoException();
                break;
            }
            case 9: {
                final String zzh = this.zzh();
                parcel2.writeNoException();
                parcel2.writeString(zzh);
                break;
            }
            case 8: {
                final String zzg = this.zzg();
                parcel2.writeNoException();
                parcel2.writeString(zzg);
                break;
            }
            case 7: {
                final double zzf = this.zzf();
                parcel2.writeNoException();
                parcel2.writeDouble(zzf);
                break;
            }
            case 6: {
                final String zze = this.zze();
                parcel2.writeNoException();
                parcel2.writeString(zze);
                break;
            }
            case 5: {
                final zznx zzd = this.zzd();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzd);
                break;
            }
            case 4: {
                final String zzc = this.zzc();
                parcel2.writeNoException();
                parcel2.writeString(zzc);
                break;
            }
            case 3: {
                final List zzb = this.zzb();
                parcel2.writeNoException();
                parcel2.writeList(zzb);
                break;
            }
            case 2: {
                final String zza = this.zza();
                parcel2.writeNoException();
                parcel2.writeString(zza);
                break;
            }
        }
        return true;
    }
}
