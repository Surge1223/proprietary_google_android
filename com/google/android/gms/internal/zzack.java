package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardItem;

public final class zzack implements RewardItem
{
    private final zzabz zza;
    
    public zzack(final zzabz zza) {
        this.zza = zza;
    }
    
    @Override
    public final int getAmount() {
        if (this.zza == null) {
            return 0;
        }
        try {
            return this.zza.zzb();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not forward getAmount to RewardItem", (Throwable)ex);
            return 0;
        }
    }
    
    @Override
    public final String getType() {
        if (this.zza == null) {
            return null;
        }
        try {
            return this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not forward getType to RewardItem", (Throwable)ex);
            return null;
        }
    }
}
