package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzdth extends IInterface
{
    void zza(final zzdtf p0, final String p1, final int p2, final String[] p3, final byte[] p4) throws RemoteException;
    
    void zza(final zzdtf p0, final String p1, final String p2, final String p3) throws RemoteException;
    
    void zzb(final zzdtf p0, final String p1) throws RemoteException;
}
