package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzbi;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.IInterface;
import java.util.ArrayList;
import java.util.List;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzon extends zzey implements zzol
{
    zzon(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeAppInstallAd");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final List zzb() throws RemoteException {
        final Parcel zza = this.zza(4, this.a_());
        final ArrayList zzc = zzfa.zzc(zza);
        zza.recycle();
        return zzc;
    }
    
    @Override
    public final String zzc() throws RemoteException {
        final Parcel zza = this.zza(5, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final zznx zzd() throws RemoteException {
        final Parcel zza = this.zza(6, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zznx zznx;
        if (strongBinder == null) {
            zznx = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
            if (queryLocalInterface instanceof zznx) {
                zznx = (zznx)queryLocalInterface;
            }
            else {
                zznx = new zznz(strongBinder);
            }
        }
        zza.recycle();
        return zznx;
    }
    
    @Override
    public final String zze() throws RemoteException {
        final Parcel zza = this.zza(7, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final double zzf() throws RemoteException {
        final Parcel zza = this.zza(8, this.a_());
        final double double1 = zza.readDouble();
        zza.recycle();
        return double1;
    }
    
    @Override
    public final String zzg() throws RemoteException {
        final Parcel zza = this.zza(9, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final String zzh() throws RemoteException {
        final Parcel zza = this.zza(10, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final zzbh zzi() throws RemoteException {
        final Parcel zza = this.zza(13, this.a_());
        final zzbh zza2 = zzbi.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzj() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zznt zzr() throws RemoteException {
        final Parcel zza = this.zza(17, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zznt zznt;
        if (strongBinder == null) {
            zznt = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
            if (queryLocalInterface instanceof zznt) {
                zznt = (zznt)queryLocalInterface;
            }
            else {
                zznt = new zznv(strongBinder);
            }
        }
        zza.recycle();
        return zznt;
    }
}
