package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbd extends zzgmo<zzbd>
{
    public Long zza;
    public Long zzb;
    public Long zzc;
    private Long zzd;
    private Long zze;
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zzd != null) {
            computeSerializedSize = n + zzgmm.zzf(1, this.zzd);
        }
        int n2 = computeSerializedSize;
        if (this.zze != null) {
            n2 = computeSerializedSize + zzgmm.zzf(2, this.zze);
        }
        int n3 = n2;
        if (this.zza != null) {
            n3 = n2 + zzgmm.zzf(3, this.zza);
        }
        int n4 = n3;
        if (this.zzb != null) {
            n4 = n3 + zzgmm.zzf(4, this.zzb);
        }
        int n5 = n4;
        if (this.zzc != null) {
            n5 = n4 + zzgmm.zzf(5, this.zzc);
        }
        return n5;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zzd != null) {
            zzgmm.zzb(1, this.zzd);
        }
        if (this.zze != null) {
            zzgmm.zzb(2, this.zze);
        }
        if (this.zza != null) {
            zzgmm.zzb(3, this.zza);
        }
        if (this.zzb != null) {
            zzgmm.zzb(4, this.zzb);
        }
        if (this.zzc != null) {
            zzgmm.zzb(5, this.zzc);
        }
        super.writeTo(zzgmm);
    }
}
