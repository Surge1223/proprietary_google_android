package com.google.android.gms.internal;

final class zzght
{
    private final zzgic zza;
    private final byte[] zzb;
    
    private zzght(final int n) {
        this.zzb = new byte[n];
        this.zza = zzgic.zza(this.zzb);
    }
    
    public final zzgho zza() {
        this.zza.zzc();
        return new zzghv(this.zzb);
    }
    
    public final zzgic zzb() {
        return this.zza;
    }
}
