package com.google.android.gms.internal;

final class zzgkd
{
    private static final zzgkb zza;
    private static final zzgkb zzb;
    
    static {
        zza = zzc();
        zzb = new zzgkc();
    }
    
    static zzgkb zza() {
        return zzgkd.zza;
    }
    
    static zzgkb zzb() {
        return zzgkd.zzb;
    }
    
    private static zzgkb zzc() {
        try {
            return (zzgkb)Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (Exception ex) {
            return null;
        }
    }
}
