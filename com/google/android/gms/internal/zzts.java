package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.IBinder;

public final class zzts extends zzey implements zztq
{
    zzts(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(1, this.a_());
    }
    
    @Override
    public final void zza(final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeInt(n);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zza(final zzot zzot, final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzot);
        a_.writeString(s);
        this.zzb(10, a_);
    }
    
    @Override
    public final void zza(final String s, final String s2) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        a_.writeString(s2);
        this.zzb(9, a_);
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zzc() throws RemoteException {
        this.zzb(4, this.a_());
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(5, this.a_());
    }
    
    @Override
    public final void zze() throws RemoteException {
        this.zzb(6, this.a_());
    }
    
    @Override
    public final void zzf() throws RemoteException {
        this.zzb(8, this.a_());
    }
}
