package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzph extends zzez implements zzpg
{
    public zzph() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.formats.client.IOnCustomTemplateAdLoadedListener");
    }
    
    public static zzpg zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IOnCustomTemplateAdLoadedListener");
        if (queryLocalInterface instanceof zzpg) {
            return (zzpg)queryLocalInterface;
        }
        return new zzpi(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            final IBinder strongBinder = parcel.readStrongBinder();
            zzot zzot;
            if (strongBinder == null) {
                zzot = null;
            }
            else {
                final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
                if (queryLocalInterface instanceof zzot) {
                    zzot = (zzot)queryLocalInterface;
                }
                else {
                    zzot = new zzov(strongBinder);
                }
            }
            this.zza(zzot);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}
