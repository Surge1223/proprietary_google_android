package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzacc extends IInterface
{
    void zza() throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final zzach p0) throws RemoteException;
    
    void zza(final zzacn p0) throws RemoteException;
    
    void zza(final String p0) throws RemoteException;
    
    void zza(final boolean p0) throws RemoteException;
    
    void zzb(final IObjectWrapper p0) throws RemoteException;
    
    boolean zzb() throws RemoteException;
    
    void zzc() throws RemoteException;
    
    void zzc(final IObjectWrapper p0) throws RemoteException;
    
    void zzd() throws RemoteException;
    
    void zze() throws RemoteException;
    
    String zzf() throws RemoteException;
}
