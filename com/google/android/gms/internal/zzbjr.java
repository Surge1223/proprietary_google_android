package com.google.android.gms.internal;

import android.content.Context;

public final class zzbjr
{
    private static zzbjr zzb;
    private zzbjq zza;
    
    static {
        zzbjr.zzb = new zzbjr();
    }
    
    public zzbjr() {
        this.zza = null;
    }
    
    public static zzbjq zza(final Context context) {
        return zzbjr.zzb.zzb(context);
    }
    
    private final zzbjq zzb(Context applicationContext) {
        synchronized (this) {
            if (this.zza == null) {
                if (applicationContext.getApplicationContext() != null) {
                    applicationContext = applicationContext.getApplicationContext();
                }
                this.zza = new zzbjq(applicationContext);
            }
            return this.zza;
        }
    }
}
