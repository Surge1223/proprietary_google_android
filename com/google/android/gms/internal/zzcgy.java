package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.IBinder;
import java.io.File;
import com.google.android.gms.feedback.FileTeleporter;
import java.util.List;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.GoogleApiClient;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.internal.zzl;

public final class zzcgy extends zzl<zzchb>
{
    private final Context zzc;
    
    public zzcgy(final Context zzc, final Looper looper, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener, final ClientSettings clientSettings) {
        super(zzc, looper, 29, clientSettings, connectionCallbacks, onConnectionFailedListener);
        this.zzc = zzc;
    }
    
    public static void zza(final List<FileTeleporter> list, final File tempDir) {
        for (int i = 0; i < list.size(); ++i) {
            final FileTeleporter fileTeleporter = list.get(i);
            if (fileTeleporter != null) {
                fileTeleporter.setTempDir(tempDir);
            }
        }
    }
    
    @Override
    protected final String getServiceDescriptor() {
        return "com.google.android.gms.feedback.internal.IFeedbackService";
    }
    
    @Override
    protected final String getStartServiceAction() {
        return "com.google.android.gms.feedback.internal.IFeedbackService";
    }
}
