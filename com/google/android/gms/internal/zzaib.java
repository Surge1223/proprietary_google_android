package com.google.android.gms.internal;

import java.util.HashMap;
import java.net.HttpURLConnection;
import com.google.android.gms.common.util.zzc;
import java.io.Writer;
import java.io.StringWriter;
import java.util.Iterator;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.google.android.gms.common.util.zzh;
import java.util.List;
import java.util.Set;
import com.google.android.gms.common.util.Clock;
import android.util.JsonWriter;

final class zzaib implements zzaic
{
    private final String zza;
    
    zzaib(final String zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final JsonWriter jsonWriter) {
        zzahx.zza(this.zza, jsonWriter);
    }
}
