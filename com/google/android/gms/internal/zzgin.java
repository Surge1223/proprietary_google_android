package com.google.android.gms.internal;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;

final class zzgin<FieldDescriptorType extends zzgip<FieldDescriptorType>>
{
    private static final zzgin zzd;
    private final zzglb<FieldDescriptorType, Object> zza;
    private boolean zzb;
    private boolean zzc;
    
    static {
        zzd = new zzgin(true);
    }
    
    private zzgin() {
        this.zzc = false;
        this.zza = zzglb.zza(16);
    }
    
    private zzgin(final boolean b) {
        this.zzc = false;
        this.zza = zzglb.zza(0);
        this.zzc();
    }
    
    static int zza(final zzgme zzgme, int zze, final Object o) {
        final int n = zze = zzgic.zze(zze);
        if (zzgme == zzgme.zzj) {
            zzgja.zza((zzgkg)o);
            zze = n << 1;
        }
        return zze + zzb(zzgme, o);
    }
    
    public static <T extends zzgip<T>> zzgin<T> zza() {
        return (zzgin<T>)zzgin.zzd;
    }
    
    private static Object zza(final Object o) {
        if (o instanceof zzgkn) {
            return ((zzgkn)o).zzb();
        }
        if (o instanceof byte[]) {
            final byte[] array = (byte[])o;
            final byte[] array2 = new byte[array.length];
            System.arraycopy(array, 0, array2, 0, array.length);
            return array2;
        }
        return o;
    }
    
    static void zza(final zzgic zzgic, final zzgme zzgme, final int n, final Object o) throws IOException {
        if (zzgme == zzgme.zzj) {
            final zzgkg zzgkg = (zzgkg)o;
            zzgja.zza(zzgkg);
            zzgic.zze(n, zzgkg);
            return;
        }
        zzgic.zza(n, zzgme.zzb());
        zza(zzgic, zzgme, o);
    }
    
    private static void zza(final zzgic zzgic, final zzgme zzgme, final Object o) throws IOException {
        switch (zzgio.zzb[zzgme.ordinal()]) {
            case 18: {
                if (o instanceof zzgjb) {
                    zzgic.zza(((zzgjb)o).zza());
                    return;
                }
                zzgic.zza((int)o);
                break;
            }
            case 17: {
                zzgic.zzb((long)o);
            }
            case 16: {
                zzgic.zzc((int)o);
            }
            case 15: {
                zzgic.zzc((long)o);
            }
            case 14: {
                zzgic.zzd((int)o);
            }
            case 13: {
                zzgic.zzb((int)o);
            }
            case 12: {
                if (o instanceof zzgho) {
                    zzgic.zza((zzgho)o);
                    return;
                }
                final byte[] array = (byte[])o;
                zzgic.zzc(array, 0, array.length);
            }
            case 11: {
                if (o instanceof zzgho) {
                    zzgic.zza((zzgho)o);
                    return;
                }
                zzgic.zza((String)o);
            }
            case 10: {
                zzgic.zza((zzgkg)o);
            }
            case 9: {
                ((zzgkg)o).zza(zzgic);
            }
            case 8: {
                zzgic.zza((boolean)o);
            }
            case 7: {
                zzgic.zzd((int)o);
            }
            case 6: {
                zzgic.zzc((long)o);
            }
            case 5: {
                zzgic.zza((int)o);
            }
            case 4: {
                zzgic.zza((long)o);
            }
            case 3: {
                zzgic.zza((long)o);
            }
            case 2: {
                zzgic.zza((float)o);
            }
            case 1: {
                zzgic.zza((double)o);
            }
        }
    }
    
    private static void zza(final zzgme zzgme, final Object o) {
        zzgja.zza(o);
        final int n = zzgio.zza[zzgme.zza().ordinal()];
        boolean b = true;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        switch (n) {
            default: {
                b = b3;
                break;
            }
            case 9: {
                if (!(o instanceof zzgkg) && !(o instanceof zzgjj)) {
                    b = b3;
                    break;
                }
                break;
            }
            case 8: {
                b = (o instanceof Integer || o instanceof zzgjb || b4);
                break;
            }
            case 7: {
                b = (o instanceof zzgho || o instanceof byte[] || b2);
                break;
            }
            case 6: {
                b = (o instanceof String);
                break;
            }
            case 5: {
                b = (o instanceof Boolean);
                break;
            }
            case 4: {
                b = (o instanceof Double);
                break;
            }
            case 3: {
                b = (o instanceof Float);
                break;
            }
            case 2: {
                b = (o instanceof Long);
                break;
            }
            case 1: {
                b = (o instanceof Integer);
                break;
            }
        }
        if (b) {
            return;
        }
        throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
    }
    
    private static int zzb(final zzgip<?> zzgip, final Object o) {
        final zzgme zzb = zzgip.zzb();
        final int zza = zzgip.zza();
        if (!zzgip.zzd()) {
            return zza(zzb, zza, o);
        }
        final boolean zze = zzgip.zze();
        final int n = 0;
        int n2 = 0;
        if (zze) {
            final Iterator<Object> iterator = (Iterator<Object>)((List)o).iterator();
            while (iterator.hasNext()) {
                n2 += zzb(zzb, iterator.next());
            }
            return zzgic.zze(zza) + n2 + zzgic.zzm(n2);
        }
        final Iterator<Object> iterator2 = (Iterator<Object>)((List)o).iterator();
        int n3 = n;
        while (iterator2.hasNext()) {
            n3 += zza(zzb, zza, iterator2.next());
        }
        return n3;
    }
    
    private static int zzb(final zzgme zzgme, final Object o) {
        switch (zzgio.zzb[zzgme.ordinal()]) {
            default: {
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
            }
            case 18: {
                if (o instanceof zzgjb) {
                    return zzgic.zzk(((zzgjb)o).zza());
                }
                return zzgic.zzk((int)o);
            }
            case 17: {
                return zzgic.zzf((long)o);
            }
            case 16: {
                return zzgic.zzh((int)o);
            }
            case 15: {
                return zzgic.zzh((long)o);
            }
            case 14: {
                return zzgic.zzj((int)o);
            }
            case 13: {
                return zzgic.zzg((int)o);
            }
            case 12: {
                if (o instanceof zzgho) {
                    return zzgic.zzb((zzgho)o);
                }
                return zzgic.zzb((byte[])o);
            }
            case 11: {
                if (o instanceof zzgho) {
                    return zzgic.zzb((zzgho)o);
                }
                return zzgic.zzb((String)o);
            }
            case 10: {
                if (o instanceof zzgjj) {
                    return zzgic.zza((zzgjn)o);
                }
                return zzgic.zzb((zzgkg)o);
            }
            case 9: {
                return zzgic.zzc((zzgkg)o);
            }
            case 8: {
                return zzgic.zzb((boolean)o);
            }
            case 7: {
                return zzgic.zzi((int)o);
            }
            case 6: {
                return zzgic.zzg((long)o);
            }
            case 5: {
                return zzgic.zzf((int)o);
            }
            case 4: {
                return zzgic.zze((long)o);
            }
            case 3: {
                return zzgic.zzd((long)o);
            }
            case 2: {
                return zzgic.zzb((float)o);
            }
            case 1: {
                return zzgic.zzb((double)o);
            }
        }
    }
    
    private final void zzb(final Map.Entry<FieldDescriptorType, Object> entry) {
        final zzgip<FieldDescriptorType> zzgip = entry.getKey();
        zzgkg zzgkg;
        if ((zzgkg = entry.getValue()) instanceof zzgjj) {
            zzgkg = zzgjj.zza();
        }
        if (zzgip.zzd()) {
            Object zza;
            if ((zza = this.zza((FieldDescriptorType)zzgip)) == null) {
                zza = new ArrayList<Object>();
            }
            final Iterator<Object> iterator = ((List<Object>)zzgkg).iterator();
            while (iterator.hasNext()) {
                ((List<Object>)zza).add(zza(iterator.next()));
            }
            this.zza.zza((FieldDescriptorType)zzgip, zza);
            return;
        }
        if (zzgip.zzc() != zzgmj.zzi) {
            this.zza.zza((FieldDescriptorType)zzgip, zza(zzgkg));
            return;
        }
        final Object zza2 = this.zza((FieldDescriptorType)zzgip);
        if (zza2 == null) {
            this.zza.zza((FieldDescriptorType)zzgip, zza(zzgkg));
            return;
        }
        zzgkg zzgkg2;
        if (zza2 instanceof zzgkn) {
            zzgkg2 = zzgip.zza((zzgkn)zza2, (zzgkn)zzgkg);
        }
        else {
            zzgkg2 = zzgip.zza(((zzgkn)zza2).zzp(), zzgkg).zzf();
        }
        this.zza.zza((FieldDescriptorType)zzgip, zzgkg2);
    }
    
    private static int zzc(final Map.Entry<FieldDescriptorType, Object> entry) {
        final zzgip<FieldDescriptorType> zzgip = entry.getKey();
        final zzgjj value = entry.getValue();
        if (zzgip.zzc() != zzgmj.zzi || zzgip.zzd() || zzgip.zze()) {
            return zzb(zzgip, value);
        }
        if (value instanceof zzgjj) {
            return zzgic.zzb(entry.getKey().zza(), value);
        }
        return zzgic.zzd(entry.getKey().zza(), (zzgkg)value);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return this == o || (o instanceof zzgin && this.zza.equals(((zzgin)o).zza));
    }
    
    @Override
    public final int hashCode() {
        return this.zza.hashCode();
    }
    
    public final Object zza(final FieldDescriptorType fieldDescriptorType) {
        final Object value = this.zza.get(fieldDescriptorType);
        if (value instanceof zzgjj) {
            return zzgjj.zza();
        }
        return value;
    }
    
    public final void zza(final zzgin<FieldDescriptorType> zzgin) {
        for (int i = 0; i < zzgin.zza.zzc(); ++i) {
            this.zzb(zzgin.zza.zzb(i));
        }
        final Iterator<Map.Entry<FieldDescriptorType, Object>> iterator = zzgin.zza.zzd().iterator();
        while (iterator.hasNext()) {
            this.zzb((Map.Entry)iterator.next());
        }
    }
    
    public final void zza(final FieldDescriptorType fieldDescriptorType, Object o) {
        if (fieldDescriptorType.zzd()) {
            if (!(o instanceof List)) {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
            final ArrayList<Object> list = new ArrayList<Object>();
            list.addAll((Collection<?>)o);
            final ArrayList<Object> list2 = list;
            final int size = list2.size();
            int i = 0;
            while (i < size) {
                final Object value = list2.get(i);
                ++i;
                zza(fieldDescriptorType.zzb(), value);
            }
            o = list;
        }
        else {
            zza(fieldDescriptorType.zzb(), o);
        }
        if (o instanceof zzgjj) {
            this.zzc = true;
        }
        this.zza.zza(fieldDescriptorType, o);
    }
    
    final boolean zzb() {
        return this.zza.isEmpty();
    }
    
    public final void zzc() {
        if (this.zzb) {
            return;
        }
        this.zza.zza();
        this.zzb = true;
    }
    
    public final boolean zzd() {
        return this.zzb;
    }
    
    public final Iterator<Map.Entry<FieldDescriptorType, Object>> zze() {
        if (this.zzc) {
            return new zzgjm<FieldDescriptorType>(this.zza.entrySet().iterator());
        }
        return this.zza.entrySet().iterator();
    }
    
    final Iterator<Map.Entry<FieldDescriptorType, Object>> zzf() {
        if (this.zzc) {
            return new zzgjm<FieldDescriptorType>(this.zza.zze().iterator());
        }
        return this.zza.zze().iterator();
    }
    
    public final int zzh() {
        int i = 0;
        int n = 0;
        while (i < this.zza.zzc()) {
            final Map.Entry<FieldDescriptorType, Object> zzb = this.zza.zzb(i);
            n += zzb(zzb.getKey(), zzb.getValue());
            ++i;
        }
        for (final Map.Entry<zzgip<?>, V> entry : this.zza.zzd()) {
            n += zzb(entry.getKey(), entry.getValue());
        }
        return n;
    }
    
    public final int zzi() {
        int i = 0;
        int n = 0;
        while (i < this.zza.zzc()) {
            n += zzc(this.zza.zzb(i));
            ++i;
        }
        final Iterator<Map.Entry<FieldDescriptorType, Object>> iterator = this.zza.zzd().iterator();
        while (iterator.hasNext()) {
            n += zzc((Map.Entry)iterator.next());
        }
        return n;
    }
}
