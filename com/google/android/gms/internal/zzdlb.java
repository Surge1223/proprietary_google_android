package com.google.android.gms.internal;

import android.os.Parcel;

public interface zzdlb<T extends zzbih>
{
    void zza(final T p0, final Parcel p1);
}
