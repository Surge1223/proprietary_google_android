package com.google.android.gms.internal;

import android.os.Process;

final class zzbjo implements Runnable
{
    private final Runnable zza;
    private final int zzb;
    
    public zzbjo(final Runnable zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void run() {
        Process.setThreadPriority(this.zzb);
        this.zza.run();
    }
}
