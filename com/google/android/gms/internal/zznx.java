package com.google.android.gms.internal;

import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zznx extends IInterface
{
    IObjectWrapper zza() throws RemoteException;
    
    Uri zzb() throws RemoteException;
    
    double zzc() throws RemoteException;
}
