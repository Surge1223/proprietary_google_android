package com.google.android.gms.internal;

import java.io.IOException;
import android.util.JsonWriter;

interface zzaic
{
    void zza(final JsonWriter p0) throws IOException;
}
