package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzdti extends zzey implements zzdth
{
    zzdti(final IBinder binder) {
        super(binder, "com.google.android.gms.phenotype.internal.IPhenotypeService");
    }
    
    @Override
    public final void zza(final zzdtf zzdtf, final String s, final int n, final String[] array, final byte[] array2) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzdtf);
        a_.writeString(s);
        a_.writeInt(n);
        a_.writeStringArray(array);
        a_.writeByteArray(array2);
        this.zzb(1, a_);
    }
    
    @Override
    public final void zza(final zzdtf zzdtf, final String s, final String s2, final String s3) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzdtf);
        a_.writeString(s);
        a_.writeString(s2);
        a_.writeString(s3);
        this.zzb(11, a_);
    }
    
    @Override
    public final void zzb(final zzdtf zzdtf, final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzdtf);
        a_.writeString(s);
        this.zzb(5, a_);
    }
}
