package com.google.android.gms.internal;

import java.util.List;
import android.util.SparseArray;
import android.os.Parcelable;
import android.os.IBinder;
import android.os.Bundle;
import android.os.Parcel;

public final class zzbig
{
    public static int zza(final Parcel parcel) {
        return zzb(parcel, 20293);
    }
    
    public static void zza(final Parcel parcel, final int n) {
        zzc(parcel, n);
    }
    
    public static void zza(final Parcel parcel, final int n, final double n2) {
        zzb(parcel, n, 8);
        parcel.writeDouble(n2);
    }
    
    public static void zza(final Parcel parcel, final int n, final float n2) {
        zzb(parcel, n, 4);
        parcel.writeFloat(n2);
    }
    
    public static void zza(final Parcel parcel, final int n, final int n2) {
        zzb(parcel, n, 4);
        parcel.writeInt(n2);
    }
    
    public static void zza(final Parcel parcel, final int n, final long n2) {
        zzb(parcel, n, 8);
        parcel.writeLong(n2);
    }
    
    public static void zza(final Parcel parcel, int zzb, final Bundle bundle, final boolean b) {
        if (bundle == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeBundle(bundle);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final IBinder binder, final boolean b) {
        if (binder == null) {
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeStrongBinder(binder);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final Parcel parcel2, final boolean b) {
        if (parcel2 == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.appendFrom(parcel2, 0, parcel2.dataSize());
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final Parcelable parcelable, final int n, final boolean b) {
        if (parcelable == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcelable.writeToParcel(parcel, n);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int i, final SparseArray<byte[]> sparseArray, final boolean b) {
        if (sparseArray == null) {
            return;
        }
        final int zzb = zzb(parcel, 1);
        final int size = sparseArray.size();
        parcel.writeInt(size);
        for (i = 0; i < size; ++i) {
            parcel.writeInt(sparseArray.keyAt(i));
            parcel.writeByteArray((byte[])sparseArray.valueAt(i));
        }
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, final int n, final Long n2, final boolean b) {
        if (n2 == null) {
            return;
        }
        zzb(parcel, n, 8);
        parcel.writeLong((long)n2);
    }
    
    public static void zza(final Parcel parcel, int zzb, final String s, final boolean b) {
        if (s == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeString(s);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int i, final List<Integer> list, final boolean b) {
        if (list == null) {
            return;
        }
        final int zzb = zzb(parcel, i);
        final int size = list.size();
        parcel.writeInt(size);
        for (i = 0; i < size; ++i) {
            parcel.writeInt((int)list.get(i));
        }
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, final int n, final boolean b) {
        zzb(parcel, n, 4);
        parcel.writeInt((int)(b ? 1 : 0));
    }
    
    public static void zza(final Parcel parcel, int zzb, final byte[] array, final boolean b) {
        if (array == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeByteArray(array);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final double[] array, final boolean b) {
        if (array == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeDoubleArray(array);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final int[] array, final boolean b) {
        if (array == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeIntArray(array);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final long[] array, final boolean b) {
        if (array == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeLongArray(array);
        zzc(parcel, zzb);
    }
    
    public static <T extends Parcelable> void zza(final Parcel parcel, int i, final T[] array, final int n, final boolean b) {
        if (array == null) {
            return;
        }
        final int zzb = zzb(parcel, i);
        final int length = array.length;
        parcel.writeInt(length);
        Parcelable parcelable;
        for (i = 0; i < length; ++i) {
            parcelable = array[i];
            if (parcelable == null) {
                parcel.writeInt(0);
            }
            else {
                zza(parcel, parcelable, n);
            }
        }
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int zzb, final String[] array, final boolean b) {
        if (array == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeStringArray(array);
        zzc(parcel, zzb);
    }
    
    public static void zza(final Parcel parcel, int i, final byte[][] array, final boolean b) {
        if (array == null) {
            return;
        }
        final int zzb = zzb(parcel, i);
        final int length = array.length;
        parcel.writeInt(length);
        for (i = 0; i < length; ++i) {
            parcel.writeByteArray(array[i]);
        }
        zzc(parcel, zzb);
    }
    
    private static <T extends Parcelable> void zza(final Parcel parcel, final T t, int dataPosition) {
        final int dataPosition2 = parcel.dataPosition();
        parcel.writeInt(1);
        final int dataPosition3 = parcel.dataPosition();
        t.writeToParcel(parcel, dataPosition);
        dataPosition = parcel.dataPosition();
        parcel.setDataPosition(dataPosition2);
        parcel.writeInt(dataPosition - dataPosition3);
        parcel.setDataPosition(dataPosition);
    }
    
    private static int zzb(final Parcel parcel, final int n) {
        parcel.writeInt(n | 0xFFFF0000);
        parcel.writeInt(0);
        return parcel.dataPosition();
    }
    
    private static void zzb(final Parcel parcel, final int n, final int n2) {
        if (n2 >= 65535) {
            parcel.writeInt(n | 0xFFFF0000);
            parcel.writeInt(n2);
            return;
        }
        parcel.writeInt(n | n2 << 16);
    }
    
    public static void zzb(final Parcel parcel, int zzb, final List<String> list, final boolean b) {
        if (list == null) {
            if (b) {
                zzb(parcel, zzb, 0);
            }
            return;
        }
        zzb = zzb(parcel, zzb);
        parcel.writeStringList((List)list);
        zzc(parcel, zzb);
    }
    
    private static void zzc(final Parcel parcel, final int n) {
        final int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(n - 4);
        parcel.writeInt(dataPosition - n);
        parcel.setDataPosition(dataPosition);
    }
    
    public static <T extends Parcelable> void zzc(final Parcel parcel, int i, final List<T> list, final boolean b) {
        if (list == null) {
            if (b) {
                zzb(parcel, i, 0);
            }
            return;
        }
        final int zzb = zzb(parcel, i);
        final int size = list.size();
        parcel.writeInt(size);
        Parcelable parcelable;
        for (i = 0; i < size; ++i) {
            parcelable = list.get(i);
            if (parcelable == null) {
                parcel.writeInt(0);
            }
            else {
                zza(parcel, parcelable, 0);
            }
        }
        zzc(parcel, zzb);
    }
}
