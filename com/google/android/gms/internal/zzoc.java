package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzoc extends zzez implements zzob
{
    public zzoc() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
    }
    
    public static zzob zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
        if (queryLocalInterface instanceof zzob) {
            return (zzob)queryLocalInterface;
        }
        return new zzod(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 5: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                break;
            }
            case 4: {
                this.zza();
                parcel2.writeNoException();
                break;
            }
            case 3: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 2: {
                final IObjectWrapper zza = this.zza(parcel.readString());
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zza);
                break;
            }
            case 1: {
                this.zza(parcel.readString(), IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
