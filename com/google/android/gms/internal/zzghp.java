package com.google.android.gms.internal;

import java.util.NoSuchElementException;
import java.util.Iterator;

final class zzghp implements Iterator
{
    private int zza;
    private final int zzb;
    private final /* synthetic */ zzgho zzc;
    
    zzghp(final zzgho zzc) {
        this.zzc = zzc;
        this.zza = 0;
        this.zzb = this.zzc.zza();
    }
    
    private final byte zza() {
        try {
            return this.zzc.zza(this.zza++);
        }
        catch (IndexOutOfBoundsException ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
    
    @Override
    public final boolean hasNext() {
        return this.zza < this.zzb;
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
