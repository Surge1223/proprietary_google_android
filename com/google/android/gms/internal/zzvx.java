package com.google.android.gms.internal;

import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.Parcelable;
import android.content.Intent;
import android.os.IBinder;

public final class zzvx extends zzey implements zzvv
{
    zzvx(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
    }
    
    @Override
    public final void zza(final int n, final int n2, final Intent intent) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeInt(n);
        a_.writeInt(n2);
        zzfa.zza(a_, (Parcelable)intent);
        this.zzb(12, a_);
    }
    
    @Override
    public final void zza(final Bundle bundle) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)bundle);
        this.zzb(1, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(13, a_);
    }
    
    @Override
    public final void zzb(final Bundle bundle) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)bundle);
        final Parcel zza = this.zza(6, a_);
        if (zza.readInt() != 0) {
            bundle.readFromParcel(zza);
        }
        zza.recycle();
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(10, this.a_());
    }
    
    @Override
    public final boolean zze() throws RemoteException {
        final Parcel zza = this.zza(11, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zzf() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zzg() throws RemoteException {
        this.zzb(3, this.a_());
    }
    
    @Override
    public final void zzh() throws RemoteException {
        this.zzb(4, this.a_());
    }
    
    @Override
    public final void zzi() throws RemoteException {
        this.zzb(5, this.a_());
    }
    
    @Override
    public final void zzj() throws RemoteException {
        this.zzb(7, this.a_());
    }
    
    @Override
    public final void zzk() throws RemoteException {
        this.zzb(8, this.a_());
    }
    
    @Override
    public final void zzl() throws RemoteException {
        this.zzb(9, this.a_());
    }
}
