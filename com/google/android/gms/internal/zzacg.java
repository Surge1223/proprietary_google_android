package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzacg extends zzey implements zzacf
{
    zzacg(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdCreator");
    }
}
