package com.google.android.gms.internal;

public enum zzgji
{
    zza((Class<?>)Void.class, (Class<?>)Void.class, (Object)null), 
    zzb((Class<?>)Integer.TYPE, (Class<?>)Integer.class, (Object)0), 
    zzc((Class<?>)Long.TYPE, (Class<?>)Long.class, (Object)0L), 
    zzd((Class<?>)Float.TYPE, (Class<?>)Float.class, (Object)0.0f), 
    zze((Class<?>)Double.TYPE, (Class<?>)Double.class, (Object)0.0), 
    zzf((Class<?>)Boolean.TYPE, (Class<?>)Boolean.class, (Object)false), 
    zzg((Class<?>)String.class, (Class<?>)String.class, (Object)""), 
    zzh((Class<?>)zzgho.class, (Class<?>)zzgho.class, (Object)zzgho.zza), 
    zzi((Class<?>)Integer.TYPE, (Class<?>)Integer.class, (Object)null), 
    zzj((Class<?>)Object.class, (Class<?>)Object.class, (Object)null);
    
    private final Class<?> zzk;
    private final Class<?> zzl;
    private final Object zzm;
    
    private zzgji(final Class<?> zzk, final Class<?> zzl, final Object zzm) {
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
    }
    
    public final Class<?> zza() {
        return this.zzl;
    }
}
