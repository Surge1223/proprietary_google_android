package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbh extends zzgmo<zzbh>
{
    public Long zza;
    private String zzb;
    private byte[] zzc;
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zza != null) {
            computeSerializedSize = n + zzgmm.zzf(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzb != null) {
            n2 = computeSerializedSize + zzgmm.zzb(3, this.zzb);
        }
        int n3 = n2;
        if (this.zzc != null) {
            n3 = n2 + zzgmm.zzb(4, this.zzc);
        }
        return n3;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zza != null) {
            zzgmm.zzb(1, this.zza);
        }
        if (this.zzb != null) {
            zzgmm.zza(3, this.zzb);
        }
        if (this.zzc != null) {
            zzgmm.zza(4, this.zzc);
        }
        super.writeTo(zzgmm);
    }
}
