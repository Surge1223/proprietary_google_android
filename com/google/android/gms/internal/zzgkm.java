package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzgkm<T> implements zzgky<T>
{
    private final zzgkg zza;
    private final zzglq<?, ?> zzb;
    private final boolean zzc;
    private final zzgij<?> zzd;
    
    private zzgkm(final Class<T> clazz, final zzglq<?, ?> zzb, final zzgij<?> zzd, final zzgkg zza) {
        this.zzb = zzb;
        this.zzc = zzd.zza(clazz);
        this.zzd = zzd;
        this.zza = zza;
    }
    
    static <T> zzgkm<T> zza(final Class<T> clazz, final zzglq<?, ?> zzglq, final zzgij<?> zzgij, final zzgkg zzgkg) {
        return new zzgkm<T>(clazz, zzglq, zzgij, zzgkg);
    }
    
    @Override
    public final int zza(final T t) {
        int hashCode = this.zzb.zzb(t).hashCode();
        if (this.zzc) {
            hashCode = hashCode * 53 + this.zzd.zza(t).hashCode();
        }
        return hashCode;
    }
    
    @Override
    public final void zza(final T t, final zzgmk zzgmk) {
        final Iterator<Map.Entry<?, Object>> zze = this.zzd.zza(t).zze();
        while (zze.hasNext()) {
            final Map.Entry<zzgip, V> entry = (Map.Entry<zzgip, V>)zze.next();
            final zzgip zzgip = entry.getKey();
            if (zzgip.zzc() != zzgmj.zzi || zzgip.zzd() || zzgip.zze()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
            if (entry instanceof zzgjl) {
                zzgmk.zzc(zzgip.zza(), ((zzgjl)entry).zza().zzc());
            }
            else {
                zzgmk.zzc(zzgip.zza(), entry.getValue());
            }
        }
        final zzglq<?, ?> zzb = this.zzb;
        zzb.zzb(zzb.zzb(t), zzgmk);
    }
    
    @Override
    public final boolean zza(final T t, final T t2) {
        return this.zzb.zzb(t).equals(this.zzb.zzb(t2)) && (!this.zzc || this.zzd.zza(t).equals(this.zzd.zza(t2)));
    }
    
    @Override
    public final int zzb(final T t) {
        final zzglq<?, ?> zzb = this.zzb;
        int n = 0 + zzb.zze(zzb.zzb(t));
        if (this.zzc) {
            n += this.zzd.zza(t).zzi();
        }
        return n;
    }
    
    @Override
    public final void zzb(final T t, final T t2) {
        zzgla.zza(this.zzb, t, t2);
        if (this.zzc) {
            zzgla.zza(this.zzd, t, t2);
        }
    }
}
