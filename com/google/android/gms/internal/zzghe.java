package com.google.android.gms.internal;

public abstract class zzghe<MessageType extends zzgkg> implements zzgkr<MessageType>
{
    private static final zzgii zza;
    
    static {
        zza = zzgii.zza();
    }
}
