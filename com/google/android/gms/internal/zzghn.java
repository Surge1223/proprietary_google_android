package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzghn
{
    public abstract void zza(final byte[] p0, final int p1, final int p2) throws IOException;
}
