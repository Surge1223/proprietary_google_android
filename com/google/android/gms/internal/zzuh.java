package com.google.android.gms.internal;

import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.internal.client.zzbh;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import java.util.ArrayList;
import com.google.android.gms.ads.reward.mediation.InitializableMediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.ads.internal.client.zzj;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import android.content.Context;
import com.google.android.gms.ads.mediation.OnContextChangedListener;
import java.util.Set;
import java.util.List;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.Collection;
import java.util.HashSet;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzx;
import java.util.Iterator;
import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import org.json.JSONObject;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzf;
import com.google.android.gms.ads.mediation.MediationAdapter;

public final class zzuh extends zzto
{
    private final MediationAdapter zza;
    private zzui zzb;
    
    public zzuh(final MediationAdapter zza) {
        this.zza = zza;
    }
    
    private final Bundle zza(String s, final zzf zzf, final String s2) throws RemoteException {
        final String value = String.valueOf(s);
        String concat;
        if (value.length() != 0) {
            concat = "Server parameters: ".concat(value);
        }
        else {
            concat = new String("Server parameters: ");
        }
        zzaid.zze(concat);
        try {
            final Bundle bundle = new Bundle();
            Bundle bundle3;
            if (s != null) {
                final JSONObject jsonObject = new JSONObject(s);
                final Bundle bundle2 = new Bundle();
                final Iterator keys = jsonObject.keys();
                while (true) {
                    bundle3 = bundle2;
                    if (!keys.hasNext()) {
                        break;
                    }
                    s = keys.next();
                    bundle2.putString(s, jsonObject.getString(s));
                }
            }
            else {
                bundle3 = bundle;
            }
            if (this.zza instanceof AdMobAdapter) {
                bundle3.putString("adJson", s2);
                if (zzf != null) {
                    bundle3.putInt("tagForChildDirectedTreatment", zzf.zzg);
                }
            }
            return bundle3;
        }
        catch (Throwable t) {
            zzaid.zzc("Could not get Server Parameters Bundle.", t);
            throw new RemoteException();
        }
    }
    
    private static boolean zza(final zzf zzf) {
        if (!zzf.zzf) {
            zzx.zza();
            if (!zzaht.zza()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final IObjectWrapper zza() throws RemoteException {
        if (!(this.zza instanceof MediationBannerAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationBannerAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationBannerAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        try {
            return zzn.zza(((MediationBannerAdapter)this.zza).getBannerView());
        }
        catch (Throwable t) {
            zzaid.zzc("Could not get banner view from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final zzf zzf, final String s) throws RemoteException {
        this.zza(zzf, s, null);
    }
    
    @Override
    public final void zza(final zzf zzf, final String s, final String s2) throws RemoteException {
        if (!(this.zza instanceof MediationRewardedVideoAdAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Requesting rewarded video ad from adapter.");
        try {
            final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter)this.zza;
            final List<String> zze = zzf.zze;
            final Bundle bundle = null;
            Set<String> set;
            if (zze != null) {
                set = new HashSet<String>(zzf.zze);
            }
            else {
                set = null;
            }
            Date date;
            if (zzf.zzb == -1L) {
                date = null;
            }
            else {
                date = new Date(zzf.zzb);
            }
            final zzug zzug = new zzug(date, zzf.zzd, set, zzf.zzk, zza(zzf), zzf.zzg, zzf.zzr);
            Bundle bundle2;
            if (zzf.zzm != null) {
                bundle2 = zzf.zzm.getBundle(mediationRewardedVideoAdAdapter.getClass().getName());
            }
            else {
                bundle2 = bundle;
            }
            mediationRewardedVideoAdAdapter.loadAd(zzug, this.zza(s, zzf, s2), bundle2);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not load rewarded video ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        try {
            ((OnContextChangedListener)this.zza).onContextChanged(zzn.zza(objectWrapper));
        }
        catch (Throwable t) {
            zzaid.zza("Could not inform adapter of changed context", t);
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final zzade zzade, final String s2) throws RemoteException {
        if (!(this.zza instanceof MediationRewardedVideoAdAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Initialize rewarded video adapter.");
        try {
            final MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter)this.zza;
            final Bundle zza = this.zza(s2, zzf, null);
            Object o;
            Bundle bundle2;
            if (zzf != null) {
                Set<String> set;
                if (zzf.zze != null) {
                    set = new HashSet<String>(zzf.zze);
                }
                else {
                    set = null;
                }
                Date date;
                if (zzf.zzb == -1L) {
                    date = null;
                }
                else {
                    date = new Date(zzf.zzb);
                }
                final zzug zzug = new zzug(date, zzf.zzd, set, zzf.zzk, zza(zzf), zzf.zzg, zzf.zzr);
                if (zzf.zzm != null) {
                    final Bundle bundle = zzf.zzm.getBundle(mediationRewardedVideoAdAdapter.getClass().getName());
                    o = zzug;
                    bundle2 = bundle;
                }
                else {
                    o = zzug;
                    bundle2 = null;
                }
            }
            else {
                o = (bundle2 = null);
            }
            mediationRewardedVideoAdAdapter.initialize(zzn.zza(objectWrapper), (MediationAdRequest)o, s, new zzadh(zzade), zza, bundle2);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not initialize rewarded video adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final zztq zztq) throws RemoteException {
        this.zza(objectWrapper, zzf, s, null, zztq);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final String s2, final zztq zztq) throws RemoteException {
        if (!(this.zza instanceof MediationInterstitialAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationInterstitialAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Requesting interstitial ad from adapter.");
        try {
            final MediationInterstitialAdapter mediationInterstitialAdapter = (MediationInterstitialAdapter)this.zza;
            final List<String> zze = zzf.zze;
            final Bundle bundle = null;
            Set<String> set;
            if (zze != null) {
                set = new HashSet<String>(zzf.zze);
            }
            else {
                set = null;
            }
            Date date;
            if (zzf.zzb == -1L) {
                date = null;
            }
            else {
                date = new Date(zzf.zzb);
            }
            final zzug zzug = new zzug(date, zzf.zzd, set, zzf.zzk, zza(zzf), zzf.zzg, zzf.zzr);
            Bundle bundle2;
            if (zzf.zzm != null) {
                bundle2 = zzf.zzm.getBundle(mediationInterstitialAdapter.getClass().getName());
            }
            else {
                bundle2 = bundle;
            }
            mediationInterstitialAdapter.requestInterstitialAd(zzn.zza(objectWrapper), new zzui(zztq), this.zza(s, zzf, s2), zzug, bundle2);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not request interstitial ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final String s2, final zztq zztq, final zznm zznm, final List<String> list) throws RemoteException {
        if (!(this.zza instanceof MediationNativeAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationNativeAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationNativeAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        try {
            final MediationNativeAdapter mediationNativeAdapter = (MediationNativeAdapter)this.zza;
            final List<String> zze = zzf.zze;
            final Bundle bundle = null;
            Set<String> set;
            if (zze != null) {
                set = new HashSet<String>(zzf.zze);
            }
            else {
                set = null;
            }
            Date date;
            if (zzf.zzb == -1L) {
                date = null;
            }
            else {
                date = new Date(zzf.zzb);
            }
            final zzul zzul = new zzul(date, zzf.zzd, set, zzf.zzk, zza(zzf), zzf.zzg, zznm, list, zzf.zzr);
            Bundle bundle2;
            if (zzf.zzm != null) {
                bundle2 = zzf.zzm.getBundle(mediationNativeAdapter.getClass().getName());
            }
            else {
                bundle2 = bundle;
            }
            this.zzb = new zzui(zztq);
            mediationNativeAdapter.requestNativeAd(zzn.zza(objectWrapper), this.zzb, this.zza(s, zzf, s2), zzul, bundle2);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not request native ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzj zzj, final zzf zzf, final String s, final zztq zztq) throws RemoteException {
        this.zza(objectWrapper, zzj, zzf, s, null, zztq);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzj zzj, final zzf zzf, final String s, final String s2, final zztq zztq) throws RemoteException {
        if (!(this.zza instanceof MediationBannerAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationBannerAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationBannerAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Requesting banner ad from adapter.");
        try {
            final MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter)this.zza;
            final List<String> zze = zzf.zze;
            final Bundle bundle = null;
            Set<String> set;
            if (zze != null) {
                set = new HashSet<String>(zzf.zze);
            }
            else {
                set = null;
            }
            Date date;
            if (zzf.zzb == -1L) {
                date = null;
            }
            else {
                date = new Date(zzf.zzb);
            }
            final zzug zzug = new zzug(date, zzf.zzd, set, zzf.zzk, zza(zzf), zzf.zzg, zzf.zzr);
            Bundle bundle2;
            if (zzf.zzm != null) {
                bundle2 = zzf.zzm.getBundle(mediationBannerAdapter.getClass().getName());
            }
            else {
                bundle2 = bundle;
            }
            mediationBannerAdapter.requestBannerAd(zzn.zza(objectWrapper), new zzui(zztq), this.zza(s, zzf, s2), com.google.android.gms.ads.zzb.zza(zzj.zze, zzj.zzb, zzj.zza), zzug, bundle2);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not request banner ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzade zzade, final List<String> list) throws RemoteException {
        if (!(this.zza instanceof InitializableMediationRewardedVideoAdAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not an InitializableMediationRewardedVideoAdAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not an InitializableMediationRewardedVideoAdAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Initialize rewarded video adapter.");
        try {
            final InitializableMediationRewardedVideoAdAdapter initializableMediationRewardedVideoAdAdapter = (InitializableMediationRewardedVideoAdAdapter)this.zza;
            final ArrayList<Bundle> list2 = new ArrayList<Bundle>();
            final Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                list2.add(this.zza(iterator.next(), null, null));
            }
            initializableMediationRewardedVideoAdAdapter.initialize(zzn.zza(objectWrapper), new zzadh(zzade), list2);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not initialize rewarded video adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        if (!(this.zza instanceof OnImmersiveModeUpdatedListener)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not an OnImmersiveModeUpdatedListener: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not an OnImmersiveModeUpdatedListener: ");
            }
            zzaid.zzd(concat);
            return;
        }
        try {
            ((OnImmersiveModeUpdatedListener)this.zza).onImmersiveModeUpdated(b);
        }
        catch (Throwable t) {
            zzaid.zzc("Could not set immersive mode.", t);
        }
    }
    
    @Override
    public final void zzb() throws RemoteException {
        if (!(this.zza instanceof MediationInterstitialAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationInterstitialAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter)this.zza).showInterstitial();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not show interstitial from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zzc() throws RemoteException {
        try {
            this.zza.onDestroy();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not destroy adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zzd() throws RemoteException {
        try {
            this.zza.onPause();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not pause adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zze() throws RemoteException {
        try {
            this.zza.onResume();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not resume adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final void zzf() throws RemoteException {
        if (!(this.zza instanceof MediationRewardedVideoAdAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Show rewarded video ad from adapter.");
        try {
            ((MediationRewardedVideoAdAdapter)this.zza).showVideo();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not show rewarded video ad from adapter.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final boolean zzg() throws RemoteException {
        if (!(this.zza instanceof MediationRewardedVideoAdAdapter)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: ");
            }
            zzaid.zze(concat);
            throw new RemoteException();
        }
        zzaid.zzb("Check if adapter is initialized.");
        try {
            return ((MediationRewardedVideoAdAdapter)this.zza).isInitialized();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not check if adapter is initialized.", t);
            throw new RemoteException();
        }
    }
    
    @Override
    public final zztw zzh() {
        final NativeAdMapper zza = this.zzb.zza();
        if (zza instanceof NativeAppInstallAdMapper) {
            return new zzuj((NativeAppInstallAdMapper)zza);
        }
        return null;
    }
    
    @Override
    public final zztz zzi() {
        final NativeAdMapper zza = this.zzb.zza();
        if (zza instanceof NativeContentAdMapper) {
            return new zzuk((NativeContentAdMapper)zza);
        }
        return null;
    }
    
    @Override
    public final Bundle zzj() {
        if (!(this.zza instanceof zzant)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a v2 MediationBannerAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a v2 MediationBannerAdapter: ");
            }
            zzaid.zze(concat);
            return new Bundle();
        }
        return ((zzant)this.zza).zza();
    }
    
    @Override
    public final Bundle zzk() {
        if (!(this.zza instanceof zzanu)) {
            final String value = String.valueOf(this.zza.getClass().getCanonicalName());
            String concat;
            if (value.length() != 0) {
                concat = "MediationAdapter is not a v2 MediationInterstitialAdapter: ".concat(value);
            }
            else {
                concat = new String("MediationAdapter is not a v2 MediationInterstitialAdapter: ");
            }
            zzaid.zze(concat);
            return new Bundle();
        }
        return ((zzanu)this.zza).getInterstitialAdapterInfo();
    }
    
    @Override
    public final Bundle zzl() {
        return new Bundle();
    }
    
    @Override
    public final boolean zzm() {
        return this.zza instanceof InitializableMediationRewardedVideoAdAdapter;
    }
    
    @Override
    public final zzot zzn() {
        final NativeCustomTemplateAd zzc = this.zzb.zzc();
        if (zzc instanceof zzow) {
            return ((zzow)zzc).zza();
        }
        return null;
    }
    
    @Override
    public final zzbh zzo() {
        if (!(this.zza instanceof com.google.android.gms.ads.mediation.zzb)) {
            return null;
        }
        try {
            return ((com.google.android.gms.ads.mediation.zzb)this.zza).getVideoController();
        }
        catch (Throwable t) {
            zzaid.zzc("Could not get video controller.", t);
            return null;
        }
    }
    
    @Override
    public final zzuc zzp() {
        final zza zzb = this.zzb.zzb();
        if (zzb != null) {
            return new zzvb(zzb);
        }
        return null;
    }
}
