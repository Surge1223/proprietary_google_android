package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;

public final class zzof extends zzey implements zzoe
{
    zzof(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegateCreator");
    }
    
    @Override
    public final IBinder zza(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)objectWrapper2);
        zzfa.zza(a_, (IInterface)objectWrapper3);
        a_.writeInt(12438000);
        final Parcel zza = this.zza(1, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zza.recycle();
        return strongBinder;
    }
}
