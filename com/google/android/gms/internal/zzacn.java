package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.zzf;
import android.os.Parcelable.Creator;

public final class zzacn extends zzbid
{
    public static final Parcelable.Creator<zzacn> CREATOR;
    public final zzf zza;
    public final String zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaco();
    }
    
    public zzacn(final zzf zza, final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, 3, this.zzb, false);
        zzbig.zza(parcel, zza);
    }
}
