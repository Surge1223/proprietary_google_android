package com.google.android.gms.internal;

import com.google.ads.AdRequest;

final class zzva
{
    static {
        zza = new int[AdRequest.ErrorCode.values().length];
        try {
            zzva.zza[AdRequest.ErrorCode.INTERNAL_ERROR.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            zzva.zza[AdRequest.ErrorCode.INVALID_REQUEST.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError2) {}
        try {
            zzva.zza[AdRequest.ErrorCode.NETWORK_ERROR.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError3) {}
        try {
            zzva.zza[AdRequest.ErrorCode.NO_FILL.ordinal()] = 4;
        }
        catch (NoSuchFieldError noSuchFieldError4) {}
        zzb = new int[AdRequest.Gender.values().length];
        try {
            zzva.zzb[AdRequest.Gender.FEMALE.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError5) {}
        try {
            zzva.zzb[AdRequest.Gender.MALE.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError6) {}
        try {
            zzva.zzb[AdRequest.Gender.UNKNOWN.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError7) {}
    }
}
