package com.google.android.gms.internal;

final class zzgjv implements zzgkz
{
    private static final zzgkf zzb;
    private final zzgkf zza;
    
    static {
        zzb = new zzgjw();
    }
    
    public zzgjv() {
        this(new zzgjx(new zzgkf[] { zzgiv.zza(), zza() }));
    }
    
    private zzgjv(final zzgkf zzgkf) {
        this.zza = zzgja.zza(zzgkf, "messageInfoFactory");
    }
    
    private static zzgkf zza() {
        try {
            return (zzgkf)Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
        }
        catch (Exception ex) {
            return zzgjv.zzb;
        }
    }
    
    private static boolean zza(final zzgke zzgke) {
        return zzgke.zza() == zzgiw.zzg.zzi;
    }
    
    @Override
    public final <T> zzgky<T> zza(final Class<T> clazz) {
        zzgla.zza(clazz);
        final zzgke zzb = this.zza.zzb(clazz);
        if (zzb.zzb()) {
            if (zzgiw.class.isAssignableFrom(clazz)) {
                return zzgkm.zza(clazz, zzgla.zzc(), zzgim.zza(), zzb.zzc());
            }
            return zzgkm.zza(clazz, zzgla.zza(), zzgim.zzb(), zzb.zzc());
        }
        else if (zzgiw.class.isAssignableFrom(clazz)) {
            if (zza(zzb)) {
                return zzgkk.zza(clazz, zzb, zzgkq.zzb(), zzgjq.zzb(), zzgla.zzc(), zzgim.zza(), zzgkd.zzb());
            }
            return zzgkk.zza(clazz, zzb, zzgkq.zzb(), zzgjq.zzb(), zzgla.zzc(), null, zzgkd.zzb());
        }
        else {
            if (zza(zzb)) {
                return zzgkk.zza(clazz, zzb, zzgkq.zza(), zzgjq.zza(), zzgla.zza(), zzgim.zzb(), zzgkd.zza());
            }
            return zzgkk.zza(clazz, zzb, zzgkq.zza(), zzgjq.zza(), zzgla.zzb(), null, zzgkd.zza());
        }
    }
}
