package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzabz extends IInterface
{
    String zza() throws RemoteException;
    
    int zzb() throws RemoteException;
}
