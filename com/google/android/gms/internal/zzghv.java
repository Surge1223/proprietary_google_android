package com.google.android.gms.internal;

import java.io.IOException;
import java.nio.charset.Charset;

class zzghv extends zzghu
{
    protected final byte[] zzb;
    
    zzghv(final byte[] zzb) {
        this.zzb = zzb;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgho)) {
            return false;
        }
        if (this.zza() != ((zzgho)o).zza()) {
            return false;
        }
        if (this.zza() == 0) {
            return true;
        }
        if (o instanceof zzghv) {
            final zzghv zzghv = (zzghv)o;
            final int zzg = this.zzg();
            final int zzg2 = zzghv.zzg();
            return (zzg == 0 || zzg2 == 0 || zzg == zzg2) && this.zza(zzghv, 0, this.zza());
        }
        return o.equals(this);
    }
    
    @Override
    public byte zza(final int n) {
        return this.zzb[n];
    }
    
    @Override
    public int zza() {
        return this.zzb.length;
    }
    
    @Override
    protected final int zza(final int n, final int n2, final int n3) {
        return zzgja.zza(n, this.zzb, this.zzh(), n3);
    }
    
    @Override
    public final zzgho zza(int zzb, final int n) {
        zzb = zzgho.zzb(0, n, this.zza());
        if (zzb == 0) {
            return zzgho.zza;
        }
        return new zzghr(this.zzb, this.zzh(), zzb);
    }
    
    @Override
    protected final String zza(final Charset charset) {
        return new String(this.zzb, this.zzh(), this.zza(), charset);
    }
    
    @Override
    final void zza(final zzghn zzghn) throws IOException {
        zzghn.zza(this.zzb, this.zzh(), this.zza());
    }
    
    @Override
    final boolean zza(final zzgho zzgho, int n, final int n2) {
        if (n2 > zzgho.zza()) {
            n = this.zza();
            final StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(n2);
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        if (n2 > zzgho.zza()) {
            n = zzgho.zza();
            final StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(n2);
            sb2.append(", ");
            sb2.append(n);
            throw new IllegalArgumentException(sb2.toString());
        }
        if (zzgho instanceof zzghv) {
            final zzghv zzghv = (zzghv)zzgho;
            final byte[] zzb = this.zzb;
            final byte[] zzb2 = zzghv.zzb;
            int zzh;
            int i;
            for (zzh = this.zzh(), i = this.zzh(), n = zzghv.zzh(); i < zzh + n2; ++i, ++n) {
                if (zzb[i] != zzb2[n]) {
                    return false;
                }
            }
            return true;
        }
        return zzgho.zza(0, n2).equals(this.zza(0, n2));
    }
    
    @Override
    public final boolean zze() {
        final int zzh = this.zzh();
        return zzgly.zza(this.zzb, zzh, this.zza() + zzh);
    }
    
    protected int zzh() {
        return 0;
    }
}
