package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzog extends IInterface
{
    void zza() throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
}
