package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcelable;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import android.content.Intent;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public abstract class zzvw extends zzez implements zzvv
{
    public static zzvv zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
        if (queryLocalInterface instanceof zzvv) {
            return (zzvv)queryLocalInterface;
        }
        return new zzvx(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 13: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 12: {
                this.zza(parcel.readInt(), parcel.readInt(), zzfa.zza(parcel, (android.os.Parcelable.Creator<Intent>)Intent.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 11: {
                final boolean zze = this.zze();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zze);
                break;
            }
            case 10: {
                this.zzd();
                parcel2.writeNoException();
                break;
            }
            case 9: {
                this.zzl();
                parcel2.writeNoException();
                break;
            }
            case 8: {
                this.zzk();
                parcel2.writeNoException();
                break;
            }
            case 7: {
                this.zzj();
                parcel2.writeNoException();
                break;
            }
            case 6: {
                final Bundle bundle = zzfa.zza(parcel, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR);
                this.zzb(bundle);
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)bundle);
                break;
            }
            case 5: {
                this.zzi();
                parcel2.writeNoException();
                break;
            }
            case 4: {
                this.zzh();
                parcel2.writeNoException();
                break;
            }
            case 3: {
                this.zzg();
                parcel2.writeNoException();
                break;
            }
            case 2: {
                this.zzf();
                parcel2.writeNoException();
                break;
            }
            case 1: {
                this.zza(zzfa.zza(parcel, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
