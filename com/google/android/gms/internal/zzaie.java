package com.google.android.gms.internal;

import java.io.IOException;
import com.google.android.gms.ads.internal.client.zzx;
import java.net.URL;
import java.net.HttpURLConnection;

public final class zzaie implements zzahw
{
    private final String zza;
    
    public zzaie() {
        this(null);
    }
    
    public zzaie(final String zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final String s) {
        try {
            final String value = String.valueOf(s);
            String concat;
            if (value.length() != 0) {
                concat = "Pinging URL: ".concat(value);
            }
            else {
                concat = new String("Pinging URL: ");
            }
            zzaid.zzb(concat);
            final HttpURLConnection httpURLConnection = (HttpURLConnection)new URL(s).openConnection();
            try {
                zzx.zza();
                zzaht.zza(true, httpURLConnection, this.zza);
                final zzahx zzahx = new zzahx();
                zzahx.zza(httpURLConnection, null);
                final int responseCode = httpURLConnection.getResponseCode();
                zzahx.zza(httpURLConnection, responseCode);
                if (responseCode < 200 || responseCode >= 300) {
                    final StringBuilder sb = new StringBuilder(65 + String.valueOf(s).length());
                    sb.append("Received non-success response code ");
                    sb.append(responseCode);
                    sb.append(" from pinging URL: ");
                    sb.append(s);
                    zzaid.zze(sb.toString());
                }
            }
            finally {
                httpURLConnection.disconnect();
            }
        }
        catch (RuntimeException ex) {
            final String message = ex.getMessage();
            final StringBuilder sb2 = new StringBuilder(27 + String.valueOf(s).length() + String.valueOf(message).length());
            sb2.append("Error while pinging URL: ");
            sb2.append(s);
            sb2.append(". ");
            sb2.append(message);
            zzaid.zze(sb2.toString());
        }
        catch (IOException ex2) {
            final String message2 = ex2.getMessage();
            final StringBuilder sb3 = new StringBuilder(27 + String.valueOf(s).length() + String.valueOf(message2).length());
            sb3.append("Error while pinging URL: ");
            sb3.append(s);
            sb3.append(". ");
            sb3.append(message2);
            zzaid.zze(sb3.toString());
        }
        catch (IndexOutOfBoundsException ex3) {
            final String message3 = ex3.getMessage();
            final StringBuilder sb4 = new StringBuilder(32 + String.valueOf(s).length() + String.valueOf(message3).length());
            sb4.append("Error while parsing ping URL: ");
            sb4.append(s);
            sb4.append(". ");
            sb4.append(message3);
            zzaid.zze(sb4.toString());
        }
    }
}
