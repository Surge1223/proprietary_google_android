package com.google.android.gms.internal;

import java.util.List;

final class zzgla
{
    private static final Class<?> zza;
    private static final zzglq<?, ?> zzb;
    private static final zzglq<?, ?> zzc;
    private static final zzglq<?, ?> zzd;
    
    static {
        zza = zzd();
        zzb = zza(false);
        zzc = zza(true);
        zzd = new zzgls();
    }
    
    static int zza(final int n, final Object o) {
        if (o instanceof zzgjn) {
            return zzgic.zza(n, (zzgjn)o);
        }
        return zzgic.zzc(n, (zzgkg)o);
    }
    
    static int zza(int n, final List<?> list) {
        final int size = list.size();
        int n2 = 0;
        final int n3 = 0;
        if (size == 0) {
            return 0;
        }
        final int n4 = n = zzgic.zze(n) * size;
        int n5;
        if (list instanceof zzgjp) {
            final zzgjp zzgjp = (zzgjp)list;
            n = n4;
            for (int i = n3; i < size; ++i) {
                final Object zzb = zzgjp.zzb(i);
                if (zzb instanceof zzgho) {
                    n += zzgic.zzb((zzgho)zzb);
                }
                else {
                    n += zzgic.zzb((String)zzb);
                }
            }
            n5 = n;
        }
        else {
            while (true) {
                n5 = n;
                if (n2 >= size) {
                    break;
                }
                final zzgho value = list.get(n2);
                if (value instanceof zzgho) {
                    n += zzgic.zzb(value);
                }
                else {
                    n += zzgic.zzb((String)value);
                }
                ++n2;
            }
        }
        return n5;
    }
    
    static int zza(final int n, final List<Long> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgju) {
            final zzgju zzgju = (zzgju)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zzd(zzgju.zzb(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zzd(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    public static zzglq<?, ?> zza() {
        return zzgla.zzb;
    }
    
    private static zzglq<?, ?> zza(final boolean b) {
        try {
            final Class<?> zze = zze();
            if (zze == null) {
                return null;
            }
            return (zzglq<?, ?>)zze.getConstructor(Boolean.TYPE).newInstance(b);
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    public static void zza(final int n, final List<String> list, final zzgmk zzgmk) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zza(n, list);
        }
    }
    
    public static void zza(final int n, final List<Double> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzg(n, list, b);
        }
    }
    
    static <T, FT extends zzgip<FT>> void zza(final zzgij<FT> zzgij, final T t, final T t2) {
        final zzgin<FT> zza = zzgij.zza(t2);
        if (!zza.zzb()) {
            zzgij.zzb(t).zza(zza);
        }
    }
    
    static <T> void zza(final zzgkb zzgkb, final T t, final T t2, final long n) {
        zzglw.zza(t, n, zzgkb.zza(zzglw.zzf(t, n), zzglw.zzf(t2, n)));
    }
    
    static <T, UT, UB> void zza(final zzglq<UT, UB> zzglq, final T t, final T t2) {
        zzglq.zza(t, zzglq.zzc(zzglq.zzb(t), zzglq.zzb(t2)));
    }
    
    public static void zza(final Class<?> clazz) {
        if (!zzgiw.class.isAssignableFrom(clazz) && zzgla.zza != null && !zzgla.zza.isAssignableFrom(clazz)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }
    
    public static boolean zza(final int n, final int n2, final int n3) {
        if (n2 < 40) {
            return true;
        }
        final long n4 = n2;
        final long n5 = n;
        final long n6 = n3;
        return n4 - n5 + 1L + 9L <= 2L * n6 + 3L + 3L * (n6 + 3L);
    }
    
    static boolean zza(final Object o, final Object o2) {
        return o == o2 || (o != null && o.equals(o2));
    }
    
    static int zzb(int n, final List<?> list) {
        final int size = list.size();
        int i = 0;
        if (size == 0) {
            return 0;
        }
        n = zzgic.zze(n) * size;
        while (i < size) {
            final Object value = list.get(i);
            if (value instanceof zzgjn) {
                n += zzgic.zza((zzgjn)value);
            }
            else {
                n += zzgic.zzb((zzgkg)value);
            }
            ++i;
        }
        return n;
    }
    
    static int zzb(final int n, final List<Long> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgju) {
            final zzgju zzgju = (zzgju)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zze(zzgju.zzb(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zze(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    public static zzglq<?, ?> zzb() {
        return zzgla.zzc;
    }
    
    public static void zzb(final int n, final List<zzgho> list, final zzgmk zzgmk) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzb(n, list);
        }
    }
    
    public static void zzb(final int n, final List<Float> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzf(n, list, b);
        }
    }
    
    static int zzc(int i, final List<zzgho> list) {
        final int size = list.size();
        final int n = 0;
        if (size == 0) {
            return 0;
        }
        final int n2 = size * zzgic.zze(i);
        i = n;
        int n3 = n2;
        while (i < list.size()) {
            n3 += zzgic.zzb(list.get(i));
            ++i;
        }
        return n3;
    }
    
    static int zzc(final int n, final List<Long> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgju) {
            final zzgju zzgju = (zzgju)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zzf(zzgju.zzb(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zzf(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    public static zzglq<?, ?> zzc() {
        return zzgla.zzd;
    }
    
    public static void zzc(final int n, final List<?> list, final zzgmk zzgmk) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzc(n, list);
        }
    }
    
    public static void zzc(final int n, final List<Long> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzc(n, list, b);
        }
    }
    
    static int zzd(final int n, final List<zzgkg> list) {
        final int size = list.size();
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n2 = 0;
        while (i < size) {
            n2 += zzgic.zzf(n, list.get(i));
            ++i;
        }
        return n2;
    }
    
    static int zzd(final int n, final List<Integer> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgiz) {
            final zzgiz zzgiz = (zzgiz)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zzk(zzgiz.zzc(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zzk(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    private static Class<?> zzd() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    public static void zzd(final int n, final List<?> list, final zzgmk zzgmk) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzd(n, list);
        }
    }
    
    public static void zzd(final int n, final List<Long> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzd(n, list, b);
        }
    }
    
    static int zze(final int n, final List<Integer> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgiz) {
            final zzgiz zzgiz = (zzgiz)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zzf(zzgiz.zzc(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zzf(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    private static Class<?> zze() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    public static void zze(final int n, final List<Long> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzn(n, list, b);
        }
    }
    
    static int zzf(final int n, final List<Integer> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgiz) {
            final zzgiz zzgiz = (zzgiz)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zzg(zzgiz.zzc(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zzg(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    public static void zzf(final int n, final List<Long> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zze(n, list, b);
        }
    }
    
    static int zzg(final int n, final List<Integer> list, final boolean b) {
        final int size = list.size();
        final int n2 = 0;
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof zzgiz) {
            final zzgiz zzgiz = (zzgiz)list;
            int n3 = 0;
            while (i < size) {
                n3 += zzgic.zzh(zzgiz.zzc(i));
                ++i;
            }
            n4 = n3;
        }
        else {
            int n5 = 0;
            int n6 = n2;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += zzgic.zzh(list.get(n6));
                ++n6;
            }
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(n4);
        }
        return n4 + size * zzgic.zze(n);
    }
    
    public static void zzg(final int n, final List<Long> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzl(n, list, b);
        }
    }
    
    static int zzh(final int n, final List<?> list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(size << 2);
        }
        return size * zzgic.zzi(n, 0);
    }
    
    public static void zzh(final int n, final List<Integer> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zza(n, list, b);
        }
    }
    
    static int zzi(final int n, final List<?> list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(size << 3);
        }
        return size * zzgic.zzg(n, 0L);
    }
    
    public static void zzi(final int n, final List<Integer> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzj(n, list, b);
        }
    }
    
    static int zzj(final int n, final List<?> list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return zzgic.zze(n) + zzgic.zzl(size);
        }
        return size * zzgic.zzb(n, true);
    }
    
    public static void zzj(final int n, final List<Integer> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzm(n, list, b);
        }
    }
    
    public static void zzk(final int n, final List<Integer> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzb(n, list, b);
        }
    }
    
    public static void zzl(final int n, final List<Integer> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzk(n, list, b);
        }
    }
    
    public static void zzm(final int n, final List<Integer> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzh(n, list, b);
        }
    }
    
    public static void zzn(final int n, final List<Boolean> list, final zzgmk zzgmk, final boolean b) {
        if (list != null && !list.isEmpty()) {
            zzgmk.zzi(n, list, b);
        }
    }
}
