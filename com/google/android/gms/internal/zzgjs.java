package com.google.android.gms.internal;

import java.util.RandomAccess;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

final class zzgjs extends zzgjq
{
    private static final Class<?> zza;
    
    static {
        zza = Collections.unmodifiableList(Collections.emptyList()).getClass();
    }
    
    private zzgjs() {
        super(null);
    }
    
    private static <L> List<L> zza(final Object o, final long n, final int n2) {
        final List<? extends L> zzc = zzc(o, n);
        Object o2;
        if (zzc.isEmpty()) {
            if (zzc instanceof zzgjp) {
                o2 = new zzgjo(n2);
            }
            else {
                o2 = new ArrayList<L>(n2);
            }
            zzglw.zza(o, n, o2);
        }
        else {
            RandomAccess randomAccess;
            if (zzgjs.zza.isAssignableFrom(zzc.getClass())) {
                final ArrayList list = new ArrayList<L>(zzc.size() + n2);
                list.addAll((Collection<? extends L>)zzc);
                zzglw.zza(o, n, list);
                randomAccess = list;
            }
            else {
                o2 = zzc;
                if (!(zzc instanceof zzglt)) {
                    return (List<L>)o2;
                }
                final zzgjo zzgjo = new zzgjo(zzc.size() + n2);
                ((zzghf<Object>)zzgjo).addAll(zzc);
                zzglw.zza(o, n, zzgjo);
                randomAccess = zzgjo;
            }
            o2 = randomAccess;
        }
        return (List<L>)o2;
    }
    
    private static <E> List<E> zzc(final Object o, final long n) {
        return (List<E>)zzglw.zzf(o, n);
    }
    
    @Override
    final <E> void zza(final Object o, final Object o2, final long n) {
        List<?> zzc = zzc(o2, n);
        final List<Object> zza = zza(o, n, zzc.size());
        final int size = zza.size();
        final int size2 = zzc.size();
        if (size > 0 && size2 > 0) {
            zza.addAll(zzc);
        }
        if (size > 0) {
            zzc = zza;
        }
        zzglw.zza(o, n, zzc);
    }
}
