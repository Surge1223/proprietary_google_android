package com.google.android.gms.internal;

import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import com.google.android.gms.ads.reward.RewardItem;
import android.os.Parcelable.Creator;

public final class zzadi extends zzbid
{
    public static final Parcelable.Creator<zzadi> CREATOR;
    public final String zza;
    public final int zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzadj();
    }
    
    public zzadi(final RewardItem rewardItem) {
        this(rewardItem.getType(), rewardItem.getAmount());
    }
    
    public zzadi(final String zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o != null && o instanceof zzadi) {
            final zzadi zzadi = (zzadi)o;
            return zzak.zza(this.zza, zzadi.zza) && zzak.zza(this.zzb, zzadi.zzb);
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb });
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, zza);
    }
}
