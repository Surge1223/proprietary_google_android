package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.CustomRenderedAd;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;

public final class zzmj extends zzmh
{
    private final OnCustomRenderedAdLoadedListener zza;
    
    public zzmj(final OnCustomRenderedAdLoadedListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzmd zzmd) {
        this.zza.onCustomRenderedAdLoaded(new zzmc(zzmd));
    }
}
