package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzbh;
import java.util.List;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzuc extends IInterface
{
    String zza() throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final IObjectWrapper p1, final IObjectWrapper p2) throws RemoteException;
    
    List zzb() throws RemoteException;
    
    void zzb(final IObjectWrapper p0) throws RemoteException;
    
    String zzc() throws RemoteException;
    
    zznx zzd() throws RemoteException;
    
    String zze() throws RemoteException;
    
    String zzf() throws RemoteException;
    
    double zzg() throws RemoteException;
    
    String zzh() throws RemoteException;
    
    String zzi() throws RemoteException;
    
    zzbh zzj() throws RemoteException;
    
    zznt zzk() throws RemoteException;
    
    IObjectWrapper zzl() throws RemoteException;
    
    IObjectWrapper zzm() throws RemoteException;
    
    IObjectWrapper zzn() throws RemoteException;
    
    Bundle zzo() throws RemoteException;
    
    boolean zzp() throws RemoteException;
    
    boolean zzq() throws RemoteException;
    
    void zzr() throws RemoteException;
}
