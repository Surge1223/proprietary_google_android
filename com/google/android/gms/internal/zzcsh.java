package com.google.android.gms.internal;

import java.io.File;
import android.os.Parcelable;
import android.os.Parcel;
import com.google.android.gms.mdh.MdhFootprint;
import java.util.List;
import android.os.Parcelable.Creator;

public final class zzcsh extends zzbid
{
    public static final Parcelable.Creator<zzcsh> CREATOR;
    private final List<MdhFootprint> zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zzdla((android.os.Parcelable.Creator<zzbih>)new zzcsj());
    }
    
    public zzcsh(final List<MdhFootprint> zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && this.zza.equals(((zzcsh)o).zza));
    }
    
    @Override
    public final int hashCode() {
        if (this.zza == null) {
            return 0;
        }
        return this.zza.hashCode();
    }
    
    @Override
    public final String toString() {
        final String value = String.valueOf(this.zza);
        final StringBuilder sb = new StringBuilder(46 + String.valueOf(value).length());
        sb.append("MdhFootprintListSafeParcelable{mdhFootprints=");
        sb.append(value);
        sb.append('}');
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        zzdla.zza(this, parcel, new zzcsi(n), null);
    }
}
