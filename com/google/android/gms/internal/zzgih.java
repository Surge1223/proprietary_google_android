package com.google.android.gms.internal;

final class zzgih
{
    private static final Class<?> zza;
    
    static {
        zza = zzc();
    }
    
    public static zzgii zza() {
        if (zzgih.zza != null) {
            try {
                return zza("getEmptyRegistry");
            }
            catch (Exception ex) {}
        }
        return zzgii.zza;
    }
    
    private static final zzgii zza(final String s) throws Exception {
        return (zzgii)zzgih.zza.getDeclaredMethod(s, (Class<?>[])new Class[0]).invoke(null, new Object[0]);
    }
    
    private static Class<?> zzc() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        }
        catch (ClassNotFoundException ex) {
            return null;
        }
    }
}
