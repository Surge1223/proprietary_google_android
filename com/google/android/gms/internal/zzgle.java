package com.google.android.gms.internal;

import java.util.Map;
import java.util.Iterator;

final class zzgle extends zzglk
{
    private final /* synthetic */ zzglb zza;
    
    private zzgle(final zzglb zza) {
        this.zza = zza;
        super(zza, null);
    }
    
    @Override
    public final Iterator<Map.Entry<Object, Object>> iterator() {
        return new zzgld(this.zza, null);
    }
}
