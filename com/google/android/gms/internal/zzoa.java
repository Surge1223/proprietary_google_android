package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import android.net.Uri;
import android.graphics.drawable.Drawable;
import com.google.android.gms.ads.formats.NativeAd;

public final class zzoa extends Image
{
    private final zznx zza;
    private final Drawable zzb;
    private final Uri zzc;
    private final double zzd;
    
    public zzoa(final zznx zza) {
        this.zza = zza;
        final Uri uri = null;
        Drawable zzb;
        try {
            final IObjectWrapper zza2 = this.zza.zza();
            if (zza2 != null) {
                zzb = zzn.zza(zza2);
            }
            else {
                zzb = null;
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get drawable.", (Throwable)ex);
            zzb = null;
        }
        this.zzb = zzb;
        Uri zzb2;
        try {
            zzb2 = this.zza.zzb();
        }
        catch (RemoteException ex2) {
            zzaid.zzb("Failed to get uri.", (Throwable)ex2);
            zzb2 = uri;
        }
        this.zzc = zzb2;
        double zzc = 1.0;
        try {
            zzc = this.zza.zzc();
        }
        catch (RemoteException ex3) {
            zzaid.zzb("Failed to get scale.", (Throwable)ex3);
        }
        this.zzd = zzc;
    }
    
    @Override
    public final Drawable getDrawable() {
        return this.zzb;
    }
    
    @Override
    public final double getScale() {
        return this.zzd;
    }
    
    @Override
    public final Uri getUri() {
        return this.zzc;
    }
}
