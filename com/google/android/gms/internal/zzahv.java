package com.google.android.gms.internal;

final class zzahv extends Thread
{
    private final /* synthetic */ String zza;
    
    zzahv(final zzahu zzahu, final String zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        new zzaie().zza(this.zza);
    }
}
