package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzckx extends zzbid
{
    public static final Parcelable.Creator<zzckx> CREATOR;
    private final int zza;
    private final String zzb;
    private final String zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzcky();
    }
    
    zzckx(final int zza, final String zzb, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
}
