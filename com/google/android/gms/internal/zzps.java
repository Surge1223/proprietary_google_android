package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import android.os.IInterface;
import java.util.Iterator;
import android.os.RemoteException;
import android.os.IBinder;
import java.util.ArrayList;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

public final class zzps extends UnifiedNativeAd
{
    private final zzpp zza;
    private final List<NativeAd.Image> zzb;
    private final zzoa zzc;
    private final VideoController zzd;
    private final NativeAd.AdChoicesInfo zze;
    
    public zzps(final zzpp zza) {
        this.zzb = new ArrayList<NativeAd.Image>();
        this.zzd = new VideoController();
        this.zza = zza;
        final NativeAd.AdChoicesInfo adChoicesInfo = null;
        final NativeAd.AdChoicesInfo adChoicesInfo2 = null;
        try {
            final List zzb = this.zza.zzb();
            if (zzb != null) {
                for (final IBinder next : zzb) {
                    zznx zznx = null;
                    Label_0133: {
                        if (next instanceof IBinder) {
                            final IBinder binder = next;
                            if (binder != null) {
                                final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                                if (queryLocalInterface instanceof zznx) {
                                    zznx = (zznx)queryLocalInterface;
                                    break Label_0133;
                                }
                                zznx = new zznz(binder);
                                break Label_0133;
                            }
                        }
                        zznx = null;
                    }
                    if (zznx != null) {
                        this.zzb.add(new zzoa(zznx));
                    }
                }
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get image.", (Throwable)ex);
        }
        zzoa zzc;
        try {
            final zznx zzd = this.zza.zzd();
            if (zzd != null) {
                zzc = new zzoa(zzd);
            }
            else {
                zzc = null;
            }
        }
        catch (RemoteException ex2) {
            zzaid.zzb("Failed to get image.", (Throwable)ex2);
            zzc = null;
        }
        this.zzc = zzc;
        NativeAd.AdChoicesInfo zze = adChoicesInfo2;
        try {
            if (this.zza.zzr() != null) {
                zze = new zznw(this.zza.zzr());
            }
        }
        catch (RemoteException ex3) {
            zzaid.zzb("Failed to get attribution info.", (Throwable)ex3);
            zze = adChoicesInfo;
        }
        this.zze = zze;
    }
    
    @Override
    public final String zza() {
        try {
            return this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get headline.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final List<NativeAd.Image> zzb() {
        return this.zzb;
    }
    
    @Override
    public final String zzc() {
        try {
            return this.zza.zzc();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get body.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final NativeAd.Image zzd() {
        return this.zzc;
    }
    
    @Override
    public final String zze() {
        try {
            return this.zza.zze();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get call to action.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final String zzf() {
        try {
            return this.zza.zzf();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get attribution.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final Double zzg() {
        try {
            final double zzg = this.zza.zzg();
            if (zzg == -1.0) {
                return null;
            }
            return zzg;
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get star rating.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final String zzh() {
        try {
            return this.zza.zzh();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get store", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final String zzi() {
        try {
            return this.zza.zzi();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get price.", (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public final VideoController zzj() {
        try {
            if (this.zza.zzj() != null) {
                this.zzd.zza(this.zza.zzj());
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Exception occurred while getting video controller", (Throwable)ex);
        }
        return this.zzd;
    }
    
    @Override
    public final Object zzk() {
        try {
            final IObjectWrapper zzp = this.zza.zzp();
            if (zzp != null) {
                return zzn.zza(zzp);
            }
        }
        catch (RemoteException ex) {
            zzaid.zzb("Failed to get mediated ad.", (Throwable)ex);
        }
        return null;
    }
}
