package com.google.android.gms.internal;

import java.util.Arrays;

final class zzgmw
{
    final int zza;
    final byte[] zzb;
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgmw)) {
            return false;
        }
        final zzgmw zzgmw = (zzgmw)o;
        return this.zza == zzgmw.zza && Arrays.equals(this.zzb, zzgmw.zzb);
    }
    
    @Override
    public final int hashCode() {
        return (527 + this.zza) * 31 + Arrays.hashCode(this.zzb);
    }
}
