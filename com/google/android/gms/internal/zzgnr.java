package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgnr extends zzgmo<zzgnr> implements Cloneable
{
    private static volatile zzgnr[] zza;
    private String zzb;
    private String zzc;
    
    public zzgnr() {
        this.zzb = "";
        this.zzc = "";
        this.zzax = null;
        this.zzay = -1;
    }
    
    public static zzgnr[] zza() {
        if (zzgnr.zza == null) {
            synchronized (zzgms.zzb) {
                if (zzgnr.zza == null) {
                    zzgnr.zza = new zzgnr[0];
                }
            }
        }
        return zzgnr.zza;
    }
    
    private final zzgnr zzb() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zzb != null) {
            computeSerializedSize = n;
            if (!this.zzb.equals("")) {
                computeSerializedSize = n + zzgmm.zzb(1, this.zzb);
            }
        }
        int n2 = computeSerializedSize;
        if (this.zzc != null) {
            n2 = computeSerializedSize;
            if (!this.zzc.equals("")) {
                n2 = computeSerializedSize + zzgmm.zzb(2, this.zzc);
            }
        }
        return n2;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgnr)) {
            return false;
        }
        final zzgnr zzgnr = (zzgnr)o;
        if (this.zzb == null) {
            if (zzgnr.zzb != null) {
                return false;
            }
        }
        else if (!this.zzb.equals(zzgnr.zzb)) {
            return false;
        }
        if (this.zzc == null) {
            if (zzgnr.zzc != null) {
                return false;
            }
        }
        else if (!this.zzc.equals(zzgnr.zzc)) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgnr.zzax);
        }
        return zzgnr.zzax == null || zzgnr.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final String zzb = this.zzb;
        int hashCode2 = 0;
        int hashCode3;
        if (zzb == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = this.zzb.hashCode();
        }
        int hashCode4;
        if (this.zzc == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = this.zzc.hashCode();
        }
        if (this.zzax != null) {
            if (!this.zzax.zzb()) {
                hashCode2 = this.zzax.hashCode();
            }
        }
        return (((527 + hashCode) * 31 + hashCode3) * 31 + hashCode4) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zzb != null && !this.zzb.equals("")) {
            zzgmm.zza(1, this.zzb);
        }
        if (this.zzc != null && !this.zzc.equals("")) {
            zzgmm.zza(2, this.zzc);
        }
        super.writeTo(zzgmm);
    }
}
