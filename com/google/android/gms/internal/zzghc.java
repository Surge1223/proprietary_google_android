package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzghc<MessageType extends zzghc<MessageType, BuilderType>, BuilderType extends zzghd<MessageType, BuilderType>> implements zzgkg
{
    private static boolean zzb;
    protected int zza;
    
    static {
        zzghc.zzb = false;
    }
    
    public zzghc() {
        this.zza = 0;
    }
    
    @Override
    public final zzgho zzj() {
        try {
            final zzght zzb = zzgho.zzb(this.zza());
            this.zza(zzb.zzb());
            return zzb.zza();
        }
        catch (IOException ex) {
            final String name = this.getClass().getName();
            final StringBuilder sb = new StringBuilder(62 + String.valueOf(name).length() + String.valueOf("ByteString").length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), ex);
        }
    }
}
