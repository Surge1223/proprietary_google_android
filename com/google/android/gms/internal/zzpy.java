package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

public final class zzpy extends zzph
{
    private final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener zza;
    
    public zzpy(final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzot zzot) {
        this.zza.onCustomTemplateAdLoaded(zzow.zza(zzot));
    }
}
