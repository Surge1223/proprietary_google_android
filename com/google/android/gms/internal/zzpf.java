package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzpf extends zzey implements zzpd
{
    zzpf(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IOnCustomClickListener");
    }
    
    @Override
    public final void zza(final zzot zzot, final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzot);
        a_.writeString(s);
        this.zzb(1, a_);
    }
}
