package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeContentAd;

public final class zzpw extends zzpb
{
    private final NativeContentAd.OnContentAdLoadedListener zza;
    
    public zzpw(final NativeContentAd.OnContentAdLoadedListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzop zzop) {
        this.zza.onContentAdLoaded(new zzos(zzop));
    }
}
