package com.google.android.gms.internal;

import java.util.HashMap;
import java.net.HttpURLConnection;
import com.google.android.gms.common.util.zzc;
import java.io.Writer;
import java.io.StringWriter;
import java.util.Iterator;
import java.io.IOException;
import java.util.UUID;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.google.android.gms.common.util.zzh;
import java.util.List;
import java.util.Set;
import com.google.android.gms.common.util.Clock;
import android.util.JsonWriter;
import java.util.Map;

final class zzahy implements zzaic
{
    private final String zza;
    private final String zzb;
    private final Map zzc;
    private final byte[] zzd;
    
    zzahy(final String zza, final String zzb, final Map zzc, final byte[] zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final void zza(final JsonWriter jsonWriter) {
        zzahx.zza(this.zza, this.zzb, this.zzc, this.zzd, jsonWriter);
    }
}
