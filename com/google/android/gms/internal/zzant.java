package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;

public interface zzant extends MediationBannerAdapter
{
    Bundle zza();
}
