package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.zzp;

public final class zzpu extends zzp<zzoj>
{
    public zzpu() {
        super("com.google.android.gms.ads.NativeAdViewHolderDelegateCreatorImpl");
    }
}
