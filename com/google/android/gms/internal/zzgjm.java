package com.google.android.gms.internal;

import java.util.Map;
import java.util.Iterator;

final class zzgjm<K> implements Iterator<Map.Entry<K, Object>>
{
    private Iterator<Map.Entry<K, Object>> zza;
    
    public zzgjm(final Iterator<Map.Entry<K, Object>> zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean hasNext() {
        return this.zza.hasNext();
    }
    
    @Override
    public final void remove() {
        this.zza.remove();
    }
}
