package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IBinder;

public final class zzace extends zzey implements zzacc
{
    zzace(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }
    
    @Override
    public final void zza() throws RemoteException {
        this.zzb(2, this.a_());
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(9, a_);
    }
    
    @Override
    public final void zza(final zzach zzach) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzach);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zza(final zzacn zzacn) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzacn);
        this.zzb(1, a_);
    }
    
    @Override
    public final void zza(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        this.zzb(13, a_);
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, b);
        this.zzb(34, a_);
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(10, a_);
    }
    
    @Override
    public final boolean zzb() throws RemoteException {
        final Parcel zza = this.zza(5, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zzc() throws RemoteException {
        this.zzb(6, this.a_());
    }
    
    @Override
    public final void zzc(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(11, a_);
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(7, this.a_());
    }
    
    @Override
    public final void zze() throws RemoteException {
        this.zzb(8, this.a_());
    }
    
    @Override
    public final String zzf() throws RemoteException {
        final Parcel zza = this.zza(12, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
}
