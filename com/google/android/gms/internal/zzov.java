package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbi;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzov extends zzey implements zzot
{
    zzov(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
    }
    
    @Override
    public final String zza(String string) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(string);
        final Parcel zza = this.zza(1, a_);
        string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final List<String> zza() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final ArrayList stringArrayList = zza.createStringArrayList();
        zza.recycle();
        return (List<String>)stringArrayList;
    }
    
    @Override
    public final boolean zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        final Parcel zza = this.zza(10, a_);
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzb() throws RemoteException {
        final Parcel zza = this.zza(11, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zznx zzb(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        final Parcel zza = this.zza(2, a_);
        final IBinder strongBinder = zza.readStrongBinder();
        zznx zznx;
        if (strongBinder == null) {
            zznx = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
            if (queryLocalInterface instanceof zznx) {
                zznx = (zznx)queryLocalInterface;
            }
            else {
                zznx = new zznz(strongBinder);
            }
        }
        zza.recycle();
        return zznx;
    }
    
    @Override
    public final zzbh zzc() throws RemoteException {
        final Parcel zza = this.zza(7, this.a_());
        final zzbh zza2 = zzbi.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zzc(final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        a_.writeString(s);
        this.zzb(5, a_);
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(6, this.a_());
    }
    
    @Override
    public final IObjectWrapper zze() throws RemoteException {
        final Parcel zza = this.zza(9, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zzf() throws RemoteException {
        this.zzb(8, this.a_());
    }
    
    @Override
    public final String zzl() throws RemoteException {
        final Parcel zza = this.zza(4, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
}
