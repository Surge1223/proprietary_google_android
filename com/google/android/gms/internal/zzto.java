package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IBinder;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzbh;
import com.google.android.gms.ads.internal.client.zzj;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.client.zzf;
import java.util.List;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;

public abstract class zzto extends zzez implements zztn
{
    public zzto() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        final zztq zztq = null;
        final zztq zztq2 = null;
        final zztq zztq3 = null;
        final zztq zztq4 = null;
        final zztq zztq5 = null;
        switch (n) {
            default: {
                return false;
            }
            case 27: {
                final zzuc zzp = this.zzp();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzp);
                break;
            }
            case 26: {
                final zzbh zzo = this.zzo();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzo);
                break;
            }
            case 25: {
                this.zza(zzfa.zza(parcel));
                parcel2.writeNoException();
                break;
            }
            case 24: {
                final zzot zzn = this.zzn();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzn);
                break;
            }
            case 23: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zzadf.zza(parcel.readStrongBinder()), parcel.createStringArrayList());
                parcel2.writeNoException();
                break;
            }
            case 22: {
                final boolean zzm = this.zzm();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzm);
                break;
            }
            case 21: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 20: {
                this.zza(zzfa.zza(parcel, zzf.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            }
            case 19: {
                final Bundle zzl = this.zzl();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzl);
                break;
            }
            case 18: {
                final Bundle zzk = this.zzk();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzk);
                break;
            }
            case 17: {
                final Bundle zzj = this.zzj();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzj);
                break;
            }
            case 16: {
                final zztz zzi = this.zzi();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzi);
                break;
            }
            case 15: {
                final zztw zzh = this.zzh();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzh);
                break;
            }
            case 14: {
                final IObjectWrapper zza = IObjectWrapper.zza.zza(parcel.readStrongBinder());
                final zzf zzf = zzfa.zza(parcel, com.google.android.gms.ads.internal.client.zzf.CREATOR);
                final String string = parcel.readString();
                final String string2 = parcel.readString();
                final IBinder strongBinder = parcel.readStrongBinder();
                zztq zztq6;
                if (strongBinder == null) {
                    zztq6 = zztq5;
                }
                else {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    if (queryLocalInterface instanceof zztq) {
                        zztq6 = (zztq)queryLocalInterface;
                    }
                    else {
                        zztq6 = new zzts(strongBinder);
                    }
                }
                this.zza(zza, zzf, string, string2, zztq6, zzfa.zza(parcel, zznm.CREATOR), parcel.createStringArrayList());
                parcel2.writeNoException();
                break;
            }
            case 13: {
                final boolean zzg = this.zzg();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzg);
                break;
            }
            case 12: {
                this.zzf();
                parcel2.writeNoException();
                break;
            }
            case 11: {
                this.zza(zzfa.zza(parcel, zzf.CREATOR), parcel.readString());
                parcel2.writeNoException();
                break;
            }
            case 10: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), zzfa.zza(parcel, zzf.CREATOR), parcel.readString(), zzadf.zza(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            }
            case 9: {
                this.zze();
                parcel2.writeNoException();
                break;
            }
            case 8: {
                this.zzd();
                parcel2.writeNoException();
                break;
            }
            case 7: {
                final IObjectWrapper zza2 = IObjectWrapper.zza.zza(parcel.readStrongBinder());
                final zzf zzf2 = zzfa.zza(parcel, zzf.CREATOR);
                final String string3 = parcel.readString();
                final String string4 = parcel.readString();
                final IBinder strongBinder2 = parcel.readStrongBinder();
                zztq zztq7;
                if (strongBinder2 == null) {
                    zztq7 = zztq;
                }
                else {
                    final IInterface queryLocalInterface2 = strongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    if (queryLocalInterface2 instanceof zztq) {
                        zztq7 = (zztq)queryLocalInterface2;
                    }
                    else {
                        zztq7 = new zzts(strongBinder2);
                    }
                }
                this.zza(zza2, zzf2, string3, string4, zztq7);
                parcel2.writeNoException();
                break;
            }
            case 6: {
                final IObjectWrapper zza3 = IObjectWrapper.zza.zza(parcel.readStrongBinder());
                final zzj zzj2 = zzfa.zza(parcel, zzj.CREATOR);
                final zzf zzf3 = zzfa.zza(parcel, zzf.CREATOR);
                final String string5 = parcel.readString();
                final String string6 = parcel.readString();
                final IBinder strongBinder3 = parcel.readStrongBinder();
                zztq zztq8;
                if (strongBinder3 == null) {
                    zztq8 = zztq2;
                }
                else {
                    final IInterface queryLocalInterface3 = strongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    if (queryLocalInterface3 instanceof zztq) {
                        zztq8 = (zztq)queryLocalInterface3;
                    }
                    else {
                        zztq8 = new zzts(strongBinder3);
                    }
                }
                this.zza(zza3, zzj2, zzf3, string5, string6, zztq8);
                parcel2.writeNoException();
                break;
            }
            case 5: {
                this.zzc();
                parcel2.writeNoException();
                break;
            }
            case 4: {
                this.zzb();
                parcel2.writeNoException();
                break;
            }
            case 3: {
                final IObjectWrapper zza4 = IObjectWrapper.zza.zza(parcel.readStrongBinder());
                final zzf zzf4 = zzfa.zza(parcel, zzf.CREATOR);
                final String string7 = parcel.readString();
                final IBinder strongBinder4 = parcel.readStrongBinder();
                zztq zztq9;
                if (strongBinder4 == null) {
                    zztq9 = zztq3;
                }
                else {
                    final IInterface queryLocalInterface4 = strongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    if (queryLocalInterface4 instanceof zztq) {
                        zztq9 = (zztq)queryLocalInterface4;
                    }
                    else {
                        zztq9 = new zzts(strongBinder4);
                    }
                }
                this.zza(zza4, zzf4, string7, zztq9);
                parcel2.writeNoException();
                break;
            }
            case 2: {
                final IObjectWrapper zza5 = this.zza();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zza5);
                break;
            }
            case 1: {
                final IObjectWrapper zza6 = IObjectWrapper.zza.zza(parcel.readStrongBinder());
                final zzj zzj3 = zzfa.zza(parcel, zzj.CREATOR);
                final zzf zzf5 = zzfa.zza(parcel, zzf.CREATOR);
                final String string8 = parcel.readString();
                final IBinder strongBinder5 = parcel.readStrongBinder();
                zztq zztq10;
                if (strongBinder5 == null) {
                    zztq10 = zztq4;
                }
                else {
                    final IInterface queryLocalInterface5 = strongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    if (queryLocalInterface5 instanceof zztq) {
                        zztq10 = (zztq)queryLocalInterface5;
                    }
                    else {
                        zztq10 = new zzts(strongBinder5);
                    }
                }
                this.zza(zza6, zzj3, zzf5, string8, zztq10);
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
