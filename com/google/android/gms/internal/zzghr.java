package com.google.android.gms.internal;

final class zzghr extends zzghv
{
    private final int zzc;
    private final int zzd;
    
    zzghr(final byte[] array, final int zzc, final int zzd) {
        super(array);
        zzgho.zzb(zzc, zzc + zzd, array.length);
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final byte zza(final int n) {
        final int zza = this.zza();
        if ((zza - (n + 1) | n) >= 0) {
            return this.zzb[this.zzc + n];
        }
        if (n < 0) {
            final StringBuilder sb = new StringBuilder(22);
            sb.append("Index < 0: ");
            sb.append(n);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder(40);
        sb2.append("Index > length: ");
        sb2.append(n);
        sb2.append(", ");
        sb2.append(zza);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }
    
    @Override
    public final int zza() {
        return this.zzd;
    }
    
    @Override
    protected final int zzh() {
        return this.zzc;
    }
}
