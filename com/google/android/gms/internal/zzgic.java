package com.google.android.gms.internal;

import java.nio.BufferOverflowException;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public abstract class zzgic extends zzghn
{
    private static final Logger zzb;
    private static final boolean zzc;
    zzgie zza;
    
    static {
        zzb = Logger.getLogger(zzgic.class.getName());
        zzc = zzglw.zza();
    }
    
    public static int zza(int zzb, final zzgjn zzgjn) {
        final int zze = zze(zzb);
        zzb = zzgjn.zzb();
        return zze + (zzg(zzb) + zzb);
    }
    
    public static int zza(final zzgjn zzgjn) {
        final int zzb = zzgjn.zzb();
        return zzg(zzb) + zzb;
    }
    
    public static zzgic zza(final ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            return new zzb(byteBuffer);
        }
        if (!byteBuffer.isDirect() || byteBuffer.isReadOnly()) {
            throw new IllegalArgumentException("ByteBuffer is read-only");
        }
        if (zzglw.zzb()) {
            return new zze(byteBuffer);
        }
        return new zzd(byteBuffer);
    }
    
    public static zzgic zza(final byte[] array) {
        return new zza(array, 0, array.length);
    }
    
    public static int zzb(final double n) {
        return 8;
    }
    
    public static int zzb(final float n) {
        return 4;
    }
    
    public static int zzb(final int n, final double n2) {
        return zze(n) + 8;
    }
    
    public static int zzb(final int n, final float n2) {
        return zze(n) + 4;
    }
    
    public static int zzb(final int n, final zzgjn zzgjn) {
        return (zze(1) << 1) + zzg(2, n) + zza(3, zzgjn);
    }
    
    public static int zzb(final int n, final String s) {
        return zze(n) + zzb(s);
    }
    
    public static int zzb(final int n, final boolean b) {
        return zze(n) + 1;
    }
    
    public static int zzb(final zzgho zzgho) {
        final int zza = zzgho.zza();
        return zzg(zza) + zza;
    }
    
    public static int zzb(final zzgkg zzgkg) {
        final int zza = zzgkg.zza();
        return zzg(zza) + zza;
    }
    
    public static int zzb(final String s) {
        int n;
        try {
            n = zzgly.zza(s);
        }
        catch (zzgmb zzgmb) {
            n = s.getBytes(zzgja.zza).length;
        }
        return zzg(n) + n;
    }
    
    public static int zzb(final boolean b) {
        return 1;
    }
    
    public static int zzb(final byte[] array) {
        final int length = array.length;
        return zzg(length) + length;
    }
    
    public static int zzc(int zza, final zzgho zzgho) {
        final int zze = zze(zza);
        zza = zzgho.zza();
        return zze + (zzg(zza) + zza);
    }
    
    public static int zzc(final int n, final zzgkg zzgkg) {
        return zze(n) + zzb(zzgkg);
    }
    
    @Deprecated
    public static int zzc(final zzgkg zzgkg) {
        return zzgkg.zza();
    }
    
    public static int zzd(final int n, final long n2) {
        return zze(n) + zze(n2);
    }
    
    public static int zzd(final int n, final zzgho zzgho) {
        return (zze(1) << 1) + zzg(2, n) + zzc(3, zzgho);
    }
    
    public static int zzd(final int n, final zzgkg zzgkg) {
        return (zze(1) << 1) + zzg(2, n) + zzc(3, zzgkg);
    }
    
    public static int zzd(final long n) {
        return zze(n);
    }
    
    public static int zze(final int n) {
        return zzg(n << 3);
    }
    
    public static int zze(final int n, final long n2) {
        return zze(n) + zze(n2);
    }
    
    public static int zze(long n) {
        if ((0xFFFFFFFFFFFFFF80L & n) == 0x0L) {
            return 1;
        }
        if (n < 0L) {
            return 10;
        }
        int n2;
        if ((0xFFFFFFF800000000L & n) != 0x0L) {
            n2 = 6;
            n >>>= 28;
        }
        else {
            n2 = 2;
        }
        int n3 = n2;
        long n4 = n;
        if ((0xFFFFFFFFFFE00000L & n) != 0x0L) {
            n3 = n2 + 2;
            n4 = n >>> 14;
        }
        int n5 = n3;
        if ((n4 & 0xFFFFFFFFFFFFC000L) != 0x0L) {
            n5 = n3 + 1;
        }
        return n5;
    }
    
    public static int zzf(final int n) {
        if (n >= 0) {
            return zzg(n);
        }
        return 10;
    }
    
    public static int zzf(final int n, final int n2) {
        return zze(n) + zzf(n2);
    }
    
    public static int zzf(final int n, final long n2) {
        return zze(n) + zze(zzi(n2));
    }
    
    @Deprecated
    public static int zzf(final int n, final zzgkg zzgkg) {
        return (zze(n) << 1) + zzgkg.zza();
    }
    
    public static int zzf(final long n) {
        return zze(zzi(n));
    }
    
    public static int zzg(final int n) {
        if ((n & 0xFFFFFF80) == 0x0) {
            return 1;
        }
        if ((n & 0xFFFFC000) == 0x0) {
            return 2;
        }
        if ((0xFFE00000 & n) == 0x0) {
            return 3;
        }
        if ((n & 0xF0000000) == 0x0) {
            return 4;
        }
        return 5;
    }
    
    public static int zzg(final int n, final int n2) {
        return zze(n) + zzg(n2);
    }
    
    public static int zzg(final int n, final long n2) {
        return zze(n) + 8;
    }
    
    public static int zzg(final long n) {
        return 8;
    }
    
    public static int zzh(final int n) {
        return zzg(zzn(n));
    }
    
    public static int zzh(final int n, final int n2) {
        return zze(n) + zzg(zzn(n2));
    }
    
    public static int zzh(final int n, final long n2) {
        return zze(n) + 8;
    }
    
    public static int zzh(final long n) {
        return 8;
    }
    
    public static int zzi(final int n) {
        return 4;
    }
    
    public static int zzi(final int n, final int n2) {
        return zze(n) + 4;
    }
    
    private static long zzi(final long n) {
        return n >> 63 ^ n << 1;
    }
    
    public static int zzj(final int n) {
        return 4;
    }
    
    public static int zzj(final int n, final int n2) {
        return zze(n) + 4;
    }
    
    public static int zzk(final int n) {
        return zzf(n);
    }
    
    public static int zzk(final int n, final int n2) {
        return zze(n) + zzf(n2);
    }
    
    static int zzl(final int n) {
        return zzg(n) + n;
    }
    
    @Deprecated
    public static int zzm(final int n) {
        return zzg(n);
    }
    
    private static int zzn(final int n) {
        return n >> 31 ^ n << 1;
    }
    
    public abstract void zza() throws IOException;
    
    public abstract void zza(final byte p0) throws IOException;
    
    public final void zza(final double n) throws IOException {
        this.zzc(Double.doubleToRawLongBits(n));
    }
    
    public final void zza(final float n) throws IOException {
        this.zzd(Float.floatToRawIntBits(n));
    }
    
    public abstract void zza(final int p0) throws IOException;
    
    public final void zza(final int n, final double n2) throws IOException {
        this.zzc(n, Double.doubleToRawLongBits(n2));
    }
    
    public final void zza(final int n, final float n2) throws IOException {
        this.zze(n, Float.floatToRawIntBits(n2));
    }
    
    public abstract void zza(final int p0, final int p1) throws IOException;
    
    public abstract void zza(final int p0, final long p1) throws IOException;
    
    public abstract void zza(final int p0, final zzgho p1) throws IOException;
    
    public abstract void zza(final int p0, final zzgkg p1) throws IOException;
    
    public abstract void zza(final int p0, final String p1) throws IOException;
    
    public abstract void zza(final int p0, final boolean p1) throws IOException;
    
    public abstract void zza(final long p0) throws IOException;
    
    public abstract void zza(final zzgho p0) throws IOException;
    
    public abstract void zza(final zzgkg p0) throws IOException;
    
    public abstract void zza(final String p0) throws IOException;
    
    final void zza(final String s, final zzgmb zzgmb) throws IOException {
        zzgic.zzb.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", zzgmb);
        final byte[] bytes = s.getBytes(zzgja.zza);
        try {
            this.zzb(bytes.length);
            this.zza(bytes, 0, bytes.length);
        }
        catch (zzc zzc) {
            throw zzc;
        }
        catch (IndexOutOfBoundsException ex) {
            throw new zzc(ex);
        }
    }
    
    public final void zza(final boolean b) throws IOException {
        this.zza((byte)(b ? 1 : 0));
    }
    
    public abstract int zzb();
    
    public abstract void zzb(final int p0) throws IOException;
    
    public abstract void zzb(final int p0, final int p1) throws IOException;
    
    public final void zzb(final int n, final long n2) throws IOException {
        this.zza(n, zzi(n2));
    }
    
    public abstract void zzb(final int p0, final zzgho p1) throws IOException;
    
    public abstract void zzb(final int p0, final zzgkg p1) throws IOException;
    
    public final void zzb(final long n) throws IOException {
        this.zza(zzi(n));
    }
    
    public abstract void zzb(final byte[] p0, final int p1, final int p2) throws IOException;
    
    public final void zzc() {
        if (this.zzb() == 0) {
            return;
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }
    
    public final void zzc(final int n) throws IOException {
        this.zzb(zzn(n));
    }
    
    public abstract void zzc(final int p0, final int p1) throws IOException;
    
    public abstract void zzc(final int p0, final long p1) throws IOException;
    
    public abstract void zzc(final long p0) throws IOException;
    
    abstract void zzc(final byte[] p0, final int p1, final int p2) throws IOException;
    
    public abstract void zzd(final int p0) throws IOException;
    
    public final void zzd(final int n, final int n2) throws IOException {
        this.zzc(n, zzn(n2));
    }
    
    public abstract void zze(final int p0, final int p1) throws IOException;
    
    @Deprecated
    public final void zze(final int n, final zzgkg zzgkg) throws IOException {
        this.zza(n, 3);
        zzgkg.zza(this);
        this.zza(n, 4);
    }
    
    static class zza extends zzgic
    {
        private final byte[] zzb;
        private final int zzc;
        private final int zzd;
        private int zze;
        
        zza(final byte[] zzb, final int n, final int n2) {
            super(null);
            if (zzb == null) {
                throw new NullPointerException("buffer");
            }
            final int length = zzb.length;
            final int zzd = n + n2;
            if ((n | n2 | length - zzd) >= 0) {
                this.zzb = zzb;
                this.zzc = n;
                this.zze = n;
                this.zzd = zzd;
                return;
            }
            throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", zzb.length, n, n2));
        }
        
        @Override
        public void zza() {
        }
        
        @Override
        public final void zza(final byte b) throws IOException {
            try {
                this.zzb[this.zze++] = b;
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zze, this.zzd, 1), ex);
            }
        }
        
        @Override
        public final void zza(final int n) throws IOException {
            if (n >= 0) {
                this.zzb(n);
                return;
            }
            this.zza((long)n);
        }
        
        @Override
        public final void zza(final int n, final int n2) throws IOException {
            this.zzb(n << 3 | n2);
        }
        
        @Override
        public final void zza(final int n, final long n2) throws IOException {
            this.zza(n, 0);
            this.zza(n2);
        }
        
        @Override
        public final void zza(final int n, final zzgho zzgho) throws IOException {
            this.zza(n, 2);
            this.zza(zzgho);
        }
        
        @Override
        public final void zza(final int n, final zzgkg zzgkg) throws IOException {
            this.zza(n, 2);
            this.zza(zzgkg);
        }
        
        @Override
        public final void zza(final int n, final String s) throws IOException {
            this.zza(n, 2);
            this.zza(s);
        }
        
        @Override
        public final void zza(final int n, final boolean b) throws IOException {
            this.zza(n, 0);
            this.zza((byte)(b ? 1 : 0));
        }
        
        @Override
        public final void zza(long n) throws IOException {
            long n2 = n;
            if (zzgic.zzc) {
                n2 = n;
                if (this.zzb() >= 10) {
                    while ((n & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                        zzglw.zza(this.zzb, this.zze++, (byte)(((int)n & 0x7F) | 0x80));
                        n >>>= 7;
                    }
                    zzglw.zza(this.zzb, this.zze++, (byte)n);
                    return;
                }
            }
            while (true) {
                if ((n2 & 0xFFFFFFFFFFFFFF80L) == 0x0L) {
                    try {
                        this.zzb[this.zze++] = (byte)n2;
                        return;
                    }
                    catch (IndexOutOfBoundsException ex) {
                        break;
                    }
                }
                this.zzb[this.zze++] = (byte)(((int)n2 & 0x7F) | 0x80);
                n2 >>>= 7;
            }
            final IndexOutOfBoundsException ex;
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zze, this.zzd, 1), ex);
        }
        
        @Override
        public final void zza(final zzgho zzgho) throws IOException {
            this.zzb(zzgho.zza());
            zzgho.zza(this);
        }
        
        @Override
        public final void zza(final zzgkg zzgkg) throws IOException {
            this.zzb(zzgkg.zza());
            zzgkg.zza(this);
        }
        
        @Override
        public final void zza(final String s) throws IOException {
            final int zze = this.zze;
            try {
                final int zzg = zzgic.zzg(s.length() * 3);
                final int zzg2 = zzgic.zzg(s.length());
                if (zzg2 == zzg) {
                    this.zze = zze + zzg2;
                    final int zza = zzgly.zza(s, this.zzb, this.zze, this.zzb());
                    this.zzb(zza - (this.zze = zze) - zzg2);
                    this.zze = zza;
                    return;
                }
                this.zzb(zzgly.zza(s));
                this.zze = zzgly.zza(s, this.zzb, this.zze, this.zzb());
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(ex);
            }
            catch (zzgmb zzgmb) {
                this.zze = zze;
                this.zza(s, zzgmb);
            }
        }
        
        @Override
        public final void zza(final byte[] array, final int n, final int n2) throws IOException {
            this.zzb(array, n, n2);
        }
        
        @Override
        public final int zzb() {
            return this.zzd - this.zze;
        }
        
        @Override
        public final void zzb(int n) throws IOException {
            int n2 = n;
            if (zzgic.zzc) {
                n2 = n;
                if (this.zzb() >= 10) {
                    while ((n & 0xFFFFFF80) != 0x0) {
                        zzglw.zza(this.zzb, this.zze++, (byte)((n & 0x7F) | 0x80));
                        n >>>= 7;
                    }
                    zzglw.zza(this.zzb, this.zze++, (byte)n);
                    return;
                }
            }
            while (true) {
                if ((n2 & 0xFFFFFF80) == 0x0) {
                    try {
                        final byte[] zzb = this.zzb;
                        n = this.zze++;
                        zzb[n] = (byte)n2;
                        return;
                    }
                    catch (IndexOutOfBoundsException ex) {
                        break;
                    }
                }
                final byte[] zzb2 = this.zzb;
                n = this.zze++;
                zzb2[n] = (byte)((n2 & 0x7F) | 0x80);
                n2 >>>= 7;
            }
            final IndexOutOfBoundsException ex;
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zze, this.zzd, 1), ex);
        }
        
        @Override
        public final void zzb(final int n, final int n2) throws IOException {
            this.zza(n, 0);
            this.zza(n2);
        }
        
        @Override
        public final void zzb(final int n, final zzgho zzgho) throws IOException {
            this.zza(1, 3);
            this.zzc(2, n);
            this.zza(3, zzgho);
            this.zza(1, 4);
        }
        
        @Override
        public final void zzb(final int n, final zzgkg zzgkg) throws IOException {
            this.zza(1, 3);
            this.zzc(2, n);
            this.zza(3, zzgkg);
            this.zza(1, 4);
        }
        
        @Override
        public final void zzb(final byte[] array, final int n, final int n2) throws IOException {
            try {
                System.arraycopy(array, n, this.zzb, this.zze, n2);
                this.zze += n2;
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zze, this.zzd, n2), ex);
            }
        }
        
        @Override
        public final void zzc(final int n, final int n2) throws IOException {
            this.zza(n, 0);
            this.zzb(n2);
        }
        
        @Override
        public final void zzc(final int n, final long n2) throws IOException {
            this.zza(n, 1);
            this.zzc(n2);
        }
        
        @Override
        public final void zzc(final long n) throws IOException {
            try {
                this.zzb[this.zze++] = (byte)n;
                this.zzb[this.zze++] = (byte)(n >> 8);
                this.zzb[this.zze++] = (byte)(n >> 16);
                this.zzb[this.zze++] = (byte)(n >> 24);
                this.zzb[this.zze++] = (byte)(n >> 32);
                this.zzb[this.zze++] = (byte)(n >> 40);
                this.zzb[this.zze++] = (byte)(n >> 48);
                this.zzb[this.zze++] = (byte)(n >> 56);
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zze, this.zzd, 1), ex);
            }
        }
        
        public final void zzc(final byte[] array, final int n, final int n2) throws IOException {
            this.zzb(n2);
            this.zzb(array, 0, n2);
        }
        
        @Override
        public final void zzd(final int n) throws IOException {
            try {
                this.zzb[this.zze++] = (byte)n;
                this.zzb[this.zze++] = (byte)(n >> 8);
                this.zzb[this.zze++] = (byte)(n >> 16);
                this.zzb[this.zze++] = (byte)(n >> 24);
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zze, this.zzd, 1), ex);
            }
        }
        
        public final int zze() {
            return this.zze - this.zzc;
        }
        
        @Override
        public final void zze(final int n, final int n2) throws IOException {
            this.zza(n, 5);
            this.zzd(n2);
        }
    }
    
    static final class zzb extends zza
    {
        private final ByteBuffer zzb;
        private int zzc;
        
        zzb(final ByteBuffer zzb) {
            super(zzb.array(), zzb.arrayOffset() + zzb.position(), zzb.remaining());
            this.zzb = zzb;
            this.zzc = zzb.position();
        }
        
        @Override
        public final void zza() {
            this.zzb.position(this.zzc + ((zza)this).zze());
        }
    }
    
    public static final class zzc extends IOException
    {
        zzc() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
        
        zzc(String s) {
            final String value = String.valueOf("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            s = String.valueOf(s);
            if (s.length() != 0) {
                s = value.concat(s);
            }
            else {
                s = new String(value);
            }
            super(s);
        }
        
        zzc(String s, final Throwable t) {
            final String value = String.valueOf("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            s = String.valueOf(s);
            if (s.length() != 0) {
                s = value.concat(s);
            }
            else {
                s = new String(value);
            }
            super(s, t);
        }
        
        zzc(final Throwable t) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", t);
        }
    }
    
    static final class zzd extends zzgic
    {
        private final ByteBuffer zzb;
        private final ByteBuffer zzc;
        private final int zzd;
        
        zzd(final ByteBuffer zzb) {
            super(null);
            this.zzb = zzb;
            this.zzc = zzb.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            this.zzd = zzb.position();
        }
        
        private final void zzc(final String s) throws IOException {
            try {
                zzgly.zza(s, this.zzc);
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(ex);
            }
        }
        
        @Override
        public final void zza() {
            this.zzb.position(this.zzc.position());
        }
        
        @Override
        public final void zza(final byte b) throws IOException {
            try {
                this.zzc.put(b);
            }
            catch (BufferOverflowException ex) {
                throw new zzc(ex);
            }
        }
        
        @Override
        public final void zza(final int n) throws IOException {
            if (n >= 0) {
                this.zzb(n);
                return;
            }
            this.zza((long)n);
        }
        
        @Override
        public final void zza(final int n, final int n2) throws IOException {
            this.zzb(n << 3 | n2);
        }
        
        @Override
        public final void zza(final int n, final long n2) throws IOException {
            this.zza(n, 0);
            this.zza(n2);
        }
        
        @Override
        public final void zza(final int n, final zzgho zzgho) throws IOException {
            this.zza(n, 2);
            this.zza(zzgho);
        }
        
        @Override
        public final void zza(final int n, final zzgkg zzgkg) throws IOException {
            this.zza(n, 2);
            this.zza(zzgkg);
        }
        
        @Override
        public final void zza(final int n, final String s) throws IOException {
            this.zza(n, 2);
            this.zza(s);
        }
        
        @Override
        public final void zza(final int n, final boolean b) throws IOException {
            this.zza(n, 0);
            this.zza((byte)(b ? 1 : 0));
        }
        
        @Override
        public final void zza(long n) throws IOException {
            while (true) {
                if ((0xFFFFFFFFFFFFFF80L & n) == 0x0L) {
                    try {
                        this.zzc.put((byte)n);
                        return;
                    }
                    catch (BufferOverflowException ex) {
                        break;
                    }
                }
                this.zzc.put((byte)(((int)n & 0x7F) | 0x80));
                n >>>= 7;
            }
            final BufferOverflowException ex;
            throw new zzc(ex);
        }
        
        @Override
        public final void zza(final zzgho zzgho) throws IOException {
            this.zzb(zzgho.zza());
            zzgho.zza(this);
        }
        
        @Override
        public final void zza(final zzgkg zzgkg) throws IOException {
            this.zzb(zzgkg.zza());
            zzgkg.zza(this);
        }
        
        @Override
        public final void zza(final String s) throws IOException {
            final int position = this.zzc.position();
            try {
                final int zzg = zzgic.zzg(s.length() * 3);
                final int zzg2 = zzgic.zzg(s.length());
                if (zzg2 == zzg) {
                    final int n = this.zzc.position() + zzg2;
                    this.zzc.position(n);
                    this.zzc(s);
                    final int position2 = this.zzc.position();
                    this.zzc.position(position);
                    this.zzb(position2 - n);
                    this.zzc.position(position2);
                    return;
                }
                this.zzb(zzgly.zza(s));
                this.zzc(s);
            }
            catch (IllegalArgumentException ex) {
                throw new zzc(ex);
            }
            catch (zzgmb zzgmb) {
                this.zzc.position(position);
                this.zza(s, zzgmb);
            }
        }
        
        @Override
        public final void zza(final byte[] array, final int n, final int n2) throws IOException {
            this.zzb(array, n, n2);
        }
        
        @Override
        public final int zzb() {
            return this.zzc.remaining();
        }
        
        @Override
        public final void zzb(int n) throws IOException {
            while (true) {
                if ((n & 0xFFFFFF80) == 0x0) {
                    try {
                        this.zzc.put((byte)n);
                        return;
                    }
                    catch (BufferOverflowException ex) {
                        break;
                    }
                }
                this.zzc.put((byte)((n & 0x7F) | 0x80));
                n >>>= 7;
            }
            final BufferOverflowException ex;
            throw new zzc(ex);
        }
        
        @Override
        public final void zzb(final int n, final int n2) throws IOException {
            this.zza(n, 0);
            this.zza(n2);
        }
        
        @Override
        public final void zzb(final int n, final zzgho zzgho) throws IOException {
            this.zza(1, 3);
            this.zzc(2, n);
            this.zza(3, zzgho);
            this.zza(1, 4);
        }
        
        @Override
        public final void zzb(final int n, final zzgkg zzgkg) throws IOException {
            this.zza(1, 3);
            this.zzc(2, n);
            this.zza(3, zzgkg);
            this.zza(1, 4);
        }
        
        @Override
        public final void zzb(final byte[] array, final int n, final int n2) throws IOException {
            try {
                this.zzc.put(array, n, n2);
            }
            catch (BufferOverflowException ex) {
                throw new zzc(ex);
            }
            catch (IndexOutOfBoundsException ex2) {
                throw new zzc(ex2);
            }
        }
        
        @Override
        public final void zzc(final int n, final int n2) throws IOException {
            this.zza(n, 0);
            this.zzb(n2);
        }
        
        @Override
        public final void zzc(final int n, final long n2) throws IOException {
            this.zza(n, 1);
            this.zzc(n2);
        }
        
        @Override
        public final void zzc(final long n) throws IOException {
            try {
                this.zzc.putLong(n);
            }
            catch (BufferOverflowException ex) {
                throw new zzc(ex);
            }
        }
        
        public final void zzc(final byte[] array, final int n, final int n2) throws IOException {
            this.zzb(n2);
            this.zzb(array, 0, n2);
        }
        
        @Override
        public final void zzd(final int n) throws IOException {
            try {
                this.zzc.putInt(n);
            }
            catch (BufferOverflowException ex) {
                throw new zzc(ex);
            }
        }
        
        @Override
        public final void zze(final int n, final int n2) throws IOException {
            this.zza(n, 5);
            this.zzd(n2);
        }
    }
    
    static final class zze extends zzgic
    {
        private final ByteBuffer zzb;
        private final ByteBuffer zzc;
        private final long zzd;
        private final long zze;
        private final long zzf;
        private final long zzg;
        private long zzh;
        
        zze(final ByteBuffer zzb) {
            super(null);
            this.zzb = zzb;
            this.zzc = zzb.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            this.zzd = zzglw.zza(zzb);
            this.zze = this.zzd + zzb.position();
            this.zzf = this.zzd + zzb.limit();
            this.zzg = this.zzf - 10L;
            this.zzh = this.zze;
        }
        
        private final void zzi(final long n) {
            this.zzc.position((int)(n - this.zzd));
        }
        
        @Override
        public final void zza() {
            this.zzb.position((int)(this.zzh - this.zzd));
        }
        
        @Override
        public final void zza(final byte b) throws IOException {
            if (this.zzh < this.zzf) {
                final long zzh = this.zzh;
                this.zzh = 1L + zzh;
                zzglw.zza(zzh, b);
                return;
            }
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zzh, this.zzf, 1));
        }
        
        @Override
        public final void zza(final int n) throws IOException {
            if (n >= 0) {
                this.zzb(n);
                return;
            }
            this.zza((long)n);
        }
        
        @Override
        public final void zza(final int n, final int n2) throws IOException {
            this.zzb(n << 3 | n2);
        }
        
        @Override
        public final void zza(final int n, final long n2) throws IOException {
            this.zza(n, 0);
            this.zza(n2);
        }
        
        @Override
        public final void zza(final int n, final zzgho zzgho) throws IOException {
            this.zza(n, 2);
            this.zza(zzgho);
        }
        
        @Override
        public final void zza(final int n, final zzgkg zzgkg) throws IOException {
            this.zza(n, 2);
            this.zza(zzgkg);
        }
        
        @Override
        public final void zza(final int n, final String s) throws IOException {
            this.zza(n, 2);
            this.zza(s);
        }
        
        @Override
        public final void zza(final int n, final boolean b) throws IOException {
            this.zza(n, 0);
            this.zza((byte)(b ? 1 : 0));
        }
        
        @Override
        public final void zza(long zzh) throws IOException {
            long n = zzh;
            if (this.zzh <= this.zzg) {
                while ((zzh & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                    zzglw.zza(this.zzh++, (byte)(((int)zzh & 0x7F) | 0x80));
                    zzh >>>= 7;
                }
                final long zzh2 = this.zzh;
                this.zzh = 1L + zzh2;
                zzglw.zza(zzh2, (byte)zzh);
                return;
            }
            while (this.zzh < this.zzf) {
                if ((n & 0xFFFFFFFFFFFFFF80L) == 0x0L) {
                    zzh = this.zzh;
                    this.zzh = 1L + zzh;
                    zzglw.zza(zzh, (byte)n);
                    return;
                }
                zzh = this.zzh++;
                zzglw.zza(zzh, (byte)(((int)n & 0x7F) | 0x80));
                n >>>= 7;
            }
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zzh, this.zzf, 1));
        }
        
        @Override
        public final void zza(final zzgho zzgho) throws IOException {
            this.zzb(zzgho.zza());
            zzgho.zza(this);
        }
        
        @Override
        public final void zza(final zzgkg zzgkg) throws IOException {
            this.zzb(zzgkg.zza());
            zzgkg.zza(this);
        }
        
        @Override
        public final void zza(final String s) throws IOException {
            final long zzh = this.zzh;
            try {
                final int zzg = zzgic.zzg(s.length() * 3);
                final int zzg2 = zzgic.zzg(s.length());
                if (zzg2 == zzg) {
                    final int n = (int)(this.zzh - this.zzd) + zzg2;
                    this.zzc.position(n);
                    zzgly.zza(s, this.zzc);
                    final int n2 = this.zzc.position() - n;
                    this.zzb(n2);
                    this.zzh += n2;
                    return;
                }
                final int zza = zzgly.zza(s);
                this.zzb(zza);
                this.zzi(this.zzh);
                zzgly.zza(s, this.zzc);
                this.zzh += zza;
            }
            catch (IndexOutOfBoundsException ex) {
                throw new zzc(ex);
            }
            catch (IllegalArgumentException ex2) {
                throw new zzc(ex2);
            }
            catch (zzgmb zzgmb) {
                this.zzi(this.zzh = zzh);
                this.zza(s, zzgmb);
            }
        }
        
        @Override
        public final void zza(final byte[] array, final int n, final int n2) throws IOException {
            this.zzb(array, n, n2);
        }
        
        @Override
        public final int zzb() {
            return (int)(this.zzf - this.zzh);
        }
        
        @Override
        public final void zzb(int n) throws IOException {
            int n2 = n;
            if (this.zzh <= this.zzg) {
                while ((n & 0xFFFFFF80) != 0x0) {
                    zzglw.zza(this.zzh++, (byte)((n & 0x7F) | 0x80));
                    n >>>= 7;
                }
                final long zzh = this.zzh;
                this.zzh = 1L + zzh;
                zzglw.zza(zzh, (byte)n);
                return;
            }
            while (this.zzh < this.zzf) {
                if ((n2 & 0xFFFFFF80) == 0x0) {
                    final long zzh2 = this.zzh;
                    this.zzh = 1L + zzh2;
                    zzglw.zza(zzh2, (byte)n2);
                    return;
                }
                zzglw.zza(this.zzh++, (byte)((n2 & 0x7F) | 0x80));
                n2 >>>= 7;
            }
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zzh, this.zzf, 1));
        }
        
        @Override
        public final void zzb(final int n, final int n2) throws IOException {
            this.zza(n, 0);
            this.zza(n2);
        }
        
        @Override
        public final void zzb(final int n, final zzgho zzgho) throws IOException {
            this.zza(1, 3);
            this.zzc(2, n);
            this.zza(3, zzgho);
            this.zza(1, 4);
        }
        
        @Override
        public final void zzb(final int n, final zzgkg zzgkg) throws IOException {
            this.zza(1, 3);
            this.zzc(2, n);
            this.zza(3, zzgkg);
            this.zza(1, 4);
        }
        
        @Override
        public final void zzb(final byte[] array, final int n, final int n2) throws IOException {
            if (array != null && n >= 0 && n2 >= 0 && array.length - n2 >= n) {
                final long zzf = this.zzf;
                final long n3 = n2;
                if (zzf - n3 >= this.zzh) {
                    zzglw.zza(array, n, this.zzh, n3);
                    this.zzh += n3;
                    return;
                }
            }
            if (array == null) {
                throw new NullPointerException("value");
            }
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", this.zzh, this.zzf, n2));
        }
        
        @Override
        public final void zzc(final int n, final int n2) throws IOException {
            this.zza(n, 0);
            this.zzb(n2);
        }
        
        @Override
        public final void zzc(final int n, final long n2) throws IOException {
            this.zza(n, 1);
            this.zzc(n2);
        }
        
        @Override
        public final void zzc(final long n) throws IOException {
            this.zzc.putLong((int)(this.zzh - this.zzd), n);
            this.zzh += 8L;
        }
        
        public final void zzc(final byte[] array, final int n, final int n2) throws IOException {
            this.zzb(n2);
            this.zzb(array, 0, n2);
        }
        
        @Override
        public final void zzd(final int n) throws IOException {
            this.zzc.putInt((int)(this.zzh - this.zzd), n);
            this.zzh += 4L;
        }
        
        @Override
        public final void zze(final int n, final int n2) throws IOException {
            this.zza(n, 5);
            this.zzd(n2);
        }
    }
}
