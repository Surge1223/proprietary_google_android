package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzaif extends zzbid
{
    public static final Parcelable.Creator<zzaif> CREATOR;
    public String zza;
    public int zzb;
    public int zzc;
    public boolean zzd;
    public boolean zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaig();
    }
    
    zzaif(final String zza, final int zzb, final int zzc, final boolean zzd, final boolean zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, 4, this.zzc);
        zzbig.zza(parcel, 5, this.zzd);
        zzbig.zza(parcel, 6, this.zze);
        zzbig.zza(parcel, zza);
    }
}
