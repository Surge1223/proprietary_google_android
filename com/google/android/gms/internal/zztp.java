package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbi;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Parcelable.Creator;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzj;
import java.util.List;
import android.os.IInterface;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.client.zzf;
import android.os.RemoteException;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;

public final class zztp extends zzey implements zztn
{
    zztp(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }
    
    @Override
    public final IObjectWrapper zza() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final void zza(final zzf zzf, final String s) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        this.zzb(11, a_);
    }
    
    @Override
    public final void zza(final zzf zzf, final String s, final String s2) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        a_.writeString(s2);
        this.zzb(20, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(21, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final zzade zzade, final String s2) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zzade);
        a_.writeString(s2);
        this.zzb(10, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final zztq zztq) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztq);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final String s2, final zztq zztq) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        a_.writeString(s2);
        zzfa.zza(a_, (IInterface)zztq);
        this.zzb(7, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzf zzf, final String s, final String s2, final zztq zztq, final zznm zznm, final List<String> list) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        a_.writeString(s2);
        zzfa.zza(a_, (IInterface)zztq);
        zzfa.zza(a_, (Parcelable)zznm);
        a_.writeStringList((List)list);
        this.zzb(14, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzj zzj, final zzf zzf, final String s, final zztq zztq) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzj);
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        zzfa.zza(a_, (IInterface)zztq);
        this.zzb(1, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzj zzj, final zzf zzf, final String s, final String s2, final zztq zztq) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzj);
        zzfa.zza(a_, (Parcelable)zzf);
        a_.writeString(s);
        a_.writeString(s2);
        zzfa.zza(a_, (IInterface)zztq);
        this.zzb(6, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzade zzade, final List<String> list) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)zzade);
        a_.writeStringList((List)list);
        this.zzb(23, a_);
    }
    
    @Override
    public final void zza(final boolean b) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, b);
        this.zzb(25, a_);
    }
    
    @Override
    public final void zzb() throws RemoteException {
        this.zzb(4, this.a_());
    }
    
    @Override
    public final void zzc() throws RemoteException {
        this.zzb(5, this.a_());
    }
    
    @Override
    public final void zzd() throws RemoteException {
        this.zzb(8, this.a_());
    }
    
    @Override
    public final void zze() throws RemoteException {
        this.zzb(9, this.a_());
    }
    
    @Override
    public final void zzf() throws RemoteException {
        this.zzb(12, this.a_());
    }
    
    @Override
    public final boolean zzg() throws RemoteException {
        final Parcel zza = this.zza(13, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zztw zzh() throws RemoteException {
        final Parcel zza = this.zza(15, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zztw zztw;
        if (strongBinder == null) {
            zztw = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
            if (queryLocalInterface instanceof zztw) {
                zztw = (zztw)queryLocalInterface;
            }
            else {
                zztw = new zzty(strongBinder);
            }
        }
        zza.recycle();
        return zztw;
    }
    
    @Override
    public final zztz zzi() throws RemoteException {
        final Parcel zza = this.zza(16, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zztz zztz;
        if (strongBinder == null) {
            zztz = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
            if (queryLocalInterface instanceof zztz) {
                zztz = (zztz)queryLocalInterface;
            }
            else {
                zztz = new zzub(strongBinder);
            }
        }
        zza.recycle();
        return zztz;
    }
    
    @Override
    public final Bundle zzj() throws RemoteException {
        final Parcel zza = this.zza(17, this.a_());
        final Bundle bundle = zzfa.zza(zza, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }
    
    @Override
    public final Bundle zzk() throws RemoteException {
        final Parcel zza = this.zza(18, this.a_());
        final Bundle bundle = zzfa.zza(zza, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }
    
    @Override
    public final Bundle zzl() throws RemoteException {
        final Parcel zza = this.zza(19, this.a_());
        final Bundle bundle = zzfa.zza(zza, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }
    
    @Override
    public final boolean zzm() throws RemoteException {
        final Parcel zza = this.zza(22, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzot zzn() throws RemoteException {
        final Parcel zza = this.zza(24, this.a_());
        final zzot zza2 = zzou.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzbh zzo() throws RemoteException {
        final Parcel zza = this.zza(26, this.a_());
        final zzbh zza2 = zzbi.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zzuc zzp() throws RemoteException {
        final Parcel zza = this.zza(27, this.a_());
        final IBinder strongBinder = zza.readStrongBinder();
        zzuc zzuc;
        if (strongBinder == null) {
            zzuc = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IUnifiedNativeAdMapper");
            if (queryLocalInterface instanceof zzuc) {
                zzuc = (zzuc)queryLocalInterface;
            }
            else {
                zzuc = new zzue(strongBinder);
            }
        }
        zza.recycle();
        return zzuc;
    }
}
