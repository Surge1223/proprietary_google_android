package com.google.android.gms.internal;

import java.nio.ByteBuffer;

abstract class zzglz
{
    static void zzb(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        final int length = charSequence.length();
        final int position = byteBuffer.position();
        int i = 0;
        int n3 = 0;
    Label_0598:
        while (true) {
            while (i < length) {
                int n = position;
                int n2 = i;
                try {
                    final char char1 = charSequence.charAt(i);
                    if (char1 < '\u0080') {
                        n = position;
                        n2 = i;
                        byteBuffer.put(position + i, (byte)char1);
                        ++i;
                        continue;
                    }
                }
                catch (IndexOutOfBoundsException ex) {
                    i = n2;
                    break Label_0598;
                }
                break;
                final int position2 = byteBuffer.position();
                final int max = Math.max(i, n - byteBuffer.position() + 1);
                final char char2 = charSequence.charAt(i);
                final StringBuilder sb = new StringBuilder(37);
                sb.append("Failed writing ");
                sb.append(char2);
                sb.append(" at index ");
                sb.append(position2 + max);
                throw new ArrayIndexOutOfBoundsException(sb.toString());
            }
            if (i == length) {
                byteBuffer.position(position + i);
                return;
            }
            n3 = position + i;
            int n;
            int n4;
            char char3;
            int n5;
            byte b;
            zzgmb zzgmb;
            char char4;
            int codePoint;
            int n6;
            byte b2;
            ByteBuffer byteBuffer2;
            int n7;
            byte b3;
            int n8;
            int n9;
            int n10;
            int n11;
            int n12;
            int n13;
            int n14;
            int n15;
            int n16;
            int n17;
            byte b4;
            ByteBuffer byteBuffer3;
            int n18;
            byte b5;
            int n19;
            int n20;
            int n21;
            int n22;
            int n23;
            int n24;
            int n25;
            int n26;
            int n27;
            byte b6;
            ByteBuffer byteBuffer4;
            int n28;
            byte b7;
            ByteBuffer byteBuffer5;
            int n29;
            int n30;
            int n31;
            int n32;
            int n33;
            int n34;
            byte b8;
            int n35;
            byte b9;
            byte b10;
            Label_0570_Outer:Label_0222_Outer:
            while (i < length) {
                n4 = i;
                char3 = charSequence.charAt(i);
                while (true) {
                Label_0445_Outer:
                    while (true) {
                        if (char3 < '\u0080') {
                            n4 = i;
                            byteBuffer.put(n3, (byte)char3);
                            break Label_0570;
                        }
                        Label_0229: {
                            if (char3 >= '\u0800') {
                                break Label_0229;
                            }
                            n5 = n3 + 1;
                            b = (byte)('\u00c0' | char3 >>> 6);
                            n4 = n5;
                            try {
                                byteBuffer.put(n3, b);
                                n4 = n5;
                                byteBuffer.put(n5, (byte)(('?' & char3) | '\u0080'));
                                n3 = n5;
                                ++i;
                                ++n3;
                                continue Label_0570_Outer;
                            Block_14_Outer:
                                while (true) {
                                    n4 = i;
                                    n4 = i;
                                    zzgmb = new zzgmb(i, length);
                                    n4 = i;
                                    throw zzgmb;
                                    Label_0427: {
                                        while (true) {
                                            i = n3;
                                            Label_0434: {
                                                try {
                                                    char4 = charSequence.charAt(n4);
                                                    i = n3;
                                                    if (Character.isSurrogatePair(char3, char4)) {
                                                        i = n3;
                                                        codePoint = Character.toCodePoint(char3, char4);
                                                        n6 = n3 + 1;
                                                        b2 = (byte)(0xF0 | codePoint >>> 18);
                                                        i = n6;
                                                        byteBuffer2 = byteBuffer;
                                                        n7 = n3;
                                                        b3 = b2;
                                                        byteBuffer2.put(n7, b3);
                                                        n8 = n6;
                                                        n9 = 1;
                                                        n10 = n8 + n9;
                                                        n11 = codePoint;
                                                        n12 = 12;
                                                        n13 = n11 >>> n12;
                                                        n14 = 63;
                                                        n15 = (n13 & n14);
                                                        n16 = 128;
                                                        n17 = (n15 | n16);
                                                        b4 = (byte)n17;
                                                        i = n10;
                                                        byteBuffer3 = byteBuffer;
                                                        n18 = n6;
                                                        b5 = b4;
                                                        byteBuffer3.put(n18, b5);
                                                        n19 = n10;
                                                        n20 = 1;
                                                        n3 = n19 + n20;
                                                        n21 = codePoint;
                                                        n22 = 6;
                                                        n23 = n21 >>> n22;
                                                        n24 = 63;
                                                        n25 = (n23 & n24);
                                                        n26 = 128;
                                                        n27 = (n25 | n26);
                                                        b6 = (byte)n27;
                                                        i = n3;
                                                        byteBuffer4 = byteBuffer;
                                                        n28 = n10;
                                                        b7 = b6;
                                                        byteBuffer4.put(n28, b7);
                                                        i = n3;
                                                        byteBuffer5 = byteBuffer;
                                                        n29 = n3;
                                                        n30 = 63;
                                                        n31 = codePoint;
                                                        n32 = (n30 & n31);
                                                        n33 = 128;
                                                        n34 = (n32 | n33);
                                                        b8 = (byte)n34;
                                                        byteBuffer5.put(n29, b8);
                                                        i = n4;
                                                        continue Label_0445_Outer;
                                                    }
                                                    break Label_0427;
                                                }
                                                catch (IndexOutOfBoundsException ex2) {
                                                    break Label_0434;
                                                }
                                                try {
                                                    byteBuffer2 = byteBuffer;
                                                    n7 = n3;
                                                    b3 = b2;
                                                    byteBuffer2.put(n7, b3);
                                                    n8 = n6;
                                                    n9 = 1;
                                                    n10 = n8 + n9;
                                                    n11 = codePoint;
                                                    n12 = 12;
                                                    n13 = n11 >>> n12;
                                                    n14 = 63;
                                                    n15 = (n13 & n14);
                                                    n16 = 128;
                                                    n17 = (n15 | n16);
                                                    b4 = (byte)n17;
                                                    i = n10;
                                                    byteBuffer3 = byteBuffer;
                                                    n18 = n6;
                                                    b5 = b4;
                                                    byteBuffer3.put(n18, b5);
                                                    n19 = n10;
                                                    n20 = 1;
                                                    n3 = n19 + n20;
                                                    n21 = codePoint;
                                                    n22 = 6;
                                                    n23 = n21 >>> n22;
                                                    n24 = 63;
                                                    n25 = (n23 & n24);
                                                    n26 = 128;
                                                    n27 = (n25 | n26);
                                                    b6 = (byte)n27;
                                                    i = n3;
                                                    byteBuffer4 = byteBuffer;
                                                    n28 = n10;
                                                    b7 = b6;
                                                    byteBuffer4.put(n28, b7);
                                                    i = n3;
                                                    byteBuffer5 = byteBuffer;
                                                    n29 = n3;
                                                    n30 = 63;
                                                    n31 = codePoint;
                                                    n32 = (n30 & n31);
                                                    n33 = 128;
                                                    n34 = (n32 | n33);
                                                    b8 = (byte)n34;
                                                    byteBuffer5.put(n29, b8);
                                                    i = n4;
                                                    continue Label_0445_Outer;
                                                }
                                                catch (IndexOutOfBoundsException ex3) {}
                                            }
                                            n = i;
                                            i = n4;
                                            continue Label_0598;
                                            n = n4;
                                            continue Label_0598;
                                            n4 = i + 1;
                                            continue Label_0222_Outer;
                                        }
                                        Label_0482: {
                                            n35 = n3 + 1;
                                        }
                                        b9 = (byte)('\u00e0' | char3 >>> 12);
                                        n4 = n35;
                                        byteBuffer.put(n3, b9);
                                        n3 = n35 + 1;
                                        b10 = (byte)((char3 >>> 6 & '?') | '\u0080');
                                        n4 = i;
                                        byteBuffer.put(n35, b10);
                                        n4 = i;
                                        byteBuffer.put(n3, (byte)((char3 & '?') | '\u0080'));
                                        continue Label_0445_Outer;
                                    }
                                    i = n4;
                                    continue Block_14_Outer;
                                }
                            }
                            // iftrue(Label_0482:, char3 < '\ud800' || '\udfff' < char3)
                            // iftrue(Label_0445:, n4 == length)
                            catch (IndexOutOfBoundsException ex4) {}
                        }
                        break;
                    }
                    continue;
                }
            }
            break;
        }
        byteBuffer.position(n3);
    }
    
    abstract int zza(final int p0, final byte[] p1, final int p2, final int p3);
    
    abstract int zza(final CharSequence p0, final byte[] p1, final int p2, final int p3);
    
    abstract void zza(final CharSequence p0, final ByteBuffer p1);
    
    final boolean zza(final byte[] array, final int n, final int n2) {
        return this.zza(0, array, n, n2) == 0;
    }
}
