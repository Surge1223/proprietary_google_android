package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.internal.client.zzbh;
import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzop extends IInterface
{
    String zza() throws RemoteException;
    
    List zzb() throws RemoteException;
    
    String zze() throws RemoteException;
    
    zznx zzf() throws RemoteException;
    
    String zzg() throws RemoteException;
    
    String zzh() throws RemoteException;
    
    zzbh zzi() throws RemoteException;
    
    IObjectWrapper zzj() throws RemoteException;
    
    zznt zzp() throws RemoteException;
}
