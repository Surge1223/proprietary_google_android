package com.google.android.gms.internal;

import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import android.accounts.Account;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzav;
import com.google.android.gms.auth.api.signin.internal.zzaa;
import com.google.android.gms.common.internal.zzau;
import android.os.IInterface;
import android.os.IBinder;
import android.os.Parcelable;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import android.os.Looper;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.signin.zzd;
import com.google.android.gms.common.internal.zzl;

public final class zzegs extends zzl<zzegq> implements zzd
{
    private final boolean zzc;
    private final ClientSettings zzd;
    private final Bundle zze;
    private Integer zzf;
    
    private zzegs(final Context context, final Looper looper, final boolean b, final ClientSettings zzd, final Bundle zze, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 44, zzd, connectionCallbacks, onConnectionFailedListener);
        this.zzc = true;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzd.getClientSessionId();
    }
    
    public zzegs(final Context context, final Looper looper, final boolean b, final ClientSettings clientSettings, final SignInOptions signInOptions, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, true, clientSettings, zza(clientSettings), connectionCallbacks, onConnectionFailedListener);
    }
    
    public static Bundle zza(final ClientSettings clientSettings) {
        final SignInOptions signInOptions = clientSettings.getSignInOptions();
        final Integer clientSessionId = clientSettings.getClientSessionId();
        final Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", (Parcelable)clientSettings.getAccount());
        if (clientSessionId != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", (int)clientSessionId);
        }
        if (signInOptions != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", signInOptions.isOfflineAccessRequested());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", signInOptions.isIdTokenRequested());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", signInOptions.getServerClientId());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", signInOptions.isForceCodeForRefreshToken());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", signInOptions.getHostedDomain());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", signInOptions.waitForAccessTokenRefresh());
            if (signInOptions.getAuthApiSignInModuleVersion() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", (long)signInOptions.getAuthApiSignInModuleVersion());
            }
            if (signInOptions.getRealClientLibraryVersion() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", (long)signInOptions.getRealClientLibraryVersion());
            }
        }
        return bundle;
    }
    
    @Override
    protected final String getServiceDescriptor() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }
    
    @Override
    protected final String getStartServiceAction() {
        return "com.google.android.gms.signin.service.START";
    }
    
    @Override
    public final boolean requiresSignIn() {
        return this.zzc;
    }
    
    @Override
    public final int zza() {
        return 12438000;
    }
    
    @Override
    public final void zza(final zzego zzego) {
        zzau.zza(zzego, "Expecting a valid ISignInCallbacks");
        try {
            final Account accountOrDefault = this.zzd.getAccountOrDefault();
            GoogleSignInAccount zza = null;
            if ("<<default account>>".equals(accountOrDefault.name)) {
                zza = zzaa.zza(this.zzad()).zza();
            }
            this.zzag().zza(new zzegt(new zzav(accountOrDefault, this.zzf, zza)), zzego);
        }
        catch (RemoteException ex) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                zzego.zza(new zzegv(8));
            }
            catch (RemoteException ex2) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", (Throwable)ex);
            }
        }
    }
    
    @Override
    protected final Bundle zzb() {
        if (!this.zzad().getPackageName().equals(this.zzd.getRealClientPackageName())) {
            this.zze.putString("com.google.android.gms.signin.internal.realClientPackageName", this.zzd.getRealClientPackageName());
        }
        return this.zze;
    }
    
    @Override
    public final void zzd() {
        this.connect((ConnectionProgressReportCallbacks)new BaseGmsClient.zzf());
    }
}
