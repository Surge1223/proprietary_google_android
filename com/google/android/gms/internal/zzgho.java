package com.google.android.gms.internal;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.io.Serializable;

public abstract class zzgho implements Serializable, Iterable<Byte>
{
    public static final zzgho zza;
    private static final zzghs zzb;
    private int zzc;
    
    static {
        zza = new zzghv(zzgja.zzb);
        zzghs zzb2;
        if (zzghg.zza()) {
            zzb2 = new zzghw(null);
        }
        else {
            zzb2 = new zzghq(null);
        }
        zzb = zzb2;
    }
    
    zzgho() {
        this.zzc = 0;
    }
    
    public static zzgho zza(final String s) {
        return new zzghv(s.getBytes(zzgja.zza));
    }
    
    static int zzb(final int n, final int n2, final int n3) {
        final int n4 = n2 - n;
        if ((n | n2 | n4 | n3 - n2) >= 0) {
            return n4;
        }
        if (n < 0) {
            final StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(n);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        }
        if (n2 < n) {
            final StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(n);
            sb2.append(", ");
            sb2.append(n2);
            throw new IndexOutOfBoundsException(sb2.toString());
        }
        final StringBuilder sb3 = new StringBuilder(37);
        sb3.append("End index: ");
        sb3.append(n2);
        sb3.append(" >= ");
        sb3.append(n3);
        throw new IndexOutOfBoundsException(sb3.toString());
    }
    
    static zzght zzb(final int n) {
        return new zzght(n, null);
    }
    
    @Override
    public abstract boolean equals(final Object p0);
    
    @Override
    public final int hashCode() {
        int zzc;
        if ((zzc = this.zzc) == 0) {
            final int zza = this.zza();
            if ((zzc = this.zza(zza, 0, zza)) == 0) {
                zzc = 1;
            }
            this.zzc = zzc;
        }
        return zzc;
    }
    
    @Override
    public final String toString() {
        return String.format("<ByteString@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), this.zza());
    }
    
    public abstract byte zza(final int p0);
    
    public abstract int zza();
    
    protected abstract int zza(final int p0, final int p1, final int p2);
    
    public abstract zzgho zza(final int p0, final int p1);
    
    protected abstract String zza(final Charset p0);
    
    abstract void zza(final zzghn p0) throws IOException;
    
    public final String zzd() {
        final Charset zza = zzgja.zza;
        if (this.zza() == 0) {
            return "";
        }
        return this.zza(zza);
    }
    
    public abstract boolean zze();
    
    protected final int zzg() {
        return this.zzc;
    }
}
