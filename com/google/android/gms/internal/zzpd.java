package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzpd extends IInterface
{
    void zza(final zzot p0, final String p1) throws RemoteException;
}
