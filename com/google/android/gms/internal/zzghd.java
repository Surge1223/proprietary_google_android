package com.google.android.gms.internal;

public abstract class zzghd<MessageType extends zzghc<MessageType, BuilderType>, BuilderType extends zzghd<MessageType, BuilderType>> implements zzgkh
{
    public abstract BuilderType zza();
    
    protected abstract BuilderType zza(final MessageType p0);
}
