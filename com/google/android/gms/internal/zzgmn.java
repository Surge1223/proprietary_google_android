package com.google.android.gms.internal;

import java.io.IOException;

public final class zzgmn extends IOException
{
    zzgmn(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder(108);
        sb.append("CodedOutputStream was writing to a flat byte array and ran out of space (pos ");
        sb.append(n);
        sb.append(" limit ");
        sb.append(n2);
        sb.append(").");
        super(sb.toString());
    }
}
