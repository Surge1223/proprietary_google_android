package com.google.android.gms.internal;

final class zzgjx implements zzgkf
{
    private zzgkf[] zza;
    
    zzgjx(final zzgkf... zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean zza(final Class<?> clazz) {
        final zzgkf[] zza = this.zza;
        for (int length = zza.length, i = 0; i < length; ++i) {
            if (zza[i].zza(clazz)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final zzgke zzb(final Class<?> clazz) {
        for (final zzgkf zzgkf : this.zza) {
            if (zzgkf.zza(clazz)) {
                return zzgkf.zzb(clazz);
            }
        }
        final String value = String.valueOf(clazz.getName());
        String concat;
        if (value.length() != 0) {
            concat = "No factory is available for message type: ".concat(value);
        }
        else {
            concat = new String("No factory is available for message type: ");
        }
        throw new UnsupportedOperationException(concat);
    }
}
