package com.google.android.gms.internal;

import java.util.List;
import java.util.Map;

interface zzgmk
{
    int zza();
    
    @Deprecated
    void zza(final int p0);
    
    void zza(final int p0, final double p1);
    
    void zza(final int p0, final float p1);
    
    void zza(final int p0, final int p1);
    
    void zza(final int p0, final long p1);
    
    void zza(final int p0, final zzgho p1);
    
     <K, V> void zza(final int p0, final zzgjz<K, V> p1, final Map<K, V> p2);
    
    void zza(final int p0, final Object p1);
    
    void zza(final int p0, final String p1);
    
    void zza(final int p0, final List<String> p1);
    
    void zza(final int p0, final List<Integer> p1, final boolean p2);
    
    void zza(final int p0, final boolean p1);
    
    @Deprecated
    void zzb(final int p0);
    
    void zzb(final int p0, final int p1);
    
    void zzb(final int p0, final long p1);
    
    @Deprecated
    void zzb(final int p0, final Object p1);
    
    void zzb(final int p0, final List<zzgho> p1);
    
    void zzb(final int p0, final List<Integer> p1, final boolean p2);
    
    void zzc(final int p0, final int p1);
    
    void zzc(final int p0, final long p1);
    
    void zzc(final int p0, final Object p1);
    
    void zzc(final int p0, final List<?> p1);
    
    void zzc(final int p0, final List<Long> p1, final boolean p2);
    
    void zzd(final int p0, final int p1);
    
    void zzd(final int p0, final long p1);
    
    @Deprecated
    void zzd(final int p0, final List<?> p1);
    
    void zzd(final int p0, final List<Long> p1, final boolean p2);
    
    void zze(final int p0, final int p1);
    
    void zze(final int p0, final long p1);
    
    void zze(final int p0, final List<Long> p1, final boolean p2);
    
    void zzf(final int p0, final int p1);
    
    void zzf(final int p0, final List<Float> p1, final boolean p2);
    
    void zzg(final int p0, final List<Double> p1, final boolean p2);
    
    void zzh(final int p0, final List<Integer> p1, final boolean p2);
    
    void zzi(final int p0, final List<Boolean> p1, final boolean p2);
    
    void zzj(final int p0, final List<Integer> p1, final boolean p2);
    
    void zzk(final int p0, final List<Integer> p1, final boolean p2);
    
    void zzl(final int p0, final List<Long> p1, final boolean p2);
    
    void zzm(final int p0, final List<Integer> p1, final boolean p2);
    
    void zzn(final int p0, final List<Long> p1, final boolean p2);
}
