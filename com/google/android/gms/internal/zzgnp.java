package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzgnp extends zzgmo<zzgnp> implements Cloneable
{
    private byte[] zza;
    private String zzb;
    private byte[][] zzc;
    private boolean zzd;
    
    public zzgnp() {
        this.zza = zzgmx.zzh;
        this.zzb = "";
        this.zzc = zzgmx.zzg;
        this.zzd = false;
        this.zzax = null;
        this.zzay = -1;
    }
    
    private final zzgnp zza() {
        try {
            final zzgnp zzgnp = super.clone();
            if (this.zzc != null && this.zzc.length > 0) {
                zzgnp.zzc = this.zzc.clone();
            }
            return zzgnp;
        }
        catch (CloneNotSupportedException ex) {
            throw new AssertionError((Object)ex);
        }
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (!Arrays.equals(this.zza, zzgmx.zzh)) {
            computeSerializedSize = n + zzgmm.zzb(1, this.zza);
        }
        int n2 = computeSerializedSize;
        if (this.zzc != null) {
            n2 = computeSerializedSize;
            if (this.zzc.length > 0) {
                int i = 0;
                int n4;
                int n3 = n4 = 0;
                while (i < this.zzc.length) {
                    final byte[] array = this.zzc[i];
                    int n5 = n3;
                    int n6 = n4;
                    if (array != null) {
                        n6 = n4 + 1;
                        n5 = n3 + zzgmm.zzc(array);
                    }
                    ++i;
                    n3 = n5;
                    n4 = n6;
                }
                n2 = computeSerializedSize + n3 + n4 * 1;
            }
        }
        int n7 = n2;
        if (this.zzd) {
            n7 = n2 + (zzgmm.zzb(3) + 1);
        }
        int n8 = n7;
        if (this.zzb != null) {
            n8 = n7;
            if (!this.zzb.equals("")) {
                n8 = n7 + zzgmm.zzb(4, this.zzb);
            }
        }
        return n8;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgnp)) {
            return false;
        }
        final zzgnp zzgnp = (zzgnp)o;
        if (!Arrays.equals(this.zza, zzgnp.zza)) {
            return false;
        }
        if (this.zzb == null) {
            if (zzgnp.zzb != null) {
                return false;
            }
        }
        else if (!this.zzb.equals(zzgnp.zzb)) {
            return false;
        }
        if (!zzgms.zza(this.zzc, zzgnp.zzc)) {
            return false;
        }
        if (this.zzd != zzgnp.zzd) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgnp.zzax);
        }
        return zzgnp.zzax == null || zzgnp.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final int hashCode2 = Arrays.hashCode(this.zza);
        final String zzb = this.zzb;
        int hashCode3 = 0;
        int hashCode4;
        if (zzb == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = this.zzb.hashCode();
        }
        final int zza = zzgms.zza(this.zzc);
        int n;
        if (this.zzd) {
            n = 1231;
        }
        else {
            n = 1237;
        }
        if (this.zzax != null) {
            if (!this.zzax.zzb()) {
                hashCode3 = this.zzax.hashCode();
            }
        }
        return (((((527 + hashCode) * 31 + hashCode2) * 31 + hashCode4) * 31 + zza) * 31 + n) * 31 + hashCode3;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (!Arrays.equals(this.zza, zzgmx.zzh)) {
            zzgmm.zza(1, this.zza);
        }
        if (this.zzc != null && this.zzc.length > 0) {
            for (int i = 0; i < this.zzc.length; ++i) {
                final byte[] array = this.zzc[i];
                if (array != null) {
                    zzgmm.zza(2, array);
                }
            }
        }
        if (this.zzd) {
            zzgmm.zza(3, this.zzd);
        }
        if (this.zzb != null && !this.zzb.equals("")) {
            zzgmm.zza(4, this.zzb);
        }
        super.writeTo(zzgmm);
    }
}
