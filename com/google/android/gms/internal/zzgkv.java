package com.google.android.gms.internal;

import java.lang.reflect.Field;

final class zzgkv
{
    private final zzgkw zza;
    private int zzaa;
    private int zzab;
    private Field zzac;
    private zzgiy<Class<?>> zzad;
    private zzgiy<zzgjc<?>> zzae;
    private zzgiy<Object> zzaf;
    private final Object[] zzb;
    private Class<?> zzc;
    private final int zzd;
    private final int zze;
    private final int zzf;
    private final int zzg;
    private final int zzh;
    private final int zzi;
    private final int zzj;
    private final int zzk;
    private final int zzl;
    private final int zzm;
    private final int[] zzn;
    private int zzo;
    private int zzp;
    private int zzq;
    private int zzr;
    private int zzs;
    private int zzt;
    private int zzu;
    private int zzv;
    private int zzw;
    private int zzx;
    private int zzy;
    private int zzz;
    
    zzgkv(final Class<?> zzc, final String s, final Object[] zzb) {
        this.zzq = Integer.MAX_VALUE;
        this.zzr = Integer.MIN_VALUE;
        this.zzs = 0;
        this.zzt = 0;
        this.zzu = 0;
        this.zzv = 0;
        this.zzw = 0;
        this.zzad = zzgiy.zza();
        this.zzae = zzgiy.zza();
        this.zzaf = zzgiy.zza();
        this.zzc = zzc;
        this.zza = new zzgkw(s);
        this.zzb = zzb;
        this.zzd = this.zza.zzb();
        this.zze = this.zza.zzb();
        final int zze = this.zze;
        int[] zzn = null;
        if (zze == 0) {
            this.zzf = 0;
            this.zzg = 0;
            this.zzh = 0;
            this.zzi = 0;
            this.zzj = 0;
            this.zzl = 0;
            this.zzk = 0;
            this.zzm = 0;
            this.zzn = null;
            return;
        }
        this.zzf = this.zza.zzb();
        this.zzg = this.zza.zzb();
        this.zzh = this.zza.zzb();
        this.zzi = this.zza.zzb();
        this.zzl = this.zza.zzb();
        this.zzk = this.zza.zzb();
        this.zzj = this.zza.zzb();
        this.zzm = this.zza.zzb();
        final int zzb2 = this.zza.zzb();
        if (zzb2 != 0) {
            zzn = new int[zzb2];
        }
        this.zzn = zzn;
        this.zzo = (this.zzf << 1) + this.zzg;
    }
    
    private static Field zza(final Class<?> clazz, final String s) {
        try {
            return clazz.getDeclaredField(s);
        }
        catch (NoSuchFieldException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    private final void zza(final int n, final zzgjc<?> zzgjc) {
        if (this.zzae == zzgiy.zza()) {
            this.zzae = new zzgiy<zzgjc<?>>();
        }
        this.zzae.zza(n, zzgjc);
    }
    
    private final void zza(final int n, final Class<?> clazz) {
        if (this.zzad == zzgiy.zza()) {
            this.zzad = new zzgiy<Class<?>>();
        }
        this.zzad.zza(n, clazz);
    }
    
    private final Object zzp() {
        return this.zzb[this.zzo++];
    }
    
    private final boolean zzq() {
        return (this.zzd & 0x1) == 0x1;
    }
    
    private final void zzr() {
        if (this.zza.zza()) {
            final int zza = this.zza.zzb;
            final int length = this.zza.zza.length();
            final StringBuilder sb = new StringBuilder(66);
            sb.append("decoder position = ");
            sb.append(zza);
            sb.append(" is not at end (length = ");
            sb.append(length);
            throw new IllegalStateException(sb.toString());
        }
        if (this.zze > 0 && this.zzw != this.zze) {
            final int zze = this.zze;
            final int zzw = this.zzw;
            final StringBuilder sb2 = new StringBuilder(50);
            sb2.append("fieldCount is ");
            sb2.append(zze);
            sb2.append(" but expected ");
            sb2.append(zzw);
            throw new IllegalStateException(sb2.toString());
        }
        if (this.zzb != null && this.zzo != this.zzb.length) {
            final int zzo = this.zzo;
            final int length2 = this.zzb.length;
            final StringBuilder sb3 = new StringBuilder(65);
            sb3.append("objectsPosition = ");
            sb3.append(zzo);
            sb3.append(" is not at end (length = ");
            sb3.append(length2);
            throw new IllegalStateException(sb3.toString());
        }
        if (this.zzn != null && this.zzp != this.zzn.length) {
            final int zzp = this.zzp;
            final int length3 = this.zzn.length;
            final StringBuilder sb4 = new StringBuilder(74);
            sb4.append("checkInitializedPosition = ");
            sb4.append(zzp);
            sb4.append(" is not at end (length = ");
            sb4.append(length3);
            throw new IllegalStateException(sb4.toString());
        }
        if (this.zze > 0 && this.zzh != this.zzq) {
            final int zzh = this.zzh;
            final int zzq = this.zzq;
            final StringBuilder sb5 = new StringBuilder(54);
            sb5.append("minFieldNumber is ");
            sb5.append(zzh);
            sb5.append(" but expected ");
            sb5.append(zzq);
            throw new IllegalStateException(sb5.toString());
        }
        if (this.zze > 0 && this.zzi != this.zzr) {
            final int zzi = this.zzi;
            final int zzr = this.zzr;
            final StringBuilder sb6 = new StringBuilder(54);
            sb6.append("maxFieldNumber is ");
            sb6.append(zzi);
            sb6.append(" but expected ");
            sb6.append(zzr);
            throw new IllegalStateException(sb6.toString());
        }
        if (this.zzj != this.zzs) {
            final int zzj = this.zzj;
            final int zzs = this.zzs;
            final StringBuilder sb7 = new StringBuilder(53);
            sb7.append("mapFieldCount is ");
            sb7.append(zzj);
            sb7.append(" but expected ");
            sb7.append(zzs);
            throw new IllegalStateException(sb7.toString());
        }
        if (this.zzm != this.zzt) {
            final int zzm = this.zzm;
            final int zzt = this.zzt;
            final StringBuilder sb8 = new StringBuilder(58);
            sb8.append("repeatedFieldCount is ");
            sb8.append(zzm);
            sb8.append(" but expected ");
            sb8.append(zzt);
            throw new IllegalStateException(sb8.toString());
        }
        if (this.zzl > 0 && this.zzu != this.zzl) {
            final int zzl = this.zzl;
            final int zzu = this.zzu;
            final StringBuilder sb9 = new StringBuilder(52);
            sb9.append("entriesCount is ");
            sb9.append(zzl);
            sb9.append(" but expected ");
            sb9.append(zzu);
            throw new IllegalStateException(sb9.toString());
        }
        if (this.zzk > 0 && this.zzv != this.zzk) {
            final int zzk = this.zzk;
            final int zzv = this.zzv;
            final StringBuilder sb10 = new StringBuilder(62);
            sb10.append("lookUpStartFieldNumber is ");
            sb10.append(zzk);
            sb10.append(" but expected ");
            sb10.append(zzv);
            throw new IllegalStateException(sb10.toString());
        }
    }
    
    final boolean zza() {
        final boolean zza = this.zza.zza();
        final boolean b = false;
        if (!zza) {
            return false;
        }
        this.zzx = this.zza.zzb();
        this.zzy = this.zza.zzb();
        this.zzz = (this.zzy & 0xFF);
        if (this.zzx < this.zzq) {
            this.zzq = this.zzx;
        }
        if (this.zzx > this.zzr) {
            this.zzr = this.zzx;
        }
        if (this.zzz == zzgiq.zzi.zza()) {
            ++this.zzs;
        }
        else if (this.zzz >= zzgiq.zzd.zza() && this.zzz <= zzgiq.zzh.zza()) {
            ++this.zzt;
        }
        ++this.zzw;
        if (zzgla.zza(this.zzq, this.zzx, this.zzw)) {
            this.zzv = this.zzx + 1;
            this.zzu = this.zzv - this.zzq;
        }
        else {
            ++this.zzu;
        }
        if ((this.zzy & 0x400) != 0x0) {
            this.zzn[this.zzp++] = this.zzx;
        }
        if (this.zzd()) {
            this.zzaa = this.zza.zzb();
            if (this.zzz != zzgiq.zza.zza() + 51 && this.zzz != zzgiq.zzc.zza() + 51) {
                if (this.zzz == zzgiq.zzb.zza() + 51 && this.zzq()) {
                    this.zza(this.zzx, (zzgjc<?>)this.zzp());
                }
            }
            else {
                this.zza(this.zzx, (Class<?>)this.zzp());
            }
        }
        else {
            this.zzac = zza(this.zzc, (String)this.zzp());
            if (this.zzh()) {
                this.zzab = this.zza.zzb();
            }
            if (this.zzz != zzgiq.zza.zza() && this.zzz != zzgiq.zzc.zza()) {
                if (this.zzz != zzgiq.zze.zza() && this.zzz != zzgiq.zzh.zza()) {
                    if (this.zzz != zzgiq.zzb.zza() && this.zzz != zzgiq.zzf.zza() && this.zzz != zzgiq.zzg.zza()) {
                        if (this.zzz == zzgiq.zzi.zza()) {
                            final int zzx = this.zzx;
                            final Object zzp = this.zzp();
                            if (this.zzaf == zzgiy.zza()) {
                                this.zzaf = new zzgiy<Object>();
                            }
                            this.zzaf.zza(zzx, zzp);
                            if ((this.zzy & 0x800) != 0x0 || b) {
                                this.zza(this.zzx, (zzgjc<?>)this.zzp());
                            }
                        }
                    }
                    else if (this.zzq()) {
                        this.zza(this.zzx, (zzgjc<?>)this.zzp());
                    }
                }
                else {
                    this.zza(this.zzx, (Class<?>)this.zzp());
                }
            }
            else {
                this.zza(this.zzx, this.zzac.getType());
            }
        }
        return true;
    }
    
    final int zzb() {
        return this.zzx;
    }
    
    final int zzc() {
        return this.zzz;
    }
    
    final boolean zzd() {
        return this.zzz > zzgiq.zzi.zza();
    }
    
    final Field zze() {
        final int n = this.zzaa << 1;
        final Object o = this.zzb[n];
        if (o instanceof Field) {
            return (Field)o;
        }
        return (Field)(this.zzb[n] = zza(this.zzc, (String)o));
    }
    
    final Field zzf() {
        final int n = (this.zzaa << 1) + 1;
        final Object o = this.zzb[n];
        if (o instanceof Field) {
            return (Field)o;
        }
        return (Field)(this.zzb[n] = zza(this.zzc, (String)o));
    }
    
    final Field zzg() {
        return this.zzac;
    }
    
    final boolean zzh() {
        return this.zzq() && this.zzz <= zzgiq.zzc.zza();
    }
    
    final Field zzi() {
        final int n = (this.zzf << 1) + this.zzab / 32;
        final Object o = this.zzb[n];
        if (o instanceof Field) {
            return (Field)o;
        }
        return (Field)(this.zzb[n] = zza(this.zzc, (String)o));
    }
    
    final int zzj() {
        return this.zzab % 32;
    }
    
    final boolean zzk() {
        return (this.zzy & 0x100) != 0x0;
    }
    
    final boolean zzl() {
        return (this.zzy & 0x200) != 0x0;
    }
    
    final zzgiy<Class<?>> zzm() {
        this.zzr();
        return this.zzad;
    }
    
    final zzgiy<zzgjc<?>> zzn() {
        this.zzr();
        return this.zzae;
    }
    
    final zzgiy<Object> zzo() {
        this.zzr();
        return this.zzaf;
    }
}
