package com.google.android.gms.internal;

final class zzgmd
{
    static {
        zza = new int[zzgme.values().length];
        try {
            zzgmd.zza[zzgme.zza.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            zzgmd.zza[zzgme.zzb.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError2) {}
        try {
            zzgmd.zza[zzgme.zzc.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError3) {}
        try {
            zzgmd.zza[zzgme.zzd.ordinal()] = 4;
        }
        catch (NoSuchFieldError noSuchFieldError4) {}
        try {
            zzgmd.zza[zzgme.zze.ordinal()] = 5;
        }
        catch (NoSuchFieldError noSuchFieldError5) {}
        try {
            zzgmd.zza[zzgme.zzf.ordinal()] = 6;
        }
        catch (NoSuchFieldError noSuchFieldError6) {}
        try {
            zzgmd.zza[zzgme.zzg.ordinal()] = 7;
        }
        catch (NoSuchFieldError noSuchFieldError7) {}
        try {
            zzgmd.zza[zzgme.zzh.ordinal()] = 8;
        }
        catch (NoSuchFieldError noSuchFieldError8) {}
        try {
            zzgmd.zza[zzgme.zzl.ordinal()] = 9;
        }
        catch (NoSuchFieldError noSuchFieldError9) {}
        try {
            zzgmd.zza[zzgme.zzm.ordinal()] = 10;
        }
        catch (NoSuchFieldError noSuchFieldError10) {}
        try {
            zzgmd.zza[zzgme.zzo.ordinal()] = 11;
        }
        catch (NoSuchFieldError noSuchFieldError11) {}
        try {
            zzgmd.zza[zzgme.zzp.ordinal()] = 12;
        }
        catch (NoSuchFieldError noSuchFieldError12) {}
        try {
            zzgmd.zza[zzgme.zzq.ordinal()] = 13;
        }
        catch (NoSuchFieldError noSuchFieldError13) {}
        try {
            zzgmd.zza[zzgme.zzr.ordinal()] = 14;
        }
        catch (NoSuchFieldError noSuchFieldError14) {}
        try {
            zzgmd.zza[zzgme.zzi.ordinal()] = 15;
        }
        catch (NoSuchFieldError noSuchFieldError15) {}
        try {
            zzgmd.zza[zzgme.zzj.ordinal()] = 16;
        }
        catch (NoSuchFieldError noSuchFieldError16) {}
        try {
            zzgmd.zza[zzgme.zzk.ordinal()] = 17;
        }
        catch (NoSuchFieldError noSuchFieldError17) {}
        try {
            zzgmd.zza[zzgme.zzn.ordinal()] = 18;
        }
        catch (NoSuchFieldError noSuchFieldError18) {}
    }
}
