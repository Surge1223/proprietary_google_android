package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzok extends zzey implements zzoj
{
    zzok(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegateCreator");
    }
}
