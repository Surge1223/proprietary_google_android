package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzgj extends IInterface
{
    String zza() throws RemoteException;
    
    boolean zza(final boolean p0) throws RemoteException;
}
