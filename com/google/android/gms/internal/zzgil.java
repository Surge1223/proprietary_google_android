package com.google.android.gms.internal;

final class zzgil
{
    static {
        zza = new int[zzgme.values().length];
        try {
            zzgil.zza[zzgme.zza.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            zzgil.zza[zzgme.zzb.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError2) {}
        try {
            zzgil.zza[zzgme.zzc.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError3) {}
        try {
            zzgil.zza[zzgme.zzd.ordinal()] = 4;
        }
        catch (NoSuchFieldError noSuchFieldError4) {}
        try {
            zzgil.zza[zzgme.zze.ordinal()] = 5;
        }
        catch (NoSuchFieldError noSuchFieldError5) {}
        try {
            zzgil.zza[zzgme.zzf.ordinal()] = 6;
        }
        catch (NoSuchFieldError noSuchFieldError6) {}
        try {
            zzgil.zza[zzgme.zzg.ordinal()] = 7;
        }
        catch (NoSuchFieldError noSuchFieldError7) {}
        try {
            zzgil.zza[zzgme.zzh.ordinal()] = 8;
        }
        catch (NoSuchFieldError noSuchFieldError8) {}
        try {
            zzgil.zza[zzgme.zzm.ordinal()] = 9;
        }
        catch (NoSuchFieldError noSuchFieldError9) {}
        try {
            zzgil.zza[zzgme.zzo.ordinal()] = 10;
        }
        catch (NoSuchFieldError noSuchFieldError10) {}
        try {
            zzgil.zza[zzgme.zzp.ordinal()] = 11;
        }
        catch (NoSuchFieldError noSuchFieldError11) {}
        try {
            zzgil.zza[zzgme.zzq.ordinal()] = 12;
        }
        catch (NoSuchFieldError noSuchFieldError12) {}
        try {
            zzgil.zza[zzgme.zzr.ordinal()] = 13;
        }
        catch (NoSuchFieldError noSuchFieldError13) {}
        try {
            zzgil.zza[zzgme.zzn.ordinal()] = 14;
        }
        catch (NoSuchFieldError noSuchFieldError14) {}
        try {
            zzgil.zza[zzgme.zzl.ordinal()] = 15;
        }
        catch (NoSuchFieldError noSuchFieldError15) {}
        try {
            zzgil.zza[zzgme.zzi.ordinal()] = 16;
        }
        catch (NoSuchFieldError noSuchFieldError16) {}
        try {
            zzgil.zza[zzgme.zzj.ordinal()] = 17;
        }
        catch (NoSuchFieldError noSuchFieldError17) {}
        try {
            zzgil.zza[zzgme.zzk.ordinal()] = 18;
        }
        catch (NoSuchFieldError noSuchFieldError18) {}
    }
}
