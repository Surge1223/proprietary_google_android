package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.Map;
import java.util.LinkedHashMap;

public final class zzgka<K, V> extends LinkedHashMap<K, V>
{
    private static final zzgka zzb;
    private boolean zza;
    
    static {
        (zzb = new zzgka()).zza = false;
    }
    
    private zzgka() {
        this.zza = true;
    }
    
    private zzgka(final Map<K, V> map) {
        super(map);
        this.zza = true;
    }
    
    private static int zza(final Object o) {
        if (o instanceof byte[]) {
            return zzgja.zzc((byte[])o);
        }
        if (!(o instanceof zzgjb)) {
            return o.hashCode();
        }
        throw new UnsupportedOperationException();
    }
    
    private final void zze() {
        if (this.zza) {
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final void clear() {
        this.zze();
        super.clear();
    }
    
    @Override
    public final Set<Map.Entry<K, V>> entrySet() {
        if (this.isEmpty()) {
            return Collections.emptySet();
        }
        return super.entrySet();
    }
    
    @Override
    public final boolean equals(Object value) {
        if (value instanceof Map) {
            final Map map = (Map)value;
            boolean b2 = false;
            Label_0167: {
                Label_0165: {
                    if (this != map) {
                        Label_0032: {
                            if (super.size() == map.size()) {
                                for (final Map.Entry<K, V> entry : super.entrySet()) {
                                    if (!map.containsKey(entry.getKey())) {
                                        break Label_0032;
                                    }
                                    value = entry.getValue();
                                    final byte[] value2 = map.get(entry.getKey());
                                    boolean b;
                                    if (value instanceof byte[] && value2 instanceof byte[]) {
                                        b = Arrays.equals((byte[])value, value2);
                                    }
                                    else {
                                        b = value.equals(value2);
                                    }
                                    if (!b) {
                                        break Label_0032;
                                    }
                                }
                                break Label_0165;
                            }
                        }
                        b2 = false;
                        break Label_0167;
                    }
                }
                b2 = true;
            }
            if (b2) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        final Iterator<Map.Entry<K, V>> iterator = super.entrySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry<K, V> entry = iterator.next();
            n += (zza(entry.getValue()) ^ zza(entry.getKey()));
        }
        return n;
    }
    
    @Override
    public final V put(final K k, final V v) {
        this.zze();
        zzgja.zza(k);
        zzgja.zza(v);
        return super.put(k, v);
    }
    
    @Override
    public final void putAll(final Map<? extends K, ? extends V> map) {
        this.zze();
        for (final K next : map.keySet()) {
            zzgja.zza((Object)next);
            zzgja.zza(map.get(next));
        }
        super.putAll(map);
    }
    
    @Override
    public final V remove(final Object o) {
        this.zze();
        return super.remove(o);
    }
    
    public final void zza(final zzgka<K, V> zzgka) {
        this.zze();
        if (!zzgka.isEmpty()) {
            this.putAll((Map<? extends K, ? extends V>)zzgka);
        }
    }
    
    public final zzgka<K, V> zzb() {
        if (this.isEmpty()) {
            return new zzgka<K, V>();
        }
        return new zzgka<K, V>(this);
    }
    
    public final boolean zzd() {
        return this.zza;
    }
}
