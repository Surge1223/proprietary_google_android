package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public abstract class zzmh extends zzez implements zzmg
{
    public zzmh() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.customrenderedad.client.IOnCustomRenderedAdLoadedListener");
    }
    
    public static zzmg zza(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.customrenderedad.client.IOnCustomRenderedAdLoadedListener");
        if (queryLocalInterface instanceof zzmg) {
            return (zzmg)queryLocalInterface;
        }
        return new zzmi(binder);
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        if (n == 1) {
            final IBinder strongBinder = parcel.readStrongBinder();
            zzmd zzmd;
            if (strongBinder == null) {
                zzmd = null;
            }
            else {
                final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
                if (queryLocalInterface instanceof zzmd) {
                    zzmd = (zzmd)queryLocalInterface;
                }
                else {
                    zzmd = new zzmf(strongBinder);
                }
            }
            this.zza(zzmd);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}
