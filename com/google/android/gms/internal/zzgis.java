package com.google.android.gms.internal;

enum zzgis
{
    zza(false), 
    zzb(true), 
    zzc(true), 
    zzd(false);
    
    private final boolean zze;
    
    private zzgis(final boolean zze) {
        this.zze = zze;
    }
}
