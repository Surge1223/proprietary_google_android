package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbi;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Parcelable.Creator;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;

public final class zzty extends zzey implements zztw
{
    zzty(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }
    
    @Override
    public final String zza() throws RemoteException {
        final Parcel zza = this.zza(2, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(11, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (IInterface)objectWrapper2);
        zzfa.zza(a_, (IInterface)objectWrapper3);
        this.zzb(22, a_);
    }
    
    @Override
    public final List zzb() throws RemoteException {
        final Parcel zza = this.zza(3, this.a_());
        final ArrayList zzc = zzfa.zzc(zza);
        zza.recycle();
        return zzc;
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(12, a_);
    }
    
    @Override
    public final String zzc() throws RemoteException {
        final Parcel zza = this.zza(4, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zzc(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(16, a_);
    }
    
    @Override
    public final zznx zzd() throws RemoteException {
        final Parcel zza = this.zza(5, this.a_());
        final zznx zza2 = zzny.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final String zze() throws RemoteException {
        final Parcel zza = this.zza(6, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final double zzf() throws RemoteException {
        final Parcel zza = this.zza(7, this.a_());
        final double double1 = zza.readDouble();
        zza.recycle();
        return double1;
    }
    
    @Override
    public final String zzg() throws RemoteException {
        final Parcel zza = this.zza(8, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final String zzh() throws RemoteException {
        final Parcel zza = this.zza(9, this.a_());
        final String string = zza.readString();
        zza.recycle();
        return string;
    }
    
    @Override
    public final void zzi() throws RemoteException {
        this.zzb(10, this.a_());
    }
    
    @Override
    public final boolean zzj() throws RemoteException {
        final Parcel zza = this.zza(13, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final boolean zzk() throws RemoteException {
        final Parcel zza = this.zza(14, this.a_());
        final boolean zza2 = zzfa.zza(zza);
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final Bundle zzl() throws RemoteException {
        final Parcel zza = this.zza(15, this.a_());
        final Bundle bundle = zzfa.zza(zza, (android.os.Parcelable.Creator<Bundle>)Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }
    
    @Override
    public final zzbh zzm() throws RemoteException {
        final Parcel zza = this.zza(17, this.a_());
        final zzbh zza2 = zzbi.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzn() throws RemoteException {
        final Parcel zza = this.zza(18, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final zznt zzo() throws RemoteException {
        final Parcel zza = this.zza(19, this.a_());
        final zznt zza2 = zznu.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzp() throws RemoteException {
        final Parcel zza = this.zza(20, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
    
    @Override
    public final IObjectWrapper zzq() throws RemoteException {
        final Parcel zza = this.zza(21, this.a_());
        final IObjectWrapper zza2 = IObjectWrapper.zza.zza(zza.readStrongBinder());
        zza.recycle();
        return zza2;
    }
}
