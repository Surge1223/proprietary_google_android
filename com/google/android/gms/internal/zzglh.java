package com.google.android.gms.internal;

import java.util.Iterator;

final class zzglh implements Iterable<Object>
{
    @Override
    public final Iterator<Object> iterator() {
        return zzglf.zza;
    }
}
