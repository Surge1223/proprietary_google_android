package com.google.android.gms.internal;

import java.util.Map;

abstract class zzgij<T extends zzgip<T>>
{
    abstract int zza(final Map.Entry<?, ?> p0);
    
    abstract zzgin<T> zza(final Object p0);
    
    abstract void zza(final zzgmk p0, final Map.Entry<?, ?> p1);
    
    abstract void zza(final Object p0, final zzgin<T> p1);
    
    abstract boolean zza(final Class<?> p0);
    
    abstract zzgin<T> zzb(final Object p0);
}
