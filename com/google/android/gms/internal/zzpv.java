package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeAppInstallAd;

public final class zzpv extends zzoy
{
    private final NativeAppInstallAd.OnAppInstallAdLoadedListener zza;
    
    public zzpv(final NativeAppInstallAd.OnAppInstallAdLoadedListener zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzol zzol) {
        this.zza.onAppInstallAdLoaded(new zzoo(zzol));
    }
}
