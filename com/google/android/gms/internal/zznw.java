package com.google.android.gms.internal;

import android.os.IInterface;
import java.util.Iterator;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.ads.formats.NativeAd;

public final class zznw extends AdChoicesInfo
{
    private final zznt zza;
    private final List<Image> zzb;
    private String zzc;
    
    public zznw(final zznt zza) {
        this.zzb = new ArrayList<Image>();
        this.zza = zza;
        try {
            this.zzc = this.zza.zza();
        }
        catch (RemoteException ex) {
            zzaid.zzb("Error while obtaining attribution text.", (Throwable)ex);
            this.zzc = "";
        }
        try {
            for (final zznx next : zza.zzb()) {
                zznx zznx = null;
                Label_0131: {
                    if (next instanceof IBinder) {
                        final IBinder binder = (IBinder)next;
                        if (binder != null) {
                            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                            if (queryLocalInterface instanceof zznx) {
                                zznx = (zznx)queryLocalInterface;
                                break Label_0131;
                            }
                            zznx = new zznz(binder);
                            break Label_0131;
                        }
                    }
                    zznx = null;
                }
                if (zznx != null) {
                    this.zzb.add(new zzoa(zznx));
                }
            }
        }
        catch (RemoteException ex2) {
            zzaid.zzb("Error while obtaining image.", (Throwable)ex2);
        }
    }
}
