package com.google.android.gms.internal;

import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationBannerListener;
import android.os.RemoteException;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.ads.AdRequest;

final class zzux implements Runnable
{
    private final /* synthetic */ AdRequest.ErrorCode zza;
    private final /* synthetic */ zzun zzb;
    
    zzux(final zzun zzb, final AdRequest.ErrorCode zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        try {
            this.zzb.zza.zza(zzuz.zza(this.zza));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
}
