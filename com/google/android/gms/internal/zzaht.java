package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.Display;
import com.google.android.gms.common.util.zzo;
import android.view.WindowManager;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.UUID;
import android.os.Build;
import java.net.HttpURLConnection;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.widget.FrameLayout$LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.android.gms.ads.internal.client.zzj;
import android.view.ViewGroup;
import java.util.Iterator;
import android.net.Uri.Builder;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.os.Build.VERSION;
import android.os.Bundle;
import java.util.StringTokenizer;
import java.security.NoSuchAlgorithmException;
import java.math.BigInteger;
import java.util.Locale;
import java.security.MessageDigest;
import android.content.ContentResolver;
import android.provider.Settings;
import android.util.TypedValue;
import android.util.DisplayMetrics;
import android.content.Context;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.search.SearchAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.AdView;
import android.os.Looper;
import android.os.Handler;

public final class zzaht
{
    public static final Handler zza;
    private static final String zzb;
    private static final String zzc;
    private static final String zzd;
    private static final String zze;
    private static final String zzf;
    private static final String zzg;
    
    static {
        zza = new Handler(Looper.getMainLooper());
        zzb = AdView.class.getName();
        zzc = InterstitialAd.class.getName();
        zzd = PublisherAdView.class.getName();
        zze = PublisherInterstitialAd.class.getName();
        zzf = SearchAdView.class.getName();
        zzg = AdLoader.class.getName();
    }
    
    public static int zza(final Context context, final int n) {
        return zza(context.getResources().getDisplayMetrics(), n);
    }
    
    public static int zza(final DisplayMetrics displayMetrics, final int n) {
        return (int)TypedValue.applyDimension(1, (float)n, displayMetrics);
    }
    
    public static String zza(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        String string;
        if (contentResolver == null) {
            string = null;
        }
        else {
            string = Settings.Secure.getString(contentResolver, "android_id");
        }
        if (string == null || zza()) {
            string = "emulator";
        }
        return zza(string);
    }
    
    public static String zza(final String s) {
        int i = 0;
        while (i < 2) {
            try {
                final MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(s.getBytes());
                return String.format(Locale.US, "%032X", new BigInteger(1, instance.digest()));
            }
            catch (ArithmeticException ex) {
                return null;
            }
            catch (NoSuchAlgorithmException ex2) {
                ++i;
                continue;
            }
            break;
        }
        return null;
    }
    
    public static String zza(final StackTraceElement[] array, String string) {
        int n = 0;
        String className;
        while (true) {
            final int n2 = n + 1;
            if (n2 >= array.length) {
                className = null;
                break;
            }
            final StackTraceElement stackTraceElement = array[n];
            final String className2 = stackTraceElement.getClassName();
            if ("loadAd".equalsIgnoreCase(stackTraceElement.getMethodName()) && (zzaht.zzb.equalsIgnoreCase(className2) || zzaht.zzc.equalsIgnoreCase(className2) || zzaht.zzd.equalsIgnoreCase(className2) || zzaht.zze.equalsIgnoreCase(className2) || zzaht.zzf.equalsIgnoreCase(className2) || zzaht.zzg.equalsIgnoreCase(className2))) {
                className = array[n2].getClassName();
                break;
            }
            n = n2;
        }
        if (string != null) {
            final StringTokenizer stringTokenizer = new StringTokenizer(string, ".");
            final StringBuilder sb = new StringBuilder();
            int n3 = 2;
            if (stringTokenizer.hasMoreElements()) {
                sb.append(stringTokenizer.nextToken());
                while (n3 > 0 && stringTokenizer.hasMoreElements()) {
                    sb.append(".");
                    sb.append(stringTokenizer.nextToken());
                    --n3;
                }
                string = sb.toString();
            }
            if (className != null && !className.contains(string)) {
                return className;
            }
        }
        return null;
    }
    
    public static void zza(final Context context, String s, final String s2, final Bundle bundle, final boolean b, final zzahw zzahw) {
        if (b) {
            Context applicationContext;
            if ((applicationContext = context.getApplicationContext()) == null) {
                applicationContext = context;
            }
            bundle.putString("os", Build.VERSION.RELEASE);
            bundle.putString("api", String.valueOf(Build.VERSION.SDK_INT));
            bundle.putString("appid", applicationContext.getPackageName());
            String string;
            if ((string = s) == null) {
                final int apkVersion = GoogleApiAvailabilityLight.getInstance().getApkVersion(context);
                final StringBuilder sb = new StringBuilder(20);
                sb.append(apkVersion);
                sb.append(".12438000");
                string = sb.toString();
            }
            bundle.putString("js", string);
        }
        final Uri.Builder appendQueryParameter = new Uri.Builder().scheme("https").path("//pagead2.googlesyndication.com/pagead/gen_204").appendQueryParameter("id", s2);
        final Iterator<String> iterator = (Iterator<String>)bundle.keySet().iterator();
        while (iterator.hasNext()) {
            s = iterator.next();
            appendQueryParameter.appendQueryParameter(s, bundle.getString(s));
        }
        zzahw.zza(appendQueryParameter.toString());
    }
    
    private final void zza(final ViewGroup viewGroup, final zzj zzj, final String text, int zza, final int backgroundColor) {
        if (viewGroup.getChildCount() != 0) {
            return;
        }
        final Context context = viewGroup.getContext();
        final TextView textView = new TextView(context);
        textView.setGravity(17);
        textView.setText((CharSequence)text);
        textView.setTextColor(zza);
        textView.setBackgroundColor(backgroundColor);
        final FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setBackgroundColor(zza);
        zza = zza(context, 3);
        frameLayout.addView((View)textView, (ViewGroup.LayoutParams)new FrameLayout$LayoutParams(zzj.zzf - zza, zzj.zzc - zza, 17));
        viewGroup.addView((View)frameLayout, zzj.zzf, zzj.zzc);
    }
    
    public static void zza(final boolean b, final HttpURLConnection httpURLConnection, final String s) {
        httpURLConnection.setConnectTimeout(60000);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setReadTimeout(60000);
        if (s != null) {
            httpURLConnection.setRequestProperty("User-Agent", s);
        }
        httpURLConnection.setUseCaches(false);
    }
    
    public static boolean zza() {
        return Build.DEVICE.startsWith("generic");
    }
    
    public static boolean zzb() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
    
    public static String zzc() {
        final UUID randomUUID = UUID.randomUUID();
        final byte[] byteArray = BigInteger.valueOf(randomUUID.getLeastSignificantBits()).toByteArray();
        final byte[] byteArray2 = BigInteger.valueOf(randomUUID.getMostSignificantBits()).toByteArray();
        String s = new BigInteger(1, byteArray).toString();
        for (int i = 0; i < 2; ++i) {
            try {
                final MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(byteArray);
                instance.update(byteArray2);
                final byte[] array = new byte[8];
                System.arraycopy(instance.digest(), 0, array, 0, 8);
                s = new BigInteger(1, array).toString();
            }
            catch (NoSuchAlgorithmException ex) {}
        }
        return s;
    }
    
    public static boolean zzc(final Context context) {
        return GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(context) == 0;
    }
    
    public static int zzd(final Context context) {
        return DynamiteModule.zzb(context, "com.google.android.gms.ads.dynamite");
    }
    
    public static int zze(final Context context) {
        return DynamiteModule.zza(context, "com.google.android.gms.ads.dynamite");
    }
    
    public static boolean zzg(final Context context) {
        if (context.getResources().getConfiguration().orientation != 2) {
            return false;
        }
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int)(displayMetrics.heightPixels / displayMetrics.density) < 600;
    }
    
    @TargetApi(17)
    public static boolean zzh(final Context context) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final Display defaultDisplay = ((WindowManager)context.getSystemService("window")).getDefaultDisplay();
        Label_0104: {
            if (zzo.zzc()) {
                defaultDisplay.getRealMetrics(displayMetrics);
                final int n = displayMetrics.heightPixels;
                final int n2 = displayMetrics.widthPixels;
                break Label_0104;
            }
            try {
                final int n = (int)Display.class.getMethod("getRawHeight", (Class<?>[])new Class[0]).invoke(defaultDisplay, new Object[0]);
                final int n2 = (int)Display.class.getMethod("getRawWidth", (Class<?>[])new Class[0]).invoke(defaultDisplay, new Object[0]);
                defaultDisplay.getMetrics(displayMetrics);
                final int heightPixels = displayMetrics.heightPixels;
                final int widthPixels = displayMetrics.widthPixels;
                return heightPixels == n && widthPixels == n2;
            }
            catch (Exception ex) {
                return false;
            }
        }
    }
    
    public static int zzi(final Context context) {
        final int identifier = context.getResources().getIdentifier("navigation_bar_width", "dimen", "android");
        if (identifier > 0) {
            return context.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }
    
    public final void zza(final Context context, final String s, final String s2, final Bundle bundle, final boolean b) {
        zza(context, null, s2, bundle, true, new zzahu(this));
    }
    
    public final void zza(final ViewGroup viewGroup, final zzj zzj, final String s) {
        this.zza(viewGroup, zzj, s, -16777216, -1);
    }
    
    public final void zza(final ViewGroup viewGroup, final zzj zzj, final String s, final String s2) {
        zzaid.zze(s2);
        this.zza(viewGroup, zzj, s, -65536, -16777216);
    }
}
