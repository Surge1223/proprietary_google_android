package com.google.android.gms.internal;

import java.lang.reflect.Type;

public enum zzgiq
{
    zza(9, zzgis.zza, zzgji.zzj), 
    zzaa(21, zzgis.zzb, zzgji.zzc), 
    zzab(22, zzgis.zzb, zzgji.zzb), 
    zzac(23, zzgis.zzb, zzgji.zzc), 
    zzad(24, zzgis.zzb, zzgji.zzb), 
    zzae(25, zzgis.zzb, zzgji.zzf), 
    zzaf(26, zzgis.zzb, zzgji.zzg), 
    zzag(28, zzgis.zzb, zzgji.zzh), 
    zzah(29, zzgis.zzb, zzgji.zzb), 
    zzai(31, zzgis.zzb, zzgji.zzb), 
    zzaj(32, zzgis.zzb, zzgji.zzc), 
    zzak(33, zzgis.zzb, zzgji.zzb), 
    zzal(34, zzgis.zzb, zzgji.zzc), 
    zzam(35, zzgis.zzc, zzgji.zze), 
    zzan(36, zzgis.zzc, zzgji.zzd), 
    zzao(37, zzgis.zzc, zzgji.zzc), 
    zzap(38, zzgis.zzc, zzgji.zzc), 
    zzaq(39, zzgis.zzc, zzgji.zzb), 
    zzar(40, zzgis.zzc, zzgji.zzc), 
    zzas(41, zzgis.zzc, zzgji.zzb), 
    zzat(42, zzgis.zzc, zzgji.zzf), 
    zzau(43, zzgis.zzc, zzgji.zzb), 
    zzav(45, zzgis.zzc, zzgji.zzb), 
    zzaw(46, zzgis.zzc, zzgji.zzc), 
    zzax(47, zzgis.zzc, zzgji.zzb), 
    zzay(48, zzgis.zzc, zzgji.zzc), 
    zzb(12, zzgis.zza, zzgji.zzi);
    
    private static final zzgiq[] zzbe;
    private static final Type[] zzbf;
    
    zzc(17, zzgis.zza, zzgji.zzj), 
    zzd(18, zzgis.zzb, zzgji.zze), 
    zze(27, zzgis.zzb, zzgji.zzj), 
    zzf(30, zzgis.zzb, zzgji.zzi), 
    zzg(44, zzgis.zzc, zzgji.zzi), 
    zzh(49, zzgis.zzb, zzgji.zzj), 
    zzi(50, zzgis.zzd, zzgji.zza), 
    zzj(0, zzgis.zza, zzgji.zze), 
    zzk(1, zzgis.zza, zzgji.zzd), 
    zzl(2, zzgis.zza, zzgji.zzc), 
    zzm(3, zzgis.zza, zzgji.zzc), 
    zzn(4, zzgis.zza, zzgji.zzb), 
    zzo(5, zzgis.zza, zzgji.zzc), 
    zzp(6, zzgis.zza, zzgji.zzb), 
    zzq(7, zzgis.zza, zzgji.zzf), 
    zzr(8, zzgis.zza, zzgji.zzg), 
    zzs(10, zzgis.zza, zzgji.zzh), 
    zzt(11, zzgis.zza, zzgji.zzb), 
    zzu(13, zzgis.zza, zzgji.zzb), 
    zzv(14, zzgis.zza, zzgji.zzc), 
    zzw(15, zzgis.zza, zzgji.zzb), 
    zzx(16, zzgis.zza, zzgji.zzc), 
    zzy(19, zzgis.zzb, zzgji.zzd), 
    zzz(20, zzgis.zzb, zzgji.zzc);
    
    private final zzgji zzaz;
    private final int zzba;
    private final zzgis zzbb;
    private final Class<?> zzbc;
    private final boolean zzbd;
    
    static {
        final zzgiq zzj2 = zzgiq.zzj;
        int i = 0;
        zzbf = new Type[0];
        final zzgiq[] values = values();
        zzbe = new zzgiq[values.length];
        while (i < values.length) {
            final zzgiq zzgiq = values[i];
            com.google.android.gms.internal.zzgiq.zzbe[zzgiq.zzba] = zzgiq;
            ++i;
        }
    }
    
    private zzgiq(final int zzba, final zzgis zzbb, final zzgji zzaz) {
        this.zzba = zzba;
        this.zzbb = zzbb;
        this.zzaz = zzaz;
        switch (zzgir.zza[zzbb.ordinal()]) {
            default: {
                this.zzbc = null;
                break;
            }
            case 2: {
                this.zzbc = zzaz.zza();
                break;
            }
            case 1: {
                this.zzbc = zzaz.zza();
                break;
            }
        }
        boolean zzbd = false;
        if (zzbb == zzgis.zza) {
            switch (zzgir.zzb[zzaz.ordinal()]) {
                default: {
                    zzbd = true;
                    break;
                }
                case 1:
                case 2:
                case 3: {
                    zzbd = zzbd;
                    break;
                }
            }
        }
        this.zzbd = zzbd;
    }
    
    public final int zza() {
        return this.zzba;
    }
}
