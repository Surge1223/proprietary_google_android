package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzwk extends zzey implements zzwi
{
    zzwk(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.purchase.client.IPlayStorePurchaseListener");
    }
}
