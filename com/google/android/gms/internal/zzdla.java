package com.google.android.gms.internal;

import java.io.RandomAccessFile;
import java.io.File;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;
import android.os.ParcelFileDescriptor$AutoCloseInputStream;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzdla<T extends zzbih> implements Parcelable.Creator<T>
{
    private final Parcelable.Creator<T> zza;
    
    public zzdla(final Parcelable.Creator<T> zza) {
        this.zza = zza;
    }
    
    private final T zza(final Parcel parcel) {
        final ParcelFileDescriptor fileDescriptor = parcel.readFileDescriptor();
        try {
            final DataInputStream dataInputStream = new DataInputStream((InputStream)new ParcelFileDescriptor$AutoCloseInputStream(fileDescriptor));
            final byte[] array = new byte[dataInputStream.available()];
            dataInputStream.read(array);
            dataInputStream.close();
            fileDescriptor.close();
            return zzdld.zza(array, this.zza);
        }
        catch (IOException ex) {
            throw new zzdlc("Couldn't read from unlinked file.", ex);
        }
    }
    
    public static <T extends zzbih> void zza(final T t, final Parcel parcel, final zzdlb<T> zzdlb, File obtain) {
        obtain = (File)Parcel.obtain();
        try {
            try {
                zzdlb.zza(t, (Parcel)obtain);
                final byte[] marshall = ((Parcel)obtain).marshall();
                final File tempFile = File.createTempFile("teleporter", ".tmp", null);
                final RandomAccessFile randomAccessFile = new RandomAccessFile(tempFile, "rw");
                randomAccessFile.write(marshall);
                randomAccessFile.seek(0L);
                tempFile.delete();
                parcel.writeFileDescriptor(randomAccessFile.getFD());
                randomAccessFile.close();
                ((Parcel)obtain).recycle();
                return;
            }
            finally {}
        }
        catch (IOException ex) {
            throw new zzdlc("Couldn't write SafeParcelable to unlinked file.", ex);
        }
        ((Parcel)obtain).recycle();
    }
}
