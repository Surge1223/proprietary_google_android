package com.google.android.gms.internal;

final class zzgjw implements zzgkf
{
    @Override
    public final boolean zza(final Class<?> clazz) {
        return false;
    }
    
    @Override
    public final zzgke zzb(final Class<?> clazz) {
        throw new IllegalStateException("This should never be called.");
    }
}
