package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;
import java.util.SortedMap;
import java.util.Iterator;
import java.util.Collections;
import java.util.Map;
import java.util.List;
import java.util.AbstractMap;

class zzglb<K extends Comparable<K>, V> extends AbstractMap<K, V>
{
    private final int zza;
    private List<zzgli> zzb;
    private Map<K, V> zzc;
    private boolean zzd;
    private volatile zzglk zze;
    private Map<K, V> zzf;
    private volatile zzgle zzg;
    
    private zzglb(final int zza) {
        this.zza = zza;
        this.zzb = Collections.emptyList();
        this.zzc = Collections.emptyMap();
        this.zzf = Collections.emptyMap();
    }
    
    private final int zza(final K k) {
        int n = this.zzb.size() - 1;
        if (n >= 0) {
            final int compareTo = k.compareTo((K)this.zzb.get(n).getKey());
            if (compareTo > 0) {
                return -(n + 2);
            }
            if (compareTo == 0) {
                return n;
            }
        }
        int i = 0;
        while (i <= n) {
            final int n2 = (i + n) / 2;
            final int compareTo2 = k.compareTo((K)this.zzb.get(n2).getKey());
            if (compareTo2 < 0) {
                n = n2 - 1;
            }
            else {
                if (compareTo2 <= 0) {
                    return n2;
                }
                i = n2 + 1;
            }
        }
        return -(i + 1);
    }
    
    static <FieldDescriptorType extends zzgip<FieldDescriptorType>> zzglb<FieldDescriptorType, Object> zza(final int n) {
        return (zzglb<FieldDescriptorType, Object>)new zzglc(n);
    }
    
    private final V zzc(final int n) {
        this.zzf();
        final Object value = this.zzb.remove(n).getValue();
        if (!this.zzc.isEmpty()) {
            final Iterator<Map.Entry<K, V>> iterator = this.zzg().entrySet().iterator();
            this.zzb.add(new zzgli((Entry<Object, Object>)iterator.next()));
            iterator.remove();
        }
        return (V)value;
    }
    
    private final void zzf() {
        if (!this.zzd) {
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    private final SortedMap<K, V> zzg() {
        this.zzf();
        if (this.zzc.isEmpty() && !(this.zzc instanceof TreeMap)) {
            this.zzc = new TreeMap<K, V>();
            this.zzf = (Map<K, V>)((TreeMap)this.zzc).descendingMap();
        }
        return (SortedMap<K, V>)(SortedMap)this.zzc;
    }
    
    @Override
    public void clear() {
        this.zzf();
        if (!this.zzb.isEmpty()) {
            this.zzb.clear();
        }
        if (!this.zzc.isEmpty()) {
            this.zzc.clear();
        }
    }
    
    @Override
    public boolean containsKey(final Object o) {
        final Comparable comparable = (Comparable)o;
        return this.zza((K)comparable) >= 0 || this.zzc.containsKey(comparable);
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        if (this.zze == null) {
            this.zze = new zzglk(this, null);
        }
        return (Set<Entry<K, V>>)this.zze;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzglb)) {
            return super.equals(o);
        }
        final zzglb zzglb = (zzglb)o;
        final int size = this.size();
        if (size != zzglb.size()) {
            return false;
        }
        final int zzc = this.zzc();
        if (zzc != zzglb.zzc()) {
            return this.entrySet().equals(zzglb.entrySet());
        }
        for (int i = 0; i < zzc; ++i) {
            if (!this.zzb(i).equals(zzglb.zzb(i))) {
                return false;
            }
        }
        return zzc == size || this.zzc.equals(zzglb.zzc);
    }
    
    @Override
    public V get(final Object o) {
        final Comparable comparable = (Comparable)o;
        final int zza = this.zza((K)comparable);
        if (zza >= 0) {
            return (V)this.zzb.get(zza).getValue();
        }
        return this.zzc.get(comparable);
    }
    
    @Override
    public int hashCode() {
        final int zzc = this.zzc();
        int i = 0;
        int n = 0;
        while (i < zzc) {
            n += this.zzb.get(i).hashCode();
            ++i;
        }
        int n2 = n;
        if (this.zzc.size() > 0) {
            n2 = n + this.zzc.hashCode();
        }
        return n2;
    }
    
    @Override
    public V remove(final Object o) {
        this.zzf();
        final Comparable comparable = (Comparable)o;
        final int zza = this.zza((K)comparable);
        if (zza >= 0) {
            return this.zzc(zza);
        }
        if (this.zzc.isEmpty()) {
            return null;
        }
        return this.zzc.remove(comparable);
    }
    
    @Override
    public int size() {
        return this.zzb.size() + this.zzc.size();
    }
    
    public final V zza(final K k, final V value) {
        this.zzf();
        final int zza = this.zza(k);
        if (zza >= 0) {
            return (V)this.zzb.get(zza).setValue(value);
        }
        this.zzf();
        if (this.zzb.isEmpty() && !(this.zzb instanceof ArrayList)) {
            this.zzb = new ArrayList<zzgli>(this.zza);
        }
        final int n = -(zza + 1);
        if (n >= this.zza) {
            return this.zzg().put(k, value);
        }
        if (this.zzb.size() == this.zza) {
            final zzgli zzgli = this.zzb.remove(this.zza - 1);
            this.zzg().put((K)zzgli.getKey(), (V)zzgli.getValue());
        }
        this.zzb.add(n, new zzgli(this, k, value));
        return null;
    }
    
    public void zza() {
        if (!this.zzd) {
            Map<K, V> zzc;
            if (this.zzc.isEmpty()) {
                zzc = Collections.emptyMap();
            }
            else {
                zzc = Collections.unmodifiableMap((Map<? extends K, ? extends V>)this.zzc);
            }
            this.zzc = zzc;
            Map<K, V> zzf;
            if (this.zzf.isEmpty()) {
                zzf = Collections.emptyMap();
            }
            else {
                zzf = Collections.unmodifiableMap((Map<? extends K, ? extends V>)this.zzf);
            }
            this.zzf = zzf;
            this.zzd = true;
        }
    }
    
    public final Entry<K, V> zzb(final int n) {
        return (Entry<K, V>)this.zzb.get(n);
    }
    
    public final boolean zzb() {
        return this.zzd;
    }
    
    public final int zzc() {
        return this.zzb.size();
    }
    
    public final Iterable<Entry<K, V>> zzd() {
        if (this.zzc.isEmpty()) {
            return zzglf.zza();
        }
        return this.zzc.entrySet();
    }
    
    final Set<Entry<K, V>> zze() {
        if (this.zzg == null) {
            this.zzg = new zzgle(this, null);
        }
        return (Set<Entry<K, V>>)this.zzg;
    }
}
