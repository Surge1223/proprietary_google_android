package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.Parcel;
import android.app.PendingIntent;
import com.google.android.gms.common.internal.zzax;
import com.google.android.gms.common.ConnectionResult;
import android.os.Parcelable.Creator;

public final class zzegv extends zzbid
{
    public static final Parcelable.Creator<zzegv> CREATOR;
    private final int zza;
    private final ConnectionResult zzb;
    private final zzax zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzegw();
    }
    
    public zzegv(final int n) {
        this(new ConnectionResult(8, null), null);
    }
    
    zzegv(final int zza, final ConnectionResult zzb, final zzax zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    private zzegv(final ConnectionResult connectionResult, final zzax zzax) {
        this(1, connectionResult, null);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, zza);
    }
    
    public final ConnectionResult zza() {
        return this.zzb;
    }
    
    public final zzax zzb() {
        return this.zzc;
    }
}
