package com.google.android.gms.internal;

import android.os.Parcelable;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;

public final class zzadg extends zzey implements zzade
{
    zzadg(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(1, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeInt(n);
        this.zzb(2, a_);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final zzadi zzadi) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        zzfa.zza(a_, (Parcelable)zzadi);
        this.zzb(7, a_);
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(3, a_);
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper, final int n) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        a_.writeInt(n);
        this.zzb(9, a_);
    }
    
    @Override
    public final void zzc(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(4, a_);
    }
    
    @Override
    public final void zzd(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(5, a_);
    }
    
    @Override
    public final void zze(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(6, a_);
    }
    
    @Override
    public final void zzf(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(8, a_);
    }
    
    @Override
    public final void zzg(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(10, a_);
    }
    
    @Override
    public final void zzh(final IObjectWrapper objectWrapper) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)objectWrapper);
        this.zzb(11, a_);
    }
}
