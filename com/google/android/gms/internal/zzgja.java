package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class zzgja
{
    static final Charset zza;
    public static final byte[] zzb;
    private static final Charset zzc;
    private static final ByteBuffer zzd;
    private static final zzghx zze;
    
    static {
        zza = Charset.forName("UTF-8");
        zzc = Charset.forName("ISO-8859-1");
        zzd = ByteBuffer.wrap(zzb = new byte[0]);
        final byte[] zzb2 = zzgja.zzb;
        zze = zzghx.zza(zzb2, 0, zzb2.length, false);
    }
    
    static int zza(int i, final byte[] array, final int n, final int n2) {
        int n3 = i;
        for (i = n; i < n + n2; ++i) {
            n3 = n3 * 31 + array[i];
        }
        return n3;
    }
    
    public static int zza(final long n) {
        return (int)(n ^ n >>> 32);
    }
    
    public static int zza(final boolean b) {
        if (b) {
            return 1231;
        }
        return 1237;
    }
    
    static <T> T zza(final T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }
    
    static Object zza(final Object o, final Object o2) {
        return ((zzgkg)o).zzp().zza((zzgkg)o2).zze();
    }
    
    static <T> T zza(final T t, final String s) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(s);
    }
    
    static boolean zza(final zzgkg zzgkg) {
        return false;
    }
    
    public static boolean zza(final byte[] array) {
        return zzgly.zza(array);
    }
    
    public static String zzb(final byte[] array) {
        return new String(array, zzgja.zza);
    }
    
    public static int zzc(final byte[] array) {
        final int length = array.length;
        final int zza = zza(length, array, 0, length);
        if (zza == 0) {
            return 1;
        }
        return zza;
    }
}
