package com.google.android.gms.internal;

import java.util.List;

public interface zzgjp extends List
{
    Object zzb(final int p0);
    
    List<?> zzd();
}
