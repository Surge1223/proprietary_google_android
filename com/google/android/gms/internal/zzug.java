package com.google.android.gms.internal;

import android.location.Location;
import java.util.Set;
import java.util.Date;
import com.google.android.gms.ads.mediation.MediationAdRequest;

public final class zzug implements MediationAdRequest
{
    private final Date zza;
    private final int zzb;
    private final Set<String> zzc;
    private final boolean zzd;
    private final Location zze;
    private final int zzf;
    private final boolean zzg;
    
    public zzug(final Date zza, final int zzb, final Set<String> zzc, final Location zze, final boolean zzd, final int zzf, final boolean zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zze = zze;
        this.zzd = zzd;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    @Override
    public final Date getBirthday() {
        return this.zza;
    }
    
    @Override
    public final int getGender() {
        return this.zzb;
    }
    
    @Override
    public final Set<String> getKeywords() {
        return this.zzc;
    }
    
    @Override
    public final Location getLocation() {
        return this.zze;
    }
    
    @Override
    public final boolean isDesignedForFamilies() {
        return this.zzg;
    }
    
    @Override
    public final boolean isTesting() {
        return this.zzd;
    }
    
    @Override
    public final int taggedForChildDirectedTreatment() {
        return this.zzf;
    }
}
