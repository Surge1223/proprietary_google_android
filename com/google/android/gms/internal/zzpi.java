package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;

public final class zzpi extends zzey implements zzpg
{
    zzpi(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.formats.client.IOnCustomTemplateAdLoadedListener");
    }
    
    @Override
    public final void zza(final zzot zzot) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzot);
        this.zzb(1, a_);
    }
}
