package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzgnk extends zzgmo<zzgnk>
{
    private static volatile zzgnk[] zza;
    private String zzb;
    private boolean[] zzc;
    private long[] zzd;
    private String[] zze;
    private zzgnl[] zzf;
    private byte[] zzg;
    private double[] zzh;
    
    public zzgnk() {
        this.zzb = "";
        this.zzc = zzgmx.zze;
        this.zzd = zzgmx.zzb;
        this.zze = zzgmx.zzf;
        this.zzf = zzgnl.zza();
        this.zzg = zzgmx.zzh;
        this.zzh = zzgmx.zzd;
        this.zzax = null;
        this.zzay = -1;
    }
    
    public static zzgnk[] zza() {
        if (zzgnk.zza == null) {
            synchronized (zzgms.zzb) {
                if (zzgnk.zza == null) {
                    zzgnk.zza = new zzgnk[0];
                }
            }
        }
        return zzgnk.zza;
    }
    
    @Override
    protected final int computeSerializedSize() {
        int computeSerializedSize;
        final int n = computeSerializedSize = super.computeSerializedSize();
        if (this.zzb != null) {
            computeSerializedSize = n;
            if (!this.zzb.equals("")) {
                computeSerializedSize = n + zzgmm.zzb(1, this.zzb);
            }
        }
        int n2 = computeSerializedSize;
        if (this.zzc != null) {
            n2 = computeSerializedSize;
            if (this.zzc.length > 0) {
                n2 = computeSerializedSize + this.zzc.length * 1 + this.zzc.length * 1;
            }
        }
        final long[] zzd = this.zzd;
        final int n3 = 0;
        int n4 = n2;
        if (zzd != null) {
            n4 = n2;
            if (this.zzd.length > 0) {
                int n5;
                for (int i = n5 = 0; i < this.zzd.length; ++i) {
                    n5 += zzgmm.zzb(this.zzd[i]);
                }
                n4 = n2 + n5 + this.zzd.length * 1;
            }
        }
        int n6 = n4;
        if (this.zze != null) {
            n6 = n4;
            if (this.zze.length > 0) {
                int j = 0;
                int n8;
                int n7 = n8 = j;
                while (j < this.zze.length) {
                    final String s = this.zze[j];
                    int n9 = n7;
                    int n10 = n8;
                    if (s != null) {
                        n10 = n8 + 1;
                        n9 = n7 + zzgmm.zza(s);
                    }
                    ++j;
                    n7 = n9;
                    n8 = n10;
                }
                n6 = n4 + n7 + n8 * 1;
            }
        }
        int n11 = n6;
        if (this.zzf != null) {
            n11 = n6;
            if (this.zzf.length > 0) {
                int n12 = n3;
                while (true) {
                    n11 = n6;
                    if (n12 >= this.zzf.length) {
                        break;
                    }
                    final zzgnl zzgnl = this.zzf[n12];
                    int n13 = n6;
                    if (zzgnl != null) {
                        n13 = n6 + zzgmm.zzb(5, zzgnl);
                    }
                    ++n12;
                    n6 = n13;
                }
            }
        }
        int n14 = n11;
        if (!Arrays.equals(this.zzg, zzgmx.zzh)) {
            n14 = n11 + zzgmm.zzb(6, this.zzg);
        }
        int n15 = n14;
        if (this.zzh != null) {
            n15 = n14;
            if (this.zzh.length > 0) {
                n15 = n14 + 8 * this.zzh.length + 1 * this.zzh.length;
            }
        }
        return n15;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzgnk)) {
            return false;
        }
        final zzgnk zzgnk = (zzgnk)o;
        if (this.zzb == null) {
            if (zzgnk.zzb != null) {
                return false;
            }
        }
        else if (!this.zzb.equals(zzgnk.zzb)) {
            return false;
        }
        if (!zzgms.zza(this.zzc, zzgnk.zzc)) {
            return false;
        }
        if (!zzgms.zza(this.zzd, zzgnk.zzd)) {
            return false;
        }
        if (!zzgms.zza(this.zze, zzgnk.zze)) {
            return false;
        }
        if (!zzgms.zza(this.zzf, zzgnk.zzf)) {
            return false;
        }
        if (!Arrays.equals(this.zzg, zzgnk.zzg)) {
            return false;
        }
        if (!zzgms.zza(this.zzh, zzgnk.zzh)) {
            return false;
        }
        if (this.zzax != null && !this.zzax.zzb()) {
            return this.zzax.equals(zzgnk.zzax);
        }
        return zzgnk.zzax == null || zzgnk.zzax.zzb();
    }
    
    @Override
    public final int hashCode() {
        final int hashCode = this.getClass().getName().hashCode();
        final String zzb = this.zzb;
        int hashCode2 = 0;
        int hashCode3;
        if (zzb == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = this.zzb.hashCode();
        }
        final int zza = zzgms.zza(this.zzc);
        final int zza2 = zzgms.zza(this.zzd);
        final int zza3 = zzgms.zza(this.zze);
        final int zza4 = zzgms.zza(this.zzf);
        final int hashCode4 = Arrays.hashCode(this.zzg);
        final int zza5 = zzgms.zza(this.zzh);
        if (this.zzax != null) {
            if (!this.zzax.zzb()) {
                hashCode2 = this.zzax.hashCode();
            }
        }
        return ((((((((527 + hashCode) * 31 + hashCode3) * 31 + zza) * 31 + zza2) * 31 + zza3) * 31 + zza4) * 31 + hashCode4) * 31 + zza5) * 31 + hashCode2;
    }
    
    @Override
    public final void writeTo(final zzgmm zzgmm) throws IOException {
        if (this.zzb != null && !this.zzb.equals("")) {
            zzgmm.zza(1, this.zzb);
        }
        final boolean[] zzc = this.zzc;
        final int n = 0;
        if (zzc != null && this.zzc.length > 0) {
            for (int i = 0; i < this.zzc.length; ++i) {
                zzgmm.zza(2, this.zzc[i]);
            }
        }
        if (this.zzd != null && this.zzd.length > 0) {
            for (int j = 0; j < this.zzd.length; ++j) {
                zzgmm.zzb(3, this.zzd[j]);
            }
        }
        if (this.zze != null && this.zze.length > 0) {
            for (int k = 0; k < this.zze.length; ++k) {
                final String s = this.zze[k];
                if (s != null) {
                    zzgmm.zza(4, s);
                }
            }
        }
        if (this.zzf != null && this.zzf.length > 0) {
            for (int l = 0; l < this.zzf.length; ++l) {
                final zzgnl zzgnl = this.zzf[l];
                if (zzgnl != null) {
                    zzgmm.zza(5, zzgnl);
                }
            }
        }
        if (!Arrays.equals(this.zzg, zzgmx.zzh)) {
            zzgmm.zza(6, this.zzg);
        }
        if (this.zzh != null && this.zzh.length > 0) {
            for (int n2 = n; n2 < this.zzh.length; ++n2) {
                zzgmm.zza(7, this.zzh[n2]);
            }
        }
        super.writeTo(zzgmm);
    }
}
