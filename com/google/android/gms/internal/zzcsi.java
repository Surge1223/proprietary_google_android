package com.google.android.gms.internal;

import java.io.File;
import android.os.Parcelable;
import com.google.android.gms.mdh.MdhFootprint;
import java.util.List;
import android.os.Parcelable.Creator;
import android.os.Parcel;

final class zzcsi implements zzdlb
{
    private final int zza;
    
    zzcsi(final int zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzbih zzbih, final Parcel parcel) {
        zzcsh.zza(this.zza, (zzcsh)zzbih, parcel);
    }
}
