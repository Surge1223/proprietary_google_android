package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzbh;
import java.util.Iterator;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import android.view.View;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.mediation.zza;

public final class zzvb extends zzud
{
    private final zza zza;
    
    public zzvb(final zza zza) {
        this.zza = zza;
    }
    
    @Override
    public final String zza() {
        return this.zza.zza();
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper) {
        zzn.zza(objectWrapper);
    }
    
    @Override
    public final void zza(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) {
        this.zza.zza(zzn.zza(objectWrapper), zzn.zza(objectWrapper2), zzn.zza(objectWrapper3));
    }
    
    @Override
    public final List zzb() {
        final List<NativeAd.Image> zzb = this.zza.zzb();
        final ArrayList<zzmo> list = new ArrayList<zzmo>();
        if (zzb != null) {
            for (final NativeAd.Image image : zzb) {
                list.add(new zzmo(image.getDrawable(), image.getUri(), image.getScale()));
            }
        }
        return list;
    }
    
    @Override
    public final void zzb(final IObjectWrapper objectWrapper) {
        zzn.zza(objectWrapper);
    }
    
    @Override
    public final String zzc() {
        return this.zza.zzc();
    }
    
    @Override
    public final zznx zzd() {
        final NativeAd.Image zzd = this.zza.zzd();
        if (zzd != null) {
            return new zzmo(zzd.getDrawable(), zzd.getUri(), zzd.getScale());
        }
        return null;
    }
    
    @Override
    public final String zze() {
        return this.zza.zze();
    }
    
    @Override
    public final String zzf() {
        return this.zza.zzf();
    }
    
    @Override
    public final double zzg() {
        if (this.zza.zzg() != null) {
            return this.zza.zzg();
        }
        return -1.0;
    }
    
    @Override
    public final String zzh() {
        return this.zza.zzh();
    }
    
    @Override
    public final String zzi() {
        return this.zza.zzi();
    }
    
    @Override
    public final zzbh zzj() {
        if (this.zza.zzj() != null) {
            return this.zza.zzj().zza();
        }
        return null;
    }
    
    @Override
    public final zznt zzk() {
        return null;
    }
    
    @Override
    public final IObjectWrapper zzl() {
        return null;
    }
    
    @Override
    public final IObjectWrapper zzm() {
        return null;
    }
    
    @Override
    public final IObjectWrapper zzn() {
        final Object zzk = this.zza.zzk();
        if (zzk == null) {
            return null;
        }
        return zzn.zza(zzk);
    }
    
    @Override
    public final Bundle zzo() {
        return this.zza.zzl();
    }
    
    @Override
    public final boolean zzp() {
        return this.zza.zzm();
    }
    
    @Override
    public final boolean zzq() {
        return this.zza.zzn();
    }
    
    @Override
    public final void zzr() {
    }
}
