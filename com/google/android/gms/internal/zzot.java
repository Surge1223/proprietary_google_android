package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbh;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzot extends IInterface
{
    String zza(final String p0) throws RemoteException;
    
    List<String> zza() throws RemoteException;
    
    boolean zza(final IObjectWrapper p0) throws RemoteException;
    
    IObjectWrapper zzb() throws RemoteException;
    
    zznx zzb(final String p0) throws RemoteException;
    
    zzbh zzc() throws RemoteException;
    
    void zzc(final String p0) throws RemoteException;
    
    void zzd() throws RemoteException;
    
    IObjectWrapper zze() throws RemoteException;
    
    void zzf() throws RemoteException;
    
    String zzl() throws RemoteException;
}
