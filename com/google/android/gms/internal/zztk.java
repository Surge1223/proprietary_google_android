package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zztk extends IInterface
{
    zztn zza(final String p0) throws RemoteException;
    
    boolean zzb(final String p0) throws RemoteException;
}
