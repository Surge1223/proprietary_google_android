package com.google.android.gms.internal;

import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import android.location.Location;
import java.util.Set;
import java.util.Date;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;

public final class zzul implements NativeMediationAdRequest
{
    private final Date zza;
    private final int zzb;
    private final Set<String> zzc;
    private final boolean zzd;
    private final Location zze;
    private final int zzf;
    private final zznm zzg;
    private final List<String> zzh;
    private final boolean zzi;
    private final Map<String, Boolean> zzj;
    
    public zzul(final Date zza, final int zzb, final Set<String> zzc, final Location zze, final boolean zzd, final int zzf, final zznm zzg, final List<String> list, final boolean zzi) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zze = zze;
        this.zzd = zzd;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzi = zzi;
        this.zzh = new ArrayList<String>();
        this.zzj = new HashMap<String, Boolean>();
        if (list != null) {
            for (final String s : list) {
                if (s.startsWith("custom:")) {
                    final String[] split = s.split(":", 3);
                    if (split.length != 3) {
                        continue;
                    }
                    if ("true".equals(split[2])) {
                        this.zzj.put(split[1], true);
                    }
                    else {
                        if (!"false".equals(split[2])) {
                            continue;
                        }
                        this.zzj.put(split[1], false);
                    }
                }
                else {
                    this.zzh.add(s);
                }
            }
        }
    }
    
    @Override
    public final Date getBirthday() {
        return this.zza;
    }
    
    @Override
    public final int getGender() {
        return this.zzb;
    }
    
    @Override
    public final Set<String> getKeywords() {
        return this.zzc;
    }
    
    @Override
    public final Location getLocation() {
        return this.zze;
    }
    
    @Override
    public final NativeAdOptions getNativeAdOptions() {
        if (this.zzg == null) {
            return null;
        }
        final NativeAdOptions.Builder setRequestMultipleImages = new NativeAdOptions.Builder().setReturnUrlsForImageAssets(this.zzg.zzb).setImageOrientation(this.zzg.zzc).setRequestMultipleImages(this.zzg.zzd);
        if (this.zzg.zza >= 2) {
            setRequestMultipleImages.setAdChoicesPlacement(this.zzg.zze);
        }
        if (this.zzg.zza >= 3 && this.zzg.zzf != null) {
            setRequestMultipleImages.setVideoOptions(new VideoOptions(this.zzg.zzf));
        }
        return setRequestMultipleImages.build();
    }
    
    @Override
    public final boolean isAppInstallAdRequested() {
        return this.zzh != null && this.zzh.contains("2");
    }
    
    @Override
    public final boolean isContentAdRequested() {
        return this.zzh != null && this.zzh.contains("1");
    }
    
    @Override
    public final boolean isDesignedForFamilies() {
        return this.zzi;
    }
    
    @Override
    public final boolean isTesting() {
        return this.zzd;
    }
    
    @Override
    public final int taggedForChildDirectedTreatment() {
        return this.zzf;
    }
    
    @Override
    public final boolean zza() {
        return this.zzh != null && this.zzh.contains("6");
    }
    
    @Override
    public final boolean zzb() {
        return this.zzh != null && this.zzh.contains("3");
    }
    
    @Override
    public final Map<String, Boolean> zzc() {
        return this.zzj;
    }
}
