package com.google.android.gms.internal;

import android.os.RemoteException;
import android.os.IInterface;

public interface zzpg extends IInterface
{
    void zza(final zzot p0) throws RemoteException;
}
