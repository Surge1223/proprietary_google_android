package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzbjj extends zzbid
{
    public static final Parcelable.Creator<zzbjj> CREATOR;
    final String zza;
    final int zzb;
    private final int zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzbjl();
    }
    
    zzbjj(final int zzc, final String zza, final int zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    zzbjj(final String zza, final int zzb) {
        this.zzc = 1;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzc);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, zza);
    }
}
