package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Bundle;
import java.util.List;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.os.IInterface;

public interface zztz extends IInterface
{
    String zza() throws RemoteException;
    
    void zza(final IObjectWrapper p0) throws RemoteException;
    
    void zza(final IObjectWrapper p0, final IObjectWrapper p1, final IObjectWrapper p2) throws RemoteException;
    
    List zzb() throws RemoteException;
    
    void zzb(final IObjectWrapper p0) throws RemoteException;
    
    String zzc() throws RemoteException;
    
    void zzc(final IObjectWrapper p0) throws RemoteException;
    
    zznx zzd() throws RemoteException;
    
    String zze() throws RemoteException;
    
    String zzf() throws RemoteException;
    
    void zzg() throws RemoteException;
    
    boolean zzh() throws RemoteException;
    
    boolean zzi() throws RemoteException;
    
    Bundle zzj() throws RemoteException;
    
    IObjectWrapper zzk() throws RemoteException;
    
    zzbh zzl() throws RemoteException;
    
    zznt zzm() throws RemoteException;
    
    IObjectWrapper zzn() throws RemoteException;
    
    IObjectWrapper zzo() throws RemoteException;
}
