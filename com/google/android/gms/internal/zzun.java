package com.google.android.gms.internal;

import com.google.ads.mediation.MediationInterstitialAdapter;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;

public final class zzun<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener
{
    private final zztq zza;
    
    public zzun(final zztq zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onFailedToReceiveAd(final MediationBannerAdapter<?, ?> mediationBannerAdapter, final AdRequest.ErrorCode errorCode) {
        final String value = String.valueOf(errorCode);
        final StringBuilder sb = new StringBuilder(47 + String.valueOf(value).length());
        sb.append("Adapter called onFailedToReceiveAd with error. ");
        sb.append(value);
        zzaid.zzb(sb.toString());
        zzx.zza();
        if (!zzaht.zzb()) {
            zzaid.zze("onFailedToReceiveAd must be called on the main UI thread.");
            zzaht.zza.post((Runnable)new zzus(this, errorCode));
            return;
        }
        try {
            this.zza.zza(zzuz.zza(errorCode));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
    
    @Override
    public final void onFailedToReceiveAd(final MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, final AdRequest.ErrorCode errorCode) {
        final String value = String.valueOf(errorCode);
        final StringBuilder sb = new StringBuilder(47 + String.valueOf(value).length());
        sb.append("Adapter called onFailedToReceiveAd with error ");
        sb.append(value);
        sb.append(".");
        zzaid.zzb(sb.toString());
        zzx.zza();
        if (!zzaht.zzb()) {
            zzaid.zze("onFailedToReceiveAd must be called on the main UI thread.");
            zzaht.zza.post((Runnable)new zzux(this, errorCode));
            return;
        }
        try {
            this.zza.zza(zzuz.zza(errorCode));
        }
        catch (RemoteException ex) {
            zzaid.zzc("Could not call onAdFailedToLoad.", (Throwable)ex);
        }
    }
}
