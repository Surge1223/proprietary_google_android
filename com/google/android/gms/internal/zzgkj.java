package com.google.android.gms.internal;

import java.util.Iterator;
import java.lang.reflect.Modifier;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.List;
import java.util.TreeSet;
import java.lang.reflect.Method;
import java.util.HashMap;

final class zzgkj
{
    static String zza(final zzgkg zzgkg, final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(s);
        zza(zzgkg, sb, 0);
        return sb.toString();
    }
    
    private static final String zza(final String s) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (Character.isUpperCase(char1)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(char1));
        }
        return sb.toString();
    }
    
    private static void zza(final zzgkg zzgkg, final StringBuilder sb, final int n) {
        final HashMap<Object, Method> hashMap = new HashMap<Object, Method>();
        final HashMap<String, Method> hashMap2 = new HashMap<String, Method>();
        final TreeSet<String> set = new TreeSet<String>();
        for (final Method method : zzgkg.getClass().getDeclaredMethods()) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    set.add(method.getName());
                }
            }
        }
        for (final String s : set) {
            final String replaceFirst = s.replaceFirst("get", "");
            if (replaceFirst.endsWith("List") && !replaceFirst.endsWith("OrBuilderList")) {
                final String value = String.valueOf(replaceFirst.substring(0, 1).toLowerCase());
                final String value2 = String.valueOf(replaceFirst.substring(1, replaceFirst.length() - 4));
                String concat;
                if (value2.length() != 0) {
                    concat = value.concat(value2);
                }
                else {
                    concat = new String(value);
                }
                final Method method2 = hashMap.get(s);
                if (method2 != null && method2.getReturnType().equals(List.class)) {
                    zza(sb, n, zza(concat), zzgiw.zza(method2, zzgkg, new Object[0]));
                    continue;
                }
            }
            if (replaceFirst.endsWith("Map")) {
                final String value3 = String.valueOf(replaceFirst.substring(0, 1).toLowerCase());
                final String value4 = String.valueOf(replaceFirst.substring(1, replaceFirst.length() - 3));
                String concat2;
                if (value4.length() != 0) {
                    concat2 = value3.concat(value4);
                }
                else {
                    concat2 = new String(value3);
                }
                final Method method3 = hashMap.get(s);
                if (method3 != null && method3.getReturnType().equals(Map.class) && !method3.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method3.getModifiers())) {
                    zza(sb, n, zza(concat2), zzgiw.zza(method3, zzgkg, new Object[0]));
                    continue;
                }
            }
            final String value5 = String.valueOf(replaceFirst);
            String concat3;
            if (value5.length() != 0) {
                concat3 = "set".concat(value5);
            }
            else {
                concat3 = new String("set");
            }
            if (hashMap2.get(concat3) != null) {
                if (replaceFirst.endsWith("Bytes")) {
                    final String value6 = String.valueOf(replaceFirst.substring(0, replaceFirst.length() - 5));
                    String concat4;
                    if (value6.length() != 0) {
                        concat4 = "get".concat(value6);
                    }
                    else {
                        concat4 = new String("get");
                    }
                    if (hashMap.containsKey(concat4)) {
                        continue;
                    }
                }
                final String value7 = String.valueOf(replaceFirst.substring(0, 1).toLowerCase());
                final String value8 = String.valueOf(replaceFirst.substring(1));
                String concat5;
                if (value8.length() != 0) {
                    concat5 = value7.concat(value8);
                }
                else {
                    concat5 = new String(value7);
                }
                final String value9 = String.valueOf(replaceFirst);
                String concat6;
                if (value9.length() != 0) {
                    concat6 = "get".concat(value9);
                }
                else {
                    concat6 = new String("get");
                }
                final Method method4 = hashMap.get(concat6);
                final String value10 = String.valueOf(replaceFirst);
                String concat7;
                if (value10.length() != 0) {
                    concat7 = "has".concat(value10);
                }
                else {
                    concat7 = new String("has");
                }
                final Method method5 = hashMap.get(concat7);
                if (method4 == null) {
                    continue;
                }
                final Object zza = zzgiw.zza(method4, zzgkg, new Object[0]);
                boolean booleanValue;
                if (method5 == null) {
                    boolean b = false;
                    Label_0968: {
                        Label_0785: {
                            if (zza instanceof Boolean) {
                                if (zza) {
                                    break Label_0785;
                                }
                            }
                            else if (zza instanceof Integer) {
                                if ((int)zza != 0) {
                                    break Label_0785;
                                }
                            }
                            else if (zza instanceof Float) {
                                if ((float)zza != 0.0f) {
                                    break Label_0785;
                                }
                            }
                            else if (zza instanceof Double) {
                                if ((double)zza != 0.0) {
                                    break Label_0785;
                                }
                            }
                            else {
                                if (zza instanceof String) {
                                    b = zza.equals("");
                                    break Label_0968;
                                }
                                if (zza instanceof zzgho) {
                                    b = zza.equals(zzgho.zza);
                                    break Label_0968;
                                }
                                if (zza instanceof zzgkg) {
                                    if (zza != ((zzgkg)zza).zzr()) {
                                        break Label_0785;
                                    }
                                }
                                else {
                                    if (!(zza instanceof Enum)) {
                                        break Label_0785;
                                    }
                                    if (((Enum)zza).ordinal() != 0) {
                                        break Label_0785;
                                    }
                                }
                            }
                            b = true;
                            break Label_0968;
                        }
                        b = false;
                    }
                    booleanValue = !b;
                }
                else {
                    booleanValue = (boolean)zzgiw.zza(method5, zzgkg, new Object[0]);
                }
                if (!booleanValue) {
                    continue;
                }
                zza(sb, n, zza(concat5), zza);
            }
        }
        if (zzgkg instanceof zzgiw.zzd) {
            final Iterator<Map.Entry<zzgiw.zze, Object>> zze = ((zzgiw.zzd)zzgkg).zzd.zze();
            while (zze.hasNext()) {
                final Map.Entry<zzgiw.zze, Object> entry = zze.next();
                final int zzb = entry.getKey().zzb;
                final StringBuilder sb2 = new StringBuilder(13);
                sb2.append("[");
                sb2.append(zzb);
                sb2.append("]");
                zza(sb, n, sb2.toString(), entry.getValue());
            }
        }
        final zzgiw zzgiw = (zzgiw)zzgkg;
        if (zzgiw.zzb != null) {
            zzgiw.zzb.zza(sb, n);
        }
    }
    
    static final void zza(final StringBuilder sb, final int n, final String s, final Object o) {
        if (o instanceof List) {
            final Iterator<Object> iterator = (Iterator<Object>)((List)o).iterator();
            while (iterator.hasNext()) {
                zza(sb, n, s, iterator.next());
            }
            return;
        }
        if (o instanceof Map) {
            final Iterator<Map.Entry<?, ?>> iterator2 = ((Map)o).entrySet().iterator();
            while (iterator2.hasNext()) {
                zza(sb, n, s, iterator2.next());
            }
            return;
        }
        sb.append('\n');
        final boolean b = false;
        final boolean b2 = false;
        for (int i = 0; i < n; ++i) {
            sb.append(' ');
        }
        sb.append(s);
        if (o instanceof String) {
            sb.append(": \"");
            sb.append(zzglm.zza(zzgho.zza((String)o)));
            sb.append('\"');
            return;
        }
        if (o instanceof zzgho) {
            sb.append(": \"");
            sb.append(zzglm.zza((zzgho)o));
            sb.append('\"');
            return;
        }
        if (o instanceof zzgiw) {
            sb.append(" {");
            zza((zzgkg)o, sb, n + 2);
            sb.append("\n");
            for (int j = b2 ? 1 : 0; j < n; ++j) {
                sb.append(' ');
            }
            sb.append("}");
            return;
        }
        if (o instanceof Map.Entry) {
            sb.append(" {");
            final Map.Entry entry = (Map.Entry)o;
            final int n2 = n + 2;
            zza(sb, n2, "key", entry.getKey());
            zza(sb, n2, "value", entry.getValue());
            sb.append("\n");
            for (int k = b ? 1 : 0; k < n; ++k) {
                sb.append(' ');
            }
            sb.append("}");
            return;
        }
        sb.append(": ");
        sb.append(o.toString());
    }
}
