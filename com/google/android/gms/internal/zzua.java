package com.google.android.gms.internal;

import android.os.RemoteException;
import java.util.List;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzbh;
import android.os.Parcelable;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IInterface;

public abstract class zzua extends zzez implements zztz
{
    public zzua() {
        this.attachInterface((IInterface)this, "com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 22: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()), IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 21: {
                final IObjectWrapper zzo = this.zzo();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzo);
                break;
            }
            case 20: {
                final IObjectWrapper zzn = this.zzn();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzn);
                break;
            }
            case 19: {
                final zznt zzm = this.zzm();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzm);
                break;
            }
            case 16: {
                final zzbh zzl = this.zzl();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzl);
                break;
            }
            case 15: {
                final IObjectWrapper zzk = this.zzk();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzk);
                break;
            }
            case 14: {
                this.zzc(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 13: {
                final Bundle zzj = this.zzj();
                parcel2.writeNoException();
                zzfa.zzb(parcel2, (Parcelable)zzj);
                break;
            }
            case 12: {
                final boolean zzi = this.zzi();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzi);
                break;
            }
            case 11: {
                final boolean zzh = this.zzh();
                parcel2.writeNoException();
                zzfa.zza(parcel2, zzh);
                break;
            }
            case 10: {
                this.zzb(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 9: {
                this.zza(IObjectWrapper.zza.zza(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            }
            case 8: {
                this.zzg();
                parcel2.writeNoException();
                break;
            }
            case 7: {
                final String zzf = this.zzf();
                parcel2.writeNoException();
                parcel2.writeString(zzf);
                break;
            }
            case 6: {
                final String zze = this.zze();
                parcel2.writeNoException();
                parcel2.writeString(zze);
                break;
            }
            case 5: {
                final zznx zzd = this.zzd();
                parcel2.writeNoException();
                zzfa.zza(parcel2, (IInterface)zzd);
                break;
            }
            case 4: {
                final String zzc = this.zzc();
                parcel2.writeNoException();
                parcel2.writeString(zzc);
                break;
            }
            case 3: {
                final List zzb = this.zzb();
                parcel2.writeNoException();
                parcel2.writeList(zzb);
                break;
            }
            case 2: {
                final String zza = this.zza();
                parcel2.writeNoException();
                parcel2.writeString(zza);
                break;
            }
        }
        return true;
    }
}
