package com.google.android.gms.location.places;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzam;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzak;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public class PlaceReport extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<PlaceReport> CREATOR;
    private final int zza;
    private final String zzb;
    private final String zzc;
    private final String zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zzm();
    }
    
    PlaceReport(final int zza, final String zzb, final String zzc, final String zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PlaceReport)) {
            return false;
        }
        final PlaceReport placeReport = (PlaceReport)o;
        return zzak.zza(this.zzb, placeReport.zzb) && zzak.zza(this.zzc, placeReport.zzc) && zzak.zza(this.zzd, placeReport.zzd);
    }
    
    public String getPlaceId() {
        return this.zzb;
    }
    
    public String getSource() {
        return this.zzd;
    }
    
    public String getTag() {
        return this.zzc;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zzb, this.zzc, this.zzd });
    }
    
    @Override
    public String toString() {
        final zzam zza = zzak.zza(this);
        zza.zza("placeId", this.zzb);
        zza.zza("tag", this.zzc);
        if (!"unknown".equals(this.zzd)) {
            zza.zza("source", this.zzd);
        }
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.getPlaceId(), false);
        zzbig.zza(parcel, 3, this.getTag(), false);
        zzbig.zza(parcel, 4, this.getSource(), false);
        zzbig.zza(parcel, zza);
    }
}
