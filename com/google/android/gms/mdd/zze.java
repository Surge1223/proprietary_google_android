package com.google.android.gms.mdd;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.accounts.Account;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zze extends zzbid
{
    public static final Parcelable.Creator<zze> CREATOR;
    private final String zza;
    private final String zzb;
    private final Account zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    public zze(final String zza, final String zzb, final Account zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, zza);
    }
}
