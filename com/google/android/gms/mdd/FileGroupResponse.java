package com.google.android.gms.mdd;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FileGroupResponse extends zzbid
{
    public static final Parcelable.Creator<FileGroupResponse> CREATOR;
    public final String groupName;
    public final List<MddFile> mddFiles;
    public final String ownerPackage;
    
    static {
        CREATOR = (Parcelable.Creator)new zzg();
    }
    
    public FileGroupResponse(final String groupName, final String ownerPackage, final List<MddFile> mddFiles) {
        this.groupName = groupName;
        this.ownerPackage = ownerPackage;
        this.mddFiles = mddFiles;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.groupName, false);
        zzbig.zzc(parcel, 2, this.mddFiles, false);
        zzbig.zza(parcel, 3, this.ownerPackage, false);
        zzbig.zza(parcel, zza);
    }
}
