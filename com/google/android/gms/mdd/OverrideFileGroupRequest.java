package com.google.android.gms.mdd;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.accounts.Account;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class OverrideFileGroupRequest extends zzbid
{
    public static final Parcelable.Creator<OverrideFileGroupRequest> CREATOR;
    public final Account account;
    public final String[] allowedReaders;
    public final String[] fileIds;
    public final String groupName;
    public final String ownerPackage;
    public final ParcelFileDescriptor[] pfds;
    
    static {
        CREATOR = (Parcelable.Creator)new zzq();
    }
    
    public OverrideFileGroupRequest(final String groupName, final String ownerPackage, final String[] allowedReaders, final String[] fileIds, final ParcelFileDescriptor[] pfds, final Account account) {
        this.groupName = groupName;
        this.ownerPackage = ownerPackage;
        this.allowedReaders = allowedReaders;
        this.fileIds = fileIds;
        this.pfds = pfds;
        this.account = account;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.groupName, false);
        zzbig.zza(parcel, 2, this.ownerPackage, false);
        zzbig.zza(parcel, 3, this.allowedReaders, false);
        zzbig.zza(parcel, 4, this.fileIds, false);
        zzbig.zza(parcel, 5, this.pfds, n, false);
        zzbig.zza(parcel, 6, (Parcelable)this.account, n, false);
        zzbig.zza(parcel, zza);
    }
}
