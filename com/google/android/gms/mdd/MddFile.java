package com.google.android.gms.mdd;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class MddFile extends zzbid
{
    public static final Parcelable.Creator<MddFile> CREATOR;
    public final String fileId;
    public final String fileUri;
    
    static {
        CREATOR = (Parcelable.Creator)new zzm();
    }
    
    public MddFile(final String fileId, final String fileUri) {
        this.fileId = fileId;
        this.fileUri = fileUri;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.fileId, false);
        zzbig.zza(parcel, 2, this.fileUri, false);
        zzbig.zza(parcel, zza);
    }
}
