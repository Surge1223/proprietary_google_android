package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zze implements Callable<Integer>
{
    private final /* synthetic */ SharedPreferences zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ Integer zzc;
    
    zze(final SharedPreferences zza, final String zzb, final Integer zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}
