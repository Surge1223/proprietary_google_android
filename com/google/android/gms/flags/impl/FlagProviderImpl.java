package com.google.android.gms.flags.impl;

import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.dynamic.zzn;
import android.content.Context;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.content.SharedPreferences;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.internal.zzcjd;

@DynamiteApi
public class FlagProviderImpl extends zzcjd
{
    private boolean zza;
    private SharedPreferences zzb;
    
    public FlagProviderImpl() {
        this.zza = false;
    }
    
    @Override
    public boolean getBooleanFlagValue(final String s, final boolean b, final int n) {
        if (!this.zza) {
            return b;
        }
        return com.google.android.gms.flags.impl.zzb.zza(this.zzb, s, b);
    }
    
    @Override
    public int getIntFlagValue(final String s, final int n, final int n2) {
        if (!this.zza) {
            return n;
        }
        return zzd.zza(this.zzb, s, n);
    }
    
    @Override
    public long getLongFlagValue(final String s, final long n, final int n2) {
        if (!this.zza) {
            return n;
        }
        return zzf.zza(this.zzb, s, n);
    }
    
    @Override
    public String getStringFlagValue(final String s, final String s2, final int n) {
        if (!this.zza) {
            return s2;
        }
        return zzh.zza(this.zzb, s, s2);
    }
    
    @Override
    public void init(final IObjectWrapper objectWrapper) {
        final Context context = zzn.zza(objectWrapper);
        if (this.zza) {
            return;
        }
        try {
            this.zzb = zzj.zza(context.createPackageContext("com.google.android.gms", 0));
            this.zza = true;
        }
        catch (Exception ex) {
            final String value = String.valueOf(ex.getMessage());
            String concat;
            if (value.length() != 0) {
                concat = "Could not retrieve sdk flags, continuing with defaults: ".concat(value);
            }
            else {
                concat = new String("Could not retrieve sdk flags, continuing with defaults: ");
            }
            Log.w("FlagProviderImpl", concat);
        }
        catch (PackageManager$NameNotFoundException ex2) {}
    }
}
