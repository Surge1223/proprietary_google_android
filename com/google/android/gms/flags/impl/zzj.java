package com.google.android.gms.flags.impl;

import java.util.concurrent.Callable;
import com.google.android.gms.internal.zzcjg;
import android.content.Context;
import android.content.SharedPreferences;

public final class zzj
{
    private static SharedPreferences zza;
    
    static {
        zzj.zza = null;
    }
    
    public static SharedPreferences zza(final Context context) throws Exception {
        synchronized (SharedPreferences.class) {
            if (zzj.zza == null) {
                zzj.zza = zzcjg.zza((Callable<SharedPreferences>)new zzk(context));
            }
            return zzj.zza;
        }
    }
}
