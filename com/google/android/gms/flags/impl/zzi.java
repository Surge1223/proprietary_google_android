package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzi implements Callable<String>
{
    private final /* synthetic */ SharedPreferences zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ String zzc;
    
    zzi(final SharedPreferences zza, final String zzb, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}
