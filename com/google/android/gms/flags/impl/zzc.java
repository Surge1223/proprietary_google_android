package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzc implements Callable<Boolean>
{
    private final /* synthetic */ SharedPreferences zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ Boolean zzc;
    
    zzc(final SharedPreferences zza, final String zzb, final Boolean zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}
