package com.google.android.gms.googlehelp;

import android.content.Context;
import com.google.android.gms.googlehelp.internal.common.zzal;
import android.app.Activity;
import com.google.android.gms.googlehelp.internal.common.zzam;
import com.google.android.gms.common.api.Api;

public final class zzd
{
    public static final Api<Object> zza;
    private static final Api.zzf<zzam> zzb;
    private static final Api.zza<zzam, Object> zzc;
    
    static {
        zzb = new Api.zzf();
        zzc = new zze();
        zza = new Api<Object>("Help.API", (Api.zza<C, Object>)zzd.zzc, (Api.zzf<C>)zzd.zzb);
    }
    
    public static zzal zza(final Activity activity) {
        return new zzal(activity);
    }
    
    public static zzal zza(final Context context) {
        return new zzal(context);
    }
}
