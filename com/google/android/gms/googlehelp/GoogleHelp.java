package com.google.android.gms.googlehelp;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable;
import android.content.Intent;
import android.text.Editable;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableStringBuilder;
import java.util.Locale;
import java.util.ArrayList;
import android.text.TextUtils;
import com.google.android.gms.feedback.ThemeSettings;
import com.google.android.gms.googlehelp.internal.common.zzau;
import android.net.Uri;
import java.util.List;
import android.graphics.Bitmap;
import android.accounts.Account;
import com.google.android.gms.googlehelp.internal.common.TogglingData;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import android.app.PendingIntent;
import com.google.android.gms.feedback.ErrorReport;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public final class GoogleHelp extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<GoogleHelp> CREATOR;
    Bundle zza;
    private boolean zzaa;
    private ErrorReport zzab;
    private int zzac;
    private PendingIntent zzad;
    private boolean zzae;
    private BaseHelpProductSpecificData zzaf;
    private BaseFeedbackProductSpecificData zzag;
    TogglingData zzb;
    int zzc;
    boolean zzd;
    boolean zze;
    int zzf;
    String zzg;
    private final int zzh;
    private String zzi;
    private Account zzj;
    private String zzk;
    private String zzl;
    private Bitmap zzm;
    private boolean zzn;
    private boolean zzo;
    private List<String> zzp;
    @Deprecated
    private Bundle zzq;
    @Deprecated
    private Bitmap zzr;
    @Deprecated
    private byte[] zzs;
    @Deprecated
    private int zzt;
    @Deprecated
    private int zzu;
    private String zzv;
    private Uri zzw;
    private List<zzau> zzx;
    private ThemeSettings zzy;
    private List<OfflineSuggestion> zzz;
    
    static {
        CREATOR = (Parcelable.Creator)new zzc();
    }
    
    GoogleHelp(final int zzh, final String zzi, final Account zzj, final Bundle zza, final String zzk, final String zzl, final Bitmap zzm, final boolean zzn, final boolean zzo, final List<String> zzp, final Bundle zzq, final Bitmap zzr, final byte[] zzs, final int zzt, final int zzu, final String zzv, final Uri zzw, final List<zzau> zzx, final int theme, final ThemeSettings themeSettings, final List<OfflineSuggestion> zzz, final boolean zzaa, final ErrorReport zzab, final TogglingData zzb, final int zzac, final PendingIntent zzad, final int zzc, final boolean zzd, final boolean zze, final int zzf, final String zzg, final boolean zzae) {
        this.zzab = new ErrorReport();
        if (!TextUtils.isEmpty((CharSequence)zzi)) {
            this.zzh = zzh;
            this.zzc = zzc;
            this.zzd = zzd;
            this.zze = zze;
            this.zzf = zzf;
            this.zzg = zzg;
            this.zzi = zzi;
            this.zzj = zzj;
            this.zza = zza;
            this.zzk = zzk;
            this.zzl = zzl;
            this.zzm = zzm;
            this.zzn = zzn;
            this.zzo = zzo;
            this.zzae = zzae;
            this.zzp = zzp;
            this.zzad = zzad;
            this.zzq = zzq;
            this.zzr = zzr;
            this.zzs = zzs;
            this.zzt = zzt;
            this.zzu = zzu;
            this.zzv = zzv;
            this.zzw = zzw;
            this.zzx = zzx;
            ThemeSettings setTheme;
            if (this.zzh < 4) {
                setTheme = new ThemeSettings().setTheme(theme);
            }
            else if (themeSettings == null) {
                setTheme = new ThemeSettings();
            }
            else {
                setTheme = themeSettings;
            }
            this.zzy = setTheme;
            this.zzz = zzz;
            this.zzaa = zzaa;
            this.zzab = zzab;
            if (this.zzab != null) {
                this.zzab.launcher = "GoogleHelp";
            }
            this.zzb = zzb;
            this.zzac = zzac;
            return;
        }
        throw new IllegalStateException("Help requires a non-empty appContext");
    }
    
    public GoogleHelp(final String s) {
        this(13, s, null, null, null, null, null, true, true, new ArrayList<String>(), null, null, null, 0, 0, null, null, new ArrayList<zzau>(), 0, null, new ArrayList<OfflineSuggestion>(), false, new ErrorReport(), null, 0, null, -1, false, false, 200, null, false);
    }
    
    public static GoogleHelp newInstance(final String s) {
        return new GoogleHelp(s);
    }
    
    public final GoogleHelp addSupportPhoneNumber(final String s) {
        return this.addSupportPhoneNumber(s, Locale.getDefault());
    }
    
    public final GoogleHelp addSupportPhoneNumber(final String s, final Locale locale) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder((CharSequence)s);
        PhoneNumberUtils.formatNumber((Editable)spannableStringBuilder, PhoneNumberUtils.getFormatTypeForLocale(locale));
        this.zzp.add(spannableStringBuilder.toString());
        return this;
    }
    
    public final Intent buildHelpIntent() {
        return new Intent("com.google.android.gms.googlehelp.HELP").setPackage("com.google.android.gms").putExtra("EXTRA_GOOGLE_HELP", (Parcelable)this);
    }
    
    public final GoogleHelp enableAccountPicker(final boolean zzae) {
        this.zzae = zzae;
        return this;
    }
    
    public final Uri getFallbackSupportUri() {
        return this.zzw;
    }
    
    public final GoogleHelp setFallbackSupportUri(final Uri zzw) {
        this.zzw = zzw;
        return this;
    }
    
    public final GoogleHelp setGoogleAccount(final Account zzj) {
        this.zzj = zzj;
        return this;
    }
    
    public final GoogleHelp setHelpPsd(final BaseHelpProductSpecificData zzaf) {
        this.zzaf = zzaf;
        return this;
    }
    
    public final GoogleHelp setThemeSettings(final ThemeSettings zzy) {
        this.zzy = zzy;
        return this;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzh);
        zzbig.zza(parcel, 2, this.zzi, false);
        zzbig.zza(parcel, 3, (Parcelable)this.zzj, n, false);
        zzbig.zza(parcel, 4, this.zza, false);
        zzbig.zza(parcel, 5, this.zzn);
        zzbig.zza(parcel, 6, this.zzo);
        zzbig.zzb(parcel, 7, this.zzp, false);
        zzbig.zza(parcel, 10, this.zzq, false);
        zzbig.zza(parcel, 11, (Parcelable)this.zzr, n, false);
        zzbig.zza(parcel, 14, this.zzv, false);
        zzbig.zza(parcel, 15, (Parcelable)this.zzw, n, false);
        zzbig.zzc(parcel, 16, this.zzx, false);
        zzbig.zza(parcel, 17, 0);
        zzbig.zzc(parcel, 18, this.zzz, false);
        zzbig.zza(parcel, 19, this.zzs, false);
        zzbig.zza(parcel, 20, this.zzt);
        zzbig.zza(parcel, 21, this.zzu);
        zzbig.zza(parcel, 22, this.zzaa);
        zzbig.zza(parcel, 23, (Parcelable)this.zzab, n, false);
        zzbig.zza(parcel, 25, (Parcelable)this.zzy, n, false);
        zzbig.zza(parcel, 28, this.zzk, false);
        zzbig.zza(parcel, 31, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, 32, this.zzac);
        zzbig.zza(parcel, 33, (Parcelable)this.zzad, n, false);
        zzbig.zza(parcel, 34, this.zzl, false);
        zzbig.zza(parcel, 35, (Parcelable)this.zzm, n, false);
        zzbig.zza(parcel, 36, this.zzc);
        zzbig.zza(parcel, 37, this.zzd);
        zzbig.zza(parcel, 38, this.zze);
        zzbig.zza(parcel, 39, this.zzf);
        zzbig.zza(parcel, 40, this.zzg, false);
        zzbig.zza(parcel, 41, this.zzae);
        zzbig.zza(parcel, zza);
    }
    
    final BaseHelpProductSpecificData zza() {
        return this.zzaf;
    }
    
    final BaseFeedbackProductSpecificData zzb() {
        return this.zzag;
    }
}
