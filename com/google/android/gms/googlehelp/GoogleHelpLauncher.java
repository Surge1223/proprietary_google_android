package com.google.android.gms.googlehelp;

import android.content.Context;
import com.google.android.gms.common.GooglePlayServicesUtil;
import android.content.Intent;
import android.app.Activity;

public class GoogleHelpLauncher
{
    public final Activity mActivity;
    private final Object zza;
    
    public GoogleHelpLauncher(final Activity mActivity) {
        this.mActivity = mActivity;
        this.zza = null;
    }
    
    private final void zza(final int n, final GoogleHelp googleHelp) {
        final Intent setData = new Intent("android.intent.action.VIEW").setData(googleHelp.getFallbackSupportUri());
        if (n != 7 && this.mActivity.getPackageManager().queryIntentActivities(setData, 0).size() > 0) {
            this.mActivity.startActivity(setData);
            return;
        }
        GooglePlayServicesUtil.showErrorDialogFragment(n, this.mActivity, 0);
    }
    
    public int isGooglePlayServicesAvailable() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable((Context)this.mActivity);
    }
    
    public void launch(final Intent intent) {
        if (!intent.getAction().equals("com.google.android.gms.googlehelp.HELP") || !intent.hasExtra("EXTRA_GOOGLE_HELP")) {
            throw new IllegalArgumentException("The intent you are trying to launch is not GoogleHelp intent! This class only supports GoogleHelp intents.");
        }
        final int googlePlayServicesAvailable = this.isGooglePlayServicesAvailable();
        if (googlePlayServicesAvailable == 0) {
            zzd.zza(this.mActivity).zza(intent);
            return;
        }
        this.zza(googlePlayServicesAvailable, (GoogleHelp)intent.getParcelableExtra("EXTRA_GOOGLE_HELP"));
    }
}
