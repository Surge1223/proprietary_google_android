package com.google.android.gms.googlehelp;

import com.google.android.gms.feedback.FeedbackOptions;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import android.content.Intent;
import android.app.Activity;
import com.google.android.gms.common.api.GoogleApiClient;

public interface zzb
{
    PendingResult<Status> zza(final GoogleApiClient p0, final Activity p1, final Intent p2);
    
    PendingResult<Status> zza(final GoogleApiClient p0, final GoogleHelp p1, final Bundle p2, final long p3);
    
    PendingResult<Status> zza(final GoogleApiClient p0, final GoogleHelp p1, final FeedbackOptions p2, final Bundle p3, final long p4);
    
    PendingResult<Status> zzb(final GoogleApiClient p0, final GoogleHelp p1, final Bundle p2, final long p3);
}
