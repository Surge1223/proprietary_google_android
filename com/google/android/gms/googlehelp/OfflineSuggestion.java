package com.google.android.gms.googlehelp;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class OfflineSuggestion extends zzbid
{
    public static final Parcelable.Creator<OfflineSuggestion> CREATOR;
    private final String zza;
    private final String zzb;
    private final String zzc;
    private final String zzd;
    
    static {
        CREATOR = (Parcelable.Creator)new zzg();
    }
    
    public OfflineSuggestion(final String zza, final String zzb, final String zzd, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb, false);
        zzbig.zza(parcel, 4, this.zzc, false);
        zzbig.zza(parcel, 5, this.zzd, false);
        zzbig.zza(parcel, zza);
    }
}
