package com.google.android.gms.googlehelp;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class SupportRequestHelp extends zzbid
{
    public static final Parcelable.Creator<SupportRequestHelp> CREATOR;
    private final GoogleHelp zza;
    private String zzb;
    private String zzc;
    private String zzd;
    private String zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzh();
    }
    
    SupportRequestHelp(final GoogleHelp zza, final String zzb, final String zzc, final String zzd, final String zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public final String getAssistantTranscript() {
        return this.zze;
    }
    
    public final String getChatPoolId() {
        return this.zzb;
    }
    
    public final String getDescription() {
        return this.zzc;
    }
    
    public final GoogleHelp getGoogleHelp() {
        return this.zza;
    }
    
    public final String getPhoneNumber() {
        return this.zzd;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.getGoogleHelp(), n, false);
        zzbig.zza(parcel, 2, this.getChatPoolId(), false);
        zzbig.zza(parcel, 3, this.getDescription(), false);
        zzbig.zza(parcel, 4, this.getPhoneNumber(), false);
        zzbig.zza(parcel, 5, this.getAssistantTranscript(), false);
        zzbig.zza(parcel, zza);
    }
}
