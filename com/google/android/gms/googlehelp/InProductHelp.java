package com.google.android.gms.googlehelp;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class InProductHelp extends zzbid
{
    public static final Parcelable.Creator<InProductHelp> CREATOR;
    private GoogleHelp zza;
    private String zzb;
    private String zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    InProductHelp(final GoogleHelp zza, final String zzb, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.zza, n, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, zza);
    }
    
    public final InProductHelp zza(final GoogleHelp zza) {
        this.zza = zza;
        return this;
    }
}
