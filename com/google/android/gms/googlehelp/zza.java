package com.google.android.gms.googlehelp;

import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.internal.zzchd;
import android.util.Pair;
import java.util.List;
import com.google.android.gms.googlehelp.internal.common.TogglingData;

public final class zza
{
    private final GoogleHelp zza;
    
    public zza(final GoogleHelp zza) {
        this.zza = zza;
    }
    
    public final TogglingData zza() {
        return this.zza.zzb;
    }
    
    public final zza zza(final int zzc) {
        this.zza.zzc = zzc;
        return this;
    }
    
    public final zza zza(final List<Pair<String, String>> list) {
        this.zza.zza = zzchd.zza(list);
        return this;
    }
    
    public final zza zza(final boolean b) {
        this.zza.zzd = true;
        return this;
    }
    
    public final BaseHelpProductSpecificData zzb() {
        return this.zza.zza();
    }
    
    public final zza zzb(final boolean b) {
        this.zza.zze = true;
        return this;
    }
    
    public final BaseFeedbackProductSpecificData zzc() {
        return this.zza.zzb();
    }
    
    public final int zzd() {
        return this.zza.zzf;
    }
}
