package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.googlehelp.zza;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import android.content.Context;
import com.google.android.gms.googlehelp.GoogleHelp;

public final class zzax
{
    private final GoogleHelp zza;
    private final zzba zzb;
    
    public zzax(final GoogleHelp googleHelp) {
        this(googleHelp, new zzaz(null));
    }
    
    private zzax(final GoogleHelp zza, final zzba zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    private final void zza(final Runnable runnable, final int priority) {
        final Thread zza = this.zzb.zza(runnable);
        zza.setPriority(priority);
        zza.start();
    }
    
    public final void zza(final Context context, final BaseFeedbackProductSpecificData baseFeedbackProductSpecificData, final BaseHelpProductSpecificData baseHelpProductSpecificData, final long n) {
        final zza zza = new zza(this.zza);
        if (baseHelpProductSpecificData != null) {
            zza.zza(true);
            this.zza(this.zzb.zza(context, this.zza, baseHelpProductSpecificData, n), 4);
        }
        if (baseFeedbackProductSpecificData != null) {
            zza.zzb(true);
            this.zza(this.zzb.zzb(context, this.zza, baseFeedbackProductSpecificData, n), 4);
            this.zza(this.zzb.zza(context, this.zza, baseFeedbackProductSpecificData, n), 4);
        }
    }
    
    public final void zza(final BaseHelpProductSpecificData baseHelpProductSpecificData, final zzf zzf) {
        if (baseHelpProductSpecificData == null) {
            zzf.zza(this.zza);
            return;
        }
        this.zza(this.zzb.zza(this.zza, baseHelpProductSpecificData, zzf), 10);
    }
}
