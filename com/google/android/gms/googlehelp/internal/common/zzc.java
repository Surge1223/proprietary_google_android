package com.google.android.gms.googlehelp.internal.common;

import java.util.List;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.internal.zzchd;
import com.google.android.gms.googlehelp.zzd;
import java.util.Collections;
import android.util.Log;
import java.util.Collection;
import android.util.Pair;
import java.util.ArrayList;
import com.google.android.gms.internal.zzche;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.content.Context;

public final class zzc implements Runnable
{
    private final Context zza;
    private final GoogleHelp zzb;
    private final BaseHelpProductSpecificData zzc;
    private final long zzd;
    
    public zzc(final Context zza, final GoogleHelp zzb, final BaseHelpProductSpecificData zzc, final long zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final void run() {
        List<Pair<String, String>> list;
        try {
            final zzche zzche = new zzche();
            zzche.zza();
            if ((list = this.zzc.getAsyncHelpPsd()) == null) {
                list = new ArrayList<Pair<String, String>>(1);
            }
            try {
                list.add((Pair<String, String>)Pair.create((Object)"gms:googlehelp:async_help_psd_collection_time_ms", (Object)String.valueOf(zzche.zzb())));
            }
            catch (UnsupportedOperationException ex2) {
                final ArrayList list2 = new ArrayList<Pair<String, String>>(list);
                list2.add((Pair<String, String>)Pair.create((Object)"gms:googlehelp:async_help_psd_collection_time_ms", (Object)String.valueOf(zzche.zzb())));
                list = (List<Pair<String, String>>)list2;
            }
        }
        catch (Exception ex) {
            Log.w("gH_GetAsyncHelpPsd", "Failed to get async help psd.", (Throwable)ex);
            list = Collections.singletonList(Pair.create((Object)"gms:googlehelp:async_help_psd_failure", (Object)"exception"));
        }
        zzan.zza(zzal.zzb.zza(com.google.android.gms.googlehelp.zzd.zza(this.zza).asGoogleApiClient(), this.zzb, zzchd.zza(list), this.zzd));
    }
}
