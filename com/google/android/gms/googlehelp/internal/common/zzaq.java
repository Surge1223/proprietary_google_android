package com.google.android.gms.googlehelp.internal.common;

import android.os.RemoteException;
import com.google.android.gms.googlehelp.GoogleHelp;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.googlehelp.InProductHelp;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.zzez;

public abstract class zzaq extends zzez implements zzap
{
    public zzaq() {
        this.attachInterface((IInterface)this, "com.google.android.gms.googlehelp.internal.common.IGoogleHelpCallbacks");
    }
    
    public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
        if (this.zza(n, parcel, parcel2, n2)) {
            return true;
        }
        switch (n) {
            default: {
                return false;
            }
            case 19: {
                this.zzl();
                parcel2.writeNoException();
                break;
            }
            case 18: {
                this.zzc(parcel.createByteArray());
                parcel2.writeNoException();
                break;
            }
            case 17: {
                this.zza(zzfa.zza(parcel, InProductHelp.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 16: {
                this.zzg();
                parcel2.writeNoException();
                break;
            }
            case 15: {
                this.zzb(parcel.createByteArray());
                parcel2.writeNoException();
                break;
            }
            case 14: {
                this.zzf();
                parcel2.writeNoException();
                break;
            }
            case 13: {
                this.zza(parcel.createByteArray());
                parcel2.writeNoException();
                break;
            }
            case 12: {
                this.zze();
                parcel2.writeNoException();
                break;
            }
            case 11: {
                this.zzd();
                parcel2.writeNoException();
                break;
            }
            case 10: {
                this.zzc();
                parcel2.writeNoException();
                break;
            }
            case 9: {
                this.zza(parcel.readInt());
                parcel2.writeNoException();
                break;
            }
            case 8: {
                this.zzb();
                break;
            }
            case 7: {
                this.zza();
                break;
            }
            case 6: {
                this.zzj();
                parcel2.writeNoException();
                break;
            }
            case 5: {
                this.zzi();
                parcel2.writeNoException();
                break;
            }
            case 4: {
                this.zzk();
                parcel2.writeNoException();
                break;
            }
            case 3: {
                this.zzh();
                parcel2.writeNoException();
                break;
            }
            case 2: {
                this.zza(zzfa.zza(parcel, TogglingData.CREATOR));
                parcel2.writeNoException();
                break;
            }
            case 1: {
                this.zza(zzfa.zza(parcel, GoogleHelp.CREATOR));
                parcel2.writeNoException();
                break;
            }
        }
        return true;
    }
}
