package com.google.android.gms.googlehelp.internal.common;

import java.io.File;
import com.google.android.gms.feedback.FileTeleporter;
import java.util.List;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.feedback.FeedbackOptions;
import com.google.android.gms.googlehelp.zzd;
import android.util.Log;
import com.google.android.gms.internal.zzcgy;
import com.google.android.gms.internal.zzche;
import android.os.Bundle;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.content.Context;

public final class zza implements Runnable
{
    private final Context zza;
    private final GoogleHelp zzb;
    private final BaseFeedbackProductSpecificData zzc;
    private final long zzd;
    
    public zza(final Context zza, final GoogleHelp zzb, final BaseFeedbackProductSpecificData zzc, final long zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final void run() {
        final Bundle bundle = new Bundle(1);
        List<FileTeleporter> asyncFeedbackPsbd;
        try {
            final zzche zzche = new zzche();
            zzche.zza();
            asyncFeedbackPsbd = this.zzc.getAsyncFeedbackPsbd();
            final File cacheDir = this.zza.getCacheDir();
            if (asyncFeedbackPsbd != null && !asyncFeedbackPsbd.isEmpty() && cacheDir != null) {
                zzcgy.zza(asyncFeedbackPsbd, cacheDir);
            }
            bundle.putString("gms:feedback:async_feedback_psbd_collection_time_ms", String.valueOf(zzche.zzb()));
        }
        catch (Exception ex) {
            Log.w("gH_GetAsyncFeedbackPsbd", "Failed to get async Feedback psbd.", (Throwable)ex);
            asyncFeedbackPsbd = null;
            bundle.putString("gms:feedback:async_feedback_psbd_failure", "exception");
        }
        zzan.zza(zzal.zzb.zza(com.google.android.gms.googlehelp.zzd.zza(this.zza).asGoogleApiClient(), this.zzb, FeedbackOptions.zza(asyncFeedbackPsbd), bundle, this.zzd));
    }
}
