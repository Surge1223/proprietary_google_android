package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.feedback.FeedbackOptions;
import android.graphics.Bitmap;
import com.google.android.gms.googlehelp.GoogleHelpTogglingRegister;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.internal.zzbii;
import com.google.android.gms.googlehelp.InProductHelp;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import android.content.Intent;
import android.app.Activity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.googlehelp.zzb;
import android.os.RemoteException;
import android.util.Log;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.os.Bundle;

final class zzah extends zzak
{
    private final /* synthetic */ Bundle zza;
    private final /* synthetic */ long zzb;
    private final /* synthetic */ GoogleHelp zzd;
    
    zzah(final zzg zzg, final GoogleApiClient googleApiClient, final Bundle zza, final long zzb, final GoogleHelp zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        super(googleApiClient);
    }
    
    @Override
    protected final void zza(final Context context, final zzar zzar) throws RemoteException {
        try {
            zzar.zza(this.zza, this.zzb, this.zzd, new zzai(this));
        }
        catch (Exception ex) {
            Log.e("gH_GoogleHelpApiImpl", "Requesting to save the async help psd failed!", (Throwable)ex);
            this.zzd(com.google.android.gms.googlehelp.internal.common.zzg.zza);
        }
    }
}
