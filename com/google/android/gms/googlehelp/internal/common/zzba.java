package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.content.Context;

interface zzba
{
    zzb zza(final Context p0, final GoogleHelp p1, final BaseFeedbackProductSpecificData p2, final long p3);
    
    zzc zza(final Context p0, final GoogleHelp p1, final BaseHelpProductSpecificData p2, final long p3);
    
    zzd zza(final GoogleHelp p0, final BaseHelpProductSpecificData p1, final zzf p2);
    
    Thread zza(final Runnable p0);
    
    zza zzb(final Context p0, final GoogleHelp p1, final BaseFeedbackProductSpecificData p2, final long p3);
}
