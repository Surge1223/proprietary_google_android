package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public class TogglingData extends zzbid implements ReflectedParcelable
{
    public static final Parcelable.Creator<TogglingData> CREATOR;
    String zza;
    String zzb;
    private String zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzbc();
    }
    
    private TogglingData() {
    }
    
    TogglingData(final String zzc, final String zza, final String zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zzc, false);
        zzbig.zza(parcel, 3, this.zza, false);
        zzbig.zza(parcel, 4, this.zzb, false);
        zzbig.zza(parcel, zza);
    }
}
