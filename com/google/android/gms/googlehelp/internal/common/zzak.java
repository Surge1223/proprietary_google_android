package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

abstract class zzak extends zzaj<Status>
{
    public zzak(final GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }
}
