package com.google.android.gms.googlehelp.internal.common;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.internal.zzl;

public final class zzam extends zzl<zzar>
{
    public zzam(final Context context, final Looper looper, final ClientSettings clientSettings, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 63, clientSettings, connectionCallbacks, onConnectionFailedListener);
    }
    
    @Override
    protected final String getServiceDescriptor() {
        return "com.google.android.gms.googlehelp.internal.common.IGoogleHelpService";
    }
    
    @Override
    protected final String getStartServiceAction() {
        return "com.google.android.gms.googlehelp.service.GoogleHelpService.START";
    }
}
