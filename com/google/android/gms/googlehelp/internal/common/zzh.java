package com.google.android.gms.googlehelp.internal.common;

import android.os.RemoteException;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.googlehelp.zza;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApiClient;
import android.app.Activity;
import android.graphics.Bitmap;
import android.content.Intent;

final class zzh extends zzak
{
    final /* synthetic */ Intent zza;
    final /* synthetic */ Bitmap zzb;
    final /* synthetic */ Activity zzd;
    final /* synthetic */ zzg zze;
    
    zzh(final zzg zze, final GoogleApiClient googleApiClient, final Intent zza, final Bitmap zzb, final Activity zzd) {
        this.zze = zze;
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        super(googleApiClient);
    }
    
    @Override
    protected final void zza(final Context context, final zzar zzar) throws RemoteException {
        final GoogleHelp googleHelp = (GoogleHelp)this.zza.getParcelableExtra("EXTRA_GOOGLE_HELP");
        final com.google.android.gms.googlehelp.zza zza = new com.google.android.gms.googlehelp.zza(googleHelp);
        final BaseHelpProductSpecificData zzb = zza.zzb();
        new zzax(googleHelp).zza(zzb, new zzi(this, zzar, this, zzb, zza.zzc()));
    }
}
