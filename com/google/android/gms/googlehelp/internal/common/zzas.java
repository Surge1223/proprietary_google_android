package com.google.android.gms.googlehelp.internal.common;

import android.graphics.Bitmap;
import com.google.android.gms.feedback.FeedbackOptions;
import android.os.RemoteException;
import android.os.Parcel;
import android.os.IInterface;
import android.os.Parcelable;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzas extends zzey implements zzar
{
    zzas(final IBinder binder) {
        super(binder, "com.google.android.gms.googlehelp.internal.common.IGoogleHelpService");
    }
    
    @Override
    public final void zza(final Bundle bundle, final long n, final GoogleHelp googleHelp, final zzap zzap) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)bundle);
        a_.writeLong(n);
        zzfa.zza(a_, (Parcelable)googleHelp);
        zzfa.zza(a_, (IInterface)zzap);
        this.zzc(8, a_);
    }
    
    @Override
    public final void zza(final FeedbackOptions feedbackOptions, final Bundle bundle, final long n, final GoogleHelp googleHelp, final zzap zzap) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)feedbackOptions);
        zzfa.zza(a_, (Parcelable)bundle);
        a_.writeLong(n);
        zzfa.zza(a_, (Parcelable)googleHelp);
        zzfa.zza(a_, (IInterface)zzap);
        this.zzc(10, a_);
    }
    
    @Override
    public final void zza(final GoogleHelp googleHelp, final Bitmap bitmap, final zzap zzap) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)googleHelp);
        zzfa.zza(a_, (Parcelable)bitmap);
        zzfa.zza(a_, (IInterface)zzap);
        this.zzb(2, a_);
    }
    
    @Override
    public final void zzb(final Bundle bundle, final long n, final GoogleHelp googleHelp, final zzap zzap) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (Parcelable)bundle);
        a_.writeLong(n);
        zzfa.zza(a_, (Parcelable)googleHelp);
        zzfa.zza(a_, (IInterface)zzap);
        this.zzc(9, a_);
    }
}
