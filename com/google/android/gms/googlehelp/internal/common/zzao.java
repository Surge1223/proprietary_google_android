package com.google.android.gms.googlehelp.internal.common;

import android.view.View;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import java.util.concurrent.ExecutionException;
import android.util.Log;
import java.util.concurrent.FutureTask;
import android.app.Activity;
import android.graphics.Bitmap;
import java.util.concurrent.Callable;

final class zzao implements Callable<Bitmap>
{
    private final /* synthetic */ Activity zza;
    
    zzao(final Activity zza) {
        this.zza = zza;
    }
}
