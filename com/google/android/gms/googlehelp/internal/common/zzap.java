package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.googlehelp.InProductHelp;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.os.RemoteException;
import android.os.IInterface;

public interface zzap extends IInterface
{
    void zza() throws RemoteException;
    
    void zza(final int p0) throws RemoteException;
    
    void zza(final GoogleHelp p0) throws RemoteException;
    
    void zza(final InProductHelp p0) throws RemoteException;
    
    void zza(final TogglingData p0) throws RemoteException;
    
    void zza(final byte[] p0) throws RemoteException;
    
    void zzb() throws RemoteException;
    
    void zzb(final byte[] p0) throws RemoteException;
    
    void zzc() throws RemoteException;
    
    void zzc(final byte[] p0) throws RemoteException;
    
    void zzd() throws RemoteException;
    
    void zze() throws RemoteException;
    
    void zzf() throws RemoteException;
    
    void zzg() throws RemoteException;
    
    void zzh() throws RemoteException;
    
    void zzi() throws RemoteException;
    
    void zzj() throws RemoteException;
    
    void zzk() throws RemoteException;
    
    void zzl() throws RemoteException;
}
