package com.google.android.gms.googlehelp.internal.common;

import android.graphics.Bitmap;
import com.google.android.gms.feedback.FeedbackOptions;
import android.os.RemoteException;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.os.Bundle;
import android.os.IInterface;

public interface zzar extends IInterface
{
    void zza(final Bundle p0, final long p1, final GoogleHelp p2, final zzap p3) throws RemoteException;
    
    void zza(final FeedbackOptions p0, final Bundle p1, final long p2, final GoogleHelp p3, final zzap p4) throws RemoteException;
    
    void zza(final GoogleHelp p0, final Bitmap p1, final zzap p2) throws RemoteException;
    
    void zzb(final Bundle p0, final long p1, final GoogleHelp p2, final zzap p3) throws RemoteException;
}
