package com.google.android.gms.googlehelp.internal.common;

import android.graphics.Bitmap;
import com.google.android.gms.googlehelp.GoogleHelpTogglingRegister;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.internal.zzbii;
import com.google.android.gms.googlehelp.InProductHelp;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import android.content.Intent;
import android.app.Activity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.googlehelp.zzb;
import android.os.RemoteException;
import android.util.Log;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.os.Bundle;
import com.google.android.gms.feedback.FeedbackOptions;

final class zzl extends zzak
{
    private final /* synthetic */ FeedbackOptions zza;
    private final /* synthetic */ Bundle zzb;
    private final /* synthetic */ long zzd;
    private final /* synthetic */ GoogleHelp zze;
    
    zzl(final zzg zzg, final GoogleApiClient googleApiClient, final FeedbackOptions zza, final Bundle zzb, final long zzd, final GoogleHelp zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
        super(googleApiClient);
    }
    
    @Override
    protected final void zza(final Context context, final zzar zzar) throws RemoteException {
        try {
            zzar.zza(this.zza, this.zzb, this.zzd, this.zze, new zzm(this));
        }
        catch (Exception ex) {
            Log.e("gH_GoogleHelpApiImpl", "Requesting to save the async feedback psbd failed!", (Throwable)ex);
            this.zzd(com.google.android.gms.googlehelp.internal.common.zzg.zza);
        }
    }
}
