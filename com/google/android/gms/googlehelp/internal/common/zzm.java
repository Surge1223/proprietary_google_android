package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.common.api.Status;

final class zzm extends zzbb
{
    private final /* synthetic */ zzl zza;
    
    zzm(final zzl zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zzb() {
        this.zza.zza((R)Status.zza);
    }
}
