package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.content.Context;

final class zzaz implements zzba
{
    @Override
    public final zzb zza(final Context context, final GoogleHelp googleHelp, final BaseFeedbackProductSpecificData baseFeedbackProductSpecificData, final long n) {
        return new zzb(context, googleHelp, baseFeedbackProductSpecificData, n);
    }
    
    @Override
    public final zzc zza(final Context context, final GoogleHelp googleHelp, final BaseHelpProductSpecificData baseHelpProductSpecificData, final long n) {
        return new zzc(context, googleHelp, baseHelpProductSpecificData, n);
    }
    
    @Override
    public final zzd zza(final GoogleHelp googleHelp, final BaseHelpProductSpecificData baseHelpProductSpecificData, final zzf zzf) {
        return new zzd(googleHelp, baseHelpProductSpecificData, zzf);
    }
    
    @Override
    public final Thread zza(final Runnable runnable) {
        return new Thread(runnable, "PsdCollector");
    }
    
    @Override
    public final zza zzb(final Context context, final GoogleHelp googleHelp, final BaseFeedbackProductSpecificData baseFeedbackProductSpecificData, final long n) {
        return new zza(context, googleHelp, baseFeedbackProductSpecificData, n);
    }
}
