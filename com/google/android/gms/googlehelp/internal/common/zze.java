package com.google.android.gms.googlehelp.internal.common;

import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import com.google.android.gms.internal.zzche;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.util.Pair;
import com.google.android.gms.googlehelp.zza;
import android.util.Log;

final class zze implements Runnable
{
    private final /* synthetic */ zzd zza;
    
    zze(final zzd zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        if (!this.zza.zza()) {
            return;
        }
        Log.w("gH_GetSyncHelpPsd", "Getting sync help psd timed out.");
        new zza(this.zza.zza).zza(com.google.android.gms.common.util.zze.zza(1, Pair.create((Object)"gms:googlehelp:sync_help_psd_failure", (Object)"timeout")));
        this.zza.zzc.zza(this.zza.zza);
    }
}
