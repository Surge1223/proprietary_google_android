package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.tasks.Task;
import android.content.Intent;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.googlehelp.zzd;
import android.app.Activity;
import com.google.android.gms.googlehelp.zzb;
import com.google.android.gms.common.api.GoogleApi;

public final class zzal extends GoogleApi<Object>
{
    static zzb zzb;
    private final Activity zzc;
    
    static {
        zzal.zzb = new zzg();
    }
    
    public zzal(final Activity zzc) {
        super(zzc, (Api<Api.ApiOptions>)com.google.android.gms.googlehelp.zzd.zza, null, GoogleApi.zza.zza);
        this.zzc = zzc;
    }
    
    public zzal(final Context context) {
        super(context, (Api<Api.ApiOptions>)com.google.android.gms.googlehelp.zzd.zza, null, GoogleApi.zza.zza);
        this.zzc = null;
    }
    
    public final Task<Void> zza(final Intent intent) {
        zzau.zza(this.zzc);
        return zzan.zza(zzal.zzb.zza(this.asGoogleApiClient(), this.zzc, intent));
    }
}
