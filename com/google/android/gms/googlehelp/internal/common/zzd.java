package com.google.android.gms.googlehelp.internal.common;

import java.util.List;
import android.util.Log;
import java.util.Collection;
import android.util.Pair;
import java.util.ArrayList;
import com.google.android.gms.internal.zzche;
import com.google.android.gms.googlehelp.zza;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import com.google.android.gms.googlehelp.GoogleHelp;

public final class zzd implements Runnable
{
    private final GoogleHelp zza;
    private final BaseHelpProductSpecificData zzb;
    private final zzf zzc;
    private boolean zzd;
    
    public zzd(final GoogleHelp zza, final BaseHelpProductSpecificData zzb, final zzf zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public final void run() {
        this.zzd = false;
        final Handler handler = new Handler(Looper.getMainLooper());
        final zze zze = new zze(this);
        handler.postDelayed((Runnable)zze, (long)new zza(this.zza).zzd());
        List<Pair<String, String>> list;
        try {
            final zzche zzche = new zzche();
            zzche.zza();
            if ((list = this.zzb.getSyncHelpPsd()) == null) {
                list = new ArrayList<Pair<String, String>>(1);
            }
            try {
                list.add((Pair<String, String>)Pair.create((Object)"gms:googlehelp:sync_help_psd_collection_time_ms", (Object)String.valueOf(zzche.zzb())));
            }
            catch (UnsupportedOperationException ex2) {
                final ArrayList list2 = new ArrayList<Pair<String, String>>(list);
                list2.add((Pair<String, String>)Pair.create((Object)"gms:googlehelp:sync_help_psd_collection_time_ms", (Object)String.valueOf(zzche.zzb())));
                list = (List<Pair<String, String>>)list2;
            }
        }
        catch (Exception ex) {
            Log.w("gH_GetSyncHelpPsd", "Failed to get sync help psd.", (Throwable)ex);
            list = com.google.android.gms.common.util.zze.zza(1, Pair.create((Object)"gms:googlehelp:sync_help_psd_failure", (Object)"exception"));
        }
        if (this.zza()) {
            handler.removeCallbacks((Runnable)zze);
            new zza(this.zza).zza(list);
            this.zzc.zza(this.zza);
        }
    }
    
    final boolean zza() {
        synchronized (this) {
            return !this.zzd && (this.zzd = true);
        }
    }
}
