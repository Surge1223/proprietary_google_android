package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.common.api.Status;

final class zzai extends zzbb
{
    private final /* synthetic */ zzah zza;
    
    zzai(final zzah zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        this.zza.zza((R)Status.zza);
    }
}
