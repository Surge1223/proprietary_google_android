package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.feedback.FeedbackOptions;
import android.os.Bundle;
import android.graphics.Bitmap;
import com.google.android.gms.googlehelp.GoogleHelpTogglingRegister;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzbii;
import com.google.android.gms.googlehelp.InProductHelp;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup;
import android.content.Intent;
import android.app.Activity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.googlehelp.zzb;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.googlehelp.GoogleHelp;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;

final class zzi implements zzf
{
    private final /* synthetic */ zzar zza;
    private final /* synthetic */ zzak zzb;
    private final /* synthetic */ BaseHelpProductSpecificData zzc;
    private final /* synthetic */ BaseFeedbackProductSpecificData zzd;
    private final /* synthetic */ zzh zze;
    
    zzi(final zzh zze, final zzar zza, final zzak zzb, final BaseHelpProductSpecificData zzc, final BaseFeedbackProductSpecificData zzd) {
        this.zze = zze;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final void zza(final GoogleHelp googleHelp) {
        try {
            this.zza.zza(googleHelp, this.zze.zzb, this.zze.zze.zza(this.zzb, this.zze.zzd, this.zze.zza, this.zzc, this.zzd));
        }
        catch (RemoteException ex) {
            Log.e("gH_GoogleHelpApiImpl", "Starting help failed!", (Throwable)ex);
            this.zze.zzd(zzg.zza);
        }
    }
}
