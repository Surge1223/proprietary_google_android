package com.google.android.gms.googlehelp.internal.common;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.content.Intent;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzau extends zzbid
{
    public static final Parcelable.Creator<zzau> CREATOR;
    private final int zza;
    private final String zzb;
    private final Intent zzc;
    
    static {
        CREATOR = (Parcelable.Creator)new zzav();
    }
    
    public zzau(final int zza, final String zzb, final Intent zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza);
        zzbig.zza(parcel, 3, this.zzb, false);
        zzbig.zza(parcel, 4, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, zza);
    }
}
