package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.feedback.FeedbackOptions;
import android.os.Bundle;
import android.graphics.Bitmap;
import com.google.android.gms.googlehelp.GoogleHelpTogglingRegister;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzbii;
import com.google.android.gms.googlehelp.InProductHelp;
import android.os.Parcelable;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import android.content.Intent;
import android.app.Activity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.googlehelp.zzb;

public final class zzg implements zzb
{
    private static final Status zza;
    
    static {
        zza = new Status(13);
    }
    
    private final zzbb zza(final zzak zzak, final Activity activity, final Intent intent, final BaseHelpProductSpecificData baseHelpProductSpecificData, final BaseFeedbackProductSpecificData baseFeedbackProductSpecificData) {
        return new zzv(this, intent, baseHelpProductSpecificData, baseFeedbackProductSpecificData, activity, zzak);
    }
    
    static String zza(final Activity activity) {
        final String string = activity.getTitle().toString();
        final int identifier = activity.getResources().getIdentifier("action_bar", "id", activity.getPackageName());
        if (identifier != 0) {
            final ViewGroup viewGroup = (ViewGroup)activity.findViewById(identifier);
            if (viewGroup != null) {
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    final View child = viewGroup.getChildAt(i);
                    if (child instanceof TextView) {
                        return ((TextView)child).getText().toString();
                    }
                }
            }
        }
        return string;
    }
    
    static void zza(final zzak zzak, final Activity activity, final Intent intent, final GoogleHelp googleHelp) {
        if (intent.hasExtra("EXTRA_GOOGLE_HELP")) {
            intent.putExtra("EXTRA_GOOGLE_HELP", (Parcelable)googleHelp);
        }
        else if (intent.hasExtra("EXTRA_IN_PRODUCT_HELP")) {
            final InProductHelp inProductHelp = zzbii.zza(intent, "EXTRA_IN_PRODUCT_HELP", InProductHelp.CREATOR);
            inProductHelp.zza(googleHelp);
            zzbii.zza(inProductHelp, intent, "EXTRA_IN_PRODUCT_HELP");
        }
        activity.startActivityForResult(intent, 123);
        zzak.zza((R)Status.zza);
    }
    
    @Override
    public final PendingResult<Status> zza(final GoogleApiClient googleApiClient, final Activity activity, final Intent intent) {
        Bitmap zza;
        if (GoogleHelpTogglingRegister.isTogglingEnabled()) {
            zza = zzan.zza(activity);
        }
        else {
            zza = null;
        }
        return googleApiClient.zza((PendingResult<Status>)new zzh(this, googleApiClient, intent, zza, activity));
    }
    
    @Override
    public final PendingResult<Status> zza(final GoogleApiClient googleApiClient, final GoogleHelp googleHelp, final Bundle bundle, final long n) {
        return googleApiClient.zza((PendingResult<Status>)new zzah(this, googleApiClient, bundle, n, googleHelp));
    }
    
    @Override
    public final PendingResult<Status> zza(final GoogleApiClient googleApiClient, final GoogleHelp googleHelp, final FeedbackOptions feedbackOptions, final Bundle bundle, final long n) {
        return googleApiClient.zza((PendingResult<Status>)new zzl(this, googleApiClient, feedbackOptions, bundle, n, googleHelp));
    }
    
    @Override
    public final PendingResult<Status> zzb(final GoogleApiClient googleApiClient, final GoogleHelp googleHelp, final Bundle bundle, final long n) {
        return googleApiClient.zza((PendingResult<Status>)new zzj(this, googleApiClient, bundle, n, googleHelp));
    }
}
