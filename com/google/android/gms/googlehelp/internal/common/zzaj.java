package com.google.android.gms.googlehelp.internal.common;

import android.os.RemoteException;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.googlehelp.zzd;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.common.api.Result;

abstract class zzaj<R extends Result> extends zzn<R, zzam>
{
    public zzaj(final GoogleApiClient googleApiClient) {
        super(com.google.android.gms.googlehelp.zzd.zza, googleApiClient);
    }
    
    protected abstract void zza(final Context p0, final zzar p1) throws RemoteException;
}
