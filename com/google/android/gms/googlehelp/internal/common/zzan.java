package com.google.android.gms.googlehelp.internal.common;

import android.content.Context;
import android.view.View;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import java.util.concurrent.ExecutionException;
import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import android.graphics.Bitmap;
import android.app.Activity;

public final class zzan
{
    public static Bitmap zza(final Activity activity) {
        final FutureTask<Bitmap> futureTask = new FutureTask<Bitmap>(new zzao(activity));
        activity.runOnUiThread((Runnable)futureTask);
        try {
            return futureTask.get();
        }
        catch (InterruptedException | ExecutionException ex) {
            Log.w("gH_Utils", "Taking screenshot failed!");
            return null;
        }
    }
    
    private static Bitmap zzc(final Activity activity) {
        try {
            final View rootView = activity.getWindow().getDecorView().getRootView();
            final View viewById = rootView.findViewById(16908290);
            if (viewById == null) {
                return null;
            }
            final int identifier = ((Context)activity).getResources().getIdentifier("status_bar_height", "dimen", "android");
            int dimensionPixelSize;
            if (identifier != 0) {
                dimensionPixelSize = ((Context)activity).getResources().getDimensionPixelSize(identifier);
            }
            else {
                dimensionPixelSize = 0;
            }
            final int n = dimensionPixelSize + viewById.getTop();
            if (n >= rootView.getHeight()) {
                return null;
            }
            final Bitmap bitmap = Bitmap.createBitmap(rootView.getWidth(), rootView.getHeight(), Bitmap$Config.RGB_565);
            rootView.draw(new Canvas(bitmap));
            return Bitmap.createBitmap(bitmap, 0, n, bitmap.getWidth(), Math.min(bitmap.getHeight() - n, viewById.getHeight()));
        }
        catch (Exception ex) {
            Log.w("gH_Utils", "Get screenshot failed!", (Throwable)ex);
            return null;
        }
    }
}
