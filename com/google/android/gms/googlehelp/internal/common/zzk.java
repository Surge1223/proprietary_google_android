package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.common.api.Status;

final class zzk extends zzbb
{
    private final /* synthetic */ zzj zza;
    
    zzk(final zzj zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        this.zza.zza((R)Status.zza);
    }
}
