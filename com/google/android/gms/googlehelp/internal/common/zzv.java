package com.google.android.gms.googlehelp.internal.common;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.googlehelp.zza;
import android.content.Context;
import com.google.android.gms.googlehelp.GoogleHelp;
import android.app.Activity;
import com.google.android.gms.feedback.BaseFeedbackProductSpecificData;
import com.google.android.gms.googlehelp.BaseHelpProductSpecificData;
import android.content.Intent;

final class zzv extends zzbb
{
    private final /* synthetic */ Intent zza;
    private final /* synthetic */ BaseHelpProductSpecificData zzb;
    private final /* synthetic */ BaseFeedbackProductSpecificData zzc;
    private final /* synthetic */ Activity zzd;
    private final /* synthetic */ zzak zze;
    private final /* synthetic */ zzg zzf;
    
    zzv(final zzg zzf, final Intent zza, final BaseHelpProductSpecificData zzb, final BaseFeedbackProductSpecificData zzc, final Activity zzd, final zzak zze) {
        this.zzf = zzf;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    @Override
    public final void zza(final GoogleHelp googleHelp) {
        final long nanoTime = System.nanoTime();
        this.zza.putExtra("EXTRA_START_TICK", nanoTime);
        if (this.zzb != null || this.zzc != null) {
            new zzax(googleHelp).zza((Context)this.zzd, this.zzc, this.zzb, nanoTime);
        }
        final zza zza = new zza(googleHelp);
        zza.zza(GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE);
        if (zza.zza() != null) {
            zza.zza().zzb = zzg.zza(this.zzd);
        }
        zzg.zza(this.zze, this.zzd, this.zza, googleHelp);
    }
}
