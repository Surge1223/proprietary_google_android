package com.google.android.gms.googlehelp;

import java.util.List;
import android.os.Bundle;
import android.accounts.Account;
import android.net.Uri;
import com.google.android.gms.googlehelp.internal.common.zzau;
import com.google.android.gms.feedback.ErrorReport;
import com.google.android.gms.feedback.ThemeSettings;
import com.google.android.gms.googlehelp.internal.common.TogglingData;
import android.app.PendingIntent;
import android.graphics.Bitmap;
import com.google.android.gms.internal.zzbie;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class zzc implements Parcelable.Creator<GoogleHelp>
{
}
