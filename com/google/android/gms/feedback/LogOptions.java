package com.google.android.gms.feedback;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class LogOptions extends zzbid
{
    public static final Parcelable.Creator<LogOptions> CREATOR;
    private String zza;
    private boolean zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzj();
    }
    
    LogOptions(final String zza, final boolean zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, zza);
    }
}
