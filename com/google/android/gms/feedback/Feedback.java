package com.google.android.gms.feedback;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.internal.zzcgy;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Api;

public final class Feedback
{
    public static final Api<Object> API;
    private static final Status zza;
    private static final Api.zzf<zzcgy> zzb;
    private static final Api.zza<zzcgy, Object> zzc;
    
    static {
        zza = new Status(13);
        zzb = new Api.zzf();
        zzc = new zzb();
        API = new Api<Object>("Feedback.API", (Api.zza<C, Object>)Feedback.zzc, (Api.zzf<C>)Feedback.zzb);
    }
    
    abstract static class zza extends zzn<Status, zzcgy>
    {
    }
}
