package com.google.android.gms.feedback;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.util.Log;
import android.content.res.TypedArray;
import android.content.Context;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class ThemeSettings extends zzbid
{
    public static final Parcelable.Creator<ThemeSettings> CREATOR;
    private int zza;
    private int zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzk();
    }
    
    public ThemeSettings() {
        this(0, 0);
    }
    
    ThemeSettings(final int zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    private static int zza(final Context context, final String s, final TypedArray typedArray) {
        try {
            try {
                final int color = typedArray.getColor(0, 0);
                typedArray.recycle();
                return color;
            }
            finally {}
        }
        catch (Exception ex) {
            final String value = String.valueOf(context);
            final StringBuilder sb = new StringBuilder(48 + String.valueOf(value).length() + String.valueOf(s).length());
            sb.append("Could not get theme color for [context: ");
            sb.append(value);
            sb.append(", attr: ");
            sb.append(s);
            Log.w("ThemeSettings", sb.toString());
            typedArray.recycle();
            return 0;
        }
        typedArray.recycle();
    }
    
    public ThemeSettings setPrimaryColor(final int zzb) {
        this.zzb = zzb;
        return this;
    }
    
    public ThemeSettings setTheme(final int zza) {
        this.zza = zza;
        return this;
    }
    
    public void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza);
        zzbig.zza(parcel, 3, this.zzb);
        zzbig.zza(parcel, zza);
    }
}
