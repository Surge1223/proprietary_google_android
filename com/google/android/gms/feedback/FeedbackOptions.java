package com.google.android.gms.feedback;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzau;
import java.util.ArrayList;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.app.ApplicationErrorReport$CrashInfo;
import android.graphics.Bitmap;
import java.util.List;
import com.google.android.gms.common.data.BitmapTeleporter;
import android.app.ApplicationErrorReport;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FeedbackOptions extends zzbid
{
    public static final Parcelable.Creator<FeedbackOptions> CREATOR;
    private String zza;
    private Bundle zzb;
    private String zzc;
    private ApplicationErrorReport zzd;
    private String zze;
    private BitmapTeleporter zzf;
    private String zzg;
    private List<FileTeleporter> zzh;
    private boolean zzi;
    private ThemeSettings zzj;
    private LogOptions zzk;
    private boolean zzl;
    private Bitmap zzm;
    private BaseFeedbackProductSpecificData zzn;
    
    static {
        CREATOR = (Parcelable.Creator)new zzh();
    }
    
    private FeedbackOptions(final ApplicationErrorReport applicationErrorReport) {
        this(null, null, null, applicationErrorReport, null, null, null, null, true, null, null, false, null);
    }
    
    FeedbackOptions(final String zza, final Bundle zzb, final String zzc, final ApplicationErrorReport zzd, final String zze, final BitmapTeleporter zzf, final String zzg, final List<FileTeleporter> zzh, final boolean zzi, final ThemeSettings zzj, final LogOptions zzk, final boolean zzl, final Bitmap zzm) {
        this.zzn = null;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
    }
    
    private final FeedbackOptions zza(final ApplicationErrorReport$CrashInfo crashInfo) {
        this.zzd.crashInfo = crashInfo;
        return this;
    }
    
    private final FeedbackOptions zza(final Bitmap zzm) {
        this.zzm = zzm;
        return this;
    }
    
    private final FeedbackOptions zza(final Bundle zzb) {
        this.zzb = zzb;
        return this;
    }
    
    private final FeedbackOptions zza(final BitmapTeleporter zzf) {
        this.zzf = zzf;
        return this;
    }
    
    private final FeedbackOptions zza(final BaseFeedbackProductSpecificData zzn) {
        this.zzn = zzn;
        return this;
    }
    
    private final FeedbackOptions zza(final LogOptions zzk) {
        this.zzk = zzk;
        return this;
    }
    
    private final FeedbackOptions zza(final ThemeSettings zzj) {
        this.zzj = zzj;
        return this;
    }
    
    private final FeedbackOptions zza(final String zza) {
        this.zza = zza;
        return this;
    }
    
    public static FeedbackOptions zza(final List<FileTeleporter> zzh) {
        final FeedbackOptions feedbackOptions = new FeedbackOptions(null);
        feedbackOptions.zzh = zzh;
        return feedbackOptions;
    }
    
    private final FeedbackOptions zza(final boolean zzi) {
        this.zzi = zzi;
        return this;
    }
    
    private final FeedbackOptions zzb(final String zzc) {
        this.zzc = zzc;
        return this;
    }
    
    private final FeedbackOptions zzb(final List<FileTeleporter> zzh) {
        this.zzh = zzh;
        return this;
    }
    
    private final FeedbackOptions zzb(final boolean zzl) {
        this.zzl = zzl;
        return this;
    }
    
    private final FeedbackOptions zzc(final String zze) {
        this.zze = zze;
        return this;
    }
    
    private final FeedbackOptions zzd(final String zzg) {
        this.zzg = zzg;
        return this;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, this.zza, false);
        zzbig.zza(parcel, 3, this.zzb, false);
        zzbig.zza(parcel, 5, this.zzc, false);
        zzbig.zza(parcel, 6, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, 7, this.zze, false);
        zzbig.zza(parcel, 8, (Parcelable)this.zzf, n, false);
        zzbig.zza(parcel, 9, this.zzg, false);
        zzbig.zzc(parcel, 10, this.zzh, false);
        zzbig.zza(parcel, 11, this.zzi);
        zzbig.zza(parcel, 12, (Parcelable)this.zzj, n, false);
        zzbig.zza(parcel, 13, (Parcelable)this.zzk, n, false);
        zzbig.zza(parcel, 14, this.zzl);
        zzbig.zza(parcel, 15, (Parcelable)this.zzm, n, false);
        zzbig.zza(parcel, zza);
    }
    
    @Deprecated
    public final String zza() {
        return this.zza;
    }
    
    @Deprecated
    public final Bundle zzb() {
        return this.zzb;
    }
    
    @Deprecated
    public final ThemeSettings zzc() {
        return this.zzj;
    }
    
    @Deprecated
    public final String zzd() {
        return this.zzc;
    }
    
    @Deprecated
    public final ApplicationErrorReport$CrashInfo zze() {
        if (this.zzd == null) {
            return null;
        }
        return this.zzd.crashInfo;
    }
    
    @Deprecated
    public final String zzf() {
        return this.zze;
    }
    
    @Deprecated
    public final Bitmap zzg() {
        return this.zzm;
    }
    
    @Deprecated
    public final BitmapTeleporter zzh() {
        return this.zzf;
    }
    
    @Deprecated
    public final String zzi() {
        return this.zzg;
    }
    
    @Deprecated
    public final List<FileTeleporter> zzj() {
        return this.zzh;
    }
    
    @Deprecated
    public final boolean zzk() {
        return this.zzi;
    }
    
    @Deprecated
    public final LogOptions zzl() {
        return this.zzk;
    }
    
    @Deprecated
    public final boolean zzm() {
        return this.zzl;
    }
    
    public static class Builder
    {
        private Bitmap zza;
        private BitmapTeleporter zzb;
        private String zzc;
        private Bundle zzd;
        private String zze;
        private String zzf;
        private List<FileTeleporter> zzg;
        private boolean zzh;
        private ThemeSettings zzi;
        private LogOptions zzj;
        private boolean zzk;
        private BaseFeedbackProductSpecificData zzl;
        
        public Builder() {
            this.zzd = new Bundle();
            this.zzg = new ArrayList<FileTeleporter>();
        }
        
        private final void zza(final boolean zzk) {
            if (this.zzd.isEmpty() && this.zzg.isEmpty() && this.zzk != zzk) {
                throw new IllegalStateException("Can't mix pii-full psd and pii-free psd");
            }
            this.zzk = zzk;
        }
        
        @Deprecated
        public Builder addPsd(final String s, final String s2) {
            if (!this.zzk) {
                this.zzd.putString(s, s2);
                return this;
            }
            throw new IllegalStateException("Can't call addPsd after psd is already certified pii free");
        }
        
        @Deprecated
        public Builder addPsdBundle(final Bundle bundle) {
            if (!this.zzk) {
                if (bundle != null) {
                    this.zzd.putAll(bundle);
                }
                return this;
            }
            throw new IllegalStateException("Can't call addPsdBundle after psd is already certified pii free");
        }
        
        public FeedbackOptions build() {
            return new FeedbackOptions(new ApplicationErrorReport(), null).zza(this.zza).zza(this.zzb).zza(this.zzc).zzb(this.zze).zza(this.zzd).zzc(this.zzf).zzb(this.zzg).zza(this.zzh).zza(this.zzi).zza(this.zzj).zzb(this.zzk).zza(this.zzl);
        }
        
        public Builder setAccountInUse(final String zzc) {
            this.zzc = zzc;
            return this;
        }
        
        @Deprecated
        public Builder setScreenshot(final Bitmap bitmap) {
            if (bitmap != null) {
                this.zzb = new BitmapTeleporter(bitmap);
            }
            return this;
        }
    }
    
    public static class CrashBuilder extends Builder
    {
        private final ApplicationErrorReport zza;
        private String zzb;
        
        public CrashBuilder() {
            this.zza = new ApplicationErrorReport();
            this.zza.crashInfo = new ApplicationErrorReport$CrashInfo();
            this.zza.crashInfo.throwLineNumber = -1;
        }
        
        @Override
        public FeedbackOptions build() {
            zzau.zza(this.zza.crashInfo.exceptionClassName);
            zzau.zza(this.zza.crashInfo.throwClassName);
            zzau.zza(this.zza.crashInfo.throwMethodName);
            zzau.zza(this.zza.crashInfo.stackTrace);
            if (TextUtils.isEmpty((CharSequence)this.zza.crashInfo.throwFileName)) {
                this.zza.crashInfo.throwFileName = "unknown";
            }
            return super.build().zza(this.zza.crashInfo).zzd(this.zzb);
        }
        
        public CrashBuilder setExceptionClassName(final String exceptionClassName) {
            this.zza.crashInfo.exceptionClassName = exceptionClassName;
            return this;
        }
        
        public CrashBuilder setExceptionMessage(final String exceptionMessage) {
            this.zza.crashInfo.exceptionMessage = exceptionMessage;
            return this;
        }
        
        public CrashBuilder setStackTrace(final String stackTrace) {
            this.zza.crashInfo.stackTrace = stackTrace;
            return this;
        }
        
        public CrashBuilder setThrowClassName(final String throwClassName) {
            this.zza.crashInfo.throwClassName = throwClassName;
            return this;
        }
        
        public CrashBuilder setThrowFileName(final String throwFileName) {
            this.zza.crashInfo.throwFileName = throwFileName;
            return this;
        }
        
        public CrashBuilder setThrowLineNumber(final int throwLineNumber) {
            this.zza.crashInfo.throwLineNumber = throwLineNumber;
            return this;
        }
        
        public CrashBuilder setThrowMethodName(final String throwMethodName) {
            this.zza.crashInfo.throwMethodName = throwMethodName;
            return this;
        }
    }
}
