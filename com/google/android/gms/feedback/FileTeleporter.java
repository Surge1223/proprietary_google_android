package com.google.android.gms.feedback;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import java.io.OutputStream;
import java.io.DataOutputStream;
import android.os.Parcel;
import android.util.Log;
import java.io.Closeable;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public class FileTeleporter extends zzbid
{
    public static final Parcelable.Creator<FileTeleporter> CREATOR;
    final String zza;
    final String zzb;
    byte[] zzc;
    private ParcelFileDescriptor zzd;
    private File zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzi();
    }
    
    FileTeleporter(final ParcelFileDescriptor zzd, final String zza, final String zzb) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    private final FileOutputStream zza() {
        if (this.zze != null) {
            try {
                final File tempFile = File.createTempFile("teleporter", ".tmp", this.zze);
                try {
                    final FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
                    this.zzd = ParcelFileDescriptor.open(tempFile, 268435456);
                    tempFile.delete();
                    return fileOutputStream;
                }
                catch (FileNotFoundException ex2) {
                    throw new IllegalStateException("Temporary file is somehow already deleted.");
                }
            }
            catch (IOException ex) {
                throw new IllegalStateException("Could not create temporary file:", ex);
            }
        }
        throw new IllegalStateException("setTempDir() must be called before writing this object to a parcel.");
    }
    
    private static void zza(final Closeable closeable) {
        try {
            closeable.close();
        }
        catch (IOException ex) {
            Log.w("FileTeleporter", "Could not close stream", (Throwable)ex);
        }
    }
    
    public void setTempDir(final File zze) {
        if (zze != null) {
            this.zze = zze;
            return;
        }
        throw new NullPointerException("Cannot set null temp directory");
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        if (this.zzd == null) {
            final DataOutputStream dataOutputStream = new DataOutputStream(this.zza());
            try {
                try {
                    dataOutputStream.writeInt(this.zzc.length);
                    dataOutputStream.writeUTF(this.zza);
                    dataOutputStream.writeUTF(this.zzb);
                    dataOutputStream.write(this.zzc);
                    zza(dataOutputStream);
                }
                finally {}
            }
            catch (IOException ex) {
                throw new IllegalStateException("Could not write into unlinked file", ex);
            }
            zza(dataOutputStream);
        }
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 2, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, 3, this.zza, false);
        zzbig.zza(parcel, 4, this.zzb, false);
        zzbig.zza(parcel, zza);
    }
}
