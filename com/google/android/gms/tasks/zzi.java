package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zzi<TResult> implements zzq<TResult>
{
    private final Executor zza;
    private final Object zzb;
    private OnCompleteListener<TResult> zzc;
    
    public zzi(final Executor zza, final OnCompleteListener<TResult> zzc) {
        this.zzb = new Object();
        this.zza = zza;
        this.zzc = zzc;
    }
    
    @Override
    public final void zza(final Task<TResult> task) {
        synchronized (this.zzb) {
            if (this.zzc == null) {
                return;
            }
            // monitorexit(this.zzb)
            this.zza.execute(new zzj(this, task));
        }
    }
}
