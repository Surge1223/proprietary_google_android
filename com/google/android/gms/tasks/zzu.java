package com.google.android.gms.tasks;

import java.util.concurrent.Executor;
import java.util.concurrent.CancellationException;
import com.google.android.gms.common.internal.zzau;

final class zzu<TResult> extends Task<TResult>
{
    private final Object zza;
    private final zzr<TResult> zzb;
    private boolean zzc;
    private volatile boolean zzd;
    private TResult zze;
    private Exception zzf;
    
    zzu() {
        this.zza = new Object();
        this.zzb = new zzr<TResult>();
    }
    
    private final void zzb() {
        zzau.zza(this.zzc, (Object)"Task is not yet complete");
    }
    
    private final void zzc() {
        zzau.zza(this.zzc ^ true, (Object)"Task is already complete");
    }
    
    private final void zzd() {
        if (!this.zzd) {
            return;
        }
        throw new CancellationException("Task is already canceled.");
    }
    
    private final void zze() {
        synchronized (this.zza) {
            if (!this.zzc) {
                return;
            }
            // monitorexit(this.zza)
            this.zzb.zza(this);
        }
    }
    
    @Override
    public final Task<TResult> addOnCompleteListener(final OnCompleteListener<TResult> onCompleteListener) {
        return this.addOnCompleteListener(TaskExecutors.MAIN_THREAD, onCompleteListener);
    }
    
    @Override
    public final Task<TResult> addOnCompleteListener(final Executor executor, final OnCompleteListener<TResult> onCompleteListener) {
        this.zzb.zza(new zzi<TResult>(executor, onCompleteListener));
        this.zze();
        return this;
    }
    
    @Override
    public final Exception getException() {
        synchronized (this.zza) {
            return this.zzf;
        }
    }
    
    @Override
    public final TResult getResult() {
        synchronized (this.zza) {
            this.zzb();
            this.zzd();
            if (this.zzf == null) {
                return this.zze;
            }
            throw new RuntimeExecutionException(this.zzf);
        }
    }
    
    @Override
    public final boolean isSuccessful() {
        synchronized (this.zza) {
            return this.zzc && !this.zzd && this.zzf == null;
        }
    }
    
    public final void zza(final Exception zzf) {
        zzau.zza(zzf, "Exception must not be null");
        synchronized (this.zza) {
            this.zzc();
            this.zzc = true;
            this.zzf = zzf;
            // monitorexit(this.zza)
            this.zzb.zza(this);
        }
    }
    
    public final void zza(final TResult zze) {
        synchronized (this.zza) {
            this.zzc();
            this.zzc = true;
            this.zze = zze;
            // monitorexit(this.zza)
            this.zzb.zza(this);
        }
    }
    
    public final boolean zzb(final Exception zzf) {
        zzau.zza(zzf, "Exception must not be null");
        synchronized (this.zza) {
            if (this.zzc) {
                return false;
            }
            this.zzc = true;
            this.zzf = zzf;
            // monitorexit(this.zza)
            this.zzb.zza(this);
            return true;
        }
    }
    
    public final boolean zzb(final TResult zze) {
        synchronized (this.zza) {
            if (this.zzc) {
                return false;
            }
            this.zzc = true;
            this.zze = zze;
            // monitorexit(this.zza)
            this.zzb.zza(this);
            return true;
        }
    }
}
