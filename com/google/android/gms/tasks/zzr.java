package com.google.android.gms.tasks;

import java.util.ArrayDeque;
import java.util.Queue;

final class zzr<TResult>
{
    private final Object zza;
    private Queue<zzq<TResult>> zzb;
    private boolean zzc;
    
    zzr() {
        this.zza = new Object();
    }
    
    public final void zza(final Task<TResult> task) {
        synchronized (this.zza) {
            if (this.zzb != null) {
                if (!this.zzc) {
                    this.zzc = true;
                    // monitorexit(this.zza)
                    while (true) {
                        final Object zza = this.zza;
                        synchronized (this.zza) {
                            final zzq<TResult> zzq = this.zzb.poll();
                            if (zzq == null) {
                                this.zzc = false;
                                return;
                            }
                            // monitorexit(this.zza)
                            zzq.zza(task);
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    public final void zza(final zzq<TResult> zzq) {
        synchronized (this.zza) {
            if (this.zzb == null) {
                this.zzb = new ArrayDeque<zzq<TResult>>();
            }
            this.zzb.add(zzq);
        }
    }
}
