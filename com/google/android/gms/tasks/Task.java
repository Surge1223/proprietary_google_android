package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

public abstract class Task<TResult>
{
    public Task<TResult> addOnCompleteListener(final OnCompleteListener<TResult> onCompleteListener) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }
    
    public Task<TResult> addOnCompleteListener(final Executor executor, final OnCompleteListener<TResult> onCompleteListener) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }
    
    public abstract Exception getException();
    
    public abstract TResult getResult();
    
    public abstract boolean isSuccessful();
}
