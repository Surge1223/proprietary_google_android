package com.google.android.gms.tasks;

public class TaskCompletionSource<TResult>
{
    private final zzu<TResult> zza;
    private final CancellationToken zzb;
    
    public TaskCompletionSource() {
        this.zza = new zzu<TResult>();
        this.zzb = null;
    }
    
    public Task<TResult> getTask() {
        return this.zza;
    }
    
    public void setException(final Exception ex) {
        this.zza.zza(ex);
    }
    
    public void setResult(final TResult tResult) {
        this.zza.zza(tResult);
    }
    
    public boolean trySetException(final Exception ex) {
        return this.zza.zzb(ex);
    }
    
    public boolean trySetResult(final TResult tResult) {
        return this.zza.zzb(tResult);
    }
}
