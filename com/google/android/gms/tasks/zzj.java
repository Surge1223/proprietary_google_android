package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zzj implements Runnable
{
    private final /* synthetic */ Task zza;
    private final /* synthetic */ zzi zzb;
    
    zzj(final zzi zzb, final Task zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        synchronized (this.zzb.zzb) {
            if (this.zzb.zzc != null) {
                this.zzb.zzc.onComplete(this.zza);
            }
        }
    }
}
