package com.google.android.gms.tasks;

import android.os.Looper;
import android.os.Handler;
import java.util.concurrent.Executor;

public final class TaskExecutors
{
    public static final Executor MAIN_THREAD;
    static final Executor zza;
    
    static {
        MAIN_THREAD = new zza();
        zza = new zzt();
    }
    
    static final class zza implements Executor
    {
        private final Handler zza;
        
        public zza() {
            this.zza = new Handler(Looper.getMainLooper());
        }
        
        @Override
        public final void execute(final Runnable runnable) {
            this.zza.post(runnable);
        }
    }
}
