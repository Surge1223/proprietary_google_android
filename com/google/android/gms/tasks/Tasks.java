package com.google.android.gms.tasks;

public final class Tasks
{
    public static <TResult> Task<TResult> forException(final Exception ex) {
        final zzu<TResult> zzu = new zzu<TResult>();
        zzu.zza(ex);
        return zzu;
    }
}
