package com.google.firebase;

import com.google.android.gms.common.internal.zzau;

public class FirebaseException extends Exception
{
    protected FirebaseException() {
    }
    
    public FirebaseException(final String s) {
        super(zzau.zza(s, (Object)"Detail message must not be empty"));
    }
}
