package com.google.firebase;

public class FirebaseApiNotAvailableException extends FirebaseException
{
    public FirebaseApiNotAvailableException(final String s) {
        super(s);
    }
}
