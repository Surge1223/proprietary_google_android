package com.google.firebase.appindexing;

import com.google.firebase.appindexing.internal.Thing;
import android.os.Bundle;
import com.google.android.gms.internal.zzgnj;
import com.google.firebase.appindexing.builders.IndexableBuilder;

public interface Indexable
{
    public static class Builder extends IndexableBuilder<Builder>
    {
        public Builder() {
            this("Thing");
        }
        
        public Builder(final String s) {
            super(s);
        }
    }
    
    public interface Metadata
    {
        public static final class Builder
        {
            private static final zzgnj zza;
            private boolean zzb;
            private int zzc;
            private String zzd;
            private final Bundle zze;
            
            static {
                zza = new zzgnj();
            }
            
            public Builder() {
                this.zzb = Builder.zza.zza;
                this.zzc = Builder.zza.zzb;
                this.zzd = Builder.zza.zzc;
                this.zze = new Bundle();
            }
            
            public final Builder put(final String s, final String... array) {
                IndexableBuilder.zza(this.zze, s, array);
                return this;
            }
            
            public final Builder setWorksOffline(final boolean zzb) {
                this.zzb = zzb;
                return this;
            }
            
            public final Thing.zza zza() {
                return new Thing.zza(this.zzb, this.zzc, this.zzd, this.zze);
            }
        }
    }
}
