package com.google.firebase.appindexing;

public class FirebaseAppIndexingArgumentTooLargeException extends FirebaseAppIndexingInvalidArgumentException
{
    public FirebaseAppIndexingArgumentTooLargeException(final String s) {
        super(s);
    }
}
