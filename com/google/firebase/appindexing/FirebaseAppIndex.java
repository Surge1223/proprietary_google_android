package com.google.firebase.appindexing;

import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.internal.zzk;
import com.google.android.gms.common.internal.zzau;
import android.content.Context;
import java.lang.ref.WeakReference;

public abstract class FirebaseAppIndex
{
    private static WeakReference<FirebaseAppIndex> zza;
    
    public static FirebaseAppIndex getInstance(final Context context) {
        synchronized (FirebaseAppIndex.class) {
            zzau.zza(context);
            FirebaseAppIndex firebaseAppIndex;
            if ((firebaseAppIndex = zza()) == null) {
                firebaseAppIndex = zza(context.getApplicationContext());
            }
            return firebaseAppIndex;
        }
    }
    
    private static FirebaseAppIndex zza() {
        if (FirebaseAppIndex.zza == null) {
            return null;
        }
        return FirebaseAppIndex.zza.get();
    }
    
    private static FirebaseAppIndex zza(final Context context) {
        final zzk zzk = new zzk(context);
        FirebaseAppIndex.zza = new WeakReference<FirebaseAppIndex>(zzk);
        return zzk;
    }
    
    public abstract Task<Void> removeAll();
    
    public abstract Task<Void> update(final Indexable... p0);
}
