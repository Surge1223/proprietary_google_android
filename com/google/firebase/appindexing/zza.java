package com.google.firebase.appindexing;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Iterator;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zza extends zzbid implements Iterable<zzc>
{
    public static final Parcelable.Creator<zza> CREATOR;
    final String[] zza;
    
    static {
        CREATOR = (Parcelable.Creator)new zze();
    }
    
    public zza(final String[] zza) {
        this.zza = zza;
    }
    
    @Override
    public final Iterator<zzc> iterator() {
        return new zzd(this, null);
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, zza);
    }
}
