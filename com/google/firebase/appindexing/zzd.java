package com.google.firebase.appindexing;

import java.util.NoSuchElementException;
import java.util.Iterator;

final class zzd implements Iterator<zzc>
{
    private int zza;
    private final /* synthetic */ zza zzb;
    
    private zzd(final zza zzb) {
        this.zzb = zzb;
        this.zza = 0;
    }
    
    @Override
    public final boolean hasNext() {
        return this.zza < this.zzb.zza.length;
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
