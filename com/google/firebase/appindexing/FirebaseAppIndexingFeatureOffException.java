package com.google.firebase.appindexing;

public class FirebaseAppIndexingFeatureOffException extends FirebaseAppIndexingException
{
    public FirebaseAppIndexingFeatureOffException(final String s) {
        super(s);
    }
}
