package com.google.firebase.appindexing.internal;

import com.google.android.gms.common.api.internal.zzdn;
import android.os.Handler;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.OnCompleteListener;

final class zzq implements OnCompleteListener
{
    private final zzo zza;
    private final zzm zzb;
    private final TaskCompletionSource zzc;
    
    zzq(final zzo zza, final zzm zzb, final TaskCompletionSource zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public final void onComplete(final Task task) {
        this.zza.zza(this.zzb, this.zzc, task);
    }
}
