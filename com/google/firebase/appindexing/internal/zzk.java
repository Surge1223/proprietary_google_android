package com.google.firebase.appindexing.internal;

import com.google.android.gms.tasks.Tasks;
import com.google.firebase.appindexing.FirebaseAppIndexingInvalidArgumentException;
import com.google.firebase.appindexing.Indexable;
import com.google.android.gms.tasks.Task;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApi;
import com.google.firebase.appindexing.FirebaseAppIndex;

public final class zzk extends FirebaseAppIndex
{
    private final GoogleApi<?> zza;
    private zzo zzb;
    
    public zzk(final Context context) {
        this(context, new zzl(context));
    }
    
    private zzk(final Context context, final GoogleApi<Object> zza) {
        this.zza = zza;
        this.zzb = new zzo(this);
    }
    
    private final Task<Void> zza(final zzak zzak) {
        return this.zzb.zza(new zzm(zzak));
    }
    
    @Override
    public final Task<Void> removeAll() {
        return this.zza(new zzak(4, null, null, null, null));
    }
    
    @Override
    public final Task<Void> update(final Indexable... array) {
        Label_0026: {
            if (array == null) {
                final Thing[] array2 = null;
                break Label_0026;
            }
            try {
                final Thing[] array3 = new Thing[array.length];
                System.arraycopy(array, 0, array3, 0, array.length);
                final Thing[] array2 = array3;
                return this.zza(new zzak(1, array2, null, null, null));
            }
            catch (ArrayStoreException ex) {
                return Tasks.forException(new FirebaseAppIndexingInvalidArgumentException("Custom Indexable-objects are not allowed. Please use the 'Indexables'-class for creating the objects."));
            }
        }
    }
}
