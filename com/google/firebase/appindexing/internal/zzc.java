package com.google.firebase.appindexing.internal;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzc extends zzbid
{
    public static final Parcelable.Creator<zzc> CREATOR;
    private int zza;
    private final boolean zzb;
    private final String zzc;
    private final String zzd;
    private final byte[] zze;
    private final boolean zzf;
    
    static {
        CREATOR = (Parcelable.Creator)new zzaj();
    }
    
    zzc(final int zza, final boolean zzb, final String zzc, final String zzd, final byte[] zze, final boolean zzf) {
        this.zza = 0;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MetadataImpl { ");
        sb.append("{ eventStatus: '");
        sb.append(this.zza);
        sb.append("' } ");
        sb.append("{ uploadable: '");
        sb.append(this.zzb);
        sb.append("' } ");
        if (this.zzc != null) {
            sb.append("{ completionToken: '");
            sb.append(this.zzc);
            sb.append("' } ");
        }
        if (this.zzd != null) {
            sb.append("{ accountName: '");
            sb.append(this.zzd);
            sb.append("' } ");
        }
        if (this.zze != null) {
            sb.append("{ ssbContext: [ ");
            for (final byte b : this.zze) {
                sb.append("0x");
                sb.append(Integer.toHexString(b));
                sb.append(" ");
            }
            sb.append("] } ");
        }
        sb.append("{ contextOnly: '");
        sb.append(this.zzf);
        sb.append("' } ");
        sb.append("}");
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, 4, this.zzd, false);
        zzbig.zza(parcel, 5, this.zze, false);
        zzbig.zza(parcel, 6, this.zzf);
        zzbig.zza(parcel, zza);
    }
}
