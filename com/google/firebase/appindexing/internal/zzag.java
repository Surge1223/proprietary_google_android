package com.google.firebase.appindexing.internal;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.IInterface;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.common.api.internal.zzcb;
import android.os.IBinder;
import com.google.android.gms.internal.zzey;

public final class zzag extends zzey implements zzaf
{
    zzag(final IBinder binder) {
        super(binder, "com.google.firebase.appindexing.internal.IAppIndexingService");
    }
    
    @Override
    public final zzi zza(final zzcb zzcb, final zzak zzak) throws RemoteException {
        final Parcel a_ = this.a_();
        zzfa.zza(a_, (IInterface)zzcb);
        zzfa.zza(a_, (Parcelable)zzak);
        final Parcel zza = this.zza(8, a_);
        final zzi zzi = zzfa.zza(zza, com.google.firebase.appindexing.internal.zzi.CREATOR);
        zza.recycle();
        return zzi;
    }
}
