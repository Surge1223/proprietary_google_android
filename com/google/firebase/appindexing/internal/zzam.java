package com.google.firebase.appindexing.internal;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Set;
import java.lang.reflect.Array;
import java.util.Arrays;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.firebase.appindexing.Indexable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;
import java.util.Comparator;

final class zzam implements Comparator
{
    static final Comparator zza;
    
    static {
        zza = new zzam();
    }
    
    @Override
    public final int compare(final Object o, final Object o2) {
        return Thing.zza((String)o, (String)o2);
    }
}
