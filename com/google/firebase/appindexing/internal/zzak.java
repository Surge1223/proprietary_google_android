package com.google.firebase.appindexing.internal;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzak extends zzbid
{
    public static final Parcelable.Creator<zzak> CREATOR;
    private final int zza;
    private final Thing[] zzb;
    private final String[] zzc;
    private final zzap zzd;
    private final String[] zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzal();
    }
    
    zzak(int zza, final Thing[] zzb, final String[] zzc, final zzap zzd, final String[] zze) {
        switch (zza) {
            default: {
                zza = 0;
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6: {
                this.zza = zza;
                this.zzb = zzb;
                this.zzc = zzc;
                this.zzd = zzd;
                this.zze = zze;
            }
        }
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza);
        zzbig.zza(parcel, 2, this.zzb, n, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, 4, (Parcelable)this.zzd, n, false);
        zzbig.zza(parcel, 5, this.zze, false);
        zzbig.zza(parcel, zza);
    }
}
