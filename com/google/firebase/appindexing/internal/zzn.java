package com.google.firebase.appindexing.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.internal.zzcc;

final class zzn extends zzcc
{
    private final /* synthetic */ TaskCompletionSource zza;
    
    zzn(final zzm zzm, final TaskCompletionSource zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza(final Status result) throws RemoteException {
        this.zza.setResult(result);
    }
}
