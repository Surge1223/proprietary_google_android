package com.google.firebase.appindexing.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcb;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzdn;

final class zzm extends zzdn<zzg, Status>
{
    private final zzak zza;
    
    zzm(final zzak zza) {
        this.zza = zza;
    }
}
