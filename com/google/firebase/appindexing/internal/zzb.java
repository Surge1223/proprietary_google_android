package com.google.firebase.appindexing.internal;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzb extends zzbid
{
    public static final Parcelable.Creator<zzb> CREATOR;
    private final String zza;
    private final String zzb;
    private final String zzc;
    private final String zzd;
    private final zzc zze;
    private final String zzf;
    
    static {
        CREATOR = (Parcelable.Creator)new zzd();
    }
    
    public zzb(final String zza, final String zzb, final String zzc, final String zzd, final zzc zze, final String zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ActionImpl { ");
        sb.append("{ actionType: '");
        sb.append(this.zza);
        sb.append("' } ");
        sb.append("{ objectName: '");
        sb.append(this.zzb);
        sb.append("' } ");
        sb.append("{ objectUrl: '");
        sb.append(this.zzc);
        sb.append("' } ");
        if (this.zzd != null) {
            sb.append("{ objectSameAs: '");
            sb.append(this.zzd);
            sb.append("' } ");
        }
        if (this.zze != null) {
            sb.append("{ metadata: '");
            sb.append(this.zze.toString());
            sb.append("' } ");
        }
        if (this.zzf != null) {
            sb.append("{ actionStatus: '");
            sb.append(this.zzf);
            sb.append("' } ");
        }
        sb.append("}");
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zza, false);
        zzbig.zza(parcel, 2, this.zzb, false);
        zzbig.zza(parcel, 3, this.zzc, false);
        zzbig.zza(parcel, 4, this.zzd, false);
        zzbig.zza(parcel, 5, (Parcelable)this.zze, n, false);
        zzbig.zza(parcel, 6, this.zzf, false);
        zzbig.zza(parcel, zza);
    }
}
