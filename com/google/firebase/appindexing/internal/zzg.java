package com.google.firebase.appindexing.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzl;

public final class zzg extends zzl<zzaf>
{
    static final Api<Object> zzc;
    private static final Api.zzf<zzg> zzd;
    private static final Api.zza<zzg, Object> zze;
    
    static {
        zzd = new Api.zzf();
        zze = new zzh();
        zzc = new Api<Object>("AppIndexing.API", (Api.zza<C, Object>)zzg.zze, (Api.zzf<C>)zzg.zzd);
    }
    
    public zzg(final Context context, final Looper looper, final ClientSettings clientSettings, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 113, clientSettings, connectionCallbacks, onConnectionFailedListener);
    }
    
    @Override
    protected final String getServiceDescriptor() {
        return "com.google.firebase.appindexing.internal.IAppIndexingService";
    }
    
    @Override
    protected final String getStartServiceAction() {
        return "com.google.android.gms.icing.APP_INDEXING_SERVICE";
    }
    
    @Override
    public final int zza() {
        return 12438000;
    }
}
