package com.google.firebase.appindexing.internal;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import com.google.firebase.appindexing.zza;
import com.google.android.gms.common.api.Status;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzbid;

public final class zze extends zzbid implements Result
{
    public static final Parcelable.Creator<zze> CREATOR;
    private final Status zza;
    private final zza zzb;
    
    static {
        CREATOR = (Parcelable.Creator)new zzf();
    }
    
    public zze(final Status zza, final zza zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final Status getStatus() {
        return this.zza;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, (Parcelable)this.getStatus(), n, false);
        zzbig.zza(parcel, 2, (Parcelable)this.zzb, n, false);
        zzbig.zza(parcel, zza);
    }
}
