package com.google.firebase.appindexing.internal;

import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.zzbid;

public final class zzi extends zzbid
{
    public static final Parcelable.Creator<zzi> CREATOR;
    private static final zzi zza;
    private static final zzi zzb;
    private static final zzi zzc;
    private final int zzd;
    
    static {
        zza = new zzi(1);
        zzb = new zzi(2);
        zzc = new zzi(3);
        CREATOR = (Parcelable.Creator)new zzj();
    }
    
    public zzi(final int zzd) {
        this.zzd = zzd;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzd);
        zzbig.zza(parcel, zza);
    }
}
