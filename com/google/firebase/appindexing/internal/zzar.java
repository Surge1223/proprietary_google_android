package com.google.firebase.appindexing.internal;

import com.google.firebase.appindexing.FirebaseAppIndexingInvalidArgumentException;
import com.google.firebase.appindexing.FirebaseAppIndexingTooManyArgumentsException;
import com.google.firebase.appindexing.FirebaseAppIndexingFeatureOffException;
import com.google.firebase.appindexing.FirebaseAppIndexingArgumentTooLargeException;
import com.google.android.gms.common.internal.zzau;
import com.google.firebase.appindexing.FirebaseAppIndexingException;
import com.google.android.gms.common.api.Status;

public final class zzar
{
    public static FirebaseAppIndexingException zza(final Status status, String s) {
        zzau.zza(status);
        final String statusMessage = status.getStatusMessage();
        if (statusMessage != null) {
            if (!statusMessage.isEmpty()) {
                s = statusMessage;
            }
        }
        switch (status.getStatusCode()) {
            default: {
                return new FirebaseAppIndexingException(s);
            }
            case 17514: {
                return new FirebaseAppIndexingArgumentTooLargeException(s);
            }
            case 17513: {
                return new FirebaseAppIndexingFeatureOffException(s);
            }
            case 17511: {
                return new FirebaseAppIndexingTooManyArgumentsException(s);
            }
            case 17510: {
                return new FirebaseAppIndexingInvalidArgumentException(s);
            }
        }
    }
}
