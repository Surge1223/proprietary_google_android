package com.google.firebase.appindexing.internal;

import android.os.Parcelable;
import com.google.android.gms.internal.zzbig;
import android.os.Parcel;
import java.util.Set;
import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Arrays;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import com.google.firebase.appindexing.Indexable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbid;

public final class Thing extends zzbid implements ReflectedParcelable, Indexable
{
    public static final Parcelable.Creator<Thing> CREATOR;
    private final int zza;
    private final Bundle zzb;
    private final zza zzc;
    private final String zzd;
    private final String zze;
    
    static {
        CREATOR = (Parcelable.Creator)new zzan();
    }
    
    public Thing(final int zza, final Bundle zzb, final zza zzc, final String zzd, final String zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzb.setClassLoader(this.getClass().getClassLoader());
    }
    
    public Thing(final Bundle zzb, final zza zzc, final String zzd, final String zze) {
        this.zza = 10;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    private static void zzb(final Bundle bundle, final StringBuilder sb) {
        try {
            final Set keySet = bundle.keySet();
            final String[] array = keySet.toArray(new String[keySet.size()]);
            Arrays.sort(array, zzam.zza);
            for (final String s : array) {
                sb.append("{ key: '");
                sb.append(s);
                sb.append("' value: ");
                final Object value = bundle.get(s);
                if (value == null) {
                    sb.append("<null>");
                }
                else if (value.getClass().isArray()) {
                    sb.append("[ ");
                    for (int j = 0; j < Array.getLength(value); ++j) {
                        sb.append("'");
                        sb.append(Array.get(value, j));
                        sb.append("' ");
                    }
                    sb.append("]");
                }
                else {
                    sb.append(value.toString());
                }
                sb.append(" } ");
            }
        }
        catch (RuntimeException ex) {
            sb.append("<error>");
        }
    }
    
    public final String getType() {
        return this.zze;
    }
    
    public final String getUrl() {
        return this.zzd;
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        String zze;
        if (this.zze.equals("Thing")) {
            zze = "Indexable";
        }
        else {
            zze = this.zze;
        }
        sb.append(zze);
        sb.append(" { { id: ");
        if (this.getUrl() == null) {
            sb.append("<null>");
        }
        else {
            sb.append("'");
            sb.append(this.getUrl());
            sb.append("'");
        }
        sb.append(" } Properties { ");
        zzb(this.zzb, sb);
        sb.append("} ");
        sb.append("Metadata { ");
        sb.append(this.zzc.toString());
        sb.append(" } ");
        sb.append("}");
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = zzbig.zza(parcel);
        zzbig.zza(parcel, 1, this.zzb, false);
        zzbig.zza(parcel, 2, (Parcelable)this.zzc, n, false);
        zzbig.zza(parcel, 3, this.getUrl(), false);
        zzbig.zza(parcel, 4, this.getType(), false);
        zzbig.zza(parcel, 1000, this.zza);
        zzbig.zza(parcel, zza);
    }
    
    public static final class zza extends zzbid
    {
        public static final Parcelable.Creator<zza> CREATOR;
        private final boolean zza;
        private final int zzb;
        private final String zzc;
        private final Bundle zzd;
        
        static {
            CREATOR = (Parcelable.Creator)new zzai();
        }
        
        public zza(final boolean zza, final int zzb, final String zzc, final Bundle bundle) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
            Bundle zzd = bundle;
            if (bundle == null) {
                zzd = new Bundle();
            }
            this.zzd = zzd;
        }
        
        @Override
        public final String toString() {
            final StringBuilder sb = new StringBuilder("worksOffline: ");
            sb.append(this.zza);
            sb.append(", score: ");
            sb.append(this.zzb);
            if (!this.zzc.isEmpty()) {
                sb.append(", accountEmail: ");
                sb.append(this.zzc);
            }
            if (this.zzd != null && !this.zzd.isEmpty()) {
                sb.append(", Properties { ");
                zzb(this.zzd, sb);
                sb.append("}");
            }
            return sb.toString();
        }
        
        public final void writeToParcel(final Parcel parcel, int zza) {
            zza = zzbig.zza(parcel);
            zzbig.zza(parcel, 1, this.zza);
            zzbig.zza(parcel, 2, this.zzb);
            zzbig.zza(parcel, 3, this.zzc, false);
            zzbig.zza(parcel, 4, this.zzd, false);
            zzbig.zza(parcel, zza);
        }
    }
}
