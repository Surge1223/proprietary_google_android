package com.google.firebase.appindexing.internal;

import android.util.Log;

public final class zzah
{
    public static int zza(final String s) {
        if (zza(3)) {
            return Log.d("FirebaseAppIndex", s);
        }
        return 0;
    }
    
    public static boolean zza(final int n) {
        return Log.isLoggable("FirebaseAppIndex", n) || Log.isLoggable("FirebaseAppIndex", n);
    }
}
