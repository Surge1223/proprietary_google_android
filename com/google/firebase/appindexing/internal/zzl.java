package com.google.firebase.appindexing.internal;

import com.google.android.gms.common.api.internal.zzdi;
import com.google.android.gms.common.api.Api;
import com.google.firebase.zzb;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApi;

final class zzl extends GoogleApi<Object>
{
    public zzl(final Context context) {
        super(context, (Api<Api.ApiOptions>)com.google.firebase.appindexing.internal.zzg.zzc, null, Looper.getMainLooper(), new zzb());
    }
}
