package com.google.firebase.appindexing.internal;

import com.google.android.gms.tasks.Tasks;
import com.google.firebase.appindexing.FirebaseAppIndexingInvalidArgumentException;
import com.google.firebase.appindexing.Indexable;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApi;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.android.gms.common.api.internal.zzdn;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import android.os.Handler;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.OnCompleteListener;

final class zzo implements OnCompleteListener<Void>, Executor
{
    private final Handler zza;
    private Task<Void> zzb;
    private final /* synthetic */ zzk zzc;
    
    public zzo(final zzk zzc) {
        this.zzc = zzc;
        this.zzb = null;
        this.zza = new Handler(zzc.zza.zzf());
    }
    
    private final void zza(final zzm zzm, final TaskCompletionSource<Void> taskCompletionSource) {
        this.zzc.zza.zzb(zzm).addOnCompleteListener(this, new zzp(taskCompletionSource));
    }
    
    @Override
    public final void execute(final Runnable runnable) {
        this.zza.post(runnable);
    }
    
    @Override
    public final void onComplete(final Task<Void> task) {
        synchronized (this) {
            if (task == this.zzb) {
                this.zzb = null;
            }
        }
    }
    
    public final Task<Void> zza(final zzm zzm) {
        final TaskCompletionSource<Void> taskCompletionSource = new TaskCompletionSource<Void>();
        final Task<Void> task = taskCompletionSource.getTask();
        synchronized (this) {
            final Task<Void> zzb = this.zzb;
            this.zzb = task;
            // monitorexit(this)
            task.addOnCompleteListener(this, this);
            if (zzb == null) {
                this.zza(zzm, taskCompletionSource);
            }
            else {
                zzb.addOnCompleteListener(this, new zzq(this, zzm, taskCompletionSource));
            }
            return task;
        }
    }
}
