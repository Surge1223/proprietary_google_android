package com.google.firebase.appindexing.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.OnCompleteListener;

final class zzp implements OnCompleteListener
{
    private final TaskCompletionSource zza;
    
    zzp(final TaskCompletionSource zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onComplete(final Task task) {
        final TaskCompletionSource zza = this.zza;
        if (!task.isSuccessful()) {
            zza.setException(task.getException());
            return;
        }
        final Status status = task.getResult();
        if (status.isSuccess()) {
            zza.setResult(null);
            return;
        }
        zza.setException(zzar.zza(status, "Indexing error, please try again."));
    }
}
