package com.google.firebase.appindexing.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcb;
import android.os.IInterface;

public interface zzaf extends IInterface
{
    zzi zza(final zzcb p0, final zzak p1) throws RemoteException;
}
