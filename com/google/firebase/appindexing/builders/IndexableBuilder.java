package com.google.firebase.appindexing.builders;

import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.internal.zzah;
import java.util.Arrays;
import com.google.android.gms.common.internal.zzau;
import com.google.firebase.appindexing.internal.Thing;
import android.os.Bundle;

public class IndexableBuilder<T extends IndexableBuilder<?>>
{
    private final Bundle zza;
    private final String zzb;
    private Thing.zza zzc;
    private String zzd;
    
    protected IndexableBuilder(final String zzb) {
        zzau.zza(zzb);
        zzau.zza(zzb);
        this.zza = new Bundle();
        this.zzb = zzb;
    }
    
    public static void zza(final Bundle bundle, final String s, final String... array) {
        zzau.zza(s);
        zzau.zza(array);
        final String[] array2 = Arrays.copyOf(array, array.length);
        if (array2.length > 0) {
            int n;
            for (int i = n = 0; i < Math.min(array2.length, 100); ++i) {
                array2[n] = array2[i];
                if (array2[i] == null) {
                    final StringBuilder sb = new StringBuilder(59);
                    sb.append("String at ");
                    sb.append(i);
                    sb.append(" is null and is ignored by put method.");
                    zzah.zza(sb.toString());
                }
                else {
                    final int length = array2[n].length();
                    final int n2 = 20000;
                    if (length > 20000) {
                        final StringBuilder sb2 = new StringBuilder(53);
                        sb2.append("String at ");
                        sb2.append(i);
                        sb2.append(" is too long, truncating string.");
                        zzah.zza(sb2.toString());
                        String substring = array2[n];
                        if (substring.length() > 20000) {
                            int n3 = n2;
                            if (Character.isHighSurrogate(substring.charAt(19999))) {
                                n3 = n2;
                                if (Character.isLowSurrogate(substring.charAt(20000))) {
                                    n3 = 19999;
                                }
                            }
                            substring = substring.substring(0, n3);
                        }
                        array2[n] = substring;
                    }
                    ++n;
                }
            }
            if (n > 0) {
                bundle.putStringArray(s, (String[])zza((String[])Arrays.copyOfRange((S[])array2, 0, n)));
            }
            return;
        }
        zzah.zza("String array is empty and is ignored by put method.");
    }
    
    private static <S> S[] zza(final S[] array) {
        if (array.length < 100) {
            return array;
        }
        zzah.zza("Input Array of elements is too big, cutting off.");
        return Arrays.copyOf(array, 100);
    }
    
    public final Indexable build() {
        final Bundle bundle = new Bundle(this.zza);
        Thing.zza zza;
        if (this.zzc == null) {
            zza = new Indexable.Metadata.Builder().zza();
        }
        else {
            zza = this.zzc;
        }
        return new Thing(bundle, zza, this.zzd, this.zzb);
    }
    
    public T put(final String s, final String... array) {
        zza(this.zza, s, array);
        return (T)this;
    }
    
    public T setMetadata(final Indexable.Metadata.Builder builder) {
        zzau.zza(this.zzc == null, (Object)"setMetadata may only be called once");
        zzau.zza(builder);
        this.zzc = builder.zza();
        return (T)this;
    }
    
    public final T setName(final String s) {
        zzau.zza(s);
        return this.put("name", s);
    }
    
    public final T setUrl(final String zzd) {
        zzau.zza(zzd);
        this.zzd = zzd;
        return (T)this;
    }
}
