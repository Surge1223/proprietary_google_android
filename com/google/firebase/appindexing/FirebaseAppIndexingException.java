package com.google.firebase.appindexing;

import com.google.firebase.FirebaseException;

public class FirebaseAppIndexingException extends FirebaseException
{
    public FirebaseAppIndexingException(final String s) {
        super(s);
    }
}
