package com.google.firebase.appindexing;

public class FirebaseAppIndexingInvalidArgumentException extends FirebaseAppIndexingException
{
    public FirebaseAppIndexingInvalidArgumentException(final String s) {
        super(s);
    }
}
