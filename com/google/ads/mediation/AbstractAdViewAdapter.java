package com.google.ads.mediation;

import android.util.AttributeSet;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.ads.BaseAdView;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.formats.zzd;
import java.util.Map;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.formats.NativeAdViewHolder;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdView;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.internal.client.zzbh;
import android.view.View;
import android.location.Location;
import java.util.Iterator;
import java.util.Set;
import java.util.Date;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.AdRequest;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import android.content.Context;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.internal.zzanu;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.mediation.zzb;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;

public abstract class AbstractAdViewAdapter implements MediationBannerAdapter, MediationNativeAdapter, OnImmersiveModeUpdatedListener, com.google.android.gms.ads.mediation.zzb, MediationRewardedVideoAdAdapter, zzanu
{
    public static final String AD_UNIT_ID_PARAMETER = "pubid";
    private AdView zza;
    private InterstitialAd zzb;
    private AdLoader zzc;
    private Context zzd;
    private InterstitialAd zze;
    private MediationRewardedVideoAdListener zzf;
    private final RewardedVideoAdListener zzg;
    
    public AbstractAdViewAdapter() {
        this.zzg = new com.google.ads.mediation.zza(this);
    }
    
    private final AdRequest zza(final Context context, final MediationAdRequest mediationAdRequest, final Bundle bundle, final Bundle bundle2) {
        final AdRequest.Builder builder = new AdRequest.Builder();
        final Date birthday = mediationAdRequest.getBirthday();
        if (birthday != null) {
            builder.setBirthday(birthday);
        }
        final int gender = mediationAdRequest.getGender();
        if (gender != 0) {
            builder.setGender(gender);
        }
        final Set<String> keywords = mediationAdRequest.getKeywords();
        if (keywords != null) {
            final Iterator<String> iterator = keywords.iterator();
            while (iterator.hasNext()) {
                builder.addKeyword(iterator.next());
            }
        }
        final Location location = mediationAdRequest.getLocation();
        if (location != null) {
            builder.setLocation(location);
        }
        if (mediationAdRequest.isTesting()) {
            zzx.zza();
            builder.addTestDevice(zzaht.zza(context));
        }
        if (mediationAdRequest.taggedForChildDirectedTreatment() != -1) {
            final int taggedForChildDirectedTreatment = mediationAdRequest.taggedForChildDirectedTreatment();
            boolean b = true;
            if (taggedForChildDirectedTreatment != 1) {
                b = false;
            }
            builder.tagForChildDirectedTreatment(b);
        }
        builder.setIsDesignedForFamilies(mediationAdRequest.isDesignedForFamilies());
        builder.addNetworkExtrasBundle(AdMobAdapter.class, this.zza(bundle, bundle2));
        return builder.build();
    }
    
    public String getAdUnitId(final Bundle bundle) {
        return bundle.getString("pubid");
    }
    
    @Override
    public View getBannerView() {
        return (View)this.zza;
    }
    
    @Override
    public Bundle getInterstitialAdapterInfo() {
        return new MediationAdapter.zza().zza(1).zza();
    }
    
    @Override
    public zzbh getVideoController() {
        if (this.zza != null) {
            final VideoController zza = this.zza.zza();
            if (zza != null) {
                return zza.zza();
            }
        }
        return null;
    }
    
    @Override
    public void initialize(final Context context, final MediationAdRequest mediationAdRequest, final String s, final MediationRewardedVideoAdListener zzf, final Bundle bundle, final Bundle bundle2) {
        this.zzd = context.getApplicationContext();
        (this.zzf = zzf).onInitializationSucceeded(this);
    }
    
    @Override
    public boolean isInitialized() {
        return this.zzf != null;
    }
    
    @Override
    public void loadAd(final MediationAdRequest mediationAdRequest, final Bundle bundle, final Bundle bundle2) {
        if (this.zzd != null && this.zzf != null) {
            (this.zze = new InterstitialAd(this.zzd)).zza(true);
            this.zze.setAdUnitId(this.getAdUnitId(bundle));
            this.zze.zza(this.zzg);
            this.zze.loadAd(this.zza(this.zzd, mediationAdRequest, bundle2, bundle));
            return;
        }
        zzaid.zzc("AdMobAdapter.loadAd called before initialize.");
    }
    
    @Override
    public void onDestroy() {
        if (this.zza != null) {
            this.zza.destroy();
            this.zza = null;
        }
        if (this.zzb != null) {
            this.zzb = null;
        }
        if (this.zzc != null) {
            this.zzc = null;
        }
        if (this.zze != null) {
            this.zze = null;
        }
    }
    
    @Override
    public void onImmersiveModeUpdated(final boolean b) {
        if (this.zzb != null) {
            this.zzb.setImmersiveMode(b);
        }
        if (this.zze != null) {
            this.zze.setImmersiveMode(b);
        }
    }
    
    @Override
    public void onPause() {
        if (this.zza != null) {
            this.zza.pause();
        }
    }
    
    @Override
    public void onResume() {
        if (this.zza != null) {
            this.zza.resume();
        }
    }
    
    @Override
    public void requestBannerAd(final Context context, final MediationBannerListener mediationBannerListener, final Bundle bundle, final AdSize adSize, final MediationAdRequest mediationAdRequest, final Bundle bundle2) {
        (this.zza = new AdView(context)).setAdSize(new AdSize(adSize.getWidth(), adSize.getHeight()));
        this.zza.setAdUnitId(this.getAdUnitId(bundle));
        this.zza.setAdListener(new zzd(this, mediationBannerListener));
        this.zza.loadAd(this.zza(context, mediationAdRequest, bundle2, bundle));
    }
    
    @Override
    public void requestInterstitialAd(final Context context, final MediationInterstitialListener mediationInterstitialListener, final Bundle bundle, final MediationAdRequest mediationAdRequest, final Bundle bundle2) {
        (this.zzb = new InterstitialAd(context)).setAdUnitId(this.getAdUnitId(bundle));
        this.zzb.setAdListener(new zze(this, mediationInterstitialListener));
        this.zzb.loadAd(this.zza(context, mediationAdRequest, bundle2, bundle));
    }
    
    @Override
    public void requestNativeAd(final Context context, final MediationNativeListener mediationNativeListener, final Bundle bundle, final NativeMediationAdRequest nativeMediationAdRequest, final Bundle bundle2) {
        final zzf zzf = new zzf(this, mediationNativeListener);
        final AdLoader.Builder withAdListener = new AdLoader.Builder(context, bundle.getString("pubid")).withAdListener(zzf);
        final NativeAdOptions nativeAdOptions = nativeMediationAdRequest.getNativeAdOptions();
        if (nativeAdOptions != null) {
            withAdListener.withNativeAdOptions(nativeAdOptions);
        }
        if (nativeMediationAdRequest.zza()) {
            withAdListener.zza(zzf);
        }
        if (nativeMediationAdRequest.isAppInstallAdRequested()) {
            withAdListener.forAppInstallAd(zzf);
        }
        if (nativeMediationAdRequest.isContentAdRequested()) {
            withAdListener.forContentAd(zzf);
        }
        if (nativeMediationAdRequest.zzb()) {
            for (final String s : nativeMediationAdRequest.zzc().keySet()) {
                zzf zzf2;
                if (nativeMediationAdRequest.zzc().get(s)) {
                    zzf2 = zzf;
                }
                else {
                    zzf2 = null;
                }
                withAdListener.forCustomTemplateAd(s, zzf, zzf2);
            }
        }
        (this.zzc = withAdListener.build()).loadAd(this.zza(context, nativeMediationAdRequest, bundle2, bundle));
    }
    
    @Override
    public void showInterstitial() {
        this.zzb.show();
    }
    
    @Override
    public void showVideo() {
        this.zze.show();
    }
    
    protected abstract Bundle zza(final Bundle p0, final Bundle p1);
    
    static final class zza extends NativeAppInstallAdMapper
    {
        private final NativeAppInstallAd zza;
        
        public zza(final NativeAppInstallAd zza) {
            this.zza = zza;
            this.setHeadline(zza.getHeadline().toString());
            this.setImages(zza.getImages());
            this.setBody(zza.getBody().toString());
            this.setIcon(zza.getIcon());
            this.setCallToAction(zza.getCallToAction().toString());
            if (zza.getStarRating() != null) {
                this.setStarRating(zza.getStarRating());
            }
            if (zza.getStore() != null) {
                this.setStore(zza.getStore().toString());
            }
            if (zza.getPrice() != null) {
                this.setPrice(zza.getPrice().toString());
            }
            this.setOverrideImpressionRecording(true);
            this.setOverrideClickHandling(true);
            this.zza(zza.getVideoController());
        }
        
        @Override
        public final void trackView(final View view) {
            if (view instanceof NativeAdView) {
                ((NativeAdView)view).setNativeAd(this.zza);
            }
            final NativeAdViewHolder nativeAdViewHolder = NativeAdViewHolder.zza.get(view);
            if (nativeAdViewHolder != null) {
                nativeAdViewHolder.setNativeAd(this.zza);
            }
        }
    }
    
    static final class zzb extends NativeContentAdMapper
    {
        private final NativeContentAd zza;
        
        public zzb(final NativeContentAd zza) {
            this.zza = zza;
            this.setHeadline(zza.getHeadline().toString());
            this.setImages(zza.getImages());
            this.setBody(zza.getBody().toString());
            if (zza.getLogo() != null) {
                this.setLogo(zza.getLogo());
            }
            this.setCallToAction(zza.getCallToAction().toString());
            this.setAdvertiser(zza.getAdvertiser().toString());
            this.setOverrideImpressionRecording(true);
            this.setOverrideClickHandling(true);
            this.zza(zza.getVideoController());
        }
        
        @Override
        public final void trackView(final View view) {
            if (view instanceof NativeAdView) {
                ((NativeAdView)view).setNativeAd(this.zza);
            }
            final NativeAdViewHolder nativeAdViewHolder = NativeAdViewHolder.zza.get(view);
            if (nativeAdViewHolder != null) {
                nativeAdViewHolder.setNativeAd(this.zza);
            }
        }
    }
    
    static final class zzc extends com.google.android.gms.ads.mediation.zza
    {
        private final UnifiedNativeAd zza;
        
        public zzc(final UnifiedNativeAd zza) {
            this.zza = zza;
            this.zza(zza.zza());
            this.zza(zza.zzb());
            this.zzb(zza.zzc());
            this.zza(zza.zzd());
            this.zzc(zza.zze());
            this.zzd(zza.zzf());
            this.zza(zza.zzg());
            this.zze(zza.zzh());
            this.zzf(zza.zzi());
            this.zza(zza.zzk());
            this.zza(true);
            this.zzb(true);
            this.zza(zza.zzj());
        }
        
        @Override
        public final void zza(final View view, final Map<String, View> map, final Map<String, View> map2) {
            if (view instanceof com.google.android.gms.ads.formats.zzd) {
                zzd.zza(this.zza);
            }
        }
    }
    
    static final class zzd extends AdListener implements AppEventListener, com.google.android.gms.ads.internal.client.zza
    {
        private final AbstractAdViewAdapter zza;
        private final MediationBannerListener zzb;
        
        public zzd(final AbstractAdViewAdapter zza, final MediationBannerListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        @Override
        public final void onAdClicked() {
            this.zzb.onAdClicked(this.zza);
        }
        
        @Override
        public final void onAdClosed() {
            this.zzb.onAdClosed(this.zza);
        }
        
        @Override
        public final void onAdFailedToLoad(final int n) {
            this.zzb.onAdFailedToLoad(this.zza, n);
        }
        
        @Override
        public final void onAdLeftApplication() {
            this.zzb.onAdLeftApplication(this.zza);
        }
        
        @Override
        public final void onAdLoaded() {
            this.zzb.onAdLoaded(this.zza);
        }
        
        @Override
        public final void onAdOpened() {
            this.zzb.onAdOpened(this.zza);
        }
        
        @Override
        public final void onAppEvent(final String s, final String s2) {
            this.zzb.zza(this.zza, s, s2);
        }
    }
    
    static final class zze extends AdListener implements com.google.android.gms.ads.internal.client.zza
    {
        private final AbstractAdViewAdapter zza;
        private final MediationInterstitialListener zzb;
        
        public zze(final AbstractAdViewAdapter zza, final MediationInterstitialListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        @Override
        public final void onAdClicked() {
            this.zzb.onAdClicked(this.zza);
        }
        
        @Override
        public final void onAdClosed() {
            this.zzb.onAdClosed(this.zza);
        }
        
        @Override
        public final void onAdFailedToLoad(final int n) {
            this.zzb.onAdFailedToLoad(this.zza, n);
        }
        
        @Override
        public final void onAdLeftApplication() {
            this.zzb.onAdLeftApplication(this.zza);
        }
        
        @Override
        public final void onAdLoaded() {
            this.zzb.onAdLoaded(this.zza);
        }
        
        @Override
        public final void onAdOpened() {
            this.zzb.onAdOpened(this.zza);
        }
    }
    
    static final class zzf extends AdListener implements OnAppInstallAdLoadedListener, OnContentAdLoadedListener, OnCustomClickListener, OnCustomTemplateAdLoadedListener, UnifiedNativeAd.zza
    {
        private final AbstractAdViewAdapter zza;
        private final MediationNativeListener zzb;
        
        public zzf(final AbstractAdViewAdapter zza, final MediationNativeListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        @Override
        public final void onAdClicked() {
            this.zzb.onAdClicked(this.zza);
        }
        
        @Override
        public final void onAdClosed() {
            this.zzb.onAdClosed(this.zza);
        }
        
        @Override
        public final void onAdFailedToLoad(final int n) {
            this.zzb.onAdFailedToLoad(this.zza, n);
        }
        
        @Override
        public final void onAdImpression() {
            this.zzb.onAdImpression(this.zza);
        }
        
        @Override
        public final void onAdLeftApplication() {
            this.zzb.onAdLeftApplication(this.zza);
        }
        
        @Override
        public final void onAdLoaded() {
        }
        
        @Override
        public final void onAdOpened() {
            this.zzb.onAdOpened(this.zza);
        }
        
        @Override
        public final void onAppInstallAdLoaded(final NativeAppInstallAd nativeAppInstallAd) {
            this.zzb.onAdLoaded(this.zza, new AbstractAdViewAdapter.zza(nativeAppInstallAd));
        }
        
        @Override
        public final void onContentAdLoaded(final NativeContentAd nativeContentAd) {
            this.zzb.onAdLoaded(this.zza, new zzb(nativeContentAd));
        }
        
        @Override
        public final void onCustomClick(final NativeCustomTemplateAd nativeCustomTemplateAd, final String s) {
            this.zzb.zza(this.zza, nativeCustomTemplateAd, s);
        }
        
        @Override
        public final void onCustomTemplateAdLoaded(final NativeCustomTemplateAd nativeCustomTemplateAd) {
            this.zzb.zza(this.zza, nativeCustomTemplateAd);
        }
        
        @Override
        public final void zza(final UnifiedNativeAd unifiedNativeAd) {
            this.zzb.zza(this.zza, new zzc(unifiedNativeAd));
        }
    }
}
