package com.google.ads.mediation;

import com.google.ads.AdRequest;

@Deprecated
public interface MediationInterstitialListener
{
    void onFailedToReceiveAd(final MediationInterstitialAdapter<?, ?> p0, final AdRequest.ErrorCode p1);
}
