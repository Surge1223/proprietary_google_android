package com.google.ads.mediation;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import java.util.Iterator;
import com.google.android.gms.internal.zzaid;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class MediationServerParameters
{
    public void load(Map<String, String> iterator) throws MappingException {
        final HashMap<Object, Field> hashMap = new HashMap<Object, Field>();
        for (final Field field : this.getClass().getFields()) {
            final Parameter parameter = field.getAnnotation(Parameter.class);
            if (parameter != null) {
                hashMap.put(parameter.name(), field);
            }
        }
        if (hashMap.isEmpty()) {
            zzaid.zze("No server options fields detected. To suppress this message either add a field with the @Parameter annotation, or override the load() method.");
        }
        iterator = ((Map<Object, Object>)iterator).entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<Object, Object> entry = iterator.next();
            final Field field2 = hashMap.remove(entry.getKey());
            if (field2 != null) {
                try {
                    field2.set(this, entry.getValue());
                }
                catch (IllegalArgumentException ex) {
                    final String s = entry.getKey();
                    final StringBuilder sb = new StringBuilder(43 + String.valueOf(s).length());
                    sb.append("Server option \"");
                    sb.append(s);
                    sb.append("\" could not be set: Bad Type");
                    zzaid.zze(sb.toString());
                }
                catch (IllegalAccessException ex2) {
                    final String s2 = entry.getKey();
                    final StringBuilder sb2 = new StringBuilder(49 + String.valueOf(s2).length());
                    sb2.append("Server option \"");
                    sb2.append(s2);
                    sb2.append("\" could not be set: Illegal Access");
                    zzaid.zze(sb2.toString());
                }
            }
            else {
                final String s3 = entry.getKey();
                final String s4 = entry.getValue();
                final StringBuilder sb3 = new StringBuilder(31 + String.valueOf(s3).length() + String.valueOf(s4).length());
                sb3.append("Unexpected server option: ");
                sb3.append(s3);
                sb3.append(" = \"");
                sb3.append(s4);
                sb3.append("\"");
                zzaid.zzb(sb3.toString());
            }
        }
        final StringBuilder sb4 = new StringBuilder();
        for (final Field field3 : hashMap.values()) {
            if (field3.getAnnotation(Parameter.class).required()) {
                final String value = String.valueOf(field3.getAnnotation(Parameter.class).name());
                String concat;
                if (value.length() != 0) {
                    concat = "Required server option missing: ".concat(value);
                }
                else {
                    concat = new String("Required server option missing: ");
                }
                zzaid.zze(concat);
                if (sb4.length() > 0) {
                    sb4.append(", ");
                }
                sb4.append(field3.getAnnotation(Parameter.class).name());
            }
        }
        if (sb4.length() > 0) {
            final String value2 = String.valueOf(sb4.toString());
            String concat2;
            if (value2.length() != 0) {
                concat2 = "Required server option(s) missing: ".concat(value2);
            }
            else {
                concat2 = new String("Required server option(s) missing: ");
            }
            throw new MappingException(concat2);
        }
    }
    
    public static final class MappingException extends Exception
    {
        public MappingException(final String s) {
            super(s);
        }
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD })
    public @interface Parameter {
        String name();
        
        boolean required() default true;
    }
}
