package com.google.ads.mediation.customevent;

import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.AdRequest;
import com.google.ads.mediation.NetworkExtras;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationServerParameters;
import android.app.Activity;
import com.google.ads.mediation.MediationBannerListener;
import com.google.android.gms.internal.zzaid;
import android.view.View;
import com.google.android.gms.common.annotation.KeepName;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.ads.mediation.MediationBannerAdapter;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter<CustomEventExtras, CustomEventServerParameters>, MediationInterstitialAdapter<CustomEventExtras, CustomEventServerParameters>
{
    private View zza;
    private CustomEventBanner zzb;
    private CustomEventInterstitial zzc;
    
    private static <T> T zza(final String s) {
        try {
            return (T)Class.forName(s).newInstance();
        }
        catch (Throwable t) {
            final String message = t.getMessage();
            final StringBuilder sb = new StringBuilder(46 + String.valueOf(s).length() + String.valueOf(message).length());
            sb.append("Could not instantiate custom event adapter: ");
            sb.append(s);
            sb.append(". ");
            sb.append(message);
            zzaid.zze(sb.toString());
            return null;
        }
    }
    
    @Override
    public final void destroy() {
        if (this.zzb != null) {
            this.zzb.destroy();
        }
        if (this.zzc != null) {
            this.zzc.destroy();
        }
    }
    
    @Override
    public final Class<CustomEventExtras> getAdditionalParametersType() {
        return CustomEventExtras.class;
    }
    
    @Override
    public final View getBannerView() {
        return this.zza;
    }
    
    @Override
    public final Class<CustomEventServerParameters> getServerParametersType() {
        return CustomEventServerParameters.class;
    }
    
    @Override
    public final void requestBannerAd(final MediationBannerListener mediationBannerListener, final Activity activity, final CustomEventServerParameters customEventServerParameters, final AdSize adSize, final MediationAdRequest mediationAdRequest, final CustomEventExtras customEventExtras) {
        this.zzb = zza(customEventServerParameters.className);
        if (this.zzb == null) {
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        Object extra;
        if (customEventExtras == null) {
            extra = null;
        }
        else {
            extra = customEventExtras.getExtra(customEventServerParameters.label);
        }
        this.zzb.requestBannerAd(new zza(this, mediationBannerListener), activity, customEventServerParameters.label, customEventServerParameters.parameter, adSize, mediationAdRequest, extra);
    }
    
    @Override
    public final void requestInterstitialAd(final MediationInterstitialListener mediationInterstitialListener, final Activity activity, final CustomEventServerParameters customEventServerParameters, final MediationAdRequest mediationAdRequest, final CustomEventExtras customEventExtras) {
        this.zzc = zza(customEventServerParameters.className);
        if (this.zzc == null) {
            mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        Object extra;
        if (customEventExtras == null) {
            extra = null;
        }
        else {
            extra = customEventExtras.getExtra(customEventServerParameters.label);
        }
        this.zzc.requestInterstitialAd(new zzb(this, mediationInterstitialListener), activity, customEventServerParameters.label, customEventServerParameters.parameter, mediationAdRequest, extra);
    }
    
    @Override
    public final void showInterstitial() {
        this.zzc.showInterstitial();
    }
    
    static final class zza implements CustomEventBannerListener
    {
        private final CustomEventAdapter zza;
        private final MediationBannerListener zzb;
        
        public zza(final CustomEventAdapter zza, final MediationBannerListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
    }
    
    final class zzb implements CustomEventInterstitialListener
    {
        private final CustomEventAdapter zza;
        private final MediationInterstitialListener zzb;
        
        public zzb(final CustomEventAdapter zza, final MediationInterstitialListener zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
    }
}
