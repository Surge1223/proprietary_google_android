package com.google.ads.mediation;

import com.google.ads.AdRequest;

@Deprecated
public interface MediationBannerListener
{
    void onFailedToReceiveAd(final MediationBannerAdapter<?, ?> p0, final AdRequest.ErrorCode p1);
}
