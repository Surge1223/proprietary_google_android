package com.google.ads.mediation;

import android.location.Location;
import java.util.Set;
import com.google.ads.AdRequest;
import java.util.Date;

@Deprecated
public class MediationAdRequest
{
    private final Date zza;
    private final AdRequest.Gender zzb;
    private final Set<String> zzc;
    private final boolean zzd;
    private final Location zze;
    
    public MediationAdRequest(final Date zza, final AdRequest.Gender zzb, final Set<String> zzc, final boolean zzd, final Location zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
}
