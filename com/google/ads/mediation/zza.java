package com.google.ads.mediation;

import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.formats.zzd;
import java.util.Map;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.formats.NativeAdViewHolder;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdView;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.internal.client.zzbh;
import android.view.View;
import android.location.Location;
import java.util.Iterator;
import java.util.Set;
import java.util.Date;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.AdRequest;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import android.content.Context;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.internal.zzanu;
import com.google.android.gms.ads.mediation.zzb;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

final class zza implements RewardedVideoAdListener
{
    private final /* synthetic */ AbstractAdViewAdapter zza;
    
    zza(final AbstractAdViewAdapter zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onRewarded(final RewardItem rewardItem) {
        this.zza.zzf.onRewarded(this.zza, rewardItem);
    }
    
    @Override
    public final void onRewardedVideoAdClosed() {
        this.zza.zzf.onAdClosed(this.zza);
        AbstractAdViewAdapter.zza(this.zza, null);
    }
    
    @Override
    public final void onRewardedVideoAdFailedToLoad(final int n) {
        this.zza.zzf.onAdFailedToLoad(this.zza, n);
    }
    
    @Override
    public final void onRewardedVideoAdLeftApplication() {
        this.zza.zzf.onAdLeftApplication(this.zza);
    }
    
    @Override
    public final void onRewardedVideoAdLoaded() {
        this.zza.zzf.onAdLoaded(this.zza);
    }
    
    @Override
    public final void onRewardedVideoAdOpened() {
        this.zza.zzf.onAdOpened(this.zza);
    }
    
    @Override
    public final void onRewardedVideoCompleted() {
        this.zza.zzf.onVideoCompleted(this.zza);
    }
    
    @Override
    public final void onRewardedVideoStarted() {
        this.zza.zzf.onVideoStarted(this.zza);
    }
}
