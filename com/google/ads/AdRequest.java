package com.google.ads;

@Deprecated
public final class AdRequest
{
    public enum ErrorCode
    {
        INTERNAL_ERROR("There was an internal error."), 
        INVALID_REQUEST("Invalid Ad request."), 
        NETWORK_ERROR("A network error occurred."), 
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory.");
        
        private final String zza;
        
        private ErrorCode(final String zza) {
            this.zza = zza;
        }
        
        @Override
        public final String toString() {
            return this.zza;
        }
    }
    
    public enum Gender
    {
        FEMALE, 
        MALE, 
        UNKNOWN;
    }
}
