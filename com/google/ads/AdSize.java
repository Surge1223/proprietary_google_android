package com.google.ads;

@Deprecated
public final class AdSize
{
    public static final AdSize BANNER;
    public static final AdSize IAB_BANNER;
    public static final AdSize IAB_LEADERBOARD;
    public static final AdSize IAB_MRECT;
    public static final AdSize IAB_WIDE_SKYSCRAPER;
    public static final AdSize SMART_BANNER;
    private final com.google.android.gms.ads.AdSize zza;
    
    static {
        SMART_BANNER = new AdSize(-1, -2, "mb");
        BANNER = new AdSize(320, 50, "mb");
        IAB_MRECT = new AdSize(300, 250, "as");
        IAB_BANNER = new AdSize(468, 60, "as");
        IAB_LEADERBOARD = new AdSize(728, 90, "as");
        IAB_WIDE_SKYSCRAPER = new AdSize(160, 600, "as");
    }
    
    private AdSize(final int n, final int n2, final String s) {
        this(new com.google.android.gms.ads.AdSize(n, n2));
    }
    
    public AdSize(final com.google.android.gms.ads.AdSize zza) {
        this.zza = zza;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof AdSize && this.zza.equals(((AdSize)o).zza);
    }
    
    public final int getHeight() {
        return this.zza.getHeight();
    }
    
    public final int getWidth() {
        return this.zza.getWidth();
    }
    
    @Override
    public final int hashCode() {
        return this.zza.hashCode();
    }
    
    @Override
    public final String toString() {
        return this.zza.toString();
    }
}
