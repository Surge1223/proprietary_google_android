package com.google.common.hash;

public final class Hashing
{
    private static final int GOOD_FAST_HASH_SEED;
    
    static {
        GOOD_FAST_HASH_SEED = (int)System.currentTimeMillis();
    }
    
    static final class ConcatenatedHashFunction extends AbstractCompositeHashFunction
    {
        private final int bits;
        
        @Override
        public boolean equals(final Object o) {
            if (!(o instanceof ConcatenatedHashFunction)) {
                return false;
            }
            final ConcatenatedHashFunction concatenatedHashFunction = (ConcatenatedHashFunction)o;
            if (this.bits == concatenatedHashFunction.bits && this.functions.length == concatenatedHashFunction.functions.length) {
                for (int i = 0; i < this.functions.length; ++i) {
                    if (!this.functions[i].equals(concatenatedHashFunction.functions[i])) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            int bits = this.bits;
            final HashFunction[] functions = this.functions;
            for (int length = functions.length, i = 0; i < length; ++i) {
                bits ^= functions[i].hashCode();
            }
            return bits;
        }
    }
}
