package com.google.common.io;

import java.lang.reflect.Method;
import java.util.logging.Logger;
import java.util.logging.Level;
import com.google.common.base.Throwables;
import java.io.IOException;
import com.google.common.base.Preconditions;
import java.util.ArrayDeque;
import java.util.Deque;
import java.io.Closeable;

public final class Closer implements Closeable
{
    private static final Suppressor SUPPRESSOR;
    private final Deque<Closeable> stack;
    final Suppressor suppressor;
    private Throwable thrown;
    
    static {
        Suppressor suppressor;
        if (SuppressingSuppressor.isAvailable()) {
            suppressor = SuppressingSuppressor.INSTANCE;
        }
        else {
            suppressor = LoggingSuppressor.INSTANCE;
        }
        SUPPRESSOR = suppressor;
    }
    
    Closer(final Suppressor suppressor) {
        this.stack = new ArrayDeque<Closeable>(4);
        this.suppressor = Preconditions.checkNotNull(suppressor);
    }
    
    @Override
    public void close() throws IOException {
        Throwable thrown = this.thrown;
        while (!this.stack.isEmpty()) {
            final Closeable closeable = this.stack.removeFirst();
            try {
                closeable.close();
            }
            catch (Throwable t) {
                if (thrown == null) {
                    thrown = t;
                }
                else {
                    this.suppressor.suppress(closeable, thrown, t);
                }
            }
        }
        if (this.thrown == null && thrown != null) {
            Throwables.propagateIfPossible(thrown, IOException.class);
            throw new AssertionError((Object)thrown);
        }
    }
    
    static final class LoggingSuppressor implements Suppressor
    {
        static final LoggingSuppressor INSTANCE;
        
        static {
            INSTANCE = new LoggingSuppressor();
        }
        
        @Override
        public void suppress(final Closeable closeable, final Throwable t, final Throwable t2) {
            final Logger logger = Closeables.logger;
            final Level warning = Level.WARNING;
            final StringBuilder sb = new StringBuilder();
            sb.append("Suppressing exception thrown when closing ");
            sb.append(closeable);
            logger.log(warning, sb.toString(), t2);
        }
    }
    
    static final class SuppressingSuppressor implements Suppressor
    {
        static final SuppressingSuppressor INSTANCE;
        static final Method addSuppressed;
        
        static {
            INSTANCE = new SuppressingSuppressor();
            addSuppressed = getAddSuppressed();
        }
        
        private static Method getAddSuppressed() {
            try {
                return Throwable.class.getMethod("addSuppressed", Throwable.class);
            }
            catch (Throwable t) {
                return null;
            }
        }
        
        static boolean isAvailable() {
            return SuppressingSuppressor.addSuppressed != null;
        }
        
        @Override
        public void suppress(final Closeable closeable, final Throwable t, final Throwable t2) {
            if (t == t2) {
                return;
            }
            try {
                SuppressingSuppressor.addSuppressed.invoke(t, t2);
            }
            catch (Throwable t3) {
                LoggingSuppressor.INSTANCE.suppress(closeable, t, t2);
            }
        }
    }
    
    interface Suppressor
    {
        void suppress(final Closeable p0, final Throwable p1, final Throwable p2);
    }
}
