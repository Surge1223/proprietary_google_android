package com.google.common.base;

public final class MoreObjects
{
    public static <T> T firstNonNull(T checkNotNull, final T t) {
        if (checkNotNull == null) {
            checkNotNull = Preconditions.checkNotNull(t);
        }
        return checkNotNull;
    }
    
    static String simpleName(final Class<?> clazz) {
        final String replaceAll = clazz.getName().replaceAll("\\$[0-9]+", "\\$");
        int n;
        if ((n = replaceAll.lastIndexOf(36)) == -1) {
            n = replaceAll.lastIndexOf(46);
        }
        return replaceAll.substring(n + 1);
    }
    
    public static ToStringHelper toStringHelper(final Object o) {
        return new ToStringHelper(simpleName(o.getClass()));
    }
    
    public static final class ToStringHelper
    {
        private final String className;
        private ValueHolder holderHead;
        private ValueHolder holderTail;
        private boolean omitNullValues;
        
        private ToStringHelper(final String s) {
            this.holderHead = new ValueHolder();
            this.holderTail = this.holderHead;
            this.omitNullValues = false;
            this.className = Preconditions.checkNotNull(s);
        }
        
        private ValueHolder addHolder() {
            final ValueHolder valueHolder = new ValueHolder();
            this.holderTail.next = valueHolder;
            return this.holderTail = valueHolder;
        }
        
        private ToStringHelper addHolder(final Object value) {
            this.addHolder().value = value;
            return this;
        }
        
        private ToStringHelper addHolder(final String s, final Object value) {
            final ValueHolder addHolder = this.addHolder();
            addHolder.value = value;
            addHolder.name = Preconditions.checkNotNull(s);
            return this;
        }
        
        public ToStringHelper add(final String s, final int n) {
            return this.addHolder(s, String.valueOf(n));
        }
        
        public ToStringHelper add(final String s, final Object o) {
            return this.addHolder(s, o);
        }
        
        public ToStringHelper addValue(final Object o) {
            return this.addHolder(o);
        }
        
        @Override
        public String toString() {
            final boolean omitNullValues = this.omitNullValues;
            String s = "";
            final StringBuilder sb = new StringBuilder(32);
            sb.append(this.className);
            final StringBuilder append = sb.append('{');
            String s2;
            for (ValueHolder valueHolder = this.holderHead.next; valueHolder != null; valueHolder = valueHolder.next, s = s2) {
                if (omitNullValues) {
                    s2 = s;
                    if (valueHolder.value == null) {
                        continue;
                    }
                }
                append.append(s);
                s2 = ", ";
                if (valueHolder.name != null) {
                    append.append(valueHolder.name);
                    append.append('=');
                }
                append.append(valueHolder.value);
            }
            append.append('}');
            return append.toString();
        }
        
        private static final class ValueHolder
        {
            String name;
            ValueHolder next;
            Object value;
        }
    }
}
