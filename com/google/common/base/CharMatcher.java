package com.google.common.base;

import java.util.Arrays;

public abstract class CharMatcher implements Predicate<Character>
{
    public static final CharMatcher ANY;
    public static final CharMatcher ASCII;
    public static final CharMatcher BREAKING_WHITESPACE;
    public static final CharMatcher DIGIT;
    public static final CharMatcher INVISIBLE;
    public static final CharMatcher JAVA_DIGIT;
    public static final CharMatcher JAVA_ISO_CONTROL;
    public static final CharMatcher JAVA_LETTER;
    public static final CharMatcher JAVA_LETTER_OR_DIGIT;
    public static final CharMatcher JAVA_LOWER_CASE;
    public static final CharMatcher JAVA_UPPER_CASE;
    private static final String NINES;
    public static final CharMatcher NONE;
    public static final CharMatcher SINGLE_WIDTH;
    public static final CharMatcher WHITESPACE;
    static final int WHITESPACE_SHIFT;
    final String description;
    
    static {
        BREAKING_WHITESPACE = new CharMatcher() {
            @Override
            public boolean matches(final char c) {
                boolean b = true;
                Label_0127: {
                    if (c != ' ' && c != '\u0085' && c != '\u1680') {
                        if (c == '\u2007') {
                            return false;
                        }
                        if (c != '\u205f' && c != '\u3000') {
                            switch (c) {
                                default: {
                                    switch (c) {
                                        default: {
                                            if (c < '\u2000' || c > '\u200a') {
                                                b = false;
                                            }
                                            return b;
                                        }
                                        case '\u2028':
                                        case '\u2029': {
                                            break Label_0127;
                                        }
                                    }
                                    break;
                                }
                                case '\t':
                                case '\n':
                                case '\u000b':
                                case '\f':
                                case '\r': {
                                    break;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            
            @Override
            public String toString() {
                return "CharMatcher.BREAKING_WHITESPACE";
            }
        };
        ASCII = inRange('\0', '\u007f', "CharMatcher.ASCII");
        final StringBuilder sb = new StringBuilder("0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10".length());
        for (int i = 0; i < "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10".length(); ++i) {
            sb.append((char)("0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10".charAt(i) + '\t'));
        }
        NINES = sb.toString();
        DIGIT = new RangesMatcher("CharMatcher.DIGIT", "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10".toCharArray(), CharMatcher.NINES.toCharArray());
        JAVA_DIGIT = new CharMatcher() {
            @Override
            public boolean matches(final char c) {
                return Character.isDigit(c);
            }
        };
        JAVA_LETTER = new CharMatcher() {
            @Override
            public boolean matches(final char c) {
                return Character.isLetter(c);
            }
        };
        JAVA_LETTER_OR_DIGIT = new CharMatcher() {
            @Override
            public boolean matches(final char c) {
                return Character.isLetterOrDigit(c);
            }
        };
        JAVA_UPPER_CASE = new CharMatcher() {
            @Override
            public boolean matches(final char c) {
                return Character.isUpperCase(c);
            }
        };
        JAVA_LOWER_CASE = new CharMatcher() {
            @Override
            public boolean matches(final char c) {
                return Character.isLowerCase(c);
            }
        };
        JAVA_ISO_CONTROL = inRange('\0', '\u001f').or(inRange('\u007f', '\u009f')).withToString("CharMatcher.JAVA_ISO_CONTROL");
        INVISIBLE = new RangesMatcher("CharMatcher.INVISIBLE", "\u0000\u007f\u00ad\u0600\u061c\u06dd\u070f\u1680\u180e\u2000\u2028\u205f\u2066\u2067\u2068\u2069\u206a\u3000\ud800\ufeff\ufff9\ufffa".toCharArray(), "  \u00ad\u0604\u061c\u06dd\u070f\u1680\u180e\u200f\u202f\u2064\u2066\u2067\u2068\u2069\u206f\u3000\uf8ff\ufeff\ufff9\ufffb".toCharArray());
        SINGLE_WIDTH = new RangesMatcher("CharMatcher.SINGLE_WIDTH", "\u0000\u05be\u05d0\u05f3\u0600\u0750\u0e00\u1e00\u2100\ufb50\ufe70\uff61".toCharArray(), "\u04f9\u05be\u05ea\u05f4\u06ff\u077f\u0e7f\u20af\u213a\ufdff\ufeff\uffdc".toCharArray());
        ANY = new FastMatcher() {
            @Override
            public boolean matches(final char c) {
                return true;
            }
            
            @Override
            public CharMatcher or(final CharMatcher charMatcher) {
                Preconditions.checkNotNull(charMatcher);
                return this;
            }
        };
        NONE = new FastMatcher() {
            @Override
            public boolean matches(final char c) {
                return false;
            }
            
            @Override
            public CharMatcher or(final CharMatcher charMatcher) {
                return Preconditions.checkNotNull(charMatcher);
            }
        };
        WHITESPACE_SHIFT = Integer.numberOfLeadingZeros("\u2002\u3000\r\u0085\u200a\u2005\u2000\u3000\u2029\u000b\u3000\u2008\u2003\u205f\u3000\u1680\t \u2006\u2001\u202f \f\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000".length() - 1);
        WHITESPACE = new FastMatcher() {
            @Override
            public boolean matches(final char c) {
                return "\u2002\u3000\r\u0085\u200a\u2005\u2000\u3000\u2029\u000b\u3000\u2008\u2003\u205f\u3000\u1680\t \u2006\u2001\u202f \f\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000".charAt(1682554634 * c >>> CharMatcher$15.WHITESPACE_SHIFT) == c;
            }
        };
    }
    
    protected CharMatcher() {
        this.description = super.toString();
    }
    
    CharMatcher(final String description) {
        this.description = description;
    }
    
    public static CharMatcher inRange(final char c, final char c2) {
        Preconditions.checkArgument(c2 >= c);
        final StringBuilder sb = new StringBuilder();
        sb.append("CharMatcher.inRange('");
        sb.append(showCharacter(c));
        sb.append("', '");
        sb.append(showCharacter(c2));
        sb.append("')");
        return inRange(c, c2, sb.toString());
    }
    
    static CharMatcher inRange(final char c, final char c2, final String s) {
        return new FastMatcher(s) {
            @Override
            public boolean matches(final char c) {
                return c <= c && c <= c2;
            }
        };
    }
    
    private static String showCharacter(final char c) {
        final char[] array2;
        final char[] array = array2 = new char[6];
        array2[0] = '\\';
        array2[1] = 'u';
        array2[3] = (array2[2] = '\0');
        array2[5] = (array2[4] = '\0');
        final int n = 0;
        char c2 = c;
        for (int i = n; i < 4; ++i) {
            array[5 - i] = "0123456789ABCDEF".charAt(c2 & '\u000f');
            c2 >>= 4;
        }
        return String.copyValueOf(array);
    }
    
    @Deprecated
    @Override
    public boolean apply(final Character c) {
        return this.matches(c);
    }
    
    public abstract boolean matches(final char p0);
    
    public CharMatcher or(final CharMatcher charMatcher) {
        return new Or(this, Preconditions.checkNotNull(charMatcher));
    }
    
    @Override
    public String toString() {
        return this.description;
    }
    
    CharMatcher withToString(final String s) {
        throw new UnsupportedOperationException();
    }
    
    abstract static class FastMatcher extends CharMatcher
    {
        FastMatcher(final String s) {
            super(s);
        }
    }
    
    private static class Or extends CharMatcher
    {
        final CharMatcher first;
        final CharMatcher second;
        
        Or(final CharMatcher charMatcher, final CharMatcher charMatcher2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.or(");
            sb.append(charMatcher);
            sb.append(", ");
            sb.append(charMatcher2);
            sb.append(")");
            this(charMatcher, charMatcher2, sb.toString());
        }
        
        Or(final CharMatcher charMatcher, final CharMatcher charMatcher2, final String s) {
            super(s);
            this.first = Preconditions.checkNotNull(charMatcher);
            this.second = Preconditions.checkNotNull(charMatcher2);
        }
        
        @Override
        public boolean matches(final char c) {
            return this.first.matches(c) || this.second.matches(c);
        }
        
        @Override
        CharMatcher withToString(final String s) {
            return new Or(this.first, this.second, s);
        }
    }
    
    private static class RangesMatcher extends CharMatcher
    {
        private final char[] rangeEnds;
        private final char[] rangeStarts;
        
        RangesMatcher(final String s, final char[] rangeStarts, final char[] rangeEnds) {
            super(s);
            this.rangeStarts = rangeStarts;
            this.rangeEnds = rangeEnds;
            Preconditions.checkArgument(rangeStarts.length == rangeEnds.length);
            for (int i = 0; i < rangeStarts.length; ++i) {
                Preconditions.checkArgument(rangeStarts[i] <= rangeEnds[i]);
                if (i + 1 < rangeStarts.length) {
                    Preconditions.checkArgument(rangeEnds[i] < rangeStarts[i + 1]);
                }
            }
        }
        
        @Override
        public boolean matches(final char c) {
            final int binarySearch = Arrays.binarySearch(this.rangeStarts, c);
            boolean b = true;
            if (binarySearch >= 0) {
                return true;
            }
            final int n = binarySearch - 1;
            if (n < 0 || c > this.rangeEnds[n]) {
                b = false;
            }
            return b;
        }
    }
}
