package com.google.common.base;

public interface Predicate<T>
{
    boolean apply(final T p0);
}
