package com.google.common.base;

import java.io.Serializable;
import java.util.Collection;

public final class Predicates
{
    private static final Joiner COMMA_JOINER;
    
    static {
        COMMA_JOINER = Joiner.on(',');
    }
    
    public static <T> Predicate<T> equalTo(final T t) {
        Predicate<T> null;
        if (t == null) {
            null = isNull();
        }
        else {
            null = new IsEqualToPredicate<T>((Object)t);
        }
        return null;
    }
    
    public static <T> Predicate<T> in(final Collection<? extends T> collection) {
        return new InPredicate<T>((Collection)collection);
    }
    
    public static <T> Predicate<T> isNull() {
        return ObjectPredicate.IS_NULL.withNarrowedType();
    }
    
    private static class InPredicate<T> implements Predicate<T>, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Collection<?> target;
        
        private InPredicate(final Collection<?> collection) {
            this.target = Preconditions.checkNotNull(collection);
        }
        
        @Override
        public boolean apply(final T t) {
            try {
                return this.target.contains(t);
            }
            catch (ClassCastException ex) {
                return false;
            }
            catch (NullPointerException ex2) {
                return false;
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof InPredicate && this.target.equals(((InPredicate)o).target);
        }
        
        @Override
        public int hashCode() {
            return this.target.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.in(");
            sb.append(this.target);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static class IsEqualToPredicate<T> implements Predicate<T>, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final T target;
        
        private IsEqualToPredicate(final T target) {
            this.target = target;
        }
        
        @Override
        public boolean apply(final T t) {
            return this.target.equals(t);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof IsEqualToPredicate && this.target.equals(((IsEqualToPredicate)o).target);
        }
        
        @Override
        public int hashCode() {
            return this.target.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.equalTo(");
            sb.append(this.target);
            sb.append(")");
            return sb.toString();
        }
    }
    
    enum ObjectPredicate implements Predicate<Object>
    {
        ALWAYS_FALSE(1) {
            @Override
            public boolean apply(final Object o) {
                return false;
            }
            
            @Override
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        }, 
        ALWAYS_TRUE(0) {
            @Override
            public boolean apply(final Object o) {
                return true;
            }
            
            @Override
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        }, 
        IS_NULL(2) {
            @Override
            public boolean apply(final Object o) {
                return o == null;
            }
            
            @Override
            public String toString() {
                return "Predicates.isNull()";
            }
        }, 
        NOT_NULL(3) {
            @Override
            public boolean apply(final Object o) {
                return o != null;
            }
            
            @Override
            public String toString() {
                return "Predicates.notNull()";
            }
        };
        
         <T> Predicate<T> withNarrowedType() {
            return (Predicate<T>)this;
        }
    }
}
