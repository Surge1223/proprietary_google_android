package com.google.common.base;

import java.io.Serializable;

public final class Suppliers
{
    static class ExpiringMemoizingSupplier<T> implements Supplier<T>, Serializable
    {
        private static final long serialVersionUID = 0L;
        final Supplier<T> delegate;
        final long durationNanos;
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Suppliers.memoizeWithExpiration(");
            sb.append(this.delegate);
            sb.append(", ");
            sb.append(this.durationNanos);
            sb.append(", NANOS)");
            return sb.toString();
        }
    }
    
    static class MemoizingSupplier<T> implements Supplier<T>, Serializable
    {
        private static final long serialVersionUID = 0L;
        final Supplier<T> delegate;
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Suppliers.memoize(");
            sb.append(this.delegate);
            sb.append(")");
            return sb.toString();
        }
    }
}
