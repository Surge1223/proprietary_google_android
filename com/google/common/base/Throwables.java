package com.google.common.base;

public final class Throwables
{
    public static <X extends Throwable> void propagateIfInstanceOf(final Throwable t, final Class<X> clazz) throws X, Throwable {
        if (t != null && clazz.isInstance(t)) {
            throw clazz.cast(t);
        }
    }
    
    public static void propagateIfPossible(final Throwable t) {
        propagateIfInstanceOf(t, Error.class);
        propagateIfInstanceOf(t, RuntimeException.class);
    }
    
    public static <X extends Throwable> void propagateIfPossible(final Throwable t, final Class<X> clazz) throws X, Throwable {
        propagateIfInstanceOf(t, (Class<Throwable>)clazz);
        propagateIfPossible(t);
    }
}
