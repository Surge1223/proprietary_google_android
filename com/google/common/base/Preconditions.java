package com.google.common.base;

public final class Preconditions
{
    private static String badElementIndex(final int n, final int n2, final String s) {
        if (n < 0) {
            return format("%s (%s) must not be negative", s, n);
        }
        if (n2 >= 0) {
            return format("%s (%s) must be less than size (%s)", s, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("negative size: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static String badPositionIndex(final int n, final int n2, final String s) {
        if (n < 0) {
            return format("%s (%s) must not be negative", s, n);
        }
        if (n2 >= 0) {
            return format("%s (%s) must not be greater than size (%s)", s, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("negative size: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static String badPositionIndexes(final int n, final int n2, final int n3) {
        if (n < 0 || n > n3) {
            return badPositionIndex(n, n3, "start index");
        }
        if (n2 >= 0 && n2 <= n3) {
            return format("end index (%s) must not be less than start index (%s)", n2, n);
        }
        return badPositionIndex(n2, n3, "end index");
    }
    
    public static void checkArgument(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static void checkArgument(final boolean b, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.valueOf(o));
    }
    
    public static void checkArgument(final boolean b, final String s, final Object... array) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(format(s, array));
    }
    
    public static int checkElementIndex(final int n, final int n2) {
        return checkElementIndex(n, n2, "index");
    }
    
    public static int checkElementIndex(final int n, final int n2, final String s) {
        if (n >= 0 && n < n2) {
            return n;
        }
        throw new IndexOutOfBoundsException(badElementIndex(n, n2, s));
    }
    
    public static <T> T checkNotNull(final T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }
    
    public static <T> T checkNotNull(final T t, final Object o) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(o));
    }
    
    public static <T> T checkNotNull(final T t, final String s, final Object... array) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(format(s, array));
    }
    
    public static int checkPositionIndex(final int n, final int n2) {
        return checkPositionIndex(n, n2, "index");
    }
    
    public static int checkPositionIndex(final int n, final int n2, final String s) {
        if (n >= 0 && n <= n2) {
            return n;
        }
        throw new IndexOutOfBoundsException(badPositionIndex(n, n2, s));
    }
    
    public static void checkPositionIndexes(final int n, final int n2, final int n3) {
        if (n >= 0 && n2 >= n && n2 <= n3) {
            return;
        }
        throw new IndexOutOfBoundsException(badPositionIndexes(n, n2, n3));
    }
    
    public static void checkState(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public static void checkState(final boolean b, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.valueOf(o));
    }
    
    public static void checkState(final boolean b, final String s, final Object... array) {
        if (b) {
            return;
        }
        throw new IllegalStateException(format(s, array));
    }
    
    static String format(String value, final Object... array) {
        value = String.valueOf(value);
        final StringBuilder sb = new StringBuilder(value.length() + 16 * array.length);
        int n = 0;
        int i;
        for (i = 0; i < array.length; ++i) {
            final int index = value.indexOf("%s", n);
            if (index == -1) {
                break;
            }
            sb.append(value.substring(n, index));
            sb.append(array[i]);
            n = index + 2;
        }
        sb.append(value.substring(n));
        if (i < array.length) {
            sb.append(" [");
            final int n2 = i + 1;
            sb.append(array[i]);
            int n3;
            for (int j = n2; j < array.length; j = n3) {
                sb.append(", ");
                n3 = j + 1;
                sb.append(array[j]);
            }
            sb.append(']');
        }
        return sb.toString();
    }
}
