package com.google.common.base;

public interface Function<F, T>
{
    T apply(final F p0);
    
    boolean equals(final Object p0);
}
