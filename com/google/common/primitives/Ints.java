package com.google.common.primitives;

import java.util.Arrays;

public final class Ints
{
    private static final byte[] asciiDigits;
    
    static {
        Arrays.fill(asciiDigits = new byte[128], (byte)(-1));
        final int n = 0;
        for (int i = 0; i <= 9; ++i) {
            Ints.asciiDigits[48 + i] = (byte)i;
        }
        for (int j = n; j <= 26; ++j) {
            Ints.asciiDigits[65 + j] = (byte)(10 + j);
            Ints.asciiDigits[97 + j] = (byte)(10 + j);
        }
    }
    
    public static int saturatedCast(final long n) {
        if (n > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (n < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int)n;
    }
}
