package com.google.common.primitives;

import java.util.Comparator;

public final class UnsignedBytes
{
    public static int compare(final byte b, final byte b2) {
        return toInt(b) - toInt(b2);
    }
    
    static Comparator<byte[]> lexicographicalComparatorJavaImpl() {
        return PureJavaComparator.INSTANCE;
    }
    
    public static int toInt(final byte b) {
        return b & 0xFF;
    }
    
    static class LexicographicalComparatorHolder
    {
        static final Comparator<byte[]> BEST_COMPARATOR;
        
        static {
            BEST_COMPARATOR = UnsignedBytes.lexicographicalComparatorJavaImpl();
        }
        
        enum PureJavaComparator implements Comparator<byte[]>
        {
            INSTANCE;
            
            @Override
            public int compare(final byte[] array, final byte[] array2) {
                for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                    final int compare = UnsignedBytes.compare(array[i], array2[i]);
                    if (compare != 0) {
                        return compare;
                    }
                }
                return array.length - array2.length;
            }
        }
    }
}
