package com.google.common.collect;

import java.util.Comparator;

final class ImmutableSortedAsList<E> extends RegularImmutableAsList<E> implements SortedIterable<E>
{
    ImmutableSortedAsList(final ImmutableSortedSet<E> set, final ImmutableList<E> list) {
        super(set, (ImmutableList<? extends E>)list);
    }
    
    @Override
    public Comparator<? super E> comparator() {
        return this.delegateCollection().comparator();
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    ImmutableSortedSet<E> delegateCollection() {
        return (ImmutableSortedSet<E>)(ImmutableSortedSet)super.delegateCollection();
    }
    
    @Override
    public int indexOf(final Object o) {
        int index = this.delegateCollection().indexOf(o);
        if (index < 0 || !this.get(index).equals(o)) {
            index = -1;
        }
        return index;
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        return this.indexOf(o);
    }
    
    @Override
    ImmutableList<E> subListUnchecked(final int n, final int n2) {
        return new RegularImmutableSortedSet<E>(super.subListUnchecked(n, n2), this.comparator()).asList();
    }
}
