package com.google.common.collect;

abstract class AbstractRangeSet<C extends Comparable> implements RangeSet<C>
{
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof RangeSet && this.asRanges().equals(((RangeSet)o).asRanges()));
    }
    
    @Override
    public final int hashCode() {
        return this.asRanges().hashCode();
    }
    
    @Override
    public final String toString() {
        return this.asRanges().toString();
    }
}
