package com.google.common.collect;

import java.io.Serializable;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;

abstract class ImmutableAsList<E> extends ImmutableList<E>
{
    private void readObject(final ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.delegateCollection().contains(o);
    }
    
    abstract ImmutableCollection<E> delegateCollection();
    
    @Override
    public boolean isEmpty() {
        return this.delegateCollection().isEmpty();
    }
    
    @Override
    boolean isPartialView() {
        return this.delegateCollection().isPartialView();
    }
    
    @Override
    public int size() {
        return this.delegateCollection().size();
    }
    
    @Override
    Object writeReplace() {
        return new SerializedForm(this.delegateCollection());
    }
    
    static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final ImmutableCollection<?> collection;
        
        SerializedForm(final ImmutableCollection<?> collection) {
            this.collection = collection;
        }
        
        Object readResolve() {
            return this.collection.asList();
        }
    }
}
