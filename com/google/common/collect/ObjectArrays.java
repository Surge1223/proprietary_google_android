package com.google.common.collect;

public final class ObjectArrays
{
    static final Object[] EMPTY_ARRAY;
    
    static {
        EMPTY_ARRAY = new Object[0];
    }
    
    static <T> T[] arraysCopyOf(final T[] array, final int n) {
        final Object[] array2 = newArray((Object[])array, n);
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, n));
        return (T[])array2;
    }
    
    static Object checkElementNotNull(final Object o, final int n) {
        if (o != null) {
            return o;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("at index ");
        sb.append(n);
        throw new NullPointerException(sb.toString());
    }
    
    static Object[] checkElementsNotNull(final Object... array) {
        return checkElementsNotNull(array, array.length);
    }
    
    static Object[] checkElementsNotNull(final Object[] array, final int n) {
        for (int i = 0; i < n; ++i) {
            checkElementNotNull(array[i], i);
        }
        return array;
    }
    
    public static <T> T[] newArray(final T[] array, final int n) {
        return Platform.newArray(array, n);
    }
}
