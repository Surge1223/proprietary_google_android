package com.google.common.collect;

import java.util.Collection;
import java.util.Set;
import java.util.Iterator;
import java.util.EnumMap;
import java.util.Map;
import java.io.Serializable;

public abstract class ImmutableMap<K, V> implements Serializable, Map<K, V>
{
    private static final Entry<?, ?>[] EMPTY_ENTRY_ARRAY;
    private transient ImmutableSet<Entry<K, V>> entrySet;
    private transient ImmutableSet<K> keySet;
    private transient ImmutableCollection<V> values;
    
    static {
        EMPTY_ENTRY_ARRAY = new Entry[0];
    }
    
    static void checkNoConflict(final boolean b, final String s, final Entry<?, ?> entry, final Entry<?, ?> entry2) {
        if (b) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Multiple entries with same ");
        sb.append(s);
        sb.append(": ");
        sb.append(entry);
        sb.append(" and ");
        sb.append(entry2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static <K, V> ImmutableMap<K, V> copyOf(final Map<? extends K, ? extends V> map) {
        if (map instanceof ImmutableMap && !(map instanceof ImmutableSortedMap)) {
            final ImmutableMap<K, V> immutableMap = (ImmutableMap<K, V>)map;
            if (!immutableMap.isPartialView()) {
                return immutableMap;
            }
        }
        else if (map instanceof EnumMap) {
            return (ImmutableMap<K, V>)copyOfEnumMapUnsafe((Map<?, ?>)map);
        }
        final Entry[] array = map.entrySet().toArray(ImmutableMap.EMPTY_ENTRY_ARRAY);
        switch (array.length) {
            default: {
                return new RegularImmutableMap<K, V>(array);
            }
            case 1: {
                final Entry entry = array[0];
                return of(entry.getKey(), entry.getValue());
            }
            case 0: {
                return of();
            }
        }
    }
    
    private static <K extends Enum<K>, V> ImmutableMap<K, V> copyOfEnumMap(final Map<K, ? extends V> map) {
        final EnumMap<K, V> enumMap = new EnumMap<K, V>(map);
        for (final Entry<K, V> entry : enumMap.entrySet()) {
            CollectPreconditions.checkEntryNotNull(entry.getKey(), entry.getValue());
        }
        return ImmutableEnumMap.asImmutable(enumMap);
    }
    
    private static <K, V> ImmutableMap<K, V> copyOfEnumMapUnsafe(final Map<? extends K, ? extends V> map) {
        return (ImmutableMap<K, V>)copyOfEnumMap((Map<Enum, ?>)map);
    }
    
    static <K, V> ImmutableMapEntry.TerminalEntry<K, V> entryOf(final K k, final V v) {
        CollectPreconditions.checkEntryNotNull(k, v);
        return (ImmutableMapEntry.TerminalEntry<K, V>)new ImmutableMapEntry.TerminalEntry(k, v);
    }
    
    public static <K, V> ImmutableMap<K, V> of() {
        return (ImmutableMap<K, V>)ImmutableBiMap.of();
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v) {
        return ImmutableBiMap.of(k, v);
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.get(o) != null;
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.values().contains(o);
    }
    
    abstract ImmutableSet<Entry<K, V>> createEntrySet();
    
    ImmutableSet<K> createKeySet() {
        return (ImmutableSet<K>)new ImmutableMapKeySet((ImmutableMap<Object, Object>)this);
    }
    
    @Override
    public ImmutableSet<Entry<K, V>> entrySet() {
        ImmutableSet<Entry<K, V>> entrySet = this.entrySet;
        if (entrySet == null) {
            entrySet = this.createEntrySet();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    @Override
    public boolean equals(final Object o) {
        return Maps.equalsImpl(this, o);
    }
    
    @Override
    public abstract V get(final Object p0);
    
    @Override
    public int hashCode() {
        return this.entrySet().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    abstract boolean isPartialView();
    
    @Override
    public ImmutableSet<K> keySet() {
        ImmutableSet<K> keySet = this.keySet;
        if (keySet == null) {
            keySet = this.createKeySet();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    @Deprecated
    @Override
    public final V put(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final void putAll(final Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final V remove(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String toString() {
        return Maps.toStringImpl(this);
    }
    
    @Override
    public ImmutableCollection<V> values() {
        ImmutableCollection<V> values = this.values;
        if (values == null) {
            values = (ImmutableCollection<V>)new ImmutableMapValues((ImmutableMap<Object, Object>)this);
            this.values = values;
        }
        return values;
    }
    
    Object writeReplace() {
        return new SerializedForm(this);
    }
    
    public static class Builder<K, V>
    {
        ImmutableMapEntry.TerminalEntry<K, V>[] entries;
        int size;
        
        public Builder() {
            this(4);
        }
        
        Builder(final int n) {
            this.entries = (ImmutableMapEntry.TerminalEntry<K, V>[])new ImmutableMapEntry.TerminalEntry[n];
            this.size = 0;
        }
        
        private void ensureCapacity(final int n) {
            if (n > this.entries.length) {
                this.entries = ObjectArrays.arraysCopyOf(this.entries, ImmutableCollection.Builder.expandedCapacity(this.entries.length, n));
            }
        }
        
        public ImmutableMap<K, V> build() {
            switch (this.size) {
                default: {
                    return new RegularImmutableMap<K, V>(this.size, this.entries);
                }
                case 1: {
                    return ImmutableMap.of(this.entries[0].getKey(), this.entries[0].getValue());
                }
                case 0: {
                    return ImmutableMap.of();
                }
            }
        }
        
        public Builder<K, V> put(final K k, final V v) {
            this.ensureCapacity(this.size + 1);
            this.entries[this.size++] = ImmutableMap.entryOf(k, v);
            return this;
        }
    }
    
    static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Object[] keys;
        private final Object[] values;
        
        SerializedForm(final ImmutableMap<?, ?> immutableMap) {
            this.keys = new Object[immutableMap.size()];
            this.values = new Object[immutableMap.size()];
            int n = 0;
            for (final Entry<Object, V> entry : immutableMap.entrySet()) {
                this.keys[n] = entry.getKey();
                this.values[n] = entry.getValue();
                ++n;
            }
        }
        
        Object createMap(final Builder<Object, Object> builder) {
            for (int i = 0; i < this.keys.length; ++i) {
                builder.put(this.keys[i], this.values[i]);
            }
            return builder.build();
        }
        
        Object readResolve() {
            return this.createMap((Builder<Object, Object>)new Builder());
        }
    }
}
