package com.google.common.collect;

final class Hashing
{
    private static int MAX_TABLE_SIZE;
    
    static {
        Hashing.MAX_TABLE_SIZE = 1073741824;
    }
    
    static int closedTableSize(int n, final double n2) {
        final int max = Math.max(n, 2);
        n = Integer.highestOneBit(max);
        if (max > (int)(n * n2)) {
            n <<= 1;
            if (n <= 0) {
                n = Hashing.MAX_TABLE_SIZE;
            }
            return n;
        }
        return n;
    }
    
    static boolean needsResizing(final int n, final int n2, final double n3) {
        return n > n2 * n3 && n2 < Hashing.MAX_TABLE_SIZE;
    }
    
    static int smear(final int n) {
        return 461845907 * Integer.rotateLeft(-862048943 * n, 15);
    }
    
    static int smearedHash(final Object o) {
        int hashCode;
        if (o == null) {
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
        }
        return smear(hashCode);
    }
}
