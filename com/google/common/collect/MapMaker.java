package com.google.common.collect;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.AbstractMap;
import java.util.concurrent.ExecutionException;
import com.google.common.base.Throwables;
import com.google.common.base.Ascii;
import java.util.concurrent.ConcurrentHashMap;
import java.io.Serializable;
import java.util.concurrent.ConcurrentMap;
import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Ticker;
import com.google.common.base.Equivalence;

public final class MapMaker extends GenericMapMaker<Object, Object>
{
    int concurrencyLevel;
    long expireAfterAccessNanos;
    long expireAfterWriteNanos;
    int initialCapacity;
    Equivalence<Object> keyEquivalence;
    MapMakerInternalMap.Strength keyStrength;
    int maximumSize;
    RemovalCause nullRemovalCause;
    Ticker ticker;
    boolean useCustomMap;
    MapMakerInternalMap.Strength valueStrength;
    
    public MapMaker() {
        this.initialCapacity = -1;
        this.concurrencyLevel = -1;
        this.maximumSize = -1;
        this.expireAfterWriteNanos = -1L;
        this.expireAfterAccessNanos = -1L;
    }
    
    private void checkExpiration(final long n, final TimeUnit timeUnit) {
        Preconditions.checkState(this.expireAfterWriteNanos == -1L, "expireAfterWrite was already set to %s ns", this.expireAfterWriteNanos);
        Preconditions.checkState(this.expireAfterAccessNanos == -1L, "expireAfterAccess was already set to %s ns", this.expireAfterAccessNanos);
        Preconditions.checkArgument(n >= 0L, "duration cannot be negative: %s %s", n, timeUnit);
    }
    
    public MapMaker concurrencyLevel(final int concurrencyLevel) {
        final int concurrencyLevel2 = this.concurrencyLevel;
        final boolean b = false;
        Preconditions.checkState(concurrencyLevel2 == -1, "concurrency level was already set to %s", this.concurrencyLevel);
        boolean b2 = b;
        if (concurrencyLevel > 0) {
            b2 = true;
        }
        Preconditions.checkArgument(b2);
        this.concurrencyLevel = concurrencyLevel;
        return this;
    }
    
    @Deprecated
    MapMaker expireAfterAccess(final long n, final TimeUnit timeUnit) {
        this.checkExpiration(n, timeUnit);
        this.expireAfterAccessNanos = timeUnit.toNanos(n);
        if (n == 0L && this.nullRemovalCause == null) {
            this.nullRemovalCause = RemovalCause.EXPIRED;
        }
        this.useCustomMap = true;
        return this;
    }
    
    @Deprecated
    MapMaker expireAfterWrite(final long n, final TimeUnit timeUnit) {
        this.checkExpiration(n, timeUnit);
        this.expireAfterWriteNanos = timeUnit.toNanos(n);
        if (n == 0L && this.nullRemovalCause == null) {
            this.nullRemovalCause = RemovalCause.EXPIRED;
        }
        this.useCustomMap = true;
        return this;
    }
    
    int getConcurrencyLevel() {
        int concurrencyLevel;
        if (this.concurrencyLevel == -1) {
            concurrencyLevel = 4;
        }
        else {
            concurrencyLevel = this.concurrencyLevel;
        }
        return concurrencyLevel;
    }
    
    long getExpireAfterAccessNanos() {
        long expireAfterAccessNanos;
        if (this.expireAfterAccessNanos == -1L) {
            expireAfterAccessNanos = 0L;
        }
        else {
            expireAfterAccessNanos = this.expireAfterAccessNanos;
        }
        return expireAfterAccessNanos;
    }
    
    long getExpireAfterWriteNanos() {
        long expireAfterWriteNanos;
        if (this.expireAfterWriteNanos == -1L) {
            expireAfterWriteNanos = 0L;
        }
        else {
            expireAfterWriteNanos = this.expireAfterWriteNanos;
        }
        return expireAfterWriteNanos;
    }
    
    int getInitialCapacity() {
        int initialCapacity;
        if (this.initialCapacity == -1) {
            initialCapacity = 16;
        }
        else {
            initialCapacity = this.initialCapacity;
        }
        return initialCapacity;
    }
    
    Equivalence<Object> getKeyEquivalence() {
        return MoreObjects.firstNonNull(this.keyEquivalence, this.getKeyStrength().defaultEquivalence());
    }
    
    MapMakerInternalMap.Strength getKeyStrength() {
        return MoreObjects.firstNonNull(this.keyStrength, MapMakerInternalMap.Strength.STRONG);
    }
    
    Ticker getTicker() {
        return MoreObjects.firstNonNull(this.ticker, Ticker.systemTicker());
    }
    
    MapMakerInternalMap.Strength getValueStrength() {
        return MoreObjects.firstNonNull(this.valueStrength, MapMakerInternalMap.Strength.STRONG);
    }
    
    public MapMaker initialCapacity(final int initialCapacity) {
        final int initialCapacity2 = this.initialCapacity;
        final boolean b = false;
        Preconditions.checkState(initialCapacity2 == -1, "initial capacity was already set to %s", this.initialCapacity);
        boolean b2 = b;
        if (initialCapacity >= 0) {
            b2 = true;
        }
        Preconditions.checkArgument(b2);
        this.initialCapacity = initialCapacity;
        return this;
    }
    
    MapMaker keyEquivalence(final Equivalence<Object> equivalence) {
        Preconditions.checkState(this.keyEquivalence == null, "key equivalence was already set to %s", this.keyEquivalence);
        this.keyEquivalence = Preconditions.checkNotNull(equivalence);
        this.useCustomMap = true;
        return this;
    }
    
    @Deprecated
    @Override
     <K, V> ConcurrentMap<K, V> makeComputingMap(final Function<? super K, ? extends V> function) {
        Serializable s;
        if (this.nullRemovalCause == null) {
            s = new ComputingMapAdapter<Object, Object>(this, function);
        }
        else {
            s = new NullComputingConcurrentMap<Object, Object>(this, function);
        }
        return (ComputingMapAdapter<K, V>)s;
    }
    
    public <K, V> ConcurrentMap<K, V> makeMap() {
        if (!this.useCustomMap) {
            return new ConcurrentHashMap<K, V>(this.getInitialCapacity(), 0.75f, this.getConcurrencyLevel());
        }
        Serializable s;
        if (this.nullRemovalCause == null) {
            s = new MapMakerInternalMap<Object, Object>(this);
        }
        else {
            s = new NullConcurrentMap<Object, Object>(this);
        }
        return (MapMakerInternalMap<K, V>)s;
    }
    
    @Deprecated
    MapMaker maximumSize(final int maximumSize) {
        final int maximumSize2 = this.maximumSize;
        final boolean b = false;
        Preconditions.checkState(maximumSize2 == -1, "maximum size was already set to %s", this.maximumSize);
        boolean b2 = b;
        if (maximumSize >= 0) {
            b2 = true;
        }
        Preconditions.checkArgument(b2, (Object)"maximum size must not be negative");
        this.maximumSize = maximumSize;
        this.useCustomMap = true;
        if (this.maximumSize == 0) {
            this.nullRemovalCause = RemovalCause.SIZE;
        }
        return this;
    }
    
    @Deprecated
     <K, V> GenericMapMaker<K, V> removalListener(final RemovalListener<K, V> removalListener) {
        Preconditions.checkState(this.removalListener == null);
        super.removalListener = Preconditions.checkNotNull(removalListener);
        this.useCustomMap = true;
        return (GenericMapMaker<K, V>)this;
    }
    
    MapMaker setKeyStrength(final MapMakerInternalMap.Strength strength) {
        final MapMakerInternalMap.Strength keyStrength = this.keyStrength;
        final boolean b = false;
        Preconditions.checkState(keyStrength == null, "Key strength was already set to %s", this.keyStrength);
        this.keyStrength = Preconditions.checkNotNull(strength);
        boolean b2 = b;
        if (this.keyStrength != MapMakerInternalMap.Strength.SOFT) {
            b2 = true;
        }
        Preconditions.checkArgument(b2, (Object)"Soft keys are not supported");
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.useCustomMap = true;
        }
        return this;
    }
    
    MapMaker setValueStrength(final MapMakerInternalMap.Strength strength) {
        Preconditions.checkState(this.valueStrength == null, "Value strength was already set to %s", this.valueStrength);
        this.valueStrength = Preconditions.checkNotNull(strength);
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.useCustomMap = true;
        }
        return this;
    }
    
    @Override
    public String toString() {
        final MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        if (this.initialCapacity != -1) {
            stringHelper.add("initialCapacity", this.initialCapacity);
        }
        if (this.concurrencyLevel != -1) {
            stringHelper.add("concurrencyLevel", this.concurrencyLevel);
        }
        if (this.maximumSize != -1) {
            stringHelper.add("maximumSize", this.maximumSize);
        }
        if (this.expireAfterWriteNanos != -1L) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.expireAfterWriteNanos);
            sb.append("ns");
            stringHelper.add("expireAfterWrite", sb.toString());
        }
        if (this.expireAfterAccessNanos != -1L) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.expireAfterAccessNanos);
            sb2.append("ns");
            stringHelper.add("expireAfterAccess", sb2.toString());
        }
        if (this.keyStrength != null) {
            stringHelper.add("keyStrength", Ascii.toLowerCase(this.keyStrength.toString()));
        }
        if (this.valueStrength != null) {
            stringHelper.add("valueStrength", Ascii.toLowerCase(this.valueStrength.toString()));
        }
        if (this.keyEquivalence != null) {
            stringHelper.addValue("keyEquivalence");
        }
        if (this.removalListener != null) {
            stringHelper.addValue("removalListener");
        }
        return stringHelper.toString();
    }
    
    public MapMaker weakKeys() {
        return this.setKeyStrength(MapMakerInternalMap.Strength.WEAK);
    }
    
    static final class ComputingMapAdapter<K, V> extends ComputingConcurrentHashMap<K, V> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        
        ComputingMapAdapter(final MapMaker mapMaker, final Function<? super K, ? extends V> function) {
            super(mapMaker, function);
        }
        
        @Override
        public V get(final Object o) {
            try {
                final V orCompute = this.getOrCompute((K)o);
                if (orCompute != null) {
                    return orCompute;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(this.computingFunction);
                sb.append(" returned null for key ");
                sb.append(o);
                sb.append(".");
                throw new NullPointerException(sb.toString());
            }
            catch (ExecutionException ex) {
                final Throwable cause = ex.getCause();
                Throwables.propagateIfInstanceOf(cause, ComputationException.class);
                throw new ComputationException(cause);
            }
        }
    }
    
    static final class NullComputingConcurrentMap<K, V> extends NullConcurrentMap<K, V>
    {
        private static final long serialVersionUID = 0L;
        final Function<? super K, ? extends V> computingFunction;
        
        NullComputingConcurrentMap(final MapMaker mapMaker, final Function<? super K, ? extends V> function) {
            super(mapMaker);
            this.computingFunction = Preconditions.checkNotNull(function);
        }
        
        private V compute(final K k) {
            Preconditions.checkNotNull(k);
            try {
                return (V)this.computingFunction.apply((Object)k);
            }
            catch (Throwable t) {
                throw new ComputationException(t);
            }
            catch (ComputationException ex) {
                throw ex;
            }
        }
        
        @Override
        public V get(final Object o) {
            final V compute = this.compute(o);
            Preconditions.checkNotNull(compute, "%s returned null for key %s.", this.computingFunction, o);
            this.notifyRemoval((K)o, compute);
            return compute;
        }
    }
    
    static class NullConcurrentMap<K, V> extends AbstractMap<K, V> implements Serializable, ConcurrentMap<K, V>
    {
        private static final long serialVersionUID = 0L;
        private final RemovalCause removalCause;
        private final RemovalListener<K, V> removalListener;
        
        NullConcurrentMap(final MapMaker mapMaker) {
            this.removalListener = mapMaker.getRemovalListener();
            this.removalCause = mapMaker.nullRemovalCause;
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return false;
        }
        
        @Override
        public boolean containsValue(final Object o) {
            return false;
        }
        
        @Override
        public Set<Entry<K, V>> entrySet() {
            return Collections.emptySet();
        }
        
        @Override
        public V get(final Object o) {
            return null;
        }
        
        void notifyRemoval(final K k, final V v) {
            this.removalListener.onRemoval((RemovalNotification<K, V>)new RemovalNotification(k, v, this.removalCause));
        }
        
        @Override
        public V put(final K k, final V v) {
            Preconditions.checkNotNull(k);
            Preconditions.checkNotNull(v);
            this.notifyRemoval(k, v);
            return null;
        }
        
        @Override
        public V putIfAbsent(final K k, final V v) {
            return this.put(k, v);
        }
        
        @Override
        public V remove(final Object o) {
            return null;
        }
        
        @Override
        public boolean remove(final Object o, final Object o2) {
            return false;
        }
        
        @Override
        public V replace(final K k, final V v) {
            Preconditions.checkNotNull(k);
            Preconditions.checkNotNull(v);
            return null;
        }
        
        @Override
        public boolean replace(final K k, final V v, final V v2) {
            Preconditions.checkNotNull(k);
            Preconditions.checkNotNull(v2);
            return false;
        }
    }
    
    enum RemovalCause
    {
        COLLECTED(2) {
        }, 
        EXPIRED(3) {
        }, 
        EXPLICIT(0) {
        }, 
        REPLACED(1) {
        }, 
        SIZE(4) {
        };
    }
    
    interface RemovalListener<K, V>
    {
        void onRemoval(final RemovalNotification<K, V> p0);
    }
    
    static final class RemovalNotification<K, V> extends ImmutableEntry<K, V>
    {
        private static final long serialVersionUID = 0L;
        private final RemovalCause cause;
        
        RemovalNotification(final K k, final V v, final RemovalCause cause) {
            super(k, v);
            this.cause = cause;
        }
    }
}
