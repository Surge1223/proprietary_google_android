package com.google.common.collect;

import java.util.Collection;
import java.util.Set;
import java.util.Map;

public abstract class ForwardingMap<K, V> extends ForwardingObject implements Map<K, V>
{
    @Override
    public void clear() {
        this.delegate().clear();
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.delegate().containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.delegate().containsValue(o);
    }
    
    @Override
    protected abstract Map<K, V> delegate();
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        return this.delegate().entrySet();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || this.delegate().equals(o);
    }
    
    @Override
    public V get(final Object o) {
        return this.delegate().get(o);
    }
    
    @Override
    public int hashCode() {
        return this.delegate().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.delegate().isEmpty();
    }
    
    @Override
    public Set<K> keySet() {
        return this.delegate().keySet();
    }
    
    @Override
    public V put(final K k, final V v) {
        return this.delegate().put(k, v);
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        this.delegate().putAll(map);
    }
    
    @Override
    public V remove(final Object o) {
        return this.delegate().remove(o);
    }
    
    @Override
    public int size() {
        return this.delegate().size();
    }
    
    protected String standardToString() {
        return Maps.toStringImpl(this);
    }
    
    @Override
    public Collection<V> values() {
        return this.delegate().values();
    }
}
