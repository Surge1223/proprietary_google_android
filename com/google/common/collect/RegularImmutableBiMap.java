package com.google.common.collect;

import java.util.Iterator;
import java.io.Serializable;
import java.util.Map;

class RegularImmutableBiMap<K, V> extends ImmutableBiMap<K, V>
{
    private final transient ImmutableMapEntry<K, V>[] entries;
    private final transient int hashCode;
    private transient ImmutableBiMap<V, K> inverse;
    private final transient ImmutableMapEntry<K, V>[] keyTable;
    private final transient int mask;
    private final transient ImmutableMapEntry<K, V>[] valueTable;
    
    RegularImmutableBiMap(final int n, final ImmutableMapEntry.TerminalEntry<?, ?>[] array) {
        final int closedTableSize = Hashing.closedTableSize(n, 1.2);
        this.mask = closedTableSize - 1;
        final ImmutableMapEntry<Object, Object>[] entryArray = createEntryArray(closedTableSize);
        final ImmutableMapEntry<Object, Object>[] entryArray2 = createEntryArray(closedTableSize);
        final ImmutableMapEntry<K, V>[] entryArray3 = createEntryArray(n);
        int hashCode = 0;
        for (int i = 0; i < n; ++i) {
            final ImmutableMapEntry.TerminalEntry<?, ?> terminalEntry = array[i];
            final Object key = terminalEntry.getKey();
            final Object value = terminalEntry.getValue();
            final int hashCode2 = key.hashCode();
            final int hashCode3 = value.hashCode();
            final int n2 = Hashing.smear(hashCode2) & this.mask;
            final int n3 = this.mask & Hashing.smear(hashCode3);
            ImmutableEntry<K, V> nextInKeyBucket;
            ImmutableMapEntry<K, V> immutableMapEntry;
            for (immutableMapEntry = (ImmutableMapEntry<K, V>)(nextInKeyBucket = (ImmutableEntry<K, V>)entryArray[n2]); nextInKeyBucket != null; nextInKeyBucket = (ImmutableEntry<K, V>)((ImmutableMapEntry<Object, Object>)nextInKeyBucket).getNextInKeyBucket()) {
                ImmutableMap.checkNoConflict(key.equals(nextInKeyBucket.getKey()) ^ true, "key", terminalEntry, nextInKeyBucket);
            }
            ImmutableEntry<K, V> nextInValueBucket;
            final ImmutableMapEntry<K, V> immutableMapEntry2 = (ImmutableMapEntry<K, V>)(nextInValueBucket = (ImmutableEntry<K, V>)entryArray2[n3]);
            final Object o = value;
            while (nextInValueBucket != null) {
                ImmutableMap.checkNoConflict(o.equals(nextInValueBucket.getValue()) ^ true, "value", terminalEntry, nextInValueBucket);
                nextInValueBucket = (ImmutableEntry<K, V>)((ImmutableMapEntry<Object, Object>)nextInValueBucket).getNextInValueBucket();
            }
            Serializable s;
            if (immutableMapEntry == null && immutableMapEntry2 == null) {
                s = terminalEntry;
            }
            else {
                s = new NonTerminalBiMapEntry<Object, Object>(terminalEntry, immutableMapEntry, immutableMapEntry2);
            }
            entryArray[n2] = (ImmutableMapEntry<Object, Object>)s;
            entryArray3[i] = (ImmutableMapEntry<K, V>)(entryArray2[n3] = (ImmutableMapEntry<Object, Object>)s);
            hashCode += (hashCode2 ^ hashCode3);
        }
        this.keyTable = (ImmutableMapEntry<K, V>[])entryArray;
        this.valueTable = (ImmutableMapEntry<K, V>[])entryArray2;
        this.entries = entryArray3;
        this.hashCode = hashCode;
    }
    
    private static <K, V> ImmutableMapEntry<K, V>[] createEntryArray(final int n) {
        return (ImmutableMapEntry<K, V>[])new ImmutableMapEntry[n];
    }
    
    @Override
    ImmutableSet<Entry<K, V>> createEntrySet() {
        return (ImmutableSet<Entry<K, V>>)new ImmutableMapEntrySet<K, V>() {
            @Override
            ImmutableList<Entry<K, V>> createAsList() {
                return new RegularImmutableAsList<Entry<K, V>>((ImmutableCollection<Entry<K, V>>)this, RegularImmutableBiMap.this.entries);
            }
            
            @Override
            public int hashCode() {
                return RegularImmutableBiMap.this.hashCode;
            }
            
            @Override
            boolean isHashCodeFast() {
                return true;
            }
            
            @Override
            public UnmodifiableIterator<Entry<K, V>> iterator() {
                return (UnmodifiableIterator<Entry<K, V>>)this.asList().iterator();
            }
            
            @Override
            ImmutableMap<K, V> map() {
                return (ImmutableMap<K, V>)RegularImmutableBiMap.this;
            }
        };
    }
    
    @Override
    public V get(final Object o) {
        if (o == null) {
            return null;
        }
        for (ImmutableEntry<K, V> nextInKeyBucket = (ImmutableEntry<K, V>)this.keyTable[Hashing.smear(o.hashCode()) & this.mask]; nextInKeyBucket != null; nextInKeyBucket = (ImmutableEntry<K, V>)((ImmutableMapEntry<Object, Object>)nextInKeyBucket).getNextInKeyBucket()) {
            if (o.equals(nextInKeyBucket.getKey())) {
                return (V)nextInKeyBucket.getValue();
            }
        }
        return null;
    }
    
    @Override
    public ImmutableBiMap<V, K> inverse() {
        ImmutableBiMap<V, K> inverse = this.inverse;
        if (inverse == null) {
            inverse = new Inverse();
            this.inverse = inverse;
        }
        return inverse;
    }
    
    @Override
    boolean isPartialView() {
        return false;
    }
    
    @Override
    public int size() {
        return this.entries.length;
    }
    
    private final class Inverse extends ImmutableBiMap<V, K>
    {
        @Override
        ImmutableSet<Entry<V, K>> createEntrySet() {
            return (ImmutableSet<Entry<V, K>>)new InverseEntrySet();
        }
        
        @Override
        public K get(final Object o) {
            if (o == null) {
                return null;
            }
            for (ImmutableMapEntry<K, Object> nextInValueBucket = (ImmutableMapEntry<K, Object>)RegularImmutableBiMap.this.valueTable[Hashing.smear(o.hashCode()) & RegularImmutableBiMap.this.mask]; nextInValueBucket != null; nextInValueBucket = nextInValueBucket.getNextInValueBucket()) {
                if (o.equals(nextInValueBucket.getValue())) {
                    return nextInValueBucket.getKey();
                }
            }
            return null;
        }
        
        @Override
        public ImmutableBiMap<K, V> inverse() {
            return (ImmutableBiMap<K, V>)RegularImmutableBiMap.this;
        }
        
        @Override
        boolean isPartialView() {
            return false;
        }
        
        @Override
        public int size() {
            return this.inverse().size();
        }
        
        @Override
        Object writeReplace() {
            return new InverseSerializedForm(RegularImmutableBiMap.this);
        }
        
        final class InverseEntrySet extends ImmutableMapEntrySet<V, K>
        {
            @Override
            ImmutableList<Entry<V, K>> createAsList() {
                return new ImmutableAsList<Entry<V, K>>() {
                    @Override
                    ImmutableCollection<Entry<V, K>> delegateCollection() {
                        return (ImmutableCollection<Entry<V, K>>)InverseEntrySet.this;
                    }
                    
                    @Override
                    public Entry<V, K> get(final int n) {
                        final ImmutableMapEntry immutableMapEntry = RegularImmutableBiMap.this.entries[n];
                        return Maps.immutableEntry((V)immutableMapEntry.getValue(), (K)immutableMapEntry.getKey());
                    }
                };
            }
            
            @Override
            public int hashCode() {
                return RegularImmutableBiMap.this.hashCode;
            }
            
            @Override
            boolean isHashCodeFast() {
                return true;
            }
            
            @Override
            public UnmodifiableIterator<Entry<V, K>> iterator() {
                return (UnmodifiableIterator<Entry<V, K>>)this.asList().iterator();
            }
            
            @Override
            ImmutableMap<V, K> map() {
                return Inverse.this;
            }
        }
    }
    
    private static class InverseSerializedForm<K, V> implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final ImmutableBiMap<K, V> forward;
        
        InverseSerializedForm(final ImmutableBiMap<K, V> forward) {
            this.forward = forward;
        }
        
        Object readResolve() {
            return this.forward.inverse();
        }
    }
    
    private static final class NonTerminalBiMapEntry<K, V> extends ImmutableMapEntry<K, V>
    {
        private final ImmutableMapEntry<K, V> nextInKeyBucket;
        private final ImmutableMapEntry<K, V> nextInValueBucket;
        
        NonTerminalBiMapEntry(final ImmutableMapEntry<K, V> immutableMapEntry, final ImmutableMapEntry<K, V> nextInKeyBucket, final ImmutableMapEntry<K, V> nextInValueBucket) {
            super(immutableMapEntry);
            this.nextInKeyBucket = nextInKeyBucket;
            this.nextInValueBucket = nextInValueBucket;
        }
        
        @Override
        ImmutableMapEntry<K, V> getNextInKeyBucket() {
            return this.nextInKeyBucket;
        }
        
        @Override
        ImmutableMapEntry<K, V> getNextInValueBucket() {
            return this.nextInValueBucket;
        }
    }
}
