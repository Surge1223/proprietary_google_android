package com.google.common.collect;

public final class Iterables
{
    public static <T> T getFirst(final Iterable<? extends T> iterable, final T t) {
        return Iterators.getNext(iterable.iterator(), t);
    }
    
    public static <T> T getOnlyElement(final Iterable<T> iterable) {
        return Iterators.getOnlyElement(iterable.iterator());
    }
}
