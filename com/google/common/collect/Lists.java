package com.google.common.collect;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.ListIterator;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.List;
import com.google.common.primitives.Ints;

public final class Lists
{
    static int computeArrayListCapacity(final int n) {
        CollectPreconditions.checkNonnegative(n, "arraySize");
        return Ints.saturatedCast(5L + n + n / 10);
    }
    
    static boolean equalsImpl(final List<?> list, final Object o) {
        final List<?> checkNotNull = Preconditions.checkNotNull(list);
        boolean b = true;
        if (o == checkNotNull) {
            return true;
        }
        if (!(o instanceof List)) {
            return false;
        }
        final List list2 = (List)o;
        if (list.size() != list2.size() || !Iterators.elementsEqual(list.iterator(), list2.iterator())) {
            b = false;
        }
        return b;
    }
    
    static int indexOfImpl(final List<?> list, final Object o) {
        final ListIterator<?> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (Objects.equal(o, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }
    
    static int lastIndexOfImpl(final List<?> list, final Object o) {
        final ListIterator<?> listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (Objects.equal(o, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }
    
    public static <E> ArrayList<E> newArrayList() {
        return new ArrayList<E>();
    }
    
    public static <E> ArrayList<E> newArrayList(final Iterable<? extends E> iterable) {
        Preconditions.checkNotNull(iterable);
        ArrayList<E> arrayList;
        if (iterable instanceof Collection) {
            arrayList = new ArrayList<E>((Collection<? extends E>)Collections2.cast((Iterable<? extends E>)iterable));
        }
        else {
            arrayList = newArrayList((Iterator<? extends E>)iterable.iterator());
        }
        return arrayList;
    }
    
    public static <E> ArrayList<E> newArrayList(final Iterator<? extends E> iterator) {
        final ArrayList<Object> arrayList = newArrayList();
        Iterators.addAll(arrayList, iterator);
        return (ArrayList<E>)arrayList;
    }
}
