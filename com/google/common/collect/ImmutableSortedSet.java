package com.google.common.collect;

import java.io.Serializable;
import java.util.SortedSet;
import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NavigableSet;

public abstract class ImmutableSortedSet<E> extends ImmutableSortedSetFauxverideShim<E> implements SortedIterable<E>, NavigableSet<E>
{
    private static final ImmutableSortedSet<Comparable> NATURAL_EMPTY_SET;
    private static final Comparator<Comparable> NATURAL_ORDER;
    final transient Comparator<? super E> comparator;
    transient ImmutableSortedSet<E> descendingSet;
    
    static {
        NATURAL_ORDER = Ordering.natural();
        NATURAL_EMPTY_SET = new EmptyImmutableSortedSet<Comparable>(ImmutableSortedSet.NATURAL_ORDER);
    }
    
    ImmutableSortedSet(final Comparator<? super E> comparator) {
        this.comparator = comparator;
    }
    
    static <E> ImmutableSortedSet<E> construct(final Comparator<? super E> comparator, final int n, final E... array) {
        if (n == 0) {
            return emptySet(comparator);
        }
        ObjectArrays.checkElementsNotNull(array, n);
        Arrays.sort(array, 0, n, comparator);
        int n2 = 1;
        int n3;
        for (int i = 1; i < n; ++i, n2 = n3) {
            final E e = array[i];
            n3 = n2;
            if (comparator.compare(e, array[n2 - 1]) != 0) {
                array[n2] = e;
                n3 = n2 + 1;
            }
        }
        Arrays.fill(array, n2, n, null);
        return new RegularImmutableSortedSet<E>(ImmutableList.asImmutableList(array, n2), (Comparator<? super Object>)comparator);
    }
    
    private static <E> ImmutableSortedSet<E> emptySet() {
        return (ImmutableSortedSet<E>)ImmutableSortedSet.NATURAL_EMPTY_SET;
    }
    
    static <E> ImmutableSortedSet<E> emptySet(final Comparator<? super E> comparator) {
        if (ImmutableSortedSet.NATURAL_ORDER.equals(comparator)) {
            return emptySet();
        }
        return new EmptyImmutableSortedSet<E>(comparator);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }
    
    static int unsafeCompare(final Comparator<?> comparator, final Object o, final Object o2) {
        return comparator.compare(o, o2);
    }
    
    @Override
    public E ceiling(final E e) {
        return Iterables.getFirst((Iterable<? extends E>)this.tailSet(e, true), (E)null);
    }
    
    @Override
    public Comparator<? super E> comparator() {
        return this.comparator;
    }
    
    ImmutableSortedSet<E> createDescendingSet() {
        return new DescendingImmutableSortedSet<E>(this);
    }
    
    @Override
    public abstract UnmodifiableIterator<E> descendingIterator();
    
    @Override
    public ImmutableSortedSet<E> descendingSet() {
        ImmutableSortedSet<E> descendingSet;
        if ((descendingSet = this.descendingSet) == null) {
            descendingSet = this.createDescendingSet();
            this.descendingSet = descendingSet;
            descendingSet.descendingSet = this;
        }
        return descendingSet;
    }
    
    @Override
    public E first() {
        return this.iterator().next();
    }
    
    @Override
    public E floor(final E e) {
        return Iterators.getNext((Iterator<? extends E>)this.headSet(e, true).descendingIterator(), (E)null);
    }
    
    @Override
    public ImmutableSortedSet<E> headSet(final E e) {
        return this.headSet(e, false);
    }
    
    @Override
    public ImmutableSortedSet<E> headSet(final E e, final boolean b) {
        return this.headSetImpl(Preconditions.checkNotNull(e), b);
    }
    
    abstract ImmutableSortedSet<E> headSetImpl(final E p0, final boolean p1);
    
    @Override
    public E higher(final E e) {
        return Iterables.getFirst((Iterable<? extends E>)this.tailSet(e, false), (E)null);
    }
    
    abstract int indexOf(final Object p0);
    
    @Override
    public abstract UnmodifiableIterator<E> iterator();
    
    @Override
    public E last() {
        return this.descendingIterator().next();
    }
    
    @Override
    public E lower(final E e) {
        return Iterators.getNext((Iterator<? extends E>)this.headSet(e, false).descendingIterator(), (E)null);
    }
    
    @Deprecated
    @Override
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableSortedSet<E> subSet(final E e, final E e2) {
        return this.subSet(e, true, e2, false);
    }
    
    @Override
    public ImmutableSortedSet<E> subSet(final E e, final boolean b, final E e2, final boolean b2) {
        Preconditions.checkNotNull(e);
        Preconditions.checkNotNull(e2);
        Preconditions.checkArgument(this.comparator.compare((Object)e, (Object)e2) <= 0);
        return this.subSetImpl(e, b, e2, b2);
    }
    
    abstract ImmutableSortedSet<E> subSetImpl(final E p0, final boolean p1, final E p2, final boolean p3);
    
    @Override
    public ImmutableSortedSet<E> tailSet(final E e) {
        return this.tailSet(e, true);
    }
    
    @Override
    public ImmutableSortedSet<E> tailSet(final E e, final boolean b) {
        return this.tailSetImpl(Preconditions.checkNotNull(e), b);
    }
    
    abstract ImmutableSortedSet<E> tailSetImpl(final E p0, final boolean p1);
    
    int unsafeCompare(final Object o, final Object o2) {
        return unsafeCompare(this.comparator, o, o2);
    }
    
    @Override
    Object writeReplace() {
        return new SerializedForm((Comparator<? super Object>)this.comparator, this.toArray());
    }
    
    public static final class Builder<E> extends ImmutableSet.Builder<E>
    {
        private final Comparator<? super E> comparator;
        
        public Builder(final Comparator<? super E> comparator) {
            this.comparator = Preconditions.checkNotNull(comparator);
        }
        
        public Builder<E> add(final E e) {
            super.add(e);
            return this;
        }
        
        public Builder<E> add(final E... array) {
            super.add(array);
            return this;
        }
        
        public Builder<E> addAll(final Iterable<? extends E> iterable) {
            super.addAll(iterable);
            return this;
        }
        
        public ImmutableSortedSet<E> build() {
            final ImmutableSortedSet<E> construct = ImmutableSortedSet.construct(this.comparator, this.size, (E[])this.contents);
            this.size = construct.size();
            return construct;
        }
    }
    
    private static class SerializedForm<E> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Comparator<? super E> comparator;
        final Object[] elements;
        
        public SerializedForm(final Comparator<? super E> comparator, final Object[] elements) {
            this.comparator = comparator;
            this.elements = elements;
        }
        
        Object readResolve() {
            return new Builder<Object>((Comparator<? super Object>)this.comparator).add(this.elements).build();
        }
    }
}
