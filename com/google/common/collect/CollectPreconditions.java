package com.google.common.collect;

import com.google.common.base.Preconditions;

final class CollectPreconditions
{
    static void checkEntryNotNull(final Object o, final Object o2) {
        if (o == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("null key in entry: null=");
            sb.append(o2);
            throw new NullPointerException(sb.toString());
        }
        if (o2 != null) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("null value in entry: ");
        sb2.append(o);
        sb2.append("=null");
        throw new NullPointerException(sb2.toString());
    }
    
    static int checkNonnegative(final int n, final String s) {
        if (n >= 0) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(" cannot be negative but was: ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static void checkRemove(final boolean b) {
        Preconditions.checkState(b, (Object)"no calls to next() since the last call to remove()");
    }
}
