package com.google.common.collect;

import java.util.Map;

final class SingletonImmutableBiMap<K, V> extends ImmutableBiMap<K, V>
{
    transient ImmutableBiMap<V, K> inverse;
    final transient K singleKey;
    final transient V singleValue;
    
    SingletonImmutableBiMap(final K singleKey, final V singleValue) {
        CollectPreconditions.checkEntryNotNull(singleKey, singleValue);
        this.singleKey = singleKey;
        this.singleValue = singleValue;
    }
    
    private SingletonImmutableBiMap(final K singleKey, final V singleValue, final ImmutableBiMap<V, K> inverse) {
        this.singleKey = singleKey;
        this.singleValue = singleValue;
        this.inverse = inverse;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.singleKey.equals(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.singleValue.equals(o);
    }
    
    @Override
    ImmutableSet<Entry<K, V>> createEntrySet() {
        return ImmutableSet.of(Maps.immutableEntry(this.singleKey, this.singleValue));
    }
    
    @Override
    ImmutableSet<K> createKeySet() {
        return ImmutableSet.of(this.singleKey);
    }
    
    @Override
    public V get(Object singleValue) {
        if (this.singleKey.equals(singleValue)) {
            singleValue = this.singleValue;
        }
        else {
            singleValue = null;
        }
        return (V)singleValue;
    }
    
    @Override
    public ImmutableBiMap<V, K> inverse() {
        final ImmutableBiMap<V, K> inverse = this.inverse;
        if (inverse == null) {
            return this.inverse = (ImmutableBiMap<V, K>)new SingletonImmutableBiMap(this.singleValue, this.singleKey, (ImmutableBiMap<Object, Object>)this);
        }
        return inverse;
    }
    
    @Override
    boolean isPartialView() {
        return false;
    }
    
    @Override
    public int size() {
        return 1;
    }
}
