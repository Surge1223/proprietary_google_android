package com.google.common.collect;

import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import com.google.common.base.Equivalence;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import com.google.common.base.Preconditions;
import com.google.common.base.Function;

class ComputingConcurrentHashMap<K, V> extends MapMakerInternalMap<K, V>
{
    private static final long serialVersionUID = 4L;
    final Function<? super K, ? extends V> computingFunction;
    
    ComputingConcurrentHashMap(final MapMaker mapMaker, final Function<? super K, ? extends V> function) {
        super(mapMaker);
        this.computingFunction = Preconditions.checkNotNull(function);
    }
    
    @Override
    Segment<K, V> createSegment(final int n, final int n2) {
        return new ComputingSegment<K, V>(this, n, n2);
    }
    
    V getOrCompute(final K k) throws ExecutionException {
        final int hash = this.hash(Preconditions.checkNotNull(k));
        return this.segmentFor(hash).getOrCompute(k, hash, this.computingFunction);
    }
    
    ComputingSegment<K, V> segmentFor(final int n) {
        return (ComputingSegment<K, V>)(ComputingSegment)super.segmentFor(n);
    }
    
    @Override
    Object writeReplace() {
        return new ComputingSerializationProxy(this.keyStrength, this.valueStrength, this.keyEquivalence, this.valueEquivalence, this.expireAfterWriteNanos, this.expireAfterAccessNanos, this.maximumSize, this.concurrencyLevel, (MapMaker.RemovalListener<? super Object, ? super Object>)this.removalListener, (ConcurrentMap<Object, Object>)this, (Function<? super Object, ?>)this.computingFunction);
    }
    
    private static final class ComputationExceptionReference<K, V> implements ValueReference<K, V>
    {
        final Throwable t;
        
        ComputationExceptionReference(final Throwable t) {
            this.t = t;
        }
        
        @Override
        public void clear(final ValueReference<K, V> valueReference) {
        }
        
        @Override
        public ValueReference<K, V> copyFor(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> referenceEntry) {
            return this;
        }
        
        @Override
        public V get() {
            return null;
        }
        
        @Override
        public ReferenceEntry<K, V> getEntry() {
            return null;
        }
        
        @Override
        public boolean isComputingReference() {
            return false;
        }
        
        @Override
        public V waitForValue() throws ExecutionException {
            throw new ExecutionException(this.t);
        }
    }
    
    private static final class ComputedReference<K, V> implements ValueReference<K, V>
    {
        final V value;
        
        ComputedReference(final V value) {
            this.value = value;
        }
        
        @Override
        public void clear(final ValueReference<K, V> valueReference) {
        }
        
        @Override
        public ValueReference<K, V> copyFor(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> referenceEntry) {
            return this;
        }
        
        @Override
        public V get() {
            return this.value;
        }
        
        @Override
        public ReferenceEntry<K, V> getEntry() {
            return null;
        }
        
        @Override
        public boolean isComputingReference() {
            return false;
        }
        
        @Override
        public V waitForValue() {
            return this.get();
        }
    }
    
    static final class ComputingSegment<K, V> extends Segment<K, V>
    {
        ComputingSegment(final MapMakerInternalMap<K, V> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
        }
        
        V compute(final K k, final int n, final ReferenceEntry<K, V> referenceEntry, final ComputingValueReference<K, V> computingValueReference) throws ExecutionException {
            V compute = null;
            final V v = null;
            System.nanoTime();
            long n3;
            final long n2 = n3 = 0L;
            try {
                // monitorenter(referenceEntry)
                compute = v;
                n3 = n2;
                try {
                    final V v2 = compute = computingValueReference.compute(k, n);
                    n3 = n2;
                    final long nanoTime = System.nanoTime();
                    compute = v2;
                    n3 = nanoTime;
                    // monitorexit(referenceEntry)
                    if (v2 != null) {
                        compute = v2;
                        n3 = nanoTime;
                        if (this.put(k, n, v2, true) != null) {
                            compute = v2;
                            n3 = nanoTime;
                            this.enqueueNotification(k, n, v2, MapMaker.RemovalCause.REPLACED);
                        }
                    }
                    return v2;
                }
                finally {
                }
                // monitorexit(referenceEntry)
            }
            finally {
                if (n3 == 0L) {
                    System.nanoTime();
                }
                if (compute == null) {
                    this.clearValue(k, n, computingValueReference);
                }
            }
        }
        
        V getOrCompute(final K k, final int n, final Function<? super K, ? extends V> function) throws ExecutionException {
            try {
                ReferenceEntry<K, V> entry;
                V liveValue;
                while (true) {
                    entry = this.getEntry(k, n);
                    if (entry != null) {
                        liveValue = this.getLiveValue(entry);
                        if (liveValue != null) {
                            break;
                        }
                    }
                    Label_0430: {
                        if (entry != null) {
                            final Object o = entry;
                            if (entry.getValueReference().isComputingReference()) {
                                break Label_0430;
                            }
                        }
                        final boolean b = true;
                        Object o2 = null;
                        this.lock();
                        try {
                            this.preWriteCleanup();
                            final int count = this.count;
                            final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                            final int n2 = table.length() - 1 & n;
                            Object next;
                            final ReferenceEntry<Object, V> referenceEntry = (ReferenceEntry<Object, V>)(next = table.get(n2));
                            boolean b2;
                            while (true) {
                                b2 = b;
                                if (next == null) {
                                    break;
                                }
                                final K key = ((ReferenceEntry<K, Object>)next).getKey();
                                if (((ReferenceEntry)next).getHash() == n && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                                    if (((ReferenceEntry<K, Object>)next).getValueReference().isComputingReference()) {
                                        b2 = false;
                                        break;
                                    }
                                    final V value = ((ReferenceEntry<K, V>)next).getValueReference().get();
                                    if (value == null) {
                                        this.enqueueNotification(key, n, value, MapMaker.RemovalCause.COLLECTED);
                                    }
                                    else {
                                        if (!this.map.expires() || !this.map.isExpired((ReferenceEntry<K, V>)next)) {
                                            this.recordLockedRead((ReferenceEntry<K, V>)next);
                                            return value;
                                        }
                                        this.enqueueNotification(key, n, value, MapMaker.RemovalCause.EXPIRED);
                                    }
                                    this.evictionQueue.remove(next);
                                    this.expirationQueue.remove(next);
                                    this.count = count - 1;
                                    b2 = b;
                                    break;
                                }
                                else {
                                    next = ((ReferenceEntry<K, V>)next).getNext();
                                }
                            }
                            Object entry2 = next;
                            if (b2) {
                                o2 = new ComputingValueReference<K, V>((Function<? super Object, ?>)function);
                                if (next == null) {
                                    entry2 = this.newEntry(k, n, (ReferenceEntry<K, V>)referenceEntry);
                                    ((ReferenceEntry<K, V>)entry2).setValueReference((ValueReference<K, V>)o2);
                                    table.set(n2, (ReferenceEntry<K, V>)entry2);
                                }
                                else {
                                    ((ReferenceEntry<K, Object>)next).setValueReference((ValueReference<K, Object>)o2);
                                    entry2 = next;
                                }
                            }
                            this.unlock();
                            this.postWriteCleanup();
                            final Object o = entry2;
                            if (b2) {
                                return (V)this.compute(k, n, (ReferenceEntry<Object, Object>)entry2, (ComputingValueReference<Object, Object>)o2);
                            }
                            Preconditions.checkState(Thread.holdsLock(o) ^ true, (Object)"Recursive computation");
                            final V waitForValue = ((ReferenceEntry<K, V>)o).getValueReference().waitForValue();
                            if (waitForValue != null) {
                                this.recordRead((ReferenceEntry<K, V>)o);
                                return waitForValue;
                            }
                            continue;
                        }
                        finally {
                            this.unlock();
                            this.postWriteCleanup();
                        }
                    }
                }
                this.recordRead(entry);
                return liveValue;
            }
            finally {
                this.postReadCleanup();
            }
        }
    }
    
    static final class ComputingSerializationProxy<K, V> extends AbstractSerializationProxy<K, V>
    {
        private static final long serialVersionUID = 4L;
        final Function<? super K, ? extends V> computingFunction;
        
        ComputingSerializationProxy(final Strength strength, final Strength strength2, final Equivalence<Object> equivalence, final Equivalence<Object> equivalence2, final long n, final long n2, final int n3, final int n4, final MapMaker.RemovalListener<? super K, ? super V> removalListener, final ConcurrentMap<K, V> concurrentMap, final Function<? super K, ? extends V> computingFunction) {
            super(strength, strength2, equivalence, equivalence2, n, n2, n3, n4, removalListener, concurrentMap);
            this.computingFunction = computingFunction;
        }
        
        private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.delegate = (ConcurrentMap<K, V>)this.readMapMaker(objectInputStream).makeComputingMap((Function<? super K, ? extends V>)this.computingFunction);
            this.readEntries(objectInputStream);
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            this.writeMapTo(objectOutputStream);
        }
        
        Object readResolve() {
            return this.delegate;
        }
    }
    
    private static final class ComputingValueReference<K, V> implements ValueReference<K, V>
    {
        volatile ValueReference<K, V> computedReference;
        final Function<? super K, ? extends V> computingFunction;
        
        public ComputingValueReference(final Function<? super K, ? extends V> computingFunction) {
            this.computedReference = MapMakerInternalMap.unset();
            this.computingFunction = computingFunction;
        }
        
        @Override
        public void clear(final ValueReference<K, V> valueReference) {
            this.setValueReference(valueReference);
        }
        
        V compute(final K k, final int n) throws ExecutionException {
            try {
                final V apply = (V)this.computingFunction.apply((Object)k);
                this.setValueReference(new ComputedReference<K, V>(apply));
                return apply;
            }
            catch (Throwable t) {
                this.setValueReference(new ComputationExceptionReference<K, V>(t));
                throw new ExecutionException(t);
            }
        }
        
        @Override
        public ValueReference<K, V> copyFor(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> referenceEntry) {
            return this;
        }
        
        @Override
        public V get() {
            return null;
        }
        
        @Override
        public ReferenceEntry<K, V> getEntry() {
            return null;
        }
        
        @Override
        public boolean isComputingReference() {
            return true;
        }
        
        void setValueReference(final ValueReference<K, V> computedReference) {
            synchronized (this) {
                if (this.computedReference == MapMakerInternalMap.UNSET) {
                    this.computedReference = computedReference;
                    this.notifyAll();
                }
            }
        }
        
        @Override
        public V waitForValue() throws ExecutionException {
            if (this.computedReference == MapMakerInternalMap.UNSET) {
                boolean b2;
                try {
                    // monitorenter(this)
                    boolean b = false;
                    try {
                    Label_0032_Outer:
                        while (this.computedReference == MapMakerInternalMap.UNSET) {
                            while (true) {
                                try {
                                    this.wait();
                                    continue Label_0032_Outer;
                                }
                                catch (InterruptedException ex) {
                                    b = true;
                                    continue;
                                }
                                break;
                            }
                            break;
                        }
                        // monitorexit(this)
                        if (b) {
                            Thread.currentThread().interrupt();
                            return this.computedReference.waitForValue();
                        }
                        return this.computedReference.waitForValue();
                    }
                    finally {
                    }
                    // monitorexit(this)
                }
                finally {
                    b2 = false;
                }
                if (b2) {
                    Thread.currentThread().interrupt();
                }
            }
            return this.computedReference.waitForValue();
        }
    }
}
