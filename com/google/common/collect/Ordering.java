package com.google.common.collect;

import java.util.concurrent.atomic.AtomicInteger;
import com.google.common.base.Function;
import java.util.Map;
import java.util.Comparator;

public abstract class Ordering<T> implements Comparator<T>
{
    public static <T> Ordering<T> from(final Comparator<T> comparator) {
        Ordering<T> ordering;
        if (comparator instanceof Ordering) {
            ordering = (Ordering<T>)comparator;
        }
        else {
            ordering = new ComparatorOrdering<T>(comparator);
        }
        return ordering;
    }
    
    public static <C extends Comparable> Ordering<C> natural() {
        return (Ordering<C>)NaturalOrdering.INSTANCE;
    }
    
    public static Ordering<Object> usingToString() {
        return UsingToStringOrdering.INSTANCE;
    }
    
    @Override
    public abstract int compare(final T p0, final T p1);
    
     <T2 extends T> Ordering<Map.Entry<T2, ?>> onKeys() {
        return this.onResultOf((Function<Map.Entry<T2, ?>, ? extends T>)Maps.keyFunction());
    }
    
    public <F> Ordering<F> onResultOf(final Function<F, ? extends T> function) {
        return (Ordering<F>)new ByFunctionOrdering((Function<Object, ?>)function, (Ordering<Object>)this);
    }
    
    public <S extends T> Ordering<S> reverse() {
        return new ReverseOrdering<S>(this);
    }
    
    static class ArbitraryOrdering extends Ordering<Object>
    {
        private Map<Object, Integer> uids;
        
        ArbitraryOrdering() {
            this.uids = (Map<Object, Integer>)Platform.tryWeakKeys(new MapMaker()).makeComputingMap((Function<? super Object, ?>)new Function<Object, Integer>() {
                final AtomicInteger counter = new AtomicInteger(0);
                
                @Override
                public Integer apply(final Object o) {
                    return this.counter.getAndIncrement();
                }
            });
        }
        
        @Override
        public int compare(final Object o, final Object o2) {
            if (o == o2) {
                return 0;
            }
            int n = -1;
            if (o == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            final int identityHashCode = this.identityHashCode(o);
            final int identityHashCode2 = this.identityHashCode(o2);
            if (identityHashCode != identityHashCode2) {
                if (identityHashCode >= identityHashCode2) {
                    n = 1;
                }
                return n;
            }
            final int compareTo = this.uids.get(o).compareTo(this.uids.get(o2));
            if (compareTo != 0) {
                return compareTo;
            }
            throw new AssertionError();
        }
        
        int identityHashCode(final Object o) {
            return System.identityHashCode(o);
        }
        
        @Override
        public String toString() {
            return "Ordering.arbitrary()";
        }
    }
    
    static class IncomparableValueException extends ClassCastException
    {
        private static final long serialVersionUID = 0L;
        final Object value;
    }
}
