package com.google.common.collect;

import java.io.Serializable;
import com.google.common.base.Preconditions;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.RandomAccess;
import java.util.List;

public abstract class ImmutableList<E> extends ImmutableCollection<E> implements List<E>, RandomAccess
{
    private static final ImmutableList<Object> EMPTY;
    
    static {
        EMPTY = new RegularImmutableList<Object>(ObjectArrays.EMPTY_ARRAY);
    }
    
    static <E> ImmutableList<E> asImmutableList(final Object[] array) {
        return asImmutableList(array, array.length);
    }
    
    static <E> ImmutableList<E> asImmutableList(final Object[] array, final int n) {
        switch (n) {
            default: {
                Object[] arraysCopy = array;
                if (n < array.length) {
                    arraysCopy = ObjectArrays.arraysCopyOf(array, n);
                }
                return new RegularImmutableList<E>(arraysCopy);
            }
            case 1: {
                return new SingletonImmutableList<E>((E)array[0]);
            }
            case 0: {
                return of();
            }
        }
    }
    
    public static <E> Builder<E> builder() {
        return new Builder<E>();
    }
    
    public static <E> ImmutableList<E> copyOf(final E[] array) {
        switch (array.length) {
            default: {
                return new RegularImmutableList<E>(ObjectArrays.checkElementsNotNull((Object[])array.clone()));
            }
            case 1: {
                return new SingletonImmutableList<E>(array[0]);
            }
            case 0: {
                return of();
            }
        }
    }
    
    public static <E> ImmutableList<E> of() {
        return (ImmutableList<E>)ImmutableList.EMPTY;
    }
    
    public static <E> ImmutableList<E> of(final E e) {
        return new SingletonImmutableList<E>(e);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }
    
    @Deprecated
    @Override
    public final void add(final int n, final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean addAll(final int n, final Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final ImmutableList<E> asList() {
        return this;
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    int copyIntoArray(final Object[] array, final int n) {
        final int size = this.size();
        for (int i = 0; i < size; ++i) {
            array[n + i] = this.get(i);
        }
        return n + size;
    }
    
    @Override
    public boolean equals(final Object o) {
        return Lists.equalsImpl(this, o);
    }
    
    @Override
    public int hashCode() {
        int n = 1;
        for (int size = this.size(), i = 0; i < size; ++i) {
            n = 31 * n + this.get(i).hashCode();
        }
        return n;
    }
    
    @Override
    public int indexOf(final Object o) {
        int indexOfImpl;
        if (o == null) {
            indexOfImpl = -1;
        }
        else {
            indexOfImpl = Lists.indexOfImpl(this, o);
        }
        return indexOfImpl;
    }
    
    @Override
    public UnmodifiableIterator<E> iterator() {
        return this.listIterator();
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        int lastIndexOfImpl;
        if (o == null) {
            lastIndexOfImpl = -1;
        }
        else {
            lastIndexOfImpl = Lists.lastIndexOfImpl(this, o);
        }
        return lastIndexOfImpl;
    }
    
    @Override
    public UnmodifiableListIterator<E> listIterator() {
        return this.listIterator(0);
    }
    
    @Override
    public UnmodifiableListIterator<E> listIterator(final int n) {
        return new AbstractIndexedListIterator<E>(this.size(), n) {
            @Override
            protected E get(final int n) {
                return ImmutableList.this.get(n);
            }
        };
    }
    
    @Deprecated
    @Override
    public final E remove(final int n) {
        throw new UnsupportedOperationException();
    }
    
    public ImmutableList<E> reverse() {
        return new ReverseImmutableList<E>(this);
    }
    
    @Deprecated
    @Override
    public final E set(final int n, final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableList<E> subList(final int n, final int n2) {
        Preconditions.checkPositionIndexes(n, n2, this.size());
        switch (n2 - n) {
            default: {
                return this.subListUnchecked(n, n2);
            }
            case 1: {
                return of(this.get(n));
            }
            case 0: {
                return of();
            }
        }
    }
    
    ImmutableList<E> subListUnchecked(final int n, final int n2) {
        return new SubList(n, n2 - n);
    }
    
    @Override
    Object writeReplace() {
        return new SerializedForm(this.toArray());
    }
    
    public static final class Builder<E> extends ArrayBasedBuilder<E>
    {
        public Builder() {
            this(4);
        }
        
        Builder(final int n) {
            super(n);
        }
        
        public Builder<E> add(final E e) {
            super.add(e);
            return this;
        }
        
        public Builder<E> add(final E... array) {
            super.add(array);
            return this;
        }
        
        public Builder<E> addAll(final Iterable<? extends E> iterable) {
            super.addAll(iterable);
            return this;
        }
        
        public ImmutableList<E> build() {
            return ImmutableList.asImmutableList(this.contents, this.size);
        }
    }
    
    private static class ReverseImmutableList<E> extends ImmutableList<E>
    {
        private final transient ImmutableList<E> forwardList;
        
        ReverseImmutableList(final ImmutableList<E> forwardList) {
            this.forwardList = forwardList;
        }
        
        private int reverseIndex(final int n) {
            return this.size() - 1 - n;
        }
        
        private int reversePosition(final int n) {
            return this.size() - n;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.forwardList.contains(o);
        }
        
        @Override
        public E get(final int n) {
            Preconditions.checkElementIndex(n, this.size());
            return this.forwardList.get(this.reverseIndex(n));
        }
        
        @Override
        public int indexOf(final Object o) {
            final int lastIndex = this.forwardList.lastIndexOf(o);
            int reverseIndex;
            if (lastIndex >= 0) {
                reverseIndex = this.reverseIndex(lastIndex);
            }
            else {
                reverseIndex = -1;
            }
            return reverseIndex;
        }
        
        @Override
        boolean isPartialView() {
            return this.forwardList.isPartialView();
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            final int index = this.forwardList.indexOf(o);
            int reverseIndex;
            if (index >= 0) {
                reverseIndex = this.reverseIndex(index);
            }
            else {
                reverseIndex = -1;
            }
            return reverseIndex;
        }
        
        @Override
        public ImmutableList<E> reverse() {
            return this.forwardList;
        }
        
        @Override
        public int size() {
            return this.forwardList.size();
        }
        
        @Override
        public ImmutableList<E> subList(final int n, final int n2) {
            Preconditions.checkPositionIndexes(n, n2, this.size());
            return this.forwardList.subList(this.reversePosition(n2), this.reversePosition(n)).reverse();
        }
    }
    
    static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Object[] elements;
        
        SerializedForm(final Object[] elements) {
            this.elements = elements;
        }
        
        Object readResolve() {
            return ImmutableList.copyOf(this.elements);
        }
    }
    
    class SubList extends ImmutableList<E>
    {
        final transient int length;
        final transient int offset;
        
        SubList(final int offset, final int length) {
            this.offset = offset;
            this.length = length;
        }
        
        @Override
        public E get(final int n) {
            Preconditions.checkElementIndex(n, this.length);
            return ImmutableList.this.get(this.offset + n);
        }
        
        @Override
        boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.length;
        }
        
        @Override
        public ImmutableList<E> subList(final int n, final int n2) {
            Preconditions.checkPositionIndexes(n, n2, this.length);
            return ImmutableList.this.subList(this.offset + n, this.offset + n2);
        }
    }
}
