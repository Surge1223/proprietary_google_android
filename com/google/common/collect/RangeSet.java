package com.google.common.collect;

import java.util.Set;

public interface RangeSet<C extends Comparable>
{
    Set<Range<C>> asRanges();
}
