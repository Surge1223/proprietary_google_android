package com.google.common.collect;

import java.io.Serializable;

final class UsingToStringOrdering extends Ordering<Object> implements Serializable
{
    static final UsingToStringOrdering INSTANCE;
    private static final long serialVersionUID = 0L;
    
    static {
        INSTANCE = new UsingToStringOrdering();
    }
    
    private Object readResolve() {
        return UsingToStringOrdering.INSTANCE;
    }
    
    @Override
    public int compare(final Object o, final Object o2) {
        return o.toString().compareTo(o2.toString());
    }
    
    @Override
    public String toString() {
        return "Ordering.usingToString()";
    }
}
