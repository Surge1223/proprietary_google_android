package com.google.common.collect;

import java.util.Iterator;
import java.util.List;
import com.google.common.base.Preconditions;

final class SingletonImmutableList<E> extends ImmutableList<E>
{
    final transient E element;
    
    SingletonImmutableList(final E e) {
        this.element = Preconditions.checkNotNull(e);
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.element.equals(o);
    }
    
    @Override
    int copyIntoArray(final Object[] array, final int n) {
        array[n] = this.element;
        return n + 1;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof List) {
            final List list = (List)o;
            if (list.size() != 1 || !this.element.equals(list.get(0))) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public E get(final int n) {
        Preconditions.checkElementIndex(n, 1);
        return this.element;
    }
    
    @Override
    public int hashCode() {
        return 31 + this.element.hashCode();
    }
    
    @Override
    public int indexOf(final Object o) {
        int n;
        if (this.element.equals(o)) {
            n = 0;
        }
        else {
            n = -1;
        }
        return n;
    }
    
    @Override
    public boolean isEmpty() {
        return false;
    }
    
    @Override
    boolean isPartialView() {
        return false;
    }
    
    @Override
    public UnmodifiableIterator<E> iterator() {
        return Iterators.singletonIterator(this.element);
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        return this.indexOf(o);
    }
    
    @Override
    public ImmutableList<E> reverse() {
        return this;
    }
    
    @Override
    public int size() {
        return 1;
    }
    
    @Override
    public ImmutableList<E> subList(final int n, final int n2) {
        Preconditions.checkPositionIndexes(n, n2, 1);
        ImmutableList<E> of;
        if (n == n2) {
            of = ImmutableList.of();
        }
        else {
            of = this;
        }
        return of;
    }
    
    @Override
    public String toString() {
        final String string = this.element.toString();
        final StringBuilder sb = new StringBuilder(string.length() + 2);
        sb.append('[');
        sb.append(string);
        sb.append(']');
        return sb.toString();
    }
}
