package com.google.common.collect;

import java.util.Set;
import java.util.Map;

final class EmptyImmutableBiMap extends ImmutableBiMap<Object, Object>
{
    static final EmptyImmutableBiMap INSTANCE;
    
    static {
        INSTANCE = new EmptyImmutableBiMap();
    }
    
    @Override
    ImmutableSet<Entry<Object, Object>> createEntrySet() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Override
    public ImmutableSet<Entry<Object, Object>> entrySet() {
        return ImmutableSet.of();
    }
    
    @Override
    public Object get(final Object o) {
        return null;
    }
    
    @Override
    public ImmutableBiMap<Object, Object> inverse() {
        return this;
    }
    
    @Override
    public boolean isEmpty() {
        return true;
    }
    
    @Override
    boolean isPartialView() {
        return false;
    }
    
    @Override
    public ImmutableSet<Object> keySet() {
        return ImmutableSet.of();
    }
    
    Object readResolve() {
        return EmptyImmutableBiMap.INSTANCE;
    }
    
    @Override
    public int size() {
        return 0;
    }
}
