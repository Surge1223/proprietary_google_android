package com.google.common.collect;

import java.util.Set;
import java.util.Collection;
import java.util.Map;

public abstract class ImmutableBiMap<K, V> extends ImmutableMap<K, V> implements BiMap<K, V>
{
    private static final Entry<?, ?>[] EMPTY_ENTRY_ARRAY;
    
    static {
        EMPTY_ENTRY_ARRAY = new Entry[0];
    }
    
    public static <K, V> ImmutableBiMap<K, V> of() {
        return (ImmutableBiMap<K, V>)EmptyImmutableBiMap.INSTANCE;
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v) {
        return new SingletonImmutableBiMap<K, V>(k, v);
    }
    
    public abstract ImmutableBiMap<V, K> inverse();
    
    @Override
    public ImmutableSet<V> values() {
        return (ImmutableSet<V>)this.inverse().keySet();
    }
    
    @Override
    Object writeReplace() {
        return new SerializedForm(this);
    }
    
    public static final class Builder<K, V> extends ImmutableMap.Builder<K, V>
    {
        public ImmutableBiMap<K, V> build() {
            switch (this.size) {
                default: {
                    return new RegularImmutableBiMap<K, V>(this.size, this.entries);
                }
                case 1: {
                    return ImmutableBiMap.of((K)this.entries[0].getKey(), (V)this.entries[0].getValue());
                }
                case 0: {
                    return ImmutableBiMap.of();
                }
            }
        }
        
        public Builder<K, V> put(final K k, final V v) {
            super.put(k, v);
            return this;
        }
    }
    
    private static class SerializedForm extends ImmutableMap.SerializedForm
    {
        private static final long serialVersionUID = 0L;
        
        SerializedForm(final ImmutableBiMap<?, ?> immutableBiMap) {
            super(immutableBiMap);
        }
        
        @Override
        Object readResolve() {
            return ((ImmutableMap.SerializedForm)this).createMap(new ImmutableBiMap.Builder<Object, Object>());
        }
    }
}
