package com.google.common.collect;

import java.io.Serializable;
import java.util.Map;

abstract class ImmutableMapEntrySet<K, V> extends ImmutableSet<Map.Entry<K, V>>
{
    @Override
    public boolean contains(Object value) {
        final boolean b = value instanceof Map.Entry;
        final boolean b2 = false;
        if (b) {
            final Map.Entry entry = (Map.Entry)value;
            value = this.map().get(entry.getKey());
            boolean b3 = b2;
            if (value != null) {
                b3 = b2;
                if (value.equals(entry.getValue())) {
                    b3 = true;
                }
            }
            return b3;
        }
        return false;
    }
    
    @Override
    boolean isPartialView() {
        return this.map().isPartialView();
    }
    
    abstract ImmutableMap<K, V> map();
    
    @Override
    public int size() {
        return this.map().size();
    }
    
    @Override
    Object writeReplace() {
        return new EntrySetSerializedForm(this.map());
    }
    
    private static class EntrySetSerializedForm<K, V> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final ImmutableMap<K, V> map;
        
        EntrySetSerializedForm(final ImmutableMap<K, V> map) {
            this.map = map;
        }
        
        Object readResolve() {
            return this.map.entrySet();
        }
    }
}
