package com.google.common.collect;

import java.util.Iterator;
import java.util.Set;
import com.google.common.base.Preconditions;

final class SingletonImmutableSet<E> extends ImmutableSet<E>
{
    private transient int cachedHashCode;
    final transient E element;
    
    SingletonImmutableSet(final E e) {
        this.element = Preconditions.checkNotNull(e);
    }
    
    SingletonImmutableSet(final E element, final int cachedHashCode) {
        this.element = element;
        this.cachedHashCode = cachedHashCode;
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.element.equals(o);
    }
    
    @Override
    int copyIntoArray(final Object[] array, final int n) {
        array[n] = this.element;
        return n + 1;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Set) {
            final Set set = (Set)o;
            if (set.size() != 1 || !this.element.equals(set.iterator().next())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        int cachedHashCode;
        if ((cachedHashCode = this.cachedHashCode) == 0) {
            cachedHashCode = (this.cachedHashCode = this.element.hashCode());
        }
        return cachedHashCode;
    }
    
    @Override
    public boolean isEmpty() {
        return false;
    }
    
    @Override
    boolean isHashCodeFast() {
        return this.cachedHashCode != 0;
    }
    
    @Override
    boolean isPartialView() {
        return false;
    }
    
    @Override
    public UnmodifiableIterator<E> iterator() {
        return Iterators.singletonIterator(this.element);
    }
    
    @Override
    public int size() {
        return 1;
    }
    
    @Override
    public String toString() {
        final String string = this.element.toString();
        final StringBuilder sb = new StringBuilder(string.length() + 2);
        sb.append('[');
        sb.append(string);
        sb.append(']');
        return sb.toString();
    }
}
