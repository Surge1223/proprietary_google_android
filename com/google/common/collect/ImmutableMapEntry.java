package com.google.common.collect;

abstract class ImmutableMapEntry<K, V> extends ImmutableEntry<K, V>
{
    ImmutableMapEntry(final ImmutableMapEntry<K, V> immutableMapEntry) {
        super(immutableMapEntry.getKey(), immutableMapEntry.getValue());
    }
    
    ImmutableMapEntry(final K k, final V v) {
        super(k, v);
        CollectPreconditions.checkEntryNotNull(k, v);
    }
    
    abstract ImmutableMapEntry<K, V> getNextInKeyBucket();
    
    abstract ImmutableMapEntry<K, V> getNextInValueBucket();
    
    static final class TerminalEntry<K, V> extends ImmutableMapEntry<K, V>
    {
        TerminalEntry(final K k, final V v) {
            super(k, v);
        }
        
        @Override
        ImmutableMapEntry<K, V> getNextInKeyBucket() {
            return null;
        }
        
        @Override
        ImmutableMapEntry<K, V> getNextInValueBucket() {
            return null;
        }
    }
}
