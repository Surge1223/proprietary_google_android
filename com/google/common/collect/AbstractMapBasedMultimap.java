package com.google.common.collect;

import com.google.common.collect.AbstractMapBasedMultimap$com.google.common.collect.AbstractMapBasedMultimap$WrappedCollection$com.google.common.collect.AbstractMapBasedMultimap$WrappedCollection$WrappedIterator;
import java.util.ListIterator;
import java.util.ConcurrentModificationException;
import java.util.AbstractCollection;
import com.google.common.collect.AbstractMapBasedMultimap$com.google.common.collect.AbstractMapBasedMultimap$KeySet;
import java.util.Comparator;
import com.google.common.collect.AbstractMapBasedMultimap$com.google.common.collect.AbstractMapBasedMultimap$AsMap;
import com.google.common.collect.AbstractMapBasedMultimap$com.google.common.collect.AbstractMapBasedMultimap$WrappedList;
import java.util.SortedSet;
import com.google.common.base.Preconditions;
import com.google.common.collect.AbstractMapBasedMultimap$com.google.common.collect.AbstractMapBasedMultimap$Itr;
import java.util.Set;
import java.util.SortedMap;
import java.util.RandomAccess;
import com.google.common.collect.AbstractMapBasedMultimap$com.google.common.collect.AbstractMapBasedMultimap$WrappedCollection;
import java.util.List;
import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.io.Serializable;

abstract class AbstractMapBasedMultimap<K, V> extends AbstractMultimap<K, V> implements Serializable
{
    private static final long serialVersionUID = 2447537837011683357L;
    private transient Map<K, Collection<V>> map;
    private transient int totalSize;
    
    static /* synthetic */ int access$212(final AbstractMapBasedMultimap abstractMapBasedMultimap, int totalSize) {
        totalSize += abstractMapBasedMultimap.totalSize;
        return abstractMapBasedMultimap.totalSize = totalSize;
    }
    
    static /* synthetic */ int access$220(final AbstractMapBasedMultimap abstractMapBasedMultimap, int totalSize) {
        totalSize = abstractMapBasedMultimap.totalSize - totalSize;
        return abstractMapBasedMultimap.totalSize = totalSize;
    }
    
    static /* synthetic */ List access$300(final AbstractMapBasedMultimap abstractMapBasedMultimap, final Object o, final List list, final WrappedCollection collection) {
        return abstractMapBasedMultimap.wrapList(o, list, collection);
    }
    
    private Iterator<V> iteratorOrListIterator(final Collection<V> collection) {
        Iterator<V> iterator;
        if (collection instanceof List) {
            iterator = ((List<V>)collection).listIterator();
        }
        else {
            iterator = collection.iterator();
        }
        return iterator;
    }
    
    private int removeValuesForKey(final Object o) {
        final Collection<V> collection = Maps.safeRemove(this.map, o);
        int size = 0;
        if (collection != null) {
            size = collection.size();
            collection.clear();
            this.totalSize -= size;
        }
        return size;
    }
    
    private List<V> wrapList(final K k, final List<V> list, final AbstractMapBasedMultimap$WrappedCollection collection) {
        Object o;
        if (list instanceof RandomAccess) {
            o = new RandomAccessWrappedList((Object)k, (List)list, (WrappedCollection)collection);
        }
        else {
            o = new WrappedList((Object)k, (List)list, (WrappedCollection)collection);
        }
        return (List<V>)o;
    }
    
    @Override
    public void clear() {
        final Iterator<Collection<V>> iterator = this.map.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
        this.map.clear();
        this.totalSize = 0;
    }
    
    @Override
    Map<K, Collection<V>> createAsMap() {
        Object o;
        if (this.map instanceof SortedMap) {
            o = new SortedAsMap((SortedMap)this.map);
        }
        else {
            o = new AsMap(this.map);
        }
        return (Map<K, Collection<V>>)o;
    }
    
    abstract Collection<V> createCollection();
    
    Collection<V> createCollection(final K k) {
        return this.createCollection();
    }
    
    @Override
    Set<K> createKeySet() {
        Object o;
        if (this.map instanceof SortedMap) {
            o = new SortedKeySet((SortedMap)this.map);
        }
        else {
            o = new KeySet(this.map);
        }
        return (Set<K>)o;
    }
    
    @Override
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }
    
    @Override
    Iterator<Map.Entry<K, V>> entryIterator() {
        return (Iterator<Map.Entry<K, V>>)new AbstractMapBasedMultimap$Itr<Map.Entry<K, V>>() {
            Map.Entry<K, V> output(final K k, final V v) {
                return Maps.immutableEntry(k, v);
            }
        };
    }
    
    final void setMap(final Map<K, Collection<V>> map) {
        this.map = map;
        this.totalSize = 0;
        for (final Collection<V> collection : map.values()) {
            Preconditions.checkArgument(collection.isEmpty() ^ true);
            this.totalSize += collection.size();
        }
    }
    
    @Override
    public int size() {
        return this.totalSize;
    }
    
    Collection<V> wrapCollection(final K k, final Collection<V> collection) {
        if (collection instanceof SortedSet) {
            return new WrappedSortedSet((Object)k, (SortedSet)collection, (WrappedCollection)null);
        }
        if (collection instanceof Set) {
            return new WrappedSet(k, (Set<V>)collection);
        }
        if (collection instanceof List) {
            return (Collection<V>)this.wrapList((Object)k, (List)collection, (WrappedCollection)null);
        }
        return new WrappedCollection((Object)k, (Collection)collection, (WrappedCollection)null);
    }
    
    private class AsMap extends ImprovedAbstractMap<K, Collection<V>>
    {
        final transient Map<K, Collection<V>> submap;
        
        AsMap(final Map<K, Collection<V>> submap) {
            this.submap = submap;
        }
        
        @Override
        public void clear() {
            if (this.submap == AbstractMapBasedMultimap.this.map) {
                AbstractMapBasedMultimap.this.clear();
            }
            else {
                Iterators.clear(new AsMapIterator());
            }
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return Maps.safeContainsKey(this.submap, o);
        }
        
        protected Set<Entry<K, Collection<V>>> createEntrySet() {
            return (Set<Entry<K, Collection<V>>>)new AsMapEntries();
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || this.submap.equals(o);
        }
        
        @Override
        public Collection<V> get(final Object o) {
            final Collection<V> collection = Maps.safeGet(this.submap, o);
            if (collection == null) {
                return null;
            }
            return AbstractMapBasedMultimap.this.wrapCollection(o, collection);
        }
        
        @Override
        public int hashCode() {
            return this.submap.hashCode();
        }
        
        @Override
        public Set<K> keySet() {
            return AbstractMapBasedMultimap.this.keySet();
        }
        
        @Override
        public Collection<V> remove(final Object o) {
            final Collection<V> collection = this.submap.remove(o);
            if (collection == null) {
                return null;
            }
            final Collection<V> collection2 = AbstractMapBasedMultimap.this.createCollection();
            collection2.addAll((Collection<? extends V>)collection);
            AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, collection.size());
            collection.clear();
            return collection2;
        }
        
        @Override
        public int size() {
            return this.submap.size();
        }
        
        @Override
        public String toString() {
            return this.submap.toString();
        }
        
        Entry<K, Collection<V>> wrapEntry(final Entry<K, Collection<V>> entry) {
            final K key = entry.getKey();
            return Maps.immutableEntry(key, AbstractMapBasedMultimap.this.wrapCollection(key, entry.getValue()));
        }
        
        class AsMapEntries extends Maps.EntrySet<K, Collection<V>>
        {
            @Override
            public boolean contains(final Object o) {
                return Collections2.safeContains(AsMap.this.submap.entrySet(), o);
            }
            
            @Override
            public Iterator<Entry<K, Collection<V>>> iterator() {
                return new AsMapIterator();
            }
            
            @Override
            Map<K, Collection<V>> map() {
                return AsMap.this;
            }
            
            @Override
            public boolean remove(final Object o) {
                if (!this.contains(o)) {
                    return false;
                }
                AbstractMapBasedMultimap.this.removeValuesForKey(((Entry)o).getKey());
                return true;
            }
        }
        
        class AsMapIterator implements Iterator<Entry<K, Collection<V>>>
        {
            Collection<V> collection;
            final Iterator<Entry<K, Collection<V>>> delegateIterator;
            
            AsMapIterator() {
                this.delegateIterator = AsMap.this.submap.entrySet().iterator();
            }
            
            @Override
            public boolean hasNext() {
                return this.delegateIterator.hasNext();
            }
            
            @Override
            public Entry<K, Collection<V>> next() {
                final Entry entry = this.delegateIterator.next();
                this.collection = entry.getValue();
                return AsMap.this.wrapEntry(entry);
            }
            
            @Override
            public void remove() {
                this.delegateIterator.remove();
                AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, this.collection.size());
                this.collection.clear();
            }
        }
    }
    
    private abstract class Itr<T> implements Iterator<T>
    {
        Collection<V> collection;
        K key;
        final Iterator<Map.Entry<K, Collection<V>>> keyIterator;
        Iterator<V> valueIterator;
        
        Itr() {
            this.keyIterator = AbstractMapBasedMultimap.this.map.entrySet().iterator();
            this.key = null;
            this.collection = null;
            this.valueIterator = Iterators.emptyModifiableIterator();
        }
        
        @Override
        public boolean hasNext() {
            return this.keyIterator.hasNext() || this.valueIterator.hasNext();
        }
        
        @Override
        public T next() {
            if (!this.valueIterator.hasNext()) {
                final Map.Entry entry = this.keyIterator.next();
                this.key = entry.getKey();
                this.collection = entry.getValue();
                this.valueIterator = this.collection.iterator();
            }
            return this.output(this.key, this.valueIterator.next());
        }
        
        abstract T output(final K p0, final V p1);
        
        @Override
        public void remove() {
            this.valueIterator.remove();
            if (this.collection.isEmpty()) {
                this.keyIterator.remove();
            }
            AbstractMapBasedMultimap.this.totalSize--;
        }
    }
    
    private class KeySet extends Maps.KeySet<K, Collection<V>>
    {
        KeySet(final Map<K, Collection<V>> map) {
            super(map);
        }
        
        @Override
        public void clear() {
            Iterators.clear(this.iterator());
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            return ((Maps.KeySet<K, Collection<V>>)this).map().keySet().containsAll(collection);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || ((Maps.KeySet<K, Collection<V>>)this).map().keySet().equals(o);
        }
        
        @Override
        public int hashCode() {
            return ((Maps.KeySet<K, Collection<V>>)this).map().keySet().hashCode();
        }
        
        @Override
        public Iterator<K> iterator() {
            return new Iterator<K>() {
                Map.Entry<K, Collection<V>> entry;
                final /* synthetic */ Iterator val$entryIterator = ((Maps.KeySet<K, Collection<V>>)this).map().entrySet().iterator();
                
                @Override
                public boolean hasNext() {
                    return this.val$entryIterator.hasNext();
                }
                
                @Override
                public K next() {
                    this.entry = (Map.Entry<K, Collection<V>>)this.val$entryIterator.next();
                    return this.entry.getKey();
                }
                
                @Override
                public void remove() {
                    CollectPreconditions.checkRemove(this.entry != null);
                    final Collection<V> collection = this.entry.getValue();
                    this.val$entryIterator.remove();
                    AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, collection.size());
                    collection.clear();
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            int size = 0;
            final Collection<V> collection = ((Maps.KeySet<K, Collection<V>>)this).map().remove(o);
            if (collection != null) {
                size = collection.size();
                collection.clear();
                AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, size);
            }
            return size > 0;
        }
    }
    
    private class RandomAccessWrappedList extends AbstractMapBasedMultimap$WrappedList implements RandomAccess
    {
        RandomAccessWrappedList(final K k, final List<V> list, final AbstractMapBasedMultimap$WrappedCollection collection) {
            super((Object)k, (List)list, (WrappedCollection)collection);
        }
    }
    
    private class SortedAsMap extends AbstractMapBasedMultimap$AsMap implements SortedMap<K, Collection<V>>
    {
        SortedSet<K> sortedKeySet;
        
        SortedAsMap(final SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }
        
        public Comparator<? super K> comparator() {
            return this.sortedMap().comparator();
        }
        
        SortedSet<K> createKeySet() {
            return new SortedKeySet(this.sortedMap());
        }
        
        public K firstKey() {
            return this.sortedMap().firstKey();
        }
        
        public SortedMap<K, Collection<V>> headMap(final K k) {
            return new SortedAsMap(this.sortedMap().headMap(k));
        }
        
        public SortedSet<K> keySet() {
            SortedSet<K> sortedKeySet = this.sortedKeySet;
            if (sortedKeySet == null) {
                sortedKeySet = this.createKeySet();
                this.sortedKeySet = sortedKeySet;
            }
            return sortedKeySet;
        }
        
        public K lastKey() {
            return this.sortedMap().lastKey();
        }
        
        SortedMap<K, Collection<V>> sortedMap() {
            return (SortedMap<K, Collection<V>>)this.submap;
        }
        
        public SortedMap<K, Collection<V>> subMap(final K k, final K i) {
            return new SortedAsMap(this.sortedMap().subMap(k, i));
        }
        
        public SortedMap<K, Collection<V>> tailMap(final K k) {
            return new SortedAsMap(this.sortedMap().tailMap(k));
        }
    }
    
    private class SortedKeySet extends AbstractMapBasedMultimap$KeySet implements SortedSet<K>
    {
        SortedKeySet(final SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }
        
        public Comparator<? super K> comparator() {
            return this.sortedMap().comparator();
        }
        
        public K first() {
            return this.sortedMap().firstKey();
        }
        
        public SortedSet<K> headSet(final K k) {
            return new SortedKeySet(this.sortedMap().headMap(k));
        }
        
        public K last() {
            return this.sortedMap().lastKey();
        }
        
        SortedMap<K, Collection<V>> sortedMap() {
            return (SortedMap<K, Collection<V>>)(SortedMap)super.map();
        }
        
        public SortedSet<K> subSet(final K k, final K i) {
            return new SortedKeySet(this.sortedMap().subMap(k, i));
        }
        
        public SortedSet<K> tailSet(final K k) {
            return new SortedKeySet(this.sortedMap().tailMap(k));
        }
    }
    
    private class WrappedCollection extends AbstractCollection<V>
    {
        final AbstractMapBasedMultimap$WrappedCollection ancestor;
        final Collection<V> ancestorDelegate;
        Collection<V> delegate;
        final K key;
        
        WrappedCollection(final K key, final Collection<V> delegate, final AbstractMapBasedMultimap$WrappedCollection ancestor) {
            this.key = key;
            this.delegate = delegate;
            this.ancestor = (WrappedCollection)ancestor;
            Collection<V> delegate2;
            if (ancestor == null) {
                delegate2 = null;
            }
            else {
                delegate2 = ((WrappedCollection)ancestor).getDelegate();
            }
            this.ancestorDelegate = delegate2;
        }
        
        @Override
        public boolean add(final V v) {
            this.refreshIfEmpty();
            final boolean empty = this.delegate.isEmpty();
            final boolean add = this.delegate.add(v);
            if (add) {
                AbstractMapBasedMultimap.this.totalSize++;
                if (empty) {
                    this.addToMap();
                }
            }
            return add;
        }
        
        @Override
        public boolean addAll(final Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean addAll = this.delegate.addAll(collection);
            if (addAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.delegate.size() - size);
                if (size == 0) {
                    this.addToMap();
                }
            }
            return addAll;
        }
        
        void addToMap() {
            if (this.ancestor != null) {
                this.ancestor.addToMap();
            }
            else {
                AbstractMapBasedMultimap.this.map.put(this.key, this.delegate);
            }
        }
        
        @Override
        public void clear() {
            final int size = this.size();
            if (size == 0) {
                return;
            }
            this.delegate.clear();
            AbstractMapBasedMultimap.access$220(AbstractMapBasedMultimap.this, size);
            this.removeIfEmpty();
        }
        
        @Override
        public boolean contains(final Object o) {
            this.refreshIfEmpty();
            return this.delegate.contains(o);
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            this.refreshIfEmpty();
            return this.delegate.containsAll(collection);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            this.refreshIfEmpty();
            return this.delegate.equals(o);
        }
        
        AbstractMapBasedMultimap$WrappedCollection getAncestor() {
            return (AbstractMapBasedMultimap$WrappedCollection)this.ancestor;
        }
        
        Collection<V> getDelegate() {
            return this.delegate;
        }
        
        K getKey() {
            return this.key;
        }
        
        @Override
        public int hashCode() {
            this.refreshIfEmpty();
            return this.delegate.hashCode();
        }
        
        @Override
        public Iterator<V> iterator() {
            this.refreshIfEmpty();
            return new WrappedIterator();
        }
        
        void refreshIfEmpty() {
            if (this.ancestor != null) {
                this.ancestor.refreshIfEmpty();
                if (this.ancestor.getDelegate() != this.ancestorDelegate) {
                    throw new ConcurrentModificationException();
                }
            }
            else if (this.delegate.isEmpty()) {
                final Collection<V> delegate = AbstractMapBasedMultimap.this.map.get(this.key);
                if (delegate != null) {
                    this.delegate = delegate;
                }
            }
        }
        
        @Override
        public boolean remove(final Object o) {
            this.refreshIfEmpty();
            final boolean remove = this.delegate.remove(o);
            if (remove) {
                AbstractMapBasedMultimap.this.totalSize--;
                this.removeIfEmpty();
            }
            return remove;
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean removeAll = this.delegate.removeAll(collection);
            if (removeAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.delegate.size() - size);
                this.removeIfEmpty();
            }
            return removeAll;
        }
        
        void removeIfEmpty() {
            if (this.ancestor != null) {
                this.ancestor.removeIfEmpty();
            }
            else if (this.delegate.isEmpty()) {
                AbstractMapBasedMultimap.this.map.remove(this.key);
            }
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            Preconditions.checkNotNull(collection);
            final int size = this.size();
            final boolean retainAll = this.delegate.retainAll(collection);
            if (retainAll) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.delegate.size() - size);
                this.removeIfEmpty();
            }
            return retainAll;
        }
        
        @Override
        public int size() {
            this.refreshIfEmpty();
            return this.delegate.size();
        }
        
        @Override
        public String toString() {
            this.refreshIfEmpty();
            return this.delegate.toString();
        }
        
        class WrappedIterator implements Iterator<V>
        {
            final Iterator<V> delegateIterator;
            final Collection<V> originalDelegate;
            
            WrappedIterator() {
                this.originalDelegate = WrappedCollection.this.delegate;
                this.delegateIterator = (Iterator<V>)AbstractMapBasedMultimap.this.iteratorOrListIterator(WrappedCollection.this.delegate);
            }
            
            WrappedIterator(final Iterator<V> delegateIterator) {
                this.originalDelegate = WrappedCollection.this.delegate;
                this.delegateIterator = delegateIterator;
            }
            
            Iterator<V> getDelegateIterator() {
                this.validateIterator();
                return this.delegateIterator;
            }
            
            @Override
            public boolean hasNext() {
                this.validateIterator();
                return this.delegateIterator.hasNext();
            }
            
            @Override
            public V next() {
                this.validateIterator();
                return this.delegateIterator.next();
            }
            
            @Override
            public void remove() {
                this.delegateIterator.remove();
                AbstractMapBasedMultimap.this.totalSize--;
                WrappedCollection.this.removeIfEmpty();
            }
            
            void validateIterator() {
                WrappedCollection.this.refreshIfEmpty();
                if (WrappedCollection.this.delegate == this.originalDelegate) {
                    return;
                }
                throw new ConcurrentModificationException();
            }
        }
    }
    
    private class WrappedList extends AbstractMapBasedMultimap$WrappedCollection implements List<V>
    {
        WrappedList(final K k, final List<V> list, final AbstractMapBasedMultimap$WrappedCollection collection) {
            super((Object)k, (Collection)list, (WrappedCollection)collection);
        }
        
        public void add(final int n, final V v) {
            this.refreshIfEmpty();
            final boolean empty = this.getDelegate().isEmpty();
            this.getListDelegate().add(n, v);
            AbstractMapBasedMultimap.this.totalSize++;
            if (empty) {
                this.addToMap();
            }
        }
        
        public boolean addAll(int size, final Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size2 = this.size();
            final boolean addAll = this.getListDelegate().addAll(size, collection);
            if (addAll) {
                size = this.getDelegate().size();
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, size - size2);
                if (size2 == 0) {
                    this.addToMap();
                }
            }
            return addAll;
        }
        
        public V get(final int n) {
            this.refreshIfEmpty();
            return this.getListDelegate().get(n);
        }
        
        List<V> getListDelegate() {
            return (List<V>)this.getDelegate();
        }
        
        public int indexOf(final Object o) {
            this.refreshIfEmpty();
            return this.getListDelegate().indexOf(o);
        }
        
        public int lastIndexOf(final Object o) {
            this.refreshIfEmpty();
            return this.getListDelegate().lastIndexOf(o);
        }
        
        public ListIterator<V> listIterator() {
            this.refreshIfEmpty();
            return new WrappedListIterator();
        }
        
        public ListIterator<V> listIterator(final int n) {
            this.refreshIfEmpty();
            return new WrappedListIterator(n);
        }
        
        public V remove(final int n) {
            this.refreshIfEmpty();
            final V remove = this.getListDelegate().remove(n);
            AbstractMapBasedMultimap.this.totalSize--;
            this.removeIfEmpty();
            return remove;
        }
        
        public V set(final int n, final V v) {
            this.refreshIfEmpty();
            return this.getListDelegate().set(n, v);
        }
        
        public List<V> subList(final int n, final int n2) {
            this.refreshIfEmpty();
            final AbstractMapBasedMultimap this$0 = AbstractMapBasedMultimap.this;
            final Object key = this.getKey();
            final List<V> subList = this.getListDelegate().subList(n, n2);
            Object ancestor;
            if (this.getAncestor() == null) {
                ancestor = this;
            }
            else {
                ancestor = this.getAncestor();
            }
            return (List<V>)AbstractMapBasedMultimap.access$300(this$0, key, subList, (WrappedCollection)ancestor);
        }
        
        private class WrappedListIterator extends AbstractMapBasedMultimap$WrappedCollection$WrappedIterator implements ListIterator<V>
        {
            WrappedListIterator() {
                super((WrappedCollection)WrappedList.this);
            }
            
            public WrappedListIterator(final int n) {
                super(WrappedList.this.getListDelegate().listIterator(n));
            }
            
            private ListIterator<V> getDelegateListIterator() {
                return (ListIterator<V>)this.getDelegateIterator();
            }
            
            public void add(final V v) {
                final boolean empty = WrappedList.this.isEmpty();
                this.getDelegateListIterator().add(v);
                AbstractMapBasedMultimap.this.totalSize++;
                if (empty) {
                    WrappedList.this.addToMap();
                }
            }
            
            public boolean hasPrevious() {
                return this.getDelegateListIterator().hasPrevious();
            }
            
            public int nextIndex() {
                return this.getDelegateListIterator().nextIndex();
            }
            
            public V previous() {
                return this.getDelegateListIterator().previous();
            }
            
            public int previousIndex() {
                return this.getDelegateListIterator().previousIndex();
            }
            
            public void set(final V v) {
                this.getDelegateListIterator().set(v);
            }
        }
    }
    
    private class WrappedSet extends AbstractMapBasedMultimap$WrappedCollection implements Set<V>
    {
        WrappedSet(final K k, final Set<V> set) {
            super((Object)k, (Collection)set, (WrappedCollection)null);
        }
        
        public boolean removeAll(final Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean removeAllImpl = Sets.removeAllImpl((Set<?>)this.delegate, collection);
            if (removeAllImpl) {
                AbstractMapBasedMultimap.access$212(AbstractMapBasedMultimap.this, this.delegate.size() - size);
                this.removeIfEmpty();
            }
            return removeAllImpl;
        }
    }
    
    private class WrappedSortedSet extends AbstractMapBasedMultimap$WrappedCollection implements SortedSet<V>
    {
        WrappedSortedSet(final K k, final SortedSet<V> set, final AbstractMapBasedMultimap$WrappedCollection collection) {
            super((Object)k, (Collection)set, (WrappedCollection)collection);
        }
        
        public Comparator<? super V> comparator() {
            return this.getSortedSetDelegate().comparator();
        }
        
        public V first() {
            this.refreshIfEmpty();
            return this.getSortedSetDelegate().first();
        }
        
        SortedSet<V> getSortedSetDelegate() {
            return (SortedSet<V>)this.getDelegate();
        }
        
        public SortedSet<V> headSet(final V v) {
            this.refreshIfEmpty();
            final AbstractMapBasedMultimap this$0 = AbstractMapBasedMultimap.this;
            final Object key = this.getKey();
            final SortedSet<V> headSet = this.getSortedSetDelegate().headSet(v);
            Object ancestor;
            if (this.getAncestor() == null) {
                ancestor = this;
            }
            else {
                ancestor = this.getAncestor();
            }
            return this$0.new WrappedSortedSet(key, (SortedSet)headSet, (WrappedCollection)ancestor);
        }
        
        public V last() {
            this.refreshIfEmpty();
            return this.getSortedSetDelegate().last();
        }
        
        public SortedSet<V> subSet(final V v, final V v2) {
            this.refreshIfEmpty();
            final AbstractMapBasedMultimap this$0 = AbstractMapBasedMultimap.this;
            final Object key = this.getKey();
            final SortedSet<V> subSet = this.getSortedSetDelegate().subSet(v, v2);
            Object ancestor;
            if (this.getAncestor() == null) {
                ancestor = this;
            }
            else {
                ancestor = this.getAncestor();
            }
            return this$0.new WrappedSortedSet(key, (SortedSet)subSet, (WrappedCollection)ancestor);
        }
        
        public SortedSet<V> tailSet(final V v) {
            this.refreshIfEmpty();
            final AbstractMapBasedMultimap this$0 = AbstractMapBasedMultimap.this;
            final Object key = this.getKey();
            final SortedSet<V> tailSet = this.getSortedSetDelegate().tailSet(v);
            Object ancestor;
            if (this.getAncestor() == null) {
                ancestor = this;
            }
            else {
                ancestor = this.getAncestor();
            }
            return this$0.new WrappedSortedSet(key, (SortedSet)tailSet, (WrappedCollection)ancestor);
        }
    }
}
