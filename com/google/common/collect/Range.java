package com.google.common.collect;

import com.google.common.base.Preconditions;
import com.google.common.base.Function;
import java.io.Serializable;
import com.google.common.base.Predicate;

public final class Range<C extends Comparable> implements Predicate<C>, Serializable
{
    private static final Range<Comparable> ALL;
    private static final Function<Range, Cut> LOWER_BOUND_FN;
    static final Ordering<Range<?>> RANGE_LEX_ORDERING;
    private static final Function<Range, Cut> UPPER_BOUND_FN;
    private static final long serialVersionUID = 0L;
    final Cut<C> lowerBound;
    final Cut<C> upperBound;
    
    static {
        LOWER_BOUND_FN = new Function<Range, Cut>() {
            @Override
            public Cut apply(final Range range) {
                return range.lowerBound;
            }
        };
        UPPER_BOUND_FN = new Function<Range, Cut>() {
            @Override
            public Cut apply(final Range range) {
                return range.upperBound;
            }
        };
        RANGE_LEX_ORDERING = new Ordering<Range<?>>() {
            @Override
            public int compare(final Range<?> range, final Range<?> range2) {
                return ComparisonChain.start().compare(range.lowerBound, range2.lowerBound).compare(range.upperBound, range2.upperBound).result();
            }
        };
        ALL = new Range<Comparable>((Cut<Comparable>)Cut.belowAll(), (Cut<Comparable>)Cut.aboveAll());
    }
    
    private Range(final Cut<C> cut, final Cut<C> cut2) {
        if (cut.compareTo(cut2) <= 0 && cut != Cut.aboveAll() && cut2 != Cut.belowAll()) {
            this.lowerBound = Preconditions.checkNotNull(cut);
            this.upperBound = Preconditions.checkNotNull(cut2);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid range: ");
        sb.append(toString(cut, cut2));
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static <C extends Comparable<?>> Range<C> all() {
        return (Range<C>)Range.ALL;
    }
    
    public static <C extends Comparable<?>> Range<C> atLeast(final C c) {
        return create((Cut<C>)Cut.belowValue((C)c), Cut.aboveAll());
    }
    
    public static <C extends Comparable<?>> Range<C> atMost(final C c) {
        return create(Cut.belowAll(), (Cut<C>)Cut.aboveValue((C)c));
    }
    
    static int compareOrThrow(final Comparable comparable, final Comparable comparable2) {
        return comparable.compareTo(comparable2);
    }
    
    static <C extends Comparable<?>> Range<C> create(final Cut<C> cut, final Cut<C> cut2) {
        return new Range<C>(cut, cut2);
    }
    
    public static <C extends Comparable<?>> Range<C> downTo(final C c, final BoundType boundType) {
        switch (boundType) {
            default: {
                throw new AssertionError();
            }
            case CLOSED: {
                return atLeast(c);
            }
            case OPEN: {
                return greaterThan(c);
            }
        }
    }
    
    public static <C extends Comparable<?>> Range<C> greaterThan(final C c) {
        return create((Cut<C>)Cut.aboveValue((C)c), Cut.aboveAll());
    }
    
    public static <C extends Comparable<?>> Range<C> lessThan(final C c) {
        return create(Cut.belowAll(), (Cut<C>)Cut.belowValue((C)c));
    }
    
    public static <C extends Comparable<?>> Range<C> range(final C c, final BoundType boundType, final C c2, final BoundType boundType2) {
        Preconditions.checkNotNull(boundType);
        Preconditions.checkNotNull(boundType2);
        Cut<C> cut;
        if (boundType == BoundType.OPEN) {
            cut = Cut.aboveValue(c);
        }
        else {
            cut = Cut.belowValue(c);
        }
        Cut<C> cut2;
        if (boundType2 == BoundType.OPEN) {
            cut2 = Cut.belowValue(c2);
        }
        else {
            cut2 = Cut.aboveValue(c2);
        }
        return create(cut, cut2);
    }
    
    private static String toString(final Cut<?> cut, final Cut<?> cut2) {
        final StringBuilder sb = new StringBuilder(16);
        cut.describeAsLowerBound(sb);
        sb.append('\u2025');
        cut2.describeAsUpperBound(sb);
        return sb.toString();
    }
    
    public static <C extends Comparable<?>> Range<C> upTo(final C c, final BoundType boundType) {
        switch (boundType) {
            default: {
                throw new AssertionError();
            }
            case CLOSED: {
                return atMost(c);
            }
            case OPEN: {
                return lessThan(c);
            }
        }
    }
    
    @Deprecated
    @Override
    public boolean apply(final C c) {
        return this.contains(c);
    }
    
    public boolean contains(final C c) {
        Preconditions.checkNotNull(c);
        return this.lowerBound.isLessThan(c) && !this.upperBound.isLessThan(c);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Range;
        final boolean b2 = false;
        if (b) {
            final Range range = (Range)o;
            boolean b3 = b2;
            if (this.lowerBound.equals(range.lowerBound)) {
                b3 = b2;
                if (this.upperBound.equals(range.upperBound)) {
                    b3 = true;
                }
            }
            return b3;
        }
        return false;
    }
    
    public boolean hasLowerBound() {
        return this.lowerBound != Cut.belowAll();
    }
    
    public boolean hasUpperBound() {
        return this.upperBound != Cut.aboveAll();
    }
    
    @Override
    public int hashCode() {
        return this.lowerBound.hashCode() * 31 + this.upperBound.hashCode();
    }
    
    public Range<C> intersection(final Range<C> range) {
        final int compareTo = this.lowerBound.compareTo(range.lowerBound);
        final int compareTo2 = this.upperBound.compareTo(range.upperBound);
        if (compareTo >= 0 && compareTo2 <= 0) {
            return this;
        }
        if (compareTo <= 0 && compareTo2 >= 0) {
            return range;
        }
        Cut<C> cut;
        if (compareTo >= 0) {
            cut = this.lowerBound;
        }
        else {
            cut = range.lowerBound;
        }
        Cut<C> cut2;
        if (compareTo2 <= 0) {
            cut2 = this.upperBound;
        }
        else {
            cut2 = range.upperBound;
        }
        return create(cut, cut2);
    }
    
    public boolean isConnected(final Range<C> range) {
        return this.lowerBound.compareTo(range.upperBound) <= 0 && range.lowerBound.compareTo(this.upperBound) <= 0;
    }
    
    public C lowerEndpoint() {
        return this.lowerBound.endpoint();
    }
    
    Object readResolve() {
        if (this.equals(Range.ALL)) {
            return all();
        }
        return this;
    }
    
    @Override
    public String toString() {
        return toString(this.lowerBound, this.upperBound);
    }
    
    public C upperEndpoint() {
        return this.upperBound.endpoint();
    }
}
