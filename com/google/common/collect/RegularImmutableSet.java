package com.google.common.collect;

import java.util.Iterator;

final class RegularImmutableSet<E> extends ImmutableSet<E>
{
    private final Object[] elements;
    private final transient int hashCode;
    private final transient int mask;
    final transient Object[] table;
    
    RegularImmutableSet(final Object[] elements, final int hashCode, final Object[] table, final int mask) {
        this.elements = elements;
        this.table = table;
        this.mask = mask;
        this.hashCode = hashCode;
    }
    
    @Override
    public boolean contains(final Object o) {
        if (o == null) {
            return false;
        }
        int smear = Hashing.smear(o.hashCode());
        while (true) {
            final Object o2 = this.table[this.mask & smear];
            if (o2 == null) {
                return false;
            }
            if (o2.equals(o)) {
                return true;
            }
            ++smear;
        }
    }
    
    @Override
    int copyIntoArray(final Object[] array, final int n) {
        System.arraycopy(this.elements, 0, array, n, this.elements.length);
        return this.elements.length + n;
    }
    
    @Override
    ImmutableList<E> createAsList() {
        return new RegularImmutableAsList<E>(this, this.elements);
    }
    
    @Override
    public int hashCode() {
        return this.hashCode;
    }
    
    @Override
    boolean isHashCodeFast() {
        return true;
    }
    
    @Override
    boolean isPartialView() {
        return false;
    }
    
    @Override
    public UnmodifiableIterator<E> iterator() {
        return Iterators.forArray((E[])this.elements);
    }
    
    @Override
    public int size() {
        return this.elements.length;
    }
}
