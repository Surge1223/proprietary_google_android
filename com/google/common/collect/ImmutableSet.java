package com.google.common.collect;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Arrays;
import com.google.common.base.Preconditions;
import java.util.Set;

public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E>
{
    public static <E> Builder<E> builder() {
        return new Builder<E>();
    }
    
    static int chooseTableSize(final int n) {
        boolean b = true;
        if (n < 751619276) {
            int n2;
            for (n2 = Integer.highestOneBit(n - 1) << 1; n2 * 0.7 < n; n2 <<= 1) {}
            return n2;
        }
        if (n >= 1073741824) {
            b = false;
        }
        Preconditions.checkArgument(b, (Object)"collection too large");
        return 1073741824;
    }
    
    private static <E> ImmutableSet<E> construct(final int n, Object... arraysCopy) {
        switch (n) {
            default: {
                final int chooseTableSize = chooseTableSize(n);
                final Object[] array = new Object[chooseTableSize];
                final int n2 = chooseTableSize - 1;
                int n3 = 0;
                int n4 = 0;
                for (int i = 0; i < n; ++i) {
                    final Object checkElementNotNull = ObjectArrays.checkElementNotNull(arraysCopy[i], i);
                    final int hashCode = checkElementNotNull.hashCode();
                    int smear = Hashing.smear(hashCode);
                    while (true) {
                        final int n5 = smear & n2;
                        final Object o = array[n5];
                        if (o == null) {
                            array[n5] = (arraysCopy[n3] = checkElementNotNull);
                            n4 += hashCode;
                            ++n3;
                            break;
                        }
                        if (o.equals(checkElementNotNull)) {
                            break;
                        }
                        ++smear;
                    }
                }
                Arrays.fill(arraysCopy, n3, n, null);
                if (n3 == 1) {
                    return new SingletonImmutableSet<E>(arraysCopy[0], n4);
                }
                if (chooseTableSize != chooseTableSize(n3)) {
                    return (ImmutableSet<E>)construct(n3, arraysCopy);
                }
                if (n3 < arraysCopy.length) {
                    arraysCopy = ObjectArrays.arraysCopyOf(arraysCopy, n3);
                }
                return new RegularImmutableSet<E>(arraysCopy, n4, array, n2);
            }
            case 1: {
                return of(arraysCopy[0]);
            }
            case 0: {
                return of();
            }
        }
    }
    
    public static <E> ImmutableSet<E> copyOf(final E[] array) {
        switch (array.length) {
            default: {
                return construct(array.length, (Object[])array.clone());
            }
            case 1: {
                return of(array[0]);
            }
            case 0: {
                return of();
            }
        }
    }
    
    public static <E> ImmutableSet<E> of() {
        return (ImmutableSet<E>)EmptyImmutableSet.INSTANCE;
    }
    
    public static <E> ImmutableSet<E> of(final E e) {
        return new SingletonImmutableSet<E>(e);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || ((!(o instanceof ImmutableSet) || !this.isHashCodeFast() || !((ImmutableSet)o).isHashCodeFast() || this.hashCode() == o.hashCode()) && Sets.equalsImpl(this, o));
    }
    
    @Override
    public int hashCode() {
        return Sets.hashCodeImpl(this);
    }
    
    boolean isHashCodeFast() {
        return false;
    }
    
    @Override
    public abstract UnmodifiableIterator<E> iterator();
    
    @Override
    Object writeReplace() {
        return new SerializedForm(this.toArray());
    }
    
    public static class Builder<E> extends ArrayBasedBuilder<E>
    {
        public Builder() {
            this(4);
        }
        
        Builder(final int n) {
            super(n);
        }
        
        public Builder<E> add(final E e) {
            super.add(e);
            return this;
        }
        
        public Builder<E> add(final E... array) {
            super.add(array);
            return this;
        }
        
        public Builder<E> addAll(final Iterable<? extends E> iterable) {
            super.addAll(iterable);
            return this;
        }
        
        public ImmutableSet<E> build() {
            final ImmutableSet<Object> access$000 = construct(this.size, this.contents);
            this.size = access$000.size();
            return (ImmutableSet<E>)access$000;
        }
    }
    
    private static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Object[] elements;
        
        SerializedForm(final Object[] elements) {
            this.elements = elements;
        }
        
        Object readResolve() {
            return ImmutableSet.copyOf(this.elements);
        }
    }
}
