package com.google.common.collect;

import java.util.AbstractSet;
import com.google.common.base.Preconditions;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;

public final class Sets
{
    static boolean equalsImpl(final Set<?> set, final Object o) {
        boolean b = true;
        if (set == o) {
            return true;
        }
        if (o instanceof Set) {
            final Set set2 = (Set)o;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    b = false;
                }
                return b;
            }
            catch (ClassCastException ex) {
                return false;
            }
            catch (NullPointerException ex2) {
                return false;
            }
        }
        return false;
    }
    
    static int hashCodeImpl(final Set<?> set) {
        int n = 0;
        for (final Object next : set) {
            int hashCode;
            if (next != null) {
                hashCode = next.hashCode();
            }
            else {
                hashCode = 0;
            }
            n += hashCode;
        }
        return n;
    }
    
    public static <E> HashSet<E> newHashSet() {
        return new HashSet<E>();
    }
    
    public static <E> HashSet<E> newHashSetWithExpectedSize(final int n) {
        return new HashSet<E>(Maps.capacity(n));
    }
    
    static boolean removeAllImpl(final Set<?> set, final Collection<?> collection) {
        Preconditions.checkNotNull(collection);
        Object elementSet = collection;
        if (collection instanceof Multiset) {
            elementSet = ((Multiset<?>)collection).elementSet();
        }
        if (elementSet instanceof Set && ((Collection)elementSet).size() > set.size()) {
            return Iterators.removeAll(set.iterator(), (Collection<?>)elementSet);
        }
        return removeAllImpl(set, ((Collection<?>)elementSet).iterator());
    }
    
    static boolean removeAllImpl(final Set<?> set, final Iterator<?> iterator) {
        boolean b = false;
        while (iterator.hasNext()) {
            b |= set.remove(iterator.next());
        }
        return b;
    }
    
    abstract static class ImprovedAbstractSet<E> extends AbstractSet<E>
    {
        @Override
        public boolean removeAll(final Collection<?> collection) {
            return Sets.removeAllImpl(this, collection);
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            return super.retainAll(Preconditions.checkNotNull(collection));
        }
    }
}
