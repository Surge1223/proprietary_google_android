package com.google.common.collect;

import java.util.Collection;
import java.util.Map;

public interface Multimap<K, V>
{
    Map<K, Collection<V>> asMap();
    
    void clear();
    
    boolean containsEntry(final Object p0, final Object p1);
    
    boolean remove(final Object p0, final Object p1);
    
    int size();
}
