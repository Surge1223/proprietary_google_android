package com.google.common.collect;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicates;
import com.google.common.base.Predicate;
import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Iterator;

public final class Iterators
{
    static final UnmodifiableListIterator<Object> EMPTY_LIST_ITERATOR;
    private static final Iterator<Object> EMPTY_MODIFIABLE_ITERATOR;
    
    static {
        EMPTY_LIST_ITERATOR = new UnmodifiableListIterator<Object>() {
            @Override
            public boolean hasNext() {
                return false;
            }
            
            @Override
            public boolean hasPrevious() {
                return false;
            }
            
            @Override
            public Object next() {
                throw new NoSuchElementException();
            }
            
            @Override
            public int nextIndex() {
                return 0;
            }
            
            @Override
            public Object previous() {
                throw new NoSuchElementException();
            }
            
            @Override
            public int previousIndex() {
                return -1;
            }
        };
        EMPTY_MODIFIABLE_ITERATOR = new Iterator<Object>() {
            @Override
            public boolean hasNext() {
                return false;
            }
            
            @Override
            public Object next() {
                throw new NoSuchElementException();
            }
            
            @Override
            public void remove() {
                CollectPreconditions.checkRemove(false);
            }
        };
    }
    
    public static <T> boolean addAll(final Collection<T> collection, final Iterator<? extends T> iterator) {
        Preconditions.checkNotNull(collection);
        Preconditions.checkNotNull(iterator);
        boolean b = false;
        while (iterator.hasNext()) {
            b |= collection.add((T)iterator.next());
        }
        return b;
    }
    
    public static <T> boolean any(final Iterator<T> iterator, final Predicate<? super T> predicate) {
        return indexOf(iterator, predicate) != -1;
    }
    
    static void clear(final Iterator<?> iterator) {
        Preconditions.checkNotNull(iterator);
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }
    
    public static boolean contains(final Iterator<?> iterator, final Object o) {
        return any(iterator, Predicates.equalTo(o));
    }
    
    public static boolean elementsEqual(final Iterator<?> iterator, final Iterator<?> iterator2) {
        while (iterator.hasNext()) {
            if (!iterator2.hasNext()) {
                return false;
            }
            if (!Objects.equal(iterator.next(), iterator2.next())) {
                return false;
            }
        }
        return iterator2.hasNext() ^ true;
    }
    
    @Deprecated
    public static <T> UnmodifiableIterator<T> emptyIterator() {
        return (UnmodifiableIterator<T>)emptyListIterator();
    }
    
    static <T> UnmodifiableListIterator<T> emptyListIterator() {
        return (UnmodifiableListIterator<T>)Iterators.EMPTY_LIST_ITERATOR;
    }
    
    static <T> Iterator<T> emptyModifiableIterator() {
        return (Iterator<T>)Iterators.EMPTY_MODIFIABLE_ITERATOR;
    }
    
    public static <T> UnmodifiableIterator<T> forArray(final T... array) {
        return forArray(array, 0, array.length, 0);
    }
    
    static <T> UnmodifiableListIterator<T> forArray(final T[] array, final int n, final int n2, final int n3) {
        Preconditions.checkArgument(n2 >= 0);
        Preconditions.checkPositionIndexes(n, n + n2, array.length);
        Preconditions.checkPositionIndex(n3, n2);
        if (n2 == 0) {
            return emptyListIterator();
        }
        return new AbstractIndexedListIterator<T>(n2, n3) {
            @Override
            protected T get(final int n) {
                return array[n + n];
            }
        };
    }
    
    public static <T> T getNext(final Iterator<? extends T> iterator, T next) {
        if (iterator.hasNext()) {
            next = (T)iterator.next();
        }
        return next;
    }
    
    public static <T> T getOnlyElement(final Iterator<T> iterator) {
        final T next = iterator.next();
        if (!iterator.hasNext()) {
            return next;
        }
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("expected one element but was: <");
        sb2.append(next);
        sb.append(sb2.toString());
        for (int n = 0; n < 4 && iterator.hasNext(); ++n) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(", ");
            sb3.append(iterator.next());
            sb.append(sb3.toString());
        }
        if (iterator.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static <T> int indexOf(final Iterator<T> iterator, final Predicate<? super T> predicate) {
        Preconditions.checkNotNull(predicate, (Object)"predicate");
        int n = 0;
        while (iterator.hasNext()) {
            if (predicate.apply(iterator.next())) {
                return n;
            }
            ++n;
        }
        return -1;
    }
    
    public static <T> PeekingIterator<T> peekingIterator(final Iterator<? extends T> iterator) {
        if (iterator instanceof PeekingImpl) {
            return (PeekingImpl<T>)iterator;
        }
        return new PeekingImpl<T>(iterator);
    }
    
    static <T> T pollNext(final Iterator<T> iterator) {
        if (iterator.hasNext()) {
            final T next = iterator.next();
            iterator.remove();
            return next;
        }
        return null;
    }
    
    public static boolean removeAll(final Iterator<?> iterator, final Collection<?> collection) {
        return removeIf(iterator, Predicates.in(collection));
    }
    
    public static <T> boolean removeIf(final Iterator<T> iterator, final Predicate<? super T> predicate) {
        Preconditions.checkNotNull(predicate);
        boolean b = false;
        while (iterator.hasNext()) {
            if (predicate.apply(iterator.next())) {
                iterator.remove();
                b = true;
            }
        }
        return b;
    }
    
    public static <T> UnmodifiableIterator<T> singletonIterator(final T t) {
        return new UnmodifiableIterator<T>() {
            boolean done;
            
            @Override
            public boolean hasNext() {
                return this.done ^ true;
            }
            
            @Override
            public T next() {
                if (!this.done) {
                    this.done = true;
                    return t;
                }
                throw new NoSuchElementException();
            }
        };
    }
    
    public static int size(final Iterator<?> iterator) {
        int n = 0;
        while (iterator.hasNext()) {
            iterator.next();
            ++n;
        }
        return n;
    }
    
    public static <F, T> Iterator<T> transform(final Iterator<F> iterator, final Function<? super F, ? extends T> function) {
        Preconditions.checkNotNull(function);
        return new TransformedIterator<F, T>(iterator) {
            @Override
            T transform(final F n) {
                return function.apply(n);
            }
        };
    }
    
    public static <T> UnmodifiableIterator<T> unmodifiableIterator(final Iterator<T> iterator) {
        Preconditions.checkNotNull(iterator);
        if (iterator instanceof UnmodifiableIterator) {
            return (UnmodifiableIterator<T>)iterator;
        }
        return new UnmodifiableIterator<T>() {
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }
            
            @Override
            public T next() {
                return iterator.next();
            }
        };
    }
    
    private static class PeekingImpl<E> implements PeekingIterator<E>
    {
        private boolean hasPeeked;
        private final Iterator<? extends E> iterator;
        private E peekedElement;
        
        public PeekingImpl(final Iterator<? extends E> iterator) {
            this.iterator = Preconditions.checkNotNull(iterator);
        }
        
        @Override
        public boolean hasNext() {
            return this.hasPeeked || this.iterator.hasNext();
        }
        
        @Override
        public E next() {
            if (!this.hasPeeked) {
                return (E)this.iterator.next();
            }
            final E peekedElement = this.peekedElement;
            this.hasPeeked = false;
            this.peekedElement = null;
            return peekedElement;
        }
        
        @Override
        public E peek() {
            if (!this.hasPeeked) {
                this.peekedElement = (E)this.iterator.next();
                this.hasPeeked = true;
            }
            return this.peekedElement;
        }
        
        @Override
        public void remove() {
            Preconditions.checkState(this.hasPeeked ^ true, (Object)"Can't remove after you've peeked at next");
            this.iterator.remove();
        }
    }
}
