package com.google.common.collect;

import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.concurrent.ExecutionException;
import java.lang.ref.SoftReference;
import java.lang.ref.Reference;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.NoSuchElementException;
import com.google.common.collect.MapMakerInternalMap$com.google.common.collect.MapMakerInternalMap$com.google.common.collect.MapMakerInternalMap$WriteThroughEntry;
import java.util.AbstractSet;
import com.google.common.collect.MapMakerInternalMap$com.google.common.collect.MapMakerInternalMap$HashIterator;
import java.io.ObjectOutputStream;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.io.ObjectInputStream;
import com.google.common.primitives.Ints;
import com.google.common.base.Preconditions;
import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Iterator;
import java.util.AbstractQueue;
import java.lang.ref.ReferenceQueue;
import java.util.Collection;
import com.google.common.base.Ticker;
import com.google.common.base.Equivalence;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.Queue;
import java.util.concurrent.ConcurrentMap;
import java.io.Serializable;
import java.util.AbstractMap;

class MapMakerInternalMap<K, V> extends AbstractMap<K, V> implements Serializable, ConcurrentMap<K, V>
{
    static final Queue<?> DISCARDING_QUEUE;
    static final ValueReference<Object, Object> UNSET;
    private static final Logger logger;
    private static final long serialVersionUID = 5L;
    final int concurrencyLevel;
    final transient EntryFactory entryFactory;
    transient Set<Entry<K, V>> entrySet;
    final long expireAfterAccessNanos;
    final long expireAfterWriteNanos;
    final Equivalence<Object> keyEquivalence;
    transient Set<K> keySet;
    final Strength keyStrength;
    final int maximumSize;
    final MapMaker.RemovalListener<K, V> removalListener;
    final Queue<MapMaker.RemovalNotification<K, V>> removalNotificationQueue;
    final transient int segmentMask;
    final transient int segmentShift;
    final transient Segment<K, V>[] segments;
    final Ticker ticker;
    final Equivalence<Object> valueEquivalence;
    final Strength valueStrength;
    transient Collection<V> values;
    
    static {
        logger = Logger.getLogger(MapMakerInternalMap.class.getName());
        UNSET = (ValueReference)new ValueReference<Object, Object>() {
            @Override
            public void clear(final ValueReference<Object, Object> valueReference) {
            }
            
            @Override
            public ValueReference<Object, Object> copyFor(final ReferenceQueue<Object> referenceQueue, final Object o, final ReferenceEntry<Object, Object> referenceEntry) {
                return this;
            }
            
            @Override
            public Object get() {
                return null;
            }
            
            @Override
            public ReferenceEntry<Object, Object> getEntry() {
                return null;
            }
            
            @Override
            public boolean isComputingReference() {
                return false;
            }
            
            @Override
            public Object waitForValue() {
                return null;
            }
        };
        DISCARDING_QUEUE = new AbstractQueue<Object>() {
            @Override
            public Iterator<Object> iterator() {
                return Iterators.emptyIterator();
            }
            
            @Override
            public boolean offer(final Object o) {
                return true;
            }
            
            @Override
            public Object peek() {
                return null;
            }
            
            @Override
            public Object poll() {
                return null;
            }
            
            @Override
            public int size() {
                return 0;
            }
        };
    }
    
    MapMakerInternalMap(final MapMaker mapMaker) {
        this.concurrencyLevel = Math.min(mapMaker.getConcurrencyLevel(), 65536);
        this.keyStrength = mapMaker.getKeyStrength();
        this.valueStrength = mapMaker.getValueStrength();
        this.keyEquivalence = mapMaker.getKeyEquivalence();
        this.valueEquivalence = this.valueStrength.defaultEquivalence();
        this.maximumSize = mapMaker.maximumSize;
        this.expireAfterAccessNanos = mapMaker.getExpireAfterAccessNanos();
        this.expireAfterWriteNanos = mapMaker.getExpireAfterWriteNanos();
        this.entryFactory = EntryFactory.getFactory(this.keyStrength, this.expires(), this.evictsBySize());
        this.ticker = mapMaker.getTicker();
        this.removalListener = mapMaker.getRemovalListener();
        Queue<MapMaker.RemovalNotification<K, V>> discardingQueue;
        if (this.removalListener == GenericMapMaker.NullListener.INSTANCE) {
            discardingQueue = discardingQueue();
        }
        else {
            discardingQueue = new ConcurrentLinkedQueue<MapMaker.RemovalNotification<K, V>>();
        }
        this.removalNotificationQueue = discardingQueue;
        int n2;
        final int n = n2 = Math.min(mapMaker.getInitialCapacity(), 1073741824);
        if (this.evictsBySize()) {
            n2 = Math.min(n, this.maximumSize);
        }
        int n3 = 0;
        int n4;
        for (n4 = 1; n4 < this.concurrencyLevel && (!this.evictsBySize() || n4 * 2 <= this.maximumSize); n4 <<= 1) {
            ++n3;
        }
        this.segmentShift = 32 - n3;
        this.segmentMask = n4 - 1;
        this.segments = this.newSegmentArray(n4);
        int n6;
        final int n5 = n6 = n2 / n4;
        if (n5 * n4 < n2) {
            n6 = n5 + 1;
        }
        int i;
        for (i = 1; i < n6; i <<= 1) {}
        final boolean evictsBySize = this.evictsBySize();
        final int n7 = 0;
        int j = 0;
        if (evictsBySize) {
            int n8 = this.maximumSize / n4 + 1;
            final int maximumSize = this.maximumSize;
            while (j < this.segments.length) {
                int n9 = n8;
                if (j == maximumSize % n4) {
                    n9 = n8 - 1;
                }
                this.segments[j] = this.createSegment(i, n9);
                ++j;
                n8 = n9;
            }
        }
        else {
            for (int k = n7; k < this.segments.length; ++k) {
                this.segments[k] = this.createSegment(i, -1);
            }
        }
    }
    
    static <K, V> void connectEvictables(final ReferenceEntry<K, V> previousEvictable, final ReferenceEntry<K, V> nextEvictable) {
        previousEvictable.setNextEvictable(nextEvictable);
        nextEvictable.setPreviousEvictable(previousEvictable);
    }
    
    static <K, V> void connectExpirables(final ReferenceEntry<K, V> previousExpirable, final ReferenceEntry<K, V> nextExpirable) {
        previousExpirable.setNextExpirable(nextExpirable);
        nextExpirable.setPreviousExpirable(previousExpirable);
    }
    
    static <E> Queue<E> discardingQueue() {
        return (Queue<E>)MapMakerInternalMap.DISCARDING_QUEUE;
    }
    
    static <K, V> ReferenceEntry<K, V> nullEntry() {
        return (ReferenceEntry<K, V>)NullEntry.INSTANCE;
    }
    
    static <K, V> void nullifyEvictable(final ReferenceEntry<K, V> referenceEntry) {
        final ReferenceEntry<K, V> nullEntry = nullEntry();
        referenceEntry.setNextEvictable(nullEntry);
        referenceEntry.setPreviousEvictable(nullEntry);
    }
    
    static <K, V> void nullifyExpirable(final ReferenceEntry<K, V> referenceEntry) {
        final ReferenceEntry<K, V> nullEntry = nullEntry();
        referenceEntry.setNextExpirable(nullEntry);
        referenceEntry.setPreviousExpirable(nullEntry);
    }
    
    static int rehash(int n) {
        n += (n << 15 ^ 0xFFFFCD7D);
        n ^= n >>> 10;
        n += n << 3;
        n ^= n >>> 6;
        n += (n << 2) + (n << 14);
        return n >>> 16 ^ n;
    }
    
    static <K, V> ValueReference<K, V> unset() {
        return (ValueReference<K, V>)MapMakerInternalMap.UNSET;
    }
    
    @Override
    public void clear() {
        final Segment<K, V>[] segments = this.segments;
        for (int length = segments.length, i = 0; i < length; ++i) {
            segments[i].clear();
        }
    }
    
    @Override
    public boolean containsKey(final Object o) {
        if (o == null) {
            return false;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).containsKey(o, hash);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        if (o == null) {
            return false;
        }
        final Segment<K, V>[] segments = this.segments;
        long n = -1L;
        long n2;
        for (int i = 0; i < 3; ++i, n = n2) {
            final int length = segments.length;
            n2 = 0L;
            for (final Segment<K, V> segment : segments) {
                final int count = segment.count;
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = segment.table;
                for (int k = 0; k < table.length(); ++k) {
                    for (ReferenceEntry<K, V> next = (ReferenceEntry<K, V>)(ReferenceEntry)table.get(k); next != null; next = next.getNext()) {
                        final V liveValue = segment.getLiveValue(next);
                        if (liveValue != null && this.valueEquivalence.equivalent(o, liveValue)) {
                            return true;
                        }
                    }
                }
                n2 += segment.modCount;
            }
            if (n2 == n) {
                break;
            }
        }
        return false;
    }
    
    ReferenceEntry<K, V> copyEntry(final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
        return this.segmentFor(referenceEntry.getHash()).copyEntry(referenceEntry, referenceEntry2);
    }
    
    Segment<K, V> createSegment(final int n, final int n2) {
        return new Segment<K, V>(this, n, n2);
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet = this.entrySet;
        if (entrySet == null) {
            entrySet = new EntrySet();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    boolean evictsBySize() {
        return this.maximumSize != -1;
    }
    
    boolean expires() {
        return this.expiresAfterWrite() || this.expiresAfterAccess();
    }
    
    boolean expiresAfterAccess() {
        return this.expireAfterAccessNanos > 0L;
    }
    
    boolean expiresAfterWrite() {
        return this.expireAfterWriteNanos > 0L;
    }
    
    @Override
    public V get(final Object o) {
        if (o == null) {
            return null;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).get(o, hash);
    }
    
    V getLiveValue(final ReferenceEntry<K, V> referenceEntry) {
        if (referenceEntry.getKey() == null) {
            return null;
        }
        final V value = referenceEntry.getValueReference().get();
        if (value == null) {
            return null;
        }
        if (this.expires() && this.isExpired(referenceEntry)) {
            return null;
        }
        return value;
    }
    
    int hash(final Object o) {
        return rehash(this.keyEquivalence.hash(o));
    }
    
    @Override
    public boolean isEmpty() {
        final Segment<K, V>[] segments = this.segments;
        long n = 0L;
        for (int i = 0; i < segments.length; ++i) {
            if (segments[i].count != 0) {
                return false;
            }
            n += segments[i].modCount;
        }
        if (n != 0L) {
            for (int j = 0; j < segments.length; ++j) {
                if (segments[j].count != 0) {
                    return false;
                }
                n -= segments[j].modCount;
            }
            if (n != 0L) {
                return false;
            }
        }
        return true;
    }
    
    boolean isExpired(final ReferenceEntry<K, V> referenceEntry) {
        return this.isExpired(referenceEntry, this.ticker.read());
    }
    
    boolean isExpired(final ReferenceEntry<K, V> referenceEntry, final long n) {
        return n - referenceEntry.getExpirationTime() > 0L;
    }
    
    boolean isLive(final ReferenceEntry<K, V> referenceEntry) {
        return this.segmentFor(referenceEntry.getHash()).getLiveValue(referenceEntry) != null;
    }
    
    @Override
    public Set<K> keySet() {
        Set<K> keySet = this.keySet;
        if (keySet == null) {
            keySet = new KeySet();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    ReferenceEntry<K, V> newEntry(final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
        return this.segmentFor(n).newEntry(k, n, referenceEntry);
    }
    
    final Segment<K, V>[] newSegmentArray(final int n) {
        return (Segment<K, V>[])new Segment[n];
    }
    
    ValueReference<K, V> newValueReference(final ReferenceEntry<K, V> referenceEntry, final V v) {
        return this.valueStrength.referenceValue(this.segmentFor(referenceEntry.getHash()), referenceEntry, v);
    }
    
    void processPendingNotifications() {
        while (true) {
            final MapMaker.RemovalNotification removalNotification = this.removalNotificationQueue.poll();
            if (removalNotification == null) {
                break;
            }
            try {
                this.removalListener.onRemoval(removalNotification);
            }
            catch (Exception ex) {
                MapMakerInternalMap.logger.log(Level.WARNING, "Exception thrown by removal listener", ex);
            }
        }
    }
    
    @Override
    public V put(final K k, final V v) {
        Preconditions.checkNotNull(k);
        Preconditions.checkNotNull(v);
        final int hash = this.hash(k);
        return this.segmentFor(hash).put(k, hash, v, false);
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        for (final Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            this.put(entry.getKey(), (V)entry.getValue());
        }
    }
    
    @Override
    public V putIfAbsent(final K k, final V v) {
        Preconditions.checkNotNull(k);
        Preconditions.checkNotNull(v);
        final int hash = this.hash(k);
        return this.segmentFor(hash).put(k, hash, v, true);
    }
    
    void reclaimKey(final ReferenceEntry<K, V> referenceEntry) {
        final int hash = referenceEntry.getHash();
        this.segmentFor(hash).reclaimKey(referenceEntry, hash);
    }
    
    void reclaimValue(final ValueReference<K, V> valueReference) {
        final ReferenceEntry<K, V> entry = valueReference.getEntry();
        final int hash = entry.getHash();
        this.segmentFor(hash).reclaimValue(entry.getKey(), hash, valueReference);
    }
    
    @Override
    public V remove(final Object o) {
        if (o == null) {
            return null;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).remove(o, hash);
    }
    
    @Override
    public boolean remove(final Object o, final Object o2) {
        if (o != null && o2 != null) {
            final int hash = this.hash(o);
            return this.segmentFor(hash).remove(o, hash, o2);
        }
        return false;
    }
    
    @Override
    public V replace(final K k, final V v) {
        Preconditions.checkNotNull(k);
        Preconditions.checkNotNull(v);
        final int hash = this.hash(k);
        return this.segmentFor(hash).replace(k, hash, v);
    }
    
    @Override
    public boolean replace(final K k, final V v, final V v2) {
        Preconditions.checkNotNull(k);
        Preconditions.checkNotNull(v2);
        if (v == null) {
            return false;
        }
        final int hash = this.hash(k);
        return this.segmentFor(hash).replace(k, hash, v, v2);
    }
    
    Segment<K, V> segmentFor(final int n) {
        return this.segments[n >>> this.segmentShift & this.segmentMask];
    }
    
    @Override
    public int size() {
        final Segment<K, V>[] segments = this.segments;
        long n = 0L;
        for (int i = 0; i < segments.length; ++i) {
            n += segments[i].count;
        }
        return Ints.saturatedCast(n);
    }
    
    boolean usesKeyReferences() {
        return this.keyStrength != Strength.STRONG;
    }
    
    boolean usesValueReferences() {
        return this.valueStrength != Strength.STRONG;
    }
    
    @Override
    public Collection<V> values() {
        Collection<V> values = this.values;
        if (values == null) {
            values = new Values();
            this.values = values;
        }
        return values;
    }
    
    Object writeReplace() {
        return new SerializationProxy(this.keyStrength, this.valueStrength, this.keyEquivalence, this.valueEquivalence, this.expireAfterWriteNanos, this.expireAfterAccessNanos, this.maximumSize, this.concurrencyLevel, (MapMaker.RemovalListener<? super Object, ? super Object>)this.removalListener, (ConcurrentMap<Object, Object>)this);
    }
    
    abstract static class AbstractReferenceEntry<K, V> implements ReferenceEntry<K, V>
    {
        @Override
        public long getExpirationTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int getHash() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public K getKey() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getNext() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ValueReference<K, V> getValueReference() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setExpirationTime(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setValueReference(final ValueReference<K, V> valueReference) {
            throw new UnsupportedOperationException();
        }
    }
    
    abstract static class AbstractSerializationProxy<K, V> extends ForwardingConcurrentMap<K, V> implements Serializable
    {
        private static final long serialVersionUID = 3L;
        final int concurrencyLevel;
        transient ConcurrentMap<K, V> delegate;
        final long expireAfterAccessNanos;
        final long expireAfterWriteNanos;
        final Equivalence<Object> keyEquivalence;
        final Strength keyStrength;
        final int maximumSize;
        final MapMaker.RemovalListener<? super K, ? super V> removalListener;
        final Equivalence<Object> valueEquivalence;
        final Strength valueStrength;
        
        AbstractSerializationProxy(final Strength keyStrength, final Strength valueStrength, final Equivalence<Object> keyEquivalence, final Equivalence<Object> valueEquivalence, final long expireAfterWriteNanos, final long expireAfterAccessNanos, final int maximumSize, final int concurrencyLevel, final MapMaker.RemovalListener<? super K, ? super V> removalListener, final ConcurrentMap<K, V> delegate) {
            this.keyStrength = keyStrength;
            this.valueStrength = valueStrength;
            this.keyEquivalence = keyEquivalence;
            this.valueEquivalence = valueEquivalence;
            this.expireAfterWriteNanos = expireAfterWriteNanos;
            this.expireAfterAccessNanos = expireAfterAccessNanos;
            this.maximumSize = maximumSize;
            this.concurrencyLevel = concurrencyLevel;
            this.removalListener = removalListener;
            this.delegate = delegate;
        }
        
        @Override
        protected ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }
        
        void readEntries(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            while (true) {
                final Object object = objectInputStream.readObject();
                if (object == null) {
                    break;
                }
                this.delegate.put((K)object, (V)objectInputStream.readObject());
            }
        }
        
        MapMaker readMapMaker(final ObjectInputStream objectInputStream) throws IOException {
            final MapMaker concurrencyLevel = new MapMaker().initialCapacity(objectInputStream.readInt()).setKeyStrength(this.keyStrength).setValueStrength(this.valueStrength).keyEquivalence(this.keyEquivalence).concurrencyLevel(this.concurrencyLevel);
            concurrencyLevel.removalListener(this.removalListener);
            if (this.expireAfterWriteNanos > 0L) {
                concurrencyLevel.expireAfterWrite(this.expireAfterWriteNanos, TimeUnit.NANOSECONDS);
            }
            if (this.expireAfterAccessNanos > 0L) {
                concurrencyLevel.expireAfterAccess(this.expireAfterAccessNanos, TimeUnit.NANOSECONDS);
            }
            if (this.maximumSize != -1) {
                concurrencyLevel.maximumSize(this.maximumSize);
            }
            return concurrencyLevel;
        }
        
        void writeMapTo(final ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeInt(this.delegate.size());
            for (final Map.Entry<Object, Object> entry : this.delegate.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }
    }
    
    enum EntryFactory
    {
        STRONG(0) {
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new StrongEntry<K, V>(k, n, referenceEntry);
            }
        }, 
        STRONG_EVICTABLE(2) {
            @Override
             <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
                final ReferenceEntry<K, V> copyEntry = super.copyEntry(segment, referenceEntry, referenceEntry2);
                this.copyEvictableEntry(referenceEntry, copyEntry);
                return copyEntry;
            }
            
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new StrongEvictableEntry<K, V>(k, n, referenceEntry);
            }
        }, 
        STRONG_EXPIRABLE(1) {
            @Override
             <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
                final ReferenceEntry<K, V> copyEntry = super.copyEntry(segment, referenceEntry, referenceEntry2);
                this.copyExpirableEntry(referenceEntry, copyEntry);
                return copyEntry;
            }
            
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new StrongExpirableEntry<K, V>(k, n, referenceEntry);
            }
        }, 
        STRONG_EXPIRABLE_EVICTABLE(3) {
            @Override
             <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
                final ReferenceEntry<K, V> copyEntry = super.copyEntry(segment, referenceEntry, referenceEntry2);
                this.copyExpirableEntry(referenceEntry, copyEntry);
                this.copyEvictableEntry(referenceEntry, copyEntry);
                return copyEntry;
            }
            
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new StrongExpirableEvictableEntry<K, V>(k, n, referenceEntry);
            }
        }, 
        WEAK(4) {
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new WeakEntry<K, V>(segment.keyReferenceQueue, k, n, referenceEntry);
            }
        }, 
        WEAK_EVICTABLE(6) {
            @Override
             <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
                final ReferenceEntry<K, V> copyEntry = super.copyEntry(segment, referenceEntry, referenceEntry2);
                this.copyEvictableEntry(referenceEntry, copyEntry);
                return copyEntry;
            }
            
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new WeakEvictableEntry<K, V>(segment.keyReferenceQueue, k, n, referenceEntry);
            }
        }, 
        WEAK_EXPIRABLE(5) {
            @Override
             <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
                final ReferenceEntry<K, V> copyEntry = super.copyEntry(segment, referenceEntry, referenceEntry2);
                this.copyExpirableEntry(referenceEntry, copyEntry);
                return copyEntry;
            }
            
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new WeakExpirableEntry<K, V>(segment.keyReferenceQueue, k, n, referenceEntry);
            }
        }, 
        WEAK_EXPIRABLE_EVICTABLE(7) {
            @Override
             <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
                final ReferenceEntry<K, V> copyEntry = super.copyEntry(segment, referenceEntry, referenceEntry2);
                this.copyExpirableEntry(referenceEntry, copyEntry);
                this.copyEvictableEntry(referenceEntry, copyEntry);
                return copyEntry;
            }
            
            @Override
             <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> segment, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
                return new WeakExpirableEvictableEntry<K, V>(segment.keyReferenceQueue, k, n, referenceEntry);
            }
        };
        
        static final EntryFactory[][] factories;
        
        static {
            factories = new EntryFactory[][] { { EntryFactory.STRONG, EntryFactory.STRONG_EXPIRABLE, EntryFactory.STRONG_EVICTABLE, EntryFactory.STRONG_EXPIRABLE_EVICTABLE }, new EntryFactory[0], { EntryFactory.WEAK, EntryFactory.WEAK_EXPIRABLE, EntryFactory.WEAK_EVICTABLE, EntryFactory.WEAK_EXPIRABLE_EVICTABLE } };
        }
        
        static EntryFactory getFactory(final Strength strength, final boolean b, final boolean b2) {
            int n;
            if (b2) {
                n = 2;
            }
            else {
                n = 0;
            }
            return EntryFactory.factories[strength.ordinal()][n | (b ? 1 : 0)];
        }
        
         <K, V> ReferenceEntry<K, V> copyEntry(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
            return this.newEntry(segment, referenceEntry.getKey(), referenceEntry.getHash(), referenceEntry2);
        }
        
         <K, V> void copyEvictableEntry(final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
            MapMakerInternalMap.connectEvictables((ReferenceEntry<K, V>)referenceEntry.getPreviousEvictable(), referenceEntry2);
            MapMakerInternalMap.connectEvictables(referenceEntry2, (ReferenceEntry<K, V>)referenceEntry.getNextEvictable());
            MapMakerInternalMap.nullifyEvictable((ReferenceEntry)referenceEntry);
        }
        
         <K, V> void copyExpirableEntry(final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
            referenceEntry2.setExpirationTime(referenceEntry.getExpirationTime());
            MapMakerInternalMap.connectExpirables(referenceEntry.getPreviousExpirable(), referenceEntry2);
            MapMakerInternalMap.connectExpirables(referenceEntry2, referenceEntry.getNextExpirable());
            MapMakerInternalMap.nullifyExpirable(referenceEntry);
        }
        
        abstract <K, V> ReferenceEntry<K, V> newEntry(final Segment<K, V> p0, final K p1, final int p2, final ReferenceEntry<K, V> p3);
    }
    
    final class EntryIterator extends MapMakerInternalMap$HashIterator<Entry<K, V>>
    {
        public Entry<K, V> next() {
            return this.nextEntry();
        }
    }
    
    final class EntrySet extends AbstractSet<Entry<K, V>>
    {
        @Override
        public void clear() {
            MapMakerInternalMap.this.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)o;
            final Object key = entry.getKey();
            if (key == null) {
                return false;
            }
            final V value = MapMakerInternalMap.this.get(key);
            boolean b3 = b2;
            if (value != null) {
                b3 = b2;
                if (MapMakerInternalMap.this.valueEquivalence.equivalent(entry.getValue(), value)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public boolean isEmpty() {
            return MapMakerInternalMap.this.isEmpty();
        }
        
        @Override
        public Iterator<Entry<K, V>> iterator() {
            return (Iterator<Entry<K, V>>)new EntryIterator();
        }
        
        @Override
        public boolean remove(Object key) {
            final boolean b = key instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)key;
            key = entry.getKey();
            boolean b3 = b2;
            if (key != null) {
                b3 = b2;
                if (MapMakerInternalMap.this.remove(key, entry.getValue())) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int size() {
            return MapMakerInternalMap.this.size();
        }
    }
    
    static final class EvictionQueue<K, V> extends AbstractQueue<ReferenceEntry<K, V>>
    {
        final ReferenceEntry<K, V> head;
        
        EvictionQueue() {
            this.head = new AbstractReferenceEntry<K, V>() {
                ReferenceEntry<K, V> nextEvictable = this;
                ReferenceEntry<K, V> previousEvictable = this;
                
                @Override
                public ReferenceEntry<K, V> getNextEvictable() {
                    return this.nextEvictable;
                }
                
                @Override
                public ReferenceEntry<K, V> getPreviousEvictable() {
                    return this.previousEvictable;
                }
                
                @Override
                public void setNextEvictable(final ReferenceEntry<K, V> nextEvictable) {
                    this.nextEvictable = nextEvictable;
                }
                
                @Override
                public void setPreviousEvictable(final ReferenceEntry<K, V> previousEvictable) {
                    this.previousEvictable = previousEvictable;
                }
            };
        }
        
        @Override
        public void clear() {
            ReferenceEntry<K, V> nextEvictable2;
            for (ReferenceEntry<K, V> nextEvictable = this.head.getNextEvictable(); nextEvictable != this.head; nextEvictable = nextEvictable2) {
                nextEvictable2 = nextEvictable.getNextEvictable();
                MapMakerInternalMap.nullifyEvictable(nextEvictable);
            }
            this.head.setNextEvictable(this.head);
            this.head.setPreviousEvictable(this.head);
        }
        
        @Override
        public boolean contains(final Object o) {
            return ((ReferenceEntry)o).getNextEvictable() != NullEntry.INSTANCE;
        }
        
        @Override
        public boolean isEmpty() {
            return this.head.getNextEvictable() == this.head;
        }
        
        @Override
        public Iterator<ReferenceEntry<K, V>> iterator() {
            return new AbstractSequentialIterator<ReferenceEntry<K, V>>(this.peek()) {
                @Override
                protected ReferenceEntry<K, V> computeNext(final ReferenceEntry<K, V> referenceEntry) {
                    ReferenceEntry<K, V> nextEvictable = referenceEntry.getNextEvictable();
                    if (nextEvictable == EvictionQueue.this.head) {
                        nextEvictable = null;
                    }
                    return nextEvictable;
                }
            };
        }
        
        @Override
        public boolean offer(final ReferenceEntry<K, V> referenceEntry) {
            MapMakerInternalMap.connectEvictables(referenceEntry.getPreviousEvictable(), referenceEntry.getNextEvictable());
            MapMakerInternalMap.connectEvictables(this.head.getPreviousEvictable(), referenceEntry);
            MapMakerInternalMap.connectEvictables(referenceEntry, this.head);
            return true;
        }
        
        @Override
        public ReferenceEntry<K, V> peek() {
            ReferenceEntry<K, V> nextEvictable = this.head.getNextEvictable();
            if (nextEvictable == this.head) {
                nextEvictable = null;
            }
            return nextEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> poll() {
            final ReferenceEntry<K, V> nextEvictable = this.head.getNextEvictable();
            if (nextEvictable == this.head) {
                return null;
            }
            this.remove(nextEvictable);
            return nextEvictable;
        }
        
        @Override
        public boolean remove(final Object o) {
            final ReferenceEntry referenceEntry = (ReferenceEntry)o;
            final ReferenceEntry<K, V> previousEvictable = referenceEntry.getPreviousEvictable();
            final ReferenceEntry<K, V> nextEvictable = referenceEntry.getNextEvictable();
            MapMakerInternalMap.connectEvictables(previousEvictable, nextEvictable);
            MapMakerInternalMap.nullifyEvictable((ReferenceEntry<Object, Object>)referenceEntry);
            return nextEvictable != NullEntry.INSTANCE;
        }
        
        @Override
        public int size() {
            int n = 0;
            Object o = this.head;
            while (true) {
                o = ((ReferenceEntry<K, V>)o).getNextEvictable();
                if (o == this.head) {
                    break;
                }
                ++n;
            }
            return n;
        }
    }
    
    static final class ExpirationQueue<K, V> extends AbstractQueue<ReferenceEntry<K, V>>
    {
        final ReferenceEntry<K, V> head;
        
        ExpirationQueue() {
            this.head = new AbstractReferenceEntry<K, V>() {
                ReferenceEntry<K, V> nextExpirable = this;
                ReferenceEntry<K, V> previousExpirable = this;
                
                @Override
                public long getExpirationTime() {
                    return Long.MAX_VALUE;
                }
                
                @Override
                public ReferenceEntry<K, V> getNextExpirable() {
                    return this.nextExpirable;
                }
                
                @Override
                public ReferenceEntry<K, V> getPreviousExpirable() {
                    return this.previousExpirable;
                }
                
                @Override
                public void setExpirationTime(final long n) {
                }
                
                @Override
                public void setNextExpirable(final ReferenceEntry<K, V> nextExpirable) {
                    this.nextExpirable = nextExpirable;
                }
                
                @Override
                public void setPreviousExpirable(final ReferenceEntry<K, V> previousExpirable) {
                    this.previousExpirable = previousExpirable;
                }
            };
        }
        
        @Override
        public void clear() {
            ReferenceEntry<K, V> nextExpirable2;
            for (ReferenceEntry<K, V> nextExpirable = this.head.getNextExpirable(); nextExpirable != this.head; nextExpirable = nextExpirable2) {
                nextExpirable2 = nextExpirable.getNextExpirable();
                MapMakerInternalMap.nullifyExpirable(nextExpirable);
            }
            this.head.setNextExpirable(this.head);
            this.head.setPreviousExpirable(this.head);
        }
        
        @Override
        public boolean contains(final Object o) {
            return ((ReferenceEntry)o).getNextExpirable() != NullEntry.INSTANCE;
        }
        
        @Override
        public boolean isEmpty() {
            return this.head.getNextExpirable() == this.head;
        }
        
        @Override
        public Iterator<ReferenceEntry<K, V>> iterator() {
            return new AbstractSequentialIterator<ReferenceEntry<K, V>>(this.peek()) {
                @Override
                protected ReferenceEntry<K, V> computeNext(final ReferenceEntry<K, V> referenceEntry) {
                    ReferenceEntry<K, V> nextExpirable = referenceEntry.getNextExpirable();
                    if (nextExpirable == ExpirationQueue.this.head) {
                        nextExpirable = null;
                    }
                    return nextExpirable;
                }
            };
        }
        
        @Override
        public boolean offer(final ReferenceEntry<K, V> referenceEntry) {
            MapMakerInternalMap.connectExpirables(referenceEntry.getPreviousExpirable(), referenceEntry.getNextExpirable());
            MapMakerInternalMap.connectExpirables(this.head.getPreviousExpirable(), referenceEntry);
            MapMakerInternalMap.connectExpirables(referenceEntry, this.head);
            return true;
        }
        
        @Override
        public ReferenceEntry<K, V> peek() {
            ReferenceEntry<K, V> nextExpirable = this.head.getNextExpirable();
            if (nextExpirable == this.head) {
                nextExpirable = null;
            }
            return nextExpirable;
        }
        
        @Override
        public ReferenceEntry<K, V> poll() {
            final ReferenceEntry<K, V> nextExpirable = this.head.getNextExpirable();
            if (nextExpirable == this.head) {
                return null;
            }
            this.remove(nextExpirable);
            return nextExpirable;
        }
        
        @Override
        public boolean remove(final Object o) {
            final ReferenceEntry referenceEntry = (ReferenceEntry)o;
            final ReferenceEntry<K, V> previousExpirable = referenceEntry.getPreviousExpirable();
            final ReferenceEntry<K, V> nextExpirable = referenceEntry.getNextExpirable();
            MapMakerInternalMap.connectExpirables(previousExpirable, nextExpirable);
            MapMakerInternalMap.nullifyExpirable((ReferenceEntry<Object, Object>)referenceEntry);
            return nextExpirable != NullEntry.INSTANCE;
        }
        
        @Override
        public int size() {
            int n = 0;
            Object o = this.head;
            while (true) {
                o = ((ReferenceEntry<K, V>)o).getNextExpirable();
                if (o == this.head) {
                    break;
                }
                ++n;
            }
            return n;
        }
    }
    
    abstract class HashIterator<E> implements Iterator<E>
    {
        Segment<K, V> currentSegment;
        AtomicReferenceArray<ReferenceEntry<K, V>> currentTable;
        MapMakerInternalMap$WriteThroughEntry lastReturned;
        ReferenceEntry<K, V> nextEntry;
        MapMakerInternalMap$WriteThroughEntry nextExternal;
        int nextSegmentIndex;
        int nextTableIndex;
        
        HashIterator() {
            this.nextSegmentIndex = MapMakerInternalMap.this.segments.length - 1;
            this.nextTableIndex = -1;
            this.advance();
        }
        
        final void advance() {
            this.nextExternal = null;
            if (this.nextInChain()) {
                return;
            }
            if (this.nextInTable()) {
                return;
            }
            while (this.nextSegmentIndex >= 0) {
                this.currentSegment = MapMakerInternalMap.this.segments[this.nextSegmentIndex--];
                if (this.currentSegment.count != 0) {
                    this.currentTable = this.currentSegment.table;
                    this.nextTableIndex = this.currentTable.length() - 1;
                    if (this.nextInTable()) {
                        return;
                    }
                    continue;
                }
            }
        }
        
        boolean advanceTo(final ReferenceEntry<K, V> referenceEntry) {
            try {
                final K key = referenceEntry.getKey();
                final Object liveValue = MapMakerInternalMap.this.getLiveValue((ReferenceEntry<K, Object>)referenceEntry);
                if (liveValue != null) {
                    this.nextExternal = new WriteThroughEntry(key, (V)liveValue);
                    return true;
                }
                return false;
            }
            finally {
                this.currentSegment.postReadCleanup();
            }
        }
        
        @Override
        public boolean hasNext() {
            return this.nextExternal != null;
        }
        
        com.google.common.collect.MapMakerInternalMap$com.google.common.collect.MapMakerInternalMap$WriteThroughEntry nextEntry() {
            if (this.nextExternal != null) {
                this.lastReturned = this.nextExternal;
                this.advance();
                return (com.google.common.collect.MapMakerInternalMap$com.google.common.collect.MapMakerInternalMap$WriteThroughEntry)this.lastReturned;
            }
            throw new NoSuchElementException();
        }
        
        boolean nextInChain() {
            if (this.nextEntry != null) {
                do {
                    this.nextEntry = this.nextEntry.getNext();
                    if (this.nextEntry != null) {
                        continue;
                    }
                    return false;
                } while (!this.advanceTo(this.nextEntry));
                return true;
            }
            return false;
        }
        
        boolean nextInTable() {
            while (this.nextTableIndex >= 0) {
                final ReferenceEntry nextEntry = (ReferenceEntry)this.currentTable.get(this.nextTableIndex--);
                this.nextEntry = (ReferenceEntry<K, V>)nextEntry;
                if (nextEntry != null && (this.advanceTo(this.nextEntry) || this.nextInChain())) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public void remove() {
            CollectPreconditions.checkRemove(this.lastReturned != null);
            MapMakerInternalMap.this.remove(this.lastReturned.getKey());
            this.lastReturned = null;
        }
    }
    
    final class KeyIterator extends MapMakerInternalMap$HashIterator<K>
    {
        public K next() {
            return this.nextEntry().getKey();
        }
    }
    
    final class KeySet extends AbstractSet<K>
    {
        @Override
        public void clear() {
            MapMakerInternalMap.this.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return MapMakerInternalMap.this.containsKey(o);
        }
        
        @Override
        public boolean isEmpty() {
            return MapMakerInternalMap.this.isEmpty();
        }
        
        @Override
        public Iterator<K> iterator() {
            return (Iterator<K>)new KeyIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            return MapMakerInternalMap.this.remove(o) != null;
        }
        
        @Override
        public int size() {
            return MapMakerInternalMap.this.size();
        }
    }
    
    private enum NullEntry implements ReferenceEntry<Object, Object>
    {
        INSTANCE;
        
        @Override
        public long getExpirationTime() {
            return 0L;
        }
        
        @Override
        public int getHash() {
            return 0;
        }
        
        @Override
        public Object getKey() {
            return null;
        }
        
        @Override
        public ReferenceEntry<Object, Object> getNext() {
            return null;
        }
        
        @Override
        public ReferenceEntry<Object, Object> getNextEvictable() {
            return this;
        }
        
        @Override
        public ReferenceEntry<Object, Object> getNextExpirable() {
            return this;
        }
        
        @Override
        public ReferenceEntry<Object, Object> getPreviousEvictable() {
            return this;
        }
        
        @Override
        public ReferenceEntry<Object, Object> getPreviousExpirable() {
            return this;
        }
        
        @Override
        public ValueReference<Object, Object> getValueReference() {
            return null;
        }
        
        @Override
        public void setExpirationTime(final long n) {
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<Object, Object> referenceEntry) {
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<Object, Object> referenceEntry) {
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<Object, Object> referenceEntry) {
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<Object, Object> referenceEntry) {
        }
        
        @Override
        public void setValueReference(final ValueReference<Object, Object> valueReference) {
        }
    }
    
    interface ReferenceEntry<K, V>
    {
        long getExpirationTime();
        
        int getHash();
        
        K getKey();
        
        ReferenceEntry<K, V> getNext();
        
        ReferenceEntry<K, V> getNextEvictable();
        
        ReferenceEntry<K, V> getNextExpirable();
        
        ReferenceEntry<K, V> getPreviousEvictable();
        
        ReferenceEntry<K, V> getPreviousExpirable();
        
        ValueReference<K, V> getValueReference();
        
        void setExpirationTime(final long p0);
        
        void setNextEvictable(final ReferenceEntry<K, V> p0);
        
        void setNextExpirable(final ReferenceEntry<K, V> p0);
        
        void setPreviousEvictable(final ReferenceEntry<K, V> p0);
        
        void setPreviousExpirable(final ReferenceEntry<K, V> p0);
        
        void setValueReference(final ValueReference<K, V> p0);
    }
    
    static class Segment<K, V> extends ReentrantLock
    {
        volatile int count;
        final Queue<ReferenceEntry<K, V>> evictionQueue;
        final Queue<ReferenceEntry<K, V>> expirationQueue;
        final ReferenceQueue<K> keyReferenceQueue;
        final MapMakerInternalMap<K, V> map;
        final int maxSegmentSize;
        int modCount;
        final AtomicInteger readCount;
        final Queue<ReferenceEntry<K, V>> recencyQueue;
        volatile AtomicReferenceArray<ReferenceEntry<K, V>> table;
        int threshold;
        final ReferenceQueue<V> valueReferenceQueue;
        
        Segment(final MapMakerInternalMap<K, V> map, final int n, final int maxSegmentSize) {
            this.readCount = new AtomicInteger();
            this.map = map;
            this.maxSegmentSize = maxSegmentSize;
            this.initTable(this.newEntryArray(n));
            final boolean usesKeyReferences = map.usesKeyReferences();
            final ReferenceQueue<V> referenceQueue = null;
            ReferenceQueue<K> keyReferenceQueue;
            if (usesKeyReferences) {
                keyReferenceQueue = new ReferenceQueue<K>();
            }
            else {
                keyReferenceQueue = null;
            }
            this.keyReferenceQueue = keyReferenceQueue;
            ReferenceQueue<V> valueReferenceQueue = referenceQueue;
            if (map.usesValueReferences()) {
                valueReferenceQueue = new ReferenceQueue<V>();
            }
            this.valueReferenceQueue = valueReferenceQueue;
            Queue<ReferenceEntry<K, V>> discardingQueue;
            if (!map.evictsBySize() && !map.expiresAfterAccess()) {
                discardingQueue = MapMakerInternalMap.discardingQueue();
            }
            else {
                discardingQueue = new ConcurrentLinkedQueue<ReferenceEntry<K, V>>();
            }
            this.recencyQueue = discardingQueue;
            Queue<ReferenceEntry<K, V>> discardingQueue2;
            if (map.evictsBySize()) {
                discardingQueue2 = (Queue<ReferenceEntry<K, V>>)new EvictionQueue();
            }
            else {
                discardingQueue2 = MapMakerInternalMap.discardingQueue();
            }
            this.evictionQueue = discardingQueue2;
            Queue<ReferenceEntry<K, V>> discardingQueue3;
            if (map.expires()) {
                discardingQueue3 = (Queue<ReferenceEntry<K, V>>)new ExpirationQueue();
            }
            else {
                discardingQueue3 = MapMakerInternalMap.discardingQueue();
            }
            this.expirationQueue = discardingQueue3;
        }
        
        void clear() {
            if (this.count != 0) {
                this.lock();
                try {
                    final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                    if (this.map.removalNotificationQueue != MapMakerInternalMap.DISCARDING_QUEUE) {
                        for (int i = 0; i < table.length(); ++i) {
                            for (ReferenceEntry<K, V> next = table.get(i); next != null; next = next.getNext()) {
                                if (!next.getValueReference().isComputingReference()) {
                                    this.enqueueNotification(next, MapMaker.RemovalCause.EXPLICIT);
                                }
                            }
                        }
                    }
                    for (int j = 0; j < table.length(); ++j) {
                        table.set(j, (ReferenceEntry<K, V>)null);
                    }
                    this.clearReferenceQueues();
                    this.evictionQueue.clear();
                    this.expirationQueue.clear();
                    this.readCount.set(0);
                    ++this.modCount;
                    this.count = 0;
                }
                finally {
                    this.unlock();
                    this.postWriteCleanup();
                }
            }
        }
        
        void clearKeyReferenceQueue() {
            while (this.keyReferenceQueue.poll() != null) {}
        }
        
        void clearReferenceQueues() {
            if (this.map.usesKeyReferences()) {
                this.clearKeyReferenceQueue();
            }
            if (this.map.usesValueReferences()) {
                this.clearValueReferenceQueue();
            }
        }
        
        boolean clearValue(final K k, final int n, final ValueReference<K, V> valueReference) {
            this.lock();
            try {
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                final int n2 = table.length() - 1 & n;
                ReferenceEntry<K, V> next;
                final ReferenceEntry referenceEntry = next = (ReferenceEntry<K, V>)(ReferenceEntry)table.get(n2);
                while (next != null) {
                    final K key = next.getKey();
                    if (next.getHash() == n && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (next.getValueReference() == valueReference) {
                            table.set(n2, this.removeFromChain(referenceEntry, next));
                            return true;
                        }
                        return false;
                    }
                    else {
                        next = next.getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        void clearValueReferenceQueue() {
            while (this.valueReferenceQueue.poll() != null) {}
        }
        
        boolean containsKey(Object value, final int n) {
            try {
                final int count = this.count;
                boolean b = false;
                if (count == 0) {
                    return false;
                }
                final ReferenceEntry<K, Object> liveEntry = this.getLiveEntry(value, n);
                if (liveEntry == null) {
                    return false;
                }
                value = liveEntry.getValueReference().get();
                if (value != null) {
                    b = true;
                }
                return b;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        boolean containsValue(final Object o) {
            try {
                if (this.count != 0) {
                    final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                    for (int length = table.length(), i = 0; i < length; ++i) {
                        for (ReferenceEntry<K, Object> next = (ReferenceEntry<K, Object>)table.get(i); next != null; next = next.getNext()) {
                            final Object liveValue = this.getLiveValue(next);
                            if (liveValue != null) {
                                if (this.map.valueEquivalence.equivalent(o, liveValue)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        ReferenceEntry<K, V> copyEntry(final ReferenceEntry<K, V> referenceEntry, final ReferenceEntry<K, V> referenceEntry2) {
            if (referenceEntry.getKey() == null) {
                return null;
            }
            final ValueReference<K, V> valueReference = referenceEntry.getValueReference();
            final V value = valueReference.get();
            if (value == null && !valueReference.isComputingReference()) {
                return null;
            }
            final ReferenceEntry<K, V> copyEntry = this.map.entryFactory.copyEntry(this, referenceEntry, referenceEntry2);
            copyEntry.setValueReference((ValueReference<K, V>)valueReference.copyFor((ReferenceQueue<V>)this.valueReferenceQueue, value, (ReferenceEntry<K, V>)copyEntry));
            return copyEntry;
        }
        
        void drainKeyReferenceQueue() {
            int n = 0;
            do {
                final Reference<? extends K> poll = this.keyReferenceQueue.poll();
                if (poll == null) {
                    break;
                }
                this.map.reclaimKey((ReferenceEntry<K, V>)poll);
            } while (++n != 16);
        }
        
        void drainRecencyQueue() {
            while (true) {
                final ReferenceEntry referenceEntry = this.recencyQueue.poll();
                if (referenceEntry == null) {
                    break;
                }
                if (this.evictionQueue.contains(referenceEntry)) {
                    this.evictionQueue.add(referenceEntry);
                }
                if (!this.map.expiresAfterAccess() || !this.expirationQueue.contains(referenceEntry)) {
                    continue;
                }
                this.expirationQueue.add(referenceEntry);
            }
        }
        
        void drainReferenceQueues() {
            if (this.map.usesKeyReferences()) {
                this.drainKeyReferenceQueue();
            }
            if (this.map.usesValueReferences()) {
                this.drainValueReferenceQueue();
            }
        }
        
        void drainValueReferenceQueue() {
            int n = 0;
            do {
                final Reference<? extends V> poll = this.valueReferenceQueue.poll();
                if (poll == null) {
                    break;
                }
                this.map.reclaimValue((ValueReference<K, V>)poll);
            } while (++n != 16);
        }
        
        void enqueueNotification(final ReferenceEntry<K, V> referenceEntry, final MapMaker.RemovalCause removalCause) {
            this.enqueueNotification(referenceEntry.getKey(), referenceEntry.getHash(), referenceEntry.getValueReference().get(), removalCause);
        }
        
        void enqueueNotification(final K k, final int n, final V v, final MapMaker.RemovalCause removalCause) {
            if (this.map.removalNotificationQueue != MapMakerInternalMap.DISCARDING_QUEUE) {
                this.map.removalNotificationQueue.offer((MapMaker.RemovalNotification<K, V>)new MapMaker.RemovalNotification(k, v, removalCause));
            }
        }
        
        boolean evictEntries() {
            if (!this.map.evictsBySize() || this.count < this.maxSegmentSize) {
                return false;
            }
            this.drainRecencyQueue();
            final ReferenceEntry referenceEntry = this.evictionQueue.remove();
            if (this.removeEntry(referenceEntry, referenceEntry.getHash(), MapMaker.RemovalCause.SIZE)) {
                return true;
            }
            throw new AssertionError();
        }
        
        void expand() {
            final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            final int length = table.length();
            if (length >= 1073741824) {
                return;
            }
            int count = this.count;
            final AtomicReferenceArray<ReferenceEntry<K, V>> entryArray = this.newEntryArray(length << 1);
            this.threshold = entryArray.length() * 3 / 4;
            final int n = entryArray.length() - 1;
            int n2;
            for (int i = 0; i < length; ++i, count = n2) {
                final ReferenceEntry<K, V> referenceEntry = table.get(i);
                n2 = count;
                if (referenceEntry != null) {
                    ReferenceEntry<K, V> referenceEntry2 = referenceEntry.getNext();
                    int n3 = referenceEntry.getHash() & n;
                    if (referenceEntry2 == null) {
                        entryArray.set(n3, referenceEntry);
                        n2 = count;
                    }
                    else {
                        ReferenceEntry<K, V> referenceEntry3 = referenceEntry;
                        while (referenceEntry2 != null) {
                            final int n4 = referenceEntry2.getHash() & n;
                            int n5;
                            if (n4 != (n5 = n3)) {
                                n5 = n4;
                                referenceEntry3 = referenceEntry2;
                            }
                            referenceEntry2 = referenceEntry2.getNext();
                            n3 = n5;
                        }
                        entryArray.set(n3, referenceEntry3);
                        for (ReferenceEntry<K, V> next = referenceEntry; next != referenceEntry3; next = next.getNext()) {
                            final int n6 = next.getHash() & n;
                            final ReferenceEntry<K, V> copyEntry = this.copyEntry(next, (ReferenceEntry<K, V>)entryArray.get(n6));
                            if (copyEntry != null) {
                                entryArray.set(n6, copyEntry);
                            }
                            else {
                                this.removeCollectedEntry(next);
                                --count;
                            }
                        }
                        n2 = count;
                    }
                }
            }
            this.table = entryArray;
            this.count = count;
        }
        
        void expireEntries() {
            this.drainRecencyQueue();
            if (this.expirationQueue.isEmpty()) {
                return;
            }
            final long read = this.map.ticker.read();
            while (true) {
                final ReferenceEntry referenceEntry = this.expirationQueue.peek();
                if (referenceEntry == null || !this.map.isExpired(referenceEntry, read)) {
                    return;
                }
                if (this.removeEntry(referenceEntry, referenceEntry.getHash(), MapMaker.RemovalCause.EXPIRED)) {
                    continue;
                }
                throw new AssertionError();
            }
        }
        
        V get(Object value, final int n) {
            try {
                final ReferenceEntry<K, V> liveEntry = this.getLiveEntry(value, n);
                if (liveEntry == null) {
                    return null;
                }
                value = liveEntry.getValueReference().get();
                if (value != null) {
                    this.recordRead(liveEntry);
                }
                else {
                    this.tryDrainReferenceQueues();
                }
                return (V)value;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        ReferenceEntry<K, V> getEntry(final Object o, final int n) {
            if (this.count != 0) {
                for (Object o2 = this.getFirst(n); o2 != null; o2 = ((ReferenceEntry<Object, V>)o2).getNext()) {
                    if (((ReferenceEntry)o2).getHash() == n) {
                        final Object key = ((ReferenceEntry<Object, V>)o2).getKey();
                        if (key == null) {
                            this.tryDrainReferenceQueues();
                        }
                        else if (this.map.keyEquivalence.equivalent(o, key)) {
                            return (ReferenceEntry<K, V>)o2;
                        }
                    }
                }
            }
            return null;
        }
        
        ReferenceEntry<K, V> getFirst(final int n) {
            final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            return (ReferenceEntry<K, V>)(ReferenceEntry)table.get(table.length() - 1 & n);
        }
        
        ReferenceEntry<K, V> getLiveEntry(final Object o, final int n) {
            final ReferenceEntry<K, V> entry = this.getEntry(o, n);
            if (entry == null) {
                return null;
            }
            if (this.map.expires() && this.map.isExpired(entry)) {
                this.tryExpireEntries();
                return null;
            }
            return entry;
        }
        
        V getLiveValue(final ReferenceEntry<K, V> referenceEntry) {
            if (referenceEntry.getKey() == null) {
                this.tryDrainReferenceQueues();
                return null;
            }
            final V value = referenceEntry.getValueReference().get();
            if (value == null) {
                this.tryDrainReferenceQueues();
                return null;
            }
            if (this.map.expires() && this.map.isExpired(referenceEntry)) {
                this.tryExpireEntries();
                return null;
            }
            return value;
        }
        
        void initTable(final AtomicReferenceArray<ReferenceEntry<K, V>> table) {
            this.threshold = table.length() * 3 / 4;
            if (this.threshold == this.maxSegmentSize) {
                ++this.threshold;
            }
            this.table = table;
        }
        
        boolean isCollected(final ValueReference<K, V> valueReference) {
            final boolean computingReference = valueReference.isComputingReference();
            boolean b = false;
            if (computingReference) {
                return false;
            }
            if (valueReference.get() == null) {
                b = true;
            }
            return b;
        }
        
        ReferenceEntry<K, V> newEntry(final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            return this.map.entryFactory.newEntry(this, k, n, referenceEntry);
        }
        
        AtomicReferenceArray<ReferenceEntry<K, V>> newEntryArray(final int n) {
            return new AtomicReferenceArray<ReferenceEntry<K, V>>(n);
        }
        
        void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 0x3F) == 0x0) {
                this.runCleanup();
            }
        }
        
        void postWriteCleanup() {
            this.runUnlockedCleanup();
        }
        
        void preWriteCleanup() {
            this.runLockedCleanup();
        }
        
        V put(final K k, final int n, final V v, final boolean b) {
            this.lock();
            try {
                this.preWriteCleanup();
                int count;
                if ((count = this.count + 1) > this.threshold) {
                    this.expand();
                    count = this.count + 1;
                }
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                final int n2 = table.length() - 1 & n;
                Object next;
                final ReferenceEntry<Object, V> referenceEntry = (ReferenceEntry<Object, V>)(next = table.get(n2));
                while (next != null) {
                    final K key = (K)((ReferenceEntry<K, V>)next).getKey();
                    if (((ReferenceEntry)next).getHash() == n && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        final ValueReference<K, V> valueReference = ((ReferenceEntry<K, V>)next).getValueReference();
                        final V value = valueReference.get();
                        if (value == null) {
                            ++this.modCount;
                            this.setValue((ReferenceEntry<K, V>)next, v);
                            if (!valueReference.isComputingReference()) {
                                this.enqueueNotification(k, n, value, MapMaker.RemovalCause.COLLECTED);
                                count = this.count;
                            }
                            else if (this.evictEntries()) {
                                count = this.count + 1;
                            }
                            this.count = count;
                            return null;
                        }
                        if (b) {
                            this.recordLockedRead((ReferenceEntry<K, V>)next);
                            return value;
                        }
                        ++this.modCount;
                        this.enqueueNotification(k, n, value, MapMaker.RemovalCause.REPLACED);
                        this.setValue((ReferenceEntry<K, V>)next, v);
                        return value;
                    }
                    else {
                        next = ((ReferenceEntry<K, V>)next).getNext();
                    }
                }
                ++this.modCount;
                final ReferenceEntry<Object, V> entry = this.newEntry((Object)k, n, referenceEntry);
                this.setValue((ReferenceEntry<K, V>)entry, v);
                table.set(n2, (ReferenceEntry<K, V>)entry);
                if (this.evictEntries()) {
                    count = this.count + 1;
                }
                this.count = count;
                return null;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        boolean reclaimKey(final ReferenceEntry<K, V> referenceEntry, int count) {
            this.lock();
            try {
                final int count2 = this.count;
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                final int n = table.length() - 1 & count;
                ReferenceEntry<K, V> next;
                for (ReferenceEntry<K, V> referenceEntry2 = next = table.get(n); next != null; next = next.getNext()) {
                    if (next == referenceEntry) {
                        ++this.modCount;
                        this.enqueueNotification(next.getKey(), count, next.getValueReference().get(), MapMaker.RemovalCause.COLLECTED);
                        final ReferenceEntry<K, V> removeFromChain = this.removeFromChain(referenceEntry2, next);
                        count = this.count;
                        table.set(n, removeFromChain);
                        this.count = count - 1;
                        return true;
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        boolean reclaimValue(final K k, int count, final ValueReference<K, V> valueReference) {
            this.lock();
            try {
                final int count2 = this.count;
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                final ReferenceEntry<Object, V> referenceEntry = (ReferenceEntry<Object, V>)(next = table.get(n));
                while (next != null) {
                    final Object key = ((ReferenceEntry<Object, V>)next).getKey();
                    if (((ReferenceEntry)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((ReferenceEntry<K, V>)next).getValueReference() == valueReference) {
                            ++this.modCount;
                            this.enqueueNotification(k, count, valueReference.get(), MapMaker.RemovalCause.COLLECTED);
                            final ReferenceEntry<Object, V> removeFromChain = this.removeFromChain(referenceEntry, (ReferenceEntry<Object, V>)next);
                            count = this.count;
                            table.set(n, (ReferenceEntry<K, V>)removeFromChain);
                            this.count = count - 1;
                            return true;
                        }
                        return false;
                    }
                    else {
                        next = ((ReferenceEntry<K, V>)next).getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                if (!this.isHeldByCurrentThread()) {
                    this.postWriteCleanup();
                }
            }
        }
        
        void recordExpirationTime(final ReferenceEntry<K, V> referenceEntry, final long n) {
            referenceEntry.setExpirationTime(this.map.ticker.read() + n);
        }
        
        void recordLockedRead(final ReferenceEntry<K, V> referenceEntry) {
            this.evictionQueue.add(referenceEntry);
            if (this.map.expiresAfterAccess()) {
                this.recordExpirationTime(referenceEntry, this.map.expireAfterAccessNanos);
                this.expirationQueue.add(referenceEntry);
            }
        }
        
        void recordRead(final ReferenceEntry<K, V> referenceEntry) {
            if (this.map.expiresAfterAccess()) {
                this.recordExpirationTime(referenceEntry, this.map.expireAfterAccessNanos);
            }
            this.recencyQueue.add(referenceEntry);
        }
        
        void recordWrite(final ReferenceEntry<K, V> referenceEntry) {
            this.drainRecencyQueue();
            this.evictionQueue.add(referenceEntry);
            if (this.map.expires()) {
                long n;
                if (this.map.expiresAfterAccess()) {
                    n = this.map.expireAfterAccessNanos;
                }
                else {
                    n = this.map.expireAfterWriteNanos;
                }
                this.recordExpirationTime(referenceEntry, n);
                this.expirationQueue.add(referenceEntry);
            }
        }
        
        V remove(final Object o, int count) {
            this.lock();
            try {
                this.preWriteCleanup();
                final int count2 = this.count;
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                for (ReferenceEntry<Object, V> referenceEntry = (ReferenceEntry<Object, V>)(next = table.get(n)); next != null; next = ((ReferenceEntry<Object, V>)next).getNext()) {
                    final K key = ((ReferenceEntry<K, V>)next).getKey();
                    if (((ReferenceEntry)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(o, key)) {
                        final ValueReference<K, V> valueReference = ((ReferenceEntry<K, V>)next).getValueReference();
                        final V value = valueReference.get();
                        MapMaker.RemovalCause removalCause;
                        if (value != null) {
                            removalCause = MapMaker.RemovalCause.EXPLICIT;
                        }
                        else {
                            if (!this.isCollected(valueReference)) {
                                return null;
                            }
                            removalCause = MapMaker.RemovalCause.COLLECTED;
                        }
                        ++this.modCount;
                        this.enqueueNotification(key, count, value, removalCause);
                        final ReferenceEntry<Object, V> removeFromChain = this.removeFromChain(referenceEntry, (ReferenceEntry<Object, V>)next);
                        count = this.count;
                        table.set(n, (ReferenceEntry<K, V>)removeFromChain);
                        this.count = count - 1;
                        return value;
                    }
                }
                return null;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        boolean remove(final Object p0, final int p1, final Object p2) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     4: aload_0        
            //     5: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.preWriteCleanup:()V
            //     8: aload_0        
            //     9: getfield        com/google/common/collect/MapMakerInternalMap$Segment.count:I
            //    12: istore          4
            //    14: aload_0        
            //    15: getfield        com/google/common/collect/MapMakerInternalMap$Segment.table:Ljava/util/concurrent/atomic/AtomicReferenceArray;
            //    18: astore          5
            //    20: aload           5
            //    22: invokevirtual   java/util/concurrent/atomic/AtomicReferenceArray.length:()I
            //    25: iconst_1       
            //    26: isub           
            //    27: iload_2        
            //    28: iand           
            //    29: istore          4
            //    31: aload           5
            //    33: iload           4
            //    35: invokevirtual   java/util/concurrent/atomic/AtomicReferenceArray.get:(I)Ljava/lang/Object;
            //    38: checkcast       Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
            //    41: astore          6
            //    43: aload           6
            //    45: astore          7
            //    47: aload           7
            //    49: ifnull          264
            //    52: aload           7
            //    54: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getKey:()Ljava/lang/Object;
            //    59: astore          8
            //    61: aload           7
            //    63: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getHash:()I
            //    68: iload_2        
            //    69: if_icmpne       248
            //    72: aload           8
            //    74: ifnull          248
            //    77: aload_0        
            //    78: getfield        com/google/common/collect/MapMakerInternalMap$Segment.map:Lcom/google/common/collect/MapMakerInternalMap;
            //    81: getfield        com/google/common/collect/MapMakerInternalMap.keyEquivalence:Lcom/google/common/base/Equivalence;
            //    84: astore          9
            //    86: aload           9
            //    88: aload_1        
            //    89: aload           8
            //    91: invokevirtual   com/google/common/base/Equivalence.equivalent:(Ljava/lang/Object;Ljava/lang/Object;)Z
            //    94: ifeq            248
            //    97: aload           7
            //    99: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getValueReference:()Lcom/google/common/collect/MapMakerInternalMap$ValueReference;
            //   104: astore          10
            //   106: aload           10
            //   108: invokeinterface com/google/common/collect/MapMakerInternalMap$ValueReference.get:()Ljava/lang/Object;
            //   113: astore          9
            //   115: aload_0        
            //   116: getfield        com/google/common/collect/MapMakerInternalMap$Segment.map:Lcom/google/common/collect/MapMakerInternalMap;
            //   119: getfield        com/google/common/collect/MapMakerInternalMap.valueEquivalence:Lcom/google/common/base/Equivalence;
            //   122: astore_1       
            //   123: aload_1        
            //   124: aload_3        
            //   125: aload           9
            //   127: invokevirtual   com/google/common/base/Equivalence.equivalent:(Ljava/lang/Object;Ljava/lang/Object;)Z
            //   130: ifeq            140
            //   133: getstatic       com/google/common/collect/MapMaker$RemovalCause.EXPLICIT:Lcom/google/common/collect/MapMaker$RemovalCause;
            //   136: astore_1       
            //   137: goto            156
            //   140: aload_0        
            //   141: aload           10
            //   143: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.isCollected:(Lcom/google/common/collect/MapMakerInternalMap$ValueReference;)Z
            //   146: ifeq            234
            //   149: getstatic       com/google/common/collect/MapMaker$RemovalCause.COLLECTED:Lcom/google/common/collect/MapMaker$RemovalCause;
            //   152: astore_1       
            //   153: goto            137
            //   156: aload_0        
            //   157: aload_0        
            //   158: getfield        com/google/common/collect/MapMakerInternalMap$Segment.modCount:I
            //   161: iconst_1       
            //   162: iadd           
            //   163: putfield        com/google/common/collect/MapMakerInternalMap$Segment.modCount:I
            //   166: aload_0        
            //   167: aload           8
            //   169: iload_2        
            //   170: aload           9
            //   172: aload_1        
            //   173: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.enqueueNotification:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/common/collect/MapMaker$RemovalCause;)V
            //   176: aload_0        
            //   177: aload           6
            //   179: aload           7
            //   181: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.removeFromChain:(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
            //   184: astore_3       
            //   185: aload_0        
            //   186: getfield        com/google/common/collect/MapMakerInternalMap$Segment.count:I
            //   189: istore_2       
            //   190: aload           5
            //   192: iload           4
            //   194: aload_3        
            //   195: invokevirtual   java/util/concurrent/atomic/AtomicReferenceArray.set:(ILjava/lang/Object;)V
            //   198: aload_0        
            //   199: iload_2        
            //   200: iconst_1       
            //   201: isub           
            //   202: putfield        com/google/common/collect/MapMakerInternalMap$Segment.count:I
            //   205: getstatic       com/google/common/collect/MapMaker$RemovalCause.EXPLICIT:Lcom/google/common/collect/MapMaker$RemovalCause;
            //   208: astore_3       
            //   209: aload_1        
            //   210: aload_3        
            //   211: if_acmpne       220
            //   214: iconst_1       
            //   215: istore          11
            //   217: goto            223
            //   220: iconst_0       
            //   221: istore          11
            //   223: aload_0        
            //   224: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   227: aload_0        
            //   228: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   231: iload           11
            //   233: ireturn        
            //   234: aload_0        
            //   235: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   238: aload_0        
            //   239: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   242: iconst_0       
            //   243: ireturn        
            //   244: astore_1       
            //   245: goto            275
            //   248: aload           7
            //   250: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getNext:()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
            //   255: astore          7
            //   257: goto            47
            //   260: astore_1       
            //   261: goto            275
            //   264: aload_0        
            //   265: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   268: aload_0        
            //   269: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   272: iconst_0       
            //   273: ireturn        
            //   274: astore_1       
            //   275: aload_0        
            //   276: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   279: aload_0        
            //   280: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   283: aload_1        
            //   284: athrow         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type
            //  -----  -----  -----  -----  ----
            //  4      43     274    275    Any
            //  52     72     274    275    Any
            //  77     86     274    275    Any
            //  86     123    244    248    Any
            //  123    137    260    264    Any
            //  140    153    260    264    Any
            //  156    209    260    264    Any
            //  248    257    260    264    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0137:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Thread.java:745)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        void removeCollectedEntry(final ReferenceEntry<K, V> referenceEntry) {
            this.enqueueNotification(referenceEntry, MapMaker.RemovalCause.COLLECTED);
            this.evictionQueue.remove(referenceEntry);
            this.expirationQueue.remove(referenceEntry);
        }
        
        boolean removeEntry(final ReferenceEntry<K, V> referenceEntry, int count, final MapMaker.RemovalCause removalCause) {
            final int count2 = this.count;
            final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
            final int n = table.length() - 1 & count;
            ReferenceEntry<K, V> next;
            for (ReferenceEntry<K, V> referenceEntry2 = next = table.get(n); next != null; next = next.getNext()) {
                if (next == referenceEntry) {
                    ++this.modCount;
                    this.enqueueNotification(next.getKey(), count, next.getValueReference().get(), removalCause);
                    final ReferenceEntry<K, V> removeFromChain = this.removeFromChain(referenceEntry2, next);
                    count = this.count;
                    table.set(n, removeFromChain);
                    this.count = count - 1;
                    return true;
                }
            }
            return false;
        }
        
        ReferenceEntry<K, V> removeFromChain(ReferenceEntry<K, V> next, final ReferenceEntry<K, V> referenceEntry) {
            this.evictionQueue.remove(referenceEntry);
            this.expirationQueue.remove(referenceEntry);
            int count = this.count;
            Object next2 = referenceEntry.getNext();
            while (next != referenceEntry) {
                final ReferenceEntry<K, V> copyEntry = this.copyEntry(next, (ReferenceEntry<K, V>)next2);
                if (copyEntry != null) {
                    next2 = copyEntry;
                }
                else {
                    this.removeCollectedEntry(next);
                    --count;
                }
                next = next.getNext();
            }
            this.count = count;
            return (ReferenceEntry<K, V>)next2;
        }
        
        V replace(final K k, int count, final V v) {
            this.lock();
            try {
                this.preWriteCleanup();
                final AtomicReferenceArray<ReferenceEntry<K, V>> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                final ReferenceEntry<Object, V> referenceEntry = (ReferenceEntry<Object, V>)(next = table.get(n));
                while (next != null) {
                    final K key = ((ReferenceEntry<K, V>)next).getKey();
                    if (((ReferenceEntry)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        final ValueReference<K, V> valueReference = ((ReferenceEntry<K, V>)next).getValueReference();
                        final V value = valueReference.get();
                        if (value == null) {
                            if (this.isCollected(valueReference)) {
                                final int count2 = this.count;
                                ++this.modCount;
                                this.enqueueNotification(key, count, value, MapMaker.RemovalCause.COLLECTED);
                                final ReferenceEntry<Object, V> removeFromChain = this.removeFromChain(referenceEntry, (ReferenceEntry<Object, V>)next);
                                count = this.count;
                                table.set(n, (ReferenceEntry<K, V>)removeFromChain);
                                this.count = count - 1;
                            }
                            return null;
                        }
                        ++this.modCount;
                        this.enqueueNotification(k, count, value, MapMaker.RemovalCause.REPLACED);
                        this.setValue((ReferenceEntry<K, V>)next, v);
                        return value;
                    }
                    else {
                        next = ((ReferenceEntry<K, V>)next).getNext();
                    }
                }
                return null;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        boolean replace(final K p0, final int p1, final V p2, final V p3) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     4: aload_0        
            //     5: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.preWriteCleanup:()V
            //     8: aload_0        
            //     9: getfield        com/google/common/collect/MapMakerInternalMap$Segment.table:Ljava/util/concurrent/atomic/AtomicReferenceArray;
            //    12: astore          5
            //    14: aload           5
            //    16: invokevirtual   java/util/concurrent/atomic/AtomicReferenceArray.length:()I
            //    19: iconst_1       
            //    20: isub           
            //    21: iload_2        
            //    22: iand           
            //    23: istore          6
            //    25: aload           5
            //    27: iload           6
            //    29: invokevirtual   java/util/concurrent/atomic/AtomicReferenceArray.get:(I)Ljava/lang/Object;
            //    32: checkcast       Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
            //    35: astore          7
            //    37: aload           7
            //    39: astore          8
            //    41: aload           8
            //    43: ifnull          281
            //    46: aload           8
            //    48: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getKey:()Ljava/lang/Object;
            //    53: astore          9
            //    55: aload           8
            //    57: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getHash:()I
            //    62: iload_2        
            //    63: if_icmpne       265
            //    66: aload           9
            //    68: ifnull          265
            //    71: aload_0        
            //    72: getfield        com/google/common/collect/MapMakerInternalMap$Segment.map:Lcom/google/common/collect/MapMakerInternalMap;
            //    75: getfield        com/google/common/collect/MapMakerInternalMap.keyEquivalence:Lcom/google/common/base/Equivalence;
            //    78: aload_1        
            //    79: aload           9
            //    81: invokevirtual   com/google/common/base/Equivalence.equivalent:(Ljava/lang/Object;Ljava/lang/Object;)Z
            //    84: ifeq            265
            //    87: aload           8
            //    89: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getValueReference:()Lcom/google/common/collect/MapMakerInternalMap$ValueReference;
            //    94: astore          10
            //    96: aload           10
            //    98: invokeinterface com/google/common/collect/MapMakerInternalMap$ValueReference.get:()Ljava/lang/Object;
            //   103: astore          11
            //   105: aload           11
            //   107: ifnonnull       186
            //   110: aload_0        
            //   111: aload           10
            //   113: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.isCollected:(Lcom/google/common/collect/MapMakerInternalMap$ValueReference;)Z
            //   116: ifeq            176
            //   119: aload_0        
            //   120: getfield        com/google/common/collect/MapMakerInternalMap$Segment.count:I
            //   123: istore          12
            //   125: aload_0        
            //   126: aload_0        
            //   127: getfield        com/google/common/collect/MapMakerInternalMap$Segment.modCount:I
            //   130: iconst_1       
            //   131: iadd           
            //   132: putfield        com/google/common/collect/MapMakerInternalMap$Segment.modCount:I
            //   135: aload_0        
            //   136: aload           9
            //   138: iload_2        
            //   139: aload           11
            //   141: getstatic       com/google/common/collect/MapMaker$RemovalCause.COLLECTED:Lcom/google/common/collect/MapMaker$RemovalCause;
            //   144: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.enqueueNotification:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/common/collect/MapMaker$RemovalCause;)V
            //   147: aload_0        
            //   148: aload           7
            //   150: aload           8
            //   152: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.removeFromChain:(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
            //   155: astore_1       
            //   156: aload_0        
            //   157: getfield        com/google/common/collect/MapMakerInternalMap$Segment.count:I
            //   160: istore_2       
            //   161: aload           5
            //   163: iload           6
            //   165: aload_1        
            //   166: invokevirtual   java/util/concurrent/atomic/AtomicReferenceArray.set:(ILjava/lang/Object;)V
            //   169: aload_0        
            //   170: iload_2        
            //   171: iconst_1       
            //   172: isub           
            //   173: putfield        com/google/common/collect/MapMakerInternalMap$Segment.count:I
            //   176: aload_0        
            //   177: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   180: aload_0        
            //   181: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   184: iconst_0       
            //   185: ireturn        
            //   186: aload_0        
            //   187: getfield        com/google/common/collect/MapMakerInternalMap$Segment.map:Lcom/google/common/collect/MapMakerInternalMap;
            //   190: getfield        com/google/common/collect/MapMakerInternalMap.valueEquivalence:Lcom/google/common/base/Equivalence;
            //   193: astore          7
            //   195: aload           7
            //   197: aload_3        
            //   198: aload           11
            //   200: invokevirtual   com/google/common/base/Equivalence.equivalent:(Ljava/lang/Object;Ljava/lang/Object;)Z
            //   203: ifeq            245
            //   206: aload_0        
            //   207: aload_0        
            //   208: getfield        com/google/common/collect/MapMakerInternalMap$Segment.modCount:I
            //   211: iconst_1       
            //   212: iadd           
            //   213: putfield        com/google/common/collect/MapMakerInternalMap$Segment.modCount:I
            //   216: aload_0        
            //   217: aload_1        
            //   218: iload_2        
            //   219: aload           11
            //   221: getstatic       com/google/common/collect/MapMaker$RemovalCause.REPLACED:Lcom/google/common/collect/MapMaker$RemovalCause;
            //   224: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.enqueueNotification:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/common/collect/MapMaker$RemovalCause;)V
            //   227: aload_0        
            //   228: aload           8
            //   230: aload           4
            //   232: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.setValue:(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Ljava/lang/Object;)V
            //   235: aload_0        
            //   236: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   239: aload_0        
            //   240: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   243: iconst_1       
            //   244: ireturn        
            //   245: aload_0        
            //   246: aload           8
            //   248: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.recordLockedRead:(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
            //   251: aload_0        
            //   252: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   255: aload_0        
            //   256: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   259: iconst_0       
            //   260: ireturn        
            //   261: astore_1       
            //   262: goto            292
            //   265: aload           8
            //   267: invokeinterface com/google/common/collect/MapMakerInternalMap$ReferenceEntry.getNext:()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
            //   272: astore          8
            //   274: goto            41
            //   277: astore_1       
            //   278: goto            292
            //   281: aload_0        
            //   282: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   285: aload_0        
            //   286: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   289: iconst_0       
            //   290: ireturn        
            //   291: astore_1       
            //   292: aload_0        
            //   293: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.unlock:()V
            //   296: aload_0        
            //   297: invokevirtual   com/google/common/collect/MapMakerInternalMap$Segment.postWriteCleanup:()V
            //   300: aload_1        
            //   301: athrow         
            //    Signature:
            //  (TK;ITV;TV;)Z
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type
            //  -----  -----  -----  -----  ----
            //  4      37     291    292    Any
            //  46     66     291    292    Any
            //  71     105    291    292    Any
            //  110    176    291    292    Any
            //  186    195    291    292    Any
            //  195    227    261    265    Any
            //  227    235    277    281    Any
            //  245    251    277    281    Any
            //  265    274    277    281    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0245:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Thread.java:745)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        void runCleanup() {
            this.runLockedCleanup();
            this.runUnlockedCleanup();
        }
        
        void runLockedCleanup() {
            if (this.tryLock()) {
                try {
                    this.drainReferenceQueues();
                    this.expireEntries();
                    this.readCount.set(0);
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        void runUnlockedCleanup() {
            if (!this.isHeldByCurrentThread()) {
                this.map.processPendingNotifications();
            }
        }
        
        void setValue(final ReferenceEntry<K, V> referenceEntry, final V v) {
            referenceEntry.setValueReference(this.map.valueStrength.referenceValue(this, referenceEntry, v));
            this.recordWrite(referenceEntry);
        }
        
        void tryDrainReferenceQueues() {
            if (this.tryLock()) {
                try {
                    this.drainReferenceQueues();
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        void tryExpireEntries() {
            if (this.tryLock()) {
                try {
                    this.expireEntries();
                }
                finally {
                    this.unlock();
                }
            }
        }
    }
    
    private static final class SerializationProxy<K, V> extends AbstractSerializationProxy<K, V>
    {
        private static final long serialVersionUID = 3L;
        
        SerializationProxy(final Strength strength, final Strength strength2, final Equivalence<Object> equivalence, final Equivalence<Object> equivalence2, final long n, final long n2, final int n3, final int n4, final MapMaker.RemovalListener<? super K, ? super V> removalListener, final ConcurrentMap<K, V> concurrentMap) {
            super(strength, strength2, equivalence, equivalence2, n, n2, n3, n4, removalListener, concurrentMap);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.delegate = (ConcurrentMap<K, V>)this.readMapMaker(objectInputStream).makeMap();
            this.readEntries(objectInputStream);
        }
        
        private Object readResolve() {
            return this.delegate;
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            this.writeMapTo(objectOutputStream);
        }
    }
    
    static final class SoftValueReference<K, V> extends SoftReference<V> implements ValueReference<K, V>
    {
        final ReferenceEntry<K, V> entry;
        
        SoftValueReference(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> entry) {
            super(v, referenceQueue);
            this.entry = entry;
        }
        
        @Override
        public void clear(final ValueReference<K, V> valueReference) {
            this.clear();
        }
        
        @Override
        public ValueReference<K, V> copyFor(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> referenceEntry) {
            return new SoftValueReference((ReferenceQueue<Object>)referenceQueue, v, (ReferenceEntry<Object, Object>)referenceEntry);
        }
        
        @Override
        public ReferenceEntry<K, V> getEntry() {
            return this.entry;
        }
        
        @Override
        public boolean isComputingReference() {
            return false;
        }
        
        @Override
        public V waitForValue() {
            return this.get();
        }
    }
    
    enum Strength
    {
        SOFT(1) {
            @Override
            Equivalence<Object> defaultEquivalence() {
                return Equivalence.identity();
            }
            
            @Override
             <K, V> ValueReference<K, V> referenceValue(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final V v) {
                return new SoftValueReference<K, V>(segment.valueReferenceQueue, v, referenceEntry);
            }
        }, 
        STRONG(0) {
            @Override
            Equivalence<Object> defaultEquivalence() {
                return Equivalence.equals();
            }
            
            @Override
             <K, V> ValueReference<K, V> referenceValue(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final V v) {
                return new StrongValueReference<K, V>(v);
            }
        }, 
        WEAK(2) {
            @Override
            Equivalence<Object> defaultEquivalence() {
                return Equivalence.identity();
            }
            
            @Override
             <K, V> ValueReference<K, V> referenceValue(final Segment<K, V> segment, final ReferenceEntry<K, V> referenceEntry, final V v) {
                return new WeakValueReference<K, V>(segment.valueReferenceQueue, v, referenceEntry);
            }
        };
        
        abstract Equivalence<Object> defaultEquivalence();
        
        abstract <K, V> ValueReference<K, V> referenceValue(final Segment<K, V> p0, final ReferenceEntry<K, V> p1, final V p2);
    }
    
    static class StrongEntry<K, V> implements ReferenceEntry<K, V>
    {
        final int hash;
        final K key;
        final ReferenceEntry<K, V> next;
        volatile ValueReference<K, V> valueReference;
        
        StrongEntry(final K key, final int hash, final ReferenceEntry<K, V> next) {
            this.valueReference = MapMakerInternalMap.unset();
            this.key = key;
            this.hash = hash;
            this.next = next;
        }
        
        @Override
        public long getExpirationTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int getHash() {
            return this.hash;
        }
        
        @Override
        public K getKey() {
            return this.key;
        }
        
        @Override
        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }
        
        @Override
        public void setExpirationTime(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setValueReference(final ValueReference<K, V> valueReference) {
            this.valueReference.clear(this.valueReference = valueReference);
        }
    }
    
    static final class StrongEvictableEntry<K, V> extends StrongEntry<K, V> implements ReferenceEntry<K, V>
    {
        ReferenceEntry<K, V> nextEvictable;
        ReferenceEntry<K, V> previousEvictable;
        
        StrongEvictableEntry(final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            super(k, n, referenceEntry);
            this.nextEvictable = MapMakerInternalMap.nullEntry();
            this.previousEvictable = MapMakerInternalMap.nullEntry();
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            return this.nextEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            return this.previousEvictable;
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> nextEvictable) {
            this.nextEvictable = nextEvictable;
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> previousEvictable) {
            this.previousEvictable = previousEvictable;
        }
    }
    
    static final class StrongExpirableEntry<K, V> extends StrongEntry<K, V> implements ReferenceEntry<K, V>
    {
        ReferenceEntry<K, V> nextExpirable;
        ReferenceEntry<K, V> previousExpirable;
        volatile long time;
        
        StrongExpirableEntry(final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            super(k, n, referenceEntry);
            this.time = Long.MAX_VALUE;
            this.nextExpirable = MapMakerInternalMap.nullEntry();
            this.previousExpirable = MapMakerInternalMap.nullEntry();
        }
        
        @Override
        public long getExpirationTime() {
            return this.time;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            return this.nextExpirable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            return this.previousExpirable;
        }
        
        @Override
        public void setExpirationTime(final long time) {
            this.time = time;
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> nextExpirable) {
            this.nextExpirable = nextExpirable;
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> previousExpirable) {
            this.previousExpirable = previousExpirable;
        }
    }
    
    static final class StrongExpirableEvictableEntry<K, V> extends StrongEntry<K, V> implements ReferenceEntry<K, V>
    {
        ReferenceEntry<K, V> nextEvictable;
        ReferenceEntry<K, V> nextExpirable;
        ReferenceEntry<K, V> previousEvictable;
        ReferenceEntry<K, V> previousExpirable;
        volatile long time;
        
        StrongExpirableEvictableEntry(final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            super(k, n, referenceEntry);
            this.time = Long.MAX_VALUE;
            this.nextExpirable = MapMakerInternalMap.nullEntry();
            this.previousExpirable = MapMakerInternalMap.nullEntry();
            this.nextEvictable = MapMakerInternalMap.nullEntry();
            this.previousEvictable = MapMakerInternalMap.nullEntry();
        }
        
        @Override
        public long getExpirationTime() {
            return this.time;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            return this.nextEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            return this.nextExpirable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            return this.previousEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            return this.previousExpirable;
        }
        
        @Override
        public void setExpirationTime(final long time) {
            this.time = time;
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> nextEvictable) {
            this.nextEvictable = nextEvictable;
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> nextExpirable) {
            this.nextExpirable = nextExpirable;
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> previousEvictable) {
            this.previousEvictable = previousEvictable;
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> previousExpirable) {
            this.previousExpirable = previousExpirable;
        }
    }
    
    static final class StrongValueReference<K, V> implements ValueReference<K, V>
    {
        final V referent;
        
        StrongValueReference(final V referent) {
            this.referent = referent;
        }
        
        @Override
        public void clear(final ValueReference<K, V> valueReference) {
        }
        
        @Override
        public ValueReference<K, V> copyFor(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> referenceEntry) {
            return this;
        }
        
        @Override
        public V get() {
            return this.referent;
        }
        
        @Override
        public ReferenceEntry<K, V> getEntry() {
            return null;
        }
        
        @Override
        public boolean isComputingReference() {
            return false;
        }
        
        @Override
        public V waitForValue() {
            return this.get();
        }
    }
    
    final class ValueIterator extends MapMakerInternalMap$HashIterator<V>
    {
        public V next() {
            return this.nextEntry().getValue();
        }
    }
    
    interface ValueReference<K, V>
    {
        void clear(final ValueReference<K, V> p0);
        
        ValueReference<K, V> copyFor(final ReferenceQueue<V> p0, final V p1, final ReferenceEntry<K, V> p2);
        
        V get();
        
        ReferenceEntry<K, V> getEntry();
        
        boolean isComputingReference();
        
        V waitForValue() throws ExecutionException;
    }
    
    final class Values extends AbstractCollection<V>
    {
        @Override
        public void clear() {
            MapMakerInternalMap.this.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return MapMakerInternalMap.this.containsValue(o);
        }
        
        @Override
        public boolean isEmpty() {
            return MapMakerInternalMap.this.isEmpty();
        }
        
        @Override
        public Iterator<V> iterator() {
            return (Iterator<V>)new ValueIterator();
        }
        
        @Override
        public int size() {
            return MapMakerInternalMap.this.size();
        }
    }
    
    static class WeakEntry<K, V> extends WeakReference<K> implements ReferenceEntry<K, V>
    {
        final int hash;
        final ReferenceEntry<K, V> next;
        volatile ValueReference<K, V> valueReference;
        
        WeakEntry(final ReferenceQueue<K> referenceQueue, final K k, final int hash, final ReferenceEntry<K, V> next) {
            super(k, referenceQueue);
            this.valueReference = MapMakerInternalMap.unset();
            this.hash = hash;
            this.next = next;
        }
        
        @Override
        public long getExpirationTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int getHash() {
            return this.hash;
        }
        
        @Override
        public K getKey() {
            return this.get();
        }
        
        @Override
        public ReferenceEntry<K, V> getNext() {
            return this.next;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ValueReference<K, V> getValueReference() {
            return this.valueReference;
        }
        
        @Override
        public void setExpirationTime(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> referenceEntry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setValueReference(final ValueReference<K, V> valueReference) {
            this.valueReference.clear(this.valueReference = valueReference);
        }
    }
    
    static final class WeakEvictableEntry<K, V> extends WeakEntry<K, V> implements ReferenceEntry<K, V>
    {
        ReferenceEntry<K, V> nextEvictable;
        ReferenceEntry<K, V> previousEvictable;
        
        WeakEvictableEntry(final ReferenceQueue<K> referenceQueue, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            super(referenceQueue, k, n, referenceEntry);
            this.nextEvictable = MapMakerInternalMap.nullEntry();
            this.previousEvictable = MapMakerInternalMap.nullEntry();
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            return this.nextEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            return this.previousEvictable;
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> nextEvictable) {
            this.nextEvictable = nextEvictable;
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> previousEvictable) {
            this.previousEvictable = previousEvictable;
        }
    }
    
    static final class WeakExpirableEntry<K, V> extends WeakEntry<K, V> implements ReferenceEntry<K, V>
    {
        ReferenceEntry<K, V> nextExpirable;
        ReferenceEntry<K, V> previousExpirable;
        volatile long time;
        
        WeakExpirableEntry(final ReferenceQueue<K> referenceQueue, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            super(referenceQueue, k, n, referenceEntry);
            this.time = Long.MAX_VALUE;
            this.nextExpirable = MapMakerInternalMap.nullEntry();
            this.previousExpirable = MapMakerInternalMap.nullEntry();
        }
        
        @Override
        public long getExpirationTime() {
            return this.time;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            return this.nextExpirable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            return this.previousExpirable;
        }
        
        @Override
        public void setExpirationTime(final long time) {
            this.time = time;
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> nextExpirable) {
            this.nextExpirable = nextExpirable;
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> previousExpirable) {
            this.previousExpirable = previousExpirable;
        }
    }
    
    static final class WeakExpirableEvictableEntry<K, V> extends WeakEntry<K, V> implements ReferenceEntry<K, V>
    {
        ReferenceEntry<K, V> nextEvictable;
        ReferenceEntry<K, V> nextExpirable;
        ReferenceEntry<K, V> previousEvictable;
        ReferenceEntry<K, V> previousExpirable;
        volatile long time;
        
        WeakExpirableEvictableEntry(final ReferenceQueue<K> referenceQueue, final K k, final int n, final ReferenceEntry<K, V> referenceEntry) {
            super(referenceQueue, k, n, referenceEntry);
            this.time = Long.MAX_VALUE;
            this.nextExpirable = MapMakerInternalMap.nullEntry();
            this.previousExpirable = MapMakerInternalMap.nullEntry();
            this.nextEvictable = MapMakerInternalMap.nullEntry();
            this.previousEvictable = MapMakerInternalMap.nullEntry();
        }
        
        @Override
        public long getExpirationTime() {
            return this.time;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextEvictable() {
            return this.nextEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> getNextExpirable() {
            return this.nextExpirable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousEvictable() {
            return this.previousEvictable;
        }
        
        @Override
        public ReferenceEntry<K, V> getPreviousExpirable() {
            return this.previousExpirable;
        }
        
        @Override
        public void setExpirationTime(final long time) {
            this.time = time;
        }
        
        @Override
        public void setNextEvictable(final ReferenceEntry<K, V> nextEvictable) {
            this.nextEvictable = nextEvictable;
        }
        
        @Override
        public void setNextExpirable(final ReferenceEntry<K, V> nextExpirable) {
            this.nextExpirable = nextExpirable;
        }
        
        @Override
        public void setPreviousEvictable(final ReferenceEntry<K, V> previousEvictable) {
            this.previousEvictable = previousEvictable;
        }
        
        @Override
        public void setPreviousExpirable(final ReferenceEntry<K, V> previousExpirable) {
            this.previousExpirable = previousExpirable;
        }
    }
    
    static final class WeakValueReference<K, V> extends WeakReference<V> implements ValueReference<K, V>
    {
        final ReferenceEntry<K, V> entry;
        
        WeakValueReference(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> entry) {
            super(v, referenceQueue);
            this.entry = entry;
        }
        
        @Override
        public void clear(final ValueReference<K, V> valueReference) {
            this.clear();
        }
        
        @Override
        public ValueReference<K, V> copyFor(final ReferenceQueue<V> referenceQueue, final V v, final ReferenceEntry<K, V> referenceEntry) {
            return new WeakValueReference((ReferenceQueue<Object>)referenceQueue, v, (ReferenceEntry<Object, Object>)referenceEntry);
        }
        
        @Override
        public ReferenceEntry<K, V> getEntry() {
            return this.entry;
        }
        
        @Override
        public boolean isComputingReference() {
            return false;
        }
        
        @Override
        public V waitForValue() {
            return this.get();
        }
    }
    
    final class WriteThroughEntry extends AbstractMapEntry<K, V>
    {
        final K key;
        V value;
        
        WriteThroughEntry(final K key, final V value) {
            this.key = key;
            this.value = value;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (b) {
                final Entry entry = (Entry)o;
                boolean b3 = b2;
                if (this.key.equals(entry.getKey())) {
                    b3 = b2;
                    if (this.value.equals(entry.getValue())) {
                        b3 = true;
                    }
                }
                return b3;
            }
            return false;
        }
        
        @Override
        public K getKey() {
            return this.key;
        }
        
        @Override
        public V getValue() {
            return this.value;
        }
        
        @Override
        public int hashCode() {
            return this.key.hashCode() ^ this.value.hashCode();
        }
        
        @Override
        public V setValue(final V value) {
            final Object put = MapMakerInternalMap.this.put(this.key, value);
            this.value = value;
            return (V)put;
        }
    }
}
