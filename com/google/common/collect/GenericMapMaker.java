package com.google.common.collect;

import java.util.concurrent.ConcurrentMap;
import com.google.common.base.Function;
import com.google.common.base.MoreObjects;

@Deprecated
abstract class GenericMapMaker<K0, V0>
{
    MapMaker.RemovalListener<K0, V0> removalListener;
    
     <K extends K0, V extends V0> MapMaker.RemovalListener<K, V> getRemovalListener() {
        return (MapMaker.RemovalListener<K, V>)MoreObjects.firstNonNull((MapMaker.RemovalListener)this.removalListener, NullListener.INSTANCE);
    }
    
    @Deprecated
    abstract <K extends K0, V extends V0> ConcurrentMap<K, V> makeComputingMap(final Function<? super K, ? extends V> p0);
    
    enum NullListener implements RemovalListener<Object, Object>
    {
        INSTANCE;
        
        @Override
        public void onRemoval(final RemovalNotification<Object, Object> removalNotification) {
        }
    }
}
