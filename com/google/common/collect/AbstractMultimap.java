package com.google.common.collect;

import com.google.common.collect.AbstractMultimap$com.google.common.collect.AbstractMultimap$Entries;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import java.util.Map;

abstract class AbstractMultimap<K, V> implements Multimap<K, V>
{
    private transient Map<K, Collection<V>> asMap;
    private transient Collection<Map.Entry<K, V>> entries;
    private transient Set<K> keySet;
    
    @Override
    public Map<K, Collection<V>> asMap() {
        Map<K, Collection<V>> asMap = this.asMap;
        if (asMap == null) {
            asMap = this.createAsMap();
            this.asMap = asMap;
        }
        return asMap;
    }
    
    @Override
    public boolean containsEntry(final Object o, final Object o2) {
        final Collection<V> collection = this.asMap().get(o);
        return collection != null && collection.contains(o2);
    }
    
    abstract Map<K, Collection<V>> createAsMap();
    
    Collection<Map.Entry<K, V>> createEntries() {
        if (this instanceof SetMultimap) {
            return new EntrySet();
        }
        return (Collection<Map.Entry<K, V>>)new Entries();
    }
    
    Set<K> createKeySet() {
        return (Set<K>)new Maps.KeySet((Map<Object, Object>)this.asMap());
    }
    
    public Collection<Map.Entry<K, V>> entries() {
        Collection<Map.Entry<K, V>> entries = this.entries;
        if (entries == null) {
            entries = this.createEntries();
            this.entries = entries;
        }
        return entries;
    }
    
    abstract Iterator<Map.Entry<K, V>> entryIterator();
    
    @Override
    public boolean equals(final Object o) {
        return Multimaps.equalsImpl(this, o);
    }
    
    @Override
    public int hashCode() {
        return this.asMap().hashCode();
    }
    
    public Set<K> keySet() {
        Set<K> keySet = this.keySet;
        if (keySet == null) {
            keySet = this.createKeySet();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    @Override
    public boolean remove(final Object o, final Object o2) {
        final Collection<V> collection = this.asMap().get(o);
        return collection != null && collection.remove(o2);
    }
    
    @Override
    public String toString() {
        return this.asMap().toString();
    }
    
    private class Entries extends Multimaps.Entries<K, V>
    {
        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return AbstractMultimap.this.entryIterator();
        }
        
        @Override
        Multimap<K, V> multimap() {
            return (Multimap<K, V>)AbstractMultimap.this;
        }
    }
    
    private class EntrySet extends AbstractMultimap$Entries implements Set<Map.Entry<K, V>>
    {
        public boolean equals(final Object o) {
            return Sets.equalsImpl(this, o);
        }
        
        public int hashCode() {
            return Sets.hashCodeImpl(this);
        }
    }
}
