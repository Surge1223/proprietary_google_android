package com.google.common.collect;

public abstract class ComparisonChain
{
    private static final ComparisonChain ACTIVE;
    private static final ComparisonChain GREATER;
    private static final ComparisonChain LESS;
    
    static {
        ACTIVE = new ComparisonChain() {
            ComparisonChain classify(final int n) {
                ComparisonChain comparisonChain;
                if (n < 0) {
                    comparisonChain = ComparisonChain.LESS;
                }
                else if (n > 0) {
                    comparisonChain = ComparisonChain.GREATER;
                }
                else {
                    comparisonChain = ComparisonChain.ACTIVE;
                }
                return comparisonChain;
            }
            
            @Override
            public ComparisonChain compare(final Comparable comparable, final Comparable comparable2) {
                return this.classify(comparable.compareTo(comparable2));
            }
            
            @Override
            public int result() {
                return 0;
            }
        };
        LESS = new InactiveComparisonChain(-1);
        GREATER = new InactiveComparisonChain(1);
    }
    
    public static ComparisonChain start() {
        return ComparisonChain.ACTIVE;
    }
    
    public abstract ComparisonChain compare(final Comparable<?> p0, final Comparable<?> p1);
    
    public abstract int result();
    
    private static final class InactiveComparisonChain extends ComparisonChain
    {
        final int result;
        
        InactiveComparisonChain(final int result) {
            super(null);
            this.result = result;
        }
        
        @Override
        public ComparisonChain compare(final Comparable comparable, final Comparable comparable2) {
            return this;
        }
        
        @Override
        public int result() {
            return this.result;
        }
    }
}
