package com.google.common.collect;

public class ComputationException extends RuntimeException
{
    private static final long serialVersionUID = 0L;
    
    public ComputationException(final Throwable t) {
        super(t);
    }
}
