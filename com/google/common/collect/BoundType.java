package com.google.common.collect;

public enum BoundType
{
    CLOSED(1) {
    }, 
    OPEN(0) {
    };
    
    static BoundType forBoolean(final boolean b) {
        BoundType boundType;
        if (b) {
            boundType = BoundType.CLOSED;
        }
        else {
            boundType = BoundType.OPEN;
        }
        return boundType;
    }
}
