package com.google.common.util.concurrent;

public final class MoreExecutors
{
    static class Application
    {
        void addShutdownHook(final Thread thread) {
            Runtime.getRuntime().addShutdownHook(thread);
        }
    }
}
