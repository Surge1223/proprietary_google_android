package com.google.common.util.concurrent;

import java.util.concurrent.TimeUnit;
import com.google.common.base.Preconditions;

public abstract class RateLimiter
{
    private volatile Object mutexDoNotUseDirectly;
    private final SleepingStopwatch stopwatch;
    
    RateLimiter(final SleepingStopwatch sleepingStopwatch) {
        this.stopwatch = Preconditions.checkNotNull(sleepingStopwatch);
    }
    
    static RateLimiter create(final SleepingStopwatch sleepingStopwatch, final double rate) {
        final SmoothRateLimiter.SmoothBursty smoothBursty = new SmoothRateLimiter.SmoothBursty(sleepingStopwatch, 1.0);
        smoothBursty.setRate(rate);
        return smoothBursty;
    }
    
    static RateLimiter create(final SleepingStopwatch sleepingStopwatch, final double rate, final long n, final TimeUnit timeUnit) {
        final SmoothRateLimiter.SmoothWarmingUp smoothWarmingUp = new SmoothRateLimiter.SmoothWarmingUp(sleepingStopwatch, n, timeUnit);
        smoothWarmingUp.setRate(rate);
        return smoothWarmingUp;
    }
    
    private Object mutex() {
        final Object mutexDoNotUseDirectly;
        if ((mutexDoNotUseDirectly = this.mutexDoNotUseDirectly) == null) {
            synchronized (this) {
                if (this.mutexDoNotUseDirectly == null) {
                    this.mutexDoNotUseDirectly = new Object();
                }
            }
        }
        return mutexDoNotUseDirectly;
    }
    
    abstract double doGetRate();
    
    abstract void doSetRate(final double p0, final long p1);
    
    public final double getRate() {
        synchronized (this.mutex()) {
            return this.doGetRate();
        }
    }
    
    public final void setRate(final double n) {
        Preconditions.checkArgument(n > 0.0 && !Double.isNaN(n), (Object)"rate must be positive");
        synchronized (this.mutex()) {
            this.doSetRate(n, this.stopwatch.readMicros());
        }
    }
    
    @Override
    public String toString() {
        return String.format("RateLimiter[stableRate=%3.1fqps]", this.getRate());
    }
    
    abstract static class SleepingStopwatch
    {
        abstract long readMicros();
    }
}
