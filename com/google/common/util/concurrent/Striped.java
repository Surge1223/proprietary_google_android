package com.google.common.util.concurrent;

import java.util.concurrent.locks.ReadWriteLock;
import com.google.common.base.Supplier;

public abstract class Striped<L>
{
    private static final Supplier<ReadWriteLock> READ_WRITE_LOCK_SUPPLIER;
    
    static {
        READ_WRITE_LOCK_SUPPLIER = new Supplier<ReadWriteLock>() {};
    }
    
    static class LargeLazyStriped<L> extends PowerOfTwoStriped<L>
    {
    }
    
    private abstract static class PowerOfTwoStriped<L> extends Striped<L>
    {
    }
    
    static class SmallLazyStriped<L> extends PowerOfTwoStriped<L>
    {
    }
}
