package com.google.common.util.concurrent;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class UncaughtExceptionHandlers
{
    static final class Exiter implements UncaughtExceptionHandler
    {
        private static final Logger logger;
        private final Runtime runtime;
        
        static {
            logger = Logger.getLogger(Exiter.class.getName());
        }
        
        @Override
        public void uncaughtException(final Thread thread, final Throwable t) {
            while (true) {
                try {
                    try {
                        Exiter.logger.log(Level.SEVERE, String.format("Caught an exception in %s.  Shutting down.", thread), t);
                        this.runtime.exit(1);
                    }
                    finally {}
                }
                catch (Throwable t2) {
                    System.err.println(t.getMessage());
                    System.err.println(t2.getMessage());
                    continue;
                }
                break;
            }
            return;
            this.runtime.exit(1);
        }
    }
}
