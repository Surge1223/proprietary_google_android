package com.google.common.util.concurrent;

import java.util.concurrent.TimeUnit;

abstract class SmoothRateLimiter extends RateLimiter
{
    double maxPermits;
    private long nextFreeTicketMicros;
    double stableIntervalMicros;
    double storedPermits;
    
    private SmoothRateLimiter(final SleepingStopwatch sleepingStopwatch) {
        super(sleepingStopwatch);
        this.nextFreeTicketMicros = 0L;
    }
    
    private void resync(final long nextFreeTicketMicros) {
        if (nextFreeTicketMicros > this.nextFreeTicketMicros) {
            this.storedPermits = Math.min(this.maxPermits, this.storedPermits + (nextFreeTicketMicros - this.nextFreeTicketMicros) / this.stableIntervalMicros);
            this.nextFreeTicketMicros = nextFreeTicketMicros;
        }
    }
    
    @Override
    final double doGetRate() {
        return TimeUnit.SECONDS.toMicros(1L) / this.stableIntervalMicros;
    }
    
    abstract void doSetRate(final double p0, final double p1);
    
    @Override
    final void doSetRate(final double n, final long n2) {
        this.resync(n2);
        this.doSetRate(n, this.stableIntervalMicros = TimeUnit.SECONDS.toMicros(1L) / n);
    }
    
    static final class SmoothBursty extends SmoothRateLimiter
    {
        final double maxBurstSeconds;
        
        SmoothBursty(final SleepingStopwatch sleepingStopwatch, final double maxBurstSeconds) {
            super(sleepingStopwatch, null);
            this.maxBurstSeconds = maxBurstSeconds;
        }
        
        @Override
        void doSetRate(double storedPermits, double maxPermits) {
            maxPermits = this.maxPermits;
            this.maxPermits = this.maxBurstSeconds * storedPermits;
            if (maxPermits == Double.POSITIVE_INFINITY) {
                this.storedPermits = this.maxPermits;
            }
            else {
                storedPermits = 0.0;
                if (maxPermits != 0.0) {
                    storedPermits = this.storedPermits * this.maxPermits / maxPermits;
                }
                this.storedPermits = storedPermits;
            }
        }
    }
    
    static final class SmoothWarmingUp extends SmoothRateLimiter
    {
        private double halfPermits;
        private double slope;
        private final long warmupPeriodMicros;
        
        SmoothWarmingUp(final SleepingStopwatch sleepingStopwatch, final long n, final TimeUnit timeUnit) {
            super(sleepingStopwatch, null);
            this.warmupPeriodMicros = timeUnit.toMicros(n);
        }
        
        @Override
        void doSetRate(double storedPermits, final double n) {
            storedPermits = this.maxPermits;
            this.maxPermits = this.warmupPeriodMicros / n;
            this.halfPermits = this.maxPermits / 2.0;
            this.slope = (3.0 * n - n) / this.halfPermits;
            if (storedPermits == Double.POSITIVE_INFINITY) {
                this.storedPermits = 0.0;
            }
            else {
                if (storedPermits == 0.0) {
                    storedPermits = this.maxPermits;
                }
                else {
                    storedPermits = this.storedPermits * this.maxPermits / storedPermits;
                }
                this.storedPermits = storedPermits;
            }
        }
    }
}
