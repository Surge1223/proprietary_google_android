package com.google.common.reflect;

import java.util.Enumeration;
import com.google.common.collect.UnmodifiableIterator;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.io.IOException;
import java.util.Iterator;
import java.util.jar.Attributes;
import com.google.common.collect.ImmutableSet;
import java.util.jar.Manifest;
import java.io.File;
import com.google.common.collect.Sets;
import java.util.Comparator;
import com.google.common.collect.Ordering;
import java.util.Set;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.base.Preconditions;
import java.net.URL;
import java.util.LinkedHashMap;
import java.net.URISyntaxException;
import java.net.URLClassLoader;
import java.util.Map;
import com.google.common.collect.Maps;
import java.net.URI;
import com.google.common.collect.ImmutableMap;
import java.util.logging.Logger;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;

public final class ClassPath
{
    private static final Splitter CLASS_PATH_ATTRIBUTE_SEPARATOR;
    private static final Predicate<ClassInfo> IS_TOP_LEVEL;
    private static final Logger logger;
    
    static {
        logger = Logger.getLogger(ClassPath.class.getName());
        IS_TOP_LEVEL = new Predicate<ClassInfo>() {
            @Override
            public boolean apply(final ClassInfo classInfo) {
                return classInfo.className.indexOf(36) == -1;
            }
        };
        CLASS_PATH_ATTRIBUTE_SEPARATOR = Splitter.on(" ").omitEmptyStrings();
    }
    
    static String getClassName(final String s) {
        return s.substring(0, s.length() - ".class".length()).replace('/', '.');
    }
    
    static ImmutableMap<URI, ClassLoader> getClassPathEntries(final ClassLoader classLoader) {
        final LinkedHashMap<URI, ClassLoader> linkedHashMap = (LinkedHashMap<URI, ClassLoader>)Maps.newLinkedHashMap();
        final ClassLoader parent = classLoader.getParent();
        if (parent != null) {
            linkedHashMap.putAll((Map<?, ?>)getClassPathEntries(parent));
        }
        if (classLoader instanceof URLClassLoader) {
            final URL[] urLs = ((URLClassLoader)classLoader).getURLs();
            final int length = urLs.length;
            int i = 0;
            while (i < length) {
                final URL url = urLs[i];
                try {
                    final URI uri = url.toURI();
                    if (!linkedHashMap.containsKey(uri)) {
                        linkedHashMap.put(uri, classLoader);
                    }
                    ++i;
                    continue;
                }
                catch (URISyntaxException ex) {
                    throw new IllegalArgumentException(ex);
                }
                break;
            }
        }
        return (ImmutableMap<URI, ClassLoader>)ImmutableMap.copyOf((Map<?, ?>)linkedHashMap);
    }
    
    public static final class ClassInfo extends ResourceInfo
    {
        private final String className;
        
        ClassInfo(final String s, final ClassLoader classLoader) {
            super(s, classLoader);
            this.className = ClassPath.getClassName(s);
        }
        
        @Override
        public String toString() {
            return this.className;
        }
    }
    
    public static class ResourceInfo
    {
        final ClassLoader loader;
        private final String resourceName;
        
        ResourceInfo(final String s, final ClassLoader classLoader) {
            this.resourceName = Preconditions.checkNotNull(s);
            this.loader = Preconditions.checkNotNull(classLoader);
        }
        
        static ResourceInfo of(final String s, final ClassLoader classLoader) {
            if (s.endsWith(".class")) {
                return (ResourceInfo)new ClassInfo(s, classLoader);
            }
            return new ResourceInfo(s, classLoader);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof ResourceInfo;
            final boolean b2 = false;
            if (b) {
                final ResourceInfo resourceInfo = (ResourceInfo)o;
                boolean b3 = b2;
                if (this.resourceName.equals(resourceInfo.resourceName)) {
                    b3 = b2;
                    if (this.loader == resourceInfo.loader) {
                        b3 = true;
                    }
                }
                return b3;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return this.resourceName.hashCode();
        }
        
        @Override
        public String toString() {
            return this.resourceName;
        }
    }
    
    static final class Scanner
    {
        private final ImmutableSortedSet.Builder<ResourceInfo> resources;
        private final Set<URI> scannedUris;
        
        Scanner() {
            this.resources = (ImmutableSortedSet.Builder<ResourceInfo>)new ImmutableSortedSet.Builder(Ordering.usingToString());
            this.scannedUris = (Set<URI>)Sets.newHashSet();
        }
        
        static URI getClassPathEntry(final File file, final String s) throws URISyntaxException {
            final URI uri = new URI(s);
            if (uri.isAbsolute()) {
                return uri;
            }
            return new File(file.getParentFile(), s.replace('/', File.separatorChar)).toURI();
        }
        
        static ImmutableSet<URI> getClassPathFromManifest(final File file, Manifest manifest) {
            if (manifest == null) {
                return ImmutableSet.of();
            }
            final ImmutableSet.Builder<URI> builder = ImmutableSet.builder();
            final String value = manifest.getMainAttributes().getValue(Attributes.Name.CLASS_PATH.toString());
            if (value != null) {
                final Iterator<String> iterator = ClassPath.CLASS_PATH_ATTRIBUTE_SEPARATOR.split(value).iterator();
                while (iterator.hasNext()) {
                    manifest = (Manifest)iterator.next();
                    try {
                        builder.add(getClassPathEntry(file, (String)manifest));
                    }
                    catch (URISyntaxException ex) {
                        final Logger access$100 = ClassPath.logger;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid Class-Path entry: ");
                        sb.append((String)manifest);
                        access$100.warning(sb.toString());
                    }
                }
            }
            return builder.build();
        }
        
        private void scanDirectory(final File file, final ClassLoader classLoader) throws IOException {
            this.scanDirectory(file, classLoader, "", ImmutableSet.of());
        }
        
        private void scanDirectory(final File file, final ClassLoader classLoader, final String s, final ImmutableSet<File> set) throws IOException {
            final File canonicalFile = file.getCanonicalFile();
            if (set.contains(canonicalFile)) {
                return;
            }
            final File[] listFiles = file.listFiles();
            if (listFiles == null) {
                final Logger access$100 = ClassPath.logger;
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot read directory ");
                sb.append(file);
                access$100.warning(sb.toString());
                return;
            }
            final ImmutableSet<File> build = ImmutableSet.builder().addAll(set).add(canonicalFile).build();
            for (final File file2 : listFiles) {
                final String name = file2.getName();
                if (file2.isDirectory()) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(s);
                    sb2.append(name);
                    sb2.append("/");
                    this.scanDirectory(file2, classLoader, sb2.toString(), build);
                }
                else {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(s);
                    sb3.append(name);
                    final String string = sb3.toString();
                    if (!string.equals("META-INF/MANIFEST.MF")) {
                        this.resources.add(ResourceInfo.of(string, classLoader));
                    }
                }
            }
        }
        
        private void scanJar(final File file, final ClassLoader classLoader) throws IOException {
            try {
                final JarFile jarFile = new JarFile(file);
                try {
                    final UnmodifiableIterator<URI> iterator = getClassPathFromManifest(file, jarFile.getManifest()).iterator();
                    while (iterator.hasNext()) {
                        this.scan(iterator.next(), classLoader);
                    }
                    final Enumeration<JarEntry> entries = jarFile.entries();
                    while (entries.hasMoreElements()) {
                        final JarEntry jarEntry = entries.nextElement();
                        if (!jarEntry.isDirectory()) {
                            if (jarEntry.getName().equals("META-INF/MANIFEST.MF")) {
                                continue;
                            }
                            this.resources.add(ResourceInfo.of(jarEntry.getName(), classLoader));
                        }
                    }
                }
                finally {
                    try {
                        jarFile.close();
                    }
                    catch (IOException ex) {}
                }
            }
            catch (IOException ex2) {}
        }
        
        void scan(final URI uri, final ClassLoader classLoader) throws IOException {
            if (uri.getScheme().equals("file") && this.scannedUris.add(uri)) {
                this.scanFrom(new File(uri), classLoader);
            }
        }
        
        void scanFrom(final File file, final ClassLoader classLoader) throws IOException {
            if (!file.exists()) {
                return;
            }
            if (file.isDirectory()) {
                this.scanDirectory(file, classLoader);
            }
            else {
                this.scanJar(file, classLoader);
            }
        }
    }
}
