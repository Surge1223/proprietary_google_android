package com.android.settings;

import android.support.v7.preference.PreferenceScreen;
import android.app.Activity;
import android.support.v7.preference.PreferenceGroup;
import android.os.Bundle;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;

public class LegalSettings extends SettingsPreferenceFragment implements Indexable
{
    static final String KEY_WALLPAPER_ATTRIBUTIONS = "wallpaper_attributions";
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            private boolean checkIntentAction(final Context context, final String s) {
                final List queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent(s), 0);
                for (int size = queryIntentActivities.size(), i = 0; i < size; ++i) {
                    if ((queryIntentActivities.get(i).activityInfo.applicationInfo.flags & 0x1) != 0x0) {
                        return true;
                    }
                }
                return false;
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (!this.checkIntentAction(context, "android.settings.TERMS")) {
                    nonIndexableKeys.add("terms");
                }
                if (!this.checkIntentAction(context, "android.settings.LICENSE")) {
                    nonIndexableKeys.add("license");
                }
                if (!this.checkIntentAction(context, "android.settings.COPYRIGHT")) {
                    nonIndexableKeys.add("copyright");
                }
                if (!this.checkIntentAction(context, "android.settings.WEBVIEW_LICENSE")) {
                    nonIndexableKeys.add("webview_license");
                }
                nonIndexableKeys.add("wallpaper_attributions");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082688;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    void checkWallpaperAttributionAvailability(final Context context) {
        if (!context.getResources().getBoolean(2131034173)) {
            this.removePreference("wallpaper_attributions");
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 225;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082688);
        final Activity activity = this.getActivity();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        Utils.updatePreferenceToSpecificActivityOrRemove((Context)activity, preferenceScreen, "terms", 1);
        Utils.updatePreferenceToSpecificActivityOrRemove((Context)activity, preferenceScreen, "license", 1);
        Utils.updatePreferenceToSpecificActivityOrRemove((Context)activity, preferenceScreen, "copyright", 1);
        Utils.updatePreferenceToSpecificActivityOrRemove((Context)activity, preferenceScreen, "webview_license", 1);
        this.checkWallpaperAttributionAvailability((Context)activity);
    }
}
