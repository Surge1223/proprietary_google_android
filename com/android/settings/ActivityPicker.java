package com.android.settings;

import android.widget.ListAdapter;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.DrawFilter;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.graphics.ColorFilter;
import android.graphics.Canvas;
import android.content.pm.ResolveInfo;
import java.util.Comparator;
import java.util.Collections;
import android.content.pm.ResolveInfo$DisplayNameComparator;
import com.android.internal.app.AlertController$AlertParams;
import android.os.Parcelable;
import android.net.Uri;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources.Theme;
import android.content.Intent$ShortcutIconResource;
import java.util.ArrayList;
import java.util.List;
import android.content.Intent;
import android.content.DialogInterface$OnClickListener;
import android.content.DialogInterface$OnCancelListener;
import com.android.internal.app.AlertActivity;

public class ActivityPicker extends AlertActivity implements DialogInterface$OnCancelListener, DialogInterface$OnClickListener
{
    private PickAdapter mAdapter;
    private Intent mBaseIntent;
    
    protected Intent getIntentForPosition(final int n) {
        return ((Item)this.mAdapter.getItem(n)).getIntent(this.mBaseIntent);
    }
    
    protected List<Item> getItems() {
        final PackageManager packageManager = this.getPackageManager();
        final ArrayList<Item> list = new ArrayList<Item>();
        final Intent intent = this.getIntent();
        final ArrayList stringArrayListExtra = intent.getStringArrayListExtra("android.intent.extra.shortcut.NAME");
        final ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.shortcut.ICON_RESOURCE");
        if (stringArrayListExtra != null && parcelableArrayListExtra != null && stringArrayListExtra.size() == parcelableArrayListExtra.size()) {
            for (int i = 0; i < stringArrayListExtra.size(); ++i) {
                final String s = stringArrayListExtra.get(i);
                Drawable drawable = null;
                try {
                    final Intent$ShortcutIconResource intent$ShortcutIconResource = parcelableArrayListExtra.get(i);
                    final Resources resourcesForApplication = packageManager.getResourcesForApplication(intent$ShortcutIconResource.packageName);
                    drawable = resourcesForApplication.getDrawable(resourcesForApplication.getIdentifier(intent$ShortcutIconResource.resourceName, (String)null, (String)null), (Resources.Theme)null);
                }
                catch (PackageManager$NameNotFoundException ex) {}
                list.add(new Item((Context)this, s, drawable));
            }
        }
        if (this.mBaseIntent != null) {
            this.putIntentItems(this.mBaseIntent, list);
        }
        return list;
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        this.setResult(0);
        this.finish();
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        this.setResult(-1, this.getIntentForPosition(n));
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent intent = this.getIntent();
        final Parcelable parcelableExtra = intent.getParcelableExtra("android.intent.extra.INTENT");
        if (parcelableExtra instanceof Intent) {
            (this.mBaseIntent = (Intent)parcelableExtra).setFlags(this.mBaseIntent.getFlags() & 0xFFFFFF3C);
        }
        else {
            (this.mBaseIntent = new Intent("android.intent.action.MAIN", (Uri)null)).addCategory("android.intent.category.DEFAULT");
        }
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        mAlertParams.mOnClickListener = (DialogInterface$OnClickListener)this;
        mAlertParams.mOnCancelListener = (DialogInterface$OnCancelListener)this;
        if (intent.hasExtra("android.intent.extra.TITLE")) {
            mAlertParams.mTitle = intent.getStringExtra("android.intent.extra.TITLE");
        }
        else {
            mAlertParams.mTitle = this.getTitle();
        }
        this.mAdapter = new PickAdapter((Context)this, this.getItems());
        mAlertParams.mAdapter = (ListAdapter)this.mAdapter;
        this.setupAlert();
    }
    
    protected void putIntentItems(final Intent intent, final List<Item> list) {
        final PackageManager packageManager = this.getPackageManager();
        int i = 0;
        final List queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        Collections.sort((List<Object>)queryIntentActivities, (Comparator<? super Object>)new ResolveInfo$DisplayNameComparator(packageManager));
        while (i < queryIntentActivities.size()) {
            list.add(new Item((Context)this, packageManager, queryIntentActivities.get(i)));
            ++i;
        }
    }
    
    private static class EmptyDrawable extends Drawable
    {
        private final int mHeight;
        private final int mWidth;
        
        EmptyDrawable(final int mWidth, final int mHeight) {
            this.mWidth = mWidth;
            this.mHeight = mHeight;
        }
        
        public void draw(final Canvas canvas) {
        }
        
        public int getIntrinsicHeight() {
            return this.mHeight;
        }
        
        public int getIntrinsicWidth() {
            return this.mWidth;
        }
        
        public int getMinimumHeight() {
            return this.mHeight;
        }
        
        public int getMinimumWidth() {
            return this.mWidth;
        }
        
        public int getOpacity() {
            return -3;
        }
        
        public void setAlpha(final int n) {
        }
        
        public void setColorFilter(final ColorFilter colorFilter) {
        }
    }
    
    private static class IconResizer
    {
        private final Canvas mCanvas;
        private final int mIconHeight;
        private final int mIconWidth;
        private final DisplayMetrics mMetrics;
        private final Rect mOldBounds;
        
        public IconResizer(final int mIconWidth, final int mIconHeight, final DisplayMetrics mMetrics) {
            this.mOldBounds = new Rect();
            (this.mCanvas = new Canvas()).setDrawFilter((DrawFilter)new PaintFlagsDrawFilter(4, 2));
            this.mMetrics = mMetrics;
            this.mIconWidth = mIconWidth;
            this.mIconHeight = mIconHeight;
        }
        
        public Drawable createIconThumbnail(final Drawable drawable) {
            int mIconWidth = this.mIconWidth;
            final int mIconHeight = this.mIconHeight;
            if (drawable == null) {
                return new EmptyDrawable(mIconWidth, mIconHeight);
            }
            int n = mIconWidth;
            int n2 = mIconHeight;
            Object o;
            try {
                if (drawable instanceof PaintDrawable) {
                    n = mIconWidth;
                    n2 = mIconHeight;
                    final PaintDrawable paintDrawable = (PaintDrawable)drawable;
                    n = mIconWidth;
                    n2 = mIconHeight;
                    paintDrawable.setIntrinsicWidth(mIconWidth);
                    n = mIconWidth;
                    n2 = mIconHeight;
                    paintDrawable.setIntrinsicHeight(mIconHeight);
                }
                else {
                    n = mIconWidth;
                    n2 = mIconHeight;
                    if (drawable instanceof BitmapDrawable) {
                        n = mIconWidth;
                        n2 = mIconHeight;
                        final BitmapDrawable bitmapDrawable = (BitmapDrawable)drawable;
                        n = mIconWidth;
                        n2 = mIconHeight;
                        if (bitmapDrawable.getBitmap().getDensity() == 0) {
                            n = mIconWidth;
                            n2 = mIconHeight;
                            bitmapDrawable.setTargetDensity(this.mMetrics);
                        }
                    }
                }
                n = mIconWidth;
                n2 = mIconHeight;
                final int intrinsicWidth = drawable.getIntrinsicWidth();
                n = mIconWidth;
                n2 = mIconHeight;
                final int intrinsicHeight = drawable.getIntrinsicHeight();
                o = drawable;
                if (intrinsicWidth > 0) {
                    o = drawable;
                    if (intrinsicHeight > 0) {
                        if (mIconWidth >= intrinsicWidth && mIconHeight >= intrinsicHeight) {
                            o = drawable;
                            if (intrinsicWidth < mIconWidth) {
                                o = drawable;
                                if (intrinsicHeight < mIconHeight) {
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    final Bitmap$Config argb_8888 = Bitmap$Config.ARGB_8888;
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    final Bitmap bitmap = Bitmap.createBitmap(this.mIconWidth, this.mIconHeight, argb_8888);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    final Canvas mCanvas = this.mCanvas;
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    mCanvas.setBitmap(bitmap);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    this.mOldBounds.set(drawable.getBounds());
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    final int n3 = (mIconWidth - intrinsicWidth) / 2;
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    final int n4 = (mIconHeight - intrinsicHeight) / 2;
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    drawable.setBounds(n3, n4, n3 + intrinsicWidth, n4 + intrinsicHeight);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    drawable.draw(mCanvas);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    drawable.setBounds(this.mOldBounds);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    o = new(android.graphics.drawable.BitmapDrawable.class);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    new BitmapDrawable(bitmap);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    ((BitmapDrawable)o).setTargetDensity(this.mMetrics);
                                    n = mIconWidth;
                                    n2 = mIconHeight;
                                    mCanvas.setBitmap((Bitmap)null);
                                }
                            }
                        }
                        else {
                            final float n5 = intrinsicWidth / intrinsicHeight;
                            int n6;
                            if (intrinsicWidth > intrinsicHeight) {
                                n6 = (int)(mIconWidth / n5);
                            }
                            else {
                                n6 = mIconHeight;
                                if (intrinsicHeight > intrinsicWidth) {
                                    mIconWidth = (int)(mIconHeight * n5);
                                    n6 = mIconHeight;
                                }
                            }
                            n = mIconWidth;
                            n2 = n6;
                            Bitmap$Config bitmap$Config;
                            if (drawable.getOpacity() != -1) {
                                n = mIconWidth;
                                n2 = n6;
                                bitmap$Config = Bitmap$Config.ARGB_8888;
                            }
                            else {
                                n = mIconWidth;
                                n2 = n6;
                                bitmap$Config = Bitmap$Config.RGB_565;
                            }
                            n = mIconWidth;
                            n2 = n6;
                            final Bitmap bitmap2 = Bitmap.createBitmap(this.mIconWidth, this.mIconHeight, bitmap$Config);
                            n = mIconWidth;
                            n2 = n6;
                            final Canvas mCanvas2 = this.mCanvas;
                            n = mIconWidth;
                            n2 = n6;
                            mCanvas2.setBitmap(bitmap2);
                            n = mIconWidth;
                            n2 = n6;
                            this.mOldBounds.set(drawable.getBounds());
                            n = mIconWidth;
                            n2 = n6;
                            final int n7 = (this.mIconWidth - mIconWidth) / 2;
                            n = mIconWidth;
                            n2 = n6;
                            final int n8 = (this.mIconHeight - n6) / 2;
                            n = mIconWidth;
                            n2 = n6;
                            drawable.setBounds(n7, n8, n7 + mIconWidth, n8 + n6);
                            n = mIconWidth;
                            n2 = n6;
                            drawable.draw(mCanvas2);
                            n = mIconWidth;
                            n2 = n6;
                            drawable.setBounds(this.mOldBounds);
                            n = mIconWidth;
                            n2 = n6;
                            o = new(android.graphics.drawable.BitmapDrawable.class);
                            n = mIconWidth;
                            n2 = n6;
                            new BitmapDrawable(bitmap2);
                            n = mIconWidth;
                            n2 = n6;
                            ((BitmapDrawable)o).setTargetDensity(this.mMetrics);
                            n = mIconWidth;
                            n2 = n6;
                            mCanvas2.setBitmap((Bitmap)null);
                        }
                    }
                }
            }
            catch (Throwable t) {
                o = new EmptyDrawable(n, n2);
            }
            return (Drawable)o;
        }
    }
    
    protected static class PickAdapter extends BaseAdapter
    {
        private final LayoutInflater mInflater;
        private final List<Item> mItems;
        
        public PickAdapter(final Context context, final List<Item> mItems) {
            this.mInflater = (LayoutInflater)context.getSystemService("layout_inflater");
            this.mItems = mItems;
        }
        
        public int getCount() {
            return this.mItems.size();
        }
        
        public Object getItem(final int n) {
            return this.mItems.get(n);
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View inflate = view;
            if (view == null) {
                inflate = this.mInflater.inflate(2131558641, viewGroup, false);
            }
            final Item item = (Item)this.getItem(n);
            final TextView textView = (TextView)inflate;
            textView.setText(item.label);
            textView.setCompoundDrawablesWithIntrinsicBounds(item.icon, (Drawable)null, (Drawable)null, (Drawable)null);
            return inflate;
        }
        
        public static class Item implements LabelledItem
        {
            protected static IconResizer sResizer;
            String className;
            Bundle extras;
            Drawable icon;
            CharSequence label;
            String packageName;
            
            Item(final Context context, final PackageManager packageManager, final ResolveInfo resolveInfo) {
                this.label = resolveInfo.loadLabel(packageManager);
                if (this.label == null && resolveInfo.activityInfo != null) {
                    this.label = resolveInfo.activityInfo.name;
                }
                this.icon = this.getResizer(context).createIconThumbnail(resolveInfo.loadIcon(packageManager));
                this.packageName = resolveInfo.activityInfo.applicationInfo.packageName;
                this.className = resolveInfo.activityInfo.name;
            }
            
            Item(final Context context, final CharSequence label, final Drawable drawable) {
                this.label = label;
                this.icon = this.getResizer(context).createIconThumbnail(drawable);
            }
            
            Intent getIntent(Intent intent) {
                intent = new Intent(intent);
                if (this.packageName != null && this.className != null) {
                    intent.setClassName(this.packageName, this.className);
                    if (this.extras != null) {
                        intent.putExtras(this.extras);
                    }
                }
                else {
                    intent.setAction("android.intent.action.CREATE_SHORTCUT");
                    intent.putExtra("android.intent.extra.shortcut.NAME", this.label);
                }
                return intent;
            }
            
            @Override
            public CharSequence getLabel() {
                return this.label;
            }
            
            protected IconResizer getResizer(final Context context) {
                if (Item.sResizer == null) {
                    final Resources resources = context.getResources();
                    final int n = (int)resources.getDimension(17104896);
                    Item.sResizer = new IconResizer(n, n, resources.getDisplayMetrics());
                }
                return Item.sResizer;
            }
        }
    }
}
