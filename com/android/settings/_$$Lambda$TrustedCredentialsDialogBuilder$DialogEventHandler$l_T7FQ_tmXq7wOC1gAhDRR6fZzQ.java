package com.android.settings;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.content.pm.UserInfo;
import com.android.internal.widget.LockPatternUtils;
import java.util.Iterator;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import java.util.List;
import android.widget.ArrayAdapter;
import android.net.http.SslCertificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import android.animation.TimeInterpolator;
import android.view.animation.AnimationUtils;
import android.view.View$OnLayoutChangeListener;
import android.content.Context;
import android.os.UserManager;
import android.widget.LinearLayout;
import android.widget.Button;
import android.app.admin.DevicePolicyManager;
import android.app.AlertDialog;
import android.view.View;
import android.app.Activity;
import android.view.View.OnClickListener;
import android.content.DialogInterface$OnShowListener;
import java.util.function.IntConsumer;

public final class _$$Lambda$TrustedCredentialsDialogBuilder$DialogEventHandler$l_T7FQ_tmXq7wOC1gAhDRR6fZzQ implements IntConsumer
{
    @Override
    public final void accept(final int n) {
        this.f$0.onCredentialConfirmed(n);
    }
}
