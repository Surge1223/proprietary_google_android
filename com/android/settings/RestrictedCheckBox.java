package com.android.settings;

import android.graphics.PorterDuff.Mode;
import android.widget.TextView;
import android.util.AttributeSet;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Context;
import android.widget.CheckBox;

public class RestrictedCheckBox extends CheckBox
{
    private Context mContext;
    private boolean mDisabledByAdmin;
    private RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    
    public RestrictedCheckBox(final Context context) {
        this(context, null);
    }
    
    public RestrictedCheckBox(final Context mContext, final AttributeSet set) {
        super(mContext, set);
        this.mContext = mContext;
    }
    
    public boolean performClick() {
        if (this.mDisabledByAdmin) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mEnforcedAdmin);
            return true;
        }
        return super.performClick();
    }
    
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin) {
        final boolean mDisabledByAdmin = mEnforcedAdmin != null;
        this.mEnforcedAdmin = mEnforcedAdmin;
        if (this.mDisabledByAdmin != mDisabledByAdmin) {
            this.mDisabledByAdmin = mDisabledByAdmin;
            RestrictedLockUtils.setTextViewAsDisabledByAdmin(this.mContext, (TextView)this, this.mDisabledByAdmin);
            if (this.mDisabledByAdmin) {
                this.getButtonDrawable().setColorFilter(this.mContext.getColor(R.color.disabled_text_color), PorterDuff.Mode.MULTIPLY);
            }
            else {
                this.getButtonDrawable().clearColorFilter();
            }
        }
    }
}
