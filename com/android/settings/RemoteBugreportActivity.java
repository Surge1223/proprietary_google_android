package com.android.settings;

import android.os.UserHandle;
import android.content.Intent;
import android.util.Log;
import android.content.DialogInterface$OnClickListener;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.os.Bundle;
import android.app.Activity;

public class RemoteBugreportActivity extends Activity
{
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final int intExtra = this.getIntent().getIntExtra("android.app.extra.bugreport_notification_type", -1);
        if (intExtra == 2) {
            new AlertDialog$Builder((Context)this).setMessage(2131889083).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    RemoteBugreportActivity.this.finish();
                }
            }).setNegativeButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    RemoteBugreportActivity.this.finish();
                }
            }).create().show();
        }
        else if (intExtra != 1 && intExtra != 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Incorrect dialog type, no dialog shown. Received: ");
            sb.append(intExtra);
            Log.e("RemoteBugreportActivity", sb.toString());
        }
        else {
            final AlertDialog$Builder setTitle = new AlertDialog$Builder((Context)this).setTitle(2131889082);
            int message;
            if (intExtra == 1) {
                message = 2131889080;
            }
            else {
                message = 2131889081;
            }
            setTitle.setMessage(message).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    RemoteBugreportActivity.this.finish();
                }
            }).setNegativeButton(2131887345, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    RemoteBugreportActivity.this.sendBroadcastAsUser(new Intent("com.android.server.action.REMOTE_BUGREPORT_SHARING_DECLINED"), UserHandle.SYSTEM, "android.permission.DUMP");
                    RemoteBugreportActivity.this.finish();
                }
            }).setPositiveButton(2131889079, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    RemoteBugreportActivity.this.sendBroadcastAsUser(new Intent("com.android.server.action.REMOTE_BUGREPORT_SHARING_ACCEPTED"), UserHandle.SYSTEM, "android.permission.DUMP");
                    RemoteBugreportActivity.this.finish();
                }
            }).create().show();
        }
    }
}
