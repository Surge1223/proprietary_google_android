package com.android.settings.dream;

import com.android.settingslib.dream.DreamBackend;
import java.util.function.Function;

public final class _$$Lambda$hBSizG3ais67bSjAeIqNEa6sDBo implements Function
{
    @Override
    public final Object apply(final Object o) {
        return new CurrentDreamPicker.DreamCandidateInfo((DreamBackend.DreamInfo)o);
    }
}
