package com.android.settings.dream;

import android.graphics.drawable.Drawable;
import android.content.Context;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import java.util.HashMap;
import android.content.ComponentName;
import com.android.settings.widget.RadioButtonPickerFragment;
import com.android.settingslib.dream.DreamBackend;
import java.util.Map;
import java.util.function.Consumer;

public final class _$$Lambda$CurrentDreamPicker$t4o3LQXIuoDz_RsLdUZZYlwB3bA implements Consumer
{
    @Override
    public final void accept(final Object o) {
        CurrentDreamPicker.lambda$getDreamComponentsMap$0(this.f$0, (DreamBackend.DreamInfo)o);
    }
}
