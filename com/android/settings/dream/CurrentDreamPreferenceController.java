package com.android.settings.dream;

import android.support.v7.preference.Preference;
import com.android.settings.widget.GearPreference;
import java.util.function.Predicate;
import java.util.Optional;
import android.content.Context;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class CurrentDreamPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final DreamBackend mBackend;
    
    public CurrentDreamPreferenceController(final Context context) {
        super(context);
        this.mBackend = DreamBackend.getInstance(context);
    }
    
    private Optional<DreamBackend.DreamInfo> getActiveDreamInfo() {
        return this.mBackend.getDreamInfos().stream().filter((Predicate<? super DreamBackend.DreamInfo>)_$$Lambda$CurrentDreamPreferenceController$JJd0D4Ql1FstWgOpYrMCLEB2pnU.INSTANCE).findFirst();
    }
    
    private void launchScreenSaverSettings() {
        final Optional<DreamBackend.DreamInfo> activeDreamInfo = this.getActiveDreamInfo();
        if (!activeDreamInfo.isPresent()) {
            return;
        }
        this.mBackend.launchSettings(this.mContext, (DreamBackend.DreamInfo)activeDreamInfo.get());
    }
    
    private void setGearClickListenerForPreference(final Preference preference) {
        if (!(preference instanceof GearPreference)) {
            return;
        }
        final GearPreference gearPreference = (GearPreference)preference;
        final Optional<DreamBackend.DreamInfo> activeDreamInfo = this.getActiveDreamInfo();
        if (activeDreamInfo.isPresent() && activeDreamInfo.get().settingsComponentName != null) {
            gearPreference.setOnGearClickListener((GearPreference.OnGearClickListener)new _$$Lambda$CurrentDreamPreferenceController$faOOwvjkeM0i38i1bxACLza6vQ4(this));
            return;
        }
        gearPreference.setOnGearClickListener(null);
    }
    
    @Override
    public String getPreferenceKey() {
        return "current_screensaver";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mBackend.getDreamInfos().size() > 0;
    }
    
    @Override
    public void updateState(final Preference gearClickListenerForPreference) {
        super.updateState(gearClickListenerForPreference);
        gearClickListenerForPreference.setSummary(this.mBackend.getActiveDreamName());
        this.setGearClickListenerForPreference(gearClickListenerForPreference);
    }
}
