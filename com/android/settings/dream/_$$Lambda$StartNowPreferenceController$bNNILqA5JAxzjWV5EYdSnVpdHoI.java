package com.android.settings.dream;

import android.support.v7.preference.Preference;
import com.android.settings.applications.LayoutPreference;
import android.widget.Button;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$StartNowPreferenceController$bNNILqA5JAxzjWV5EYdSnVpdHoI implements View.OnClickListener
{
    public final void onClick(final View view) {
        StartNowPreferenceController.lambda$displayPreference$0(this.f$0, view);
    }
}
