package com.android.settings.dream;

import android.support.v7.preference.Preference;
import java.util.function.Predicate;
import java.util.Optional;
import android.content.Context;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settings.widget.GearPreference;

public final class _$$Lambda$CurrentDreamPreferenceController$faOOwvjkeM0i38i1bxACLza6vQ4 implements OnGearClickListener
{
    @Override
    public final void onGearClick(final GearPreference gearPreference) {
        CurrentDreamPreferenceController.lambda$setGearClickListenerForPreference$0(this.f$0, gearPreference);
    }
}
