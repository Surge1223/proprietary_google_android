package com.android.settings.dream;

import android.support.v7.preference.Preference;
import android.view.View.OnClickListener;
import com.android.settings.applications.LayoutPreference;
import android.widget.Button;
import android.support.v7.preference.PreferenceScreen;
import android.view.View;
import android.content.Context;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class StartNowPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final DreamBackend mBackend;
    
    public StartNowPreferenceController(final Context context) {
        super(context);
        this.mBackend = DreamBackend.getInstance(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        ((LayoutPreference)preferenceScreen.findPreference(this.getPreferenceKey())).findViewById(2131362092).setOnClickListener((View.OnClickListener)new _$$Lambda$StartNowPreferenceController$bNNILqA5JAxzjWV5EYdSnVpdHoI(this));
    }
    
    @Override
    public String getPreferenceKey() {
        return "dream_start_now_button_container";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        ((LayoutPreference)preference).findViewById(2131362092).setEnabled(this.mBackend.getWhenToDreamSetting() != 3);
    }
}
