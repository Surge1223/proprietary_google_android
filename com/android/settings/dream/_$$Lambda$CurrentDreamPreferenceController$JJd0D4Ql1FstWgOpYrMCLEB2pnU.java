package com.android.settings.dream;

import android.support.v7.preference.Preference;
import com.android.settings.widget.GearPreference;
import java.util.Optional;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settingslib.dream.DreamBackend;
import java.util.function.Predicate;

public final class _$$Lambda$CurrentDreamPreferenceController$JJd0D4Ql1FstWgOpYrMCLEB2pnU implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return ((DreamBackend.DreamInfo)o).isActive;
    }
}
