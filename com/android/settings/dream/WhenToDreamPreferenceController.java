package com.android.settings.dream;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WhenToDreamPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final DreamBackend mBackend;
    
    WhenToDreamPreferenceController(final Context context) {
        super(context);
        this.mBackend = DreamBackend.getInstance(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "when_to_start";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        preference.setSummary(preference.getContext().getString(DreamSettings.getDreamSettingDescriptionResId(this.mBackend.getWhenToDreamSetting())));
    }
}
