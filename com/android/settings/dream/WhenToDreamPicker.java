package com.android.settings.dream;

import android.graphics.drawable.Drawable;
import android.content.Context;
import java.util.ArrayList;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.widget.RadioButtonPickerFragment;

public class WhenToDreamPicker extends RadioButtonPickerFragment
{
    private DreamBackend mBackend;
    
    private String[] entries() {
        return this.getResources().getStringArray(2130903171);
    }
    
    private String[] keys() {
        return this.getResources().getStringArray(2130903172);
    }
    
    @Override
    protected List<? extends CandidateInfo> getCandidates() {
        final String[] entries = this.entries();
        final String[] keys = this.keys();
        final ArrayList<WhenToDreamCandidateInfo> list = new ArrayList<WhenToDreamCandidateInfo>();
        if (entries == null || entries.length <= 0) {
            return null;
        }
        if (keys != null && keys.length == entries.length) {
            for (int i = 0; i < entries.length; ++i) {
                list.add(new WhenToDreamCandidateInfo(entries[i], keys[i]));
            }
            return list;
        }
        throw new IllegalArgumentException("Entries and values must be of the same length.");
    }
    
    @Override
    protected String getDefaultKey() {
        return DreamSettings.getKeyFromSetting(this.mBackend.getWhenToDreamSetting());
    }
    
    @Override
    public int getMetricsCategory() {
        return 47;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082864;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mBackend = DreamBackend.getInstance(context);
    }
    
    @Override
    protected void onSelectionPerformed(final boolean b) {
        super.onSelectionPerformed(b);
        this.getActivity().finish();
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        this.mBackend.setWhenToDream(DreamSettings.getSettingFromPrefKey(s));
        return true;
    }
    
    private final class WhenToDreamCandidateInfo extends CandidateInfo
    {
        private final String key;
        private final String name;
        
        WhenToDreamCandidateInfo(final String name, final String key) {
            super(true);
            this.name = name;
            this.key = key;
        }
        
        @Override
        public String getKey() {
            return this.key;
        }
        
        @Override
        public Drawable loadIcon() {
            return null;
        }
        
        @Override
        public CharSequence loadLabel() {
            return this.name;
        }
    }
}
