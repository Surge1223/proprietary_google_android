package com.android.settings.dream;

import android.graphics.drawable.Drawable;
import android.content.Context;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import java.util.function.Consumer;
import java.util.HashMap;
import android.content.ComponentName;
import java.util.Map;
import com.android.settingslib.dream.DreamBackend;
import com.android.settings.widget.RadioButtonPickerFragment;

public final class CurrentDreamPicker extends RadioButtonPickerFragment
{
    private DreamBackend mBackend;
    
    private Map<String, ComponentName> getDreamComponentsMap() {
        final HashMap<String, ComponentName> hashMap = new HashMap<String, ComponentName>();
        this.mBackend.getDreamInfos().forEach(new _$$Lambda$CurrentDreamPicker$t4o3LQXIuoDz_RsLdUZZYlwB3bA(hashMap));
        return hashMap;
    }
    
    @Override
    protected List<? extends CandidateInfo> getCandidates() {
        return this.mBackend.getDreamInfos().stream().map((Function<? super Object, ?>)_$$Lambda$hBSizG3ais67bSjAeIqNEa6sDBo.INSTANCE).collect((Collector<? super Object, ?, List<? extends CandidateInfo>>)Collectors.toList());
    }
    
    @Override
    protected String getDefaultKey() {
        return this.mBackend.getActiveDream().flattenToString();
    }
    
    @Override
    public int getMetricsCategory() {
        return 47;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082737;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mBackend = DreamBackend.getInstance(context);
    }
    
    @Override
    protected void onSelectionPerformed(final boolean b) {
        super.onSelectionPerformed(b);
        this.getActivity().finish();
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        final Map<String, ComponentName> dreamComponentsMap = this.getDreamComponentsMap();
        if (dreamComponentsMap.get(s) != null) {
            this.mBackend.setActiveDream(dreamComponentsMap.get(s));
            return true;
        }
        return false;
    }
    
    private static final class DreamCandidateInfo extends CandidateInfo
    {
        private final Drawable icon;
        private final String key;
        private final CharSequence name;
        
        DreamCandidateInfo(final DreamBackend.DreamInfo dreamInfo) {
            super(true);
            this.name = dreamInfo.caption;
            this.icon = dreamInfo.icon;
            this.key = dreamInfo.componentName.flattenToString();
        }
        
        @Override
        public String getKey() {
            return this.key;
        }
        
        @Override
        public Drawable loadIcon() {
            return this.icon;
        }
        
        @Override
        public CharSequence loadLabel() {
            return this.name;
        }
    }
}
