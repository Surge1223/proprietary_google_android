package com.android.settings.dream;

import com.android.settingslib.dream.DreamBackend;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class DreamSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082769;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context) {
        final ArrayList<WhenToDreamPreferenceController> list = (ArrayList<WhenToDreamPreferenceController>)new ArrayList<StartNowPreferenceController>();
        list.add((StartNowPreferenceController)new CurrentDreamPreferenceController(context));
        list.add((StartNowPreferenceController)new WhenToDreamPreferenceController(context));
        list.add(new StartNowPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    static int getDreamSettingDescriptionResId(final int n) {
        switch (n) {
            default: {
                return 2131888936;
            }
            case 2: {
                return 2131888934;
            }
            case 1: {
                return 2131888933;
            }
            case 0: {
                return 2131888938;
            }
        }
    }
    
    static String getKeyFromSetting(final int n) {
        switch (n) {
            default: {
                return "never";
            }
            case 2: {
                return "either_charging_or_docked";
            }
            case 1: {
                return "while_docked_only";
            }
            case 0: {
                return "while_charging_only";
            }
        }
    }
    
    static int getSettingFromPrefKey(final String s) {
        final int hashCode = s.hashCode();
        int n = 0;
        Label_0090: {
            if (hashCode != -1592701525) {
                if (hashCode != -294641318) {
                    if (hashCode != 104712844) {
                        if (hashCode == 1019349036) {
                            if (s.equals("while_charging_only")) {
                                n = 0;
                                break Label_0090;
                            }
                        }
                    }
                    else if (s.equals("never")) {
                        n = 3;
                        break Label_0090;
                    }
                }
                else if (s.equals("either_charging_or_docked")) {
                    n = 2;
                    break Label_0090;
                }
            }
            else if (s.equals("while_docked_only")) {
                n = 1;
                break Label_0090;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return 3;
            }
            case 2: {
                return 2;
            }
            case 1: {
                return 1;
            }
            case 0: {
                return 0;
            }
        }
    }
    
    static CharSequence getSummaryTextFromBackend(final DreamBackend dreamBackend, final Context context) {
        if (!dreamBackend.isEnabled()) {
            return context.getString(2131888937);
        }
        return dreamBackend.getActiveDreamName();
    }
    
    public static CharSequence getSummaryTextWithDreamName(final Context context) {
        return getSummaryTextFromBackend(DreamBackend.getInstance(context), context);
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887827;
    }
    
    @Override
    protected String getLogTag() {
        return "DreamSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 47;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082769;
    }
}
