package com.android.settings.datausage;

import android.net.Uri;
import android.provider.Settings;
import android.os.Handler;
import android.os.Looper;
import android.database.ContentObserver;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v7.preference.Preference;
import android.net.NetworkTemplate;
import com.android.settings.Utils;
import com.android.settings.overlay.FeatureFactory;
import android.os.Parcelable;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Checkable;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import java.util.Iterator;
import java.util.List;
import android.telephony.SubscriptionInfo;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionManager$OnSubscriptionsChangedListener;
import com.android.settingslib.CustomDialogPreference;

public class CellDataPreference extends CustomDialogPreference implements TemplatePreference
{
    public boolean mChecked;
    private final DataStateListener mDataStateListener;
    public boolean mMultiSimDialog;
    final SubscriptionManager$OnSubscriptionsChangedListener mOnSubscriptionsChangeListener;
    public int mSubId;
    SubscriptionManager mSubscriptionManager;
    private TelephonyManager mTelephonyManager;
    
    public CellDataPreference(final Context context, final AttributeSet set) {
        super(context, set, TypedArrayUtils.getAttr(context, R.attr.switchPreferenceStyle, 16843629));
        this.mSubId = -1;
        this.mOnSubscriptionsChangeListener = new SubscriptionManager$OnSubscriptionsChangedListener() {
            public void onSubscriptionsChanged() {
                CellDataPreference.this.updateEnabled();
            }
        };
        this.mDataStateListener = (DataStateListener)new DataStateListener() {
            public void onChange(final boolean b) {
                CellDataPreference.this.updateChecked();
            }
        };
    }
    
    private void disableDataForOtherSubscriptions(final int n) {
        final List activeSubscriptionInfoList = this.mSubscriptionManager.getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList != null) {
            for (final SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                if (subscriptionInfo.getSubscriptionId() != n) {
                    this.mTelephonyManager.setDataEnabled(subscriptionInfo.getSubscriptionId(), false);
                }
            }
        }
    }
    
    private void setChecked(final boolean mChecked) {
        if (this.mChecked == mChecked) {
            return;
        }
        this.mChecked = mChecked;
        this.notifyChanged();
    }
    
    private void setMobileDataEnabled(final boolean checked) {
        this.mTelephonyManager.setDataEnabled(this.mSubId, checked);
        this.setChecked(checked);
    }
    
    private void showDisableDialog(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        alertDialog$Builder.setTitle((CharSequence)null).setMessage(2131887241).setPositiveButton(17039370, dialogInterface$OnClickListener).setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
    }
    
    private void showMultiSimDialog(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        final SubscriptionInfo activeSubscriptionInfo = this.mSubscriptionManager.getActiveSubscriptionInfo(this.mSubId);
        final SubscriptionInfo defaultDataSubscriptionInfo = this.mSubscriptionManager.getDefaultDataSubscriptionInfo();
        String s;
        if (defaultDataSubscriptionInfo == null) {
            s = this.getContext().getResources().getString(2131889165);
        }
        else {
            s = defaultDataSubscriptionInfo.getDisplayName().toString();
        }
        alertDialog$Builder.setTitle(2131889124);
        final Context context = this.getContext();
        CharSequence displayName;
        if (activeSubscriptionInfo != null) {
            displayName = activeSubscriptionInfo.getDisplayName();
        }
        else {
            displayName = null;
        }
        alertDialog$Builder.setMessage((CharSequence)context.getString(2131889123, new Object[] { String.valueOf(displayName), s }));
        alertDialog$Builder.setPositiveButton(R.string.okay, dialogInterface$OnClickListener);
        alertDialog$Builder.setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
    }
    
    private void updateChecked() {
        this.setChecked(this.mTelephonyManager.getDataEnabled(this.mSubId));
    }
    
    private void updateEnabled() {
        this.setEnabled(this.mSubscriptionManager.getActiveSubscriptionInfo(this.mSubId) != null);
    }
    
    @Override
    public void onAttached() {
        super.onAttached();
        this.mDataStateListener.setListener(true, this.mSubId, this.getContext());
        if (this.mSubscriptionManager != null) {
            this.mSubscriptionManager.addOnSubscriptionsChangedListener(this.mOnSubscriptionsChangeListener);
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(16908352);
        viewById.setClickable(false);
        ((Checkable)viewById).setChecked(this.mChecked);
    }
    
    @Override
    protected void onClick(final DialogInterface dialogInterface, final int n) {
        if (n != -1) {
            return;
        }
        if (this.mMultiSimDialog) {
            this.mSubscriptionManager.setDefaultDataSubId(this.mSubId);
            this.setMobileDataEnabled(true);
            this.disableDataForOtherSubscriptions(this.mSubId);
        }
        else {
            this.setMobileDataEnabled(false);
        }
    }
    
    @Override
    public void onDetached() {
        this.mDataStateListener.setListener(false, this.mSubId, this.getContext());
        if (this.mSubscriptionManager != null) {
            this.mSubscriptionManager.removeOnSubscriptionsChangedListener(this.mOnSubscriptionsChangeListener);
        }
        super.onDetached();
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        if (this.mMultiSimDialog) {
            this.showMultiSimDialog(alertDialog$Builder, dialogInterface$OnClickListener);
        }
        else {
            this.showDisableDialog(alertDialog$Builder, dialogInterface$OnClickListener);
        }
    }
    
    @Override
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        final CellDataState cellDataState = (CellDataState)parcelable;
        super.onRestoreInstanceState(cellDataState.getSuperState());
        this.mTelephonyManager = TelephonyManager.from(this.getContext());
        this.mChecked = cellDataState.mChecked;
        this.mMultiSimDialog = cellDataState.mMultiSimDialog;
        if (this.mSubId == -1) {
            this.mSubId = cellDataState.mSubId;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getKey());
            sb.append(this.mSubId);
            this.setKey(sb.toString());
        }
        this.notifyChanged();
    }
    
    @Override
    protected Parcelable onSaveInstanceState() {
        final CellDataState cellDataState = new CellDataState(super.onSaveInstanceState());
        cellDataState.mChecked = this.mChecked;
        cellDataState.mMultiSimDialog = this.mMultiSimDialog;
        cellDataState.mSubId = this.mSubId;
        return (Parcelable)cellDataState;
    }
    
    @Override
    protected void performClick(final View view) {
        final Context context = this.getContext();
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 178, this.mChecked ^ true);
        final SubscriptionInfo activeSubscriptionInfo = this.mSubscriptionManager.getActiveSubscriptionInfo(this.mSubId);
        final SubscriptionInfo defaultDataSubscriptionInfo = this.mSubscriptionManager.getDefaultDataSubscriptionInfo();
        if (this.mChecked) {
            if (!Utils.showSimCardTile(this.getContext()) || (defaultDataSubscriptionInfo != null && activeSubscriptionInfo != null && activeSubscriptionInfo.getSubscriptionId() == defaultDataSubscriptionInfo.getSubscriptionId())) {
                this.setMobileDataEnabled(false);
                if (defaultDataSubscriptionInfo != null && activeSubscriptionInfo != null && activeSubscriptionInfo.getSubscriptionId() == defaultDataSubscriptionInfo.getSubscriptionId()) {
                    this.disableDataForOtherSubscriptions(this.mSubId);
                }
                return;
            }
            this.mMultiSimDialog = false;
            super.performClick(view);
        }
        else if (Utils.showSimCardTile(this.getContext())) {
            this.mMultiSimDialog = true;
            if (defaultDataSubscriptionInfo != null && activeSubscriptionInfo != null && activeSubscriptionInfo.getSubscriptionId() == defaultDataSubscriptionInfo.getSubscriptionId()) {
                this.setMobileDataEnabled(true);
                this.disableDataForOtherSubscriptions(this.mSubId);
                return;
            }
            super.performClick(view);
        }
        else {
            this.setMobileDataEnabled(true);
        }
    }
    
    @Override
    public void setTemplate(final NetworkTemplate networkTemplate, final int mSubId, final NetworkServices networkServices) {
        if (mSubId != -1) {
            this.mSubscriptionManager = SubscriptionManager.from(this.getContext());
            this.mTelephonyManager = TelephonyManager.from(this.getContext());
            this.mSubscriptionManager.addOnSubscriptionsChangedListener(this.mOnSubscriptionsChangeListener);
            if (this.mSubId == -1) {
                this.mSubId = mSubId;
                final StringBuilder sb = new StringBuilder();
                sb.append(this.getKey());
                sb.append(mSubId);
                this.setKey(sb.toString());
            }
            this.updateEnabled();
            this.updateChecked();
            return;
        }
        throw new IllegalArgumentException("CellDataPreference needs a SubscriptionInfo");
    }
    
    public static class CellDataState extends BaseSavedState
    {
        public static final Parcelable.Creator<CellDataState> CREATOR;
        public boolean mChecked;
        public boolean mMultiSimDialog;
        public int mSubId;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<CellDataState>() {
                public CellDataState createFromParcel(final Parcel parcel) {
                    return new CellDataState(parcel);
                }
                
                public CellDataState[] newArray(final int n) {
                    return new CellDataState[n];
                }
            };
        }
        
        public CellDataState(final Parcel parcel) {
            super(parcel);
            final byte byte1 = parcel.readByte();
            final boolean b = false;
            this.mChecked = (byte1 != 0);
            boolean mMultiSimDialog = b;
            if (parcel.readByte() != 0) {
                mMultiSimDialog = true;
            }
            this.mMultiSimDialog = mMultiSimDialog;
            this.mSubId = parcel.readInt();
        }
        
        public CellDataState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeByte((byte)(byte)(this.mChecked ? 1 : 0));
            parcel.writeByte((byte)(byte)(this.mMultiSimDialog ? 1 : 0));
            parcel.writeInt(this.mSubId);
        }
    }
    
    public abstract static class DataStateListener extends ContentObserver
    {
        public DataStateListener() {
            super(new Handler(Looper.getMainLooper()));
        }
        
        public void setListener(final boolean b, final int n, final Context context) {
            if (b) {
                Uri uri = Settings.Global.getUriFor("mobile_data");
                if (TelephonyManager.getDefault().getSimCount() != 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("mobile_data");
                    sb.append(n);
                    uri = Settings.Global.getUriFor(sb.toString());
                }
                context.getContentResolver().registerContentObserver(uri, false, (ContentObserver)this);
            }
            else {
                context.getContentResolver().unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
