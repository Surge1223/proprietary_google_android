package com.android.settings.datausage;

import java.util.Objects;
import android.widget.Button;
import android.widget.ProgressBar;
import android.text.format.Formatter$BytesResult;
import android.text.style.AbsoluteSizeSpan;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.support.v7.preference.PreferenceViewHolder;
import android.text.TextUtils;
import com.android.settingslib.utils.StringUtil;
import com.android.settingslib.Utils;
import android.widget.TextView;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import android.net.NetworkTemplate;
import android.os.Bundle;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.content.Intent;
import com.android.internal.annotations.VisibleForTesting;
import android.graphics.Typeface;
import android.support.v7.preference.Preference;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$DataUsageSummaryPreference$1NKWVGupHVFnsudApVgFBRMGUJg implements View.OnClickListener
{
    public final void onClick(final View view) {
        DataUsageSummaryPreference.lambda$onBindViewHolder$1(this.f$0, view);
    }
}
