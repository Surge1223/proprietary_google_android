package com.android.settings.datausage;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v7.preference.Preference;
import android.util.ArraySet;
import com.android.settingslib.utils.AsyncLoader;

public class AppPrefLoader extends AsyncLoader<ArraySet<Preference>>
{
    private PackageManager mPackageManager;
    private ArraySet<String> mPackages;
    private Context mPrefContext;
    
    public AppPrefLoader(final Context mPrefContext, final ArraySet<String> mPackages, final PackageManager mPackageManager) {
        super(mPrefContext);
        this.mPackages = mPackages;
        this.mPackageManager = mPackageManager;
        this.mPrefContext = mPrefContext;
    }
    
    public ArraySet<Preference> loadInBackground() {
        final ArraySet set = new ArraySet();
        for (int i = 1; i < this.mPackages.size(); ++i) {
            try {
                final ApplicationInfo applicationInfo = this.mPackageManager.getApplicationInfo((String)this.mPackages.valueAt(i), 0);
                final Preference preference = new Preference(this.mPrefContext);
                preference.setIcon(applicationInfo.loadIcon(this.mPackageManager));
                preference.setTitle(applicationInfo.loadLabel(this.mPackageManager));
                preference.setSelectable(false);
                set.add((Object)preference);
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        return (ArraySet<Preference>)set;
    }
    
    @Override
    protected void onDiscardResult(final ArraySet<Preference> set) {
    }
}
