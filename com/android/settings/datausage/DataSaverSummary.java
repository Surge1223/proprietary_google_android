package com.android.settings.datausage;

import android.widget.Switch;
import java.util.ArrayList;
import android.arch.lifecycle.Lifecycle;
import android.app.Application;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.widget.SwitchBar;
import com.android.settings.applications.AppStateBaseBridge;
import com.android.settings.SettingsPreferenceFragment;

public class DataSaverSummary extends SettingsPreferenceFragment implements Callback, Listener, OnSwitchChangeListener, Callbacks
{
    private ApplicationsState mApplicationsState;
    private DataSaverBackend mDataSaverBackend;
    private AppStateDataUsageBridge mDataUsageBridge;
    private Session mSession;
    private SwitchBar mSwitchBar;
    private boolean mSwitching;
    private Preference mUnrestrictedAccess;
    
    @Override
    public int getHelpResource() {
        return 2131887805;
    }
    
    @Override
    public int getMetricsCategory() {
        return 348;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        (this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar()).setSwitchBarText(2131887209, 2131887209);
        this.mSwitchBar.show();
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onBlacklistStatusChanged(final int n, final boolean b) {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082738);
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(17039739);
        this.mUnrestrictedAccess = this.findPreference("unrestricted_access");
        this.mApplicationsState = ApplicationsState.getInstance((Application)this.getContext().getApplicationContext());
        this.mDataSaverBackend = new DataSaverBackend(this.getContext());
        this.mDataUsageBridge = new AppStateDataUsageBridge(this.mApplicationsState, this, this.mDataSaverBackend);
        this.mSession = this.mApplicationsState.newSession((ApplicationsState.Callbacks)this, this.getLifecycle());
    }
    
    @Override
    public void onDataSaverChanged(final boolean checked) {
        synchronized (this) {
            this.mSwitchBar.setChecked(checked);
            this.mSwitching = false;
        }
    }
    
    @Override
    public void onExtraInfoUpdated() {
        if (!this.isAdded()) {
            return;
        }
        final ArrayList<AppEntry> allApps = this.mSession.getAllApps();
        final int size = allApps.size();
        int n = 0;
        int n2;
        for (int i = 0; i < size; ++i, n = n2) {
            final AppEntry appEntry = allApps.get(i);
            if (!ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER.filterApp(appEntry)) {
                n2 = n;
            }
            else {
                n2 = n;
                if (appEntry.extraInfo != null) {
                    n2 = n;
                    if (((AppStateDataUsageBridge.DataUsageState)appEntry.extraInfo).isDataSaverWhitelisted) {
                        n2 = n + 1;
                    }
                }
            }
        }
        this.mUnrestrictedAccess.setSummary(this.getResources().getQuantityString(2131755024, n, new Object[] { n }));
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mDataSaverBackend.remListener((DataSaverBackend.Listener)this);
        this.mDataUsageBridge.pause();
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mDataSaverBackend.refreshWhitelist();
        this.mDataSaverBackend.refreshBlacklist();
        this.mDataSaverBackend.addListener((DataSaverBackend.Listener)this);
        this.mDataUsageBridge.resume();
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean dataSaverEnabled) {
        synchronized (this) {
            if (this.mSwitching) {
                return;
            }
            this.mSwitching = true;
            this.mDataSaverBackend.setDataSaverEnabled(dataSaverEnabled);
        }
    }
    
    @Override
    public void onWhitelistStatusChanged(final int n, final boolean b) {
    }
}
