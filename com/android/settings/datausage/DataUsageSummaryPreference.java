package com.android.settings.datausage;

import java.util.Objects;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.text.format.Formatter$BytesResult;
import android.text.style.AbsoluteSizeSpan;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.support.v7.preference.PreferenceViewHolder;
import android.text.TextUtils;
import com.android.settingslib.utils.StringUtil;
import com.android.settingslib.Utils;
import android.widget.TextView;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import android.net.NetworkTemplate;
import android.os.Bundle;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.content.Intent;
import com.android.internal.annotations.VisibleForTesting;
import android.graphics.Typeface;
import android.support.v7.preference.Preference;

public class DataUsageSummaryPreference extends Preference
{
    private static final long MILLIS_IN_A_DAY;
    @VisibleForTesting
    static final Typeface SANS_SERIF_MEDIUM;
    private static final long WARNING_AGE;
    private CharSequence mCarrierName;
    private boolean mChartEnabled;
    private long mCycleEndTimeMs;
    private long mDataplanSize;
    private long mDataplanUse;
    private CharSequence mEndLabel;
    private boolean mHasMobileData;
    private Intent mLaunchIntent;
    private String mLimitInfoText;
    private int mNumPlans;
    private float mProgress;
    private long mSnapshotTimeMs;
    private CharSequence mStartLabel;
    private String mUsagePeriod;
    private boolean mWifiMode;
    
    static {
        MILLIS_IN_A_DAY = TimeUnit.DAYS.toMillis(1L);
        WARNING_AGE = TimeUnit.HOURS.toMillis(6L);
        SANS_SERIF_MEDIUM = Typeface.create("sans-serif-medium", 0);
    }
    
    public DataUsageSummaryPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChartEnabled = true;
        this.setLayoutResource(2131558533);
    }
    
    private long calculateTruncatedUpdateAge() {
        final long n = System.currentTimeMillis() - this.mSnapshotTimeMs;
        if (n >= TimeUnit.DAYS.toMillis(1L)) {
            return n / TimeUnit.DAYS.toMillis(1L) * TimeUnit.DAYS.toMillis(1L);
        }
        if (n >= TimeUnit.HOURS.toMillis(1L)) {
            return n / TimeUnit.HOURS.toMillis(1L) * TimeUnit.HOURS.toMillis(1L);
        }
        if (n >= TimeUnit.MINUTES.toMillis(1L)) {
            return n / TimeUnit.MINUTES.toMillis(1L) * TimeUnit.MINUTES.toMillis(1L);
        }
        return 0L;
    }
    
    private static void launchWifiDataUsage(final Context context) {
        final Bundle arguments = new Bundle(1);
        arguments.putParcelable("network_template", (Parcelable)NetworkTemplate.buildTemplateWifiWildcard());
        final SubSettingLauncher setSourceMetricsCategory = new SubSettingLauncher(context).setArguments(arguments).setDestination(DataUsageList.class.getName()).setSourceMetricsCategory(0);
        setSourceMetricsCategory.setTitle(context.getString(2131889944));
        setSourceMetricsCategory.launch();
    }
    
    private void setCarrierInfoTextStyle(final TextView textView, final int n, final Typeface typeface) {
        textView.setTextColor(Utils.getColorAttr(this.getContext(), n));
        textView.setTypeface(typeface);
    }
    
    private void updateCarrierInfo(final TextView textView) {
        if (this.mNumPlans > 0 && this.mSnapshotTimeMs >= 0L) {
            textView.setVisibility(0);
            final long calculateTruncatedUpdateAge = this.calculateTruncatedUpdateAge();
            CharSequence formatElapsedTime = null;
            int n;
            if (calculateTruncatedUpdateAge == 0L) {
                if (this.mCarrierName != null) {
                    n = 2131886973;
                }
                else {
                    n = 2131888410;
                }
            }
            else {
                if (this.mCarrierName != null) {
                    n = 2131886974;
                }
                else {
                    n = 2131888411;
                }
                formatElapsedTime = StringUtil.formatElapsedTime(this.getContext(), calculateTruncatedUpdateAge, false);
            }
            textView.setText(TextUtils.expandTemplate(this.getContext().getText(n), new CharSequence[] { this.mCarrierName, formatElapsedTime }));
            if (calculateTruncatedUpdateAge <= DataUsageSummaryPreference.WARNING_AGE) {
                this.setCarrierInfoTextStyle(textView, 16842808, Typeface.SANS_SERIF);
            }
            else {
                this.setCarrierInfoTextStyle(textView, 16844099, DataUsageSummaryPreference.SANS_SERIF_MEDIUM);
            }
        }
        else {
            textView.setVisibility(8);
        }
    }
    
    private void updateCycleTimeText(final PreferenceViewHolder preferenceViewHolder) {
        final TextView textView = (TextView)preferenceViewHolder.findViewById(2131362033);
        final long n = this.mCycleEndTimeMs - System.currentTimeMillis();
        if (n <= 0L) {
            textView.setText((CharSequence)this.getContext().getString(2131886684));
        }
        else {
            final int n2 = (int)(n / DataUsageSummaryPreference.MILLIS_IN_A_DAY);
            String text;
            if (n2 < 1) {
                text = this.getContext().getString(2131886683);
            }
            else {
                text = this.getContext().getResources().getQuantityString(2131755023, n2, new Object[] { n2 });
            }
            textView.setText((CharSequence)text);
        }
    }
    
    private void updateDataUsageLabels(final PreferenceViewHolder preferenceViewHolder) {
        final TextView textView = (TextView)preferenceViewHolder.findViewById(2131362049);
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(this.getContext().getResources(), this.mDataplanUse, 10);
        final SpannableString spannableString = new SpannableString((CharSequence)formatBytes.value);
        spannableString.setSpan((Object)new AbsoluteSizeSpan(this.getContext().getResources().getDimensionPixelSize(2131165690)), 0, spannableString.length(), 33);
        textView.setText(TextUtils.expandTemplate(this.getContext().getText(2131887303), new CharSequence[] { spannableString, formatBytes.units }));
        final MeasurableLinearLayout measurableLinearLayout = (MeasurableLinearLayout)preferenceViewHolder.findViewById(2131362781);
        if (this.mHasMobileData && this.mNumPlans >= 0 && this.mDataplanSize > 0L) {
            final TextView textView2 = (TextView)preferenceViewHolder.findViewById(2131362044);
            final long n = this.mDataplanSize - this.mDataplanUse;
            if (n >= 0L) {
                final CharSequence expandTemplate = TextUtils.expandTemplate(this.getContext().getText(2131887205), new CharSequence[] { DataUsageUtils.formatDataUsage(this.getContext(), n) });
                final TextView textView3 = textView2;
                textView3.setText(expandTemplate);
                textView3.setTextColor(Utils.getColorAttr(this.getContext(), 16843829));
            }
            else {
                final TextView textView4 = textView2;
                textView4.setText(TextUtils.expandTemplate(this.getContext().getText(2131887204), new CharSequence[] { DataUsageUtils.formatDataUsage(this.getContext(), -n) }));
                textView4.setTextColor(Utils.getColorAttr(this.getContext(), 16844099));
            }
            measurableLinearLayout.setChildren((View)textView, (View)textView2);
        }
        else {
            measurableLinearLayout.setChildren((View)textView, null);
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final ProgressBar progressBar = (ProgressBar)preferenceViewHolder.findViewById(2131362071);
        final boolean mChartEnabled = this.mChartEnabled;
        final int n = 8;
        if (mChartEnabled && (!TextUtils.isEmpty(this.mStartLabel) || !TextUtils.isEmpty(this.mEndLabel))) {
            progressBar.setVisibility(0);
            preferenceViewHolder.findViewById(2131362312).setVisibility(0);
            progressBar.setProgress((int)(this.mProgress * 100.0f));
            ((TextView)preferenceViewHolder.findViewById(16908308)).setText(this.mStartLabel);
            ((TextView)preferenceViewHolder.findViewById(16908309)).setText(this.mEndLabel);
        }
        else {
            progressBar.setVisibility(8);
            preferenceViewHolder.findViewById(2131362312).setVisibility(8);
        }
        this.updateDataUsageLabels(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(2131362783);
        final TextView textView2 = (TextView)preferenceViewHolder.findViewById(2131361966);
        final Button button = (Button)preferenceViewHolder.findViewById(2131362326);
        final TextView textView3 = (TextView)preferenceViewHolder.findViewById(2131362039);
        if (this.mWifiMode) {
            textView.setText(2131887301);
            textView.setVisibility(0);
            ((TextView)preferenceViewHolder.findViewById(2131362033)).setText((CharSequence)this.mUsagePeriod);
            textView2.setVisibility(8);
            textView3.setVisibility(8);
            button.setOnClickListener((View.OnClickListener)new _$$Lambda$DataUsageSummaryPreference$zBjNn20lFyV2SqYMtfKeIRkAo7w(this));
            button.setText(2131888021);
            button.setVisibility(0);
        }
        else {
            int visibility;
            if (this.mNumPlans > 1) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            textView.setVisibility(visibility);
            this.updateCycleTimeText(preferenceViewHolder);
            this.updateCarrierInfo(textView2);
            if (this.mLaunchIntent != null) {
                button.setOnClickListener((View.OnClickListener)new _$$Lambda$DataUsageSummaryPreference$1NKWVGupHVFnsudApVgFBRMGUJg(this));
                button.setVisibility(0);
            }
            else {
                button.setVisibility(8);
            }
            int visibility2;
            if (TextUtils.isEmpty((CharSequence)this.mLimitInfoText)) {
                visibility2 = n;
            }
            else {
                visibility2 = 0;
            }
            textView3.setVisibility(visibility2);
            textView3.setText((CharSequence)this.mLimitInfoText);
        }
    }
    
    public void setChartEnabled(final boolean mChartEnabled) {
        if (this.mChartEnabled != mChartEnabled) {
            this.mChartEnabled = mChartEnabled;
            this.notifyChanged();
        }
    }
    
    public void setLabels(final CharSequence mStartLabel, final CharSequence mEndLabel) {
        this.mStartLabel = mStartLabel;
        this.mEndLabel = mEndLabel;
        this.notifyChanged();
    }
    
    public void setLimitInfo(final String mLimitInfoText) {
        if (!Objects.equals(mLimitInfoText, this.mLimitInfoText)) {
            this.mLimitInfoText = mLimitInfoText;
            this.notifyChanged();
        }
    }
    
    public void setProgress(final float mProgress) {
        this.mProgress = mProgress;
        this.notifyChanged();
    }
    
    public void setUsageInfo(final long mCycleEndTimeMs, final long mSnapshotTimeMs, final CharSequence mCarrierName, final int mNumPlans, final Intent mLaunchIntent) {
        this.mCycleEndTimeMs = mCycleEndTimeMs;
        this.mSnapshotTimeMs = mSnapshotTimeMs;
        this.mCarrierName = mCarrierName;
        this.mNumPlans = mNumPlans;
        this.mLaunchIntent = mLaunchIntent;
        this.notifyChanged();
    }
    
    void setUsageNumbers(final long mDataplanUse, final long mDataplanSize, final boolean mHasMobileData) {
        this.mDataplanUse = mDataplanUse;
        this.mDataplanSize = mDataplanSize;
        this.mHasMobileData = mHasMobileData;
        this.notifyChanged();
    }
    
    void setWifiMode(final boolean mWifiMode, final String mUsagePeriod) {
        this.mWifiMode = mWifiMode;
        this.mUsagePeriod = mUsagePeriod;
        this.notifyChanged();
    }
}
