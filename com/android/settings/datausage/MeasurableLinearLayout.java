package com.android.settings.datausage;

import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

public class MeasurableLinearLayout extends LinearLayout
{
    private View mDisposableView;
    private View mFixedView;
    
    public MeasurableLinearLayout(final Context context) {
        super(context, (AttributeSet)null);
    }
    
    public MeasurableLinearLayout(final Context context, final AttributeSet set) {
        super(context, set, 0);
    }
    
    public MeasurableLinearLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n, 0);
    }
    
    public MeasurableLinearLayout(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (this.mDisposableView != null && this.getMeasuredWidth() - this.mFixedView.getMeasuredWidth() < this.mDisposableView.getMeasuredWidth()) {
            this.mDisposableView.setVisibility(8);
            super.onMeasure(n, n2);
        }
        else if (this.mDisposableView != null && this.mDisposableView.getVisibility() != 0) {
            this.mDisposableView.setVisibility(0);
            super.onMeasure(n, n2);
        }
    }
    
    public void setChildren(final View mFixedView, final View mDisposableView) {
        this.mFixedView = mFixedView;
        this.mDisposableView = mDisposableView;
    }
}
