package com.android.settings.datausage;

import com.android.settings.Utils;
import java.util.Iterator;
import android.net.NetworkStatsHistory$Entry;
import android.view.View;
import android.widget.AdapterView;
import java.util.Objects;
import java.time.ZonedDateTime;
import android.util.Pair;
import android.net.NetworkPolicyManager;
import com.android.settingslib.net.ChartData;
import android.net.NetworkPolicy;
import android.content.Context;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.ArrayAdapter;

public class CycleAdapter extends ArrayAdapter<CycleItem>
{
    private final AdapterView$OnItemSelectedListener mListener;
    private final SpinnerInterface mSpinner;
    
    public CycleAdapter(final Context context, final SpinnerInterface mSpinner, final AdapterView$OnItemSelectedListener mListener, final boolean b) {
        int n;
        if (b) {
            n = 2131558555;
        }
        else {
            n = 2131558527;
        }
        super(context, n);
        this.setDropDownViewResource(17367049);
        this.mSpinner = mSpinner;
        this.mListener = mListener;
        this.mSpinner.setAdapter(this);
        this.mSpinner.setOnItemSelectedListener(this.mListener);
    }
    
    public int findNearestPosition(final CycleItem cycleItem) {
        if (cycleItem != null) {
            for (int i = this.getCount() - 1; i >= 0; --i) {
                if (((CycleItem)this.getItem(i)).compareTo(cycleItem) >= 0) {
                    return i;
                }
            }
        }
        return 0;
    }
    
    public boolean updateCycleList(final NetworkPolicy networkPolicy, final ChartData chartData) {
        final CycleItem cycleItem = (CycleItem)this.mSpinner.getSelectedItem();
        this.clear();
        final Context context = this.getContext();
        final NetworkStatsHistory$Entry networkStatsHistory$Entry = null;
        final NetworkStatsHistory$Entry networkStatsHistory$Entry2 = null;
        long start = Long.MAX_VALUE;
        long end = Long.MIN_VALUE;
        if (chartData != null) {
            start = chartData.network.getStart();
            end = chartData.network.getEnd();
        }
        final long currentTimeMillis = System.currentTimeMillis();
        long n = start;
        if (start == Long.MAX_VALUE) {
            n = currentTimeMillis;
        }
        long n2 = end;
        if (end == Long.MIN_VALUE) {
            n2 = currentTimeMillis + 1L;
        }
        boolean b;
        NetworkStatsHistory$Entry networkStatsHistory$Entry3;
        if (networkPolicy != null) {
            final Iterator cycleIterator = NetworkPolicyManager.cycleIterator(networkPolicy);
            b = false;
            networkStatsHistory$Entry3 = networkStatsHistory$Entry2;
            while (cycleIterator.hasNext()) {
                final Pair pair = cycleIterator.next();
                final long epochMilli = ((ZonedDateTime)pair.first).toInstant().toEpochMilli();
                final long epochMilli2 = ((ZonedDateTime)pair.second).toInstant().toEpochMilli();
                boolean b2;
                if (chartData != null) {
                    networkStatsHistory$Entry3 = chartData.network.getValues(epochMilli, epochMilli2, networkStatsHistory$Entry3);
                    b2 = (networkStatsHistory$Entry3.rxBytes + networkStatsHistory$Entry3.txBytes > 0L);
                }
                else {
                    b2 = true;
                }
                if (b2) {
                    this.add((Object)new CycleItem(context, epochMilli, epochMilli2));
                    b = true;
                }
            }
        }
        else {
            b = false;
            networkStatsHistory$Entry3 = networkStatsHistory$Entry;
        }
        if (!b) {
            while (n2 > n) {
                final long n3 = n2 - 2419200000L;
                boolean b3;
                if (chartData != null) {
                    networkStatsHistory$Entry3 = chartData.network.getValues(n3, n2, networkStatsHistory$Entry3);
                    b3 = (networkStatsHistory$Entry3.rxBytes + networkStatsHistory$Entry3.txBytes > 0L);
                }
                else {
                    b3 = true;
                }
                if (b3) {
                    this.add((Object)new CycleItem(context, n3, n2));
                }
                n2 = n3;
            }
        }
        if (this.getCount() > 0) {
            final int nearestPosition = this.findNearestPosition(cycleItem);
            this.mSpinner.setSelection(nearestPosition);
            if (!Objects.equals(this.getItem(nearestPosition), cycleItem)) {
                this.mListener.onItemSelected((AdapterView)null, (View)null, nearestPosition, 0L);
                return false;
            }
        }
        return true;
    }
    
    public static class CycleItem implements Comparable<CycleItem>
    {
        public long end;
        public CharSequence label;
        public long start;
        
        public CycleItem(final Context context, final long start, final long end) {
            this.label = Utils.formatDateRange(context, start, end);
            this.start = start;
            this.end = end;
        }
        
        @Override
        public int compareTo(final CycleItem cycleItem) {
            return Long.compare(this.start, cycleItem.start);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof CycleItem;
            final boolean b2 = false;
            if (b) {
                final CycleItem cycleItem = (CycleItem)o;
                boolean b3 = b2;
                if (this.start == cycleItem.start) {
                    b3 = b2;
                    if (this.end == cycleItem.end) {
                        b3 = true;
                    }
                }
                return b3;
            }
            return false;
        }
        
        @Override
        public String toString() {
            return this.label.toString();
        }
    }
    
    public interface SpinnerInterface
    {
        Object getSelectedItem();
        
        void setAdapter(final CycleAdapter p0);
        
        void setOnItemSelectedListener(final AdapterView$OnItemSelectedListener p0);
        
        void setSelection(final int p0);
    }
}
