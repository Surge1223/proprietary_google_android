package com.android.settings.datausage;

import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.utils.ThreadUtils;
import android.os.RemoteException;
import android.net.INetworkPolicyListener$Stub;
import android.util.SparseIntArray;
import android.net.NetworkPolicyManager;
import android.net.INetworkPolicyListener;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import java.util.ArrayList;
import android.content.Context;

public class DataSaverBackend
{
    private boolean mBlacklistInitialized;
    private final Context mContext;
    private final ArrayList<Listener> mListeners;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final INetworkPolicyListener mPolicyListener;
    private final NetworkPolicyManager mPolicyManager;
    private SparseIntArray mUidPolicies;
    private boolean mWhitelistInitialized;
    
    public DataSaverBackend(final Context mContext) {
        this.mListeners = new ArrayList<Listener>();
        this.mUidPolicies = new SparseIntArray();
        this.mPolicyListener = (INetworkPolicyListener)new INetworkPolicyListener$Stub() {
            public void onMeteredIfacesChanged(final String[] array) throws RemoteException {
            }
            
            public void onRestrictBackgroundChanged(final boolean b) throws RemoteException {
                ThreadUtils.postOnMainThread(new _$$Lambda$DataSaverBackend$1$1851XOwRm2qYDEpp81v4WIVwIHs(this, b));
            }
            
            public void onSubscriptionOverride(final int n, final int n2, final int n3) {
            }
            
            public void onUidPoliciesChanged(final int n, final int n2) {
                ThreadUtils.postOnMainThread(new _$$Lambda$DataSaverBackend$1$ZKxRzvcfxNqOKXdNkDOsoxL7i9I(this, n, n2));
            }
            
            public void onUidRulesChanged(final int n, final int n2) throws RemoteException {
            }
        };
        this.mContext = mContext;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(mContext).getMetricsFeatureProvider();
        this.mPolicyManager = NetworkPolicyManager.from(mContext);
    }
    
    private void handleBlacklistChanged(final int n, final boolean b) {
        for (int i = 0; i < this.mListeners.size(); ++i) {
            this.mListeners.get(i).onBlacklistStatusChanged(n, b);
        }
    }
    
    private void handleRestrictBackgroundChanged(final boolean b) {
        for (int i = 0; i < this.mListeners.size(); ++i) {
            this.mListeners.get(i).onDataSaverChanged(b);
        }
    }
    
    private void handleUidPoliciesChanged(final int n, final int n2) {
        this.loadWhitelist();
        this.loadBlacklist();
        final SparseIntArray mUidPolicies = this.mUidPolicies;
        boolean b = false;
        final int value = mUidPolicies.get(n, 0);
        if (n2 == 0) {
            this.mUidPolicies.delete(n);
        }
        else {
            this.mUidPolicies.put(n, n2);
        }
        final boolean b2 = value == 4;
        final boolean b3 = value == 1;
        final boolean b4 = n2 == 4;
        if (n2 == 1) {
            b = true;
        }
        if (b2 != b4) {
            this.handleWhitelistChanged(n, b4);
        }
        if (b3 != b) {
            this.handleBlacklistChanged(n, b);
        }
    }
    
    private void handleWhitelistChanged(final int n, final boolean b) {
        for (int i = 0; i < this.mListeners.size(); ++i) {
            this.mListeners.get(i).onWhitelistStatusChanged(n, b);
        }
    }
    
    private void loadBlacklist() {
        if (this.mBlacklistInitialized) {
            return;
        }
        final int[] uidsWithPolicy = this.mPolicyManager.getUidsWithPolicy(1);
        for (int length = uidsWithPolicy.length, i = 0; i < length; ++i) {
            this.mUidPolicies.put(uidsWithPolicy[i], 1);
        }
        this.mBlacklistInitialized = true;
    }
    
    private void loadWhitelist() {
        if (this.mWhitelistInitialized) {
            return;
        }
        final int[] uidsWithPolicy = this.mPolicyManager.getUidsWithPolicy(4);
        for (int length = uidsWithPolicy.length, i = 0; i < length; ++i) {
            this.mUidPolicies.put(uidsWithPolicy[i], 4);
        }
        this.mWhitelistInitialized = true;
    }
    
    public void addListener(final Listener listener) {
        this.mListeners.add(listener);
        if (this.mListeners.size() == 1) {
            this.mPolicyManager.registerListener(this.mPolicyListener);
        }
        listener.onDataSaverChanged(this.isDataSaverEnabled());
    }
    
    public int getWhitelistedCount() {
        int n = 0;
        this.loadWhitelist();
        int n2;
        for (int i = 0; i < this.mUidPolicies.size(); ++i, n = n2) {
            n2 = n;
            if (this.mUidPolicies.valueAt(i) == 4) {
                n2 = n + 1;
            }
        }
        return n;
    }
    
    public boolean isBlacklisted(final int n) {
        this.loadBlacklist();
        final SparseIntArray mUidPolicies = this.mUidPolicies;
        boolean b = false;
        if (mUidPolicies.get(n, 0) == 1) {
            b = true;
        }
        return b;
    }
    
    public boolean isDataSaverEnabled() {
        return this.mPolicyManager.getRestrictBackground();
    }
    
    public boolean isWhitelisted(final int n) {
        this.loadWhitelist();
        final SparseIntArray mUidPolicies = this.mUidPolicies;
        boolean b = false;
        if (mUidPolicies.get(n, 0) == 4) {
            b = true;
        }
        return b;
    }
    
    public void refreshBlacklist() {
        this.loadBlacklist();
    }
    
    public void refreshWhitelist() {
        this.loadWhitelist();
    }
    
    public void remListener(final Listener listener) {
        this.mListeners.remove(listener);
        if (this.mListeners.size() == 0) {
            this.mPolicyManager.unregisterListener(this.mPolicyListener);
        }
    }
    
    public void setDataSaverEnabled(final boolean restrictBackground) {
        this.mPolicyManager.setRestrictBackground(restrictBackground);
        this.mMetricsFeatureProvider.action(this.mContext, 394, restrictBackground ? 1 : 0);
    }
    
    public void setIsBlacklisted(final int n, final String s, final boolean b) {
        this.mPolicyManager.setUidPolicy(n, (int)(b ? 1 : 0));
        this.mUidPolicies.put(n, (int)(b ? 1 : 0));
        if ((b ? 1 : 0) != 0) {
            this.mMetricsFeatureProvider.action(this.mContext, 396, s, (Pair<Integer, Object>[])new Pair[0]);
        }
    }
    
    public void setIsWhitelisted(final int n, final String s, final boolean b) {
        int n2;
        if (b) {
            n2 = 4;
        }
        else {
            n2 = 0;
        }
        this.mPolicyManager.setUidPolicy(n, n2);
        this.mUidPolicies.put(n, n2);
        if (b) {
            this.mMetricsFeatureProvider.action(this.mContext, 395, s, (Pair<Integer, Object>[])new Pair[0]);
        }
    }
    
    public interface Listener
    {
        void onBlacklistStatusChanged(final int p0, final boolean p1);
        
        void onDataSaverChanged(final boolean p0);
        
        void onWhitelistStatusChanged(final int p0, final boolean p1);
    }
}
