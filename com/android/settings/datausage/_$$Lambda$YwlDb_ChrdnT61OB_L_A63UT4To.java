package com.android.settings.datausage;

import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$YwlDb_ChrdnT61OB_L_A63UT4To implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return new DataUsageSummary.SummaryProvider(activity, summaryLoader);
    }
}
