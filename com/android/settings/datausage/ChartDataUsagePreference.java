package com.android.settings.datausage;

import android.support.v7.preference.PreferenceViewHolder;
import android.net.NetworkStatsHistory$Entry;
import android.util.SparseIntArray;
import android.text.format.Formatter$BytesResult;
import android.text.style.ForegroundColorSpan;
import android.text.TextUtils;
import android.text.SpannableStringBuilder;
import android.text.format.Formatter;
import com.android.settings.graph.UsageView;
import com.android.settingslib.Utils;
import android.util.AttributeSet;
import android.content.Context;
import android.net.NetworkPolicy;
import android.net.NetworkStatsHistory;
import android.support.v7.preference.Preference;

public class ChartDataUsagePreference extends Preference
{
    private long mEnd;
    private final int mLimitColor;
    private NetworkStatsHistory mNetwork;
    private NetworkPolicy mPolicy;
    private int mSecondaryColor;
    private int mSeriesColor;
    private long mStart;
    private final int mWarningColor;
    
    public ChartDataUsagePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setSelectable(false);
        this.mLimitColor = Utils.getColorAttr(context, 16844099);
        this.mWarningColor = Utils.getColorAttr(context, 16842808);
        this.setLayoutResource(2131558530);
    }
    
    private void bindNetworkPolicy(final UsageView usageView, final NetworkPolicy networkPolicy, final int n) {
        final CharSequence[] sideLabels = new CharSequence[3];
        int mWarningColor = 0;
        int mLimitColor = 0;
        if (networkPolicy == null) {
            return;
        }
        if (networkPolicy.limitBytes != -1L) {
            mLimitColor = this.mLimitColor;
            sideLabels[2] = this.getLabel(networkPolicy.limitBytes, 2131887286, this.mLimitColor);
        }
        if (networkPolicy.warningBytes != -1L) {
            usageView.setDividerLoc((int)(networkPolicy.warningBytes / 524288L));
            final float n2 = networkPolicy.warningBytes / 524288L / n;
            usageView.setSideLabelWeights(1.0f - n2, n2);
            mWarningColor = this.mWarningColor;
            sideLabels[1] = this.getLabel(networkPolicy.warningBytes, 2131887287, this.mWarningColor);
        }
        usageView.setSideLabels(sideLabels);
        usageView.setDividerColors(mWarningColor, mLimitColor);
    }
    
    private CharSequence getLabel(final long n, final int n2, final int n3) {
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(this.getContext().getResources(), n, 9);
        return (CharSequence)new SpannableStringBuilder().append(TextUtils.expandTemplate(this.getContext().getText(n2), new CharSequence[] { formatBytes.value, formatBytes.units }), (Object)new ForegroundColorSpan(n3), 0);
    }
    
    private int toInt(final long n) {
        return (int)(n / 60000L);
    }
    
    void calcPoints(final UsageView usageView) {
        final SparseIntArray sparseIntArray = new SparseIntArray();
        final int indexAfter = this.mNetwork.getIndexAfter(this.mStart);
        final int indexAfter2 = this.mNetwork.getIndexAfter(this.mEnd);
        if (indexAfter < 0) {
            return;
        }
        sparseIntArray.put(0, 0);
        long n = 0L;
        NetworkStatsHistory$Entry values = null;
        for (int i = indexAfter; i <= indexAfter2; ++i) {
            values = this.mNetwork.getValues(i, values);
            final long bucketStart = values.bucketStart;
            final long bucketDuration = values.bucketDuration;
            n += values.rxBytes + values.txBytes;
            if (i == 0) {
                sparseIntArray.put(this.toInt(bucketStart - this.mStart) - 1, -1);
            }
            sparseIntArray.put(this.toInt(bucketStart - this.mStart + 1L), (int)(n / 524288L));
            sparseIntArray.put(this.toInt(bucketDuration + bucketStart - this.mStart), (int)(n / 524288L));
        }
        if (sparseIntArray.size() > 1) {
            usageView.addPath(sparseIntArray);
        }
    }
    
    public long getInspectEnd() {
        return this.mEnd;
    }
    
    public long getInspectStart() {
        return this.mStart;
    }
    
    public int getTop() {
        int i = this.mNetwork.getIndexBefore(this.mStart);
        final int indexAfter = this.mNetwork.getIndexAfter(this.mEnd);
        long n = 0L;
        NetworkStatsHistory$Entry values = null;
        while (i <= indexAfter) {
            values = this.mNetwork.getValues(i, values);
            n += values.rxBytes + values.txBytes;
            ++i;
        }
        long max;
        if (this.mPolicy != null) {
            max = Math.max(this.mPolicy.limitBytes, this.mPolicy.warningBytes);
        }
        else {
            max = 0L;
        }
        return (int)(Math.max(n, max) / 524288L);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final UsageView usageView = (UsageView)preferenceViewHolder.findViewById(2131362047);
        if (this.mNetwork == null) {
            return;
        }
        final int top = this.getTop();
        usageView.clearPaths();
        usageView.configureGraph(this.toInt(this.mEnd - this.mStart), top);
        this.calcPoints(usageView);
        usageView.setBottomLabels(new CharSequence[] { com.android.settings.Utils.formatDateRange(this.getContext(), this.mStart, this.mStart), com.android.settings.Utils.formatDateRange(this.getContext(), this.mEnd, this.mEnd) });
        this.bindNetworkPolicy(usageView, this.mPolicy, top);
    }
    
    public void setColors(final int mSeriesColor, final int mSecondaryColor) {
        this.mSeriesColor = mSeriesColor;
        this.mSecondaryColor = mSecondaryColor;
        this.notifyChanged();
    }
    
    public void setNetworkPolicy(final NetworkPolicy mPolicy) {
        this.mPolicy = mPolicy;
        this.notifyChanged();
    }
    
    public void setNetworkStats(final NetworkStatsHistory mNetwork) {
        this.mNetwork = mNetwork;
        this.notifyChanged();
    }
    
    public void setVisibleRange(final long mStart, final long mEnd) {
        this.mStart = mStart;
        this.mEnd = mEnd;
        this.notifyChanged();
    }
}
