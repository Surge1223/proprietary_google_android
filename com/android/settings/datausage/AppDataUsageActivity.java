package com.android.settings.datausage;

import android.content.pm.PackageManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.Parcelable;
import com.android.settingslib.AppItem;
import android.os.Bundle;
import com.android.settings.SettingsActivity;

public class AppDataUsageActivity extends SettingsActivity
{
    @Override
    protected boolean isValidFragment(final String s) {
        return super.isValidFragment(s) || AppDataUsage.class.getName().equals(s);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        final Intent intent = this.getIntent();
        Object schemeSpecificPart = intent.getData().getSchemeSpecificPart();
        final PackageManager packageManager = this.getPackageManager();
        try {
            final int packageUid = packageManager.getPackageUid((String)schemeSpecificPart, 0);
            final Bundle bundle2 = new Bundle();
            schemeSpecificPart = new AppItem(packageUid);
            ((AppItem)schemeSpecificPart).addUid(packageUid);
            bundle2.putParcelable("app_item", (Parcelable)schemeSpecificPart);
            intent.putExtra(":settings:show_fragment_args", bundle2);
            intent.putExtra(":settings:show_fragment", AppDataUsage.class.getName());
            intent.putExtra(":settings:show_fragment_title_resid", 2131886344);
            super.onCreate(bundle);
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("invalid package: ");
            sb.append((String)schemeSpecificPart);
            Log.w("AppDataUsageActivity", sb.toString());
            try {
                super.onCreate(bundle);
            }
            catch (Exception ex2) {}
            finally {
                this.finish();
            }
        }
    }
}
