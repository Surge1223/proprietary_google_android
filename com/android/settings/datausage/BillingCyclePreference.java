package com.android.settings.datausage;

import android.util.FeatureFlagUtils;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.Intent;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.content.Context;
import android.net.NetworkTemplate;
import android.support.v7.preference.Preference;

public class BillingCyclePreference extends Preference implements TemplatePreference
{
    private final CellDataPreference.DataStateListener mListener;
    private NetworkServices mServices;
    private int mSubId;
    private NetworkTemplate mTemplate;
    
    public BillingCyclePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mListener = new CellDataPreference.DataStateListener() {
            public void onChange(final boolean b) {
                BillingCyclePreference.this.updateEnabled();
            }
        };
    }
    
    private void updateEnabled() {
        try {
            this.setEnabled(this.mServices.mNetworkService.isBandwidthControlEnabled() && this.mServices.mTelephonyManager.getDataEnabled(this.mSubId) && this.mServices.mUserManager.isAdminUser());
        }
        catch (RemoteException ex) {
            this.setEnabled(false);
        }
    }
    
    @Override
    public Intent getIntent() {
        final Bundle arguments = new Bundle();
        arguments.putParcelable("network_template", (Parcelable)this.mTemplate);
        return new SubSettingLauncher(this.getContext()).setDestination(BillingCycleSettings.class.getName()).setArguments(arguments).setTitle(2131886681).setSourceMetricsCategory(0).toIntent();
    }
    
    @Override
    public void onAttached() {
        super.onAttached();
        this.mListener.setListener(true, this.mSubId, this.getContext());
    }
    
    @Override
    public void onDetached() {
        this.mListener.setListener(false, this.mSubId, this.getContext());
        super.onDetached();
    }
    
    @Override
    public void setTemplate(final NetworkTemplate mTemplate, int policyCycleDay, final NetworkServices mServices) {
        this.mTemplate = mTemplate;
        this.mSubId = policyCycleDay;
        this.mServices = mServices;
        policyCycleDay = mServices.mPolicyEditor.getPolicyCycleDay(this.mTemplate);
        if (FeatureFlagUtils.isEnabled(this.getContext(), "settings_data_usage_v2")) {
            this.setSummary(null);
        }
        else if (policyCycleDay != -1) {
            this.setSummary(this.getContext().getString(2131886682, new Object[] { policyCycleDay }));
        }
        else {
            this.setSummary(null);
        }
        this.setIntent(this.getIntent());
    }
}
