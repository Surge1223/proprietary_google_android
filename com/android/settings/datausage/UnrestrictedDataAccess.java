package com.android.settings.datausage;

import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.RestrictedPreferenceHelper;
import com.android.settings.widget.AppSwitchPreference;
import android.view.View;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.support.v7.preference.PreferenceGroup;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.arch.lifecycle.Lifecycle;
import android.app.Application;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import java.util.ArrayList;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.AppStateBaseBridge;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class UnrestrictedDataAccess extends SettingsPreferenceFragment implements OnPreferenceChangeListener, Callback, Callbacks
{
    private ApplicationsState mApplicationsState;
    private DataSaverBackend mDataSaverBackend;
    private AppStateDataUsageBridge mDataUsageBridge;
    private boolean mExtraLoaded;
    private AppFilter mFilter;
    private Session mSession;
    private boolean mShowSystem;
    
    private void rebuild() {
        final ArrayList<AppEntry> rebuild = this.mSession.rebuild(this.mFilter, ApplicationsState.ALPHA_COMPARATOR);
        if (rebuild != null) {
            this.onRebuildComplete(rebuild);
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887834;
    }
    
    @Override
    public int getMetricsCategory() {
        return 349;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082850;
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 781;
        }
        else {
            n = 782;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setAnimationAllowed(true);
        this.mApplicationsState = ApplicationsState.getInstance((Application)this.getContext().getApplicationContext());
        this.mDataSaverBackend = new DataSaverBackend(this.getContext());
        this.mDataUsageBridge = new AppStateDataUsageBridge(this.mApplicationsState, this, this.mDataSaverBackend);
        this.mSession = this.mApplicationsState.newSession((ApplicationsState.Callbacks)this, this.getLifecycle());
        this.mShowSystem = (bundle != null && bundle.getBoolean("show_system"));
        ApplicationsState.AppFilter mFilter;
        if (this.mShowSystem) {
            mFilter = ApplicationsState.FILTER_ALL_ENABLED;
        }
        else {
            mFilter = ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER;
        }
        this.mFilter = mFilter;
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        int n;
        if (this.mShowSystem) {
            n = 2131888295;
        }
        else {
            n = 2131888305;
        }
        menu.add(0, 43, 0, n);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mDataUsageBridge.release();
    }
    
    @Override
    public void onExtraInfoUpdated() {
        this.mExtraLoaded = true;
        this.rebuild();
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 43) {
            this.mShowSystem ^= true;
            int title;
            if (this.mShowSystem) {
                title = 2131888295;
            }
            else {
                title = 2131888305;
            }
            menuItem.setTitle(title);
            ApplicationsState.AppFilter mFilter;
            if (this.mShowSystem) {
                mFilter = ApplicationsState.FILTER_ALL_ENABLED;
            }
            else {
                mFilter = ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER;
            }
            this.mFilter = mFilter;
            if (this.mExtraLoaded) {
                this.rebuild();
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mDataUsageBridge.pause();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean b = preference instanceof AccessPreference;
        boolean isDataSaverWhitelisted = false;
        if (b) {
            final AccessPreference accessPreference = (AccessPreference)preference;
            if (o == Boolean.TRUE) {
                isDataSaverWhitelisted = true;
            }
            this.logSpecialPermissionChange(isDataSaverWhitelisted, accessPreference.mEntry.info.packageName);
            this.mDataSaverBackend.setIsWhitelisted(accessPreference.mEntry.info.uid, accessPreference.mEntry.info.packageName, isDataSaverWhitelisted);
            accessPreference.mState.isDataSaverWhitelisted = isDataSaverWhitelisted;
            return true;
        }
        return false;
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
        if (this.getContext() == null) {
            return;
        }
        this.cacheRemoveAllPrefs(this.getPreferenceScreen());
        for (int size = list.size(), i = 0; i < size; ++i) {
            final AppEntry appEntry = list.get(i);
            if (this.shouldAddPreference(appEntry)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(appEntry.info.packageName);
                sb.append("|");
                sb.append(appEntry.info.uid);
                final String string = sb.toString();
                AccessPreference accessPreference = (AccessPreference)this.getCachedPreference(string);
                if (accessPreference == null) {
                    accessPreference = new AccessPreference(this.getPrefContext(), appEntry);
                    accessPreference.setKey(string);
                    accessPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                    this.getPreferenceScreen().addPreference(accessPreference);
                }
                else {
                    accessPreference.setDisabledByAdmin(RestrictedLockUtils.checkIfMeteredDataRestricted(this.getContext(), appEntry.info.packageName, UserHandle.getUserId(appEntry.info.uid)));
                    accessPreference.reuse();
                }
                accessPreference.setOrder(i);
            }
        }
        this.setLoading(false, true);
        this.removeCachedPrefs(this.getPreferenceScreen());
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mDataUsageBridge.resume();
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("show_system", this.mShowSystem);
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.setLoading(true, false);
    }
    
    @VisibleForTesting
    boolean shouldAddPreference(final AppEntry appEntry) {
        return appEntry != null && UserHandle.isApp(appEntry.info.uid);
    }
    
    @VisibleForTesting
    class AccessPreference extends AppSwitchPreference implements DataSaverBackend.Listener
    {
        private final AppEntry mEntry;
        private final RestrictedPreferenceHelper mHelper;
        private final AppStateDataUsageBridge.DataUsageState mState;
        
        public AccessPreference(final Context context, final AppEntry mEntry) {
            super(context);
            this.setWidgetLayoutResource(R.layout.restricted_switch_widget);
            this.mHelper = new RestrictedPreferenceHelper(context, this, null);
            this.mEntry = mEntry;
            this.mState = (AppStateDataUsageBridge.DataUsageState)this.mEntry.extraInfo;
            this.mEntry.ensureLabel(this.getContext());
            this.setDisabledByAdmin(RestrictedLockUtils.checkIfMeteredDataRestricted(context, mEntry.info.packageName, UserHandle.getUserId(mEntry.info.uid)));
            this.setState();
            if (this.mEntry.icon != null) {
                this.setIcon(this.mEntry.icon);
            }
        }
        
        private void setState() {
            this.setTitle(this.mEntry.label);
            if (this.mState != null) {
                this.setChecked(this.mState.isDataSaverWhitelisted);
                if (this.isDisabledByAdmin()) {
                    this.setSummary(R.string.disabled_by_admin);
                }
                else if (this.mState.isDataSaverBlacklisted) {
                    this.setSummary(2131888829);
                }
                else {
                    this.setSummary("");
                }
            }
        }
        
        @VisibleForTesting
        public AppEntry getEntryForTest() {
            return this.mEntry;
        }
        
        public boolean isDisabledByAdmin() {
            return this.mHelper.isDisabledByAdmin();
        }
        
        @Override
        public void onAttached() {
            super.onAttached();
            UnrestrictedDataAccess.this.mDataSaverBackend.addListener((DataSaverBackend.Listener)this);
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            if (this.mEntry.icon == null) {
                preferenceViewHolder.itemView.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        UnrestrictedDataAccess.this.mApplicationsState.ensureIcon(AccessPreference.this.mEntry);
                        AccessPreference.this.setIcon(AccessPreference.this.mEntry.icon);
                    }
                });
            }
            final boolean disabledByAdmin = this.isDisabledByAdmin();
            final View viewById = preferenceViewHolder.findViewById(16908312);
            final boolean b = false;
            if (disabledByAdmin) {
                viewById.setVisibility(0);
            }
            else {
                int visibility;
                if (this.mState != null && this.mState.isDataSaverBlacklisted) {
                    visibility = 4;
                }
                else {
                    visibility = 0;
                }
                viewById.setVisibility(visibility);
            }
            super.onBindViewHolder(preferenceViewHolder);
            this.mHelper.onBindViewHolder(preferenceViewHolder);
            final View viewById2 = preferenceViewHolder.findViewById(R.id.restricted_icon);
            int visibility2;
            if (disabledByAdmin) {
                visibility2 = 0;
            }
            else {
                visibility2 = 8;
            }
            viewById2.setVisibility(visibility2);
            final View viewById3 = preferenceViewHolder.findViewById(16908352);
            int visibility3;
            if (disabledByAdmin) {
                visibility3 = 8;
            }
            else {
                visibility3 = (b ? 1 : 0);
            }
            viewById3.setVisibility(visibility3);
        }
        
        @Override
        public void onBlacklistStatusChanged(final int n, final boolean isDataSaverBlacklisted) {
            if (this.mState != null && this.mEntry.info.uid == n) {
                this.mState.isDataSaverBlacklisted = isDataSaverBlacklisted;
                this.reuse();
            }
        }
        
        @Override
        protected void onClick() {
            if (this.mState.isDataSaverBlacklisted) {
                AppInfoDashboardFragment.startAppInfoFragment(AppDataUsage.class, 2131886344, null, UnrestrictedDataAccess.this, this.mEntry);
            }
            else {
                super.onClick();
            }
        }
        
        @Override
        public void onDataSaverChanged(final boolean b) {
        }
        
        @Override
        public void onDetached() {
            UnrestrictedDataAccess.this.mDataSaverBackend.remListener((DataSaverBackend.Listener)this);
            super.onDetached();
        }
        
        @Override
        public void onWhitelistStatusChanged(final int n, final boolean isDataSaverWhitelisted) {
            if (this.mState != null && this.mEntry.info.uid == n) {
                this.mState.isDataSaverWhitelisted = isDataSaverWhitelisted;
                this.reuse();
            }
        }
        
        @Override
        public void performClick() {
            if (!this.mHelper.performClick()) {
                super.performClick();
            }
        }
        
        public void reuse() {
            this.setState();
            this.notifyChanged();
        }
        
        public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin disabledByAdmin) {
            this.mHelper.setDisabledByAdmin(disabledByAdmin);
        }
    }
}
