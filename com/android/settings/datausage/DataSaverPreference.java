package com.android.settings.datausage;

import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class DataSaverPreference extends Preference implements Listener
{
    private final DataSaverBackend mDataSaverBackend;
    
    public DataSaverPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mDataSaverBackend = new DataSaverBackend(context);
    }
    
    @Override
    public void onAttached() {
        super.onAttached();
        this.mDataSaverBackend.addListener((DataSaverBackend.Listener)this);
    }
    
    @Override
    public void onBlacklistStatusChanged(final int n, final boolean b) {
    }
    
    @Override
    public void onDataSaverChanged(final boolean b) {
        int summary;
        if (b) {
            summary = 2131887208;
        }
        else {
            summary = 2131887207;
        }
        this.setSummary(summary);
    }
    
    @Override
    public void onDetached() {
        super.onDetached();
        this.mDataSaverBackend.remListener((DataSaverBackend.Listener)this);
    }
    
    @Override
    public void onWhitelistStatusChanged(final int n, final boolean b) {
    }
}
