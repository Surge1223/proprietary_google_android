package com.android.settings.datausage;

import com.android.settingslib.net.DataUsageController;
import android.util.FeatureFlagUtils;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.net.NetworkTemplate;
import android.support.v7.preference.Preference;

public class DataUsagePreference extends Preference implements TemplatePreference
{
    private int mSubId;
    private NetworkTemplate mTemplate;
    private int mTitleRes;
    
    public DataUsagePreference(final Context context, final AttributeSet set) {
        super(context, set);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, new int[] { 16843233 }, TypedArrayUtils.getAttr(context, R.attr.preferenceStyle, 16842894), 0);
        this.mTitleRes = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
    }
    
    @Override
    public Intent getIntent() {
        final Bundle arguments = new Bundle();
        arguments.putParcelable("network_template", (Parcelable)this.mTemplate);
        arguments.putInt("sub_id", this.mSubId);
        final SubSettingLauncher setSourceMetricsCategory = new SubSettingLauncher(this.getContext()).setArguments(arguments).setDestination(DataUsageList.class.getName()).setSourceMetricsCategory(0);
        if (FeatureFlagUtils.isEnabled(this.getContext(), "settings_data_usage_v2")) {
            if (this.mTemplate.isMatchRuleMobile()) {
                setSourceMetricsCategory.setTitle(2131886343);
            }
            else {
                setSourceMetricsCategory.setTitle(this.mTitleRes);
            }
        }
        else if (this.mTitleRes > 0) {
            setSourceMetricsCategory.setTitle(this.mTitleRes);
        }
        else {
            setSourceMetricsCategory.setTitle(this.getTitle());
        }
        return setSourceMetricsCategory.toIntent();
    }
    
    @Override
    public void setTemplate(final NetworkTemplate mTemplate, final int mSubId, final NetworkServices networkServices) {
        this.mTemplate = mTemplate;
        this.mSubId = mSubId;
        final DataUsageController.DataUsageInfo dataUsageInfo = new DataUsageController(this.getContext()).getDataUsageInfo(this.mTemplate);
        if (FeatureFlagUtils.isEnabled(this.getContext(), "settings_data_usage_v2")) {
            if (this.mTemplate.isMatchRuleMobile()) {
                this.setTitle(2131886343);
            }
            else {
                this.setTitle(this.mTitleRes);
                this.setSummary(this.getContext().getString(2131887293, new Object[] { DataUsageUtils.formatDataUsage(this.getContext(), dataUsageInfo.usageLevel), dataUsageInfo.period }));
            }
        }
        else {
            this.setTitle(this.mTitleRes);
            this.setSummary(this.getContext().getString(2131887293, new Object[] { DataUsageUtils.formatDataUsage(this.getContext(), dataUsageInfo.usageLevel), dataUsageInfo.period }));
        }
        this.setIntent(this.getIntent());
    }
}
