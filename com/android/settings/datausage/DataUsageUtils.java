package com.android.settings.datausage;

import android.net.INetworkStatsSession;
import android.os.RemoteException;
import android.net.TrafficStats;
import android.net.INetworkStatsService$Stub;
import android.os.ServiceManager;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import android.net.NetworkTemplate;
import java.util.List;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.format.Formatter$BytesResult;
import android.text.BidiFormatter;
import android.text.format.Formatter;
import android.content.Context;

public final class DataUsageUtils
{
    public static CharSequence formatDataUsage(final Context context, final long n) {
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(context.getResources(), n, 8);
        return BidiFormatter.getInstance().unicodeWrap(context.getString(17039897, new Object[] { formatBytes.value, formatBytes.units }));
    }
    
    public static int getDefaultSubscriptionId(final Context context) {
        final SubscriptionManager from = SubscriptionManager.from(context);
        if (from == null) {
            return -1;
        }
        SubscriptionInfo defaultDataSubscriptionInfo;
        if ((defaultDataSubscriptionInfo = from.getDefaultDataSubscriptionInfo()) == null) {
            final List allSubscriptionInfoList = from.getAllSubscriptionInfoList();
            if (allSubscriptionInfoList.size() == 0) {
                return -1;
            }
            defaultDataSubscriptionInfo = allSubscriptionInfoList.get(0);
        }
        return defaultDataSubscriptionInfo.getSubscriptionId();
    }
    
    static NetworkTemplate getDefaultTemplate(final Context context, final int n) {
        if (hasMobileData(context) && n != -1) {
            final TelephonyManager from = TelephonyManager.from(context);
            return NetworkTemplate.normalize(NetworkTemplate.buildTemplateMobileAll(from.getSubscriberId(n)), from.getMergedSubscriberIds());
        }
        if (hasWifiRadio(context)) {
            return NetworkTemplate.buildTemplateWifiWildcard();
        }
        return NetworkTemplate.buildTemplateEthernet();
    }
    
    public static boolean hasEthernet(final Context context) {
        final boolean networkSupported = ConnectivityManager.from(context).isNetworkSupported(9);
        try {
            final INetworkStatsSession openSession = INetworkStatsService$Stub.asInterface(ServiceManager.getService("netstats")).openSession();
            long totalBytes;
            if (openSession != null) {
                totalBytes = openSession.getSummaryForNetwork(NetworkTemplate.buildTemplateEthernet(), Long.MIN_VALUE, Long.MAX_VALUE).getTotalBytes();
                TrafficStats.closeQuietly(openSession);
            }
            else {
                totalBytes = 0L;
            }
            return networkSupported && totalBytes > 0L;
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    public static boolean hasMobileData(final Context context) {
        final ConnectivityManager from = ConnectivityManager.from(context);
        boolean b = false;
        if (from != null) {
            b = b;
            if (from.isNetworkSupported(0)) {
                b = true;
            }
        }
        return b;
    }
    
    public static boolean hasSim(final Context context) {
        final int simState = ((TelephonyManager)context.getSystemService((Class)TelephonyManager.class)).getSimState();
        boolean b = true;
        if (simState == 1 || simState == 0) {
            b = false;
        }
        return b;
    }
    
    public static boolean hasWifiRadio(final Context context) {
        final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class);
        boolean b = true;
        if (connectivityManager == null || !connectivityManager.isNetworkSupported(1)) {
            b = false;
        }
        return b;
    }
}
