package com.android.settings.datausage;

import android.text.format.Time;
import android.widget.NumberPicker;
import android.net.NetworkPolicy;
import android.app.Activity;
import android.app.AlertDialog$Builder;
import android.view.ViewGroup;
import android.content.Context;
import android.view.LayoutInflater;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.app.Fragment;
import android.widget.Spinner;
import android.widget.EditText;
import android.view.View;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.util.FeatureFlagUtils;
import android.os.Bundle;
import com.android.settingslib.NetworkPolicyEditor;
import android.net.NetworkTemplate;
import android.support.v14.preference.SwitchPreference;
import com.android.settingslib.net.DataUsageController;
import com.android.internal.annotations.VisibleForTesting;
import android.support.v7.preference.Preference;

public class BillingCycleSettings extends DataUsageBase implements OnPreferenceChangeListener, DataUsageEditController
{
    @VisibleForTesting
    static final String KEY_SET_DATA_LIMIT = "set_data_limit";
    private Preference mBillingCycle;
    private Preference mDataLimit;
    private DataUsageController mDataUsageController;
    private Preference mDataWarning;
    private SwitchPreference mEnableDataLimit;
    private SwitchPreference mEnableDataWarning;
    private NetworkTemplate mNetworkTemplate;
    
    private void setPolicyWarningBytes(final long n) {
        this.services.mPolicyEditor.setPolicyWarningBytes(this.mNetworkTemplate, n);
        this.updatePrefs();
    }
    
    @Override
    public int getMetricsCategory() {
        return 342;
    }
    
    @Override
    public NetworkPolicyEditor getNetworkPolicyEditor() {
        return this.services.mPolicyEditor;
    }
    
    @Override
    public NetworkTemplate getNetworkTemplate() {
        return this.mNetworkTemplate;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mDataUsageController = new DataUsageController(this.getContext());
        this.mNetworkTemplate = (NetworkTemplate)this.getArguments().getParcelable("network_template");
        this.addPreferencesFromResource(2132082723);
        this.mBillingCycle = this.findPreference("billing_cycle");
        (this.mEnableDataWarning = (SwitchPreference)this.findPreference("set_data_warning")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mDataWarning = this.findPreference("data_warning");
        (this.mEnableDataLimit = (SwitchPreference)this.findPreference("set_data_limit")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mDataLimit = this.findPreference("data_limit");
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131887306);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mEnableDataLimit == preference) {
            if (!(boolean)o) {
                this.setPolicyLimitBytes(-1L);
                return true;
            }
            ConfirmLimitFragment.show(this);
            return false;
        }
        else {
            if (this.mEnableDataWarning == preference) {
                if (o) {
                    this.setPolicyWarningBytes(this.mDataUsageController.getDefaultWarningLevel());
                }
                else {
                    this.setPolicyWarningBytes(-1L);
                }
                return true;
            }
            return false;
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.mBillingCycle) {
            CycleEditorFragment.show(this);
            return true;
        }
        if (preference == this.mDataWarning) {
            BytesEditorFragment.show(this, false);
            return true;
        }
        if (preference == this.mDataLimit) {
            BytesEditorFragment.show(this, true);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updatePrefs();
    }
    
    @VisibleForTesting
    void setPolicyLimitBytes(final long n) {
        this.services.mPolicyEditor.setPolicyLimitBytes(this.mNetworkTemplate, n);
        this.updatePrefs();
    }
    
    @VisibleForTesting
    void setUpForTest(final NetworkPolicyEditor mPolicyEditor, final Preference mBillingCycle, final Preference mDataLimit, final Preference mDataWarning, final SwitchPreference mEnableDataLimit, final SwitchPreference mEnableDataWarning) {
        this.services.mPolicyEditor = mPolicyEditor;
        this.mBillingCycle = mBillingCycle;
        this.mDataLimit = mDataLimit;
        this.mDataWarning = mDataWarning;
        this.mEnableDataLimit = mEnableDataLimit;
        this.mEnableDataWarning = mEnableDataWarning;
    }
    
    @Override
    public void updateDataUsage() {
        this.updatePrefs();
    }
    
    @VisibleForTesting
    void updatePrefs() {
        final int policyCycleDay = this.services.mPolicyEditor.getPolicyCycleDay(this.mNetworkTemplate);
        if (FeatureFlagUtils.isEnabled(this.getContext(), "settings_data_usage_v2")) {
            this.mBillingCycle.setSummary(null);
        }
        else if (policyCycleDay != -1) {
            this.mBillingCycle.setSummary(this.getString(2131886682, new Object[] { policyCycleDay }));
        }
        else {
            this.mBillingCycle.setSummary(null);
        }
        final long policyWarningBytes = this.services.mPolicyEditor.getPolicyWarningBytes(this.mNetworkTemplate);
        if (policyWarningBytes != -1L) {
            this.mDataWarning.setSummary(DataUsageUtils.formatDataUsage(this.getContext(), policyWarningBytes));
            this.mDataWarning.setEnabled(true);
            this.mEnableDataWarning.setChecked(true);
        }
        else {
            this.mDataWarning.setSummary(null);
            this.mDataWarning.setEnabled(false);
            this.mEnableDataWarning.setChecked(false);
        }
        final long policyLimitBytes = this.services.mPolicyEditor.getPolicyLimitBytes(this.mNetworkTemplate);
        if (policyLimitBytes != -1L) {
            this.mDataLimit.setSummary(DataUsageUtils.formatDataUsage(this.getContext(), policyLimitBytes));
            this.mDataLimit.setEnabled(true);
            this.mEnableDataLimit.setChecked(true);
        }
        else {
            this.mDataLimit.setSummary(null);
            this.mDataLimit.setEnabled(false);
            this.mEnableDataLimit.setChecked(false);
        }
    }
    
    public static class BytesEditorFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        private View mView;
        
        private String formatText(final float n) {
            return String.valueOf(Math.round(n * 100.0f) / 100.0f);
        }
        
        private void setupPicker(final EditText editText, final Spinner spinner) {
            final NetworkPolicyEditor networkPolicyEditor = ((DataUsageEditController)this.getTargetFragment()).getNetworkPolicyEditor();
            final NetworkTemplate networkTemplate = (NetworkTemplate)this.getArguments().getParcelable("template");
            long n;
            if (this.getArguments().getBoolean("limit")) {
                n = networkPolicyEditor.getPolicyLimitBytes(networkTemplate);
            }
            else {
                n = networkPolicyEditor.getPolicyWarningBytes(networkTemplate);
            }
            if (n > 1.61061274E9f) {
                final String formatText = this.formatText(n / 1.07374182E9f);
                editText.setText((CharSequence)formatText);
                editText.setSelection(0, formatText.length());
                spinner.setSelection(1);
            }
            else {
                final String formatText2 = this.formatText(n / 1048576.0f);
                editText.setText((CharSequence)formatText2);
                editText.setSelection(0, formatText2.length());
                spinner.setSelection(0);
            }
        }
        
        public static void show(final DataUsageEditController dataUsageEditController, final boolean b) {
            if (!(dataUsageEditController instanceof Fragment)) {
                return;
            }
            final Fragment fragment = (Fragment)dataUsageEditController;
            if (!fragment.isAdded()) {
                return;
            }
            final Bundle arguments = new Bundle();
            arguments.putParcelable("template", (Parcelable)dataUsageEditController.getNetworkTemplate());
            arguments.putBoolean("limit", b);
            final BytesEditorFragment bytesEditorFragment = new BytesEditorFragment();
            bytesEditorFragment.setArguments(arguments);
            bytesEditorFragment.setTargetFragment(fragment, 0);
            bytesEditorFragment.show(fragment.getFragmentManager(), "warningEditor");
        }
        
        public int getMetricsCategory() {
            return 550;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            if (n != -1) {
                return;
            }
            final DataUsageEditController dataUsageEditController = (DataUsageEditController)this.getTargetFragment();
            final NetworkPolicyEditor networkPolicyEditor = dataUsageEditController.getNetworkPolicyEditor();
            final NetworkTemplate networkTemplate = (NetworkTemplate)this.getArguments().getParcelable("template");
            final boolean boolean1 = this.getArguments().getBoolean("limit");
            final EditText editText = (EditText)this.mView.findViewById(2131361955);
            final Spinner spinner = (Spinner)this.mView.findViewById(2131362616);
            final String string = editText.getText().toString();
            String s = null;
            Label_0107: {
                if (!string.isEmpty()) {
                    s = string;
                    if (!string.equals(".")) {
                        break Label_0107;
                    }
                }
                s = "0";
            }
            final float floatValue = Float.valueOf(s);
            long n2;
            if (spinner.getSelectedItemPosition() == 0) {
                n2 = 1048576L;
            }
            else {
                n2 = 1073741824L;
            }
            final long min = Math.min(53687091200000L, (long)(floatValue * n2));
            if (boolean1) {
                networkPolicyEditor.setPolicyLimitBytes(networkTemplate, min);
            }
            else {
                networkPolicyEditor.setPolicyWarningBytes(networkTemplate, min);
            }
            dataUsageEditController.updateDataUsage();
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final LayoutInflater from = LayoutInflater.from((Context)activity);
            final boolean boolean1 = this.getArguments().getBoolean("limit");
            this.mView = from.inflate(2131558524, (ViewGroup)null, false);
            this.setupPicker((EditText)this.mView.findViewById(2131361955), (Spinner)this.mView.findViewById(2131362616));
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            int title;
            if (boolean1) {
                title = 2131887254;
            }
            else {
                title = 2131887299;
            }
            return (Dialog)alertDialog$Builder.setTitle(title).setView(this.mView).setPositiveButton(2131887235, (DialogInterface$OnClickListener)this).create();
        }
    }
    
    public static class ConfirmLimitFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        @VisibleForTesting
        static final String EXTRA_LIMIT_BYTES = "limitBytes";
        
        public static void show(final BillingCycleSettings billingCycleSettings) {
            if (!billingCycleSettings.isAdded()) {
                return;
            }
            final NetworkPolicy policy = billingCycleSettings.services.mPolicyEditor.getPolicy(billingCycleSettings.mNetworkTemplate);
            if (policy == null) {
                return;
            }
            billingCycleSettings.getResources();
            final long max = Math.max(5368709120L, (long)(policy.warningBytes * 1.2f));
            final Bundle arguments = new Bundle();
            arguments.putLong("limitBytes", max);
            final ConfirmLimitFragment confirmLimitFragment = new ConfirmLimitFragment();
            confirmLimitFragment.setArguments(arguments);
            confirmLimitFragment.setTargetFragment((Fragment)billingCycleSettings, 0);
            confirmLimitFragment.show(billingCycleSettings.getFragmentManager(), "confirmLimit");
        }
        
        public int getMetricsCategory() {
            return 551;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final BillingCycleSettings billingCycleSettings = (BillingCycleSettings)this.getTargetFragment();
            if (n != -1) {
                return;
            }
            final long long1 = this.getArguments().getLong("limitBytes");
            if (billingCycleSettings != null) {
                billingCycleSettings.setPolicyLimitBytes(long1);
            }
            billingCycleSettings.getPreferenceManager().getSharedPreferences().edit().putBoolean("set_data_limit", true).apply();
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887253).setMessage(2131887252).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
        }
    }
    
    public static class CycleEditorFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        private NumberPicker mCycleDayPicker;
        
        public static void show(final BillingCycleSettings billingCycleSettings) {
            if (!billingCycleSettings.isAdded()) {
                return;
            }
            final Bundle arguments = new Bundle();
            arguments.putParcelable("template", (Parcelable)billingCycleSettings.mNetworkTemplate);
            final CycleEditorFragment cycleEditorFragment = new CycleEditorFragment();
            cycleEditorFragment.setArguments(arguments);
            cycleEditorFragment.setTargetFragment((Fragment)billingCycleSettings, 0);
            cycleEditorFragment.show(billingCycleSettings.getFragmentManager(), "cycleEditor");
        }
        
        public int getMetricsCategory() {
            return 549;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final NetworkTemplate networkTemplate = (NetworkTemplate)this.getArguments().getParcelable("template");
            final DataUsageEditController dataUsageEditController = (DataUsageEditController)this.getTargetFragment();
            final NetworkPolicyEditor networkPolicyEditor = dataUsageEditController.getNetworkPolicyEditor();
            this.mCycleDayPicker.clearFocus();
            networkPolicyEditor.setPolicyCycleDay(networkTemplate, this.mCycleDayPicker.getValue(), new Time().timezone);
            dataUsageEditController.updateDataUsage();
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final NetworkPolicyEditor networkPolicyEditor = ((DataUsageEditController)this.getTargetFragment()).getNetworkPolicyEditor();
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            final View inflate = LayoutInflater.from(alertDialog$Builder.getContext()).inflate(2131558526, (ViewGroup)null, false);
            this.mCycleDayPicker = (NumberPicker)inflate.findViewById(2131362032);
            final int policyCycleDay = networkPolicyEditor.getPolicyCycleDay((NetworkTemplate)this.getArguments().getParcelable("template"));
            this.mCycleDayPicker.setMinValue(1);
            this.mCycleDayPicker.setMaxValue(31);
            this.mCycleDayPicker.setValue(policyCycleDay);
            this.mCycleDayPicker.setWrapSelectorWheel(true);
            return (Dialog)alertDialog$Builder.setTitle(2131887237).setView(inflate).setPositiveButton(2131887235, (DialogInterface$OnClickListener)this).create();
        }
    }
}
