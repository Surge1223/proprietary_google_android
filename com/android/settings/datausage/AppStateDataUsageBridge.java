package com.android.settings.datausage;

import java.util.ArrayList;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.AppStateBaseBridge;

public class AppStateDataUsageBridge extends AppStateBaseBridge
{
    private final DataSaverBackend mDataSaverBackend;
    
    public AppStateDataUsageBridge(final ApplicationsState applicationsState, final Callback callback, final DataSaverBackend mDataSaverBackend) {
        super(applicationsState, callback);
        this.mDataSaverBackend = mDataSaverBackend;
    }
    
    @Override
    protected void loadAllExtraInfo() {
        final ArrayList<AppEntry> allApps = this.mAppSession.getAllApps();
        for (int size = allApps.size(), i = 0; i < size; ++i) {
            final AppEntry appEntry = allApps.get(i);
            appEntry.extraInfo = new DataUsageState(this.mDataSaverBackend.isWhitelisted(appEntry.info.uid), this.mDataSaverBackend.isBlacklisted(appEntry.info.uid));
        }
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = new DataUsageState(this.mDataSaverBackend.isWhitelisted(n), this.mDataSaverBackend.isBlacklisted(n));
    }
    
    public static class DataUsageState
    {
        public boolean isDataSaverBlacklisted;
        public boolean isDataSaverWhitelisted;
        
        public DataUsageState(final boolean isDataSaverWhitelisted, final boolean isDataSaverBlacklisted) {
            this.isDataSaverWhitelisted = isDataSaverWhitelisted;
            this.isDataSaverBlacklisted = isDataSaverBlacklisted;
        }
    }
}
