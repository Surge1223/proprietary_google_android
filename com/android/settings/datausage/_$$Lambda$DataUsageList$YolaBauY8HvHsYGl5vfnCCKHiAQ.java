package com.android.settings.datausage;

import android.content.Intent;
import android.widget.SpinnerAdapter;
import android.os.AsyncTask;
import android.net.TrafficStats;
import android.os.RemoteException;
import android.util.Log;
import java.util.Collections;
import android.os.UserHandle;
import android.os.UserManager;
import android.app.ActivityManager;
import android.net.NetworkPolicy;
import android.net.NetworkStatsHistory$Entry;
import android.app.Activity;
import android.graphics.Color;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import java.util.Iterator;
import java.util.List;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.net.ConnectivityManager;
import java.util.ArrayList;
import android.net.NetworkStats$Entry;
import android.util.SparseArray;
import com.android.settingslib.AppItem;
import com.android.settingslib.net.SummaryForAllUidLoader;
import android.net.NetworkStatsHistory;
import android.content.Context;
import com.android.settingslib.net.ChartDataLoader;
import android.content.Loader;
import android.os.Bundle;
import android.widget.AdapterView;
import android.support.v7.preference.Preference;
import com.android.settingslib.net.UidDetailProvider;
import android.net.NetworkTemplate;
import android.net.NetworkStats;
import android.net.INetworkStatsSession;
import com.android.settings.widget.LoadingViewController;
import android.widget.Spinner;
import android.widget.AdapterView$OnItemSelectedListener;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settingslib.net.ChartData;
import android.support.v7.preference.PreferenceGroup;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$DataUsageList$YolaBauY8HvHsYGl5vfnCCKHiAQ implements View.OnClickListener
{
    public final void onClick(final View view) {
        DataUsageList.lambda$onViewCreated$0(this.f$0, view);
    }
}
