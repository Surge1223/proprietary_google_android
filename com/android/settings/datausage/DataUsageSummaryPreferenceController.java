package com.android.settings.datausage;

import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.util.RecurrenceRule;
import android.telephony.SubscriptionInfo;
import android.util.Log;
import java.util.List;
import java.util.Collection;
import com.android.internal.util.CollectionUtils;
import android.telephony.SubscriptionPlan;
import android.net.NetworkPolicyManager;
import android.view.View;
import android.app.Fragment;
import android.content.Context;
import android.telephony.SubscriptionManager;
import com.android.settingslib.NetworkPolicyEditor;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.widget.EntityHeaderController;
import android.net.NetworkTemplate;
import com.android.settingslib.net.DataUsageController;
import android.app.Activity;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.BasePreferenceController;

public class DataUsageSummaryPreferenceController extends BasePreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart
{
    private static final String KEY = "status_header";
    private static final long PETA = 1000000000000000L;
    private static final float RELATIVE_SIZE_LARGE = 1.5625f;
    private static final float RELATIVE_SIZE_SMALL = 0.64f;
    private static final String TAG = "DataUsageController";
    private final Activity mActivity;
    private CharSequence mCarrierName;
    private long mCycleEnd;
    private long mCycleStart;
    private long mDataBarSize;
    private final DataUsageInfoController mDataInfoController;
    private final DataUsageController mDataUsageController;
    private final DataUsageSummary mDataUsageSummary;
    private final int mDataUsageTemplate;
    private int mDataplanCount;
    private long mDataplanSize;
    private long mDataplanUse;
    private final NetworkTemplate mDefaultTemplate;
    private final EntityHeaderController mEntityHeaderController;
    private final boolean mHasMobileData;
    private final Lifecycle mLifecycle;
    private Intent mManageSubscriptionIntent;
    private final NetworkPolicyEditor mPolicyEditor;
    private long mSnapshotTime;
    private final SubscriptionManager mSubscriptionManager;
    
    public DataUsageSummaryPreferenceController(final Activity mActivity, final Lifecycle mLifecycle, final DataUsageSummary mDataUsageSummary) {
        super((Context)mActivity, "status_header");
        this.mActivity = mActivity;
        this.mEntityHeaderController = EntityHeaderController.newInstance(mActivity, mDataUsageSummary, null);
        this.mLifecycle = mLifecycle;
        this.mDataUsageSummary = mDataUsageSummary;
        final int defaultSubscriptionId = DataUsageUtils.getDefaultSubscriptionId((Context)mActivity);
        this.mDefaultTemplate = DataUsageUtils.getDefaultTemplate((Context)mActivity, defaultSubscriptionId);
        this.mPolicyEditor = new NetworkPolicyEditor(NetworkPolicyManager.from((Context)mActivity));
        this.mHasMobileData = (DataUsageUtils.hasMobileData((Context)mActivity) && defaultSubscriptionId != -1);
        this.mDataUsageController = new DataUsageController((Context)mActivity);
        this.mDataInfoController = new DataUsageInfoController();
        if (this.mHasMobileData) {
            this.mDataUsageTemplate = 2131886988;
        }
        else if (DataUsageUtils.hasWifiRadio((Context)mActivity)) {
            this.mDataUsageTemplate = 2131889943;
        }
        else {
            this.mDataUsageTemplate = 2131887611;
        }
        this.mSubscriptionManager = (SubscriptionManager)this.mContext.getSystemService("telephony_subscription_service");
    }
    
    DataUsageSummaryPreferenceController(final DataUsageController mDataUsageController, final DataUsageInfoController mDataInfoController, final NetworkTemplate mDefaultTemplate, final NetworkPolicyEditor mPolicyEditor, final int mDataUsageTemplate, final boolean mHasMobileData, final SubscriptionManager mSubscriptionManager, final Activity mActivity, final Lifecycle mLifecycle, final EntityHeaderController mEntityHeaderController, final DataUsageSummary mDataUsageSummary) {
        super((Context)mActivity, "status_header");
        this.mDataUsageController = mDataUsageController;
        this.mDataInfoController = mDataInfoController;
        this.mDefaultTemplate = mDefaultTemplate;
        this.mPolicyEditor = mPolicyEditor;
        this.mDataUsageTemplate = mDataUsageTemplate;
        this.mHasMobileData = mHasMobileData;
        this.mSubscriptionManager = mSubscriptionManager;
        this.mActivity = mActivity;
        this.mLifecycle = mLifecycle;
        this.mEntityHeaderController = mEntityHeaderController;
        this.mDataUsageSummary = mDataUsageSummary;
    }
    
    public static SubscriptionPlan getPrimaryPlan(final SubscriptionManager subscriptionManager, final int n) {
        final List subscriptionPlans = subscriptionManager.getSubscriptionPlans(n);
        final boolean empty = CollectionUtils.isEmpty((Collection)subscriptionPlans);
        SubscriptionPlan subscriptionPlan = null;
        if (empty) {
            return null;
        }
        final SubscriptionPlan subscriptionPlan2 = subscriptionPlans.get(0);
        if (subscriptionPlan2.getDataLimitBytes() > 0L && saneSize(subscriptionPlan2.getDataUsageBytes()) && subscriptionPlan2.getCycleRule() != null) {
            subscriptionPlan = subscriptionPlan2;
        }
        return subscriptionPlan;
    }
    
    private void refreshDataplanInfo(final DataUsageController.DataUsageInfo dataUsageInfo) {
        this.mCarrierName = null;
        this.mDataplanCount = 0;
        this.mDataplanSize = -1L;
        this.mDataBarSize = this.mDataInfoController.getSummaryLimit(dataUsageInfo);
        this.mDataplanUse = dataUsageInfo.usageLevel;
        this.mCycleStart = dataUsageInfo.cycleStart;
        this.mCycleEnd = dataUsageInfo.cycleEnd;
        this.mSnapshotTime = -1L;
        final int defaultSubscriptionId = SubscriptionManager.getDefaultSubscriptionId();
        final SubscriptionInfo defaultDataSubscriptionInfo = this.mSubscriptionManager.getDefaultDataSubscriptionInfo();
        if (defaultDataSubscriptionInfo != null && this.mHasMobileData) {
            this.mCarrierName = defaultDataSubscriptionInfo.getCarrierName();
            final List subscriptionPlans = this.mSubscriptionManager.getSubscriptionPlans(defaultSubscriptionId);
            final SubscriptionPlan primaryPlan = getPrimaryPlan(this.mSubscriptionManager, defaultSubscriptionId);
            if (primaryPlan != null) {
                this.mDataplanCount = subscriptionPlans.size();
                this.mDataplanSize = primaryPlan.getDataLimitBytes();
                if (unlimited(this.mDataplanSize)) {
                    this.mDataplanSize = -1L;
                }
                this.mDataBarSize = this.mDataplanSize;
                this.mDataplanUse = primaryPlan.getDataUsageBytes();
                final RecurrenceRule cycleRule = primaryPlan.getCycleRule();
                if (cycleRule != null && cycleRule.start != null && cycleRule.end != null) {
                    this.mCycleStart = cycleRule.start.toEpochSecond() * 1000L;
                    this.mCycleEnd = cycleRule.end.toEpochSecond() * 1000L;
                }
                this.mSnapshotTime = primaryPlan.getDataUsageTime();
            }
        }
        this.mManageSubscriptionIntent = this.mSubscriptionManager.createManageSubscriptionIntent(defaultSubscriptionId);
        final StringBuilder sb = new StringBuilder();
        sb.append("Have ");
        sb.append(this.mDataplanCount);
        sb.append(" plans, dflt sub-id ");
        sb.append(defaultSubscriptionId);
        sb.append(", intent ");
        sb.append(this.mManageSubscriptionIntent);
        Log.i("DataUsageController", sb.toString());
    }
    
    private static boolean saneSize(final long n) {
        return n >= 0L && n < 1000000000000000L;
    }
    
    public static boolean unlimited(final long n) {
        return n == Long.MAX_VALUE;
    }
    
    @Override
    public int getAvailabilityStatus() {
        return (!DataUsageUtils.hasSim((Context)this.mActivity) && !DataUsageUtils.hasWifiRadio(this.mContext)) ? 1 : 0;
    }
    
    @Override
    public void onStart() {
        this.mEntityHeaderController.setRecyclerView(this.mDataUsageSummary.getListView(), this.mLifecycle);
        this.mEntityHeaderController.styleActionBar(this.mActivity);
    }
    
    void setCarrierValues(final String mCarrierName, final long mSnapshotTime, final long mCycleEnd, final Intent mManageSubscriptionIntent) {
        this.mCarrierName = mCarrierName;
        this.mSnapshotTime = mSnapshotTime;
        this.mCycleEnd = mCycleEnd;
        this.mManageSubscriptionIntent = mManageSubscriptionIntent;
    }
    
    void setPlanValues(final int mDataplanCount, final long n, final long mDataplanUse) {
        this.mDataplanCount = mDataplanCount;
        this.mDataplanSize = n;
        this.mDataBarSize = n;
        this.mDataplanUse = mDataplanUse;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final DataUsageSummaryPreference dataUsageSummaryPreference = (DataUsageSummaryPreference)preference;
        if (DataUsageUtils.hasSim((Context)this.mActivity)) {
            final DataUsageController.DataUsageInfo dataUsageInfo = this.mDataUsageController.getDataUsageInfo(this.mDefaultTemplate);
            this.mDataInfoController.updateDataLimit(dataUsageInfo, this.mPolicyEditor.getPolicy(this.mDefaultTemplate));
            dataUsageSummaryPreference.setWifiMode(false, null);
            if (this.mSubscriptionManager != null) {
                this.refreshDataplanInfo(dataUsageInfo);
            }
            if (dataUsageInfo.warningLevel > 0L && dataUsageInfo.limitLevel > 0L) {
                dataUsageSummaryPreference.setLimitInfo(TextUtils.expandTemplate(this.mContext.getText(2131886990), new CharSequence[] { DataUsageUtils.formatDataUsage(this.mContext, dataUsageInfo.warningLevel), DataUsageUtils.formatDataUsage(this.mContext, dataUsageInfo.limitLevel) }).toString());
            }
            else if (dataUsageInfo.warningLevel > 0L) {
                dataUsageSummaryPreference.setLimitInfo(TextUtils.expandTemplate(this.mContext.getText(2131886989), new CharSequence[] { DataUsageUtils.formatDataUsage(this.mContext, dataUsageInfo.warningLevel) }).toString());
            }
            else if (dataUsageInfo.limitLevel > 0L) {
                dataUsageSummaryPreference.setLimitInfo(TextUtils.expandTemplate(this.mContext.getText(2131886987), new CharSequence[] { DataUsageUtils.formatDataUsage(this.mContext, dataUsageInfo.limitLevel) }).toString());
            }
            else {
                dataUsageSummaryPreference.setLimitInfo(null);
            }
            dataUsageSummaryPreference.setUsageNumbers(this.mDataplanUse, this.mDataplanSize, this.mHasMobileData);
            if (this.mDataBarSize <= 0L) {
                dataUsageSummaryPreference.setChartEnabled(false);
            }
            else {
                dataUsageSummaryPreference.setChartEnabled(true);
                dataUsageSummaryPreference.setLabels(DataUsageUtils.formatDataUsage(this.mContext, 0L), DataUsageUtils.formatDataUsage(this.mContext, this.mDataBarSize));
                dataUsageSummaryPreference.setProgress(this.mDataplanUse / this.mDataBarSize);
            }
            dataUsageSummaryPreference.setUsageInfo(this.mCycleEnd, this.mSnapshotTime, this.mCarrierName, this.mDataplanCount, this.mManageSubscriptionIntent);
            return;
        }
        final DataUsageController.DataUsageInfo dataUsageInfo2 = this.mDataUsageController.getDataUsageInfo(NetworkTemplate.buildTemplateWifiWildcard());
        dataUsageSummaryPreference.setWifiMode(true, dataUsageInfo2.period);
        dataUsageSummaryPreference.setLimitInfo(null);
        dataUsageSummaryPreference.setUsageNumbers(dataUsageInfo2.usageLevel, -1L, true);
        dataUsageSummaryPreference.setChartEnabled(false);
        dataUsageSummaryPreference.setUsageInfo(dataUsageInfo2.cycleEnd, -1L, null, 0, null);
    }
}
