package com.android.settings.datausage;

import android.net.NetworkTemplate;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class NetworkRestrictionsPreference extends Preference implements TemplatePreference
{
    public NetworkRestrictionsPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public void setTemplate(final NetworkTemplate networkTemplate, final int n, final NetworkServices networkServices) {
    }
}
