package com.android.settings.datausage;

import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.util.Log;
import android.net.TrafficStats;
import com.android.settingslib.net.UidDetail;
import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.os.RemoteException;
import com.android.settingslib.net.UidDetailProvider;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.net.NetworkStatsHistory$Entry;
import android.content.pm.PackageManager;
import java.util.Iterator;
import android.content.Context;
import com.android.settingslib.net.ChartDataLoader;
import android.content.Loader;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.net.NetworkTemplate;
import android.net.INetworkStatsSession;
import com.android.settingslib.RestrictedSwitchPreference;
import android.net.NetworkPolicy;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.graphics.drawable.Drawable;
import android.widget.AdapterView$OnItemSelectedListener;
import com.android.settingslib.net.ChartData;
import android.content.Intent;
import android.util.ArraySet;
import android.app.LoaderManager$LoaderCallbacks;
import android.support.v7.preference.PreferenceCategory;
import com.android.settingslib.AppItem;
import android.support.v7.preference.Preference;

public class AppDataUsage extends DataUsageBase implements OnPreferenceChangeListener, Listener
{
    private AppItem mAppItem;
    private PreferenceCategory mAppList;
    private final LoaderManager$LoaderCallbacks<ArraySet<Preference>> mAppPrefCallbacks;
    private Preference mAppSettings;
    private Intent mAppSettingsIntent;
    private Preference mBackgroundUsage;
    private ChartData mChartData;
    private final LoaderManager$LoaderCallbacks<ChartData> mChartDataCallbacks;
    private SpinnerPreference mCycle;
    private CycleAdapter mCycleAdapter;
    private AdapterView$OnItemSelectedListener mCycleListener;
    private DataSaverBackend mDataSaverBackend;
    private long mEnd;
    private Preference mForegroundUsage;
    private Drawable mIcon;
    private CharSequence mLabel;
    private PackageManagerWrapper mPackageManagerWrapper;
    private String mPackageName;
    private final ArraySet<String> mPackages;
    private NetworkPolicy mPolicy;
    private RestrictedSwitchPreference mRestrictBackground;
    private long mStart;
    private INetworkStatsSession mStatsSession;
    private NetworkTemplate mTemplate;
    private Preference mTotalUsage;
    private RestrictedSwitchPreference mUnrestrictedData;
    
    public AppDataUsage() {
        this.mPackages = (ArraySet<String>)new ArraySet();
        this.mCycleListener = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                final CycleAdapter.CycleItem cycleItem = (CycleAdapter.CycleItem)AppDataUsage.this.mCycle.getSelectedItem();
                AppDataUsage.this.mStart = cycleItem.start;
                AppDataUsage.this.mEnd = cycleItem.end;
                AppDataUsage.this.bindData();
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        };
        this.mChartDataCallbacks = (LoaderManager$LoaderCallbacks<ChartData>)new LoaderManager$LoaderCallbacks<ChartData>() {
            public Loader<ChartData> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<ChartData>)new ChartDataLoader((Context)AppDataUsage.this.getActivity(), AppDataUsage.this.mStatsSession, bundle);
            }
            
            public void onLoadFinished(final Loader<ChartData> loader, final ChartData chartData) {
                AppDataUsage.this.mChartData = chartData;
                AppDataUsage.this.mCycleAdapter.updateCycleList(AppDataUsage.this.mPolicy, AppDataUsage.this.mChartData);
                AppDataUsage.this.bindData();
            }
            
            public void onLoaderReset(final Loader<ChartData> loader) {
            }
        };
        this.mAppPrefCallbacks = (LoaderManager$LoaderCallbacks<ArraySet<Preference>>)new LoaderManager$LoaderCallbacks<ArraySet<Preference>>() {
            public Loader<ArraySet<Preference>> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<ArraySet<Preference>>)new AppPrefLoader(InstrumentedPreferenceFragment.this.getPrefContext(), AppDataUsage.this.mPackages, SettingsPreferenceFragment.this.getPackageManager());
            }
            
            public void onLoadFinished(final Loader<ArraySet<Preference>> loader, final ArraySet<Preference> set) {
                if (set != null && AppDataUsage.this.mAppList != null) {
                    final Iterator iterator = set.iterator();
                    while (iterator.hasNext()) {
                        AppDataUsage.this.mAppList.addPreference(iterator.next());
                    }
                }
            }
            
            public void onLoaderReset(final Loader<ArraySet<Preference>> loader) {
            }
        };
    }
    
    private void addUid(int i) {
        final String[] packagesForUid = this.getPackageManager().getPackagesForUid(i);
        if (packagesForUid != null) {
            for (i = 0; i < packagesForUid.length; ++i) {
                this.mPackages.add((Object)packagesForUid[i]);
            }
        }
    }
    
    private void bindData() {
        long n;
        long n2;
        if (this.mChartData != null && this.mStart != 0L) {
            this.mCycle.setVisible(true);
            final long currentTimeMillis = System.currentTimeMillis();
            final NetworkStatsHistory$Entry values = this.mChartData.detailDefault.getValues(this.mStart, this.mEnd, currentTimeMillis, (NetworkStatsHistory$Entry)null);
            n = values.rxBytes + values.txBytes;
            final NetworkStatsHistory$Entry values2 = this.mChartData.detailForeground.getValues(this.mStart, this.mEnd, currentTimeMillis, values);
            n2 = values2.rxBytes + values2.txBytes;
        }
        else {
            n2 = 0L;
            n = 0L;
            this.mCycle.setVisible(false);
        }
        final Context context = this.getContext();
        this.mTotalUsage.setSummary(DataUsageUtils.formatDataUsage(context, n + n2));
        this.mForegroundUsage.setSummary(DataUsageUtils.formatDataUsage(context, n2));
        this.mBackgroundUsage.setSummary(DataUsageUtils.formatDataUsage(context, n));
    }
    
    private boolean getAppRestrictBackground() {
        return (this.services.mPolicyManager.getUidPolicy(this.mAppItem.key) & 0x1) != 0x0;
    }
    
    private boolean getUnrestrictData() {
        return this.mDataSaverBackend != null && this.mDataSaverBackend.isWhitelisted(this.mAppItem.key);
    }
    
    private void updatePrefs(final boolean b, final boolean checked) {
        final RestrictedLockUtils.EnforcedAdmin checkIfMeteredDataRestricted = RestrictedLockUtils.checkIfMeteredDataRestricted(this.getContext(), this.mPackageName, UserHandle.getUserId(this.mAppItem.key));
        if (this.mRestrictBackground != null) {
            this.mRestrictBackground.setChecked(b ^ true);
            this.mRestrictBackground.setDisabledByAdmin(checkIfMeteredDataRestricted);
        }
        if (this.mUnrestrictedData != null) {
            if (b) {
                this.mUnrestrictedData.setVisible(false);
            }
            else {
                this.mUnrestrictedData.setVisible(true);
                this.mUnrestrictedData.setChecked(checked);
                this.mUnrestrictedData.setDisabledByAdmin(checkIfMeteredDataRestricted);
            }
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 343;
    }
    
    @Override
    public void onBlacklistStatusChanged(final int n, final boolean b) {
        if (this.mAppItem.uids.get(n, false)) {
            this.updatePrefs(b, this.getUnrestrictData());
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mPackageManagerWrapper = new PackageManagerWrapper(this.getPackageManager());
        final Bundle arguments = this.getArguments();
        try {
            this.mStatsSession = this.services.mStatsService.openSession();
            AppItem mAppItem;
            if (arguments != null) {
                mAppItem = (AppItem)arguments.getParcelable("app_item");
            }
            else {
                mAppItem = null;
            }
            this.mAppItem = mAppItem;
            NetworkTemplate mTemplate;
            if (arguments != null) {
                mTemplate = (NetworkTemplate)arguments.getParcelable("network_template");
            }
            else {
                mTemplate = null;
            }
            this.mTemplate = mTemplate;
            if (this.mTemplate == null) {
                final Context context = this.getContext();
                this.mTemplate = DataUsageUtils.getDefaultTemplate(context, DataUsageUtils.getDefaultSubscriptionId(context));
            }
            if (this.mAppItem == null) {
                int n;
                if (arguments != null) {
                    n = arguments.getInt("uid", -1);
                }
                else {
                    n = this.getActivity().getIntent().getIntExtra("uid", -1);
                }
                if (n == -1) {
                    this.getActivity().finish();
                }
                else {
                    this.addUid(n);
                    (this.mAppItem = new AppItem(n)).addUid(n);
                }
            }
            else {
                for (int i = 0; i < this.mAppItem.uids.size(); ++i) {
                    this.addUid(this.mAppItem.uids.keyAt(i));
                }
            }
            this.addPreferencesFromResource(2132082707);
            this.mTotalUsage = this.findPreference("total_usage");
            this.mForegroundUsage = this.findPreference("foreground_usage");
            this.mBackgroundUsage = this.findPreference("background_usage");
            this.mCycle = (SpinnerPreference)this.findPreference("cycle");
            this.mCycleAdapter = new CycleAdapter(this.getContext(), (CycleAdapter.SpinnerInterface)this.mCycle, this.mCycleListener, false);
            if (this.mAppItem.key > 0) {
                if (this.mPackages.size() != 0) {
                    try {
                        final ApplicationInfo applicationInfoAsUser = this.mPackageManagerWrapper.getApplicationInfoAsUser((String)this.mPackages.valueAt(0), 0, UserHandle.getUserId(this.mAppItem.key));
                        this.mIcon = IconDrawableFactory.newInstance((Context)this.getActivity()).getBadgedIcon(applicationInfoAsUser);
                        this.mLabel = applicationInfoAsUser.loadLabel(this.mPackageManagerWrapper.getPackageManager());
                        this.mPackageName = applicationInfoAsUser.packageName;
                    }
                    catch (PackageManager$NameNotFoundException ex2) {}
                }
                if (!UserHandle.isApp(this.mAppItem.key)) {
                    this.removePreference("unrestricted_data_saver");
                    this.removePreference("restrict_background");
                }
                else {
                    (this.mRestrictBackground = (RestrictedSwitchPreference)this.findPreference("restrict_background")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                    (this.mUnrestrictedData = (RestrictedSwitchPreference)this.findPreference("unrestricted_data_saver")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                }
                this.mDataSaverBackend = new DataSaverBackend(this.getContext());
                this.mAppSettings = this.findPreference("app_settings");
                (this.mAppSettingsIntent = new Intent("android.intent.action.MANAGE_NETWORK_USAGE")).addCategory("android.intent.category.DEFAULT");
                final PackageManager packageManager = this.getPackageManager();
                final boolean b = false;
                final Iterator iterator = this.mPackages.iterator();
                boolean b2;
                while (true) {
                    b2 = b;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    this.mAppSettingsIntent.setPackage((String)iterator.next());
                    if (packageManager.resolveActivity(this.mAppSettingsIntent, 0) != null) {
                        b2 = true;
                        break;
                    }
                }
                if (!b2) {
                    this.removePreference("app_settings");
                    this.mAppSettings = null;
                }
                if (this.mPackages.size() > 1) {
                    this.mAppList = (PreferenceCategory)this.findPreference("app_list");
                    this.getLoaderManager().initLoader(3, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this.mAppPrefCallbacks);
                }
                else {
                    this.removePreference("app_list");
                }
            }
            else {
                final Activity activity = this.getActivity();
                final UidDetail uidDetail = new UidDetailProvider((Context)activity).getUidDetail(this.mAppItem.key, true);
                this.mIcon = uidDetail.icon;
                this.mLabel = uidDetail.label;
                this.mPackageName = ((Context)activity).getPackageName();
                this.removePreference("unrestricted_data_saver");
                this.removePreference("app_settings");
                this.removePreference("restrict_background");
                this.removePreference("app_list");
            }
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    @Override
    public void onDataSaverChanged(final boolean b) {
    }
    
    @Override
    public void onDestroy() {
        TrafficStats.closeQuietly(this.mStatsSession);
        super.onDestroy();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (this.mDataSaverBackend != null) {
            this.mDataSaverBackend.remListener((DataSaverBackend.Listener)this);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mRestrictBackground) {
            this.mDataSaverBackend.setIsBlacklisted(this.mAppItem.key, this.mPackageName, (boolean)o ^ true);
            this.updatePrefs();
            return true;
        }
        if (preference == this.mUnrestrictedData) {
            this.mDataSaverBackend.setIsWhitelisted(this.mAppItem.key, this.mPackageName, (boolean)o);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.mAppSettings) {
            this.getActivity().startActivityAsUser(this.mAppSettingsIntent, new UserHandle(UserHandle.getUserId(this.mAppItem.key)));
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mDataSaverBackend != null) {
            this.mDataSaverBackend.addListener((DataSaverBackend.Listener)this);
        }
        this.mPolicy = this.services.mPolicyEditor.getPolicy(this.mTemplate);
        this.getLoaderManager().restartLoader(2, ChartDataLoader.buildArgs(this.mTemplate, this.mAppItem), (LoaderManager$LoaderCallbacks)this.mChartDataCallbacks);
        this.updatePrefs();
    }
    
    @Override
    public void onViewCreated(View packageName, final Bundle bundle) {
        super.onViewCreated((View)packageName, bundle);
        if (this.mPackages.size() != 0) {
            packageName = this.mPackages.valueAt(0);
        }
        else {
            packageName = null;
        }
        int packageUidAsUser = 0;
        if (packageName != null) {
            try {
                packageUidAsUser = this.mPackageManagerWrapper.getPackageUidAsUser((String)packageName, UserHandle.getUserId(this.mAppItem.key));
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping UID because cannot find package ");
                sb.append((String)packageName);
                Log.w("AppDataUsage", sb.toString());
                packageUidAsUser = packageUidAsUser;
            }
        }
        final boolean hasAppInfoLink = this.mAppItem.key > 0;
        final Activity activity = this.getActivity();
        this.getPreferenceScreen().addPreference(EntityHeaderController.newInstance(activity, this, null).setRecyclerView(this.getListView(), this.getLifecycle()).setUid(packageUidAsUser).setHasAppInfoLink(hasAppInfoLink).setButtonActions(0, 0).setIcon(this.mIcon).setLabel(this.mLabel).setPackageName((String)packageName).done(activity, this.getPrefContext()));
    }
    
    @Override
    public void onWhitelistStatusChanged(final int n, final boolean b) {
        if (this.mAppItem.uids.get(n, false)) {
            this.updatePrefs(this.getAppRestrictBackground(), b);
        }
    }
    
    void updatePrefs() {
        this.updatePrefs(this.getAppRestrictBackground(), this.getUnrestrictData());
    }
}
