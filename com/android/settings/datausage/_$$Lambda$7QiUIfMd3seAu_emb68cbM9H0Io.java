package com.android.settings.datausage;

import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$7QiUIfMd3seAu_emb68cbM9H0Io implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return new DataUsageSummaryLegacy.SummaryProvider(activity, summaryLoader);
    }
}
