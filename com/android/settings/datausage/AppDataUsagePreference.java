package com.android.settings.datausage;

import android.widget.ProgressBar;
import android.support.v7.preference.PreferenceViewHolder;
import android.graphics.drawable.Drawable;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settingslib.net.UidDetailProvider;
import android.content.Context;
import com.android.settingslib.AppItem;
import com.android.settingslib.net.UidDetail;
import com.android.settings.widget.AppPreference;

public class AppDataUsagePreference extends AppPreference
{
    private UidDetail mDetail;
    private final AppItem mItem;
    private final int mPercent;
    
    public AppDataUsagePreference(final Context context, final AppItem mItem, final int mPercent, final UidDetailProvider uidDetailProvider) {
        super(context);
        this.mItem = mItem;
        this.mPercent = mPercent;
        if (mItem.restricted && mItem.total <= 0L) {
            this.setSummary(2131887223);
        }
        else {
            this.setSummary(DataUsageUtils.formatDataUsage(context, mItem.total));
        }
        this.mDetail = uidDetailProvider.getUidDetail(mItem.key, false);
        if (this.mDetail != null) {
            this.setAppInfo();
        }
        else {
            ThreadUtils.postOnBackgroundThread(new _$$Lambda$AppDataUsagePreference$1CecIqCNArEHKTwkPb92cZEWQPk(this, uidDetailProvider));
        }
    }
    
    private void setAppInfo() {
        if (this.mDetail != null) {
            this.setIcon(this.mDetail.icon);
            this.setTitle(this.mDetail.label);
        }
        else {
            this.setIcon(null);
            this.setTitle(null);
        }
    }
    
    public AppItem getItem() {
        return this.mItem;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final ProgressBar progressBar = (ProgressBar)preferenceViewHolder.findViewById(16908301);
        if (this.mItem.restricted && this.mItem.total <= 0L) {
            progressBar.setVisibility(8);
        }
        else {
            progressBar.setVisibility(0);
        }
        progressBar.setProgress(this.mPercent);
    }
}
