package com.android.settings.datausage;

import android.telephony.SubscriptionPlan;
import android.telephony.SubscriptionManager;
import com.android.settingslib.Utils;
import com.android.settingslib.net.DataUsageController;
import android.content.ComponentName;
import android.content.Intent;
import android.view.MenuItem;
import android.os.Bundle;
import com.android.settingslib.NetworkPolicyEditor;
import android.app.Activity;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settingslib.core.AbstractPreferenceController;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.text.format.Formatter$BytesResult;
import android.text.BidiFormatter;
import android.text.style.RelativeSizeSpan;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.text.TextUtils;
import android.telephony.SubscriptionInfo;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.net.NetworkTemplate;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;

public class DataUsageSummary extends DataUsageBaseFragment implements DataUsageEditController, Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private NetworkTemplate mDefaultTemplate;
    private DataUsageSummaryPreferenceController mSummaryController;
    private DataUsageSummaryPreference mSummaryPreference;
    
    static {
        SUMMARY_PROVIDER_FACTORY = (SummaryProviderFactory)_$$Lambda$YwlDb_ChrdnT61OB_L_A63UT4To.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (!DataUsageUtils.hasMobileData(context)) {
                    nonIndexableKeys.add("mobile_category");
                    nonIndexableKeys.add("data_usage_enable");
                    nonIndexableKeys.add("cellular_data_usage");
                    nonIndexableKeys.add("billing_preference");
                }
                if (!DataUsageUtils.hasWifiRadio(context)) {
                    nonIndexableKeys.add("wifi_data_usage");
                }
                nonIndexableKeys.add("wifi_category");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082739;
                list.add(searchIndexableResource);
                final SearchIndexableResource searchIndexableResource2 = new SearchIndexableResource(context);
                searchIndexableResource2.xmlResId = 2132082740;
                list.add(searchIndexableResource2);
                final SearchIndexableResource searchIndexableResource3 = new SearchIndexableResource(context);
                searchIndexableResource3.xmlResId = 2132082745;
                list.add(searchIndexableResource3);
                return list;
            }
        };
    }
    
    private void addEthernetSection() {
        ((TemplatePreferenceCategory)this.inflatePreferences(2132082741)).setTemplate(NetworkTemplate.buildTemplateEthernet(), 0, this.services);
    }
    
    private void addMobileSection(final int n, final SubscriptionInfo subscriptionInfo) {
        final TemplatePreferenceCategory templatePreferenceCategory = (TemplatePreferenceCategory)this.inflatePreferences(2132082740);
        templatePreferenceCategory.setTemplate(this.getNetworkTemplate(n), n, this.services);
        templatePreferenceCategory.pushTemplates(this.services);
        if (subscriptionInfo != null && !TextUtils.isEmpty(subscriptionInfo.getDisplayName())) {
            templatePreferenceCategory.findPreference("mobile_category").setTitle(subscriptionInfo.getDisplayName());
        }
    }
    
    static CharSequence formatUsage(final Context context, final String s, final long n) {
        return formatUsage(context, s, n, 1.5625f, 0.64f);
    }
    
    static CharSequence formatUsage(final Context context, final String s, final long n, final float n2, final float n3) {
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(context.getResources(), n, 10);
        final SpannableString spannableString = new SpannableString((CharSequence)formatBytes.value);
        spannableString.setSpan((Object)new RelativeSizeSpan(n2), 0, spannableString.length(), 18);
        final CharSequence expandTemplate = TextUtils.expandTemplate((CharSequence)new SpannableString((CharSequence)context.getString(17039897).replace("%1$s", "^1").replace("%2$s", "^2")), new CharSequence[] { spannableString, formatBytes.units });
        final SpannableString spannableString2 = new SpannableString((CharSequence)s);
        spannableString2.setSpan((Object)new RelativeSizeSpan(n3), 0, spannableString2.length(), 18);
        return TextUtils.expandTemplate((CharSequence)spannableString2, new CharSequence[] { BidiFormatter.getInstance().unicodeWrap(expandTemplate.toString()) });
    }
    
    private NetworkTemplate getNetworkTemplate(final int n) {
        return NetworkTemplate.normalize(NetworkTemplate.buildTemplateMobileAll(this.services.mTelephonyManager.getSubscriberId(n)), this.services.mTelephonyManager.getMergedSubscriberIds());
    }
    
    private Preference inflatePreferences(final int n) {
        final PreferenceScreen inflateFromResource = this.getPreferenceManager().inflateFromResource(this.getPrefContext(), n, null);
        final Preference preference = inflateFromResource.getPreference(0);
        inflateFromResource.removeAll();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preference.setOrder(preferenceScreen.getPreferenceCount());
        preferenceScreen.addPreference(preference);
        return preference;
    }
    
    private void updateState() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        for (int i = 1; i < preferenceScreen.getPreferenceCount(); ++i) {
            final Preference preference = preferenceScreen.getPreference(i);
            if (preference instanceof TemplatePreferenceCategory) {
                ((TemplatePreferenceCategory)preference).pushTemplates(this.services);
            }
        }
    }
    
    void addMobileSection(final int n) {
        this.addMobileSection(n, null);
    }
    
    void addWifiSection() {
        ((TemplatePreferenceCategory)this.inflatePreferences(2132082745)).setTemplate(NetworkTemplate.buildTemplateWifiWildcard(), 0, this.services);
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final Activity activity = this.getActivity();
        final ArrayList<DataUsageSummaryPreferenceController> list = (ArrayList<DataUsageSummaryPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(this.mSummaryController = new DataUsageSummaryPreferenceController(activity, this.getLifecycle(), this));
        this.getLifecycle().addObserver(this.mSummaryController);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887806;
    }
    
    @Override
    protected String getLogTag() {
        return "DataUsageSummary";
    }
    
    @Override
    public int getMetricsCategory() {
        return 37;
    }
    
    @Override
    public NetworkPolicyEditor getNetworkPolicyEditor() {
        return this.services.mPolicyEditor;
    }
    
    @Override
    public NetworkTemplate getNetworkTemplate() {
        return this.mDefaultTemplate;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082739;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        boolean hasMobileData = DataUsageUtils.hasMobileData(context);
        final int defaultSubscriptionId = DataUsageUtils.getDefaultSubscriptionId(context);
        if (defaultSubscriptionId == -1) {
            hasMobileData = false;
        }
        this.mDefaultTemplate = DataUsageUtils.getDefaultTemplate(context, defaultSubscriptionId);
        this.mSummaryPreference = (DataUsageSummaryPreference)this.findPreference("status_header");
        if (!hasMobileData || !this.isAdmin()) {
            this.removePreference("restrict_background");
        }
        final boolean hasWifiRadio = DataUsageUtils.hasWifiRadio(context);
        if (hasMobileData) {
            this.addMobileSection(defaultSubscriptionId);
            if (DataUsageUtils.hasSim(context) && hasWifiRadio) {
                this.addWifiSection();
            }
        }
        else if (hasWifiRadio) {
            this.addWifiSection();
        }
        if (DataUsageUtils.hasEthernet(context)) {
            this.addEthernetSection();
        }
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 2131362048) {
            return false;
        }
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(new ComponentName("com.android.phone", "com.android.phone.MobileNetworkSettings"));
        this.startActivity(intent);
        return true;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.findPreference("status_header")) {
            BillingCycleSettings.BytesEditorFragment.show(this, false);
            return false;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateState();
    }
    
    @Override
    public void updateDataUsage() {
        this.updateState();
        this.mSummaryController.updateState(this.mSummaryPreference);
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Activity mActivity;
        private final DataUsageController mDataController;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Activity mActivity, final SummaryLoader mSummaryLoader) {
            this.mActivity = mActivity;
            this.mSummaryLoader = mSummaryLoader;
            this.mDataController = new DataUsageController((Context)mActivity);
        }
        
        private CharSequence formatFallbackData() {
            final DataUsageController.DataUsageInfo dataUsageInfo = this.mDataController.getDataUsageInfo();
            if (dataUsageInfo == null) {
                return DataUsageUtils.formatDataUsage((Context)this.mActivity, 0L);
            }
            if (dataUsageInfo.limitLevel <= 0L) {
                return DataUsageUtils.formatDataUsage((Context)this.mActivity, dataUsageInfo.usageLevel);
            }
            return Utils.formatPercentage(dataUsageInfo.usageLevel, dataUsageInfo.limitLevel);
        }
        
        private CharSequence formatUsedData() {
            final SubscriptionManager subscriptionManager = (SubscriptionManager)this.mActivity.getSystemService("telephony_subscription_service");
            final int defaultSubscriptionId = SubscriptionManager.getDefaultSubscriptionId();
            if (defaultSubscriptionId == -1) {
                return this.formatFallbackData();
            }
            final SubscriptionPlan primaryPlan = DataUsageSummaryPreferenceController.getPrimaryPlan(subscriptionManager, defaultSubscriptionId);
            if (primaryPlan == null) {
                return this.formatFallbackData();
            }
            if (DataUsageSummaryPreferenceController.unlimited(primaryPlan.getDataLimitBytes())) {
                return DataUsageUtils.formatDataUsage((Context)this.mActivity, primaryPlan.getDataUsageBytes());
            }
            return Utils.formatPercentage(primaryPlan.getDataUsageBytes(), primaryPlan.getDataLimitBytes());
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                if (DataUsageUtils.hasSim((Context)this.mActivity)) {
                    this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mActivity.getString(2131887284, new Object[] { this.formatUsedData() }));
                }
                else {
                    final DataUsageController.DataUsageInfo dataUsageInfo = this.mDataController.getDataUsageInfo(NetworkTemplate.buildTemplateWifiWildcard());
                    if (dataUsageInfo == null) {
                        this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, null);
                    }
                    else {
                        this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, TextUtils.expandTemplate(this.mActivity.getText(2131887300), new CharSequence[] { DataUsageUtils.formatDataUsage((Context)this.mActivity, dataUsageInfo.usageLevel) }));
                    }
                }
            }
        }
    }
}
