package com.android.settings.datausage;

import android.app.Activity;
import android.os.UserManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.android.settingslib.NetworkPolicyEditor;
import android.content.Context;
import android.net.NetworkPolicyManager;
import android.net.INetworkStatsService$Stub;
import android.os.INetworkManagementService$Stub;
import android.os.ServiceManager;
import android.os.Bundle;
import android.net.NetworkPolicy;
import android.os.RemoteException;
import android.util.Log;
import com.android.settings.SettingsPreferenceFragment;

@Deprecated
public abstract class DataUsageBase extends SettingsPreferenceFragment
{
    protected final TemplatePreference.NetworkServices services;
    
    public DataUsageBase() {
        this.services = new TemplatePreference.NetworkServices();
    }
    
    private boolean isDataEnabled(final int n) {
        return n == -1 || this.services.mTelephonyManager.getDataEnabled(n);
    }
    
    protected boolean isBandwidthControlEnabled() {
        try {
            return this.services.mNetworkService.isBandwidthControlEnabled();
        }
        catch (RemoteException ex) {
            Log.w("DataUsageBase", "problem talking with INetworkManagementService: ", (Throwable)ex);
            return false;
        }
    }
    
    protected boolean isMobileDataAvailable(final int n) {
        return this.services.mSubscriptionManager.getActiveSubscriptionInfo(n) != null;
    }
    
    protected boolean isNetworkPolicyModifiable(final NetworkPolicy networkPolicy, final int n) {
        return networkPolicy != null && this.isBandwidthControlEnabled() && this.services.mUserManager.isAdminUser() && this.isDataEnabled(n);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.services.mNetworkService = INetworkManagementService$Stub.asInterface(ServiceManager.getService("network_management"));
        this.services.mStatsService = INetworkStatsService$Stub.asInterface(ServiceManager.getService("netstats"));
        this.services.mPolicyManager = NetworkPolicyManager.from((Context)activity);
        this.services.mPolicyEditor = new NetworkPolicyEditor(this.services.mPolicyManager);
        this.services.mTelephonyManager = TelephonyManager.from((Context)activity);
        this.services.mSubscriptionManager = SubscriptionManager.from((Context)activity);
        this.services.mUserManager = UserManager.get((Context)activity);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.services.mPolicyEditor.read();
    }
}
