package com.android.settings.datausage;

import android.os.UserManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.android.settingslib.NetworkPolicyEditor;
import android.net.NetworkPolicyManager;
import android.net.INetworkStatsService$Stub;
import android.os.INetworkManagementService$Stub;
import android.os.ServiceManager;
import android.os.Bundle;
import android.net.INetworkStatsSession;
import android.os.RemoteException;
import android.net.TrafficStats;
import android.net.NetworkTemplate;
import android.net.ConnectivityManager;
import android.content.Context;
import com.android.settings.dashboard.DashboardFragment;

public abstract class DataUsageBaseFragment extends DashboardFragment
{
    protected final TemplatePreference.NetworkServices services;
    
    public DataUsageBaseFragment() {
        this.services = new TemplatePreference.NetworkServices();
    }
    
    public boolean hasEthernet(final Context context) {
        final boolean networkSupported = ConnectivityManager.from(context).isNetworkSupported(9);
        try {
            final INetworkStatsSession openSession = this.services.mStatsService.openSession();
            long totalBytes;
            if (openSession != null) {
                totalBytes = openSession.getSummaryForNetwork(NetworkTemplate.buildTemplateEthernet(), Long.MIN_VALUE, Long.MAX_VALUE).getTotalBytes();
                TrafficStats.closeQuietly(openSession);
            }
            else {
                totalBytes = 0L;
            }
            return networkSupported && totalBytes > 0L;
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    protected boolean isAdmin() {
        return this.services.mUserManager.isAdminUser();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        this.services.mNetworkService = INetworkManagementService$Stub.asInterface(ServiceManager.getService("network_management"));
        this.services.mStatsService = INetworkStatsService$Stub.asInterface(ServiceManager.getService("netstats"));
        this.services.mPolicyManager = (NetworkPolicyManager)context.getSystemService("netpolicy");
        this.services.mPolicyEditor = new NetworkPolicyEditor(this.services.mPolicyManager);
        this.services.mTelephonyManager = TelephonyManager.from(context);
        this.services.mSubscriptionManager = SubscriptionManager.from(context);
        this.services.mUserManager = UserManager.get(context);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.services.mPolicyEditor.read();
    }
}
