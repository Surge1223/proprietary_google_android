package com.android.settings.datausage;

import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.net.INetworkStatsService;
import android.net.NetworkPolicyManager;
import com.android.settingslib.NetworkPolicyEditor;
import android.os.INetworkManagementService;
import android.net.NetworkTemplate;

public interface TemplatePreference
{
    void setTemplate(final NetworkTemplate p0, final int p1, final NetworkServices p2);
    
    public static class NetworkServices
    {
        INetworkManagementService mNetworkService;
        NetworkPolicyEditor mPolicyEditor;
        NetworkPolicyManager mPolicyManager;
        INetworkStatsService mStatsService;
        SubscriptionManager mSubscriptionManager;
        TelephonyManager mTelephonyManager;
        UserManager mUserManager;
    }
}
