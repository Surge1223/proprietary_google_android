package com.android.settings.datausage;

import android.support.v7.preference.Preference;
import android.util.AttributeSet;
import android.content.Context;
import android.net.NetworkTemplate;
import android.support.v7.preference.PreferenceCategory;

public class TemplatePreferenceCategory extends PreferenceCategory implements TemplatePreference
{
    private int mSubId;
    private NetworkTemplate mTemplate;
    
    public TemplatePreferenceCategory(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public boolean addPreference(final Preference preference) {
        if (preference instanceof TemplatePreference) {
            return super.addPreference(preference);
        }
        throw new IllegalArgumentException("TemplatePreferenceCategories can only hold TemplatePreferences");
    }
    
    public void pushTemplates(final NetworkServices networkServices) {
        if (this.mTemplate != null) {
            for (int i = 0; i < this.getPreferenceCount(); ++i) {
                ((TemplatePreference)this.getPreference(i)).setTemplate(this.mTemplate, this.mSubId, networkServices);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("null mTemplate for ");
        sb.append(this.getKey());
        throw new RuntimeException(sb.toString());
    }
    
    @Override
    public void setTemplate(final NetworkTemplate mTemplate, final int mSubId, final NetworkServices networkServices) {
        this.mTemplate = mTemplate;
        this.mSubId = mSubId;
    }
}
