package com.android.settings.datausage;

import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import android.support.v7.preference.PreferenceViewHolder;
import android.view.View;
import android.widget.AdapterView;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.AdapterView$OnItemSelectedListener;
import android.support.v7.preference.Preference;

public class SpinnerPreference extends Preference implements SpinnerInterface
{
    private CycleAdapter mAdapter;
    private Object mCurrentObject;
    private AdapterView$OnItemSelectedListener mListener;
    private final AdapterView$OnItemSelectedListener mOnSelectedListener;
    private int mPosition;
    
    public SpinnerPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mOnSelectedListener = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                if (SpinnerPreference.this.mPosition == n) {
                    return;
                }
                SpinnerPreference.this.mPosition = n;
                SpinnerPreference.this.mCurrentObject = SpinnerPreference.this.mAdapter.getItem(n);
                SpinnerPreference.this.mListener.onItemSelected((AdapterView)adapterView, view, n, n2);
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
                SpinnerPreference.this.mListener.onNothingSelected((AdapterView)adapterView);
            }
        };
        this.setLayoutResource(2131558528);
    }
    
    @Override
    public Object getSelectedItem() {
        return this.mCurrentObject;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final Spinner spinner = (Spinner)preferenceViewHolder.findViewById(2131362034);
        spinner.setAdapter((SpinnerAdapter)this.mAdapter);
        spinner.setSelection(this.mPosition);
        spinner.setOnItemSelectedListener(this.mOnSelectedListener);
    }
    
    @Override
    protected void performClick(final View view) {
        view.findViewById(2131362034).performClick();
    }
    
    @Override
    public void setAdapter(final CycleAdapter mAdapter) {
        this.mAdapter = mAdapter;
        this.notifyChanged();
    }
    
    @Override
    public void setOnItemSelectedListener(final AdapterView$OnItemSelectedListener mListener) {
        this.mListener = mListener;
    }
    
    @Override
    public void setSelection(final int mPosition) {
        this.mPosition = mPosition;
        this.mCurrentObject = this.mAdapter.getItem(this.mPosition);
        this.notifyChanged();
    }
}
