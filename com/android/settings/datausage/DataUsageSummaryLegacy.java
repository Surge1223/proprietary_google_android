package com.android.settings.datausage;

import com.android.settingslib.Utils;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.view.MenuItem;
import android.os.UserManager;
import android.view.MenuInflater;
import android.view.Menu;
import android.net.NetworkPolicyManager;
import android.os.Bundle;
import com.android.settingslib.core.AbstractPreferenceController;
import android.support.v7.preference.PreferenceScreen;
import android.text.format.Formatter$BytesResult;
import android.text.BidiFormatter;
import android.text.style.RelativeSizeSpan;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.text.TextUtils;
import android.telephony.SubscriptionInfo;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.SummaryPreference;
import com.android.settingslib.NetworkPolicyEditor;
import android.support.v7.preference.Preference;
import android.net.NetworkTemplate;
import com.android.settingslib.net.DataUsageController;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;

public class DataUsageSummaryLegacy extends DataUsageBaseFragment implements DataUsageEditController, Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private DataUsageInfoController mDataInfoController;
    private DataUsageController mDataUsageController;
    private int mDataUsageTemplate;
    private NetworkTemplate mDefaultTemplate;
    private Preference mLimitPreference;
    private NetworkPolicyEditor mPolicyEditor;
    private SummaryPreference mSummaryPreference;
    
    static {
        SUMMARY_PROVIDER_FACTORY = (SummaryProviderFactory)_$$Lambda$7QiUIfMd3seAu_emb68cbM9H0Io.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (!DataUsageUtils.hasMobileData(context)) {
                    nonIndexableKeys.add("mobile_category");
                    nonIndexableKeys.add("data_usage_enable");
                    nonIndexableKeys.add("cellular_data_usage");
                    nonIndexableKeys.add("billing_preference");
                }
                if (!DataUsageUtils.hasWifiRadio(context)) {
                    nonIndexableKeys.add("wifi_data_usage");
                }
                nonIndexableKeys.add("wifi_category");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082742;
                list.add(searchIndexableResource);
                final SearchIndexableResource searchIndexableResource2 = new SearchIndexableResource(context);
                searchIndexableResource2.xmlResId = 2132082740;
                list.add(searchIndexableResource2);
                final SearchIndexableResource searchIndexableResource3 = new SearchIndexableResource(context);
                searchIndexableResource3.xmlResId = 2132082745;
                list.add(searchIndexableResource3);
                return list;
            }
        };
    }
    
    private void addEthernetSection() {
        ((TemplatePreferenceCategory)this.inflatePreferences(2132082741)).setTemplate(NetworkTemplate.buildTemplateEthernet(), 0, this.services);
    }
    
    private void addMobileSection(final int n) {
        this.addMobileSection(n, null);
    }
    
    private void addMobileSection(final int n, final SubscriptionInfo subscriptionInfo) {
        final TemplatePreferenceCategory templatePreferenceCategory = (TemplatePreferenceCategory)this.inflatePreferences(2132082740);
        templatePreferenceCategory.setTemplate(this.getNetworkTemplate(n), n, this.services);
        templatePreferenceCategory.pushTemplates(this.services);
        if (subscriptionInfo != null && !TextUtils.isEmpty(subscriptionInfo.getDisplayName())) {
            templatePreferenceCategory.findPreference("mobile_category").setTitle(subscriptionInfo.getDisplayName());
        }
    }
    
    private void addWifiSection() {
        ((TemplatePreferenceCategory)this.inflatePreferences(2132082745)).setTemplate(NetworkTemplate.buildTemplateWifiWildcard(), 0, this.services);
    }
    
    static CharSequence formatUsage(final Context context, final String s, final long n) {
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(context.getResources(), n, 2);
        final SpannableString spannableString = new SpannableString((CharSequence)formatBytes.value);
        spannableString.setSpan((Object)new RelativeSizeSpan(1.5625f), 0, spannableString.length(), 18);
        final CharSequence expandTemplate = TextUtils.expandTemplate((CharSequence)new SpannableString((CharSequence)context.getString(17039897).replace("%1$s", "^1").replace("%2$s", "^2")), new CharSequence[] { spannableString, formatBytes.units });
        final SpannableString spannableString2 = new SpannableString((CharSequence)s);
        spannableString2.setSpan((Object)new RelativeSizeSpan(0.64f), 0, spannableString2.length(), 18);
        return TextUtils.expandTemplate((CharSequence)spannableString2, new CharSequence[] { BidiFormatter.getInstance().unicodeWrap(expandTemplate.toString()) });
    }
    
    private NetworkTemplate getNetworkTemplate(final int n) {
        return NetworkTemplate.normalize(NetworkTemplate.buildTemplateMobileAll(this.services.mTelephonyManager.getSubscriberId(n)), this.services.mTelephonyManager.getMergedSubscriberIds());
    }
    
    private Preference inflatePreferences(final int n) {
        final PreferenceScreen inflateFromResource = this.getPreferenceManager().inflateFromResource(this.getPrefContext(), n, null);
        final Preference preference = inflateFromResource.getPreference(0);
        inflateFromResource.removeAll();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preference.setOrder(preferenceScreen.getPreferenceCount());
        preferenceScreen.addPreference(preference);
        return preference;
    }
    
    private void updateState() {
        final DataUsageController.DataUsageInfo dataUsageInfo = this.mDataUsageController.getDataUsageInfo(this.mDefaultTemplate);
        final Context context = this.getContext();
        this.mDataInfoController.updateDataLimit(dataUsageInfo, this.services.mPolicyEditor.getPolicy(this.mDefaultTemplate));
        final SummaryPreference mSummaryPreference = this.mSummaryPreference;
        final int n = 1;
        if (mSummaryPreference != null) {
            this.mSummaryPreference.setTitle(formatUsage(context, this.getString(this.mDataUsageTemplate), dataUsageInfo.usageLevel));
            final long summaryLimit = this.mDataInfoController.getSummaryLimit(dataUsageInfo);
            this.mSummaryPreference.setSummary(dataUsageInfo.period);
            if (summaryLimit <= 0L) {
                this.mSummaryPreference.setChartEnabled(false);
            }
            else {
                this.mSummaryPreference.setChartEnabled(true);
                this.mSummaryPreference.setLabels(Formatter.formatFileSize(context, 0L), Formatter.formatFileSize(context, summaryLimit));
                this.mSummaryPreference.setRatios(dataUsageInfo.usageLevel / summaryLimit, 0.0f, (summaryLimit - dataUsageInfo.usageLevel) / summaryLimit);
            }
        }
        if (this.mLimitPreference != null && (dataUsageInfo.warningLevel > 0L || dataUsageInfo.limitLevel > 0L)) {
            final String formatFileSize = Formatter.formatFileSize(context, dataUsageInfo.warningLevel);
            final String formatFileSize2 = Formatter.formatFileSize(context, dataUsageInfo.limitLevel);
            final Preference mLimitPreference = this.mLimitPreference;
            int n2;
            if (dataUsageInfo.limitLevel <= 0L) {
                n2 = 2131886992;
            }
            else {
                n2 = 2131886991;
            }
            mLimitPreference.setSummary(this.getString(n2, new Object[] { formatFileSize, formatFileSize2 }));
        }
        else if (this.mLimitPreference != null) {
            this.mLimitPreference.setSummary(null);
        }
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        for (int i = n; i < preferenceScreen.getPreferenceCount(); ++i) {
            ((TemplatePreferenceCategory)preferenceScreen.getPreference(i)).pushTemplates(this.services);
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return null;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887806;
    }
    
    @Override
    protected String getLogTag() {
        return "DataUsageSummaryLegacy";
    }
    
    @Override
    public int getMetricsCategory() {
        return 37;
    }
    
    @Override
    public NetworkPolicyEditor getNetworkPolicyEditor() {
        return this.services.mPolicyEditor;
    }
    
    @Override
    public NetworkTemplate getNetworkTemplate() {
        return this.mDefaultTemplate;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082742;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        this.mPolicyEditor = new NetworkPolicyEditor(NetworkPolicyManager.from(context));
        boolean hasMobileData = DataUsageUtils.hasMobileData(context);
        this.mDataUsageController = new DataUsageController(context);
        this.mDataInfoController = new DataUsageInfoController();
        final int defaultSubscriptionId = DataUsageUtils.getDefaultSubscriptionId(context);
        if (defaultSubscriptionId == -1) {
            hasMobileData = false;
        }
        this.mDefaultTemplate = DataUsageUtils.getDefaultTemplate(context, defaultSubscriptionId);
        this.mSummaryPreference = (SummaryPreference)this.findPreference("status_header");
        if (!hasMobileData || !this.isAdmin()) {
            this.removePreference("restrict_background_legacy");
        }
        int n = 0;
        if (hasMobileData) {
            this.mLimitPreference = this.findPreference("limit_summary");
            final List activeSubscriptionInfoList = this.services.mSubscriptionManager.getActiveSubscriptionInfoList();
            if (activeSubscriptionInfoList == null || activeSubscriptionInfoList.size() == 0) {
                this.addMobileSection(defaultSubscriptionId);
            }
            while (activeSubscriptionInfoList != null && n < activeSubscriptionInfoList.size()) {
                final SubscriptionInfo subscriptionInfo = activeSubscriptionInfoList.get(n);
                if (activeSubscriptionInfoList.size() > 1) {
                    this.addMobileSection(subscriptionInfo.getSubscriptionId(), subscriptionInfo);
                }
                else {
                    this.addMobileSection(subscriptionInfo.getSubscriptionId());
                }
                ++n;
            }
            this.mSummaryPreference.setSelectable(true);
        }
        else {
            this.removePreference("limit_summary");
            this.mSummaryPreference.setSelectable(false);
        }
        final boolean hasWifiRadio = DataUsageUtils.hasWifiRadio(context);
        if (hasWifiRadio) {
            this.addWifiSection();
        }
        if (this.hasEthernet(context)) {
            this.addEthernetSection();
        }
        int mDataUsageTemplate;
        if (hasMobileData) {
            mDataUsageTemplate = 2131886988;
        }
        else if (hasWifiRadio) {
            mDataUsageTemplate = 2131889943;
        }
        else {
            mDataUsageTemplate = 2131887611;
        }
        this.mDataUsageTemplate = mDataUsageTemplate;
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        if (UserManager.get(this.getContext()).isAdminUser()) {
            menuInflater.inflate(2131623937, menu);
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 2131362048) {
            return false;
        }
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(new ComponentName("com.android.phone", "com.android.phone.MobileNetworkSettings"));
        this.startActivity(intent);
        return true;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.findPreference("status_header")) {
            BillingCycleSettings.BytesEditorFragment.show(this, false);
            return false;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateState();
    }
    
    @Override
    public void updateDataUsage() {
        this.updateState();
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Activity mActivity;
        private final DataUsageController mDataController;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Activity mActivity, final SummaryLoader mSummaryLoader) {
            this.mActivity = mActivity;
            this.mSummaryLoader = mSummaryLoader;
            this.mDataController = new DataUsageController((Context)mActivity);
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                final DataUsageController.DataUsageInfo dataUsageInfo = this.mDataController.getDataUsageInfo();
                String s;
                if (dataUsageInfo == null) {
                    s = Formatter.formatFileSize((Context)this.mActivity, 0L);
                }
                else if (dataUsageInfo.limitLevel <= 0L) {
                    s = Formatter.formatFileSize((Context)this.mActivity, dataUsageInfo.usageLevel);
                }
                else {
                    s = Utils.formatPercentage(dataUsageInfo.usageLevel, dataUsageInfo.limitLevel);
                }
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mActivity.getString(2131887284, new Object[] { s }));
            }
        }
    }
}
