package com.android.settings.datausage;

import android.net.NetworkPolicy;
import com.android.settingslib.net.DataUsageController;

public class DataUsageInfoController
{
    public long getSummaryLimit(final DataUsageController.DataUsageInfo dataUsageInfo) {
        long n;
        if ((n = dataUsageInfo.limitLevel) <= 0L) {
            n = dataUsageInfo.warningLevel;
        }
        long usageLevel = n;
        if (dataUsageInfo.usageLevel > n) {
            usageLevel = dataUsageInfo.usageLevel;
        }
        return usageLevel;
    }
    
    public void updateDataLimit(final DataUsageController.DataUsageInfo dataUsageInfo, final NetworkPolicy networkPolicy) {
        if (dataUsageInfo != null && networkPolicy != null) {
            if (networkPolicy.warningBytes >= 0L) {
                dataUsageInfo.warningLevel = networkPolicy.warningBytes;
            }
            if (networkPolicy.limitBytes >= 0L) {
                dataUsageInfo.limitLevel = networkPolicy.limitBytes;
            }
        }
    }
}
