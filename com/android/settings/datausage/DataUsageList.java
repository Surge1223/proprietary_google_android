package com.android.settings.datausage;

import android.content.Intent;
import android.widget.SpinnerAdapter;
import android.view.View.OnClickListener;
import android.os.AsyncTask;
import android.net.TrafficStats;
import android.os.RemoteException;
import android.util.Log;
import java.util.Collections;
import android.os.UserHandle;
import android.os.UserManager;
import android.app.ActivityManager;
import android.net.NetworkPolicy;
import android.net.NetworkStatsHistory$Entry;
import android.app.Activity;
import android.graphics.Color;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import java.util.Iterator;
import java.util.List;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.net.ConnectivityManager;
import java.util.ArrayList;
import android.net.NetworkStats$Entry;
import android.util.SparseArray;
import com.android.settingslib.AppItem;
import com.android.settingslib.net.SummaryForAllUidLoader;
import android.net.NetworkStatsHistory;
import android.content.Context;
import com.android.settingslib.net.ChartDataLoader;
import android.content.Loader;
import android.os.Bundle;
import android.widget.AdapterView;
import android.support.v7.preference.Preference;
import com.android.settingslib.net.UidDetailProvider;
import android.net.NetworkTemplate;
import android.net.NetworkStats;
import android.net.INetworkStatsSession;
import com.android.settings.widget.LoadingViewController;
import android.view.View;
import android.widget.Spinner;
import android.widget.AdapterView$OnItemSelectedListener;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settingslib.net.ChartData;
import android.support.v7.preference.PreferenceGroup;

public class DataUsageList extends DataUsageBase
{
    private PreferenceGroup mApps;
    private ChartDataUsagePreference mChart;
    private ChartData mChartData;
    private final LoaderManager$LoaderCallbacks<ChartData> mChartDataCallbacks;
    private CycleAdapter mCycleAdapter;
    private AdapterView$OnItemSelectedListener mCycleListener;
    private Spinner mCycleSpinner;
    private final CellDataPreference.DataStateListener mDataStateListener;
    private View mHeader;
    private LoadingViewController mLoadingViewController;
    private INetworkStatsSession mStatsSession;
    int mSubId;
    private final LoaderManager$LoaderCallbacks<NetworkStats> mSummaryCallbacks;
    NetworkTemplate mTemplate;
    private UidDetailProvider mUidDetailProvider;
    private Preference mUsageAmount;
    
    public DataUsageList() {
        this.mDataStateListener = new CellDataPreference.DataStateListener() {
            public void onChange(final boolean b) {
                DataUsageList.this.updatePolicy();
            }
        };
        this.mSubId = -1;
        this.mCycleListener = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                final CycleAdapter.CycleItem cycleItem = (CycleAdapter.CycleItem)DataUsageList.this.mCycleSpinner.getSelectedItem();
                DataUsageList.this.mChart.setVisibleRange(cycleItem.start, cycleItem.end);
                DataUsageList.this.updateDetailData();
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        };
        this.mChartDataCallbacks = (LoaderManager$LoaderCallbacks<ChartData>)new LoaderManager$LoaderCallbacks<ChartData>() {
            public Loader<ChartData> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<ChartData>)new ChartDataLoader((Context)DataUsageList.this.getActivity(), DataUsageList.this.mStatsSession, bundle);
            }
            
            public void onLoadFinished(final Loader<ChartData> loader, final ChartData chartData) {
                DataUsageList.this.mLoadingViewController.showContent(false);
                DataUsageList.this.mChartData = chartData;
                DataUsageList.this.mChart.setNetworkStats(DataUsageList.this.mChartData.network);
                DataUsageList.this.updatePolicy();
            }
            
            public void onLoaderReset(final Loader<ChartData> loader) {
                DataUsageList.this.mChartData = null;
                DataUsageList.this.mChart.setNetworkStats(null);
            }
        };
        this.mSummaryCallbacks = (LoaderManager$LoaderCallbacks<NetworkStats>)new LoaderManager$LoaderCallbacks<NetworkStats>() {
            private void updateEmptyVisible() {
                final int preferenceCount = DataUsageList.this.mApps.getPreferenceCount();
                int n = false ? 1 : 0;
                final boolean b = preferenceCount != 0;
                if (DataUsageList.this.getPreferenceScreen().getPreferenceCount() != 0) {
                    n = (true ? 1 : 0);
                }
                if ((b ? 1 : 0) != n) {
                    if (DataUsageList.this.mApps.getPreferenceCount() != 0) {
                        DataUsageList.this.getPreferenceScreen().addPreference(DataUsageList.this.mUsageAmount);
                        DataUsageList.this.getPreferenceScreen().addPreference(DataUsageList.this.mApps);
                    }
                    else {
                        DataUsageList.this.getPreferenceScreen().removeAll();
                    }
                }
            }
            
            public Loader<NetworkStats> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<NetworkStats>)new SummaryForAllUidLoader((Context)DataUsageList.this.getActivity(), DataUsageList.this.mStatsSession, bundle);
            }
            
            public void onLoadFinished(final Loader<NetworkStats> loader, final NetworkStats networkStats) {
                DataUsageList.this.bindStats(networkStats, DataUsageList.this.services.mPolicyManager.getUidsWithPolicy(1));
                this.updateEmptyVisible();
            }
            
            public void onLoaderReset(final Loader<NetworkStats> loader) {
                DataUsageList.this.bindStats(null, new int[0]);
                this.updateEmptyVisible();
            }
        };
    }
    
    private static long accumulate(final int n, final SparseArray<AppItem> sparseArray, final NetworkStats$Entry networkStats$Entry, final int category, final ArrayList<AppItem> list, final long n2) {
        final int uid = networkStats$Entry.uid;
        AppItem appItem;
        if ((appItem = (AppItem)sparseArray.get(n)) == null) {
            appItem = new AppItem(n);
            appItem.category = category;
            list.add(appItem);
            sparseArray.put(appItem.key, (Object)appItem);
        }
        appItem.addUid(uid);
        appItem.total += networkStats$Entry.rxBytes + networkStats$Entry.txBytes;
        return Math.max(n2, appItem.total);
    }
    
    public static boolean hasReadyMobileRadio(final Context context) {
        final ConnectivityManager from = ConnectivityManager.from(context);
        final TelephonyManager from2 = TelephonyManager.from(context);
        final List activeSubscriptionInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        final boolean b = false;
        if (activeSubscriptionInfoList == null) {
            return false;
        }
        boolean b2 = true;
        final Iterator<SubscriptionInfo> iterator = activeSubscriptionInfoList.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            boolean b3 = true;
            if (!hasNext) {
                break;
            }
            if (from2.getSimState(iterator.next().getSimSlotIndex()) != 5) {
                b3 = false;
            }
            b2 &= b3;
        }
        boolean b4 = b;
        if (from.isNetworkSupported(0)) {
            b4 = b;
            if (b2) {
                b4 = true;
            }
        }
        return b4;
    }
    
    private void startAppDataUsage(final AppItem appItem) {
        final Bundle arguments = new Bundle();
        arguments.putParcelable("app_item", (Parcelable)appItem);
        arguments.putParcelable("network_template", (Parcelable)this.mTemplate);
        new SubSettingLauncher(this.getContext()).setDestination(AppDataUsage.class.getName()).setTitle(2131886344).setArguments(arguments).setSourceMetricsCategory(this.getMetricsCategory()).launch();
    }
    
    private void updateBody() {
        if (!this.isAdded()) {
            return;
        }
        final Activity activity = this.getActivity();
        this.getLoaderManager().restartLoader(2, ChartDataLoader.buildArgs(this.mTemplate, null), (LoaderManager$LoaderCallbacks)this.mChartDataCallbacks);
        this.getActivity().invalidateOptionsMenu();
        int n2;
        final int n = n2 = ((Context)activity).getColor(2131099844);
        if (this.mSubId != -1) {
            final SubscriptionInfo activeSubscriptionInfo = this.services.mSubscriptionManager.getActiveSubscriptionInfo(this.mSubId);
            n2 = n;
            if (activeSubscriptionInfo != null) {
                n2 = activeSubscriptionInfo.getIconTint();
            }
        }
        this.mChart.setColors(n2, Color.argb(127, Color.red(n2), Color.green(n2), Color.blue(n2)));
    }
    
    private void updateDetailData() {
        final long inspectStart = this.mChart.getInspectStart();
        final long inspectEnd = this.mChart.getInspectEnd();
        final long currentTimeMillis = System.currentTimeMillis();
        final Activity activity = this.getActivity();
        NetworkStatsHistory$Entry values = null;
        if (this.mChartData != null) {
            values = this.mChartData.network.getValues(inspectStart, inspectEnd, currentTimeMillis, (NetworkStatsHistory$Entry)null);
        }
        this.getLoaderManager().restartLoader(3, SummaryForAllUidLoader.buildArgs(this.mTemplate, inspectStart, inspectEnd), (LoaderManager$LoaderCallbacks)this.mSummaryCallbacks);
        long n;
        if (values != null) {
            n = values.rxBytes + values.txBytes;
        }
        else {
            n = 0L;
        }
        this.mUsageAmount.setTitle(this.getString(2131887304, new Object[] { DataUsageUtils.formatDataUsage((Context)activity, n) }));
    }
    
    private void updatePolicy() {
        final NetworkPolicy policy = this.services.mPolicyEditor.getPolicy(this.mTemplate);
        final View viewById = this.mHeader.findViewById(2131362144);
        if (this.isNetworkPolicyModifiable(policy, this.mSubId) && this.isMobileDataAvailable(this.mSubId)) {
            this.mChart.setNetworkPolicy(policy);
            viewById.setVisibility(0);
        }
        else {
            this.mChart.setNetworkPolicy(null);
            viewById.setVisibility(8);
        }
        if (this.mCycleAdapter.updateCycleList(policy, this.mChartData)) {
            this.updateDetailData();
        }
    }
    
    public void bindStats(final NetworkStats networkStats, final int[] array) {
        final ArrayList<Comparable> list = new ArrayList<Comparable>();
        final int currentUser = ActivityManager.getCurrentUser();
        final UserManager value = UserManager.get(this.getContext());
        final List userProfiles = value.getUserProfiles();
        final SparseArray sparseArray = new SparseArray();
        NetworkStats$Entry values = null;
        int size;
        if (networkStats != null) {
            size = networkStats.size();
        }
        else {
            size = 0;
        }
        long n = 0L;
        for (int i = 0; i < size; ++i) {
            values = networkStats.getValues(i, values);
            final int uid = values.uid;
            final int userId = UserHandle.getUserId(uid);
            int n2;
            int n3;
            if (UserHandle.isApp(uid)) {
                if (userProfiles.contains(new UserHandle(userId))) {
                    if (userId != currentUser) {
                        n = accumulate(UidDetailProvider.buildKeyForUser(userId), (SparseArray<AppItem>)sparseArray, values, 0, (ArrayList<AppItem>)list, n);
                    }
                    n2 = uid;
                    n3 = 2;
                }
                else {
                    int buildKeyForUser;
                    int n4;
                    if (value.getUserInfo(userId) == null) {
                        buildKeyForUser = -4;
                        n4 = 2;
                    }
                    else {
                        buildKeyForUser = UidDetailProvider.buildKeyForUser(userId);
                        n4 = 0;
                    }
                    final int n5 = buildKeyForUser;
                    n3 = n4;
                    n2 = n5;
                }
            }
            else {
                if (uid != -4 && uid != -5) {
                    n2 = 1000;
                }
                else {
                    n2 = uid;
                }
                n3 = 2;
            }
            n = accumulate(n2, (SparseArray<AppItem>)sparseArray, values, n3, (ArrayList<AppItem>)list, n);
        }
        for (final int n6 : array) {
            if (userProfiles.contains(new UserHandle(UserHandle.getUserId(n6)))) {
                AppItem appItem;
                if ((appItem = (AppItem)sparseArray.get(n6)) == null) {
                    appItem = new AppItem(n6);
                    appItem.total = -1L;
                    list.add(appItem);
                    sparseArray.put(appItem.key, (Object)appItem);
                }
                appItem.restricted = true;
            }
        }
        Collections.sort(list);
        this.mApps.removeAll();
        for (int k = 0; k < list.size(); ++k) {
            int n7;
            if (n != 0L) {
                n7 = (int)(list.get(k).total * 100L / n);
            }
            else {
                n7 = 0;
            }
            final AppDataUsagePreference appDataUsagePreference = new AppDataUsagePreference(this.getContext(), list.get(k), n7, this.mUidDetailProvider);
            appDataUsagePreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(final Preference preference) {
                    DataUsageList.this.startAppDataUsage(((AppDataUsagePreference)preference).getItem());
                    return true;
                }
            });
            this.mApps.addPreference(appDataUsagePreference);
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 341;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        if (!this.isBandwidthControlEnabled()) {
            Log.w("DataUsage", "No bandwidth control; leaving");
            this.getActivity().finish();
        }
        try {
            this.mStatsSession = this.services.mStatsService.openSession();
            this.mUidDetailProvider = new UidDetailProvider((Context)activity);
            this.addPreferencesFromResource(2132082743);
            this.mUsageAmount = this.findPreference("usage_amount");
            this.mChart = (ChartDataUsagePreference)this.findPreference("chart_data");
            this.mApps = (PreferenceGroup)this.findPreference("apps_group");
            this.processArgument();
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    @Override
    public void onDestroy() {
        this.mUidDetailProvider.clearCache();
        this.mUidDetailProvider = null;
        TrafficStats.closeQuietly(this.mStatsSession);
        super.onDestroy();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mDataStateListener.setListener(false, this.mSubId, this.getContext());
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mDataStateListener.setListener(true, this.mSubId, this.getContext());
        this.updateBody();
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(final Void... array) {
                try {
                    Thread.sleep(2000L);
                    DataUsageList.this.services.mStatsService.forceUpdate();
                }
                catch (RemoteException ex) {}
                catch (InterruptedException ex2) {}
                return null;
            }
            
            protected void onPostExecute(final Void void1) {
                if (DataUsageList.this.isAdded()) {
                    DataUsageList.this.updateBody();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])new Void[0]);
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.mHeader = this.setPinnedHeaderView(2131558453);
        this.mHeader.findViewById(2131362144).setOnClickListener((View.OnClickListener)new _$$Lambda$DataUsageList$YolaBauY8HvHsYGl5vfnCCKHiAQ(this));
        this.mCycleSpinner = (Spinner)this.mHeader.findViewById(2131362145);
        this.mCycleAdapter = new CycleAdapter(this.mCycleSpinner.getContext(), (CycleAdapter.SpinnerInterface)new CycleAdapter.SpinnerInterface() {
            @Override
            public Object getSelectedItem() {
                return DataUsageList.this.mCycleSpinner.getSelectedItem();
            }
            
            @Override
            public void setAdapter(final CycleAdapter adapter) {
                DataUsageList.this.mCycleSpinner.setAdapter((SpinnerAdapter)adapter);
            }
            
            @Override
            public void setOnItemSelectedListener(final AdapterView$OnItemSelectedListener onItemSelectedListener) {
                DataUsageList.this.mCycleSpinner.setOnItemSelectedListener(onItemSelectedListener);
            }
            
            @Override
            public void setSelection(final int selection) {
                DataUsageList.this.mCycleSpinner.setSelection(selection);
            }
        }, this.mCycleListener, true);
        (this.mLoadingViewController = new LoadingViewController(this.getView().findViewById(2131362342), (View)this.getListView())).showLoadingViewDelayed();
    }
    
    void processArgument() {
        final Bundle arguments = this.getArguments();
        if (arguments != null) {
            this.mSubId = arguments.getInt("sub_id", -1);
            this.mTemplate = (NetworkTemplate)arguments.getParcelable("network_template");
        }
        if (this.mTemplate == null && this.mSubId == -1) {
            final Intent intent = this.getIntent();
            this.mSubId = intent.getIntExtra("android.provider.extra.SUB_ID", -1);
            this.mTemplate = (NetworkTemplate)intent.getParcelableExtra("network_template");
        }
    }
}
