package com.android.settings.location;

import android.provider.Settings;
import android.util.Log;
import android.content.Intent;
import android.os.UserHandle;
import com.android.settings.Utils;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.os.UserManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class LocationEnabler implements LifecycleObserver, OnPause, OnResume
{
    static final IntentFilter INTENT_FILTER_LOCATION_MODE_CHANGED;
    private final Context mContext;
    private final LocationModeChangeListener mListener;
    BroadcastReceiver mReceiver;
    private final UserManager mUserManager;
    
    static {
        INTENT_FILTER_LOCATION_MODE_CHANGED = new IntentFilter("android.location.MODE_CHANGED");
    }
    
    public LocationEnabler(final Context mContext, final LocationModeChangeListener mListener, final Lifecycle lifecycle) {
        this.mContext = mContext;
        this.mListener = mListener;
        this.mUserManager = (UserManager)mContext.getSystemService("user");
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private boolean isRestricted() {
        return this.mUserManager.hasUserRestriction("no_share_location");
    }
    
    RestrictedLockUtils.EnforcedAdmin getShareLocationEnforcedAdmin(final int n) {
        RestrictedLockUtils.EnforcedAdmin enforcedAdmin;
        if ((enforcedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_share_location", n)) == null) {
            enforcedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_config_location", n);
        }
        return enforcedAdmin;
    }
    
    boolean hasShareLocationRestriction(final int n) {
        return RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_share_location", n);
    }
    
    boolean isEnabled(final int n) {
        return n != 0 && !this.isRestricted();
    }
    
    boolean isManagedProfileRestrictedByBase() {
        final UserHandle managedProfile = Utils.getManagedProfile(this.mUserManager);
        return managedProfile != null && this.hasShareLocationRestriction(managedProfile.getIdentifier());
    }
    
    @Override
    public void onPause() {
        try {
            this.mContext.unregisterReceiver(this.mReceiver);
        }
        catch (RuntimeException ex) {}
    }
    
    @Override
    public void onResume() {
        if (this.mReceiver == null) {
            this.mReceiver = new BroadcastReceiver() {
                public void onReceive(final Context context, final Intent intent) {
                    if (Log.isLoggable("LocationEnabler", 3)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Received location mode change intent: ");
                        sb.append(intent);
                        Log.d("LocationEnabler", sb.toString());
                    }
                    LocationEnabler.this.refreshLocationMode();
                }
            };
        }
        this.mContext.registerReceiver(this.mReceiver, LocationEnabler.INTENT_FILTER_LOCATION_MODE_CHANGED);
        this.refreshLocationMode();
    }
    
    void refreshLocationMode() {
        final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), "location_mode", 0);
        if (Log.isLoggable("LocationEnabler", 4)) {
            Log.i("LocationEnabler", "Location mode has been changed");
        }
        if (this.mListener != null) {
            this.mListener.onLocationModeChanged(int1, this.isRestricted());
        }
    }
    
    void setLocationEnabled(final boolean b) {
        final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), "location_mode", 0);
        if (this.isRestricted()) {
            if (Log.isLoggable("LocationEnabler", 4)) {
                Log.i("LocationEnabler", "Restricted user, not setting location mode");
            }
            if (this.mListener != null) {
                this.mListener.onLocationModeChanged(int1, true);
            }
            return;
        }
        com.android.settingslib.Utils.updateLocationEnabled(this.mContext, b, UserHandle.myUserId(), 1);
        this.refreshLocationMode();
    }
    
    public interface LocationModeChangeListener
    {
        void onLocationModeChanged(final int p0, final boolean p1);
    }
}
