package com.android.settings.location;

import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.Utils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.RestrictedSwitchPreference;

public class LocationForWorkPreferenceController extends LocationBasePreferenceController
{
    private RestrictedSwitchPreference mPreference;
    
    public LocationForWorkPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, lifecycle);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (RestrictedSwitchPreference)preferenceScreen.findPreference("managed_profile_location_switch");
    }
    
    @Override
    public String getPreferenceKey() {
        return "managed_profile_location_switch";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("managed_profile_location_switch".equals(preference.getKey())) {
            final boolean checked = this.mPreference.isChecked();
            this.mUserManager.setUserRestriction("no_share_location", checked ^ true, Utils.getManagedProfile(this.mUserManager));
            final RestrictedSwitchPreference mPreference = this.mPreference;
            int summary;
            if (checked) {
                summary = 2131889422;
            }
            else {
                summary = 2131889421;
            }
            mPreference.setSummary(summary);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return Utils.getManagedProfile(this.mUserManager) != null;
    }
    
    @Override
    public void onLocationModeChanged(int summary, final boolean b) {
        if (this.mPreference.isVisible() && this.isAvailable()) {
            final RestrictedLockUtils.EnforcedAdmin shareLocationEnforcedAdmin = this.mLocationEnabler.getShareLocationEnforcedAdmin(Utils.getManagedProfile(this.mUserManager).getIdentifier());
            final boolean managedProfileRestrictedByBase = this.mLocationEnabler.isManagedProfileRestrictedByBase();
            if (!managedProfileRestrictedByBase && shareLocationEnforcedAdmin != null) {
                this.mPreference.setDisabledByAdmin(shareLocationEnforcedAdmin);
                this.mPreference.setChecked(false);
            }
            else {
                final boolean enabled = this.mLocationEnabler.isEnabled(summary);
                this.mPreference.setEnabled(enabled);
                summary = 2131889421;
                if (!enabled) {
                    this.mPreference.setChecked(false);
                }
                else {
                    this.mPreference.setChecked(managedProfileRestrictedByBase ^ true);
                    if (managedProfileRestrictedByBase) {
                        summary = 2131889421;
                    }
                    else {
                        summary = 2131889422;
                    }
                }
                this.mPreference.setSummary(summary);
            }
        }
    }
}
