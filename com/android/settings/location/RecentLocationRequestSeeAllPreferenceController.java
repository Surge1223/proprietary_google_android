package com.android.settings.location;

import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.widget.AppPreference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.location.RecentLocationApps;
import android.support.v7.preference.PreferenceCategory;

public class RecentLocationRequestSeeAllPreferenceController extends LocationBasePreferenceController
{
    private PreferenceCategory mCategoryAllRecentLocationRequests;
    private final RecentLocationRequestSeeAllFragment mFragment;
    private RecentLocationApps mRecentLocationApps;
    
    public RecentLocationRequestSeeAllPreferenceController(final Context context, final Lifecycle lifecycle, final RecentLocationRequestSeeAllFragment recentLocationRequestSeeAllFragment) {
        this(context, lifecycle, recentLocationRequestSeeAllFragment, new RecentLocationApps(context));
    }
    
    RecentLocationRequestSeeAllPreferenceController(final Context context, final Lifecycle lifecycle, final RecentLocationRequestSeeAllFragment mFragment, final RecentLocationApps mRecentLocationApps) {
        super(context, lifecycle);
        this.mFragment = mFragment;
        this.mRecentLocationApps = mRecentLocationApps;
    }
    
    AppPreference createAppPreference(final Context context, final RecentLocationApps.Request request) {
        final AppPreference appPreference = new AppPreference(context);
        appPreference.setSummary(request.contentDescription);
        appPreference.setIcon(request.icon);
        appPreference.setTitle(request.label);
        appPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new RecentLocationRequestPreferenceController.PackageEntryClickedListener(this.mFragment, request.packageName, request.userHandle));
        return appPreference;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mCategoryAllRecentLocationRequests = (PreferenceCategory)preferenceScreen.findPreference("all_recent_location_requests");
    }
    
    @Override
    public String getPreferenceKey() {
        return "all_recent_location_requests";
    }
    
    @Override
    public void onLocationModeChanged(final int n, final boolean b) {
        this.mCategoryAllRecentLocationRequests.setEnabled(this.mLocationEnabler.isEnabled(n));
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.mCategoryAllRecentLocationRequests.removeAll();
        final Iterator<RecentLocationApps.Request> iterator = this.mRecentLocationApps.getAppListSorted().iterator();
        while (iterator.hasNext()) {
            this.mCategoryAllRecentLocationRequests.addPreference(this.createAppPreference(preference.getContext(), iterator.next()));
        }
    }
}
