package com.android.settings.location;

import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class RecentLocationRequestSeeAllFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> getPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082784;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle, final RecentLocationRequestSeeAllFragment recentLocationRequestSeeAllFragment) {
        final ArrayList<RecentLocationRequestSeeAllPreferenceController> list = (ArrayList<RecentLocationRequestSeeAllPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new RecentLocationRequestSeeAllPreferenceController(context, lifecycle, recentLocationRequestSeeAllFragment));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle(), this);
    }
    
    @Override
    protected String getLogTag() {
        return "RecentLocationReqAll";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1325;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082784;
    }
}
