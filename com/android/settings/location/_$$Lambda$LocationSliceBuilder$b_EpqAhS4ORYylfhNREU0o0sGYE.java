package com.android.settings.location;

import com.android.settingslib.Utils;
import androidx.slice.Slice;
import android.app.PendingIntent;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import android.content.Intent;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;
import androidx.slice.builders.ListBuilder;
import androidx.slice.builders.SliceAction;
import android.support.v4.graphics.drawable.IconCompat;
import android.support.v4.util.Consumer;

public final class _$$Lambda$LocationSliceBuilder$b_EpqAhS4ORYylfhNREU0o0sGYE implements Consumer
{
    @Override
    public final void accept(final Object o) {
        LocationSliceBuilder.lambda$getSlice$0(this.f$0, this.f$1, this.f$2, (ListBuilder.RowBuilder)o);
    }
}
