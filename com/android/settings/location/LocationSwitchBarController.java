package com.android.settings.location;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.widget.Switch;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.widget.SwitchBar;

public class LocationSwitchBarController implements LocationModeChangeListener, OnSwitchChangeListener, LifecycleObserver, OnStart, OnStop
{
    private final LocationEnabler mLocationEnabler;
    private final Switch mSwitch;
    private final SwitchBar mSwitchBar;
    private boolean mValidListener;
    
    public LocationSwitchBarController(final Context context, final SwitchBar mSwitchBar, final Lifecycle lifecycle) {
        this.mSwitchBar = mSwitchBar;
        this.mSwitch = this.mSwitchBar.getSwitch();
        this.mLocationEnabler = new LocationEnabler(context, (LocationEnabler.LocationModeChangeListener)this, lifecycle);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void onLocationModeChanged(int myUserId, final boolean b) {
        final boolean enabled = this.mLocationEnabler.isEnabled(myUserId);
        myUserId = UserHandle.myUserId();
        final RestrictedLockUtils.EnforcedAdmin shareLocationEnforcedAdmin = this.mLocationEnabler.getShareLocationEnforcedAdmin(myUserId);
        if (!this.mLocationEnabler.hasShareLocationRestriction(myUserId) && shareLocationEnforcedAdmin != null) {
            this.mSwitchBar.setDisabledByAdmin(shareLocationEnforcedAdmin);
        }
        else {
            this.mSwitchBar.setEnabled(b ^ true);
        }
        if (enabled != this.mSwitch.isChecked()) {
            if (this.mValidListener) {
                this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            }
            this.mSwitch.setChecked(enabled);
            if (this.mValidListener) {
                this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            }
        }
    }
    
    @Override
    public void onStart() {
        if (!this.mValidListener) {
            this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            this.mValidListener = true;
        }
    }
    
    @Override
    public void onStop() {
        if (this.mValidListener) {
            this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            this.mValidListener = false;
        }
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean locationEnabled) {
        this.mLocationEnabler.setLocationEnabled(locationEnabled);
    }
}
