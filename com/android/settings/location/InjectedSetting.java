package com.android.settings.location;

import android.util.Log;
import android.text.TextUtils;
import android.content.Intent;
import java.util.Objects;
import android.os.UserHandle;
import com.android.internal.annotations.Immutable;

@Immutable
class InjectedSetting
{
    public final String className;
    public final int iconId;
    public final UserHandle mUserHandle;
    public final String packageName;
    public final String settingsActivity;
    public final String title;
    public final String userRestriction;
    
    private InjectedSetting(final Builder builder) {
        this.packageName = builder.mPackageName;
        this.className = builder.mClassName;
        this.title = builder.mTitle;
        this.iconId = builder.mIconId;
        this.mUserHandle = builder.mUserHandle;
        this.settingsActivity = builder.mSettingsActivity;
        this.userRestriction = builder.mUserRestriction;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof InjectedSetting)) {
            return false;
        }
        final InjectedSetting injectedSetting = (InjectedSetting)o;
        if (!Objects.equals(this.packageName, injectedSetting.packageName) || !Objects.equals(this.className, injectedSetting.className) || !Objects.equals(this.title, injectedSetting.title) || !Objects.equals(this.iconId, injectedSetting.iconId) || !Objects.equals(this.mUserHandle, injectedSetting.mUserHandle) || !Objects.equals(this.settingsActivity, injectedSetting.settingsActivity) || !Objects.equals(this.userRestriction, injectedSetting.userRestriction)) {
            b = false;
        }
        return b;
    }
    
    public Intent getServiceIntent() {
        final Intent intent = new Intent();
        intent.setClassName(this.packageName, this.className);
        return intent;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.packageName.hashCode();
        final int hashCode2 = this.className.hashCode();
        final int hashCode3 = this.title.hashCode();
        final int iconId = this.iconId;
        final UserHandle mUserHandle = this.mUserHandle;
        int hashCode4 = 0;
        int hashCode5;
        if (mUserHandle == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = this.mUserHandle.hashCode();
        }
        final int hashCode6 = this.settingsActivity.hashCode();
        if (this.userRestriction != null) {
            hashCode4 = this.userRestriction.hashCode();
        }
        return 31 * (31 * (31 * (31 * (31 * (31 * hashCode + hashCode2) + hashCode3) + iconId) + hashCode5) + hashCode6) + hashCode4;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InjectedSetting{mPackageName='");
        sb.append(this.packageName);
        sb.append('\'');
        sb.append(", mClassName='");
        sb.append(this.className);
        sb.append('\'');
        sb.append(", label=");
        sb.append(this.title);
        sb.append(", iconId=");
        sb.append(this.iconId);
        sb.append(", userId=");
        sb.append(this.mUserHandle.getIdentifier());
        sb.append(", settingsActivity='");
        sb.append(this.settingsActivity);
        sb.append('\'');
        sb.append(", userRestriction='");
        sb.append(this.userRestriction);
        sb.append('}');
        return sb.toString();
    }
    
    public static class Builder
    {
        private String mClassName;
        private int mIconId;
        private String mPackageName;
        private String mSettingsActivity;
        private String mTitle;
        private UserHandle mUserHandle;
        private String mUserRestriction;
        
        public InjectedSetting build() {
            if (this.mPackageName != null && this.mClassName != null && !TextUtils.isEmpty((CharSequence)this.mTitle) && !TextUtils.isEmpty((CharSequence)this.mSettingsActivity)) {
                return new InjectedSetting(this, null);
            }
            if (Log.isLoggable("SettingsInjector", 5)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Illegal setting specification: package=");
                sb.append(this.mPackageName);
                sb.append(", class=");
                sb.append(this.mClassName);
                sb.append(", title=");
                sb.append(this.mTitle);
                sb.append(", settingsActivity=");
                sb.append(this.mSettingsActivity);
                Log.w("SettingsInjector", sb.toString());
            }
            return null;
        }
        
        public Builder setClassName(final String mClassName) {
            this.mClassName = mClassName;
            return this;
        }
        
        public Builder setIconId(final int mIconId) {
            this.mIconId = mIconId;
            return this;
        }
        
        public Builder setPackageName(final String mPackageName) {
            this.mPackageName = mPackageName;
            return this;
        }
        
        public Builder setSettingsActivity(final String mSettingsActivity) {
            this.mSettingsActivity = mSettingsActivity;
            return this;
        }
        
        public Builder setTitle(final String mTitle) {
            this.mTitle = mTitle;
            return this;
        }
        
        public Builder setUserHandle(final UserHandle mUserHandle) {
            this.mUserHandle = mUserHandle;
            return this;
        }
        
        public Builder setUserRestriction(final String mUserRestriction) {
            this.mUserRestriction = mUserRestriction;
            return this;
        }
    }
}
