package com.android.settings.location;

import android.support.v4.util.Consumer;
import androidx.slice.builders.SliceAction;
import com.android.settingslib.Utils;
import androidx.slice.builders.ListBuilder;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.Slice;
import android.app.PendingIntent;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import android.content.Intent;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;

public class LocationSliceBuilder
{
    public static final Uri LOCATION_URI;
    
    static {
        LOCATION_URI = new Uri.Builder().scheme("content").authority("android.settings.slices").appendPath("action").appendPath("location").build();
    }
    
    public static Intent getIntent(final Context context) {
        return DatabaseIndexingUtils.buildSearchResultPageIntent(context, LocationSettings.class.getName(), "location", context.getText(2131888059).toString(), 63).setClassName(context.getPackageName(), SubSettings.class.getName()).setData(new Uri.Builder().appendPath("location").build());
    }
    
    private static PendingIntent getPrimaryAction(final Context context) {
        return PendingIntent.getActivity(context, 0, getIntent(context), 0);
    }
    
    public static Slice getSlice(final Context context) {
        final IconCompat withResource = IconCompat.createWithResource(context, 2131231151);
        final String string = context.getString(2131888059);
        return new ListBuilder(context, LocationSliceBuilder.LOCATION_URI, -1L).setAccentColor(Utils.getColorAccent(context)).addRow(new _$$Lambda$LocationSliceBuilder$b_EpqAhS4ORYylfhNREU0o0sGYE(string, withResource, new SliceAction(getPrimaryAction(context), withResource, string))).build();
    }
}
