package com.android.settings.location;

import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class LocationBasePreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LocationModeChangeListener
{
    protected final LocationEnabler mLocationEnabler;
    protected final UserManager mUserManager;
    
    public LocationBasePreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mLocationEnabler = new LocationEnabler(context, (LocationEnabler.LocationModeChangeListener)this, lifecycle);
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
}
