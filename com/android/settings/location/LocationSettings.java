package com.android.settings.location;

import com.android.settings.widget.SwitchBar;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.Comparator;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.Preference;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.search.BaseSearchIndexProvider;
import android.content.Context;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class LocationSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private LocationSwitchBarController mSwitchBarController;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new LocationSettings.SummaryProvider((Context)activity, summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082786;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    static void addPreferencesSorted(final List<Preference> list, final PreferenceGroup preferenceGroup) {
        Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<Preference>() {
            @Override
            public int compare(final Preference preference, final Preference preference2) {
                return preference.getTitle().toString().compareTo(preference2.getTitle().toString());
            }
        });
        final Iterator<Preference> iterator = list.iterator();
        while (iterator.hasNext()) {
            preferenceGroup.addPreference(iterator.next());
        }
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final LocationSettings locationSettings, final Lifecycle lifecycle) {
        final ArrayList<LocationForWorkPreferenceController> list = (ArrayList<LocationForWorkPreferenceController>)new ArrayList<LocationFooterPreferenceController>();
        list.add((LocationFooterPreferenceController)new AppLocationPermissionPreferenceController(context));
        list.add((LocationFooterPreferenceController)new LocationForWorkPreferenceController(context, lifecycle));
        list.add((LocationFooterPreferenceController)new RecentLocationRequestPreferenceController(context, locationSettings, lifecycle));
        list.add((LocationFooterPreferenceController)new LocationScanningPreferenceController(context));
        list.add((LocationFooterPreferenceController)new LocationServicePreferenceController(context, locationSettings, lifecycle));
        list.add(new LocationFooterPreferenceController(context, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887815;
    }
    
    @Override
    protected String getLogTag() {
        return "LocationSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 63;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082786;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        final SwitchBar switchBar = settingsActivity.getSwitchBar();
        switchBar.setSwitchBarText(2131888058, 2131888058);
        this.mSwitchBarController = new LocationSwitchBarController((Context)settingsActivity, switchBar, this.getLifecycle());
        switchBar.show();
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, LocationPreferenceController.getLocationSummary(this.mContext));
            }
        }
    }
}
