package com.android.settings.location;

import java.util.Iterator;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.widget.RestrictedAppPreference;
import android.util.Log;
import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import android.os.UserHandle;
import android.support.v7.preference.Preference;
import java.util.List;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.support.v7.preference.PreferenceCategory;
import android.content.IntentFilter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class LocationServicePreferenceController extends LocationBasePreferenceController implements LifecycleObserver, OnPause, OnResume
{
    static final IntentFilter INTENT_FILTER_INJECTED_SETTING_CHANGED;
    private PreferenceCategory mCategoryLocationServices;
    private final LocationSettings mFragment;
    BroadcastReceiver mInjectedSettingsReceiver;
    private final SettingsInjector mInjector;
    
    static {
        INTENT_FILTER_INJECTED_SETTING_CHANGED = new IntentFilter("android.location.InjectedSettingChanged");
    }
    
    public LocationServicePreferenceController(final Context context, final LocationSettings locationSettings, final Lifecycle lifecycle) {
        this(context, locationSettings, lifecycle, new SettingsInjector(context));
    }
    
    LocationServicePreferenceController(final Context context, final LocationSettings mFragment, final Lifecycle lifecycle, final SettingsInjector mInjector) {
        super(context, lifecycle);
        this.mFragment = mFragment;
        this.mInjector = mInjector;
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private List<Preference> getLocationServices() {
        final SettingsInjector mInjector = this.mInjector;
        final Context context = this.mFragment.getPreferenceManager().getContext();
        int myUserId;
        if (this.mLocationEnabler.isManagedProfileRestrictedByBase()) {
            myUserId = UserHandle.myUserId();
        }
        else {
            myUserId = -2;
        }
        return mInjector.getInjectedSettings(context, myUserId);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mCategoryLocationServices = (PreferenceCategory)preferenceScreen.findPreference("location_services");
    }
    
    @Override
    public String getPreferenceKey() {
        return "location_services";
    }
    
    @Override
    public boolean isAvailable() {
        final SettingsInjector mInjector = this.mInjector;
        int myUserId;
        if (this.mLocationEnabler.isManagedProfileRestrictedByBase()) {
            myUserId = UserHandle.myUserId();
        }
        else {
            myUserId = -2;
        }
        return mInjector.hasInjectedSettings(myUserId);
    }
    
    @Override
    public void onLocationModeChanged(final int n, final boolean b) {
        this.mInjector.reloadStatusMessages();
    }
    
    @Override
    public void onPause() {
        this.mContext.unregisterReceiver(this.mInjectedSettingsReceiver);
    }
    
    @Override
    public void onResume() {
        if (this.mInjectedSettingsReceiver == null) {
            this.mInjectedSettingsReceiver = new BroadcastReceiver() {
                public void onReceive(final Context context, final Intent intent) {
                    if (Log.isLoggable("LocationServicePrefCtrl", 3)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Received settings change intent: ");
                        sb.append(intent);
                        Log.d("LocationServicePrefCtrl", sb.toString());
                    }
                    LocationServicePreferenceController.this.mInjector.reloadStatusMessages();
                }
            };
        }
        this.mContext.registerReceiver(this.mInjectedSettingsReceiver, LocationServicePreferenceController.INTENT_FILTER_INJECTED_SETTING_CHANGED);
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.mCategoryLocationServices.removeAll();
        final List<Preference> locationServices = this.getLocationServices();
        for (final Preference preference2 : locationServices) {
            if (preference2 instanceof RestrictedAppPreference) {
                ((RestrictedAppPreference)preference2).checkRestrictionAndSetDisabled();
            }
        }
        LocationSettings.addPreferencesSorted(locationServices, this.mCategoryLocationServices);
    }
}
