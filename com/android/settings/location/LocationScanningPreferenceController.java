package com.android.settings.location;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class LocationScanningPreferenceController extends BasePreferenceController
{
    static final String KEY_LOCATION_SCANNING = "location_scanning";
    
    public LocationScanningPreferenceController(final Context context) {
        super(context, "location_scanning");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034146)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
