package com.android.settings.location;

import android.content.pm.PackageManager;
import com.android.settingslib.widget.FooterPreference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.content.pm.ApplicationInfo;
import android.content.pm.ActivityInfo;
import java.util.Iterator;
import java.util.List;
import android.content.pm.ResolveInfo;
import java.util.Collections;
import android.util.Log;
import java.util.ArrayList;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import java.util.Collection;
import android.content.Context;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class LocationFooterPreferenceController extends LocationBasePreferenceController implements LifecycleObserver, OnPause
{
    private static final Intent INJECT_INTENT;
    private final Context mContext;
    private Collection<ComponentName> mFooterInjectors;
    private final PackageManager mPackageManager;
    
    static {
        INJECT_INTENT = new Intent("com.android.settings.location.DISPLAYED_FOOTER");
    }
    
    public LocationFooterPreferenceController(final Context mContext, final Lifecycle lifecycle) {
        super(mContext, lifecycle);
        this.mContext = mContext;
        this.mPackageManager = this.mContext.getPackageManager();
        this.mFooterInjectors = new ArrayList<ComponentName>();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private Collection<FooterData> getFooterData() {
        final List queryBroadcastReceivers = this.mPackageManager.queryBroadcastReceivers(LocationFooterPreferenceController.INJECT_INTENT, 128);
        if (queryBroadcastReceivers == null) {
            if (Log.isLoggable("LocationFooter", 6)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to resolve intent ");
                sb.append(LocationFooterPreferenceController.INJECT_INTENT);
                Log.e("LocationFooter", sb.toString());
                return (Collection<FooterData>)Collections.emptyList();
            }
        }
        else if (Log.isLoggable("LocationFooter", 3)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Found broadcast receivers: ");
            sb2.append(queryBroadcastReceivers);
            Log.d("LocationFooter", sb2.toString());
        }
        final ArrayList list = new ArrayList<FooterData>(queryBroadcastReceivers.size());
        for (final ResolveInfo resolveInfo : queryBroadcastReceivers) {
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            final ApplicationInfo applicationInfo = activityInfo.applicationInfo;
            if ((applicationInfo.flags & 0x1) == 0x0 && Log.isLoggable("LocationFooter", 5)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Ignoring attempt to inject footer from app not in system image: ");
                sb3.append(resolveInfo);
                Log.w("LocationFooter", sb3.toString());
            }
            else if (activityInfo.metaData == null && Log.isLoggable("LocationFooter", 3)) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("No METADATA in broadcast receiver ");
                sb4.append(activityInfo.name);
                Log.d("LocationFooter", sb4.toString());
            }
            else {
                final int int1 = activityInfo.metaData.getInt("com.android.settings.location.FOOTER_STRING");
                if (int1 == 0) {
                    if (!Log.isLoggable("LocationFooter", 5)) {
                        continue;
                    }
                    Log.w("LocationFooter", "No mapping of integer exists for com.android.settings.location.FOOTER_STRING");
                }
                else {
                    list.add(new FooterData(int1, applicationInfo, new ComponentName(activityInfo.packageName, activityInfo.name)));
                }
            }
        }
        return (Collection<FooterData>)list;
    }
    
    @Override
    public String getPreferenceKey() {
        return "location_footer";
    }
    
    @Override
    public boolean isAvailable() {
        return this.getFooterData().isEmpty() ^ true;
    }
    
    @Override
    public void onLocationModeChanged(final int n, final boolean b) {
    }
    
    @Override
    public void onPause() {
        for (final ComponentName component : this.mFooterInjectors) {
            final Intent intent = new Intent("com.android.settings.location.REMOVED_FOOTER");
            intent.setComponent(component);
            this.mContext.sendBroadcast(intent);
        }
    }
    
    void sendBroadcastFooterDisplayed(final ComponentName component) {
        final Intent intent = new Intent("com.android.settings.location.DISPLAYED_FOOTER");
        intent.setComponent(component);
        this.mContext.sendBroadcast(intent);
    }
    
    @Override
    public void updateState(final Preference preference) {
        final PreferenceCategory preferenceCategory = (PreferenceCategory)preference;
        preferenceCategory.removeAll();
        this.mFooterInjectors.clear();
        for (final FooterData footerData : this.getFooterData()) {
            final FooterPreference footerPreference = new FooterPreference(preference.getContext());
            try {
                footerPreference.setTitle(this.mPackageManager.getResourcesForApplication(footerData.applicationInfo).getString(footerData.footerStringRes));
                preferenceCategory.addPreference(footerPreference);
                this.sendBroadcastFooterDisplayed(footerData.componentName);
                this.mFooterInjectors.add(footerData.componentName);
            }
            catch (PackageManager$NameNotFoundException ex) {
                if (!Log.isLoggable("LocationFooter", 5)) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Resources not found for application ");
                sb.append(footerData.applicationInfo.packageName);
                Log.w("LocationFooter", sb.toString());
            }
        }
    }
    
    private static class FooterData
    {
        final ApplicationInfo applicationInfo;
        final ComponentName componentName;
        final int footerStringRes;
        
        FooterData(final int footerStringRes, final ApplicationInfo applicationInfo, final ComponentName componentName) {
            this.footerStringRes = footerStringRes;
            this.applicationInfo = applicationInfo;
            this.componentName = componentName;
        }
    }
}
