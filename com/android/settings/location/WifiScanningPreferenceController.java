package com.android.settings.location;

import android.provider.Settings;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiScanningPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public WifiScanningPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_always_scanning";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("wifi_always_scanning".equals(preference.getKey())) {
            Settings.Global.putInt(this.mContext.getContentResolver(), "wifi_scan_always_enabled", (int)(((SwitchPreference)preference).isChecked() ? 1 : 0));
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "wifi_scan_always_enabled", 0);
        boolean checked = true;
        if (int1 != 1) {
            checked = false;
        }
        switchPreference.setChecked(checked);
    }
}
