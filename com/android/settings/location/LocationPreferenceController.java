package com.android.settings.location;

import android.content.IntentFilter;
import com.android.settings.search.InlineListPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v7.preference.Preference;
import android.content.BroadcastReceiver;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class LocationPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private Context mContext;
    BroadcastReceiver mLocationProvidersChangedReceiver;
    private Preference mPreference;
    
    public LocationPreferenceController(final Context mContext, final Lifecycle lifecycle) {
        super(mContext);
        this.mContext = mContext;
        this.mLocationProvidersChangedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.location.PROVIDERS_CHANGED")) {
                    LocationPreferenceController.this.updateSummary();
                }
            }
        };
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    public static String getLocationSummary(final Context context) {
        if (Settings.Secure.getInt(context.getContentResolver(), "location_mode", 0) != 0) {
            return context.getString(2131888051);
        }
        return context.getString(2131888050);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference("location");
    }
    
    @Override
    public String getPreferenceKey() {
        return "location";
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineListPayload("location_mode", 2, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, LocationSettings.class.getName(), "location", this.mContext.getString(2131888059)), this.isAvailable(), 4, 0);
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onPause() {
        if (this.mLocationProvidersChangedReceiver != null) {
            this.mContext.unregisterReceiver(this.mLocationProvidersChangedReceiver);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mLocationProvidersChangedReceiver != null) {
            this.mContext.registerReceiver(this.mLocationProvidersChangedReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(getLocationSummary(this.mContext));
    }
    
    public void updateSummary() {
        this.updateState(this.mPreference);
    }
}
