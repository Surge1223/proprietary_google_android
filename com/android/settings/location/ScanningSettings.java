package com.android.settings.location;

import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class ScanningSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082785;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context) {
        final ArrayList<BluetoothScanningPreferenceController> list = (ArrayList<BluetoothScanningPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new WifiScanningPreferenceController(context));
        list.add(new BluetoothScanningPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context);
    }
    
    @Override
    protected String getLogTag() {
        return "ScanningSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 131;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082785;
    }
}
