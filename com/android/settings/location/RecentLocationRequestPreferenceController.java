package com.android.settings.location;

import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.os.UserHandle;
import java.util.Iterator;
import java.util.List;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.widget.AppPreference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settingslib.location.RecentLocationApps;
import android.support.v7.preference.PreferenceCategory;

public class RecentLocationRequestPreferenceController extends LocationBasePreferenceController
{
    static final String KEY_SEE_ALL_BUTTON = "recent_location_requests_see_all_button";
    private PreferenceCategory mCategoryRecentLocationRequests;
    private final LocationSettings mFragment;
    private final RecentLocationApps mRecentLocationApps;
    private Preference mSeeAllButton;
    
    public RecentLocationRequestPreferenceController(final Context context, final LocationSettings locationSettings, final Lifecycle lifecycle) {
        this(context, locationSettings, lifecycle, new RecentLocationApps(context));
    }
    
    RecentLocationRequestPreferenceController(final Context context, final LocationSettings mFragment, final Lifecycle lifecycle, final RecentLocationApps mRecentLocationApps) {
        super(context, lifecycle);
        this.mFragment = mFragment;
        this.mRecentLocationApps = mRecentLocationApps;
    }
    
    AppPreference createAppPreference(final Context context) {
        return new AppPreference(context);
    }
    
    AppPreference createAppPreference(final Context context, final RecentLocationApps.Request request) {
        final AppPreference appPreference = this.createAppPreference(context);
        appPreference.setSummary(request.contentDescription);
        appPreference.setIcon(request.icon);
        appPreference.setTitle(request.label);
        appPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new PackageEntryClickedListener(this.mFragment, request.packageName, request.userHandle));
        return appPreference;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mCategoryRecentLocationRequests = (PreferenceCategory)preferenceScreen.findPreference("recent_location_requests");
        this.mSeeAllButton = preferenceScreen.findPreference("recent_location_requests_see_all_button");
    }
    
    @Override
    public String getPreferenceKey() {
        return "recent_location_requests";
    }
    
    @Override
    public void onLocationModeChanged(final int n, final boolean b) {
        this.mCategoryRecentLocationRequests.setEnabled(this.mLocationEnabler.isEnabled(n));
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.mCategoryRecentLocationRequests.removeAll();
        final Preference mSeeAllButton = this.mSeeAllButton;
        int i = 0;
        mSeeAllButton.setVisible(false);
        final Context context = preference.getContext();
        final List<RecentLocationApps.Request> appListSorted = this.mRecentLocationApps.getAppListSorted();
        if (appListSorted.size() > 3) {
            while (i < 3) {
                this.mCategoryRecentLocationRequests.addPreference(this.createAppPreference(context, appListSorted.get(i)));
                ++i;
            }
            this.mSeeAllButton.setVisible(true);
        }
        else if (appListSorted.size() > 0) {
            final Iterator<RecentLocationApps.Request> iterator = appListSorted.iterator();
            while (iterator.hasNext()) {
                this.mCategoryRecentLocationRequests.addPreference(this.createAppPreference(context, iterator.next()));
            }
        }
        else {
            final AppPreference appPreference = this.createAppPreference(context);
            appPreference.setTitle(2131888049);
            appPreference.setSelectable(false);
            this.mCategoryRecentLocationRequests.addPreference(appPreference);
        }
    }
    
    static class PackageEntryClickedListener implements OnPreferenceClickListener
    {
        private final DashboardFragment mFragment;
        private final String mPackage;
        private final UserHandle mUserHandle;
        
        public PackageEntryClickedListener(final DashboardFragment mFragment, final String mPackage, final UserHandle mUserHandle) {
            this.mFragment = mFragment;
            this.mPackage = mPackage;
            this.mUserHandle = mUserHandle;
        }
        
        @Override
        public boolean onPreferenceClick(final Preference preference) {
            final Bundle arguments = new Bundle();
            arguments.putString("package", this.mPackage);
            new SubSettingLauncher(this.mFragment.getContext()).setDestination(AppInfoDashboardFragment.class.getName()).setArguments(arguments).setTitle(2131886405).setUserHandle(this.mUserHandle).setSourceMetricsCategory(this.mFragment.getMetricsCategory()).launch();
            return true;
        }
    }
}
