package com.android.settings.location;

import android.content.res.TypedArray;
import java.util.Collection;
import android.os.Looper;
import android.os.Parcelable;
import android.os.Messenger;
import android.os.Bundle;
import android.os.Message;
import android.app.ActivityManager;
import android.os.SystemClock;
import android.os.UserManager;
import android.content.res.XmlResourceParser;
import android.content.pm.ServiceInfo;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.R$styleable;
import android.util.AttributeSet;
import android.content.res.Resources;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.pm.ResolveInfo;
import java.util.ArrayList;
import android.content.Intent;
import android.os.UserHandle;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager;
import com.android.settings.widget.RestrictedAppPreference;
import com.android.settings.widget.AppPreference;
import android.text.TextUtils;
import android.content.pm.PackageManager;
import android.util.Log;
import android.util.IconDrawableFactory;
import android.content.pm.PackageItemInfo;
import android.support.v7.preference.Preference;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import android.os.Handler;
import android.content.Context;

class SettingsInjector
{
    private final Context mContext;
    private final Handler mHandler;
    private final Set<Setting> mSettings;
    
    public SettingsInjector(final Context mContext) {
        this.mContext = mContext;
        this.mSettings = new HashSet<Setting>();
        this.mHandler = new StatusLoadingHandler();
    }
    
    private Preference addServiceSetting(final Context context, final List<Preference> list, final InjectedSetting injectedSetting) {
        final PackageManager packageManager = this.mContext.getPackageManager();
        Drawable badgedIcon = null;
        try {
            final PackageItemInfo packageItemInfo = new PackageItemInfo();
            packageItemInfo.icon = injectedSetting.iconId;
            packageItemInfo.packageName = injectedSetting.packageName;
            badgedIcon = IconDrawableFactory.newInstance(this.mContext).getBadgedIcon(packageItemInfo, packageManager.getApplicationInfo(injectedSetting.packageName, 128), injectedSetting.mUserHandle.getIdentifier());
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Can't get ApplicationInfo for ");
            sb.append(injectedSetting.packageName);
            Log.e("SettingsInjector", sb.toString(), (Throwable)ex);
        }
        AppPreference appPreference;
        if (TextUtils.isEmpty((CharSequence)injectedSetting.userRestriction)) {
            appPreference = new AppPreference(context);
        }
        else {
            appPreference = new RestrictedAppPreference(context, injectedSetting.userRestriction);
        }
        appPreference.setTitle(injectedSetting.title);
        appPreference.setSummary(null);
        appPreference.setIcon(badgedIcon);
        appPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new ServiceSettingClickedListener(injectedSetting));
        list.add(appPreference);
        return appPreference;
    }
    
    private List<InjectedSetting> getSettings(final UserHandle userHandle) {
        final PackageManager packageManager = this.mContext.getPackageManager();
        final Intent intent = new Intent("android.location.SettingInjectorService");
        final int identifier = userHandle.getIdentifier();
        final List queryIntentServicesAsUser = packageManager.queryIntentServicesAsUser(intent, 128, identifier);
        if (Log.isLoggable("SettingsInjector", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Found services for profile id ");
            sb.append(identifier);
            sb.append(": ");
            sb.append(queryIntentServicesAsUser);
            Log.d("SettingsInjector", sb.toString());
        }
        final ArrayList list = new ArrayList<InjectedSetting>(queryIntentServicesAsUser.size());
        for (final ResolveInfo resolveInfo : queryIntentServicesAsUser) {
            try {
                final InjectedSetting serviceInfo = parseServiceInfo(resolveInfo, userHandle, packageManager);
                if (serviceInfo == null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unable to load service info ");
                    sb2.append(resolveInfo);
                    Log.w("SettingsInjector", sb2.toString());
                }
                else {
                    list.add(serviceInfo);
                }
            }
            catch (IOException ex) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to load service info ");
                sb3.append(resolveInfo);
                Log.w("SettingsInjector", sb3.toString(), (Throwable)ex);
            }
            catch (XmlPullParserException ex2) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Unable to load service info ");
                sb4.append(resolveInfo);
                Log.w("SettingsInjector", sb4.toString(), (Throwable)ex2);
            }
        }
        if (Log.isLoggable("SettingsInjector", 3)) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Loaded settings for profile id ");
            sb5.append(identifier);
            sb5.append(": ");
            sb5.append(list);
            Log.d("SettingsInjector", sb5.toString());
        }
        return (List<InjectedSetting>)list;
    }
    
    private static InjectedSetting parseAttributes(final String packageName, final String className, final UserHandle userHandle, Resources obtainAttributes, final AttributeSet set) {
        obtainAttributes = (Resources)obtainAttributes.obtainAttributes(set, R$styleable.SettingInjectorService);
        try {
            final String string = ((TypedArray)obtainAttributes).getString(1);
            final int resourceId = ((TypedArray)obtainAttributes).getResourceId(0, 0);
            final String string2 = ((TypedArray)obtainAttributes).getString(2);
            final String string3 = ((TypedArray)obtainAttributes).getString(3);
            if (Log.isLoggable("SettingsInjector", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("parsed title: ");
                sb.append(string);
                sb.append(", iconId: ");
                sb.append(resourceId);
                sb.append(", settingsActivity: ");
                sb.append(string2);
                Log.d("SettingsInjector", sb.toString());
            }
            return new InjectedSetting.Builder().setPackageName(packageName).setClassName(className).setTitle(string).setIconId(resourceId).setUserHandle(userHandle).setSettingsActivity(string2).setUserRestriction(string3).build();
        }
        finally {
            ((TypedArray)obtainAttributes).recycle();
        }
    }
    
    private static InjectedSetting parseServiceInfo(final ResolveInfo resolveInfo, final UserHandle userHandle, final PackageManager packageManager) throws XmlPullParserException, IOException {
        final ServiceInfo serviceInfo = resolveInfo.serviceInfo;
        if ((serviceInfo.applicationInfo.flags & 0x1) == 0x0 && Log.isLoggable("SettingsInjector", 5)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring attempt to inject setting from app not in system image: ");
            sb.append(resolveInfo);
            Log.w("SettingsInjector", sb.toString());
            return null;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            final XmlResourceParser loadXmlMetaData = serviceInfo.loadXmlMetaData(packageManager, "android.location.SettingInjectorService");
            if (loadXmlMetaData == null) {
                xmlResourceParser = loadXmlMetaData;
                xmlResourceParser = loadXmlMetaData;
                xmlResourceParser = loadXmlMetaData;
                final StringBuilder sb2 = new StringBuilder();
                xmlResourceParser = loadXmlMetaData;
                sb2.append("No android.location.SettingInjectorService meta-data for ");
                xmlResourceParser = loadXmlMetaData;
                sb2.append(resolveInfo);
                xmlResourceParser = loadXmlMetaData;
                sb2.append(": ");
                xmlResourceParser = loadXmlMetaData;
                sb2.append(serviceInfo);
                xmlResourceParser = loadXmlMetaData;
                final XmlPullParserException ex = new XmlPullParserException(sb2.toString());
                xmlResourceParser = loadXmlMetaData;
                throw ex;
            }
            xmlResourceParser = loadXmlMetaData;
            final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)loadXmlMetaData);
            int next;
            do {
                xmlResourceParser = loadXmlMetaData;
                next = loadXmlMetaData.next();
            } while (next != 1 && next != 2);
            xmlResourceParser = loadXmlMetaData;
            if ("injected-location-setting".equals(loadXmlMetaData.getName())) {
                xmlResourceParser = loadXmlMetaData;
                final Resources resourcesForApplicationAsUser = packageManager.getResourcesForApplicationAsUser(serviceInfo.packageName, userHandle.getIdentifier());
                xmlResourceParser = loadXmlMetaData;
                final InjectedSetting attributes = parseAttributes(serviceInfo.packageName, serviceInfo.name, userHandle, resourcesForApplicationAsUser, attributeSet);
                if (loadXmlMetaData != null) {
                    loadXmlMetaData.close();
                }
                return attributes;
            }
            xmlResourceParser = loadXmlMetaData;
            xmlResourceParser = loadXmlMetaData;
            final XmlPullParserException ex2 = new XmlPullParserException("Meta-data does not start with injected-location-setting tag");
            xmlResourceParser = loadXmlMetaData;
            throw ex2;
        }
        catch (PackageManager$NameNotFoundException ex3) {}
        finally {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        }
    }
    
    public List<Preference> getInjectedSettings(final Context context, final int n) {
        final List userProfiles = ((UserManager)this.mContext.getSystemService("user")).getUserProfiles();
        final ArrayList<Preference> list = new ArrayList<Preference>();
        for (int size = userProfiles.size(), i = 0; i < size; ++i) {
            final UserHandle userHandle = userProfiles.get(i);
            if (n == -2 || n == userHandle.getIdentifier()) {
                for (final InjectedSetting injectedSetting : this.getSettings(userHandle)) {
                    this.mSettings.add(new Setting(injectedSetting, this.addServiceSetting(context, list, injectedSetting)));
                }
            }
        }
        this.reloadStatusMessages();
        return list;
    }
    
    public boolean hasInjectedSettings(final int n) {
        final List userProfiles = ((UserManager)this.mContext.getSystemService("user")).getUserProfiles();
        for (int size = userProfiles.size(), i = 0; i < size; ++i) {
            final UserHandle userHandle = userProfiles.get(i);
            if (n == -2 || n == userHandle.getIdentifier()) {
                final Iterator<Object> iterator = this.getSettings(userHandle).iterator();
                if (iterator.hasNext()) {
                    iterator.next();
                    return true;
                }
            }
        }
        return false;
    }
    
    public void reloadStatusMessages() {
        if (Log.isLoggable("SettingsInjector", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("reloadingStatusMessages: ");
            sb.append(this.mSettings);
            Log.d("SettingsInjector", sb.toString());
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(1));
    }
    
    private class ServiceSettingClickedListener implements OnPreferenceClickListener
    {
        private InjectedSetting mInfo;
        
        public ServiceSettingClickedListener(final InjectedSetting mInfo) {
            this.mInfo = mInfo;
        }
        
        @Override
        public boolean onPreferenceClick(final Preference preference) {
            final Intent intent = new Intent();
            intent.setClassName(this.mInfo.packageName, this.mInfo.settingsActivity);
            intent.setFlags(268468224);
            SettingsInjector.this.mContext.startActivityAsUser(intent, this.mInfo.mUserHandle);
            return true;
        }
    }
    
    private final class Setting
    {
        public final Preference preference;
        public final InjectedSetting setting;
        public long startMillis;
        
        private Setting(final InjectedSetting setting, final Preference preference) {
            this.setting = setting;
            this.preference = preference;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Setting && this.setting.equals(((Setting)o).setting));
        }
        
        public long getElapsedTime() {
            return SystemClock.elapsedRealtime() - this.startMillis;
        }
        
        @Override
        public int hashCode() {
            return this.setting.hashCode();
        }
        
        public void maybeLogElapsedTime() {
            if (Log.isLoggable("SettingsInjector", 3) && this.startMillis != 0L) {
                final long elapsedTime = this.getElapsedTime();
                final StringBuilder sb = new StringBuilder();
                sb.append(this);
                sb.append(" update took ");
                sb.append(elapsedTime);
                sb.append(" millis");
                Log.d("SettingsInjector", sb.toString());
            }
        }
        
        public void startService() {
            if (!((ActivityManager)SettingsInjector.this.mContext.getSystemService("activity")).isUserRunning(this.setting.mUserHandle.getIdentifier())) {
                if (Log.isLoggable("SettingsInjector", 2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Cannot start service as user ");
                    sb.append(this.setting.mUserHandle.getIdentifier());
                    sb.append(" is not running");
                    Log.v("SettingsInjector", sb.toString());
                }
                return;
            }
            final Handler handler = new Handler() {
                public void handleMessage(final Message message) {
                    final Bundle data = message.getData();
                    final boolean boolean1 = data.getBoolean("enabled", true);
                    if (Log.isLoggable("SettingsInjector", 3)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(Setting.this.setting);
                        sb.append(": received ");
                        sb.append(message);
                        sb.append(", bundle: ");
                        sb.append(data);
                        Log.d("SettingsInjector", sb.toString());
                    }
                    Setting.this.preference.setSummary(null);
                    Setting.this.preference.setEnabled(boolean1);
                    SettingsInjector.this.mHandler.sendMessage(SettingsInjector.this.mHandler.obtainMessage(2, (Object)Setting.this));
                }
            };
            final Messenger messenger = new Messenger((Handler)handler);
            final Intent serviceIntent = this.setting.getServiceIntent();
            serviceIntent.putExtra("messenger", (Parcelable)messenger);
            if (Log.isLoggable("SettingsInjector", 3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(this.setting);
                sb2.append(": sending update intent: ");
                sb2.append(serviceIntent);
                sb2.append(", handler: ");
                sb2.append(handler);
                Log.d("SettingsInjector", sb2.toString());
                this.startMillis = SystemClock.elapsedRealtime();
            }
            else {
                this.startMillis = 0L;
            }
            SettingsInjector.this.mContext.startServiceAsUser(serviceIntent, this.setting.mUserHandle);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Setting{setting=");
            sb.append(this.setting);
            sb.append(", preference=");
            sb.append(this.preference);
            sb.append('}');
            return sb.toString();
        }
    }
    
    private final class StatusLoadingHandler extends Handler
    {
        private boolean mReloadRequested;
        private Set<Setting> mSettingsBeingLoaded;
        private Set<Setting> mSettingsToLoad;
        private Set<Setting> mTimedOutSettings;
        
        private StatusLoadingHandler() {
            super(Looper.getMainLooper());
            this.mSettingsToLoad = new HashSet<Setting>();
            this.mSettingsBeingLoaded = new HashSet<Setting>();
            this.mTimedOutSettings = new HashSet<Setting>();
        }
        
        public void handleMessage(final Message message) {
            if (Log.isLoggable("SettingsInjector", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("handleMessage start: ");
                sb.append(message);
                sb.append(", ");
                sb.append(this);
                Log.d("SettingsInjector", sb.toString());
            }
            switch (message.what) {
                default: {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unexpected what: ");
                    sb2.append(message);
                    Log.wtf("SettingsInjector", sb2.toString());
                    break;
                }
                case 3: {
                    final Setting setting = (Setting)message.obj;
                    this.mSettingsBeingLoaded.remove(setting);
                    this.mTimedOutSettings.add(setting);
                    if (Log.isLoggable("SettingsInjector", 5)) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Timed out after ");
                        sb3.append(setting.getElapsedTime());
                        sb3.append(" millis trying to get status for: ");
                        sb3.append(setting);
                        Log.w("SettingsInjector", sb3.toString());
                        break;
                    }
                    break;
                }
                case 2: {
                    final Setting setting2 = (Setting)message.obj;
                    setting2.maybeLogElapsedTime();
                    this.mSettingsBeingLoaded.remove(setting2);
                    this.mTimedOutSettings.remove(setting2);
                    this.removeMessages(3, (Object)setting2);
                    break;
                }
                case 1: {
                    this.mReloadRequested = true;
                    break;
                }
            }
            if (this.mSettingsBeingLoaded.size() > 0 || this.mTimedOutSettings.size() > 1) {
                if (Log.isLoggable("SettingsInjector", 2)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("too many services already live for ");
                    sb4.append(message);
                    sb4.append(", ");
                    sb4.append(this);
                    Log.v("SettingsInjector", sb4.toString());
                }
                return;
            }
            if (this.mReloadRequested && this.mSettingsToLoad.isEmpty() && this.mSettingsBeingLoaded.isEmpty() && this.mTimedOutSettings.isEmpty()) {
                if (Log.isLoggable("SettingsInjector", 2)) {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("reloading because idle and reload requesteed ");
                    sb5.append(message);
                    sb5.append(", ");
                    sb5.append(this);
                    Log.v("SettingsInjector", sb5.toString());
                }
                this.mSettingsToLoad.addAll(SettingsInjector.this.mSettings);
                this.mReloadRequested = false;
            }
            final Iterator<Setting> iterator = this.mSettingsToLoad.iterator();
            if (!iterator.hasNext()) {
                if (Log.isLoggable("SettingsInjector", 2)) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("nothing left to do for ");
                    sb6.append(message);
                    sb6.append(", ");
                    sb6.append(this);
                    Log.v("SettingsInjector", sb6.toString());
                }
                return;
            }
            final Setting setting3 = iterator.next();
            iterator.remove();
            setting3.startService();
            this.mSettingsBeingLoaded.add(setting3);
            this.sendMessageDelayed(this.obtainMessage(3, (Object)setting3), 1000L);
            if (Log.isLoggable("SettingsInjector", 3)) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("handleMessage end ");
                sb7.append(message);
                sb7.append(", ");
                sb7.append(this);
                sb7.append(", started loading ");
                sb7.append(setting3);
                Log.d("SettingsInjector", sb7.toString());
            }
        }
        
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("StatusLoadingHandler{mSettingsToLoad=");
            sb.append(this.mSettingsToLoad);
            sb.append(", mSettingsBeingLoaded=");
            sb.append(this.mSettingsBeingLoaded);
            sb.append(", mTimedOutSettings=");
            sb.append(this.mTimedOutSettings);
            sb.append(", mReloadRequested=");
            sb.append(this.mReloadRequested);
            sb.append('}');
            return sb.toString();
        }
    }
}
