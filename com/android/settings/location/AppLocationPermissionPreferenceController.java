package com.android.settings.location;

import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AppLocationPermissionPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public AppLocationPermissionPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "app_level_permissions";
    }
    
    @Override
    public boolean isAvailable() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Global.getInt(contentResolver, "location_settings_link_to_permissions_enabled", 1) != 1) {
            b = false;
        }
        return b;
    }
}
