package com.android.settings;

import java.util.Iterator;
import android.content.pm.ShortcutInfo$Builder;
import android.content.pm.ShortcutInfo;
import java.util.ArrayList;
import android.content.pm.ShortcutManager;
import android.os.UserHandle;
import android.os.UserManager;
import java.util.List;
import com.android.settings.shortcut.CreateShortcut;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.content.pm.UserInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.BroadcastReceiver;

public class SettingsInitialize extends BroadcastReceiver
{
    private void managedProfileSetup(final Context context, final PackageManager packageManager, Intent intent, final UserInfo userInfo) {
        if (userInfo != null && userInfo.isManagedProfile()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Received broadcast: ");
            sb.append(intent.getAction());
            sb.append(". Setting up intent forwarding for managed profile.");
            Log.i("Settings", sb.toString());
            packageManager.clearCrossProfileIntentFilters(userInfo.id);
            intent = new Intent();
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setPackage(context.getPackageName());
            final List queryIntentActivities = packageManager.queryIntentActivities(intent, 705);
            for (int size = queryIntentActivities.size(), i = 0; i < size; ++i) {
                final ResolveInfo resolveInfo = queryIntentActivities.get(i);
                if (resolveInfo.filter != null && resolveInfo.activityInfo != null && resolveInfo.activityInfo.metaData != null && resolveInfo.activityInfo.metaData.getBoolean("com.android.settings.PRIMARY_PROFILE_CONTROLLED")) {
                    packageManager.addCrossProfileIntentFilter(resolveInfo.filter, userInfo.id, userInfo.profileGroupId, 2);
                }
            }
            packageManager.setComponentEnabledSetting(new ComponentName(context, (Class)Settings.class), 2, 1);
            packageManager.setComponentEnabledSetting(new ComponentName(context, (Class)CreateShortcut.class), 2, 1);
        }
    }
    
    private void webviewSettingSetup(final Context context, final PackageManager packageManager, final UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        final ComponentName componentName = new ComponentName("com.android.settings", "com.android.settings.WebViewImplementation");
        int n;
        if (userInfo.isAdmin()) {
            n = 1;
        }
        else {
            n = 2;
        }
        packageManager.setComponentEnabledSetting(componentName, n, 1);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final UserInfo userInfo = ((UserManager)context.getSystemService("user")).getUserInfo(UserHandle.myUserId());
        final PackageManager packageManager = context.getPackageManager();
        this.managedProfileSetup(context, packageManager, intent, userInfo);
        this.webviewSettingSetup(context, packageManager, userInfo);
        this.refreshExistingShortcuts(context);
    }
    
    void refreshExistingShortcuts(final Context context) {
        final ShortcutManager shortcutManager = (ShortcutManager)context.getSystemService((Class)ShortcutManager.class);
        final List pinnedShortcuts = shortcutManager.getPinnedShortcuts();
        final ArrayList<ShortcutInfo> list = new ArrayList<ShortcutInfo>();
        for (final ShortcutInfo shortcutInfo : pinnedShortcuts) {
            final Intent intent = shortcutInfo.getIntent();
            intent.setFlags(335544320);
            list.add(new ShortcutInfo$Builder(context, shortcutInfo.getId()).setIntent(intent).build());
        }
        shortcutManager.updateShortcuts((List)list);
    }
}
