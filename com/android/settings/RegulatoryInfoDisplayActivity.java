package com.android.settings;

import android.content.DialogInterface;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;
import android.content.res.Resources;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.content.res.Resources$NotFoundException;
import android.graphics.BitmapFactory;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.os.Bundle;
import android.os.SystemProperties;
import java.util.Locale;
import android.text.TextUtils;
import android.content.DialogInterface$OnDismissListener;
import android.app.Activity;

public class RegulatoryInfoDisplayActivity extends Activity implements DialogInterface$OnDismissListener
{
    private final String REGULATORY_INFO_RESOURCE;
    
    public RegulatoryInfoDisplayActivity() {
        this.REGULATORY_INFO_RESOURCE = "regulatory_info";
    }
    
    public static String getRegulatoryInfoImageFileName() {
        final String sku = getSku();
        if (TextUtils.isEmpty((CharSequence)sku)) {
            return "/data/misc/elabel/regulatory_info.png";
        }
        return String.format(Locale.US, "/data/misc/elabel/regulatory_info_%s.png", sku.toLowerCase());
    }
    
    private int getResourceId() {
        final int identifier = this.getResources().getIdentifier("regulatory_info", "drawable", this.getPackageName());
        final String sku = getSku();
        int n = identifier;
        if (!TextUtils.isEmpty((CharSequence)sku)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("regulatory_info_");
            sb.append(sku.toLowerCase());
            final int identifier2 = this.getResources().getIdentifier(sb.toString(), "drawable", this.getPackageName());
            n = identifier;
            if (identifier2 != 0) {
                n = identifier2;
            }
        }
        return n;
    }
    
    public static String getSku() {
        return SystemProperties.get("ro.boot.hardware.sku", "");
    }
    
    protected void onCreate(Bundle setOnDismissListener) {
        super.onCreate(setOnDismissListener);
        final Resources resources = this.getResources();
        if (!resources.getBoolean(2131034159)) {
            this.finish();
        }
        setOnDismissListener = (Bundle)new AlertDialog$Builder((Context)this).setTitle(2131888799).setOnDismissListener((DialogInterface$OnDismissListener)this);
        int n = 0;
        final Bitmap decodeFile = BitmapFactory.decodeFile(getRegulatoryInfoImageFileName());
        if (decodeFile != null) {
            n = 1;
        }
        int resourceId = 0;
        if (n == 0) {
            resourceId = this.getResourceId();
        }
        if (resourceId != 0) {
            try {
                final Drawable drawable = this.getDrawable(resourceId);
                if (drawable.getIntrinsicWidth() > 2 && drawable.getIntrinsicHeight() > 2) {
                    n = 1;
                }
                else {
                    n = 0;
                }
            }
            catch (Resources$NotFoundException ex) {
                n = 0;
            }
        }
        final CharSequence text = resources.getText(2131888798);
        if (n != 0) {
            final View inflate = this.getLayoutInflater().inflate(2131558706, (ViewGroup)null);
            final ImageView imageView = (ImageView)inflate.findViewById(2131362514);
            if (decodeFile != null) {
                imageView.setImageBitmap(decodeFile);
            }
            else {
                imageView.setImageResource(resourceId);
            }
            ((AlertDialog$Builder)setOnDismissListener).setView(inflate);
            ((AlertDialog$Builder)setOnDismissListener).show();
        }
        else if (text.length() > 0) {
            ((AlertDialog$Builder)setOnDismissListener).setMessage(text);
            ((TextView)((AlertDialog$Builder)setOnDismissListener).show().findViewById(16908299)).setGravity(17);
        }
        else {
            this.finish();
        }
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        this.finish();
    }
}
