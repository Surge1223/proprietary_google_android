package com.android.settings.wfd;

import android.util.TypedValue;
import android.widget.ImageView;
import android.content.IntentFilter;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.net.wifi.p2p.WifiP2pManager$ChannelListener;
import android.os.Looper;
import android.os.Bundle;
import com.android.internal.app.MediaRouteDialogPresenter;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.widget.EditText;
import android.view.ViewGroup;
import android.util.Slog;
import android.net.wifi.p2p.WifiP2pManager$ActionListener;
import android.app.Activity;
import android.provider.Settings;
import android.support.v7.preference.ListPreference;
import android.support.v14.preference.SwitchPreference;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.hardware.display.WifiDisplay;
import android.media.MediaRouter$RouteInfo;
import android.media.MediaRouter$SimpleCallback;
import android.net.Uri;
import android.content.Intent;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager$Channel;
import android.hardware.display.WifiDisplayStatus;
import android.database.ContentObserver;
import android.media.MediaRouter$Callback;
import android.media.MediaRouter;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.widget.TextView;
import android.hardware.display.DisplayManager;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public final class WifiDisplaySettings extends SettingsPreferenceFragment implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private boolean mAutoGO;
    private PreferenceGroup mCertCategory;
    private DisplayManager mDisplayManager;
    private TextView mEmptyView;
    private final Handler mHandler;
    private boolean mListen;
    private int mListenChannel;
    private int mOperatingChannel;
    private int mPendingChanges;
    private final BroadcastReceiver mReceiver;
    private MediaRouter mRouter;
    private final MediaRouter$Callback mRouterCallback;
    private final ContentObserver mSettingsObserver;
    private boolean mStarted;
    private final Runnable mUpdateRunnable;
    private boolean mWifiDisplayCertificationOn;
    private boolean mWifiDisplayOnSetting;
    private WifiDisplayStatus mWifiDisplayStatus;
    private WifiP2pManager$Channel mWifiP2pChannel;
    private WifiP2pManager mWifiP2pManager;
    private int mWpsConfig;
    
    static {
        SUMMARY_PROVIDER_FACTORY = (SummaryLoader.SummaryProviderFactory)_$$Lambda$WifiDisplaySettings$FSGRkDMrB620EgLXH7J2ShDkw60.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082870;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    public WifiDisplaySettings() {
        this.mWpsConfig = 4;
        this.mUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                final int access$1000 = WifiDisplaySettings.this.mPendingChanges;
                WifiDisplaySettings.this.mPendingChanges = 0;
                WifiDisplaySettings.this.update(access$1000);
            }
        };
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED")) {
                    WifiDisplaySettings.this.scheduleUpdate(4);
                }
            }
        };
        this.mSettingsObserver = new ContentObserver(new Handler()) {
            public void onChange(final boolean b, final Uri uri) {
                WifiDisplaySettings.this.scheduleUpdate(1);
            }
        };
        this.mRouterCallback = (MediaRouter$Callback)new MediaRouter$SimpleCallback() {
            public void onRouteAdded(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                WifiDisplaySettings.this.scheduleUpdate(2);
            }
            
            public void onRouteChanged(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                WifiDisplaySettings.this.scheduleUpdate(2);
            }
            
            public void onRouteRemoved(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                WifiDisplaySettings.this.scheduleUpdate(2);
            }
            
            public void onRouteSelected(final MediaRouter mediaRouter, final int n, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                WifiDisplaySettings.this.scheduleUpdate(2);
            }
            
            public void onRouteUnselected(final MediaRouter mediaRouter, final int n, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                WifiDisplaySettings.this.scheduleUpdate(2);
            }
        };
        this.mHandler = new Handler();
    }
    
    private void buildCertificationMenu(final PreferenceScreen preferenceScreen) {
        if (this.mCertCategory == null) {
            (this.mCertCategory = new PreferenceCategory(this.getPrefContext())).setTitle(2131889956);
            this.mCertCategory.setOrder(1);
        }
        else {
            this.mCertCategory.removeAll();
        }
        preferenceScreen.addPreference(this.mCertCategory);
        if (!this.mWifiDisplayStatus.getSessionInfo().getGroupId().isEmpty()) {
            final Preference preference = new Preference(this.getPrefContext());
            preference.setTitle(2131889970);
            preference.setSummary(this.mWifiDisplayStatus.getSessionInfo().toString());
            this.mCertCategory.addPreference(preference);
            if (this.mWifiDisplayStatus.getSessionInfo().getSessionId() != 0) {
                this.mCertCategory.addPreference(new Preference(this.getPrefContext()) {
                    @Override
                    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
                        super.onBindViewHolder(preferenceViewHolder);
                        final Button button = (Button)preferenceViewHolder.findViewById(2131362331);
                        button.setText(2131889968);
                        button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                            public void onClick(final View view) {
                                WifiDisplaySettings.this.mDisplayManager.pauseWifiDisplay();
                            }
                        });
                        final Button button2 = (Button)preferenceViewHolder.findViewById(2131362531);
                        button2.setText(2131889969);
                        button2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                            public void onClick(final View view) {
                                WifiDisplaySettings.this.mDisplayManager.resumeWifiDisplay();
                            }
                        });
                    }
                });
                this.mCertCategory.setLayoutResource(2131558858);
            }
        }
        final SwitchPreference switchPreference = new SwitchPreference(this.getPrefContext()) {
            @Override
            protected void onClick() {
                WifiDisplaySettings.this.mListen ^= true;
                WifiDisplaySettings.this.setListenMode(WifiDisplaySettings.this.mListen);
                this.setChecked(WifiDisplaySettings.this.mListen);
            }
        };
        switchPreference.setTitle(2131889961);
        switchPreference.setChecked(this.mListen);
        this.mCertCategory.addPreference(switchPreference);
        final SwitchPreference switchPreference2 = new SwitchPreference(this.getPrefContext()) {
            @Override
            protected void onClick() {
                WifiDisplaySettings.this.mAutoGO ^= true;
                if (WifiDisplaySettings.this.mAutoGO) {
                    WifiDisplaySettings.this.startAutoGO();
                }
                else {
                    WifiDisplaySettings.this.stopAutoGO();
                }
                this.setChecked(WifiDisplaySettings.this.mAutoGO);
            }
        };
        switchPreference2.setTitle(2131889954);
        switchPreference2.setChecked(this.mAutoGO);
        this.mCertCategory.addPreference(switchPreference2);
        final ListPreference listPreference = new ListPreference(this.getPrefContext());
        listPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                final int int1 = Integer.parseInt((String)o);
                if (int1 != WifiDisplaySettings.this.mWpsConfig) {
                    WifiDisplaySettings.this.mWpsConfig = int1;
                    WifiDisplaySettings.this.getActivity().invalidateOptionsMenu();
                    Settings.Global.putInt(WifiDisplaySettings.this.getActivity().getContentResolver(), "wifi_display_wps_config", WifiDisplaySettings.this.mWpsConfig);
                }
                return true;
            }
        });
        this.mWpsConfig = Settings.Global.getInt(this.getActivity().getContentResolver(), "wifi_display_wps_config", 4);
        listPreference.setKey("wps");
        listPreference.setTitle(2131889976);
        listPreference.setEntries(new String[] { "Default", "PBC", "KEYPAD", "DISPLAY" });
        listPreference.setEntryValues(new String[] { "4", "0", "2", "1" });
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.mWpsConfig);
        listPreference.setValue(sb.toString());
        listPreference.setSummary("%1$s");
        this.mCertCategory.addPreference(listPreference);
        final ListPreference listPreference2 = new ListPreference(this.getPrefContext());
        listPreference2.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                final int int1 = Integer.parseInt((String)o);
                if (int1 != WifiDisplaySettings.this.mListenChannel) {
                    WifiDisplaySettings.this.mListenChannel = int1;
                    WifiDisplaySettings.this.getActivity().invalidateOptionsMenu();
                    WifiDisplaySettings.this.setWifiP2pChannels(WifiDisplaySettings.this.mListenChannel, WifiDisplaySettings.this.mOperatingChannel);
                }
                return true;
            }
        });
        listPreference2.setKey("listening_channel");
        listPreference2.setTitle(2131889960);
        listPreference2.setEntries(new String[] { "Auto", "1", "6", "11" });
        listPreference2.setEntryValues(new String[] { "0", "1", "6", "11" });
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(this.mListenChannel);
        listPreference2.setValue(sb2.toString());
        listPreference2.setSummary("%1$s");
        this.mCertCategory.addPreference(listPreference2);
        final ListPreference listPreference3 = new ListPreference(this.getPrefContext());
        listPreference3.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                final int int1 = Integer.parseInt((String)o);
                if (int1 != WifiDisplaySettings.this.mOperatingChannel) {
                    WifiDisplaySettings.this.mOperatingChannel = int1;
                    WifiDisplaySettings.this.getActivity().invalidateOptionsMenu();
                    WifiDisplaySettings.this.setWifiP2pChannels(WifiDisplaySettings.this.mListenChannel, WifiDisplaySettings.this.mOperatingChannel);
                }
                return true;
            }
        });
        listPreference3.setKey("operating_channel");
        listPreference3.setTitle(2131889963);
        listPreference3.setEntries(new String[] { "Auto", "1", "6", "11", "36" });
        listPreference3.setEntryValues(new String[] { "0", "1", "6", "11", "36" });
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(this.mOperatingChannel);
        listPreference3.setValue(sb3.toString());
        listPreference3.setSummary("%1$s");
        this.mCertCategory.addPreference(listPreference3);
    }
    
    private RoutePreference createRoutePreference(final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
        final WifiDisplay wifiDisplay = this.findWifiDisplay(mediaRouter$RouteInfo.getDeviceAddress());
        if (wifiDisplay != null) {
            return (RoutePreference)new WifiDisplayRoutePreference(this.getPrefContext(), mediaRouter$RouteInfo, wifiDisplay);
        }
        return new RoutePreference(this.getPrefContext(), mediaRouter$RouteInfo);
    }
    
    private WifiDisplay findWifiDisplay(final String s) {
        if (this.mWifiDisplayStatus != null && s != null) {
            for (final WifiDisplay wifiDisplay : this.mWifiDisplayStatus.getDisplays()) {
                if (wifiDisplay.getDeviceAddress().equals(s)) {
                    return wifiDisplay;
                }
            }
        }
        return null;
    }
    
    public static boolean isAvailable(final Context context) {
        return context.getSystemService("display") != null && context.getPackageManager().hasSystemFeature("android.hardware.wifi.direct") && context.getSystemService("wifip2p") != null;
    }
    
    private void pairWifiDisplay(final WifiDisplay wifiDisplay) {
        if (wifiDisplay.canConnect()) {
            this.mDisplayManager.connectWifiDisplay(wifiDisplay.getDeviceAddress());
        }
    }
    
    private void scheduleUpdate(final int n) {
        if (this.mStarted) {
            if (this.mPendingChanges == 0) {
                this.mHandler.post(this.mUpdateRunnable);
            }
            this.mPendingChanges |= n;
        }
    }
    
    private void setListenMode(final boolean b) {
        this.mWifiP2pManager.listen(this.mWifiP2pChannel, b, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
            public void onFailure(final int n) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to ");
                String s;
                if (b) {
                    s = "entered";
                }
                else {
                    s = "exited";
                }
                sb.append(s);
                sb.append(" listen mode with reason ");
                sb.append(n);
                sb.append(".");
                Slog.e("WifiDisplaySettings", sb.toString());
            }
            
            public void onSuccess() {
            }
        });
    }
    
    private void setWifiP2pChannels(final int n, final int n2) {
        this.mWifiP2pManager.setWifiP2pChannels(this.mWifiP2pChannel, n, n2, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
            public void onFailure(final int n) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to set wifi p2p channels with reason ");
                sb.append(n);
                sb.append(".");
                Slog.e("WifiDisplaySettings", sb.toString());
            }
            
            public void onSuccess() {
            }
        });
    }
    
    private void showWifiDisplayOptionsDialog(final WifiDisplay wifiDisplay) {
        final View inflate = this.getActivity().getLayoutInflater().inflate(2131558888, (ViewGroup)null);
        final EditText editText = (EditText)inflate.findViewById(2131362382);
        editText.setText((CharSequence)wifiDisplay.getFriendlyDisplayName());
        new AlertDialog$Builder((Context)this.getActivity()).setCancelable(true).setTitle(2131889967).setView(inflate).setPositiveButton(2131889964, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String trim = editText.getText().toString().trim();
                String s = null;
                Label_0039: {
                    if (!trim.isEmpty()) {
                        s = trim;
                        if (!trim.equals(wifiDisplay.getDeviceName())) {
                            break Label_0039;
                        }
                    }
                    s = null;
                }
                WifiDisplaySettings.this.mDisplayManager.renameWifiDisplay(wifiDisplay.getDeviceAddress(), s);
            }
        }).setNegativeButton(2131889965, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                WifiDisplaySettings.this.mDisplayManager.forgetWifiDisplay(wifiDisplay.getDeviceAddress());
            }
        }).create().show();
    }
    
    private void startAutoGO() {
        this.mWifiP2pManager.createGroup(this.mWifiP2pChannel, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
            public void onFailure(final int n) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to start AutoGO with reason ");
                sb.append(n);
                sb.append(".");
                Slog.e("WifiDisplaySettings", sb.toString());
            }
            
            public void onSuccess() {
            }
        });
    }
    
    private void stopAutoGO() {
        this.mWifiP2pManager.removeGroup(this.mWifiP2pChannel, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
            public void onFailure(final int n) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to stop AutoGO with reason ");
                sb.append(n);
                sb.append(".");
                Slog.e("WifiDisplaySettings", sb.toString());
            }
            
            public void onSuccess() {
            }
        });
    }
    
    private void toggleRoute(final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
        if (mediaRouter$RouteInfo.isSelected()) {
            MediaRouteDialogPresenter.showDialogFragment(this.getActivity(), 4, (View.OnClickListener)null);
        }
        else {
            mediaRouter$RouteInfo.select();
        }
    }
    
    private void unscheduleUpdate() {
        if (this.mPendingChanges != 0) {
            this.mPendingChanges = 0;
            this.mHandler.removeCallbacks(this.mUpdateRunnable);
        }
    }
    
    private void update(int i) {
        boolean b = false;
        final int n = 0;
        if ((i & 0x1) != 0x0) {
            final int int1 = Settings.Global.getInt(this.getContentResolver(), "wifi_display_on", 0);
            final boolean b2 = true;
            this.mWifiDisplayOnSetting = (int1 != 0);
            this.mWifiDisplayCertificationOn = (Settings.Global.getInt(this.getContentResolver(), "wifi_display_certification_on", 0) != 0 && b2);
            this.mWpsConfig = Settings.Global.getInt(this.getContentResolver(), "wifi_display_wps_config", 4);
            b = true;
        }
        if ((i & 0x4) != 0x0) {
            this.mWifiDisplayStatus = this.mDisplayManager.getWifiDisplayStatus();
            b = true;
        }
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        int routeCount;
        MediaRouter$RouteInfo route;
        for (routeCount = this.mRouter.getRouteCount(), i = 0; i < routeCount; ++i) {
            route = this.mRouter.getRouteAt(i);
            if (route.matchesTypes(4)) {
                preferenceScreen.addPreference(this.createRoutePreference(route));
            }
        }
        if (this.mWifiDisplayStatus != null && this.mWifiDisplayStatus.getFeatureState() == 3) {
            final WifiDisplay[] displays = this.mWifiDisplayStatus.getDisplays();
            int length;
            WifiDisplay wifiDisplay;
            for (length = displays.length, i = n; i < length; ++i) {
                wifiDisplay = displays[i];
                if (!wifiDisplay.isRemembered() && wifiDisplay.isAvailable() && !wifiDisplay.equals(this.mWifiDisplayStatus.getActiveDisplay())) {
                    preferenceScreen.addPreference(new UnpairedWifiDisplayPreference(this.getPrefContext(), wifiDisplay));
                }
            }
            if (this.mWifiDisplayCertificationOn) {
                this.buildCertificationMenu(preferenceScreen);
            }
        }
        if (b) {
            this.getActivity().invalidateOptionsMenu();
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887825;
    }
    
    @Override
    public int getMetricsCategory() {
        return 102;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        (this.mEmptyView = (TextView)this.getView().findViewById(16908292)).setText(2131889962);
        this.setEmptyView((View)this.mEmptyView);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mRouter = (MediaRouter)((Context)activity).getSystemService("media_router");
        this.mDisplayManager = (DisplayManager)((Context)activity).getSystemService("display");
        this.mWifiP2pManager = (WifiP2pManager)((Context)activity).getSystemService("wifip2p");
        this.mWifiP2pChannel = this.mWifiP2pManager.initialize((Context)activity, Looper.getMainLooper(), (WifiP2pManager$ChannelListener)null);
        this.addPreferencesFromResource(2132082870);
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        if (this.mWifiDisplayStatus != null && this.mWifiDisplayStatus.getFeatureState() != 0) {
            final MenuItem add = menu.add(0, 1, 0, 2131889959);
            add.setCheckable(true);
            add.setChecked(this.mWifiDisplayOnSetting);
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 1) {
            return super.onOptionsItemSelected(menuItem);
        }
        menuItem.setChecked(this.mWifiDisplayOnSetting = (menuItem.isChecked() ^ true));
        Settings.Global.putInt(this.getContentResolver(), "wifi_display_on", (int)(this.mWifiDisplayOnSetting ? 1 : 0));
        return true;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.mStarted = true;
        final Activity activity = this.getActivity();
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED");
        ((Context)activity).registerReceiver(this.mReceiver, intentFilter);
        this.getContentResolver().registerContentObserver(Settings.Global.getUriFor("wifi_display_on"), false, this.mSettingsObserver);
        this.getContentResolver().registerContentObserver(Settings.Global.getUriFor("wifi_display_certification_on"), false, this.mSettingsObserver);
        this.getContentResolver().registerContentObserver(Settings.Global.getUriFor("wifi_display_wps_config"), false, this.mSettingsObserver);
        this.mRouter.addCallback(4, this.mRouterCallback, 1);
        this.update(-1);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.mStarted = false;
        ((Context)this.getActivity()).unregisterReceiver(this.mReceiver);
        this.getContentResolver().unregisterContentObserver(this.mSettingsObserver);
        this.mRouter.removeCallback(this.mRouterCallback);
        this.unscheduleUpdate();
    }
    
    private class RoutePreference extends Preference implements OnPreferenceClickListener
    {
        private final MediaRouter$RouteInfo mRoute;
        
        public RoutePreference(final Context context, final MediaRouter$RouteInfo mRoute) {
            super(context);
            this.mRoute = mRoute;
            this.setTitle(mRoute.getName());
            this.setSummary(mRoute.getDescription());
            this.setEnabled(mRoute.isEnabled());
            if (mRoute.isSelected()) {
                this.setOrder(2);
                if (mRoute.isConnecting()) {
                    this.setSummary(2131889973);
                }
                else {
                    this.setSummary(2131889972);
                }
            }
            else if (this.isEnabled()) {
                this.setOrder(3);
            }
            else {
                this.setOrder(4);
                if (mRoute.getStatusCode() == 5) {
                    this.setSummary(2131889974);
                }
                else {
                    this.setSummary(2131889975);
                }
            }
            this.setOnPreferenceClickListener((OnPreferenceClickListener)this);
        }
        
        @Override
        public boolean onPreferenceClick(final Preference preference) {
            WifiDisplaySettings.this.toggleRoute(this.mRoute);
            return true;
        }
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final MediaRouter mRouter;
        private final MediaRouter$Callback mRouterCallback;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mRouterCallback = (MediaRouter$Callback)new MediaRouter$SimpleCallback() {
                public void onRouteAdded(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                    SummaryProvider.this.updateSummary();
                }
                
                public void onRouteChanged(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                    SummaryProvider.this.updateSummary();
                }
                
                public void onRouteRemoved(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                    SummaryProvider.this.updateSummary();
                }
                
                public void onRouteSelected(final MediaRouter mediaRouter, final int n, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                    SummaryProvider.this.updateSummary();
                }
                
                public void onRouteUnselected(final MediaRouter mediaRouter, final int n, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
                    SummaryProvider.this.updateSummary();
                }
            };
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
            this.mRouter = (MediaRouter)mContext.getSystemService("media_router");
        }
        
        private void updateSummary() {
            final String string = this.mContext.getString(2131887439);
            final int routeCount = this.mRouter.getRouteCount();
            int n = 0;
            String string2;
            while (true) {
                string2 = string;
                if (n >= routeCount) {
                    break;
                }
                final MediaRouter$RouteInfo route = this.mRouter.getRouteAt(n);
                if (route.matchesTypes(4) && route.isSelected() && !route.isConnecting()) {
                    string2 = this.mContext.getString(2131889972);
                    break;
                }
                ++n;
            }
            this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, string2);
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mRouter.addCallback(4, this.mRouterCallback);
                this.updateSummary();
            }
            else {
                this.mRouter.removeCallback(this.mRouterCallback);
            }
        }
    }
    
    private class UnpairedWifiDisplayPreference extends Preference implements OnPreferenceClickListener
    {
        private final WifiDisplay mDisplay;
        
        public UnpairedWifiDisplayPreference(final Context context, final WifiDisplay mDisplay) {
            super(context);
            this.mDisplay = mDisplay;
            this.setTitle(mDisplay.getFriendlyDisplayName());
            this.setSummary(17041132);
            this.setEnabled(mDisplay.canConnect());
            if (this.isEnabled()) {
                this.setOrder(3);
            }
            else {
                this.setOrder(4);
                this.setSummary(2131889974);
            }
            this.setOnPreferenceClickListener((OnPreferenceClickListener)this);
        }
        
        @Override
        public boolean onPreferenceClick(final Preference preference) {
            WifiDisplaySettings.this.pairWifiDisplay(this.mDisplay);
            return true;
        }
    }
    
    private class WifiDisplayRoutePreference extends RoutePreference implements View.OnClickListener
    {
        private final WifiDisplay mDisplay;
        
        public WifiDisplayRoutePreference(final Context context, final MediaRouter$RouteInfo mediaRouter$RouteInfo, final WifiDisplay mDisplay) {
            super(context, mediaRouter$RouteInfo);
            this.mDisplay = mDisplay;
            this.setWidgetLayoutResource(2131558889);
        }
        
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            super.onBindViewHolder(preferenceViewHolder);
            final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131362072);
            if (imageView != null) {
                imageView.setOnClickListener((View.OnClickListener)this);
                if (!this.isEnabled()) {
                    final TypedValue typedValue = new TypedValue();
                    this.getContext().getTheme().resolveAttribute(16842803, typedValue, true);
                    imageView.setImageAlpha((int)(typedValue.getFloat() * 255.0f));
                    imageView.setEnabled(true);
                }
            }
        }
        
        public void onClick(final View view) {
            WifiDisplaySettings.this.showWifiDisplayOptionsDialog(this.mDisplay);
        }
    }
}
