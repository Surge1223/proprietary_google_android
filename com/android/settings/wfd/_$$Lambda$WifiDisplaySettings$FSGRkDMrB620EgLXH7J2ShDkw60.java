package com.android.settings.wfd;

import android.util.TypedValue;
import android.widget.ImageView;
import android.content.IntentFilter;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.net.wifi.p2p.WifiP2pManager$ChannelListener;
import android.os.Looper;
import android.os.Bundle;
import com.android.internal.app.MediaRouteDialogPresenter;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.widget.EditText;
import android.view.ViewGroup;
import android.util.Slog;
import android.net.wifi.p2p.WifiP2pManager$ActionListener;
import android.provider.Settings;
import android.support.v7.preference.ListPreference;
import android.support.v14.preference.SwitchPreference;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.hardware.display.WifiDisplay;
import android.media.MediaRouter$RouteInfo;
import android.media.MediaRouter$SimpleCallback;
import android.net.Uri;
import android.content.Intent;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager$Channel;
import android.hardware.display.WifiDisplayStatus;
import android.database.ContentObserver;
import android.media.MediaRouter$Callback;
import android.media.MediaRouter;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.widget.TextView;
import android.hardware.display.DisplayManager;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$WifiDisplaySettings$FSGRkDMrB620EgLXH7J2ShDkw60 implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return WifiDisplaySettings.lambda$static$0(activity, summaryLoader);
    }
}
