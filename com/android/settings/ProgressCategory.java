package com.android.settings;

import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ProgressCategory extends ProgressCategoryBase
{
    private int mEmptyTextRes;
    private boolean mNoDeviceFoundAdded;
    private Preference mNoDeviceFoundPreference;
    private boolean mProgress;
    
    public ProgressCategory(final Context context) {
        super(context);
        this.mProgress = false;
        this.setLayoutResource(2131558668);
    }
    
    public ProgressCategory(final Context context, final AttributeSet set) {
        super(context, set);
        this.mProgress = false;
        this.setLayoutResource(2131558668);
    }
    
    public ProgressCategory(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mProgress = false;
        this.setLayoutResource(2131558668);
    }
    
    public ProgressCategory(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mProgress = false;
        this.setLayoutResource(2131558668);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362545);
        final boolean b = this.getPreferenceCount() == 0 || (this.getPreferenceCount() == 1 && this.getPreference(0) == this.mNoDeviceFoundPreference);
        int visibility;
        if (this.mProgress) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        viewById.setVisibility(visibility);
        if (!this.mProgress && b) {
            if (!this.mNoDeviceFoundAdded) {
                if (this.mNoDeviceFoundPreference == null) {
                    (this.mNoDeviceFoundPreference = new Preference(this.getContext())).setLayoutResource(2131558660);
                    this.mNoDeviceFoundPreference.setTitle(this.mEmptyTextRes);
                    this.mNoDeviceFoundPreference.setSelectable(false);
                }
                this.addPreference(this.mNoDeviceFoundPreference);
                this.mNoDeviceFoundAdded = true;
            }
        }
        else if (this.mNoDeviceFoundAdded) {
            this.removePreference(this.mNoDeviceFoundPreference);
            this.mNoDeviceFoundAdded = false;
        }
    }
    
    public void setEmptyTextRes(final int mEmptyTextRes) {
        this.mEmptyTextRes = mEmptyTextRes;
    }
    
    public void setProgress(final boolean mProgress) {
        this.mProgress = mProgress;
        this.notifyChanged();
    }
}
