package com.android.settings.support;

import java.text.ParseException;
import android.text.TextUtils;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public final class SupportPhone implements Parcelable
{
    public static final Parcelable.Creator<SupportPhone> CREATOR;
    public final boolean isTollFree;
    public final String language;
    public final String number;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<SupportPhone>() {
            public SupportPhone createFromParcel(final Parcel parcel) {
                return new SupportPhone(parcel);
            }
            
            public SupportPhone[] newArray(final int n) {
                return new SupportPhone[n];
            }
        };
    }
    
    protected SupportPhone(final Parcel parcel) {
        this.language = parcel.readString();
        this.number = parcel.readString();
        this.isTollFree = (parcel.readInt() != 0);
    }
    
    public SupportPhone(final String s) throws ParseException {
        final String[] split = s.split(":");
        if (split.length == 3) {
            this.language = split[0];
            this.isTollFree = TextUtils.equals((CharSequence)split[1], (CharSequence)"tollfree");
            this.number = split[2];
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Phone config is invalid ");
        sb.append(s);
        throw new ParseException(sb.toString(), 0);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.language);
        parcel.writeString(this.number);
        parcel.writeInt((int)(this.isTollFree ? 1 : 0));
    }
}
