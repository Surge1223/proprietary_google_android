package com.android.settings.support.actionbar;

public interface HelpResourceProvider
{
    default int getHelpResource() {
        return 2131887775;
    }
}
