package com.android.settings.support.actionbar;

import android.app.Activity;
import android.os.Bundle;
import com.android.settingslib.HelpUtils;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settingslib.core.lifecycle.ObservablePreferenceFragment;
import com.android.settingslib.core.lifecycle.ObservableFragment;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class HelpMenuController implements LifecycleObserver, OnCreateOptionsMenu
{
    private final Fragment mHost;
    
    private HelpMenuController(final Fragment mHost) {
        this.mHost = mHost;
    }
    
    public static void init(final ObservableFragment observableFragment) {
        observableFragment.getLifecycle().addObserver(new HelpMenuController(observableFragment));
    }
    
    public static void init(final ObservablePreferenceFragment observablePreferenceFragment) {
        observablePreferenceFragment.getLifecycle().addObserver(new HelpMenuController(observablePreferenceFragment));
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final Bundle arguments = this.mHost.getArguments();
        int n = 0;
        if (arguments != null && arguments.containsKey("help_uri_resource")) {
            n = arguments.getInt("help_uri_resource");
        }
        else if (this.mHost instanceof HelpResourceProvider) {
            n = ((HelpResourceProvider)this.mHost).getHelpResource();
        }
        String string = null;
        if (n != 0) {
            string = this.mHost.getContext().getString(n);
        }
        final Activity activity = this.mHost.getActivity();
        if (string != null && activity != null) {
            HelpUtils.prepareHelpMenuItem(activity, menu, string, this.mHost.getClass().getName());
        }
    }
}
