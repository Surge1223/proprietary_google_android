package com.android.settings.support;

import com.android.settings.overlay.SupportFeatureProvider;
import com.android.settings.overlay.FeatureFactory;
import android.os.Bundle;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import android.app.Activity;

public class SupportDashboardActivity extends Activity implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (!context.getResources().getBoolean(2131034178)) {
                    nonIndexableKeys.add("support_dashboard_activity");
                }
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.title = context.getString(2131888552);
                searchIndexableRaw.screenTitle = context.getString(2131889054);
                searchIndexableRaw.summaryOn = context.getString(2131889411);
                searchIndexableRaw.iconResId = 2131231043;
                searchIndexableRaw.intentTargetPackage = context.getPackageName();
                searchIndexableRaw.intentTargetClass = SupportDashboardActivity.class.getName();
                searchIndexableRaw.intentAction = "android.intent.action.MAIN";
                searchIndexableRaw.key = "support_dashboard_activity";
                list.add(searchIndexableRaw);
                return list;
            }
        };
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final SupportFeatureProvider supportFeatureProvider = FeatureFactory.getFactory((Context)this).getSupportFeatureProvider((Context)this);
        if (supportFeatureProvider != null) {
            supportFeatureProvider.startSupportV2(this);
            this.finish();
        }
    }
}
