package com.android.settings.support;

import android.os.Bundle;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.util.Log;
import com.android.settings.overlay.SupportFeatureProvider;
import android.net.Uri;
import android.text.TextUtils;
import com.android.settings.overlay.FeatureFactory;
import java.util.List;
import android.content.Intent;
import android.content.Context;
import android.app.Activity;

public class NewDeviceIntroSuggestionActivity extends Activity
{
    static final long PERMANENT_DISMISS_THRESHOLD = 1209600000L;
    static final String PREF_KEY_SUGGGESTION_COMPLETE = "pref_new_device_intro_suggestion_complete";
    static final String PREF_KEY_SUGGGESTION_FIRST_DISPLAY_TIME = "pref_new_device_intro_suggestion_first_display_time_ms";
    
    private static boolean canOpenUrlInBrowser(final Context context) {
        final Intent launchIntent = getLaunchIntent(context);
        final boolean b = false;
        if (launchIntent == null) {
            return false;
        }
        final List queryIntentActivities = context.getPackageManager().queryIntentActivities(launchIntent, 0);
        boolean b2 = b;
        if (queryIntentActivities != null) {
            b2 = b;
            if (queryIntentActivities.size() != 0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    static Intent getLaunchIntent(final Context context) {
        final SupportFeatureProvider supportFeatureProvider = FeatureFactory.getFactory(context).getSupportFeatureProvider(context);
        if (supportFeatureProvider == null) {
            return null;
        }
        final String newDeviceIntroUrl = supportFeatureProvider.getNewDeviceIntroUrl(context);
        if (TextUtils.isEmpty((CharSequence)newDeviceIntroUrl)) {
            return null;
        }
        return new Intent().setAction("android.intent.action.VIEW").addCategory("android.intent.category.BROWSABLE").setData(Uri.parse(newDeviceIntroUrl));
    }
    
    private static boolean hasLaunchedBefore(final Context context) {
        return FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).getSharedPrefs(context).getBoolean("pref_new_device_intro_suggestion_complete", false);
    }
    
    private static boolean isExpired(final Context context) {
        final SharedPreferences sharedPrefs = FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).getSharedPrefs(context);
        final long currentTimeMillis = System.currentTimeMillis();
        long long1;
        if (!sharedPrefs.contains("pref_new_device_intro_suggestion_first_display_time_ms")) {
            long1 = currentTimeMillis;
            sharedPrefs.edit().putLong("pref_new_device_intro_suggestion_first_display_time_ms", currentTimeMillis).commit();
        }
        else {
            long1 = sharedPrefs.getLong("pref_new_device_intro_suggestion_first_display_time_ms", -1L);
        }
        final boolean b = currentTimeMillis > 1209600000L + long1;
        final StringBuilder sb = new StringBuilder();
        sb.append("is suggestion expired: ");
        sb.append(b);
        Log.d("NewDeviceIntroSugg", sb.toString());
        return b;
    }
    
    public static boolean isSuggestionComplete(final Context context) {
        return isTipsInstalledAsSystemApp(context) || !isSupported(context) || isExpired(context) || hasLaunchedBefore(context) || !canOpenUrlInBrowser(context);
    }
    
    private static boolean isSupported(final Context context) {
        return context.getResources().getBoolean(2131034129);
    }
    
    private static boolean isTipsInstalledAsSystemApp(final Context context) {
        boolean b = false;
        try {
            if (context.getPackageManager().getPackageInfo("com.google.android.apps.tips", 1048576) != null) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.w("NewDeviceIntroSugg", "Cannot find the package: com.google.android.apps.tips", (Throwable)ex);
            return false;
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent launchIntent = getLaunchIntent((Context)this);
        if (launchIntent != null) {
            FeatureFactory.getFactory((Context)this).getSuggestionFeatureProvider((Context)this).getSharedPrefs((Context)this).edit().putBoolean("pref_new_device_intro_suggestion_complete", true).commit();
            this.startActivity(launchIntent);
        }
        this.finish();
    }
}
