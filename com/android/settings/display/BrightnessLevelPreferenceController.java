package com.android.settings.display;

import android.service.vr.IVrManager$Stub;
import android.os.ServiceManager;
import android.service.vr.IVrManager;
import android.os.RemoteException;
import android.util.Log;
import android.support.v7.preference.PreferenceScreen;
import java.text.NumberFormat;
import com.android.settingslib.display.BrightnessUtils;
import android.os.PowerManager;
import android.os.Handler;
import android.os.Looper;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BrightnessLevelPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart, OnStop
{
    private static final Uri BRIGHTNESS_ADJ_URI;
    private static final Uri BRIGHTNESS_FOR_VR_URI;
    private static final Uri BRIGHTNESS_URI;
    private ContentObserver mBrightnessObserver;
    private final ContentResolver mContentResolver;
    private final int mMaxBrightness;
    private final int mMaxVrBrightness;
    private final int mMinBrightness;
    private final int mMinVrBrightness;
    private Preference mPreference;
    
    static {
        BRIGHTNESS_URI = Settings.System.getUriFor("screen_brightness");
        BRIGHTNESS_FOR_VR_URI = Settings.System.getUriFor("screen_brightness_for_vr");
        BRIGHTNESS_ADJ_URI = Settings.System.getUriFor("screen_auto_brightness_adj");
    }
    
    public BrightnessLevelPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mBrightnessObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
            public void onChange(final boolean b) {
                BrightnessLevelPreferenceController.this.updatedSummary(BrightnessLevelPreferenceController.this.mPreference);
            }
        };
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
        final PowerManager powerManager = (PowerManager)context.getSystemService("power");
        this.mMinBrightness = powerManager.getMinimumScreenBrightnessSetting();
        this.mMaxBrightness = powerManager.getMaximumScreenBrightnessSetting();
        this.mMinVrBrightness = powerManager.getMinimumScreenBrightnessForVrSetting();
        this.mMaxVrBrightness = powerManager.getMaximumScreenBrightnessForVrSetting();
        this.mContentResolver = this.mContext.getContentResolver();
    }
    
    private double getCurrentBrightness() {
        int n;
        if (this.isInVrMode()) {
            n = BrightnessUtils.convertLinearToGamma(Settings.System.getInt(this.mContentResolver, "screen_brightness_for_vr", this.mMaxBrightness), this.mMinVrBrightness, this.mMaxVrBrightness);
        }
        else {
            n = BrightnessUtils.convertLinearToGamma(Settings.System.getInt(this.mContentResolver, "screen_brightness", this.mMinBrightness), this.mMinBrightness, this.mMaxBrightness);
        }
        return this.getPercentage(n, 0, 1023);
    }
    
    private double getPercentage(final double n, final int n2, final int n3) {
        if (n > n3) {
            return 1.0;
        }
        if (n < n2) {
            return 0.0;
        }
        return (n - n2) / (n3 - n2);
    }
    
    private void updatedSummary(final Preference preference) {
        if (preference != null) {
            preference.setSummary(NumberFormat.getPercentInstance().format(this.getCurrentBrightness()));
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference("brightness");
    }
    
    @Override
    public String getPreferenceKey() {
        return "brightness";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    boolean isInVrMode() {
        final IVrManager safeGetVrManager = this.safeGetVrManager();
        if (safeGetVrManager != null) {
            try {
                return safeGetVrManager.getVrModeState();
            }
            catch (RemoteException ex) {
                Log.e("BrightnessPrefCtrl", "Failed to check vr mode!", (Throwable)ex);
            }
        }
        return false;
    }
    
    @Override
    public void onStart() {
        this.mContentResolver.registerContentObserver(BrightnessLevelPreferenceController.BRIGHTNESS_URI, false, this.mBrightnessObserver);
        this.mContentResolver.registerContentObserver(BrightnessLevelPreferenceController.BRIGHTNESS_FOR_VR_URI, false, this.mBrightnessObserver);
        this.mContentResolver.registerContentObserver(BrightnessLevelPreferenceController.BRIGHTNESS_ADJ_URI, false, this.mBrightnessObserver);
    }
    
    @Override
    public void onStop() {
        this.mContentResolver.unregisterContentObserver(this.mBrightnessObserver);
    }
    
    IVrManager safeGetVrManager() {
        return IVrManager$Stub.asInterface(ServiceManager.getService("vrmanager"));
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updatedSummary(preference);
    }
}
