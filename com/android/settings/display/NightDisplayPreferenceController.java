package com.android.settings.display;

import com.android.internal.app.ColorDisplayController;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class NightDisplayPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public NightDisplayPreferenceController(final Context context) {
        super(context);
    }
    
    public static boolean isSuggestionComplete(final Context context) {
        final boolean boolean1 = context.getResources().getBoolean(2131034130);
        boolean b = true;
        if (!boolean1) {
            return true;
        }
        if (new ColorDisplayController(context).getAutoMode() == 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String getPreferenceKey() {
        return "night_display";
    }
    
    @Override
    public boolean isAvailable() {
        return ColorDisplayController.isAvailable(this.mContext);
    }
}
