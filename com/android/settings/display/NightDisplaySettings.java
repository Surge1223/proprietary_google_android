package com.android.settings.display;

import android.support.v7.preference.Preference;
import android.app.TimePickerDialog$OnTimeSetListener;
import android.app.TimePickerDialog;
import android.text.format.DateFormat;
import android.app.Dialog;
import android.os.Bundle;
import java.time.LocalTime;
import android.widget.TimePicker;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.internal.app.ColorDisplayController;
import com.android.settings.search.Indexable;
import com.android.internal.app.ColorDisplayController$Callback;
import com.android.settings.dashboard.DashboardFragment;

public class NightDisplaySettings extends DashboardFragment implements ColorDisplayController$Callback, Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private ColorDisplayController mController;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082794;
                list.add(searchIndexableResource);
                return list;
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return ColorDisplayController.isAvailable(context);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context) {
        final ArrayList<NightDisplayFooterPreferenceController> list = (ArrayList<NightDisplayFooterPreferenceController>)new ArrayList<AbstractPreferenceController>(1);
        list.add(new NightDisplayFooterPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context);
    }
    
    public int getDialogMetricsCategory(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 1: {
                return 589;
            }
            case 0: {
                return 588;
            }
        }
    }
    
    public int getHelpResource() {
        return 2131887822;
    }
    
    @Override
    protected String getLogTag() {
        return "NightDisplaySettings";
    }
    
    public int getMetricsCategory() {
        return 488;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082794;
    }
    
    public void onActivated(final boolean b) {
        this.updatePreferenceStates();
    }
    
    public void onAutoModeChanged(final int n) {
        this.updatePreferenceStates();
    }
    
    public void onColorTemperatureChanged(final int n) {
        this.updatePreferenceStates();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mController = new ColorDisplayController(this.getContext());
    }
    
    public Dialog onCreateDialog(final int n) {
        if (n != 0 && n != 1) {
            return super.onCreateDialog(n);
        }
        LocalTime localTime;
        if (n == 0) {
            localTime = this.mController.getCustomStartTime();
        }
        else {
            localTime = this.mController.getCustomEndTime();
        }
        final Context context = this.getContext();
        return (Dialog)new TimePickerDialog(context, (TimePickerDialog$OnTimeSetListener)new _$$Lambda$NightDisplaySettings$EHQrigX4B__bQ2Ww7B_DCA_KncQ(this, n), localTime.getHour(), localTime.getMinute(), DateFormat.is24HourFormat(context));
    }
    
    public void onCustomEndTimeChanged(final LocalTime localTime) {
        this.updatePreferenceStates();
    }
    
    public void onCustomStartTimeChanged(final LocalTime localTime) {
        this.updatePreferenceStates();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if ("night_display_end_time".equals(preference.getKey())) {
            this.showDialog(1);
            return true;
        }
        if ("night_display_start_time".equals(preference.getKey())) {
            this.showDialog(0);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.mController.setListener((ColorDisplayController$Callback)this);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.mController.setListener((ColorDisplayController$Callback)null);
    }
}
