package com.android.settings.display;

import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

public class MessageBubbleBackground extends LinearLayout
{
    private final int mSnapWidthPixels;
    
    public MessageBubbleBackground(final Context context, final AttributeSet set) {
        super(context, set);
        this.mSnapWidthPixels = context.getResources().getDimensionPixelSize(2131165349);
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        final int n3 = this.getPaddingLeft() + this.getPaddingRight();
        super.onMeasure(View$MeasureSpec.makeMeasureSpec(Math.min(View$MeasureSpec.getSize(n) - n3, (int)(Math.ceil((this.getMeasuredWidth() - n3) / this.mSnapWidthPixels) * this.mSnapWidthPixels)) + n3, 1073741824), n2);
    }
}
