package com.android.settings.display;

import android.support.v7.preference.Preference;
import android.app.TimePickerDialog;
import android.text.format.DateFormat;
import android.app.Dialog;
import android.os.Bundle;
import java.time.LocalTime;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.internal.app.ColorDisplayController;
import com.android.settings.search.Indexable;
import com.android.internal.app.ColorDisplayController$Callback;
import com.android.settings.dashboard.DashboardFragment;
import android.widget.TimePicker;
import android.app.TimePickerDialog$OnTimeSetListener;

public final class _$$Lambda$NightDisplaySettings$EHQrigX4B__bQ2Ww7B_DCA_KncQ implements TimePickerDialog$OnTimeSetListener
{
    public final void onTimeSet(final TimePicker timePicker, final int n, final int n2) {
        NightDisplaySettings.lambda$onCreateDialog$0(this.f$0, this.f$1, timePicker, n, n2);
    }
}
