package com.android.settings.display;

import android.support.v7.preference.Preference;
import java.util.List;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.Intent;
import android.content.ComponentName;
import android.text.TextUtils;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WallpaperPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final String mWallpaperClass;
    private final String mWallpaperPackage;
    
    public WallpaperPreferenceController(final Context context) {
        super(context);
        this.mWallpaperPackage = this.mContext.getString(2131887100);
        this.mWallpaperClass = this.mContext.getString(2131887099);
    }
    
    private void disablePreferenceIfManaged(final RestrictedPreference restrictedPreference) {
        if (restrictedPreference != null) {
            restrictedPreference.setDisabledByAdmin(null);
            if (RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_set_wallpaper", UserHandle.myUserId())) {
                restrictedPreference.setEnabled(false);
            }
            else {
                restrictedPreference.checkRestrictionAndSetDisabled("no_set_wallpaper");
            }
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "wallpaper";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean empty = TextUtils.isEmpty((CharSequence)this.mWallpaperPackage);
        final boolean b = false;
        if (!empty && !TextUtils.isEmpty((CharSequence)this.mWallpaperClass)) {
            final ComponentName component = new ComponentName(this.mWallpaperPackage, this.mWallpaperClass);
            final PackageManager packageManager = this.mContext.getPackageManager();
            final Intent intent = new Intent();
            intent.setComponent(component);
            final List queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            boolean b2 = b;
            if (queryIntentActivities != null) {
                b2 = b;
                if (queryIntentActivities.size() != 0) {
                    b2 = true;
                }
            }
            return b2;
        }
        Log.e("WallpaperPrefController", "No Wallpaper picker specified!");
        return false;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.disablePreferenceIfManaged((RestrictedPreference)preference);
    }
}
