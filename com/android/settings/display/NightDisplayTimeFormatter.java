package com.android.settings.display;

import java.util.Calendar;
import java.time.LocalTime;
import com.android.internal.app.ColorDisplayController;
import java.util.TimeZone;
import android.content.Context;
import java.text.DateFormat;

public class NightDisplayTimeFormatter
{
    private DateFormat mTimeFormatter;
    
    NightDisplayTimeFormatter(final Context context) {
        (this.mTimeFormatter = android.text.format.DateFormat.getTimeFormat(context)).setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    
    private String getAutoModeSummary(final Context context, final ColorDisplayController colorDisplayController) {
        final boolean activated = colorDisplayController.isActivated();
        final int autoMode = colorDisplayController.getAutoMode();
        if (autoMode == 1) {
            if (activated) {
                return context.getString(2131888400, new Object[] { this.getFormattedTimeString(colorDisplayController.getCustomEndTime()) });
            }
            return context.getString(2131888396, new Object[] { this.getFormattedTimeString(colorDisplayController.getCustomStartTime()) });
        }
        else {
            if (autoMode == 2) {
                int n;
                if (activated) {
                    n = 2131888402;
                }
                else {
                    n = 2131888398;
                }
                return context.getString(n);
            }
            int n2;
            if (activated) {
                n2 = 2131888401;
            }
            else {
                n2 = 2131888397;
            }
            return context.getString(n2);
        }
    }
    
    public String getAutoModeTimeSummary(final Context context, final ColorDisplayController colorDisplayController) {
        int n;
        if (colorDisplayController.isActivated()) {
            n = 2131888399;
        }
        else {
            n = 2131888395;
        }
        return context.getString(n, new Object[] { this.getAutoModeSummary(context, colorDisplayController) });
    }
    
    public String getFormattedTimeString(final LocalTime localTime) {
        final Calendar instance = Calendar.getInstance();
        instance.setTimeZone(this.mTimeFormatter.getTimeZone());
        instance.set(11, localTime.getHour());
        instance.set(12, localTime.getMinute());
        instance.set(13, 0);
        instance.set(14, 0);
        return this.mTimeFormatter.format(instance.getTime());
    }
}
