package com.android.settings.display;

import android.content.res.Resources;
import com.android.settings.accessibility.ToggleFontSizePreferenceFragment;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class FontSizePreferenceController extends BasePreferenceController
{
    public FontSizePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public CharSequence getSummary() {
        final float float1 = Settings.System.getFloat(this.mContext.getContentResolver(), "font_scale", 1.0f);
        final Resources resources = this.mContext.getResources();
        return resources.getStringArray(2130903118)[ToggleFontSizePreferenceFragment.fontSizeValueToIndex(float1, resources.getStringArray(2130903119))];
    }
}
