package com.android.settings.display;

import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.DropDownPreference;
import com.android.internal.app.ColorDisplayController;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class NightDisplayAutoModePreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    private ColorDisplayController mController;
    private DropDownPreference mPreference;
    
    public NightDisplayAutoModePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mController = new ColorDisplayController(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = (DropDownPreference)preferenceScreen.findPreference(this.getPreferenceKey())).setEntries(new CharSequence[] { this.mContext.getString(2131888387), this.mContext.getString(2131888386), this.mContext.getString(2131888389) });
        this.mPreference.setEntryValues(new CharSequence[] { String.valueOf(0), String.valueOf(1), String.valueOf(2) });
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (ColorDisplayController.isAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return this.mController.setAutoMode(Integer.parseInt((String)o));
    }
    
    @Override
    public final void updateState(final Preference preference) {
        this.mPreference.setValue(String.valueOf(this.mController.getAutoMode()));
    }
}
