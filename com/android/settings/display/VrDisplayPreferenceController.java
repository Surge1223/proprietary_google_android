package com.android.settings.display;

import android.provider.Settings;
import android.app.ActivityManager;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class VrDisplayPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public VrDisplayPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "vr_display_pref";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getPackageManager().hasSystemFeature("android.hardware.vr.high_performance");
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (Settings.Secure.getIntForUser(this.mContext.getContentResolver(), "vr_display_mode", 0, ActivityManager.getCurrentUser()) == 0) {
            preference.setSummary(2131887451);
        }
        else {
            preference.setSummary(2131887452);
        }
    }
}
