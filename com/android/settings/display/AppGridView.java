package com.android.settings.display;

import android.widget.ImageView;
import android.view.ViewGroup;
import android.view.View;
import java.util.Iterator;
import java.util.Collection;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import android.net.Uri;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.ArrayAdapter;
import android.content.pm.PackageItemInfo;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.util.IconDrawableFactory;
import android.content.pm.ResolveInfo;
import android.widget.ListAdapter;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.GridView;

public class AppGridView extends GridView
{
    public AppGridView(final Context context) {
        this(context, null);
    }
    
    public AppGridView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AppGridView(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public AppGridView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setNumColumns(-1);
        this.setColumnWidth(this.getResources().getDimensionPixelSize(2131165537));
        this.setAdapter((ListAdapter)new AppsAdapter(context, 2131558730, 16908308, 16908295));
    }
    
    public static class ActivityEntry implements Comparable<ActivityEntry>
    {
        public final ResolveInfo info;
        public final String label;
        private final IconDrawableFactory mIconFactory;
        private final int mUserId;
        
        public ActivityEntry(final ResolveInfo info, final String label, final IconDrawableFactory mIconFactory) {
            this.info = info;
            this.label = label;
            this.mIconFactory = mIconFactory;
            this.mUserId = UserHandle.myUserId();
        }
        
        @Override
        public int compareTo(final ActivityEntry activityEntry) {
            return this.label.compareToIgnoreCase(activityEntry.label);
        }
        
        public Drawable getIcon() {
            return this.mIconFactory.getBadgedIcon((PackageItemInfo)this.info.activityInfo, this.info.activityInfo.applicationInfo, this.mUserId);
        }
        
        @Override
        public String toString() {
            return this.label;
        }
    }
    
    public static class AppsAdapter extends ArrayAdapter<ActivityEntry>
    {
        private final int mIconResId;
        private final PackageManager mPackageManager;
        
        public AppsAdapter(final Context context, final int n, final int n2, final int mIconResId) {
            super(context, n, n2);
            this.mIconResId = mIconResId;
            this.mPackageManager = context.getPackageManager();
            this.loadAllApps();
        }
        
        private void loadAllApps() {
            final Intent intent = new Intent("android.intent.action.MAIN", (Uri)null);
            intent.addCategory("android.intent.category.LAUNCHER");
            final PackageManager mPackageManager = this.mPackageManager;
            final ArrayList<ActivityEntry> list = (ArrayList<ActivityEntry>)new ArrayList<Comparable>();
            final List queryIntentActivities = mPackageManager.queryIntentActivities(intent, 0);
            final IconDrawableFactory instance = IconDrawableFactory.newInstance(this.getContext());
            for (final ResolveInfo resolveInfo : queryIntentActivities) {
                final CharSequence loadLabel = resolveInfo.loadLabel(mPackageManager);
                if (loadLabel != null) {
                    list.add(new ActivityEntry(resolveInfo, loadLabel.toString(), instance));
                }
            }
            Collections.sort((List<Comparable>)list);
            this.addAll((Collection)list);
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final View view2 = super.getView(n, view, viewGroup);
            ((ImageView)view2.findViewById(this.mIconResId)).setImageDrawable(((ActivityEntry)this.getItem(n)).getIcon());
            return view2;
        }
        
        public boolean hasStableIds() {
            return true;
        }
        
        public boolean isEnabled(final int n) {
            return false;
        }
    }
}
