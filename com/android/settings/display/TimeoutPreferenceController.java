package com.android.settings.display;

import com.android.settingslib.RestrictedLockUtils;
import android.content.ComponentName;
import android.os.UserHandle;
import android.app.admin.DevicePolicyManager;
import android.util.Log;
import android.provider.Settings;
import com.android.settings.TimeoutListPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class TimeoutPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final String mScreenTimeoutKey;
    
    public TimeoutPreferenceController(final Context context, final String mScreenTimeoutKey) {
        super(context);
        this.mScreenTimeoutKey = mScreenTimeoutKey;
    }
    
    public static CharSequence getTimeoutDescription(final long n, final CharSequence[] array, final CharSequence[] array2) {
        if (n >= 0L && array != null && array2 != null && array2.length == array.length) {
            for (int i = 0; i < array2.length; ++i) {
                if (n == Long.parseLong(array2[i].toString())) {
                    return array[i];
                }
            }
            return null;
        }
        return null;
    }
    
    private void updateTimeoutPreferenceDescription(final TimeoutListPreference timeoutListPreference, final long n) {
        final CharSequence[] entries = timeoutListPreference.getEntries();
        final CharSequence[] entryValues = timeoutListPreference.getEntryValues();
        String summary;
        if (timeoutListPreference.isDisabledByAdmin()) {
            summary = this.mContext.getString(2131887427);
        }
        else {
            final CharSequence timeoutDescription = getTimeoutDescription(n, entries, entryValues);
            if (timeoutDescription == null) {
                summary = "";
            }
            else {
                summary = this.mContext.getString(2131888903, new Object[] { timeoutDescription });
            }
        }
        timeoutListPreference.setSummary(summary);
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mScreenTimeoutKey;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        try {
            final int int1 = Integer.parseInt((String)o);
            Settings.System.putInt(this.mContext.getContentResolver(), "screen_off_timeout", int1);
            this.updateTimeoutPreferenceDescription((TimeoutListPreference)preference, int1);
        }
        catch (NumberFormatException ex) {
            Log.e("TimeoutPrefContr", "could not persist screen timeout setting", (Throwable)ex);
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final TimeoutListPreference timeoutListPreference = (TimeoutListPreference)preference;
        final long long1 = Settings.System.getLong(this.mContext.getContentResolver(), "screen_off_timeout", 30000L);
        timeoutListPreference.setValue(String.valueOf(long1));
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)this.mContext.getSystemService("device_policy");
        if (devicePolicyManager != null) {
            timeoutListPreference.removeUnusableTimeouts(devicePolicyManager.getMaximumTimeToLock((ComponentName)null, UserHandle.myUserId()), RestrictedLockUtils.checkIfMaximumTimeToLockIsSet(this.mContext));
        }
        this.updateTimeoutPreferenceDescription(timeoutListPreference, long1);
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_config_screen_timeout", UserHandle.myUserId());
        if (checkIfRestrictionEnforced != null) {
            timeoutListPreference.removeUnusableTimeouts(0L, checkIfRestrictionEnforced);
        }
    }
}
