package com.android.settings.display;

import android.support.v7.preference.Preference;
import android.text.TextUtils;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import java.time.LocalTime;
import android.view.View;
import android.content.Context;
import android.widget.Button;
import android.view.View.OnClickListener;
import com.android.internal.app.ColorDisplayController;
import com.android.settings.core.TogglePreferenceController;

public class NightDisplayActivationPreferenceController extends TogglePreferenceController
{
    private ColorDisplayController mController;
    private final View.OnClickListener mListener;
    private NightDisplayTimeFormatter mTimeFormatter;
    private Button mTurnOffButton;
    private Button mTurnOnButton;
    
    public NightDisplayActivationPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                NightDisplayActivationPreferenceController.this.mController.setActivated(NightDisplayActivationPreferenceController.this.mController.isActivated() ^ true);
                NightDisplayActivationPreferenceController.this.updateStateInternal();
            }
        };
        this.mController = new ColorDisplayController(context);
        this.mTimeFormatter = new NightDisplayTimeFormatter(context);
    }
    
    private void updateStateInternal() {
        if (this.mTurnOnButton != null && this.mTurnOffButton != null) {
            final boolean activated = this.mController.isActivated();
            final int autoMode = this.mController.getAutoMode();
            String s;
            if (autoMode == 1) {
                final Context mContext = this.mContext;
                int n;
                if (activated) {
                    n = 2131888380;
                }
                else {
                    n = 2131888383;
                }
                final NightDisplayTimeFormatter mTimeFormatter = this.mTimeFormatter;
                LocalTime localTime;
                if (activated) {
                    localTime = this.mController.getCustomStartTime();
                }
                else {
                    localTime = this.mController.getCustomEndTime();
                }
                s = mContext.getString(n, new Object[] { mTimeFormatter.getFormattedTimeString(localTime) });
            }
            else if (autoMode == 2) {
                final Context mContext2 = this.mContext;
                int n2;
                if (activated) {
                    n2 = 2131888382;
                }
                else {
                    n2 = 2131888385;
                }
                s = mContext2.getString(n2);
            }
            else {
                final Context mContext3 = this.mContext;
                int n3;
                if (activated) {
                    n3 = 2131888381;
                }
                else {
                    n3 = 2131888384;
                }
                s = mContext3.getString(n3);
            }
            if (activated) {
                this.mTurnOnButton.setVisibility(8);
                this.mTurnOffButton.setVisibility(0);
                this.mTurnOffButton.setText((CharSequence)s);
            }
            else {
                this.mTurnOnButton.setVisibility(0);
                this.mTurnOffButton.setVisibility(8);
                this.mTurnOnButton.setText((CharSequence)s);
            }
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final LayoutPreference layoutPreference = (LayoutPreference)preferenceScreen.findPreference(this.getPreferenceKey());
        (this.mTurnOnButton = (Button)layoutPreference.findViewById(2131362399)).setOnClickListener(this.mListener);
        (this.mTurnOffButton = (Button)layoutPreference.findViewById(2131362398)).setOnClickListener(this.mListener);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (ColorDisplayController.isAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mTimeFormatter.getAutoModeTimeSummary(this.mContext, this.mController);
    }
    
    @Override
    public boolean isChecked() {
        return this.mController.isActivated();
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"night_display_activated");
    }
    
    @Override
    public boolean setChecked(final boolean activated) {
        return this.mController.setActivated(activated);
    }
    
    @Override
    public final void updateState(final Preference preference) {
        this.updateStateInternal();
    }
}
