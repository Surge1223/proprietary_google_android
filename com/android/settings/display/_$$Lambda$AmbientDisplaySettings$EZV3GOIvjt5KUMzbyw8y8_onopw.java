package com.android.settings.display;

import com.android.settings.gestures.PickupGesturePreferenceController;
import com.android.settings.gestures.DoubleTapScreenPreferenceController;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public final class _$$Lambda$AmbientDisplaySettings$EZV3GOIvjt5KUMzbyw8y8_onopw implements OnPreferenceChangedCallback
{
    @Override
    public final void onPreferenceChanged() {
        this.f$0.updatePreferenceStates();
    }
}
