package com.android.settings.display;

import android.os.Bundle;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class AutoBrightnessSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082718;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    public int getHelpResource() {
        return 2131887796;
    }
    
    @Override
    protected String getLogTag() {
        return "AutoBrightnessSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1381;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082718;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131886468);
    }
}
