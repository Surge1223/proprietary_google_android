package com.android.settings.display;

import android.provider.Settings;
import android.text.TextUtils;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.content.Context;
import android.os.UserHandle;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.core.TogglePreferenceController;

public class AmbientDisplayAlwaysOnPreferenceController extends TogglePreferenceController
{
    private static final int MY_USER;
    private final int OFF;
    private final int ON;
    private OnPreferenceChangedCallback mCallback;
    private AmbientDisplayConfiguration mConfig;
    
    static {
        MY_USER = UserHandle.myUserId();
    }
    
    public AmbientDisplayAlwaysOnPreferenceController(final Context context, final String s) {
        super(context, s);
        this.ON = 1;
        this.OFF = 0;
    }
    
    public static boolean accessibilityInversionEnabled(final AmbientDisplayConfiguration ambientDisplayConfiguration) {
        return ambientDisplayConfiguration.accessibilityInversionEnabled(AmbientDisplayAlwaysOnPreferenceController.MY_USER);
    }
    
    public static boolean isAlwaysOnEnabled(final AmbientDisplayConfiguration ambientDisplayConfiguration) {
        return ambientDisplayConfiguration.alwaysOnEnabled(AmbientDisplayAlwaysOnPreferenceController.MY_USER);
    }
    
    public static boolean isAvailable(final AmbientDisplayConfiguration ambientDisplayConfiguration) {
        return ambientDisplayConfiguration.alwaysOnAvailableForUser(AmbientDisplayAlwaysOnPreferenceController.MY_USER);
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mConfig == null) {
            this.mConfig = new AmbientDisplayConfiguration(this.mContext);
        }
        int n;
        if (isAvailable(this.mConfig)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("doze_always_on", 2, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, AmbientDisplaySettings.class.getName(), this.getPreferenceKey(), this.mContext.getString(2131886304)), this.isAvailable(), 1);
    }
    
    @Override
    public boolean isChecked() {
        return this.mConfig.alwaysOnEnabled(AmbientDisplayAlwaysOnPreferenceController.MY_USER);
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"ambient_display_always_on");
    }
    
    public AmbientDisplayAlwaysOnPreferenceController setCallback(final OnPreferenceChangedCallback mCallback) {
        this.mCallback = mCallback;
        return this;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "doze_always_on", (int)(b ? 1 : 0));
        if (this.mCallback != null) {
            this.mCallback.onPreferenceChanged();
        }
        return true;
    }
    
    public AmbientDisplayAlwaysOnPreferenceController setConfig(final AmbientDisplayConfiguration mConfig) {
        this.mConfig = mConfig;
        return this;
    }
    
    public interface OnPreferenceChangedCallback
    {
        void onPreferenceChanged();
    }
}
