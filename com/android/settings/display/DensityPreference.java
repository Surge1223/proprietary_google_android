package com.android.settings.display;

import android.util.Slog;
import com.android.settingslib.display.DisplayDensityUtils;
import com.android.settings.Utils;
import android.widget.EditText;
import android.view.View;
import java.text.NumberFormat;
import android.text.BidiFormatter;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.CustomEditTextPreference;

public class DensityPreference extends CustomEditTextPreference
{
    public DensityPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    private int getCurrentSwDp() {
        final DisplayMetrics displayMetrics = this.getContext().getResources().getDisplayMetrics();
        return (int)(Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels) / displayMetrics.density);
    }
    
    @Override
    public void onAttached() {
        super.onAttached();
        this.setSummary(this.getContext().getString(2131887369, new Object[] { BidiFormatter.getInstance().unicodeWrap(NumberFormat.getInstance().format(this.getCurrentSwDp())) }));
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        final EditText editTextCursorPosition = (EditText)view.findViewById(16908291);
        if (editTextCursorPosition != null) {
            editTextCursorPosition.setInputType(2);
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getCurrentSwDp());
            sb.append("");
            editTextCursorPosition.setText((CharSequence)sb.toString());
            Utils.setEditTextCursorPosition(editTextCursorPosition);
        }
    }
    
    @Override
    protected void onDialogClosed(final boolean b) {
        if (b) {
            try {
                final DisplayMetrics displayMetrics = this.getContext().getResources().getDisplayMetrics();
                DisplayDensityUtils.setForcedDisplayDensity(0, Math.max(160 * Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels) / Math.max(Integer.parseInt(this.getText()), 320), 120));
            }
            catch (Exception ex) {
                Slog.e("DensityPreference", "Couldn't save density", (Throwable)ex);
            }
        }
    }
}
