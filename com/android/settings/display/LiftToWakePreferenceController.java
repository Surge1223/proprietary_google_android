package com.android.settings.display;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.hardware.SensorManager;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class LiftToWakePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public LiftToWakePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "lift_to_wake";
    }
    
    @Override
    public boolean isAvailable() {
        final SensorManager sensorManager = (SensorManager)this.mContext.getSystemService("sensor");
        return sensorManager != null && sensorManager.getDefaultSensor(23) != null;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "wake_gesture_enabled", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Secure.getInt(contentResolver, "wake_gesture_enabled", 0);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        if (int1 != 0) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
