package com.android.settings.display;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class BatteryPercentagePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public BatteryPercentagePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "battery_percentage";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(17956898);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.System.putInt(this.mContext.getContentResolver(), "status_bar_show_battery_percent", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.System.getInt(contentResolver, "status_bar_show_battery_percent", 0);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        if (int1 == 1) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
