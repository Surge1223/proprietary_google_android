package com.android.settings.display;

public class ScreenZoomPreferenceFragmentForSetupWizard extends ScreenZoomSettings
{
    @Override
    public int getMetricsCategory() {
        return 370;
    }
    
    @Override
    public void onStop() {
        if (this.mCurrentIndex != this.mInitialIndex) {
            this.mMetricsFeatureProvider.action(this.getContext(), 370, this.mCurrentIndex);
        }
        super.onStop();
    }
}
