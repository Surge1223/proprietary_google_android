package com.android.settings.display;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.UserHandle;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import android.support.v7.preference.Preference;
import com.android.settings.core.TogglePreferenceController;

public class AmbientDisplayNotificationsPreferenceController extends TogglePreferenceController implements OnPreferenceChangeListener
{
    static final String KEY_AMBIENT_DISPLAY_NOTIFICATIONS = "ambient_display_notification";
    private static final int MY_USER;
    private final int OFF;
    private final int ON;
    private AmbientDisplayConfiguration mConfig;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    
    static {
        MY_USER = UserHandle.myUserId();
    }
    
    public AmbientDisplayNotificationsPreferenceController(final Context context, final String s) {
        super(context, s);
        this.ON = 1;
        this.OFF = 0;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mConfig == null) {
            this.mConfig = new AmbientDisplayConfiguration(this.mContext);
        }
        int n;
        if (this.mConfig.pulseOnNotificationAvailable()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("doze_enabled", 2, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, AmbientDisplaySettings.class.getName(), "ambient_display_notification", this.mContext.getString(2131886304)), this.isAvailable(), 1);
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("ambient_display_notification".equals(preference.getKey())) {
            this.mMetricsFeatureProvider.action(this.mContext, 495, (Pair<Integer, Object>[])new Pair[0]);
        }
        return false;
    }
    
    @Override
    public boolean isChecked() {
        return this.mConfig.pulseOnNotificationEnabled(AmbientDisplayNotificationsPreferenceController.MY_USER);
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"ambient_display_notification");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "doze_enabled", (int)(b ? 1 : 0));
        return true;
    }
    
    public AmbientDisplayNotificationsPreferenceController setConfig(final AmbientDisplayConfiguration mConfig) {
        this.mConfig = mConfig;
        return this;
    }
}
