package com.android.settings.display;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.internal.app.ColorDisplayController;
import com.android.settings.core.BasePreferenceController;

public class NightDisplayCustomEndTimePreferenceController extends BasePreferenceController
{
    private ColorDisplayController mController;
    private NightDisplayTimeFormatter mTimeFormatter;
    
    public NightDisplayCustomEndTimePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mController = new ColorDisplayController(context);
        this.mTimeFormatter = new NightDisplayTimeFormatter(context);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (ColorDisplayController.isAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public final void updateState(final Preference preference) {
        final int autoMode = this.mController.getAutoMode();
        boolean visible = true;
        if (autoMode != 1) {
            visible = false;
        }
        preference.setVisible(visible);
        preference.setSummary(this.mTimeFormatter.getFormattedTimeString(this.mController.getCustomEndTime()));
    }
}
