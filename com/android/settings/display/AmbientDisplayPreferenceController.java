package com.android.settings.display;

import android.support.v7.preference.Preference;
import android.content.Context;
import android.os.UserHandle;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AmbientDisplayPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private static final int MY_USER_ID;
    private final AmbientDisplayConfiguration mConfig;
    private final String mKey;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public AmbientDisplayPreferenceController(final Context context, final AmbientDisplayConfiguration mConfig, final String mKey) {
        super(context);
        this.mConfig = mConfig;
        this.mKey = mKey;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mKey;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mConfig.available();
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (this.mConfig.alwaysOnEnabled(AmbientDisplayPreferenceController.MY_USER_ID)) {
            preference.setSummary(2131886302);
        }
        else if (this.mConfig.pulseOnNotificationEnabled(AmbientDisplayPreferenceController.MY_USER_ID)) {
            preference.setSummary(2131886303);
        }
        else if (this.mConfig.enabled(AmbientDisplayPreferenceController.MY_USER_ID)) {
            preference.setSummary(2131889422);
        }
        else {
            preference.setSummary(2131889421);
        }
    }
}
