package com.android.settings.display;

import android.support.v7.preference.Preference;
import com.android.internal.app.ColorDisplayController;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class NightDisplayFooterPreferenceController extends BasePreferenceController
{
    public NightDisplayFooterPreferenceController(final Context context) {
        super(context, "footer_preference");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (ColorDisplayController.isAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setTitle(2131888404);
    }
}
