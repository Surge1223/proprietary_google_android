package com.android.settings.display;

import android.view.View$MeasureSpec;
import android.content.res.Resources;
import android.view.View;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout;

public class ConversationMessageView extends FrameLayout
{
    private TextView mContactIconView;
    private final int mIconBackgroundColor;
    private final CharSequence mIconText;
    private final int mIconTextColor;
    private final boolean mIncoming;
    private LinearLayout mMessageBubble;
    private final CharSequence mMessageText;
    private ViewGroup mMessageTextAndInfoView;
    private TextView mMessageTextView;
    private TextView mStatusTextView;
    private final CharSequence mTimestampText;
    
    public ConversationMessageView(final Context context) {
        this(context, null);
    }
    
    public ConversationMessageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ConversationMessageView(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public ConversationMessageView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ConversationMessageView);
        this.mIncoming = obtainStyledAttributes.getBoolean(3, true);
        this.mMessageText = obtainStyledAttributes.getString(4);
        this.mTimestampText = obtainStyledAttributes.getString(5);
        this.mIconText = obtainStyledAttributes.getString(1);
        this.mIconTextColor = obtainStyledAttributes.getColor(2, 0);
        this.mIconBackgroundColor = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        LayoutInflater.from(context).inflate(2131558505, (ViewGroup)this);
        LayoutInflater.from(context).inflate(2131558504, (ViewGroup)this);
    }
    
    private static Drawable getTintedDrawable(final Context context, final Drawable drawable, final int n) {
        final Drawable$ConstantState constantState = drawable.getConstantState();
        Drawable mutate;
        if (constantState != null) {
            mutate = constantState.newDrawable(context.getResources()).mutate();
        }
        else {
            mutate = drawable;
        }
        mutate.setColorFilter(n, PorterDuff.Mode.SRC_ATOP);
        return mutate;
    }
    
    private static boolean isLayoutRtl(final View view) {
        final int layoutDirection = view.getLayoutDirection();
        boolean b = true;
        if (layoutDirection == 0) {
            b = false;
        }
        return b;
    }
    
    private void updateTextAppearance() {
        int n;
        if (this.mIncoming) {
            n = 2131099810;
        }
        else {
            n = 2131099811;
        }
        int textColor;
        if (this.mIncoming) {
            textColor = 2131099880;
        }
        else {
            textColor = 2131099881;
        }
        final int color = this.getContext().getColor(n);
        this.mMessageTextView.setTextColor(color);
        this.mMessageTextView.setLinkTextColor(color);
        this.mStatusTextView.setTextColor(textColor);
    }
    
    private void updateViewAppearance() {
        final Resources resources = this.getResources();
        final int dimensionPixelOffset = resources.getDimensionPixelOffset(2131165481);
        int dimensionPixelOffset2 = resources.getDimensionPixelOffset(2131165485);
        final int dimensionPixelOffset3 = resources.getDimensionPixelOffset(2131165486);
        final int dimensionPixelOffset4 = resources.getDimensionPixelOffset(2131165484);
        int n;
        if (this.mIncoming) {
            n = dimensionPixelOffset2 + dimensionPixelOffset;
        }
        else {
            n = dimensionPixelOffset2;
            dimensionPixelOffset2 += dimensionPixelOffset;
        }
        int gravity;
        if (this.mIncoming) {
            gravity = 8388627;
        }
        else {
            gravity = 8388629;
        }
        final int dimensionPixelSize = resources.getDimensionPixelSize(2131165483);
        final int dimensionPixelOffset5 = resources.getDimensionPixelOffset(2131165482);
        int n2;
        if (this.mIncoming) {
            n2 = 2131231230;
        }
        else {
            n2 = 2131231231;
        }
        int n3;
        if (this.mIncoming) {
            n3 = 2131099804;
        }
        else {
            n3 = 2131099805;
        }
        final Context context = this.getContext();
        this.mMessageTextAndInfoView.setBackground(getTintedDrawable(context, context.getDrawable(n2), context.getColor(n3)));
        if (isLayoutRtl((View)this)) {
            this.mMessageTextAndInfoView.setPadding(dimensionPixelOffset2, dimensionPixelOffset3 + dimensionPixelOffset5, n, dimensionPixelOffset4);
        }
        else {
            this.mMessageTextAndInfoView.setPadding(n, dimensionPixelOffset3 + dimensionPixelOffset5, dimensionPixelOffset2, dimensionPixelOffset4);
        }
        this.setPadding(this.getPaddingLeft(), dimensionPixelSize, this.getPaddingRight(), 0);
        this.mMessageBubble.setGravity(gravity);
        this.updateTextAppearance();
    }
    
    private void updateViewContent() {
        this.mMessageTextView.setText(this.mMessageText);
        this.mStatusTextView.setText(this.mTimestampText);
        this.mContactIconView.setText(this.mIconText);
        this.mContactIconView.setTextColor(this.mIconTextColor);
        this.mContactIconView.setBackground(getTintedDrawable(this.getContext(), this.getContext().getDrawable(2131230902), this.mIconBackgroundColor));
    }
    
    protected void onFinishInflate() {
        this.mMessageBubble = (LinearLayout)this.findViewById(2131362364);
        this.mMessageTextAndInfoView = (ViewGroup)this.findViewById(2131362367);
        this.mMessageTextView = (TextView)this.findViewById(2131362366);
        this.mStatusTextView = (TextView)this.findViewById(2131362365);
        this.mContactIconView = (TextView)this.findViewById(2131362022);
        this.updateViewContent();
    }
    
    protected void onLayout(final boolean b, int n, int n2, final int n3, int measuredHeight) {
        final boolean layoutRtl = isLayoutRtl((View)this);
        final int measuredWidth = this.mContactIconView.getMeasuredWidth();
        measuredHeight = this.mContactIconView.getMeasuredHeight();
        final int paddingTop = this.getPaddingTop();
        final int n4 = n3 - n - measuredWidth - this.getPaddingLeft() - this.getPaddingRight();
        final int measuredHeight2 = this.mMessageBubble.getMeasuredHeight();
        if (this.mIncoming) {
            if (layoutRtl) {
                n2 = n3 - n - this.getPaddingRight() - measuredWidth;
                n = n2 - n4;
            }
            else {
                n2 = this.getPaddingLeft();
                n = n2 + measuredWidth;
            }
        }
        else if (layoutRtl) {
            n2 = this.getPaddingLeft();
            n = n2 + measuredWidth;
        }
        else {
            n2 = n3 - n - this.getPaddingRight() - measuredWidth;
            n = n2 - n4;
        }
        this.mContactIconView.layout(n2, paddingTop, n2 + measuredWidth, paddingTop + measuredHeight);
        this.mMessageBubble.layout(n, paddingTop, n + n4, paddingTop + measuredHeight2);
    }
    
    protected void onMeasure(int size, int n) {
        this.updateViewAppearance();
        size = View$MeasureSpec.getSize(size);
        n = View$MeasureSpec.makeMeasureSpec(0, 0);
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
        this.mContactIconView.measure(measureSpec, measureSpec);
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(Math.max(this.mContactIconView.getMeasuredWidth(), this.mContactIconView.getMeasuredHeight()), 1073741824);
        this.mContactIconView.measure(measureSpec2, measureSpec2);
        this.mMessageBubble.measure(View$MeasureSpec.makeMeasureSpec(size - this.mContactIconView.getMeasuredWidth() * 2 - this.getResources().getDimensionPixelSize(2131165481) - this.getPaddingLeft() - this.getPaddingRight(), Integer.MIN_VALUE), n);
        n = Math.max(this.mContactIconView.getMeasuredHeight(), this.mMessageBubble.getMeasuredHeight());
        this.setMeasuredDimension(size, this.getPaddingBottom() + n + this.getPaddingTop());
    }
}
