package com.android.settings.display;

import android.text.TextUtils;
import com.android.settingslib.display.DisplayDensityUtils;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ScreenZoomPreference extends Preference
{
    public ScreenZoomPreference(final Context context, final AttributeSet set) {
        super(context, set, TypedArrayUtils.getAttr(context, R.attr.preferenceStyle, 16842894));
        final DisplayDensityUtils displayDensityUtils = new DisplayDensityUtils(context);
        if (displayDensityUtils.getCurrentIndex() < 0) {
            this.setVisible(false);
            this.setEnabled(false);
        }
        else if (TextUtils.isEmpty(this.getSummary())) {
            this.setSummary(displayDensityUtils.getEntries()[displayDensityUtils.getCurrentIndex()]);
        }
    }
}
