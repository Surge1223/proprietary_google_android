package com.android.settings.display;

import android.os.Bundle;
import android.content.res.Configuration;
import com.android.settingslib.display.DisplayDensityUtils;
import android.content.res.Resources;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.PreviewSeekBarPreferenceFragment;

public class ScreenZoomSettings extends PreviewSeekBarPreferenceFragment implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private int mDefaultDensity;
    private int[] mValues;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final Resources resources = context.getResources();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.title = resources.getString(2131888928);
                searchIndexableRaw.key = "screen_zoom_settings";
                searchIndexableRaw.screenTitle = resources.getString(2131888928);
                searchIndexableRaw.keywords = resources.getString(2131888916);
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>(1);
                list.add(searchIndexableRaw);
                return list;
            }
        };
    }
    
    @Override
    protected void commit() {
        final int n = this.mValues[this.mCurrentIndex];
        if (n == this.mDefaultDensity) {
            DisplayDensityUtils.clearForcedDisplayDensity(0);
        }
        else {
            DisplayDensityUtils.setForcedDisplayDensity(0, n);
        }
    }
    
    @Override
    protected Configuration createConfig(Configuration configuration, final int n) {
        configuration = new Configuration(configuration);
        configuration.densityDpi = this.mValues[n];
        return configuration;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887807;
    }
    
    @Override
    public int getMetricsCategory() {
        return 339;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mActivityLayoutResId = 2131558727;
        this.mPreviewSampleResIds = new int[] { 2131558728, 2131558729, 2131558731 };
        final DisplayDensityUtils displayDensityUtils = new DisplayDensityUtils(this.getContext());
        final int currentIndex = displayDensityUtils.getCurrentIndex();
        if (currentIndex < 0) {
            final int densityDpi = this.getResources().getDisplayMetrics().densityDpi;
            this.mValues = new int[] { densityDpi };
            this.mEntries = new String[] { this.getString(DisplayDensityUtils.SUMMARY_DEFAULT) };
            this.mInitialIndex = 0;
            this.mDefaultDensity = densityDpi;
        }
        else {
            this.mValues = displayDensityUtils.getValues();
            this.mEntries = displayDensityUtils.getEntries();
            this.mInitialIndex = currentIndex;
            this.mDefaultDensity = displayDensityUtils.getDefaultDensity();
        }
        this.getActivity().setTitle(2131888928);
    }
}
