package com.android.settings.display;

import java.time.LocalTime;
import android.util.AttributeSet;
import android.content.Context;
import com.android.internal.app.ColorDisplayController;
import com.android.internal.app.ColorDisplayController$Callback;
import android.support.v14.preference.SwitchPreference;

public class NightDisplayPreference extends SwitchPreference implements ColorDisplayController$Callback
{
    private ColorDisplayController mController;
    private NightDisplayTimeFormatter mTimeFormatter;
    
    public NightDisplayPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mController = new ColorDisplayController(context);
        this.mTimeFormatter = new NightDisplayTimeFormatter(context);
    }
    
    private void updateSummary() {
        this.setSummary(this.mTimeFormatter.getAutoModeTimeSummary(this.getContext(), this.mController));
    }
    
    public void onActivated(final boolean b) {
        this.updateSummary();
    }
    
    public void onAttached() {
        super.onAttached();
        this.mController.setListener((ColorDisplayController$Callback)this);
        this.updateSummary();
    }
    
    public void onAutoModeChanged(final int n) {
        this.updateSummary();
    }
    
    public void onCustomEndTimeChanged(final LocalTime localTime) {
        this.updateSummary();
    }
    
    public void onCustomStartTimeChanged(final LocalTime localTime) {
        this.updateSummary();
    }
    
    public void onDetached() {
        super.onDetached();
        this.mController.setListener((ColorDisplayController$Callback)null);
    }
}
