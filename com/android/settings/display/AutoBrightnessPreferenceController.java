package com.android.settings.display;

import android.text.TextUtils;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.DisplaySettings;
import com.android.settings.search.ResultPayload;
import android.content.Context;
import com.android.settings.core.TogglePreferenceController;

public class AutoBrightnessPreferenceController extends TogglePreferenceController
{
    private final int DEFAULT_VALUE;
    private final String SYSTEM_KEY;
    
    public AutoBrightnessPreferenceController(final Context context, final String s) {
        super(context, s);
        this.SYSTEM_KEY = "screen_brightness_mode";
        this.DEFAULT_VALUE = 0;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(17956895)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("screen_brightness_mode", 1, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, DisplaySettings.class.getName(), this.getPreferenceKey(), this.mContext.getString(2131887447)), this.isAvailable(), 0);
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.System.getInt(contentResolver, "screen_brightness_mode", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"auto_brightness");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        Settings.System.putInt(this.mContext.getContentResolver(), "screen_brightness_mode", (int)(b ? 1 : 0));
        return true;
    }
}
