package com.android.settings.display;

import android.text.TextUtils;
import com.android.internal.view.RotationPolicy;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.internal.view.RotationPolicy$RotationPolicyListener;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settings.core.TogglePreferenceController;

public class AutoRotatePreferenceController extends TogglePreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private Preference mPreference;
    private RotationPolicy$RotationPolicyListener mRotationPolicyListener;
    
    public AutoRotatePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (RotationPolicy.isRotationLockToggleVisible(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public boolean isChecked() {
        return RotationPolicy.isRotationLocked(this.mContext) ^ true;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"auto_rotate");
    }
    
    @Override
    public void onPause() {
        if (this.mRotationPolicyListener != null) {
            RotationPolicy.unregisterRotationPolicyListener(this.mContext, this.mRotationPolicyListener);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mRotationPolicyListener == null) {
            this.mRotationPolicyListener = new RotationPolicy$RotationPolicyListener() {
                public void onChange() {
                    if (AutoRotatePreferenceController.this.mPreference != null) {
                        AutoRotatePreferenceController.this.updateState(AutoRotatePreferenceController.this.mPreference);
                    }
                }
            };
        }
        RotationPolicy.registerRotationPolicyListener(this.mContext, this.mRotationPolicyListener);
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        final boolean b2 = b ^ true;
        this.mMetricsFeatureProvider.action(this.mContext, 203, b2);
        RotationPolicy.setRotationLock(this.mContext, b2);
        return true;
    }
    
    @Override
    public void updateState(final Preference mPreference) {
        super.updateState(this.mPreference = mPreference);
    }
}
