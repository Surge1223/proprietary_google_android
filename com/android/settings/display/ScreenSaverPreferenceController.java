package com.android.settings.display;

import com.android.settings.dream.DreamSettings;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ScreenSaverPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public ScreenSaverPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "screensaver";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(17956945);
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(DreamSettings.getSummaryTextWithDreamName(this.mContext));
    }
}
