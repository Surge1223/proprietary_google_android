package com.android.settings.display;

import android.support.v7.preference.Preference;
import android.text.TextUtils;
import com.android.settings.widget.SeekBarPreference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.internal.app.ColorDisplayController;
import com.android.settings.core.SliderPreferenceController;

public class NightDisplayIntensityPreferenceController extends SliderPreferenceController
{
    private ColorDisplayController mController;
    
    public NightDisplayIntensityPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mController = new ColorDisplayController(context);
    }
    
    private int convertTemperature(final int n) {
        return this.mController.getMaximumColorTemperature() - n;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final SeekBarPreference seekBarPreference = (SeekBarPreference)preferenceScreen.findPreference(this.getPreferenceKey());
        seekBarPreference.setContinuousUpdates(true);
        seekBarPreference.setMax(this.getMaxSteps());
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (!ColorDisplayController.isAvailable(this.mContext)) {
            return 2;
        }
        if (!this.mController.isActivated()) {
            return 4;
        }
        return 0;
    }
    
    @Override
    public int getMaxSteps() {
        return this.convertTemperature(this.mController.getMinimumColorTemperature());
    }
    
    @Override
    public int getSliderPosition() {
        return this.convertTemperature(this.mController.getColorTemperature());
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"night_display_temperature");
    }
    
    @Override
    public boolean setSliderPosition(final int n) {
        return this.mController.setColorTemperature(this.convertTemperature(n));
    }
    
    @Override
    public final void updateState(final Preference preference) {
        super.updateState(preference);
        preference.setEnabled(this.mController.isActivated());
    }
}
