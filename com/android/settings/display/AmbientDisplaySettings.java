package com.android.settings.display;

import com.android.settings.gestures.PickupGesturePreferenceController;
import com.android.settings.gestures.DoubleTapScreenPreferenceController;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class AmbientDisplaySettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private AmbientDisplayConfiguration mConfig;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082703;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private AmbientDisplayConfiguration getConfig(final Context context) {
        if (this.mConfig == null) {
            this.mConfig = new AmbientDisplayConfiguration(context);
        }
        return this.mConfig;
    }
    
    @Override
    protected String getLogTag() {
        return "AmbientDisplaySettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1003;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082703;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(AmbientDisplayAlwaysOnPreferenceController.class).setConfig(this.getConfig(context)).setCallback((AmbientDisplayAlwaysOnPreferenceController.OnPreferenceChangedCallback)new _$$Lambda$AmbientDisplaySettings$EZV3GOIvjt5KUMzbyw8y8_onopw(this));
        this.use(AmbientDisplayNotificationsPreferenceController.class).setConfig(this.getConfig(context));
        this.use(DoubleTapScreenPreferenceController.class).setConfig(this.getConfig(context));
        this.use(PickupGesturePreferenceController.class).setConfig(this.getConfig(context));
    }
}
