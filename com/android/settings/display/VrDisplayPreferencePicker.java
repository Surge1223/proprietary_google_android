package com.android.settings.display;

import android.graphics.drawable.Drawable;
import com.android.settingslib.widget.CandidateInfo;
import android.text.TextUtils;
import android.provider.Settings;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import com.android.settings.widget.RadioButtonPickerFragment;

public class VrDisplayPreferencePicker extends RadioButtonPickerFragment
{
    @Override
    protected List<VrCandidateInfo> getCandidates() {
        final ArrayList<VrCandidateInfo> list = new ArrayList<VrCandidateInfo>();
        final Context context = this.getContext();
        list.add(new VrCandidateInfo(context, 0, 2131887451));
        list.add(new VrCandidateInfo(context, 1, 2131887452));
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        final int intForUser = Settings.Secure.getIntForUser(this.getContext().getContentResolver(), "vr_display_mode", 0, this.mUserId);
        final StringBuilder sb = new StringBuilder();
        sb.append("vr_display_pref_");
        sb.append(intForUser);
        return sb.toString();
    }
    
    @Override
    public int getMetricsCategory() {
        return 921;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082860;
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return false;
        }
        switch (s) {
            default: {
                return false;
            }
            case "vr_display_pref_1": {
                return Settings.Secure.putIntForUser(this.getContext().getContentResolver(), "vr_display_mode", 1, this.mUserId);
            }
            case "vr_display_pref_0": {
                return Settings.Secure.putIntForUser(this.getContext().getContentResolver(), "vr_display_mode", 0, this.mUserId);
            }
        }
    }
    
    static class VrCandidateInfo extends CandidateInfo
    {
        public final String label;
        public final int value;
        
        public VrCandidateInfo(final Context context, final int value, final int n) {
            super(true);
            this.value = value;
            this.label = context.getString(n);
        }
        
        @Override
        public String getKey() {
            final StringBuilder sb = new StringBuilder();
            sb.append("vr_display_pref_");
            sb.append(this.value);
            return sb.toString();
        }
        
        @Override
        public Drawable loadIcon() {
            return null;
        }
        
        @Override
        public CharSequence loadLabel() {
            return this.label;
        }
    }
}
