package com.android.settings.display;

import android.text.TextUtils;
import android.support.v7.preference.ListPreference;
import java.util.Objects;
import android.util.Pair;
import java.util.ArrayList;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.List;
import android.os.UserHandle;
import com.android.settings.overlay.FeatureFactory;
import android.os.ServiceManager;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.settings.wrapper.OverlayManagerWrapper;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class ThemePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final OverlayManagerWrapper mOverlayService;
    private final PackageManager mPackageManager;
    
    public ThemePreferenceController(final Context context) {
        OverlayManagerWrapper overlayManagerWrapper;
        if (ServiceManager.getService("overlay") != null) {
            overlayManagerWrapper = new OverlayManagerWrapper();
        }
        else {
            overlayManagerWrapper = null;
        }
        this(context, overlayManagerWrapper);
    }
    
    ThemePreferenceController(final Context context, final OverlayManagerWrapper mOverlayService) {
        super(context);
        this.mOverlayService = mOverlayService;
        this.mPackageManager = context.getPackageManager();
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    private String getTheme() {
        final List<OverlayManagerWrapper.OverlayInfo> overlayInfosForTarget = this.mOverlayService.getOverlayInfosForTarget("android", UserHandle.myUserId());
        for (int i = 0; i < overlayInfosForTarget.size(); ++i) {
            if (overlayInfosForTarget.get(i).isEnabled() && this.isTheme(overlayInfosForTarget.get(i))) {
                return overlayInfosForTarget.get(i).packageName;
            }
        }
        return null;
    }
    
    private boolean isTheme(final OverlayManagerWrapper.OverlayInfo overlayInfo) {
        final boolean equals = "android.theme".equals(overlayInfo.category);
        final boolean b = false;
        if (!equals) {
            return false;
        }
        try {
            final PackageInfo packageInfo = this.mPackageManager.getPackageInfo(overlayInfo.packageName, 0);
            boolean b2 = b;
            if (packageInfo != null) {
                final boolean staticOverlayPackage = packageInfo.isStaticOverlayPackage();
                b2 = b;
                if (!staticOverlayPackage) {
                    b2 = true;
                }
            }
            return b2;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    String[] getAvailableThemes() {
        final List<OverlayManagerWrapper.OverlayInfo> overlayInfosForTarget = this.mOverlayService.getOverlayInfosForTarget("android", UserHandle.myUserId());
        final ArrayList list = new ArrayList<String>(overlayInfosForTarget.size());
        for (int i = 0; i < overlayInfosForTarget.size(); ++i) {
            if (this.isTheme(overlayInfosForTarget.get(i))) {
                list.add(overlayInfosForTarget.get(i).packageName);
            }
        }
        return list.toArray(new String[list.size()]);
    }
    
    String getCurrentTheme() {
        return this.getTheme();
    }
    
    @Override
    public String getPreferenceKey() {
        return "theme";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("theme".equals(preference.getKey())) {
            this.mMetricsFeatureProvider.action(this.mContext, 816, (Pair<Integer, Object>[])new Pair[0]);
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        final OverlayManagerWrapper mOverlayService = this.mOverlayService;
        final boolean b = false;
        if (mOverlayService == null) {
            return false;
        }
        final String[] availableThemes = this.getAvailableThemes();
        boolean b2 = b;
        if (availableThemes != null) {
            b2 = b;
            if (availableThemes.length > 1) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (Objects.equals(o, this.getTheme())) {
            return true;
        }
        this.mOverlayService.setEnabledExclusiveInCategory((String)o, UserHandle.myUserId());
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ListPreference listPreference = (ListPreference)preference;
        final String[] availableThemes = this.getAvailableThemes();
        final CharSequence[] entries = new CharSequence[availableThemes.length];
        final int n = 0;
        for (int i = 0; i < availableThemes.length; ++i) {
            try {
                entries[i] = this.mPackageManager.getApplicationInfo(availableThemes[i], 0).loadLabel(this.mPackageManager);
            }
            catch (PackageManager$NameNotFoundException ex) {
                entries[i] = availableThemes[i];
            }
        }
        listPreference.setEntries(entries);
        listPreference.setEntryValues(availableThemes);
        final String currentTheme = this.getCurrentTheme();
        final CharSequence charSequence = null;
        int n2 = n;
        CharSequence charSequence2;
        while (true) {
            charSequence2 = charSequence;
            if (n2 >= availableThemes.length) {
                break;
            }
            if (TextUtils.equals((CharSequence)availableThemes[n2], (CharSequence)currentTheme)) {
                charSequence2 = entries[n2];
                break;
            }
            ++n2;
        }
        CharSequence string = charSequence2;
        if (TextUtils.isEmpty(charSequence2)) {
            string = this.mContext.getString(2131887363);
        }
        listPreference.setSummary(string);
        listPreference.setValue(currentTheme);
    }
}
