package com.android.settings.display;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class CameraGesturePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public CameraGesturePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "camera_gesture";
    }
    
    @Override
    public boolean isAvailable() {
        final int integer = this.mContext.getResources().getInteger(17694752);
        boolean b = true;
        if (integer == -1 || SystemProperties.getBoolean("gesture.disable_camera_launch", false)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "camera_gesture_disabled", (int)(((boolean)o ^ true) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Secure.getInt(contentResolver, "camera_gesture_disabled", 0);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        if (int1 == 0) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
