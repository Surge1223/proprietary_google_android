package com.android.settings.display;

import android.os.RemoteException;
import android.util.Log;
import android.os.Parcel;
import android.os.ServiceManager;
import android.os.IBinder;
import com.android.internal.annotations.VisibleForTesting;
import android.content.Context;
import com.android.internal.app.ColorDisplayController;
import com.android.settings.core.BasePreferenceController;

public class ColorModePreferenceController extends BasePreferenceController
{
    private static final String KEY_COLOR_MODE = "color_mode";
    private static final int SURFACE_FLINGER_TRANSACTION_QUERY_WIDE_COLOR = 1024;
    private static final String TAG = "ColorModePreference";
    private ColorDisplayController mColorDisplayController;
    private final ConfigurationWrapper mConfigWrapper;
    
    public ColorModePreferenceController(final Context context) {
        super(context, "color_mode");
        this.mConfigWrapper = new ConfigurationWrapper();
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mConfigWrapper.isScreenWideColorGamut() && !this.getColorDisplayController().getAccessibilityTransformActivated()) {
            n = 0;
        }
        else {
            n = 3;
        }
        return n;
    }
    
    @VisibleForTesting
    ColorDisplayController getColorDisplayController() {
        if (this.mColorDisplayController == null) {
            this.mColorDisplayController = new ColorDisplayController(this.mContext);
        }
        return this.mColorDisplayController;
    }
    
    @Override
    public CharSequence getSummary() {
        final int colorMode = this.getColorDisplayController().getColorMode();
        if (colorMode == 3) {
            return this.mContext.getText(2131887030);
        }
        if (colorMode == 2) {
            return this.mContext.getText(2131887033);
        }
        if (colorMode == 1) {
            return this.mContext.getText(2131887031);
        }
        return this.mContext.getText(2131887032);
    }
    
    @VisibleForTesting
    static class ConfigurationWrapper
    {
        private final IBinder mSurfaceFlinger;
        
        ConfigurationWrapper() {
            this.mSurfaceFlinger = ServiceManager.getService("SurfaceFlinger");
        }
        
        boolean isScreenWideColorGamut() {
            if (this.mSurfaceFlinger != null) {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                obtain.writeInterfaceToken("android.ui.ISurfaceComposer");
                try {
                    try {
                        this.mSurfaceFlinger.transact(1024, obtain, obtain2, 0);
                        final boolean boolean1 = obtain2.readBoolean();
                        obtain.recycle();
                        obtain2.recycle();
                        return boolean1;
                    }
                    finally {}
                }
                catch (RemoteException ex) {
                    Log.e("ColorModePreference", "Failed to query wide color support", (Throwable)ex);
                    obtain.recycle();
                    obtain2.recycle();
                    return false;
                }
                obtain.recycle();
                obtain2.recycle();
            }
            return false;
        }
    }
}
