package com.android.settings.display;

import android.util.FeatureFlagUtils;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class SystemUiThemePreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    private ListPreference mSystemUiThemePref;
    
    public SystemUiThemePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mSystemUiThemePref = (ListPreference)preferenceScreen.findPreference(this.getPreferenceKey())).setValue(Integer.toString(Settings.Secure.getInt(this.mContext.getContentResolver(), "theme_mode", 0)));
    }
    
    @Override
    public int getAvailabilityStatus() {
        return (FeatureFlagUtils.isEnabled(this.mContext, "settings_systemui_theme") ^ true) ? 1 : 0;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mSystemUiThemePref.getEntries()[this.mSystemUiThemePref.findIndexOfValue(Integer.toString(Settings.Secure.getInt(this.mContext.getContentResolver(), "theme_mode", 0)))];
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "theme_mode", Integer.parseInt((String)o));
        this.refreshSummary(preference);
        return true;
    }
}
