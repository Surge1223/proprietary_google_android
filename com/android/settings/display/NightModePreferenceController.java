package com.android.settings.display;

import android.util.Log;
import android.app.UiModeManager;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class NightModePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public NightModePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        if (!this.isAvailable()) {
            this.setVisible(preferenceScreen, "night_mode", false);
            return;
        }
        final ListPreference listPreference = (ListPreference)preferenceScreen.findPreference("night_mode");
        if (listPreference != null) {
            listPreference.setValue(String.valueOf(((UiModeManager)this.mContext.getSystemService("uimode")).getNightMode()));
            listPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "night_mode";
    }
    
    @Override
    public boolean isAvailable() {
        return false;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        try {
            ((UiModeManager)this.mContext.getSystemService("uimode")).setNightMode(Integer.parseInt((String)o));
            return true;
        }
        catch (NumberFormatException ex) {
            Log.e("NightModePrefContr", "could not persist night mode setting", (Throwable)ex);
            return false;
        }
    }
}
