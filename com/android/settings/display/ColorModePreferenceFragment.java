package com.android.settings.display;

import android.graphics.drawable.Drawable;
import android.content.Context;
import java.util.ArrayList;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import android.support.v7.preference.Preference;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import com.android.internal.app.ColorDisplayController;
import com.android.internal.app.ColorDisplayController$Callback;
import com.android.settings.widget.RadioButtonPickerFragment;

public class ColorModePreferenceFragment extends RadioButtonPickerFragment implements ColorDisplayController$Callback
{
    static final String KEY_COLOR_MODE_AUTOMATIC = "color_mode_automatic";
    static final String KEY_COLOR_MODE_BOOSTED = "color_mode_boosted";
    static final String KEY_COLOR_MODE_NATURAL = "color_mode_natural";
    static final String KEY_COLOR_MODE_SATURATED = "color_mode_saturated";
    private ColorDisplayController mController;
    
    @Override
    protected void addStaticPreferences(final PreferenceScreen preferenceScreen) {
        this.configureAndInstallPreview(new LayoutPreference(preferenceScreen.getContext(), 2131558488), preferenceScreen);
    }
    
    void configureAndInstallPreview(final LayoutPreference layoutPreference, final PreferenceScreen preferenceScreen) {
        layoutPreference.setSelectable(false);
        preferenceScreen.addPreference(layoutPreference);
    }
    
    @Override
    protected List<? extends CandidateInfo> getCandidates() {
        final Context context = this.getContext();
        final int[] intArray = context.getResources().getIntArray(17235990);
        final ArrayList<ColorModeCandidateInfo> list = new ArrayList<ColorModeCandidateInfo>();
        if (intArray != null) {
            for (final int n : intArray) {
                if (n == 0) {
                    list.add(new ColorModeCandidateInfo(context.getText(2131887032), "color_mode_natural", true));
                }
                else if (n == 1) {
                    list.add(new ColorModeCandidateInfo(context.getText(2131887031), "color_mode_boosted", true));
                }
                else if (n == 2) {
                    list.add(new ColorModeCandidateInfo(context.getText(2131887033), "color_mode_saturated", true));
                }
                else if (n == 3) {
                    list.add(new ColorModeCandidateInfo(context.getText(2131887030), "color_mode_automatic", true));
                }
            }
        }
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        final int colorMode = this.mController.getColorMode();
        if (colorMode == 3) {
            return "color_mode_automatic";
        }
        if (colorMode == 2) {
            return "color_mode_saturated";
        }
        if (colorMode == 1) {
            return "color_mode_boosted";
        }
        return "color_mode_natural";
    }
    
    public int getMetricsCategory() {
        return 1143;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082733;
    }
    
    public void onAccessibilityTransformChanged(final boolean b) {
        if (b) {
            this.getActivity().onBackPressed();
        }
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        (this.mController = new ColorDisplayController(context)).setListener((ColorDisplayController$Callback)this);
    }
    
    public void onDetach() {
        super.onDetach();
        if (this.mController != null) {
            this.mController.setListener((ColorDisplayController$Callback)null);
            this.mController = null;
        }
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        final int hashCode = s.hashCode();
        int n = 0;
        Label_0090: {
            if (hashCode != -2029194174) {
                if (hashCode != -739564821) {
                    if (hashCode != -365217559) {
                        if (hashCode == 765917269) {
                            if (s.equals("color_mode_saturated")) {
                                n = 2;
                                break Label_0090;
                            }
                        }
                    }
                    else if (s.equals("color_mode_natural")) {
                        n = 0;
                        break Label_0090;
                    }
                }
                else if (s.equals("color_mode_automatic")) {
                    n = 3;
                    break Label_0090;
                }
            }
            else if (s.equals("color_mode_boosted")) {
                n = 1;
                break Label_0090;
            }
            n = -1;
        }
        switch (n) {
            case 3: {
                this.mController.setColorMode(3);
                break;
            }
            case 2: {
                this.mController.setColorMode(2);
                break;
            }
            case 1: {
                this.mController.setColorMode(1);
                break;
            }
            case 0: {
                this.mController.setColorMode(0);
                break;
            }
        }
        return true;
    }
    
    static class ColorModeCandidateInfo extends CandidateInfo
    {
        private final String mKey;
        private final CharSequence mLabel;
        
        ColorModeCandidateInfo(final CharSequence mLabel, final String mKey, final boolean b) {
            super(b);
            this.mLabel = mLabel;
            this.mKey = mKey;
        }
        
        @Override
        public String getKey() {
            return this.mKey;
        }
        
        @Override
        public Drawable loadIcon() {
            return null;
        }
        
        @Override
        public CharSequence loadLabel() {
            return this.mLabel;
        }
    }
}
