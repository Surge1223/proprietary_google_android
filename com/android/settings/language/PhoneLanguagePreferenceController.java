package com.android.settings.language;

import com.android.settings.overlay.FeatureFactory;
import java.util.List;
import com.android.settings.localepicker.LocaleListEditor;
import com.android.settings.core.SubSettingLauncher;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class PhoneLanguagePreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public PhoneLanguagePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "phone_language";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!"phone_language".equals(preference.getKey())) {
            return false;
        }
        new SubSettingLauncher(this.mContext).setDestination(LocaleListEditor.class.getName()).setSourceMetricsCategory(750).setTitle(2131888620).launch();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        final boolean boolean1 = this.mContext.getResources().getBoolean(2131034155);
        boolean b = true;
        if (!boolean1 || this.mContext.getAssets().getLocales().length <= 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void updateNonIndexableKeys(final List<String> list) {
        list.add(this.getPreferenceKey());
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (preference == null) {
            return;
        }
        preference.setSummary(FeatureFactory.getFactory(this.mContext).getLocaleFeatureProvider().getLocaleNames());
    }
}
