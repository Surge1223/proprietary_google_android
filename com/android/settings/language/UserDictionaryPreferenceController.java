package com.android.settings.language;

import java.io.Serializable;
import android.os.Bundle;
import com.android.settings.inputmethod.UserDictionarySettings;
import android.support.v7.preference.Preference;
import com.android.settings.inputmethod.UserDictionaryList;
import java.util.TreeSet;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class UserDictionaryPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public UserDictionaryPreferenceController(final Context context) {
        super(context);
    }
    
    protected TreeSet<String> getDictionaryLocales() {
        return UserDictionaryList.getUserDictionaryLocalesSet(this.mContext);
    }
    
    @Override
    public String getPreferenceKey() {
        return "key_user_dictionary_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.isAvailable() && preference != null) {
            final TreeSet<String> dictionaryLocales = this.getDictionaryLocales();
            final Bundle extras = preference.getExtras();
            Serializable s;
            if (dictionaryLocales.size() <= 1) {
                if (!dictionaryLocales.isEmpty()) {
                    extras.putString("locale", (String)dictionaryLocales.first());
                }
                s = UserDictionarySettings.class;
            }
            else {
                s = UserDictionaryList.class;
            }
            preference.setFragment(((Class)s).getCanonicalName());
        }
    }
}
