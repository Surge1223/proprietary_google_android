package com.android.settings.language;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class PointerSpeedController extends BasePreferenceController
{
    static final String KEY_POINTER_SPEED = "pointer_speed";
    
    public PointerSpeedController(final Context context) {
        super(context, "pointer_speed");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034157)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
