package com.android.settings.language;

import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.ContentResolver;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.content.ComponentName;
import android.text.TextUtils;
import android.provider.Settings;
import com.android.settings.applications.defaultapps.DefaultAutofillPreferenceController;
import com.android.settings.inputmethod.SpellCheckerPreferenceController;
import android.speech.tts.TtsEngines;
import com.android.settings.widget.PreferenceCategoryController;
import com.android.settings.inputmethod.PhysicalKeyboardPreferenceController;
import com.android.settings.inputmethod.VirtualKeyboardPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$LanguageAndInputSettings$VvwbgRiPWoRSuoMu5QPyPqZ5AEc implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return LanguageAndInputSettings.lambda$static$0(activity, summaryLoader);
    }
}
