package com.android.settings.language;

import android.content.Context;
import android.speech.tts.TtsEngines;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class TtsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final TtsEngines mTtsEngines;
    
    public TtsPreferenceController(final Context context, final TtsEngines mTtsEngines) {
        super(context);
        this.mTtsEngines = mTtsEngines;
    }
    
    @Override
    public String getPreferenceKey() {
        return "tts_settings_summary";
    }
    
    @Override
    public boolean isAvailable() {
        return !this.mTtsEngines.getEngines().isEmpty() && this.mContext.getResources().getBoolean(2131034169);
    }
}
