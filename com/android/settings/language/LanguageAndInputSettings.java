package com.android.settings.language;

import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.ContentResolver;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.content.ComponentName;
import android.text.TextUtils;
import android.provider.Settings;
import android.app.Activity;
import com.android.settings.applications.defaultapps.DefaultAutofillPreferenceController;
import com.android.settings.inputmethod.SpellCheckerPreferenceController;
import android.speech.tts.TtsEngines;
import com.android.settings.widget.PreferenceCategoryController;
import com.android.settings.inputmethod.PhysicalKeyboardPreferenceController;
import com.android.settings.inputmethod.VirtualKeyboardPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class LanguageAndInputSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = (SummaryProviderFactory)_$$Lambda$LanguageAndInputSettings$VvwbgRiPWoRSuoMu5QPyPqZ5AEc.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("tts_settings_summary");
                nonIndexableKeys.add("physical_keyboard_pref");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082783;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<PhoneLanguagePreferenceController> list = (ArrayList<PhoneLanguagePreferenceController>)new ArrayList<UserDictionaryPreferenceController>();
        list.add((UserDictionaryPreferenceController)new PhoneLanguagePreferenceController(context));
        final VirtualKeyboardPreferenceController virtualKeyboardPreferenceController = new VirtualKeyboardPreferenceController(context);
        final PhysicalKeyboardPreferenceController physicalKeyboardPreferenceController = new PhysicalKeyboardPreferenceController(context, lifecycle);
        list.add((UserDictionaryPreferenceController)virtualKeyboardPreferenceController);
        list.add((UserDictionaryPreferenceController)physicalKeyboardPreferenceController);
        list.add((UserDictionaryPreferenceController)new PreferenceCategoryController(context, "keyboards_category").setChildren(Arrays.asList(virtualKeyboardPreferenceController, physicalKeyboardPreferenceController)));
        final TtsPreferenceController ttsPreferenceController = new TtsPreferenceController(context, new TtsEngines(context));
        list.add((UserDictionaryPreferenceController)ttsPreferenceController);
        final PointerSpeedController pointerSpeedController = new PointerSpeedController(context);
        list.add((UserDictionaryPreferenceController)pointerSpeedController);
        list.add((UserDictionaryPreferenceController)new PreferenceCategoryController(context, "pointer_and_tts_category").setChildren(Arrays.asList(pointerSpeedController, ttsPreferenceController)));
        list.add((UserDictionaryPreferenceController)new SpellCheckerPreferenceController(context));
        list.add((UserDictionaryPreferenceController)new DefaultAutofillPreferenceController(context));
        list.add(new UserDictionaryPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    protected String getLogTag() {
        return "LangAndInputSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 750;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082783;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final Activity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        activity.setTitle(2131888013);
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            final ContentResolver contentResolver = this.mContext.getContentResolver();
            if (b) {
                final String string = Settings.Secure.getString(contentResolver, "default_input_method");
                if (!TextUtils.isEmpty((CharSequence)string)) {
                    final PackageManager packageManager = this.mContext.getPackageManager();
                    final String packageName = ComponentName.unflattenFromString(string).getPackageName();
                    for (final InputMethodInfo inputMethodInfo : ((InputMethodManager)this.mContext.getSystemService("input_method")).getInputMethodList()) {
                        if (TextUtils.equals((CharSequence)inputMethodInfo.getPackageName(), (CharSequence)packageName)) {
                            this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, inputMethodInfo.loadLabel(packageManager));
                            return;
                        }
                    }
                }
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, "");
            }
        }
    }
}
