package com.android.settings;

import android.content.Context;

public interface SelfAvailablePreference
{
    boolean isAvailable(final Context p0);
}
