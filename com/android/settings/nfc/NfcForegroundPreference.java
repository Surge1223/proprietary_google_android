package com.android.settings.nfc;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.DropDownPreference;

public class NfcForegroundPreference extends DropDownPreference implements OnPreferenceChangeListener, Callback
{
    private final PaymentBackend mPaymentBackend;
    
    public NfcForegroundPreference(final Context context, final PaymentBackend mPaymentBackend) {
        super(context);
        (this.mPaymentBackend = mPaymentBackend).registerCallback((PaymentBackend.Callback)this);
        this.setTitle(this.getContext().getString(2131888377));
        this.setEntries(new CharSequence[] { this.getContext().getString(2131888369), this.getContext().getString(2131888368) });
        this.setEntryValues(new CharSequence[] { "1", "0" });
        this.refresh();
        this.setOnPreferenceChangeListener((OnPreferenceChangeListener)this);
    }
    
    @Override
    public void onPaymentAppsChanged() {
        this.refresh();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final String s = (String)o;
        this.setSummary(this.getEntries()[this.findIndexOfValue(s)]);
        this.mPaymentBackend.setForegroundMode(Integer.parseInt(s) != 0);
        return true;
    }
    
    void refresh() {
        if (this.mPaymentBackend.isForegroundMode()) {
            this.setValue("1");
        }
        else {
            this.setValue("0");
        }
        this.setSummary(this.getEntry());
    }
}
