package com.android.settings.nfc;

import android.content.Context;
import android.support.v14.preference.SwitchPreference;

public class NfcEnabler extends BaseNfcEnabler
{
    private final SwitchPreference mPreference;
    
    public NfcEnabler(final Context context, final SwitchPreference mPreference) {
        super(context);
        this.mPreference = mPreference;
    }
    
    @Override
    protected void handleNfcStateChanged(final int n) {
        switch (n) {
            case 4: {
                this.mPreference.setChecked(false);
                this.mPreference.setEnabled(false);
                break;
            }
            case 3: {
                this.mPreference.setChecked(true);
                this.mPreference.setEnabled(true);
                break;
            }
            case 2: {
                this.mPreference.setChecked(true);
                this.mPreference.setEnabled(false);
                break;
            }
            case 1: {
                this.mPreference.setChecked(false);
                this.mPreference.setEnabled(true);
                break;
            }
        }
    }
}
