package com.android.settings.nfc;

import android.widget.CompoundButton;
import android.app.Dialog;
import android.widget.RadioButton;
import android.view.ViewGroup;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.BaseAdapter;
import java.util.List;
import android.widget.ListAdapter;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.content.Intent;
import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.View.OnClickListener;
import com.android.settingslib.CustomDialogPreference;

public class NfcPaymentPreference extends CustomDialogPreference implements View.OnClickListener, Callback
{
    private final NfcPaymentAdapter mAdapter;
    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final PaymentBackend mPaymentBackend;
    private ImageView mSettingsButtonView;
    
    public NfcPaymentPreference(final Context mContext, final PaymentBackend mPaymentBackend) {
        super(mContext, null);
        this.mPaymentBackend = mPaymentBackend;
        this.mContext = mContext;
        mPaymentBackend.registerCallback((PaymentBackend.Callback)this);
        this.mAdapter = new NfcPaymentAdapter();
        this.setDialogTitle(mContext.getString(2131888372));
        this.mLayoutInflater = (LayoutInflater)mContext.getSystemService("layout_inflater");
        this.setWidgetLayoutResource(2131558679);
        this.refresh();
    }
    
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        (this.mSettingsButtonView = (ImageView)preferenceViewHolder.findViewById(2131362594)).setOnClickListener((View.OnClickListener)this);
        this.updateSettingsVisibility();
    }
    
    public void onClick(final View view) {
        final PaymentBackend.PaymentAppInfo defaultApp = this.mPaymentBackend.getDefaultApp();
        if (defaultApp != null && defaultApp.settingsComponent != null) {
            final Intent intent = new Intent("android.intent.action.MAIN");
            intent.setComponent(defaultApp.settingsComponent);
            intent.addFlags(268435456);
            try {
                this.mContext.startActivity(intent);
            }
            catch (ActivityNotFoundException ex) {
                Log.e("NfcPaymentPreference", "Settings activity not found.");
            }
        }
    }
    
    public void onPaymentAppsChanged() {
        this.refresh();
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        super.onPrepareDialogBuilder(alertDialog$Builder, dialogInterface$OnClickListener);
        alertDialog$Builder.setSingleChoiceItems((ListAdapter)this.mAdapter, 0, dialogInterface$OnClickListener);
    }
    
    public void refresh() {
        final List<PaymentBackend.PaymentAppInfo> paymentAppInfos = this.mPaymentBackend.getPaymentAppInfos();
        final PaymentBackend.PaymentAppInfo defaultApp = this.mPaymentBackend.getDefaultApp();
        if (paymentAppInfos != null) {
            this.mAdapter.updateApps(paymentAppInfos.toArray(new PaymentAppInfo[paymentAppInfos.size()]), defaultApp);
        }
        this.setTitle(2131888366);
        if (defaultApp != null) {
            this.setSummary(defaultApp.label);
        }
        else {
            this.setSummary(this.mContext.getString(2131888367));
        }
        this.updateSettingsVisibility();
    }
    
    void updateSettingsVisibility() {
        if (this.mSettingsButtonView != null) {
            final PaymentBackend.PaymentAppInfo defaultApp = this.mPaymentBackend.getDefaultApp();
            if (defaultApp != null && defaultApp.settingsComponent != null) {
                this.mSettingsButtonView.setVisibility(0);
            }
            else {
                this.mSettingsButtonView.setVisibility(8);
            }
        }
    }
    
    class NfcPaymentAdapter extends BaseAdapter implements View.OnClickListener, CompoundButton$OnCheckedChangeListener
    {
        private PaymentAppInfo[] appInfos;
        
        public int getCount() {
            return this.appInfos.length;
        }
        
        public PaymentAppInfo getItem(final int n) {
            return this.appInfos[n];
        }
        
        public long getItemId(final int n) {
            return this.appInfos[n].componentName.hashCode();
        }
        
        public View getView(final int n, View inflate, final ViewGroup viewGroup) {
            final PaymentAppInfo paymentAppInfo = this.appInfos[n];
            ViewHolder tag;
            if (inflate == null) {
                inflate = NfcPaymentPreference.this.mLayoutInflater.inflate(2131558618, viewGroup, false);
                tag = new ViewHolder();
                tag.imageView = (ImageView)inflate.findViewById(2131361901);
                tag.radioButton = (RadioButton)inflate.findViewById(2131361942);
                inflate.setTag((Object)tag);
            }
            else {
                tag = (ViewHolder)inflate.getTag();
            }
            tag.imageView.setImageDrawable(paymentAppInfo.banner);
            tag.imageView.setTag((Object)paymentAppInfo);
            tag.imageView.setContentDescription(paymentAppInfo.label);
            tag.imageView.setOnClickListener((View.OnClickListener)this);
            tag.radioButton.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
            tag.radioButton.setChecked(paymentAppInfo.isDefault);
            tag.radioButton.setContentDescription(paymentAppInfo.label);
            tag.radioButton.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
            tag.radioButton.setTag((Object)paymentAppInfo);
            return inflate;
        }
        
        void makeDefault(final PaymentAppInfo paymentAppInfo) {
            if (!paymentAppInfo.isDefault) {
                NfcPaymentPreference.this.mPaymentBackend.setDefaultPaymentApp(paymentAppInfo.componentName);
            }
            final Dialog dialog = NfcPaymentPreference.this.getDialog();
            if (dialog != null) {
                dialog.dismiss();
            }
        }
        
        public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
            this.makeDefault((PaymentAppInfo)compoundButton.getTag());
        }
        
        public void onClick(final View view) {
            this.makeDefault((PaymentAppInfo)view.getTag());
        }
        
        public void updateApps(final PaymentAppInfo[] appInfos, final PaymentAppInfo paymentAppInfo) {
            this.appInfos = appInfos;
            this.notifyDataSetChanged();
        }
        
        public class ViewHolder
        {
            public ImageView imageView;
            public RadioButton radioButton;
        }
    }
}
