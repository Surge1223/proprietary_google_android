package com.android.settings.nfc;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.nfc.NfcAdapter;
import android.content.IntentFilter;
import android.content.Context;

public abstract class BaseNfcEnabler
{
    private final Context mContext;
    private final IntentFilter mIntentFilter;
    protected final NfcAdapter mNfcAdapter;
    private final BroadcastReceiver mReceiver;
    
    public BaseNfcEnabler(final Context mContext) {
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if ("android.nfc.action.ADAPTER_STATE_CHANGED".equals(intent.getAction())) {
                    BaseNfcEnabler.this.handleNfcStateChanged(intent.getIntExtra("android.nfc.extra.ADAPTER_STATE", 1));
                }
            }
        };
        this.mContext = mContext;
        this.mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        if (!this.isNfcAvailable()) {
            this.mIntentFilter = null;
            return;
        }
        this.mIntentFilter = new IntentFilter("android.nfc.action.ADAPTER_STATE_CHANGED");
    }
    
    protected abstract void handleNfcStateChanged(final int p0);
    
    public boolean isNfcAvailable() {
        return this.mNfcAdapter != null;
    }
    
    public void pause() {
        if (!this.isNfcAvailable()) {
            return;
        }
        this.mContext.unregisterReceiver(this.mReceiver);
    }
    
    public void resume() {
        if (!this.isNfcAvailable()) {
            return;
        }
        this.handleNfcStateChanged(this.mNfcAdapter.getAdapterState());
        this.mContext.registerReceiver(this.mReceiver, this.mIntentFilter);
    }
}
