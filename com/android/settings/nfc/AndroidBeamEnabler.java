package com.android.settings.nfc;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.content.Context;
import com.android.settingslib.RestrictedPreference;

public class AndroidBeamEnabler extends BaseNfcEnabler
{
    private final boolean mBeamDisallowedBySystem;
    private final RestrictedPreference mPreference;
    
    public AndroidBeamEnabler(final Context context, final RestrictedPreference mPreference) {
        super(context);
        this.mPreference = mPreference;
        this.mBeamDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction(context, "no_outgoing_beam", UserHandle.myUserId());
        if (!this.isNfcAvailable()) {
            this.mPreference.setEnabled(false);
            return;
        }
        if (this.mBeamDisallowedBySystem) {
            this.mPreference.setEnabled(false);
        }
    }
    
    @Override
    protected void handleNfcStateChanged(final int n) {
        switch (n) {
            case 4: {
                this.mPreference.setEnabled(false);
                break;
            }
            case 3: {
                if (this.mBeamDisallowedBySystem) {
                    this.mPreference.setDisabledByAdmin(null);
                    this.mPreference.setEnabled(false);
                }
                else {
                    this.mPreference.checkRestrictionAndSetDisabled("no_outgoing_beam");
                }
                if (this.mNfcAdapter.isNdefPushEnabled() && this.mPreference.isEnabled()) {
                    this.mPreference.setSummary(2131886311);
                    break;
                }
                this.mPreference.setSummary(2131886310);
                break;
            }
            case 2: {
                this.mPreference.setEnabled(false);
                break;
            }
            case 1: {
                this.mPreference.setEnabled(false);
                this.mPreference.setSummary(2131886307);
                break;
            }
        }
    }
}
