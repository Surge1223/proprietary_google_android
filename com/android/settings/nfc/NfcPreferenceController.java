package com.android.settings.nfc;

import android.text.TextUtils;
import android.content.IntentFilter;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import android.content.Context;
import android.nfc.NfcAdapter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.TogglePreferenceController;

public class NfcPreferenceController extends TogglePreferenceController implements LifecycleObserver, OnPause, OnResume
{
    public static final String KEY_TOGGLE_NFC = "toggle_nfc";
    private NfcAirplaneModeObserver mAirplaneModeObserver;
    private final NfcAdapter mNfcAdapter;
    private NfcEnabler mNfcEnabler;
    
    public NfcPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mNfcAdapter = NfcAdapter.getDefaultAdapter(context);
    }
    
    public static boolean isToggleableInAirplaneMode(final Context context) {
        final String string = Settings.Global.getString(context.getContentResolver(), "airplane_mode_toggleable_radios");
        return string != null && string.contains("nfc");
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (!this.isAvailable()) {
            this.mNfcEnabler = null;
            return;
        }
        final SwitchPreference switchPreference = (SwitchPreference)preferenceScreen.findPreference(this.getPreferenceKey());
        this.mNfcEnabler = new NfcEnabler(this.mContext, switchPreference);
        if (!isToggleableInAirplaneMode(this.mContext)) {
            this.mAirplaneModeObserver = new NfcAirplaneModeObserver(this.mContext, this.mNfcAdapter, switchPreference);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mNfcAdapter != null) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.nfc.action.ADAPTER_STATE_CHANGED");
        intentFilter.addAction("android.nfc.extra.ADAPTER_STATE");
        return intentFilter;
    }
    
    @Override
    public boolean hasAsyncUpdate() {
        return true;
    }
    
    @Override
    public boolean isChecked() {
        return this.mNfcAdapter.isEnabled();
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"toggle_nfc");
    }
    
    @Override
    public void onPause() {
        if (this.mAirplaneModeObserver != null) {
            this.mAirplaneModeObserver.unregister();
        }
        if (this.mNfcEnabler != null) {
            this.mNfcEnabler.pause();
        }
    }
    
    @Override
    public void onResume() {
        if (this.mAirplaneModeObserver != null) {
            this.mAirplaneModeObserver.register();
        }
        if (this.mNfcEnabler != null) {
            this.mNfcEnabler.resume();
        }
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        if (b) {
            this.mNfcAdapter.enable();
        }
        else {
            this.mNfcAdapter.disable();
        }
        return true;
    }
}
