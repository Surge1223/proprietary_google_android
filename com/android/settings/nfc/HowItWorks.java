package com.android.settings.nfc;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;
import android.app.Activity;

public class HowItWorks extends Activity
{
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131558617);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        ((Button)this.findViewById(2131362390)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                HowItWorks.this.finish();
            }
        });
    }
    
    public boolean onNavigateUp() {
        this.finish();
        return true;
    }
}
