package com.android.settings.nfc;

import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.nfc.NfcAdapter;
import android.content.Context;
import android.net.Uri;
import android.database.ContentObserver;

public class NfcAirplaneModeObserver extends ContentObserver
{
    static final Uri AIRPLANE_MODE_URI;
    private int mAirplaneMode;
    private final Context mContext;
    private final NfcAdapter mNfcAdapter;
    private final Preference mPreference;
    
    static {
        AIRPLANE_MODE_URI = Settings.Global.getUriFor("airplane_mode_on");
    }
    
    public NfcAirplaneModeObserver(final Context mContext, final NfcAdapter mNfcAdapter, final Preference mPreference) {
        super(new Handler(Looper.getMainLooper()));
        this.mContext = mContext;
        this.mNfcAdapter = mNfcAdapter;
        this.mPreference = mPreference;
        this.updateNfcPreference();
    }
    
    private void updateNfcPreference() {
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "airplane_mode_on", this.mAirplaneMode);
        if (int1 == this.mAirplaneMode) {
            return;
        }
        this.mAirplaneMode = int1;
        final int mAirplaneMode = this.mAirplaneMode;
        boolean enabled = true;
        if (mAirplaneMode == 1) {
            enabled = false;
        }
        if (enabled) {
            this.mNfcAdapter.enable();
        }
        else {
            this.mNfcAdapter.disable();
        }
        this.mPreference.setEnabled(enabled);
    }
    
    public void onChange(final boolean b, final Uri uri) {
        super.onChange(b, uri);
        this.updateNfcPreference();
    }
    
    public void register() {
        this.mContext.getContentResolver().registerContentObserver(NfcAirplaneModeObserver.AIRPLANE_MODE_URI, false, (ContentObserver)this);
    }
    
    public void unregister() {
        this.mContext.getContentResolver().unregisterContentObserver((ContentObserver)this);
    }
}
