package com.android.settings.nfc;

import android.graphics.drawable.Drawable;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.nfc.cardemulation.ApduServiceInfo;
import java.util.Iterator;
import android.provider.Settings$SettingNotFoundException;
import java.util.List;
import android.provider.Settings;
import android.content.ComponentName;
import android.os.Message;
import com.android.internal.content.PackageMonitor;
import android.os.Handler;
import android.content.Context;
import android.nfc.cardemulation.CardEmulation;
import java.util.ArrayList;
import android.nfc.NfcAdapter;

public class PaymentBackend
{
    private final NfcAdapter mAdapter;
    private ArrayList<PaymentAppInfo> mAppInfos;
    private ArrayList<Callback> mCallbacks;
    private final CardEmulation mCardEmuManager;
    private final Context mContext;
    private PaymentAppInfo mDefaultAppInfo;
    private final Handler mHandler;
    private final PackageMonitor mSettingsPackageMonitor;
    
    public PaymentBackend(final Context mContext) {
        this.mSettingsPackageMonitor = new SettingsPackageMonitor();
        this.mCallbacks = new ArrayList<Callback>();
        this.mHandler = new Handler() {
            public void dispatchMessage(final Message message) {
                PaymentBackend.this.refresh();
            }
        };
        this.mContext = mContext;
        this.mAdapter = NfcAdapter.getDefaultAdapter(mContext);
        this.mCardEmuManager = CardEmulation.getInstance(this.mAdapter);
        this.refresh();
    }
    
    public PaymentAppInfo getDefaultApp() {
        return this.mDefaultAppInfo;
    }
    
    ComponentName getDefaultPaymentApp() {
        final String string = Settings.Secure.getString(this.mContext.getContentResolver(), "nfc_payment_default_component");
        if (string != null) {
            return ComponentName.unflattenFromString(string);
        }
        return null;
    }
    
    public List<PaymentAppInfo> getPaymentAppInfos() {
        return this.mAppInfos;
    }
    
    boolean isForegroundMode() {
        boolean b = false;
        try {
            if (Settings.Secure.getInt(this.mContext.getContentResolver(), "nfc_payment_foreground") != 0) {
                b = true;
            }
            return b;
        }
        catch (Settings$SettingNotFoundException ex) {
            return false;
        }
    }
    
    void makeCallbacks() {
        final Iterator<Callback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onPaymentAppsChanged();
        }
    }
    
    public void onPause() {
        this.mSettingsPackageMonitor.unregister();
    }
    
    public void onResume() {
        this.mSettingsPackageMonitor.register(this.mContext, this.mContext.getMainLooper(), false);
        this.refresh();
    }
    
    public void refresh() {
        final PackageManager packageManager = this.mContext.getPackageManager();
        final List services = this.mCardEmuManager.getServices("payment");
        final ArrayList<PaymentAppInfo> mAppInfos = new ArrayList<PaymentAppInfo>();
        if (services == null) {
            this.makeCallbacks();
            return;
        }
        final ComponentName defaultPaymentApp = this.getDefaultPaymentApp();
        PaymentAppInfo mDefaultAppInfo = null;
        for (final ApduServiceInfo apduServiceInfo : services) {
            final PaymentAppInfo paymentAppInfo = new PaymentAppInfo();
            paymentAppInfo.label = apduServiceInfo.loadLabel(packageManager);
            if (paymentAppInfo.label == null) {
                paymentAppInfo.label = apduServiceInfo.loadAppLabel(packageManager);
            }
            paymentAppInfo.isDefault = apduServiceInfo.getComponent().equals((Object)defaultPaymentApp);
            if (paymentAppInfo.isDefault) {
                mDefaultAppInfo = paymentAppInfo;
            }
            paymentAppInfo.componentName = apduServiceInfo.getComponent();
            final String settingsActivityName = apduServiceInfo.getSettingsActivityName();
            if (settingsActivityName != null) {
                paymentAppInfo.settingsComponent = new ComponentName(paymentAppInfo.componentName.getPackageName(), settingsActivityName);
            }
            else {
                paymentAppInfo.settingsComponent = null;
            }
            paymentAppInfo.description = apduServiceInfo.getDescription();
            paymentAppInfo.banner = apduServiceInfo.loadBanner(packageManager);
            mAppInfos.add(paymentAppInfo);
        }
        this.mAppInfos = mAppInfos;
        this.mDefaultAppInfo = mDefaultAppInfo;
        this.makeCallbacks();
    }
    
    public void registerCallback(final Callback callback) {
        this.mCallbacks.add(callback);
    }
    
    public void setDefaultPaymentApp(final ComponentName componentName) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        String flattenToString;
        if (componentName != null) {
            flattenToString = componentName.flattenToString();
        }
        else {
            flattenToString = null;
        }
        Settings.Secure.putString(contentResolver, "nfc_payment_default_component", flattenToString);
        this.refresh();
    }
    
    void setForegroundMode(final boolean b) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "nfc_payment_foreground", (int)(b ? 1 : 0));
    }
    
    public interface Callback
    {
        void onPaymentAppsChanged();
    }
    
    public static class PaymentAppInfo
    {
        Drawable banner;
        public ComponentName componentName;
        CharSequence description;
        boolean isDefault;
        public CharSequence label;
        public ComponentName settingsComponent;
    }
    
    private class SettingsPackageMonitor extends PackageMonitor
    {
        public void onPackageAdded(final String s, final int n) {
            PaymentBackend.this.mHandler.obtainMessage().sendToTarget();
        }
        
        public void onPackageAppeared(final String s, final int n) {
            PaymentBackend.this.mHandler.obtainMessage().sendToTarget();
        }
        
        public void onPackageDisappeared(final String s, final int n) {
            PaymentBackend.this.mHandler.obtainMessage().sendToTarget();
        }
        
        public void onPackageRemoved(final String s, final int n) {
            PaymentBackend.this.mHandler.obtainMessage().sendToTarget();
        }
    }
}
