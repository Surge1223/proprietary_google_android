package com.android.settings.nfc;

import android.view.ViewGroup;
import android.view.View;
import android.view.MenuItem;
import android.content.Intent;
import android.view.MenuInflater;
import android.view.Menu;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.os.Bundle;
import android.content.res.Resources;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public class PaymentSettings extends SettingsPreferenceFragment implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private PaymentBackend mPaymentBackend;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (context.getPackageManager().hasSystemFeature("android.hardware.nfc")) {
                    return nonIndexableKeys;
                }
                nonIndexableKeys.add("payment");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final Resources resources = context.getResources();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.key = "payment";
                searchIndexableRaw.title = resources.getString(2131888376);
                searchIndexableRaw.screenTitle = resources.getString(2131888376);
                searchIndexableRaw.keywords = resources.getString(2131887981);
                list.add(searchIndexableRaw);
                return list;
            }
        };
    }
    
    @Override
    public int getMetricsCategory() {
        return 70;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082793;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mPaymentBackend = new PaymentBackend((Context)this.getActivity());
        this.setHasOptionsMenu(true);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        final List<PaymentBackend.PaymentAppInfo> paymentAppInfos = this.mPaymentBackend.getPaymentAppInfos();
        if (paymentAppInfos != null && paymentAppInfos.size() > 0) {
            final NfcPaymentPreference nfcPaymentPreference = new NfcPaymentPreference(this.getPrefContext(), this.mPaymentBackend);
            nfcPaymentPreference.setKey("payment");
            preferenceScreen.addPreference(nfcPaymentPreference);
            preferenceScreen.addPreference(new NfcForegroundPreference(this.getPrefContext(), this.mPaymentBackend));
        }
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        final MenuItem add = menu.add(2131888370);
        add.setIntent(new Intent((Context)this.getActivity(), (Class)HowItWorks.class));
        add.setShowAsActionFlags(0);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mPaymentBackend.onPause();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mPaymentBackend.onResume();
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        final ViewGroup viewGroup = (ViewGroup)this.getListView().getParent();
        final View inflate = this.getActivity().getLayoutInflater().inflate(2131558616, viewGroup, false);
        viewGroup.addView(inflate);
        this.setEmptyView(inflate);
    }
}
