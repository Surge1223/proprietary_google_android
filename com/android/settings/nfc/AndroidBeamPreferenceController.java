package com.android.settings.nfc;

import android.support.v7.preference.Preference;
import com.android.settingslib.RestrictedPreference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.nfc.NfcAdapter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class AndroidBeamPreferenceController extends BasePreferenceController implements LifecycleObserver, OnPause, OnResume
{
    public static final String KEY_ANDROID_BEAM_SETTINGS = "android_beam_settings";
    private NfcAirplaneModeObserver mAirplaneModeObserver;
    private AndroidBeamEnabler mAndroidBeamEnabler;
    private final NfcAdapter mNfcAdapter;
    
    public AndroidBeamPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mNfcAdapter = NfcAdapter.getDefaultAdapter(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (!this.isAvailable()) {
            this.mAndroidBeamEnabler = null;
            return;
        }
        final RestrictedPreference restrictedPreference = (RestrictedPreference)preferenceScreen.findPreference(this.getPreferenceKey());
        this.mAndroidBeamEnabler = new AndroidBeamEnabler(this.mContext, restrictedPreference);
        if (!NfcPreferenceController.isToggleableInAirplaneMode(this.mContext)) {
            this.mAirplaneModeObserver = new NfcAirplaneModeObserver(this.mContext, this.mNfcAdapter, restrictedPreference);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mNfcAdapter != null) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public void onPause() {
        if (this.mAirplaneModeObserver != null) {
            this.mAirplaneModeObserver.unregister();
        }
        if (this.mAndroidBeamEnabler != null) {
            this.mAndroidBeamEnabler.pause();
        }
    }
    
    @Override
    public void onResume() {
        if (this.mAirplaneModeObserver != null) {
            this.mAirplaneModeObserver.register();
        }
        if (this.mAndroidBeamEnabler != null) {
            this.mAndroidBeamEnabler.resume();
        }
    }
}
