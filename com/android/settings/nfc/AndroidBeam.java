package com.android.settings.nfc;

import android.widget.Switch;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import android.os.UserManager;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settingslib.HelpUtils;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.Context;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.view.View;
import android.nfc.NfcAdapter;
import com.android.settings.widget.SwitchBar;
import com.android.settings.core.InstrumentedFragment;

public class AndroidBeam extends InstrumentedFragment implements OnSwitchChangeListener
{
    private boolean mBeamDisallowedByBase;
    private boolean mBeamDisallowedByOnlyAdmin;
    private NfcAdapter mNfcAdapter;
    private CharSequence mOldActivityTitle;
    private SwitchBar mSwitchBar;
    private View mView;
    
    @Override
    public int getMetricsCategory() {
        return 69;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        this.mOldActivityTitle = settingsActivity.getActionBar().getTitle();
        this.mSwitchBar = settingsActivity.getSwitchBar();
        if (this.mBeamDisallowedByOnlyAdmin) {
            this.mSwitchBar.hide();
        }
        else {
            this.mSwitchBar.setChecked(!this.mBeamDisallowedByBase && this.mNfcAdapter.isNdefPushEnabled());
            this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            this.mSwitchBar.setEnabled(this.mBeamDisallowedByBase ^ true);
            this.mSwitchBar.show();
        }
        settingsActivity.setTitle(2131886312);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mNfcAdapter = NfcAdapter.getDefaultAdapter((Context)this.getActivity());
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        HelpUtils.prepareHelpMenuItem(this.getActivity(), menu, 2131887773, this.getClass().getName());
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.getActivity(), "no_outgoing_beam", UserHandle.myUserId());
        UserManager.get((Context)this.getActivity());
        this.mBeamDisallowedByBase = RestrictedLockUtils.hasBaseUserRestriction((Context)this.getActivity(), "no_outgoing_beam", UserHandle.myUserId());
        if (!this.mBeamDisallowedByBase && checkIfRestrictionEnforced != null) {
            new ActionDisabledByAdminDialogHelper(this.getActivity()).prepareDialogBuilder("no_outgoing_beam", checkIfRestrictionEnforced).show();
            this.mBeamDisallowedByOnlyAdmin = true;
            return new View(this.getContext());
        }
        return this.mView = layoutInflater.inflate(2131558444, viewGroup, false);
    }
    
    public void onDestroyView() {
        super.onDestroyView();
        if (this.mOldActivityTitle != null) {
            this.getActivity().getActionBar().setTitle(this.mOldActivityTitle);
        }
        if (!this.mBeamDisallowedByOnlyAdmin) {
            this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            this.mSwitchBar.hide();
        }
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean checked) {
        this.mSwitchBar.setEnabled(false);
        boolean b;
        if (checked) {
            b = this.mNfcAdapter.enableNdefPush();
        }
        else {
            b = this.mNfcAdapter.disableNdefPush();
        }
        if (b) {
            this.mSwitchBar.setChecked(checked);
        }
        this.mSwitchBar.setEnabled(true);
    }
}
