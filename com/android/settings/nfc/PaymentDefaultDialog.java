package com.android.settings.nfc;

import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.content.DialogInterface;
import com.android.internal.app.AlertController$AlertParams;
import java.util.Iterator;
import android.util.Log;
import android.content.ComponentName;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public final class PaymentDefaultDialog extends AlertActivity implements DialogInterface$OnClickListener
{
    private PaymentBackend mBackend;
    private ComponentName mNewDefault;
    
    private boolean buildDialog(final ComponentName mNewDefault, final String s) {
        if (mNewDefault == null || s == null) {
            Log.e("PaymentDefaultDialog", "Component or category are null");
            return false;
        }
        if (!"payment".equals(s)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Don't support defaults for category ");
            sb.append(s);
            Log.e("PaymentDefaultDialog", sb.toString());
            return false;
        }
        PaymentBackend.PaymentAppInfo paymentAppInfo = null;
        PaymentBackend.PaymentAppInfo paymentAppInfo2 = null;
        for (final PaymentBackend.PaymentAppInfo paymentAppInfo3 : this.mBackend.getPaymentAppInfos()) {
            if (mNewDefault.equals((Object)paymentAppInfo3.componentName)) {
                paymentAppInfo = paymentAppInfo3;
            }
            if (paymentAppInfo3.isDefault) {
                paymentAppInfo2 = paymentAppInfo3;
            }
        }
        if (paymentAppInfo == null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Component ");
            sb2.append(mNewDefault);
            sb2.append(" is not a registered payment service.");
            Log.e("PaymentDefaultDialog", sb2.toString());
            return false;
        }
        final ComponentName defaultPaymentApp = this.mBackend.getDefaultPaymentApp();
        if (defaultPaymentApp != null && defaultPaymentApp.equals((Object)mNewDefault)) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Component ");
            sb3.append(mNewDefault);
            sb3.append(" is already default.");
            Log.e("PaymentDefaultDialog", sb3.toString());
            return false;
        }
        this.mNewDefault = mNewDefault;
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        mAlertParams.mTitle = this.getString(2131888375);
        if (paymentAppInfo2 == null) {
            mAlertParams.mMessage = String.format(this.getString(2131888373), this.sanitizePaymentAppCaption(paymentAppInfo.label.toString()));
        }
        else {
            mAlertParams.mMessage = String.format(this.getString(2131888374), this.sanitizePaymentAppCaption(paymentAppInfo.label.toString()), this.sanitizePaymentAppCaption(paymentAppInfo2.label.toString()));
        }
        mAlertParams.mPositiveButtonText = this.getString(2131890232);
        mAlertParams.mNegativeButtonText = this.getString(2131888406);
        mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
        ((PaymentDefaultDialog)(mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this)).setupAlert();
        return true;
    }
    
    private String sanitizePaymentAppCaption(String trim) {
        trim = trim.replace('\n', ' ').replace('\r', ' ').trim();
        if (trim.length() > 40) {
            return trim.substring(0, 40);
        }
        return trim;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            this.mBackend.setDefaultPaymentApp(this.mNewDefault);
            this.setResult(-1);
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mBackend = new PaymentBackend((Context)this);
        final Intent intent = this.getIntent();
        final ComponentName componentName = (ComponentName)intent.getParcelableExtra("component");
        final String stringExtra = intent.getStringExtra("category");
        this.setResult(0);
        if (!this.buildDialog(componentName, stringExtra)) {
            this.finish();
        }
    }
}
