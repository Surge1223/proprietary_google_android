package com.android.settings;

import com.android.settingslib.datetime.ZoneGetter;
import java.util.Calendar;
import android.os.UserManager;
import android.provider.SearchIndexableResource;
import com.android.settings.search.BaseSearchIndexProvider;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.datetime.TimeChangeListenerMixin;
import com.android.settings.datetime.TimeZonePreferenceController;
import com.android.settings.datetime.AutoTimeFormatPreferenceController;
import com.android.settings.datetime.AutoTimePreferenceController;
import com.android.settings.datetime.UpdateTimeAndDateCallback;
import com.android.settings.datetime.AutoTimeZonePreferenceController;
import com.android.settings.datetime.TimeFormatPreferenceController;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.datetime.TimePreferenceController;
import com.android.settings.datetime.DatePreferenceController;
import com.android.settings.dashboard.DashboardFragment;

public class DateTimeSettings extends DashboardFragment implements DatePreferenceHost, TimePreferenceHost
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new DateTimeSettings.SummaryProvider((Context)activity, summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new DateTimeSearchIndexProvider();
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<TimeFormatPreferenceController> list = new ArrayList<TimeFormatPreferenceController>();
        final Activity activity = this.getActivity();
        final boolean booleanExtra = activity.getIntent().getBooleanExtra("firstRun", false);
        final AutoTimeZonePreferenceController autoTimeZonePreferenceController = new AutoTimeZonePreferenceController((Context)activity, this, booleanExtra);
        final AutoTimePreferenceController autoTimePreferenceController = new AutoTimePreferenceController((Context)activity, this);
        final AutoTimeFormatPreferenceController autoTimeFormatPreferenceController = new AutoTimeFormatPreferenceController((Context)activity, this);
        list.add((TimeFormatPreferenceController)autoTimeZonePreferenceController);
        list.add((TimeFormatPreferenceController)autoTimePreferenceController);
        list.add((TimeFormatPreferenceController)autoTimeFormatPreferenceController);
        list.add(new TimeFormatPreferenceController((Context)activity, this, booleanExtra));
        list.add((TimeFormatPreferenceController)new TimeZonePreferenceController((Context)activity, autoTimeZonePreferenceController));
        list.add((TimeFormatPreferenceController)new TimePreferenceController((Context)activity, (TimePreferenceController.TimePreferenceHost)this, autoTimePreferenceController));
        list.add((TimeFormatPreferenceController)new DatePreferenceController((Context)activity, (DatePreferenceController.DatePreferenceHost)this, autoTimePreferenceController));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 1: {
                return 608;
            }
            case 0: {
                return 607;
            }
        }
    }
    
    @Override
    protected String getLogTag() {
        return "DateTimeSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 38;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082746;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.getLifecycle().addObserver(new TimeChangeListenerMixin(context, this));
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 1: {
                return (Dialog)this.use(TimePreferenceController.class).buildTimePicker(this.getActivity());
            }
            case 0: {
                return (Dialog)this.use(DatePreferenceController.class).buildDatePicker(this.getActivity());
            }
        }
    }
    
    @Override
    public void showDatePicker() {
        this.showDialog(0);
    }
    
    @Override
    public void showTimePicker() {
        this.removeDialog(1);
        this.showDialog(1);
    }
    
    @Override
    public void updateTimeAndDateDisplay(final Context context) {
        this.updatePreferenceStates();
    }
    
    private static class DateTimeSearchIndexProvider extends BaseSearchIndexProvider
    {
        @Override
        public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
            final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
            if (UserManager.isDeviceInDemoMode(context)) {
                return list;
            }
            final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
            searchIndexableResource.xmlResId = 2132082746;
            list.add(searchIndexableResource);
            return list;
        }
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                final Calendar instance = Calendar.getInstance();
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, ZoneGetter.getTimeZoneOffsetAndName(this.mContext, instance.getTimeZone(), instance.getTime()));
            }
        }
    }
}
