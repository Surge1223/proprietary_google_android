package com.android.settings;

import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.core.SubSettingLauncher;
import android.content.DialogInterface$OnDismissListener;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.Bundle;
import android.telephony.euicc.EuiccManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import android.view.View$OnScrollChangeListener;
import android.os.Environment;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.pm.PackageManager;
import android.accounts.AuthenticatorDescription;
import android.accounts.Account;
import java.util.List;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.content.pm.PackageManager;
import android.content.res.Resources$NotFoundException;
import android.util.Log;
import android.content.pm.UserInfo;
import android.view.LayoutInflater;
import android.accounts.AccountManager;
import android.os.UserHandle;
import android.widget.LinearLayout;
import android.os.UserManager;
import android.content.DialogInterface;
import android.os.SystemProperties;
import android.widget.TextView;
import android.view.ViewGroup;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.ScrollView;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.view.View;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import com.android.settings.core.InstrumentedFragment;

public class MasterClear extends InstrumentedFragment implements ViewTreeObserver$OnGlobalLayoutListener
{
    static final int CREDENTIAL_CONFIRM_REQUEST = 56;
    static final int KEYGUARD_REQUEST = 55;
    private View mContentView;
    CheckBox mEsimStorage;
    private View mEsimStorageContainer;
    CheckBox mExternalStorage;
    private View mExternalStorageContainer;
    Button mInitiateButton;
    protected final View.OnClickListener mInitiateListener;
    ScrollView mScrollView;
    
    public MasterClear() {
        this.mInitiateListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Context context = view.getContext();
                if (Utils.isDemoUser(context)) {
                    final ComponentName deviceOwnerComponent = Utils.getDeviceOwnerComponent(context);
                    if (deviceOwnerComponent != null) {
                        context.startActivity(new Intent().setPackage(deviceOwnerComponent.getPackageName()).setAction("android.intent.action.FACTORY_RESET"));
                    }
                    return;
                }
                if (MasterClear.this.runKeyguardConfirmation(55)) {
                    return;
                }
                final Intent accountConfirmationIntent = MasterClear.this.getAccountConfirmationIntent();
                if (accountConfirmationIntent != null) {
                    MasterClear.this.showAccountCredentialConfirmation(accountConfirmationIntent);
                }
                else {
                    MasterClear.this.showFinalConfirmation();
                }
            }
        };
    }
    
    private void getContentDescription(final View view, final StringBuffer sb) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                this.getContentDescription(viewGroup.getChildAt(i), sb);
            }
        }
        else if (view instanceof TextView) {
            sb.append(((TextView)view).getText());
            sb.append(",");
        }
    }
    
    private boolean isExtStorageEncrypted() {
        return "".equals(SystemProperties.get("vold.decrypt")) ^ true;
    }
    
    private void loadAccountList(final UserManager userManager) {
        final View viewById = this.mContentView.findViewById(2131361804);
        final LinearLayout linearLayout = (LinearLayout)this.mContentView.findViewById(2131361803);
        linearLayout.removeAllViews();
        final Activity activity = this.getActivity();
        final List profiles = userManager.getProfiles(UserHandle.myUserId());
        final int size = profiles.size();
        final AccountManager value = AccountManager.get((Context)activity);
        final LayoutInflater layoutInflater = (LayoutInflater)((Context)activity).getSystemService("layout_inflater");
        int n = 0;
        for (int i = 0; i < size; ++i) {
            final UserInfo userInfo = profiles.get(i);
            final int id = userInfo.id;
            final UserHandle userHandle = new UserHandle(id);
            final Account[] accountsAsUser = value.getAccountsAsUser(id);
            final int length = accountsAsUser.length;
            if (length != 0) {
                final AuthenticatorDescription[] authenticatorTypesAsUser = AccountManager.get((Context)activity).getAuthenticatorTypesAsUser(id);
                final int length2 = authenticatorTypesAsUser.length;
                if (size > 1) {
                    final View inflateCategoryHeader = Utils.inflateCategoryHeader(layoutInflater, (ViewGroup)linearLayout);
                    final TextView textView = (TextView)inflateCategoryHeader.findViewById(16908310);
                    int text;
                    if (userInfo.isManagedProfile()) {
                        text = R.string.category_work;
                    }
                    else {
                        text = R.string.category_personal;
                    }
                    textView.setText(text);
                    linearLayout.addView(inflateCategoryHeader);
                }
                for (final Account account : accountsAsUser) {
                    Object userBadgedIcon = null;
                    for (int k = 0; k < length2; ++k) {
                        if (account.type.equals(authenticatorTypesAsUser[k].type)) {
                            userBadgedIcon = authenticatorTypesAsUser[k];
                            break;
                        }
                    }
                    if (userBadgedIcon == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("No descriptor for account name=");
                        sb.append(account.name);
                        sb.append(" type=");
                        sb.append(account.type);
                        Log.w("MasterClear", sb.toString());
                    }
                    else {
                        Drawable drawable = null;
                        Label_0541: {
                            Label_0494: {
                                try {
                                    try {
                                        if (((AuthenticatorDescription)userBadgedIcon).iconId != 0) {
                                            try {
                                                final Context packageContextAsUser = ((Context)activity).createPackageContextAsUser(((AuthenticatorDescription)userBadgedIcon).packageName, 0, userHandle);
                                                final PackageManager packageManager = ((Context)activity).getPackageManager();
                                                try {
                                                    drawable = (Drawable)(userBadgedIcon = packageManager.getUserBadgedIcon(packageContextAsUser.getDrawable(((AuthenticatorDescription)userBadgedIcon).iconId), userHandle));
                                                }
                                                catch (Resources$NotFoundException drawable) {}
                                                catch (PackageManager$NameNotFoundException ex) {}
                                            }
                                            catch (PackageManager$NameNotFoundException drawable) {
                                                break Label_0494;
                                            }
                                        }
                                        userBadgedIcon = drawable;
                                        break Label_0541;
                                    }
                                    catch (Resources$NotFoundException ex2) {}
                                    final Drawable drawable2 = null;
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Invalid icon id for account type ");
                                    sb2.append(((AuthenticatorDescription)userBadgedIcon).type);
                                    Log.w("MasterClear", sb2.toString(), (Throwable)drawable);
                                    userBadgedIcon = drawable2;
                                    break Label_0541;
                                }
                                catch (PackageManager$NameNotFoundException ex3) {}
                            }
                            final Drawable drawable3 = null;
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Bad package name for account type ");
                            sb3.append(((AuthenticatorDescription)userBadgedIcon).type);
                            Log.w("MasterClear", sb3.toString());
                            userBadgedIcon = drawable3;
                        }
                        Object defaultActivityIcon = userBadgedIcon;
                        if (userBadgedIcon == null) {
                            defaultActivityIcon = ((Context)activity).getPackageManager().getDefaultActivityIcon();
                        }
                        final View inflate = layoutInflater.inflate(2131558610, (ViewGroup)linearLayout, false);
                        ((ImageView)inflate.findViewById(16908294)).setImageDrawable((Drawable)defaultActivityIcon);
                        ((TextView)inflate.findViewById(16908310)).setText((CharSequence)account.name);
                        linearLayout.addView(inflate);
                    }
                }
                n += length;
            }
        }
        boolean b = true;
        if (n > 0) {
            viewById.setVisibility(0);
            linearLayout.setVisibility(0);
        }
        final boolean b2 = false;
        final View viewById2 = this.mContentView.findViewById(2131362427);
        if (userManager.getUserCount() - size <= 0) {
            b = false;
        }
        int visibility;
        if (b) {
            visibility = (b2 ? 1 : 0);
        }
        else {
            visibility = 8;
        }
        viewById2.setVisibility(visibility);
    }
    
    private boolean runKeyguardConfirmation(final int n) {
        return new ChooseLockSettingsHelper(this.getActivity(), this).launchConfirmationActivity(n, this.getActivity().getResources().getText(2131888246));
    }
    
    void establishInitialState() {
        (this.mInitiateButton = (Button)this.mContentView.findViewById(2131362271)).setOnClickListener(this.mInitiateListener);
        this.mExternalStorageContainer = this.mContentView.findViewById(2131362123);
        this.mExternalStorage = (CheckBox)this.mContentView.findViewById(2131362122);
        this.mEsimStorageContainer = this.mContentView.findViewById(2131362120);
        this.mEsimStorage = (CheckBox)this.mContentView.findViewById(2131362119);
        if (this.mScrollView != null) {
            this.mScrollView.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        this.mScrollView = (ScrollView)this.mContentView.findViewById(2131362357);
        final boolean externalStorageEmulated = Environment.isExternalStorageEmulated();
        if (!externalStorageEmulated && (Environment.isExternalStorageRemovable() || !this.isExtStorageEncrypted())) {
            this.mExternalStorageContainer.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    MasterClear.this.mExternalStorage.toggle();
                }
            });
        }
        else {
            this.mExternalStorageContainer.setVisibility(8);
            this.mContentView.findViewById(2131362124).setVisibility(8);
            this.mContentView.findViewById(2131361863).setVisibility(0);
            this.mExternalStorage.setChecked(externalStorageEmulated ^ true);
        }
        if (this.showWipeEuicc()) {
            if (this.showWipeEuiccCheckbox()) {
                ((TextView)this.mContentView.findViewById(2131362121)).setText(2131887597);
                this.mEsimStorageContainer.setVisibility(0);
                this.mEsimStorageContainer.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        MasterClear.this.mEsimStorage.toggle();
                    }
                });
            }
            else {
                this.mContentView.findViewById(2131361862).setVisibility(0);
                this.mContentView.findViewById(2131362402).setVisibility(0);
                this.mEsimStorage.setChecked(true);
            }
        }
        this.loadAccountList((UserManager)this.getActivity().getSystemService("user"));
        final StringBuffer contentDescription = new StringBuffer();
        final View viewById = this.mContentView.findViewById(2131362356);
        this.getContentDescription(viewById, contentDescription);
        viewById.setContentDescription((CharSequence)contentDescription);
        this.mScrollView.setOnScrollChangeListener((View$OnScrollChangeListener)new View$OnScrollChangeListener() {
            public void onScrollChange(final View view, final int n, final int n2, final int n3, final int n4) {
                if (view instanceof ScrollView && MasterClear.this.hasReachedBottom((ScrollView)view)) {
                    MasterClear.this.mInitiateButton.setEnabled(true);
                    MasterClear.this.mScrollView.setOnScrollChangeListener((View$OnScrollChangeListener)null);
                }
            }
        });
        this.mScrollView.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
    }
    
    Intent getAccountConfirmationIntent() {
        final Activity activity = this.getActivity();
        final String string = ((Context)activity).getString(2131886239);
        final String string2 = ((Context)activity).getString(2131886225);
        final String string3 = ((Context)activity).getString(2131886224);
        if (!TextUtils.isEmpty((CharSequence)string) && !TextUtils.isEmpty((CharSequence)string2) && !TextUtils.isEmpty((CharSequence)string3)) {
            final Account[] accountsByType = AccountManager.get((Context)activity).getAccountsByType(string);
            if (accountsByType != null && accountsByType.length > 0) {
                final Intent setComponent = new Intent().setPackage(string2).setComponent(new ComponentName(string2, string3));
                final ResolveInfo resolveActivity = ((Context)activity).getPackageManager().resolveActivity(setComponent, 0);
                if (resolveActivity != null && resolveActivity.activityInfo != null && string2.equals(resolveActivity.activityInfo.packageName)) {
                    return setComponent;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to resolve Activity: ");
                sb.append(string2);
                sb.append("/");
                sb.append(string3);
                Log.i("MasterClear", sb.toString());
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("No ");
                sb2.append(string);
                sb2.append(" accounts installed!");
                Log.d("MasterClear", sb2.toString());
            }
            return null;
        }
        Log.i("MasterClear", "Resources not set for account confirmation.");
        return null;
    }
    
    public int getMetricsCategory() {
        return 66;
    }
    
    boolean hasReachedBottom(final ScrollView scrollView) {
        if (scrollView.getChildCount() < 1) {
            return true;
        }
        boolean b = false;
        if (scrollView.getChildAt(0).getBottom() - (scrollView.getHeight() + scrollView.getScrollY()) <= 0) {
            b = true;
        }
        return b;
    }
    
    protected boolean isEuiccEnabled(final Context context) {
        return ((EuiccManager)context.getSystemService("euicc")).isEnabled();
    }
    
    boolean isValidRequestCode(final int n) {
        return n == 55 || n == 56;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        this.onActivityResultInternal(n, n2, intent);
    }
    
    void onActivityResultInternal(final int n, final int n2, Intent accountConfirmationIntent) {
        if (!this.isValidRequestCode(n)) {
            return;
        }
        if (n2 != -1) {
            this.establishInitialState();
            return;
        }
        if (56 != n) {
            accountConfirmationIntent = this.getAccountConfirmationIntent();
            if (accountConfirmationIntent != null) {
                this.showAccountCredentialConfirmation(accountConfirmationIntent);
                return;
            }
        }
        this.showFinalConfirmation();
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131888246);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final Context context = this.getContext();
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced(context, "no_factory_reset", UserHandle.myUserId());
        if ((!UserManager.get(context).isAdminUser() || RestrictedLockUtils.hasBaseUserRestriction(context, "no_factory_reset", UserHandle.myUserId())) && !Utils.isDemoUser(context)) {
            return layoutInflater.inflate(2131558612, (ViewGroup)null);
        }
        if (checkIfRestrictionEnforced != null) {
            new ActionDisabledByAdminDialogHelper(this.getActivity()).prepareDialogBuilder("no_factory_reset", checkIfRestrictionEnforced).setOnDismissListener((DialogInterface$OnDismissListener)new _$$Lambda$MasterClear$Z9cDw51Fmk6mKl61MZqDIXg0_34(this)).show();
            return new View(this.getContext());
        }
        this.mContentView = layoutInflater.inflate(2131558609, (ViewGroup)null);
        this.establishInitialState();
        return this.mContentView;
    }
    
    public void onGlobalLayout() {
        this.mInitiateButton.setEnabled(this.hasReachedBottom(this.mScrollView));
    }
    
    void showAccountCredentialConfirmation(final Intent intent) {
        this.startActivityForResult(intent, 56);
    }
    
    void showFinalConfirmation() {
        final Bundle arguments = new Bundle();
        arguments.putBoolean("erase_sd", this.mExternalStorage.isChecked());
        arguments.putBoolean("erase_esim", this.mEsimStorage.isChecked());
        new SubSettingLauncher(this.getContext()).setDestination(MasterClearConfirm.class.getName()).setArguments(arguments).setTitle(2131888233).setSourceMetricsCategory(this.getMetricsCategory()).launch();
    }
    
    boolean showWipeEuicc() {
        final Context context = this.getContext();
        final boolean euiccEnabled = this.isEuiccEnabled(context);
        boolean b = false;
        if (!euiccEnabled) {
            return false;
        }
        final ContentResolver contentResolver = context.getContentResolver();
        if (Settings.Global.getInt(contentResolver, "euicc_provisioned", 0) != 0 || Settings.Global.getInt(contentResolver, "development_settings_enabled", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    boolean showWipeEuiccCheckbox() {
        return SystemProperties.getBoolean("masterclear.allow_retain_esim_profiles_after_fdr", false);
    }
}
