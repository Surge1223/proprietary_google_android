package com.android.settings;

import android.graphics.PorterDuff.Mode;
import android.widget.TextView;
import android.util.AttributeSet;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Context;
import android.widget.RadioButton;

public class RestrictedRadioButton extends RadioButton
{
    private Context mContext;
    private boolean mDisabledByAdmin;
    private RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    
    public RestrictedRadioButton(final Context context) {
        this(context, null);
    }
    
    public RestrictedRadioButton(final Context context, final AttributeSet set) {
        this(context, set, 16842878);
    }
    
    public RestrictedRadioButton(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public RestrictedRadioButton(final Context mContext, final AttributeSet set, final int n, final int n2) {
        super(mContext, set, n, n2);
        this.mContext = mContext;
    }
    
    public boolean isDisabledByAdmin() {
        return this.mDisabledByAdmin;
    }
    
    public boolean performClick() {
        if (this.mDisabledByAdmin) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mEnforcedAdmin);
            return true;
        }
        return super.performClick();
    }
    
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin) {
        final boolean mDisabledByAdmin = mEnforcedAdmin != null;
        this.mEnforcedAdmin = mEnforcedAdmin;
        if (this.mDisabledByAdmin != mDisabledByAdmin) {
            this.mDisabledByAdmin = mDisabledByAdmin;
            RestrictedLockUtils.setTextViewAsDisabledByAdmin(this.mContext, (TextView)this, this.mDisabledByAdmin);
            if (this.mDisabledByAdmin) {
                this.getButtonDrawable().setColorFilter(this.mContext.getColor(R.color.disabled_text_color), PorterDuff.Mode.MULTIPLY);
            }
            else {
                this.getButtonDrawable().clearColorFilter();
            }
        }
    }
}
