package com.android.settings;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v7.preference.Preference;
import android.os.Parcelable;
import android.content.ContentResolver;
import android.provider.Settings;
import android.view.View;
import android.os.Handler;
import android.util.AttributeSet;
import android.content.Context;
import android.database.ContentObserver;
import android.widget.SeekBar;
import android.hardware.input.InputManager;
import android.widget.SeekBar$OnSeekBarChangeListener;

public class PointerSpeedPreference extends SeekBarDialogPreference implements SeekBar$OnSeekBarChangeListener
{
    private final InputManager mIm;
    private int mOldSpeed;
    private boolean mRestoredOldState;
    private SeekBar mSeekBar;
    private ContentObserver mSpeedObserver;
    private boolean mTouchInProgress;
    
    public PointerSpeedPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mSpeedObserver = new ContentObserver(new Handler()) {
            public void onChange(final boolean b) {
                PointerSpeedPreference.this.onSpeedChanged();
            }
        };
        this.mIm = (InputManager)this.getContext().getSystemService("input");
    }
    
    private void onSpeedChanged() {
        this.mSeekBar.setProgress(this.mIm.getPointerSpeed(this.getContext()) + 7);
    }
    
    private void restoreOldState() {
        if (this.mRestoredOldState) {
            return;
        }
        this.mIm.tryPointerSpeed(this.mOldSpeed);
        this.mRestoredOldState = true;
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        (this.mSeekBar = SeekBarDialogPreference.getSeekBar(view)).setMax(14);
        this.mOldSpeed = this.mIm.getPointerSpeed(this.getContext());
        this.mSeekBar.setProgress(this.mOldSpeed + 7);
        this.mSeekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)this);
    }
    
    protected void onClick() {
        super.onClick();
        this.getContext().getContentResolver().registerContentObserver(Settings.System.getUriFor("pointer_speed"), true, this.mSpeedObserver);
        this.mRestoredOldState = false;
    }
    
    protected void onDialogClosed(final boolean b) {
        super.onDialogClosed(b);
        final ContentResolver contentResolver = this.getContext().getContentResolver();
        if (b) {
            this.mIm.setPointerSpeed(this.getContext(), this.mSeekBar.getProgress() - 7);
        }
        else {
            this.restoreOldState();
        }
        contentResolver.unregisterContentObserver(this.mSpeedObserver);
    }
    
    public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
        if (!this.mTouchInProgress) {
            this.mIm.tryPointerSpeed(n - 7);
        }
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable != null && parcelable.getClass().equals(SavedState.class)) {
            final SavedState savedState = (SavedState)parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            this.mOldSpeed = savedState.oldSpeed;
            this.mIm.tryPointerSpeed(savedState.progress - 7);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }
    
    protected Parcelable onSaveInstanceState() {
        final Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (this.getDialog() != null && this.getDialog().isShowing()) {
            final SavedState savedState = new SavedState(onSaveInstanceState);
            savedState.progress = this.mSeekBar.getProgress();
            savedState.oldSpeed = this.mOldSpeed;
            this.restoreOldState();
            return (Parcelable)savedState;
        }
        return onSaveInstanceState;
    }
    
    public void onStartTrackingTouch(final SeekBar seekBar) {
        this.mTouchInProgress = true;
    }
    
    public void onStopTrackingTouch(final SeekBar seekBar) {
        this.mTouchInProgress = false;
        this.mIm.tryPointerSpeed(seekBar.getProgress() - 7);
    }
    
    private static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        int oldSpeed;
        int progress;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel) {
            super(parcel);
            this.progress = parcel.readInt();
            this.oldSpeed = parcel.readInt();
        }
        
        public SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.progress);
            parcel.writeInt(this.oldSpeed);
        }
    }
}
