package com.android.settings.users;

import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedSwitchPreference;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class AddUserWhenLockedPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final String mPrefKey;
    private boolean mShouldUpdateUserList;
    private final UserCapabilities mUserCaps;
    
    public AddUserWhenLockedPreferenceController(final Context context, final String mPrefKey, final Lifecycle lifecycle) {
        super(context);
        this.mPrefKey = mPrefKey;
        this.mUserCaps = UserCapabilities.create(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPrefKey;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUserCaps.isAdmin() && (!this.mUserCaps.disallowAddUser() || this.mUserCaps.disallowAddUserSetByAdmin());
    }
    
    @Override
    public void onPause() {
        this.mShouldUpdateUserList = true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final Boolean b = (Boolean)o;
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        int n;
        if (b != null && b) {
            n = 1;
        }
        else {
            n = 0;
        }
        Settings.Global.putInt(contentResolver, "add_users_when_locked", n);
        return true;
    }
    
    @Override
    public void onResume() {
        if (this.mShouldUpdateUserList) {
            this.mUserCaps.updateAddUserCapabilities(this.mContext);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "add_users_when_locked", 0);
        boolean checked = true;
        if (int1 != 1) {
            checked = false;
        }
        restrictedSwitchPreference.setChecked(checked);
        RestrictedLockUtils.EnforcedAdmin enforcedAdmin;
        if (this.mUserCaps.disallowAddUser()) {
            enforcedAdmin = this.mUserCaps.getEnforcedAdmin();
        }
        else {
            enforcedAdmin = null;
        }
        restrictedSwitchPreference.setDisabledByAdmin(enforcedAdmin);
    }
}
