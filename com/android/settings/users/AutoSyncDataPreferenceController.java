package com.android.settings.users;

import android.os.Parcelable;
import android.app.Activity;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.ContentResolver;
import android.util.Log;
import android.app.ActivityManager;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.os.Process;
import android.content.Context;
import android.os.UserManager;
import android.os.UserHandle;
import android.app.Fragment;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AutoSyncDataPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final Fragment mParentFragment;
    protected UserHandle mUserHandle;
    protected final UserManager mUserManager;
    
    public AutoSyncDataPreferenceController(final Context context, final Fragment mParentFragment) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mParentFragment = mParentFragment;
        this.mUserHandle = Process.myUserHandle();
    }
    
    @Override
    public String getPreferenceKey() {
        return "auto_sync_account_data";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.getPreferenceKey().equals(preference.getKey())) {
            final SwitchPreference switchPreference = (SwitchPreference)preference;
            final boolean checked = switchPreference.isChecked();
            switchPreference.setChecked(checked ^ true);
            if (ActivityManager.isUserAMonkey()) {
                Log.d("AutoSyncDataController", "ignoring monkey's attempt to flip sync state");
            }
            else {
                ConfirmAutoSyncChangeFragment.show(this.mParentFragment, checked, this.mUserHandle, switchPreference);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        final boolean managedProfile = this.mUserManager.isManagedProfile();
        boolean b = true;
        if (managedProfile || (!this.mUserManager.isRestrictedProfile() && this.mUserManager.getProfiles(UserHandle.myUserId()).size() != 1)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)preference).setChecked(ContentResolver.getMasterSyncAutomaticallyAsUser(this.mUserHandle.getIdentifier()));
    }
    
    public static class ConfirmAutoSyncChangeFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        boolean mEnabling;
        SwitchPreference mPreference;
        UserHandle mUserHandle;
        
        public static void show(final Fragment fragment, final boolean mEnabling, final UserHandle mUserHandle, final SwitchPreference mPreference) {
            if (!fragment.isAdded()) {
                return;
            }
            final ConfirmAutoSyncChangeFragment confirmAutoSyncChangeFragment = new ConfirmAutoSyncChangeFragment();
            confirmAutoSyncChangeFragment.mEnabling = mEnabling;
            confirmAutoSyncChangeFragment.mUserHandle = mUserHandle;
            confirmAutoSyncChangeFragment.setTargetFragment(fragment, 0);
            confirmAutoSyncChangeFragment.mPreference = mPreference;
            confirmAutoSyncChangeFragment.show(fragment.getFragmentManager(), "confirmAutoSyncChange");
        }
        
        public int getMetricsCategory() {
            return 535;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            if (n == -1) {
                ContentResolver.setMasterSyncAutomaticallyAsUser(this.mEnabling, this.mUserHandle.getIdentifier());
                if (this.mPreference != null) {
                    this.mPreference.setChecked(this.mEnabling);
                }
            }
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            if (bundle != null) {
                this.mEnabling = bundle.getBoolean("enabling");
                this.mUserHandle = (UserHandle)bundle.getParcelable("userHandle");
            }
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            if (!this.mEnabling) {
                alertDialog$Builder.setTitle(2131887227);
                alertDialog$Builder.setMessage(2131887226);
            }
            else {
                alertDialog$Builder.setTitle(2131887229);
                alertDialog$Builder.setMessage(2131887228);
            }
            alertDialog$Builder.setPositiveButton(17039370, (DialogInterface$OnClickListener)this);
            alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
        
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putBoolean("enabling", this.mEnabling);
            bundle.putParcelable("userHandle", (Parcelable)this.mUserHandle);
        }
    }
}
