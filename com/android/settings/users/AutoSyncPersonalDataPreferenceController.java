package com.android.settings.users;

import android.os.UserHandle;
import android.app.Fragment;
import android.content.Context;

public class AutoSyncPersonalDataPreferenceController extends AutoSyncDataPreferenceController
{
    public AutoSyncPersonalDataPreferenceController(final Context context, final Fragment fragment) {
        super(context, fragment);
    }
    
    @Override
    public String getPreferenceKey() {
        return "auto_sync_personal_account_data";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean managedProfile = this.mUserManager.isManagedProfile();
        boolean b = true;
        if (managedProfile || this.mUserManager.isLinkedUser() || this.mUserManager.getProfiles(UserHandle.myUserId()).size() <= 1) {
            b = false;
        }
        return b;
    }
}
