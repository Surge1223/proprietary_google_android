package com.android.settings.users;

import android.content.pm.UserInfo;
import android.content.Intent;
import android.content.SharedPreferences;
import com.android.settings.Utils;
import android.os.UserManager;
import android.os.UserHandle;
import android.content.Context;
import android.content.BroadcastReceiver;

public class ProfileUpdateReceiver extends BroadcastReceiver
{
    private static void copyProfileName(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("profile", 0);
        if (sharedPreferences.contains("name_copied_once")) {
            return;
        }
        final int myUserId = UserHandle.myUserId();
        final UserManager userManager = (UserManager)context.getSystemService("user");
        final String meProfileName = Utils.getMeProfileName(context, false);
        if (meProfileName != null && meProfileName.length() > 0) {
            userManager.setUserName(myUserId, meProfileName);
            sharedPreferences.edit().putBoolean("name_copied_once", true).commit();
        }
    }
    
    public void onReceive(final Context context, final Intent intent) {
        new Thread() {
            @Override
            public void run() {
                UserSettings.copyMeProfilePhoto(context, null);
                copyProfileName(context);
            }
        }.start();
    }
}
