package com.android.settings.users;

import android.app.AlertDialog$Builder;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.app.Dialog;
import android.content.Context;
import android.os.UserHandle;
import android.os.Bundle;
import com.android.settings.security.OwnerInfoPreferenceController;
import android.content.DialogInterface;
import android.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import com.android.internal.widget.LockPatternUtils;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class OwnerInfoSettings extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    private LockPatternUtils mLockPatternUtils;
    private EditText mOwnerInfo;
    private int mUserId;
    private View mView;
    
    private void initView() {
        final String ownerInfo = this.mLockPatternUtils.getOwnerInfo(this.mUserId);
        this.mOwnerInfo = (EditText)this.mView.findViewById(2131362429);
        if (!TextUtils.isEmpty((CharSequence)ownerInfo)) {
            this.mOwnerInfo.setText((CharSequence)ownerInfo);
            this.mOwnerInfo.setSelection(ownerInfo.length());
        }
    }
    
    public static void show(final Fragment fragment) {
        if (!fragment.isAdded()) {
            return;
        }
        final OwnerInfoSettings ownerInfoSettings = new OwnerInfoSettings();
        ownerInfoSettings.setTargetFragment(fragment, 0);
        ownerInfoSettings.show(fragment.getFragmentManager(), "ownerInfo");
    }
    
    public int getMetricsCategory() {
        return 531;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            final String string = this.mOwnerInfo.getText().toString();
            this.mLockPatternUtils.setOwnerInfoEnabled(TextUtils.isEmpty((CharSequence)string) ^ true, this.mUserId);
            this.mLockPatternUtils.setOwnerInfo(string, this.mUserId);
            if (this.getTargetFragment() instanceof OwnerInfoPreferenceController.OwnerInfoCallback) {
                ((OwnerInfoPreferenceController.OwnerInfoCallback)this.getTargetFragment()).onOwnerInfoUpdated();
            }
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mUserId = UserHandle.myUserId();
        this.mLockPatternUtils = new LockPatternUtils((Context)this.getActivity());
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        this.mView = LayoutInflater.from((Context)this.getActivity()).inflate(2131558638, (ViewGroup)null);
        this.initView();
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888549).setView(this.mView).setPositiveButton(2131888889, (DialogInterface$OnClickListener)this).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)this).show();
    }
}
