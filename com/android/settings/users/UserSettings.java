package com.android.settings.users;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.SharedPreferences;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import java.util.HashMap;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.os.Process;
import android.app.Dialog;
import android.support.v7.preference.PreferenceScreen;
import android.content.IntentFilter;
import android.provider.Settings;
import android.view.View;
import java.util.Comparator;
import java.util.Collections;
import com.android.settingslib.RestrictedLockUtils;
import android.util.AttributeSet;
import com.android.settings.Utils;
import android.util.Log;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.app.ActivityManager;
import android.app.Activity;
import com.android.internal.widget.LockPatternUtils;
import java.util.Iterator;
import com.android.internal.util.UserIcons;
import com.android.settingslib.drawable.CircleFramedDrawable;
import java.io.InputStream;
import android.net.Uri;
import java.io.IOException;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract$Contacts;
import android.os.UserHandle;
import android.provider.ContactsContract$Profile;
import android.content.res.Resources;
import android.content.pm.UserInfo;
import android.content.Intent;
import android.os.Message;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.Lifecycle;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.os.UserManager;
import android.support.v7.preference.PreferenceGroup;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.graphics.drawable.Drawable;
import com.android.settingslib.RestrictedPreference;
import android.graphics.Bitmap;
import android.util.SparseArray;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;
import android.content.DialogInterface$OnDismissListener;
import com.android.settings.SettingsPreferenceFragment;

public class UserSettings extends SettingsPreferenceFragment implements DialogInterface$OnDismissListener, OnPreferenceClickListener, View.OnClickListener, Indexable, OnContentChangedCallback
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private static SparseArray<Bitmap> sDarkDefaultUserBitmapCache;
    private RestrictedPreference mAddUser;
    private AddUserWhenLockedPreferenceController mAddUserWhenLockedPreferenceController;
    private int mAddedUserId;
    private boolean mAddingUser;
    private String mAddingUserName;
    private Drawable mDefaultIconDrawable;
    private EditUserInfoController mEditUserInfoController;
    private Handler mHandler;
    private UserPreference mMePreference;
    private int mRemovingUserId;
    private boolean mShouldUpdateUserList;
    private UserCapabilities mUserCaps;
    private BroadcastReceiver mUserChangeReceiver;
    private SparseArray<Bitmap> mUserIcons;
    private PreferenceGroup mUserListCategory;
    private final Object mUserLock;
    private UserManager mUserManager;
    
    static {
        UserSettings.sDarkDefaultUserBitmapCache = (SparseArray<Bitmap>)new SparseArray();
        SUMMARY_PROVIDER_FACTORY = (SummaryLoader.SummaryProviderFactory)_$$Lambda$UserSettings$Eg6plZiaX7G7UUvF4Q46lU8PMRw.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeysFromXml(final Context context, final int n) {
                final List<String> nonIndexableKeysFromXml = super.getNonIndexableKeysFromXml(context, n);
                new AddUserWhenLockedPreferenceController(context, "user_settings_add_users_when_locked", null).updateNonIndexableKeys(nonIndexableKeysFromXml);
                new AutoSyncDataPreferenceController(context, null).updateNonIndexableKeys(nonIndexableKeysFromXml);
                new AutoSyncPersonalDataPreferenceController(context, null).updateNonIndexableKeys(nonIndexableKeysFromXml);
                new AutoSyncWorkDataPreferenceController(context, null).updateNonIndexableKeys(nonIndexableKeysFromXml);
                return nonIndexableKeysFromXml;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082855;
                list.add(searchIndexableResource);
                return list;
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return UserCapabilities.create(context).mEnabled;
            }
        };
    }
    
    public UserSettings() {
        this.mRemovingUserId = -1;
        this.mAddedUserId = 0;
        this.mShouldUpdateUserList = true;
        this.mUserLock = new Object();
        this.mUserIcons = (SparseArray<Bitmap>)new SparseArray();
        this.mEditUserInfoController = new EditUserInfoController();
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case 3: {
                        UserSettings.this.onManageUserClicked(message.arg1, true);
                        break;
                    }
                    case 2: {
                        UserSettings.this.onUserCreated(message.arg1);
                        break;
                    }
                    case 1: {
                        UserSettings.this.updateUserList();
                        break;
                    }
                }
            }
        };
        this.mUserChangeReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.intent.action.USER_REMOVED")) {
                    UserSettings.this.mRemovingUserId = -1;
                }
                else if (intent.getAction().equals("android.intent.action.USER_INFO_CHANGED")) {
                    final int intExtra = intent.getIntExtra("android.intent.extra.user_handle", -1);
                    if (intExtra != -1) {
                        UserSettings.this.mUserIcons.remove(intExtra);
                    }
                }
                UserSettings.this.mHandler.sendEmptyMessage(1);
            }
        };
    }
    
    private void addUserNow(final int n) {
        synchronized (this.mUserLock) {
            this.mAddingUser = true;
            String mAddingUserName;
            if (n == 1) {
                mAddingUserName = this.getString(2131889743);
            }
            else {
                mAddingUserName = this.getString(2131889742);
            }
            this.mAddingUserName = mAddingUserName;
            new Thread() {
                @Override
                public void run() {
                    UserInfo userInfo;
                    if (n == 1) {
                        userInfo = UserSettings.this.createTrustedUser();
                    }
                    else {
                        userInfo = UserSettings.this.createRestrictedProfile();
                    }
                    if (userInfo == null) {
                        UserSettings.this.mAddingUser = false;
                        return;
                    }
                    synchronized (UserSettings.this.mUserLock) {
                        if (n == 1) {
                            UserSettings.this.mHandler.sendEmptyMessage(1);
                            if (!UserSettings.this.mUserCaps.mDisallowSwitchUser) {
                                UserSettings.this.mHandler.sendMessage(UserSettings.this.mHandler.obtainMessage(2, userInfo.id, userInfo.serialNumber));
                            }
                        }
                        else {
                            UserSettings.this.mHandler.sendMessage(UserSettings.this.mHandler.obtainMessage(3, userInfo.id, userInfo.serialNumber));
                        }
                    }
                }
            }.start();
        }
    }
    
    static boolean assignDefaultPhoto(final Context context, final int n) {
        if (context == null) {
            return false;
        }
        ((UserManager)context.getSystemService("user")).setUserIcon(n, getDefaultUserIconAsBitmap(context.getResources(), n));
        return true;
    }
    
    static void copyMeProfilePhoto(final Context context, final UserInfo userInfo) {
        final Uri content_URI = ContactsContract$Profile.CONTENT_URI;
        int n;
        if (userInfo != null) {
            n = userInfo.id;
        }
        else {
            n = UserHandle.myUserId();
        }
        final InputStream openContactPhotoInputStream = ContactsContract$Contacts.openContactPhotoInputStream(context.getContentResolver(), content_URI, true);
        if (openContactPhotoInputStream == null) {
            assignDefaultPhoto(context, n);
            return;
        }
        ((UserManager)context.getSystemService("user")).setUserIcon(n, BitmapFactory.decodeStream(openContactPhotoInputStream));
        try {
            openContactPhotoInputStream.close();
        }
        catch (IOException ex) {}
    }
    
    private UserInfo createRestrictedProfile() {
        final UserInfo restrictedProfile = this.mUserManager.createRestrictedProfile(this.mAddingUserName);
        if (restrictedProfile != null && !assignDefaultPhoto((Context)this.getActivity(), restrictedProfile.id)) {
            return null;
        }
        return restrictedProfile;
    }
    
    private UserInfo createTrustedUser() {
        final UserInfo user = this.mUserManager.createUser(this.mAddingUserName, 0);
        if (user != null && !assignDefaultPhoto((Context)this.getActivity(), user.id)) {
            return null;
        }
        return user;
    }
    
    private Drawable encircle(final Bitmap bitmap) {
        return CircleFramedDrawable.getInstance((Context)this.getActivity(), bitmap);
    }
    
    private void exitGuest() {
        if (!this.mUserCaps.mIsGuest) {
            return;
        }
        this.removeThisUser();
    }
    
    private void finishLoadProfile(final String s) {
        if (this.getActivity() == null) {
            return;
        }
        this.mMePreference.setTitle(this.getString(2131889764, new Object[] { s }));
        final int myUserId = UserHandle.myUserId();
        final Bitmap userIcon = this.mUserManager.getUserIcon(myUserId);
        if (userIcon != null) {
            this.mMePreference.setIcon(this.encircle(userIcon));
            this.mUserIcons.put(myUserId, (Object)userIcon);
        }
    }
    
    private static Bitmap getDefaultUserIconAsBitmap(final Resources resources, final int n) {
        Bitmap convertToBitmap;
        if ((convertToBitmap = (Bitmap)UserSettings.sDarkDefaultUserBitmapCache.get(n)) == null) {
            convertToBitmap = UserIcons.convertToBitmap(UserIcons.getDefaultUserIcon(resources, n, false));
            UserSettings.sDarkDefaultUserBitmapCache.put(n, (Object)convertToBitmap);
        }
        return convertToBitmap;
    }
    
    private Drawable getEncircledDefaultIcon() {
        if (this.mDefaultIconDrawable == null) {
            this.mDefaultIconDrawable = this.encircle(getDefaultUserIconAsBitmap(this.getContext().getResources(), -10000));
        }
        return this.mDefaultIconDrawable;
    }
    
    private int getMaxRealUsers() {
        final int maxSupportedUsers = UserManager.getMaxSupportedUsers();
        final List users = this.mUserManager.getUsers();
        int n = 0;
        final Iterator<UserInfo> iterator = users.iterator();
        while (iterator.hasNext()) {
            int n2 = n;
            if (iterator.next().isManagedProfile()) {
                n2 = n + 1;
            }
            n = n2;
        }
        return maxSupportedUsers + 1 - n;
    }
    
    private boolean hasLockscreenSecurity() {
        return new LockPatternUtils((Context)this.getActivity()).isSecure(UserHandle.myUserId());
    }
    
    private boolean isInitialized(final UserInfo userInfo) {
        return (userInfo.flags & 0x10) != 0x0;
    }
    
    private void launchChooseLockscreen() {
        final Intent intent = new Intent("android.app.action.SET_NEW_PASSWORD");
        intent.putExtra("minimum_quality", 65536);
        this.startActivityForResult(intent, 10);
    }
    
    private void loadIconsAsync(final List<Integer> list) {
        new AsyncTask<List<Integer>, Void, Void>() {
            protected Void doInBackground(final List<Integer>... array) {
                for (final int intValue : array[0]) {
                    Bitmap bitmap;
                    if ((bitmap = UserSettings.this.mUserManager.getUserIcon(intValue)) == null) {
                        bitmap = getDefaultUserIconAsBitmap(UserSettings.this.getContext().getResources(), intValue);
                    }
                    UserSettings.this.mUserIcons.append(intValue, (Object)bitmap);
                }
                return null;
            }
            
            protected void onPostExecute(final Void void1) {
                UserSettings.this.updateUserList();
            }
        }.execute((Object[])new List[] { list });
    }
    
    private void loadProfile() {
        if (this.mUserCaps.mIsGuest) {
            this.mMePreference.setIcon(this.getEncircledDefaultIcon());
            this.mMePreference.setTitle(2131889732);
            return;
        }
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(final Void... array) {
                final UserInfo userInfo = UserSettings.this.mUserManager.getUserInfo(UserHandle.myUserId());
                if (userInfo.iconPath == null || userInfo.iconPath.equals("")) {
                    UserSettings.copyMeProfilePhoto((Context)UserSettings.this.getActivity(), userInfo);
                }
                return userInfo.name;
            }
            
            protected void onPostExecute(final String s) {
                UserSettings.this.finishLoadProfile(s);
            }
        }.execute((Object[])new Void[0]);
    }
    
    private void onAddUserClicked(final int n) {
        synchronized (this.mUserLock) {
            if (this.mRemovingUserId == -1 && !this.mAddingUser) {
                switch (n) {
                    case 2: {
                        if (this.hasLockscreenSecurity()) {
                            this.addUserNow(2);
                            break;
                        }
                        this.showDialog(7);
                        break;
                    }
                    case 1: {
                        this.showDialog(2);
                        break;
                    }
                }
            }
        }
    }
    
    private void onManageUserClicked(final int n, final boolean b) {
        this.mAddingUser = false;
        if (n == -11) {
            final Bundle arguments = new Bundle();
            arguments.putBoolean("guest_user", true);
            new SubSettingLauncher(this.getContext()).setDestination(UserDetailsSettings.class.getName()).setArguments(arguments).setTitle(R.string.user_guest).setSourceMetricsCategory(this.getMetricsCategory()).launch();
            return;
        }
        final UserInfo userInfo = this.mUserManager.getUserInfo(n);
        if (userInfo.isRestricted() && this.mUserCaps.mIsAdmin) {
            final Bundle arguments2 = new Bundle();
            arguments2.putInt("user_id", n);
            arguments2.putBoolean("new_user", b);
            new SubSettingLauncher(this.getContext()).setDestination(RestrictedProfileSettings.class.getName()).setArguments(arguments2).setTitle(2131889752).setSourceMetricsCategory(this.getMetricsCategory()).launch();
        }
        else if (userInfo.id == UserHandle.myUserId()) {
            OwnerInfoSettings.show(this);
        }
        else if (this.mUserCaps.mIsAdmin) {
            final Bundle arguments3 = new Bundle();
            arguments3.putInt("user_id", n);
            new SubSettingLauncher(this.getContext()).setDestination(UserDetailsSettings.class.getName()).setArguments(arguments3).setTitle(userInfo.name).setSourceMetricsCategory(this.getMetricsCategory()).launch();
        }
    }
    
    private void onRemoveUserClicked(final int mRemovingUserId) {
        synchronized (this.mUserLock) {
            if (this.mRemovingUserId == -1 && !this.mAddingUser) {
                this.mRemovingUserId = mRemovingUserId;
                this.showDialog(1);
            }
        }
    }
    
    private void onUserCreated(final int mAddedUserId) {
        this.mAddedUserId = mAddedUserId;
        this.mAddingUser = false;
        if (!this.isResumed()) {
            Log.w("UserSettings", "Cannot show dialog after onPause");
            return;
        }
        if (this.mUserManager.getUserInfo(mAddedUserId).isRestricted()) {
            this.showDialog(4);
        }
        else {
            this.showDialog(3);
        }
    }
    
    private void removeThisUser() {
        if (!this.mUserManager.canSwitchUsers()) {
            Log.w("UserSettings", "Cannot remove current user when switching is disabled");
            return;
        }
        try {
            ActivityManager.getService().switchUser(0);
            ((UserManager)this.getContext().getSystemService((Class)UserManager.class)).removeUser(UserHandle.myUserId());
        }
        catch (RemoteException ex) {
            Log.e("UserSettings", "Unable to remove self user");
        }
    }
    
    private void removeUserNow() {
        if (this.mRemovingUserId == UserHandle.myUserId()) {
            this.removeThisUser();
        }
        else {
            new Thread() {
                @Override
                public void run() {
                    synchronized (UserSettings.this.mUserLock) {
                        UserSettings.this.mUserManager.removeUser(UserSettings.this.mRemovingUserId);
                        UserSettings.this.mHandler.sendEmptyMessage(1);
                    }
                }
            }.start();
        }
    }
    
    private void setPhotoId(final Preference preference, final UserInfo userInfo) {
        final Bitmap bitmap = (Bitmap)this.mUserIcons.get(userInfo.id);
        if (bitmap != null) {
            preference.setIcon(this.encircle(bitmap));
        }
    }
    
    private void switchUserNow(final int n) {
        try {
            ActivityManager.getService().switchUser(n);
        }
        catch (RemoteException ex) {}
    }
    
    private void updateUserList() {
        if (this.getActivity() == null) {
            return;
        }
        final List users = this.mUserManager.getUsers(true);
        final Activity activity = this.getActivity();
        final boolean voiceCapable = Utils.isVoiceCapable((Context)activity);
        final ArrayList<Integer> list = new ArrayList<Integer>();
        final ArrayList<Object> list2 = new ArrayList<Object>();
        int id = -11;
        list2.add(this.mMePreference);
        final Iterator<UserInfo> iterator = users.iterator();
        RestrictedLockUtils.EnforcedAdmin enforcedAdmin;
        while (true) {
            final boolean hasNext = iterator.hasNext();
            enforcedAdmin = null;
            if (!hasNext) {
                break;
            }
            final UserInfo userInfo = iterator.next();
            if (!userInfo.supportsSwitchToByUser()) {
                continue;
            }
            UserPreference mMePreference;
            if (userInfo.id == UserHandle.myUserId()) {
                mMePreference = this.mMePreference;
            }
            else {
                if (userInfo.isGuest()) {
                    id = userInfo.id;
                    continue;
                }
                final boolean b = this.mUserCaps.mIsAdmin && (voiceCapable || userInfo.isRestricted());
                final boolean b2 = this.mUserCaps.mIsAdmin && !voiceCapable && !userInfo.isRestricted() && !userInfo.isGuest();
                final Context prefContext = this.getPrefContext();
                final int id2 = userInfo.id;
                Object o;
                if (b) {
                    o = this;
                }
                else {
                    o = null;
                }
                Object o2;
                if (b2) {
                    o2 = this;
                }
                else {
                    o2 = null;
                }
                mMePreference = new UserPreference(prefContext, null, id2, (View.OnClickListener)o, (View.OnClickListener)o2);
                final StringBuilder sb = new StringBuilder();
                sb.append("id=");
                sb.append(userInfo.id);
                mMePreference.setKey(sb.toString());
                list2.add(mMePreference);
                if (userInfo.isAdmin()) {
                    mMePreference.setSummary(2131889690);
                }
                mMePreference.setTitle(userInfo.name);
                mMePreference.setSelectable(false);
            }
            if (mMePreference == null) {
                continue;
            }
            if (!this.isInitialized(userInfo)) {
                if (userInfo.isRestricted()) {
                    mMePreference.setSummary(2131889762);
                }
                else {
                    mMePreference.setSummary(2131889761);
                }
                if (!this.mUserCaps.mDisallowSwitchUser) {
                    mMePreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
                    mMePreference.setSelectable(true);
                }
            }
            else if (userInfo.isRestricted()) {
                mMePreference.setSummary(2131889763);
            }
            if (userInfo.iconPath != null) {
                if (this.mUserIcons.get(userInfo.id) == null) {
                    list.add(userInfo.id);
                    mMePreference.setIcon(this.getEncircledDefaultIcon());
                }
                else {
                    this.setPhotoId(mMePreference, userInfo);
                }
            }
            else {
                mMePreference.setIcon(this.getEncircledDefaultIcon());
            }
        }
        if (this.mAddingUser) {
            final UserPreference userPreference = new UserPreference(this.getPrefContext(), null, -10, null, null);
            userPreference.setEnabled(false);
            userPreference.setTitle(this.mAddingUserName);
            userPreference.setIcon(this.getEncircledDefaultIcon());
            list2.add(userPreference);
        }
        if (!this.mUserCaps.mIsGuest && (this.mUserCaps.mCanAddGuest || this.mUserCaps.mDisallowAddUserSetByAdmin)) {
            final Context prefContext2 = this.getPrefContext();
            Object o3;
            if (this.mUserCaps.mIsAdmin && voiceCapable) {
                o3 = this;
            }
            else {
                o3 = null;
            }
            final UserPreference userPreference2 = new UserPreference(prefContext2, null, -11, (View.OnClickListener)o3, null);
            userPreference2.setTitle(R.string.user_guest);
            userPreference2.setIcon(this.getEncircledDefaultIcon());
            list2.add(userPreference2);
            if (this.mUserCaps.mDisallowAddUser) {
                userPreference2.setDisabledByAdmin(this.mUserCaps.mEnforcedAdmin);
            }
            else if (this.mUserCaps.mDisallowSwitchUser) {
                userPreference2.setDisabledByAdmin(RestrictedLockUtils.getDeviceOwner((Context)activity));
            }
            else {
                userPreference2.setDisabledByAdmin(null);
            }
            userPreference2.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$UserSettings$wMqzhBHYMbgNNY7TSuzlNB8n9UY(this, id));
        }
        Collections.sort(list2, (Comparator<? super Object>)UserPreference.SERIAL_NUMBER_COMPARATOR);
        this.getActivity().invalidateOptionsMenu();
        if (list.size() > 0) {
            this.loadIconsAsync(list);
        }
        this.mUserListCategory.removeAll();
        if (this.mUserCaps.mCanAddRestrictedProfile) {
            this.mUserListCategory.setTitle(2131889739);
        }
        else {
            this.mUserListCategory.setTitle(null);
        }
        for (final UserPreference userPreference3 : list2) {
            userPreference3.setOrder(Integer.MAX_VALUE);
            this.mUserListCategory.addPreference(userPreference3);
        }
        if ((this.mUserCaps.mCanAddUser || this.mUserCaps.mDisallowAddUserSetByAdmin) && Utils.isDeviceProvisioned((Context)this.getActivity())) {
            final boolean canAddMoreUsers = this.mUserManager.canAddMoreUsers();
            this.mAddUser.setEnabled(canAddMoreUsers && !this.mAddingUser);
            if (!canAddMoreUsers) {
                this.mAddUser.setSummary(this.getString(2131889677, new Object[] { this.getMaxRealUsers() }));
            }
            else {
                this.mAddUser.setSummary(null);
            }
            if (this.mAddUser.isEnabled()) {
                final RestrictedPreference mAddUser = this.mAddUser;
                RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin = enforcedAdmin;
                if (this.mUserCaps.mDisallowAddUser) {
                    mEnforcedAdmin = this.mUserCaps.mEnforcedAdmin;
                }
                mAddUser.setDisabledByAdmin(mEnforcedAdmin);
            }
        }
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 9: {
                return 601;
            }
            case 8: {
                return 600;
            }
            case 7: {
                return 599;
            }
            case 6: {
                return 598;
            }
            case 5: {
                return 594;
            }
            case 4: {
                return 597;
            }
            case 3: {
                return 596;
            }
            case 2: {
                return 595;
            }
            case 1: {
                return 591;
            }
        }
    }
    
    public int getHelpResource() {
        return 2131887838;
    }
    
    public int getMetricsCategory() {
        return 96;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 10) {
            if (n2 != 0 && this.hasLockscreenSecurity()) {
                this.addUserNow(2);
            }
        }
        else {
            this.mEditUserInfoController.onActivityResult(n, n2, intent);
        }
    }
    
    public void onClick(final View view) {
        if (view.getTag() instanceof UserPreference) {
            final int userId = ((UserPreference)view.getTag()).getUserId();
            final int id = view.getId();
            if (id != 2131362354) {
                if (id == 2131362750) {
                    final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced(this.getContext(), "no_remove_user", UserHandle.myUserId());
                    if (checkIfRestrictionEnforced != null) {
                        RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.getContext(), checkIfRestrictionEnforced);
                    }
                    else {
                        this.onRemoveUserClicked(userId);
                    }
                }
            }
            else {
                this.onManageUserClicked(userId, false);
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082855);
        if (Settings.Global.getInt(this.getContext().getContentResolver(), "device_provisioned", 0) == 0) {
            this.getActivity().finish();
            return;
        }
        final Activity activity = this.getActivity();
        this.mAddUserWhenLockedPreferenceController = new AddUserWhenLockedPreferenceController((Context)activity, "user_settings_add_users_when_locked", this.getLifecycle());
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        this.mAddUserWhenLockedPreferenceController.displayPreference(preferenceScreen);
        preferenceScreen.findPreference(this.mAddUserWhenLockedPreferenceController.getPreferenceKey()).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this.mAddUserWhenLockedPreferenceController);
        if (bundle != null) {
            if (bundle.containsKey("adding_user")) {
                this.mAddedUserId = bundle.getInt("adding_user");
            }
            if (bundle.containsKey("removing_user")) {
                this.mRemovingUserId = bundle.getInt("removing_user");
            }
            this.mEditUserInfoController.onRestoreInstanceState(bundle);
        }
        this.mUserCaps = UserCapabilities.create((Context)activity);
        this.mUserManager = (UserManager)((Context)activity).getSystemService("user");
        if (!this.mUserCaps.mEnabled) {
            return;
        }
        final int myUserId = UserHandle.myUserId();
        this.mUserListCategory = (PreferenceGroup)this.findPreference("user_list");
        (this.mMePreference = new UserPreference(this.getPrefContext(), null, myUserId, null, null)).setKey("user_me");
        this.mMePreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        if (this.mUserCaps.mIsAdmin) {
            this.mMePreference.setSummary(2131889690);
        }
        (this.mAddUser = (RestrictedPreference)this.findPreference("user_add")).useAdminDisabledSummary(false);
        if (this.mUserCaps.mCanAddUser && Utils.isDeviceProvisioned((Context)this.getActivity())) {
            this.mAddUser.setVisible(true);
            this.mAddUser.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
            if (!this.mUserCaps.mCanAddRestrictedProfile) {
                this.mAddUser.setTitle(2131889683);
            }
        }
        else {
            this.mAddUser.setVisible(false);
        }
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.USER_REMOVED");
        intentFilter.addAction("android.intent.action.USER_INFO_CHANGED");
        ((Context)activity).registerReceiverAsUser(this.mUserChangeReceiver, UserHandle.ALL, intentFilter, (String)null, this.mHandler);
        this.loadProfile();
        this.updateUserList();
        this.mShouldUpdateUserList = false;
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return null;
        }
        int n2 = 2;
        switch (n) {
            default: {
                return null;
            }
            case 9: {
                return this.mEditUserInfoController.createDialog(this, null, this.mMePreference.getTitle(), 2131888697, (EditUserInfoController.OnContentChangedCallback)this, Process.myUserHandle());
            }
            case 8: {
                return (Dialog)new AlertDialog$Builder((Context)activity).setTitle(2131889730).setMessage(2131889729).setPositiveButton(2131889731, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserSettings.this.exitGuest();
                    }
                }).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
            }
            case 7: {
                return (Dialog)new AlertDialog$Builder((Context)activity).setMessage(2131889741).setPositiveButton(2131889753, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserSettings.this.launchChooseLockscreen();
                    }
                }).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
            }
            case 6: {
                final ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
                final HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("title", this.getString(2131889682));
                hashMap.put("summary", this.getString(2131889681));
                final HashMap<String, String> hashMap2 = new HashMap<String, String>();
                hashMap2.put("title", this.getString(2131889680));
                hashMap2.put("summary", this.getString(2131889679));
                list.add(hashMap);
                list.add(hashMap2);
                final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
                final SimpleAdapter simpleAdapter = new SimpleAdapter(alertDialog$Builder.getContext(), (List)list, 2131558859, new String[] { "title", "summary" }, new int[] { R.id.title, 2131362668 });
                alertDialog$Builder.setTitle(2131889688);
                alertDialog$Builder.setAdapter((ListAdapter)simpleAdapter, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, int n) {
                        final UserSettings this$0 = UserSettings.this;
                        if (n == 0) {
                            n = 1;
                        }
                        else {
                            n = 2;
                        }
                        this$0.onAddUserClicked(n);
                    }
                });
                return (Dialog)alertDialog$Builder.create();
            }
            case 5: {
                return (Dialog)new AlertDialog$Builder((Context)activity).setMessage(2131889692).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
            }
            case 4: {
                return (Dialog)new AlertDialog$Builder((Context)activity).setMessage(2131889759).setPositiveButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserSettings.this.switchUserNow(UserSettings.this.mAddedUserId);
                    }
                }).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
            }
            case 3: {
                return (Dialog)new AlertDialog$Builder((Context)activity).setTitle(2131889758).setMessage(2131889757).setPositiveButton(2131889756, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserSettings.this.switchUserNow(UserSettings.this.mAddedUserId);
                    }
                }).setNegativeButton(2131889755, (DialogInterface$OnClickListener)null).create();
            }
            case 2: {
                final SharedPreferences preferences = this.getActivity().getPreferences(0);
                final boolean boolean1 = preferences.getBoolean("key_add_user_long_message_displayed", false);
                int message;
                if (boolean1) {
                    message = 2131889685;
                }
                else {
                    message = 2131889684;
                }
                if (n == 2) {
                    n2 = 1;
                }
                return (Dialog)new AlertDialog$Builder((Context)activity).setTitle(2131889687).setMessage(message).setPositiveButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserSettings.this.addUserNow(n2);
                        if (!boolean1) {
                            preferences.edit().putBoolean("key_add_user_long_message_displayed", true).apply();
                        }
                    }
                }).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
            }
            case 1: {
                return UserDialogs.createRemoveDialog((Context)this.getActivity(), this.mRemovingUserId, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserSettings.this.removeUserNow();
                    }
                });
            }
        }
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final UserManager userManager = (UserManager)this.getContext().getSystemService((Class)UserManager.class);
        final boolean hasUserRestriction = userManager.hasUserRestriction("no_remove_user");
        final boolean canSwitchUsers = userManager.canSwitchUsers();
        if (!this.mUserCaps.mIsAdmin && (hasUserRestriction ^ true) && canSwitchUsers) {
            menu.add(0, 1, 0, (CharSequence)this.getResources().getString(2131889749, new Object[] { this.mUserManager.getUserName() })).setShowAsAction(0);
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    public void onDestroy() {
        super.onDestroy();
        if (this.mUserCaps != null && this.mUserCaps.mEnabled) {
            this.getActivity().unregisterReceiver(this.mUserChangeReceiver);
        }
    }
    
    @Override
    public void onDialogShowing() {
        super.onDialogShowing();
        this.setOnDismissListener((DialogInterface$OnDismissListener)this);
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        synchronized (this.mUserLock) {
            this.mRemovingUserId = -1;
            this.updateUserList();
        }
    }
    
    public void onLabelChanged(final CharSequence title) {
        this.mMePreference.setTitle(title);
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 1) {
            this.onRemoveUserClicked(UserHandle.myUserId());
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
    
    public void onPause() {
        this.mShouldUpdateUserList = true;
        super.onPause();
    }
    
    public void onPhotoChanged(final Drawable icon) {
        this.mMePreference.setIcon(icon);
    }
    
    public boolean onPreferenceClick(final Preference preference) {
        if (preference == this.mMePreference) {
            if (this.mUserCaps.mIsGuest) {
                this.showDialog(8);
                return true;
            }
            if (this.mUserManager.isLinkedUser()) {
                this.onManageUserClicked(UserHandle.myUserId(), false);
            }
            else {
                this.showDialog(9);
            }
        }
        else if (preference instanceof UserPreference) {
            final UserInfo userInfo = this.mUserManager.getUserInfo(((UserPreference)preference).getUserId());
            if (!this.isInitialized(userInfo)) {
                this.mHandler.sendMessage(this.mHandler.obtainMessage(2, userInfo.id, userInfo.serialNumber));
            }
        }
        else if (preference == this.mAddUser) {
            if (this.mUserCaps.mCanAddRestrictedProfile) {
                this.showDialog(6);
            }
            else {
                this.onAddUserClicked(1);
            }
        }
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (!this.mUserCaps.mEnabled) {
            return;
        }
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (this.mAddUserWhenLockedPreferenceController.isAvailable()) {
            this.mAddUserWhenLockedPreferenceController.updateState(preferenceScreen.findPreference(this.mAddUserWhenLockedPreferenceController.getPreferenceKey()));
        }
        if (this.mShouldUpdateUserList) {
            this.mUserCaps.updateAddUserCapabilities((Context)this.getActivity());
            this.loadProfile();
            this.updateUserList();
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mEditUserInfoController.onSaveInstanceState(bundle);
        bundle.putInt("adding_user", this.mAddedUserId);
        bundle.putInt("removing_user", this.mRemovingUserId);
    }
    
    public void startActivityForResult(final Intent intent, final int n) {
        this.mEditUserInfoController.startingActivityForResult();
        super.startActivityForResult(intent, n);
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getString(2131889765, new Object[] { ((UserManager)this.mContext.getSystemService((Class)UserManager.class)).getUserInfo(UserHandle.myUserId()).name }));
            }
        }
    }
}
