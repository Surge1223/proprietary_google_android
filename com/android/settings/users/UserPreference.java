package com.android.settings.users;

import android.view.View;
import android.widget.ImageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.os.UserManager;
import android.graphics.drawable.Drawable;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View.OnClickListener;
import java.util.Comparator;
import com.android.settingslib.RestrictedPreference;

public class UserPreference extends RestrictedPreference
{
    public static final Comparator<UserPreference> SERIAL_NUMBER_COMPARATOR;
    private View.OnClickListener mDeleteClickListener;
    private int mSerialNumber;
    private View.OnClickListener mSettingsClickListener;
    private int mUserId;
    
    static {
        SERIAL_NUMBER_COMPARATOR = new Comparator<UserPreference>() {
            @Override
            public int compare(final UserPreference userPreference, final UserPreference userPreference2) {
                final int access$000 = userPreference.getSerialNumber();
                final int access$2 = userPreference2.getSerialNumber();
                if (access$000 < access$2) {
                    return -1;
                }
                if (access$000 > access$2) {
                    return 1;
                }
                return 0;
            }
        };
    }
    
    public UserPreference(final Context context, final AttributeSet set) {
        this(context, set, -10, null, null);
    }
    
    UserPreference(final Context context, final AttributeSet set, final int mUserId, final View.OnClickListener mSettingsClickListener, final View.OnClickListener mDeleteClickListener) {
        super(context, set);
        this.mSerialNumber = -1;
        this.mUserId = -10;
        if (mDeleteClickListener != null || mSettingsClickListener != null) {
            this.setWidgetLayoutResource(2131558716);
        }
        this.mDeleteClickListener = mDeleteClickListener;
        this.mSettingsClickListener = mSettingsClickListener;
        this.mUserId = mUserId;
        this.useAdminDisabledSummary(true);
    }
    
    private boolean canDeleteUser() {
        return this.mDeleteClickListener != null && !RestrictedLockUtils.hasBaseUserRestriction(this.getContext(), "no_remove_user", UserHandle.myUserId());
    }
    
    private void dimIcon(final boolean b) {
        final Drawable icon = this.getIcon();
        if (icon != null) {
            final Drawable mutate = icon.mutate();
            int alpha;
            if (b) {
                alpha = 102;
            }
            else {
                alpha = 255;
            }
            mutate.setAlpha(alpha);
            this.setIcon(icon);
        }
    }
    
    private int getSerialNumber() {
        if (this.mUserId == UserHandle.myUserId()) {
            return Integer.MIN_VALUE;
        }
        if (this.mSerialNumber < 0) {
            if (this.mUserId == -10) {
                return Integer.MAX_VALUE;
            }
            if (this.mUserId == -11) {
                return 2147483646;
            }
            this.mSerialNumber = ((UserManager)this.getContext().getSystemService("user")).getUserSerialNumber(this.mUserId);
            if (this.mSerialNumber < 0) {
                return this.mUserId;
            }
        }
        return this.mSerialNumber;
    }
    
    public int getUserId() {
        return this.mUserId;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final boolean disabledByAdmin = this.isDisabledByAdmin();
        this.dimIcon(disabledByAdmin);
        final View viewById = preferenceViewHolder.findViewById(2131362787);
        final boolean b = false;
        if (viewById != null) {
            int visibility;
            if (disabledByAdmin) {
                visibility = 8;
            }
            else {
                visibility = 0;
            }
            viewById.setVisibility(visibility);
        }
        if (!disabledByAdmin) {
            final View viewById2 = preferenceViewHolder.findViewById(2131362077);
            final View viewById3 = preferenceViewHolder.findViewById(2131362078);
            final View viewById4 = preferenceViewHolder.findViewById(2131362750);
            if (viewById4 != null) {
                if (this.canDeleteUser()) {
                    viewById4.setVisibility(0);
                    viewById2.setVisibility(0);
                    viewById4.setOnClickListener(this.mDeleteClickListener);
                    viewById4.setTag((Object)this);
                }
                else {
                    viewById4.setVisibility(8);
                    viewById2.setVisibility(8);
                }
            }
            final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131362354);
            if (imageView != null) {
                if (this.mSettingsClickListener != null) {
                    imageView.setVisibility(0);
                    int visibility2;
                    if (this.mDeleteClickListener == null) {
                        visibility2 = (b ? 1 : 0);
                    }
                    else {
                        visibility2 = 8;
                    }
                    viewById3.setVisibility(visibility2);
                    imageView.setOnClickListener(this.mSettingsClickListener);
                    imageView.setTag((Object)this);
                }
                else {
                    imageView.setVisibility(8);
                    viewById3.setVisibility(8);
                }
            }
        }
    }
    
    @Override
    protected boolean shouldHideSecondTarget() {
        final boolean disabledByAdmin = this.isDisabledByAdmin();
        boolean b = true;
        if (disabledByAdmin) {
            return true;
        }
        if (this.canDeleteUser()) {
            return false;
        }
        if (this.mSettingsClickListener != null) {
            b = false;
        }
        return b;
    }
}
