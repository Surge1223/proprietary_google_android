package com.android.settings.users;

import java.io.File;
import android.os.Bundle;
import android.content.Intent;
import android.content.pm.UserInfo;
import android.view.View;
import android.app.Activity;
import android.text.Editable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import com.android.settingslib.Utils;
import com.android.settingslib.drawable.CircleFramedDrawable;
import android.widget.ImageView;
import android.widget.EditText;
import android.view.ViewGroup;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.app.Fragment;
import android.os.UserManager;
import android.os.UserHandle;
import android.graphics.Bitmap;
import android.app.Dialog;

public class EditUserInfoController
{
    private Dialog mEditUserInfoDialog;
    private EditUserPhotoController mEditUserPhotoController;
    private Bitmap mSavedPhoto;
    private UserHandle mUser;
    private UserManager mUserManager;
    private boolean mWaitingForActivityResult;
    
    public EditUserInfoController() {
        this.mWaitingForActivityResult = false;
    }
    
    public void clear() {
        this.mEditUserPhotoController.removeNewUserPhotoBitmapFile();
        this.mEditUserInfoDialog = null;
        this.mSavedPhoto = null;
    }
    
    public Dialog createDialog(final Fragment fragment, final Drawable drawable, final CharSequence charSequence, final int n, final OnContentChangedCallback onContentChangedCallback, final UserHandle mUser) {
        final Activity activity = fragment.getActivity();
        this.mUser = mUser;
        if (this.mUserManager == null) {
            this.mUserManager = UserManager.get((Context)activity);
        }
        final View inflate = activity.getLayoutInflater().inflate(2131558547, (ViewGroup)null);
        final UserInfo userInfo = this.mUserManager.getUserInfo(this.mUser.getIdentifier());
        final EditText editText = (EditText)inflate.findViewById(2131362795);
        editText.setText((CharSequence)userInfo.name);
        final ImageView imageView = (ImageView)inflate.findViewById(2131362796);
        Drawable imageDrawable;
        if (this.mSavedPhoto != null) {
            imageDrawable = CircleFramedDrawable.getInstance((Context)activity, this.mSavedPhoto);
        }
        else if ((imageDrawable = drawable) == null) {
            imageDrawable = Utils.getUserIcon((Context)activity, this.mUserManager, userInfo);
        }
        imageView.setImageDrawable(imageDrawable);
        this.mEditUserPhotoController = new EditUserPhotoController(fragment, imageView, this.mSavedPhoto, imageDrawable, this.mWaitingForActivityResult);
        this.mEditUserInfoDialog = (Dialog)new AlertDialog$Builder((Context)activity).setTitle(2131888697).setView(inflate).setCancelable(true).setPositiveButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (n == -1) {
                    final Editable text = editText.getText();
                    if (!TextUtils.isEmpty((CharSequence)text) && (charSequence == null || !((CharSequence)text).toString().equals(charSequence.toString()))) {
                        if (onContentChangedCallback != null) {
                            onContentChangedCallback.onLabelChanged(((CharSequence)text).toString());
                        }
                        EditUserInfoController.this.mUserManager.setUserName(EditUserInfoController.this.mUser.getIdentifier(), ((CharSequence)text).toString());
                    }
                    final Drawable newUserPhotoDrawable = EditUserInfoController.this.mEditUserPhotoController.getNewUserPhotoDrawable();
                    final Bitmap newUserPhotoBitmap = EditUserInfoController.this.mEditUserPhotoController.getNewUserPhotoBitmap();
                    if (newUserPhotoDrawable != null && newUserPhotoBitmap != null && !newUserPhotoDrawable.equals(drawable)) {
                        if (onContentChangedCallback != null) {
                            onContentChangedCallback.onPhotoChanged(newUserPhotoDrawable);
                        }
                        new AsyncTask<Void, Void, Void>() {
                            protected Void doInBackground(final Void... array) {
                                EditUserInfoController.this.mUserManager.setUserIcon(EditUserInfoController.this.mUser.getIdentifier(), EditUserInfoController.this.mEditUserPhotoController.getNewUserPhotoBitmap());
                                return null;
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])null);
                    }
                    fragment.getActivity().removeDialog(1);
                }
                EditUserInfoController.this.clear();
            }
        }).setNegativeButton(17039360, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                EditUserInfoController.this.clear();
            }
        }).create();
        this.mEditUserInfoDialog.getWindow().setSoftInputMode(4);
        return this.mEditUserInfoDialog;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        this.mWaitingForActivityResult = false;
        if (this.mEditUserInfoDialog != null && this.mEditUserInfoDialog.isShowing() && this.mEditUserPhotoController.onActivityResult(n, n2, intent)) {
            return;
        }
    }
    
    public void onRestoreInstanceState(final Bundle bundle) {
        final String string = bundle.getString("pending_photo");
        if (string != null) {
            this.mSavedPhoto = EditUserPhotoController.loadNewUserPhotoBitmap(new File(string));
        }
        this.mWaitingForActivityResult = bundle.getBoolean("awaiting_result", false);
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        if (this.mEditUserInfoDialog != null && this.mEditUserInfoDialog.isShowing() && this.mEditUserPhotoController != null) {
            final File saveNewUserPhotoBitmap = this.mEditUserPhotoController.saveNewUserPhotoBitmap();
            if (saveNewUserPhotoBitmap != null) {
                bundle.putString("pending_photo", saveNewUserPhotoBitmap.getPath());
            }
        }
        if (this.mWaitingForActivityResult) {
            bundle.putBoolean("awaiting_result", this.mWaitingForActivityResult);
        }
    }
    
    public void startingActivityForResult() {
        this.mWaitingForActivityResult = true;
    }
    
    public interface OnContentChangedCallback
    {
        void onLabelChanged(final CharSequence p0);
        
        void onPhotoChanged(final Drawable p0);
    }
}
