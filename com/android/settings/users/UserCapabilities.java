package com.android.settings.users;

import android.provider.Settings;
import android.content.pm.UserInfo;
import android.app.admin.DevicePolicyManager;
import android.os.UserHandle;
import com.android.settings.Utils;
import android.os.UserManager;
import android.content.Context;
import com.android.settingslib.RestrictedLockUtils;

public class UserCapabilities
{
    boolean mCanAddGuest;
    boolean mCanAddRestrictedProfile;
    boolean mCanAddUser;
    boolean mDisallowAddUser;
    boolean mDisallowAddUserSetByAdmin;
    boolean mDisallowSwitchUser;
    boolean mEnabled;
    RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    boolean mIsAdmin;
    boolean mIsGuest;
    
    private UserCapabilities() {
        this.mEnabled = true;
        this.mCanAddUser = true;
        this.mCanAddRestrictedProfile = true;
    }
    
    public static UserCapabilities create(final Context context) {
        final UserManager userManager = (UserManager)context.getSystemService("user");
        final UserCapabilities userCapabilities = new UserCapabilities();
        if (UserManager.supportsMultipleUsers() && !Utils.isMonkeyRunning()) {
            final UserInfo userInfo = userManager.getUserInfo(UserHandle.myUserId());
            userCapabilities.mIsGuest = userInfo.isGuest();
            userCapabilities.mIsAdmin = userInfo.isAdmin();
            if (((DevicePolicyManager)context.getSystemService("device_policy")).isDeviceManaged() || Utils.isVoiceCapable(context)) {
                userCapabilities.mCanAddRestrictedProfile = false;
            }
            userCapabilities.updateAddUserCapabilities(context);
            return userCapabilities;
        }
        userCapabilities.mEnabled = false;
        return userCapabilities;
    }
    
    public boolean disallowAddUser() {
        return this.mDisallowAddUser;
    }
    
    public boolean disallowAddUserSetByAdmin() {
        return this.mDisallowAddUserSetByAdmin;
    }
    
    public RestrictedLockUtils.EnforcedAdmin getEnforcedAdmin() {
        return this.mEnforcedAdmin;
    }
    
    public boolean isAdmin() {
        return this.mIsAdmin;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UserCapabilities{mEnabled=");
        sb.append(this.mEnabled);
        sb.append(", mCanAddUser=");
        sb.append(this.mCanAddUser);
        sb.append(", mCanAddRestrictedProfile=");
        sb.append(this.mCanAddRestrictedProfile);
        sb.append(", mIsAdmin=");
        sb.append(this.mIsAdmin);
        sb.append(", mIsGuest=");
        sb.append(this.mIsGuest);
        sb.append(", mCanAddGuest=");
        sb.append(this.mCanAddGuest);
        sb.append(", mDisallowAddUser=");
        sb.append(this.mDisallowAddUser);
        sb.append(", mEnforcedAdmin=");
        sb.append(this.mEnforcedAdmin);
        sb.append(", mDisallowSwitchUser=");
        sb.append(this.mDisallowSwitchUser);
        sb.append('}');
        return sb.toString();
    }
    
    public void updateAddUserCapabilities(final Context context) {
        this.mEnforcedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(context, "no_add_user", UserHandle.myUserId());
        final boolean hasBaseUserRestriction = RestrictedLockUtils.hasBaseUserRestriction(context, "no_add_user", UserHandle.myUserId());
        final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin = this.mEnforcedAdmin;
        final boolean b = false;
        this.mDisallowAddUserSetByAdmin = (mEnforcedAdmin != null && !hasBaseUserRestriction);
        this.mDisallowAddUser = (this.mEnforcedAdmin != null || hasBaseUserRestriction);
        this.mCanAddUser = true;
        if (!this.mIsAdmin || UserManager.getMaxSupportedUsers() < 2 || !UserManager.supportsMultipleUsers() || this.mDisallowAddUser) {
            this.mCanAddUser = false;
        }
        final boolean b2 = this.mIsAdmin || Settings.Global.getInt(context.getContentResolver(), "add_users_when_locked", 0) == 1;
        boolean mCanAddGuest = b;
        if (!this.mIsGuest) {
            mCanAddGuest = b;
            if (!this.mDisallowAddUser) {
                mCanAddGuest = b;
                if (b2) {
                    mCanAddGuest = true;
                }
            }
        }
        this.mCanAddGuest = mCanAddGuest;
        this.mDisallowSwitchUser = ((UserManager)context.getSystemService("user")).hasUserRestriction("no_user_switch");
    }
}
