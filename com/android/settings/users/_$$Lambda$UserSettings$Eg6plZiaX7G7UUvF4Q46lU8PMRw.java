package com.android.settings.users;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.SharedPreferences;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import java.util.HashMap;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.os.Process;
import android.app.Dialog;
import android.support.v7.preference.PreferenceScreen;
import android.content.IntentFilter;
import android.provider.Settings;
import android.view.View;
import java.util.Comparator;
import java.util.Collections;
import com.android.settingslib.RestrictedLockUtils;
import android.util.AttributeSet;
import com.android.settings.Utils;
import android.util.Log;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.app.ActivityManager;
import com.android.internal.widget.LockPatternUtils;
import java.util.Iterator;
import com.android.internal.util.UserIcons;
import com.android.settingslib.drawable.CircleFramedDrawable;
import java.io.InputStream;
import android.net.Uri;
import java.io.IOException;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract$Contacts;
import android.os.UserHandle;
import android.provider.ContactsContract$Profile;
import android.content.res.Resources;
import android.content.pm.UserInfo;
import android.content.Intent;
import android.os.Message;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.Lifecycle;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.os.UserManager;
import android.support.v7.preference.PreferenceGroup;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.graphics.drawable.Drawable;
import com.android.settingslib.RestrictedPreference;
import android.graphics.Bitmap;
import android.util.SparseArray;
import com.android.settings.search.Indexable;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;
import android.content.DialogInterface$OnDismissListener;
import com.android.settings.SettingsPreferenceFragment;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$UserSettings$Eg6plZiaX7G7UUvF4Q46lU8PMRw implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return UserSettings.lambda$static$1(activity, summaryLoader);
    }
}
