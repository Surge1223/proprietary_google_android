package com.android.settings.users;

import java.util.Iterator;
import android.os.Bundle;
import android.content.res.Resources;
import android.os.UserManager;
import android.content.RestrictionEntry;
import java.util.ArrayList;
import android.os.UserHandle;
import android.content.Context;

public class RestrictionUtils
{
    public static final int[] sRestrictionDescriptions;
    public static final String[] sRestrictionKeys;
    public static final int[] sRestrictionTitles;
    
    static {
        sRestrictionKeys = new String[] { "no_share_location" };
        sRestrictionTitles = new int[] { 2131888837 };
        sRestrictionDescriptions = new int[] { 2131888836 };
    }
    
    public static ArrayList<RestrictionEntry> getRestrictions(final Context context, final UserHandle userHandle) {
        final Resources resources = context.getResources();
        final ArrayList<RestrictionEntry> list = new ArrayList<RestrictionEntry>();
        final Bundle userRestrictions = UserManager.get(context).getUserRestrictions(userHandle);
        for (int i = 0; i < RestrictionUtils.sRestrictionKeys.length; ++i) {
            final RestrictionEntry restrictionEntry = new RestrictionEntry(RestrictionUtils.sRestrictionKeys[i], userRestrictions.getBoolean(RestrictionUtils.sRestrictionKeys[i], false) ^ true);
            restrictionEntry.setTitle(resources.getString(RestrictionUtils.sRestrictionTitles[i]));
            restrictionEntry.setDescription(resources.getString(RestrictionUtils.sRestrictionDescriptions[i]));
            restrictionEntry.setType(1);
            list.add(restrictionEntry);
        }
        return list;
    }
    
    public static void setRestrictions(final Context context, final ArrayList<RestrictionEntry> list, final UserHandle userHandle) {
        final UserManager value = UserManager.get(context);
        for (final RestrictionEntry restrictionEntry : list) {
            value.setUserRestriction(restrictionEntry.getKey(), restrictionEntry.getSelectedState() ^ true, userHandle);
        }
    }
}
