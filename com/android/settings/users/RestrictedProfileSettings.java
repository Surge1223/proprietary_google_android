package com.android.settings.users;

import android.content.pm.UserInfo;
import com.android.settings.Utils;
import android.graphics.drawable.Drawable;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.Fragment;
import android.app.Dialog;
import android.content.Intent;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.widget.ImageView;

public class RestrictedProfileSettings extends AppRestrictionsFragment implements OnContentChangedCallback
{
    private ImageView mDeleteButton;
    private EditUserInfoController mEditUserInfoController;
    private View mHeaderView;
    private ImageView mUserIconView;
    private TextView mUserNameView;
    
    public RestrictedProfileSettings() {
        this.mEditUserInfoController = new EditUserInfoController();
    }
    
    private void removeUser() {
        this.getView().post((Runnable)new Runnable() {
            @Override
            public void run() {
                RestrictedProfileSettings.this.mUserManager.removeUser(RestrictedProfileSettings.this.mUser.getIdentifier());
                RestrictedProfileSettings.this.finishFragment();
            }
        });
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 2: {
                return 591;
            }
            case 1: {
                return 590;
            }
        }
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        (this.mHeaderView = this.setPinnedHeaderView(2131558874)).setOnClickListener((View.OnClickListener)this);
        this.mUserIconView = (ImageView)this.mHeaderView.findViewById(16908294);
        this.mUserNameView = (TextView)this.mHeaderView.findViewById(16908310);
        (this.mDeleteButton = (ImageView)this.mHeaderView.findViewById(2131362057)).setOnClickListener((View.OnClickListener)this);
        super.onActivityCreated(bundle);
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        this.mEditUserInfoController.onActivityResult(n, n2, intent);
    }
    
    @Override
    public void onClick(final View view) {
        if (view == this.mHeaderView) {
            this.showDialog(1);
        }
        else if (view == this.mDeleteButton) {
            this.showDialog(2);
        }
        else {
            super.onClick(view);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.mEditUserInfoController.onRestoreInstanceState(bundle);
        }
        this.init(bundle);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (n == 1) {
            return this.mEditUserInfoController.createDialog(this, this.mUserIconView.getDrawable(), this.mUserNameView.getText(), 2131888697, (EditUserInfoController.OnContentChangedCallback)this, this.mUser);
        }
        if (n == 2) {
            return UserDialogs.createRemoveDialog((Context)this.getActivity(), this.mUser.getIdentifier(), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    RestrictedProfileSettings.this.removeUser();
                }
            });
        }
        return null;
    }
    
    @Override
    public void onLabelChanged(final CharSequence text) {
        this.mUserNameView.setText(text);
    }
    
    @Override
    public void onPhotoChanged(final Drawable imageDrawable) {
        this.mUserIconView.setImageDrawable(imageDrawable);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final UserInfo existingUser = Utils.getExistingUser(this.mUserManager, this.mUser);
        if (existingUser == null) {
            this.finishFragment();
        }
        else {
            ((TextView)this.mHeaderView.findViewById(16908310)).setText((CharSequence)existingUser.name);
            ((ImageView)this.mHeaderView.findViewById(16908294)).setImageDrawable(com.android.settingslib.Utils.getUserIcon((Context)this.getActivity(), this.mUserManager, existingUser));
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mEditUserInfoController.onSaveInstanceState(bundle);
    }
    
    public void startActivityForResult(final Intent intent, final int n) {
        this.mEditUserInfoController.startingActivityForResult();
        super.startActivityForResult(intent, n);
    }
}
