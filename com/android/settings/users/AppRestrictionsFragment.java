package com.android.settings.users;

import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.Switch;
import android.view.ViewGroup;
import android.support.v7.preference.PreferenceViewHolder;
import android.os.AsyncTask$Status;
import android.content.IntentFilter;
import java.util.StringTokenizer;
import android.view.View;
import android.content.RestrictionsManager;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.IPackageManager$Stub;
import android.os.ServiceManager;
import android.os.Process;
import android.os.Bundle;
import android.os.Handler;
import android.graphics.drawable.Drawable;
import android.app.Activity;
import android.os.RemoteException;
import android.content.pm.ResolveInfo;
import java.util.List;
import com.android.settings.Utils;
import java.util.Iterator;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import java.util.Set;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import android.support.v14.preference.MultiSelectListPreference;
import android.content.RestrictionEntry;
import java.util.ArrayList;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.os.UserHandle;
import android.content.pm.PackageInfo;
import android.content.BroadcastReceiver;
import android.content.pm.PackageManager;
import android.content.pm.IPackageManager;
import java.util.HashMap;
import android.os.AsyncTask;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.users.AppRestrictionsHelper;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class AppRestrictionsFragment extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnPreferenceClickListener, View.OnClickListener, OnDisableUiForPackageListener
{
    private static final String TAG;
    private PreferenceGroup mAppList;
    private boolean mAppListChanged;
    private AsyncTask mAppLoadingTask;
    private int mCustomRequestCode;
    private HashMap<Integer, AppRestrictionsPreference> mCustomRequestMap;
    private boolean mFirstTime;
    private AppRestrictionsHelper mHelper;
    protected IPackageManager mIPm;
    private boolean mNewUser;
    protected PackageManager mPackageManager;
    private BroadcastReceiver mPackageObserver;
    protected boolean mRestrictedProfile;
    private PackageInfo mSysPackageInfo;
    protected UserHandle mUser;
    private BroadcastReceiver mUserBackgrounding;
    protected UserManager mUserManager;
    
    static {
        TAG = AppRestrictionsFragment.class.getSimpleName();
    }
    
    public AppRestrictionsFragment() {
        this.mFirstTime = true;
        this.mCustomRequestCode = 1000;
        this.mCustomRequestMap = new HashMap<Integer, AppRestrictionsPreference>();
        this.mUserBackgrounding = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (AppRestrictionsFragment.this.mAppListChanged) {
                    AppRestrictionsFragment.this.mHelper.applyUserAppsStates((AppRestrictionsHelper.OnDisableUiForPackageListener)AppRestrictionsFragment.this);
                }
            }
        };
        this.mPackageObserver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                AppRestrictionsFragment.this.onPackageChanged(intent);
            }
        };
    }
    
    private void addLocationAppRestrictionsPreference(final SelectableAppInfo selectableAppInfo, final AppRestrictionsPreference appRestrictionsPreference) {
        final String packageName = selectableAppInfo.packageName;
        appRestrictionsPreference.setIcon(2131231130);
        appRestrictionsPreference.setKey(this.getKeyForPackage(packageName));
        final ArrayList<RestrictionEntry> restrictions = RestrictionUtils.getRestrictions((Context)this.getActivity(), this.mUser);
        final RestrictionEntry restrictionEntry = restrictions.get(0);
        appRestrictionsPreference.setTitle(restrictionEntry.getTitle());
        appRestrictionsPreference.setRestrictions(restrictions);
        appRestrictionsPreference.setSummary(restrictionEntry.getDescription());
        appRestrictionsPreference.setChecked(restrictionEntry.getSelectedState());
        appRestrictionsPreference.setPersistent(false);
        appRestrictionsPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        appRestrictionsPreference.setOrder(100);
        this.mAppList.addPreference(appRestrictionsPreference);
    }
    
    private String findInArray(final String[] array, final String[] array2, final String s) {
        for (int i = 0; i < array2.length; ++i) {
            if (array2[i].equals(s)) {
                return array[i];
            }
        }
        return s;
    }
    
    private int generateCustomActivityRequestCode(final AppRestrictionsPreference appRestrictionsPreference) {
        ++this.mCustomRequestCode;
        this.mCustomRequestMap.put(this.mCustomRequestCode, appRestrictionsPreference);
        return this.mCustomRequestCode;
    }
    
    private String getKeyForPackage(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("pkg_");
        sb.append(s);
        return sb.toString();
    }
    
    private String getPackageSummary(final PackageInfo packageInfo, final SelectableAppInfo selectableAppInfo) {
        if (selectableAppInfo.masterEntry != null) {
            if (this.mRestrictedProfile && packageInfo.restrictedAccountType != null) {
                return this.getString(2131886399, new Object[] { selectableAppInfo.masterEntry.activityName });
            }
            return this.getString(2131889751, new Object[] { selectableAppInfo.masterEntry.activityName });
        }
        else {
            if (packageInfo.restrictedAccountType != null) {
                return this.getString(2131886398);
            }
            return null;
        }
    }
    
    private boolean isAppEnabledForUser(final PackageInfo packageInfo) {
        final boolean b = false;
        if (packageInfo == null) {
            return false;
        }
        final int flags = packageInfo.applicationInfo.flags;
        final int privateFlags = packageInfo.applicationInfo.privateFlags;
        boolean b2 = b;
        if ((0x800000 & flags) != 0x0) {
            b2 = b;
            if ((privateFlags & 0x1) == 0x0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    private static boolean isAppUnsupportedInRestrictedProfile(final PackageInfo packageInfo) {
        return packageInfo.requiredAccountType != null && packageInfo.restrictedAccountType == null;
    }
    
    private boolean isPlatformSigned(final PackageInfo packageInfo) {
        boolean b2;
        final boolean b = b2 = false;
        if (packageInfo != null) {
            b2 = b;
            if (packageInfo.signatures != null) {
                b2 = b;
                if (this.mSysPackageInfo.signatures[0].equals((Object)packageInfo.signatures[0])) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    private void onAppSettingsIconClicked(final AppRestrictionsPreference appRestrictionsPreference) {
        if (appRestrictionsPreference.getKey().startsWith("pkg_")) {
            if (appRestrictionsPreference.isPanelOpen()) {
                this.removeRestrictionsForApp(appRestrictionsPreference);
            }
            else {
                this.requestRestrictionsForApp(appRestrictionsPreference.getKey().substring("pkg_".length()), appRestrictionsPreference, true);
            }
            appRestrictionsPreference.setPanelOpen(appRestrictionsPreference.isPanelOpen() ^ true);
        }
    }
    
    private void onPackageChanged(final Intent intent) {
        final String action = intent.getAction();
        final AppRestrictionsPreference appRestrictionsPreference = (AppRestrictionsPreference)this.findPreference(this.getKeyForPackage(intent.getData().getSchemeSpecificPart()));
        if (appRestrictionsPreference == null) {
            return;
        }
        if (("android.intent.action.PACKAGE_ADDED".equals(action) && appRestrictionsPreference.isChecked()) || ("android.intent.action.PACKAGE_REMOVED".equals(action) && !appRestrictionsPreference.isChecked())) {
            appRestrictionsPreference.setEnabled(true);
        }
    }
    
    private void onRestrictionsReceived(final AppRestrictionsPreference appRestrictionsPreference, final ArrayList<RestrictionEntry> restrictions) {
        this.removeRestrictionsForApp(appRestrictionsPreference);
        int n = 1;
        for (final RestrictionEntry restrictionEntry : restrictions) {
            Preference preference = null;
            switch (restrictionEntry.getType()) {
                case 4: {
                    preference = new MultiSelectListPreference(this.getPrefContext());
                    preference.setTitle(restrictionEntry.getTitle());
                    ((MultiSelectListPreference)preference).setEntryValues(restrictionEntry.getChoiceValues());
                    ((MultiSelectListPreference)preference).setEntries(restrictionEntry.getChoiceEntries());
                    final HashSet<Object> values = new HashSet<Object>();
                    Collections.addAll(values, restrictionEntry.getAllSelectedStrings());
                    ((MultiSelectListPreference)preference).setValues((Set<String>)values);
                    ((MultiSelectListPreference)preference).setDialogTitle(restrictionEntry.getTitle());
                    break;
                }
                case 2:
                case 3: {
                    final ListPreference listPreference = new ListPreference(this.getPrefContext());
                    listPreference.setTitle(restrictionEntry.getTitle());
                    String value;
                    if ((value = restrictionEntry.getSelectedString()) == null) {
                        value = restrictionEntry.getDescription();
                    }
                    listPreference.setSummary(this.findInArray(restrictionEntry.getChoiceEntries(), restrictionEntry.getChoiceValues(), value));
                    listPreference.setEntryValues(restrictionEntry.getChoiceValues());
                    listPreference.setEntries(restrictionEntry.getChoiceEntries());
                    listPreference.setValue(value);
                    listPreference.setDialogTitle(restrictionEntry.getTitle());
                    preference = listPreference;
                    break;
                }
                case 1: {
                    preference = new SwitchPreference(this.getPrefContext());
                    preference.setTitle(restrictionEntry.getTitle());
                    preference.setSummary(restrictionEntry.getDescription());
                    ((SwitchPreference)preference).setChecked(restrictionEntry.getSelectedState());
                    break;
                }
            }
            int n2 = n;
            if (preference != null) {
                preference.setPersistent(false);
                preference.setOrder(appRestrictionsPreference.getOrder() + n);
                final StringBuilder sb = new StringBuilder();
                sb.append(appRestrictionsPreference.getKey().substring("pkg_".length()));
                sb.append(";");
                sb.append(restrictionEntry.getKey());
                preference.setKey(sb.toString());
                this.mAppList.addPreference(preference);
                preference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                preference.setIcon(2131230910);
                appRestrictionsPreference.mChildren.add(preference);
                n2 = n + 1;
            }
            n = n2;
        }
        appRestrictionsPreference.setRestrictions(restrictions);
        if (n == 1 && appRestrictionsPreference.isImmutable() && appRestrictionsPreference.isChecked()) {
            this.mAppList.removePreference(appRestrictionsPreference);
        }
    }
    
    private void populateApps() {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        final PackageManager mPackageManager = this.mPackageManager;
        final IPackageManager miPm = this.mIPm;
        final int identifier = this.mUser.getIdentifier();
        if (Utils.getExistingUser(this.mUserManager, this.mUser) == null) {
            return;
        }
        this.mAppList.removeAll();
        final List queryBroadcastReceivers = mPackageManager.queryBroadcastReceivers(new Intent("android.intent.action.GET_RESTRICTION_ENTRIES"), 0);
        for (final SelectableAppInfo selectableAppInfo : this.mHelper.getVisibleApps()) {
            final String packageName = selectableAppInfo.packageName;
            if (packageName == null) {
                continue;
            }
            final boolean equals = packageName.equals(((Context)activity).getPackageName());
            final AppRestrictionsPreference appRestrictionsPreference = new AppRestrictionsPreference(this.getPrefContext(), (View.OnClickListener)this);
            final boolean resolveInfoListHasPackage = this.resolveInfoListHasPackage(queryBroadcastReceivers, packageName);
            if (equals) {
                this.addLocationAppRestrictionsPreference(selectableAppInfo, appRestrictionsPreference);
                this.mHelper.setPackageSelected(packageName, true);
            }
            else {
                Drawable mutate = null;
                PackageInfo packageInfo;
                try {
                    packageInfo = miPm.getPackageInfo(packageName, 4194368, identifier);
                }
                catch (RemoteException ex) {
                    packageInfo = null;
                }
                if (packageInfo == null) {
                    continue;
                }
                if (this.mRestrictedProfile && isAppUnsupportedInRestrictedProfile(packageInfo)) {
                    continue;
                }
                if (selectableAppInfo.icon != null) {
                    mutate = selectableAppInfo.icon.mutate();
                }
                appRestrictionsPreference.setIcon(mutate);
                appRestrictionsPreference.setChecked(false);
                appRestrictionsPreference.setTitle(selectableAppInfo.activityName);
                appRestrictionsPreference.setKey(this.getKeyForPackage(packageName));
                appRestrictionsPreference.setSettingsEnabled(resolveInfoListHasPackage && selectableAppInfo.masterEntry == null);
                appRestrictionsPreference.setPersistent(false);
                appRestrictionsPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                appRestrictionsPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
                appRestrictionsPreference.setSummary(this.getPackageSummary(packageInfo, selectableAppInfo));
                if (!packageInfo.requiredForAllUsers && !this.isPlatformSigned(packageInfo)) {
                    if (!this.mNewUser && this.isAppEnabledForUser(packageInfo)) {
                        appRestrictionsPreference.setChecked(true);
                    }
                }
                else {
                    appRestrictionsPreference.setChecked(true);
                    appRestrictionsPreference.setImmutable(true);
                    if (!resolveInfoListHasPackage) {
                        continue;
                    }
                    if (selectableAppInfo.masterEntry == null) {
                        this.requestRestrictionsForApp(packageName, appRestrictionsPreference, false);
                    }
                }
                if (selectableAppInfo.masterEntry != null) {
                    appRestrictionsPreference.setImmutable(true);
                    appRestrictionsPreference.setChecked(this.mHelper.isPackageSelected(packageName));
                }
                appRestrictionsPreference.setOrder(100 * (this.mAppList.getPreferenceCount() + 2));
                this.mHelper.setPackageSelected(packageName, appRestrictionsPreference.isChecked());
                this.mAppList.addPreference(appRestrictionsPreference);
            }
        }
        this.mAppListChanged = true;
        if (this.mNewUser && this.mFirstTime) {
            this.mFirstTime = false;
            this.mHelper.applyUserAppsStates((AppRestrictionsHelper.OnDisableUiForPackageListener)this);
        }
    }
    
    private void removeRestrictionsForApp(final AppRestrictionsPreference appRestrictionsPreference) {
        final Iterator<Preference> iterator = appRestrictionsPreference.mChildren.iterator();
        while (iterator.hasNext()) {
            this.mAppList.removePreference(iterator.next());
        }
        appRestrictionsPreference.mChildren.clear();
    }
    
    private void requestRestrictionsForApp(final String package1, final AppRestrictionsPreference appRestrictionsPreference, final boolean b) {
        final Bundle applicationRestrictions = this.mUserManager.getApplicationRestrictions(package1, this.mUser);
        final Intent intent = new Intent("android.intent.action.GET_RESTRICTION_ENTRIES");
        intent.setPackage(package1);
        intent.putExtra("android.intent.extra.restrictions_bundle", applicationRestrictions);
        intent.addFlags(32);
        this.getActivity().sendOrderedBroadcast(intent, (String)null, (BroadcastReceiver)new RestrictionsResultReceiver(package1, appRestrictionsPreference, b), (Handler)null, -1, (String)null, (Bundle)null);
    }
    
    private boolean resolveInfoListHasPackage(final List<ResolveInfo> list, final String s) {
        final Iterator<ResolveInfo> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().activityInfo.packageName.equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    private void updateAllEntries(final String s, final boolean checked) {
        for (int i = 0; i < this.mAppList.getPreferenceCount(); ++i) {
            final Preference preference = this.mAppList.getPreference(i);
            if (preference instanceof AppRestrictionsPreference && s.equals(preference.getKey())) {
                ((AppRestrictionsPreference)preference).setChecked(checked);
            }
        }
    }
    
    protected PreferenceGroup getAppPreferenceGroup() {
        return this.getPreferenceScreen();
    }
    
    public int getMetricsCategory() {
        return 97;
    }
    
    protected void init(Bundle arguments) {
        if (arguments != null) {
            this.mUser = new UserHandle(arguments.getInt("user_id"));
        }
        else {
            arguments = this.getArguments();
            if (arguments != null) {
                if (arguments.containsKey("user_id")) {
                    this.mUser = new UserHandle(arguments.getInt("user_id"));
                }
                this.mNewUser = arguments.getBoolean("new_user", false);
            }
        }
        if (this.mUser == null) {
            this.mUser = Process.myUserHandle();
        }
        this.mHelper = new AppRestrictionsHelper(this.getContext(), this.mUser);
        this.mPackageManager = this.getActivity().getPackageManager();
        this.mIPm = IPackageManager$Stub.asInterface(ServiceManager.getService("package"));
        this.mUserManager = (UserManager)this.getActivity().getSystemService("user");
        this.mRestrictedProfile = this.mUserManager.getUserInfo(this.mUser.getIdentifier()).isRestricted();
        try {
            this.mSysPackageInfo = this.mPackageManager.getPackageInfo("android", 64);
        }
        catch (PackageManager$NameNotFoundException ex) {}
        this.addPreferencesFromResource(2132082714);
        (this.mAppList = this.getAppPreferenceGroup()).setOrderingAsAdded(false);
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        final AppRestrictionsPreference appRestrictionsPreference = this.mCustomRequestMap.get(n);
        if (appRestrictionsPreference == null) {
            final String tag = AppRestrictionsFragment.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown requestCode ");
            sb.append(n);
            Log.w(tag, sb.toString());
            return;
        }
        if (n2 == -1) {
            final String substring = appRestrictionsPreference.getKey().substring("pkg_".length());
            final ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.restrictions_list");
            final Bundle bundleExtra = intent.getBundleExtra("android.intent.extra.restrictions_bundle");
            if (parcelableArrayListExtra != null) {
                appRestrictionsPreference.setRestrictions(parcelableArrayListExtra);
                this.mUserManager.setApplicationRestrictions(substring, RestrictionsManager.convertRestrictionsToBundle((List)parcelableArrayListExtra), this.mUser);
            }
            else if (bundleExtra != null) {
                this.mUserManager.setApplicationRestrictions(substring, bundleExtra, this.mUser);
            }
        }
        this.mCustomRequestMap.remove(n);
    }
    
    public void onClick(final View view) {
        if (view.getTag() instanceof AppRestrictionsPreference) {
            final AppRestrictionsPreference appRestrictionsPreference = (AppRestrictionsPreference)view.getTag();
            if (view.getId() == 2131361877) {
                this.onAppSettingsIconClicked(appRestrictionsPreference);
            }
            else if (!appRestrictionsPreference.isImmutable()) {
                appRestrictionsPreference.setChecked(appRestrictionsPreference.isChecked() ^ true);
                final String substring = appRestrictionsPreference.getKey().substring("pkg_".length());
                if (substring.equals(this.getActivity().getPackageName())) {
                    ((RestrictionEntry)appRestrictionsPreference.restrictions.get(0)).setSelectedState(appRestrictionsPreference.isChecked());
                    RestrictionUtils.setRestrictions((Context)this.getActivity(), appRestrictionsPreference.restrictions, this.mUser);
                    return;
                }
                this.mHelper.setPackageSelected(substring, appRestrictionsPreference.isChecked());
                if (appRestrictionsPreference.isChecked() && appRestrictionsPreference.hasSettings && appRestrictionsPreference.restrictions == null) {
                    this.requestRestrictionsForApp(substring, appRestrictionsPreference, false);
                }
                this.mAppListChanged = true;
                if (!this.mRestrictedProfile) {
                    this.mHelper.applyUserAppState(substring, appRestrictionsPreference.isChecked(), (AppRestrictionsHelper.OnDisableUiForPackageListener)this);
                }
                this.updateAllEntries(appRestrictionsPreference.getKey(), appRestrictionsPreference.isChecked());
            }
        }
    }
    
    public void onDisableUiForPackage(final String s) {
        final AppRestrictionsPreference appRestrictionsPreference = (AppRestrictionsPreference)this.findPreference(this.getKeyForPackage(s));
        if (appRestrictionsPreference != null) {
            appRestrictionsPreference.setEnabled(false);
        }
    }
    
    public void onPause() {
        super.onPause();
        this.mNewUser = false;
        this.getActivity().unregisterReceiver(this.mUserBackgrounding);
        this.getActivity().unregisterReceiver(this.mPackageObserver);
        if (this.mAppListChanged) {
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(final Void... array) {
                    AppRestrictionsFragment.this.mHelper.applyUserAppsStates((AppRestrictionsHelper.OnDisableUiForPackageListener)AppRestrictionsFragment.this);
                    return null;
                }
            }.execute((Object[])new Void[0]);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final String key = preference.getKey();
        if (key != null && key.contains(";")) {
            final StringTokenizer stringTokenizer = new StringTokenizer(key, ";");
            final String nextToken = stringTokenizer.nextToken();
            final String nextToken2 = stringTokenizer.nextToken();
            final PreferenceGroup mAppList = this.mAppList;
            final StringBuilder sb = new StringBuilder();
            sb.append("pkg_");
            sb.append(nextToken);
            final ArrayList<RestrictionEntry> restrictions = ((AppRestrictionsPreference)mAppList.findPreference(sb.toString())).getRestrictions();
            if (restrictions != null) {
                for (final RestrictionEntry restrictionEntry : restrictions) {
                    if (restrictionEntry.getKey().equals(nextToken2)) {
                        switch (restrictionEntry.getType()) {
                            default: {
                                continue;
                            }
                            case 4: {
                                final Set set = (Set)o;
                                final String[] allSelectedStrings = new String[set.size()];
                                set.toArray(allSelectedStrings);
                                restrictionEntry.setAllSelectedStrings(allSelectedStrings);
                                break;
                            }
                            case 2:
                            case 3: {
                                final ListPreference listPreference = (ListPreference)preference;
                                restrictionEntry.setSelectedString((String)o);
                                listPreference.setSummary(this.findInArray(restrictionEntry.getChoiceEntries(), restrictionEntry.getChoiceValues(), (String)o));
                                break;
                            }
                            case 1: {
                                restrictionEntry.setSelectedState((boolean)o);
                                break;
                            }
                        }
                        this.mUserManager.setApplicationRestrictions(nextToken, RestrictionsManager.convertRestrictionsToBundle((List)restrictions), this.mUser);
                        break;
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (preference.getKey().startsWith("pkg_")) {
            final AppRestrictionsPreference appRestrictionsPreference = (AppRestrictionsPreference)preference;
            if (!appRestrictionsPreference.isImmutable()) {
                final String substring = appRestrictionsPreference.getKey().substring("pkg_".length());
                final boolean checked = appRestrictionsPreference.isChecked() ^ true;
                appRestrictionsPreference.setChecked(checked);
                this.mHelper.setPackageSelected(substring, checked);
                this.updateAllEntries(appRestrictionsPreference.getKey(), checked);
                this.mAppListChanged = true;
                this.mHelper.applyUserAppState(substring, checked, (AppRestrictionsHelper.OnDisableUiForPackageListener)this);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.getActivity().registerReceiver(this.mUserBackgrounding, new IntentFilter("android.intent.action.USER_BACKGROUND"));
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.getActivity().registerReceiver(this.mPackageObserver, intentFilter);
        this.mAppListChanged = false;
        if (this.mAppLoadingTask == null || this.mAppLoadingTask.getStatus() == AsyncTask$Status.FINISHED) {
            this.mAppLoadingTask = new AppLoadingTask().execute((Object[])new Void[0]);
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("user_id", this.mUser.getIdentifier());
    }
    
    private class AppLoadingTask extends AsyncTask<Void, Void, Void>
    {
        protected Void doInBackground(final Void... array) {
            AppRestrictionsFragment.this.mHelper.fetchAndMergeApps();
            return null;
        }
        
        protected void onPostExecute(final Void void1) {
            AppRestrictionsFragment.this.populateApps();
        }
    }
    
    static class AppRestrictionsPreference extends SwitchPreference
    {
        private boolean hasSettings;
        private boolean immutable;
        private View.OnClickListener listener;
        private List<Preference> mChildren;
        private boolean panelOpen;
        private ArrayList<RestrictionEntry> restrictions;
        
        AppRestrictionsPreference(final Context context, final View.OnClickListener listener) {
            super(context);
            this.mChildren = new ArrayList<Preference>();
            this.setLayoutResource(2131558645);
            this.listener = listener;
        }
        
        private void setSettingsEnabled(final boolean hasSettings) {
            this.hasSettings = hasSettings;
        }
        
        ArrayList<RestrictionEntry> getRestrictions() {
            return this.restrictions;
        }
        
        boolean isImmutable() {
            return this.immutable;
        }
        
        boolean isPanelOpen() {
            return this.panelOpen;
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            super.onBindViewHolder(preferenceViewHolder);
            final View viewById = preferenceViewHolder.findViewById(2131361877);
            final boolean hasSettings = this.hasSettings;
            final int n = 8;
            int visibility;
            if (hasSettings) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
            final View viewById2 = preferenceViewHolder.findViewById(2131362596);
            int visibility2;
            if (this.hasSettings) {
                visibility2 = 0;
            }
            else {
                visibility2 = n;
            }
            viewById2.setVisibility(visibility2);
            viewById.setOnClickListener(this.listener);
            viewById.setTag((Object)this);
            final View viewById3 = preferenceViewHolder.findViewById(2131361876);
            viewById3.setOnClickListener(this.listener);
            viewById3.setTag((Object)this);
            final ViewGroup viewGroup = (ViewGroup)preferenceViewHolder.findViewById(16908312);
            viewGroup.setEnabled(this.isImmutable() ^ true);
            if (viewGroup.getChildCount() > 0) {
                final Switch switch1 = (Switch)viewGroup.getChildAt(0);
                switch1.setEnabled(this.isImmutable() ^ true);
                switch1.setTag((Object)this);
                switch1.setClickable(true);
                switch1.setFocusable(true);
                switch1.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
                    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                        AppRestrictionsPreference.this.listener.onClick((View)switch1);
                    }
                });
            }
        }
        
        void setImmutable(final boolean immutable) {
            this.immutable = immutable;
        }
        
        void setPanelOpen(final boolean panelOpen) {
            this.panelOpen = panelOpen;
        }
        
        void setRestrictions(final ArrayList<RestrictionEntry> restrictions) {
            this.restrictions = restrictions;
        }
    }
    
    class RestrictionsResultReceiver extends BroadcastReceiver
    {
        boolean invokeIfCustom;
        String packageName;
        AppRestrictionsPreference preference;
        
        RestrictionsResultReceiver(final String packageName, final AppRestrictionsPreference preference, final boolean invokeIfCustom) {
            this.packageName = packageName;
            this.preference = preference;
            this.invokeIfCustom = invokeIfCustom;
        }
        
        private void assertSafeToStartCustomActivity(final Intent intent) {
            if (intent.getPackage() != null && intent.getPackage().equals(this.packageName)) {
                return;
            }
            final List queryIntentActivities = AppRestrictionsFragment.this.mPackageManager.queryIntentActivities(intent, 0);
            if (queryIntentActivities.size() != 1) {
                return;
            }
            if (this.packageName.equals(queryIntentActivities.get(0).activityInfo.packageName)) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Application ");
            sb.append(this.packageName);
            sb.append(" is not allowed to start activity ");
            sb.append(intent);
            throw new SecurityException(sb.toString());
        }
        
        public void onReceive(final Context context, Intent intent) {
            final Bundle resultExtras = this.getResultExtras(true);
            final ArrayList parcelableArrayList = resultExtras.getParcelableArrayList("android.intent.extra.restrictions_list");
            intent = (Intent)resultExtras.getParcelable("android.intent.extra.restrictions_intent");
            if (parcelableArrayList != null && intent == null) {
                AppRestrictionsFragment.this.onRestrictionsReceived(this.preference, parcelableArrayList);
                if (AppRestrictionsFragment.this.mRestrictedProfile) {
                    AppRestrictionsFragment.this.mUserManager.setApplicationRestrictions(this.packageName, RestrictionsManager.convertRestrictionsToBundle((List)parcelableArrayList), AppRestrictionsFragment.this.mUser);
                }
            }
            else if (intent != null) {
                this.preference.setRestrictions(parcelableArrayList);
                if (this.invokeIfCustom && AppRestrictionsFragment.this.isResumed()) {
                    this.assertSafeToStartCustomActivity(intent);
                    AppRestrictionsFragment.this.startActivityForResult(intent, AppRestrictionsFragment.this.generateCustomActivityRequestCode(this.preference));
                }
            }
        }
    }
}
