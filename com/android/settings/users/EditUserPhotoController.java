package com.android.settings.users;

import android.widget.TextView;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.graphics.Bitmap$CompressFormat;
import java.io.FileOutputStream;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListAdapter;
import java.util.List;
import android.widget.ListPopupWindow;
import java.util.ArrayList;
import com.android.settingslib.drawable.CircleFramedDrawable;
import android.graphics.BitmapFactory;
import android.database.Cursor;
import android.provider.ContactsContract$DisplayPhoto;
import android.os.StrictMode;
import android.support.v4.content.FileProvider;
import java.io.File;
import java.io.OutputStream;
import java.io.InputStream;
import android.content.ContentResolver;
import java.io.IOException;
import android.util.Log;
import libcore.io.Streams;
import android.os.AsyncTask;
import android.content.pm.PackageManager;
import android.content.ClipData;
import android.os.Parcelable;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.app.Fragment;
import android.net.Uri;
import android.content.Context;

public class EditUserPhotoController
{
    private final Context mContext;
    private final Uri mCropPictureUri;
    private final Fragment mFragment;
    private final ImageView mImageView;
    private Bitmap mNewUserPhotoBitmap;
    private Drawable mNewUserPhotoDrawable;
    private final int mPhotoSize;
    private final Uri mTakePictureUri;
    
    public EditUserPhotoController(final Fragment mFragment, final ImageView mImageView, final Bitmap mNewUserPhotoBitmap, final Drawable mNewUserPhotoDrawable, final boolean b) {
        this.mContext = mImageView.getContext();
        this.mFragment = mFragment;
        this.mImageView = mImageView;
        this.mCropPictureUri = this.createTempImageUri(this.mContext, "CropEditUserPhoto.jpg", b ^ true);
        this.mTakePictureUri = this.createTempImageUri(this.mContext, "TakeEditUserPhoto2.jpg", b ^ true);
        this.mPhotoSize = getPhotoSize(this.mContext);
        this.mImageView.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                EditUserPhotoController.this.showUpdatePhotoPopup();
            }
        });
        this.mNewUserPhotoBitmap = mNewUserPhotoBitmap;
        this.mNewUserPhotoDrawable = mNewUserPhotoDrawable;
    }
    
    private void appendCropExtras(final Intent intent) {
        intent.putExtra("crop", "true");
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", this.mPhotoSize);
        intent.putExtra("outputY", this.mPhotoSize);
    }
    
    private void appendOutputExtra(final Intent intent, final Uri uri) {
        intent.putExtra("output", (Parcelable)uri);
        intent.addFlags(3);
        intent.setClipData(ClipData.newRawUri((CharSequence)"output", uri));
    }
    
    private boolean canChoosePhoto() {
        final Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        final PackageManager packageManager = this.mImageView.getContext().getPackageManager();
        boolean b = false;
        if (packageManager.queryIntentActivities(intent, 0).size() > 0) {
            b = true;
        }
        return b;
    }
    
    private boolean canTakePhoto() {
        return this.mImageView.getContext().getPackageManager().queryIntentActivities(new Intent("android.media.action.IMAGE_CAPTURE"), 65536).size() > 0;
    }
    
    private void choosePhoto() {
        final Intent intent = new Intent("android.intent.action.GET_CONTENT", (Uri)null);
        intent.setType("image/*");
        this.appendOutputExtra(intent, this.mTakePictureUri);
        this.mFragment.startActivityForResult(intent, 1001);
    }
    
    private void copyAndCropPhoto(final Uri uri) {
        new AsyncTask<Void, Void, Void>() {
            private static /* synthetic */ void $closeResource(final Throwable t, final AutoCloseable autoCloseable) {
                if (t != null) {
                    try {
                        autoCloseable.close();
                    }
                    catch (Throwable t2) {
                        t.addSuppressed(t2);
                    }
                }
                else {
                    autoCloseable.close();
                }
            }
            
            protected Void doInBackground(final Void... t) {
                final ContentResolver contentResolver = EditUserPhotoController.this.mContext.getContentResolver();
                try {
                    final InputStream openInputStream = contentResolver.openInputStream(uri);
                    Throwable t3 = null;
                    try {
                        final OutputStream openOutputStream = contentResolver.openOutputStream(EditUserPhotoController.this.mTakePictureUri);
                        try {
                            Streams.copy(openInputStream, openOutputStream);
                            if (openOutputStream != null) {
                                $closeResource(null, openOutputStream);
                            }
                            if (openInputStream != null) {
                                $closeResource(null, openInputStream);
                            }
                        }
                        catch (Throwable t) {
                            try {
                                throw t;
                            }
                            finally {}
                        }
                        if (openOutputStream != null) {
                            $closeResource(t, openOutputStream);
                        }
                        throw;
                    }
                    catch (Throwable t3) {
                        try {
                            throw t3;
                        }
                        finally {}
                    }
                    finally {
                        t3 = null;
                    }
                    if (openInputStream != null) {
                        $closeResource(t3, openInputStream);
                    }
                    throw;
                }
                catch (IOException ex) {
                    Log.w("EditUserPhotoController", "Failed to copy photo", (Throwable)ex);
                }
                return null;
            }
            
            protected void onPostExecute(final Void void1) {
                if (!EditUserPhotoController.this.mFragment.isAdded()) {
                    return;
                }
                EditUserPhotoController.this.cropPhoto();
            }
        }.execute((Object[])new Void[0]);
    }
    
    private Uri createTempImageUri(final Context context, final String s, final boolean b) {
        final File cacheDir = context.getCacheDir();
        cacheDir.mkdirs();
        final File file = new File(cacheDir, s);
        if (b) {
            file.delete();
        }
        return FileProvider.getUriForFile(context, "com.android.settings.files", file);
    }
    
    private void cropPhoto() {
        final Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(this.mTakePictureUri, "image/*");
        this.appendOutputExtra(intent, this.mCropPictureUri);
        this.appendCropExtras(intent);
        if (intent.resolveActivity(this.mContext.getPackageManager()) != null) {
            try {
                StrictMode.disableDeathOnFileUriExposure();
                this.mFragment.startActivityForResult(intent, 1003);
                return;
            }
            finally {
                StrictMode.enableDeathOnFileUriExposure();
            }
        }
        this.onPhotoCropped(this.mTakePictureUri, false);
    }
    
    private static int getPhotoSize(final Context context) {
        final Cursor query = context.getContentResolver().query(ContactsContract$DisplayPhoto.CONTENT_MAX_DIMENSIONS_URI, new String[] { "display_max_dim" }, (String)null, (String[])null, (String)null);
        try {
            query.moveToFirst();
            return query.getInt(0);
        }
        finally {
            query.close();
        }
    }
    
    static Bitmap loadNewUserPhotoBitmap(final File file) {
        return BitmapFactory.decodeFile(file.getAbsolutePath());
    }
    
    private void onPhotoCropped(final Uri uri, final boolean b) {
        new AsyncTask<Void, Void, Bitmap>() {
            protected Bitmap doInBackground(final Void... p0) {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: getfield        com/android/settings/users/EditUserPhotoController$6.val$cropped:Z
                //     4: ifeq            125
                //     7: aconst_null    
                //     8: astore_2       
                //     9: aconst_null    
                //    10: astore_1       
                //    11: aload_0        
                //    12: getfield        com/android/settings/users/EditUserPhotoController$6.this$0:Lcom/android/settings/users/EditUserPhotoController;
                //    15: invokestatic    com/android/settings/users/EditUserPhotoController.access$300:(Lcom/android/settings/users/EditUserPhotoController;)Landroid/content/Context;
                //    18: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
                //    21: aload_0        
                //    22: getfield        com/android/settings/users/EditUserPhotoController$6.val$data:Landroid/net/Uri;
                //    25: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
                //    28: astore_3       
                //    29: aload_3        
                //    30: astore_1       
                //    31: aload_3        
                //    32: astore_2       
                //    33: aload_3        
                //    34: invokestatic    android/graphics/BitmapFactory.decodeStream:(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
                //    37: astore          4
                //    39: aload_3        
                //    40: ifnull          60
                //    43: aload_3        
                //    44: invokevirtual   java/io/InputStream.close:()V
                //    47: goto            60
                //    50: astore_1       
                //    51: ldc             "EditUserPhotoController"
                //    53: ldc             "Cannot close image stream"
                //    55: aload_1        
                //    56: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
                //    59: pop            
                //    60: aload           4
                //    62: areturn        
                //    63: astore_2       
                //    64: goto            102
                //    67: astore_3       
                //    68: aload_2        
                //    69: astore_1       
                //    70: ldc             "EditUserPhotoController"
                //    72: ldc             "Cannot find image file"
                //    74: aload_3        
                //    75: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
                //    78: pop            
                //    79: aload_2        
                //    80: ifnull          100
                //    83: aload_2        
                //    84: invokevirtual   java/io/InputStream.close:()V
                //    87: goto            100
                //    90: astore_1       
                //    91: ldc             "EditUserPhotoController"
                //    93: ldc             "Cannot close image stream"
                //    95: aload_1        
                //    96: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
                //    99: pop            
                //   100: aconst_null    
                //   101: areturn        
                //   102: aload_1        
                //   103: ifnull          123
                //   106: aload_1        
                //   107: invokevirtual   java/io/InputStream.close:()V
                //   110: goto            123
                //   113: astore_1       
                //   114: ldc             "EditUserPhotoController"
                //   116: ldc             "Cannot close image stream"
                //   118: aload_1        
                //   119: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
                //   122: pop            
                //   123: aload_2        
                //   124: athrow         
                //   125: aload_0        
                //   126: getfield        com/android/settings/users/EditUserPhotoController$6.this$0:Lcom/android/settings/users/EditUserPhotoController;
                //   129: invokestatic    com/android/settings/users/EditUserPhotoController.access$700:(Lcom/android/settings/users/EditUserPhotoController;)I
                //   132: aload_0        
                //   133: getfield        com/android/settings/users/EditUserPhotoController$6.this$0:Lcom/android/settings/users/EditUserPhotoController;
                //   136: invokestatic    com/android/settings/users/EditUserPhotoController.access$700:(Lcom/android/settings/users/EditUserPhotoController;)I
                //   139: getstatic       android/graphics/Bitmap$Config.ARGB_8888:Landroid/graphics/Bitmap$Config;
                //   142: invokestatic    android/graphics/Bitmap.createBitmap:(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
                //   145: astore_1       
                //   146: new             Landroid/graphics/Canvas;
                //   149: dup            
                //   150: aload_1        
                //   151: invokespecial   android/graphics/Canvas.<init>:(Landroid/graphics/Bitmap;)V
                //   154: astore_2       
                //   155: aload_0        
                //   156: getfield        com/android/settings/users/EditUserPhotoController$6.this$0:Lcom/android/settings/users/EditUserPhotoController;
                //   159: invokestatic    com/android/settings/users/EditUserPhotoController.access$300:(Lcom/android/settings/users/EditUserPhotoController;)Landroid/content/Context;
                //   162: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
                //   165: aload_0        
                //   166: getfield        com/android/settings/users/EditUserPhotoController$6.val$data:Landroid/net/Uri;
                //   169: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
                //   172: invokestatic    android/graphics/BitmapFactory.decodeStream:(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
                //   175: astore_3       
                //   176: aload_3        
                //   177: ifnull          273
                //   180: aload_3        
                //   181: invokevirtual   android/graphics/Bitmap.getWidth:()I
                //   184: aload_3        
                //   185: invokevirtual   android/graphics/Bitmap.getHeight:()I
                //   188: invokestatic    java/lang/Math.min:(II)I
                //   191: istore          5
                //   193: aload_3        
                //   194: invokevirtual   android/graphics/Bitmap.getWidth:()I
                //   197: iload           5
                //   199: isub           
                //   200: iconst_2       
                //   201: idiv           
                //   202: istore          6
                //   204: aload_3        
                //   205: invokevirtual   android/graphics/Bitmap.getHeight:()I
                //   208: iload           5
                //   210: isub           
                //   211: iconst_2       
                //   212: idiv           
                //   213: istore          7
                //   215: aload_2        
                //   216: aload_3        
                //   217: new             Landroid/graphics/Rect;
                //   220: dup            
                //   221: iload           6
                //   223: iload           7
                //   225: iload           6
                //   227: iload           5
                //   229: iadd           
                //   230: iload           7
                //   232: iload           5
                //   234: iadd           
                //   235: invokespecial   android/graphics/Rect.<init>:(IIII)V
                //   238: new             Landroid/graphics/Rect;
                //   241: dup            
                //   242: iconst_0       
                //   243: iconst_0       
                //   244: aload_0        
                //   245: getfield        com/android/settings/users/EditUserPhotoController$6.this$0:Lcom/android/settings/users/EditUserPhotoController;
                //   248: invokestatic    com/android/settings/users/EditUserPhotoController.access$700:(Lcom/android/settings/users/EditUserPhotoController;)I
                //   251: aload_0        
                //   252: getfield        com/android/settings/users/EditUserPhotoController$6.this$0:Lcom/android/settings/users/EditUserPhotoController;
                //   255: invokestatic    com/android/settings/users/EditUserPhotoController.access$700:(Lcom/android/settings/users/EditUserPhotoController;)I
                //   258: invokespecial   android/graphics/Rect.<init>:(IIII)V
                //   261: new             Landroid/graphics/Paint;
                //   264: dup            
                //   265: invokespecial   android/graphics/Paint.<init>:()V
                //   268: invokevirtual   android/graphics/Canvas.drawBitmap:(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
                //   271: aload_1        
                //   272: areturn        
                //   273: aconst_null    
                //   274: areturn        
                //   275: astore_1       
                //   276: aconst_null    
                //   277: areturn        
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                           
                //  -----  -----  -----  -----  -------------------------------
                //  11     29     67     102    Ljava/io/FileNotFoundException;
                //  11     29     63     125    Any
                //  33     39     67     102    Ljava/io/FileNotFoundException;
                //  33     39     63     125    Any
                //  43     47     50     60     Ljava/io/IOException;
                //  70     79     63     125    Any
                //  83     87     90     100    Ljava/io/IOException;
                //  106    110    113    123    Ljava/io/IOException;
                //  155    176    275    278    Ljava/io/FileNotFoundException;
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index: 151, Size: 151
                //     at java.util.ArrayList.rangeCheck(ArrayList.java:653)
                //     at java.util.ArrayList.get(ArrayList.java:429)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3569)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Thread.java:745)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
            
            protected void onPostExecute(final Bitmap bitmap) {
                if (bitmap != null) {
                    EditUserPhotoController.this.mNewUserPhotoBitmap = bitmap;
                    EditUserPhotoController.this.mNewUserPhotoDrawable = CircleFramedDrawable.getInstance(EditUserPhotoController.this.mImageView.getContext(), EditUserPhotoController.this.mNewUserPhotoBitmap);
                    EditUserPhotoController.this.mImageView.setImageDrawable(EditUserPhotoController.this.mNewUserPhotoDrawable);
                }
                new File(EditUserPhotoController.this.mContext.getCacheDir(), "TakeEditUserPhoto2.jpg").delete();
                new File(EditUserPhotoController.this.mContext.getCacheDir(), "CropEditUserPhoto.jpg").delete();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])null);
    }
    
    private void showUpdatePhotoPopup() {
        final boolean canTakePhoto = this.canTakePhoto();
        final boolean canChoosePhoto = this.canChoosePhoto();
        if (!canTakePhoto && !canChoosePhoto) {
            return;
        }
        final Context context = this.mImageView.getContext();
        final ArrayList<RestrictedMenuItem> list = new ArrayList<RestrictedMenuItem>();
        if (canTakePhoto) {
            list.add(new RestrictedMenuItem(context, context.getString(2131889736), "no_set_user_icon", new Runnable() {
                @Override
                public void run() {
                    EditUserPhotoController.this.takePhoto();
                }
            }));
        }
        if (canChoosePhoto) {
            list.add(new RestrictedMenuItem(context, context.getString(2131889734), "no_set_user_icon", new Runnable() {
                @Override
                public void run() {
                    EditUserPhotoController.this.choosePhoto();
                }
            }));
        }
        final ListPopupWindow listPopupWindow = new ListPopupWindow(context);
        listPopupWindow.setAnchorView((View)this.mImageView);
        listPopupWindow.setModal(true);
        listPopupWindow.setInputMethodMode(2);
        listPopupWindow.setAdapter((ListAdapter)new RestrictedPopupMenuAdapter(context, list));
        listPopupWindow.setWidth(Math.max(this.mImageView.getWidth(), context.getResources().getDimensionPixelSize(2131165680)));
        listPopupWindow.setDropDownGravity(8388611);
        listPopupWindow.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                listPopupWindow.dismiss();
                ((RestrictedMenuItem)adapterView.getAdapter().getItem(n)).doAction();
            }
        });
        listPopupWindow.show();
    }
    
    private void takePhoto() {
        final Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        this.appendOutputExtra(intent, this.mTakePictureUri);
        this.mFragment.startActivityForResult(intent, 1002);
    }
    
    public Bitmap getNewUserPhotoBitmap() {
        return this.mNewUserPhotoBitmap;
    }
    
    public Drawable getNewUserPhotoDrawable() {
        return this.mNewUserPhotoDrawable;
    }
    
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (n2 != -1) {
            return false;
        }
        Uri uri;
        if (intent != null && intent.getData() != null) {
            uri = intent.getData();
        }
        else {
            uri = this.mTakePictureUri;
        }
        switch (n) {
            default: {
                return false;
            }
            case 1003: {
                this.onPhotoCropped(uri, true);
                return true;
            }
            case 1001:
            case 1002: {
                if (this.mTakePictureUri.equals((Object)uri)) {
                    this.cropPhoto();
                }
                else {
                    this.copyAndCropPhoto(uri);
                }
                return true;
            }
        }
    }
    
    void removeNewUserPhotoBitmapFile() {
        new File(this.mContext.getCacheDir(), "NewUserPhoto.png").delete();
    }
    
    File saveNewUserPhotoBitmap() {
        if (this.mNewUserPhotoBitmap == null) {
            return null;
        }
        try {
            final File file = new File(this.mContext.getCacheDir(), "NewUserPhoto.png");
            final FileOutputStream fileOutputStream = new FileOutputStream(file);
            this.mNewUserPhotoBitmap.compress(Bitmap$CompressFormat.PNG, 100, (OutputStream)fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            return file;
        }
        catch (IOException ex) {
            Log.e("EditUserPhotoController", "Cannot create temp file", (Throwable)ex);
            return null;
        }
    }
    
    private static final class RestrictedMenuItem
    {
        private final Runnable mAction;
        private final RestrictedLockUtils.EnforcedAdmin mAdmin;
        private final Context mContext;
        private final boolean mIsRestrictedByBase;
        private final String mTitle;
        
        public RestrictedMenuItem(final Context mContext, final String mTitle, final String s, final Runnable mAction) {
            this.mContext = mContext;
            this.mTitle = mTitle;
            this.mAction = mAction;
            final int myUserId = UserHandle.myUserId();
            this.mAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(mContext, s, myUserId);
            this.mIsRestrictedByBase = RestrictedLockUtils.hasBaseUserRestriction(this.mContext, s, myUserId);
        }
        
        final void doAction() {
            if (this.isRestrictedByBase()) {
                return;
            }
            if (this.isRestrictedByAdmin()) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mAdmin);
                return;
            }
            this.mAction.run();
        }
        
        final boolean isRestrictedByAdmin() {
            return this.mAdmin != null;
        }
        
        final boolean isRestrictedByBase() {
            return this.mIsRestrictedByBase;
        }
        
        @Override
        public String toString() {
            return this.mTitle;
        }
    }
    
    private static final class RestrictedPopupMenuAdapter extends ArrayAdapter<RestrictedMenuItem>
    {
        public RestrictedPopupMenuAdapter(final Context context, final List<RestrictedMenuItem> list) {
            super(context, 2131558714, 2131362725, (List)list);
        }
        
        public View getView(int visibility, final View view, final ViewGroup viewGroup) {
            final View view2 = super.getView(visibility, view, viewGroup);
            final RestrictedMenuItem restrictedMenuItem = (RestrictedMenuItem)this.getItem(visibility);
            final TextView textView = (TextView)view2.findViewById(2131362725);
            final ImageView imageView = (ImageView)view2.findViewById(R.id.restricted_icon);
            final boolean restrictedByAdmin = restrictedMenuItem.isRestrictedByAdmin();
            visibility = 0;
            textView.setEnabled(!restrictedByAdmin && !restrictedMenuItem.isRestrictedByBase());
            if (!restrictedMenuItem.isRestrictedByAdmin() || restrictedMenuItem.isRestrictedByBase()) {
                visibility = 8;
            }
            imageView.setVisibility(visibility);
            return view2;
        }
    }
}
