package com.android.settings.users;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.Utils;
import android.view.View;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import android.os.UserManager;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;

public final class UserDialogs
{
    public static Dialog createEnablePhoneCallsAndSmsDialog(final Context context, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        return (Dialog)new AlertDialog$Builder(context).setTitle(2131889725).setMessage(2131889724).setPositiveButton(R.string.okay, dialogInterface$OnClickListener).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
    }
    
    public static Dialog createEnablePhoneCallsDialog(final Context context, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        return (Dialog)new AlertDialog$Builder(context).setTitle(2131889727).setMessage(2131889726).setPositiveButton(R.string.okay, dialogInterface$OnClickListener).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
    }
    
    public static Dialog createRemoveDialog(final Context context, final int n, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        final UserInfo userInfo = ((UserManager)context.getSystemService("user")).getUserInfo(n);
        final AlertDialog$Builder setNegativeButton = new AlertDialog$Builder(context).setPositiveButton(2131889702, dialogInterface$OnClickListener).setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
        if (userInfo.isManagedProfile()) {
            setNegativeButton.setTitle(2131890212);
            final View removeManagedUserDialogView = createRemoveManagedUserDialogView(context, n);
            if (removeManagedUserDialogView != null) {
                setNegativeButton.setView(removeManagedUserDialogView);
            }
            else {
                setNegativeButton.setMessage(2131890211);
            }
        }
        else if (UserHandle.myUserId() == n) {
            setNegativeButton.setTitle(2131889695);
            setNegativeButton.setMessage(2131889694);
        }
        else if (userInfo.isRestricted()) {
            setNegativeButton.setTitle(2131889747);
            setNegativeButton.setMessage(2131889746);
        }
        else {
            setNegativeButton.setTitle(2131889696);
            setNegativeButton.setMessage(2131889693);
        }
        return (Dialog)setNegativeButton.create();
    }
    
    private static View createRemoveManagedUserDialogView(final Context context, final int n) {
        final PackageManager packageManager = context.getPackageManager();
        final ApplicationInfo adminApplicationInfo = Utils.getAdminApplicationInfo(context, n);
        if (adminApplicationInfo == null) {
            return null;
        }
        final View inflate = ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(2131558535, (ViewGroup)null);
        ((ImageView)inflate.findViewById(2131362061)).setImageDrawable(packageManager.getUserBadgedIcon(packageManager.getApplicationIcon(adminApplicationInfo), new UserHandle(n)));
        final CharSequence applicationLabel = packageManager.getApplicationLabel(adminApplicationInfo);
        final CharSequence userBadgedLabel = packageManager.getUserBadgedLabel(applicationLabel, new UserHandle(n));
        final TextView textView = (TextView)inflate.findViewById(2131362060);
        textView.setText(applicationLabel);
        if (!applicationLabel.toString().contentEquals(userBadgedLabel)) {
            textView.setContentDescription(userBadgedLabel);
        }
        return inflate;
    }
}
