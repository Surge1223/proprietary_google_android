package com.android.settings.users;

import android.os.UserHandle;
import java.util.List;
import android.content.Context;
import android.os.UserManager;

public class UserFeatureProviderImpl implements UserFeatureProvider
{
    UserManager mUm;
    
    public UserFeatureProviderImpl(final Context context) {
        this.mUm = (UserManager)context.getSystemService("user");
    }
    
    @Override
    public List<UserHandle> getUserProfiles() {
        return (List<UserHandle>)this.mUm.getUserProfiles();
    }
}
