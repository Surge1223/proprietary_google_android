package com.android.settings.users;

import android.os.UserHandle;
import com.android.settings.Utils;
import android.app.Fragment;
import android.content.Context;

public class AutoSyncWorkDataPreferenceController extends AutoSyncPersonalDataPreferenceController
{
    public AutoSyncWorkDataPreferenceController(final Context context, final Fragment fragment) {
        super(context, fragment);
        this.mUserHandle = Utils.getManagedProfileWithDisabled(this.mUserManager);
    }
    
    @Override
    public String getPreferenceKey() {
        return "auto_sync_work_account_data";
    }
    
    @Override
    public boolean isAvailable() {
        final UserHandle mUserHandle = this.mUserHandle;
        boolean b = true;
        if (mUserHandle == null || this.mUserManager.isManagedProfile() || this.mUserManager.isLinkedUser() || this.mUserManager.getProfiles(UserHandle.myUserId()).size() <= 1) {
            b = false;
        }
        return b;
    }
}
