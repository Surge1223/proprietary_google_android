package com.android.settings.users;

import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.Dialog;
import android.app.Activity;
import android.content.Context;
import com.android.settingslib.RestrictedLockUtils;
import java.util.Iterator;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.pm.UserInfo;
import android.support.v14.preference.SwitchPreference;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class UserDetailsSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnPreferenceClickListener
{
    private static final String TAG;
    private Bundle mDefaultGuestRestrictions;
    private boolean mGuestUser;
    private SwitchPreference mPhonePref;
    private Preference mRemoveUserPref;
    private UserInfo mUserInfo;
    private UserManager mUserManager;
    
    static {
        TAG = UserDetailsSettings.class.getSimpleName();
    }
    
    void enableCallsAndSms(final boolean checked) {
        this.mPhonePref.setChecked(checked);
        if (this.mGuestUser) {
            this.mDefaultGuestRestrictions.putBoolean("no_outgoing_calls", checked ^ true);
            this.mDefaultGuestRestrictions.putBoolean("no_sms", true);
            this.mUserManager.setDefaultGuestRestrictions(this.mDefaultGuestRestrictions);
            for (final UserInfo userInfo : this.mUserManager.getUsers(true)) {
                if (userInfo.isGuest()) {
                    final UserHandle of = UserHandle.of(userInfo.id);
                    for (final String s : this.mDefaultGuestRestrictions.keySet()) {
                        this.mUserManager.setUserRestriction(s, this.mDefaultGuestRestrictions.getBoolean(s), of);
                    }
                }
            }
        }
        else {
            final UserHandle of2 = UserHandle.of(this.mUserInfo.id);
            this.mUserManager.setUserRestriction("no_outgoing_calls", checked ^ true, of2);
            this.mUserManager.setUserRestriction("no_sms", checked ^ true, of2);
        }
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 3: {
                return 593;
            }
            case 2: {
                return 592;
            }
            case 1: {
                return 591;
            }
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 98;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mUserManager = (UserManager)((Context)activity).getSystemService("user");
        this.addPreferencesFromResource(2132082854);
        this.mPhonePref = (SwitchPreference)this.findPreference("enable_calling");
        this.mRemoveUserPref = this.findPreference("remove_user");
        if (!(this.mGuestUser = this.getArguments().getBoolean("guest_user", false))) {
            final int int1 = this.getArguments().getInt("user_id", -1);
            if (int1 == -1) {
                throw new RuntimeException("Arguments to this fragment must contain the user id");
            }
            this.mUserInfo = this.mUserManager.getUserInfo(int1);
            this.mPhonePref.setChecked(this.mUserManager.hasUserRestriction("no_outgoing_calls", new UserHandle(int1)) ^ true);
            this.mRemoveUserPref.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        }
        else {
            this.removePreference("remove_user");
            this.mPhonePref.setTitle(2131889723);
            this.mDefaultGuestRestrictions = this.mUserManager.getDefaultGuestRestrictions();
            this.mPhonePref.setChecked(this.mDefaultGuestRestrictions.getBoolean("no_outgoing_calls") ^ true);
        }
        if (RestrictedLockUtils.hasBaseUserRestriction((Context)activity, "no_remove_user", UserHandle.myUserId())) {
            this.removePreference("remove_user");
        }
        this.mPhonePref.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (this.getActivity() == null) {
            return null;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported dialogId ");
                sb.append(n);
                throw new IllegalArgumentException(sb.toString());
            }
            case 3: {
                return UserDialogs.createEnablePhoneCallsAndSmsDialog((Context)this.getActivity(), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserDetailsSettings.this.enableCallsAndSms(true);
                    }
                });
            }
            case 2: {
                return UserDialogs.createEnablePhoneCallsDialog((Context)this.getActivity(), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserDetailsSettings.this.enableCallsAndSms(true);
                    }
                });
            }
            case 1: {
                return UserDialogs.createRemoveDialog((Context)this.getActivity(), this.mUserInfo.id, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        UserDetailsSettings.this.removeUser();
                    }
                });
            }
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (Boolean.TRUE.equals(o)) {
            int n;
            if (this.mGuestUser) {
                n = 2;
            }
            else {
                n = 3;
            }
            this.showDialog(n);
            return false;
        }
        this.enableCallsAndSms(false);
        return true;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (preference != this.mRemoveUserPref) {
            return false;
        }
        if (this.mUserManager.isAdminUser()) {
            this.showDialog(1);
            return true;
        }
        throw new RuntimeException("Only admins can remove a user");
    }
    
    void removeUser() {
        this.mUserManager.removeUser(this.mUserInfo.id);
        this.finishFragment();
    }
}
