package com.android.settings.gestures;

import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.widget.VideoPreference;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.support.v7.preference.Preference;
import com.android.settings.core.TogglePreferenceController;

public abstract class GesturePreferenceController extends TogglePreferenceController implements OnPreferenceChangeListener, LifecycleObserver, OnCreate, OnPause, OnResume, OnSaveInstanceState
{
    static final String KEY_VIDEO_PAUSED = "key_video_paused";
    boolean mVideoPaused;
    private VideoPreference mVideoPreference;
    
    public GesturePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    protected boolean canHandleClicks() {
        return true;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mVideoPreference = (VideoPreference)preferenceScreen.findPreference(this.getVideoPrefKey());
        }
    }
    
    @Override
    public CharSequence getSummary() {
        final Context mContext = this.mContext;
        int n;
        if (this.isChecked()) {
            n = 2131887732;
        }
        else {
            n = 2131887731;
        }
        return mContext.getText(n);
    }
    
    protected abstract String getVideoPrefKey();
    
    @Override
    public void onCreate(final Bundle bundle) {
        if (bundle != null) {
            this.mVideoPaused = bundle.getBoolean("key_video_paused", false);
        }
    }
    
    @Override
    public void onPause() {
        if (this.mVideoPreference != null) {
            this.mVideoPaused = this.mVideoPreference.isVideoPaused();
            this.mVideoPreference.onViewInvisible();
        }
    }
    
    @Override
    public void onResume() {
        if (this.mVideoPreference != null) {
            this.mVideoPreference.onViewVisible(this.mVideoPaused);
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putBoolean("key_video_paused", this.mVideoPaused);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (preference != null) {
            preference.setEnabled(this.canHandleClicks());
        }
    }
}
