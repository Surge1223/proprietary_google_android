package com.android.settings.gestures;

import android.text.TextUtils;
import android.provider.Settings;
import android.content.SharedPreferences;
import com.android.settings.Utils;
import android.content.Context;

public class SwipeToNotificationPreferenceController extends GesturePreferenceController
{
    private static final int OFF = 0;
    private static final int ON = 1;
    private static final String PREF_KEY_VIDEO = "gesture_swipe_down_fingerprint_video";
    private static final String SECURE_KEY = "system_navigation_keys_enabled";
    
    public SwipeToNotificationPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    public static boolean isAvailable(final Context context) {
        return isGestureAvailable(context);
    }
    
    private static boolean isGestureAvailable(final Context context) {
        return Utils.hasFingerprintHardware(context) && context.getResources().getBoolean(17957045);
    }
    
    public static boolean isSuggestionComplete(final Context context, final SharedPreferences sharedPreferences) {
        final boolean gestureAvailable = isGestureAvailable(context);
        boolean b = false;
        if (!gestureAvailable || sharedPreferences.getBoolean("pref_swipe_to_notification_suggestion_complete", false)) {
            b = true;
        }
        return b;
    }
    
    public static boolean isSwipeToNotificationOn(final Context context) {
        final int int1 = Settings.Secure.getInt(context.getContentResolver(), "system_navigation_keys_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean setSwipeToNotification(final Context context, final boolean b) {
        return Settings.Secure.putInt(context.getContentResolver(), "system_navigation_keys_enabled", (int)(b ? 1 : 0));
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (isAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_swipe_down_fingerprint_video";
    }
    
    @Override
    public boolean isChecked() {
        return isSwipeToNotificationOn(this.mContext);
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_swipe_down_fingerprint");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        setSwipeToNotification(this.mContext, b);
        return true;
    }
}
