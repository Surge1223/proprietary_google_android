package com.android.settings.gestures;

import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.overlay.FeatureFactory;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class DoubleTapScreenSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082766;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    public int getHelpResource() {
        return 2131887808;
    }
    
    @Override
    protected String getLogTag() {
        return "DoubleTapScreen";
    }
    
    @Override
    public int getMetricsCategory() {
        return 754;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082766;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).getSharedPrefs(context).edit().putBoolean("pref_double_tap_screen_suggestion_complete", true).apply();
        this.use(DoubleTapScreenPreferenceController.class).setConfig(new AmbientDisplayConfiguration(context));
    }
}
