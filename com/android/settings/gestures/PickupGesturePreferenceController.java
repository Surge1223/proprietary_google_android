package com.android.settings.gestures;

import android.provider.Settings;
import android.text.TextUtils;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.content.SharedPreferences;
import android.os.UserHandle;
import android.content.Context;
import com.android.internal.hardware.AmbientDisplayConfiguration;

public class PickupGesturePreferenceController extends GesturePreferenceController
{
    private static final String PREF_KEY_VIDEO = "gesture_pick_up_video";
    private final int OFF;
    private final int ON;
    private final String SECURE_KEY;
    private AmbientDisplayConfiguration mAmbientConfig;
    private final String mPickUpPrefKey;
    private final int mUserId;
    
    public PickupGesturePreferenceController(final Context context, final String mPickUpPrefKey) {
        super(context, mPickUpPrefKey);
        this.ON = 1;
        this.OFF = 0;
        this.SECURE_KEY = "doze_pulse_on_pick_up";
        this.mUserId = UserHandle.myUserId();
        this.mPickUpPrefKey = mPickUpPrefKey;
    }
    
    public static boolean isSuggestionComplete(final Context context, final SharedPreferences sharedPreferences) {
        final AmbientDisplayConfiguration ambientDisplayConfiguration = new AmbientDisplayConfiguration(context);
        boolean b = false;
        if (sharedPreferences.getBoolean("pref_pickup_gesture_suggestion_complete", false) || !ambientDisplayConfiguration.pulseOnPickupAvailable()) {
            b = true;
        }
        return b;
    }
    
    public boolean canHandleClicks() {
        return this.pulseOnPickupCanBeModified();
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mAmbientConfig == null) {
            this.mAmbientConfig = new AmbientDisplayConfiguration(this.mContext);
        }
        if (!this.mAmbientConfig.dozePulsePickupSensorAvailable()) {
            return 2;
        }
        if (!this.mAmbientConfig.ambientDisplayAvailable()) {
            return 4;
        }
        return 0;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPickUpPrefKey;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("doze_pulse_on_pick_up", 2, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, PickupGestureSettings.class.getName(), this.mPickUpPrefKey, this.mContext.getString(2131887447)), this.isAvailable(), 1);
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_pick_up_video";
    }
    
    @Override
    public boolean isChecked() {
        return this.mAmbientConfig.pulseOnPickupEnabled(this.mUserId);
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_pick_up");
    }
    
    boolean pulseOnPickupCanBeModified() {
        return this.mAmbientConfig.pulseOnPickupCanBeModified(this.mUserId);
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.Secure.putInt(this.mContext.getContentResolver(), "doze_pulse_on_pick_up", (int)(b ? 1 : 0));
    }
    
    public PickupGesturePreferenceController setConfig(final AmbientDisplayConfiguration mAmbientConfig) {
        this.mAmbientConfig = mAmbientConfig;
        return this;
    }
}
