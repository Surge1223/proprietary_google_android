package com.android.settings.gestures;

import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class GestureSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private AmbientDisplayConfiguration mAmbientDisplayConfig;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("gesture_assist_input_summary");
                nonIndexableKeys.add("gesture_swipe_down_fingerprint_input_summary");
                nonIndexableKeys.add("gesture_double_tap_power_input_summary");
                nonIndexableKeys.add("gesture_double_twist_input_summary");
                nonIndexableKeys.add("gesture_swipe_up_input_summary");
                nonIndexableKeys.add("gesture_double_tap_screen_input_summary");
                nonIndexableKeys.add("gesture_pick_up_input_summary");
                nonIndexableKeys.add("gesture_prevent_ringing_summary");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082776;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private AmbientDisplayConfiguration getConfig(final Context context) {
        if (this.mAmbientDisplayConfig == null) {
            this.mAmbientDisplayConfig = new AmbientDisplayConfiguration(context);
        }
        return this.mAmbientDisplayConfig;
    }
    
    @Override
    protected String getLogTag() {
        return "GestureSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 459;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082776;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(AssistGestureSettingsPreferenceController.class).setAssistOnly(false);
        this.use(PickupGesturePreferenceController.class).setConfig(this.getConfig(context));
        this.use(DoubleTapScreenPreferenceController.class).setConfig(this.getConfig(context));
    }
}
