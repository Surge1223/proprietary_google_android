package com.android.settings.gestures;

import java.util.Collection;
import com.android.settings.overlay.FeatureFactory;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class AssistGestureSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082717;
                return Arrays.asList(searchIndexableResource);
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                final AssistGestureSettingsPreferenceController assistGestureSettingsPreferenceController = new AssistGestureSettingsPreferenceController(context, "gesture_assist_input_summary");
                assistGestureSettingsPreferenceController.setAssistOnly(false);
                return assistGestureSettingsPreferenceController.isAvailable();
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<AbstractPreferenceController>();
        list.addAll(FeatureFactory.getFactory(context).getAssistGestureFeatureProvider().getControllers(context, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    protected String getLogTag() {
        return "AssistGesture";
    }
    
    @Override
    public int getMetricsCategory() {
        return 996;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082717;
    }
}
