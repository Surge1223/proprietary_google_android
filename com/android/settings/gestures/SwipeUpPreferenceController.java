package com.android.settings.gestures;

import android.text.TextUtils;
import android.provider.Settings;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;

public class SwipeUpPreferenceController extends GesturePreferenceController
{
    private static final String ACTION_QUICKSTEP = "android.intent.action.QUICKSTEP_SERVICE";
    private static final String PREF_KEY_VIDEO = "gesture_swipe_up_video";
    private final int OFF;
    private final int ON;
    private final UserManager mUserManager;
    
    public SwipeUpPreferenceController(final Context context, final String s) {
        super(context, s);
        this.ON = 1;
        this.OFF = 0;
        this.mUserManager = (UserManager)context.getSystemService("user");
    }
    
    static boolean isGestureAvailable(final Context context) {
        return context.getResources().getBoolean(17957053) && context.getPackageManager().resolveService(new Intent("android.intent.action.QUICKSTEP_SERVICE").setPackage(ComponentName.unflattenFromString(context.getString(17039711)).getPackageName()), 1048576) != null;
    }
    
    public static void setSwipeUpPreference(final Context context, final UserManager userManager, final int n) {
        Settings.Secure.putInt(context.getContentResolver(), "swipe_up_to_switch_apps_enabled", n);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (isGestureAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_swipe_up_video";
    }
    
    @Override
    public boolean isChecked() {
        return Settings.Secure.getInt(this.mContext.getContentResolver(), "swipe_up_to_switch_apps_enabled", (int)(this.mContext.getResources().getBoolean(17957052) ? 1 : 0)) != 0;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_swipe_up");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        setSwipeUpPreference(this.mContext, this.mUserManager, b ? 1 : 0);
        return true;
    }
}
