package com.android.settings.gestures;

import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class PreventRingingGestureSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082812;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    public int getHelpResource() {
        return 2131887780;
    }
    
    @Override
    protected String getLogTag() {
        return "RingingGestureSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1360;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082812;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
    }
}
