package com.android.settings.gestures;

import com.android.settings.overlay.FeatureFactory;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class DoubleTapPowerSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082765;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    protected String getLogTag() {
        return "DoubleTapPower";
    }
    
    @Override
    public int getMetricsCategory() {
        return 752;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082765;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).getSharedPrefs(context).edit().putBoolean("pref_double_tap_power_suggestion_complete", true).apply();
    }
}
