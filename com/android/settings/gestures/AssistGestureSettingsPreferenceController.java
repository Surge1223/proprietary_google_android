package com.android.settings.gestures;

import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import com.android.internal.annotations.VisibleForTesting;

public class AssistGestureSettingsPreferenceController extends GesturePreferenceController
{
    private static final int OFF = 0;
    private static final int ON = 1;
    private static final String PREF_KEY_VIDEO = "gesture_assist_video";
    private static final String SECURE_KEY_ASSIST = "assist_gesture_enabled";
    private static final String SECURE_KEY_SILENCE = "assist_gesture_silence_alerts_enabled";
    private final String mAssistGesturePrefKey;
    @VisibleForTesting
    boolean mAssistOnly;
    private final AssistGestureFeatureProvider mFeatureProvider;
    private Preference mPreference;
    private PreferenceScreen mScreen;
    private boolean mWasAvailable;
    
    public AssistGestureSettingsPreferenceController(final Context context, final String mAssistGesturePrefKey) {
        super(context, mAssistGesturePrefKey);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getAssistGestureFeatureProvider();
        this.mWasAvailable = this.isAvailable();
        this.mAssistGesturePrefKey = mAssistGesturePrefKey;
    }
    
    private boolean isAssistGestureEnabled() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Secure.getInt(contentResolver, "assist_gesture_enabled", 1) == 0) {
            b = false;
        }
        return b;
    }
    
    private boolean isSilenceGestureEnabled() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Secure.getInt(contentResolver, "assist_gesture_silence_alerts_enabled", 1) == 0) {
            b = false;
        }
        return b;
    }
    
    private void updatePreference() {
        if (this.mPreference == null) {
            return;
        }
        if (this.isAvailable()) {
            if (this.mScreen.findPreference(this.getPreferenceKey()) == null) {
                this.mScreen.addPreference(this.mPreference);
            }
        }
        else {
            this.mScreen.removePreference(this.mPreference);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        this.mScreen = mScreen;
        this.mPreference = mScreen.findPreference(this.getPreferenceKey());
        super.displayPreference(mScreen);
    }
    
    @Override
    public int getAvailabilityStatus() {
        boolean b;
        if (this.mAssistOnly) {
            b = this.mFeatureProvider.isSupported(this.mContext);
        }
        else {
            b = this.mFeatureProvider.isSensorAvailable(this.mContext);
        }
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("assist_gesture_enabled", 2, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, AssistGestureSettings.class.getName(), this.mAssistGesturePrefKey, this.mContext.getString(2131887447)), this.isAvailable(), 1);
    }
    
    @Override
    public CharSequence getSummary() {
        final boolean assistGestureEnabled = this.isAssistGestureEnabled();
        final boolean b = false;
        boolean b3;
        final boolean b2 = b3 = (assistGestureEnabled && this.mFeatureProvider.isSupported(this.mContext));
        if (!this.mAssistOnly) {
            b3 = (b2 || this.isSilenceGestureEnabled() || b);
        }
        final Context mContext = this.mContext;
        int n;
        if (b3) {
            n = 2131887732;
        }
        else {
            n = 2131887731;
        }
        return mContext.getText(n);
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_assist_video";
    }
    
    @Override
    public boolean isChecked() {
        final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), "assist_gesture_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void onResume() {
        if (this.mWasAvailable != this.isAvailable()) {
            this.updatePreference();
            this.mWasAvailable = this.isAvailable();
        }
    }
    
    public AssistGestureSettingsPreferenceController setAssistOnly(final boolean mAssistOnly) {
        this.mAssistOnly = mAssistOnly;
        return this;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.Secure.putInt(this.mContext.getContentResolver(), "assist_gesture_enabled", (int)(b ? 1 : 0));
    }
}
