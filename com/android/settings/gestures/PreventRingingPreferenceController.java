package com.android.settings.gestures;

import android.support.v7.preference.ListPreference;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.widget.VideoPreference;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class PreventRingingPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnCreate, OnPause, OnResume, OnSaveInstanceState
{
    static final String KEY_VIDEO_PAUSED = "key_video_paused";
    private static final String PREF_KEY_VIDEO = "gesture_prevent_ringing_video";
    private final String SECURE_KEY;
    boolean mVideoPaused;
    private VideoPreference mVideoPreference;
    
    public PreventRingingPreferenceController(final Context context, final String s) {
        super(context, s);
        this.SECURE_KEY = "volume_hush_gesture";
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mVideoPreference = (VideoPreference)preferenceScreen.findPreference(this.getVideoPrefKey());
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(17957074)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        int n = 0;
        switch (Settings.Secure.getInt(this.mContext.getContentResolver(), "volume_hush_gesture", 1)) {
            default: {
                n = 2131888636;
                break;
            }
            case 2: {
                n = 2131888634;
                break;
            }
            case 1: {
                n = 2131888638;
                break;
            }
        }
        return this.mContext.getString(n);
    }
    
    protected String getVideoPrefKey() {
        return "gesture_prevent_ringing_video";
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        if (bundle != null) {
            this.mVideoPaused = bundle.getBoolean("key_video_paused", false);
        }
    }
    
    @Override
    public void onPause() {
        if (this.mVideoPreference != null) {
            this.mVideoPaused = this.mVideoPreference.isVideoPaused();
            this.mVideoPreference.onViewInvisible();
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "volume_hush_gesture", Integer.parseInt((String)o));
        preference.setSummary(this.getSummary());
        return true;
    }
    
    @Override
    public void onResume() {
        if (this.mVideoPreference != null) {
            this.mVideoPreference.onViewVisible(this.mVideoPaused);
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putBoolean("key_video_paused", this.mVideoPaused);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (preference != null && preference instanceof ListPreference) {
            final ListPreference listPreference = (ListPreference)preference;
            final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), "volume_hush_gesture", 1);
            switch (int1) {
                default: {
                    listPreference.setValue(String.valueOf(0));
                    break;
                }
                case 2: {
                    listPreference.setValue(String.valueOf(int1));
                    break;
                }
                case 1: {
                    listPreference.setValue(String.valueOf(int1));
                    break;
                }
            }
        }
    }
}
