package com.android.settings.gestures;

import android.provider.Settings;
import android.text.TextUtils;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.content.SharedPreferences;
import android.os.UserHandle;
import android.content.Context;
import com.android.internal.hardware.AmbientDisplayConfiguration;

public class DoubleTapScreenPreferenceController extends GesturePreferenceController
{
    private static final String PREF_KEY_VIDEO = "gesture_double_tap_screen_video";
    private final int OFF;
    private final int ON;
    private final String SECURE_KEY;
    private AmbientDisplayConfiguration mAmbientConfig;
    private final String mDoubleTapScreenPrefKey;
    private final int mUserId;
    
    public DoubleTapScreenPreferenceController(final Context context, final String mDoubleTapScreenPrefKey) {
        super(context, mDoubleTapScreenPrefKey);
        this.ON = 1;
        this.OFF = 0;
        this.SECURE_KEY = "doze_pulse_on_double_tap";
        this.mUserId = UserHandle.myUserId();
        this.mDoubleTapScreenPrefKey = mDoubleTapScreenPrefKey;
    }
    
    public static boolean isSuggestionComplete(final Context context, final SharedPreferences sharedPreferences) {
        return isSuggestionComplete(new AmbientDisplayConfiguration(context), sharedPreferences);
    }
    
    static boolean isSuggestionComplete(final AmbientDisplayConfiguration ambientDisplayConfiguration, final SharedPreferences sharedPreferences) {
        final boolean pulseOnDoubleTapAvailable = ambientDisplayConfiguration.pulseOnDoubleTapAvailable();
        boolean b = false;
        if (!pulseOnDoubleTapAvailable || sharedPreferences.getBoolean("pref_double_tap_screen_suggestion_complete", false)) {
            b = true;
        }
        return b;
    }
    
    @Override
    protected boolean canHandleClicks() {
        return this.mAmbientConfig.alwaysOnEnabled(this.mUserId) ^ true;
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mAmbientConfig == null) {
            this.mAmbientConfig = new AmbientDisplayConfiguration(this.mContext);
        }
        if (!this.mAmbientConfig.doubleTapSensorAvailable()) {
            return 2;
        }
        if (!this.mAmbientConfig.ambientDisplayAvailable()) {
            return 4;
        }
        return 0;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("doze_pulse_on_double_tap", 2, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, DoubleTapScreenSettings.class.getName(), this.mDoubleTapScreenPrefKey, this.mContext.getString(2131887447)), this.isAvailable(), 1);
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_double_tap_screen_video";
    }
    
    @Override
    public boolean isChecked() {
        return this.mAmbientConfig.pulseOnDoubleTapEnabled(this.mUserId);
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_double_tap_screen");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.Secure.putInt(this.mContext.getContentResolver(), "doze_pulse_on_double_tap", (int)(b ? 1 : 0));
    }
    
    public DoubleTapScreenPreferenceController setConfig(final AmbientDisplayConfiguration mAmbientConfig) {
        this.mAmbientConfig = mAmbientConfig;
        return this;
    }
}
