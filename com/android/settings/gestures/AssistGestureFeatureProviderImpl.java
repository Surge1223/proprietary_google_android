package com.android.settings.gestures;

import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class AssistGestureFeatureProviderImpl implements AssistGestureFeatureProvider
{
    @Override
    public List<AbstractPreferenceController> getControllers(final Context context, final Lifecycle lifecycle) {
        return new ArrayList<AbstractPreferenceController>();
    }
    
    @Override
    public boolean isSensorAvailable(final Context context) {
        return false;
    }
    
    @Override
    public boolean isSupported(final Context context) {
        return false;
    }
}
