package com.android.settings.gestures;

import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public interface AssistGestureFeatureProvider
{
    List<AbstractPreferenceController> getControllers(final Context p0, final Lifecycle p1);
    
    boolean isSensorAvailable(final Context p0);
    
    boolean isSupported(final Context p0);
}
