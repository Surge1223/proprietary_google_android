package com.android.settings.gestures;

import android.content.ContentResolver;
import android.provider.Settings;
import android.content.SharedPreferences;
import java.util.Iterator;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.text.TextUtils;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;

public class DoubleTwistPreferenceController extends GesturePreferenceController
{
    private static final String PREF_KEY_VIDEO = "gesture_double_twist_video";
    private final int OFF;
    private final int ON;
    private final String mDoubleTwistPrefKey;
    private final UserManager mUserManager;
    
    public DoubleTwistPreferenceController(final Context context, final String mDoubleTwistPrefKey) {
        super(context, mDoubleTwistPrefKey);
        this.ON = 1;
        this.OFF = 0;
        this.mDoubleTwistPrefKey = mDoubleTwistPrefKey;
        this.mUserManager = (UserManager)context.getSystemService("user");
    }
    
    public static int getManagedProfileId(final UserManager userManager) {
        return Utils.getManagedProfileId(userManager, UserHandle.myUserId());
    }
    
    public static boolean isGestureAvailable(final Context context) {
        final Resources resources = context.getResources();
        final String string = resources.getString(2131887724);
        final String string2 = resources.getString(2131887725);
        if (!TextUtils.isEmpty((CharSequence)string) && !TextUtils.isEmpty((CharSequence)string2)) {
            for (final Sensor sensor : ((SensorManager)context.getSystemService("sensor")).getSensorList(-1)) {
                if (string.equals(sensor.getName()) && string2.equals(sensor.getVendor())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static boolean isSuggestionComplete(final Context context, final SharedPreferences sharedPreferences) {
        final boolean gestureAvailable = isGestureAvailable(context);
        boolean b = false;
        if (!gestureAvailable || sharedPreferences.getBoolean("pref_double_twist_suggestion_complete", false)) {
            b = true;
        }
        return b;
    }
    
    public static void setDoubleTwistPreference(final Context context, final UserManager userManager, final int n) {
        Settings.Secure.putInt(context.getContentResolver(), "camera_double_twist_to_flip_enabled", n);
        final int managedProfileId = getManagedProfileId(userManager);
        if (managedProfileId != -10000) {
            Settings.Secure.putIntForUser(context.getContentResolver(), "camera_double_twist_to_flip_enabled", n, managedProfileId);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (isGestureAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mDoubleTwistPrefKey;
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_double_twist_video";
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Secure.getInt(contentResolver, "camera_double_twist_to_flip_enabled", 1) == 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_double_twist");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        setDoubleTwistPreference(this.mContext, this.mUserManager, b ? 1 : 0);
        return true;
    }
}
