package com.android.settings.gestures;

import android.content.ContentResolver;
import android.provider.Settings;
import java.util.Iterator;
import java.util.ArrayList;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.core.BasePreferenceController;

public class GesturesSettingPreferenceController extends BasePreferenceController
{
    private static final String FAKE_PREF_KEY = "fake_key_only_for_get_available";
    private static final String KEY_GESTURES_SETTINGS = "gesture_settings";
    private final AssistGestureFeatureProvider mFeatureProvider;
    private List<AbstractPreferenceController> mGestureControllers;
    
    public GesturesSettingPreferenceController(final Context context) {
        super(context, "gesture_settings");
        this.mFeatureProvider = FeatureFactory.getFactory(context).getAssistGestureFeatureProvider();
    }
    
    private static List<AbstractPreferenceController> buildAllPreferenceControllers(final Context context) {
        final AmbientDisplayConfiguration ambientDisplayConfiguration = new AmbientDisplayConfiguration(context);
        final ArrayList<SwipeToNotificationPreferenceController> list = (ArrayList<SwipeToNotificationPreferenceController>)new ArrayList<PreventRingingPreferenceController>();
        list.add((PreventRingingPreferenceController)new AssistGestureSettingsPreferenceController(context, "fake_key_only_for_get_available").setAssistOnly(false));
        list.add((PreventRingingPreferenceController)new SwipeToNotificationPreferenceController(context, "fake_key_only_for_get_available"));
        list.add((PreventRingingPreferenceController)new DoubleTwistPreferenceController(context, "fake_key_only_for_get_available"));
        list.add((PreventRingingPreferenceController)new DoubleTapPowerPreferenceController(context, "fake_key_only_for_get_available"));
        list.add((PreventRingingPreferenceController)new PickupGesturePreferenceController(context, "fake_key_only_for_get_available").setConfig(ambientDisplayConfiguration));
        list.add((PreventRingingPreferenceController)new DoubleTapScreenPreferenceController(context, "fake_key_only_for_get_available").setConfig(ambientDisplayConfiguration));
        list.add(new PreventRingingPreferenceController(context, "fake_key_only_for_get_available"));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mGestureControllers == null) {
            this.mGestureControllers = buildAllPreferenceControllers(this.mContext);
        }
        boolean b = false;
        final Iterator<AbstractPreferenceController> iterator = this.mGestureControllers.iterator();
        boolean b2;
        while (true) {
            final boolean hasNext = iterator.hasNext();
            b2 = false;
            final boolean b3 = false;
            if (!hasNext) {
                break;
            }
            final AbstractPreferenceController abstractPreferenceController = iterator.next();
            b = (b || abstractPreferenceController.isAvailable() || b3);
        }
        int n;
        if (b) {
            n = (b2 ? 1 : 0);
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        if (!this.mFeatureProvider.isSensorAvailable(this.mContext)) {
            return "";
        }
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        final boolean b2 = Settings.Secure.getInt(contentResolver, "assist_gesture_enabled", 1) != 0;
        if (Settings.Secure.getInt(contentResolver, "assist_gesture_silence_alerts_enabled", 1) == 0) {
            b = false;
        }
        if (this.mFeatureProvider.isSupported(this.mContext) && b2) {
            return this.mContext.getText(2131888009);
        }
        if (b) {
            return this.mContext.getText(2131888008);
        }
        return this.mContext.getText(2131888007);
    }
}
