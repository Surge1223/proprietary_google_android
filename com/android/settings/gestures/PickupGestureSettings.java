package com.android.settings.gestures;

import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.overlay.FeatureFactory;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class PickupGestureSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082802;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    public int getHelpResource() {
        return 2131887823;
    }
    
    @Override
    protected String getLogTag() {
        return "PickupGestureSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 753;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082802;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).getSharedPrefs(context).edit().putBoolean("pref_pickup_gesture_suggestion_complete", true).apply();
        this.use(PickupGesturePreferenceController.class).setConfig(new AmbientDisplayConfiguration(context));
    }
}
