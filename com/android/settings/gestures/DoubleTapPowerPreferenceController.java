package com.android.settings.gestures;

import android.text.TextUtils;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.content.SharedPreferences;
import android.content.Context;

public class DoubleTapPowerPreferenceController extends GesturePreferenceController
{
    static final int OFF = 1;
    static final int ON = 0;
    private static final String PREF_KEY_VIDEO = "gesture_double_tap_power_video";
    private final String SECURE_KEY;
    private final String mDoubleTapPowerKey;
    
    public DoubleTapPowerPreferenceController(final Context context, final String mDoubleTapPowerKey) {
        super(context, mDoubleTapPowerKey);
        this.SECURE_KEY = "camera_double_tap_power_gesture_disabled";
        this.mDoubleTapPowerKey = mDoubleTapPowerKey;
    }
    
    private static boolean isGestureAvailable(final Context context) {
        return context.getResources().getBoolean(17956909);
    }
    
    public static boolean isSuggestionComplete(final Context context, final SharedPreferences sharedPreferences) {
        final boolean gestureAvailable = isGestureAvailable(context);
        boolean b = false;
        if (!gestureAvailable || sharedPreferences.getBoolean("pref_double_tap_power_suggestion_complete", false)) {
            b = true;
        }
        return b;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (isGestureAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("camera_double_tap_power_gesture_disabled", 2, 0, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, DoubleTapPowerSettings.class.getName(), this.mDoubleTapPowerKey, this.mContext.getString(2131887447)), this.isAvailable(), 0);
    }
    
    @Override
    protected String getVideoPrefKey() {
        return "gesture_double_tap_power_video";
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, "camera_double_tap_power_gesture_disabled", 0) == 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"gesture_double_tap_power");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.Secure.putInt(this.mContext.getContentResolver(), "camera_double_tap_power_gesture_disabled", (int)((b ^ true) ? 1 : 0));
    }
}
