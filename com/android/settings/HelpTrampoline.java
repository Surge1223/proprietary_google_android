package com.android.settings;

import android.content.Intent;
import android.content.ActivityNotFoundException;
import android.content.res.Resources$NotFoundException;
import android.util.Log;
import android.content.Context;
import com.android.settingslib.HelpUtils;
import android.text.TextUtils;
import android.os.Bundle;
import android.app.Activity;

public class HelpTrampoline extends Activity
{
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        try {
            final String stringExtra = this.getIntent().getStringExtra("android.intent.extra.TEXT");
            if (TextUtils.isEmpty((CharSequence)stringExtra)) {
                this.finishAndRemoveTask();
                return;
            }
            final Intent helpIntent = HelpUtils.getHelpIntent((Context)this, this.getResources().getString(this.getResources().getIdentifier(stringExtra, "string", this.getPackageName())), null);
            if (helpIntent != null) {
                this.startActivityForResult(helpIntent, 0);
            }
        }
        catch (Resources$NotFoundException | ActivityNotFoundException ex) {
            final Throwable t;
            Log.w("HelpTrampoline", "Failed to resolve help", t);
        }
        this.finish();
    }
}
