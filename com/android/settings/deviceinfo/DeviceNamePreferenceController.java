package com.android.settings.deviceinfo;

import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.net.wifi.WifiConfiguration;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.os.Build;
import android.provider.Settings;
import android.text.Spanned;
import android.text.SpannedString;
import com.android.settings.bluetooth.BluetoothLengthDeviceNameFilter;
import android.content.Context;
import android.net.wifi.WifiManager;
import com.android.settings.wifi.tether.WifiDeviceNameTextValidator;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.widget.ValidatedEditTextPreference;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class DeviceNamePreferenceController extends BasePreferenceController implements OnPreferenceChangeListener, Validator, LifecycleObserver, OnCreate, OnSaveInstanceState
{
    public static final int DEVICE_NAME_SET_WARNING_ID = 1;
    private static final String KEY_PENDING_DEVICE_NAME = "key_pending_device_name";
    private static final String PREF_KEY = "device_name";
    private LocalBluetoothManager mBluetoothManager;
    private String mDeviceName;
    private DeviceNamePreferenceHost mHost;
    private String mPendingDeviceName;
    private ValidatedEditTextPreference mPreference;
    private final WifiDeviceNameTextValidator mWifiDeviceNameTextValidator;
    protected WifiManager mWifiManager;
    
    public DeviceNamePreferenceController(final Context context) {
        super(context, "device_name");
        this.mWifiManager = (WifiManager)context.getSystemService("wifi");
        this.mWifiDeviceNameTextValidator = new WifiDeviceNameTextValidator();
        this.initializeDeviceName();
    }
    
    private static final String getFilteredBluetoothString(final String s) {
        final CharSequence filter = new BluetoothLengthDeviceNameFilter().filter(s, 0, s.length(), (Spanned)new SpannedString((CharSequence)""), 0, 0);
        if (filter == null) {
            return s;
        }
        return filter.toString();
    }
    
    private void initializeDeviceName() {
        this.mDeviceName = Settings.Global.getString(this.mContext.getContentResolver(), "device_name");
        if (this.mDeviceName == null) {
            this.mDeviceName = Build.MODEL;
        }
    }
    
    private void setBluetoothDeviceName(final String s) {
        if (this.mBluetoothManager == null) {
            return;
        }
        final LocalBluetoothAdapter bluetoothAdapter = this.mBluetoothManager.getBluetoothAdapter();
        if (bluetoothAdapter != null) {
            bluetoothAdapter.setName(getFilteredBluetoothString(s));
        }
    }
    
    private void setDeviceName(final String tetherSsidName) {
        this.setSettingsGlobalDeviceName(this.mDeviceName = tetherSsidName);
        this.setBluetoothDeviceName(tetherSsidName);
        this.setTetherSsidName(tetherSsidName);
        this.mPreference.setSummary(this.getSummary());
    }
    
    private void setSettingsGlobalDeviceName(final String s) {
        Settings.Global.putString(this.mContext.getContentResolver(), "device_name", s);
    }
    
    private void setTetherSsidName(final String ssid) {
        final WifiConfiguration wifiApConfiguration = this.mWifiManager.getWifiApConfiguration();
        wifiApConfiguration.SSID = ssid;
        this.mWifiManager.setWifiApConfiguration(wifiApConfiguration);
    }
    
    public void confirmDeviceName() {
        if (this.mPendingDeviceName != null) {
            this.setDeviceName(this.mPendingDeviceName);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (ValidatedEditTextPreference)preferenceScreen.findPreference("device_name");
        final CharSequence summary = this.getSummary();
        this.mPreference.setSummary(summary);
        this.mPreference.setText(summary.toString());
        this.mPreference.setValidator((ValidatedEditTextPreference.Validator)this);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public String getPreferenceKey() {
        return "device_name";
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mDeviceName;
    }
    
    @Override
    public boolean isTextValid(final String s) {
        return this.mWifiDeviceNameTextValidator.isTextValid(s);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        if (bundle != null) {
            this.mPendingDeviceName = bundle.getString("key_pending_device_name", (String)null);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mPendingDeviceName = (String)o;
        if (this.mHost != null) {
            this.mHost.showDeviceNameWarningDialog(this.mPendingDeviceName);
        }
        return true;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putString("key_pending_device_name", this.mPendingDeviceName);
    }
    
    public void setHost(final DeviceNamePreferenceHost mHost) {
        this.mHost = mHost;
    }
    
    public void setLocalBluetoothManager(final LocalBluetoothManager mBluetoothManager) {
        this.mBluetoothManager = mBluetoothManager;
    }
    
    public interface DeviceNamePreferenceHost
    {
        void showDeviceNameWarningDialog(final String p0);
    }
}
