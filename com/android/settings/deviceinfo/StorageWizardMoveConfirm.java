package com.android.settings.deviceinfo;

import java.util.Iterator;
import android.content.Context;
import android.text.TextUtils;
import android.app.Activity;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.pm.UserInfo;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.content.pm.PackageManager;
import com.android.internal.util.Preconditions;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.content.pm.ApplicationInfo;

public class StorageWizardMoveConfirm extends StorageWizardBase
{
    private ApplicationInfo mApp;
    private String mPackageName;
    
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 100) {
            if (n2 == -1) {
                this.onNavigateNext(null);
            }
            else {
                Log.w("StorageSettings", "Failed to confirm credentials");
            }
        }
        else {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mVolume == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558769);
        try {
            this.mPackageName = this.getIntent().getStringExtra("android.intent.extra.PACKAGE_NAME");
            this.mApp = this.getPackageManager().getApplicationInfo(this.mPackageName, 0);
            Preconditions.checkState(this.getPackageManager().getPackageCandidateVolumes(this.mApp).contains(this.mVolume));
            final String string = this.getPackageManager().getApplicationLabel(this.mApp).toString();
            final String bestVolumeDescription = this.mStorage.getBestVolumeDescription(this.mVolume);
            this.setIcon(2131231166);
            this.setHeaderText(2131889368, string);
            this.setBodyText(2131889367, string, bestVolumeDescription);
            this.setNextButtonText(2131888325, new CharSequence[0]);
        }
        catch (PackageManager$NameNotFoundException ex) {
            this.finish();
        }
    }
    
    @Override
    public void onNavigateNext(final View view) {
        if (StorageManager.isFileEncryptedNativeOrEmulated()) {
            for (final UserInfo userInfo : ((UserManager)this.getSystemService((Class)UserManager.class)).getUsers()) {
                if (!StorageManager.isUserKeyUnlocked(userInfo.id)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("User ");
                    sb.append(userInfo.id);
                    sb.append(" is currently locked; requesting unlock");
                    Log.d("StorageSettings", sb.toString());
                    new ChooseLockSettingsHelper(this).launchConfirmationActivityForAnyUser(100, null, null, TextUtils.expandTemplate(this.getText(2131889372), new CharSequence[] { userInfo.name }), userInfo.id);
                    return;
                }
            }
        }
        final String string = this.getPackageManager().getApplicationLabel(this.mApp).toString();
        final int movePackage = this.getPackageManager().movePackage(this.mPackageName, this.mVolume);
        final Intent intent = new Intent((Context)this, (Class)StorageWizardMoveProgress.class);
        intent.putExtra("android.content.pm.extra.MOVE_ID", movePackage);
        intent.putExtra("android.intent.extra.TITLE", string);
        intent.putExtra("android.os.storage.extra.VOLUME_ID", this.mVolume.getId());
        this.startActivity(intent);
        this.finishAffinity();
    }
}
