package com.android.settings.deviceinfo;

import android.content.Intent;
import android.telephony.TelephonyManager;
import com.android.settings.deviceinfo.firmwareversion.FirmwareVersionPreferenceController;
import com.android.settings.deviceinfo.imei.ImeiInfoPreferenceController;
import com.android.settings.deviceinfo.simstatus.SimStatusPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Fragment;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class DeviceInfoSettings extends DashboardFragment implements Indexable
{
    static final int NON_SIM_PREFERENCES_COUNT = 2;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    static final int SIM_PREFERENCES_COUNT = 3;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new DeviceInfoSettings.SummaryProvider(summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("legal_container");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082758;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Activity activity, final Fragment fragment, final Lifecycle lifecycle) {
        final ArrayList<SimStatusPreferenceController> list = (ArrayList<SimStatusPreferenceController>)new ArrayList<BuildNumberPreferenceController>();
        list.add((BuildNumberPreferenceController)new PhoneNumberPreferenceController(context));
        list.add((BuildNumberPreferenceController)new SimStatusPreferenceController(context, fragment));
        list.add((BuildNumberPreferenceController)new DeviceModelPreferenceController(context, fragment));
        list.add((BuildNumberPreferenceController)new ImeiInfoPreferenceController(context, fragment));
        list.add((BuildNumberPreferenceController)new FirmwareVersionPreferenceController(context, fragment));
        list.add((BuildNumberPreferenceController)new IpAddressPreferenceController(context, lifecycle));
        list.add((BuildNumberPreferenceController)new WifiMacAddressPreferenceController(context, lifecycle));
        list.add((BuildNumberPreferenceController)new BluetoothAddressPreferenceController(context, lifecycle));
        list.add((BuildNumberPreferenceController)new RegulatoryInfoPreferenceController(context));
        list.add((BuildNumberPreferenceController)new SafetyInfoPreferenceController(context));
        list.add((BuildNumberPreferenceController)new ManualPreferenceController(context));
        list.add((BuildNumberPreferenceController)new FeedbackPreferenceController(fragment, context));
        list.add((BuildNumberPreferenceController)new FccEquipmentIdPreferenceController(context));
        list.add(new BuildNumberPreferenceController(context, activity, fragment, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getActivity(), this, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887769;
    }
    
    @Override
    public int getInitialExpandedChildCount() {
        return Math.max(3, ((TelephonyManager)this.getContext().getSystemService("phone")).getPhoneCount() * 3) + 2;
    }
    
    @Override
    protected String getLogTag() {
        return "DeviceInfoSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 40;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082758;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (this.use(BuildNumberPreferenceController.class).onActivityResult(n, n2, intent)) {
            return;
        }
        super.onActivityResult(n, n2, intent);
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final SummaryLoader mSummaryLoader) {
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, DeviceModelPreferenceController.getDeviceModel());
            }
        }
    }
}
