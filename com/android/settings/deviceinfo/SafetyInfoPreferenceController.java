package com.android.settings.deviceinfo;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.Intent;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class SafetyInfoPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private static final Intent INTENT_PROBE;
    private final PackageManager mPackageManager;
    
    static {
        INTENT_PROBE = new Intent("android.settings.SHOW_SAFETY_AND_REGULATORY_INFO");
    }
    
    public SafetyInfoPreferenceController(final Context context) {
        super(context);
        this.mPackageManager = this.mContext.getPackageManager();
    }
    
    @Override
    public String getPreferenceKey() {
        return "safety_info";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mPackageManager.queryIntentActivities(SafetyInfoPreferenceController.INTENT_PROBE, 0).isEmpty() ^ true;
    }
}
