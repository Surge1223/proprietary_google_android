package com.android.settings.deviceinfo;

import android.support.v7.preference.PreferenceScreen;
import com.android.settings.accounts.AccountDetailDashboardFragment;
import com.android.settings.core.SubSettingLauncher;
import android.os.Process;
import android.os.Parcelable;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import com.android.settings.accounts.AccountFeatureProvider;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.accounts.Account;
import com.android.settings.core.BasePreferenceController;

public class BrandedAccountPreferenceController extends BasePreferenceController
{
    private static final String KEY_PREFERENCE_TITLE = "branded_account";
    private final Account[] mAccounts;
    
    public BrandedAccountPreferenceController(final Context context) {
        super(context, "branded_account");
        this.mAccounts = FeatureFactory.getFactory(this.mContext).getAccountFeatureProvider().getAccounts(this.mContext);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final AccountFeatureProvider accountFeatureProvider = FeatureFactory.getFactory(this.mContext).getAccountFeatureProvider();
        final Preference preference = preferenceScreen.findPreference("branded_account");
        if (preference != null && (this.mAccounts == null || this.mAccounts.length == 0)) {
            preferenceScreen.removePreference(preference);
            return;
        }
        preference.setSummary(this.mAccounts[0].name);
        preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$BrandedAccountPreferenceController$rFwl4JPEzufcbKCkFgByL5d4NMI(this, accountFeatureProvider));
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mAccounts != null && this.mAccounts.length > 0) {
            return 0;
        }
        return 3;
    }
}
