package com.android.settings.deviceinfo;

import android.text.format.Formatter$BytesResult;
import java.io.File;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.app.Activity;
import com.android.internal.util.Preconditions;
import android.provider.DocumentsContract;
import android.app.ActivityManager;
import android.os.UserManager;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.os.Bundle;
import java.util.Objects;
import android.os.storage.VolumeRecord;
import android.content.Context;
import android.view.View;
import android.os.storage.VolumeInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.storage.StorageManager;
import android.os.storage.StorageEventListener;
import android.support.v7.preference.Preference;
import android.os.storage.DiskInfo;
import com.android.settings.SettingsPreferenceFragment;

public class PublicVolumeSettings extends SettingsPreferenceFragment
{
    private DiskInfo mDisk;
    private Preference mFormatPrivate;
    private Preference mFormatPublic;
    private boolean mIsPermittedToAdopt;
    private Preference mMount;
    private final StorageEventListener mStorageListener;
    private StorageManager mStorageManager;
    private StorageSummaryPreference mSummary;
    private Button mUnmount;
    private final View.OnClickListener mUnmountListener;
    private VolumeInfo mVolume;
    private String mVolumeId;
    
    public PublicVolumeSettings() {
        this.mUnmountListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                new StorageSettings.UnmountTask((Context)PublicVolumeSettings.this.getActivity(), PublicVolumeSettings.this.mVolume).execute((Object[])new Void[0]);
            }
        };
        this.mStorageListener = new StorageEventListener() {
            public void onVolumeRecordChanged(final VolumeRecord volumeRecord) {
                if (Objects.equals(PublicVolumeSettings.this.mVolume.getFsUuid(), volumeRecord.getFsUuid())) {
                    PublicVolumeSettings.this.mVolume = PublicVolumeSettings.this.mStorageManager.findVolumeById(PublicVolumeSettings.this.mVolumeId);
                    PublicVolumeSettings.this.update();
                }
            }
            
            public void onVolumeStateChanged(final VolumeInfo volumeInfo, final int n, final int n2) {
                if (Objects.equals(PublicVolumeSettings.this.mVolume.getId(), volumeInfo.getId())) {
                    PublicVolumeSettings.this.mVolume = volumeInfo;
                    PublicVolumeSettings.this.update();
                }
            }
        };
    }
    
    private void addPreference(final Preference preference) {
        preference.setOrder(Integer.MAX_VALUE);
        this.getPreferenceScreen().addPreference(preference);
    }
    
    private Preference buildAction(final int title) {
        final Preference preference = new Preference(this.getPrefContext());
        preference.setTitle(title);
        return preference;
    }
    
    private boolean isVolumeValid() {
        return this.mVolume != null && this.mVolume.getType() == 0 && this.mVolume.isMountedReadable();
    }
    
    @Override
    public int getMetricsCategory() {
        return 42;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        if (!this.isVolumeValid()) {
            return;
        }
        final int dimensionPixelSize = this.getResources().getDimensionPixelSize(2131165679);
        final ViewGroup buttonBar = this.getButtonBar();
        buttonBar.removeAllViews();
        buttonBar.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        buttonBar.addView((View)this.mUnmount, new ViewGroup.LayoutParams(-1, -2));
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mIsPermittedToAdopt = (UserManager.get((Context)activity).isAdminUser() && !ActivityManager.isUserAMonkey());
        this.mStorageManager = (StorageManager)((Context)activity).getSystemService((Class)StorageManager.class);
        if ("android.provider.action.DOCUMENT_ROOT_SETTINGS".equals(this.getActivity().getIntent().getAction())) {
            this.mVolume = this.mStorageManager.findVolumeByUuid(DocumentsContract.getRootId(this.getActivity().getIntent().getData()));
        }
        else {
            final String string = this.getArguments().getString("android.os.storage.extra.VOLUME_ID");
            if (string != null) {
                this.mVolume = this.mStorageManager.findVolumeById(string);
            }
        }
        if (!this.isVolumeValid()) {
            this.getActivity().finish();
            return;
        }
        Preconditions.checkNotNull((Object)(this.mDisk = this.mStorageManager.findDiskById(this.mVolume.getDiskId())));
        this.mVolumeId = this.mVolume.getId();
        this.addPreferencesFromResource(2132082761);
        this.getPreferenceScreen().setOrderingAsAdded(true);
        this.mSummary = new StorageSummaryPreference(this.getPrefContext());
        this.mMount = this.buildAction(2131889293);
        (this.mUnmount = new Button((Context)this.getActivity())).setText(2131889296);
        this.mUnmount.setOnClickListener(this.mUnmountListener);
        this.mFormatPublic = this.buildAction(2131889287);
        if (this.mIsPermittedToAdopt) {
            this.mFormatPrivate = this.buildAction(2131889288);
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mStorageManager.unregisterListener(this.mStorageListener);
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.mMount) {
            new StorageSettings.MountTask((Context)this.getActivity(), this.mVolume).execute((Object[])new Void[0]);
        }
        else if (preference == this.mFormatPublic) {
            StorageWizardFormatConfirm.showPublic(this.getActivity(), this.mDisk.getId());
        }
        else if (preference == this.mFormatPrivate) {
            StorageWizardFormatConfirm.showPrivate(this.getActivity(), this.mDisk.getId());
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mVolume = this.mStorageManager.findVolumeById(this.mVolumeId);
        if (!this.isVolumeValid()) {
            this.getActivity().finish();
            return;
        }
        this.mStorageManager.registerListener(this.mStorageListener);
        this.update();
    }
    
    public void update() {
        if (!this.isVolumeValid()) {
            this.getActivity().finish();
            return;
        }
        this.getActivity().setTitle((CharSequence)this.mStorageManager.getBestVolumeDescription(this.mVolume));
        final Activity activity = this.getActivity();
        this.getPreferenceScreen().removeAll();
        if (this.mVolume.isMountedReadable()) {
            this.addPreference(this.mSummary);
            final File path = this.mVolume.getPath();
            final long totalSpace = path.getTotalSpace();
            final long n = totalSpace - path.getFreeSpace();
            final Formatter$BytesResult formatBytes = Formatter.formatBytes(this.getResources(), n, 0);
            this.mSummary.setTitle(TextUtils.expandTemplate(this.getText(2131889309), new CharSequence[] { formatBytes.value, formatBytes.units }));
            this.mSummary.setSummary(this.getString(2131889323, new Object[] { Formatter.formatFileSize((Context)activity, totalSpace) }));
            this.mSummary.setPercent(n, totalSpace);
        }
        if (this.mVolume.getState() == 0) {
            this.addPreference(this.mMount);
        }
        if (this.mVolume.isMountedReadable()) {
            this.getButtonBar().setVisibility(0);
        }
        this.addPreference(this.mFormatPublic);
        if (this.mDisk.isAdoptable() && this.mIsPermittedToAdopt) {
            this.addPreference(this.mFormatPrivate);
        }
    }
}
