package com.android.settings.deviceinfo;

import java.util.function.Consumer;
import java.util.function.Predicate;
import android.content.Loader;
import android.os.Bundle;
import com.android.settingslib.core.AbstractPreferenceController;
import android.graphics.drawable.Drawable;
import android.app.LoaderManager$LoaderCallbacks;
import android.util.SparseArray;
import com.android.settings.deviceinfo.storage.UserIconLoader;

public final class _$$Lambda$StorageDashboardFragment$IconLoaderCallbacks$yGwysNy4Bq4_2nwwvU2QePhZgvU implements FetchUserIconTask
{
    @Override
    public final SparseArray getUserIcons() {
        return StorageDashboardFragment.IconLoaderCallbacks.lambda$onCreateLoader$0(this.f$0);
    }
}
