package com.android.settings.deviceinfo;

import android.view.View;
import android.os.Bundle;

public class StorageWizardReady extends StorageWizardBase
{
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mDisk == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558769);
        this.setHeaderText(2131889375, this.getDiskShortDescription());
        if (this.findFirstVolume(1) != null) {
            if (this.getIntent().getBooleanExtra("migrate_skip", false)) {
                this.setBodyText(2131889377, this.getDiskDescription());
            }
            else {
                this.setBodyText(2131889378, this.getDiskDescription(), this.getDiskShortDescription());
            }
        }
        else {
            this.setBodyText(2131889376, this.getDiskDescription());
        }
        this.setNextButtonText(2131887493, new CharSequence[0]);
    }
    
    @Override
    public void onNavigateNext(final View view) {
        this.finishAffinity();
    }
}
