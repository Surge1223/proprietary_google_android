package com.android.settings.deviceinfo;

import java.util.Iterator;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.graphics.Paint.Style;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.Collection;
import android.graphics.Paint;
import android.view.View;

public class PercentageBarChart extends View
{
    private final Paint mEmptyPaint;
    private Collection<Entry> mEntries;
    private int mMinTickWidth;
    
    public PercentageBarChart(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEmptyPaint = new Paint();
        this.mMinTickWidth = 1;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.PercentageBarChart);
        this.mMinTickWidth = obtainStyledAttributes.getDimensionPixelSize(1, 1);
        final int color = obtainStyledAttributes.getColor(0, -16777216);
        obtainStyledAttributes.recycle();
        this.mEmptyPaint.setColor(color);
        this.mEmptyPaint.setStyle(Paint.Style.FILL);
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final int paddingLeft = this.getPaddingLeft();
        final int n = this.getWidth() - this.getPaddingRight();
        final int paddingTop = this.getPaddingTop();
        final int n2 = this.getHeight() - this.getPaddingBottom();
        final int n3 = n - paddingLeft;
        if (this.isLayoutRtl()) {
            float n5;
            float n4 = n5 = n;
            if (this.mEntries != null) {
                final Iterator<Entry> iterator = this.mEntries.iterator();
                while (true) {
                    n5 = n4;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final Entry entry = iterator.next();
                    float max;
                    if (entry.percentage == 0.0f) {
                        max = 0.0f;
                    }
                    else {
                        max = Math.max(this.mMinTickWidth, n3 * entry.percentage);
                    }
                    final float n6 = n4 - max;
                    if (n6 < paddingLeft) {
                        canvas.drawRect((float)paddingLeft, (float)paddingTop, n4, (float)n2, entry.paint);
                        return;
                    }
                    canvas.drawRect(n6, (float)paddingTop, n4, (float)n2, entry.paint);
                    n4 = n6;
                }
            }
            canvas.drawRect((float)paddingLeft, (float)paddingTop, n5, (float)n2, this.mEmptyPaint);
        }
        else {
            float n8;
            float n7 = n8 = paddingLeft;
            if (this.mEntries != null) {
                final Iterator<Entry> iterator2 = this.mEntries.iterator();
                while (true) {
                    n8 = n7;
                    if (!iterator2.hasNext()) {
                        break;
                    }
                    final Entry entry2 = iterator2.next();
                    float max2;
                    if (entry2.percentage == 0.0f) {
                        max2 = 0.0f;
                    }
                    else {
                        max2 = Math.max(this.mMinTickWidth, n3 * entry2.percentage);
                    }
                    final float n9 = n7 + max2;
                    if (n9 > n) {
                        canvas.drawRect(n7, (float)paddingTop, (float)n, (float)n2, entry2.paint);
                        return;
                    }
                    canvas.drawRect(n7, (float)paddingTop, n9, (float)n2, entry2.paint);
                    n7 = n9;
                }
            }
            canvas.drawRect(n8, (float)paddingTop, (float)n, (float)n2, this.mEmptyPaint);
        }
    }
    
    public void setBackgroundColor(final int color) {
        this.mEmptyPaint.setColor(color);
    }
    
    public void setEntries(final Collection<Entry> mEntries) {
        this.mEntries = mEntries;
    }
    
    public static class Entry implements Comparable<Entry>
    {
        public final int order;
        public final Paint paint;
        public final float percentage;
        
        @Override
        public int compareTo(final Entry entry) {
            return this.order - entry.order;
        }
    }
}
