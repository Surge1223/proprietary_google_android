package com.android.settings.deviceinfo;

import com.android.settingslib.DeviceInfoUtils;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.app.Fragment;
import android.content.Intent;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class FeedbackPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final Intent intent;
    private final Fragment mHost;
    
    public FeedbackPreferenceController(final Fragment mHost, final Context context) {
        super(context);
        this.mHost = mHost;
        this.intent = new Intent("android.intent.action.BUG_REPORT");
    }
    
    @Override
    public String getPreferenceKey() {
        return "device_feedback";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"device_feedback")) {
            return false;
        }
        if (!this.isAvailable()) {
            return false;
        }
        this.mHost.startActivityForResult(this.intent, 0);
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return TextUtils.isEmpty((CharSequence)DeviceInfoUtils.getFeedbackReporterPackage(this.mContext)) ^ true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.intent.setPackage(DeviceInfoUtils.getFeedbackReporterPackage(this.mContext));
        preference.setIntent(this.intent);
        if (this.isAvailable() && !preference.isVisible()) {
            preference.setVisible(true);
        }
        else if (!this.isAvailable() && preference.isVisible()) {
            preference.setVisible(false);
        }
    }
}
