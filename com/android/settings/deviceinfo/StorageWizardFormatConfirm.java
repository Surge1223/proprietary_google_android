package com.android.settings.deviceinfo;

import android.os.storage.DiskInfo;
import android.content.DialogInterface$OnClickListener;
import android.text.TextUtils;
import android.app.AlertDialog$Builder;
import android.os.storage.StorageManager;
import android.app.Dialog;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.DialogInterface;
import android.content.Context;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class StorageWizardFormatConfirm extends InstrumentedDialogFragment
{
    private static void show(final Activity activity, final String s, final String s2, final boolean b) {
        final Bundle arguments = new Bundle();
        arguments.putString("android.os.storage.extra.DISK_ID", s);
        arguments.putString("format_forget_uuid", s2);
        arguments.putBoolean("format_private", b);
        final StorageWizardFormatConfirm storageWizardFormatConfirm = new StorageWizardFormatConfirm();
        storageWizardFormatConfirm.setArguments(arguments);
        storageWizardFormatConfirm.showAllowingStateLoss(activity.getFragmentManager(), "format_warning");
    }
    
    public static void showPrivate(final Activity activity, final String s) {
        show(activity, s, null, true);
    }
    
    public static void showPublic(final Activity activity, final String s) {
        show(activity, s, null, false);
    }
    
    @Override
    public int getMetricsCategory() {
        return 1375;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Context context = this.getContext();
        final Bundle arguments = this.getArguments();
        final String string = arguments.getString("android.os.storage.extra.DISK_ID");
        final String string2 = arguments.getString("format_forget_uuid");
        final boolean boolean1 = arguments.getBoolean("format_private", false);
        final DiskInfo diskById = ((StorageManager)context.getSystemService((Class)StorageManager.class)).findDiskById(string);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(context);
        alertDialog$Builder.setTitle(TextUtils.expandTemplate(this.getText(2131889332), new CharSequence[] { diskById.getShortDescription() }));
        alertDialog$Builder.setMessage(TextUtils.expandTemplate(this.getText(2131889331), new CharSequence[] { diskById.getDescription(), diskById.getShortDescription(), diskById.getShortDescription() }));
        alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
        alertDialog$Builder.setPositiveButton(TextUtils.expandTemplate(this.getText(2131889330), new CharSequence[] { diskById.getShortDescription() }), (DialogInterface$OnClickListener)new _$$Lambda$StorageWizardFormatConfirm$c4jIKjriuaEtVR7ERojcHILapk8(context, string, string2, boolean1));
        return (Dialog)alertDialog$Builder.create();
    }
}
