package com.android.settings.deviceinfo;

import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settingslib.applications.StorageStatsSource;
import android.os.UserManager;
import android.content.Loader;
import android.os.UserHandle;
import com.android.settings.Utils;
import android.os.Bundle;
import com.android.settingslib.deviceinfo.StorageVolumeProvider;
import android.app.Fragment;
import com.android.settingslib.deviceinfo.StorageManagerVolumeProvider;
import android.os.storage.StorageManager;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.os.storage.VolumeInfo;
import com.android.settings.deviceinfo.storage.StorageItemPreferenceController;
import com.android.settings.deviceinfo.storage.StorageAsyncLoader;
import android.util.SparseArray;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.dashboard.DashboardFragment;

public class StorageProfileFragment extends DashboardFragment implements LoaderManager$LoaderCallbacks<SparseArray<StorageAsyncLoader.AppsStorageResult>>
{
    private StorageItemPreferenceController mPreferenceController;
    private int mUserId;
    private VolumeInfo mVolume;
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<StorageItemPreferenceController> list = (ArrayList<StorageItemPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(this.mPreferenceController = new StorageItemPreferenceController(context, this, this.mVolume, new StorageManagerVolumeProvider((StorageManager)context.getSystemService((Class)StorageManager.class)), true));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "StorageProfileFragment";
    }
    
    public int getMetricsCategory() {
        return 845;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082837;
    }
    
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        arguments = this.getArguments();
        this.mVolume = Utils.maybeInitializeVolume((StorageManager)((Context)this.getActivity()).getSystemService((Class)StorageManager.class), arguments);
        if (this.mVolume == null) {
            this.getActivity().finish();
            return;
        }
        this.mPreferenceController.setVolume(this.mVolume);
        this.mUserId = arguments.getInt("userId", UserHandle.myUserId());
        this.mPreferenceController.setUserId(UserHandle.of(this.mUserId));
    }
    
    public Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>> onCreateLoader(final int n, final Bundle bundle) {
        final Context context = this.getContext();
        return (Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>>)new StorageAsyncLoader(context, (UserManager)context.getSystemService((Class)UserManager.class), this.mVolume.fsUuid, new StorageStatsSource(context), new PackageManagerWrapper(context.getPackageManager()));
    }
    
    public void onLoadFinished(final Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>> loader, final SparseArray<StorageAsyncLoader.AppsStorageResult> sparseArray) {
        this.mPreferenceController.onLoadFinished(sparseArray, this.mUserId);
    }
    
    public void onLoaderReset(final Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>> loader) {
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.getLoaderManager().initLoader(0, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
    }
    
    void setPreferenceController(final StorageItemPreferenceController mPreferenceController) {
        this.mPreferenceController = mPreferenceController;
    }
}
