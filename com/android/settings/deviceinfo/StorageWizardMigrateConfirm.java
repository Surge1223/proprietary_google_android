package com.android.settings.deviceinfo;

import java.util.Iterator;
import java.util.Objects;
import android.widget.Toast;
import android.text.TextUtils;
import android.app.Activity;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.pm.UserInfo;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;

public class StorageWizardMigrateConfirm extends StorageWizardBase
{
    private MigrateEstimateTask mEstimate;
    
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 100) {
            if (n2 == -1) {
                this.onNavigateNext(null);
            }
            else {
                Log.w("StorageSettings", "Failed to confirm credentials");
            }
        }
        else {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131558769);
        if (this.mVolume == null) {
            this.mVolume = this.findFirstVolume(1);
        }
        if (this.getPackageManager().getPrimaryStorageCurrentVolume() != null && this.mVolume != null) {
            this.setIcon(2131231166);
            this.setHeaderText(2131889366, this.getDiskShortDescription());
            this.setBodyText(2131888267, new CharSequence[0]);
            this.setAuxChecklist();
            (this.mEstimate = new MigrateEstimateTask(this) {
                @Override
                public void onPostExecute(final String s, final String s2) {
                    StorageWizardMigrateConfirm.this.setBodyText(2131889359, StorageWizardMigrateConfirm.this.getDiskDescription(), s, s2);
                }
            }).copyFrom(this.getIntent());
            this.mEstimate.execute((Object[])new Void[0]);
            this.setBackButtonText(2131889364, new CharSequence[0]);
            this.setNextButtonText(2131889365, new CharSequence[0]);
            return;
        }
        Log.d("StorageSettings", "Missing either source or target volume");
        this.finish();
    }
    
    @Override
    public void onNavigateBack(final View view) {
        FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1413, (Pair<Integer, Object>[])new Pair[0]);
        final Intent intent = new Intent((Context)this, (Class)StorageWizardReady.class);
        intent.putExtra("migrate_skip", true);
        this.startActivity(intent);
    }
    
    @Override
    public void onNavigateNext(final View view) {
        if (StorageManager.isFileEncryptedNativeOrEmulated()) {
            for (final UserInfo userInfo : ((UserManager)this.getSystemService((Class)UserManager.class)).getUsers()) {
                if (!StorageManager.isUserKeyUnlocked(userInfo.id)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("User ");
                    sb.append(userInfo.id);
                    sb.append(" is currently locked; requesting unlock");
                    Log.d("StorageSettings", sb.toString());
                    new ChooseLockSettingsHelper(this).launchConfirmationActivityForAnyUser(100, null, null, TextUtils.expandTemplate(this.getText(2131889372), new CharSequence[] { userInfo.name }), userInfo.id);
                    return;
                }
            }
        }
        try {
            final int movePrimaryStorage = this.getPackageManager().movePrimaryStorage(this.mVolume);
            FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1412, (Pair<Integer, Object>[])new Pair[0]);
            final Intent intent = new Intent((Context)this, (Class)StorageWizardMigrateProgress.class);
            intent.putExtra("android.os.storage.extra.VOLUME_ID", this.mVolume.getId());
            intent.putExtra("android.content.pm.extra.MOVE_ID", movePrimaryStorage);
            this.startActivity(intent);
            this.finishAffinity();
        }
        catch (IllegalStateException ex2) {
            Toast.makeText((Context)this, (CharSequence)this.getString(2131886316), 1).show();
            this.finishAffinity();
        }
        catch (IllegalArgumentException ex) {
            if (Objects.equals(this.mVolume.getFsUuid(), ((StorageManager)this.getSystemService("storage")).getPrimaryStorageVolume().getUuid())) {
                final Intent intent2 = new Intent((Context)this, (Class)StorageWizardReady.class);
                intent2.putExtra("android.os.storage.extra.DISK_ID", this.getIntent().getStringExtra("android.os.storage.extra.DISK_ID"));
                this.startActivity(intent2);
                this.finishAffinity();
                return;
            }
            throw ex;
        }
    }
}
