package com.android.settings.deviceinfo;

import android.content.pm.IPackageMoveObserver$Stub;
import android.os.SystemProperties;
import android.widget.Toast;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageManager;
import android.content.pm.IPackageMoveObserver;
import android.util.Log;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import android.os.IVoldTaskListener;
import android.os.IVoldTaskListener$Stub;
import android.os.PersistableBundle;
import java.util.concurrent.CompletableFuture;
import android.os.AsyncTask;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class StorageWizardFormatProgress extends StorageWizardBase
{
    private boolean mFormatPrivate;
    private PartitionTask mTask;
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mDisk == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558772);
        this.setKeepScreenOn(true);
        this.mFormatPrivate = this.getIntent().getBooleanExtra("format_private", false);
        this.setHeaderText(2131889334, this.getDiskShortDescription());
        this.setBodyText(2131889333, this.getDiskDescription());
        this.mTask = (PartitionTask)this.getLastNonConfigurationInstance();
        if (this.mTask == null) {
            (this.mTask = new PartitionTask()).setActivity(this);
            this.mTask.execute((Object[])new Void[0]);
        }
        else {
            this.mTask.setActivity(this);
        }
    }
    
    public void onFormatFinished() {
        final Intent intent = new Intent((Context)this, (Class)StorageWizardFormatSlow.class);
        intent.putExtra("format_slow", false);
        this.startActivity(intent);
        this.finishAffinity();
    }
    
    public void onFormatFinishedSlow() {
        final Intent intent = new Intent((Context)this, (Class)StorageWizardFormatSlow.class);
        intent.putExtra("format_slow", true);
        this.startActivity(intent);
        this.finishAffinity();
    }
    
    public Object onRetainNonConfigurationInstance() {
        return this.mTask;
    }
    
    public static class PartitionTask extends AsyncTask<Void, Integer, Exception>
    {
        public StorageWizardFormatProgress mActivity;
        private volatile long mPrivateBench;
        private volatile int mProgress;
        
        public PartitionTask() {
            this.mProgress = 20;
        }
        
        static /* synthetic */ void access$100(final PartitionTask partitionTask, final Object[] array) {
            partitionTask.publishProgress(array);
        }
        
        protected Exception doInBackground(final Void... array) {
            final StorageWizardFormatProgress mActivity = this.mActivity;
            final StorageManager mStorage = this.mActivity.mStorage;
            try {
                if (mActivity.mFormatPrivate) {
                    mStorage.partitionPrivate(mActivity.mDisk.getId());
                    this.publishProgress((Object[])new Integer[] { 40 });
                    final VolumeInfo firstVolume = mActivity.findFirstVolume(1, 25);
                    final CompletableFuture<PersistableBundle> completableFuture = new CompletableFuture<PersistableBundle>();
                    mStorage.benchmark(firstVolume.getId(), (IVoldTaskListener)new IVoldTaskListener$Stub() {
                        public void onFinished(final int n, final PersistableBundle persistableBundle) {
                            completableFuture.complete(persistableBundle);
                        }
                        
                        public void onStatus(final int n, final PersistableBundle persistableBundle) {
                            PartitionTask.access$100(PartitionTask.this, new Integer[] { 40 + n * 40 / 100 });
                        }
                    });
                    this.mPrivateBench = completableFuture.get(60L, TimeUnit.SECONDS).getLong("run", Long.MAX_VALUE);
                    if (mActivity.mDisk.isDefaultPrimary() && Objects.equals(mStorage.getPrimaryStorageUuid(), "primary_physical")) {
                        Log.d("StorageSettings", "Just formatted primary physical; silently moving storage to new emulated volume");
                        mStorage.setPrimaryStorageUuid(firstVolume.getFsUuid(), (IPackageMoveObserver)new SilentObserver());
                    }
                }
                else {
                    mStorage.partitionPublic(mActivity.mDisk.getId());
                }
                return null;
            }
            catch (Exception ex) {
                return ex;
            }
        }
        
        protected void onPostExecute(final Exception ex) {
            final StorageWizardFormatProgress mActivity = this.mActivity;
            if (mActivity.isDestroyed()) {
                return;
            }
            if (ex != null) {
                Log.e("StorageSettings", "Failed to partition", (Throwable)ex);
                Toast.makeText((Context)mActivity, (CharSequence)ex.getMessage(), 1).show();
                mActivity.finishAffinity();
                return;
            }
            if (mActivity.mFormatPrivate) {
                final StringBuilder sb = new StringBuilder();
                sb.append("New volume took ");
                sb.append(this.mPrivateBench);
                sb.append("ms to run benchmark");
                Log.d("StorageSettings", sb.toString());
                if (this.mPrivateBench <= 2000L && !SystemProperties.getBoolean("sys.debug.storage_slow", false)) {
                    this.mActivity.onFormatFinished();
                }
                else {
                    this.mActivity.onFormatFinishedSlow();
                }
            }
            else {
                this.mActivity.onFormatFinished();
            }
        }
        
        protected void onProgressUpdate(final Integer... array) {
            this.mProgress = array[0];
            this.mActivity.setCurrentProgress(this.mProgress);
        }
        
        public void setActivity(final StorageWizardFormatProgress mActivity) {
            (this.mActivity = mActivity).setCurrentProgress(this.mProgress);
        }
    }
    
    private static class SilentObserver extends IPackageMoveObserver$Stub
    {
        public void onCreated(final int n, final Bundle bundle) {
        }
        
        public void onStatusChanged(final int n, final int n2, final long n3) {
        }
    }
}
