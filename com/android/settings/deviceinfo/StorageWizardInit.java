package com.android.settings.deviceinfo;

import android.app.Activity;
import android.content.Intent;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.view.View;
import android.app.ActivityManager;
import android.content.Context;
import android.os.UserManager;
import android.os.Bundle;
import android.widget.Button;

public class StorageWizardInit extends StorageWizardBase
{
    private Button mExternal;
    private Button mInternal;
    private boolean mIsPermittedToAdopt;
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mDisk == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558770);
        this.mIsPermittedToAdopt = (UserManager.get((Context)this).isAdminUser() && !ActivityManager.isUserAMonkey());
        this.setHeaderText(2131889348, this.getDiskShortDescription());
        this.mExternal = (Button)this.requireViewById(2131362657);
        this.mInternal = (Button)this.requireViewById(2131362658);
        this.setBackButtonText(2131889346, new CharSequence[0]);
        if (!this.mDisk.isAdoptable()) {
            this.onNavigateExternal(null);
        }
        else if (!this.mIsPermittedToAdopt) {
            this.mInternal.setEnabled(false);
        }
    }
    
    @Override
    public void onNavigateBack(final View view) {
        this.finish();
    }
    
    public void onNavigateExternal(final View view) {
        if (view != null) {
            FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1407, (Pair<Integer, Object>[])new Pair[0]);
        }
        if (this.mVolume != null && this.mVolume.getType() == 0 && this.mVolume.getState() != 6) {
            this.mStorage.setVolumeInited(this.mVolume.getFsUuid(), true);
            final Intent intent = new Intent((Context)this, (Class)StorageWizardReady.class);
            intent.putExtra("android.os.storage.extra.DISK_ID", this.mDisk.getId());
            this.startActivity(intent);
            this.finish();
        }
        else {
            StorageWizardFormatConfirm.showPublic(this, this.mDisk.getId());
        }
    }
    
    public void onNavigateInternal(final View view) {
        if (view != null) {
            FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1408, (Pair<Integer, Object>[])new Pair[0]);
        }
        StorageWizardFormatConfirm.showPrivate(this, this.mDisk.getId());
    }
}
