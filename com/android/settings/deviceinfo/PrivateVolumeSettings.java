package com.android.settings.deviceinfo;

import android.os.Build.VERSION;
import android.view.View;
import android.widget.EditText;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Parcelable;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.pm.IPackageDataObserver;
import android.content.pm.PackageInfo;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.pm.IPackageDataObserver$Stub;
import com.android.settings.applications.manageapplications.ManageApplications;
import com.android.settings.Settings;
import android.app.Fragment;
import com.android.settings.core.SubSettingLauncher;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Environment;
import android.text.format.Formatter$BytesResult;
import android.support.v7.preference.PreferenceScreen;
import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.text.format.Formatter;
import com.android.settings.Utils;
import java.util.HashMap;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.content.Intent;
import android.os.UserHandle;
import android.util.Log;
import android.support.v7.preference.PreferenceGroup;
import java.util.Objects;
import android.os.storage.VolumeRecord;
import com.google.android.collect.Lists;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.os.storage.StorageEventListener;
import android.os.storage.VolumeInfo;
import com.android.settingslib.deviceinfo.StorageMeasurement;
import android.support.v7.preference.PreferenceCategory;
import java.util.List;
import android.support.v7.preference.Preference;
import android.content.pm.UserInfo;
import com.android.settings.SettingsPreferenceFragment;

public class PrivateVolumeSettings extends SettingsPreferenceFragment
{
    private static final int[] ITEMS_NO_SHOW_SHARED;
    private static final int[] ITEMS_SHOW_SHARED;
    private UserInfo mCurrentUser;
    private Preference mExplore;
    private int mHeaderPoolIndex;
    private List<PreferenceCategory> mHeaderPreferencePool;
    private int mItemPoolIndex;
    private List<StorageItemPreference> mItemPreferencePool;
    private StorageMeasurement mMeasure;
    private boolean mNeedsUpdate;
    private final StorageMeasurement.MeasurementReceiver mReceiver;
    private VolumeInfo mSharedVolume;
    private final StorageEventListener mStorageListener;
    private StorageManager mStorageManager;
    private StorageSummaryPreference mSummary;
    private long mSystemSize;
    private long mTotalSize;
    private UserManager mUserManager;
    private VolumeInfo mVolume;
    private String mVolumeId;
    
    static {
        ITEMS_NO_SHOW_SHARED = new int[] { 2131889254, 2131889263 };
        ITEMS_SHOW_SHARED = new int[] { 2131889254, 2131889261, 2131889264, 2131889255, 2131889263, 2131889262 };
    }
    
    public PrivateVolumeSettings() {
        this.mItemPreferencePool = (List<StorageItemPreference>)Lists.newArrayList();
        this.mHeaderPreferencePool = (List<PreferenceCategory>)Lists.newArrayList();
        this.mReceiver = new StorageMeasurement.MeasurementReceiver() {
            @Override
            public void onDetailsChanged(final MeasurementDetails measurementDetails) {
                PrivateVolumeSettings.this.updateDetails(measurementDetails);
            }
        };
        this.mStorageListener = new StorageEventListener() {
            public void onVolumeRecordChanged(final VolumeRecord volumeRecord) {
                if (Objects.equals(PrivateVolumeSettings.this.mVolume.getFsUuid(), volumeRecord.getFsUuid())) {
                    PrivateVolumeSettings.this.mVolume = PrivateVolumeSettings.this.mStorageManager.findVolumeById(PrivateVolumeSettings.this.mVolumeId);
                    PrivateVolumeSettings.this.update();
                }
            }
            
            public void onVolumeStateChanged(final VolumeInfo volumeInfo, final int n, final int n2) {
                if (Objects.equals(PrivateVolumeSettings.this.mVolume.getId(), volumeInfo.getId())) {
                    PrivateVolumeSettings.this.mVolume = volumeInfo;
                    PrivateVolumeSettings.this.update();
                }
            }
        };
        this.setRetainInstance(true);
    }
    
    private PreferenceCategory addCategory(final PreferenceGroup preferenceGroup, final CharSequence title) {
        PreferenceCategory preferenceCategory;
        if (this.mHeaderPoolIndex < this.mHeaderPreferencePool.size()) {
            preferenceCategory = this.mHeaderPreferencePool.get(this.mHeaderPoolIndex);
        }
        else {
            preferenceCategory = new PreferenceCategory(this.getPrefContext());
            this.mHeaderPreferencePool.add(preferenceCategory);
        }
        preferenceCategory.setTitle(title);
        preferenceCategory.removeAll();
        this.addPreference(preferenceGroup, preferenceCategory);
        ++this.mHeaderPoolIndex;
        return preferenceCategory;
    }
    
    private void addDetailItems(final PreferenceGroup preferenceGroup, final boolean b, final int n) {
        int[] array;
        if (b) {
            array = PrivateVolumeSettings.ITEMS_SHOW_SHARED;
        }
        else {
            array = PrivateVolumeSettings.ITEMS_NO_SHOW_SHARED;
        }
        for (int i = 0; i < array.length; ++i) {
            this.addItem(preferenceGroup, array[i], null, n);
        }
    }
    
    private void addItem(final PreferenceGroup preferenceGroup, final int title, final CharSequence title2, final int userHandle) {
        if (title == 2131889263) {
            if (this.mSystemSize <= 0L) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping System storage because its size is ");
                sb.append(this.mSystemSize);
                Log.w("PrivateVolumeSettings", sb.toString());
                return;
            }
            if (userHandle != UserHandle.myUserId()) {
                return;
            }
        }
        StorageItemPreference buildItem;
        if (this.mItemPoolIndex < this.mItemPreferencePool.size()) {
            buildItem = this.mItemPreferencePool.get(this.mItemPoolIndex);
        }
        else {
            buildItem = this.buildItem();
            this.mItemPreferencePool.add(buildItem);
        }
        if (title2 != null) {
            buildItem.setTitle(title2);
            buildItem.setKey(title2.toString());
        }
        else {
            buildItem.setTitle(title);
            buildItem.setKey(Integer.toString(title));
        }
        buildItem.setSummary(2131888267);
        buildItem.userHandle = userHandle;
        this.addPreference(preferenceGroup, buildItem);
        ++this.mItemPoolIndex;
    }
    
    private void addPreference(final PreferenceGroup preferenceGroup, final Preference preference) {
        preference.setOrder(Integer.MAX_VALUE);
        preferenceGroup.addPreference(preference);
    }
    
    private Preference buildAction(final int title) {
        final Preference preference = new Preference(this.getPrefContext());
        preference.setTitle(title);
        preference.setKey(Integer.toString(title));
        return preference;
    }
    
    private StorageItemPreference buildItem() {
        final StorageItemPreference storageItemPreference = new StorageItemPreference(this.getPrefContext());
        storageItemPreference.setIcon(2131230910);
        return storageItemPreference;
    }
    
    private Intent getIntentForStorage(final String s, final String s2) {
        final Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(DocumentsContract.buildRootUri(s, s2), "vnd.android.document/root");
        intent.addCategory("android.intent.category.DEFAULT");
        return intent;
    }
    
    private boolean isVolumeValid() {
        final VolumeInfo mVolume = this.mVolume;
        boolean b = true;
        if (mVolume == null || this.mVolume.getType() != 1 || !this.mVolume.isMountedReadable()) {
            b = false;
        }
        return b;
    }
    
    private void setTitle() {
        this.getActivity().setTitle((CharSequence)this.mStorageManager.getBestVolumeDescription(this.mVolume));
    }
    
    static void setVolumeSize(final Bundle bundle, final long n) {
        bundle.putLong("volume_size", n);
    }
    
    private static long totalValues(final StorageMeasurement.MeasurementDetails measurementDetails, int n, final String... array) {
        long n2 = 0L;
        final HashMap hashMap = (HashMap)measurementDetails.mediaSize.get(n);
        long n3;
        if (hashMap != null) {
            final int length = array.length;
            n = 0;
            while (true) {
                n3 = n2;
                if (n >= length) {
                    break;
                }
                final String s = array[n];
                long n4 = n2;
                if (hashMap.containsKey(s)) {
                    n4 = n2 + hashMap.get(s);
                }
                ++n;
                n2 = n4;
            }
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("MeasurementDetails mediaSize array does not have key for user ");
            sb.append(n);
            Log.w("PrivateVolumeSettings", sb.toString());
            n3 = n2;
        }
        return n3;
    }
    
    private void update() {
        if (!this.isVolumeValid()) {
            this.getActivity().finish();
            return;
        }
        this.setTitle();
        this.getFragmentManager().invalidateOptionsMenu();
        final Activity activity = this.getActivity();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        this.addPreference(preferenceScreen, this.mSummary);
        final List users = this.mUserManager.getUsers();
        final int size = users.size();
        final boolean b = size > 1;
        final boolean b2 = this.mSharedVolume != null && this.mSharedVolume.isMountedReadable();
        this.mItemPoolIndex = 0;
        this.mHeaderPoolIndex = 0;
        int n = 0;
        int n2;
        for (int i = 0; i < size; ++i, n = n2) {
            final UserInfo userInfo = users.get(i);
            n2 = n;
            if (Utils.isProfileOf(this.mCurrentUser, userInfo)) {
                PreferenceGroup addCategory;
                if (b) {
                    addCategory = this.addCategory(preferenceScreen, userInfo.name);
                }
                else {
                    addCategory = preferenceScreen;
                }
                this.addDetailItems(addCategory, b2, userInfo.id);
                n2 = n + 1;
            }
        }
        if (size - n > 0) {
            final PreferenceCategory addCategory2 = this.addCategory(preferenceScreen, this.getText(2131889302));
            for (int j = 0; j < size; ++j) {
                final UserInfo userInfo2 = users.get(j);
                if (!Utils.isProfileOf(this.mCurrentUser, userInfo2)) {
                    this.addItem(addCategory2, 0, userInfo2.name, userInfo2.id);
                }
            }
        }
        this.addItem(preferenceScreen, 2131889256, null, -10000);
        if (b2) {
            this.addPreference(preferenceScreen, this.mExplore);
        }
        final long n3 = this.mTotalSize - this.mVolume.getPath().getFreeSpace();
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(this.getResources(), n3, 0);
        this.mSummary.setTitle(TextUtils.expandTemplate(this.getText(2131889309), new CharSequence[] { formatBytes.value, formatBytes.units }));
        this.mSummary.setSummary(this.getString(2131889323, new Object[] { Formatter.formatFileSize((Context)activity, this.mTotalSize) }));
        this.mSummary.setPercent(n3, this.mTotalSize);
        this.mMeasure.forceMeasure();
        this.mNeedsUpdate = false;
    }
    
    private void updateDetails(final StorageMeasurement.MeasurementDetails measurementDetails) {
        int n = 0;
        long n2 = 0L;
        long n3 = 0L;
        long n4 = 0L;
        StorageItemPreference storageItemPreference = null;
        for (int i = 0; i < this.mItemPoolIndex; ++i) {
            final StorageItemPreference storageItemPreference2 = this.mItemPreferencePool.get(i);
            final int userHandle = storageItemPreference2.userHandle;
            int int1;
            try {
                int1 = Integer.parseInt(storageItemPreference2.getKey());
            }
            catch (NumberFormatException ex) {
                int1 = n;
            }
            if (int1 != 0) {
                Label_0322: {
                    switch (int1) {
                        default: {
                            switch (int1) {
                                case 2131889264: {
                                    final String[] array = { null };
                                    array[n] = Environment.DIRECTORY_MOVIES;
                                    final long totalValues = totalValues(measurementDetails, userHandle, array);
                                    this.updatePreference(storageItemPreference2, totalValues);
                                    n4 += totalValues;
                                    break;
                                }
                                case 2131889263: {
                                    this.updatePreference(storageItemPreference2, this.mSystemSize);
                                    n4 += this.mSystemSize;
                                    break;
                                }
                                case 2131889262: {
                                    final String[] array2 = { null };
                                    array2[n] = Environment.DIRECTORY_DOWNLOADS;
                                    final long totalValues2 = totalValues(measurementDetails, userHandle, array2);
                                    final long value = measurementDetails.miscSize.get(userHandle);
                                    n2 += totalValues2;
                                    n3 += value;
                                    n4 += value + totalValues2;
                                    storageItemPreference = storageItemPreference2;
                                    break Label_0322;
                                }
                                case 2131889261: {
                                    final long totalValues3 = totalValues(measurementDetails, userHandle, Environment.DIRECTORY_DCIM, Environment.DIRECTORY_PICTURES);
                                    this.updatePreference(storageItemPreference2, totalValues3);
                                    n4 += totalValues3;
                                    break Label_0322;
                                }
                            }
                            continue;
                        }
                        case 2131889256: {
                            this.updatePreference(storageItemPreference2, measurementDetails.cacheSize);
                            n4 += measurementDetails.cacheSize;
                            break;
                        }
                        case 2131889255: {
                            final String directory_MUSIC = Environment.DIRECTORY_MUSIC;
                            n = 0;
                            final long totalValues4 = totalValues(measurementDetails, userHandle, directory_MUSIC, Environment.DIRECTORY_ALARMS, Environment.DIRECTORY_NOTIFICATIONS, Environment.DIRECTORY_RINGTONES, Environment.DIRECTORY_PODCASTS);
                            this.updatePreference(storageItemPreference2, totalValues4);
                            n4 += totalValues4;
                            continue;
                        }
                        case 2131889254: {
                            this.updatePreference(storageItemPreference2, measurementDetails.appsSize.get(userHandle));
                            n4 += measurementDetails.appsSize.get(userHandle);
                            continue;
                        }
                    }
                }
                n = 0;
            }
            else {
                final long value2 = measurementDetails.usersSize.get(userHandle);
                this.updatePreference(storageItemPreference2, value2);
                n4 += value2;
            }
        }
        if (storageItemPreference != null) {
            final long n5 = this.mTotalSize - measurementDetails.availSize;
            final long n6 = n5 - n4;
            final StringBuilder sb = new StringBuilder();
            sb.append("Other items: \n\tmTotalSize: ");
            sb.append(this.mTotalSize);
            sb.append(" availSize: ");
            sb.append(measurementDetails.availSize);
            sb.append(" usedSize: ");
            sb.append(n5);
            sb.append("\n\taccountedSize: ");
            sb.append(n4);
            sb.append(" unaccountedSize size: ");
            sb.append(n6);
            sb.append("\n\ttotalMiscSize: ");
            sb.append(n3);
            sb.append(" totalDownloadsSize: ");
            sb.append(n2);
            sb.append("\n\tdetails: ");
            sb.append(measurementDetails);
            Log.v("PrivateVolumeSettings", sb.toString());
            this.updatePreference(storageItemPreference, n3 + n2 + n6);
        }
    }
    
    private void updatePreference(final StorageItemPreference storageItemPreference, final long n) {
        storageItemPreference.setStorageSize(n, this.mTotalSize);
    }
    
    @Override
    public int getMetricsCategory() {
        return 42;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mUserManager = (UserManager)((Context)activity).getSystemService((Class)UserManager.class);
        this.mStorageManager = (StorageManager)((Context)activity).getSystemService((Class)StorageManager.class);
        this.mVolumeId = this.getArguments().getString("android.os.storage.extra.VOLUME_ID");
        this.mVolume = this.mStorageManager.findVolumeById(this.mVolumeId);
        final long totalSpace = this.mVolume.getPath().getTotalSpace();
        this.mTotalSize = this.getArguments().getLong("volume_size", 0L);
        this.mSystemSize = this.mTotalSize - totalSpace;
        if (this.mTotalSize <= 0L) {
            this.mTotalSize = totalSpace;
            this.mSystemSize = 0L;
        }
        this.mSharedVolume = this.mStorageManager.findEmulatedForPrivate(this.mVolume);
        (this.mMeasure = new StorageMeasurement((Context)activity, this.mVolume, this.mSharedVolume)).setReceiver(this.mReceiver);
        if (!this.isVolumeValid()) {
            this.getActivity().finish();
            return;
        }
        this.addPreferencesFromResource(2132082761);
        this.getPreferenceScreen().setOrderingAsAdded(true);
        this.mSummary = new StorageSummaryPreference(this.getPrefContext());
        this.mCurrentUser = this.mUserManager.getUserInfo(UserHandle.myUserId());
        this.mExplore = this.buildAction(2131889285);
        this.setHasOptionsMenu(this.mNeedsUpdate = true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(2131623941, menu);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mMeasure != null) {
            this.mMeasure.onDestroy();
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final Activity activity = this.getActivity();
        final Bundle bundle = new Bundle();
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2131362654: {
                bundle.putString("android.os.storage.extra.VOLUME_ID", this.mVolume.getId());
                new SubSettingLauncher((Context)activity).setDestination(PrivateVolumeUnmount.class.getCanonicalName()).setTitle(2131889296).setSourceMetricsCategory(this.getMetricsCategory()).setArguments(bundle).launch();
                return true;
            }
            case 2131362653: {
                RenameFragment.show(this, this.mVolume);
                return true;
            }
            case 2131362651: {
                new StorageSettings.MountTask((Context)activity, this.mVolume).execute((Object[])new Void[0]);
                return true;
            }
            case 2131362650: {
                final Intent intent = new Intent((Context)activity, (Class)StorageWizardMigrateConfirm.class);
                intent.putExtra("android.os.storage.extra.VOLUME_ID", this.mVolume.getId());
                this.startActivity(intent);
                return true;
            }
            case 2131362649: {
                this.startActivity(new Intent("android.os.storage.action.MANAGE_STORAGE"));
                return true;
            }
            case 2131362648: {
                bundle.putString("android.os.storage.extra.VOLUME_ID", this.mVolume.getId());
                new SubSettingLauncher((Context)activity).setDestination(PrivateVolumeFormat.class.getCanonicalName()).setTitle(2131889287).setSourceMetricsCategory(this.getMetricsCategory()).setArguments(bundle).launch();
                return true;
            }
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mStorageManager.unregisterListener(this.mStorageListener);
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        int userHandle;
        if (preference instanceof StorageItemPreference) {
            userHandle = ((StorageItemPreference)preference).userHandle;
        }
        else {
            userHandle = -1;
        }
        int int1;
        try {
            int1 = Integer.parseInt(preference.getKey());
        }
        catch (NumberFormatException ex) {
            int1 = 0;
        }
        Intent intent = null;
        if (int1 == 0) {
            UserInfoFragment.show(this, preference.getTitle(), preference.getSummary());
            return true;
        }
        Label_0307: {
            if (int1 != 2131889285) {
                switch (int1) {
                    default: {
                        switch (int1) {
                            default: {
                                break Label_0307;
                            }
                            case 2131889264: {
                                intent = this.getIntentForStorage("com.android.providers.media.documents", "videos_root");
                                break Label_0307;
                            }
                            case 2131889263: {
                                SystemInfoFragment.show(this);
                                return true;
                            }
                            case 2131889262: {
                                OtherInfoFragment.show(this, this.mStorageManager.getBestVolumeDescription(this.mVolume), this.mSharedVolume, userHandle);
                                return true;
                            }
                            case 2131889261: {
                                intent = this.getIntentForStorage("com.android.providers.media.documents", "images_root");
                                break Label_0307;
                            }
                        }
                        break;
                    }
                    case 2131889256: {
                        ConfirmClearCacheFragment.show(this);
                        return true;
                    }
                    case 2131889255: {
                        intent = this.getIntentForStorage("com.android.providers.media.documents", "audio_root");
                        break;
                    }
                    case 2131889254: {
                        final Bundle arguments = new Bundle();
                        arguments.putString("classname", Settings.StorageUseActivity.class.getName());
                        arguments.putString("volumeUuid", this.mVolume.getFsUuid());
                        arguments.putString("volumeName", this.mVolume.getDescription());
                        arguments.putInt("storageType", 2);
                        intent = new SubSettingLauncher((Context)this.getActivity()).setDestination(ManageApplications.class.getName()).setArguments(arguments).setTitle(2131886412).setSourceMetricsCategory(this.getMetricsCategory()).toIntent();
                        break;
                    }
                }
            }
            else {
                intent = this.mSharedVolume.buildBrowseIntent();
            }
        }
        if (intent != null) {
            intent.putExtra("android.intent.extra.USER_ID", userHandle);
            Utils.launchIntent(this, intent);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        if (!this.isVolumeValid()) {
            return;
        }
        final MenuItem item = menu.findItem(2131362653);
        final MenuItem item2 = menu.findItem(2131362651);
        final MenuItem item3 = menu.findItem(2131362654);
        final MenuItem item4 = menu.findItem(2131362648);
        final MenuItem item5 = menu.findItem(2131362650);
        final MenuItem item6 = menu.findItem(2131362649);
        final boolean equals = "private".equals(this.mVolume.getId());
        final boolean b = true;
        if (equals) {
            item.setVisible(false);
            item2.setVisible(false);
            item3.setVisible(false);
            item4.setVisible(false);
            item6.setVisible(this.getResources().getBoolean(2131034177));
        }
        else {
            item.setVisible(this.mVolume.getType() == 1);
            item2.setVisible(this.mVolume.getState() == 0);
            item3.setVisible(this.mVolume.isMountedReadable());
            item4.setVisible(true);
            item6.setVisible(false);
        }
        item4.setTitle(2131889289);
        final VolumeInfo primaryStorageCurrentVolume = this.getActivity().getPackageManager().getPrimaryStorageCurrentVolume();
        item5.setVisible(primaryStorageCurrentVolume != null && primaryStorageCurrentVolume.getType() == 1 && !Objects.equals(this.mVolume, primaryStorageCurrentVolume) && b);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mVolume = this.mStorageManager.findVolumeById(this.mVolumeId);
        if (!this.isVolumeValid()) {
            this.getActivity().finish();
            return;
        }
        this.mStorageManager.registerListener(this.mStorageListener);
        if (this.mNeedsUpdate) {
            this.update();
        }
        else {
            this.setTitle();
        }
    }
    
    private static class ClearCacheObserver extends IPackageDataObserver$Stub
    {
        private int mRemaining;
        private final PrivateVolumeSettings mTarget;
        
        public ClearCacheObserver(final PrivateVolumeSettings mTarget, final int mRemaining) {
            this.mTarget = mTarget;
            this.mRemaining = mRemaining;
        }
        
        public void onRemoveCompleted(final String s, final boolean b) {
            synchronized (this) {
                final int mRemaining = this.mRemaining - 1;
                this.mRemaining = mRemaining;
                if (mRemaining == 0) {
                    this.mTarget.getActivity().runOnUiThread((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            ClearCacheObserver.this.mTarget.update();
                        }
                    });
                }
            }
        }
    }
    
    public static class ConfirmClearCacheFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment) {
            if (!fragment.isAdded()) {
                return;
            }
            final ConfirmClearCacheFragment confirmClearCacheFragment = new ConfirmClearCacheFragment();
            confirmClearCacheFragment.setTargetFragment(fragment, 0);
            confirmClearCacheFragment.show(fragment.getFragmentManager(), "confirmClearCache");
        }
        
        @Override
        public int getMetricsCategory() {
            return 564;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            alertDialog$Builder.setTitle(2131888269);
            alertDialog$Builder.setMessage((CharSequence)this.getString(2131888268));
            alertDialog$Builder.setPositiveButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, int i) {
                    final PrivateVolumeSettings privateVolumeSettings = (PrivateVolumeSettings)ConfirmClearCacheFragment.this.getTargetFragment();
                    final PackageManager packageManager = ((Context)activity).getPackageManager();
                    final int[] profileIdsWithDisabled = ((UserManager)((Context)activity).getSystemService((Class)UserManager.class)).getProfileIdsWithDisabled(((Context)activity).getUserId());
                    int length;
                    int n;
                    List installedPackagesAsUser;
                    ClearCacheObserver clearCacheObserver;
                    Iterator<PackageInfo> iterator;
                    for (length = profileIdsWithDisabled.length, i = 0; i < length; ++i) {
                        n = profileIdsWithDisabled[i];
                        installedPackagesAsUser = packageManager.getInstalledPackagesAsUser(0, n);
                        clearCacheObserver = new ClearCacheObserver(privateVolumeSettings, installedPackagesAsUser.size());
                        iterator = installedPackagesAsUser.iterator();
                        while (iterator.hasNext()) {
                            packageManager.deleteApplicationCacheFilesAsUser(iterator.next().packageName, n, (IPackageDataObserver)clearCacheObserver);
                        }
                    }
                }
            });
            alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
    
    public static class OtherInfoFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment, final String s, final VolumeInfo volumeInfo, final int n) {
            if (!fragment.isAdded()) {
                return;
            }
            final OtherInfoFragment otherInfoFragment = new OtherInfoFragment();
            otherInfoFragment.setTargetFragment(fragment, 0);
            final Bundle arguments = new Bundle();
            arguments.putString("android.intent.extra.TITLE", s);
            final Intent buildBrowseIntent = volumeInfo.buildBrowseIntent();
            buildBrowseIntent.putExtra("android.intent.extra.USER_ID", n);
            arguments.putParcelable("android.intent.extra.INTENT", (Parcelable)buildBrowseIntent);
            otherInfoFragment.setArguments(arguments);
            otherInfoFragment.show(fragment.getFragmentManager(), "otherInfo");
        }
        
        @Override
        public int getMetricsCategory() {
            return 566;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final String string = this.getArguments().getString("android.intent.extra.TITLE");
            final Intent intent = (Intent)this.getArguments().getParcelable("android.intent.extra.INTENT");
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            alertDialog$Builder.setMessage(TextUtils.expandTemplate(this.getText(2131889257), new CharSequence[] { string }));
            alertDialog$Builder.setPositiveButton(2131889285, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    Utils.launchIntent((Fragment)OtherInfoFragment.this, intent);
                }
            });
            alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
    
    public static class RenameFragment extends InstrumentedDialogFragment
    {
        public static void show(final PrivateVolumeSettings privateVolumeSettings, final VolumeInfo volumeInfo) {
            if (!privateVolumeSettings.isAdded()) {
                return;
            }
            final RenameFragment renameFragment = new RenameFragment();
            renameFragment.setTargetFragment((Fragment)privateVolumeSettings, 0);
            final Bundle arguments = new Bundle();
            arguments.putString("android.os.storage.extra.FS_UUID", volumeInfo.getFsUuid());
            renameFragment.setArguments(arguments);
            renameFragment.show(privateVolumeSettings.getFragmentManager(), "rename");
        }
        
        @Override
        public int getMetricsCategory() {
            return 563;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final StorageManager storageManager = (StorageManager)((Context)activity).getSystemService((Class)StorageManager.class);
            final String string = this.getArguments().getString("android.os.storage.extra.FS_UUID");
            storageManager.findVolumeByUuid(string);
            final VolumeRecord recordByUuid = storageManager.findRecordByUuid(string);
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            final View inflate = LayoutInflater.from(alertDialog$Builder.getContext()).inflate(2131558540, (ViewGroup)null, false);
            final EditText editText = (EditText)inflate.findViewById(2131362098);
            editText.setText((CharSequence)recordByUuid.getNickname());
            alertDialog$Builder.setTitle(2131889305);
            alertDialog$Builder.setView(inflate);
            alertDialog$Builder.setPositiveButton(2131888889, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    storageManager.setVolumeNickname(string, editText.getText().toString());
                }
            });
            alertDialog$Builder.setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
    
    public static class SystemInfoFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment) {
            if (!fragment.isAdded()) {
                return;
            }
            final SystemInfoFragment systemInfoFragment = new SystemInfoFragment();
            systemInfoFragment.setTargetFragment(fragment, 0);
            systemInfoFragment.show(fragment.getFragmentManager(), "systemInfo");
        }
        
        @Override
        public int getMetricsCategory() {
            return 565;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage((CharSequence)this.getContext().getString(2131889258, new Object[] { Build.VERSION.RELEASE })).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
        }
    }
    
    public static class UserInfoFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment, final CharSequence charSequence, final CharSequence charSequence2) {
            if (!fragment.isAdded()) {
                return;
            }
            final UserInfoFragment userInfoFragment = new UserInfoFragment();
            userInfoFragment.setTargetFragment(fragment, 0);
            final Bundle arguments = new Bundle();
            arguments.putCharSequence("android.intent.extra.TITLE", charSequence);
            arguments.putCharSequence("android.intent.extra.SUBJECT", charSequence2);
            userInfoFragment.setArguments(arguments);
            userInfoFragment.show(fragment.getFragmentManager(), "userInfo");
        }
        
        @Override
        public int getMetricsCategory() {
            return 567;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final CharSequence charSequence = this.getArguments().getCharSequence("android.intent.extra.TITLE");
            final CharSequence charSequence2 = this.getArguments().getCharSequence("android.intent.extra.SUBJECT");
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            alertDialog$Builder.setMessage(TextUtils.expandTemplate(this.getText(2131889259), new CharSequence[] { charSequence, charSequence2 }));
            alertDialog$Builder.setPositiveButton(17039370, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
}
