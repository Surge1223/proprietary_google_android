package com.android.settings.deviceinfo;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import java.text.NumberFormat;
import android.util.Log;
import android.widget.Toast;
import android.os.AsyncTask;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.text.format.Formatter$BytesResult;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.os.storage.VolumeRecord;
import java.util.Comparator;
import java.util.Collections;
import com.android.settingslib.deviceinfo.StorageVolumeProvider;
import com.android.settingslib.deviceinfo.PrivateStorageInfo;
import com.android.settingslib.deviceinfo.StorageManagerVolumeProvider;
import android.support.v7.preference.Preference;
import android.app.Activity;
import android.content.Intent;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.os.storage.DiskInfo;
import java.util.Iterator;
import android.os.storage.VolumeInfo;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.graphics.Color;
import android.os.storage.StorageManager;
import android.os.storage.StorageEventListener;
import android.support.v7.preference.PreferenceCategory;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public class StorageSettings extends SettingsPreferenceFragment implements Indexable
{
    static final int[] COLOR_PRIVATE;
    static final int COLOR_PUBLIC;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private static long sTotalInternalStorage;
    private PreferenceCategory mExternalCategory;
    private boolean mHasLaunchedPrivateVolumeSettings;
    private PreferenceCategory mInternalCategory;
    private StorageSummaryPreference mInternalSummary;
    private final StorageEventListener mStorageListener;
    private StorageManager mStorageManager;
    
    static {
        COLOR_PUBLIC = Color.parseColor("#ff9e9e9e");
        COLOR_PRIVATE = new int[] { Color.parseColor("#ff26a69a"), Color.parseColor("#ffab47bc"), Color.parseColor("#fff2a600"), Color.parseColor("#ffec407a"), Color.parseColor("#ffc0ca33") };
        SUMMARY_PROVIDER_FACTORY = (SummaryLoader.SummaryProviderFactory)_$$Lambda$StorageSettings$pjFUgWj2HWW95DLVydfI8EgfTdg.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.title = context.getString(2131889306);
                searchIndexableRaw.key = "storage_settings";
                searchIndexableRaw.screenTitle = context.getString(2131889306);
                searchIndexableRaw.keywords = context.getString(2131887992);
                list.add(searchIndexableRaw);
                final SearchIndexableRaw searchIndexableRaw2 = new SearchIndexableRaw(context);
                searchIndexableRaw2.title = context.getString(2131887909);
                searchIndexableRaw2.key = "storage_settings_internal_storage";
                searchIndexableRaw2.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw2);
                final SearchIndexableRaw searchIndexableRaw3 = new SearchIndexableRaw(context);
                final StorageManager storageManager = (StorageManager)context.getSystemService((Class)StorageManager.class);
                for (final VolumeInfo volumeInfo : storageManager.getVolumes()) {
                    if (isInteresting(volumeInfo)) {
                        searchIndexableRaw3.title = storageManager.getBestVolumeDescription(volumeInfo);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("storage_settings_volume_");
                        sb.append(volumeInfo.id);
                        searchIndexableRaw3.key = sb.toString();
                        searchIndexableRaw3.screenTitle = context.getString(2131889306);
                        list.add(searchIndexableRaw3);
                    }
                }
                final SearchIndexableRaw searchIndexableRaw4 = new SearchIndexableRaw(context);
                searchIndexableRaw4.title = context.getString(2131888283);
                searchIndexableRaw4.key = "storage_settings_memory_size";
                searchIndexableRaw4.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw4);
                final SearchIndexableRaw searchIndexableRaw5 = new SearchIndexableRaw(context);
                searchIndexableRaw5.title = context.getString(2131888263);
                searchIndexableRaw5.key = "storage_settings_memory_available";
                searchIndexableRaw5.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw5);
                final SearchIndexableRaw searchIndexableRaw6 = new SearchIndexableRaw(context);
                searchIndexableRaw6.title = context.getString(2131888262);
                searchIndexableRaw6.key = "storage_settings_apps_space";
                searchIndexableRaw6.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw6);
                final SearchIndexableRaw searchIndexableRaw7 = new SearchIndexableRaw(context);
                searchIndexableRaw7.title = context.getString(2131888270);
                searchIndexableRaw7.key = "storage_settings_dcim_space";
                searchIndexableRaw7.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw7);
                final SearchIndexableRaw searchIndexableRaw8 = new SearchIndexableRaw(context);
                searchIndexableRaw8.title = context.getString(2131888280);
                searchIndexableRaw8.key = "storage_settings_music_space";
                searchIndexableRaw8.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw8);
                final SearchIndexableRaw searchIndexableRaw9 = new SearchIndexableRaw(context);
                searchIndexableRaw9.title = context.getString(2131888278);
                searchIndexableRaw9.key = "storage_settings_misc_space";
                searchIndexableRaw9.screenTitle = context.getString(2131889306);
                list.add(searchIndexableRaw9);
                final SearchIndexableRaw searchIndexableRaw10 = new SearchIndexableRaw(context);
                searchIndexableRaw10.title = context.getString(2131889290);
                searchIndexableRaw10.key = "storage_settings_free_space";
                searchIndexableRaw10.screenTitle = context.getString(2131889290);
                searchIndexableRaw10.intentAction = "android.os.storage.action.MANAGE_STORAGE";
                searchIndexableRaw10.intentTargetPackage = context.getString(2131887096);
                searchIndexableRaw10.intentTargetClass = context.getString(2131887095);
                list.add(searchIndexableRaw10);
                return list;
            }
        };
    }
    
    public StorageSettings() {
        this.mHasLaunchedPrivateVolumeSettings = false;
        this.mStorageListener = new StorageEventListener() {
            public void onDiskDestroyed(final DiskInfo diskInfo) {
                StorageSettings.this.refresh();
            }
            
            public void onVolumeStateChanged(final VolumeInfo volumeInfo, final int n, final int n2) {
                if (isInteresting(volumeInfo)) {
                    StorageSettings.this.refresh();
                }
            }
        };
    }
    
    static boolean handlePublicVolumeClick(final Context context, final VolumeInfo volumeInfo) {
        final Intent buildBrowseIntent = volumeInfo.buildBrowseIntent();
        if (volumeInfo.isMountedReadable() && buildBrowseIntent != null) {
            context.startActivity(buildBrowseIntent);
            return true;
        }
        final Bundle arguments = new Bundle();
        arguments.putString("android.os.storage.extra.VOLUME_ID", volumeInfo.getId());
        new SubSettingLauncher(context).setDestination(PublicVolumeSettings.class.getCanonicalName()).setTitle(-1).setSourceMetricsCategory(42).setArguments(arguments).launch();
        return true;
    }
    
    private static boolean isInteresting(final VolumeInfo volumeInfo) {
        switch (volumeInfo.getType()) {
            default: {
                return false;
            }
            case 0:
            case 1: {
                return true;
            }
        }
    }
    
    private void refresh() {
        synchronized (this) {
            final Context prefContext = this.getPrefContext();
            this.getPreferenceScreen().removeAll();
            this.mInternalCategory.removeAll();
            this.mExternalCategory.removeAll();
            this.mInternalCategory.addPreference(this.mInternalSummary);
            final PrivateStorageInfo privateStorageInfo = PrivateStorageInfo.getPrivateStorageInfo(new StorageManagerVolumeProvider(this.mStorageManager));
            final long totalBytes = privateStorageInfo.totalBytes;
            final long totalBytes2 = privateStorageInfo.totalBytes;
            final long freeBytes = privateStorageInfo.freeBytes;
            final List volumes = this.mStorageManager.getVolumes();
            Collections.sort((List<Object>)volumes, VolumeInfo.getDescriptionComparator());
            final Iterator<VolumeInfo> iterator = volumes.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final VolumeInfo volumeInfo = iterator.next();
                if (volumeInfo.getType() == 1) {
                    this.mInternalCategory.addPreference(new StorageVolumePreference(prefContext, volumeInfo, StorageSettings.COLOR_PRIVATE[n % StorageSettings.COLOR_PRIVATE.length], PrivateStorageInfo.getTotalSize(volumeInfo, StorageSettings.sTotalInternalStorage)));
                    ++n;
                }
                else {
                    if (volumeInfo.getType() != 0) {
                        continue;
                    }
                    this.mExternalCategory.addPreference(new StorageVolumePreference(prefContext, volumeInfo, StorageSettings.COLOR_PUBLIC, 0L));
                }
            }
            for (final VolumeRecord volumeRecord : this.mStorageManager.getVolumeRecords()) {
                if (volumeRecord.getType() == 1 && this.mStorageManager.findVolumeByUuid(volumeRecord.getFsUuid()) == null) {
                    final Drawable drawable = prefContext.getDrawable(2131231154);
                    drawable.mutate();
                    drawable.setTint(StorageSettings.COLOR_PUBLIC);
                    final Preference preference = new Preference(prefContext);
                    preference.setKey(volumeRecord.getFsUuid());
                    preference.setTitle(volumeRecord.getNickname());
                    preference.setSummary(17039870);
                    preference.setIcon(drawable);
                    this.mInternalCategory.addPreference(preference);
                }
            }
            for (final DiskInfo diskInfo : this.mStorageManager.getDisks()) {
                if (diskInfo.volumeCount == 0 && diskInfo.size > 0L) {
                    final Preference preference2 = new Preference(prefContext);
                    preference2.setKey(diskInfo.getId());
                    preference2.setTitle(diskInfo.getDescription());
                    preference2.setSummary(17039876);
                    preference2.setIcon(2131231154);
                    this.mExternalCategory.addPreference(preference2);
                }
            }
            final Formatter$BytesResult formatBytes = Formatter.formatBytes(this.getResources(), totalBytes2 - freeBytes, 0);
            this.mInternalSummary.setTitle(TextUtils.expandTemplate(this.getText(2131889309), new CharSequence[] { formatBytes.value, formatBytes.units }));
            this.mInternalSummary.setSummary(this.getString(2131889324, new Object[] { Formatter.formatFileSize(prefContext, totalBytes) }));
            if (this.mInternalCategory.getPreferenceCount() > 0) {
                this.getPreferenceScreen().addPreference(this.mInternalCategory);
            }
            if (this.mExternalCategory.getPreferenceCount() > 0) {
                this.getPreferenceScreen().addPreference(this.mExternalCategory);
            }
            if (this.mInternalCategory.getPreferenceCount() == 2 && this.mExternalCategory.getPreferenceCount() == 0 && !this.mHasLaunchedPrivateVolumeSettings) {
                this.mHasLaunchedPrivateVolumeSettings = true;
                final Bundle arguments = new Bundle();
                arguments.putString("android.os.storage.extra.VOLUME_ID", "private");
                new SubSettingLauncher((Context)this.getActivity()).setDestination(StorageDashboardFragment.class.getName()).setArguments(arguments).setTitle(2131889306).setSourceMetricsCategory(this.getMetricsCategory()).launch();
                this.finish();
            }
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887787;
    }
    
    @Override
    public int getMetricsCategory() {
        return 42;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mStorageManager = (StorageManager)((Context)this.getActivity()).getSystemService((Class)StorageManager.class);
        if (StorageSettings.sTotalInternalStorage <= 0L) {
            StorageSettings.sTotalInternalStorage = this.mStorageManager.getPrimaryStorageSize();
        }
        this.addPreferencesFromResource(2132082760);
        this.mInternalCategory = (PreferenceCategory)this.findPreference("storage_internal");
        this.mExternalCategory = (PreferenceCategory)this.findPreference("storage_external");
        this.mInternalSummary = new StorageSummaryPreference(this.getPrefContext());
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mStorageManager.unregisterListener(this.mStorageListener);
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        final String key = preference.getKey();
        if (preference instanceof StorageVolumePreference) {
            final VolumeInfo volumeById = this.mStorageManager.findVolumeById(key);
            if (volumeById == null) {
                return false;
            }
            if (volumeById.getState() == 0) {
                VolumeUnmountedFragment.show(this, volumeById.getId());
                return true;
            }
            if (volumeById.getState() == 6) {
                DiskInitFragment.show(this, 2131889265, volumeById.getDiskId());
                return true;
            }
            if (volumeById.getType() == 1) {
                final Bundle bundle = new Bundle();
                bundle.putString("android.os.storage.extra.VOLUME_ID", volumeById.getId());
                if ("private".equals(volumeById.getId())) {
                    new SubSettingLauncher(this.getContext()).setDestination(StorageDashboardFragment.class.getCanonicalName()).setTitle(2131889306).setSourceMetricsCategory(this.getMetricsCategory()).setArguments(bundle).launch();
                }
                else {
                    PrivateVolumeSettings.setVolumeSize(bundle, PrivateStorageInfo.getTotalSize(volumeById, StorageSettings.sTotalInternalStorage));
                    new SubSettingLauncher(this.getContext()).setDestination(PrivateVolumeSettings.class.getCanonicalName()).setTitle(-1).setSourceMetricsCategory(this.getMetricsCategory()).setArguments(bundle).launch();
                }
                return true;
            }
            return volumeById.getType() == 0 && handlePublicVolumeClick(this.getContext(), volumeById);
        }
        else {
            if (key.startsWith("disk:")) {
                DiskInitFragment.show(this, 2131889267, key);
                return true;
            }
            final Bundle arguments = new Bundle();
            arguments.putString("android.os.storage.extra.FS_UUID", key);
            new SubSettingLauncher(this.getContext()).setDestination(PrivateVolumeForget.class.getCanonicalName()).setTitle(2131889286).setSourceMetricsCategory(this.getMetricsCategory()).setArguments(arguments).launch();
            return true;
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mStorageManager.registerListener(this.mStorageListener);
        this.refresh();
    }
    
    public static class DiskInitFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment, final int n, final String s) {
            final Bundle arguments = new Bundle();
            arguments.putInt("android.intent.extra.TEXT", n);
            arguments.putString("android.os.storage.extra.DISK_ID", s);
            final DiskInitFragment diskInitFragment = new DiskInitFragment();
            diskInitFragment.setArguments(arguments);
            diskInitFragment.setTargetFragment(fragment, 0);
            diskInitFragment.show(fragment.getFragmentManager(), "disk_init");
        }
        
        @Override
        public int getMetricsCategory() {
            return 561;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final StorageManager storageManager = (StorageManager)((Context)activity).getSystemService((Class)StorageManager.class);
            final int int1 = this.getArguments().getInt("android.intent.extra.TEXT");
            final String string = this.getArguments().getString("android.os.storage.extra.DISK_ID");
            final DiskInfo diskById = storageManager.findDiskById(string);
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            alertDialog$Builder.setMessage(TextUtils.expandTemplate(this.getText(int1), new CharSequence[] { diskById.getDescription() }));
            alertDialog$Builder.setPositiveButton(2131889295, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    final Intent intent = new Intent(activity, (Class)StorageWizardInit.class);
                    intent.putExtra("android.os.storage.extra.DISK_ID", string);
                    DiskInitFragment.this.startActivity(intent);
                }
            });
            alertDialog$Builder.setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
    
    public static class MountTask extends AsyncTask<Void, Void, Exception>
    {
        private final Context mContext;
        private final String mDescription;
        private final StorageManager mStorageManager;
        private final String mVolumeId;
        
        public MountTask(final Context context, final VolumeInfo volumeInfo) {
            this.mContext = context.getApplicationContext();
            this.mStorageManager = (StorageManager)this.mContext.getSystemService((Class)StorageManager.class);
            this.mVolumeId = volumeInfo.getId();
            this.mDescription = this.mStorageManager.getBestVolumeDescription(volumeInfo);
        }
        
        protected Exception doInBackground(final Void... array) {
            try {
                this.mStorageManager.mount(this.mVolumeId);
                return null;
            }
            catch (Exception ex) {
                return ex;
            }
        }
        
        protected void onPostExecute(final Exception ex) {
            if (ex == null) {
                Toast.makeText(this.mContext, (CharSequence)this.mContext.getString(2131889298, new Object[] { this.mDescription }), 0).show();
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to mount ");
                sb.append(this.mVolumeId);
                Log.e("StorageSettings", sb.toString(), (Throwable)ex);
                Toast.makeText(this.mContext, (CharSequence)this.mContext.getString(2131889297, new Object[] { this.mDescription }), 0).show();
            }
        }
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mLoader;
        private final StorageManagerVolumeProvider mStorageManagerVolumeProvider;
        
        private SummaryProvider(final Context mContext, final SummaryLoader mLoader) {
            this.mContext = mContext;
            this.mLoader = mLoader;
            this.mStorageManagerVolumeProvider = new StorageManagerVolumeProvider((StorageManager)this.mContext.getSystemService((Class)StorageManager.class));
        }
        
        private void updateSummary() {
            final NumberFormat percentInstance = NumberFormat.getPercentInstance();
            final PrivateStorageInfo privateStorageInfo = PrivateStorageInfo.getPrivateStorageInfo(this.mStorageManagerVolumeProvider);
            this.mLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getString(2131889311, new Object[] { percentInstance.format((privateStorageInfo.totalBytes - privateStorageInfo.freeBytes) / privateStorageInfo.totalBytes), Formatter.formatFileSize(this.mContext, privateStorageInfo.freeBytes) }));
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.updateSummary();
            }
        }
    }
    
    public static class UnmountTask extends AsyncTask<Void, Void, Exception>
    {
        private final Context mContext;
        private final String mDescription;
        private final StorageManager mStorageManager;
        private final String mVolumeId;
        
        public UnmountTask(final Context context, final VolumeInfo volumeInfo) {
            this.mContext = context.getApplicationContext();
            this.mStorageManager = (StorageManager)this.mContext.getSystemService((Class)StorageManager.class);
            this.mVolumeId = volumeInfo.getId();
            this.mDescription = this.mStorageManager.getBestVolumeDescription(volumeInfo);
        }
        
        protected Exception doInBackground(final Void... array) {
            try {
                this.mStorageManager.unmount(this.mVolumeId);
                return null;
            }
            catch (Exception ex) {
                return ex;
            }
        }
        
        protected void onPostExecute(final Exception ex) {
            if (ex == null) {
                Toast.makeText(this.mContext, (CharSequence)this.mContext.getString(2131889318, new Object[] { this.mDescription }), 0).show();
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to unmount ");
                sb.append(this.mVolumeId);
                Log.e("StorageSettings", sb.toString(), (Throwable)ex);
                Toast.makeText(this.mContext, (CharSequence)this.mContext.getString(2131889317, new Object[] { this.mDescription }), 0).show();
            }
        }
    }
    
    public static class VolumeUnmountedFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment, final String s) {
            final Bundle arguments = new Bundle();
            arguments.putString("android.os.storage.extra.VOLUME_ID", s);
            final VolumeUnmountedFragment volumeUnmountedFragment = new VolumeUnmountedFragment();
            volumeUnmountedFragment.setArguments(arguments);
            volumeUnmountedFragment.setTargetFragment(fragment, 0);
            volumeUnmountedFragment.show(fragment.getFragmentManager(), "volume_unmounted");
        }
        
        @Override
        public int getMetricsCategory() {
            return 562;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final VolumeInfo volumeById = ((StorageManager)((Context)activity).getSystemService((Class)StorageManager.class)).findVolumeById(this.getArguments().getString("android.os.storage.extra.VOLUME_ID"));
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            alertDialog$Builder.setMessage(TextUtils.expandTemplate(this.getText(2131889266), new CharSequence[] { volumeById.getDisk().getDescription() }));
            alertDialog$Builder.setPositiveButton(2131889293, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                private boolean wasAdminSupportIntentShown(final String s) {
                    final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced((Context)VolumeUnmountedFragment.this.getActivity(), s, UserHandle.myUserId());
                    final boolean hasBaseUserRestriction = RestrictedLockUtils.hasBaseUserRestriction((Context)VolumeUnmountedFragment.this.getActivity(), s, UserHandle.myUserId());
                    if (checkIfRestrictionEnforced != null && !hasBaseUserRestriction) {
                        RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)VolumeUnmountedFragment.this.getActivity(), checkIfRestrictionEnforced);
                        return true;
                    }
                    return false;
                }
                
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    if (this.wasAdminSupportIntentShown("no_physical_media")) {
                        return;
                    }
                    if (volumeById.disk != null && volumeById.disk.isUsb() && this.wasAdminSupportIntentShown("no_usb_file_transfer")) {
                        return;
                    }
                    new MountTask(activity, volumeById).execute((Object[])new Void[0]);
                }
            });
            alertDialog$Builder.setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
}
