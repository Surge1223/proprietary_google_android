package com.android.settings.deviceinfo;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.os.storage.StorageManager;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.os.storage.VolumeInfo;
import android.os.storage.DiskInfo;
import android.view.View.OnClickListener;
import com.android.settings.core.InstrumentedPreferenceFragment;

public class PrivateVolumeFormat extends InstrumentedPreferenceFragment
{
    private final View.OnClickListener mConfirmListener;
    private DiskInfo mDisk;
    private VolumeInfo mVolume;
    
    public PrivateVolumeFormat() {
        this.mConfirmListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Intent intent = new Intent((Context)PrivateVolumeFormat.this.getActivity(), (Class)StorageWizardFormatProgress.class);
                intent.putExtra("android.os.storage.extra.DISK_ID", PrivateVolumeFormat.this.mDisk.getId());
                intent.putExtra("format_private", false);
                intent.putExtra("format_forget_uuid", PrivateVolumeFormat.this.mVolume.getFsUuid());
                PrivateVolumeFormat.this.startActivity(intent);
                PrivateVolumeFormat.this.getActivity().finish();
            }
        };
    }
    
    @Override
    public int getMetricsCategory() {
        return 42;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final StorageManager storageManager = (StorageManager)this.getActivity().getSystemService((Class)StorageManager.class);
        this.mVolume = storageManager.findVolumeById(this.getArguments().getString("android.os.storage.extra.VOLUME_ID"));
        this.mDisk = storageManager.findDiskById(this.mVolume.getDiskId());
        final View inflate = layoutInflater.inflate(2131558761, viewGroup, false);
        final TextView textView = (TextView)inflate.findViewById(2131361913);
        final Button button = (Button)inflate.findViewById(2131362002);
        textView.setText(TextUtils.expandTemplate(this.getText(2131889276), new CharSequence[] { this.mDisk.getDescription() }));
        button.setOnClickListener(this.mConfirmListener);
        return inflate;
    }
}
