package com.android.settings.deviceinfo;

import android.util.MathUtils;
import android.graphics.Color;
import android.widget.TextView;
import android.widget.ProgressBar;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import android.support.v7.preference.Preference;

public class StorageSummaryPreference extends Preference
{
    private int mPercent;
    
    public StorageSummaryPreference(final Context context) {
        super(context);
        this.mPercent = -1;
        this.setLayoutResource(2131558764);
        this.setEnabled(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        final ProgressBar progressBar = (ProgressBar)preferenceViewHolder.findViewById(16908301);
        if (this.mPercent != -1) {
            progressBar.setVisibility(0);
            progressBar.setProgress(this.mPercent);
            progressBar.setScaleY(7.0f);
        }
        else {
            progressBar.setVisibility(8);
        }
        ((TextView)preferenceViewHolder.findViewById(16908304)).setTextColor(Color.parseColor("#8a000000"));
        super.onBindViewHolder(preferenceViewHolder);
    }
    
    public void setPercent(final long n, final long n2) {
        final int n3 = (int)(100L * n / n2);
        int n4;
        if (n > 0L) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        this.mPercent = MathUtils.constrain(n3, n4, 100);
    }
}
