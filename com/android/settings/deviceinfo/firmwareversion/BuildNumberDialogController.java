package com.android.settings.deviceinfo.firmwareversion;

import android.os.Build;
import android.text.BidiFormatter;

public class BuildNumberDialogController
{
    static final int BUILD_NUMBER_VALUE_ID = 2131361941;
    private final FirmwareVersionDialogFragment mDialog;
    
    public BuildNumberDialogController(final FirmwareVersionDialogFragment mDialog) {
        this.mDialog = mDialog;
    }
    
    public void initialize() {
        this.mDialog.setText(2131361941, BidiFormatter.getInstance().unicodeWrap(Build.DISPLAY));
    }
}
