package com.android.settings.deviceinfo.firmwareversion;

import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.os.Build.VERSION;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.app.Fragment;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class FirmwareVersionPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final Fragment mFragment;
    
    public FirmwareVersionPreferenceController(final Context context, final Fragment mFragment) {
        super(context);
        this.mFragment = mFragment;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        if (preference != null) {
            preference.setSummary(Build.VERSION.RELEASE);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "firmware_version";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return false;
        }
        FirmwareVersionDialogFragment.show(this.mFragment);
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
}
