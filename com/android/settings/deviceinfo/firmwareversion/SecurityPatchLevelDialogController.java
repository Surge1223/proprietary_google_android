package com.android.settings.deviceinfo.firmwareversion;

import android.util.Log;
import android.content.Intent;
import android.view.View;
import android.text.TextUtils;
import com.android.settingslib.DeviceInfoUtils;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;
import android.net.Uri;
import android.view.View.OnClickListener;

public class SecurityPatchLevelDialogController implements View.OnClickListener
{
    private static final Uri INTENT_URI_DATA;
    static final int SECURITY_PATCH_LABEL_ID = 2131362572;
    static final int SECURITY_PATCH_VALUE_ID = 2131362573;
    private final Context mContext;
    private final String mCurrentPatch;
    private final FirmwareVersionDialogFragment mDialog;
    private final PackageManagerWrapper mPackageManager;
    
    static {
        INTENT_URI_DATA = Uri.parse("https://source.android.com/security/bulletin/");
    }
    
    public SecurityPatchLevelDialogController(final FirmwareVersionDialogFragment mDialog) {
        this.mDialog = mDialog;
        this.mContext = mDialog.getContext();
        this.mPackageManager = new PackageManagerWrapper(this.mContext.getPackageManager());
        this.mCurrentPatch = DeviceInfoUtils.getSecurityPatch();
    }
    
    private void registerListeners() {
        this.mDialog.registerClickListener(2131362572, (View.OnClickListener)this);
        this.mDialog.registerClickListener(2131362573, (View.OnClickListener)this);
    }
    
    public void initialize() {
        if (TextUtils.isEmpty((CharSequence)this.mCurrentPatch)) {
            this.mDialog.removeSettingFromScreen(2131362572);
            this.mDialog.removeSettingFromScreen(2131362573);
            return;
        }
        this.registerListeners();
        this.mDialog.setText(2131362573, this.mCurrentPatch);
    }
    
    public void onClick(final View view) {
        final Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(SecurityPatchLevelDialogController.INTENT_URI_DATA);
        if (this.mPackageManager.queryIntentActivities(intent, 0).isEmpty()) {
            Log.w("SecurityPatchCtrl", "Stop click action on 2131362573: queryIntentActivities() returns empty");
            return;
        }
        this.mContext.startActivity(intent);
    }
}
