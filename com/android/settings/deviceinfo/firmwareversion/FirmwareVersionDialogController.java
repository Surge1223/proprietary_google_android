package com.android.settings.deviceinfo.firmwareversion;

import com.android.internal.app.PlatLogoActivity;
import android.content.Intent;
import android.util.Log;
import android.os.SystemClock;
import android.view.View;
import android.os.UserHandle;
import android.os.Build.VERSION;
import android.os.UserManager;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Context;
import android.view.View.OnClickListener;

public class FirmwareVersionDialogController implements View.OnClickListener
{
    static final int FIRMWARE_VERSION_LABEL_ID = 2131362159;
    static final int FIRMWARE_VERSION_VALUE_ID = 2131362160;
    private final Context mContext;
    private final FirmwareVersionDialogFragment mDialog;
    private RestrictedLockUtils.EnforcedAdmin mFunDisallowedAdmin;
    private boolean mFunDisallowedBySystem;
    private final long[] mHits;
    private final UserManager mUserManager;
    
    public FirmwareVersionDialogController(final FirmwareVersionDialogFragment mDialog) {
        this.mHits = new long[3];
        this.mDialog = mDialog;
        this.mContext = mDialog.getContext();
        this.mUserManager = (UserManager)this.mContext.getSystemService("user");
    }
    
    private void registerClickListeners() {
        this.mDialog.registerClickListener(2131362159, (View.OnClickListener)this);
        this.mDialog.registerClickListener(2131362160, (View.OnClickListener)this);
    }
    
    void arrayCopy() {
        System.arraycopy(this.mHits, 1, this.mHits, 0, this.mHits.length - 1);
    }
    
    public void initialize() {
        this.initializeAdminPermissions();
        this.registerClickListeners();
        this.mDialog.setText(2131362160, Build.VERSION.RELEASE);
    }
    
    void initializeAdminPermissions() {
        this.mFunDisallowedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_fun", UserHandle.myUserId());
        this.mFunDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_fun", UserHandle.myUserId());
    }
    
    public void onClick(View setClassName) {
        this.arrayCopy();
        this.mHits[this.mHits.length - 1] = SystemClock.uptimeMillis();
        if (this.mHits[0] >= SystemClock.uptimeMillis() - 500L) {
            if (this.mUserManager.hasUserRestriction("no_fun")) {
                if (this.mFunDisallowedAdmin != null && !this.mFunDisallowedBySystem) {
                    RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mFunDisallowedAdmin);
                }
                Log.d("firmwareDialogCtrl", "Sorry, no fun for you!");
                return;
            }
            setClassName = (View)new Intent("android.intent.action.MAIN").setClassName("android", PlatLogoActivity.class.getName());
            try {
                this.mContext.startActivity((Intent)setClassName);
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to start activity ");
                sb.append(((Intent)setClassName).toString());
                Log.e("firmwareDialogCtrl", sb.toString());
            }
        }
    }
}
