package com.android.settings.deviceinfo.firmwareversion;

import android.widget.TextView;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.Fragment;
import android.view.View;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class FirmwareVersionDialogFragment extends InstrumentedDialogFragment
{
    private View mRootView;
    
    private void initializeControllers() {
        new FirmwareVersionDialogController(this).initialize();
        new SecurityPatchLevelDialogController(this).initialize();
        new BasebandVersionDialogController(this).initialize();
        new KernelVersionDialogController(this).initialize();
        new BuildNumberDialogController(this).initialize();
    }
    
    public static void show(final Fragment fragment) {
        final FragmentManager childFragmentManager = fragment.getChildFragmentManager();
        if (childFragmentManager.findFragmentByTag("firmwareVersionDialog") == null) {
            new FirmwareVersionDialogFragment().show(childFragmentManager, "firmwareVersionDialog");
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 1247;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final AlertDialog$Builder setPositiveButton = new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887673).setPositiveButton(17039370, (DialogInterface$OnClickListener)null);
        this.mRootView = LayoutInflater.from((Context)this.getActivity()).inflate(2131558541, (ViewGroup)null);
        this.initializeControllers();
        return (Dialog)setPositiveButton.setView(this.mRootView).create();
    }
    
    public void registerClickListener(final int n, final View.OnClickListener onClickListener) {
        final View viewById = this.mRootView.findViewById(n);
        if (viewById != null) {
            viewById.setOnClickListener(onClickListener);
        }
    }
    
    public void removeSettingFromScreen(final int n) {
        final View viewById = this.mRootView.findViewById(n);
        if (viewById != null) {
            viewById.setVisibility(8);
        }
    }
    
    public void setText(final int n, final CharSequence text) {
        final TextView textView = (TextView)this.mRootView.findViewById(n);
        if (textView != null) {
            textView.setText(text);
        }
    }
}
