package com.android.settings.deviceinfo.firmwareversion;

import com.android.settingslib.DeviceInfoUtils;

public class KernelVersionDialogController
{
    static int KERNEL_VERSION_VALUE_ID;
    private final FirmwareVersionDialogFragment mDialog;
    
    static {
        KernelVersionDialogController.KERNEL_VERSION_VALUE_ID = 2131362299;
    }
    
    public KernelVersionDialogController(final FirmwareVersionDialogFragment mDialog) {
        this.mDialog = mDialog;
    }
    
    public void initialize() {
        this.mDialog.setText(KernelVersionDialogController.KERNEL_VERSION_VALUE_ID, DeviceInfoUtils.getFormattedKernelVersion(this.mDialog.getContext()));
    }
}
