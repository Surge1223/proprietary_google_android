package com.android.settings.deviceinfo.firmwareversion;

import android.content.Context;
import android.os.SystemProperties;
import com.android.settingslib.Utils;

public class BasebandVersionDialogController
{
    static final String BASEBAND_PROPERTY = "gsm.version.baseband";
    static final int BASEBAND_VERSION_LABEL_ID = 2131361902;
    static final int BASEBAND_VERSION_VALUE_ID = 2131361903;
    private final FirmwareVersionDialogFragment mDialog;
    
    public BasebandVersionDialogController(final FirmwareVersionDialogFragment mDialog) {
        this.mDialog = mDialog;
    }
    
    public void initialize() {
        final Context context = this.mDialog.getContext();
        if (Utils.isWifiOnly(context)) {
            this.mDialog.removeSettingFromScreen(2131361902);
            this.mDialog.removeSettingFromScreen(2131361903);
            return;
        }
        this.mDialog.setText(2131361903, SystemProperties.get("gsm.version.baseband", context.getString(2131887389)));
    }
}
