package com.android.settings.deviceinfo;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.os.storage.StorageManager;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.View;
import android.os.storage.VolumeInfo;
import android.os.storage.DiskInfo;
import android.view.View.OnClickListener;
import com.android.settings.SettingsPreferenceFragment;

public class PrivateVolumeUnmount extends SettingsPreferenceFragment
{
    private final View.OnClickListener mConfirmListener;
    private DiskInfo mDisk;
    private VolumeInfo mVolume;
    
    public PrivateVolumeUnmount() {
        this.mConfirmListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                new StorageSettings.UnmountTask((Context)PrivateVolumeUnmount.this.getActivity(), PrivateVolumeUnmount.this.mVolume).execute((Object[])new Void[0]);
                PrivateVolumeUnmount.this.getActivity().finish();
            }
        };
    }
    
    @Override
    public int getMetricsCategory() {
        return 42;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final StorageManager storageManager = (StorageManager)this.getActivity().getSystemService((Class)StorageManager.class);
        this.mVolume = storageManager.findVolumeById(this.getArguments().getString("android.os.storage.extra.VOLUME_ID"));
        this.mDisk = storageManager.findDiskById(this.mVolume.getDiskId());
        final View inflate = layoutInflater.inflate(2131558762, viewGroup, false);
        final TextView textView = (TextView)inflate.findViewById(2131361913);
        final Button button = (Button)inflate.findViewById(2131362002);
        textView.setText(TextUtils.expandTemplate(this.getText(2131889278), new CharSequence[] { this.mDisk.getDescription() }));
        button.setOnClickListener(this.mConfirmListener);
        return inflate;
    }
}
