package com.android.settings.deviceinfo;

import com.android.settings.deviceinfo.storage.VolumeSizesLoader;
import java.util.function.Consumer;
import java.util.function.Predicate;
import com.android.settings.deviceinfo.storage.UserIconLoader;
import android.graphics.drawable.Drawable;
import com.android.settings.widget.EntityHeaderController;
import android.view.View;
import com.android.settingslib.applications.StorageStatsSource;
import android.content.Loader;
import com.android.settings.Utils;
import android.os.Bundle;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.app.Activity;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.deviceinfo.storage.AutomaticStorageManagementSwitchPreferenceController;
import android.app.usage.StorageStatsManager;
import android.os.UserHandle;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.Collection;
import com.android.settings.deviceinfo.storage.SecondaryUserController;
import com.android.settingslib.deviceinfo.StorageVolumeProvider;
import android.app.Fragment;
import com.android.settingslib.deviceinfo.StorageManagerVolumeProvider;
import java.util.ArrayList;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.os.storage.VolumeInfo;
import com.android.settings.deviceinfo.storage.StorageSummaryDonutPreferenceController;
import com.android.settingslib.deviceinfo.PrivateStorageInfo;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.deviceinfo.storage.StorageItemPreferenceController;
import com.android.settings.deviceinfo.storage.CachedStorageValuesHelper;
import com.android.settings.search.Indexable;
import com.android.settings.deviceinfo.storage.StorageAsyncLoader;
import android.util.SparseArray;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.dashboard.DashboardFragment;

public class StorageDashboardFragment extends DashboardFragment implements LoaderManager$LoaderCallbacks<SparseArray<StorageAsyncLoader.AppsStorageResult>>
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private SparseArray<StorageAsyncLoader.AppsStorageResult> mAppsResult;
    private CachedStorageValuesHelper mCachedStorageValuesHelper;
    private PrivateVolumeOptionMenuController mOptionMenuController;
    private StorageItemPreferenceController mPreferenceController;
    private List<AbstractPreferenceController> mSecondaryUsers;
    private PrivateStorageInfo mStorageInfo;
    private StorageSummaryDonutPreferenceController mSummaryController;
    private VolumeInfo mVolume;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                final StorageManager storageManager = (StorageManager)context.getSystemService((Class)StorageManager.class);
                final UserManager userManager = (UserManager)context.getSystemService((Class)UserManager.class);
                final ArrayList<StorageItemPreferenceController> list = (ArrayList<StorageItemPreferenceController>)new ArrayList<AbstractPreferenceController>();
                list.add(new StorageSummaryDonutPreferenceController(context));
                list.add(new StorageItemPreferenceController(context, null, null, new StorageManagerVolumeProvider(storageManager)));
                list.addAll((Collection<? extends AbstractPreferenceController>)SecondaryUserController.getSecondaryUserControllers(context, userManager));
                return (List<AbstractPreferenceController>)list;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082836;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private void initializeCacheProvider() {
        this.mCachedStorageValuesHelper = new CachedStorageValuesHelper(this.getContext(), UserHandle.myUserId());
        this.initializeCachedValues();
        this.onReceivedSizes();
    }
    
    private boolean isQuotaSupported() {
        return ((StorageStatsManager)this.getActivity().getSystemService((Class)StorageStatsManager.class)).isQuotaSupported(this.mVolume.fsUuid);
    }
    
    private void maybeCacheFreshValues() {
        if (this.mStorageInfo != null && this.mAppsResult != null) {
            this.mCachedStorageValuesHelper.cacheResult(this.mStorageInfo, (StorageAsyncLoader.AppsStorageResult)this.mAppsResult.get(UserHandle.myUserId()));
        }
    }
    
    private void onReceivedSizes() {
        if (this.mStorageInfo != null) {
            final long usedSize = this.mStorageInfo.totalBytes - this.mStorageInfo.freeBytes;
            this.mSummaryController.updateBytes(usedSize, this.mStorageInfo.totalBytes);
            this.mPreferenceController.setVolume(this.mVolume);
            this.mPreferenceController.setUsedSize(usedSize);
            this.mPreferenceController.setTotalSize(this.mStorageInfo.totalBytes);
            for (int i = 0; i < this.mSecondaryUsers.size(); ++i) {
                final AbstractPreferenceController abstractPreferenceController = this.mSecondaryUsers.get(i);
                if (abstractPreferenceController instanceof SecondaryUserController) {
                    ((SecondaryUserController)abstractPreferenceController).setTotalSize(this.mStorageInfo.totalBytes);
                }
            }
        }
        if (this.mAppsResult == null) {
            return;
        }
        this.mPreferenceController.onLoadFinished(this.mAppsResult, UserHandle.myUserId());
        this.updateSecondaryUserControllers(this.mSecondaryUsers, this.mAppsResult);
        if (this.getView().findViewById(2131362342).getVisibility() == 0) {
            this.setLoading(false, true);
        }
    }
    
    private void updateSecondaryUserControllers(final List<AbstractPreferenceController> list, final SparseArray<StorageAsyncLoader.AppsStorageResult> sparseArray) {
        for (int i = 0; i < list.size(); ++i) {
            final AbstractPreferenceController abstractPreferenceController = list.get(i);
            if (abstractPreferenceController instanceof StorageAsyncLoader.ResultHandler) {
                ((StorageAsyncLoader.ResultHandler)abstractPreferenceController).handleResult(sparseArray);
            }
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<StorageItemPreferenceController> list = (ArrayList<StorageItemPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(this.mSummaryController = new StorageSummaryDonutPreferenceController(context));
        list.add(this.mPreferenceController = new StorageItemPreferenceController(context, this, this.mVolume, new StorageManagerVolumeProvider((StorageManager)context.getSystemService((Class)StorageManager.class))));
        list.addAll((Collection<? extends AbstractPreferenceController>)(this.mSecondaryUsers = SecondaryUserController.getSecondaryUserControllers(context, (UserManager)context.getSystemService((Class)UserManager.class))));
        final AutomaticStorageManagementSwitchPreferenceController automaticStorageManagementSwitchPreferenceController = new AutomaticStorageManagementSwitchPreferenceController(context, this.mMetricsFeatureProvider, this.getFragmentManager());
        this.getLifecycle().addObserver(automaticStorageManagementSwitchPreferenceController);
        list.add((StorageSummaryDonutPreferenceController)automaticStorageManagementSwitchPreferenceController);
        return (List<AbstractPreferenceController>)list;
    }
    
    public SparseArray<StorageAsyncLoader.AppsStorageResult> getAppsStorageResult() {
        return this.mAppsResult;
    }
    
    public int getHelpResource() {
        return 2131887830;
    }
    
    @Override
    protected String getLogTag() {
        return "StorageDashboardFrag";
    }
    
    public int getMetricsCategory() {
        return 745;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082836;
    }
    
    public PrivateStorageInfo getPrivateStorageInfo() {
        return this.mStorageInfo;
    }
    
    public void initializeCachedValues() {
        final PrivateStorageInfo cachedPrivateStorageInfo = this.mCachedStorageValuesHelper.getCachedPrivateStorageInfo();
        final SparseArray<StorageAsyncLoader.AppsStorageResult> cachedAppsStorageResult = this.mCachedStorageValuesHelper.getCachedAppsStorageResult();
        if (cachedPrivateStorageInfo != null && cachedAppsStorageResult != null) {
            this.mStorageInfo = cachedPrivateStorageInfo;
            this.mAppsResult = cachedAppsStorageResult;
        }
    }
    
    void initializeOptionsMenu(final Activity activity) {
        this.mOptionMenuController = new PrivateVolumeOptionMenuController((Context)activity, this.mVolume, new PackageManagerWrapper(activity.getPackageManager()));
        this.getLifecycle().addObserver(this.mOptionMenuController);
        this.setHasOptionsMenu(true);
        activity.invalidateOptionsMenu();
    }
    
    public void maybeSetLoading(final boolean b) {
        if ((b && (this.mStorageInfo == null || this.mAppsResult == null)) || (!b && this.mStorageInfo == null)) {
            this.setLoading(true, false);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mVolume = Utils.maybeInitializeVolume((StorageManager)activity.getSystemService((Class)StorageManager.class), this.getArguments());
        if (this.mVolume == null) {
            activity.finish();
            return;
        }
        this.initializeOptionsMenu(activity);
    }
    
    public Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>> onCreateLoader(final int n, final Bundle bundle) {
        final Context context = this.getContext();
        return (Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>>)new StorageAsyncLoader(context, (UserManager)context.getSystemService((Class)UserManager.class), this.mVolume.fsUuid, new StorageStatsSource(context), new PackageManagerWrapper(context.getPackageManager()));
    }
    
    public void onLoadFinished(final Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>> loader, final SparseArray<StorageAsyncLoader.AppsStorageResult> mAppsResult) {
        this.mAppsResult = mAppsResult;
        this.maybeCacheFreshValues();
        this.onReceivedSizes();
    }
    
    public void onLoaderReset(final Loader<SparseArray<StorageAsyncLoader.AppsStorageResult>> loader) {
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.getLoaderManager().restartLoader(0, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
        this.getLoaderManager().restartLoader(2, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)new VolumeSizeCallbacks());
        this.getLoaderManager().initLoader(1, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)new IconLoaderCallbacks());
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.initializeCacheProvider();
        this.maybeSetLoading(this.isQuotaSupported());
        final Activity activity = this.getActivity();
        EntityHeaderController.newInstance(activity, this, null).setRecyclerView(this.getListView(), this.getLifecycle()).styleActionBar(activity);
    }
    
    public void setAppsStorageResult(final SparseArray<StorageAsyncLoader.AppsStorageResult> mAppsResult) {
        this.mAppsResult = mAppsResult;
    }
    
    public void setCachedStorageValuesHelper(final CachedStorageValuesHelper mCachedStorageValuesHelper) {
        this.mCachedStorageValuesHelper = mCachedStorageValuesHelper;
    }
    
    public void setPrivateStorageInfo(final PrivateStorageInfo mStorageInfo) {
        this.mStorageInfo = mStorageInfo;
    }
    
    protected void setVolume(final VolumeInfo mVolume) {
        this.mVolume = mVolume;
    }
    
    public final class IconLoaderCallbacks implements LoaderManager$LoaderCallbacks<SparseArray<Drawable>>
    {
        public Loader<SparseArray<Drawable>> onCreateLoader(final int n, final Bundle bundle) {
            return (Loader<SparseArray<Drawable>>)new UserIconLoader(StorageDashboardFragment.this.getContext(), (UserIconLoader.FetchUserIconTask)new _$$Lambda$StorageDashboardFragment$IconLoaderCallbacks$yGwysNy4Bq4_2nwwvU2QePhZgvU(this));
        }
        
        public void onLoadFinished(final Loader<SparseArray<Drawable>> loader, final SparseArray<Drawable> sparseArray) {
            StorageDashboardFragment.this.mSecondaryUsers.stream().filter((Predicate)_$$Lambda$StorageDashboardFragment$IconLoaderCallbacks$7UIHa462aQ5cO1d2zsPI99b5Y1Y.INSTANCE).forEach(new _$$Lambda$StorageDashboardFragment$IconLoaderCallbacks$Jn0eBlqBHbuO_2COJ4jEmaXSJJc(sparseArray));
        }
        
        public void onLoaderReset(final Loader<SparseArray<Drawable>> loader) {
        }
    }
    
    public final class VolumeSizeCallbacks implements LoaderManager$LoaderCallbacks<PrivateStorageInfo>
    {
        public Loader<PrivateStorageInfo> onCreateLoader(final int n, final Bundle bundle) {
            final Context context = StorageDashboardFragment.this.getContext();
            return (Loader<PrivateStorageInfo>)new VolumeSizesLoader(context, new StorageManagerVolumeProvider((StorageManager)context.getSystemService((Class)StorageManager.class)), (StorageStatsManager)context.getSystemService((Class)StorageStatsManager.class), StorageDashboardFragment.this.mVolume);
        }
        
        public void onLoadFinished(final Loader<PrivateStorageInfo> loader, final PrivateStorageInfo privateStorageInfo) {
            if (privateStorageInfo == null) {
                StorageDashboardFragment.this.getActivity().finish();
                return;
            }
            StorageDashboardFragment.this.mStorageInfo = privateStorageInfo;
            StorageDashboardFragment.this.maybeCacheFreshValues();
            StorageDashboardFragment.this.onReceivedSizes();
        }
        
        public void onLoaderReset(final Loader<PrivateStorageInfo> loader) {
        }
    }
}
