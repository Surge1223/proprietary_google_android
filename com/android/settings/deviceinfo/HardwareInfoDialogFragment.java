package com.android.settings.deviceinfo;

import android.text.TextUtils;
import android.widget.TextView;
import android.view.View;
import android.os.SystemProperties;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Build;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class HardwareInfoDialogFragment extends InstrumentedDialogFragment
{
    public static HardwareInfoDialogFragment newInstance() {
        return new HardwareInfoDialogFragment();
    }
    
    @Override
    public int getMetricsCategory() {
        return 862;
    }
    
    String getSerialNumber() {
        return Build.getSerial();
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final AlertDialog$Builder setPositiveButton = new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887741).setPositiveButton(17039370, (DialogInterface$OnClickListener)null);
        final View inflate = LayoutInflater.from(setPositiveButton.getContext()).inflate(2131558542, (ViewGroup)null);
        this.setText(inflate, 2131362376, 2131362377, DeviceModelPreferenceController.getDeviceModel());
        this.setText(inflate, 2131362586, 2131362587, this.getSerialNumber());
        this.setText(inflate, 2131362185, 2131362186, SystemProperties.get("ro.boot.hardware.revision"));
        return (Dialog)setPositiveButton.setView(inflate).create();
    }
    
    void setText(final View view, final int n, final int n2, final String text) {
        if (view == null) {
            return;
        }
        final View viewById = view.findViewById(n);
        final TextView textView = (TextView)view.findViewById(n2);
        if (!TextUtils.isEmpty((CharSequence)text)) {
            viewById.setVisibility(0);
            textView.setVisibility(0);
            textView.setText((CharSequence)text);
        }
        else {
            viewById.setVisibility(8);
            textView.setVisibility(8);
        }
    }
}
