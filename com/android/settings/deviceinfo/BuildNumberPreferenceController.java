package com.android.settings.deviceinfo;

import android.os.UserHandle;
import android.content.ComponentName;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.Intent;
import android.util.Pair;
import com.android.settings.Utils;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.os.Build;
import android.text.BidiFormatter;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.development.DevelopmentSettingsEnabler;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.app.Fragment;
import android.widget.Toast;
import com.android.settingslib.RestrictedLockUtils;
import android.app.Activity;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BuildNumberPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnResume
{
    private final Activity mActivity;
    private RestrictedLockUtils.EnforcedAdmin mDebuggingFeaturesDisallowedAdmin;
    private boolean mDebuggingFeaturesDisallowedBySystem;
    private int mDevHitCountdown;
    private Toast mDevHitToast;
    private final Fragment mFragment;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private boolean mProcessingLastDevHit;
    private final UserManager mUm;
    
    public BuildNumberPreferenceController(final Context context, final Activity mActivity, final Fragment mFragment, final Lifecycle lifecycle) {
        super(context);
        this.mActivity = mActivity;
        this.mFragment = mFragment;
        this.mUm = (UserManager)context.getSystemService("user");
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private void enableDevelopmentSettings() {
        this.mDevHitCountdown = 0;
        this.mProcessingLastDevHit = false;
        DevelopmentSettingsEnabler.setDevelopmentSettingsEnabled(this.mContext, true);
        if (this.mDevHitToast != null) {
            this.mDevHitToast.cancel();
        }
        (this.mDevHitToast = Toast.makeText(this.mContext, 2131889090, 1)).show();
    }
    
    @Override
    public void displayPreference(PreferenceScreen preference) {
        super.displayPreference(preference);
        preference = (PreferenceScreen)preference.findPreference("build_number");
        if (preference != null) {
            try {
                preference.setSummary(BidiFormatter.getInstance().unicodeWrap(Build.DISPLAY));
                preference.setEnabled(true);
            }
            catch (Exception ex) {
                preference.setSummary(2131887389);
            }
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "build_number";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"build_number")) {
            return false;
        }
        if (Utils.isMonkeyRunning()) {
            return false;
        }
        if (!this.mUm.isAdminUser() && !this.mUm.isDemoUser()) {
            this.mMetricsFeatureProvider.action(this.mContext, 847, (Pair<Integer, Object>[])new Pair[0]);
            return false;
        }
        if (!Utils.isDeviceProvisioned(this.mContext)) {
            this.mMetricsFeatureProvider.action(this.mContext, 847, (Pair<Integer, Object>[])new Pair[0]);
            return false;
        }
        if (this.mUm.hasUserRestriction("no_debugging_features")) {
            if (this.mUm.isDemoUser()) {
                final ComponentName deviceOwnerComponent = Utils.getDeviceOwnerComponent(this.mContext);
                if (deviceOwnerComponent != null) {
                    final Intent setAction = new Intent().setPackage(deviceOwnerComponent.getPackageName()).setAction("com.android.settings.action.REQUEST_DEBUG_FEATURES");
                    if (this.mContext.getPackageManager().resolveActivity(setAction, 0) != null) {
                        this.mContext.startActivity(setAction);
                        return false;
                    }
                }
            }
            if (this.mDebuggingFeaturesDisallowedAdmin != null && !this.mDebuggingFeaturesDisallowedBySystem) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mDebuggingFeaturesDisallowedAdmin);
            }
            this.mMetricsFeatureProvider.action(this.mContext, 847, (Pair<Integer, Object>[])new Pair[0]);
            return false;
        }
        if (this.mDevHitCountdown > 0) {
            --this.mDevHitCountdown;
            if (this.mDevHitCountdown == 0 && !this.mProcessingLastDevHit) {
                ++this.mDevHitCountdown;
                if (!(this.mProcessingLastDevHit = new ChooseLockSettingsHelper(this.mActivity, this.mFragment).launchConfirmationActivity(100, this.mContext.getString(2131889578)))) {
                    this.enableDevelopmentSettings();
                }
                this.mMetricsFeatureProvider.action(this.mContext, 847, Pair.create((Object)848, (Object)(int)((this.mProcessingLastDevHit ^ true) ? 1 : 0)));
            }
            else if (this.mDevHitCountdown > 0 && this.mDevHitCountdown < 5) {
                if (this.mDevHitToast != null) {
                    this.mDevHitToast.cancel();
                }
                (this.mDevHitToast = Toast.makeText(this.mContext, (CharSequence)this.mContext.getResources().getQuantityString(2131755062, this.mDevHitCountdown, new Object[] { this.mDevHitCountdown }), 0)).show();
            }
            this.mMetricsFeatureProvider.action(this.mContext, 847, Pair.create((Object)848, (Object)0));
        }
        else if (this.mDevHitCountdown < 0) {
            if (this.mDevHitToast != null) {
                this.mDevHitToast.cancel();
            }
            (this.mDevHitToast = Toast.makeText(this.mContext, 2131889089, 1)).show();
            this.mMetricsFeatureProvider.action(this.mContext, 847, Pair.create((Object)848, (Object)1));
        }
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (n != 100) {
            return false;
        }
        if (n2 == -1) {
            this.enableDevelopmentSettings();
        }
        this.mProcessingLastDevHit = false;
        return true;
    }
    
    @Override
    public void onResume() {
        this.mDebuggingFeaturesDisallowedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_debugging_features", UserHandle.myUserId());
        this.mDebuggingFeaturesDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_debugging_features", UserHandle.myUserId());
        int mDevHitCountdown;
        if (DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(this.mContext)) {
            mDevHitCountdown = -1;
        }
        else {
            mDevHitCountdown = 7;
        }
        this.mDevHitCountdown = mDevHitCountdown;
        this.mDevHitToast = null;
    }
}
