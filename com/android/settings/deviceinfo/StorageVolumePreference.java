package com.android.settings.deviceinfo;

import android.widget.ProgressBar;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.widget.ImageView;
import android.support.v7.preference.PreferenceViewHolder;
import java.io.File;
import android.graphics.drawable.Drawable;
import com.android.settingslib.Utils;
import android.text.format.Formatter;
import android.util.Log;
import java.io.IOException;
import android.app.usage.StorageStatsManager;
import android.view.View;
import android.content.Context;
import android.os.storage.VolumeInfo;
import android.view.View.OnClickListener;
import android.os.storage.StorageManager;
import android.support.v7.preference.Preference;

public class StorageVolumePreference extends Preference
{
    private static final String TAG;
    private int mColor;
    private final StorageManager mStorageManager;
    private final View.OnClickListener mUnmountListener;
    private int mUsedPercent;
    private final VolumeInfo mVolume;
    
    static {
        TAG = StorageVolumePreference.class.getSimpleName();
    }
    
    public StorageVolumePreference(final Context context, final VolumeInfo mVolume, final int mColor, long totalSpace) {
        super(context);
        this.mUsedPercent = -1;
        this.mUnmountListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                new StorageSettings.UnmountTask(StorageVolumePreference.this.getContext(), StorageVolumePreference.this.mVolume).execute((Object[])new Void[0]);
            }
        };
        this.mStorageManager = (StorageManager)context.getSystemService((Class)StorageManager.class);
        this.mVolume = mVolume;
        this.mColor = mColor;
        this.setLayoutResource(2131558766);
        this.setKey(mVolume.getId());
        this.setTitle(this.mStorageManager.getBestVolumeDescription(mVolume));
        Drawable icon;
        if ("private".equals(mVolume.getId())) {
            icon = context.getDrawable(2131231157);
        }
        else {
            icon = context.getDrawable(2131231154);
        }
        if (mVolume.isMountedReadable()) {
            final File path = mVolume.getPath();
            long freeBytes = 0L;
            final long n = 0L;
            long freeSpace;
            long n3;
            if (mVolume.getType() == 1) {
                final StorageStatsManager storageStatsManager = (StorageStatsManager)context.getSystemService((Class)StorageStatsManager.class);
                long totalBytes;
                try {
                    totalBytes = storageStatsManager.getTotalBytes(mVolume.getFsUuid());
                    try {
                        totalSpace = (freeBytes = storageStatsManager.getFreeBytes(mVolume.getFsUuid()));
                        totalSpace = totalBytes - freeBytes;
                    }
                    catch (IOException ex) {}
                }
                catch (IOException ex) {
                    totalBytes = totalSpace;
                }
                final IOException ex;
                Log.w(StorageVolumePreference.TAG, (Throwable)ex);
                totalSpace = n;
                final long n2 = totalBytes;
                freeSpace = freeBytes;
                n3 = n2;
            }
            else {
                if (totalSpace <= 0L) {
                    totalSpace = path.getTotalSpace();
                }
                freeSpace = path.getFreeSpace();
                final long n4 = totalSpace - freeSpace;
                n3 = totalSpace;
                totalSpace = n4;
            }
            this.setSummary(context.getString(2131889321, new Object[] { Formatter.formatFileSize(context, totalSpace), Formatter.formatFileSize(context, n3) }));
            if (n3 > 0L) {
                this.mUsedPercent = (int)(100L * totalSpace / n3);
            }
            if (freeSpace < this.mStorageManager.getStorageLowBytes(path)) {
                this.mColor = Utils.getColorAttr(context, 16844099);
                icon = context.getDrawable(2131231193);
            }
        }
        else {
            this.setSummary(mVolume.getStateDescription());
            this.mUsedPercent = -1;
        }
        icon.mutate();
        icon.setTint(this.mColor);
        this.setIcon(icon);
        if (mVolume.getType() == 0 && mVolume.isMountedReadable()) {
            this.setWidgetLayoutResource(2131558671);
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131362774);
        if (imageView != null) {
            imageView.setImageTintList(ColorStateList.valueOf(Color.parseColor("#8a000000")));
            imageView.setOnClickListener(this.mUnmountListener);
        }
        final ProgressBar progressBar = (ProgressBar)preferenceViewHolder.findViewById(16908301);
        if (this.mVolume.getType() == 1 && this.mUsedPercent != -1) {
            progressBar.setVisibility(0);
            progressBar.setProgress(this.mUsedPercent);
            progressBar.setProgressTintList(ColorStateList.valueOf(this.mColor));
        }
        else {
            progressBar.setVisibility(8);
        }
        super.onBindViewHolder(preferenceViewHolder);
    }
}
