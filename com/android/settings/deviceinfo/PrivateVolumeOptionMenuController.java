package com.android.settings.deviceinfo;

import java.util.Objects;
import android.content.Intent;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.storage.VolumeInfo;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnPrepareOptionsMenu;
import com.android.settingslib.core.lifecycle.events.OnOptionsItemSelected;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class PrivateVolumeOptionMenuController implements LifecycleObserver, OnCreateOptionsMenu, OnOptionsItemSelected, OnPrepareOptionsMenu
{
    private Context mContext;
    private PackageManagerWrapper mPm;
    private VolumeInfo mVolumeInfo;
    
    public PrivateVolumeOptionMenuController(final Context mContext, final VolumeInfo mVolumeInfo, final PackageManagerWrapper mPm) {
        this.mContext = mContext;
        this.mVolumeInfo = mVolumeInfo;
        this.mPm = mPm;
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        menu.add(0, 100, 0, 2131889292);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 100) {
            final Intent intent = new Intent(this.mContext, (Class)StorageWizardMigrateConfirm.class);
            intent.putExtra("android.os.storage.extra.VOLUME_ID", this.mVolumeInfo.getId());
            this.mContext.startActivity(intent);
            return true;
        }
        return false;
    }
    
    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        if (this.mVolumeInfo == null) {
            return;
        }
        final VolumeInfo primaryStorageCurrentVolume = this.mPm.getPrimaryStorageCurrentVolume();
        final MenuItem item = menu.findItem(100);
        if (item != null) {
            boolean visible = true;
            if (primaryStorageCurrentVolume == null || primaryStorageCurrentVolume.getType() != 1 || Objects.equals(this.mVolumeInfo, primaryStorageCurrentVolume)) {
                visible = false;
            }
            item.setVisible(visible);
        }
    }
}
