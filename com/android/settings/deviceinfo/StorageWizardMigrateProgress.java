package com.android.settings.deviceinfo;

import android.os.Handler;
import android.os.Bundle;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$MoveCallback;

public class StorageWizardMigrateProgress extends StorageWizardBase
{
    private final PackageManager$MoveCallback mCallback;
    private int mMoveId;
    
    public StorageWizardMigrateProgress() {
        this.mCallback = new PackageManager$MoveCallback() {
            public void onStatusChanged(final int n, final int currentProgress, final long n2) {
                if (StorageWizardMigrateProgress.this.mMoveId != n) {
                    return;
                }
                final StorageWizardMigrateProgress this$0 = StorageWizardMigrateProgress.this;
                if (PackageManager.isMoveStatusFinished(currentProgress)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Finished with status ");
                    sb.append(currentProgress);
                    Log.d("StorageSettings", sb.toString());
                    if (currentProgress == -100) {
                        if (StorageWizardMigrateProgress.this.mDisk != null) {
                            final Intent intent = new Intent("com.android.systemui.action.FINISH_WIZARD");
                            intent.addFlags(1073741824);
                            StorageWizardMigrateProgress.this.sendBroadcast(intent);
                            if (!StorageWizardMigrateProgress.this.isFinishing()) {
                                final Intent intent2 = new Intent((Context)this$0, (Class)StorageWizardReady.class);
                                intent2.putExtra("android.os.storage.extra.DISK_ID", StorageWizardMigrateProgress.this.mDisk.getId());
                                StorageWizardMigrateProgress.this.startActivity(intent2);
                            }
                        }
                    }
                    else {
                        Toast.makeText((Context)this$0, (CharSequence)StorageWizardMigrateProgress.this.getString(2131887900), 1).show();
                    }
                    StorageWizardMigrateProgress.this.finishAffinity();
                }
                else {
                    StorageWizardMigrateProgress.this.setCurrentProgress(currentProgress);
                }
            }
        };
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mVolume == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558772);
        this.mMoveId = this.getIntent().getIntExtra("android.content.pm.extra.MOVE_ID", -1);
        this.setIcon(2131231166);
        this.setHeaderText(2131889357, new CharSequence[0]);
        this.setAuxChecklist();
        this.getPackageManager().registerMoveCallback(this.mCallback, new Handler());
        this.mCallback.onStatusChanged(this.mMoveId, this.getPackageManager().getMoveStatus(this.mMoveId), -1L);
    }
}
