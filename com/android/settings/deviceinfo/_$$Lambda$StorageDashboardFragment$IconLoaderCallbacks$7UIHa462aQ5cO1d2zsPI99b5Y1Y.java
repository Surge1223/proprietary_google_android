package com.android.settings.deviceinfo;

import java.util.function.Consumer;
import android.content.Loader;
import android.os.Bundle;
import com.android.settings.deviceinfo.storage.UserIconLoader;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.function.Predicate;

public final class _$$Lambda$StorageDashboardFragment$IconLoaderCallbacks$7UIHa462aQ5cO1d2zsPI99b5Y1Y implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return StorageDashboardFragment.IconLoaderCallbacks.lambda$onLoadFinished$1((AbstractPreferenceController)o);
    }
}
