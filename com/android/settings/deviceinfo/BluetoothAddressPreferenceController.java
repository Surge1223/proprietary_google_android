package com.android.settings.deviceinfo;

import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.deviceinfo.AbstractBluetoothAddressPreferenceController;

public class BluetoothAddressPreferenceController extends AbstractBluetoothAddressPreferenceController implements PreferenceControllerMixin
{
    public BluetoothAddressPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, lifecycle);
    }
}
