package com.android.settings.deviceinfo;

import com.android.settings.utils.FileSizeFormatter;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ProgressBar;
import android.support.v7.preference.Preference;

public class StorageItemPreference extends Preference
{
    private ProgressBar mProgressBar;
    private int mProgressPercent;
    public int userHandle;
    
    public StorageItemPreference(final Context context) {
        this(context, null);
    }
    
    public StorageItemPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mProgressPercent = -1;
        this.setLayoutResource(2131558763);
        this.setSummary(2131888267);
    }
    
    private static int getGigabyteSuffix(final Resources resources) {
        return resources.getIdentifier("gigabyteShort", "string", "android");
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        this.mProgressBar = (ProgressBar)preferenceViewHolder.findViewById(16908301);
        this.updateProgressBar();
        super.onBindViewHolder(preferenceViewHolder);
    }
    
    public void setStorageSize(final long n, final long n2) {
        this.setSummary(FileSizeFormatter.formatFileSize(this.getContext(), n, getGigabyteSuffix(this.getContext().getResources()), 1000000000L));
        if (n2 == 0L) {
            this.mProgressPercent = 0;
        }
        else {
            this.mProgressPercent = (int)(100L * n / n2);
        }
        this.updateProgressBar();
    }
    
    protected void updateProgressBar() {
        if (this.mProgressBar != null && this.mProgressPercent != -1) {
            this.mProgressBar.setMax(100);
            this.mProgressBar.setProgress(this.mProgressPercent);
        }
    }
}
