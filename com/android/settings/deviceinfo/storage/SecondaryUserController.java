package com.android.settings.deviceinfo.storage;

import android.util.SparseArray;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.Utils;
import java.util.ArrayList;
import java.util.List;
import android.os.UserManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.content.pm.UserInfo;
import com.android.settings.deviceinfo.StorageItemPreference;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class SecondaryUserController extends AbstractPreferenceController implements PreferenceControllerMixin, ResultHandler, UserIconHandler
{
    private long mSize;
    private StorageItemPreference mStoragePreference;
    private long mTotalSizeBytes;
    private UserInfo mUser;
    private Drawable mUserIcon;
    
    SecondaryUserController(final Context context, final UserInfo mUser) {
        super(context);
        this.mUser = mUser;
        this.mSize = -1L;
    }
    
    public static List<AbstractPreferenceController> getSecondaryUserControllers(final Context context, final UserManager userManager) {
        final ArrayList<NoSecondaryUserController> list = new ArrayList<NoSecondaryUserController>();
        final UserInfo primaryUser = userManager.getPrimaryUser();
        boolean b = false;
        final List users = userManager.getUsers();
        for (int i = 0; i < users.size(); ++i) {
            final UserInfo userInfo = users.get(i);
            if (!userInfo.isPrimary()) {
                if (userInfo != null && !Utils.isProfileOf(primaryUser, userInfo)) {
                    list.add((NoSecondaryUserController)new SecondaryUserController(context, userInfo));
                    b = true;
                }
                else {
                    list.add((NoSecondaryUserController)new UserProfileController(context, userInfo, 6));
                }
            }
        }
        if (!b) {
            list.add(new NoSecondaryUserController(context));
        }
        return (List<AbstractPreferenceController>)list;
    }
    
    private void maybeSetIcon() {
        if (this.mUserIcon != null && this.mStoragePreference != null) {
            this.mStoragePreference.setIcon(this.mUserIcon);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        if (this.mStoragePreference == null) {
            this.mStoragePreference = new StorageItemPreference(preferenceScreen.getContext());
            final PreferenceGroup preferenceGroup = (PreferenceGroup)preferenceScreen.findPreference("pref_secondary_users");
            this.mStoragePreference.setTitle(this.mUser.name);
            final StorageItemPreference mStoragePreference = this.mStoragePreference;
            final StringBuilder sb = new StringBuilder();
            sb.append("pref_user_");
            sb.append(this.mUser.id);
            mStoragePreference.setKey(sb.toString());
            if (this.mSize != -1L) {
                this.mStoragePreference.setStorageSize(this.mSize, this.mTotalSizeBytes);
            }
            preferenceGroup.setVisible(true);
            preferenceGroup.addPreference(this.mStoragePreference);
            this.maybeSetIcon();
        }
    }
    
    @Override
    public String getPreferenceKey() {
        String key;
        if (this.mStoragePreference != null) {
            key = this.mStoragePreference.getKey();
        }
        else {
            key = null;
        }
        return key;
    }
    
    public UserInfo getUser() {
        return this.mUser;
    }
    
    @Override
    public void handleResult(final SparseArray<AppsStorageResult> sparseArray) {
        final AppsStorageResult appsStorageResult = (AppsStorageResult)sparseArray.get(this.getUser().id);
        if (appsStorageResult != null) {
            this.setSize(appsStorageResult.externalStats.totalBytes);
        }
    }
    
    @Override
    public void handleUserIcons(final SparseArray<Drawable> sparseArray) {
        this.mUserIcon = (Drawable)sparseArray.get(this.mUser.id);
        this.maybeSetIcon();
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void setSize(final long mSize) {
        this.mSize = mSize;
        if (this.mStoragePreference != null) {
            this.mStoragePreference.setStorageSize(this.mSize, this.mTotalSizeBytes);
        }
    }
    
    public void setTotalSize(final long mTotalSizeBytes) {
        this.mTotalSizeBytes = mTotalSizeBytes;
    }
    
    private static class NoSecondaryUserController extends AbstractPreferenceController implements PreferenceControllerMixin
    {
        public NoSecondaryUserController(final Context context) {
            super(context);
        }
        
        @Override
        public void displayPreference(final PreferenceScreen preferenceScreen) {
            final PreferenceGroup preferenceGroup = (PreferenceGroup)preferenceScreen.findPreference("pref_secondary_users");
            if (preferenceGroup == null) {
                return;
            }
            preferenceScreen.removePreference(preferenceGroup);
        }
        
        @Override
        public String getPreferenceKey() {
            return null;
        }
        
        @Override
        public boolean isAvailable() {
            return true;
        }
    }
}
