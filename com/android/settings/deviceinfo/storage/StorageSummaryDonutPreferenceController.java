package com.android.settings.deviceinfo.storage;

import android.text.format.Formatter$BytesResult;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class StorageSummaryDonutPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private StorageSummaryDonutPreference mSummary;
    private long mTotalBytes;
    private long mUsedBytes;
    
    public StorageSummaryDonutPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        (this.mSummary = (StorageSummaryDonutPreference)preferenceScreen.findPreference("pref_summary")).setEnabled(true);
    }
    
    @Override
    public String getPreferenceKey() {
        return "pref_summary";
    }
    
    public void invalidateData() {
        if (this.mSummary != null) {
            this.updateState(this.mSummary);
        }
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void updateBytes(final long mUsedBytes, final long mTotalBytes) {
        this.mUsedBytes = mUsedBytes;
        this.mTotalBytes = mTotalBytes;
        this.invalidateData();
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final StorageSummaryDonutPreference storageSummaryDonutPreference = (StorageSummaryDonutPreference)preference;
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(this.mContext.getResources(), this.mUsedBytes, 0);
        storageSummaryDonutPreference.setTitle(TextUtils.expandTemplate(this.mContext.getText(2131889310), new CharSequence[] { formatBytes.value, formatBytes.units }));
        storageSummaryDonutPreference.setSummary(this.mContext.getString(2131889322, new Object[] { Formatter.formatShortFileSize(this.mContext, this.mTotalBytes) }));
        storageSummaryDonutPreference.setPercent(this.mUsedBytes, this.mTotalBytes);
        storageSummaryDonutPreference.setEnabled(true);
    }
}
