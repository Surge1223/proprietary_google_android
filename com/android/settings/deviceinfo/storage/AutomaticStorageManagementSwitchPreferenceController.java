package com.android.settings.deviceinfo.storage;

import com.android.settings.deletionhelper.ActivationWarningFragment;
import android.os.SystemProperties;
import android.provider.Settings;
import com.android.settingslib.Utils;
import android.app.ActivityManager;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.widget.MasterSwitchController;
import com.android.settings.widget.MasterSwitchPreference;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.app.FragmentManager;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AutomaticStorageManagementSwitchPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, OnSwitchChangeListener, LifecycleObserver, OnResume
{
    static final String STORAGE_MANAGER_ENABLED_BY_DEFAULT_PROPERTY = "ro.storage_manager.enabled";
    private final FragmentManager mFragmentManager;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private MasterSwitchPreference mSwitch;
    private MasterSwitchController mSwitchController;
    
    public AutomaticStorageManagementSwitchPreferenceController(final Context context, final MetricsFeatureProvider mMetricsFeatureProvider, final FragmentManager mFragmentManager) {
        super(context);
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        this.mFragmentManager = mFragmentManager;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mSwitch = (MasterSwitchPreference)preferenceScreen.findPreference("toggle_asm");
    }
    
    @Override
    public String getPreferenceKey() {
        return "toggle_asm";
    }
    
    @Override
    public boolean isAvailable() {
        return ActivityManager.isLowRamDeviceStatic() ^ true;
    }
    
    @Override
    public void onResume() {
        if (!this.isAvailable()) {
            return;
        }
        this.mSwitch.setChecked(Utils.isStorageManagerEnabled(this.mContext));
        if (this.mSwitch != null) {
            (this.mSwitchController = new MasterSwitchController(this.mSwitch)).setListener((SwitchWidgetController.OnSwitchChangeListener)this);
            this.mSwitchController.startListening();
        }
    }
    
    @Override
    public boolean onSwitchToggled(final boolean b) {
        this.mMetricsFeatureProvider.action(this.mContext, 489, b);
        Settings.Secure.putInt(this.mContext.getContentResolver(), "automatic_storage_manager_enabled", (int)(b ? 1 : 0));
        boolean b2 = false;
        final boolean boolean1 = SystemProperties.getBoolean("ro.storage_manager.enabled", false);
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "automatic_storage_manager_turned_off_by_policy", 0) != 0) {
            b2 = true;
        }
        if ((b ? 1 : 0) != 0 && (!boolean1 || b2)) {
            ActivationWarningFragment.newInstance().show(this.mFragmentManager, "ActivationWarningFragment");
        }
        return true;
    }
}
