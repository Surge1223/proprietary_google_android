package com.android.settings.deviceinfo.storage;

import android.util.SparseArray;
import com.android.settings.deviceinfo.StorageProfileFragment;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.Utils;
import android.graphics.drawable.Drawable;
import com.android.internal.util.Preconditions;
import android.content.Context;
import android.content.pm.UserInfo;
import com.android.settings.deviceinfo.StorageItemPreference;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class UserProfileController extends AbstractPreferenceController implements PreferenceControllerMixin, ResultHandler, UserIconHandler
{
    private final int mPreferenceOrder;
    private StorageItemPreference mStoragePreference;
    private long mTotalSizeBytes;
    private UserInfo mUser;
    
    public UserProfileController(final Context context, final UserInfo userInfo, final int mPreferenceOrder) {
        super(context);
        this.mUser = (UserInfo)Preconditions.checkNotNull((Object)userInfo);
        this.mPreferenceOrder = mPreferenceOrder;
    }
    
    private static Drawable applyTint(final Context context, Drawable mutate) {
        mutate = mutate.mutate();
        mutate.setTint(Utils.getColorAttr(context, 16843817));
        return mutate;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        (this.mStoragePreference = new StorageItemPreference(preferenceScreen.getContext())).setOrder(this.mPreferenceOrder);
        final StorageItemPreference mStoragePreference = this.mStoragePreference;
        final StringBuilder sb = new StringBuilder();
        sb.append("pref_profile_");
        sb.append(this.mUser.id);
        mStoragePreference.setKey(sb.toString());
        this.mStoragePreference.setTitle(this.mUser.name);
        preferenceScreen.addPreference(this.mStoragePreference);
    }
    
    @Override
    public String getPreferenceKey() {
        final StringBuilder sb = new StringBuilder();
        sb.append("pref_profile_");
        sb.append(this.mUser.id);
        return sb.toString();
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (preference != null && this.mStoragePreference == preference) {
            final Bundle arguments = new Bundle();
            arguments.putInt("userId", this.mUser.id);
            arguments.putString("android.os.storage.extra.VOLUME_ID", "private");
            new SubSettingLauncher(this.mContext).setDestination(StorageProfileFragment.class.getName()).setArguments(arguments).setTitle(this.mUser.name).setSourceMetricsCategory(42).launch();
            return true;
        }
        return false;
    }
    
    @Override
    public void handleResult(final SparseArray<AppsStorageResult> sparseArray) {
        Preconditions.checkNotNull((Object)sparseArray);
        final AppsStorageResult appsStorageResult = (AppsStorageResult)sparseArray.get(this.mUser.id);
        if (appsStorageResult != null) {
            this.setSize(appsStorageResult.externalStats.totalBytes + appsStorageResult.otherAppsSize + appsStorageResult.videoAppsSize + appsStorageResult.musicAppsSize + appsStorageResult.gamesSize, this.mTotalSizeBytes);
        }
    }
    
    @Override
    public void handleUserIcons(final SparseArray<Drawable> sparseArray) {
        final Drawable drawable = (Drawable)sparseArray.get(this.mUser.id);
        if (drawable != null) {
            this.mStoragePreference.setIcon(applyTint(this.mContext, drawable));
        }
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void setSize(final long n, final long n2) {
        if (this.mStoragePreference != null) {
            this.mStoragePreference.setStorageSize(n, n2);
        }
    }
}
