package com.android.settings.deviceinfo.storage;

import android.text.TextPaint;
import android.text.style.StyleSpan;
import android.content.Intent;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.view.View;
import android.widget.Button;
import com.android.settings.widget.DonutView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;

public class StorageSummaryDonutPreference extends Preference implements View.OnClickListener
{
    private double mPercent;
    
    public StorageSummaryDonutPreference(final Context context) {
        this(context, null);
    }
    
    public StorageSummaryDonutPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPercent = -1.0;
        this.setLayoutResource(2131558765);
        this.setEnabled(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        preferenceViewHolder.itemView.setClickable(false);
        final DonutView donutView = (DonutView)preferenceViewHolder.findViewById(2131362089);
        if (donutView != null) {
            donutView.setPercentage(this.mPercent);
        }
        final Button button = (Button)preferenceViewHolder.findViewById(2131362064);
        if (button != null) {
            button.setOnClickListener((View.OnClickListener)this);
        }
    }
    
    public void onClick(final View view) {
        if (view != null && 2131362064 == view.getId()) {
            final Context context = this.getContext();
            FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 840, (Pair<Integer, Object>[])new Pair[0]);
            this.getContext().startActivity(new Intent("android.os.storage.action.MANAGE_STORAGE"));
        }
    }
    
    public void setPercent(final long n, final long n2) {
        if (n2 == 0L) {
            return;
        }
        this.mPercent = n / n2;
    }
    
    private static class BoldLinkSpan extends StyleSpan
    {
        public BoldLinkSpan() {
            super(1);
        }
        
        public void updateDrawState(final TextPaint textPaint) {
            super.updateDrawState(textPaint);
            textPaint.setColor(textPaint.linkColor);
        }
    }
}
