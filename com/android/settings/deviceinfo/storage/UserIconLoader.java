package com.android.settings.deviceinfo.storage;

import java.util.Iterator;
import com.android.settingslib.Utils;
import android.content.pm.UserInfo;
import android.os.UserManager;
import com.android.internal.util.Preconditions;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import com.android.settingslib.utils.AsyncLoader;

public class UserIconLoader extends AsyncLoader<SparseArray<Drawable>>
{
    private FetchUserIconTask mTask;
    
    public UserIconLoader(final Context context, final FetchUserIconTask fetchUserIconTask) {
        super(context);
        this.mTask = (FetchUserIconTask)Preconditions.checkNotNull((Object)fetchUserIconTask);
    }
    
    public static SparseArray<Drawable> loadUserIconsWithContext(final Context context) {
        final SparseArray sparseArray = new SparseArray();
        final UserManager userManager = (UserManager)context.getSystemService((Class)UserManager.class);
        for (final UserInfo userInfo : userManager.getUsers()) {
            sparseArray.put(userInfo.id, (Object)Utils.getUserIcon(context, userManager, userInfo));
        }
        return (SparseArray<Drawable>)sparseArray;
    }
    
    public SparseArray<Drawable> loadInBackground() {
        return this.mTask.getUserIcons();
    }
    
    @Override
    protected void onDiscardResult(final SparseArray<Drawable> sparseArray) {
    }
    
    public interface FetchUserIconTask
    {
        SparseArray<Drawable> getUserIcons();
    }
    
    public interface UserIconHandler
    {
        void handleUserIcons(final SparseArray<Drawable> p0);
    }
}
