package com.android.settings.deviceinfo.storage;

import com.android.settingslib.applications.StorageStatsSource;
import android.util.SparseArray;
import com.android.settingslib.deviceinfo.PrivateStorageInfo;
import android.provider.Settings;
import java.util.concurrent.TimeUnit;
import android.content.Context;
import android.content.SharedPreferences;

public class CachedStorageValuesHelper
{
    public static final String SHARED_PREFERENCES_NAME = "CachedStorageValues";
    private final Long mClobberThreshold;
    protected Clock mClock;
    private final SharedPreferences mSharedPreferences;
    private final int mUserId;
    
    public CachedStorageValuesHelper(final Context context, final int mUserId) {
        this.mSharedPreferences = context.getSharedPreferences("CachedStorageValues", 0);
        this.mClock = new Clock();
        this.mUserId = mUserId;
        this.mClobberThreshold = Settings.Global.getLong(context.getContentResolver(), "storage_settings_clobber_threshold", TimeUnit.MINUTES.toMillis(5L));
    }
    
    private boolean isDataValid() {
        final int int1 = this.mSharedPreferences.getInt("user_id", -1);
        final int mUserId = this.mUserId;
        boolean b = false;
        if (int1 != mUserId) {
            return false;
        }
        if (this.mClock.getCurrentTime() - this.mSharedPreferences.getLong("last_query_timestamp", Long.MAX_VALUE) < this.mClobberThreshold) {
            b = true;
        }
        return b;
    }
    
    public void cacheResult(final PrivateStorageInfo privateStorageInfo, final StorageAsyncLoader.AppsStorageResult appsStorageResult) {
        this.mSharedPreferences.edit().putLong("free_bytes", privateStorageInfo.freeBytes).putLong("total_bytes", privateStorageInfo.totalBytes).putLong("game_apps_size", appsStorageResult.gamesSize).putLong("music_apps_size", appsStorageResult.musicAppsSize).putLong("video_apps_size", appsStorageResult.videoAppsSize).putLong("photo_apps_size", appsStorageResult.photosAppsSize).putLong("other_apps_size", appsStorageResult.otherAppsSize).putLong("cache_apps_size", appsStorageResult.cacheSize).putLong("external_total_bytes", appsStorageResult.externalStats.totalBytes).putLong("external_audio_bytes", appsStorageResult.externalStats.audioBytes).putLong("external_video_bytes", appsStorageResult.externalStats.videoBytes).putLong("external_image_bytes", appsStorageResult.externalStats.imageBytes).putLong("external_apps_bytes", appsStorageResult.externalStats.appBytes).putInt("user_id", this.mUserId).putLong("last_query_timestamp", this.mClock.getCurrentTime()).apply();
    }
    
    public SparseArray<StorageAsyncLoader.AppsStorageResult> getCachedAppsStorageResult() {
        if (!this.isDataValid()) {
            return null;
        }
        final long long1 = this.mSharedPreferences.getLong("game_apps_size", -1L);
        final long long2 = this.mSharedPreferences.getLong("music_apps_size", -1L);
        final long long3 = this.mSharedPreferences.getLong("video_apps_size", -1L);
        final long long4 = this.mSharedPreferences.getLong("photo_apps_size", -1L);
        final long long5 = this.mSharedPreferences.getLong("other_apps_size", -1L);
        final long long6 = this.mSharedPreferences.getLong("cache_apps_size", -1L);
        if (long1 < 0L || long2 < 0L || long3 < 0L || long4 < 0L || long5 < 0L || long6 < 0L) {
            return null;
        }
        final long long7 = this.mSharedPreferences.getLong("external_total_bytes", -1L);
        final long long8 = this.mSharedPreferences.getLong("external_audio_bytes", -1L);
        final long long9 = this.mSharedPreferences.getLong("external_video_bytes", -1L);
        final long long10 = this.mSharedPreferences.getLong("external_image_bytes", -1L);
        final long long11 = this.mSharedPreferences.getLong("external_apps_bytes", -1L);
        if (long7 >= 0L && long8 >= 0L && long9 >= 0L && long10 >= 0L && long11 >= 0L) {
            final StorageStatsSource.ExternalStorageStats externalStats = new StorageStatsSource.ExternalStorageStats(long7, long8, long9, long10, long11);
            final StorageAsyncLoader.AppsStorageResult appsStorageResult = new StorageAsyncLoader.AppsStorageResult();
            appsStorageResult.gamesSize = long1;
            appsStorageResult.musicAppsSize = long2;
            appsStorageResult.videoAppsSize = long3;
            appsStorageResult.photosAppsSize = long4;
            appsStorageResult.otherAppsSize = long5;
            appsStorageResult.cacheSize = long6;
            appsStorageResult.externalStats = externalStats;
            final SparseArray sparseArray = new SparseArray();
            sparseArray.append(this.mUserId, (Object)appsStorageResult);
            return (SparseArray<StorageAsyncLoader.AppsStorageResult>)sparseArray;
        }
        return null;
    }
    
    public PrivateStorageInfo getCachedPrivateStorageInfo() {
        if (!this.isDataValid()) {
            return null;
        }
        final long long1 = this.mSharedPreferences.getLong("free_bytes", -1L);
        final long long2 = this.mSharedPreferences.getLong("total_bytes", -1L);
        if (long1 >= 0L && long2 >= 0L) {
            return new PrivateStorageInfo(long1, long2);
        }
        return null;
    }
    
    static class Clock
    {
        public long getCurrentTime() {
            return System.currentTimeMillis();
        }
    }
}
