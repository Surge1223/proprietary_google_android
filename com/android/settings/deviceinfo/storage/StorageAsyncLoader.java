package com.android.settings.deviceinfo.storage;

import java.util.Collections;
import android.content.pm.UserInfo;
import java.util.Comparator;
import java.util.List;
import java.io.IOException;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.UserHandle;
import android.util.Log;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.applications.StorageStatsSource;
import android.util.ArraySet;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.util.SparseArray;
import com.android.settingslib.utils.AsyncLoader;

public class StorageAsyncLoader extends AsyncLoader<SparseArray<AppsStorageResult>>
{
    private PackageManagerWrapper mPackageManager;
    private ArraySet<String> mSeenPackages;
    private StorageStatsSource mStatsManager;
    private UserManager mUserManager;
    private String mUuid;
    
    public StorageAsyncLoader(final Context context, final UserManager mUserManager, final String mUuid, final StorageStatsSource mStatsManager, final PackageManagerWrapper mPackageManager) {
        super(context);
        this.mUserManager = mUserManager;
        this.mUuid = mUuid;
        this.mStatsManager = mStatsManager;
        this.mPackageManager = mPackageManager;
    }
    
    private AppsStorageResult getStorageResultForUser(final int n) {
        Log.d("StorageAsyncLoader", "Loading apps");
        final List<ApplicationInfo> installedApplicationsAsUser = this.mPackageManager.getInstalledApplicationsAsUser(0, n);
        final AppsStorageResult appsStorageResult = new AppsStorageResult();
        final UserHandle of = UserHandle.of(n);
        for (int size = installedApplicationsAsUser.size(), i = 0; i < size; ++i) {
            final ApplicationInfo applicationInfo = installedApplicationsAsUser.get(i);
            try {
                final StorageStatsSource.AppStorageStats statsForPackage = this.mStatsManager.getStatsForPackage(this.mUuid, applicationInfo.packageName, of);
                final long dataBytes = statsForPackage.getDataBytes();
                final long cacheQuotaBytes = this.mStatsManager.getCacheQuotaBytes(this.mUuid, applicationInfo.uid);
                final long cacheBytes = statsForPackage.getCacheBytes();
                long n2 = dataBytes;
                if (cacheQuotaBytes < cacheBytes) {
                    n2 = dataBytes - cacheBytes + cacheQuotaBytes;
                }
                long n3 = n2;
                if (!this.mSeenPackages.contains((Object)applicationInfo.packageName)) {
                    n3 = n2 + statsForPackage.getCodeBytes();
                    this.mSeenPackages.add((Object)applicationInfo.packageName);
                }
                switch (applicationInfo.category) {
                    default: {
                        if ((applicationInfo.flags & 0x2000000) != 0x0) {
                            appsStorageResult.gamesSize += n3;
                            break;
                        }
                        appsStorageResult.otherAppsSize += n3;
                        break;
                    }
                    case 3: {
                        appsStorageResult.photosAppsSize += n3;
                        break;
                    }
                    case 2: {
                        appsStorageResult.videoAppsSize += n3;
                        break;
                    }
                    case 1: {
                        appsStorageResult.musicAppsSize += n3;
                        break;
                    }
                    case 0: {
                        appsStorageResult.gamesSize += n3;
                        break;
                    }
                }
            }
            catch (PackageManager$NameNotFoundException | IOException ex2) {
                final Throwable t;
                Log.w("StorageAsyncLoader", "App unexpectedly not found", t);
            }
        }
        Log.d("StorageAsyncLoader", "Loading external stats");
        try {
            appsStorageResult.externalStats = this.mStatsManager.getExternalStorageStats(this.mUuid, UserHandle.of(n));
        }
        catch (IOException ex) {
            Log.w("StorageAsyncLoader", (Throwable)ex);
        }
        Log.d("StorageAsyncLoader", "Obtaining result completed");
        return appsStorageResult;
    }
    
    private SparseArray<AppsStorageResult> loadApps() {
        this.mSeenPackages = (ArraySet<String>)new ArraySet();
        final SparseArray sparseArray = new SparseArray();
        final List users = this.mUserManager.getUsers();
        Collections.sort((List<Object>)users, (Comparator<? super Object>)new Comparator<UserInfo>() {
            @Override
            public int compare(final UserInfo userInfo, final UserInfo userInfo2) {
                return Integer.compare(userInfo.id, userInfo2.id);
            }
        });
        for (int i = 0; i < users.size(); ++i) {
            final UserInfo userInfo = users.get(i);
            sparseArray.put(userInfo.id, (Object)this.getStorageResultForUser(userInfo.id));
        }
        return (SparseArray<AppsStorageResult>)sparseArray;
    }
    
    public SparseArray<AppsStorageResult> loadInBackground() {
        return this.loadApps();
    }
    
    @Override
    protected void onDiscardResult(final SparseArray<AppsStorageResult> sparseArray) {
    }
    
    public static class AppsStorageResult
    {
        public long cacheSize;
        public StorageStatsSource.ExternalStorageStats externalStats;
        public long gamesSize;
        public long musicAppsSize;
        public long otherAppsSize;
        public long photosAppsSize;
        public long videoAppsSize;
    }
    
    public interface ResultHandler
    {
        void handleResult(final SparseArray<AppsStorageResult> p0);
    }
}
