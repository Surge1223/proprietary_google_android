package com.android.settings.deviceinfo.storage;

import java.io.IOException;
import android.content.Context;
import com.android.settingslib.deviceinfo.StorageVolumeProvider;
import android.os.storage.VolumeInfo;
import android.app.usage.StorageStatsManager;
import com.android.settingslib.deviceinfo.PrivateStorageInfo;
import com.android.settingslib.utils.AsyncLoader;

public class VolumeSizesLoader extends AsyncLoader<PrivateStorageInfo>
{
    private StorageStatsManager mStats;
    private VolumeInfo mVolume;
    private StorageVolumeProvider mVolumeProvider;
    
    public VolumeSizesLoader(final Context context, final StorageVolumeProvider mVolumeProvider, final StorageStatsManager mStats, final VolumeInfo mVolume) {
        super(context);
        this.mVolumeProvider = mVolumeProvider;
        this.mStats = mStats;
        this.mVolume = mVolume;
    }
    
    static PrivateStorageInfo getVolumeSize(final StorageVolumeProvider storageVolumeProvider, final StorageStatsManager storageStatsManager, final VolumeInfo volumeInfo) throws IOException {
        return new PrivateStorageInfo(storageVolumeProvider.getFreeBytes(storageStatsManager, volumeInfo), storageVolumeProvider.getTotalBytes(storageStatsManager, volumeInfo));
    }
    
    public PrivateStorageInfo loadInBackground() {
        try {
            return getVolumeSize(this.mVolumeProvider, this.mStats, this.mVolume);
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    protected void onDiscardResult(final PrivateStorageInfo privateStorageInfo) {
    }
}
