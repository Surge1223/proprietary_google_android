package com.android.settings.deviceinfo.storage;

import android.util.SparseArray;
import android.util.Pair;
import com.android.settings.deviceinfo.PrivateVolumeSettings;
import android.support.v7.preference.Preference;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.os.Bundle;
import com.android.settings.applications.manageapplications.ManageApplications;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.Settings;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.storage.VolumeInfo;
import com.android.settingslib.deviceinfo.StorageVolumeProvider;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.app.Fragment;
import com.android.settings.deviceinfo.StorageItemPreference;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class StorageItemPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    static final String AUDIO_KEY = "pref_music_audio";
    static final String FILES_KEY = "pref_files";
    static final String GAME_KEY = "pref_games";
    static final String MOVIES_KEY = "pref_movies";
    static final String OTHER_APPS_KEY = "pref_other_apps";
    static final String PHOTO_KEY = "pref_photos_videos";
    static final String SYSTEM_KEY = "pref_system";
    private StorageItemPreference mAppPreference;
    private StorageItemPreference mAudioPreference;
    private StorageItemPreference mFilePreference;
    private final Fragment mFragment;
    private StorageItemPreference mGamePreference;
    private boolean mIsWorkProfile;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private StorageItemPreference mMoviesPreference;
    private StorageItemPreference mPhotoPreference;
    private PreferenceScreen mScreen;
    private final StorageVolumeProvider mSvp;
    private StorageItemPreference mSystemPreference;
    private long mTotalSize;
    private long mUsedBytes;
    private int mUserId;
    private VolumeInfo mVolume;
    
    public StorageItemPreferenceController(final Context context, final Fragment mFragment, final VolumeInfo mVolume, final StorageVolumeProvider mSvp) {
        super(context);
        this.mFragment = mFragment;
        this.mVolume = mVolume;
        this.mSvp = mSvp;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mUserId = UserHandle.myUserId();
    }
    
    public StorageItemPreferenceController(final Context context, final Fragment fragment, final VolumeInfo volumeInfo, final StorageVolumeProvider storageVolumeProvider, final boolean mIsWorkProfile) {
        this(context, fragment, volumeInfo, storageVolumeProvider);
        this.mIsWorkProfile = mIsWorkProfile;
    }
    
    private static Drawable applyTint(final Context context, Drawable mutate) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { 16843817 });
        mutate = mutate.mutate();
        mutate.setTint(obtainStyledAttributes.getColor(0, 0));
        obtainStyledAttributes.recycle();
        return mutate;
    }
    
    private Intent getAppsIntent() {
        if (this.mVolume == null) {
            return null;
        }
        final Bundle workAnnotatedBundle = this.getWorkAnnotatedBundle(3);
        workAnnotatedBundle.putString("classname", Settings.StorageUseActivity.class.getName());
        workAnnotatedBundle.putString("volumeUuid", this.mVolume.getFsUuid());
        workAnnotatedBundle.putString("volumeName", this.mVolume.getDescription());
        return new SubSettingLauncher(this.mContext).setDestination(ManageApplications.class.getName()).setTitle(2131886412).setArguments(workAnnotatedBundle).setSourceMetricsCategory(this.mMetricsFeatureProvider.getMetricsCategory(this.mFragment)).toIntent();
    }
    
    private Intent getAudioIntent() {
        if (this.mVolume == null) {
            return null;
        }
        final Bundle workAnnotatedBundle = this.getWorkAnnotatedBundle(4);
        workAnnotatedBundle.putString("classname", Settings.StorageUseActivity.class.getName());
        workAnnotatedBundle.putString("volumeUuid", this.mVolume.getFsUuid());
        workAnnotatedBundle.putString("volumeName", this.mVolume.getDescription());
        workAnnotatedBundle.putInt("storageType", 1);
        return new SubSettingLauncher(this.mContext).setDestination(ManageApplications.class.getName()).setTitle(2131889300).setArguments(workAnnotatedBundle).setSourceMetricsCategory(this.mMetricsFeatureProvider.getMetricsCategory(this.mFragment)).toIntent();
    }
    
    private Intent getFilesIntent() {
        return this.mSvp.findEmulatedForPrivate(this.mVolume).buildBrowseIntent();
    }
    
    private Intent getGamesIntent() {
        final Bundle workAnnotatedBundle = this.getWorkAnnotatedBundle(1);
        workAnnotatedBundle.putString("classname", Settings.GamesStorageActivity.class.getName());
        return new SubSettingLauncher(this.mContext).setDestination(ManageApplications.class.getName()).setTitle(2131887720).setArguments(workAnnotatedBundle).setSourceMetricsCategory(this.mMetricsFeatureProvider.getMetricsCategory(this.mFragment)).toIntent();
    }
    
    private Intent getMoviesIntent() {
        final Bundle workAnnotatedBundle = this.getWorkAnnotatedBundle(1);
        workAnnotatedBundle.putString("classname", Settings.MoviesStorageActivity.class.getName());
        return new SubSettingLauncher(this.mContext).setDestination(ManageApplications.class.getName()).setTitle(2131889299).setArguments(workAnnotatedBundle).setSourceMetricsCategory(this.mMetricsFeatureProvider.getMetricsCategory(this.mFragment)).toIntent();
    }
    
    private Intent getPhotosIntent() {
        final Bundle workAnnotatedBundle = this.getWorkAnnotatedBundle(2);
        workAnnotatedBundle.putString("classname", Settings.PhotosStorageActivity.class.getName());
        workAnnotatedBundle.putInt("storageType", 3);
        return new SubSettingLauncher(this.mContext).setDestination(ManageApplications.class.getName()).setTitle(2131889304).setArguments(workAnnotatedBundle).setSourceMetricsCategory(this.mMetricsFeatureProvider.getMetricsCategory(this.mFragment)).toIntent();
    }
    
    private Bundle getWorkAnnotatedBundle(final int n) {
        final Bundle bundle = new Bundle(2 + n);
        bundle.putBoolean("workProfileOnly", this.mIsWorkProfile);
        bundle.putInt("workId", this.mUserId);
        return bundle;
    }
    
    private void launchIntent(final Intent intent) {
        try {
            final int intExtra = intent.getIntExtra("android.intent.extra.USER_ID", -1);
            if (intExtra == -1) {
                this.mFragment.startActivity(intent);
            }
            else {
                this.mFragment.getActivity().startActivityAsUser(intent, new UserHandle(intExtra));
            }
        }
        catch (ActivityNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No activity found for ");
            sb.append(intent);
            Log.w("StorageItemPreference", sb.toString());
        }
    }
    
    private void setFilesPreferenceVisibility() {
        if (this.mScreen != null) {
            final VolumeInfo emulatedForPrivate = this.mSvp.findEmulatedForPrivate(this.mVolume);
            if (emulatedForPrivate == null || !emulatedForPrivate.isMountedReadable()) {
                this.mScreen.removePreference(this.mFilePreference);
            }
            else {
                this.mScreen.addPreference(this.mFilePreference);
            }
        }
    }
    
    private void tintPreference(final Preference preference) {
        if (preference != null) {
            preference.setIcon(applyTint(this.mContext, preference.getIcon()));
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        this.mScreen = mScreen;
        this.mPhotoPreference = (StorageItemPreference)mScreen.findPreference("pref_photos_videos");
        this.mAudioPreference = (StorageItemPreference)mScreen.findPreference("pref_music_audio");
        this.mGamePreference = (StorageItemPreference)mScreen.findPreference("pref_games");
        this.mMoviesPreference = (StorageItemPreference)mScreen.findPreference("pref_movies");
        this.mAppPreference = (StorageItemPreference)mScreen.findPreference("pref_other_apps");
        this.mSystemPreference = (StorageItemPreference)mScreen.findPreference("pref_system");
        this.mFilePreference = (StorageItemPreference)mScreen.findPreference("pref_files");
        this.setFilesPreferenceVisibility();
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (preference == null) {
            return false;
        }
        Intent intent = null;
        if (preference.getKey() == null) {
            return false;
        }
        final String key = preference.getKey();
        int n = 0;
        Label_0204: {
            switch (key.hashCode()) {
                case 1161100765: {
                    if (key.equals("pref_other_apps")) {
                        n = 4;
                        break Label_0204;
                    }
                    break;
                }
                case 1007071179: {
                    if (key.equals("pref_system")) {
                        n = 6;
                        break Label_0204;
                    }
                    break;
                }
                case 826139871: {
                    if (key.equals("pref_movies")) {
                        n = 3;
                        break Label_0204;
                    }
                    break;
                }
                case 283435296: {
                    if (key.equals("pref_music_audio")) {
                        n = 1;
                        break Label_0204;
                    }
                    break;
                }
                case -1488779334: {
                    if (key.equals("pref_photos_videos")) {
                        n = 0;
                        break Label_0204;
                    }
                    break;
                }
                case -1641885275: {
                    if (key.equals("pref_games")) {
                        n = 2;
                        break Label_0204;
                    }
                    break;
                }
                case -1642571429: {
                    if (key.equals("pref_files")) {
                        n = 5;
                        break Label_0204;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            case 6: {
                final PrivateVolumeSettings.SystemInfoFragment systemInfoFragment = new PrivateVolumeSettings.SystemInfoFragment();
                systemInfoFragment.setTargetFragment(this.mFragment, 0);
                systemInfoFragment.show(this.mFragment.getFragmentManager(), "SystemInfo");
                return true;
            }
            case 5: {
                intent = this.getFilesIntent();
                FeatureFactory.getFactory(this.mContext).getMetricsFeatureProvider().action(this.mContext, 841, (Pair<Integer, Object>[])new Pair[0]);
                break;
            }
            case 4: {
                if (this.mVolume == null) {
                    break;
                }
                intent = this.getAppsIntent();
                break;
            }
            case 3: {
                intent = this.getMoviesIntent();
                break;
            }
            case 2: {
                intent = this.getGamesIntent();
                break;
            }
            case 1: {
                intent = this.getAudioIntent();
                break;
            }
            case 0: {
                intent = this.getPhotosIntent();
                break;
            }
        }
        if (intent != null) {
            intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
            this.launchIntent(intent);
            return true;
        }
        return super.handlePreferenceTreeClick(preference);
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void onLoadFinished(final SparseArray<StorageAsyncLoader.AppsStorageResult> sparseArray, int i) {
        final StorageAsyncLoader.AppsStorageResult appsStorageResult = (StorageAsyncLoader.AppsStorageResult)sparseArray.get(i);
        this.mPhotoPreference.setStorageSize(appsStorageResult.photosAppsSize + appsStorageResult.externalStats.imageBytes + appsStorageResult.externalStats.videoBytes, this.mTotalSize);
        this.mAudioPreference.setStorageSize(appsStorageResult.musicAppsSize + appsStorageResult.externalStats.audioBytes, this.mTotalSize);
        this.mGamePreference.setStorageSize(appsStorageResult.gamesSize, this.mTotalSize);
        this.mMoviesPreference.setStorageSize(appsStorageResult.videoAppsSize, this.mTotalSize);
        this.mAppPreference.setStorageSize(appsStorageResult.otherAppsSize, this.mTotalSize);
        this.mFilePreference.setStorageSize(appsStorageResult.externalStats.totalBytes - appsStorageResult.externalStats.audioBytes - appsStorageResult.externalStats.videoBytes - appsStorageResult.externalStats.imageBytes - appsStorageResult.externalStats.appBytes, this.mTotalSize);
        if (this.mSystemPreference != null) {
            long n = 0L;
            StorageAsyncLoader.AppsStorageResult appsStorageResult2;
            for (i = 0; i < sparseArray.size(); ++i) {
                appsStorageResult2 = (StorageAsyncLoader.AppsStorageResult)sparseArray.valueAt(i);
                n = n + (appsStorageResult2.gamesSize + appsStorageResult2.musicAppsSize + appsStorageResult2.videoAppsSize + appsStorageResult2.photosAppsSize + appsStorageResult2.otherAppsSize) + (appsStorageResult2.externalStats.totalBytes - appsStorageResult2.externalStats.appBytes);
            }
            this.mSystemPreference.setStorageSize(Math.max(1073741824L, this.mUsedBytes - n), this.mTotalSize);
        }
    }
    
    public void setTotalSize(final long mTotalSize) {
        this.mTotalSize = mTotalSize;
    }
    
    public void setUsedSize(final long mUsedBytes) {
        this.mUsedBytes = mUsedBytes;
    }
    
    public void setUserId(final UserHandle userHandle) {
        this.mUserId = userHandle.getIdentifier();
        this.tintPreference(this.mPhotoPreference);
        this.tintPreference(this.mMoviesPreference);
        this.tintPreference(this.mAudioPreference);
        this.tintPreference(this.mGamePreference);
        this.tintPreference(this.mAppPreference);
        this.tintPreference(this.mSystemPreference);
        this.tintPreference(this.mFilePreference);
    }
    
    public void setVolume(final VolumeInfo mVolume) {
        this.mVolume = mVolume;
        this.setFilesPreferenceVisibility();
    }
}
