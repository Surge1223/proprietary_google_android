package com.android.settings.deviceinfo;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import java.text.NumberFormat;
import android.util.Log;
import android.widget.Toast;
import android.os.AsyncTask;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.text.format.Formatter$BytesResult;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.os.storage.VolumeRecord;
import java.util.Comparator;
import java.util.Collections;
import com.android.settingslib.deviceinfo.StorageVolumeProvider;
import com.android.settingslib.deviceinfo.PrivateStorageInfo;
import com.android.settingslib.deviceinfo.StorageManagerVolumeProvider;
import android.support.v7.preference.Preference;
import android.content.Intent;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.os.storage.DiskInfo;
import java.util.Iterator;
import android.os.storage.VolumeInfo;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.graphics.Color;
import android.os.storage.StorageManager;
import android.os.storage.StorageEventListener;
import android.support.v7.preference.PreferenceCategory;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$StorageSettings$pjFUgWj2HWW95DLVydfI8EgfTdg implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return StorageSettings.lambda$static$0(activity, summaryLoader);
    }
}
