package com.android.settings.deviceinfo;

import android.os.storage.VolumeInfo;
import android.util.Log;
import android.os.storage.StorageManager;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class StorageUnmountReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final StorageManager storageManager = (StorageManager)context.getSystemService((Class)StorageManager.class);
        final String stringExtra = intent.getStringExtra("android.os.storage.extra.VOLUME_ID");
        final VolumeInfo volumeById = storageManager.findVolumeById(stringExtra);
        if (volumeById != null) {
            new StorageSettings.UnmountTask(context, volumeById).execute((Object[])new Void[0]);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Missing volume ");
            sb.append(stringExtra);
            Log.w("StorageSettings", sb.toString());
        }
    }
}
