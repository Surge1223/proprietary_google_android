package com.android.settings.deviceinfo;

import android.support.v7.preference.PreferenceScreen;
import com.android.settings.accounts.AccountDetailDashboardFragment;
import com.android.settings.core.SubSettingLauncher;
import android.os.Process;
import android.os.Parcelable;
import android.os.Bundle;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.accounts.Account;
import com.android.settings.core.BasePreferenceController;
import com.android.settings.accounts.AccountFeatureProvider;
import android.support.v7.preference.Preference;

public final class _$$Lambda$BrandedAccountPreferenceController$rFwl4JPEzufcbKCkFgByL5d4NMI implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return BrandedAccountPreferenceController.lambda$displayPreference$0(this.f$0, this.f$1, preference);
    }
}
