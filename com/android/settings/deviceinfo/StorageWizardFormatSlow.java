package com.android.settings.deviceinfo;

import android.os.storage.VolumeInfo;
import android.text.TextUtils;
import android.content.Intent;
import android.util.Pair;
import android.content.Context;
import com.android.settings.overlay.FeatureFactory;
import android.view.View;
import android.os.Bundle;

public class StorageWizardFormatSlow extends StorageWizardBase
{
    private boolean mFormatPrivate;
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mDisk == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558769);
        this.mFormatPrivate = this.getIntent().getBooleanExtra("format_private", false);
        this.setHeaderText(2131889383, this.getDiskShortDescription());
        this.setBodyText(2131889380, this.getDiskDescription(), this.getDiskShortDescription(), this.getDiskShortDescription(), this.getDiskShortDescription());
        this.setBackButtonText(2131889382, new CharSequence[0]);
        this.setNextButtonText(2131889381, new CharSequence[0]);
        if (!this.getIntent().getBooleanExtra("format_slow", false)) {
            this.onNavigateNext(null);
        }
    }
    
    @Override
    public void onNavigateBack(final View view) {
        FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1411, (Pair<Integer, Object>[])new Pair[0]);
        this.startActivity(new Intent((Context)this, (Class)StorageWizardInit.class));
        this.finishAffinity();
    }
    
    @Override
    public void onNavigateNext(final View view) {
        boolean b = false;
        final boolean b2 = false;
        if (view != null) {
            FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1410, (Pair<Integer, Object>[])new Pair[0]);
        }
        else {
            FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 1409, (Pair<Integer, Object>[])new Pair[0]);
        }
        final String stringExtra = this.getIntent().getStringExtra("format_forget_uuid");
        if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
            this.mStorage.forgetVolume(stringExtra);
        }
        if (this.mFormatPrivate) {
            final VolumeInfo primaryStorageCurrentVolume = this.getPackageManager().getPrimaryStorageCurrentVolume();
            b = b2;
            if (primaryStorageCurrentVolume != null) {
                b = b2;
                if ("private".equals(primaryStorageCurrentVolume.getId())) {
                    b = true;
                }
            }
        }
        if (b) {
            final Intent intent = new Intent((Context)this, (Class)StorageWizardMigrateConfirm.class);
            intent.putExtra("android.os.storage.extra.DISK_ID", this.mDisk.getId());
            this.startActivity(intent);
        }
        else {
            final Intent intent2 = new Intent((Context)this, (Class)StorageWizardReady.class);
            intent2.putExtra("android.os.storage.extra.DISK_ID", this.mDisk.getId());
            this.startActivity(intent2);
        }
        this.finishAffinity();
    }
}
