package com.android.settings.deviceinfo;

import android.os.storage.DiskInfo;
import android.text.TextUtils;
import android.app.AlertDialog$Builder;
import android.os.storage.StorageManager;
import android.app.Dialog;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.Context;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$StorageWizardFormatConfirm$c4jIKjriuaEtVR7ERojcHILapk8 implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        StorageWizardFormatConfirm.lambda$onCreateDialog$0(this.f$0, this.f$1, this.f$2, this.f$3, dialogInterface, n);
    }
}
