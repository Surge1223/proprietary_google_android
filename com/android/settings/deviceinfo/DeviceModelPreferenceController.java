package com.android.settings.deviceinfo;

import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.DeviceInfoUtils;
import android.os.Build;
import android.content.Context;
import android.app.Fragment;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class DeviceModelPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final Fragment mHost;
    
    public DeviceModelPreferenceController(final Context context, final Fragment mHost) {
        super(context);
        this.mHost = mHost;
    }
    
    public static String getDeviceModel() {
        final StringBuilder sb = new StringBuilder();
        sb.append(Build.MODEL);
        sb.append(DeviceInfoUtils.getMsvSuffix());
        return sb.toString();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("device_model");
        if (preference != null) {
            preference.setSummary(this.mContext.getResources().getString(2131888324, new Object[] { getDeviceModel() }));
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "device_model";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"device_model")) {
            return false;
        }
        HardwareInfoDialogFragment.newInstance().show(this.mHost.getFragmentManager(), "HardwareInfo");
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034142);
    }
}
