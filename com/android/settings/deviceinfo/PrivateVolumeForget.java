package com.android.settings.deviceinfo;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.os.storage.StorageManager;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.app.Fragment;
import android.view.View;
import android.os.storage.VolumeRecord;
import android.view.View.OnClickListener;
import com.android.settings.SettingsPreferenceFragment;

public class PrivateVolumeForget extends SettingsPreferenceFragment
{
    private final View.OnClickListener mConfirmListener;
    private VolumeRecord mRecord;
    
    public PrivateVolumeForget() {
        this.mConfirmListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ForgetConfirmFragment.show(PrivateVolumeForget.this, PrivateVolumeForget.this.mRecord.getFsUuid());
            }
        };
    }
    
    @Override
    public int getMetricsCategory() {
        return 42;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final StorageManager storageManager = (StorageManager)this.getActivity().getSystemService((Class)StorageManager.class);
        final String string = this.getArguments().getString("android.os.storage.extra.FS_UUID");
        if (string == null) {
            this.getActivity().finish();
            return null;
        }
        this.mRecord = storageManager.findRecordByUuid(string);
        if (this.mRecord == null) {
            this.getActivity().finish();
            return null;
        }
        final View inflate = layoutInflater.inflate(2131558760, viewGroup, false);
        final TextView textView = (TextView)inflate.findViewById(2131361913);
        final Button button = (Button)inflate.findViewById(2131362002);
        textView.setText(TextUtils.expandTemplate(this.getText(2131889275), new CharSequence[] { this.mRecord.getNickname() }));
        button.setOnClickListener(this.mConfirmListener);
        return inflate;
    }
    
    public static class ForgetConfirmFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment, final String s) {
            final Bundle arguments = new Bundle();
            arguments.putString("android.os.storage.extra.FS_UUID", s);
            final ForgetConfirmFragment forgetConfirmFragment = new ForgetConfirmFragment();
            forgetConfirmFragment.setArguments(arguments);
            forgetConfirmFragment.setTargetFragment(fragment, 0);
            forgetConfirmFragment.show(fragment.getFragmentManager(), "forget_confirm");
        }
        
        @Override
        public int getMetricsCategory() {
            return 559;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Activity activity = this.getActivity();
            final StorageManager storageManager = (StorageManager)((Context)activity).getSystemService((Class)StorageManager.class);
            final String string = this.getArguments().getString("android.os.storage.extra.FS_UUID");
            final VolumeRecord recordByUuid = storageManager.findRecordByUuid(string);
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
            alertDialog$Builder.setTitle(TextUtils.expandTemplate(this.getText(2131889274), new CharSequence[] { recordByUuid.getNickname() }));
            alertDialog$Builder.setMessage(TextUtils.expandTemplate(this.getText(2131889273), new CharSequence[] { recordByUuid.getNickname() }));
            alertDialog$Builder.setPositiveButton(2131889286, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    storageManager.forgetVolume(string);
                    ForgetConfirmFragment.this.getActivity().finish();
                }
            });
            alertDialog$Builder.setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
            return (Dialog)alertDialog$Builder.create();
        }
    }
}
