package com.android.settings.deviceinfo;

import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.deviceinfo.AbstractIpAddressPreferenceController;

public class IpAddressPreferenceController extends AbstractIpAddressPreferenceController implements PreferenceControllerMixin
{
    public IpAddressPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, lifecycle);
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034174);
    }
}
