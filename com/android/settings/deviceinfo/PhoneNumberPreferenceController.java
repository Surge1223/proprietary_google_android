package com.android.settings.deviceinfo;

import java.util.Iterator;
import android.text.TextDirectionHeuristics;
import android.text.BidiFormatter;
import android.text.TextUtils;
import com.android.settingslib.DeviceInfoUtils;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.SubscriptionInfo;
import java.util.ArrayList;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.support.v7.preference.Preference;
import java.util.List;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class PhoneNumberPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final List<Preference> mPreferenceList;
    private final SubscriptionManager mSubscriptionManager;
    private final TelephonyManager mTelephonyManager;
    
    public PhoneNumberPreferenceController(final Context context) {
        super(context);
        this.mPreferenceList = new ArrayList<Preference>();
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
        this.mSubscriptionManager = (SubscriptionManager)context.getSystemService("telephony_subscription_service");
    }
    
    private CharSequence getPhoneNumber(final int n) {
        final SubscriptionInfo subscriptionInfo = this.getSubscriptionInfo(n);
        if (subscriptionInfo == null) {
            return this.mContext.getString(2131887389);
        }
        return this.getFormattedPhoneNumber(subscriptionInfo);
    }
    
    private CharSequence getPreferenceTitle(final int n) {
        String s;
        if (this.mTelephonyManager.getPhoneCount() > 1) {
            s = this.mContext.getString(2131889240, new Object[] { n + 1 });
        }
        else {
            s = this.mContext.getString(2131889239);
        }
        return s;
    }
    
    Preference createNewPreference(final Context context) {
        return new Preference(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        this.mPreferenceList.add(preference);
        final int order = preference.getOrder();
        for (int i = 1; i < this.mTelephonyManager.getPhoneCount(); ++i) {
            final Preference newPreference = this.createNewPreference(preferenceScreen.getContext());
            newPreference.setOrder(order + i);
            final StringBuilder sb = new StringBuilder();
            sb.append("phone_number");
            sb.append(i);
            newPreference.setKey(sb.toString());
            preferenceScreen.addPreference(newPreference);
            this.mPreferenceList.add(newPreference);
        }
    }
    
    CharSequence getFormattedPhoneNumber(final SubscriptionInfo subscriptionInfo) {
        final String formattedPhoneNumber = DeviceInfoUtils.getFormattedPhoneNumber(this.mContext, subscriptionInfo);
        String s;
        if (TextUtils.isEmpty((CharSequence)formattedPhoneNumber)) {
            s = this.mContext.getString(2131887389);
        }
        else {
            s = BidiFormatter.getInstance().unicodeWrap(formattedPhoneNumber, TextDirectionHeuristics.LTR);
        }
        return s;
    }
    
    @Override
    public String getPreferenceKey() {
        return "phone_number";
    }
    
    SubscriptionInfo getSubscriptionInfo(final int n) {
        final List activeSubscriptionInfoList = this.mSubscriptionManager.getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList != null) {
            for (final SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                if (subscriptionInfo.getSimSlotIndex() == n) {
                    return subscriptionInfo;
                }
            }
        }
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mTelephonyManager.isVoiceCapable();
    }
    
    @Override
    public void updateState(Preference preference) {
        for (int i = 0; i < this.mPreferenceList.size(); ++i) {
            preference = this.mPreferenceList.get(i);
            preference.setTitle(this.getPreferenceTitle(i));
            preference.setSummary(this.getPhoneNumber(i));
        }
    }
}
