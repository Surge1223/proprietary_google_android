package com.android.settings.deviceinfo;

import android.os.Handler;
import android.os.Bundle;
import android.content.Context;
import android.widget.Toast;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$MoveCallback;

public class StorageWizardMoveProgress extends StorageWizardBase
{
    private final PackageManager$MoveCallback mCallback;
    private int mMoveId;
    
    public StorageWizardMoveProgress() {
        this.mCallback = new PackageManager$MoveCallback() {
            public void onStatusChanged(final int n, final int currentProgress, final long n2) {
                if (StorageWizardMoveProgress.this.mMoveId != n) {
                    return;
                }
                if (PackageManager.isMoveStatusFinished(currentProgress)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Finished with status ");
                    sb.append(currentProgress);
                    Log.d("StorageSettings", sb.toString());
                    if (currentProgress != -100) {
                        Toast.makeText((Context)StorageWizardMoveProgress.this, StorageWizardMoveProgress.this.moveStatusToMessage(currentProgress), 1).show();
                    }
                    StorageWizardMoveProgress.this.finishAffinity();
                }
                else {
                    StorageWizardMoveProgress.this.setCurrentProgress(currentProgress);
                }
            }
        };
    }
    
    private CharSequence moveStatusToMessage(final int n) {
        if (n == -8) {
            return this.getString(2131888330);
        }
        switch (n) {
            default: {
                return this.getString(2131887900);
            }
            case -1: {
                return this.getString(2131887900);
            }
            case -2: {
                return this.getString(2131887486);
            }
            case -3: {
                return this.getString(2131889445);
            }
            case -4: {
                return this.getString(2131886352);
            }
            case -5: {
                return this.getString(2131887911);
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mVolume == null) {
            this.finish();
            return;
        }
        this.setContentView(2131558772);
        this.mMoveId = this.getIntent().getIntExtra("android.content.pm.extra.MOVE_ID", -1);
        final String stringExtra = this.getIntent().getStringExtra("android.intent.extra.TITLE");
        final String bestVolumeDescription = this.mStorage.getBestVolumeDescription(this.mVolume);
        this.setIcon(2131231166);
        this.setHeaderText(2131889371, stringExtra);
        this.setBodyText(2131889369, bestVolumeDescription, stringExtra);
        this.getPackageManager().registerMoveCallback(this.mCallback, new Handler());
        this.mCallback.onStatusChanged(this.mMoveId, this.getPackageManager().getMoveStatus(this.mMoveId), -1L);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.getPackageManager().unregisterMoveCallback(this.mCallback);
    }
}
