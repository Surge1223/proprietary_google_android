package com.android.settings.deviceinfo.simstatus;

import android.support.v7.preference.PreferenceScreen;
import java.util.Iterator;
import android.telephony.SubscriptionInfo;
import java.util.ArrayList;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.support.v7.preference.Preference;
import java.util.List;
import android.app.Fragment;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.deviceinfo.AbstractSimStatusImeiInfoPreferenceController;

public class SimStatusPreferenceController extends AbstractSimStatusImeiInfoPreferenceController implements PreferenceControllerMixin
{
    private final Fragment mFragment;
    private final List<Preference> mPreferenceList;
    private final SubscriptionManager mSubscriptionManager;
    private final TelephonyManager mTelephonyManager;
    
    public SimStatusPreferenceController(final Context context, final Fragment mFragment) {
        super(context);
        this.mPreferenceList = new ArrayList<Preference>();
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
        this.mSubscriptionManager = (SubscriptionManager)context.getSystemService("telephony_subscription_service");
        this.mFragment = mFragment;
    }
    
    private CharSequence getCarrierName(final int n) {
        final List activeSubscriptionInfoList = this.mSubscriptionManager.getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList != null) {
            for (final SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                if (subscriptionInfo.getSimSlotIndex() == n) {
                    return subscriptionInfo.getCarrierName();
                }
            }
        }
        return this.mContext.getText(2131887391);
    }
    
    private String getPreferenceTitle(final int n) {
        String s;
        if (this.mTelephonyManager.getPhoneCount() > 1) {
            s = this.mContext.getString(2131889172, new Object[] { n + 1 });
        }
        else {
            s = this.mContext.getString(2131889171);
        }
        return s;
    }
    
    Preference createNewPreference(final Context context) {
        return new Preference(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        if (this.isAvailable() && preference != null && preference.isVisible()) {
            this.mPreferenceList.add(preference);
            final int order = preference.getOrder();
            for (int i = 1; i < this.mTelephonyManager.getPhoneCount(); ++i) {
                final Preference newPreference = this.createNewPreference(preferenceScreen.getContext());
                newPreference.setOrder(order + i);
                final StringBuilder sb = new StringBuilder();
                sb.append("sim_status");
                sb.append(i);
                newPreference.setKey(sb.toString());
                preferenceScreen.addPreference(newPreference);
                this.mPreferenceList.add(newPreference);
            }
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "sim_status";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        final int index = this.mPreferenceList.indexOf(preference);
        if (index == -1) {
            return false;
        }
        SimStatusDialogFragment.show(this.mFragment, index, this.getPreferenceTitle(index));
        return true;
    }
    
    @Override
    public void updateState(Preference preference) {
        for (int i = 0; i < this.mPreferenceList.size(); ++i) {
            preference = this.mPreferenceList.get(i);
            preference.setTitle(this.getPreferenceTitle(i));
            preference.setSummary(this.getCarrierName(i));
        }
    }
}
