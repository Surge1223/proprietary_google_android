package com.android.settings.deviceinfo.simstatus;

import android.text.TextUtils;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.View;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class SimStatusDialogFragment extends InstrumentedDialogFragment
{
    private SimStatusDialogController mController;
    private View mRootView;
    
    public static void show(final Fragment fragment, final int n, final String s) {
        final FragmentManager childFragmentManager = fragment.getChildFragmentManager();
        if (childFragmentManager.findFragmentByTag("SimStatusDialog") == null) {
            final Bundle arguments = new Bundle();
            arguments.putInt("arg_key_sim_slot", n);
            arguments.putString("arg_key_dialog_title", s);
            final SimStatusDialogFragment simStatusDialogFragment = new SimStatusDialogFragment();
            simStatusDialogFragment.setArguments(arguments);
            simStatusDialogFragment.show(childFragmentManager, "SimStatusDialog");
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 1246;
    }
    
    public Dialog onCreateDialog(Bundle arguments) {
        arguments = this.getArguments();
        final int int1 = arguments.getInt("arg_key_sim_slot");
        final String string = arguments.getString("arg_key_dialog_title");
        this.mController = new SimStatusDialogController(this, this.mLifecycle, int1);
        final AlertDialog$Builder setPositiveButton = new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)string).setPositiveButton(17039370, (DialogInterface$OnClickListener)null);
        this.mRootView = LayoutInflater.from(setPositiveButton.getContext()).inflate(2131558544, (ViewGroup)null);
        this.mController.initialize();
        return (Dialog)setPositiveButton.setView(this.mRootView).create();
    }
    
    public void removeSettingFromScreen(final int n) {
        final View viewById = this.mRootView.findViewById(n);
        if (viewById != null) {
            viewById.setVisibility(8);
        }
    }
    
    public void setText(final int n, final CharSequence charSequence) {
        final TextView textView = (TextView)this.mRootView.findViewById(n);
        CharSequence string = charSequence;
        if (TextUtils.isEmpty(charSequence)) {
            string = this.getResources().getString(2131887389);
        }
        if (textView != null) {
            textView.setText(string);
        }
    }
}
