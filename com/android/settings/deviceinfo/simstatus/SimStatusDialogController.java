package com.android.settings.deviceinfo.simstatus;

import android.os.UserHandle;
import android.os.Handler;
import android.content.IntentFilter;
import com.android.settingslib.DeviceInfoUtils;
import android.text.TextDirectionHeuristics;
import android.text.BidiFormatter;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.PersistableBundle;
import java.util.List;
import android.telephony.SubscriptionManager;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.os.Bundle;
import android.telephony.CellBroadcastMessage;
import android.text.TextUtils;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionInfo;
import android.content.res.Resources;
import android.telephony.PhoneStateListener;
import android.telephony.euicc.EuiccManager;
import android.content.Context;
import android.telephony.CarrierConfigManager;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class SimStatusDialogController implements LifecycleObserver, OnPause, OnResume
{
    static final int CELLULAR_NETWORK_STATE = 2131362046;
    static final int CELL_DATA_NETWORK_TYPE_VALUE_ID = 2131362042;
    static final int CELL_VOICE_NETWORK_TYPE_VALUE_ID = 2131362811;
    static final int EID_INFO_VALUE_ID = 2131362130;
    static final int ICCID_INFO_LABEL_ID = 2131362234;
    static final int ICCID_INFO_VALUE_ID = 2131362235;
    static final int IMS_REGISTRATION_STATE_LABEL_ID = 2131362258;
    static final int IMS_REGISTRATION_STATE_VALUE_ID = 2131362259;
    static final int NETWORK_PROVIDER_VALUE_ID = 2131362425;
    static final int OPERATOR_INFO_LABEL_ID = 2131362323;
    static final int OPERATOR_INFO_VALUE_ID = 2131362324;
    static final int PHONE_NUMBER_VALUE_ID = 2131362411;
    static final int ROAMING_INFO_VALUE_ID = 2131362536;
    static final int SERVICE_STATE_VALUE_ID = 2131362592;
    static final int SIGNAL_STRENGTH_LABEL_ID = 2131362610;
    static final int SIGNAL_STRENGTH_VALUE_ID = 2131362611;
    private final BroadcastReceiver mAreaInfoReceiver;
    private final CarrierConfigManager mCarrierConfigManager;
    private final Context mContext;
    private final SimStatusDialogFragment mDialog;
    private final EuiccManager mEuiccManager;
    private PhoneStateListener mPhoneStateListener;
    private final Resources mRes;
    private boolean mShowLatestAreaInfo;
    private final SubscriptionInfo mSubscriptionInfo;
    private final TelephonyManager mTelephonyManager;
    
    public SimStatusDialogController(final SimStatusDialogFragment mDialog, final Lifecycle lifecycle, final int n) {
        this.mAreaInfoReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (TextUtils.equals((CharSequence)intent.getAction(), (CharSequence)"com.android.cellbroadcastreceiver.CB_AREA_INFO_RECEIVED")) {
                    final Bundle extras = intent.getExtras();
                    if (extras == null) {
                        return;
                    }
                    final CellBroadcastMessage cellBroadcastMessage = (CellBroadcastMessage)extras.get("message");
                    if (cellBroadcastMessage != null && SimStatusDialogController.this.mSubscriptionInfo.getSubscriptionId() == cellBroadcastMessage.getSubId()) {
                        SimStatusDialogController.this.mDialog.setText(2131362324, cellBroadcastMessage.getMessageBody());
                    }
                }
            }
        };
        this.mDialog = mDialog;
        this.mContext = mDialog.getContext();
        this.mSubscriptionInfo = this.getPhoneSubscriptionInfo(n);
        this.mTelephonyManager = (TelephonyManager)this.mContext.getSystemService("phone");
        this.mCarrierConfigManager = (CarrierConfigManager)this.mContext.getSystemService("carrier_config");
        this.mEuiccManager = (EuiccManager)this.mContext.getSystemService("euicc");
        this.mRes = this.mContext.getResources();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private SubscriptionInfo getPhoneSubscriptionInfo(final int n) {
        final List activeSubscriptionInfoList = SubscriptionManager.from(this.mContext).getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList != null && activeSubscriptionInfoList.size() > n) {
            return activeSubscriptionInfoList.get(n);
        }
        return null;
    }
    
    private void resetSignalStrength() {
        this.mDialog.setText(2131362611, "0");
    }
    
    private void updateDataState(final int n) {
        String s = null;
        switch (n) {
            default: {
                s = this.mRes.getString(2131888746);
                break;
            }
            case 3: {
                s = this.mRes.getString(2131888727);
                break;
            }
            case 2: {
                s = this.mRes.getString(2131888724);
                break;
            }
            case 1: {
                s = this.mRes.getString(2131888725);
                break;
            }
            case 0: {
                s = this.mRes.getString(2131888726);
                break;
            }
        }
        this.mDialog.setText(2131362046, s);
    }
    
    private void updateEid() {
        this.mDialog.setText(2131362130, this.mEuiccManager.getEid());
    }
    
    private void updateIccidNumber() {
        final int subscriptionId = this.mSubscriptionInfo.getSubscriptionId();
        final PersistableBundle configForSubId = this.mCarrierConfigManager.getConfigForSubId(subscriptionId);
        boolean boolean1 = false;
        if (configForSubId != null) {
            boolean1 = configForSubId.getBoolean("show_iccid_in_sim_status_bool");
        }
        if (!boolean1) {
            this.mDialog.removeSettingFromScreen(2131362234);
            this.mDialog.removeSettingFromScreen(2131362235);
        }
        else {
            this.mDialog.setText(2131362235, this.getSimSerialNumber(subscriptionId));
        }
    }
    
    private void updateImsRegistrationState() {
        final int subscriptionId = this.mSubscriptionInfo.getSubscriptionId();
        final PersistableBundle configForSubId = this.mCarrierConfigManager.getConfigForSubId(subscriptionId);
        if (configForSubId != null && configForSubId.getBoolean("show_ims_registration_status_bool")) {
            final boolean imsRegistered = this.mTelephonyManager.isImsRegistered(subscriptionId);
            final SimStatusDialogFragment mDialog = this.mDialog;
            final Resources mRes = this.mRes;
            int n;
            if (imsRegistered) {
                n = R.string.ims_reg_status_registered;
            }
            else {
                n = R.string.ims_reg_status_not_registered;
            }
            mDialog.setText(2131362259, mRes.getString(n));
        }
        else {
            this.mDialog.removeSettingFromScreen(2131362258);
            this.mDialog.removeSettingFromScreen(2131362259);
        }
    }
    
    private void updateLatestAreaInfo() {
        if (!(this.mShowLatestAreaInfo = (Resources.getSystem().getBoolean(17957022) && this.mTelephonyManager.getPhoneType() != 2))) {
            this.mDialog.removeSettingFromScreen(2131362323);
            this.mDialog.removeSettingFromScreen(2131362324);
        }
    }
    
    private void updateNetworkProvider(final ServiceState serviceState) {
        this.mDialog.setText(2131362425, serviceState.getOperatorAlphaLong());
    }
    
    private void updateNetworkType() {
        String networkTypeName = null;
        String networkTypeName2 = null;
        final int subscriptionId = this.mSubscriptionInfo.getSubscriptionId();
        final int dataNetworkType = this.mTelephonyManager.getDataNetworkType(subscriptionId);
        final int voiceNetworkType = this.mTelephonyManager.getVoiceNetworkType(subscriptionId);
        if (dataNetworkType != 0) {
            final TelephonyManager mTelephonyManager = this.mTelephonyManager;
            networkTypeName = TelephonyManager.getNetworkTypeName(dataNetworkType);
        }
        if (voiceNetworkType != 0) {
            final TelephonyManager mTelephonyManager2 = this.mTelephonyManager;
            networkTypeName2 = TelephonyManager.getNetworkTypeName(voiceNetworkType);
        }
        final boolean b = false;
        boolean boolean1;
        try {
            final Context packageContext = this.mContext.createPackageContext("com.android.systemui", 0);
            boolean1 = packageContext.getResources().getBoolean(packageContext.getResources().getIdentifier("config_show4GForLTE", "bool", "com.android.systemui"));
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e("SimStatusDialogCtrl", "NameNotFoundException for show4GForLTE");
            boolean1 = b;
        }
        String s = networkTypeName;
        String s2 = networkTypeName2;
        if (boolean1) {
            String s3 = networkTypeName;
            if ("LTE".equals(networkTypeName)) {
                s3 = "4G";
            }
            s = s3;
            s2 = networkTypeName2;
            if ("LTE".equals(networkTypeName2)) {
                s2 = "4G";
                s = s3;
            }
        }
        this.mDialog.setText(2131362811, s2);
        this.mDialog.setText(2131362042, s);
    }
    
    private void updatePhoneNumber() {
        this.mDialog.setText(2131362411, BidiFormatter.getInstance().unicodeWrap(this.getPhoneNumber(), TextDirectionHeuristics.LTR));
    }
    
    private void updateRoamingStatus(final ServiceState serviceState) {
        if (serviceState.getRoaming()) {
            this.mDialog.setText(2131362536, this.mRes.getString(2131888740));
        }
        else {
            this.mDialog.setText(2131362536, this.mRes.getString(2131888741));
        }
    }
    
    private void updateServiceState(final ServiceState serviceState) {
        final int state = serviceState.getState();
        if (state == 1 || state == 3) {
            this.resetSignalStrength();
        }
        String s = null;
        switch (state) {
            default: {
                s = this.mRes.getString(2131888746);
                break;
            }
            case 3: {
                s = this.mRes.getString(2131888744);
                break;
            }
            case 1:
            case 2: {
                s = this.mRes.getString(2131888745);
                break;
            }
            case 0: {
                s = this.mRes.getString(2131888743);
                break;
            }
        }
        this.mDialog.setText(2131362592, s);
    }
    
    private void updateSignalStrength(final SignalStrength signalStrength) {
        final PersistableBundle configForSubId = this.mCarrierConfigManager.getConfigForSubId(this.mSubscriptionInfo.getSubscriptionId());
        boolean boolean1 = true;
        if (configForSubId != null) {
            boolean1 = configForSubId.getBoolean("show_signal_strength_in_sim_status_bool");
        }
        if (!boolean1) {
            this.mDialog.removeSettingFromScreen(2131362610);
            this.mDialog.removeSettingFromScreen(2131362611);
            return;
        }
        final int state = this.getCurrentServiceState().getState();
        if (state == 0 && 3 != state) {
            final int dbm = this.getDbm(signalStrength);
            final int asuLevel = this.getAsuLevel(signalStrength);
            int n;
            if ((n = dbm) == -1) {
                n = 0;
            }
            int n2;
            if ((n2 = asuLevel) == -1) {
                n2 = 0;
            }
            this.mDialog.setText(2131362611, this.mRes.getString(2131889169, new Object[] { n, n2 }));
        }
    }
    
    int getAsuLevel(final SignalStrength signalStrength) {
        return signalStrength.getAsuLevel();
    }
    
    ServiceState getCurrentServiceState() {
        return this.mTelephonyManager.getServiceStateForSubscriber(this.mSubscriptionInfo.getSubscriptionId());
    }
    
    int getDbm(final SignalStrength signalStrength) {
        return signalStrength.getDbm();
    }
    
    String getPhoneNumber() {
        return DeviceInfoUtils.getFormattedPhoneNumber(this.mContext, this.mSubscriptionInfo);
    }
    
    PhoneStateListener getPhoneStateListener() {
        return new PhoneStateListener(this.mSubscriptionInfo.getSubscriptionId()) {
            public void onDataConnectionStateChanged(final int n) {
                SimStatusDialogController.this.updateDataState(n);
                SimStatusDialogController.this.updateNetworkType();
            }
            
            public void onServiceStateChanged(final ServiceState serviceState) {
                SimStatusDialogController.this.updateNetworkProvider(serviceState);
                SimStatusDialogController.this.updateServiceState(serviceState);
                SimStatusDialogController.this.updateRoamingStatus(serviceState);
            }
            
            public void onSignalStrengthsChanged(final SignalStrength signalStrength) {
                SimStatusDialogController.this.updateSignalStrength(signalStrength);
            }
        };
    }
    
    SignalStrength getSignalStrength() {
        return this.mTelephonyManager.getSignalStrength();
    }
    
    String getSimSerialNumber(final int n) {
        return this.mTelephonyManager.getSimSerialNumber(n);
    }
    
    public void initialize() {
        this.updateEid();
        if (this.mSubscriptionInfo == null) {
            return;
        }
        this.mPhoneStateListener = this.getPhoneStateListener();
        final ServiceState currentServiceState = this.getCurrentServiceState();
        this.updateNetworkProvider(currentServiceState);
        this.updatePhoneNumber();
        this.updateLatestAreaInfo();
        this.updateServiceState(currentServiceState);
        this.updateSignalStrength(this.getSignalStrength());
        this.updateNetworkType();
        this.updateRoamingStatus(currentServiceState);
        this.updateIccidNumber();
        this.updateImsRegistrationState();
    }
    
    @Override
    public void onPause() {
        if (this.mSubscriptionInfo == null) {
            return;
        }
        this.mTelephonyManager.listen(this.mPhoneStateListener, 0);
        if (this.mShowLatestAreaInfo) {
            this.mContext.unregisterReceiver(this.mAreaInfoReceiver);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSubscriptionInfo == null) {
            return;
        }
        this.mTelephonyManager.listen(this.mPhoneStateListener, 321);
        if (this.mShowLatestAreaInfo) {
            this.mContext.registerReceiver(this.mAreaInfoReceiver, new IntentFilter("com.android.cellbroadcastreceiver.CB_AREA_INFO_RECEIVED"), "android.permission.RECEIVE_EMERGENCY_BROADCAST", (Handler)null);
            final Intent intent = new Intent("com.android.cellbroadcastreceiver.GET_LATEST_CB_AREA_INFO");
            intent.setPackage("com.android.cellbroadcastreceiver");
            this.mContext.sendBroadcastAsUser(intent, UserHandle.ALL, "android.permission.RECEIVE_EMERGENCY_BROADCAST");
        }
    }
}
