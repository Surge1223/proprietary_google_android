package com.android.settings.deviceinfo.aboutphone;

import android.content.Intent;
import android.content.pm.UserInfo;
import android.os.Bundle;
import android.view.View;
import android.os.Process;
import android.os.UserManager;
import com.android.settings.widget.EntityHeaderController;
import com.android.settings.applications.LayoutPreference;
import com.android.settings.deviceinfo.FccEquipmentIdPreferenceController;
import com.android.settings.deviceinfo.FeedbackPreferenceController;
import com.android.settings.deviceinfo.ManualPreferenceController;
import com.android.settings.deviceinfo.SafetyInfoPreferenceController;
import com.android.settings.deviceinfo.RegulatoryInfoPreferenceController;
import com.android.settings.deviceinfo.BluetoothAddressPreferenceController;
import com.android.settings.deviceinfo.WifiMacAddressPreferenceController;
import com.android.settings.deviceinfo.IpAddressPreferenceController;
import com.android.settings.deviceinfo.firmwareversion.FirmwareVersionPreferenceController;
import com.android.settings.deviceinfo.imei.ImeiInfoPreferenceController;
import com.android.settings.deviceinfo.DeviceModelPreferenceController;
import android.app.Fragment;
import com.android.settings.deviceinfo.simstatus.SimStatusPreferenceController;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.bluetooth.Utils;
import com.android.settings.deviceinfo.BrandedAccountPreferenceController;
import com.android.settings.deviceinfo.PhoneNumberPreferenceController;
import com.android.settings.accounts.EmergencyInfoPreferenceController;
import com.android.settings.deviceinfo.BuildNumberPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.deviceinfo.DeviceNamePreferenceController;
import com.android.settings.dashboard.DashboardFragment;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$MyDeviceInfoFragment$pzCelMuIMGm16asu34w_Ge8IYsk implements SummaryProviderFactory
{
    @Override
    public final SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
        return MyDeviceInfoFragment.lambda$static$0(activity, summaryLoader);
    }
}
