package com.android.settings.deviceinfo.aboutphone;

import android.content.Intent;
import android.content.pm.UserInfo;
import android.os.Bundle;
import android.view.View;
import android.os.Process;
import android.os.UserManager;
import com.android.settings.widget.EntityHeaderController;
import com.android.settings.applications.LayoutPreference;
import com.android.settings.deviceinfo.FccEquipmentIdPreferenceController;
import com.android.settings.deviceinfo.FeedbackPreferenceController;
import com.android.settings.deviceinfo.ManualPreferenceController;
import com.android.settings.deviceinfo.SafetyInfoPreferenceController;
import com.android.settings.deviceinfo.RegulatoryInfoPreferenceController;
import com.android.settings.deviceinfo.BluetoothAddressPreferenceController;
import com.android.settings.deviceinfo.WifiMacAddressPreferenceController;
import com.android.settings.deviceinfo.IpAddressPreferenceController;
import com.android.settings.deviceinfo.firmwareversion.FirmwareVersionPreferenceController;
import com.android.settings.deviceinfo.imei.ImeiInfoPreferenceController;
import com.android.settings.deviceinfo.DeviceModelPreferenceController;
import android.app.Fragment;
import com.android.settings.deviceinfo.simstatus.SimStatusPreferenceController;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.bluetooth.Utils;
import com.android.settings.deviceinfo.BrandedAccountPreferenceController;
import com.android.settings.deviceinfo.PhoneNumberPreferenceController;
import com.android.settings.accounts.EmergencyInfoPreferenceController;
import com.android.settings.deviceinfo.BuildNumberPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Activity;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.deviceinfo.DeviceNamePreferenceController;
import com.android.settings.dashboard.DashboardFragment;

public class MyDeviceInfoFragment extends DashboardFragment implements DeviceNamePreferenceHost
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = (SummaryProviderFactory)_$$Lambda$MyDeviceInfoFragment$pzCelMuIMGm16asu34w_Ge8IYsk.INSTANCE;
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("legal_container");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082790;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Activity activity, final MyDeviceInfoFragment host, final Lifecycle lifecycle) {
        final ArrayList<PhoneNumberPreferenceController> list = (ArrayList<PhoneNumberPreferenceController>)new ArrayList<BuildNumberPreferenceController>();
        list.add((BuildNumberPreferenceController)new EmergencyInfoPreferenceController(context));
        list.add((BuildNumberPreferenceController)new PhoneNumberPreferenceController(context));
        list.add((BuildNumberPreferenceController)new BrandedAccountPreferenceController(context));
        final DeviceNamePreferenceController deviceNamePreferenceController = new DeviceNamePreferenceController(context);
        deviceNamePreferenceController.setLocalBluetoothManager(Utils.getLocalBtManager(context));
        deviceNamePreferenceController.setHost((DeviceNamePreferenceController.DeviceNamePreferenceHost)host);
        if (lifecycle != null) {
            lifecycle.addObserver(deviceNamePreferenceController);
        }
        list.add((BuildNumberPreferenceController)deviceNamePreferenceController);
        list.add((BuildNumberPreferenceController)new SimStatusPreferenceController(context, host));
        list.add((BuildNumberPreferenceController)new DeviceModelPreferenceController(context, host));
        list.add((BuildNumberPreferenceController)new ImeiInfoPreferenceController(context, host));
        list.add((BuildNumberPreferenceController)new FirmwareVersionPreferenceController(context, host));
        list.add((BuildNumberPreferenceController)new IpAddressPreferenceController(context, lifecycle));
        list.add((BuildNumberPreferenceController)new WifiMacAddressPreferenceController(context, lifecycle));
        list.add((BuildNumberPreferenceController)new BluetoothAddressPreferenceController(context, lifecycle));
        list.add((BuildNumberPreferenceController)new RegulatoryInfoPreferenceController(context));
        list.add((BuildNumberPreferenceController)new SafetyInfoPreferenceController(context));
        list.add((BuildNumberPreferenceController)new ManualPreferenceController(context));
        list.add((BuildNumberPreferenceController)new FeedbackPreferenceController(host, context));
        list.add((BuildNumberPreferenceController)new FccEquipmentIdPreferenceController(context));
        list.add(new BuildNumberPreferenceController(context, activity, host, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    private void initHeader() {
        final View viewById = ((LayoutPreference)this.getPreferenceScreen().findPreference("my_device_info_header")).findViewById(2131362112);
        final Activity activity = this.getActivity();
        final Bundle arguments = this.getArguments();
        final EntityHeaderController setButtonActions = EntityHeaderController.newInstance(activity, this, viewById).setRecyclerView(this.getListView(), this.getLifecycle()).setButtonActions(0, 0);
        if (arguments.getInt("icon_id", 0) == 0) {
            final UserManager userManager = (UserManager)this.getActivity().getSystemService("user");
            final UserInfo existingUser = com.android.settings.Utils.getExistingUser(userManager, Process.myUserHandle());
            setButtonActions.setLabel(existingUser.name);
            setButtonActions.setIcon(com.android.settingslib.Utils.getUserIcon((Context)this.getActivity(), userManager, existingUser));
        }
        setButtonActions.done(activity, true);
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getActivity(), this, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887769;
    }
    
    @Override
    protected String getLogTag() {
        return "MyDeviceInfoFragment";
    }
    
    @Override
    public int getMetricsCategory() {
        return 40;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082790;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (this.use(BuildNumberPreferenceController.class).onActivityResult(n, n2, intent)) {
            return;
        }
        super.onActivityResult(n, n2, intent);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.initHeader();
    }
    
    public void onSetDeviceNameConfirm() {
        this.use(DeviceNamePreferenceController.class).confirmDeviceName();
    }
    
    @Override
    public void showDeviceNameWarningDialog(final String s) {
        DeviceNameWarningDialog.show(this);
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final SummaryLoader mSummaryLoader) {
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, DeviceModelPreferenceController.getDeviceModel());
            }
        }
    }
}
