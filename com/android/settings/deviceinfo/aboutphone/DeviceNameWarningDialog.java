package com.android.settings.deviceinfo.aboutphone;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class DeviceNameWarningDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    public static void show(final MyDeviceInfoFragment myDeviceInfoFragment) {
        final FragmentManager fragmentManager = myDeviceInfoFragment.getActivity().getFragmentManager();
        if (fragmentManager.findFragmentByTag("DeviceNameWarningDlg") != null) {
            return;
        }
        final DeviceNameWarningDialog deviceNameWarningDialog = new DeviceNameWarningDialog();
        deviceNameWarningDialog.setTargetFragment((Fragment)myDeviceInfoFragment, 0);
        deviceNameWarningDialog.show(fragmentManager, "DeviceNameWarningDlg");
    }
    
    public int getMetricsCategory() {
        return 1219;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final MyDeviceInfoFragment myDeviceInfoFragment = (MyDeviceInfoFragment)this.getTargetFragment();
        if (n == -1) {
            myDeviceInfoFragment.onSetDeviceNameConfirm();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888337).setMessage(2131886132).setCancelable(false).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
    }
}
