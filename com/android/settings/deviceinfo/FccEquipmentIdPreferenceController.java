package com.android.settings.deviceinfo;

import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.os.SystemProperties;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class FccEquipmentIdPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public FccEquipmentIdPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("fcc_equipment_id");
        if (preference != null) {
            preference.setSummary(SystemProperties.get("ro.ril.fccid", this.mContext.getResources().getString(2131887389)));
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "fcc_equipment_id";
    }
    
    @Override
    public boolean isAvailable() {
        return TextUtils.isEmpty((CharSequence)SystemProperties.get("ro.ril.fccid")) ^ true;
    }
}
