package com.android.settings.deviceinfo;

import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.app.usage.ExternalStorageStats;
import java.util.Iterator;
import java.util.UUID;
import android.os.storage.VolumeInfo;
import java.io.IOException;
import android.os.UserHandle;
import android.content.pm.UserInfo;
import android.telecom.Log;
import android.app.usage.StorageStatsManager;
import android.os.storage.StorageManager;
import android.os.UserManager;
import android.content.Intent;
import android.content.Context;
import android.os.AsyncTask;

public abstract class MigrateEstimateTask extends AsyncTask<Void, Void, Long>
{
    private final Context mContext;
    private long mSizeBytes;
    
    public MigrateEstimateTask(final Context mContext) {
        this.mSizeBytes = -1L;
        this.mContext = mContext;
    }
    
    public void copyFrom(final Intent intent) {
        this.mSizeBytes = intent.getLongExtra("size_bytes", -1L);
    }
    
    protected Long doInBackground(final Void... array) {
        if (this.mSizeBytes != -1L) {
            return this.mSizeBytes;
        }
        final UserManager userManager = (UserManager)this.mContext.getSystemService((Class)UserManager.class);
        final StorageManager storageManager = (StorageManager)this.mContext.getSystemService((Class)StorageManager.class);
        final StorageStatsManager storageStatsManager = (StorageStatsManager)this.mContext.getSystemService((Class)StorageStatsManager.class);
        final VolumeInfo emulatedForPrivate = storageManager.findEmulatedForPrivate(this.mContext.getPackageManager().getPrimaryStorageCurrentVolume());
        if (emulatedForPrivate == null) {
            Log.w("StorageSettings", "Failed to find current primary storage", new Object[0]);
            return -1L;
        }
        try {
            final UUID uuidForPath = storageManager.getUuidForPath(emulatedForPrivate.getPath());
            final StringBuilder sb = new StringBuilder();
            sb.append("Measuring size of ");
            sb.append(uuidForPath);
            Log.d("StorageSettings", sb.toString(), new Object[0]);
            long n = 0L;
            for (final UserInfo userInfo : userManager.getUsers()) {
                final ExternalStorageStats queryExternalStatsForUser = storageStatsManager.queryExternalStatsForUser(uuidForPath, UserHandle.of(userInfo.id));
                final long n2 = n += queryExternalStatsForUser.getTotalBytes();
                if (userInfo.id == 0) {
                    n = n2 + queryExternalStatsForUser.getObbBytes();
                }
            }
            return n;
        }
        catch (IOException ex) {
            Log.w("StorageSettings", "Failed to measure", new Object[] { ex });
            return -1L;
        }
    }
    
    protected void onPostExecute(final Long n) {
        this.mSizeBytes = n;
        this.onPostExecute(Formatter.formatFileSize(this.mContext, this.mSizeBytes), DateUtils.formatDuration(Math.max(this.mSizeBytes * 1000L / 10485760L, 1000L)).toString());
    }
    
    public abstract void onPostExecute(final String p0, final String p1);
}
