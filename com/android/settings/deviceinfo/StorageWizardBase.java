package com.android.settings.deviceinfo;

import android.graphics.drawable.Drawable;
import com.android.settingslib.Utils;
import java.text.NumberFormat;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.view.View;
import android.text.TextUtils;
import android.os.Bundle;
import android.widget.ProgressBar;
import com.android.setupwizardlib.GlifLayout;
import java.util.Iterator;
import android.os.SystemClock;
import android.util.Log;
import java.util.Objects;
import android.content.Intent;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.DiskInfo;
import android.widget.Button;
import android.app.Activity;

public abstract class StorageWizardBase extends Activity
{
    private Button mBack;
    protected DiskInfo mDisk;
    private Button mNext;
    protected StorageManager mStorage;
    private final StorageEventListener mStorageListener;
    protected VolumeInfo mVolume;
    
    public StorageWizardBase() {
        this.mStorageListener = new StorageEventListener() {
            public void onDiskDestroyed(final DiskInfo diskInfo) {
                if (StorageWizardBase.this.mDisk.id.equals(diskInfo.id)) {
                    StorageWizardBase.this.finish();
                }
            }
        };
    }
    
    private void copyBooleanExtra(final Intent intent, final Intent intent2, final String s) {
        if (intent.hasExtra(s) && !intent2.hasExtra(s)) {
            intent2.putExtra(s, intent.getBooleanExtra(s, false));
        }
    }
    
    private void copyStringExtra(final Intent intent, final Intent intent2, final String s) {
        if (intent.hasExtra(s) && !intent2.hasExtra(s)) {
            intent2.putExtra(s, intent.getStringExtra(s));
        }
    }
    
    protected VolumeInfo findFirstVolume(final int n) {
        return this.findFirstVolume(n, 1);
    }
    
    protected VolumeInfo findFirstVolume(final int n, int n2) {
        while (true) {
            for (final VolumeInfo volumeInfo : this.mStorage.getVolumes()) {
                if (Objects.equals(this.mDisk.getId(), volumeInfo.getDiskId()) && volumeInfo.getType() == n && volumeInfo.getState() == 2) {
                    return volumeInfo;
                }
            }
            if (--n2 <= 0) {
                return null;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Missing mounted volume of type ");
            sb.append(n);
            sb.append(" hosted by disk ");
            sb.append(this.mDisk.getId());
            sb.append("; trying again");
            Log.w("StorageSettings", sb.toString());
            SystemClock.sleep(250L);
        }
    }
    
    protected CharSequence getDiskDescription() {
        if (this.mDisk != null) {
            return this.mDisk.getDescription();
        }
        if (this.mVolume != null) {
            return this.mVolume.getDescription();
        }
        return this.getText(R.string.unknown);
    }
    
    protected CharSequence getDiskShortDescription() {
        if (this.mDisk != null) {
            return this.mDisk.getShortDescription();
        }
        if (this.mVolume != null) {
            return this.mVolume.getDescription();
        }
        return this.getText(R.string.unknown);
    }
    
    protected GlifLayout getGlifLayout() {
        return (GlifLayout)this.requireViewById(2131362597);
    }
    
    protected ProgressBar getProgressBar() {
        return (ProgressBar)this.requireViewById(2131362660);
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mStorage = (StorageManager)this.getSystemService((Class)StorageManager.class);
        final String stringExtra = this.getIntent().getStringExtra("android.os.storage.extra.VOLUME_ID");
        if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
            this.mVolume = this.mStorage.findVolumeById(stringExtra);
        }
        final String stringExtra2 = this.getIntent().getStringExtra("android.os.storage.extra.DISK_ID");
        if (!TextUtils.isEmpty((CharSequence)stringExtra2)) {
            this.mDisk = this.mStorage.findDiskById(stringExtra2);
        }
        else if (this.mVolume != null) {
            this.mDisk = this.mVolume.getDisk();
        }
        if (this.mDisk != null) {
            this.mStorage.registerListener(this.mStorageListener);
        }
    }
    
    protected void onDestroy() {
        this.mStorage.unregisterListener(this.mStorageListener);
        super.onDestroy();
    }
    
    public void onNavigateBack(final View view) {
        throw new UnsupportedOperationException();
    }
    
    public void onNavigateNext(final View view) {
        throw new UnsupportedOperationException();
    }
    
    protected void setAuxChecklist() {
        final FrameLayout frameLayout = (FrameLayout)this.requireViewById(2131362655);
        frameLayout.addView(LayoutInflater.from(frameLayout.getContext()).inflate(2131558767, (ViewGroup)frameLayout, false));
        frameLayout.setVisibility(0);
        ((TextView)frameLayout.requireViewById(2131362659)).setText(TextUtils.expandTemplate(this.getText(2131889363), new CharSequence[] { this.getDiskShortDescription() }));
    }
    
    protected void setBackButtonText(final int n, final CharSequence... array) {
        this.mBack.setText(TextUtils.expandTemplate(this.getText(n), array));
        this.mBack.setVisibility(0);
    }
    
    protected void setBodyText(final int n, final CharSequence... array) {
        final TextView textView = (TextView)this.requireViewById(2131362656);
        textView.setText(TextUtils.expandTemplate(this.getText(n), array));
        textView.setVisibility(0);
    }
    
    public void setContentView(final int contentView) {
        super.setContentView(contentView);
        this.mBack = (Button)this.requireViewById(2131362647);
        this.mNext = (Button)this.requireViewById(2131362652);
        this.setIcon(17302733);
    }
    
    protected void setCurrentProgress(final int progress) {
        this.getProgressBar().setProgress(progress);
        ((TextView)this.requireViewById(2131362661)).setText((CharSequence)NumberFormat.getPercentInstance().format(progress / 100.0));
    }
    
    protected void setHeaderText(final int n, final CharSequence... array) {
        final CharSequence expandTemplate = TextUtils.expandTemplate(this.getText(n), array);
        this.getGlifLayout().setHeaderText(expandTemplate);
        this.setTitle(expandTemplate);
    }
    
    protected void setIcon(final int n) {
        final GlifLayout glifLayout = this.getGlifLayout();
        final Drawable mutate = this.getDrawable(n).mutate();
        mutate.setTint(Utils.getColorAccent(glifLayout.getContext()));
        glifLayout.setIcon(mutate);
    }
    
    protected void setKeepScreenOn(final boolean keepScreenOn) {
        this.getGlifLayout().setKeepScreenOn(keepScreenOn);
    }
    
    protected void setNextButtonText(final int n, final CharSequence... array) {
        this.mNext.setText(TextUtils.expandTemplate(this.getText(n), array));
        this.mNext.setVisibility(0);
    }
    
    public void startActivity(final Intent intent) {
        final Intent intent2 = this.getIntent();
        this.copyStringExtra(intent2, intent, "android.os.storage.extra.DISK_ID");
        this.copyStringExtra(intent2, intent, "android.os.storage.extra.VOLUME_ID");
        this.copyStringExtra(intent2, intent, "format_forget_uuid");
        this.copyBooleanExtra(intent2, intent, "format_private");
        this.copyBooleanExtra(intent2, intent, "format_slow");
        this.copyBooleanExtra(intent2, intent, "migrate_skip");
        super.startActivity(intent);
    }
}
