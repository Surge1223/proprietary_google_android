package com.android.settings.deviceinfo;

import java.util.function.Predicate;
import android.content.Loader;
import android.os.Bundle;
import com.android.settings.deviceinfo.storage.UserIconLoader;
import android.graphics.drawable.Drawable;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settingslib.core.AbstractPreferenceController;
import android.util.SparseArray;
import java.util.function.Consumer;

public final class _$$Lambda$StorageDashboardFragment$IconLoaderCallbacks$Jn0eBlqBHbuO_2COJ4jEmaXSJJc implements Consumer
{
    @Override
    public final void accept(final Object o) {
        StorageDashboardFragment.IconLoaderCallbacks.lambda$onLoadFinished$2(this.f$0, (AbstractPreferenceController)o);
    }
}
