package com.android.settings.deviceinfo.imei;

import android.support.v7.preference.PreferenceScreen;
import java.util.ArrayList;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.support.v7.preference.Preference;
import java.util.List;
import android.app.Fragment;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.deviceinfo.AbstractSimStatusImeiInfoPreferenceController;

public class ImeiInfoPreferenceController extends AbstractSimStatusImeiInfoPreferenceController implements PreferenceControllerMixin
{
    private final Fragment mFragment;
    private final boolean mIsMultiSim;
    private final List<Preference> mPreferenceList;
    private final TelephonyManager mTelephonyManager;
    
    public ImeiInfoPreferenceController(final Context context, final Fragment mFragment) {
        super(context);
        this.mPreferenceList = new ArrayList<Preference>();
        this.mFragment = mFragment;
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
        final int phoneCount = this.mTelephonyManager.getPhoneCount();
        boolean mIsMultiSim = true;
        if (phoneCount <= 1) {
            mIsMultiSim = false;
        }
        this.mIsMultiSim = mIsMultiSim;
    }
    
    private CharSequence getTitleForCdmaPhone(final int n) {
        String s;
        if (this.mIsMultiSim) {
            s = this.mContext.getString(2131888259, new Object[] { n + 1 });
        }
        else {
            s = this.mContext.getString(2131889236);
        }
        return s;
    }
    
    private CharSequence getTitleForGsmPhone(final int n) {
        String s;
        if (this.mIsMultiSim) {
            s = this.mContext.getString(2131887864, new Object[] { n + 1 });
        }
        else {
            s = this.mContext.getString(2131889232);
        }
        return s;
    }
    
    private void updatePreference(final Preference preference, final int n) {
        if (this.mTelephonyManager.getPhoneType() == 2) {
            preference.setTitle(this.getTitleForCdmaPhone(n));
            preference.setSummary(this.getMeid(n));
        }
        else {
            preference.setTitle(this.getTitleForGsmPhone(n));
            preference.setSummary(this.mTelephonyManager.getImei(n));
        }
    }
    
    Preference createNewPreference(final Context context) {
        return new Preference(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        if (this.isAvailable() && preference != null && preference.isVisible()) {
            this.mPreferenceList.add(preference);
            this.updatePreference(preference, 0);
            final int order = preference.getOrder();
            for (int i = 1; i < this.mTelephonyManager.getPhoneCount(); ++i) {
                final Preference newPreference = this.createNewPreference(preferenceScreen.getContext());
                newPreference.setOrder(order + i);
                final StringBuilder sb = new StringBuilder();
                sb.append("imei_info");
                sb.append(i);
                newPreference.setKey(sb.toString());
                preferenceScreen.addPreference(newPreference);
                this.mPreferenceList.add(newPreference);
                this.updatePreference(newPreference, i);
            }
        }
    }
    
    String getMeid(final int n) {
        return this.mTelephonyManager.getMeid(n);
    }
    
    @Override
    public String getPreferenceKey() {
        return "imei_info";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        final int index = this.mPreferenceList.indexOf(preference);
        if (index == -1) {
            return false;
        }
        ImeiInfoDialogFragment.show(this.mFragment, index, preference.getTitle().toString());
        return true;
    }
}
