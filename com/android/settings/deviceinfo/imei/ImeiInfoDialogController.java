package com.android.settings.deviceinfo.imei;

import android.text.Spannable;
import android.content.res.Resources;
import android.text.style.TtsSpan$DigitsBuilder;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import java.util.List;
import android.telephony.SubscriptionManager;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionInfo;

public class ImeiInfoDialogController
{
    static final int ID_CDMA_SETTINGS = 2131361968;
    static final int ID_GSM_SETTINGS = 2131362184;
    static final int ID_IMEI_SV_VALUE = 2131362251;
    static final int ID_IMEI_VALUE = 2131362252;
    static final int ID_MEID_NUMBER_VALUE = 2131362361;
    static final int ID_MIN_NUMBER_VALUE = 2131362374;
    static final int ID_PRL_VERSION_VALUE = 2131362478;
    private final ImeiInfoDialogFragment mDialog;
    private final int mSlotId;
    private final SubscriptionInfo mSubscriptionInfo;
    private final TelephonyManager mTelephonyManager;
    
    public ImeiInfoDialogController(final ImeiInfoDialogFragment mDialog, final int mSlotId) {
        this.mDialog = mDialog;
        this.mSlotId = mSlotId;
        final Context context = mDialog.getContext();
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
        this.mSubscriptionInfo = this.getSubscriptionInfo(context, mSlotId);
    }
    
    private SubscriptionInfo getSubscriptionInfo(final Context context, final int n) {
        final List activeSubscriptionInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList == null) {
            return null;
        }
        return activeSubscriptionInfoList.get(n);
    }
    
    private static CharSequence getTextAsDigits(final CharSequence charSequence) {
        Object o = charSequence;
        if (TextUtils.isDigitsOnly(charSequence)) {
            o = new SpannableStringBuilder(charSequence);
            ((Spannable)o).setSpan((Object)new TtsSpan$DigitsBuilder(charSequence.toString()).build(), 0, ((Spannable)o).length(), 33);
        }
        return (CharSequence)o;
    }
    
    private void updateDialogForCdmaPhone() {
        final Resources resources = this.mDialog.getContext().getResources();
        this.mDialog.setText(2131362361, this.getMeid());
        final ImeiInfoDialogFragment mDialog = this.mDialog;
        String cdmaMin;
        if (this.mSubscriptionInfo != null) {
            cdmaMin = this.mTelephonyManager.getCdmaMin(this.mSubscriptionInfo.getSubscriptionId());
        }
        else {
            cdmaMin = "";
        }
        mDialog.setText(2131362374, cdmaMin);
        if (resources.getBoolean(2131034128)) {
            this.mDialog.setText(2131362373, resources.getString(2131889238));
        }
        this.mDialog.setText(2131362478, this.getCdmaPrlVersion());
        if (this.mSubscriptionInfo != null && this.isCdmaLteEnabled()) {
            this.mDialog.setText(2131362252, getTextAsDigits(this.mTelephonyManager.getImei(this.mSlotId)));
            this.mDialog.setText(2131362251, getTextAsDigits(this.mTelephonyManager.getDeviceSoftwareVersion(this.mSlotId)));
        }
        else {
            this.mDialog.removeViewFromScreen(2131362184);
        }
    }
    
    private void updateDialogForGsmPhone() {
        this.mDialog.setText(2131362252, getTextAsDigits(this.mTelephonyManager.getImei(this.mSlotId)));
        this.mDialog.setText(2131362251, getTextAsDigits(this.mTelephonyManager.getDeviceSoftwareVersion(this.mSlotId)));
        this.mDialog.removeViewFromScreen(2131361968);
    }
    
    String getCdmaPrlVersion() {
        return this.mTelephonyManager.getCdmaPrlVersion();
    }
    
    String getMeid() {
        return this.mTelephonyManager.getMeid(this.mSlotId);
    }
    
    boolean isCdmaLteEnabled() {
        final int lteOnCdmaMode = this.mTelephonyManager.getLteOnCdmaMode(this.mSubscriptionInfo.getSubscriptionId());
        boolean b = true;
        if (lteOnCdmaMode != 1) {
            b = false;
        }
        return b;
    }
    
    public void populateImeiInfo() {
        if (this.mTelephonyManager.getPhoneType() == 2) {
            this.updateDialogForCdmaPhone();
        }
        else {
            this.updateDialogForGsmPhone();
        }
    }
}
