package com.android.settings;

import com.android.settings.display.BrightnessLevelPreferenceController;
import com.android.settings.display.ThemePreferenceController;
import com.android.settings.display.WallpaperPreferenceController;
import com.android.settings.display.ShowOperatorNamePreferenceController;
import com.android.settings.display.VrDisplayPreferenceController;
import com.android.settings.display.TimeoutPreferenceController;
import com.android.settings.display.TapToWakePreferenceController;
import com.android.settings.display.AmbientDisplayPreferenceController;
import com.android.internal.hardware.AmbientDisplayConfiguration;
import com.android.settings.display.ScreenSaverPreferenceController;
import com.android.settings.display.NightModePreferenceController;
import com.android.settings.display.NightDisplayPreferenceController;
import com.android.settings.display.LiftToWakePreferenceController;
import com.android.settings.display.CameraGesturePreferenceController;
import com.android.settings.display.ColorModePreferenceController;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class DisplaySettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("display_settings_screen_zoom");
                nonIndexableKeys.add("wallpaper");
                nonIndexableKeys.add("night_display");
                nonIndexableKeys.add("auto_brightness_entry");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082764;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<LiftToWakePreferenceController> list = (ArrayList<LiftToWakePreferenceController>)new ArrayList<ColorModePreferenceController>();
        list.add((ColorModePreferenceController)new CameraGesturePreferenceController(context));
        list.add((ColorModePreferenceController)new LiftToWakePreferenceController(context));
        list.add((ColorModePreferenceController)new NightDisplayPreferenceController(context));
        list.add((ColorModePreferenceController)new NightModePreferenceController(context));
        list.add((ColorModePreferenceController)new ScreenSaverPreferenceController(context));
        list.add((ColorModePreferenceController)new AmbientDisplayPreferenceController(context, new AmbientDisplayConfiguration(context), "ambient_display"));
        list.add((ColorModePreferenceController)new TapToWakePreferenceController(context));
        list.add((ColorModePreferenceController)new TimeoutPreferenceController(context, "screen_timeout"));
        list.add((ColorModePreferenceController)new VrDisplayPreferenceController(context));
        list.add((ColorModePreferenceController)new ShowOperatorNamePreferenceController(context));
        list.add((ColorModePreferenceController)new WallpaperPreferenceController(context));
        list.add((ColorModePreferenceController)new ThemePreferenceController(context));
        list.add((ColorModePreferenceController)new BrightnessLevelPreferenceController(context, lifecycle));
        list.add(new ColorModePreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887776;
    }
    
    @Override
    protected String getLogTag() {
        return "DisplaySettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 46;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082764;
    }
}
