package com.android.settings.accessibility;

import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class VibrationSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082698;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    @Override
    protected String getLogTag() {
        return "VibrationSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1292;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082698;
    }
}
