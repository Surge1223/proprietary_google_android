package com.android.settings.accessibility;

import android.text.TextUtils;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.TogglePreferenceController;

public class MagnificationNavbarPreferenceController extends TogglePreferenceController
{
    private boolean mIsFromSUW;
    
    public MagnificationNavbarPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mIsFromSUW = false;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (MagnificationPreferenceFragment.isApplicable(this.mContext.getResources())) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        int n;
        if (this.mIsFromSUW) {
            n = 2131886177;
        }
        else if (this.isChecked()) {
            n = 2131886155;
        }
        else {
            n = 2131886154;
        }
        return this.mContext.getText(n);
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.getPreferenceKey().equals(preference.getKey())) {
            final Bundle extras = preference.getExtras();
            extras.putString("preference_key", "accessibility_display_magnification_navbar_enabled");
            extras.putInt("title_res", 2131886179);
            extras.putInt("summary_res", 2131886178);
            extras.putBoolean("checked", this.isChecked());
            extras.putBoolean("from_suw", this.mIsFromSUW);
        }
        return false;
    }
    
    @Override
    public boolean isChecked() {
        return MagnificationPreferenceFragment.isChecked(this.mContext.getContentResolver(), "accessibility_display_magnification_navbar_enabled");
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"screen_magnification_navbar_preference_screen");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return MagnificationPreferenceFragment.setChecked(this.mContext.getContentResolver(), "accessibility_display_magnification_navbar_enabled", b);
    }
    
    public void setIsFromSUW(final boolean mIsFromSUW) {
        this.mIsFromSUW = mIsFromSUW;
    }
}
