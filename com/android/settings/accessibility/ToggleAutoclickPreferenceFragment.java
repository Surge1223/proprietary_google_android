package com.android.settings.accessibility;

import android.widget.Switch;
import android.content.ContentResolver;
import android.provider.Settings;
import android.os.Bundle;
import android.content.res.Resources;
import com.android.settings.widget.SeekBarPreference;
import com.android.settings.widget.SwitchBar;
import android.support.v7.preference.Preference;

public class ToggleAutoclickPreferenceFragment extends ToggleFeaturePreferenceFragment implements OnPreferenceChangeListener, OnSwitchChangeListener
{
    private static final int[] mAutoclickPreferenceSummaries;
    private SeekBarPreference mDelay;
    
    static {
        mAutoclickPreferenceSummaries = new int[] { 2131755011, 2131755015, 2131755013, 2131755012, 2131755014 };
    }
    
    private int delayToSeekBarProgress(final int n) {
        return (n - 200) / 100;
    }
    
    static CharSequence getAutoclickPreferenceSummary(final Resources resources, final int n) {
        return resources.getQuantityString(ToggleAutoclickPreferenceFragment.mAutoclickPreferenceSummaries[getAutoclickPreferenceSummaryIndex(n)], n, new Object[] { n });
    }
    
    private static int getAutoclickPreferenceSummaryIndex(final int n) {
        if (n <= 200) {
            return 0;
        }
        if (n >= 1000) {
            return ToggleAutoclickPreferenceFragment.mAutoclickPreferenceSummaries.length - 1;
        }
        return (n - 200) / (800 / (ToggleAutoclickPreferenceFragment.mAutoclickPreferenceSummaries.length - 1));
    }
    
    private int seekBarProgressToDelay(final int n) {
        return n * 100 + 200;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887797;
    }
    
    @Override
    public int getMetricsCategory() {
        return 335;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082689;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final int int1 = Settings.Secure.getInt(this.getContentResolver(), "accessibility_autoclick_delay", 600);
        (this.mDelay = (SeekBarPreference)this.findPreference("autoclick_delay")).setMax(this.delayToSeekBarProgress(1000));
        this.mDelay.setProgress(this.delayToSeekBarProgress(int1));
        this.mDelay.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131886141);
    }
    
    @Override
    protected void onInstallSwitchBarToggleSwitch() {
        super.onInstallSwitchBarToggleSwitch();
        final ContentResolver contentResolver = this.getContentResolver();
        final boolean b = false;
        final int int1 = Settings.Secure.getInt(contentResolver, "accessibility_autoclick_enabled", 0);
        this.mSwitchBar.setCheckedInternal(int1 == 1);
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        final SeekBarPreference mDelay = this.mDelay;
        boolean enabled = b;
        if (int1 == 1) {
            enabled = true;
        }
        mDelay.setEnabled(enabled);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mDelay && o instanceof Integer) {
            Settings.Secure.putInt(this.getContentResolver(), "accessibility_autoclick_delay", this.seekBarProgressToDelay((int)o));
            return true;
        }
        return false;
    }
    
    @Override
    protected void onPreferenceToggled(final String s, final boolean enabled) {
        Settings.Secure.putInt(this.getContentResolver(), s, (int)(enabled ? 1 : 0));
        this.mDelay.setEnabled(enabled);
    }
    
    @Override
    protected void onRemoveSwitchBarToggleSwitch() {
        super.onRemoveSwitchBarToggleSwitch();
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        this.onPreferenceToggled("accessibility_autoclick_enabled", b);
    }
}
