package com.android.settings.accessibility;

import android.widget.Toast;
import android.view.MotionEvent;
import android.os.storage.StorageManager;
import android.text.BidiFormatter;
import java.util.List;
import android.accessibilityservice.AccessibilityServiceInfo$CapabilityInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.Window;
import android.app.AlertDialog;
import android.view.View.OnTouchListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface$OnClickListener;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;

public class AccessibilityServiceWarning
{
    public static Dialog createCapabilitiesDialog(final Activity activity, final AccessibilityServiceInfo accessibilityServiceInfo, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        final AlertDialog create = new AlertDialog$Builder((Context)activity).setTitle((CharSequence)activity.getString(2131887535, new Object[] { getServiceName((Context)activity, accessibilityServiceInfo) })).setView(createEnableDialogContentView((Context)activity, accessibilityServiceInfo)).setPositiveButton(17039370, dialogInterface$OnClickListener).setNegativeButton(17039360, dialogInterface$OnClickListener).create();
        final -$$Lambda$AccessibilityServiceWarning$D3xqJyTKInilYjQAxG1fpVU1D1M instance = _$$Lambda$AccessibilityServiceWarning$D3xqJyTKInilYjQAxG1fpVU1D1M.INSTANCE;
        final Window window = create.getWindow();
        final WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.privateFlags |= 0x80000;
        window.setAttributes(attributes);
        create.create();
        create.getButton(-1).setOnTouchListener((View.OnTouchListener)instance);
        create.setCanceledOnTouchOutside(true);
        return (Dialog)create;
    }
    
    private static View createEnableDialogContentView(final Context context, final AccessibilityServiceInfo accessibilityServiceInfo) {
        final LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
        final View inflate = layoutInflater.inflate(2131558550, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131362109);
        if (isFullDiskEncrypted()) {
            textView.setText((CharSequence)context.getString(2131887531, new Object[] { getServiceName(context, accessibilityServiceInfo) }));
            textView.setVisibility(0);
        }
        else {
            textView.setVisibility(8);
        }
        ((TextView)inflate.findViewById(2131361964)).setText((CharSequence)context.getString(2131886952, new Object[] { getServiceName(context, accessibilityServiceInfo) }));
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131361963);
        final View inflate2 = layoutInflater.inflate(17367097, (ViewGroup)null);
        ((ImageView)inflate2.findViewById(16909166)).setImageDrawable(context.getDrawable(17302765));
        ((TextView)inflate2.findViewById(16909170)).setText((CharSequence)context.getString(2131886954));
        ((TextView)inflate2.findViewById(16909172)).setText((CharSequence)context.getString(2131886953));
        final List capabilityInfos = accessibilityServiceInfo.getCapabilityInfos(context);
        linearLayout.addView(inflate2);
        for (int size = capabilityInfos.size(), i = 0; i < size; ++i) {
            final AccessibilityServiceInfo$CapabilityInfo accessibilityServiceInfo$CapabilityInfo = capabilityInfos.get(i);
            final View inflate3 = layoutInflater.inflate(17367097, (ViewGroup)null);
            ((ImageView)inflate3.findViewById(16909166)).setImageDrawable(context.getDrawable(17302765));
            ((TextView)inflate3.findViewById(16909170)).setText((CharSequence)context.getString(accessibilityServiceInfo$CapabilityInfo.titleResId));
            ((TextView)inflate3.findViewById(16909172)).setText((CharSequence)context.getString(accessibilityServiceInfo$CapabilityInfo.descResId));
            linearLayout.addView(inflate3);
        }
        return inflate;
    }
    
    private static CharSequence getServiceName(final Context context, final AccessibilityServiceInfo accessibilityServiceInfo) {
        return BidiFormatter.getInstance(context.getResources().getConfiguration().getLocales().get(0)).unicodeWrap(accessibilityServiceInfo.getResolveInfo().loadLabel(context.getPackageManager()));
    }
    
    private static boolean isFullDiskEncrypted() {
        return StorageManager.isNonDefaultBlockEncrypted();
    }
}
