package com.android.settings.accessibility;

import com.android.settings.widget.SwitchBar;
import android.os.Bundle;
import android.content.ContentResolver;
import android.widget.Switch;
import android.provider.Settings;
import com.android.settingslib.accessibility.AccessibilityUtils;
import android.os.UserHandle;
import android.content.ComponentName;
import com.android.internal.accessibility.AccessibilityShortcutController;
import com.android.internal.accessibility.AccessibilityShortcutController$ToggleableFrameworkFeatureInfo;
import android.view.accessibility.AccessibilityManager;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.Handler;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import android.database.ContentObserver;
import com.android.settings.search.Indexable;

public class AccessibilityShortcutPreferenceFragment extends ToggleFeaturePreferenceFragment implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private final ContentObserver mContentObserver;
    private SwitchPreference mOnLockScreenSwitchPreference;
    private Preference mServicePreference;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return false;
            }
        };
    }
    
    public AccessibilityShortcutPreferenceFragment() {
        this.mContentObserver = new ContentObserver(new Handler()) {
            public void onChange(final boolean b) {
                AccessibilityShortcutPreferenceFragment.this.updatePreferences();
            }
        };
    }
    
    private static AccessibilityServiceInfo getServiceInfo(final Context context) {
        return AccessibilityManager.getInstance(context).getInstalledServiceInfoWithComponentName(getShortcutComponent(context));
    }
    
    public static CharSequence getServiceName(final Context context) {
        if (!shortcutFeatureAvailable(context)) {
            return context.getString(2131886168);
        }
        final AccessibilityServiceInfo serviceInfo = getServiceInfo(context);
        if (serviceInfo != null) {
            return serviceInfo.getResolveInfo().loadLabel(context.getPackageManager());
        }
        return AccessibilityShortcutController.getFrameworkShortcutFeaturesMap().get(getShortcutComponent(context)).getLabel(context);
    }
    
    private static ComponentName getShortcutComponent(final Context context) {
        final String shortcutTargetServiceComponentNameString = AccessibilityUtils.getShortcutTargetServiceComponentNameString(context, UserHandle.myUserId());
        if (shortcutTargetServiceComponentNameString == null) {
            return null;
        }
        return ComponentName.unflattenFromString(shortcutTargetServiceComponentNameString);
    }
    
    private static boolean shortcutFeatureAvailable(final Context context) {
        final ComponentName shortcutComponent = getShortcutComponent(context);
        boolean b = false;
        if (shortcutComponent == null) {
            return false;
        }
        if (AccessibilityShortcutController.getFrameworkShortcutFeaturesMap().containsKey(shortcutComponent)) {
            return true;
        }
        if (getServiceInfo(context) != null) {
            b = true;
        }
        return b;
    }
    
    private void updatePreferences() {
        final ContentResolver contentResolver = this.getContentResolver();
        final Context context = this.getContext();
        this.mServicePreference.setSummary(getServiceName(context));
        final boolean shortcutFeatureAvailable = shortcutFeatureAvailable(context);
        final boolean b = false;
        if (!shortcutFeatureAvailable) {
            Settings.Secure.putInt(this.getContentResolver(), "accessibility_shortcut_enabled", 0);
        }
        this.mSwitchBar.setChecked(Settings.Secure.getInt(contentResolver, "accessibility_shortcut_enabled", 1) == 1);
        boolean checked = b;
        if (Settings.Secure.getInt(contentResolver, "accessibility_shortcut_on_lock_screen", Settings.Secure.getInt(contentResolver, "accessibility_shortcut_dialog_shown", 0)) == 1) {
            checked = true;
        }
        this.mOnLockScreenSwitchPreference.setChecked(checked);
        this.mServicePreference.setEnabled(this.mToggleSwitch.isChecked());
        this.mOnLockScreenSwitchPreference.setEnabled(this.mToggleSwitch.isChecked());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887791;
    }
    
    @Override
    public int getMetricsCategory() {
        return 6;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082696;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mServicePreference = this.findPreference("accessibility_shortcut_service");
        (this.mOnLockScreenSwitchPreference = (SwitchPreference)this.findPreference("accessibility_shortcut_on_lock_screen")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new _$$Lambda$AccessibilityShortcutPreferenceFragment$v5UnURHl_V2dl7gTZw_kdUDDZ6E(this));
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131886189);
    }
    
    @Override
    protected void onInstallSwitchBarToggleSwitch() {
        super.onInstallSwitchBarToggleSwitch();
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)new _$$Lambda$AccessibilityShortcutPreferenceFragment$B1JGpZUcoOdF9ofKXLGiPDgZ6Bo(this));
    }
    
    @Override
    public void onPause() {
        this.getContentResolver().unregisterContentObserver(this.mContentObserver);
        super.onPause();
    }
    
    @Override
    protected void onPreferenceToggled(final String s, final boolean b) {
        Settings.Secure.putInt(this.getContentResolver(), s, (int)(b ? 1 : 0));
        this.updatePreferences();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updatePreferences();
        this.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("accessibility_shortcut_dialog_shown"), false, this.mContentObserver);
    }
}
