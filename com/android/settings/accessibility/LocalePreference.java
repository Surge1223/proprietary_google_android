package com.android.settings.accessibility;

import java.util.List;
import com.android.internal.app.LocalePicker$LocaleInfo;
import com.android.internal.app.LocalePicker;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.ListPreference;

public class LocalePreference extends ListPreference
{
    public LocalePreference(final Context context) {
        super(context);
        this.init(context);
    }
    
    public LocalePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(context);
    }
    
    public void init(final Context context) {
        int i = 0;
        final List allAssetLocales = LocalePicker.getAllAssetLocales(context, false);
        final int size = allAssetLocales.size();
        final CharSequence[] entries = new CharSequence[size + 1];
        final CharSequence[] entryValues = new CharSequence[size + 1];
        entries[0] = context.getResources().getString(2131888035);
        entryValues[0] = "";
        while (i < size) {
            final LocalePicker$LocaleInfo localePicker$LocaleInfo = allAssetLocales.get(i);
            entries[i + 1] = localePicker$LocaleInfo.toString();
            entryValues[i + 1] = localePicker$LocaleInfo.getLocale().toString();
            ++i;
        }
        this.setEntries(entries);
        this.setEntryValues(entryValues);
    }
}
