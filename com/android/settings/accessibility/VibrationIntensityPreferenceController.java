package com.android.settings.accessibility;

import android.os.Handler;
import android.os.Looper;
import android.database.ContentObserver;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.net.Uri;
import android.content.Context;
import android.os.Vibrator;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public abstract class VibrationIntensityPreferenceController extends BasePreferenceController implements LifecycleObserver, OnStart, OnStop
{
    private Preference mPreference;
    private final String mSettingKey;
    private final SettingObserver mSettingsContentObserver;
    protected final Vibrator mVibrator;
    
    public VibrationIntensityPreferenceController(final Context context, final String s, final String mSettingKey) {
        super(context, s);
        this.mVibrator = (Vibrator)this.mContext.getSystemService((Class)Vibrator.class);
        this.mSettingKey = mSettingKey;
        this.mSettingsContentObserver = (SettingObserver)new SettingObserver(mSettingKey) {
            public void onChange(final boolean b, final Uri uri) {
                VibrationIntensityPreferenceController.this.updateState(VibrationIntensityPreferenceController.this.mPreference);
            }
        };
    }
    
    public static CharSequence getIntensityString(final Context context, final int n) {
        if (context.getResources().getBoolean(2131034180)) {
            switch (n) {
                default: {
                    return "";
                }
                case 3: {
                    return context.getString(2131886206);
                }
                case 2: {
                    return context.getString(2131886208);
                }
                case 1: {
                    return context.getString(2131886207);
                }
                case 0: {
                    return context.getString(2131886209);
                }
            }
        }
        else {
            if (n == 0) {
                return context.getString(2131889421);
            }
            return context.getString(2131889422);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    protected abstract int getDefaultIntensity();
    
    @Override
    public CharSequence getSummary() {
        return getIntensityString(this.mContext, Settings.System.getInt(this.mContext.getContentResolver(), this.mSettingKey, this.getDefaultIntensity()));
    }
    
    @Override
    public void onStart() {
        this.mContext.getContentResolver().registerContentObserver(this.mSettingsContentObserver.uri, false, (ContentObserver)this.mSettingsContentObserver);
    }
    
    @Override
    public void onStop() {
        this.mContext.getContentResolver().unregisterContentObserver((ContentObserver)this.mSettingsContentObserver);
    }
    
    private static class SettingObserver extends ContentObserver
    {
        public final Uri uri;
        
        public SettingObserver(final String s) {
            super(new Handler(Looper.getMainLooper()));
            this.uri = Settings.System.getUriFor(s);
        }
    }
}
