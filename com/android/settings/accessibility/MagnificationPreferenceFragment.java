package com.android.settings.accessibility;

import android.support.v7.preference.Preference;
import android.os.Bundle;
import android.content.ContentResolver;
import android.content.res.Resources;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityManager;
import android.provider.Settings;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public final class MagnificationPreferenceFragment extends DashboardFragment
{
    static final int OFF = 0;
    static final int ON = 1;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private boolean mLaunchedFromSuw;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("magnification_preference_screen_title");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082691;
                return Arrays.asList(searchIndexableResource);
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return MagnificationPreferenceFragment.isApplicable(context.getResources());
            }
        };
    }
    
    public MagnificationPreferenceFragment() {
        this.mLaunchedFromSuw = false;
    }
    
    static CharSequence getConfigurationWarningStringForSecureSettingsKey(String string, final Context context) {
        if (!"accessibility_display_magnification_navbar_enabled".equals(string)) {
            return null;
        }
        if (Settings.Secure.getInt(context.getContentResolver(), "accessibility_display_magnification_navbar_enabled", 0) == 0) {
            return null;
        }
        final AccessibilityManager accessibilityManager = (AccessibilityManager)context.getSystemService("accessibility");
        string = Settings.Secure.getString(context.getContentResolver(), "accessibility_button_target_component");
        if (!TextUtils.isEmpty((CharSequence)string) && !"com.android.server.accessibility.MagnificationController".equals(string)) {
            final ComponentName unflattenFromString = ComponentName.unflattenFromString(string);
            final List enabledAccessibilityServiceList = accessibilityManager.getEnabledAccessibilityServiceList(-1);
            for (int size = enabledAccessibilityServiceList.size(), i = 0; i < size; ++i) {
                final AccessibilityServiceInfo accessibilityServiceInfo = enabledAccessibilityServiceList.get(i);
                if (accessibilityServiceInfo.getComponentName().equals((Object)unflattenFromString)) {
                    return context.getString(2131886176, new Object[] { accessibilityServiceInfo.getResolveInfo().loadLabel(context.getPackageManager()) });
                }
            }
        }
        return null;
    }
    
    static boolean isApplicable(final Resources resources) {
        return resources.getBoolean(17957024);
    }
    
    static boolean isChecked(final ContentResolver contentResolver, final String s) {
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, s, 0) == 1) {
            b = true;
        }
        return b;
    }
    
    static boolean setChecked(final ContentResolver contentResolver, final String s, final boolean b) {
        return Settings.Secure.putInt(contentResolver, s, (int)(b ? 1 : 0));
    }
    
    @Override
    public int getHelpResource() {
        return 2131887817;
    }
    
    @Override
    protected String getLogTag() {
        return "MagnificationPreferenceFragment";
    }
    
    @Override
    public int getMetricsCategory() {
        return 922;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082691;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        final Bundle arguments = this.getArguments();
        if (arguments != null && arguments.containsKey("from_suw")) {
            this.mLaunchedFromSuw = arguments.getBoolean("from_suw");
        }
        this.use(MagnificationGesturesPreferenceController.class).setIsFromSUW(this.mLaunchedFromSuw);
        this.use(MagnificationNavbarPreferenceController.class).setIsFromSUW(this.mLaunchedFromSuw);
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (this.mLaunchedFromSuw) {
            preference.setFragment(ToggleScreenMagnificationPreferenceFragmentForSetupWizard.class.getName());
            final Bundle extras = preference.getExtras();
            extras.putInt("help_uri_resource", 0);
            extras.putBoolean("need_search_icon_in_action_bar", false);
        }
        return super.onPreferenceTreeClick(preference);
    }
}
