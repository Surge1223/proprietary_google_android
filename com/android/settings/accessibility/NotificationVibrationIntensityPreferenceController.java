package com.android.settings.accessibility;

import android.content.Context;

public class NotificationVibrationIntensityPreferenceController extends VibrationIntensityPreferenceController
{
    static final String PREF_KEY = "notification_vibration_preference_screen";
    
    public NotificationVibrationIntensityPreferenceController(final Context context) {
        super(context, "notification_vibration_preference_screen", "notification_vibration_intensity");
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    protected int getDefaultIntensity() {
        return this.mVibrator.getDefaultNotificationVibrationIntensity();
    }
}
