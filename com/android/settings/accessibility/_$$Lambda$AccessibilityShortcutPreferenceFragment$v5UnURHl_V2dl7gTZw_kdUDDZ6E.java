package com.android.settings.accessibility;

import com.android.settings.widget.SwitchBar;
import android.os.Bundle;
import android.content.ContentResolver;
import android.widget.Switch;
import android.provider.Settings;
import com.android.settingslib.accessibility.AccessibilityUtils;
import android.os.UserHandle;
import android.content.ComponentName;
import com.android.internal.accessibility.AccessibilityShortcutController;
import com.android.internal.accessibility.AccessibilityShortcutController$ToggleableFrameworkFeatureInfo;
import android.view.accessibility.AccessibilityManager;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.Handler;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v14.preference.SwitchPreference;
import android.database.ContentObserver;
import com.android.settings.search.Indexable;
import android.support.v7.preference.Preference;

public final class _$$Lambda$AccessibilityShortcutPreferenceFragment$v5UnURHl_V2dl7gTZw_kdUDDZ6E implements OnPreferenceChangeListener
{
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return AccessibilityShortcutPreferenceFragment.lambda$onCreate$0(this.f$0, preference, o);
    }
}
