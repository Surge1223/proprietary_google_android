package com.android.settings.accessibility;

import android.os.Vibrator;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.content.pm.ResolveInfo;
import java.util.Set;
import android.os.Parcelable;
import com.android.settingslib.RestrictedLockUtils;
import android.support.v4.content.ContextCompat;
import com.android.settingslib.RestrictedPreference;
import android.os.Bundle;
import android.app.Activity;
import android.content.res.Resources;
import com.android.settingslib.accessibility.AccessibilityUtils;
import android.os.UserHandle;
import android.view.accessibility.AccessibilityManager;
import com.android.settings.Utils;
import android.view.KeyCharacterMap;
import android.content.ContentResolver;
import android.provider.Settings;
import android.provider.Settings;
import android.provider.Settings;
import com.android.internal.view.RotationPolicy;
import android.text.TextUtils;
import android.accessibilityservice.AccessibilityServiceInfo;
import java.util.Iterator;
import java.util.Collection;
import android.net.Uri;
import com.android.internal.accessibility.AccessibilityShortcutController$ToggleableFrameworkFeatureInfo;
import com.android.internal.accessibility.AccessibilityShortcutController;
import android.util.ArrayMap;
import java.util.HashMap;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v14.preference.SwitchPreference;
import com.android.internal.content.PackageMonitor;
import android.support.v7.preference.ListPreference;
import com.android.internal.view.RotationPolicy$RotationPolicyListener;
import android.content.ComponentName;
import android.os.Handler;
import android.app.admin.DevicePolicyManager;
import android.support.v7.preference.PreferenceCategory;
import java.util.Map;
import com.android.settings.search.Indexable;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class AccessibilitySettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener, Indexable
{
    private static final String[] CATEGORIES;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private static final String[] TOGGLE_ANIMATION_TARGETS;
    private Preference mAccessibilityShortcutPreferenceScreen;
    private Preference mAutoclickPreferenceScreen;
    private Preference mCaptioningPreferenceScreen;
    private final Map<String, PreferenceCategory> mCategoryToPrefCategoryMap;
    private Preference mDisplayDaltonizerPreferenceScreen;
    private Preference mDisplayMagnificationPreferenceScreen;
    private DevicePolicyManager mDpm;
    private Preference mFontSizePreferenceScreen;
    private final Handler mHandler;
    private int mLongPressTimeoutDefault;
    private final Map<String, String> mLongPressTimeoutValueToTitleMap;
    private final Map<ComponentName, PreferenceCategory> mPreBundledServiceComponentToCategoryMap;
    private final RotationPolicy$RotationPolicyListener mRotationPolicyListener;
    private ListPreference mSelectLongPressTimeoutPreference;
    private final Map<Preference, PreferenceCategory> mServicePreferenceToPreferenceCategoryMap;
    private final SettingsContentObserver mSettingsContentObserver;
    private final PackageMonitor mSettingsPackageMonitor;
    private SwitchPreference mToggleDisableAnimationsPreference;
    private SwitchPreference mToggleHighTextContrastPreference;
    private SwitchPreference mToggleInversionPreference;
    private SwitchPreference mToggleLargePointerIconPreference;
    private SwitchPreference mToggleLockScreenRotationPreference;
    private SwitchPreference mToggleMasterMonoPreference;
    private SwitchPreference mTogglePowerButtonEndsCallPreference;
    private final Runnable mUpdateRunnable;
    private Preference mVibrationPreferenceScreen;
    
    static {
        CATEGORIES = new String[] { "screen_reader_category", "audio_and_captions_category", "display_category", "interaction_control_category", "experimental_category", "user_installed_services_category" };
        TOGGLE_ANIMATION_TARGETS = new String[] { "window_animation_scale", "transition_animation_scale", "animator_duration_scale" };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("font_size_preference_screen");
                nonIndexableKeys.add("accessibility_settings_screen_zoom");
                nonIndexableKeys.add("tts_settings_preference");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082693;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    public AccessibilitySettings() {
        this.mLongPressTimeoutValueToTitleMap = new HashMap<String, String>();
        this.mHandler = new Handler();
        this.mUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                if (AccessibilitySettings.this.getActivity() != null) {
                    AccessibilitySettings.this.updateServicePreferences();
                }
            }
        };
        this.mSettingsPackageMonitor = new PackageMonitor() {
            private void sendUpdate() {
                AccessibilitySettings.this.mHandler.postDelayed(AccessibilitySettings.this.mUpdateRunnable, 1000L);
            }
            
            public void onPackageAdded(final String s, final int n) {
                this.sendUpdate();
            }
            
            public void onPackageAppeared(final String s, final int n) {
                this.sendUpdate();
            }
            
            public void onPackageDisappeared(final String s, final int n) {
                this.sendUpdate();
            }
            
            public void onPackageRemoved(final String s, final int n) {
                this.sendUpdate();
            }
        };
        this.mRotationPolicyListener = new RotationPolicy$RotationPolicyListener() {
            public void onChange() {
                AccessibilitySettings.this.updateLockScreenRotationCheckbox();
            }
        };
        this.mCategoryToPrefCategoryMap = (Map<String, PreferenceCategory>)new ArrayMap();
        this.mServicePreferenceToPreferenceCategoryMap = (Map<Preference, PreferenceCategory>)new ArrayMap();
        this.mPreBundledServiceComponentToCategoryMap = (Map<ComponentName, PreferenceCategory>)new ArrayMap();
        final Collection<AccessibilityShortcutController$ToggleableFrameworkFeatureInfo> values = (Collection<AccessibilityShortcutController$ToggleableFrameworkFeatureInfo>)AccessibilityShortcutController.getFrameworkShortcutFeaturesMap().values();
        final ArrayList list = new ArrayList<String>(values.size());
        final Iterator<AccessibilityShortcutController$ToggleableFrameworkFeatureInfo> iterator = values.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getSettingKey());
        }
        this.mSettingsContentObserver = new SettingsContentObserver(this.mHandler, list) {
            public void onChange(final boolean b, final Uri uri) {
                AccessibilitySettings.this.updateAllPreferences();
            }
        };
    }
    
    private static void configureMagnificationPreferenceIfNeeded(final Preference preference) {
        final Context context = preference.getContext();
        if (!MagnificationPreferenceFragment.isApplicable(context.getResources())) {
            preference.setFragment(ToggleScreenMagnificationPreferenceFragment.class.getName());
            MagnificationGesturesPreferenceController.populateMagnificationGesturesPreferenceExtras(preference.getExtras(), context);
        }
    }
    
    public static CharSequence getServiceSummary(final Context context, final AccessibilityServiceInfo accessibilityServiceInfo, final boolean b) {
        String s;
        if (b) {
            s = context.getString(2131886193);
        }
        else {
            s = context.getString(2131886192);
        }
        final CharSequence loadSummary = accessibilityServiceInfo.loadSummary(context.getPackageManager());
        String string = context.getString(R.string.preference_summary_default_combination, new Object[] { s, loadSummary });
        if (TextUtils.isEmpty(loadSummary)) {
            string = s;
        }
        return string;
    }
    
    private void handleLockScreenRotationPreferenceClick() {
        RotationPolicy.setRotationLockForAccessibility((Context)this.getActivity(), this.mToggleLockScreenRotationPreference.isChecked() ^ true);
    }
    
    private void handleLongPressTimeoutPreferenceChange(final String s) {
        Settings.Secure.putInt(this.getContentResolver(), "long_press_timeout", Integer.parseInt(s));
        this.mSelectLongPressTimeoutPreference.setSummary(this.mLongPressTimeoutValueToTitleMap.get(s));
    }
    
    private void handleToggleDisableAnimations() {
        String s;
        if (this.mToggleDisableAnimationsPreference.isChecked()) {
            s = "0";
        }
        else {
            s = "1";
        }
        final String[] toggle_ANIMATION_TARGETS = AccessibilitySettings.TOGGLE_ANIMATION_TARGETS;
        for (int length = toggle_ANIMATION_TARGETS.length, i = 0; i < length; ++i) {
            Settings.Global.putString(this.getContentResolver(), toggle_ANIMATION_TARGETS[i], s);
        }
    }
    
    private void handleToggleInversionPreferenceChange(final boolean b) {
        Settings.Secure.putInt(this.getContentResolver(), "accessibility_display_inversion_enabled", (int)(b ? 1 : 0));
    }
    
    private void handleToggleLargePointerIconPreferenceClick() {
        Settings.Secure.putInt(this.getContentResolver(), "accessibility_large_pointer_icon", (int)(this.mToggleLargePointerIconPreference.isChecked() ? 1 : 0));
    }
    
    private void handleToggleMasterMonoPreferenceClick() {
        Settings.System.putIntForUser(this.getContentResolver(), "master_mono", (int)(this.mToggleMasterMonoPreference.isChecked() ? 1 : 0), -2);
    }
    
    private void handleTogglePowerButtonEndsCallPreferenceClick() {
        final ContentResolver contentResolver = this.getContentResolver();
        int n;
        if (this.mTogglePowerButtonEndsCallPreference.isChecked()) {
            n = 2;
        }
        else {
            n = 1;
        }
        Settings.Secure.putInt(contentResolver, "incall_power_button_behavior", n);
    }
    
    private void handleToggleTextContrastPreferenceClick() {
        Settings.Secure.putInt(this.getContentResolver(), "high_text_contrast_enabled", (int)(this.mToggleHighTextContrastPreference.isChecked() ? 1 : 0));
    }
    
    private void initializeAllPreferences() {
        final int n = 0;
        for (int i = 0; i < AccessibilitySettings.CATEGORIES.length; ++i) {
            this.mCategoryToPrefCategoryMap.put(AccessibilitySettings.CATEGORIES[i], (PreferenceCategory)this.findPreference(AccessibilitySettings.CATEGORIES[i]));
        }
        this.mToggleHighTextContrastPreference = (SwitchPreference)this.findPreference("toggle_high_text_contrast_preference");
        (this.mToggleInversionPreference = (SwitchPreference)this.findPreference("toggle_inversion_preference")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mTogglePowerButtonEndsCallPreference = (SwitchPreference)this.findPreference("toggle_power_button_ends_call_preference");
        if (!KeyCharacterMap.deviceHasKey(26) || !Utils.isVoiceCapable((Context)this.getActivity())) {
            this.mCategoryToPrefCategoryMap.get("interaction_control_category").removePreference(this.mTogglePowerButtonEndsCallPreference);
        }
        this.mToggleLockScreenRotationPreference = (SwitchPreference)this.findPreference("toggle_lock_screen_rotation_preference");
        if (!RotationPolicy.isRotationSupported((Context)this.getActivity())) {
            this.mCategoryToPrefCategoryMap.get("interaction_control_category").removePreference(this.mToggleLockScreenRotationPreference);
        }
        this.mToggleLargePointerIconPreference = (SwitchPreference)this.findPreference("toggle_large_pointer_icon");
        this.mToggleDisableAnimationsPreference = (SwitchPreference)this.findPreference("toggle_disable_animations");
        this.mToggleMasterMonoPreference = (SwitchPreference)this.findPreference("toggle_master_mono");
        (this.mSelectLongPressTimeoutPreference = (ListPreference)this.findPreference("select_long_press_timeout_preference")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        if (this.mLongPressTimeoutValueToTitleMap.size() == 0) {
            final String[] stringArray = this.getResources().getStringArray(2130903130);
            this.mLongPressTimeoutDefault = Integer.parseInt(stringArray[0]);
            final String[] stringArray2 = this.getResources().getStringArray(2130903129);
            for (int length = stringArray.length, j = n; j < length; ++j) {
                this.mLongPressTimeoutValueToTitleMap.put(stringArray[j], stringArray2[j]);
            }
        }
        this.mCaptioningPreferenceScreen = this.findPreference("captioning_preference_screen");
        configureMagnificationPreferenceIfNeeded(this.mDisplayMagnificationPreferenceScreen = this.findPreference("magnification_preference_screen"));
        this.mFontSizePreferenceScreen = this.findPreference("font_size_preference_screen");
        this.mAutoclickPreferenceScreen = this.findPreference("autoclick_preference_screen");
        this.mDisplayDaltonizerPreferenceScreen = this.findPreference("daltonizer_preference_screen");
        this.mAccessibilityShortcutPreferenceScreen = this.findPreference("accessibility_shortcut_preference");
        this.mVibrationPreferenceScreen = this.findPreference("vibration_preference_screen");
    }
    
    private void initializePreBundledServicesMapFromArray(final String s, int i) {
        final String[] stringArray = this.getResources().getStringArray(i);
        final PreferenceCategory preferenceCategory = this.mCategoryToPrefCategoryMap.get(s);
        for (i = 0; i < stringArray.length; ++i) {
            this.mPreBundledServiceComponentToCategoryMap.put(ComponentName.unflattenFromString(stringArray[i]), preferenceCategory);
        }
    }
    
    public static boolean isColorTransformAccelerated(final Context context) {
        return context.getResources().getBoolean(17957019);
    }
    
    private void updateAccessibilityShortcut(final Preference preference) {
        if (AccessibilityManager.getInstance((Context)this.getActivity()).getInstalledAccessibilityServiceList().isEmpty()) {
            this.mAccessibilityShortcutPreferenceScreen.setSummary(this.getString(2131886169));
            this.mAccessibilityShortcutPreferenceScreen.setEnabled(false);
        }
        else {
            this.mAccessibilityShortcutPreferenceScreen.setEnabled(true);
            CharSequence summary;
            if (AccessibilityUtils.isShortcutEnabled(this.getContext(), UserHandle.myUserId())) {
                summary = AccessibilityShortcutPreferenceFragment.getServiceName(this.getContext());
            }
            else {
                summary = this.getString(2131886154);
            }
            this.mAccessibilityShortcutPreferenceScreen.setSummary(summary);
        }
    }
    
    private void updateAllPreferences() {
        this.updateSystemPreferences();
        this.updateServicePreferences();
    }
    
    private void updateAutoclickSummary(final Preference preference) {
        final int int1 = Settings.Secure.getInt(this.getContentResolver(), "accessibility_autoclick_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        if (!b) {
            preference.setSummary(2131886154);
            return;
        }
        preference.setSummary(ToggleAutoclickPreferenceFragment.getAutoclickPreferenceSummary(this.getResources(), Settings.Secure.getInt(this.getContentResolver(), "accessibility_autoclick_delay", 600)));
    }
    
    private void updateDisableAnimationsToggle() {
        final boolean b = true;
        final String[] toggle_ANIMATION_TARGETS = AccessibilitySettings.TOGGLE_ANIMATION_TARGETS;
        final int length = toggle_ANIMATION_TARGETS.length;
        int n = 0;
        boolean checked;
        while (true) {
            checked = b;
            if (n >= length) {
                break;
            }
            if (!TextUtils.equals((CharSequence)Settings.Global.getString(this.getContentResolver(), toggle_ANIMATION_TARGETS[n]), (CharSequence)"0")) {
                checked = false;
                break;
            }
            ++n;
        }
        this.mToggleDisableAnimationsPreference.setChecked(checked);
    }
    
    private void updateFeatureSummary(final String s, final Preference preference) {
        final ContentResolver contentResolver = this.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, s, 0) == 1) {
            b = true;
        }
        int summary;
        if (b) {
            summary = 2131886155;
        }
        else {
            summary = 2131886154;
        }
        preference.setSummary(summary);
    }
    
    private void updateFontSizeSummary(final Preference preference) {
        final float float1 = Settings.System.getFloat(this.getContext().getContentResolver(), "font_scale", 1.0f);
        final Resources resources = this.getContext().getResources();
        preference.setSummary(resources.getStringArray(2130903118)[ToggleFontSizePreferenceFragment.fontSizeValueToIndex(float1, resources.getStringArray(2130903119))]);
    }
    
    private void updateLockScreenRotationCheckbox() {
        final Activity activity = this.getActivity();
        if (activity != null) {
            this.mToggleLockScreenRotationPreference.setChecked(RotationPolicy.isRotationLocked((Context)activity) ^ true);
        }
    }
    
    private void updateMagnificationSummary(final Preference preference) {
        final int int1 = Settings.Secure.getInt(this.getContentResolver(), "accessibility_display_magnification_enabled", 0);
        boolean b = true;
        final boolean b2 = int1 == 1;
        if (Settings.Secure.getInt(this.getContentResolver(), "accessibility_display_magnification_navbar_enabled", 0) != 1) {
            b = false;
        }
        int summary;
        if (!b2 && !b) {
            summary = 2131886154;
        }
        else if (!b2 && b) {
            summary = 2131886179;
        }
        else if (b2 && !b) {
            summary = 2131886175;
        }
        else {
            summary = 2131886181;
        }
        preference.setSummary(summary);
    }
    
    private void updateMasterMono() {
        final int intForUser = Settings.System.getIntForUser(this.getContentResolver(), "master_mono", 0, -2);
        boolean checked = true;
        if (intForUser != 1) {
            checked = false;
        }
        this.mToggleMasterMonoPreference.setChecked(checked);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887771;
    }
    
    @Override
    public int getMetricsCategory() {
        return 2;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082693);
        this.initializeAllPreferences();
        this.mDpm = (DevicePolicyManager)this.getActivity().getSystemService("device_policy");
    }
    
    @Override
    public void onPause() {
        this.mSettingsPackageMonitor.unregister();
        this.mSettingsContentObserver.unregister(this.getContentResolver());
        if (RotationPolicy.isRotationSupported((Context)this.getActivity())) {
            RotationPolicy.unregisterRotationPolicyListener((Context)this.getActivity(), this.mRotationPolicyListener);
        }
        super.onPause();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mSelectLongPressTimeoutPreference == preference) {
            this.handleLongPressTimeoutPreferenceChange((String)o);
            return true;
        }
        if (this.mToggleInversionPreference == preference) {
            this.handleToggleInversionPreferenceChange((boolean)o);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (this.mToggleHighTextContrastPreference == preference) {
            this.handleToggleTextContrastPreferenceClick();
            return true;
        }
        if (this.mTogglePowerButtonEndsCallPreference == preference) {
            this.handleTogglePowerButtonEndsCallPreferenceClick();
            return true;
        }
        if (this.mToggleLockScreenRotationPreference == preference) {
            this.handleLockScreenRotationPreferenceClick();
            return true;
        }
        if (this.mToggleLargePointerIconPreference == preference) {
            this.handleToggleLargePointerIconPreferenceClick();
            return true;
        }
        if (this.mToggleDisableAnimationsPreference == preference) {
            this.handleToggleDisableAnimations();
            return true;
        }
        if (this.mToggleMasterMonoPreference == preference) {
            this.handleToggleMasterMonoPreferenceClick();
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateAllPreferences();
        this.mSettingsPackageMonitor.register((Context)this.getActivity(), this.getActivity().getMainLooper(), false);
        this.mSettingsContentObserver.register(this.getContentResolver());
        if (RotationPolicy.isRotationSupported((Context)this.getActivity())) {
            RotationPolicy.registerRotationPolicyListener((Context)this.getActivity(), this.mRotationPolicyListener);
        }
    }
    
    protected void updateServicePreferences() {
        final ArrayList<Preference> list = new ArrayList<Preference>(this.mServicePreferenceToPreferenceCategoryMap.keySet());
        for (int i = 0; i < list.size(); ++i) {
            final Preference preference = list.get(i);
            this.mServicePreferenceToPreferenceCategoryMap.get(preference).removePreference(preference);
        }
        this.initializePreBundledServicesMapFromArray("screen_reader_category", 2130903104);
        this.initializePreBundledServicesMapFromArray("audio_and_captions_category", 2130903101);
        this.initializePreBundledServicesMapFromArray("display_category", 2130903102);
        this.initializePreBundledServicesMapFromArray("interaction_control_category", 2130903103);
        final AccessibilityManager instance = AccessibilityManager.getInstance((Context)this.getActivity());
        final List installedAccessibilityServiceList = instance.getInstalledAccessibilityServiceList();
        final List enabledAccessibilityServiceList = instance.getEnabledAccessibilityServiceList(-1);
        final Set<ComponentName> enabledServicesFromSettings = AccessibilityUtils.getEnabledServicesFromSettings((Context)this.getActivity());
        final List permittedAccessibilityServices = this.mDpm.getPermittedAccessibilityServices(UserHandle.myUserId());
        final PreferenceCategory preferenceCategory = this.mCategoryToPrefCategoryMap.get("user_installed_services_category");
        if (this.findPreference("user_installed_services_category") == null) {
            this.getPreferenceScreen().addPreference(preferenceCategory);
        }
        for (int j = 0; j < installedAccessibilityServiceList.size(); ++j) {
            final AccessibilityServiceInfo accessibilityServiceInfo = installedAccessibilityServiceList.get(j);
            final ResolveInfo resolveInfo = accessibilityServiceInfo.getResolveInfo();
            final RestrictedPreference restrictedPreference = new RestrictedPreference(preferenceCategory.getContext());
            final String string = resolveInfo.loadLabel(this.getPackageManager()).toString();
            Drawable drawable;
            if (resolveInfo.getIconResource() == 0) {
                drawable = ContextCompat.getDrawable(this.getContext(), 2131689472);
            }
            else {
                drawable = resolveInfo.loadIcon(this.getPackageManager());
            }
            final ServiceInfo serviceInfo = resolveInfo.serviceInfo;
            final String packageName = serviceInfo.packageName;
            final ComponentName componentName = new ComponentName(packageName, serviceInfo.name);
            restrictedPreference.setKey(componentName.flattenToString());
            restrictedPreference.setTitle(string);
            Utils.setSafeIcon(restrictedPreference, drawable);
            final boolean contains = enabledServicesFromSettings.contains(componentName);
            String s = accessibilityServiceInfo.loadDescription(this.getPackageManager());
            if (TextUtils.isEmpty((CharSequence)s)) {
                s = this.getString(2131886184);
            }
            if (contains && AccessibilityUtils.hasServiceCrashed(packageName, serviceInfo.name, enabledAccessibilityServiceList)) {
                restrictedPreference.setSummary(2131886194);
                s = this.getString(2131886148);
            }
            else {
                restrictedPreference.setSummary(getServiceSummary(this.getContext(), accessibilityServiceInfo, contains));
            }
            if (permittedAccessibilityServices != null && !permittedAccessibilityServices.contains(packageName) && !contains) {
                final RestrictedLockUtils.EnforcedAdmin checkIfAccessibilityServiceDisallowed = RestrictedLockUtils.checkIfAccessibilityServiceDisallowed((Context)this.getActivity(), packageName, UserHandle.myUserId());
                if (checkIfAccessibilityServiceDisallowed != null) {
                    restrictedPreference.setDisabledByAdmin(checkIfAccessibilityServiceDisallowed);
                }
                else {
                    restrictedPreference.setEnabled(false);
                }
            }
            else {
                restrictedPreference.setEnabled(true);
            }
            restrictedPreference.setFragment(ToggleAccessibilityServicePreferenceFragment.class.getName());
            restrictedPreference.setPersistent(true);
            final Bundle extras = restrictedPreference.getExtras();
            extras.putString("preference_key", restrictedPreference.getKey());
            extras.putBoolean("checked", contains);
            extras.putString("title", string);
            extras.putParcelable("resolve_info", (Parcelable)resolveInfo);
            extras.putString("summary", s);
            final String settingsActivityName = accessibilityServiceInfo.getSettingsActivityName();
            if (!TextUtils.isEmpty((CharSequence)settingsActivityName)) {
                extras.putString("settings_title", this.getString(2131886167));
                extras.putString("settings_component_name", new ComponentName(packageName, settingsActivityName).flattenToString());
            }
            extras.putParcelable("component_name", (Parcelable)componentName);
            PreferenceCategory preferenceCategory2 = preferenceCategory;
            if (this.mPreBundledServiceComponentToCategoryMap.containsKey(componentName)) {
                preferenceCategory2 = this.mPreBundledServiceComponentToCategoryMap.get(componentName);
            }
            restrictedPreference.setOrder(-1);
            preferenceCategory2.addPreference(restrictedPreference);
            this.mServicePreferenceToPreferenceCategoryMap.put(restrictedPreference, preferenceCategory2);
        }
        if (preferenceCategory.getPreferenceCount() == 0) {
            this.getPreferenceScreen().removePreference(preferenceCategory);
        }
    }
    
    protected void updateSystemPreferences() {
        if (isColorTransformAccelerated(this.getContext())) {
            final PreferenceCategory preferenceCategory = this.mCategoryToPrefCategoryMap.get("experimental_category");
            final PreferenceCategory preferenceCategory2 = this.mCategoryToPrefCategoryMap.get("display_category");
            preferenceCategory.removePreference(this.mToggleInversionPreference);
            preferenceCategory.removePreference(this.mDisplayDaltonizerPreferenceScreen);
            this.mToggleInversionPreference.setOrder(this.mToggleLargePointerIconPreference.getOrder());
            this.mDisplayDaltonizerPreferenceScreen.setOrder(this.mToggleInversionPreference.getOrder());
            this.mToggleInversionPreference.setSummary(2131889403);
            preferenceCategory2.addPreference(this.mToggleInversionPreference);
            preferenceCategory2.addPreference(this.mDisplayDaltonizerPreferenceScreen);
        }
        final SwitchPreference mToggleHighTextContrastPreference = this.mToggleHighTextContrastPreference;
        final int int1 = Settings.Secure.getInt(this.getContentResolver(), "high_text_contrast_enabled", 0);
        final boolean b = true;
        mToggleHighTextContrastPreference.setChecked(int1 == 1);
        this.mToggleInversionPreference.setChecked(Settings.Secure.getInt(this.getContentResolver(), "accessibility_display_inversion_enabled", 0) == 1);
        if (KeyCharacterMap.deviceHasKey(26) && Utils.isVoiceCapable((Context)this.getActivity())) {
            this.mTogglePowerButtonEndsCallPreference.setChecked(Settings.Secure.getInt(this.getContentResolver(), "incall_power_button_behavior", 1) == 2);
        }
        this.updateLockScreenRotationCheckbox();
        this.mToggleLargePointerIconPreference.setChecked(Settings.Secure.getInt(this.getContentResolver(), "accessibility_large_pointer_icon", 0) != 0 && b);
        this.updateDisableAnimationsToggle();
        this.updateMasterMono();
        final String value = String.valueOf(Settings.Secure.getInt(this.getContentResolver(), "long_press_timeout", this.mLongPressTimeoutDefault));
        this.mSelectLongPressTimeoutPreference.setValue(value);
        this.mSelectLongPressTimeoutPreference.setSummary(this.mLongPressTimeoutValueToTitleMap.get(value));
        this.updateVibrationSummary(this.mVibrationPreferenceScreen);
        this.updateFeatureSummary("accessibility_captioning_enabled", this.mCaptioningPreferenceScreen);
        this.updateFeatureSummary("accessibility_display_daltonizer_enabled", this.mDisplayDaltonizerPreferenceScreen);
        this.updateMagnificationSummary(this.mDisplayMagnificationPreferenceScreen);
        this.updateFontSizeSummary(this.mFontSizePreferenceScreen);
        this.updateAutoclickSummary(this.mAutoclickPreferenceScreen);
        this.updateAccessibilityShortcut(this.mAccessibilityShortcutPreferenceScreen);
    }
    
    void updateVibrationSummary(final Preference preference) {
        final Context context = this.getContext();
        final Vibrator vibrator = (Vibrator)context.getSystemService((Class)Vibrator.class);
        final int int1 = Settings.System.getInt(context.getContentResolver(), "notification_vibration_intensity", vibrator.getDefaultNotificationVibrationIntensity());
        final CharSequence intensityString = VibrationIntensityPreferenceController.getIntensityString(context, int1);
        final int int2 = Settings.System.getInt(context.getContentResolver(), "haptic_feedback_intensity", vibrator.getDefaultHapticFeedbackIntensity());
        final CharSequence intensityString2 = VibrationIntensityPreferenceController.getIntensityString(context, int2);
        if (this.mVibrationPreferenceScreen == null) {
            this.mVibrationPreferenceScreen = this.findPreference("vibration_preference_screen");
        }
        if (int1 == int2) {
            this.mVibrationPreferenceScreen.setSummary(intensityString);
        }
        else {
            this.mVibrationPreferenceScreen.setSummary(this.getString(2131886211, new Object[] { intensityString, intensityString2 }));
        }
    }
}
