package com.android.settings.accessibility;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.database.ContentObserver;
import android.util.Log;
import android.media.AudioAttributes$Builder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import java.util.Iterator;
import android.provider.Settings;
import java.util.function.Function;
import java.util.Comparator;
import java.util.Collection;
import java.util.ArrayList;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import android.content.Context;
import android.util.ArrayMap;
import java.util.Map;
import com.android.settings.widget.RadioButtonPickerFragment;

public abstract class VibrationPreferenceFragment extends RadioButtonPickerFragment
{
    static final String KEY_INTENSITY_HIGH = "intensity_high";
    static final String KEY_INTENSITY_LOW = "intensity_low";
    static final String KEY_INTENSITY_MEDIUM = "intensity_medium";
    static final String KEY_INTENSITY_OFF = "intensity_off";
    static final String KEY_INTENSITY_ON = "intensity_on";
    private final Map<String, VibrationIntensityCandidateInfo> mCandidates;
    private final SettingsObserver mSettingsObserver;
    
    public VibrationPreferenceFragment() {
        this.mCandidates = (Map<String, VibrationIntensityCandidateInfo>)new ArrayMap();
        this.mSettingsObserver = new SettingsObserver();
    }
    
    private void loadCandidates(final Context context) {
        if (context.getResources().getBoolean(2131034180)) {
            this.mCandidates.put("intensity_off", new VibrationIntensityCandidateInfo("intensity_off", 2131886209, 0));
            this.mCandidates.put("intensity_low", new VibrationIntensityCandidateInfo("intensity_low", 2131886207, 1));
            this.mCandidates.put("intensity_medium", new VibrationIntensityCandidateInfo("intensity_medium", 2131886208, 2));
            this.mCandidates.put("intensity_high", new VibrationIntensityCandidateInfo("intensity_high", 2131886206, 3));
        }
        else {
            this.mCandidates.put("intensity_off", new VibrationIntensityCandidateInfo("intensity_off", 2131889421, 0));
            this.mCandidates.put("intensity_on", new VibrationIntensityCandidateInfo("intensity_on", 2131889422, this.getDefaultVibrationIntensity()));
        }
    }
    
    @Override
    protected List<? extends CandidateInfo> getCandidates() {
        final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<CandidateInfo>(this.mCandidates.values());
        list.sort((Comparator<? super CandidateInfo>)Comparator.comparing((Function<? super Object, ? extends Comparable>)_$$Lambda$_Oh9z60fg9jQX72D1CuzQSHZqtM.INSTANCE).reversed());
        return (List<? extends CandidateInfo>)list;
    }
    
    @Override
    protected String getDefaultKey() {
        final int int1 = Settings.System.getInt(this.getContext().getContentResolver(), this.getVibrationIntensitySetting(), this.getDefaultVibrationIntensity());
        for (final VibrationIntensityCandidateInfo vibrationIntensityCandidateInfo : this.mCandidates.values()) {
            final int intensity = vibrationIntensityCandidateInfo.getIntensity();
            final boolean b = false;
            final boolean b2 = intensity == int1;
            boolean b3 = b;
            if (vibrationIntensityCandidateInfo.getKey().equals("intensity_on")) {
                b3 = b;
                if (int1 != 0) {
                    b3 = true;
                }
            }
            if (!b2 && !b3) {
                continue;
            }
            return vibrationIntensityCandidateInfo.getKey();
        }
        return null;
    }
    
    protected abstract int getDefaultVibrationIntensity();
    
    protected int getPreviewVibrationAudioAttributesUsage() {
        return 0;
    }
    
    protected abstract String getVibrationIntensitySetting();
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mSettingsObserver.register();
        if (this.mCandidates.isEmpty()) {
            this.loadCandidates(context);
        }
    }
    
    public void onDetach() {
        super.onDetach();
        this.mSettingsObserver.unregister();
    }
    
    protected void onVibrationIntensitySelected(final int n) {
    }
    
    protected void playVibrationPreview() {
        final Vibrator vibrator = (Vibrator)this.getContext().getSystemService((Class)Vibrator.class);
        final VibrationEffect value = VibrationEffect.get(0);
        final AudioAttributes$Builder audioAttributes$Builder = new AudioAttributes$Builder();
        audioAttributes$Builder.setUsage(this.getPreviewVibrationAudioAttributesUsage());
        vibrator.vibrate(value, audioAttributes$Builder.build());
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        final VibrationIntensityCandidateInfo vibrationIntensityCandidateInfo = this.mCandidates.get(s);
        if (vibrationIntensityCandidateInfo == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Tried to set unknown intensity (key=");
            sb.append(s);
            sb.append(")!");
            Log.e("VibrationPreferenceFragment", sb.toString());
            return false;
        }
        Settings.System.putInt(this.getContext().getContentResolver(), this.getVibrationIntensitySetting(), vibrationIntensityCandidateInfo.getIntensity());
        this.onVibrationIntensitySelected(vibrationIntensityCandidateInfo.getIntensity());
        return true;
    }
    
    private class SettingsObserver extends ContentObserver
    {
        public SettingsObserver() {
            super(new Handler());
        }
        
        public void onChange(final boolean b, final Uri uri) {
            VibrationPreferenceFragment.this.updateCandidates();
            VibrationPreferenceFragment.this.playVibrationPreview();
        }
        
        public void register() {
            VibrationPreferenceFragment.this.getContext().getContentResolver().registerContentObserver(Settings.System.getUriFor(VibrationPreferenceFragment.this.getVibrationIntensitySetting()), false, (ContentObserver)this);
        }
        
        public void unregister() {
            VibrationPreferenceFragment.this.getContext().getContentResolver().unregisterContentObserver((ContentObserver)this);
        }
    }
    
    class VibrationIntensityCandidateInfo extends CandidateInfo
    {
        private int mIntensity;
        private String mKey;
        private int mLabelId;
        
        public VibrationIntensityCandidateInfo(final String mKey, final int mLabelId, final int mIntensity) {
            super(true);
            this.mKey = mKey;
            this.mLabelId = mLabelId;
            this.mIntensity = mIntensity;
        }
        
        public int getIntensity() {
            return this.mIntensity;
        }
        
        @Override
        public String getKey() {
            return this.mKey;
        }
        
        @Override
        public Drawable loadIcon() {
            return null;
        }
        
        @Override
        public CharSequence loadLabel() {
            return VibrationPreferenceFragment.this.getContext().getString(this.mLabelId);
        }
    }
}
