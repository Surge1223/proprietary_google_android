package com.android.settings.accessibility;

import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class DividerAllowedBelowPreference extends Preference
{
    public DividerAllowedBelowPreference(final Context context) {
        super(context);
    }
    
    public DividerAllowedBelowPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public DividerAllowedBelowPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        preferenceViewHolder.setDividerAllowedBelow(true);
    }
}
