package com.android.settings.accessibility;

import android.provider.Settings;
import android.content.ContentResolver;
import java.util.Collection;
import java.util.ArrayList;
import android.os.Handler;
import java.util.List;
import android.database.ContentObserver;

abstract class SettingsContentObserver extends ContentObserver
{
    private final List<String> mKeysToObserve;
    
    public SettingsContentObserver(final Handler handler) {
        super(handler);
        (this.mKeysToObserve = new ArrayList<String>(2)).add("accessibility_enabled");
        this.mKeysToObserve.add("enabled_accessibility_services");
    }
    
    public SettingsContentObserver(final Handler handler, final List<String> list) {
        this(handler);
        this.mKeysToObserve.addAll(list);
    }
    
    public void register(final ContentResolver contentResolver) {
        for (int i = 0; i < this.mKeysToObserve.size(); ++i) {
            contentResolver.registerContentObserver(Settings.Secure.getUriFor((String)this.mKeysToObserve.get(i)), false, (ContentObserver)this);
        }
    }
    
    public void unregister(final ContentResolver contentResolver) {
        contentResolver.unregisterContentObserver((ContentObserver)this);
    }
}
