package com.android.settings.accessibility;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsActivity;
import android.view.View;
import android.content.pm.ResolveInfo;
import android.content.Context;
import android.os.Bundle;
import com.android.settings.widget.ToggleSwitch;
import com.android.settings.widget.SwitchBar;
import android.content.Intent;
import com.android.settings.SettingsPreferenceFragment;

public abstract class ToggleFeaturePreferenceFragment extends SettingsPreferenceFragment
{
    protected String mPreferenceKey;
    protected Intent mSettingsIntent;
    protected CharSequence mSettingsTitle;
    protected SwitchBar mSwitchBar;
    protected ToggleSwitch mToggleSwitch;
    
    private void installActionBarToggleSwitch() {
        this.mSwitchBar.show();
        this.onInstallSwitchBarToggleSwitch();
    }
    
    private void removeActionBarToggleSwitch() {
        this.mToggleSwitch.setOnBeforeCheckedChangeListener(null);
        this.onRemoveSwitchBarToggleSwitch();
        this.mSwitchBar.hide();
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.installActionBarToggleSwitch();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.getPreferenceScreenResId() <= 0) {
            this.setPreferenceScreen(this.getPreferenceManager().createPreferenceScreen((Context)this.getActivity()));
        }
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.removeActionBarToggleSwitch();
    }
    
    protected void onInstallSwitchBarToggleSwitch() {
    }
    
    protected abstract void onPreferenceToggled(final String p0, final boolean p1);
    
    protected void onProcessArguments(final Bundle bundle) {
        this.mPreferenceKey = bundle.getString("preference_key");
        if (bundle.containsKey("checked")) {
            this.mSwitchBar.setCheckedInternal(bundle.getBoolean("checked"));
        }
        if (bundle.containsKey("resolve_info")) {
            this.getActivity().setTitle((CharSequence)((ResolveInfo)bundle.getParcelable("resolve_info")).loadLabel(this.getPackageManager()).toString());
        }
        else if (bundle.containsKey("title")) {
            this.setTitle(bundle.getString("title"));
        }
        if (bundle.containsKey("summary_res")) {
            this.mFooterPreferenceMixin.createFooterPreference().setTitle(bundle.getInt("summary_res"));
        }
        else if (bundle.containsKey("summary")) {
            this.mFooterPreferenceMixin.createFooterPreference().setTitle(bundle.getCharSequence("summary"));
        }
    }
    
    protected void onRemoveSwitchBarToggleSwitch() {
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.updateSwitchBarText(this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar());
        this.mToggleSwitch = this.mSwitchBar.getSwitch();
        this.onProcessArguments(this.getArguments());
        if (this.mSettingsTitle != null && this.mSettingsIntent != null) {
            final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
            final Preference preference = new Preference(preferenceScreen.getContext());
            preference.setTitle(this.mSettingsTitle);
            preference.setIconSpaceReserved(true);
            preference.setIntent(this.mSettingsIntent);
            preferenceScreen.addPreference(preference);
        }
    }
    
    public void setTitle(final String title) {
        this.getActivity().setTitle((CharSequence)title);
    }
    
    protected void updateSwitchBarText(final SwitchBar switchBar) {
        switchBar.setSwitchBarText(2131886185, 2131886185);
    }
}
