package com.android.settings.accessibility;

import android.widget.Toast;
import android.os.storage.StorageManager;
import android.text.BidiFormatter;
import java.util.List;
import android.accessibilityservice.AccessibilityServiceInfo$CapabilityInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.WindowManager.LayoutParams;
import android.view.Window;
import android.app.AlertDialog;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface$OnClickListener;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public final class _$$Lambda$AccessibilityServiceWarning$D3xqJyTKInilYjQAxG1fpVU1D1M implements View.OnTouchListener
{
    public final boolean onTouch(final View view, final MotionEvent motionEvent) {
        return AccessibilityServiceWarning.lambda$createCapabilitiesDialog$0(view, motionEvent);
    }
}
