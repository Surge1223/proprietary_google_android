package com.android.settings.accessibility;

import android.widget.TextView;
import com.android.internal.widget.SubtitleView;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.view.accessibility.CaptioningManager;

public class PresetPreference extends ListDialogPreference
{
    private final CaptioningManager mCaptioningManager;
    
    public PresetPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setDialogLayoutResource(2131558572);
        this.setListItemLayoutResource(2131558694);
        this.mCaptioningManager = (CaptioningManager)context.getSystemService("captioning");
    }
    
    @Override
    protected void onBindListItem(final View view, final int n) {
        final View viewById = view.findViewById(2131362466);
        final SubtitleView subtitleView = (SubtitleView)view.findViewById(2131362463);
        CaptionPropertiesFragment.applyCaptionProperties(this.mCaptioningManager, subtitleView, viewById, this.getValueAt(n));
        subtitleView.setTextSize(32.0f * this.getContext().getResources().getDisplayMetrics().density);
        final CharSequence title = this.getTitleAt(n);
        if (title != null) {
            ((TextView)view.findViewById(2131362668)).setText(title);
        }
    }
    
    @Override
    public boolean shouldDisableDependents() {
        return this.getValue() != -1 || super.shouldDisableDependents();
    }
}
