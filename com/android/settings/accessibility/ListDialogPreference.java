package com.android.settings.accessibility;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v7.preference.Preference;
import android.widget.BaseAdapter;
import android.os.Parcelable;
import android.app.Dialog;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.AbsListView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.res.TypedArray;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.CustomDialogPreference;

public abstract class ListDialogPreference extends CustomDialogPreference
{
    private CharSequence[] mEntryTitles;
    private int[] mEntryValues;
    private int mListItemLayout;
    private OnValueChangedListener mOnValueChangedListener;
    private int mValue;
    private int mValueIndex;
    private boolean mValueSet;
    
    public ListDialogPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    protected int getIndexForValue(final int n) {
        final int[] mEntryValues = this.mEntryValues;
        if (mEntryValues != null) {
            for (int length = mEntryValues.length, i = 0; i < length; ++i) {
                if (mEntryValues[i] == n) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    @Override
    public CharSequence getSummary() {
        if (this.mValueIndex >= 0) {
            return this.getTitleAt(this.mValueIndex);
        }
        return null;
    }
    
    protected CharSequence getTitleAt(final int n) {
        if (this.mEntryTitles != null && this.mEntryTitles.length > n) {
            return this.mEntryTitles[n];
        }
        return null;
    }
    
    public int getValue() {
        return this.mValue;
    }
    
    protected int getValueAt(final int n) {
        return this.mEntryValues[n];
    }
    
    protected abstract void onBindListItem(final View p0, final int p1);
    
    @Override
    protected Object onGetDefaultValue(final TypedArray typedArray, final int n) {
        return typedArray.getInt(n, 0);
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        super.onPrepareDialogBuilder(alertDialog$Builder, dialogInterface$OnClickListener);
        final View inflate = LayoutInflater.from(this.getContext()).inflate(this.getDialogLayoutResource(), (ViewGroup)null);
        final ListPreferenceAdapter adapter = new ListPreferenceAdapter();
        final AbsListView absListView = (AbsListView)inflate.findViewById(16908298);
        absListView.setAdapter((ListAdapter)adapter);
        absListView.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                if (ListDialogPreference.this.callChangeListener((int)n2)) {
                    ListDialogPreference.this.setValue((int)n2);
                }
                final Dialog dialog = ListDialogPreference.this.getDialog();
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        final int indexForValue = this.getIndexForValue(this.mValue);
        if (indexForValue != -1) {
            absListView.setSelection(indexForValue);
        }
        alertDialog$Builder.setView(inflate);
        alertDialog$Builder.setPositiveButton((CharSequence)null, (DialogInterface$OnClickListener)null);
    }
    
    @Override
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable != null && parcelable.getClass().equals(SavedState.class)) {
            final SavedState savedState = (SavedState)parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            this.setValue(savedState.value);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }
    
    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (this.isPersistent()) {
            return onSaveInstanceState;
        }
        final SavedState savedState = new SavedState(onSaveInstanceState);
        savedState.value = this.getValue();
        return (Parcelable)savedState;
    }
    
    @Override
    protected void onSetInitialValue(final boolean b, final Object o) {
        int value;
        if (b) {
            value = this.getPersistedInt(this.mValue);
        }
        else {
            value = (int)o;
        }
        this.setValue(value);
    }
    
    public void setListItemLayoutResource(final int mListItemLayout) {
        this.mListItemLayout = mListItemLayout;
    }
    
    public void setOnValueChangedListener(final OnValueChangedListener mOnValueChangedListener) {
        this.mOnValueChangedListener = mOnValueChangedListener;
    }
    
    public void setTitles(final CharSequence[] mEntryTitles) {
        this.mEntryTitles = mEntryTitles;
    }
    
    public void setValue(final int mValue) {
        final boolean b = this.mValue != mValue;
        if (b || !this.mValueSet) {
            this.mValue = mValue;
            this.mValueIndex = this.getIndexForValue(mValue);
            this.mValueSet = true;
            this.persistInt(mValue);
            if (b) {
                this.notifyDependencyChange(this.shouldDisableDependents());
                this.notifyChanged();
            }
            if (this.mOnValueChangedListener != null) {
                this.mOnValueChangedListener.onValueChanged(this, mValue);
            }
        }
    }
    
    public void setValues(final int[] mEntryValues) {
        this.mEntryValues = mEntryValues;
        if (this.mValueSet && this.mValueIndex == -1) {
            this.mValueIndex = this.getIndexForValue(this.mValue);
        }
    }
    
    private class ListPreferenceAdapter extends BaseAdapter
    {
        private LayoutInflater mInflater;
        
        public int getCount() {
            return ListDialogPreference.this.mEntryValues.length;
        }
        
        public Integer getItem(final int n) {
            return ListDialogPreference.this.mEntryValues[n];
        }
        
        public long getItemId(final int n) {
            return ListDialogPreference.this.mEntryValues[n];
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View inflate = view;
            if (view == null) {
                if (this.mInflater == null) {
                    this.mInflater = LayoutInflater.from(viewGroup.getContext());
                }
                inflate = this.mInflater.inflate(ListDialogPreference.this.mListItemLayout, viewGroup, false);
            }
            ListDialogPreference.this.onBindListItem(inflate, n);
            return inflate;
        }
        
        public boolean hasStableIds() {
            return true;
        }
    }
    
    public interface OnValueChangedListener
    {
        void onValueChanged(final ListDialogPreference p0, final int p1);
    }
    
    private static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        public int value;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel) {
            super(parcel);
            this.value = parcel.readInt();
        }
        
        public SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.value);
        }
    }
}
