package com.android.settings.accessibility;

import android.widget.Switch;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.preference.ListPreference;
import com.android.settings.widget.SwitchBar;
import android.support.v7.preference.Preference;

public class ToggleDaltonizerPreferenceFragment extends ToggleFeaturePreferenceFragment implements OnPreferenceChangeListener, OnSwitchChangeListener
{
    private ListPreference mType;
    
    private void initPreferences() {
        final String string = Integer.toString(Settings.Secure.getInt(this.getContentResolver(), "accessibility_display_daltonizer", 12));
        this.mType.setValue(string);
        this.mType.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        if (this.mType.findIndexOfValue(string) < 0) {
            this.mType.setSummary(this.getString(2131887198, new Object[] { this.getString(2131889173) }));
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887803;
    }
    
    @Override
    public int getMetricsCategory() {
        return 5;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082690;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mType = (ListPreference)this.findPreference("type");
        if (!AccessibilitySettings.isColorTransformAccelerated((Context)this.getActivity())) {
            this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131886150);
        }
        this.initPreferences();
    }
    
    @Override
    protected void onInstallSwitchBarToggleSwitch() {
        super.onInstallSwitchBarToggleSwitch();
        final SwitchBar mSwitchBar = this.mSwitchBar;
        final int int1 = Settings.Secure.getInt(this.getContentResolver(), "accessibility_display_daltonizer_enabled", 0);
        boolean checkedInternal = true;
        if (int1 != 1) {
            checkedInternal = false;
        }
        mSwitchBar.setCheckedInternal(checkedInternal);
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mType) {
            Settings.Secure.putInt(this.getContentResolver(), "accessibility_display_daltonizer", Integer.parseInt((String)o));
            preference.setSummary("%s");
        }
        return true;
    }
    
    @Override
    protected void onPreferenceToggled(final String s, final boolean b) {
        Settings.Secure.putInt(this.getContentResolver(), "accessibility_display_daltonizer_enabled", (int)(b ? 1 : 0));
    }
    
    @Override
    protected void onRemoveSwitchBarToggleSwitch() {
        super.onRemoveSwitchBarToggleSwitch();
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        this.onPreferenceToggled(this.mPreferenceKey, b);
    }
    
    @Override
    protected void updateSwitchBarText(final SwitchBar switchBar) {
        switchBar.setSwitchBarText(2131886147, 2131886147);
    }
}
