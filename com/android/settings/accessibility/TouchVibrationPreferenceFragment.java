package com.android.settings.accessibility;

import android.content.ContentResolver;
import android.provider.Settings;
import android.os.Vibrator;

public class TouchVibrationPreferenceFragment extends VibrationPreferenceFragment
{
    @Override
    protected int getDefaultVibrationIntensity() {
        return ((Vibrator)this.getContext().getSystemService((Class)Vibrator.class)).getDefaultHapticFeedbackIntensity();
    }
    
    @Override
    public int getMetricsCategory() {
        return 1294;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082697;
    }
    
    @Override
    protected int getPreviewVibrationAudioAttributesUsage() {
        return 13;
    }
    
    @Override
    protected String getVibrationIntensitySetting() {
        return "haptic_feedback_intensity";
    }
    
    public void onVibrationIntensitySelected(int n) {
        final int n2 = 0;
        if (n != 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        final ContentResolver contentResolver = this.getContext().getContentResolver();
        if (n != 0) {
            n = 1;
        }
        else {
            n = n2;
        }
        Settings.System.putInt(contentResolver, "haptic_feedback_enabled", n);
    }
}
