package com.android.settings.accessibility;

import android.widget.TextView;
import com.android.internal.widget.SubtitleView;
import android.view.View;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;

public class EdgeTypePreference extends ListDialogPreference
{
    public EdgeTypePreference(final Context context, final AttributeSet set) {
        super(context, set);
        final Resources resources = context.getResources();
        this.setValues(resources.getIntArray(2130903088));
        this.setTitles(resources.getStringArray(2130903087));
        this.setDialogLayoutResource(2131558572);
        this.setListItemLayoutResource(2131558694);
    }
    
    @Override
    protected void onBindListItem(final View view, final int n) {
        final SubtitleView subtitleView = (SubtitleView)view.findViewById(2131362463);
        subtitleView.setForegroundColor(-1);
        subtitleView.setBackgroundColor(0);
        subtitleView.setTextSize(32.0f * this.getContext().getResources().getDisplayMetrics().density);
        subtitleView.setEdgeType(this.getValueAt(n));
        subtitleView.setEdgeColor(-16777216);
        final CharSequence title = this.getTitleAt(n);
        if (title != null) {
            ((TextView)view.findViewById(2131362668)).setText(title);
        }
    }
    
    @Override
    public boolean shouldDisableDependents() {
        return this.getValue() == 0 || super.shouldDisableDependents();
    }
}
