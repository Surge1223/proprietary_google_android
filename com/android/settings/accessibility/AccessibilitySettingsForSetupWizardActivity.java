package com.android.settings.accessibility;

import com.android.settingslib.core.instrumentation.Instrumentable;
import android.content.Context;
import com.android.settings.core.SubSettingLauncher;
import android.support.v7.preference.Preference;
import android.support.v14.preference.PreferenceFragment;
import android.view.Menu;
import android.os.Bundle;
import com.android.settings.SettingsActivity;

public class AccessibilitySettingsForSetupWizardActivity extends SettingsActivity
{
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        return true;
    }
    
    @Override
    public boolean onNavigateUp() {
        this.onBackPressed();
        this.getWindow().getDecorView().sendAccessibilityEvent(32);
        return true;
    }
    
    @Override
    public boolean onPreferenceStartFragment(final PreferenceFragment preferenceFragment, final Preference preference) {
        Bundle extras;
        if ((extras = preference.getExtras()) == null) {
            extras = new Bundle();
        }
        int metricsCategory = 0;
        extras.putInt("help_uri_resource", 0);
        extras.putBoolean("need_search_icon_in_action_bar", false);
        final SubSettingLauncher setArguments = new SubSettingLauncher((Context)this).setDestination(preference.getFragment()).setArguments(extras);
        if (preferenceFragment instanceof Instrumentable) {
            metricsCategory = ((Instrumentable)preferenceFragment).getMetricsCategory();
        }
        setArguments.setSourceMetricsCategory(metricsCategory).launch();
        return true;
    }
    
    protected void onRestoreInstanceState(final Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.setTitle(bundle.getCharSequence("activity_title"));
    }
    
    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        bundle.putCharSequence("activity_title", this.getTitle());
        super.onSaveInstanceState(bundle);
    }
}
