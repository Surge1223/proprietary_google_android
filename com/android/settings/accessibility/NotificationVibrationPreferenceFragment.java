package com.android.settings.accessibility;

import android.os.Vibrator;

public class NotificationVibrationPreferenceFragment extends VibrationPreferenceFragment
{
    @Override
    protected int getDefaultVibrationIntensity() {
        return ((Vibrator)this.getContext().getSystemService((Class)Vibrator.class)).getDefaultNotificationVibrationIntensity();
    }
    
    @Override
    public int getMetricsCategory() {
        return 1293;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082692;
    }
    
    @Override
    protected int getPreviewVibrationAudioAttributesUsage() {
        return 5;
    }
    
    @Override
    protected String getVibrationIntensitySetting() {
        return "notification_vibration_intensity";
    }
}
