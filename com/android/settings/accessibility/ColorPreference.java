package com.android.settings.accessibility;

import android.text.TextUtils;
import android.support.v7.preference.PreferenceViewHolder;
import android.widget.TextView;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.view.View;
import android.graphics.Color;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;

public class ColorPreference extends ListDialogPreference
{
    private ColorDrawable mPreviewColor;
    private boolean mPreviewEnabled;
    
    public ColorPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setDialogLayoutResource(2131558572);
        this.setListItemLayoutResource(2131558489);
    }
    
    @Override
    protected CharSequence getTitleAt(int green) {
        final CharSequence title = super.getTitleAt(green);
        if (title != null) {
            return title;
        }
        final int value = this.getValueAt(green);
        final int red = Color.red(value);
        green = Color.green(value);
        return this.getContext().getString(2131887025, new Object[] { red, green, Color.blue(value) });
    }
    
    @Override
    protected void onBindListItem(final View view, final int n) {
        final int value = this.getValueAt(n);
        final int alpha = Color.alpha(value);
        final ImageView imageView = (ImageView)view.findViewById(2131361998);
        if (alpha < 255) {
            imageView.setBackgroundResource(2131231300);
        }
        else {
            imageView.setBackground((Drawable)null);
        }
        final Drawable drawable = imageView.getDrawable();
        if (drawable instanceof ColorDrawable) {
            ((ColorDrawable)drawable).setColor(value);
        }
        else {
            imageView.setImageDrawable((Drawable)new ColorDrawable(value));
        }
        final CharSequence title = this.getTitleAt(n);
        if (title != null) {
            ((TextView)view.findViewById(2131362668)).setText(title);
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        if (this.mPreviewEnabled) {
            final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131361997);
            final int value = this.getValue();
            if (Color.alpha(value) < 255) {
                imageView.setBackgroundResource(2131231300);
            }
            else {
                imageView.setBackground((Drawable)null);
            }
            if (this.mPreviewColor == null) {
                imageView.setImageDrawable((Drawable)(this.mPreviewColor = new ColorDrawable(value)));
            }
            else {
                this.mPreviewColor.setColor(value);
            }
            final CharSequence summary = this.getSummary();
            if (!TextUtils.isEmpty(summary)) {
                imageView.setContentDescription(summary);
            }
            else {
                imageView.setContentDescription((CharSequence)null);
            }
            float alpha;
            if (this.isEnabled()) {
                alpha = 1.0f;
            }
            else {
                alpha = 0.2f;
            }
            imageView.setAlpha(alpha);
        }
    }
    
    @Override
    public boolean shouldDisableDependents() {
        return Color.alpha(this.getValue()) == 0 || super.shouldDisableDependents();
    }
}
