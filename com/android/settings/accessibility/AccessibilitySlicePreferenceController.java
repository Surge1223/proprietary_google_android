package com.android.settings.accessibility;

import com.android.settingslib.accessibility.AccessibilityUtils;
import android.provider.Settings;
import java.util.Iterator;
import android.view.accessibility.AccessibilityManager;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.ComponentName;
import com.android.settings.core.TogglePreferenceController;

public class AccessibilitySlicePreferenceController extends TogglePreferenceController
{
    private final int OFF;
    private final int ON;
    private final ComponentName mComponentName;
    
    public AccessibilitySlicePreferenceController(final Context context, final String s) {
        super(context, s);
        this.ON = 1;
        this.OFF = 0;
        this.mComponentName = ComponentName.unflattenFromString(this.getPreferenceKey());
        if (this.mComponentName != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal Component Name from: ");
        sb.append(s);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private AccessibilityServiceInfo getAccessibilityServiceInfo() {
        for (final AccessibilityServiceInfo accessibilityServiceInfo : ((AccessibilityManager)this.mContext.getSystemService((Class)AccessibilityManager.class)).getInstalledAccessibilityServiceList()) {
            if (this.mComponentName.equals((Object)accessibilityServiceInfo.getComponentName())) {
                return accessibilityServiceInfo;
            }
        }
        return null;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.getAccessibilityServiceInfo() == null) {
            n = 2;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        final AccessibilityServiceInfo accessibilityServiceInfo = this.getAccessibilityServiceInfo();
        CharSequence serviceSummary;
        if (accessibilityServiceInfo == null) {
            serviceSummary = "";
        }
        else {
            serviceSummary = AccessibilitySettings.getServiceSummary(this.mContext, accessibilityServiceInfo, this.isChecked());
        }
        return serviceSummary;
    }
    
    @Override
    public boolean isChecked() {
        final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), "accessibility_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b && AccessibilityUtils.getEnabledServicesFromSettings(this.mContext).contains(this.mComponentName);
    }
    
    @Override
    public boolean isSliceable() {
        return true;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        final AccessibilityServiceInfo accessibilityServiceInfo = this.getAccessibilityServiceInfo();
        boolean b2 = false;
        if (accessibilityServiceInfo == null) {
            return false;
        }
        AccessibilityUtils.setAccessibilityServiceState(this.mContext, this.mComponentName, b);
        if (b == this.isChecked()) {
            b2 = true;
        }
        return b2;
    }
}
