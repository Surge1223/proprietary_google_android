package com.android.settings.accessibility;

import android.os.Bundle;
import android.text.TextUtils;
import android.os.Parcelable;
import android.content.ComponentName;
import android.content.pm.ServiceInfo;
import java.util.Iterator;
import android.view.accessibility.AccessibilityManager;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class AccessibilitySettingsForSetupWizard extends SettingsPreferenceFragment implements OnPreferenceChangeListener
{
    private Preference mDisplayMagnificationPreference;
    private Preference mScreenReaderPreference;
    private Preference mSelectToSpeakPreference;
    
    private static void configureMagnificationPreferenceIfNeeded(final Preference preference) {
        final Context context = preference.getContext();
        if (!MagnificationPreferenceFragment.isApplicable(context.getResources())) {
            preference.setFragment(ToggleScreenMagnificationPreferenceFragmentForSetupWizard.class.getName());
            MagnificationGesturesPreferenceController.populateMagnificationGesturesPreferenceExtras(preference.getExtras(), context);
        }
    }
    
    private AccessibilityServiceInfo findService(final String s, final String s2) {
        for (final AccessibilityServiceInfo accessibilityServiceInfo : ((AccessibilityManager)this.getActivity().getSystemService((Class)AccessibilityManager.class)).getInstalledAccessibilityServiceList()) {
            final ServiceInfo serviceInfo = accessibilityServiceInfo.getResolveInfo().serviceInfo;
            if (s.equals(serviceInfo.packageName) && s2.equals(serviceInfo.name)) {
                return accessibilityServiceInfo;
            }
        }
        return null;
    }
    
    private void updateAccessibilityServicePreference(final Preference preference, final AccessibilityServiceInfo accessibilityServiceInfo) {
        if (accessibilityServiceInfo == null) {
            this.getPreferenceScreen().removePreference(preference);
            return;
        }
        final ServiceInfo serviceInfo = accessibilityServiceInfo.getResolveInfo().serviceInfo;
        final String string = accessibilityServiceInfo.getResolveInfo().loadLabel(this.getPackageManager()).toString();
        preference.setTitle(string);
        final ComponentName componentName = new ComponentName(serviceInfo.packageName, serviceInfo.name);
        preference.setKey(componentName.flattenToString());
        final Bundle extras = preference.getExtras();
        extras.putParcelable("component_name", (Parcelable)componentName);
        extras.putString("preference_key", preference.getKey());
        extras.putString("title", string);
        String s;
        if (TextUtils.isEmpty((CharSequence)(s = accessibilityServiceInfo.loadDescription(this.getPackageManager())))) {
            s = this.getString(2131886184);
        }
        extras.putString("summary", s);
    }
    
    @Override
    public int getMetricsCategory() {
        return 367;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.setHasOptionsMenu(false);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082694);
        this.mDisplayMagnificationPreference = this.findPreference("screen_magnification_preference");
        this.mScreenReaderPreference = this.findPreference("screen_reader_preference");
        this.mSelectToSpeakPreference = this.findPreference("select_to_speak_preference");
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return false;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (this.mDisplayMagnificationPreference == preference) {
            this.mDisplayMagnificationPreference.getExtras().putBoolean("from_suw", true);
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateAccessibilityServicePreference(this.mScreenReaderPreference, this.findService("com.google.android.marvin.talkback", "com.google.android.marvin.talkback.TalkBackService"));
        this.updateAccessibilityServicePreference(this.mSelectToSpeakPreference, this.findService("com.google.android.marvin.talkback", "com.google.android.accessibility.selecttospeak.SelectToSpeakService"));
        configureMagnificationPreferenceIfNeeded(this.mDisplayMagnificationPreference);
    }
}
