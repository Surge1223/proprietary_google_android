package com.android.settings.accessibility;

import android.text.TextUtils;
import com.android.settings.widget.ToggleSwitch;
import android.view.MenuInflater;
import android.view.Menu;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.password.ConfirmDeviceCredentialActivity;
import android.content.DialogInterface;
import android.provider.Settings;
import android.content.Intent;
import com.android.settingslib.accessibility.AccessibilityUtils;
import android.os.storage.StorageManager;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.content.Context;
import android.view.accessibility.AccessibilityManager;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.UserHandle;
import android.net.Uri;
import android.os.Handler;
import com.android.internal.widget.LockPatternUtils;
import android.content.ComponentName;
import android.content.DialogInterface$OnClickListener;

public class ToggleAccessibilityServicePreferenceFragment extends ToggleFeaturePreferenceFragment implements DialogInterface$OnClickListener
{
    private ComponentName mComponentName;
    private LockPatternUtils mLockPatternUtils;
    private final SettingsContentObserver mSettingsContentObserver;
    private int mShownDialogId;
    
    public ToggleAccessibilityServicePreferenceFragment() {
        this.mSettingsContentObserver = new SettingsContentObserver(new Handler()) {
            public void onChange(final boolean b, final Uri uri) {
                ToggleAccessibilityServicePreferenceFragment.this.updateSwitchBarToggleSwitch();
            }
        };
    }
    
    private String createConfirmCredentialReasonMessage() {
        int n = 2131887532;
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(UserHandle.myUserId());
        if (keyguardStoredPasswordQuality != 65536) {
            if (keyguardStoredPasswordQuality == 131072 || keyguardStoredPasswordQuality == 196608) {
                n = 2131887534;
            }
        }
        else {
            n = 2131887533;
        }
        return this.getString(n, new Object[] { this.getAccessibilityServiceInfo().getResolveInfo().loadLabel(this.getPackageManager()) });
    }
    
    private AccessibilityServiceInfo getAccessibilityServiceInfo() {
        final List installedAccessibilityServiceList = AccessibilityManager.getInstance((Context)this.getActivity()).getInstalledAccessibilityServiceList();
        for (int size = installedAccessibilityServiceList.size(), i = 0; i < size; ++i) {
            final AccessibilityServiceInfo accessibilityServiceInfo = installedAccessibilityServiceList.get(i);
            final ResolveInfo resolveInfo = accessibilityServiceInfo.getResolveInfo();
            if (this.mComponentName.getPackageName().equals(resolveInfo.serviceInfo.packageName) && this.mComponentName.getClassName().equals(resolveInfo.serviceInfo.name)) {
                return accessibilityServiceInfo;
            }
        }
        return null;
    }
    
    private void handleConfirmServiceEnabled(final boolean checkedInternal) {
        this.mSwitchBar.setCheckedInternal(checkedInternal);
        this.getArguments().putBoolean("checked", checkedInternal);
        this.onPreferenceToggled(this.mPreferenceKey, checkedInternal);
    }
    
    private boolean isFullDiskEncrypted() {
        return StorageManager.isNonDefaultBlockEncrypted();
    }
    
    private void updateSwitchBarToggleSwitch() {
        this.mSwitchBar.setCheckedInternal(AccessibilityUtils.getEnabledServicesFromSettings((Context)this.getActivity()).contains(this.mComponentName));
    }
    
    public int getDialogMetricsCategory(final int n) {
        if (n == 1) {
            return 583;
        }
        return 584;
    }
    
    public int getMetricsCategory() {
        return 4;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1) {
            if (n2 == -1) {
                this.handleConfirmServiceEnabled(true);
                if (this.isFullDiskEncrypted()) {
                    this.mLockPatternUtils.clearEncryptionPassword();
                    Settings.Global.putInt(this.getContentResolver(), "require_password_to_decrypt", 0);
                }
            }
            else {
                this.handleConfirmServiceEnabled(false);
            }
        }
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        boolean b = false;
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case -1: {
                if (this.mShownDialogId != 1) {
                    this.handleConfirmServiceEnabled(false);
                    break;
                }
                if (this.isFullDiskEncrypted()) {
                    this.startActivityForResult(ConfirmDeviceCredentialActivity.createIntent(this.createConfirmCredentialReasonMessage(), null), 1);
                    break;
                }
                this.handleConfirmServiceEnabled(true);
                break;
            }
            case -2: {
                if (this.mShownDialogId == 2) {
                    b = true;
                }
                this.handleConfirmServiceEnabled(b);
                break;
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mLockPatternUtils = new LockPatternUtils((Context)this.getActivity());
    }
    
    public Dialog onCreateDialog(final int n) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 2: {
                this.mShownDialogId = 2;
                final AccessibilityServiceInfo accessibilityServiceInfo = this.getAccessibilityServiceInfo();
                if (accessibilityServiceInfo == null) {
                    return null;
                }
                return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)this.getString(2131887420, new Object[] { accessibilityServiceInfo.getResolveInfo().loadLabel(this.getPackageManager()) })).setMessage((CharSequence)this.getString(2131887419, new Object[] { accessibilityServiceInfo.getResolveInfo().loadLabel(this.getPackageManager()) })).setCancelable(true).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
            }
            case 1: {
                this.mShownDialogId = 1;
                final AccessibilityServiceInfo accessibilityServiceInfo2 = this.getAccessibilityServiceInfo();
                if (accessibilityServiceInfo2 == null) {
                    return null;
                }
                return AccessibilityServiceWarning.createCapabilitiesDialog(this.getActivity(), accessibilityServiceInfo2, (DialogInterface$OnClickListener)this);
            }
        }
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
    }
    
    @Override
    protected void onInstallSwitchBarToggleSwitch() {
        super.onInstallSwitchBarToggleSwitch();
        this.mToggleSwitch.setOnBeforeCheckedChangeListener((ToggleSwitch.OnBeforeCheckedChangeListener)new ToggleSwitch.OnBeforeCheckedChangeListener() {
            @Override
            public boolean onBeforeCheckedChanged(final ToggleSwitch toggleSwitch, final boolean b) {
                if (b) {
                    ToggleAccessibilityServicePreferenceFragment.this.mSwitchBar.setCheckedInternal(false);
                    ToggleAccessibilityServicePreferenceFragment.this.getArguments().putBoolean("checked", false);
                    SettingsPreferenceFragment.this.showDialog(1);
                }
                else {
                    ToggleAccessibilityServicePreferenceFragment.this.mSwitchBar.setCheckedInternal(true);
                    ToggleAccessibilityServicePreferenceFragment.this.getArguments().putBoolean("checked", true);
                    SettingsPreferenceFragment.this.showDialog(2);
                }
                return true;
            }
        });
    }
    
    public void onPause() {
        this.mSettingsContentObserver.unregister(this.getContentResolver());
        super.onPause();
    }
    
    public void onPreferenceToggled(final String s, final boolean b) {
        AccessibilityUtils.setAccessibilityServiceState((Context)this.getActivity(), ComponentName.unflattenFromString(s), b);
    }
    
    @Override
    protected void onProcessArguments(final Bundle bundle) {
        super.onProcessArguments(bundle);
        final String string = bundle.getString("settings_title");
        final String string2 = bundle.getString("settings_component_name");
        if (!TextUtils.isEmpty((CharSequence)string) && !TextUtils.isEmpty((CharSequence)string2)) {
            final Intent setComponent = new Intent("android.intent.action.MAIN").setComponent(ComponentName.unflattenFromString(string2.toString()));
            if (!this.getPackageManager().queryIntentActivities(setComponent, 0).isEmpty()) {
                this.mSettingsTitle = string;
                this.mSettingsIntent = setComponent;
                this.setHasOptionsMenu(true);
            }
        }
        this.mComponentName = (ComponentName)bundle.getParcelable("component_name");
    }
    
    public void onResume() {
        this.mSettingsContentObserver.register(this.getContentResolver());
        this.updateSwitchBarToggleSwitch();
        super.onResume();
    }
}
