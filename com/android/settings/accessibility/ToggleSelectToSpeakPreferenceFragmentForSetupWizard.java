package com.android.settings.accessibility;

import android.os.Bundle;

public class ToggleSelectToSpeakPreferenceFragmentForSetupWizard extends ToggleAccessibilityServicePreferenceFragment
{
    private boolean mToggleSwitchWasInitiallyChecked;
    
    @Override
    public int getMetricsCategory() {
        return 371;
    }
    
    @Override
    protected void onProcessArguments(final Bundle bundle) {
        super.onProcessArguments(bundle);
        this.mToggleSwitchWasInitiallyChecked = this.mToggleSwitch.isChecked();
    }
    
    public void onStop() {
        if (this.mToggleSwitch.isChecked() != this.mToggleSwitchWasInitiallyChecked) {
            this.mMetricsFeatureProvider.action(this.getContext(), 817, this.mToggleSwitch.isChecked());
        }
        super.onStop();
    }
}
