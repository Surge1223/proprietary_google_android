package com.android.settings.accessibility;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.os.Bundle;
import android.content.res.Configuration;
import android.provider.Settings;
import com.android.settings.PreviewSeekBarPreferenceFragment;

public class ToggleFontSizePreferenceFragment extends PreviewSeekBarPreferenceFragment
{
    private float[] mValues;
    
    public static int fontSizeValueToIndex(final float n, final String[] array) {
        float float1 = Float.parseFloat(array[0]);
        for (int i = 1; i < array.length; ++i) {
            final float float2 = Float.parseFloat(array[i]);
            if (n < (float2 - float1) * 0.5f + float1) {
                return i - 1;
            }
            float1 = float2;
        }
        return array.length - 1;
    }
    
    @Override
    protected void commit() {
        if (this.getContext() == null) {
            return;
        }
        Settings.System.putFloat(this.getContext().getContentResolver(), "font_scale", this.mValues[this.mCurrentIndex]);
    }
    
    @Override
    protected Configuration createConfig(Configuration configuration, final int n) {
        configuration = new Configuration(configuration);
        configuration.fontScale = this.mValues[n];
        return configuration;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887812;
    }
    
    @Override
    public int getMetricsCategory() {
        return 340;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mActivityLayoutResId = 2131558570;
        int i = 0;
        this.mPreviewSampleResIds = new int[] { 2131558571 };
        final Resources resources = this.getContext().getResources();
        final ContentResolver contentResolver = this.getContext().getContentResolver();
        this.mEntries = resources.getStringArray(2130903118);
        final String[] stringArray = resources.getStringArray(2130903119);
        this.mInitialIndex = fontSizeValueToIndex(Settings.System.getFloat(contentResolver, "font_scale", 1.0f), stringArray);
        this.mValues = new float[stringArray.length];
        while (i < stringArray.length) {
            this.mValues[i] = Float.parseFloat(stringArray[i]);
            ++i;
        }
        this.getActivity().setTitle(2131889484);
    }
}
