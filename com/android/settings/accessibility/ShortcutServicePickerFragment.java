package com.android.settings.accessibility;

import android.content.pm.PackageManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Binder;
import android.app.Fragment;
import android.os.Bundle;
import android.os.IBinder;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.provider.Settings;
import android.app.Activity;
import android.text.TextUtils;
import com.android.settings.widget.RadioButtonPreference;
import com.android.settingslib.accessibility.AccessibilityUtils;
import android.os.UserHandle;
import java.util.Iterator;
import java.util.Map;
import android.accessibilityservice.AccessibilityServiceInfo;
import com.android.internal.accessibility.AccessibilityShortcutController$ToggleableFrameworkFeatureInfo;
import android.content.ComponentName;
import com.android.internal.accessibility.AccessibilityShortcutController;
import java.util.ArrayList;
import android.view.accessibility.AccessibilityManager;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import com.android.settings.widget.RadioButtonPickerFragment;

public class ShortcutServicePickerFragment extends RadioButtonPickerFragment
{
    private void onServiceConfirmed(final String s) {
        this.onRadioButtonConfirmed(s);
    }
    
    @Override
    protected List<? extends CandidateInfo> getCandidates() {
        final List installedAccessibilityServiceList = ((AccessibilityManager)this.getContext().getSystemService((Class)AccessibilityManager.class)).getInstalledAccessibilityServiceList();
        final int size = installedAccessibilityServiceList.size();
        final ArrayList list = new ArrayList<FrameworkCandidateInfo>(size);
        final Map frameworkShortcutFeaturesMap = AccessibilityShortcutController.getFrameworkShortcutFeaturesMap();
        for (final ComponentName componentName : frameworkShortcutFeaturesMap.keySet()) {
            int n;
            if (componentName.equals((Object)AccessibilityShortcutController.COLOR_INVERSION_COMPONENT_NAME)) {
                n = 2131230996;
            }
            else if (componentName.equals((Object)AccessibilityShortcutController.DALTONIZER_COMPONENT_NAME)) {
                n = 2131230997;
            }
            else {
                n = 2131230910;
            }
            list.add(new FrameworkCandidateInfo((AccessibilityShortcutController$ToggleableFrameworkFeatureInfo)frameworkShortcutFeaturesMap.get(componentName), n, componentName.flattenToString()));
        }
        for (int i = 0; i < size; ++i) {
            list.add((FrameworkCandidateInfo)new ServiceCandidateInfo(installedAccessibilityServiceList.get(i)));
        }
        return (List<? extends CandidateInfo>)list;
    }
    
    @Override
    protected String getDefaultKey() {
        final String shortcutTargetServiceComponentNameString = AccessibilityUtils.getShortcutTargetServiceComponentNameString(this.getContext(), UserHandle.myUserId());
        if (shortcutTargetServiceComponentNameString != null) {
            final ComponentName unflattenFromString = ComponentName.unflattenFromString(shortcutTargetServiceComponentNameString);
            if (unflattenFromString != null) {
                return unflattenFromString.flattenToString();
            }
        }
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 6;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082695;
    }
    
    @Override
    public void onRadioButtonClicked(final RadioButtonPreference radioButtonPreference) {
        final String key = radioButtonPreference.getKey();
        if (TextUtils.isEmpty((CharSequence)key)) {
            super.onRadioButtonClicked(radioButtonPreference);
        }
        else if (AccessibilityShortcutController.getFrameworkShortcutFeaturesMap().containsKey(ComponentName.unflattenFromString(key))) {
            this.onRadioButtonConfirmed(key);
        }
        else {
            final Activity activity = this.getActivity();
            if (activity != null) {
                ConfirmationDialogFragment.newInstance(this, key).show(activity.getFragmentManager(), "ConfirmationDialogFragment");
            }
        }
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        Settings.Secure.putString(this.getContext().getContentResolver(), "accessibility_shortcut_target_service", s);
        return true;
    }
    
    public static class ConfirmationDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        private IBinder mToken;
        
        public static ConfirmationDialogFragment newInstance(final ShortcutServicePickerFragment shortcutServicePickerFragment, final String s) {
            final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
            final Bundle arguments = new Bundle();
            arguments.putString("extra_key", s);
            confirmationDialogFragment.setArguments(arguments);
            confirmationDialogFragment.setTargetFragment((Fragment)shortcutServicePickerFragment, 0);
            confirmationDialogFragment.mToken = (IBinder)new Binder();
            return confirmationDialogFragment;
        }
        
        public int getMetricsCategory() {
            return 6;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final Fragment targetFragment = this.getTargetFragment();
            if (n == -1 && targetFragment instanceof ShortcutServicePickerFragment) {
                ((ShortcutServicePickerFragment)targetFragment).onServiceConfirmed(this.getArguments().getString("extra_key"));
            }
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return AccessibilityServiceWarning.createCapabilitiesDialog(this.getActivity(), ((AccessibilityManager)this.getActivity().getSystemService((Class)AccessibilityManager.class)).getInstalledServiceInfoWithComponentName(ComponentName.unflattenFromString(this.getArguments().getString("extra_key"))), (DialogInterface$OnClickListener)this);
        }
    }
    
    private class FrameworkCandidateInfo extends CandidateInfo
    {
        final int mIconResId;
        final String mKey;
        final AccessibilityShortcutController$ToggleableFrameworkFeatureInfo mToggleableFrameworkFeatureInfo;
        
        public FrameworkCandidateInfo(final AccessibilityShortcutController$ToggleableFrameworkFeatureInfo mToggleableFrameworkFeatureInfo, final int mIconResId, final String mKey) {
            super(true);
            this.mToggleableFrameworkFeatureInfo = mToggleableFrameworkFeatureInfo;
            this.mIconResId = mIconResId;
            this.mKey = mKey;
        }
        
        @Override
        public String getKey() {
            return this.mKey;
        }
        
        @Override
        public Drawable loadIcon() {
            return ShortcutServicePickerFragment.this.getContext().getDrawable(this.mIconResId);
        }
        
        @Override
        public CharSequence loadLabel() {
            return this.mToggleableFrameworkFeatureInfo.getLabel(ShortcutServicePickerFragment.this.getContext());
        }
    }
    
    private class ServiceCandidateInfo extends CandidateInfo
    {
        final AccessibilityServiceInfo mServiceInfo;
        
        public ServiceCandidateInfo(final AccessibilityServiceInfo mServiceInfo) {
            super(true);
            this.mServiceInfo = mServiceInfo;
        }
        
        @Override
        public String getKey() {
            return this.mServiceInfo.getComponentName().flattenToString();
        }
        
        @Override
        public Drawable loadIcon() {
            final ResolveInfo resolveInfo = this.mServiceInfo.getResolveInfo();
            Drawable drawable;
            if (resolveInfo.getIconResource() == 0) {
                drawable = ShortcutServicePickerFragment.this.getContext().getDrawable(2131689472);
            }
            else {
                drawable = resolveInfo.loadIcon(ShortcutServicePickerFragment.this.getContext().getPackageManager());
            }
            return drawable;
        }
        
        @Override
        public CharSequence loadLabel() {
            final PackageManagerWrapper packageManagerWrapper = new PackageManagerWrapper(ShortcutServicePickerFragment.this.getContext().getPackageManager());
            final CharSequence loadLabel = this.mServiceInfo.getResolveInfo().serviceInfo.loadLabel(packageManagerWrapper.getPackageManager());
            if (loadLabel != null) {
                return loadLabel;
            }
            final ComponentName componentName = this.mServiceInfo.getComponentName();
            if (componentName != null) {
                try {
                    return packageManagerWrapper.getApplicationInfoAsUser(componentName.getPackageName(), 0, UserHandle.myUserId()).loadLabel(packageManagerWrapper.getPackageManager());
                }
                catch (PackageManager$NameNotFoundException ex) {
                    return null;
                }
            }
            return null;
        }
    }
}
