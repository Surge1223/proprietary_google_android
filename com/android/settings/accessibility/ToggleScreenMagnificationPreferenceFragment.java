package com.android.settings.accessibility;

import android.content.res.Resources;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout$LayoutParams;
import android.widget.MediaController;
import android.net.Uri;
import android.media.MediaPlayer;
import android.media.MediaPlayer$OnPreparedListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.widget.ImageView;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.widget.Switch;
import android.widget.VideoView;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.widget.SwitchBar;

public class ToggleScreenMagnificationPreferenceFragment extends ToggleFeaturePreferenceFragment implements OnSwitchChangeListener
{
    protected Preference mConfigWarningPreference;
    private boolean mInitialSetting;
    private boolean mLaunchFromSuw;
    protected VideoPreference mVideoPreference;
    
    public ToggleScreenMagnificationPreferenceFragment() {
        this.mLaunchFromSuw = false;
        this.mInitialSetting = false;
    }
    
    private void updateConfigurationWarningIfNeeded() {
        final CharSequence configurationWarningStringForSecureSettingsKey = MagnificationPreferenceFragment.getConfigurationWarningStringForSecureSettingsKey(this.mPreferenceKey, this.getPrefContext());
        if (configurationWarningStringForSecureSettingsKey != null) {
            this.mConfigWarningPreference.setSummary(configurationWarningStringForSecureSettingsKey);
        }
        this.mConfigWarningPreference.setVisible(configurationWarningStringForSecureSettingsKey != null);
    }
    
    @Override
    public int getMetricsCategory() {
        return 7;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        (this.mVideoPreference = new VideoPreference(this.getPrefContext())).setSelectable(false);
        this.mVideoPreference.setPersistent(false);
        this.mVideoPreference.setLayoutResource(2131558605);
        (this.mConfigWarningPreference = new Preference(this.getPrefContext())).setSelectable(false);
        this.mConfigWarningPreference.setPersistent(false);
        this.mConfigWarningPreference.setVisible(false);
        this.mConfigWarningPreference.setIcon(2131231193);
        final PreferenceScreen preferenceScreen = this.getPreferenceManager().getPreferenceScreen();
        preferenceScreen.setOrderingAsAdded(false);
        this.mVideoPreference.setOrder(0);
        this.mConfigWarningPreference.setOrder(2);
        preferenceScreen.addPreference(this.mVideoPreference);
        preferenceScreen.addPreference(this.mConfigWarningPreference);
    }
    
    @Override
    protected void onInstallSwitchBarToggleSwitch() {
        super.onInstallSwitchBarToggleSwitch();
        this.mSwitchBar.setCheckedInternal(MagnificationPreferenceFragment.isChecked(this.getContentResolver(), this.mPreferenceKey));
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    protected void onPreferenceToggled(final String s, final boolean b) {
        MagnificationPreferenceFragment.setChecked(this.getContentResolver(), s, b);
        this.updateConfigurationWarningIfNeeded();
    }
    
    @Override
    protected void onProcessArguments(final Bundle bundle) {
        super.onProcessArguments(bundle);
        if (bundle == null) {
            return;
        }
        if (bundle.containsKey("video_resource")) {
            this.mVideoPreference.setVisible(true);
            bundle.getInt("video_resource");
        }
        else {
            this.mVideoPreference.setVisible(false);
        }
        if (bundle.containsKey("from_suw")) {
            this.mLaunchFromSuw = bundle.getBoolean("from_suw");
        }
        if (bundle.containsKey("checked")) {
            this.mInitialSetting = bundle.getBoolean("checked");
        }
        if (bundle.containsKey("title_res")) {
            final int int1 = bundle.getInt("title_res");
            if (int1 > 0) {
                this.getActivity().setTitle(int1);
            }
        }
    }
    
    @Override
    protected void onRemoveSwitchBarToggleSwitch() {
        super.onRemoveSwitchBarToggleSwitch();
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final VideoView videoView = (VideoView)this.getView().findViewById(2131362802);
        if (videoView != null) {
            videoView.start();
        }
        this.updateConfigurationWarningIfNeeded();
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        this.onPreferenceToggled(this.mPreferenceKey, b);
    }
    
    protected class VideoPreference extends Preference
    {
        private ViewTreeObserver$OnGlobalLayoutListener mLayoutListener;
        private ImageView mVideoBackgroundView;
        
        public VideoPreference(final Context context) {
            super(context);
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            super.onBindViewHolder(preferenceViewHolder);
            final Resources resources = InstrumentedPreferenceFragment.this.getPrefContext().getResources();
            final int dimensionPixelSize = resources.getDimensionPixelSize(2131165529);
            final int dimensionPixelSize2 = resources.getDimensionPixelSize(2131165532);
            final int dimensionPixelSize3 = resources.getDimensionPixelSize(2131165530);
            final int dimensionPixelSize4 = resources.getDimensionPixelSize(2131165531);
            preferenceViewHolder.setDividerAllowedAbove(false);
            preferenceViewHolder.setDividerAllowedBelow(false);
            this.mVideoBackgroundView = (ImageView)preferenceViewHolder.findViewById(2131362803);
            final VideoView videoView = (VideoView)preferenceViewHolder.findViewById(2131362802);
            videoView.setOnPreparedListener((MediaPlayer$OnPreparedListener)new MediaPlayer$OnPreparedListener() {
                public void onPrepared(final MediaPlayer mediaPlayer) {
                    mediaPlayer.setLooping(true);
                }
            });
            videoView.setAudioFocusRequest(0);
            final Bundle arguments = ToggleScreenMagnificationPreferenceFragment.this.getArguments();
            if (arguments != null && arguments.containsKey("video_resource")) {
                videoView.setVideoURI(Uri.parse(String.format("%s://%s/%s", "android.resource", InstrumentedPreferenceFragment.this.getPrefContext().getPackageName(), arguments.getInt("video_resource"))));
            }
            videoView.setMediaController((MediaController)null);
            this.mLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    final int width = VideoPreference.this.mVideoBackgroundView.getWidth();
                    final RelativeLayout$LayoutParams layoutParams = (RelativeLayout$LayoutParams)videoView.getLayoutParams();
                    layoutParams.width = dimensionPixelSize2 * width / dimensionPixelSize;
                    layoutParams.height = dimensionPixelSize3 * width / dimensionPixelSize;
                    layoutParams.setMargins(0, dimensionPixelSize4 * width / dimensionPixelSize, 0, 0);
                    videoView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                    videoView.invalidate();
                    videoView.start();
                }
            };
            this.mVideoBackgroundView.getViewTreeObserver().addOnGlobalLayoutListener(this.mLayoutListener);
        }
        
        @Override
        protected void onPrepareForRemoval() {
            this.mVideoBackgroundView.getViewTreeObserver().removeOnGlobalLayoutListener(this.mLayoutListener);
        }
    }
}
