package com.android.settings.accessibility;

import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.os.Bundle;
import android.content.Context;
import com.android.settings.core.TogglePreferenceController;

public class MagnificationGesturesPreferenceController extends TogglePreferenceController
{
    private boolean mIsFromSUW;
    
    public MagnificationGesturesPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mIsFromSUW = false;
    }
    
    static void populateMagnificationGesturesPreferenceExtras(final Bundle bundle, final Context context) {
        bundle.putString("preference_key", "accessibility_display_magnification_enabled");
        bundle.putInt("title_res", 2131886175);
        bundle.putInt("summary_res", 2131886182);
        bundle.putInt("video_resource", 2131820545);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public CharSequence getSummary() {
        int n;
        if (this.mIsFromSUW) {
            n = 2131886180;
        }
        else if (this.isChecked()) {
            n = 2131886155;
        }
        else {
            n = 2131886154;
        }
        return this.mContext.getString(n);
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.getPreferenceKey().equals(preference.getKey())) {
            final Bundle extras = preference.getExtras();
            populateMagnificationGesturesPreferenceExtras(extras, this.mContext);
            extras.putBoolean("checked", this.isChecked());
            extras.putBoolean("from_suw", this.mIsFromSUW);
        }
        return false;
    }
    
    @Override
    public boolean isChecked() {
        return MagnificationPreferenceFragment.isChecked(this.mContext.getContentResolver(), "accessibility_display_magnification_enabled");
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"screen_magnification_gestures_preference_screen");
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return MagnificationPreferenceFragment.setChecked(this.mContext.getContentResolver(), "accessibility_display_magnification_enabled", b);
    }
    
    public void setIsFromSUW(final boolean mIsFromSUW) {
        this.mIsFromSUW = mIsFromSUW;
    }
}
