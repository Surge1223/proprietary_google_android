package com.android.settings.accessibility;

import android.view.View$OnLayoutChangeListener;
import android.content.ContentResolver;
import android.provider.Settings;
import android.preference.PreferenceFrameLayout$LayoutParams;
import android.preference.PreferenceFrameLayout;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.accessibility.CaptioningManager$CaptionStyle;
import android.content.res.Resources;
import java.util.Locale;
import android.content.Context;
import com.android.settingslib.accessibility.AccessibilityUtils;
import com.android.settings.widget.ToggleSwitch;
import com.android.settings.widget.SwitchBar;
import android.view.View;
import com.android.internal.widget.SubtitleView;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceCategory;
import android.view.accessibility.CaptioningManager;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class CaptionPropertiesFragment extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnValueChangedListener
{
    private ColorPreference mBackgroundColor;
    private ColorPreference mBackgroundOpacity;
    private CaptioningManager mCaptioningManager;
    private PreferenceCategory mCustom;
    private ColorPreference mEdgeColor;
    private EdgeTypePreference mEdgeType;
    private ListPreference mFontSize;
    private ColorPreference mForegroundColor;
    private ColorPreference mForegroundOpacity;
    private LocalePreference mLocale;
    private PresetPreference mPreset;
    private SubtitleView mPreviewText;
    private View mPreviewViewport;
    private View mPreviewWindow;
    private boolean mShowingCustom;
    private SwitchBar mSwitchBar;
    private ToggleSwitch mToggleSwitch;
    private ListPreference mTypeface;
    private ColorPreference mWindowColor;
    private ColorPreference mWindowOpacity;
    
    public static void applyCaptionProperties(final CaptioningManager captioningManager, final SubtitleView subtitleView, final View view, final int style) {
        subtitleView.setStyle(style);
        final Context context = subtitleView.getContext();
        context.getContentResolver();
        final float fontScale = captioningManager.getFontScale();
        if (view != null) {
            subtitleView.setTextSize(0.0533f * (Math.max(9 * view.getWidth(), 16 * view.getHeight()) / 16.0f) * fontScale);
        }
        else {
            subtitleView.setTextSize(context.getResources().getDimension(2131165324) * fontScale);
        }
        final Locale locale = captioningManager.getLocale();
        if (locale != null) {
            subtitleView.setText(AccessibilityUtils.getTextForLocale(context, locale, 2131886964));
        }
        else {
            subtitleView.setText(2131886964);
        }
    }
    
    private void initializeAllPreferences() {
        this.mLocale = (LocalePreference)this.findPreference("captioning_locale");
        this.mFontSize = (ListPreference)this.findPreference("captioning_font_size");
        final Resources resources = this.getResources();
        final int[] intArray = resources.getIntArray(2130903094);
        final String[] stringArray = resources.getStringArray(2130903093);
        (this.mPreset = (PresetPreference)this.findPreference("captioning_preset")).setValues(intArray);
        this.mPreset.setTitles(stringArray);
        this.mCustom = (PreferenceCategory)this.findPreference("custom");
        this.mShowingCustom = true;
        final int[] intArray2 = resources.getIntArray(2130903086);
        final String[] stringArray2 = resources.getStringArray(2130903085);
        (this.mForegroundColor = (ColorPreference)this.mCustom.findPreference("captioning_foreground_color")).setTitles(stringArray2);
        this.mForegroundColor.setValues(intArray2);
        final int[] intArray3 = resources.getIntArray(2130903092);
        final String[] stringArray3 = resources.getStringArray(2130903091);
        (this.mForegroundOpacity = (ColorPreference)this.mCustom.findPreference("captioning_foreground_opacity")).setTitles(stringArray3);
        this.mForegroundOpacity.setValues(intArray3);
        (this.mEdgeColor = (ColorPreference)this.mCustom.findPreference("captioning_edge_color")).setTitles(stringArray2);
        this.mEdgeColor.setValues(intArray2);
        final int[] array = new int[intArray2.length + 1];
        final String[] array2 = new String[stringArray2.length + 1];
        System.arraycopy(intArray2, 0, array, 1, intArray2.length);
        System.arraycopy(stringArray2, 0, array2, 1, stringArray2.length);
        array2[array[0] = 0] = this.getString(2131887037);
        (this.mBackgroundColor = (ColorPreference)this.mCustom.findPreference("captioning_background_color")).setTitles(array2);
        this.mBackgroundColor.setValues(array);
        (this.mBackgroundOpacity = (ColorPreference)this.mCustom.findPreference("captioning_background_opacity")).setTitles(stringArray3);
        this.mBackgroundOpacity.setValues(intArray3);
        (this.mWindowColor = (ColorPreference)this.mCustom.findPreference("captioning_window_color")).setTitles(array2);
        this.mWindowColor.setValues(array);
        (this.mWindowOpacity = (ColorPreference)this.mCustom.findPreference("captioning_window_opacity")).setTitles(stringArray3);
        this.mWindowOpacity.setValues(intArray3);
        this.mEdgeType = (EdgeTypePreference)this.mCustom.findPreference("captioning_edge_type");
        this.mTypeface = (ListPreference)this.mCustom.findPreference("captioning_typeface");
    }
    
    private void installSwitchBarToggleSwitch() {
        this.onInstallSwitchBarToggleSwitch();
        this.mSwitchBar.show();
    }
    
    private void installUpdateListeners() {
        this.mPreset.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mForegroundColor.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mForegroundOpacity.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mEdgeColor.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mBackgroundColor.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mBackgroundOpacity.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mWindowColor.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mWindowOpacity.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mEdgeType.setOnValueChangedListener((ListDialogPreference.OnValueChangedListener)this);
        this.mTypeface.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mFontSize.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mLocale.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
    }
    
    private int mergeColorOpacity(final ColorPreference colorPreference, final ColorPreference colorPreference2) {
        final int value = colorPreference.getValue();
        final int value2 = colorPreference2.getValue();
        int alpha;
        if (!CaptioningManager$CaptionStyle.hasColor(value)) {
            alpha = (0xFFFF00 | Color.alpha(value2));
        }
        else if (value == 0) {
            alpha = Color.alpha(value2);
        }
        else {
            alpha = ((0xFFFFFF & value) | (0xFF000000 & value2));
        }
        return alpha;
    }
    
    private void parseColorOpacity(final ColorPreference colorPreference, final ColorPreference colorPreference2, int n) {
        int value;
        if (!CaptioningManager$CaptionStyle.hasColor(n)) {
            value = 16777215;
            n = (n & 0xFF) << 24;
        }
        else if (n >>> 24 == 0) {
            value = 0;
            n = (n & 0xFF) << 24;
        }
        else {
            final int n2 = 0xFF000000 & n;
            value = (n | 0xFF000000);
            n = n2;
        }
        colorPreference2.setValue(0xFFFFFF | n);
        colorPreference.setValue(value);
    }
    
    private void refreshPreviewText() {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        final SubtitleView mPreviewText = this.mPreviewText;
        if (mPreviewText != null) {
            applyCaptionProperties(this.mCaptioningManager, mPreviewText, this.mPreviewViewport, this.mCaptioningManager.getRawUserStyle());
            final Locale locale = this.mCaptioningManager.getLocale();
            if (locale != null) {
                mPreviewText.setText(AccessibilityUtils.getTextForLocale((Context)activity, locale, 2131886965));
            }
            else {
                mPreviewText.setText(2131886965);
            }
            final CaptioningManager$CaptionStyle userStyle = this.mCaptioningManager.getUserStyle();
            if (userStyle.hasWindowColor()) {
                this.mPreviewWindow.setBackgroundColor(userStyle.windowColor);
            }
            else {
                this.mPreviewWindow.setBackgroundColor(CaptioningManager$CaptionStyle.DEFAULT.windowColor);
            }
        }
    }
    
    private void refreshShowingCustom() {
        final boolean b = this.mPreset.getValue() == -1;
        if (!b && this.mShowingCustom) {
            this.getPreferenceScreen().removePreference(this.mCustom);
            this.mShowingCustom = false;
        }
        else if (b && !this.mShowingCustom) {
            this.getPreferenceScreen().addPreference(this.mCustom);
            this.mShowingCustom = true;
        }
    }
    
    private void removeSwitchBarToggleSwitch() {
        this.mSwitchBar.hide();
        this.mToggleSwitch.setOnBeforeCheckedChangeListener(null);
    }
    
    private void updateAllPreferences() {
        this.mPreset.setValue(this.mCaptioningManager.getRawUserStyle());
        this.mFontSize.setValue(Float.toString(this.mCaptioningManager.getFontScale()));
        final CaptioningManager$CaptionStyle customStyle = CaptioningManager$CaptionStyle.getCustomStyle(this.getContentResolver());
        this.mEdgeType.setValue(customStyle.edgeType);
        this.mEdgeColor.setValue(customStyle.edgeColor);
        final boolean hasForegroundColor = customStyle.hasForegroundColor();
        final int n = 16777215;
        int foregroundColor;
        if (hasForegroundColor) {
            foregroundColor = customStyle.foregroundColor;
        }
        else {
            foregroundColor = 16777215;
        }
        this.parseColorOpacity(this.mForegroundColor, this.mForegroundOpacity, foregroundColor);
        int backgroundColor;
        if (customStyle.hasBackgroundColor()) {
            backgroundColor = customStyle.backgroundColor;
        }
        else {
            backgroundColor = 16777215;
        }
        this.parseColorOpacity(this.mBackgroundColor, this.mBackgroundOpacity, backgroundColor);
        int windowColor = n;
        if (customStyle.hasWindowColor()) {
            windowColor = customStyle.windowColor;
        }
        this.parseColorOpacity(this.mWindowColor, this.mWindowOpacity, windowColor);
        String mRawTypeface = customStyle.mRawTypeface;
        final ListPreference mTypeface = this.mTypeface;
        if (mRawTypeface == null) {
            mRawTypeface = "";
        }
        mTypeface.setValue(mRawTypeface);
        String rawLocale = this.mCaptioningManager.getRawLocale();
        final LocalePreference mLocale = this.mLocale;
        if (rawLocale == null) {
            rawLocale = "";
        }
        mLocale.setValue(rawLocale);
    }
    
    @Override
    public int getMetricsCategory() {
        return 3;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final boolean enabled = this.mCaptioningManager.isEnabled();
        (this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar()).setSwitchBarText(2131886143, 2131886143);
        this.mSwitchBar.setCheckedInternal(enabled);
        this.mToggleSwitch = this.mSwitchBar.getSwitch();
        this.getPreferenceScreen().setEnabled(enabled);
        this.refreshPreviewText();
        this.installSwitchBarToggleSwitch();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mCaptioningManager = (CaptioningManager)this.getSystemService("captioning");
        this.addPreferencesFromResource(2132082730);
        this.initializeAllPreferences();
        this.updateAllPreferences();
        this.refreshShowingCustom();
        this.installUpdateListeners();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558480, viewGroup, false);
        if (viewGroup instanceof PreferenceFrameLayout) {
            ((PreferenceFrameLayout$LayoutParams)inflate.getLayoutParams()).removeBorders = true;
        }
        ((ViewGroup)inflate.findViewById(2131362492)).addView(super.onCreateView(layoutInflater, viewGroup, bundle), -1, -1);
        return inflate;
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.removeSwitchBarToggleSwitch();
    }
    
    protected void onInstallSwitchBarToggleSwitch() {
        this.mToggleSwitch.setOnBeforeCheckedChangeListener((ToggleSwitch.OnBeforeCheckedChangeListener)new ToggleSwitch.OnBeforeCheckedChangeListener() {
            @Override
            public boolean onBeforeCheckedChanged(final ToggleSwitch toggleSwitch, final boolean b) {
                CaptionPropertiesFragment.this.mSwitchBar.setCheckedInternal(b);
                Settings.Secure.putInt(CaptionPropertiesFragment.this.getActivity().getContentResolver(), "accessibility_captioning_enabled", (int)(b ? 1 : 0));
                CaptionPropertiesFragment.this.getPreferenceScreen().setEnabled(b);
                if (CaptionPropertiesFragment.this.mPreviewText != null) {
                    final SubtitleView access$200 = CaptionPropertiesFragment.this.mPreviewText;
                    int visibility;
                    if ((b ? 1 : 0) != 0) {
                        visibility = 0;
                    }
                    else {
                        visibility = 4;
                    }
                    access$200.setVisibility(visibility);
                }
                return false;
            }
        });
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final ContentResolver contentResolver = this.getActivity().getContentResolver();
        if (this.mTypeface == preference) {
            Settings.Secure.putString(contentResolver, "accessibility_captioning_typeface", (String)o);
        }
        else if (this.mFontSize == preference) {
            Settings.Secure.putFloat(contentResolver, "accessibility_captioning_font_scale", Float.parseFloat((String)o));
        }
        else if (this.mLocale == preference) {
            Settings.Secure.putString(contentResolver, "accessibility_captioning_locale", (String)o);
        }
        this.refreshPreviewText();
        return true;
    }
    
    @Override
    public void onValueChanged(final ListDialogPreference listDialogPreference, final int n) {
        final ContentResolver contentResolver = this.getActivity().getContentResolver();
        if (this.mForegroundColor != listDialogPreference && this.mForegroundOpacity != listDialogPreference) {
            if (this.mBackgroundColor != listDialogPreference && this.mBackgroundOpacity != listDialogPreference) {
                if (this.mWindowColor != listDialogPreference && this.mWindowOpacity != listDialogPreference) {
                    if (this.mEdgeColor == listDialogPreference) {
                        Settings.Secure.putInt(contentResolver, "accessibility_captioning_edge_color", n);
                    }
                    else if (this.mPreset == listDialogPreference) {
                        Settings.Secure.putInt(contentResolver, "accessibility_captioning_preset", n);
                        this.refreshShowingCustom();
                    }
                    else if (this.mEdgeType == listDialogPreference) {
                        Settings.Secure.putInt(contentResolver, "accessibility_captioning_edge_type", n);
                    }
                }
                else {
                    Settings.Secure.putInt(contentResolver, "accessibility_captioning_window_color", this.mergeColorOpacity(this.mWindowColor, this.mWindowOpacity));
                }
            }
            else {
                Settings.Secure.putInt(contentResolver, "accessibility_captioning_background_color", this.mergeColorOpacity(this.mBackgroundColor, this.mBackgroundOpacity));
            }
        }
        else {
            Settings.Secure.putInt(contentResolver, "accessibility_captioning_foreground_color", this.mergeColorOpacity(this.mForegroundColor, this.mForegroundOpacity));
        }
        this.refreshPreviewText();
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        final boolean enabled = this.mCaptioningManager.isEnabled();
        this.mPreviewText = (SubtitleView)view.findViewById(2131362465);
        final SubtitleView mPreviewText = this.mPreviewText;
        int visibility;
        if (enabled) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        mPreviewText.setVisibility(visibility);
        this.mPreviewWindow = view.findViewById(2131362467);
        (this.mPreviewViewport = view.findViewById(2131362466)).addOnLayoutChangeListener((View$OnLayoutChangeListener)new View$OnLayoutChangeListener() {
            public void onLayoutChange(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
                CaptionPropertiesFragment.this.refreshPreviewText();
            }
        });
    }
}
