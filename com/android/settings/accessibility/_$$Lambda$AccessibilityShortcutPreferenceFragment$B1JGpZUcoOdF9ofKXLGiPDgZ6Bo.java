package com.android.settings.accessibility;

import android.os.Bundle;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settingslib.accessibility.AccessibilityUtils;
import android.os.UserHandle;
import android.content.ComponentName;
import com.android.internal.accessibility.AccessibilityShortcutController;
import com.android.internal.accessibility.AccessibilityShortcutController$ToggleableFrameworkFeatureInfo;
import android.view.accessibility.AccessibilityManager;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.Handler;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import android.database.ContentObserver;
import com.android.settings.search.Indexable;
import android.widget.Switch;
import com.android.settings.widget.SwitchBar;

public final class _$$Lambda$AccessibilityShortcutPreferenceFragment$B1JGpZUcoOdF9ofKXLGiPDgZ6Bo implements OnSwitchChangeListener
{
    @Override
    public final void onSwitchChanged(final Switch switch1, final boolean b) {
        AccessibilityShortcutPreferenceFragment.lambda$onInstallSwitchBarToggleSwitch$1(this.f$0, switch1, b);
    }
}
