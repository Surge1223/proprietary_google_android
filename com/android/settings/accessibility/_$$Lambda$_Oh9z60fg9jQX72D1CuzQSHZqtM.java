package com.android.settings.accessibility;

import java.util.function.Function;

public final class _$$Lambda$_Oh9z60fg9jQX72D1CuzQSHZqtM implements Function
{
    @Override
    public final Object apply(final Object o) {
        return ((VibrationPreferenceFragment.VibrationIntensityCandidateInfo)o).getIntensity();
    }
}
