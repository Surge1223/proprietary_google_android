package com.android.settings;

import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class TestingSettingsBroadcastReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SECRET_CODE")) {
            intent = new Intent("android.intent.action.MAIN");
            intent.setClass(context, (Class)Settings.TestingSettingsActivity.class);
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
    }
}
