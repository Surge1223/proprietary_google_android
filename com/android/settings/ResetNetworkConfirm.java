package com.android.settings;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.os.RecoverySystem;
import android.os.AsyncTask;
import android.content.DialogInterface$OnDismissListener;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.widget.Toast;
import android.telephony.SubscriptionManager;
import android.content.DialogInterface;
import android.net.Uri;
import android.provider.Telephony$Sms;
import android.bluetooth.BluetoothAdapter;
import android.app.Activity;
import android.content.Context;
import com.android.ims.ImsManager;
import android.bluetooth.BluetoothManager;
import android.net.NetworkPolicyManager;
import android.telephony.TelephonyManager;
import android.net.wifi.WifiManager;
import android.net.ConnectivityManager;
import android.view.View.OnClickListener;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;

public class ResetNetworkConfirm extends InstrumentedFragment
{
    private View mContentView;
    boolean mEraseEsim;
    EraseEsimAsyncTask mEraseEsimTask;
    private View.OnClickListener mFinalClickListener;
    private int mSubId;
    
    public ResetNetworkConfirm() {
        this.mSubId = -1;
        this.mFinalClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (Utils.isMonkeyRunning()) {
                    return;
                }
                final Activity activity = ResetNetworkConfirm.this.getActivity();
                final ConnectivityManager connectivityManager = (ConnectivityManager)((Context)activity).getSystemService("connectivity");
                if (connectivityManager != null) {
                    connectivityManager.factoryReset();
                }
                final WifiManager wifiManager = (WifiManager)((Context)activity).getSystemService("wifi");
                if (wifiManager != null) {
                    wifiManager.factoryReset();
                }
                final TelephonyManager telephonyManager = (TelephonyManager)((Context)activity).getSystemService("phone");
                if (telephonyManager != null) {
                    telephonyManager.factoryReset(ResetNetworkConfirm.this.mSubId);
                }
                final NetworkPolicyManager networkPolicyManager = (NetworkPolicyManager)((Context)activity).getSystemService("netpolicy");
                if (networkPolicyManager != null) {
                    networkPolicyManager.factoryReset(telephonyManager.getSubscriberId(ResetNetworkConfirm.this.mSubId));
                }
                final BluetoothManager bluetoothManager = (BluetoothManager)((Context)activity).getSystemService("bluetooth");
                if (bluetoothManager != null) {
                    final BluetoothAdapter adapter = bluetoothManager.getAdapter();
                    if (adapter != null) {
                        adapter.factoryReset();
                    }
                }
                ImsManager.factoryReset((Context)activity);
                ResetNetworkConfirm.this.restoreDefaultApn((Context)activity);
                ResetNetworkConfirm.this.esimFactoryReset((Context)activity, ((Context)activity).getPackageName());
                ResetNetworkConfirm.this.cleanUpSmsRawTable((Context)activity);
            }
        };
    }
    
    private void cleanUpSmsRawTable(final Context context) {
        context.getContentResolver().delete(Uri.withAppendedPath(Telephony$Sms.CONTENT_URI, "raw/permanentDelete"), (String)null, (String[])null);
    }
    
    private void establishFinalConfirmationState() {
        this.mContentView.findViewById(2131362135).setOnClickListener(this.mFinalClickListener);
    }
    
    private void restoreDefaultApn(final Context context) {
        Uri uri = Uri.parse("content://telephony/carriers/restore");
        if (SubscriptionManager.isUsableSubIdValue(this.mSubId)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("subId/");
            sb.append(String.valueOf(this.mSubId));
            uri = Uri.withAppendedPath(uri, sb.toString());
        }
        context.getContentResolver().delete(uri, (String)null, (String[])null);
    }
    
    void esimFactoryReset(final Context context, final String s) {
        if (this.mEraseEsim) {
            (this.mEraseEsimTask = new EraseEsimAsyncTask(context, s)).execute((Object[])new Void[0]);
        }
        else {
            Toast.makeText(context, 2131888818, 0).show();
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 84;
    }
    
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        arguments = this.getArguments();
        if (arguments != null) {
            this.mSubId = arguments.getInt("subscription", -1);
            this.mEraseEsim = arguments.getBoolean("erase_esim");
        }
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.getActivity(), "no_network_reset", UserHandle.myUserId());
        if (RestrictedLockUtils.hasBaseUserRestriction((Context)this.getActivity(), "no_network_reset", UserHandle.myUserId())) {
            return layoutInflater.inflate(2131558614, (ViewGroup)null);
        }
        if (checkIfRestrictionEnforced != null) {
            new ActionDisabledByAdminDialogHelper(this.getActivity()).prepareDialogBuilder("no_network_reset", checkIfRestrictionEnforced).setOnDismissListener((DialogInterface$OnDismissListener)new _$$Lambda$ResetNetworkConfirm$YTG2_gTxf5vyFkKGLAaR8nzFOxo(this)).show();
            return new View(this.getContext());
        }
        this.mContentView = layoutInflater.inflate(2131558711, (ViewGroup)null);
        this.establishFinalConfirmationState();
        return this.mContentView;
    }
    
    @Override
    public void onDestroy() {
        if (this.mEraseEsimTask != null) {
            this.mEraseEsimTask.cancel(true);
            this.mEraseEsimTask = null;
        }
        super.onDestroy();
    }
    
    private static class EraseEsimAsyncTask extends AsyncTask<Void, Void, Boolean>
    {
        private final Context mContext;
        private final String mPackageName;
        
        EraseEsimAsyncTask(final Context mContext, final String mPackageName) {
            this.mContext = mContext;
            this.mPackageName = mPackageName;
        }
        
        protected Boolean doInBackground(final Void... array) {
            return RecoverySystem.wipeEuiccData(this.mContext, this.mPackageName);
        }
        
        protected void onPostExecute(final Boolean b) {
            if (b) {
                Toast.makeText(this.mContext, 2131888818, 0).show();
            }
            else {
                new AlertDialog$Builder(this.mContext).setTitle(2131888815).setMessage(2131888814).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).show();
            }
        }
    }
}
