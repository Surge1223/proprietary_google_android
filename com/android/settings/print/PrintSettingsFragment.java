package com.android.settings.print;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.print.PrintServicesLoader;
import android.printservice.PrintServiceInfo;
import android.print.PrintJobId;
import java.util.Collection;
import android.print.PrintJob;
import android.print.PrintManager;
import android.print.PrintManager$PrintJobStateChangeListener;
import android.content.AsyncTaskLoader;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import java.util.Iterator;
import android.text.format.DateUtils;
import android.content.Loader;
import android.print.PrintJobInfo;
import android.widget.TextView;
import android.app.LoaderManager$LoaderCallbacks;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.view.View;
import android.net.Uri;
import android.text.TextUtils;
import android.provider.Settings;
import android.content.Intent;
import android.support.v7.preference.Preference;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.widget.Button;
import android.support.v7.preference.PreferenceCategory;
import com.android.settings.search.Indexable;
import android.view.View.OnClickListener;
import com.android.settings.utils.ProfileSettingsPreferenceFragment;

public class PrintSettingsFragment extends ProfileSettingsPreferenceFragment implements View.OnClickListener, Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private PreferenceCategory mActivePrintJobsCategory;
    private Button mAddNewServiceButton;
    private PrintJobsController mPrintJobsController;
    private PreferenceCategory mPrintServicesCategory;
    private PrintServicesController mPrintServicesController;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082815;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private Intent createAddNewServiceIntentOrNull() {
        final String string = Settings.Secure.getString(this.getContentResolver(), "print_service_search_uri");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        return new Intent("android.intent.action.VIEW", Uri.parse(string));
    }
    
    private Preference newAddServicePreferenceOrNull() {
        final Intent addNewServiceIntentOrNull = this.createAddNewServiceIntentOrNull();
        if (addNewServiceIntentOrNull == null) {
            return null;
        }
        final Preference preference = new Preference(this.getPrefContext());
        preference.setTitle(2131888651);
        preference.setIcon(2131231064);
        preference.setOrder(2147483646);
        preference.setIntent(addNewServiceIntentOrNull);
        preference.setPersistent(false);
        return preference;
    }
    
    private void startSubSettingsIfNeeded() {
        if (this.getArguments() == null) {
            return;
        }
        final String string = this.getArguments().getString("EXTRA_PRINT_SERVICE_COMPONENT_NAME");
        if (string != null) {
            this.getArguments().remove("EXTRA_PRINT_SERVICE_COMPONENT_NAME");
            final Preference preference = this.findPreference(string);
            if (preference != null) {
                preference.performClick();
            }
        }
    }
    
    public int getHelpResource() {
        return 2131887781;
    }
    
    @Override
    protected String getIntentActionString() {
        return "android.settings.ACTION_PRINT_SETTINGS";
    }
    
    public int getMetricsCategory() {
        return 80;
    }
    
    public void onClick(final View view) {
        if (this.mAddNewServiceButton == view) {
            final Intent addNewServiceIntentOrNull = this.createAddNewServiceIntentOrNull();
            if (addNewServiceIntentOrNull != null) {
                try {
                    this.startActivity(addNewServiceIntentOrNull);
                }
                catch (ActivityNotFoundException ex) {
                    Log.w("PrintSettingsFragment", "Unable to start activity", (Throwable)ex);
                }
            }
        }
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.addPreferencesFromResource(2132082815);
        this.mActivePrintJobsCategory = (PreferenceCategory)this.findPreference("print_jobs_category");
        this.mPrintServicesCategory = (PreferenceCategory)this.findPreference("print_services_category");
        this.getPreferenceScreen().removePreference(this.mActivePrintJobsCategory);
        this.mPrintJobsController = new PrintJobsController();
        this.getLoaderManager().initLoader(1, (Bundle)null, (LoaderManager$LoaderCallbacks)this.mPrintJobsController);
        this.mPrintServicesController = new PrintServicesController();
        this.getLoaderManager().initLoader(2, (Bundle)null, (LoaderManager$LoaderCallbacks)this.mPrintServicesController);
        return onCreateView;
    }
    
    public void onStart() {
        super.onStart();
        this.setHasOptionsMenu(true);
        this.startSubSettingsIfNeeded();
    }
    
    @Override
    public void onViewCreated(View inflate, final Bundle bundle) {
        super.onViewCreated(inflate, bundle);
        final ViewGroup viewGroup = (ViewGroup)this.getListView().getParent();
        inflate = this.getActivity().getLayoutInflater().inflate(2131558548, viewGroup, false);
        ((TextView)inflate.findViewById(R.id.message)).setText(2131888655);
        if (this.createAddNewServiceIntentOrNull() != null) {
            (this.mAddNewServiceButton = (Button)inflate.findViewById(2131361840)).setOnClickListener((View.OnClickListener)this);
            this.mAddNewServiceButton.setVisibility(0);
        }
        viewGroup.addView(inflate);
        this.setEmptyView(inflate);
    }
    
    private final class PrintJobsController implements LoaderManager$LoaderCallbacks<List<PrintJobInfo>>
    {
        public Loader<List<PrintJobInfo>> onCreateLoader(final int n, final Bundle bundle) {
            if (n == 1) {
                return (Loader<List<PrintJobInfo>>)new PrintJobsLoader(PrintSettingsFragment.this.getContext());
            }
            return null;
        }
        
        public void onLoadFinished(final Loader<List<PrintJobInfo>> loader, final List<PrintJobInfo> list) {
            if (list != null && !list.isEmpty()) {
                if (PrintSettingsFragment.this.getPreferenceScreen().findPreference("print_jobs_category") == null) {
                    PrintSettingsFragment.this.getPreferenceScreen().addPreference(PrintSettingsFragment.this.mActivePrintJobsCategory);
                }
                PrintSettingsFragment.this.mActivePrintJobsCategory.removeAll();
                final Context access$600 = InstrumentedPreferenceFragment.this.getPrefContext();
                if (access$600 == null) {
                    Log.w("PrintSettingsFragment", "No preference context, skip adding print jobs");
                    return;
                }
                for (final PrintJobInfo printJobInfo : list) {
                    final Preference preference = new Preference(access$600);
                    preference.setPersistent(false);
                    preference.setFragment(PrintJobSettingsFragment.class.getName());
                    preference.setKey(printJobInfo.getId().flattenToString());
                    final int state = printJobInfo.getState();
                    if (state != 6) {
                        switch (state) {
                            case 4: {
                                if (!printJobInfo.isCancelling()) {
                                    preference.setTitle(PrintSettingsFragment.this.getString(2131888641, new Object[] { printJobInfo.getLabel() }));
                                    break;
                                }
                                preference.setTitle(PrintSettingsFragment.this.getString(2131888643, new Object[] { printJobInfo.getLabel() }));
                                break;
                            }
                            case 2:
                            case 3: {
                                if (!printJobInfo.isCancelling()) {
                                    preference.setTitle(PrintSettingsFragment.this.getString(2131888658, new Object[] { printJobInfo.getLabel() }));
                                    break;
                                }
                                preference.setTitle(PrintSettingsFragment.this.getString(2131888643, new Object[] { printJobInfo.getLabel() }));
                                break;
                            }
                        }
                    }
                    else {
                        preference.setTitle(PrintSettingsFragment.this.getString(2131888645, new Object[] { printJobInfo.getLabel() }));
                    }
                    preference.setSummary(PrintSettingsFragment.this.getString(2131888648, new Object[] { printJobInfo.getPrinterName(), DateUtils.formatSameDayTime(printJobInfo.getCreationTime(), printJobInfo.getCreationTime(), 3, 3) }));
                    final TypedArray obtainStyledAttributes = PrintSettingsFragment.this.getActivity().obtainStyledAttributes(new int[] { 16843817 });
                    final int color = obtainStyledAttributes.getColor(0, 0);
                    obtainStyledAttributes.recycle();
                    final int state2 = printJobInfo.getState();
                    Label_0520: {
                        if (state2 != 6) {
                            switch (state2) {
                                default: {
                                    break Label_0520;
                                }
                                case 2:
                                case 3: {
                                    final Drawable drawable = PrintSettingsFragment.this.getActivity().getDrawable(17302725);
                                    drawable.setTint(color);
                                    preference.setIcon(drawable);
                                    break Label_0520;
                                }
                                case 4: {
                                    break;
                                }
                            }
                        }
                        final Drawable drawable2 = PrintSettingsFragment.this.getActivity().getDrawable(17302726);
                        drawable2.setTint(color);
                        preference.setIcon(drawable2);
                    }
                    preference.getExtras().putString("EXTRA_PRINT_JOB_ID", printJobInfo.getId().flattenToString());
                    PrintSettingsFragment.this.mActivePrintJobsCategory.addPreference(preference);
                }
            }
            else {
                PrintSettingsFragment.this.getPreferenceScreen().removePreference(PrintSettingsFragment.this.mActivePrintJobsCategory);
            }
        }
        
        public void onLoaderReset(final Loader<List<PrintJobInfo>> loader) {
            PrintSettingsFragment.this.getPreferenceScreen().removePreference(PrintSettingsFragment.this.mActivePrintJobsCategory);
        }
    }
    
    private static final class PrintJobsLoader extends AsyncTaskLoader<List<PrintJobInfo>>
    {
        private PrintManager$PrintJobStateChangeListener mPrintJobStateChangeListener;
        private List<PrintJobInfo> mPrintJobs;
        private final PrintManager mPrintManager;
        
        public PrintJobsLoader(final Context context) {
            super(context);
            this.mPrintJobs = new ArrayList<PrintJobInfo>();
            this.mPrintManager = ((PrintManager)context.getSystemService("print")).getGlobalPrintManagerForUser(context.getUserId());
        }
        
        static /* synthetic */ void access$700(final PrintJobsLoader printJobsLoader) {
            printJobsLoader.onForceLoad();
        }
        
        public void deliverResult(final List<PrintJobInfo> list) {
            if (this.isStarted()) {
                super.deliverResult((Object)list);
            }
        }
        
        public List<PrintJobInfo> loadInBackground() {
            List<PrintJobInfo> list = null;
            final List printJobs = this.mPrintManager.getPrintJobs();
            List<PrintJobInfo> list2;
            for (int size = printJobs.size(), i = 0; i < size; ++i, list = list2) {
                final PrintJobInfo info = printJobs.get(i).getInfo();
                list2 = list;
                if (PrintSettingPreferenceController.shouldShowToUser(info)) {
                    if ((list2 = list) == null) {
                        list2 = new ArrayList<PrintJobInfo>();
                    }
                    list2.add(info);
                }
            }
            return list;
        }
        
        protected void onReset() {
            this.onStopLoading();
            this.mPrintJobs.clear();
            if (this.mPrintJobStateChangeListener != null) {
                this.mPrintManager.removePrintJobStateChangeListener(this.mPrintJobStateChangeListener);
                this.mPrintJobStateChangeListener = null;
            }
        }
        
        protected void onStartLoading() {
            if (!this.mPrintJobs.isEmpty()) {
                this.deliverResult(new ArrayList<PrintJobInfo>(this.mPrintJobs));
            }
            if (this.mPrintJobStateChangeListener == null) {
                this.mPrintJobStateChangeListener = (PrintManager$PrintJobStateChangeListener)new PrintManager$PrintJobStateChangeListener() {
                    public void onPrintJobStateChanged(final PrintJobId printJobId) {
                        PrintJobsLoader.access$700(PrintJobsLoader.this);
                    }
                };
                this.mPrintManager.addPrintJobStateChangeListener(this.mPrintJobStateChangeListener);
            }
            if (this.mPrintJobs.isEmpty()) {
                this.onForceLoad();
            }
        }
        
        protected void onStopLoading() {
            this.onCancelLoad();
        }
    }
    
    private final class PrintServicesController implements LoaderManager$LoaderCallbacks<List<PrintServiceInfo>>
    {
        public Loader<List<PrintServiceInfo>> onCreateLoader(final int n, final Bundle bundle) {
            final PrintManager printManager = (PrintManager)PrintSettingsFragment.this.getContext().getSystemService("print");
            if (printManager != null) {
                return (Loader<List<PrintServiceInfo>>)new PrintServicesLoader(printManager, PrintSettingsFragment.this.getContext(), 3);
            }
            return null;
        }
        
        public void onLoadFinished(final Loader<List<PrintServiceInfo>> loader, final List<PrintServiceInfo> list) {
            if (list.isEmpty()) {
                PrintSettingsFragment.this.getPreferenceScreen().removePreference(PrintSettingsFragment.this.mPrintServicesCategory);
                return;
            }
            if (PrintSettingsFragment.this.getPreferenceScreen().findPreference("print_services_category") == null) {
                PrintSettingsFragment.this.getPreferenceScreen().addPreference(PrintSettingsFragment.this.mPrintServicesCategory);
            }
            PrintSettingsFragment.this.mPrintServicesCategory.removeAll();
            final PackageManager packageManager = PrintSettingsFragment.this.getActivity().getPackageManager();
            final Context access$300 = InstrumentedPreferenceFragment.this.getPrefContext();
            if (access$300 == null) {
                Log.w("PrintSettingsFragment", "No preference context, skip adding print services");
                return;
            }
            for (final PrintServiceInfo printServiceInfo : list) {
                final Preference preference = new Preference(access$300);
                final String string = printServiceInfo.getResolveInfo().loadLabel(packageManager).toString();
                preference.setTitle(string);
                final ComponentName componentName = printServiceInfo.getComponentName();
                preference.setKey(componentName.flattenToString());
                preference.setFragment(PrintServiceSettingsFragment.class.getName());
                preference.setPersistent(false);
                if (printServiceInfo.isEnabled()) {
                    preference.setSummary(PrintSettingsFragment.this.getString(2131888647));
                }
                else {
                    preference.setSummary(PrintSettingsFragment.this.getString(2131888646));
                }
                final Drawable loadIcon = printServiceInfo.getResolveInfo().loadIcon(packageManager);
                if (loadIcon != null) {
                    preference.setIcon(loadIcon);
                }
                final Bundle extras = preference.getExtras();
                extras.putBoolean("EXTRA_CHECKED", printServiceInfo.isEnabled());
                extras.putString("EXTRA_TITLE", string);
                extras.putString("EXTRA_SERVICE_COMPONENT_NAME", componentName.flattenToString());
                PrintSettingsFragment.this.mPrintServicesCategory.addPreference(preference);
            }
            final Preference access$301 = PrintSettingsFragment.this.newAddServicePreferenceOrNull();
            if (access$301 != null) {
                PrintSettingsFragment.this.mPrintServicesCategory.addPreference(access$301);
            }
        }
        
        public void onLoaderReset(final Loader<List<PrintServiceInfo>> loader) {
            PrintSettingsFragment.this.getPreferenceScreen().removePreference(PrintSettingsFragment.this.mPrintServicesCategory);
        }
    }
}
