package com.android.settings.print;

import android.print.PrinterDiscoverySession$OnPrintersChangeListener;
import java.util.LinkedHashMap;
import android.print.PrinterId;
import java.util.Map;
import android.print.PrinterDiscoverySession;
import android.util.TypedValue;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import java.util.Collection;
import android.widget.Filter$FilterResults;
import android.widget.Filter;
import java.util.ArrayList;
import android.widget.Filterable;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.app.Activity;
import android.content.Context;
import android.view.accessibility.AccessibilityManager;
import android.view.View$OnAttachStateChangeListener;
import android.widget.SearchView$OnQueryTextListener;
import android.view.MenuInflater;
import android.view.Menu;
import android.print.PrintServicesLoader;
import android.content.Loader;
import android.text.TextUtils;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.print.PrintManager;
import android.content.IntentSender$SendIntentException;
import android.util.Log;
import android.print.PrinterInfo;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListAdapter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import com.android.settings.SettingsActivity;
import android.widget.ListView;
import com.android.settings.widget.ToggleSwitch;
import android.widget.SearchView;
import android.database.DataSetObserver;
import android.content.ComponentName;
import android.content.Intent;
import com.android.settings.widget.SwitchBar;
import android.printservice.PrintServiceInfo;
import java.util.List;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.SettingsPreferenceFragment;

public class PrintServiceSettingsFragment extends SettingsPreferenceFragment implements LoaderManager$LoaderCallbacks<List<PrintServiceInfo>>, OnSwitchChangeListener
{
    private Intent mAddPrintersIntent;
    private ComponentName mComponentName;
    private final DataSetObserver mDataObserver;
    private int mLastUnfilteredItemCount;
    private String mPreferenceKey;
    private PrintersAdapter mPrintersAdapter;
    private SearchView mSearchView;
    private boolean mServiceEnabled;
    private Intent mSettingsIntent;
    private SwitchBar mSwitchBar;
    private ToggleSwitch mToggleSwitch;
    
    public PrintServiceSettingsFragment() {
        this.mDataObserver = new DataSetObserver() {
            private void invalidateOptionsMenuIfNeeded() {
                final int unfilteredCount = PrintServiceSettingsFragment.this.mPrintersAdapter.getUnfilteredCount();
                if ((PrintServiceSettingsFragment.this.mLastUnfilteredItemCount <= 0 && unfilteredCount > 0) || (PrintServiceSettingsFragment.this.mLastUnfilteredItemCount > 0 && unfilteredCount <= 0)) {
                    PrintServiceSettingsFragment.this.getActivity().invalidateOptionsMenu();
                }
                PrintServiceSettingsFragment.this.mLastUnfilteredItemCount = unfilteredCount;
            }
            
            public void onChanged() {
                this.invalidateOptionsMenuIfNeeded();
                PrintServiceSettingsFragment.this.updateEmptyView();
            }
            
            public void onInvalidated() {
                this.invalidateOptionsMenuIfNeeded();
            }
        };
    }
    
    private ListView getBackupListView() {
        return (ListView)this.getView().findViewById(2131361897);
    }
    
    private void initComponents() {
        (this.mPrintersAdapter = new PrintersAdapter()).registerDataSetObserver(this.mDataObserver);
        (this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar()).addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        this.mSwitchBar.show();
        (this.mToggleSwitch = this.mSwitchBar.getSwitch()).setOnBeforeCheckedChangeListener((ToggleSwitch.OnBeforeCheckedChangeListener)new ToggleSwitch.OnBeforeCheckedChangeListener() {
            @Override
            public boolean onBeforeCheckedChanged(final ToggleSwitch toggleSwitch, final boolean b) {
                PrintServiceSettingsFragment.this.onPreferenceToggled(PrintServiceSettingsFragment.this.mPreferenceKey, b);
                return false;
            }
        });
        this.getBackupListView().setSelector((Drawable)new ColorDrawable(0));
        this.getBackupListView().setAdapter((ListAdapter)this.mPrintersAdapter);
        this.getBackupListView().setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                final PrinterInfo printerInfo = (PrinterInfo)PrintServiceSettingsFragment.this.mPrintersAdapter.getItem(n);
                if (printerInfo.getInfoIntent() != null) {
                    try {
                        PrintServiceSettingsFragment.this.getActivity().startIntentSender(printerInfo.getInfoIntent().getIntentSender(), (Intent)null, 0, 0, 0);
                    }
                    catch (IntentSender$SendIntentException ex) {
                        Log.e("PrintServiceSettingsFragment", "Could not execute info intent: %s", (Throwable)ex);
                    }
                }
            }
        });
    }
    
    private void onPreferenceToggled(final String s, final boolean b) {
        ((PrintManager)this.getContext().getSystemService("print")).setPrintServiceEnabled(this.mComponentName, b);
    }
    
    private void updateEmptyView() {
        final ViewGroup viewGroup = (ViewGroup)this.getListView().getParent();
        final View emptyView = this.getBackupListView().getEmptyView();
        if (!this.mToggleSwitch.isChecked()) {
            View view;
            if ((view = emptyView) != null) {
                view = emptyView;
                if (emptyView.getId() != 2131362103) {
                    viewGroup.removeView(emptyView);
                    view = null;
                }
            }
            if (view == null) {
                final View inflate = this.getActivity().getLayoutInflater().inflate(2131558548, viewGroup, false);
                ((ImageView)inflate.findViewById(R.id.icon)).setContentDescription((CharSequence)this.getString(2131888663));
                ((TextView)inflate.findViewById(R.id.message)).setText(2131888663);
                viewGroup.addView(inflate);
                this.getBackupListView().setEmptyView(inflate);
            }
        }
        else if (this.mPrintersAdapter.getUnfilteredCount() <= 0) {
            View view2;
            if ((view2 = emptyView) != null) {
                view2 = emptyView;
                if (emptyView.getId() != 2131362104) {
                    viewGroup.removeView(emptyView);
                    view2 = null;
                }
            }
            if (view2 == null) {
                final View inflate2 = this.getActivity().getLayoutInflater().inflate(2131558549, viewGroup, false);
                viewGroup.addView(inflate2);
                this.getBackupListView().setEmptyView(inflate2);
            }
        }
        else if (this.mPrintersAdapter.getCount() <= 0) {
            View view3;
            if ((view3 = emptyView) != null) {
                view3 = emptyView;
                if (emptyView.getId() != 2131362103) {
                    viewGroup.removeView(emptyView);
                    view3 = null;
                }
            }
            if (view3 == null) {
                final View inflate3 = this.getActivity().getLayoutInflater().inflate(2131558548, viewGroup, false);
                ((ImageView)inflate3.findViewById(R.id.icon)).setContentDescription((CharSequence)this.getString(2131888654));
                ((TextView)inflate3.findViewById(R.id.message)).setText(2131888654);
                viewGroup.addView(inflate3);
                this.getBackupListView().setEmptyView(inflate3);
            }
        }
    }
    
    private void updateUiForArguments() {
        final Bundle arguments = this.getArguments();
        this.mComponentName = ComponentName.unflattenFromString(arguments.getString("EXTRA_SERVICE_COMPONENT_NAME"));
        this.mPreferenceKey = this.mComponentName.flattenToString();
        this.mSwitchBar.setCheckedInternal(arguments.getBoolean("EXTRA_CHECKED"));
        this.getLoaderManager().initLoader(2, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
        this.setHasOptionsMenu(true);
    }
    
    private void updateUiForServiceState() {
        if (this.mServiceEnabled) {
            this.mSwitchBar.setCheckedInternal(true);
            this.mPrintersAdapter.enable();
        }
        else {
            this.mSwitchBar.setCheckedInternal(false);
            this.mPrintersAdapter.disable();
        }
        this.getActivity().invalidateOptionsMenu();
    }
    
    public int getMetricsCategory() {
        return 79;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String string = this.getArguments().getString("EXTRA_TITLE");
        if (!TextUtils.isEmpty((CharSequence)string)) {
            this.getActivity().setTitle((CharSequence)string);
        }
    }
    
    public Loader<List<PrintServiceInfo>> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<List<PrintServiceInfo>>)new PrintServicesLoader((PrintManager)this.getContext().getSystemService("print"), this.getContext(), 3);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(2131623940, menu);
        final MenuItem item = menu.findItem(2131362468);
        if (this.mServiceEnabled && this.mAddPrintersIntent != null) {
            item.setIntent(this.mAddPrintersIntent);
        }
        else {
            menu.removeItem(2131362468);
        }
        final MenuItem item2 = menu.findItem(2131362470);
        if (this.mServiceEnabled && this.mSettingsIntent != null) {
            item2.setIntent(this.mSettingsIntent);
        }
        else {
            menu.removeItem(2131362470);
        }
        final MenuItem item3 = menu.findItem(2131362469);
        if (this.mServiceEnabled && this.mPrintersAdapter.getUnfilteredCount() > 0) {
            (this.mSearchView = (SearchView)item3.getActionView()).setOnQueryTextListener((SearchView$OnQueryTextListener)new SearchView$OnQueryTextListener() {
                public boolean onQueryTextChange(final String s) {
                    PrintServiceSettingsFragment.this.mPrintersAdapter.getFilter().filter((CharSequence)s);
                    return true;
                }
                
                public boolean onQueryTextSubmit(final String s) {
                    return true;
                }
            });
            this.mSearchView.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener() {
                public void onViewAttachedToWindow(final View view) {
                    if (AccessibilityManager.getInstance((Context)PrintServiceSettingsFragment.this.getActivity()).isEnabled()) {
                        view.announceForAccessibility((CharSequence)PrintServiceSettingsFragment.this.getString(2131888661));
                    }
                }
                
                public void onViewDetachedFromWindow(final View view) {
                    final Activity activity = PrintServiceSettingsFragment.this.getActivity();
                    if (activity != null && !activity.isFinishing() && AccessibilityManager.getInstance((Context)activity).isEnabled()) {
                        view.announceForAccessibility((CharSequence)PrintServiceSettingsFragment.this.getString(2131888660));
                    }
                }
            });
        }
        else {
            menu.removeItem(2131362469);
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.mServiceEnabled = this.getArguments().getBoolean("EXTRA_CHECKED");
        return onCreateView;
    }
    
    public void onDestroyView() {
        super.onDestroyView();
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        this.mSwitchBar.hide();
    }
    
    public void onLoadFinished(final Loader<List<PrintServiceInfo>> loader, final List<PrintServiceInfo> list) {
        PrintServiceInfo printServiceInfo2;
        final PrintServiceInfo printServiceInfo = printServiceInfo2 = null;
        if (list != null) {
            final int size = list.size();
            int n = 0;
            while (true) {
                printServiceInfo2 = printServiceInfo;
                if (n >= size) {
                    break;
                }
                if (list.get(n).getComponentName().equals((Object)this.mComponentName)) {
                    printServiceInfo2 = list.get(n);
                    break;
                }
                ++n;
            }
        }
        if (printServiceInfo2 == null) {
            this.finishFragment();
        }
        this.mServiceEnabled = printServiceInfo2.isEnabled();
        if (printServiceInfo2.getSettingsActivityName() != null) {
            final Intent mSettingsIntent = new Intent("android.intent.action.MAIN");
            mSettingsIntent.setComponent(new ComponentName(printServiceInfo2.getComponentName().getPackageName(), printServiceInfo2.getSettingsActivityName()));
            final List queryIntentActivities = this.getPackageManager().queryIntentActivities(mSettingsIntent, 0);
            if (!queryIntentActivities.isEmpty() && queryIntentActivities.get(0).activityInfo.exported) {
                this.mSettingsIntent = mSettingsIntent;
            }
        }
        else {
            this.mSettingsIntent = null;
        }
        if (printServiceInfo2.getAddPrintersActivityName() != null) {
            final Intent mAddPrintersIntent = new Intent("android.intent.action.MAIN");
            mAddPrintersIntent.setComponent(new ComponentName(printServiceInfo2.getComponentName().getPackageName(), printServiceInfo2.getAddPrintersActivityName()));
            final List queryIntentActivities2 = this.getPackageManager().queryIntentActivities(mAddPrintersIntent, 0);
            if (!queryIntentActivities2.isEmpty() && queryIntentActivities2.get(0).activityInfo.exported) {
                this.mAddPrintersIntent = mAddPrintersIntent;
            }
        }
        else {
            this.mAddPrintersIntent = null;
        }
        this.updateUiForServiceState();
    }
    
    public void onLoaderReset(final Loader<List<PrintServiceInfo>> loader) {
        this.updateUiForServiceState();
    }
    
    public void onPause() {
        if (this.mSearchView != null) {
            this.mSearchView.setOnQueryTextListener((SearchView$OnQueryTextListener)null);
        }
        super.onPause();
    }
    
    public void onStart() {
        super.onStart();
        this.updateEmptyView();
        this.updateUiForServiceState();
    }
    
    public void onStop() {
        super.onStop();
    }
    
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        this.updateEmptyView();
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.initComponents();
        this.updateUiForArguments();
        this.getListView().setVisibility(8);
        this.getBackupListView().setVisibility(0);
    }
    
    private final class PrintersAdapter extends BaseAdapter implements LoaderManager$LoaderCallbacks<List<PrinterInfo>>, Filterable
    {
        private final List<PrinterInfo> mFilteredPrinters;
        private CharSequence mLastSearchString;
        private final Object mLock;
        private final List<PrinterInfo> mPrinters;
        
        private PrintersAdapter() {
            this.mLock = new Object();
            this.mPrinters = new ArrayList<PrinterInfo>();
            this.mFilteredPrinters = new ArrayList<PrinterInfo>();
        }
        
        public void disable() {
            PrintServiceSettingsFragment.this.getLoaderManager().destroyLoader(1);
            this.mPrinters.clear();
        }
        
        public void enable() {
            PrintServiceSettingsFragment.this.getLoaderManager().initLoader(1, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
        }
        
        public int getCount() {
            synchronized (this.mLock) {
                return this.mFilteredPrinters.size();
            }
        }
        
        public Filter getFilter() {
            return new Filter() {
                protected Filter$FilterResults performFiltering(final CharSequence charSequence) {
                    synchronized (PrintersAdapter.this.mLock) {
                        if (TextUtils.isEmpty(charSequence)) {
                            return null;
                        }
                        final Filter$FilterResults filter$FilterResults = new Filter$FilterResults();
                        final ArrayList<PrinterInfo> values = new ArrayList<PrinterInfo>();
                        final String lowerCase = charSequence.toString().toLowerCase();
                        for (int size = PrintersAdapter.this.mPrinters.size(), i = 0; i < size; ++i) {
                            final PrinterInfo printerInfo = PrintersAdapter.this.mPrinters.get(i);
                            final String name = printerInfo.getName();
                            if (name != null && name.toLowerCase().contains(lowerCase)) {
                                values.add(printerInfo);
                            }
                        }
                        filter$FilterResults.values = values;
                        filter$FilterResults.count = values.size();
                        return filter$FilterResults;
                    }
                }
                
                protected void publishResults(final CharSequence charSequence, final Filter$FilterResults filter$FilterResults) {
                    synchronized (PrintersAdapter.this.mLock) {
                        PrintersAdapter.this.mLastSearchString = charSequence;
                        PrintersAdapter.this.mFilteredPrinters.clear();
                        if (filter$FilterResults == null) {
                            PrintersAdapter.this.mFilteredPrinters.addAll(PrintersAdapter.this.mPrinters);
                        }
                        else {
                            PrintersAdapter.this.mFilteredPrinters.addAll((Collection)filter$FilterResults.values);
                        }
                        // monitorexit(PrintersAdapter.access$600(this.this$1))
                        PrintersAdapter.this.notifyDataSetChanged();
                    }
                }
            };
        }
        
        public Object getItem(final int n) {
            synchronized (this.mLock) {
                return this.mFilteredPrinters.get(n);
            }
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public int getUnfilteredCount() {
            return this.mPrinters.size();
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View inflate = view;
            if (view == null) {
                inflate = PrintServiceSettingsFragment.this.getActivity().getLayoutInflater().inflate(2131558697, viewGroup, false);
            }
            inflate.setEnabled(this.isActionable(n));
            final PrinterInfo printerInfo = (PrinterInfo)this.getItem(n);
            final String name = printerInfo.getName();
            final String description = printerInfo.getDescription();
            final Drawable loadIcon = printerInfo.loadIcon((Context)PrintServiceSettingsFragment.this.getActivity());
            ((TextView)inflate.findViewById(R.id.title)).setText((CharSequence)name);
            final TextView textView = (TextView)inflate.findViewById(2131362665);
            if (!TextUtils.isEmpty((CharSequence)description)) {
                textView.setText((CharSequence)description);
                textView.setVisibility(0);
            }
            else {
                textView.setText((CharSequence)null);
                textView.setVisibility(8);
            }
            final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131362378);
            if (printerInfo.getInfoIntent() != null) {
                linearLayout.setVisibility(0);
                linearLayout.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        try {
                            PrintServiceSettingsFragment.this.getActivity().startIntentSender(printerInfo.getInfoIntent().getIntentSender(), (Intent)null, 0, 0, 0);
                        }
                        catch (IntentSender$SendIntentException ex) {
                            Log.e("PrintServiceSettingsFragment", "Could not execute pending info intent: %s", (Throwable)ex);
                        }
                    }
                });
            }
            else {
                linearLayout.setVisibility(8);
            }
            final ImageView imageView = (ImageView)inflate.findViewById(R.id.icon);
            if (loadIcon != null) {
                imageView.setVisibility(0);
                if (!this.isActionable(n)) {
                    loadIcon.mutate();
                    final TypedValue typedValue = new TypedValue();
                    PrintServiceSettingsFragment.this.getActivity().getTheme().resolveAttribute(16842803, typedValue, true);
                    loadIcon.setAlpha((int)(typedValue.getFloat() * 255.0f));
                }
                imageView.setImageDrawable(loadIcon);
            }
            else {
                imageView.setVisibility(8);
            }
            return inflate;
        }
        
        public boolean isActionable(final int n) {
            return ((PrinterInfo)this.getItem(n)).getStatus() != 3;
        }
        
        public Loader<List<PrinterInfo>> onCreateLoader(final int n, final Bundle bundle) {
            if (n == 1) {
                return new PrintersLoader(PrintServiceSettingsFragment.this.getContext());
            }
            return null;
        }
        
        public void onLoadFinished(final Loader<List<PrinterInfo>> loader, final List<PrinterInfo> list) {
            synchronized (this.mLock) {
                this.mPrinters.clear();
                for (int size = list.size(), i = 0; i < size; ++i) {
                    final PrinterInfo printerInfo = list.get(i);
                    if (printerInfo.getId().getServiceName().equals((Object)PrintServiceSettingsFragment.this.mComponentName)) {
                        this.mPrinters.add(printerInfo);
                    }
                }
                this.mFilteredPrinters.clear();
                this.mFilteredPrinters.addAll(this.mPrinters);
                if (!TextUtils.isEmpty(this.mLastSearchString)) {
                    this.getFilter().filter(this.mLastSearchString);
                }
                // monitorexit(this.mLock)
                this.notifyDataSetChanged();
            }
        }
        
        public void onLoaderReset(final Loader<List<PrinterInfo>> loader) {
            synchronized (this.mLock) {
                this.mPrinters.clear();
                this.mFilteredPrinters.clear();
                this.mLastSearchString = null;
                // monitorexit(this.mLock)
                this.notifyDataSetInvalidated();
            }
        }
    }
    
    private static class PrintersLoader extends Loader<List<PrinterInfo>>
    {
        private PrinterDiscoverySession mDiscoverySession;
        private final Map<PrinterId, PrinterInfo> mPrinters;
        
        public PrintersLoader(final Context context) {
            super(context);
            this.mPrinters = new LinkedHashMap<PrinterId, PrinterInfo>();
        }
        
        private boolean cancelInternal() {
            if (this.mDiscoverySession != null && this.mDiscoverySession.isPrinterDiscoveryStarted()) {
                this.mDiscoverySession.stopPrinterDiscovery();
                return true;
            }
            return false;
        }
        
        private void loadInternal() {
            if (this.mDiscoverySession == null) {
                (this.mDiscoverySession = ((PrintManager)this.getContext().getSystemService("print")).createPrinterDiscoverySession()).setOnPrintersChangeListener((PrinterDiscoverySession$OnPrintersChangeListener)new PrinterDiscoverySession$OnPrintersChangeListener() {
                    public void onPrintersChanged() {
                        PrintersLoader.this.deliverResult(new ArrayList<PrinterInfo>(PrintersLoader.this.mDiscoverySession.getPrinters()));
                    }
                });
            }
            this.mDiscoverySession.startPrinterDiscovery((List)null);
        }
        
        public void deliverResult(final List<PrinterInfo> list) {
            if (this.isStarted()) {
                super.deliverResult((Object)list);
            }
        }
        
        protected void onAbandon() {
            this.onStopLoading();
        }
        
        protected boolean onCancelLoad() {
            return this.cancelInternal();
        }
        
        protected void onForceLoad() {
            this.loadInternal();
        }
        
        protected void onReset() {
            this.onStopLoading();
            this.mPrinters.clear();
            if (this.mDiscoverySession != null) {
                this.mDiscoverySession.destroy();
                this.mDiscoverySession = null;
            }
        }
        
        protected void onStartLoading() {
            if (!this.mPrinters.isEmpty()) {
                this.deliverResult(new ArrayList<PrinterInfo>(this.mPrinters.values()));
            }
            this.onForceLoad();
        }
        
        protected void onStopLoading() {
            this.onCancelLoad();
        }
    }
}
