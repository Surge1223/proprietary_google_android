package com.android.settings.print;

import android.view.MenuItem;
import android.view.View;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.Menu;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import android.print.PrintJobInfo;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.print.PrintJob;
import android.print.PrintManager;
import android.print.PrintManager$PrintJobStateChangeListener;
import android.print.PrintJobId;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class PrintJobSettingsFragment extends SettingsPreferenceFragment
{
    private static final String LOG_TAG;
    private Preference mMessagePreference;
    private PrintJobId mPrintJobId;
    private Preference mPrintJobPreference;
    private final PrintManager$PrintJobStateChangeListener mPrintJobStateChangeListener;
    private PrintManager mPrintManager;
    
    static {
        LOG_TAG = PrintJobSettingsFragment.class.getSimpleName();
    }
    
    public PrintJobSettingsFragment() {
        this.mPrintJobStateChangeListener = (PrintManager$PrintJobStateChangeListener)new PrintManager$PrintJobStateChangeListener() {
            public void onPrintJobStateChanged(final PrintJobId printJobId) {
                PrintJobSettingsFragment.this.updateUi();
            }
        };
    }
    
    private PrintJob getPrintJob() {
        return this.mPrintManager.getPrintJob(this.mPrintJobId);
    }
    
    private void processArguments() {
        String s;
        if ((s = this.getArguments().getString("EXTRA_PRINT_JOB_ID")) == null && (s = this.getIntent().getStringExtra("EXTRA_PRINT_JOB_ID")) == null) {
            Log.w(PrintJobSettingsFragment.LOG_TAG, "EXTRA_PRINT_JOB_ID not set");
            this.finish();
            return;
        }
        this.mPrintJobId = PrintJobId.unflattenFromString(s);
    }
    
    private void updateUi() {
        final PrintJob printJob = this.getPrintJob();
        if (printJob == null) {
            this.finish();
            return;
        }
        if (!printJob.isCancelled() && !printJob.isCompleted()) {
            final PrintJobInfo info = printJob.getInfo();
            final int state = info.getState();
            if (state != 6) {
                switch (state) {
                    case 4: {
                        if (!printJob.getInfo().isCancelling()) {
                            this.mPrintJobPreference.setTitle(this.getString(2131888641, new Object[] { info.getLabel() }));
                            break;
                        }
                        this.mPrintJobPreference.setTitle(this.getString(2131888643, new Object[] { info.getLabel() }));
                        break;
                    }
                    case 2:
                    case 3: {
                        if (!printJob.getInfo().isCancelling()) {
                            this.mPrintJobPreference.setTitle(this.getString(2131888658, new Object[] { info.getLabel() }));
                            break;
                        }
                        this.mPrintJobPreference.setTitle(this.getString(2131888643, new Object[] { info.getLabel() }));
                        break;
                    }
                    case 1: {
                        this.mPrintJobPreference.setTitle(this.getString(2131888644, new Object[] { info.getLabel() }));
                        break;
                    }
                }
            }
            else {
                this.mPrintJobPreference.setTitle(this.getString(2131888645, new Object[] { info.getLabel() }));
            }
            this.mPrintJobPreference.setSummary(this.getString(2131888648, new Object[] { info.getPrinterName(), DateUtils.formatSameDayTime(info.getCreationTime(), info.getCreationTime(), 3, 3) }));
            final TypedArray obtainStyledAttributes = this.getActivity().obtainStyledAttributes(new int[] { 16843817 });
            final int color = obtainStyledAttributes.getColor(0, 0);
            obtainStyledAttributes.recycle();
            final int state2 = info.getState();
            Label_0424: {
                if (state2 != 6) {
                    switch (state2) {
                        default: {
                            break Label_0424;
                        }
                        case 2:
                        case 3: {
                            final Drawable drawable = this.getActivity().getDrawable(17302725);
                            drawable.setTint(color);
                            this.mPrintJobPreference.setIcon(drawable);
                            break Label_0424;
                        }
                        case 4: {
                            break;
                        }
                    }
                }
                final Drawable drawable2 = this.getActivity().getDrawable(17302726);
                drawable2.setTint(color);
                this.mPrintJobPreference.setIcon(drawable2);
            }
            final CharSequence status = info.getStatus(this.getPackageManager());
            if (!TextUtils.isEmpty(status)) {
                if (this.getPreferenceScreen().findPreference("print_job_message_preference") == null) {
                    this.getPreferenceScreen().addPreference(this.mMessagePreference);
                }
                this.mMessagePreference.setSummary(status);
            }
            else {
                this.getPreferenceScreen().removePreference(this.mMessagePreference);
            }
            this.getActivity().invalidateOptionsMenu();
            return;
        }
        this.finish();
    }
    
    @Override
    public int getMetricsCategory() {
        return 78;
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        final PrintJob printJob = this.getPrintJob();
        if (printJob == null) {
            return;
        }
        if (!printJob.getInfo().isCancelling()) {
            menu.add(0, 1, 0, (CharSequence)this.getString(2131888642)).setShowAsAction(1);
        }
        if (printJob.isFailed()) {
            menu.add(0, 2, 0, (CharSequence)this.getString(2131888659)).setShowAsAction(1);
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.addPreferencesFromResource(2132082814);
        this.mPrintJobPreference = this.findPreference("print_job_preference");
        this.mMessagePreference = this.findPreference("print_job_message_preference");
        this.mPrintManager = ((PrintManager)this.getActivity().getSystemService("print")).getGlobalPrintManagerForUser(this.getActivity().getUserId());
        this.getActivity().getActionBar().setTitle(2131888656);
        this.processArguments();
        this.setHasOptionsMenu(true);
        return onCreateView;
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final PrintJob printJob = this.getPrintJob();
        if (printJob != null) {
            switch (menuItem.getItemId()) {
                case 2: {
                    printJob.restart();
                    this.finish();
                    return true;
                }
                case 1: {
                    printJob.cancel();
                    this.finish();
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.mPrintManager.addPrintJobStateChangeListener(this.mPrintJobStateChangeListener);
        this.updateUi();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.mPrintManager.removePrintJobStateChangeListener(this.mPrintJobStateChangeListener);
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.getListView().setEnabled(false);
    }
}
