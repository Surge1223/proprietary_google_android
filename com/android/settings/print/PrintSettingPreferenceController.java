package com.android.settings.print;

import com.android.settingslib.RestrictedPreference;
import android.print.PrintJobId;
import java.util.Iterator;
import java.util.List;
import android.print.PrintJob;
import android.support.v7.preference.PreferenceScreen;
import android.print.PrintJobInfo;
import android.content.Context;
import android.print.PrintManager;
import android.support.v7.preference.Preference;
import android.content.pm.PackageManager;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.print.PrintManager$PrintJobStateChangeListener;
import com.android.settings.core.BasePreferenceController;

public class PrintSettingPreferenceController extends BasePreferenceController implements PrintManager$PrintJobStateChangeListener, LifecycleObserver, OnStart, OnStop
{
    private static final String KEY_PRINTING_SETTINGS = "connected_device_printing";
    private final PackageManager mPackageManager;
    private Preference mPreference;
    private final PrintManager mPrintManager;
    
    public PrintSettingPreferenceController(final Context context) {
        super(context, "connected_device_printing");
        this.mPackageManager = context.getPackageManager();
        this.mPrintManager = ((PrintManager)context.getSystemService("print")).getGlobalPrintManagerForUser(context.getUserId());
    }
    
    static boolean shouldShowToUser(final PrintJobInfo printJobInfo) {
        final int state = printJobInfo.getState();
        if (state != 6) {
            switch (state) {
                default: {
                    return false;
                }
                case 2:
                case 3:
                case 4: {
                    break;
                }
            }
        }
        return true;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mPackageManager.hasSystemFeature("android.software.print")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    public CharSequence getSummary() {
        final List printJobs = this.mPrintManager.getPrintJobs();
        int n = 0;
        int n2 = 0;
        if (printJobs != null) {
            final Iterator<PrintJob> iterator = printJobs.iterator();
            while (true) {
                n = n2;
                if (!iterator.hasNext()) {
                    break;
                }
                int n3 = n2;
                if (shouldShowToUser(iterator.next().getInfo())) {
                    n3 = n2 + 1;
                }
                n2 = n3;
            }
        }
        if (n > 0) {
            return this.mContext.getResources().getQuantityString(2131755054, n, new Object[] { n });
        }
        final List printServices = this.mPrintManager.getPrintServices(1);
        if (printServices != null && !printServices.isEmpty()) {
            final int size = printServices.size();
            return this.mContext.getResources().getQuantityString(2131755055, size, new Object[] { size });
        }
        return this.mContext.getText(2131888665);
    }
    
    public void onPrintJobStateChanged(final PrintJobId printJobId) {
        this.updateState(this.mPreference);
    }
    
    public void onStart() {
        this.mPrintManager.addPrintJobStateChangeListener((PrintManager$PrintJobStateChangeListener)this);
    }
    
    public void onStop() {
        this.mPrintManager.removePrintJobStateChangeListener((PrintManager$PrintJobStateChangeListener)this);
    }
    
    public void updateState(final Preference preference) {
        super.updateState(preference);
        ((RestrictedPreference)preference).checkRestrictionAndSetDisabled("no_printing");
    }
}
