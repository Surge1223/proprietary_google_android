package com.android.settings;

import android.content.IntentFilter;
import android.support.v7.preference.Preference;
import android.telephony.TelephonyManager;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.widget.EditText;
import android.text.TextUtils;
import android.widget.Toast;
import android.util.Log;
import android.widget.TabHost$TabSpec;
import android.view.View;
import android.telephony.SubscriptionInfo;
import com.android.internal.telephony.PhoneFactory;
import android.telephony.SubscriptionManager;
import android.content.Intent;
import android.content.Context;
import android.os.AsyncResult;
import android.os.Message;
import android.widget.TabWidget;
import android.widget.TabHost$OnTabChangeListener;
import android.widget.TabHost;
import android.content.BroadcastReceiver;
import android.content.res.Resources;
import android.support.v14.preference.SwitchPreference;
import com.android.internal.telephony.Phone;
import android.widget.ListView;
import android.os.Handler;
import android.widget.TabHost$TabContentFactory;

public class IccLockSettings extends SettingsPreferenceFragment implements OnPinEnteredListener
{
    private int mDialogState;
    private TabHost$TabContentFactory mEmptyTabContent;
    private String mError;
    private Handler mHandler;
    private ListView mListView;
    private String mNewPin;
    private String mOldPin;
    private Phone mPhone;
    private String mPin;
    private EditPinPreference mPinDialog;
    private SwitchPreference mPinToggle;
    private Resources mRes;
    private final BroadcastReceiver mSimStateReceiver;
    private TabHost mTabHost;
    private TabHost$OnTabChangeListener mTabListener;
    private TabWidget mTabWidget;
    private boolean mToState;
    
    public IccLockSettings() {
        this.mDialogState = 0;
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                final AsyncResult asyncResult = (AsyncResult)message.obj;
                final int what = message.what;
                final boolean b = false;
                boolean b2 = false;
                switch (what) {
                    case 102: {
                        IccLockSettings.this.updatePreferences();
                        break;
                    }
                    case 101: {
                        final IccLockSettings this$0 = IccLockSettings.this;
                        if (asyncResult.exception == null) {
                            b2 = true;
                        }
                        this$0.iccPinChanged(b2, message.arg1);
                        break;
                    }
                    case 100: {
                        final IccLockSettings this$2 = IccLockSettings.this;
                        boolean b3 = b;
                        if (asyncResult.exception == null) {
                            b3 = true;
                        }
                        this$2.iccLockChanged(b3, message.arg1);
                        break;
                    }
                }
            }
        };
        this.mSimStateReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if ("android.intent.action.SIM_STATE_CHANGED".equals(intent.getAction())) {
                    IccLockSettings.this.mHandler.sendMessage(IccLockSettings.this.mHandler.obtainMessage(102));
                }
            }
        };
        this.mTabListener = (TabHost$OnTabChangeListener)new TabHost$OnTabChangeListener() {
            public void onTabChanged(final String s) {
                final SubscriptionInfo activeSubscriptionInfoForSimSlotIndex = SubscriptionManager.from(IccLockSettings.this.getActivity().getBaseContext()).getActiveSubscriptionInfoForSimSlotIndex(Integer.parseInt(s));
                final IccLockSettings this$0 = IccLockSettings.this;
                Phone phone;
                if (activeSubscriptionInfoForSimSlotIndex == null) {
                    phone = null;
                }
                else {
                    phone = PhoneFactory.getPhone(SubscriptionManager.getPhoneId(activeSubscriptionInfoForSimSlotIndex.getSubscriptionId()));
                }
                this$0.mPhone = phone;
                IccLockSettings.this.updatePreferences();
            }
        };
        this.mEmptyTabContent = (TabHost$TabContentFactory)new TabHost$TabContentFactory() {
            public View createTabContent(final String s) {
                return new View(IccLockSettings.this.mTabHost.getContext());
            }
        };
    }
    
    private TabHost$TabSpec buildTabSpec(final String s, final String indicator) {
        return this.mTabHost.newTabSpec(s).setIndicator((CharSequence)indicator).setContent(this.mEmptyTabContent);
    }
    
    private String getPinPasswordErrorMessage(final int n) {
        String s;
        if (n == 0) {
            s = this.mRes.getString(2131890231);
        }
        else if (n > 0) {
            s = this.mRes.getQuantityString(2131755074, n, new Object[] { n });
        }
        else {
            s = this.mRes.getString(2131888574);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("getPinPasswordErrorMessage: attemptsRemaining=");
        sb.append(n);
        sb.append(" displayMessage=");
        sb.append(s);
        Log.d("IccLockSettings", sb.toString());
        return s;
    }
    
    private void iccLockChanged(final boolean b, final int n) {
        if (b) {
            this.mPinToggle.setChecked(this.mToState);
        }
        else {
            Toast.makeText(this.getContext(), (CharSequence)this.getPinPasswordErrorMessage(n), 1).show();
        }
        this.mPinToggle.setEnabled(true);
        this.resetDialogState();
    }
    
    private void iccPinChanged(final boolean b, final int n) {
        if (!b) {
            Toast.makeText(this.getContext(), (CharSequence)this.getPinPasswordErrorMessage(n), 1).show();
        }
        else {
            Toast.makeText(this.getContext(), (CharSequence)this.mRes.getString(2131889127), 0).show();
        }
        this.resetDialogState();
    }
    
    private boolean reasonablePin(final String s) {
        return s != null && s.length() >= 4 && s.length() <= 8;
    }
    
    private void resetDialogState() {
        this.mError = null;
        this.mDialogState = 2;
        this.mPin = "";
        this.setDialogValues();
        this.mDialogState = 0;
    }
    
    private void setDialogValues() {
        this.mPinDialog.setText(this.mPin);
        String s = "";
        switch (this.mDialogState) {
            case 4: {
                s = this.mRes.getString(2131889162);
                this.mPinDialog.setDialogTitle(this.mRes.getString(2131889126));
                break;
            }
            case 3: {
                s = this.mRes.getString(2131889136);
                this.mPinDialog.setDialogTitle(this.mRes.getString(2131889126));
                break;
            }
            case 2: {
                s = this.mRes.getString(2131889138);
                this.mPinDialog.setDialogTitle(this.mRes.getString(2131889126));
                break;
            }
            case 1: {
                final String string = this.mRes.getString(2131889139);
                final EditPinPreference mPinDialog = this.mPinDialog;
                String dialogTitle;
                if (this.mToState) {
                    dialogTitle = this.mRes.getString(2131889134);
                }
                else {
                    dialogTitle = this.mRes.getString(2131889128);
                }
                mPinDialog.setDialogTitle(dialogTitle);
                s = string;
                break;
            }
        }
        String string2 = s;
        if (this.mError != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mError);
            sb.append("\n");
            sb.append(s);
            string2 = sb.toString();
            this.mError = null;
        }
        this.mPinDialog.setDialogMessage(string2);
    }
    
    private void showPinDialog() {
        if (this.mDialogState == 0) {
            return;
        }
        this.setDialogValues();
        this.mPinDialog.showPinDialog();
        final EditText editText = this.mPinDialog.getEditText();
        if (!TextUtils.isEmpty((CharSequence)this.mPin) && editText != null) {
            editText.setSelection(this.mPin.length());
        }
    }
    
    private void tryChangeIccLockState() {
        this.mPhone.getIccCard().setIccLockEnabled(this.mToState, this.mPin, Message.obtain(this.mHandler, 100));
        this.mPinToggle.setEnabled(false);
    }
    
    private void tryChangePin() {
        this.mPhone.getIccCard().changeIccLockPassword(this.mOldPin, this.mNewPin, Message.obtain(this.mHandler, 101));
    }
    
    private void updatePreferences() {
        final EditPinPreference mPinDialog = this.mPinDialog;
        final boolean b = false;
        if (mPinDialog != null) {
            this.mPinDialog.setEnabled(this.mPhone != null);
        }
        if (this.mPinToggle != null) {
            final SwitchPreference mPinToggle = this.mPinToggle;
            boolean enabled = b;
            if (this.mPhone != null) {
                enabled = true;
            }
            mPinToggle.setEnabled(enabled);
            if (this.mPhone != null) {
                this.mPinToggle.setChecked(this.mPhone.getIccCard().getIccLockEnabled());
            }
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887814;
    }
    
    @Override
    public int getMetricsCategory() {
        return 56;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (Utils.isMonkeyRunning()) {
            this.finish();
            return;
        }
        this.addPreferencesFromResource(2132082830);
        this.mPinDialog = (EditPinPreference)this.findPreference("sim_pin");
        this.mPinToggle = (SwitchPreference)this.findPreference("sim_toggle");
        if (bundle != null && bundle.containsKey("dialogState")) {
            this.mDialogState = bundle.getInt("dialogState");
            this.mPin = bundle.getString("dialogPin");
            this.mError = bundle.getString("dialogError");
            this.mToState = bundle.getBoolean("enableState");
            switch (this.mDialogState) {
                case 4: {
                    this.mOldPin = bundle.getString("oldPinCode");
                    this.mNewPin = bundle.getString("newPinCode");
                    break;
                }
                case 3: {
                    this.mOldPin = bundle.getString("oldPinCode");
                    break;
                }
            }
        }
        this.mPinDialog.setOnPinEnteredListener((EditPinPreference.OnPinEnteredListener)this);
        this.getPreferenceScreen().setPersistent(false);
        this.mRes = this.getResources();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final int simCount = ((TelephonyManager)this.getContext().getSystemService("phone")).getSimCount();
        if (simCount > 1) {
            final View inflate = layoutInflater.inflate(2131558592, viewGroup, false);
            final ViewGroup viewGroup2 = (ViewGroup)inflate.findViewById(2131362462);
            Utils.prepareCustomPreferencesList(viewGroup, inflate, (View)viewGroup2, false);
            viewGroup2.addView(super.onCreateView(layoutInflater, viewGroup2, bundle));
            this.mTabHost = (TabHost)inflate.findViewById(16908306);
            this.mTabWidget = (TabWidget)inflate.findViewById(16908307);
            this.mListView = (ListView)inflate.findViewById(16908298);
            this.mTabHost.setup();
            this.mTabHost.setOnTabChangedListener(this.mTabListener);
            this.mTabHost.clearAllTabs();
            final SubscriptionManager from = SubscriptionManager.from(this.getContext());
            for (int i = 0; i < simCount; ++i) {
                final SubscriptionInfo activeSubscriptionInfoForSimSlotIndex = from.getActiveSubscriptionInfoForSimSlotIndex(i);
                final TabHost mTabHost = this.mTabHost;
                CharSequence charSequence;
                if (activeSubscriptionInfoForSimSlotIndex == null) {
                    charSequence = this.getContext().getString(2131889133, new Object[] { i + 1 });
                }
                else {
                    charSequence = activeSubscriptionInfoForSimSlotIndex.getDisplayName();
                }
                mTabHost.addTab(this.buildTabSpec(String.valueOf(i), String.valueOf(charSequence)));
            }
            final SubscriptionInfo activeSubscriptionInfoForSimSlotIndex2 = from.getActiveSubscriptionInfoForSimSlotIndex(0);
            Phone phone;
            if (activeSubscriptionInfoForSimSlotIndex2 == null) {
                phone = null;
            }
            else {
                phone = PhoneFactory.getPhone(SubscriptionManager.getPhoneId(activeSubscriptionInfoForSimSlotIndex2.getSubscriptionId()));
            }
            this.mPhone = phone;
            if (bundle != null && bundle.containsKey("currentTab")) {
                this.mTabHost.setCurrentTabByTag(bundle.getString("currentTab"));
            }
            return inflate;
        }
        this.mPhone = PhoneFactory.getDefaultPhone();
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.getContext().unregisterReceiver(this.mSimStateReceiver);
    }
    
    @Override
    public void onPinEntered(final EditPinPreference editPinPreference, final boolean b) {
        if (!b) {
            this.resetDialogState();
            return;
        }
        this.mPin = editPinPreference.getText();
        if (!this.reasonablePin(this.mPin)) {
            this.mError = this.mRes.getString(2131889113);
            this.showPinDialog();
            return;
        }
        switch (this.mDialogState) {
            case 4: {
                if (!this.mPin.equals(this.mNewPin)) {
                    this.mError = this.mRes.getString(2131889158);
                    this.mDialogState = 3;
                    this.mPin = null;
                    this.showPinDialog();
                    break;
                }
                this.mError = null;
                this.tryChangePin();
                break;
            }
            case 3: {
                this.mNewPin = this.mPin;
                this.mDialogState = 4;
                this.mPin = null;
                this.showPinDialog();
                break;
            }
            case 2: {
                this.mOldPin = this.mPin;
                this.mDialogState = 3;
                this.mError = null;
                this.mPin = null;
                this.showPinDialog();
                break;
            }
            case 1: {
                this.tryChangeIccLockState();
                break;
            }
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.mPinToggle) {
            this.mToState = this.mPinToggle.isChecked();
            this.mPinToggle.setChecked(this.mToState ^ true);
            this.mDialogState = 1;
            this.showPinDialog();
        }
        else if (preference == this.mPinDialog) {
            this.mDialogState = 2;
            return false;
        }
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.getContext().registerReceiver(this.mSimStateReceiver, new IntentFilter("android.intent.action.SIM_STATE_CHANGED"));
        if (this.mDialogState != 0) {
            this.showPinDialog();
        }
        else {
            this.resetDialogState();
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        if (this.mPinDialog.isDialogOpen()) {
            bundle.putInt("dialogState", this.mDialogState);
            bundle.putString("dialogPin", this.mPinDialog.getEditText().getText().toString());
            bundle.putString("dialogError", this.mError);
            bundle.putBoolean("enableState", this.mToState);
            switch (this.mDialogState) {
                case 4: {
                    bundle.putString("oldPinCode", this.mOldPin);
                    bundle.putString("newPinCode", this.mNewPin);
                    break;
                }
                case 3: {
                    bundle.putString("oldPinCode", this.mOldPin);
                    break;
                }
            }
        }
        else {
            super.onSaveInstanceState(bundle);
        }
        if (this.mTabHost != null) {
            bundle.putString("currentTab", this.mTabHost.getCurrentTabTag());
        }
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.updatePreferences();
    }
}
