package com.android.settings;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.os.UserManager;
import android.os.Bundle;

public class TestingSettings extends SettingsPreferenceFragment
{
    @Override
    public int getMetricsCategory() {
        return 89;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082842);
        if (!UserManager.get(this.getContext()).isAdminUser()) {
            this.getPreferenceScreen().removePreference(this.findPreference("radio_info_settings"));
        }
    }
}
