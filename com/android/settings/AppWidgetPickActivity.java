package com.android.settings;

import android.content.Intent;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.Bundle;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import java.util.List;
import android.appwidget.AppWidgetManager;

public class AppWidgetPickActivity extends ActivityPicker implements ItemConstructor<PickAdapter.Item>
{
    private int mAppWidgetId;
    private AppWidgetLoader<PickAdapter.Item> mAppWidgetLoader;
    private AppWidgetManager mAppWidgetManager;
    List<PickAdapter.Item> mItems;
    private PackageManager mPackageManager;
    
    public PickAdapter.Item createItem(final Context context, final AppWidgetProviderInfo appWidgetProviderInfo, final Bundle extras) {
        final String label = appWidgetProviderInfo.label;
        Drawable drawable = null;
        Drawable drawableForDensity = null;
        if (appWidgetProviderInfo.icon != 0) {
            try {
                final int densityDpi = context.getResources().getDisplayMetrics().densityDpi;
                if (densityDpi == 160 || densityDpi == 213 || densityDpi == 240 || densityDpi == 320 || densityDpi != 480) {}
                drawableForDensity = this.mPackageManager.getResourcesForApplication(appWidgetProviderInfo.provider.getPackageName()).getDrawableForDensity(appWidgetProviderInfo.icon, (int)(densityDpi * 0.75f + 0.5f));
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Can't load icon drawable 0x");
                sb.append(Integer.toHexString(appWidgetProviderInfo.icon));
                sb.append(" for provider: ");
                sb.append(appWidgetProviderInfo.provider);
                Log.w("AppWidgetPickActivity", sb.toString());
            }
            drawable = drawableForDensity;
            if (drawableForDensity == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Can't load icon drawable 0x");
                sb2.append(Integer.toHexString(appWidgetProviderInfo.icon));
                sb2.append(" for provider: ");
                sb2.append(appWidgetProviderInfo.provider);
                Log.w("AppWidgetPickActivity", sb2.toString());
                drawable = drawableForDensity;
            }
        }
        final PickAdapter.Item item = new PickAdapter.Item(context, label, drawable);
        item.packageName = appWidgetProviderInfo.provider.getPackageName();
        item.className = appWidgetProviderInfo.provider.getClassName();
        item.extras = extras;
        return item;
    }
    
    @Override
    protected List<PickAdapter.Item> getItems() {
        return this.mItems = this.mAppWidgetLoader.getItems(this.getIntent());
    }
    
    @Override
    public void onClick(final DialogInterface dialogInterface, int n) {
        final Intent intentForPosition = this.getIntentForPosition(n);
        if (this.mItems.get(n).extras != null) {
            this.setResultData(-1, intentForPosition);
        }
        else {
            Bundle bundle = null;
            try {
                if (intentForPosition.getExtras() != null) {
                    bundle = intentForPosition.getExtras().getBundle("appWidgetOptions");
                }
                this.mAppWidgetManager.bindAppWidgetId(this.mAppWidgetId, intentForPosition.getComponent(), bundle);
                n = -1;
            }
            catch (IllegalArgumentException ex) {
                n = 0;
            }
            this.setResultData(n, null);
        }
        this.finish();
    }
    
    public void onCreate(final Bundle bundle) {
        this.mPackageManager = this.getPackageManager();
        this.mAppWidgetManager = AppWidgetManager.getInstance((Context)this);
        this.mAppWidgetLoader = new AppWidgetLoader<PickAdapter.Item>((Context)this, this.mAppWidgetManager, (AppWidgetLoader.ItemConstructor<PickAdapter.Item>)this);
        super.onCreate(bundle);
        this.setResultData(0, null);
        final Intent intent = this.getIntent();
        if (intent.hasExtra("appWidgetId")) {
            this.mAppWidgetId = intent.getIntExtra("appWidgetId", 0);
        }
        else {
            this.finish();
        }
    }
    
    void setResultData(final int n, Intent intent) {
        if (intent == null) {
            intent = new Intent();
        }
        intent.putExtra("appWidgetId", this.mAppWidgetId);
        this.setResult(n, intent);
    }
}
