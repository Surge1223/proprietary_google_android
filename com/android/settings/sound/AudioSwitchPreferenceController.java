package com.android.settings.sound;

import android.content.Intent;
import android.media.MediaRouter$RouteGroup;
import android.media.MediaRouter$RouteInfo;
import android.media.AudioDeviceInfo;
import android.support.v7.preference.ListPreference;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.HeadsetProfile;
import com.android.settingslib.bluetooth.HearingAidProfile;
import com.android.settingslib.bluetooth.A2dpProfile;
import android.util.FeatureFlagUtils;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.media.MediaRouter$Callback;
import android.media.AudioDeviceCallback;
import com.android.settings.bluetooth.Utils;
import android.text.TextUtils;
import java.util.concurrent.ExecutionException;
import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.ArrayList;
import android.os.Looper;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import android.media.MediaRouter;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.os.Handler;
import android.bluetooth.BluetoothDevice;
import java.util.List;
import android.media.AudioManager;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.bluetooth.BluetoothCallback;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public abstract class AudioSwitchPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener, BluetoothCallback, LifecycleObserver, OnStart, OnStop
{
    private static final int INVALID_INDEX = -1;
    private static final String TAG = "AudioSwitchPreferenceController";
    protected final AudioManager mAudioManager;
    private final AudioManagerAudioDeviceCallback mAudioManagerAudioDeviceCallback;
    protected AudioSwitchCallback mAudioSwitchPreferenceCallback;
    protected final List<BluetoothDevice> mConnectedDevices;
    private final Handler mHandler;
    private LocalBluetoothManager mLocalBluetoothManager;
    protected final MediaRouter mMediaRouter;
    private final MediaRouterCallback mMediaRouterCallback;
    protected Preference mPreference;
    protected LocalBluetoothProfileManager mProfileManager;
    private final WiredHeadsetBroadcastReceiver mReceiver;
    protected int mSelectedIndex;
    
    public AudioSwitchPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
        this.mMediaRouter = (MediaRouter)context.getSystemService("media_router");
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mAudioManagerAudioDeviceCallback = new AudioManagerAudioDeviceCallback();
        this.mReceiver = new WiredHeadsetBroadcastReceiver();
        this.mMediaRouterCallback = new MediaRouterCallback();
        this.mConnectedDevices = new ArrayList<BluetoothDevice>();
        final FutureTask<LocalBluetoothManager> futureTask = new FutureTask<LocalBluetoothManager>(new _$$Lambda$AudioSwitchPreferenceController$GC_sYSWqqCmy3hCGLKM8AEFN__Y(this));
        try {
            futureTask.run();
            this.mLocalBluetoothManager = futureTask.get();
            if (this.mLocalBluetoothManager == null) {
                Log.e("AudioSwitchPreferenceController", "Bluetooth is not supported on this device");
                return;
            }
            this.mProfileManager = this.mLocalBluetoothManager.getProfileManager();
        }
        catch (InterruptedException | ExecutionException ex) {
            final Throwable t;
            Log.w("AudioSwitchPreferenceController", "Error getting LocalBluetoothManager.", t);
        }
    }
    
    private int getConnectedDeviceIndex(final String s) {
        if (this.mConnectedDevices != null) {
            for (int i = 0; i < this.mConnectedDevices.size(); ++i) {
                if (TextUtils.equals((CharSequence)this.mConnectedDevices.get(i).getAddress(), (CharSequence)s)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    private void register() {
        this.mLocalBluetoothManager.getEventManager().registerCallback(this);
        this.mAudioManager.registerAudioDeviceCallback((AudioDeviceCallback)this.mAudioManagerAudioDeviceCallback, this.mHandler);
        this.mMediaRouter.addCallback(4, (MediaRouter$Callback)this.mMediaRouterCallback);
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.HEADSET_PLUG");
        intentFilter.addAction("android.media.STREAM_DEVICES_CHANGED_ACTION");
        this.mContext.registerReceiver((BroadcastReceiver)this.mReceiver, intentFilter);
    }
    
    private void unregister() {
        this.mLocalBluetoothManager.getEventManager().unregisterCallback(this);
        this.mAudioManager.unregisterAudioDeviceCallback((AudioDeviceCallback)this.mAudioManagerAudioDeviceCallback);
        this.mMediaRouter.removeCallback((MediaRouter$Callback)this.mMediaRouterCallback);
        this.mContext.unregisterReceiver((BroadcastReceiver)this.mReceiver);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = preferenceScreen.findPreference(this.mPreferenceKey)).setVisible(false);
    }
    
    protected BluetoothDevice findActiveDevice(final int n) {
        if (n != 3 && n != 0) {
            return null;
        }
        if (this.isStreamFromOutputDevice(3, 896)) {
            return this.mProfileManager.getA2dpProfile().getActiveDevice();
        }
        if (this.isStreamFromOutputDevice(0, 112)) {
            return this.mProfileManager.getHeadsetProfile().getActiveDevice();
        }
        if (this.isStreamFromOutputDevice(n, 134217728)) {
            for (final BluetoothDevice bluetoothDevice : this.mProfileManager.getHearingAidProfile().getActiveDevices()) {
                if (bluetoothDevice != null && this.mConnectedDevices.contains(bluetoothDevice)) {
                    return bluetoothDevice;
                }
            }
        }
        return null;
    }
    
    @Override
    public final int getAvailabilityStatus() {
        return (!FeatureFlagUtils.isEnabled(this.mContext, "settings_audio_switcher") || !this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) ? 1 : 0;
    }
    
    protected List<BluetoothDevice> getConnectedA2dpDevices() {
        final ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
        final A2dpProfile a2dpProfile = this.mProfileManager.getA2dpProfile();
        if (a2dpProfile == null) {
            return list;
        }
        for (final BluetoothDevice bluetoothDevice : a2dpProfile.getConnectedDevices()) {
            if (bluetoothDevice.isConnected()) {
                list.add(bluetoothDevice);
            }
        }
        return list;
    }
    
    protected List<BluetoothDevice> getConnectedHearingAidDevices() {
        final ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
        final HearingAidProfile hearingAidProfile = this.mProfileManager.getHearingAidProfile();
        if (hearingAidProfile == null) {
            return list;
        }
        final ArrayList<Long> list2 = new ArrayList<Long>();
        for (final BluetoothDevice bluetoothDevice : hearingAidProfile.getConnectedDevices()) {
            final long hiSyncId = hearingAidProfile.getHiSyncId(bluetoothDevice);
            if (!list2.contains(hiSyncId) && bluetoothDevice.isConnected()) {
                list2.add(hiSyncId);
                list.add(bluetoothDevice);
            }
        }
        return list;
    }
    
    protected List<BluetoothDevice> getConnectedHfpDevices() {
        final ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
        final HeadsetProfile headsetProfile = this.mProfileManager.getHeadsetProfile();
        if (headsetProfile == null) {
            return list;
        }
        for (final BluetoothDevice bluetoothDevice : headsetProfile.getConnectedDevices()) {
            if (bluetoothDevice.isConnected()) {
                list.add(bluetoothDevice);
            }
        }
        return list;
    }
    
    int getDefaultDeviceIndex() {
        return this.mConnectedDevices.size();
    }
    
    protected boolean isStreamFromOutputDevice(final int n, final int n2) {
        return (this.mAudioManager.getDevicesForStream(n) & n2) != 0x0;
    }
    
    @Override
    public void onActiveDeviceChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        this.updateState(this.mPreference);
    }
    
    @Override
    public void onAudioModeChanged() {
        this.updateState(this.mPreference);
    }
    
    @Override
    public void onBluetoothStateChanged(final int n) {
    }
    
    @Override
    public void onConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onDeviceAdded(final CachedBluetoothDevice cachedBluetoothDevice) {
        this.updateState(this.mPreference);
    }
    
    @Override
    public void onDeviceBondStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onDeviceDeleted(final CachedBluetoothDevice cachedBluetoothDevice) {
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final String s = (String)o;
        if (!(preference instanceof ListPreference)) {
            return false;
        }
        final ListPreference listPreference = (ListPreference)preference;
        if (TextUtils.equals((CharSequence)s, this.mContext.getText(2131888252))) {
            this.mSelectedIndex = this.getDefaultDeviceIndex();
            this.setActiveBluetoothDevice(null);
            listPreference.setSummary(this.mContext.getText(2131888252));
        }
        else {
            final int connectedDeviceIndex = this.getConnectedDeviceIndex(s);
            if (connectedDeviceIndex == -1) {
                return false;
            }
            final BluetoothDevice activeBluetoothDevice = this.mConnectedDevices.get(connectedDeviceIndex);
            this.mSelectedIndex = connectedDeviceIndex;
            this.setActiveBluetoothDevice(activeBluetoothDevice);
            listPreference.setSummary(activeBluetoothDevice.getAliasName());
        }
        return true;
    }
    
    @Override
    public void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
        this.updateState(this.mPreference);
    }
    
    @Override
    public void onScanningStateChanged(final boolean b) {
    }
    
    @Override
    public void onStart() {
        if (this.mLocalBluetoothManager == null) {
            Log.e("AudioSwitchPreferenceController", "Bluetooth is not supported on this device");
            return;
        }
        this.mLocalBluetoothManager.setForegroundActivity(this.mContext);
        this.register();
    }
    
    @Override
    public void onStop() {
        if (this.mLocalBluetoothManager == null) {
            Log.e("AudioSwitchPreferenceController", "Bluetooth is not supported on this device");
            return;
        }
        this.mLocalBluetoothManager.setForegroundActivity(null);
        this.unregister();
    }
    
    public abstract void setActiveBluetoothDevice(final BluetoothDevice p0);
    
    public void setCallback(final AudioSwitchCallback mAudioSwitchPreferenceCallback) {
        this.mAudioSwitchPreferenceCallback = mAudioSwitchPreferenceCallback;
    }
    
    void setPreference(final CharSequence[] entries, final CharSequence[] entryValues, final Preference preference) {
        final ListPreference listPreference = (ListPreference)preference;
        listPreference.setEntries(entries);
        listPreference.setEntryValues(entryValues);
        listPreference.setValueIndex(this.mSelectedIndex);
        listPreference.setSummary(entries[this.mSelectedIndex]);
        this.mAudioSwitchPreferenceCallback.onPreferenceDataChanged(listPreference);
    }
    
    void setupPreferenceEntries(final CharSequence[] array, final CharSequence[] array2, final BluetoothDevice bluetoothDevice) {
        array[this.mSelectedIndex = this.getDefaultDeviceIndex()] = this.mContext.getText(2131888252);
        array2[this.mSelectedIndex] = this.mContext.getText(2131888252);
        for (int i = 0; i < this.mConnectedDevices.size(); ++i) {
            final BluetoothDevice bluetoothDevice2 = this.mConnectedDevices.get(i);
            array[i] = bluetoothDevice2.getAliasName();
            array2[i] = bluetoothDevice2.getAddress();
            if (bluetoothDevice2.equals((Object)bluetoothDevice)) {
                this.mSelectedIndex = i;
            }
        }
    }
    
    private class AudioManagerAudioDeviceCallback extends AudioDeviceCallback
    {
        public void onAudioDevicesAdded(final AudioDeviceInfo[] array) {
            AudioSwitchPreferenceController.this.updateState(AudioSwitchPreferenceController.this.mPreference);
        }
        
        public void onAudioDevicesRemoved(final AudioDeviceInfo[] array) {
            AudioSwitchPreferenceController.this.updateState(AudioSwitchPreferenceController.this.mPreference);
        }
    }
    
    public interface AudioSwitchCallback
    {
        void onPreferenceDataChanged(final ListPreference p0);
    }
    
    private class MediaRouterCallback extends MediaRouter$Callback
    {
        public void onRouteAdded(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
            if (mediaRouter$RouteInfo != null && !mediaRouter$RouteInfo.isDefault()) {
                AudioSwitchPreferenceController.this.updateState(AudioSwitchPreferenceController.this.mPreference);
            }
        }
        
        public void onRouteChanged(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
            if (mediaRouter$RouteInfo != null && !mediaRouter$RouteInfo.isDefault()) {
                AudioSwitchPreferenceController.this.updateState(AudioSwitchPreferenceController.this.mPreference);
            }
        }
        
        public void onRouteGrouped(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo, final MediaRouter$RouteGroup mediaRouter$RouteGroup, final int n) {
        }
        
        public void onRouteRemoved(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
        }
        
        public void onRouteSelected(final MediaRouter mediaRouter, final int n, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
        }
        
        public void onRouteUngrouped(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo, final MediaRouter$RouteGroup mediaRouter$RouteGroup) {
        }
        
        public void onRouteUnselected(final MediaRouter mediaRouter, final int n, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
        }
        
        public void onRouteVolumeChanged(final MediaRouter mediaRouter, final MediaRouter$RouteInfo mediaRouter$RouteInfo) {
        }
    }
    
    private class WiredHeadsetBroadcastReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if ("android.intent.action.HEADSET_PLUG".equals(action) || "android.media.STREAM_DEVICES_CHANGED_ACTION".equals(action)) {
                AudioSwitchPreferenceController.this.updateState(AudioSwitchPreferenceController.this.mPreference);
            }
        }
    }
}
