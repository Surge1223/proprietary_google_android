package com.android.settings.sound;

import java.util.Collection;
import android.support.v7.preference.Preference;
import com.android.settingslib.bluetooth.HeadsetProfile;
import com.android.settingslib.bluetooth.HearingAidProfile;
import com.android.settingslib.Utils;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

public class HandsFreeProfileOutputPreferenceController extends AudioSwitchPreferenceController
{
    public HandsFreeProfileOutputPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public void setActiveBluetoothDevice(final BluetoothDevice bluetoothDevice) {
        if (!Utils.isAudioModeOngoingCall(this.mContext)) {
            return;
        }
        final HearingAidProfile hearingAidProfile = this.mProfileManager.getHearingAidProfile();
        final HeadsetProfile headsetProfile = this.mProfileManager.getHeadsetProfile();
        if (hearingAidProfile != null && headsetProfile != null && bluetoothDevice == null) {
            headsetProfile.setActiveDevice(null);
            hearingAidProfile.setActiveDevice(null);
            return;
        }
        if (hearingAidProfile != null && hearingAidProfile.getHiSyncId(bluetoothDevice) != 0L) {
            hearingAidProfile.setActiveDevice(bluetoothDevice);
        }
        if (headsetProfile != null) {
            headsetProfile.setActiveDevice(bluetoothDevice);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (preference == null) {
            return;
        }
        if (!Utils.isAudioModeOngoingCall(this.mContext)) {
            this.mPreference.setVisible(false);
            preference.setSummary(this.mContext.getText(2131888252));
            return;
        }
        this.mConnectedDevices.clear();
        this.mConnectedDevices.addAll(this.getConnectedHfpDevices());
        this.mConnectedDevices.addAll(this.getConnectedHearingAidDevices());
        final int size = this.mConnectedDevices.size();
        if (size == 0) {
            this.mPreference.setVisible(false);
            final CharSequence text = this.mContext.getText(2131888252);
            final CharSequence[] array = { text };
            this.mSelectedIndex = this.getDefaultDeviceIndex();
            preference.setSummary(text);
            this.setPreference(array, array, preference);
            return;
        }
        this.mPreference.setVisible(true);
        final CharSequence[] array2 = new CharSequence[size + 1];
        final CharSequence[] array3 = new CharSequence[size + 1];
        this.setupPreferenceEntries(array2, array3, this.findActiveDevice(0));
        if (this.isStreamFromOutputDevice(0, 67108864)) {
            this.mSelectedIndex = this.getDefaultDeviceIndex();
        }
        this.setPreference(array2, array3, preference);
    }
}
