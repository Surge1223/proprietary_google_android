package com.android.settings.sound;

import java.util.Collection;
import com.android.settingslib.Utils;
import android.support.v7.preference.Preference;
import com.android.settingslib.bluetooth.A2dpProfile;
import com.android.settingslib.bluetooth.HearingAidProfile;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

public class MediaOutputPreferenceController extends AudioSwitchPreferenceController
{
    public MediaOutputPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public void setActiveBluetoothDevice(final BluetoothDevice bluetoothDevice) {
        if (this.mAudioManager.getMode() != 0) {
            return;
        }
        final HearingAidProfile hearingAidProfile = this.mProfileManager.getHearingAidProfile();
        final A2dpProfile a2dpProfile = this.mProfileManager.getA2dpProfile();
        if (hearingAidProfile != null && a2dpProfile != null && bluetoothDevice == null) {
            hearingAidProfile.setActiveDevice(null);
            a2dpProfile.setActiveDevice(null);
            return;
        }
        if (hearingAidProfile != null && hearingAidProfile.getHiSyncId(bluetoothDevice) != 0L) {
            hearingAidProfile.setActiveDevice(bluetoothDevice);
        }
        if (a2dpProfile != null) {
            a2dpProfile.setActiveDevice(bluetoothDevice);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (preference == null) {
            return;
        }
        if (this.isStreamFromOutputDevice(3, 32768)) {
            this.mPreference.setVisible(false);
            preference.setSummary(this.mContext.getText(2131888254));
            return;
        }
        if (Utils.isAudioModeOngoingCall(this.mContext)) {
            this.mPreference.setVisible(false);
            preference.setSummary(this.mContext.getText(2131888251));
            return;
        }
        this.mConnectedDevices.clear();
        if (this.mAudioManager.getMode() == 0) {
            this.mConnectedDevices.addAll(this.getConnectedA2dpDevices());
            this.mConnectedDevices.addAll(this.getConnectedHearingAidDevices());
        }
        final int size = this.mConnectedDevices.size();
        if (size == 0) {
            this.mPreference.setVisible(false);
            final CharSequence text = this.mContext.getText(2131888252);
            final CharSequence[] array = { text };
            this.mSelectedIndex = this.getDefaultDeviceIndex();
            preference.setSummary(text);
            this.setPreference(array, array, preference);
            return;
        }
        this.mPreference.setVisible(true);
        final CharSequence[] array2 = new CharSequence[size + 1];
        final CharSequence[] array3 = new CharSequence[size + 1];
        this.setupPreferenceEntries(array2, array3, this.findActiveDevice(3));
        if (this.isStreamFromOutputDevice(3, 67108864)) {
            this.mSelectedIndex = this.getDefaultDeviceIndex();
        }
        this.setPreference(array2, array3, preference);
    }
}
