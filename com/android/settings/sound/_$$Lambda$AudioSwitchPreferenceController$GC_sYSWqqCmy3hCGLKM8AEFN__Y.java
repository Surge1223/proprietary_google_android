package com.android.settings.sound;

import android.content.Intent;
import android.media.MediaRouter$RouteGroup;
import android.media.MediaRouter$RouteInfo;
import android.media.AudioDeviceInfo;
import android.support.v7.preference.ListPreference;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.HeadsetProfile;
import com.android.settingslib.bluetooth.HearingAidProfile;
import com.android.settingslib.bluetooth.A2dpProfile;
import android.util.FeatureFlagUtils;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.media.MediaRouter$Callback;
import android.media.AudioDeviceCallback;
import com.android.settings.bluetooth.Utils;
import android.text.TextUtils;
import java.util.concurrent.ExecutionException;
import android.util.Log;
import java.util.concurrent.FutureTask;
import java.util.ArrayList;
import android.os.Looper;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import android.media.MediaRouter;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.os.Handler;
import android.bluetooth.BluetoothDevice;
import java.util.List;
import android.media.AudioManager;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.bluetooth.BluetoothCallback;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;
import java.util.concurrent.Callable;

public final class _$$Lambda$AudioSwitchPreferenceController$GC_sYSWqqCmy3hCGLKM8AEFN__Y implements Callable
{
    @Override
    public final Object call() {
        return AudioSwitchPreferenceController.lambda$new$0(this.f$0);
    }
}
