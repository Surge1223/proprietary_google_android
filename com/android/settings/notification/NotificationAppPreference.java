package com.android.settings.notification;

import com.android.settingslib.RestrictedLockUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Switch;
import com.android.settings.widget.MasterSwitchPreference;

public class NotificationAppPreference extends MasterSwitchPreference
{
    private boolean mChecked;
    private boolean mEnableSwitch;
    private Switch mSwitch;
    
    public NotificationAppPreference(final Context context) {
        super(context);
        this.mEnableSwitch = true;
    }
    
    public NotificationAppPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEnableSwitch = true;
    }
    
    public NotificationAppPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mEnableSwitch = true;
    }
    
    public NotificationAppPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mEnableSwitch = true;
    }
    
    @Override
    protected int getSecondTargetResId() {
        return 2131558683;
    }
    
    @Override
    public boolean isChecked() {
        return this.mSwitch != null && this.mChecked;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(16908312);
        if (viewById != null) {
            viewById.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    if (NotificationAppPreference.this.mSwitch != null && !NotificationAppPreference.this.mSwitch.isEnabled()) {
                        return;
                    }
                    NotificationAppPreference.this.setChecked(NotificationAppPreference.this.mChecked ^ true);
                    if (!NotificationAppPreference.this.callChangeListener(NotificationAppPreference.this.mChecked)) {
                        NotificationAppPreference.this.setChecked(NotificationAppPreference.this.mChecked ^ true);
                    }
                    else {
                        Preference.this.persistBoolean(NotificationAppPreference.this.mChecked);
                    }
                }
            });
        }
        this.mSwitch = (Switch)preferenceViewHolder.findViewById(R.id.switchWidget);
        if (this.mSwitch != null) {
            this.mSwitch.setContentDescription(this.getTitle());
            this.mSwitch.setChecked(this.mChecked);
            this.mSwitch.setEnabled(this.mEnableSwitch);
        }
    }
    
    @Override
    public void setChecked(final boolean b) {
        this.mChecked = b;
        if (this.mSwitch != null) {
            this.mSwitch.setChecked(b);
        }
    }
    
    @Override
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin enforcedAdmin) {
        this.setSwitchEnabled(enforcedAdmin == null);
    }
    
    @Override
    public void setSwitchEnabled(final boolean b) {
        this.mEnableSwitch = b;
        if (this.mSwitch != null) {
            this.mSwitch.setEnabled(b);
        }
    }
}
