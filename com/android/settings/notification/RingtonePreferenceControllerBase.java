package com.android.settings.notification;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class RingtonePreferenceControllerBase extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public RingtonePreferenceControllerBase(final Context context) {
        super(context);
    }
    
    public abstract int getRingtoneType();
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final String title = Ringtone.getTitle(this.mContext, RingtoneManager.getActualDefaultRingtoneUri(this.mContext, this.getRingtoneType()), false, true);
        if (title != null) {
            preference.setSummary(title);
        }
    }
}
