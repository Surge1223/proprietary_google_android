package com.android.settings.notification;

import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;

public class ZenModeCallsSettings extends ZenModeSettingsBase implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                return super.getNonIndexableKeys(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082879;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<ZenModeStarredContactsPreferenceController> list = (ArrayList<ZenModeStarredContactsPreferenceController>)new ArrayList<ZenModeBehaviorFooterPreferenceController>();
        list.add((ZenModeBehaviorFooterPreferenceController)new ZenModeCallsPreferenceController(context, lifecycle));
        list.add((ZenModeBehaviorFooterPreferenceController)new ZenModeStarredContactsPreferenceController(context, lifecycle, 8));
        list.add((ZenModeBehaviorFooterPreferenceController)new ZenModeRepeatCallersPreferenceController(context, lifecycle, context.getResources().getInteger(17694935)));
        list.add(new ZenModeBehaviorFooterPreferenceController(context, lifecycle, 2131890291));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    public int getMetricsCategory() {
        return 141;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082879;
    }
}
