package com.android.settings.notification;

import android.content.res.Resources;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class DockAudioMediaPreferenceController extends SettingPrefController
{
    public DockAudioMediaPreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(1, "dock_audio_media", "dock_audio_media_enabled", 0, new int[] { 0, 1 }) {
            @Override
            protected String getCaption(final Resources resources, final int n) {
                switch (n) {
                    default: {
                        throw new IllegalArgumentException();
                    }
                    case 1: {
                        return resources.getString(2131887469);
                    }
                    case 0: {
                        return resources.getString(2131887468);
                    }
                }
            }
            
            @Override
            public boolean isApplicable(final Context context) {
                return context.getResources().getBoolean(2131034184);
            }
        };
    }
}
