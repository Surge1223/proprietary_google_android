package com.android.settings.notification;

import android.util.Log;
import android.media.AudioManager;
import android.service.notification.ZenModeConfig;
import android.content.Intent;
import android.net.Uri;
import android.app.NotificationManager;
import android.service.notification.Condition;
import android.content.res.Resources;
import java.util.Locale;
import android.content.Context;
import android.text.format.DateFormat;
import android.os.UserHandle;
import com.android.settings.utils.VoiceSettingsActivity;

public class ZenModeVoiceActivity extends VoiceSettingsActivity
{
    private CharSequence getChangeSummary(int n, final int n2) {
        int n3 = -1;
        int n4 = -1;
        int n5 = -1;
        int n6 = -1;
        if (n != 0) {
            if (n == 3) {
                n3 = 2131890386;
                n4 = 2131755080;
                n5 = 2131755079;
                n6 = 2131890385;
            }
        }
        else {
            n3 = 2131890387;
        }
        if (n2 < 0 || n == 0) {
            return this.getString(n3);
        }
        final long currentTimeMillis = System.currentTimeMillis();
        final long n7 = 60000 * n2;
        String s;
        if (DateFormat.is24HourFormat((Context)this, UserHandle.myUserId())) {
            s = "Hm";
        }
        else {
            s = "hma";
        }
        final CharSequence format = DateFormat.format((CharSequence)DateFormat.getBestDateTimePattern(Locale.getDefault(), s), currentTimeMillis + n7);
        final Resources resources = this.getResources();
        if (n2 < 60) {
            return resources.getQuantityString(n4, n2, new Object[] { n2, format });
        }
        if (n2 % 60 != 0) {
            return resources.getString(n6, new Object[] { format });
        }
        n = n2 / 60;
        return resources.getQuantityString(n5, n, new Object[] { n, format });
    }
    
    private void setZenModeConfig(final int n, final Condition condition) {
        if (condition != null) {
            NotificationManager.from((Context)this).setZenMode(n, condition.id, "ZenModeVoiceActivity");
        }
        else {
            NotificationManager.from((Context)this).setZenMode(n, (Uri)null, "ZenModeVoiceActivity");
        }
    }
    
    @Override
    protected boolean onVoiceSettingInteraction(final Intent intent) {
        if (intent.hasExtra("android.settings.extra.do_not_disturb_mode_enabled")) {
            final int intExtra = intent.getIntExtra("android.settings.extra.do_not_disturb_mode_minutes", -1);
            Condition condition = null;
            final Condition condition2 = null;
            int n = 0;
            if (intent.getBooleanExtra("android.settings.extra.do_not_disturb_mode_enabled", false)) {
                Condition timeCondition = condition2;
                if (intExtra > 0) {
                    timeCondition = ZenModeConfig.toTimeCondition((Context)this, intExtra, UserHandle.myUserId());
                }
                n = 3;
                condition = timeCondition;
            }
            this.setZenModeConfig(n, condition);
            final AudioManager audioManager = (AudioManager)this.getSystemService("audio");
            if (audioManager != null) {
                audioManager.adjustStreamVolume(5, 0, 1);
            }
            this.notifySuccess(this.getChangeSummary(n, intExtra));
        }
        else {
            Log.v("ZenModeVoiceActivity", "Missing extra android.provider.Settings.EXTRA_DO_NOT_DISTURB_MODE_ENABLED");
            this.finish();
        }
        return false;
    }
}
