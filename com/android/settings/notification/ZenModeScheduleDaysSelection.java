package com.android.settings.notification;

import java.util.Arrays;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.view.LayoutInflater;
import java.util.Calendar;
import android.view.View;
import android.content.Context;
import android.widget.LinearLayout;
import android.util.SparseBooleanArray;
import java.text.SimpleDateFormat;
import android.widget.ScrollView;

public class ZenModeScheduleDaysSelection extends ScrollView
{
    private final SimpleDateFormat mDayFormat;
    private final SparseBooleanArray mDays;
    private final LinearLayout mLayout;
    
    public ZenModeScheduleDaysSelection(final Context context, final int[] array) {
        super(context);
        this.mDayFormat = new SimpleDateFormat("EEEE");
        this.mDays = new SparseBooleanArray();
        this.mLayout = new LinearLayout(this.mContext);
        final int dimensionPixelSize = context.getResources().getDimensionPixelSize(2131165711);
        this.mLayout.setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
        this.addView((View)this.mLayout);
        if (array != null) {
            for (int i = 0; i < array.length; ++i) {
                this.mDays.put(array[i], true);
            }
        }
        this.mLayout.setOrientation(1);
        final Calendar instance = Calendar.getInstance();
        final int[] daysOfWeekForLocale = getDaysOfWeekForLocale(instance);
        final LayoutInflater from = LayoutInflater.from(context);
        for (int j = 0; j < daysOfWeekForLocale.length; ++j) {
            final int n = daysOfWeekForLocale[j];
            final CheckBox checkBox = (CheckBox)from.inflate(2131558909, (ViewGroup)this, false);
            instance.set(7, n);
            checkBox.setText((CharSequence)this.mDayFormat.format(instance.getTime()));
            checkBox.setChecked(this.mDays.get(n));
            checkBox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
                public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                    ZenModeScheduleDaysSelection.this.mDays.put(n, b);
                    ZenModeScheduleDaysSelection.this.onChanged(ZenModeScheduleDaysSelection.this.getDays());
                }
            });
            this.mLayout.addView((View)checkBox);
        }
    }
    
    private int[] getDays() {
        final SparseBooleanArray sparseBooleanArray = new SparseBooleanArray(this.mDays.size());
        final int n = 0;
        for (int i = 0; i < this.mDays.size(); ++i) {
            final int key = this.mDays.keyAt(i);
            if (this.mDays.valueAt(i)) {
                sparseBooleanArray.put(key, true);
            }
        }
        final int[] array = new int[sparseBooleanArray.size()];
        for (int j = n; j < array.length; ++j) {
            array[j] = sparseBooleanArray.keyAt(j);
        }
        Arrays.sort(array);
        return array;
    }
    
    protected static int[] getDaysOfWeekForLocale(final Calendar calendar) {
        final int[] array = new int[7];
        int firstDayOfWeek = calendar.getFirstDayOfWeek();
        for (int i = 0; i < array.length; ++i) {
            int n;
            if ((n = firstDayOfWeek) > 7) {
                n = 1;
            }
            array[i] = n;
            firstDayOfWeek = n + 1;
        }
        return array;
    }
    
    protected void onChanged(final int[] array) {
    }
}
