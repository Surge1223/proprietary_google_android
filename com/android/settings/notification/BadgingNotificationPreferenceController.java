package com.android.settings.notification;

import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.text.TextUtils;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.search.InlineSwitchPayload;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.search.ResultPayload;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settings.core.TogglePreferenceController;

public class BadgingNotificationPreferenceController extends TogglePreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    static final int OFF = 0;
    static final int ON = 1;
    private static final String TAG = "BadgeNotifPrefContr";
    private SettingObserver mSettingObserver;
    
    public BadgingNotificationPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("notification_badging");
        if (preference != null) {
            this.mSettingObserver = new SettingObserver(preference);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(17957000)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public ResultPayload getResultPayload() {
        return new InlineSwitchPayload("notification_badging", 2, 1, DatabaseIndexingUtils.buildSearchResultPageIntent(this.mContext, ConfigureNotificationSettings.class.getName(), this.getPreferenceKey(), this.mContext.getString(2131887104)), this.isAvailable(), 1);
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Secure.getInt(contentResolver, "notification_badging", 1) != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"notification_badging");
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), false);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), true);
        }
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.Secure.putInt(this.mContext.getContentResolver(), "notification_badging", (int)(b ? 1 : 0));
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri NOTIFICATION_BADGING_URI;
        private final Preference mPreference;
        
        public SettingObserver(final Preference mPreference) {
            super(new Handler());
            this.NOTIFICATION_BADGING_URI = Settings.Secure.getUriFor("notification_badging");
            this.mPreference = mPreference;
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.NOTIFICATION_BADGING_URI.equals((Object)uri)) {
                BadgingNotificationPreferenceController.this.updateState(this.mPreference);
            }
        }
        
        public void register(final ContentResolver contentResolver, final boolean b) {
            if (b) {
                contentResolver.registerContentObserver(this.NOTIFICATION_BADGING_URI, false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
