package com.android.settings.notification;

import com.android.settingslib.RestrictedSwitchPreference;
import android.content.Context;
import android.os.Vibrator;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class VibrationPreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final Vibrator mVibrator;
    
    public VibrationPreferenceController(final Context context, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
        this.mVibrator = (Vibrator)context.getSystemService("vibrator");
    }
    
    @Override
    public String getPreferenceKey() {
        return "vibrate";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        final boolean b = false;
        if (available && this.mChannel != null) {
            boolean b2 = b;
            if (this.checkCanBeVisible(3)) {
                b2 = b;
                if (!this.isDefaultChannel()) {
                    b2 = b;
                    if (this.mVibrator != null) {
                        b2 = b;
                        if (this.mVibrator.hasVibrator()) {
                            b2 = true;
                        }
                    }
                }
            }
            return b2;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            this.mChannel.enableVibration((boolean)o);
            this.saveChannel();
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mChannel != null) {
            final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
            restrictedSwitchPreference.setDisabledByAdmin(this.mAdmin);
            restrictedSwitchPreference.setEnabled(!restrictedSwitchPreference.isDisabledByAdmin() && this.isChannelConfigurable());
            restrictedSwitchPreference.setChecked(this.mChannel.shouldVibrate());
        }
    }
}
