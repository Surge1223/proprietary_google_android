package com.android.settings.notification;

import com.android.settingslib.notification.EnableZenModeDialog;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class SettingsEnableZenModeDialog extends InstrumentedDialogFragment
{
    @Override
    public int getMetricsCategory() {
        return 1286;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return new EnableZenModeDialog(this.getContext()).createDialog();
    }
}
