package com.android.settings.notification;

import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.widget.Button;
import android.app.FragmentManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$ZenModeButtonPreferenceController$KAk_Mj51Obvq4mW4RobrcR4_CRM implements View.OnClickListener
{
    public final void onClick(final View view) {
        ZenModeButtonPreferenceController.lambda$updateZenButtonOnClickListener$1(this.f$0, view);
    }
}
