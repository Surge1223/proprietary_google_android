package com.android.settings.notification;

import com.android.settingslib.RestrictedPreference;
import android.os.UserHandle;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.os.UserManager;
import android.content.pm.PackageManager;
import com.android.settings.accounts.AccountRestrictionHelper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class EmergencyBroadcastPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private AccountRestrictionHelper mHelper;
    private PackageManager mPm;
    private final String mPrefKey;
    private UserManager mUserManager;
    
    EmergencyBroadcastPreferenceController(final Context context, final AccountRestrictionHelper mHelper, final String mPrefKey) {
        super(context);
        this.mPrefKey = mPrefKey;
        this.mHelper = mHelper;
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mPm = this.mContext.getPackageManager();
    }
    
    public EmergencyBroadcastPreferenceController(final Context context, final String s) {
        this(context, new AccountRestrictionHelper(context), s);
    }
    
    private boolean isCellBroadcastAppLinkEnabled() {
        boolean boolean1;
        final boolean b = boolean1 = this.mContext.getResources().getBoolean(17956916);
        if (b) {
            try {
                final int applicationEnabledSetting = this.mPm.getApplicationEnabledSetting("com.android.cellbroadcastreceiver");
                boolean1 = b;
                if (applicationEnabledSetting == 2) {
                    boolean1 = false;
                }
            }
            catch (IllegalArgumentException ex) {
                boolean1 = false;
            }
        }
        return boolean1;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPrefKey;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUserManager.isAdminUser() && this.isCellBroadcastAppLinkEnabled() && !this.mHelper.hasBaseUserRestriction("no_config_cell_broadcasts", UserHandle.myUserId());
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof RestrictedPreference)) {
            return;
        }
        ((RestrictedPreference)preference).checkRestrictionAndSetDisabled("no_config_cell_broadcasts");
    }
}
