package com.android.settings.notification;

import android.service.notification.ScheduleCalendar;
import android.content.ContentResolver;
import android.os.Handler;
import android.database.ContentObserver;
import android.net.Uri;
import android.service.notification.ZenModeConfig;
import android.provider.Settings;
import android.app.NotificationManager$Policy;
import android.support.v7.preference.Preference;
import android.app.AlarmManager$AlarmClockInfo;
import android.app.ActivityManager;
import android.app.AlarmManager;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import android.support.v7.preference.PreferenceScreen;
import android.app.NotificationManager;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class AbstractZenModePreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    protected static ZenModeConfigWrapper mZenModeConfigWrapper;
    private final String KEY;
    protected final ZenModeBackend mBackend;
    protected MetricsFeatureProvider mMetricsFeatureProvider;
    private final NotificationManager mNotificationManager;
    protected PreferenceScreen mScreen;
    @VisibleForTesting
    protected SettingObserver mSettingObserver;
    
    public AbstractZenModePreferenceController(final Context context, final String key, final Lifecycle lifecycle) {
        super(context);
        AbstractZenModePreferenceController.mZenModeConfigWrapper = new ZenModeConfigWrapper(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
        this.KEY = key;
        this.mNotificationManager = (NotificationManager)context.getSystemService("notification");
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(this.mContext).getMetricsFeatureProvider();
        this.mBackend = ZenModeBackend.getInstance(context);
    }
    
    private static long getNextAlarm(final Context context) {
        final AlarmManager$AlarmClockInfo nextAlarmClock = ((AlarmManager)context.getSystemService("alarm")).getNextAlarmClock(ActivityManager.getCurrentUser());
        long triggerTime;
        if (nextAlarmClock != null) {
            triggerTime = nextAlarmClock.getTriggerTime();
        }
        else {
            triggerTime = 0L;
        }
        return triggerTime;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        super.displayPreference(mScreen);
        this.mScreen = mScreen;
        final Preference preference = mScreen.findPreference(this.KEY);
        if (preference != null) {
            this.mSettingObserver = new SettingObserver(preference);
        }
    }
    
    protected NotificationManager$Policy getPolicy() {
        return this.mNotificationManager.getNotificationPolicy();
    }
    
    @Override
    public String getPreferenceKey() {
        return this.KEY;
    }
    
    protected int getZenDuration() {
        return Settings.Global.getInt(this.mContext.getContentResolver(), "zen_duration", 0);
    }
    
    protected int getZenMode() {
        return Settings.Global.getInt(this.mContext.getContentResolver(), "zen_mode", this.mBackend.mZenMode);
    }
    
    protected ZenModeConfig getZenModeConfig() {
        return this.mNotificationManager.getZenModeConfig();
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.unregister(this.mContext.getContentResolver());
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver());
            this.mSettingObserver.onChange(false, null);
        }
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri ZEN_MODE_CONFIG_ETAG_URI;
        private final Uri ZEN_MODE_DURATION_URI;
        private final Uri ZEN_MODE_URI;
        private final Preference mPreference;
        
        public SettingObserver(final Preference mPreference) {
            super(new Handler());
            this.ZEN_MODE_URI = Settings.Global.getUriFor("zen_mode");
            this.ZEN_MODE_CONFIG_ETAG_URI = Settings.Global.getUriFor("zen_mode_config_etag");
            this.ZEN_MODE_DURATION_URI = Settings.Global.getUriFor("zen_duration");
            this.mPreference = mPreference;
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (uri == null || this.ZEN_MODE_URI.equals((Object)uri) || this.ZEN_MODE_CONFIG_ETAG_URI.equals((Object)uri) || this.ZEN_MODE_DURATION_URI.equals((Object)uri)) {
                AbstractZenModePreferenceController.this.mBackend.updatePolicy();
                AbstractZenModePreferenceController.this.mBackend.updateZenMode();
                if (AbstractZenModePreferenceController.this.mScreen != null) {
                    AbstractZenModePreferenceController.this.displayPreference(AbstractZenModePreferenceController.this.mScreen);
                }
                AbstractZenModePreferenceController.this.updateState(this.mPreference);
            }
        }
        
        public void register(final ContentResolver contentResolver) {
            contentResolver.registerContentObserver(this.ZEN_MODE_URI, false, (ContentObserver)this, -1);
            contentResolver.registerContentObserver(this.ZEN_MODE_CONFIG_ETAG_URI, false, (ContentObserver)this, -1);
            contentResolver.registerContentObserver(this.ZEN_MODE_DURATION_URI, false, (ContentObserver)this, -1);
        }
        
        public void unregister(final ContentResolver contentResolver) {
            contentResolver.unregisterContentObserver((ContentObserver)this);
        }
    }
    
    @VisibleForTesting
    static class ZenModeConfigWrapper
    {
        private final Context mContext;
        
        public ZenModeConfigWrapper(final Context mContext) {
            this.mContext = mContext;
        }
        
        private boolean isToday(final long n) {
            return ZenModeConfig.isToday(n);
        }
        
        protected CharSequence getFormattedTime(final long n, final int n2) {
            return ZenModeConfig.getFormattedTime(this.mContext, n, this.isToday(n), n2);
        }
        
        protected String getOwnerCaption(final String s) {
            return ZenModeConfig.getOwnerCaption(this.mContext, s);
        }
        
        protected boolean isTimeRule(final Uri uri) {
            return ZenModeConfig.isValidEventConditionId(uri) || ZenModeConfig.isValidScheduleConditionId(uri);
        }
        
        protected long parseAutomaticRuleEndTime(final Uri uri) {
            if (ZenModeConfig.isValidEventConditionId(uri)) {
                return Long.MAX_VALUE;
            }
            if (ZenModeConfig.isValidScheduleConditionId(uri)) {
                final ScheduleCalendar scheduleCalendar = ZenModeConfig.toScheduleCalendar(uri);
                final long nextChangeTime = scheduleCalendar.getNextChangeTime(System.currentTimeMillis());
                if (scheduleCalendar.exitAtAlarm()) {
                    final long access$000 = getNextAlarm(this.mContext);
                    scheduleCalendar.maybeSetNextAlarm(System.currentTimeMillis(), access$000);
                    if (scheduleCalendar.shouldExitForAlarm(nextChangeTime)) {
                        return access$000;
                    }
                }
                return nextChangeTime;
            }
            return -1L;
        }
        
        protected long parseManualRuleTime(final Uri uri) {
            return ZenModeConfig.tryParseCountdownConditionId(uri);
        }
    }
}
