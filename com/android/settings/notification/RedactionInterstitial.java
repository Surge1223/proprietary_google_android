package com.android.settings.notification;

import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import com.android.settings.Utils;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.SetupRedactionInterstitial;
import android.view.View;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.RestrictedRadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup$OnCheckedChangeListener;
import android.view.View.OnClickListener;
import com.android.settings.SettingsPreferenceFragment;
import android.widget.LinearLayout;
import android.os.Bundle;
import com.android.settings.SetupWizardUtils;
import android.content.res.Resources.Theme;
import android.os.UserManager;
import android.content.Intent;
import android.content.Context;
import com.android.settings.SettingsActivity;

public class RedactionInterstitial extends SettingsActivity
{
    public static Intent createStartIntent(final Context context, final int n) {
        final Intent intent = new Intent(context, (Class)RedactionInterstitial.class);
        int n2;
        if (UserManager.get(context).isManagedProfile(n)) {
            n2 = 2131888088;
        }
        else {
            n2 = 2131888087;
        }
        return intent.putExtra(":settings:show_fragment_title_resid", n2).putExtra("android.intent.extra.USER_ID", n);
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", RedactionInterstitialFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return RedactionInterstitialFragment.class.getName().equals(s);
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        super.onApplyThemeResource(resources$Theme, SetupWizardUtils.getTheme(this.getIntent()), b);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public static class RedactionInterstitialFragment extends SettingsPreferenceFragment implements View.OnClickListener, RadioGroup$OnCheckedChangeListener
    {
        private RadioGroup mRadioGroup;
        private RestrictedRadioButton mRedactSensitiveButton;
        private RestrictedRadioButton mShowAllButton;
        private int mUserId;
        
        private void checkNotificationFeaturesAndSetDisabled(final RestrictedRadioButton restrictedRadioButton, final int n) {
            restrictedRadioButton.setDisabledByAdmin(RestrictedLockUtils.checkIfKeyguardFeaturesDisabled((Context)this.getActivity(), n, this.mUserId));
        }
        
        private void loadFromSettings() {
            final boolean managedProfile = UserManager.get(this.getContext()).isManagedProfile(this.mUserId);
            boolean b = true;
            final boolean b2 = managedProfile || Settings.Secure.getIntForUser(this.getContentResolver(), "lock_screen_show_notifications", 0, this.mUserId) != 0;
            if (Settings.Secure.getIntForUser(this.getContentResolver(), "lock_screen_allow_private_notifications", 1, this.mUserId) == 0) {
                b = false;
            }
            int n = 2131362225;
            if (b2) {
                if (b && !this.mShowAllButton.isDisabledByAdmin()) {
                    n = 2131362604;
                }
                else {
                    n = n;
                    if (!this.mRedactSensitiveButton.isDisabledByAdmin()) {
                        n = 2131362511;
                    }
                }
            }
            this.mRadioGroup.check(n);
        }
        
        public int getMetricsCategory() {
            return 74;
        }
        
        public void onCheckedChanged(final RadioGroup radioGroup, int n) {
            final boolean b = false;
            final boolean b2 = n == 2131362604;
            if (n != 2131362225) {
                n = 1;
            }
            else {
                n = 0;
            }
            final ContentResolver contentResolver = this.getContentResolver();
            int n2;
            if (b2) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            Settings.Secure.putIntForUser(contentResolver, "lock_screen_allow_private_notifications", n2, this.mUserId);
            final ContentResolver contentResolver2 = this.getContentResolver();
            int n3 = b ? 1 : 0;
            if (n != 0) {
                n3 = 1;
            }
            Settings.Secure.putIntForUser(contentResolver2, "lock_screen_show_notifications", n3, this.mUserId);
        }
        
        public void onClick(final View view) {
            if (view.getId() == 2131362512) {
                SetupRedactionInterstitial.setEnabled(this.getContext(), false);
                final RedactionInterstitial redactionInterstitial = (RedactionInterstitial)this.getActivity();
                if (redactionInterstitial != null) {
                    redactionInterstitial.setResult(-1, (Intent)null);
                    this.finish();
                }
            }
        }
        
        @Override
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            return layoutInflater.inflate(2131558704, viewGroup, false);
        }
        
        @Override
        public void onResume() {
            super.onResume();
            this.checkNotificationFeaturesAndSetDisabled(this.mShowAllButton, 12);
            this.checkNotificationFeaturesAndSetDisabled(this.mRedactSensitiveButton, 4);
            this.loadFromSettings();
        }
        
        public void onViewCreated(final View view, final Bundle bundle) {
            super.onViewCreated(view, bundle);
            this.mRadioGroup = (RadioGroup)view.findViewById(2131362507);
            this.mShowAllButton = (RestrictedRadioButton)view.findViewById(2131362604);
            this.mRedactSensitiveButton = (RestrictedRadioButton)view.findViewById(2131362511);
            this.mRadioGroup.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)this);
            this.mUserId = Utils.getUserIdFromBundle(this.getContext(), this.getActivity().getIntent().getExtras());
            if (UserManager.get(this.getContext()).isManagedProfile(this.mUserId)) {
                ((TextView)view.findViewById(R.id.message)).setText(2131888086);
                this.mShowAllButton.setText(2131888093);
                this.mRedactSensitiveButton.setText(2131888091);
                ((RadioButton)view.findViewById(2131362225)).setVisibility(8);
            }
            ((Button)view.findViewById(2131362512)).setOnClickListener((View.OnClickListener)this);
        }
    }
}
