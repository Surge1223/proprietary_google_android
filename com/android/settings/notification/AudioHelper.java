package com.android.settings.notification;

import android.media.AudioSystem;
import android.os.UserHandle;
import android.os.UserManager;
import com.android.settings.Utils;
import android.content.Context;
import android.media.AudioManager;

public class AudioHelper
{
    private AudioManager mAudioManager;
    private Context mContext;
    
    public AudioHelper(final Context mContext) {
        this.mContext = mContext;
        this.mAudioManager = (AudioManager)this.mContext.getSystemService("audio");
    }
    
    public Context createPackageContextAsUser(final int n) {
        return Utils.createPackageContextAsUser(this.mContext, n);
    }
    
    public int getManagedProfileId(final UserManager userManager) {
        return Utils.getManagedProfileId(userManager, UserHandle.myUserId());
    }
    
    public int getMaxVolume(final int n) {
        return this.mAudioManager.getStreamMaxVolume(n);
    }
    
    public int getRingerModeInternal() {
        return this.mAudioManager.getRingerModeInternal();
    }
    
    public int getStreamVolume(final int n) {
        return this.mAudioManager.getStreamVolume(n);
    }
    
    public boolean isSingleVolume() {
        return AudioSystem.isSingleVolume(this.mContext);
    }
    
    public boolean isUserUnlocked(final UserManager userManager, final int n) {
        return userManager.isUserUnlocked(n);
    }
    
    public boolean setStreamVolume(final int n, final int n2) {
        this.mAudioManager.setStreamVolume(n, n2, 0);
        return true;
    }
}
