package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;

public class NotificationsOffPreferenceController extends NotificationPreferenceController implements PreferenceControllerMixin
{
    public NotificationsOffPreferenceController(final Context context) {
        super(context, null);
    }
    
    @Override
    public String getPreferenceKey() {
        return "block_desc";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mAppRow != null && (super.isAvailable() ^ true);
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null) {
            if (this.mChannel != null) {
                preference.setTitle(2131887004);
            }
            else if (this.mChannelGroup != null) {
                preference.setTitle(2131887003);
            }
            else {
                preference.setTitle(2131886387);
            }
        }
        preference.setSelectable(false);
    }
}
