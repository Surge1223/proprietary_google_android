package com.android.settings.notification;

import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.ComponentName;
import android.content.Context;

public class SuppressorHelper
{
    public static String getSuppressionText(final Context context, final ComponentName componentName) {
        String string;
        if (componentName != null) {
            string = context.getString(17040331, new Object[] { getSuppressorCaption(context, componentName) });
        }
        else {
            string = null;
        }
        return string;
    }
    
    static String getSuppressorCaption(final Context context, final ComponentName componentName) {
        final PackageManager packageManager = context.getPackageManager();
        try {
            final ServiceInfo serviceInfo = packageManager.getServiceInfo(componentName, 0);
            if (serviceInfo != null) {
                final CharSequence loadLabel = serviceInfo.loadLabel(packageManager);
                if (loadLabel != null) {
                    final String trim = loadLabel.toString().trim();
                    if (trim.length() > 0) {
                        return trim;
                    }
                }
            }
        }
        catch (Throwable t) {
            Log.w("SuppressorHelper", "Error loading suppressor caption", t);
        }
        return componentName.getPackageName();
    }
}
