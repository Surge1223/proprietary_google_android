package com.android.settings.notification;

import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ZenModeMediaPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceChangeListener
{
    private final ZenModeBackend mBackend;
    
    public ZenModeMediaPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "zen_mode_media", lifecycle);
        this.mBackend = ZenModeBackend.getInstance(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_media";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        if (ZenModeSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onPrefChange allowMedia=");
            sb.append(booleanValue);
            Log.d("PrefControllerMixin", sb.toString());
        }
        this.mBackend.saveSoundPolicy(64, booleanValue);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        switch (this.getZenMode()) {
            default: {
                switchPreference.setEnabled(true);
                switchPreference.setChecked(this.mBackend.isPriorityCategoryEnabled(64));
                break;
            }
            case 3: {
                switchPreference.setEnabled(false);
                switchPreference.setChecked(true);
                break;
            }
            case 2: {
                switchPreference.setEnabled(false);
                switchPreference.setChecked(false);
                break;
            }
        }
    }
}
