package com.android.settings.notification;

import android.app.NotificationManager;
import android.support.v4.graphics.drawable.IconCompat;
import com.android.settingslib.Utils;
import androidx.slice.Slice;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;
import android.content.IntentFilter;
import androidx.slice.builders.ListBuilder;
import androidx.slice.builders.SliceAction;
import android.support.v4.util.Consumer;

public final class _$$Lambda$ZenModeSliceBuilder$sz_ZmwW0wKJApaEBVBuTr2mkXrg implements Consumer
{
    @Override
    public final void accept(final Object o) {
        ZenModeSliceBuilder.lambda$getSlice$0(this.f$0, this.f$1, this.f$2, (ListBuilder.RowBuilder)o);
    }
}
