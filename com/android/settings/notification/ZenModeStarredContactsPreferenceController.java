package com.android.settings.notification;

import java.util.Collection;
import android.icu.text.ListFormatter;
import android.support.v7.preference.PreferenceScreen;
import android.database.Cursor;
import android.provider.ContactsContract$Contacts;
import java.util.ArrayList;
import java.util.List;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.internal.annotations.VisibleForTesting;
import android.content.Intent;
import android.support.v7.preference.Preference;

public class ZenModeStarredContactsPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceClickListener
{
    @VisibleForTesting
    Intent mFallbackIntent;
    private final PackageManager mPackageManager;
    private Preference mPreference;
    private final int mPriorityCategory;
    @VisibleForTesting
    Intent mStarredContactsIntent;
    
    public ZenModeStarredContactsPreferenceController(final Context context, final Lifecycle lifecycle, final int mPriorityCategory) {
        super(context, "zen_mode_starred_contacts", lifecycle);
        this.mPriorityCategory = mPriorityCategory;
        this.mPackageManager = this.mContext.getPackageManager();
        this.mStarredContactsIntent = new Intent("com.android.contacts.action.LIST_STARRED");
        (this.mFallbackIntent = new Intent("android.intent.action.MAIN")).addCategory("android.intent.category.APP_CONTACTS");
    }
    
    private List<String> getStarredContacts() {
        final ArrayList<String> list = new ArrayList<String>();
        final Cursor query = this.mContext.getContentResolver().query(ContactsContract$Contacts.CONTENT_URI, new String[] { "display_name" }, "starred=1", (String[])null, "times_contacted");
        if (query.moveToFirst()) {
            do {
                list.add(query.getString(0));
            } while (query.moveToNext());
        }
        return list;
    }
    
    private boolean isIntentValid() {
        return this.mStarredContactsIntent.resolveActivity(this.mPackageManager) != null || this.mFallbackIntent.resolveActivity(this.mPackageManager) != null;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = preferenceScreen.findPreference("zen_mode_starred_contacts")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_starred_contacts";
    }
    
    @Override
    public boolean isAvailable() {
        final int mPriorityCategory = this.mPriorityCategory;
        final boolean b = true;
        boolean b2 = true;
        if (mPriorityCategory == 8) {
            if (!this.mBackend.isPriorityCategoryEnabled(8) || this.mBackend.getPriorityCallSenders() != 2 || !this.isIntentValid()) {
                b2 = false;
            }
            return b2;
        }
        return this.mPriorityCategory == 4 && this.mBackend.isPriorityCategoryEnabled(4) && this.mBackend.getPriorityMessageSenders() == 2 && this.isIntentValid() && b;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (this.mStarredContactsIntent.resolveActivity(this.mPackageManager) != null) {
            this.mContext.startActivity(this.mStarredContactsIntent);
        }
        else {
            this.mContext.startActivity(this.mFallbackIntent);
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final List<String> starredContacts = this.getStarredContacts();
        final int size = starredContacts.size();
        final ArrayList<String> list = new ArrayList<String>();
        if (size == 0) {
            list.add(this.mContext.getString(2131890321));
        }
        else {
            for (int n = 0; n < 2 && n < size; ++n) {
                list.add(starredContacts.get(n));
            }
            if (size == 3) {
                list.add(starredContacts.get(2));
            }
            else if (size > 2) {
                list.add(this.mContext.getResources().getQuantityString(2131755078, size - 2, new Object[] { size - 2 }));
            }
        }
        this.mPreference.setSummary(ListFormatter.getInstance().format((Collection)list));
    }
}
