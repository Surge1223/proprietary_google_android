package com.android.settings.notification;

import android.text.TextUtils;
import android.content.Context;

public class AlarmVolumePreferenceController extends VolumeSeekBarPreferenceController
{
    private static final String KEY_ALARM_VOLUME = "alarm_volume";
    
    public AlarmVolumePreferenceController(final Context context) {
        super(context, "alarm_volume");
    }
    
    public int getAudioStream() {
        return 4;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034132) && !this.mHelper.isSingleVolume()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    public int getMuteIcon() {
        return 17302265;
    }
    
    @Override
    public String getPreferenceKey() {
        return "alarm_volume";
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"alarm_volume");
    }
}
