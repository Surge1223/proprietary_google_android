package com.android.settings.notification;

import android.os.Bundle;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;

public class ZenModeRestrictNotificationsSettings extends ZenModeSettingsBase implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                return super.getNonIndexableKeys(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082882;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<ZenModeVisEffectsAllPreferenceController> list = (ArrayList<ZenModeVisEffectsAllPreferenceController>)new ArrayList<ZenFooterPreferenceController>();
        list.add((ZenFooterPreferenceController)new ZenModeVisEffectsNonePreferenceController(context, lifecycle, "zen_mute_notifications"));
        list.add((ZenFooterPreferenceController)new ZenModeVisEffectsAllPreferenceController(context, lifecycle, "zen_hide_notifications"));
        list.add((ZenFooterPreferenceController)new ZenModeVisEffectsCustomPreferenceController(context, lifecycle, "zen_custom"));
        list.add(new ZenFooterPreferenceController(context, lifecycle, "footer_preference"));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887777;
    }
    
    @Override
    public int getMetricsCategory() {
        return 1400;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082882;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
}
