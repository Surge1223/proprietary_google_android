package com.android.settings.notification;

import android.text.method.MovementMethod;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class NotificationFooterPreference extends Preference
{
    public NotificationFooterPreference(final Context context) {
        this(context, null);
    }
    
    public NotificationFooterPreference(final Context context, final AttributeSet set) {
        super(context, set, TypedArrayUtils.getAttr(context, R.attr.footerPreferenceStyle, 16842894));
        this.init();
    }
    
    private void init() {
        this.setIcon(R.drawable.ic_info_outline_24dp);
        this.setSelectable(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.itemView.findViewById(16908310);
        textView.setMovementMethod((MovementMethod)new LinkMovementMethod());
        textView.setClickable(false);
        textView.setLongClickable(false);
    }
}
