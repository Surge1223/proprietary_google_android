package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.app.NotificationManager$Policy;
import com.android.settings.core.SubSettingLauncher;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public final class _$$Lambda$ZenModeVisEffectsCustomPreferenceController$hYHNs4_TKsGpjPSCluD3oYAyplI implements OnGearClickListener
{
    @Override
    public final void onGearClick(final ZenCustomRadioButtonPreference zenCustomRadioButtonPreference) {
        ZenModeVisEffectsCustomPreferenceController.lambda$displayPreference$0(this.f$0, zenCustomRadioButtonPreference);
    }
}
