package com.android.settings.notification;

import android.util.Log;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import java.util.Iterator;
import android.preference.PreferenceManager$OnActivityResultListener;
import android.content.Intent;
import java.util.Collection;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.SettingsPreferenceFragment;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;

public class ChannelNotificationSettings extends NotificationSettingsBase
{
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        (this.mControllers = new ArrayList<NotificationPreferenceController>()).add(new HeaderPreferenceController(context, this));
        this.mControllers.add(new BlockPreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new ImportancePreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new AllowSoundPreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new SoundPreferenceController(context, this, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new VibrationPreferenceController(context, this.mBackend));
        this.mControllers.add(new AppLinkPreferenceController(context));
        this.mControllers.add(new DescriptionPreferenceController(context));
        this.mControllers.add(new VisibilityPreferenceController(context, new LockPatternUtils(context), this.mBackend));
        this.mControllers.add(new LightsPreferenceController(context, this.mBackend));
        this.mControllers.add(new BadgePreferenceController(context, this.mBackend));
        this.mControllers.add(new DndPreferenceController(context, this.mBackend));
        this.mControllers.add(new NotificationsOffPreferenceController(context));
        return new ArrayList<AbstractPreferenceController>(this.mControllers);
    }
    
    @Override
    protected String getLogTag() {
        return "ChannelSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 265;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082732;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        for (final NotificationPreferenceController notificationPreferenceController : this.mControllers) {
            if (notificationPreferenceController instanceof PreferenceManager$OnActivityResultListener) {
                ((PreferenceManager$OnActivityResultListener)notificationPreferenceController).onActivityResult(n, n2, intent);
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        final Bundle arguments = this.getArguments();
        if (preferenceScreen != null && arguments != null && !arguments.getBoolean("fromSettings", false)) {
            preferenceScreen.setInitialExpandedChildrenCount(Integer.MAX_VALUE);
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mUid >= 0 && !TextUtils.isEmpty((CharSequence)this.mPkg) && this.mPkgInfo != null && this.mChannel != null) {
            for (final NotificationPreferenceController notificationPreferenceController : this.mControllers) {
                notificationPreferenceController.onResume(this.mAppRow, this.mChannel, this.mChannelGroup, this.mSuspendedAppsAdmin);
                notificationPreferenceController.displayPreference(this.getPreferenceScreen());
            }
            this.updatePreferenceStates();
            return;
        }
        Log.w("ChannelSettings", "Missing package or uid or packageinfo or channel");
        this.finish();
    }
}
