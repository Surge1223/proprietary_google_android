package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.content.ComponentName;
import java.util.Iterator;
import android.net.Uri;
import android.service.notification.ZenModeConfig;
import android.service.notification.ZenModeConfig$ZenRule;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class ZenModeBehaviorFooterPreferenceController extends AbstractZenModePreferenceController
{
    private final int mTitleRes;
    
    public ZenModeBehaviorFooterPreferenceController(final Context context, final Lifecycle lifecycle, final int mTitleRes) {
        super(context, "footer_preference", lifecycle);
        this.mTitleRes = mTitleRes;
    }
    
    private boolean isDeprecatedZenMode(final int n) {
        switch (n) {
            default: {
                return false;
            }
            case 2:
            case 3: {
                return true;
            }
        }
    }
    
    protected String getFooterText() {
        if (this.isDeprecatedZenMode(this.getZenMode())) {
            final ZenModeConfig zenModeConfig = this.getZenModeConfig();
            if (zenModeConfig.manualRule != null && this.isDeprecatedZenMode(zenModeConfig.manualRule.zenMode)) {
                final Uri conditionId = zenModeConfig.manualRule.conditionId;
                if (zenModeConfig.manualRule.enabler == null) {
                    return this.mContext.getString(2131890331);
                }
                final String ownerCaption = ZenModeBehaviorFooterPreferenceController.mZenModeConfigWrapper.getOwnerCaption(zenModeConfig.manualRule.enabler);
                if (!ownerCaption.isEmpty()) {
                    return this.mContext.getString(2131890258, new Object[] { ownerCaption });
                }
            }
            for (final ZenModeConfig$ZenRule zenModeConfig$ZenRule : zenModeConfig.automaticRules.values()) {
                if (zenModeConfig$ZenRule.isAutomaticActive() && this.isDeprecatedZenMode(zenModeConfig$ZenRule.zenMode)) {
                    final ComponentName component = zenModeConfig$ZenRule.component;
                    if (component != null) {
                        return this.mContext.getString(2131890258, new Object[] { component.getPackageName() });
                    }
                    continue;
                }
            }
            return this.mContext.getString(2131890390);
        }
        return this.mContext.getString(this.mTitleRes);
    }
    
    @Override
    public String getPreferenceKey() {
        return "footer_preference";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        preference.setTitle(this.getFooterText());
    }
}
