package com.android.settings.notification;

import android.text.TextUtils;
import com.android.settings.Utils;
import android.content.Context;

public class NotificationVolumePreferenceController extends RingVolumePreferenceController
{
    private static final String KEY_NOTIFICATION_VOLUME = "notification_volume";
    
    public NotificationVolumePreferenceController(final Context context) {
        super(context, "notification_volume");
    }
    
    @Override
    public int getAudioStream() {
        return 5;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034154) && !Utils.isVoiceCapable(this.mContext) && !this.mHelper.isSingleVolume()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public int getMuteIcon() {
        return 2131231079;
    }
    
    @Override
    public String getPreferenceKey() {
        return "notification_volume";
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"notification_volume");
    }
}
