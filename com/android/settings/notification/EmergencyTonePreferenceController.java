package com.android.settings.notification;

import android.telephony.TelephonyManager;
import android.content.res.Resources;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class EmergencyTonePreferenceController extends SettingPrefController
{
    public EmergencyTonePreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(1, "emergency_tone", "emergency_tone", 0, new int[] { 1, 2, 0 }) {
            @Override
            protected String getCaption(final Resources resources, final int n) {
                switch (n) {
                    default: {
                        throw new IllegalArgumentException();
                    }
                    case 2: {
                        return resources.getString(2131887517);
                    }
                    case 1: {
                        return resources.getString(2131887513);
                    }
                    case 0: {
                        return resources.getString(2131887514);
                    }
                }
            }
            
            @Override
            public boolean isApplicable(final Context context) {
                final TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
                return telephonyManager != null && telephonyManager.getCurrentPhoneType() == 2;
            }
        };
    }
}
