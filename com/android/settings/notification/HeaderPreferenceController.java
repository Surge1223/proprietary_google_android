package com.android.settings.notification;

import android.app.Activity;
import android.app.Fragment;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.text.BidiFormatter;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.content.Context;
import com.android.settings.widget.EntityHeaderController;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.core.PreferenceControllerMixin;
import android.arch.lifecycle.LifecycleObserver;

public class HeaderPreferenceController extends NotificationPreferenceController implements LifecycleObserver, PreferenceControllerMixin
{
    private final PreferenceFragment mFragment;
    private EntityHeaderController mHeaderController;
    private boolean mStarted;
    
    public HeaderPreferenceController(final Context context, final PreferenceFragment mFragment) {
        super(context, null);
        this.mStarted = false;
        this.mFragment = mFragment;
    }
    
    CharSequence getLabel() {
        CharSequence charSequence;
        if (this.mChannel != null && !this.isDefaultChannel()) {
            charSequence = this.mChannel.getName();
        }
        else if (this.mChannelGroup != null) {
            charSequence = this.mChannelGroup.getName();
        }
        else {
            charSequence = this.mAppRow.label;
        }
        return charSequence;
    }
    
    @Override
    public String getPreferenceKey() {
        return "pref_app_header";
    }
    
    @Override
    public CharSequence getSummary() {
        if (this.mChannel != null && !this.isDefaultChannel()) {
            if (this.mChannelGroup != null && !TextUtils.isEmpty(this.mChannelGroup.getName())) {
                final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                final BidiFormatter instance = BidiFormatter.getInstance();
                spannableStringBuilder.append((CharSequence)instance.unicodeWrap(this.mAppRow.label.toString()));
                spannableStringBuilder.append(instance.unicodeWrap(this.mContext.getText(2131888442)));
                spannableStringBuilder.append((CharSequence)instance.unicodeWrap(this.mChannelGroup.getName().toString()));
                return spannableStringBuilder.toString();
            }
            return this.mAppRow.label.toString();
        }
        else {
            if (this.mChannelGroup != null) {
                return this.mAppRow.label.toString();
            }
            return "";
        }
    }
    
    @Override
    public boolean isAvailable() {
        return this.mAppRow != null;
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onStart() {
        this.mStarted = true;
        if (this.mHeaderController != null) {
            this.mHeaderController.styleActionBar(this.mFragment.getActivity());
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null && this.mFragment != null) {
            Activity activity = null;
            if (this.mStarted) {
                activity = this.mFragment.getActivity();
            }
            this.mHeaderController = EntityHeaderController.newInstance(this.mFragment.getActivity(), this.mFragment, ((LayoutPreference)preference).findViewById(2131362112));
            this.mHeaderController.setIcon(this.mAppRow.icon).setLabel(this.getLabel()).setSummary(this.getSummary()).setPackageName(this.mAppRow.pkg).setUid(this.mAppRow.uid).setButtonActions(1, 0).setHasAppInfoLink(true).done(activity, this.mContext).findViewById(2131362112).setVisibility(0);
        }
    }
}
