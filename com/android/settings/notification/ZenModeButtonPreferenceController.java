package com.android.settings.notification;

import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import android.view.View.OnClickListener;
import android.view.View;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.widget.Button;
import android.app.FragmentManager;
import com.android.settings.core.PreferenceControllerMixin;

public class ZenModeButtonPreferenceController extends AbstractZenModePreferenceController implements PreferenceControllerMixin
{
    private FragmentManager mFragment;
    private Button mZenButtonOff;
    private Button mZenButtonOn;
    
    public ZenModeButtonPreferenceController(final Context context, final Lifecycle lifecycle, final FragmentManager mFragment) {
        super(context, "zen_mode_settings_button_container", lifecycle);
        this.mFragment = mFragment;
    }
    
    private void updateButtons() {
        switch (this.getZenMode()) {
            default: {
                this.mZenButtonOff.setVisibility(8);
                this.updateZenButtonOnClickListener();
                this.mZenButtonOn.setVisibility(0);
                break;
            }
            case 1:
            case 2:
            case 3: {
                this.mZenButtonOff.setVisibility(0);
                this.mZenButtonOn.setVisibility(8);
                break;
            }
        }
    }
    
    private void updateZenButtonOnClickListener() {
        final int zenDuration = this.getZenDuration();
        switch (zenDuration) {
            default: {
                this.mZenButtonOn.setOnClickListener((View.OnClickListener)new _$$Lambda$ZenModeButtonPreferenceController$NQfCfaUFz6J6tbPXZDP09CGnoAo(this, zenDuration));
                break;
            }
            case 0: {
                this.mZenButtonOn.setOnClickListener((View.OnClickListener)new _$$Lambda$ZenModeButtonPreferenceController$16_xvFNOTseGHNtlUJrmr4Oa8o8(this));
                break;
            }
            case -1: {
                this.mZenButtonOn.setOnClickListener((View.OnClickListener)new _$$Lambda$ZenModeButtonPreferenceController$KAk_Mj51Obvq4mW4RobrcR4_CRM(this));
                break;
            }
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_settings_button_container";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (this.mZenButtonOn == null) {
            this.mZenButtonOn = ((LayoutPreference)preference).findViewById(2131362849);
            this.updateZenButtonOnClickListener();
        }
        if (this.mZenButtonOff == null) {
            (this.mZenButtonOff = ((LayoutPreference)preference).findViewById(2131362848)).setOnClickListener((View.OnClickListener)new _$$Lambda$ZenModeButtonPreferenceController$RnfY8k3LZN005jbH9s0d6akYfFk(this));
        }
        this.updateButtons();
    }
}
