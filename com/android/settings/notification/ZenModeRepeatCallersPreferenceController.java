package com.android.settings.notification;

import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ZenModeRepeatCallersPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceChangeListener
{
    private final ZenModeBackend mBackend;
    private final int mRepeatCallersThreshold;
    
    public ZenModeRepeatCallersPreferenceController(final Context context, final Lifecycle lifecycle, final int mRepeatCallersThreshold) {
        super(context, "zen_mode_repeat_callers", lifecycle);
        this.mRepeatCallersThreshold = mRepeatCallersThreshold;
        this.mBackend = ZenModeBackend.getInstance(context);
    }
    
    private void setRepeatCallerSummary(final Preference preference) {
        preference.setSummary(this.mContext.getString(2131890335, new Object[] { this.mRepeatCallersThreshold }));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.setRepeatCallerSummary(preferenceScreen.findPreference("zen_mode_repeat_callers"));
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_repeat_callers";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        if (ZenModeSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onPrefChange allowRepeatCallers=");
            sb.append(booleanValue);
            Log.d("PrefControllerMixin", sb.toString());
        }
        this.mMetricsFeatureProvider.action(this.mContext, 171, booleanValue);
        this.mBackend.saveSoundPolicy(16, booleanValue);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        switch (this.getZenMode()) {
            default: {
                if (this.mBackend.isPriorityCategoryEnabled(8) && this.mBackend.getPriorityCallSenders() == 0) {
                    switchPreference.setEnabled(false);
                    switchPreference.setChecked(true);
                    break;
                }
                switchPreference.setEnabled(true);
                switchPreference.setChecked(this.mBackend.isPriorityCategoryEnabled(16));
                break;
            }
            case 2:
            case 3: {
                switchPreference.setEnabled(false);
                switchPreference.setChecked(false);
                break;
            }
        }
    }
}
