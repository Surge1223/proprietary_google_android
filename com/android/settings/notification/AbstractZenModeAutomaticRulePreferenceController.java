package com.android.settings.notification;

import android.util.Pair;
import android.support.v7.preference.Preference;
import java.util.Arrays;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.ComponentName;
import android.content.pm.ServiceInfo;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.service.notification.ZenModeConfig;
import java.util.Set;
import android.content.pm.PackageManager;
import android.app.Fragment;
import java.util.List;
import android.app.AutomaticZenRule;
import java.util.Map;
import java.util.Comparator;
import com.android.settings.core.PreferenceControllerMixin;

public abstract class AbstractZenModeAutomaticRulePreferenceController extends AbstractZenModePreferenceController implements PreferenceControllerMixin
{
    private static final Comparator<Map.Entry<String, AutomaticZenRule>> RULE_COMPARATOR;
    private static List<String> mDefaultRuleIds;
    protected ZenModeBackend mBackend;
    protected Fragment mParent;
    protected PackageManager mPm;
    protected Set<Map.Entry<String, AutomaticZenRule>> mRules;
    
    static {
        RULE_COMPARATOR = new Comparator<Map.Entry<String, AutomaticZenRule>>() {
            private String key(final AutomaticZenRule automaticZenRule) {
                int n;
                if (ZenModeConfig.isValidScheduleConditionId(automaticZenRule.getConditionId())) {
                    n = 1;
                }
                else if (ZenModeConfig.isValidEventConditionId(automaticZenRule.getConditionId())) {
                    n = 2;
                }
                else {
                    n = 3;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(n);
                sb.append(automaticZenRule.getName().toString());
                return sb.toString();
            }
            
            @Override
            public int compare(final Map.Entry<String, AutomaticZenRule> entry, final Map.Entry<String, AutomaticZenRule> entry2) {
                final boolean contains = getDefaultRuleIds().contains(entry.getKey());
                if (contains != getDefaultRuleIds().contains(entry2.getKey())) {
                    int n;
                    if (contains) {
                        n = -1;
                    }
                    else {
                        n = 1;
                    }
                    return n;
                }
                final int compare = Long.compare(entry.getValue().getCreationTime(), entry2.getValue().getCreationTime());
                if (compare != 0) {
                    return compare;
                }
                return this.key(entry.getValue()).compareTo(this.key(entry2.getValue()));
            }
        };
    }
    
    public AbstractZenModeAutomaticRulePreferenceController(final Context context, final String s, final Fragment mParent, final Lifecycle lifecycle) {
        super(context, s, lifecycle);
        this.mBackend = ZenModeBackend.getInstance(context);
        this.mPm = this.mContext.getPackageManager();
        this.mParent = mParent;
    }
    
    private static List<String> getDefaultRuleIds() {
        if (AbstractZenModeAutomaticRulePreferenceController.mDefaultRuleIds == null) {
            AbstractZenModeAutomaticRulePreferenceController.mDefaultRuleIds = (List<String>)ZenModeConfig.DEFAULT_RULE_IDS;
        }
        return AbstractZenModeAutomaticRulePreferenceController.mDefaultRuleIds;
    }
    
    public static ZenRuleInfo getRuleInfo(final PackageManager packageManager, final ServiceInfo serviceInfo) {
        if (serviceInfo == null || serviceInfo.metaData == null) {
            return null;
        }
        final String string = serviceInfo.metaData.getString("android.service.zen.automatic.ruleType");
        final ComponentName settingsActivity = getSettingsActivity(serviceInfo);
        if (string != null && !string.trim().isEmpty() && settingsActivity != null) {
            final ZenRuleInfo zenRuleInfo = new ZenRuleInfo();
            zenRuleInfo.serviceComponent = new ComponentName(serviceInfo.packageName, serviceInfo.name);
            zenRuleInfo.settingsAction = "android.settings.ZEN_MODE_EXTERNAL_RULE_SETTINGS";
            zenRuleInfo.title = string;
            zenRuleInfo.packageName = serviceInfo.packageName;
            zenRuleInfo.configurationActivity = getSettingsActivity(serviceInfo);
            zenRuleInfo.packageLabel = serviceInfo.applicationInfo.loadLabel(packageManager);
            zenRuleInfo.ruleInstanceLimit = serviceInfo.metaData.getInt("android.service.zen.automatic.ruleInstanceLimit", -1);
            return zenRuleInfo;
        }
        return null;
    }
    
    protected static Intent getRuleIntent(final String action, final ComponentName component, final String s) {
        final Intent putExtra = new Intent().addFlags(67108864).putExtra("android.service.notification.extra.RULE_ID", s);
        if (component != null) {
            putExtra.setComponent(component);
        }
        else {
            putExtra.setAction(action);
        }
        return putExtra;
    }
    
    protected static ComponentName getSettingsActivity(final ServiceInfo serviceInfo) {
        if (serviceInfo == null || serviceInfo.metaData == null) {
            return null;
        }
        final String string = serviceInfo.metaData.getString("android.service.zen.automatic.configurationActivity");
        if (string != null) {
            return ComponentName.unflattenFromString(string);
        }
        return null;
    }
    
    private Set<Map.Entry<String, AutomaticZenRule>> getZenModeRules() {
        return NotificationManager.from(this.mContext).getAutomaticZenRules().entrySet();
    }
    
    protected void showNameRuleDialog(final ZenRuleInfo zenRuleInfo, final Fragment fragment) {
        ZenRuleNameDialog.show(fragment, null, zenRuleInfo.defaultConditionId, (ZenRuleNameDialog.PositiveClickListener)new RuleNameChangeListener(zenRuleInfo));
    }
    
    protected Map.Entry<String, AutomaticZenRule>[] sortedRules() {
        if (this.mRules == null) {
            this.mRules = this.getZenModeRules();
        }
        final Map.Entry[] array = this.mRules.toArray((Map.Entry[])new Map.Entry[this.mRules.size()]);
        Arrays.sort(array, (Comparator<? super Map.Entry>)AbstractZenModeAutomaticRulePreferenceController.RULE_COMPARATOR);
        return (Map.Entry<String, AutomaticZenRule>[])array;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.mRules = this.getZenModeRules();
    }
    
    public class RuleNameChangeListener implements PositiveClickListener
    {
        ZenRuleInfo mRuleInfo;
        
        public RuleNameChangeListener(final ZenRuleInfo mRuleInfo) {
            this.mRuleInfo = mRuleInfo;
        }
        
        @Override
        public void onOk(String addZenRule, final Fragment fragment) {
            AbstractZenModeAutomaticRulePreferenceController.this.mMetricsFeatureProvider.action(AbstractZenModeAutomaticRulePreferenceController.this.mContext, 1267, (Pair<Integer, Object>[])new Pair[0]);
            addZenRule = AbstractZenModeAutomaticRulePreferenceController.this.mBackend.addZenRule(new AutomaticZenRule(addZenRule, this.mRuleInfo.serviceComponent, this.mRuleInfo.defaultConditionId, 2, true));
            if (addZenRule != null) {
                fragment.startActivity(AbstractZenModeAutomaticRulePreferenceController.getRuleIntent(this.mRuleInfo.settingsAction, null, addZenRule));
            }
        }
    }
}
