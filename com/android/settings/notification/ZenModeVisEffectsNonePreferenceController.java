package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class ZenModeVisEffectsNonePreferenceController extends AbstractZenModePreferenceController implements OnRadioButtonClickListener
{
    private final String KEY;
    private ZenCustomRadioButtonPreference mPreference;
    
    public ZenModeVisEffectsNonePreferenceController(final Context context, final Lifecycle lifecycle, final String key) {
        super(context, key, lifecycle);
        this.KEY = key;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = (ZenCustomRadioButtonPreference)preferenceScreen.findPreference(this.KEY)).setOnRadioButtonClickListener((ZenCustomRadioButtonPreference.OnRadioButtonClickListener)this);
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onRadioButtonClick(final ZenCustomRadioButtonPreference zenCustomRadioButtonPreference) {
        this.mMetricsFeatureProvider.action(this.mContext, 1396, true);
        this.mBackend.saveVisualEffectsPolicy(511, false);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.mPreference.setChecked(this.mBackend.mPolicy.suppressedVisualEffects == 0);
    }
}
