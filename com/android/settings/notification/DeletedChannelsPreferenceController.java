package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;

public class DeletedChannelsPreferenceController extends NotificationPreferenceController implements PreferenceControllerMixin
{
    public DeletedChannelsPreferenceController(final Context context, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
    }
    
    @Override
    public String getPreferenceKey() {
        return "deleted";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        boolean b = false;
        if (!available) {
            return false;
        }
        if (this.mChannel == null && !this.hasValidGroup()) {
            if (this.mBackend.getDeletedChannelCount(this.mAppRow.pkg, this.mAppRow.uid) > 0) {
                b = true;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null) {
            final int deletedChannelCount = this.mBackend.getDeletedChannelCount(this.mAppRow.pkg, this.mAppRow.uid);
            preference.setTitle(this.mContext.getResources().getQuantityString(2131755028, deletedChannelCount, new Object[] { deletedChannelCount }));
        }
        preference.setSelectable(false);
    }
}
