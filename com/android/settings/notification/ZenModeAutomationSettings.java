package com.android.settings.notification;

import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.utils.ZenServiceListing;
import android.app.Fragment;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.utils.ManagedServiceSettings;
import com.android.settings.search.Indexable;

public class ZenModeAutomationSettings extends ZenModeSettingsBase
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    protected final ManagedServiceSettings.Config CONFIG;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("zen_mode_add_automatic_rule");
                nonIndexableKeys.add("zen_mode_automatic_rules");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082877;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    public ZenModeAutomationSettings() {
        this.CONFIG = getConditionProviderConfig();
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Fragment fragment, final ZenServiceListing zenServiceListing, final Lifecycle lifecycle) {
        final ArrayList<ZenModeAutomaticRulesPreferenceController> list = (ArrayList<ZenModeAutomaticRulesPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new ZenModeAddAutomaticRulePreferenceController(context, fragment, zenServiceListing, lifecycle));
        list.add(new ZenModeAutomaticRulesPreferenceController(context, fragment, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    protected static ManagedServiceSettings.Config getConditionProviderConfig() {
        return new ManagedServiceSettings.Config.Builder().setTag("ZenModeSettings").setIntentAction("android.service.notification.ConditionProviderService").setPermission("android.permission.BIND_CONDITION_PROVIDER_SERVICE").setNoun("condition provider").build();
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ZenServiceListing zenServiceListing = new ZenServiceListing(this.getContext(), this.CONFIG);
        zenServiceListing.reloadApprovedServices();
        return buildPreferenceControllers(context, this, zenServiceListing, this.getLifecycle());
    }
    
    @Override
    public int getMetricsCategory() {
        return 142;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082877;
    }
}
