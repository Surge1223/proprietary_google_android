package com.android.settings.notification;

import android.net.Uri;
import android.database.ContentObserver;
import android.os.Bundle;
import android.provider.Settings;
import android.content.ContentResolver;
import android.util.Log;
import android.os.Handler;
import android.content.Context;
import com.android.settings.dashboard.RestrictedDashboardFragment;

public abstract class ZenModeSettingsBase extends RestrictedDashboardFragment
{
    protected static final boolean DEBUG;
    protected ZenModeBackend mBackend;
    protected Context mContext;
    private final Handler mHandler;
    private final SettingsObserver mSettingsObserver;
    protected int mZenMode;
    
    static {
        DEBUG = Log.isLoggable("ZenModeSettings", 3);
    }
    
    public ZenModeSettingsBase() {
        super("no_adjust_volume");
        this.mHandler = new Handler();
        this.mSettingsObserver = new SettingsObserver();
    }
    
    private void updateZenMode(final boolean b) {
        final int int1 = Settings.Global.getInt(this.getContentResolver(), "zen_mode", this.mZenMode);
        if (int1 == this.mZenMode) {
            return;
        }
        this.mZenMode = int1;
        if (ZenModeSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("updateZenMode mZenMode=");
            sb.append(this.mZenMode);
            sb.append(" ");
            sb.append(b);
            Log.d("ZenModeSettings", sb.toString());
        }
    }
    
    @Override
    protected String getLogTag() {
        return "ZenModeSettings";
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.mContext = (Context)this.getActivity();
        this.mBackend = ZenModeBackend.getInstance(this.mContext);
        super.onCreate(bundle);
        this.updateZenMode(false);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mSettingsObserver.unregister();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateZenMode(true);
        this.mSettingsObserver.register();
        if (this.isUiRestricted()) {
            if (this.isUiRestrictedByOnlyAdmin()) {
                this.getPreferenceScreen().removeAll();
                return;
            }
            this.finish();
        }
    }
    
    protected void onZenModeConfigChanged() {
    }
    
    private final class SettingsObserver extends ContentObserver
    {
        private final Uri ZEN_MODE_CONFIG_ETAG_URI;
        private final Uri ZEN_MODE_URI;
        
        private SettingsObserver() {
            super(ZenModeSettingsBase.this.mHandler);
            this.ZEN_MODE_URI = Settings.Global.getUriFor("zen_mode");
            this.ZEN_MODE_CONFIG_ETAG_URI = Settings.Global.getUriFor("zen_mode_config_etag");
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.ZEN_MODE_URI.equals((Object)uri)) {
                ZenModeSettingsBase.this.updateZenMode(true);
            }
            if (this.ZEN_MODE_CONFIG_ETAG_URI.equals((Object)uri)) {
                ZenModeSettingsBase.this.mBackend.updatePolicy();
                ZenModeSettingsBase.this.onZenModeConfigChanged();
            }
        }
        
        public void register() {
            SettingsPreferenceFragment.this.getContentResolver().registerContentObserver(this.ZEN_MODE_URI, false, (ContentObserver)this);
            SettingsPreferenceFragment.this.getContentResolver().registerContentObserver(this.ZEN_MODE_CONFIG_ETAG_URI, false, (ContentObserver)this);
        }
        
        public void unregister() {
            SettingsPreferenceFragment.this.getContentResolver().unregisterContentObserver((ContentObserver)this);
        }
    }
}
