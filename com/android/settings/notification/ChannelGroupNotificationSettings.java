package com.android.settings.notification;

import java.util.Collection;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import java.util.Iterator;
import android.support.v7.preference.PreferenceGroup;
import android.app.NotificationChannel;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import android.support.v7.preference.Preference;
import android.util.Log;

public class ChannelGroupNotificationSettings extends NotificationSettingsBase
{
    private void populateChannelList() {
        if (!this.mDynamicPreferences.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Notification channel group posted twice to settings - old size ");
            sb.append(this.mDynamicPreferences.size());
            sb.append(", new size ");
            sb.append(this.mDynamicPreferences.size());
            Log.w("ChannelGroupSettings", sb.toString());
            final Iterator<Preference> iterator = this.mDynamicPreferences.iterator();
            while (iterator.hasNext()) {
                this.getPreferenceScreen().removePreference(iterator.next());
            }
        }
        if (this.mChannelGroup.getChannels().isEmpty()) {
            final Preference preference = new Preference(this.getPrefContext());
            preference.setTitle(2131888412);
            preference.setEnabled(false);
            this.getPreferenceScreen().addPreference(preference);
            this.mDynamicPreferences.add(preference);
        }
        else {
            final List channels = this.mChannelGroup.getChannels();
            Collections.sort((List<Object>)channels, (Comparator<? super Object>)this.mChannelComparator);
            final Iterator<NotificationChannel> iterator2 = channels.iterator();
            while (iterator2.hasNext()) {
                this.mDynamicPreferences.add(this.populateSingleChannelPrefs(this.getPreferenceScreen(), iterator2.next(), this.mChannelGroup.isBlocked()));
            }
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        (this.mControllers = new ArrayList<NotificationPreferenceController>()).add(new HeaderPreferenceController(context, this));
        this.mControllers.add(new BlockPreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new AppLinkPreferenceController(context));
        this.mControllers.add(new NotificationsOffPreferenceController(context));
        this.mControllers.add(new DescriptionPreferenceController(context));
        return new ArrayList<AbstractPreferenceController>(this.mControllers);
    }
    
    @Override
    protected String getLogTag() {
        return "ChannelGroupSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1218;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082796;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mAppRow != null && this.mChannelGroup != null) {
            this.populateChannelList();
            for (final NotificationPreferenceController notificationPreferenceController : this.mControllers) {
                notificationPreferenceController.onResume(this.mAppRow, this.mChannel, this.mChannelGroup, this.mSuspendedAppsAdmin);
                notificationPreferenceController.displayPreference(this.getPreferenceScreen());
            }
            this.updatePreferenceStates();
            return;
        }
        Log.w("ChannelGroupSettings", "Missing package or uid or packageinfo or group");
        this.finish();
    }
}
