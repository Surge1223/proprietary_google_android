package com.android.settings.notification;

import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.IntentFilter;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import android.media.Ringtone;
import com.android.settings.DefaultRingtonePreference;
import android.media.RingtoneManager;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.PreferenceGroup;
import android.os.UserManager;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import android.support.v7.preference.Preference;

public final class _$$Lambda$WorkSoundPreferenceController$XBbO1oM_StZ54wAnUJEnnExa5OU implements OnPreferenceChangeListener
{
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return WorkSoundPreferenceController.lambda$updateWorkPreferences$0(this.f$0, preference, o);
    }
}
