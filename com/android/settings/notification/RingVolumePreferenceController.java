package com.android.settings.notification;

import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.os.Message;
import android.os.Looper;
import android.os.Handler;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.text.TextUtils;
import com.android.settings.Utils;
import java.util.Objects;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Vibrator;
import android.content.ComponentName;

public class RingVolumePreferenceController extends VolumeSeekBarPreferenceController
{
    private static final String KEY_RING_VOLUME = "ring_volume";
    private static final String TAG = "RingVolumeController";
    private final H mHandler;
    private int mMuteIcon;
    private final RingReceiver mReceiver;
    private int mRingerMode;
    private ComponentName mSuppressor;
    private Vibrator mVibrator;
    
    public RingVolumePreferenceController(final Context context) {
        this(context, "ring_volume");
    }
    
    public RingVolumePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mRingerMode = -1;
        this.mReceiver = new RingReceiver();
        this.mHandler = new H();
        this.mVibrator = (Vibrator)this.mContext.getSystemService("vibrator");
        if (this.mVibrator != null && !this.mVibrator.hasVibrator()) {
            this.mVibrator = null;
        }
        this.updateRingerMode();
    }
    
    private void updateEffectsSuppressor() {
        final ComponentName effectsSuppressor = NotificationManager.from(this.mContext).getEffectsSuppressor();
        if (Objects.equals(effectsSuppressor, this.mSuppressor)) {
            return;
        }
        this.mSuppressor = effectsSuppressor;
        if (this.mPreference != null) {
            this.mPreference.setSuppressionText(SuppressorHelper.getSuppressionText(this.mContext, effectsSuppressor));
        }
        this.updatePreferenceIcon();
    }
    
    private void updatePreferenceIcon() {
        if (this.mPreference != null) {
            if (this.mRingerMode == 1) {
                this.mMuteIcon = 2131231189;
                this.mPreference.showIcon(2131231189);
            }
            else if (this.mRingerMode == 0) {
                this.mMuteIcon = 2131231079;
                this.mPreference.showIcon(2131231079);
            }
            else {
                this.mPreference.showIcon(2131231078);
            }
        }
    }
    
    private void updateRingerMode() {
        final int ringerModeInternal = this.mHelper.getRingerModeInternal();
        if (this.mRingerMode == ringerModeInternal) {
            return;
        }
        this.mRingerMode = ringerModeInternal;
        this.updatePreferenceIcon();
    }
    
    public int getAudioStream() {
        return 2;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (Utils.isVoiceCapable(this.mContext) && !this.mHelper.isSingleVolume()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    public int getMuteIcon() {
        return this.mMuteIcon;
    }
    
    @Override
    public String getPreferenceKey() {
        return "ring_volume";
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"ring_volume");
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    @Override
    public void onPause() {
        super.onPause();
        this.mReceiver.register(false);
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    @Override
    public void onResume() {
        super.onResume();
        this.mReceiver.register(true);
        this.updateEffectsSuppressor();
        this.updatePreferenceIcon();
    }
    
    private final class H extends Handler
    {
        private H() {
            super(Looper.getMainLooper());
        }
        
        public void handleMessage(final Message message) {
            switch (message.what) {
                case 2: {
                    RingVolumePreferenceController.this.updateRingerMode();
                    break;
                }
                case 1: {
                    RingVolumePreferenceController.this.updateEffectsSuppressor();
                    break;
                }
            }
        }
    }
    
    private class RingReceiver extends BroadcastReceiver
    {
        private boolean mRegistered;
        
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if ("android.os.action.ACTION_EFFECTS_SUPPRESSOR_CHANGED".equals(action)) {
                RingVolumePreferenceController.this.mHandler.sendEmptyMessage(1);
            }
            else if ("android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION".equals(action)) {
                RingVolumePreferenceController.this.mHandler.sendEmptyMessage(2);
            }
        }
        
        public void register(final boolean mRegistered) {
            if (this.mRegistered == mRegistered) {
                return;
            }
            if (mRegistered) {
                final IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.os.action.ACTION_EFFECTS_SUPPRESSOR_CHANGED");
                intentFilter.addAction("android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION");
                RingVolumePreferenceController.this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter);
            }
            else {
                RingVolumePreferenceController.this.mContext.unregisterReceiver((BroadcastReceiver)this);
            }
            this.mRegistered = mRegistered;
        }
    }
}
