package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ZenModeAutomationPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final ZenModeSettings.SummaryBuilder mSummaryBuilder;
    
    public ZenModeAutomationPreferenceController(final Context context) {
        super(context);
        this.mSummaryBuilder = new ZenModeSettings.SummaryBuilder(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_automation_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(this.mSummaryBuilder.getAutomaticRulesSummary());
    }
}
