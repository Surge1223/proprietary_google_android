package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.os.SystemProperties;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BootSoundPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    static final String PROPERTY_BOOT_SOUNDS = "persist.sys.bootanim.play_sound";
    
    public BootSoundPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            ((SwitchPreference)preferenceScreen.findPreference("boot_sounds")).setChecked(SystemProperties.getBoolean("persist.sys.bootanim.play_sound", true));
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "boot_sounds";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("boot_sounds".equals(preference.getKey())) {
            String s;
            if (((SwitchPreference)preference).isChecked()) {
                s = "1";
            }
            else {
                s = "0";
            }
            SystemProperties.set("persist.sys.bootanim.play_sound", s);
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034183);
    }
}
