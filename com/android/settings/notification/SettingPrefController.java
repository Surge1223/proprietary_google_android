package com.android.settings.notification;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Handler;
import com.android.internal.annotations.VisibleForTesting;
import android.database.ContentObserver;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class SettingPrefController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private SettingsPreferenceFragment mParent;
    protected SettingPref mPreference;
    protected SettingsObserver mSettingsObserver;
    
    public SettingPrefController(final Context context, final SettingsPreferenceFragment mParent, final Lifecycle lifecycle) {
        super(context);
        this.mParent = mParent;
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mPreference.init(this.mParent);
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mSettingsObserver = new SettingsObserver();
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPreference.getKey();
    }
    
    @Override
    public boolean isAvailable() {
        return this.mPreference.isApplicable(this.mContext);
    }
    
    @Override
    public void onPause() {
        if (this.mSettingsObserver != null) {
            this.mSettingsObserver.register(false);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingsObserver != null) {
            this.mSettingsObserver.register(true);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.mPreference.update(this.mContext);
    }
    
    @VisibleForTesting
    final class SettingsObserver extends ContentObserver
    {
        public SettingsObserver() {
            super(new Handler());
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (SettingPrefController.this.mPreference.getUri().equals((Object)uri)) {
                SettingPrefController.this.mPreference.update(SettingPrefController.this.mContext);
            }
        }
        
        public void register(final boolean b) {
            final ContentResolver contentResolver = SettingPrefController.this.mContext.getContentResolver();
            if (b) {
                contentResolver.registerContentObserver(SettingPrefController.this.mPreference.getUri(), false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
