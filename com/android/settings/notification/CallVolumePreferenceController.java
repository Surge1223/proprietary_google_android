package com.android.settings.notification;

import android.text.TextUtils;
import android.content.Context;
import android.media.AudioManager;

public class CallVolumePreferenceController extends VolumeSeekBarPreferenceController
{
    private AudioManager mAudioManager;
    
    public CallVolumePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mAudioManager = (AudioManager)context.getSystemService((Class)AudioManager.class);
    }
    
    public int getAudioStream() {
        if (this.mAudioManager.isBluetoothScoOn()) {
            return 6;
        }
        return 0;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034136) && !this.mHelper.isSingleVolume()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    public int getMuteIcon() {
        return 2131231056;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"call_volume");
    }
}
