package com.android.settings.notification;

import android.preference.SeekBarVolumizer;
import android.os.UserHandle;
import android.app.Fragment;
import android.support.v7.preference.Preference;
import android.text.TextUtils;
import android.os.Bundle;
import java.util.Iterator;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.sound.HandsFreeProfileOutputPreferenceController;
import com.android.settings.sound.AudioSwitchPreferenceController;
import com.android.settings.sound.MediaOutputPreferenceController;
import android.content.Intent;
import android.support.v7.preference.ListPreference;
import com.android.settings.widget.PreferenceCategoryController;
import com.android.settings.SettingsPreferenceFragment;
import java.util.ArrayList;
import android.os.Message;
import android.os.Looper;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.RingtonePreference;
import android.os.Handler;
import com.android.settings.widget.UpdatableListPreferenceDialogFragment;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.dashboard.DashboardFragment;

public class SoundSettings extends DashboardFragment
{
    public static final BaseSearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    static final int STOP_SAMPLE = 1;
    private UpdatableListPreferenceDialogFragment mDialogFragment;
    final Handler mHandler;
    private String mHfpOutputControllerKey;
    private String mMediaOutputControllerKey;
    private RingtonePreference mRequestPreference;
    final VolumePreferenceCallback mVolumeCallback;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add(new ZenModePreferenceController(context, null, "zen_mode").getPreferenceKey());
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082833;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    public SoundSettings() {
        this.mVolumeCallback = new VolumePreferenceCallback();
        this.mHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(final Message message) {
                if (message.what == 1) {
                    SoundSettings.this.mVolumeCallback.stopSample();
                }
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final SoundSettings soundSettings, final Lifecycle lifecycle) {
        final ArrayList<PreferenceCategoryController> list = (ArrayList<PreferenceCategoryController>)new ArrayList<WorkSoundPreferenceController>();
        list.add((WorkSoundPreferenceController)new ZenModePreferenceController(context, lifecycle, "zen_mode"));
        list.add((WorkSoundPreferenceController)new PhoneRingtonePreferenceController(context));
        list.add((WorkSoundPreferenceController)new AlarmRingtonePreferenceController(context));
        list.add((WorkSoundPreferenceController)new NotificationRingtonePreferenceController(context));
        list.add(new WorkSoundPreferenceController(context, soundSettings, lifecycle));
        final DialPadTonePreferenceController dialPadTonePreferenceController = new DialPadTonePreferenceController(context, soundSettings, lifecycle);
        final ScreenLockSoundPreferenceController screenLockSoundPreferenceController = new ScreenLockSoundPreferenceController(context, soundSettings, lifecycle);
        final ChargingSoundPreferenceController chargingSoundPreferenceController = new ChargingSoundPreferenceController(context, soundSettings, lifecycle);
        final DockingSoundPreferenceController dockingSoundPreferenceController = new DockingSoundPreferenceController(context, soundSettings, lifecycle);
        final TouchSoundPreferenceController touchSoundPreferenceController = new TouchSoundPreferenceController(context, soundSettings, lifecycle);
        final VibrateOnTouchPreferenceController vibrateOnTouchPreferenceController = new VibrateOnTouchPreferenceController(context, soundSettings, lifecycle);
        final DockAudioMediaPreferenceController dockAudioMediaPreferenceController = new DockAudioMediaPreferenceController(context, soundSettings, lifecycle);
        final BootSoundPreferenceController bootSoundPreferenceController = new BootSoundPreferenceController(context);
        final EmergencyTonePreferenceController emergencyTonePreferenceController = new EmergencyTonePreferenceController(context, soundSettings, lifecycle);
        list.add((WorkSoundPreferenceController)dialPadTonePreferenceController);
        list.add((WorkSoundPreferenceController)screenLockSoundPreferenceController);
        list.add((WorkSoundPreferenceController)chargingSoundPreferenceController);
        list.add((WorkSoundPreferenceController)dockingSoundPreferenceController);
        list.add((WorkSoundPreferenceController)touchSoundPreferenceController);
        list.add((WorkSoundPreferenceController)vibrateOnTouchPreferenceController);
        list.add((WorkSoundPreferenceController)dockAudioMediaPreferenceController);
        list.add((WorkSoundPreferenceController)bootSoundPreferenceController);
        list.add((WorkSoundPreferenceController)emergencyTonePreferenceController);
        list.add(new PreferenceCategoryController(context, "other_sounds_and_vibrations_category").setChildren(Arrays.asList(dialPadTonePreferenceController, screenLockSoundPreferenceController, chargingSoundPreferenceController, dockingSoundPreferenceController, touchSoundPreferenceController, vibrateOnTouchPreferenceController, dockAudioMediaPreferenceController, bootSoundPreferenceController, emergencyTonePreferenceController)));
        return (List<AbstractPreferenceController>)list;
    }
    
    private void onPreferenceDataChanged(final ListPreference listPreference) {
        if (this.mDialogFragment != null) {
            this.mDialogFragment.onListPreferenceUpdated(listPreference);
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this, this.getLifecycle());
    }
    
    void enableWorkSync() {
        final WorkSoundPreferenceController workSoundPreferenceController = this.use(WorkSoundPreferenceController.class);
        if (workSoundPreferenceController != null) {
            workSoundPreferenceController.enableWorkSync();
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887829;
    }
    
    @Override
    protected String getLogTag() {
        return "SoundSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 336;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082833;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (this.mRequestPreference != null) {
            this.mRequestPreference.onActivityResult(n, n2, intent);
            this.mRequestPreference = null;
        }
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        final ArrayList<AlarmVolumePreferenceController> list = new ArrayList<AlarmVolumePreferenceController>();
        list.add(this.use(AlarmVolumePreferenceController.class));
        list.add((AlarmVolumePreferenceController)this.use(MediaVolumePreferenceController.class));
        list.add((AlarmVolumePreferenceController)this.use(RingVolumePreferenceController.class));
        list.add((AlarmVolumePreferenceController)this.use(NotificationVolumePreferenceController.class));
        list.add((AlarmVolumePreferenceController)this.use(CallVolumePreferenceController.class));
        this.use(MediaOutputPreferenceController.class).setCallback((AudioSwitchPreferenceController.AudioSwitchCallback)new _$$Lambda$SoundSettings$jhjL65jex6M9bCQGfPcGogvuH7o(this));
        this.mMediaOutputControllerKey = this.use(MediaOutputPreferenceController.class).getPreferenceKey();
        this.use(HandsFreeProfileOutputPreferenceController.class).setCallback((AudioSwitchPreferenceController.AudioSwitchCallback)new _$$Lambda$SoundSettings$P7n4dsft5tPdi8YUFcItX8K9whw(this));
        this.mHfpOutputControllerKey = this.use(HandsFreeProfileOutputPreferenceController.class).getPreferenceKey();
        for (final AlarmVolumePreferenceController alarmVolumePreferenceController : list) {
            alarmVolumePreferenceController.setCallback(this.mVolumeCallback);
            this.getLifecycle().addObserver(alarmVolumePreferenceController);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            final String string = bundle.getString("selected_preference", (String)null);
            if (!TextUtils.isEmpty((CharSequence)string)) {
                this.mRequestPreference = (RingtonePreference)this.findPreference(string);
            }
            this.mDialogFragment = (UpdatableListPreferenceDialogFragment)this.getFragmentManager().findFragmentByTag("SoundSettings");
        }
    }
    
    @Override
    public void onDisplayPreferenceDialog(final Preference preference) {
        int n;
        if (this.mHfpOutputControllerKey.equals(preference.getKey())) {
            n = 1416;
        }
        else if (this.mMediaOutputControllerKey.equals(preference.getKey())) {
            n = 1415;
        }
        else {
            n = 0;
        }
        (this.mDialogFragment = UpdatableListPreferenceDialogFragment.newInstance(preference.getKey(), n)).setTargetFragment((Fragment)this, 0);
        this.mDialogFragment.show(this.getFragmentManager(), "SoundSettings");
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mVolumeCallback.stopSample();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof RingtonePreference) {
            (this.mRequestPreference = (RingtonePreference)preference).onPrepareRingtonePickerIntent(this.mRequestPreference.getIntent());
            this.startActivityForResultAsUser(this.mRequestPreference.getIntent(), 200, (Bundle)null, UserHandle.of(this.mRequestPreference.getUserId()));
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mRequestPreference != null) {
            bundle.putString("selected_preference", this.mRequestPreference.getKey());
        }
    }
    
    final class VolumePreferenceCallback implements Callback
    {
        private SeekBarVolumizer mCurrent;
        
        @Override
        public void onSampleStarting(final SeekBarVolumizer mCurrent) {
            if (this.mCurrent != null && this.mCurrent != mCurrent) {
                this.mCurrent.stopSample();
            }
            this.mCurrent = mCurrent;
            if (this.mCurrent != null) {
                SoundSettings.this.mHandler.removeMessages(1);
                SoundSettings.this.mHandler.sendEmptyMessageDelayed(1, 2000L);
            }
        }
        
        @Override
        public void onStreamValueChanged(final int n, final int n2) {
            if (this.mCurrent != null) {
                SoundSettings.this.mHandler.removeMessages(1);
                SoundSettings.this.mHandler.sendEmptyMessageDelayed(1, 2000L);
            }
        }
        
        public void stopSample() {
            if (this.mCurrent != null) {
                this.mCurrent.stopSample();
            }
        }
    }
}
