package com.android.settings.notification;

import android.service.notification.ZenModeConfig;
import java.util.Collection;
import android.icu.text.ListFormatter;
import java.util.Iterator;
import java.util.Map;
import android.app.AutomaticZenRule;
import android.app.NotificationManager;
import java.util.ArrayList;
import java.util.List;
import android.app.NotificationManager$Policy;
import android.content.Context;
import java.util.function.Predicate;

public final class _$$Lambda$ZenModeSettings$SummaryBuilder$_hUbn9epxyVxqc9qNo66a_LO5Ug implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return ZenModeSettings.SummaryBuilder.lambda$getSoundSettingSummary$0((Integer)o);
    }
}
