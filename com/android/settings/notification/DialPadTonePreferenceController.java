package com.android.settings.notification;

import com.android.settings.Utils;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class DialPadTonePreferenceController extends SettingPrefController
{
    public DialPadTonePreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(2, "dial_pad_tones", "dtmf_tone", 1, new int[0]) {
            @Override
            public boolean isApplicable(final Context context) {
                return Utils.isVoiceCapable(context);
            }
        };
    }
}
