package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.text.TextUtils;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;

public class DescriptionPreferenceController extends NotificationPreferenceController implements PreferenceControllerMixin
{
    public DescriptionPreferenceController(final Context context) {
        super(context, null);
    }
    
    @Override
    public String getPreferenceKey() {
        return "desc";
    }
    
    @Override
    public boolean isAvailable() {
        return super.isAvailable() && (this.mChannel != null || this.hasValidGroup()) && ((this.mChannel != null && !TextUtils.isEmpty((CharSequence)this.mChannel.getDescription())) || (this.hasValidGroup() && !TextUtils.isEmpty((CharSequence)this.mChannelGroup.getDescription())));
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null) {
            if (this.mChannel != null) {
                preference.setTitle(this.mChannel.getDescription());
            }
            else if (this.hasValidGroup()) {
                preference.setTitle(this.mChannelGroup.getDescription());
            }
        }
        preference.setEnabled(false);
        preference.setSelectable(false);
    }
}
