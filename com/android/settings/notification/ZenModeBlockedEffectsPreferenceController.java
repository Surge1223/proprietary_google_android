package com.android.settings.notification;

import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;

public class ZenModeBlockedEffectsPreferenceController extends AbstractZenModePreferenceController implements PreferenceControllerMixin
{
    private final ZenModeSettings.SummaryBuilder mSummaryBuilder;
    
    public ZenModeBlockedEffectsPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "zen_mode_block_effects_settings", lifecycle);
        this.mSummaryBuilder = new ZenModeSettings.SummaryBuilder(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_block_effects_settings";
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mSummaryBuilder.getBlockedEffectsSummary(this.getPolicy());
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
}
