package com.android.settings.notification;

import android.support.v7.preference.PreferenceScreen;
import android.widget.Toast;
import android.content.IntentFilter;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.widget.MasterCheckBoxPreference;
import android.support.v7.preference.PreferenceGroup;
import android.arch.lifecycle.LifecycleObserver;
import android.os.UserHandle;
import android.content.pm.ActivityInfo;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import java.util.ArrayList;
import android.util.Log;
import com.android.settingslib.RestrictedLockUtils;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.BroadcastReceiver;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.preference.Preference;
import java.util.List;
import android.content.Context;
import android.app.NotificationChannelGroup;
import android.os.Bundle;
import com.android.settings.dashboard.DashboardFragment;
import android.app.NotificationChannel;
import java.util.Comparator;

public final class _$$Lambda$NotificationSettingsBase$_zFOM6q_03lCRFkOVmbrRVoBxkk implements Comparator
{
    @Override
    public final int compare(final Object o, final Object o2) {
        return NotificationSettingsBase.lambda$new$0((NotificationChannel)o, (NotificationChannel)o2);
    }
}
