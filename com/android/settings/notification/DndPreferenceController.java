package com.android.settings.notification;

import com.android.settingslib.RestrictedSwitchPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class DndPreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public DndPreferenceController(final Context context, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bypass_dnd";
    }
    
    @Override
    public boolean isAvailable() {
        return super.isAvailable() && this.mChannel != null;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            this.mChannel.setBypassDnd((boolean)o);
            this.mChannel.lockFields(1);
            this.saveChannel();
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mChannel != null) {
            final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
            restrictedSwitchPreference.setDisabledByAdmin(this.mAdmin);
            restrictedSwitchPreference.setEnabled(this.isChannelConfigurable() && !restrictedSwitchPreference.isDisabledByAdmin());
            restrictedSwitchPreference.setChecked(this.mChannel.canBypassDnd());
        }
    }
}
