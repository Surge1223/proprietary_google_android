package com.android.settings.notification;

import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.RestrictedLockUtils;
import android.util.Log;
import java.util.ArrayList;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.ComponentName;
import android.app.admin.DevicePolicyManager;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.Context;
import com.android.settings.RestrictedListPreference;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class LockScreenNotificationPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private RestrictedListPreference mLockscreen;
    private RestrictedListPreference mLockscreenProfile;
    private int mLockscreenSelectedValue;
    private int mLockscreenSelectedValueProfile;
    private final int mProfileUserId;
    private final boolean mSecure;
    private final boolean mSecureProfile;
    private final String mSettingKey;
    private SettingObserver mSettingObserver;
    private final String mWorkSettingCategoryKey;
    private final String mWorkSettingKey;
    
    public LockScreenNotificationPreferenceController(final Context context) {
        this(context, null, null, null);
    }
    
    public LockScreenNotificationPreferenceController(final Context context, final String mSettingKey, final String mWorkSettingCategoryKey, final String mWorkSettingKey) {
        super(context);
        this.mSettingKey = mSettingKey;
        this.mWorkSettingCategoryKey = mWorkSettingCategoryKey;
        this.mWorkSettingKey = mWorkSettingKey;
        this.mProfileUserId = Utils.getManagedProfileId(UserManager.get(context), UserHandle.myUserId());
        final LockPatternUtils lockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
        this.mSecure = lockPatternUtils.isSecure(UserHandle.myUserId());
        this.mSecureProfile = (this.mProfileUserId != -10000 && lockPatternUtils.isSecure(this.mProfileUserId));
    }
    
    private boolean adminAllowsUnredactedNotifications(final int n) {
        return (((DevicePolicyManager)this.mContext.getSystemService((Class)DevicePolicyManager.class)).getKeyguardDisabledFeatures((ComponentName)null, n) & 0x8) == 0x0;
    }
    
    private static boolean getAllowPrivateNotifications(final Context context, final int n) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getIntForUser(contentResolver, "lock_screen_allow_private_notifications", 0, n) != 0) {
            b = true;
        }
        return b;
    }
    
    private static boolean getLockscreenNotificationsEnabled(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, "lock_screen_show_notifications", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    public static int getSummaryResource(final Context context) {
        final boolean lockscreenNotificationsEnabled = getLockscreenNotificationsEnabled(context);
        final boolean b = !FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context).isSecure(UserHandle.myUserId()) || getAllowPrivateNotifications(context, UserHandle.myUserId());
        int n;
        if (!lockscreenNotificationsEnabled) {
            n = 2131888089;
        }
        else if (b) {
            n = 2131888092;
        }
        else {
            n = 2131888090;
        }
        return n;
    }
    
    private void initLockScreenNotificationPrefDisplay() {
        final ArrayList<String> list = new ArrayList<String>();
        final ArrayList<String> list2 = new ArrayList<String>();
        final String string = this.mContext.getString(2131888092);
        final String string2 = Integer.toString(2131888092);
        list.add(string);
        list2.add(string2);
        this.setRestrictedIfNotificationFeaturesDisabled(string, string2, 12);
        if (this.mSecure) {
            final String string3 = this.mContext.getString(2131888090);
            final String string4 = Integer.toString(2131888090);
            list.add(string3);
            list2.add(string4);
            this.setRestrictedIfNotificationFeaturesDisabled(string3, string4, 4);
        }
        list.add(this.mContext.getString(2131888089));
        list2.add(Integer.toString(2131888089));
        this.mLockscreen.setEntries(list.toArray(new CharSequence[list.size()]));
        this.mLockscreen.setEntryValues(list2.toArray(new CharSequence[list2.size()]));
        this.updateLockscreenNotifications();
        if (this.mLockscreen.getEntries().length > 1) {
            this.mLockscreen.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        }
        else {
            this.mLockscreen.setEnabled(false);
        }
    }
    
    private void initLockscreenNotificationPrefForProfile() {
        if (this.mLockscreenProfile == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Preference not found: ");
            sb.append(this.mWorkSettingKey);
            Log.i("LockScreenNotifPref", sb.toString());
            return;
        }
        final ArrayList<String> list = new ArrayList<String>();
        final ArrayList<String> list2 = new ArrayList<String>();
        final String string = this.mContext.getString(2131888093);
        final String string2 = Integer.toString(2131888093);
        list.add(string);
        list2.add(string2);
        this.setRestrictedIfNotificationFeaturesDisabled(string, string2, 12);
        if (this.mSecureProfile) {
            final String string3 = this.mContext.getString(2131888091);
            final String string4 = Integer.toString(2131888091);
            list.add(string3);
            list2.add(string4);
            this.setRestrictedIfNotificationFeaturesDisabled(string3, string4, 4);
        }
        this.mLockscreenProfile.setEntries(list.toArray(new CharSequence[list.size()]));
        this.mLockscreenProfile.setEntryValues(list2.toArray(new CharSequence[list2.size()]));
        this.updateLockscreenNotificationsForProfile();
        if (this.mLockscreenProfile.getEntries().length > 1) {
            this.mLockscreenProfile.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        }
        else {
            this.mLockscreenProfile.setEnabled(false);
        }
    }
    
    private void setRestrictedIfNotificationFeaturesDisabled(final CharSequence charSequence, final CharSequence charSequence2, final int n) {
        final RestrictedLockUtils.EnforcedAdmin checkIfKeyguardFeaturesDisabled = RestrictedLockUtils.checkIfKeyguardFeaturesDisabled(this.mContext, n, UserHandle.myUserId());
        if (checkIfKeyguardFeaturesDisabled != null && this.mLockscreen != null) {
            this.mLockscreen.addRestrictedItem(new RestrictedListPreference.RestrictedItem(charSequence, charSequence2, checkIfKeyguardFeaturesDisabled));
        }
        if (this.mProfileUserId != -10000) {
            final RestrictedLockUtils.EnforcedAdmin checkIfKeyguardFeaturesDisabled2 = RestrictedLockUtils.checkIfKeyguardFeaturesDisabled(this.mContext, n, this.mProfileUserId);
            if (checkIfKeyguardFeaturesDisabled2 != null && this.mLockscreenProfile != null) {
                this.mLockscreenProfile.addRestrictedItem(new RestrictedListPreference.RestrictedItem(charSequence, charSequence2, checkIfKeyguardFeaturesDisabled2));
            }
        }
    }
    
    private void updateLockscreenNotifications() {
        if (this.mLockscreen == null) {
            return;
        }
        this.mLockscreenSelectedValue = getSummaryResource(this.mContext);
        this.mLockscreen.setSummary("%s");
        this.mLockscreen.setValue(Integer.toString(this.mLockscreenSelectedValue));
    }
    
    private void updateLockscreenNotificationsForProfile() {
        if (this.mProfileUserId == -10000) {
            return;
        }
        if (this.mLockscreenProfile == null) {
            return;
        }
        final boolean b = this.adminAllowsUnredactedNotifications(this.mProfileUserId) && (!this.mSecureProfile || getAllowPrivateNotifications(this.mContext, this.mProfileUserId));
        this.mLockscreenProfile.setSummary("%s");
        int mLockscreenSelectedValueProfile;
        if (b) {
            mLockscreenSelectedValueProfile = 2131888093;
        }
        else {
            mLockscreenSelectedValueProfile = 2131888091;
        }
        this.mLockscreenSelectedValueProfile = mLockscreenSelectedValueProfile;
        this.mLockscreenProfile.setValue(Integer.toString(this.mLockscreenSelectedValueProfile));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mLockscreen = (RestrictedListPreference)preferenceScreen.findPreference(this.mSettingKey);
        if (this.mLockscreen == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Preference not found: ");
            sb.append(this.mSettingKey);
            Log.i("LockScreenNotifPref", sb.toString());
            return;
        }
        if (this.mProfileUserId != -10000) {
            (this.mLockscreenProfile = (RestrictedListPreference)preferenceScreen.findPreference(this.mWorkSettingKey)).setRequiresActiveUnlockedProfile(true);
            this.mLockscreenProfile.setProfileUserId(this.mProfileUserId);
        }
        else {
            this.setVisible(preferenceScreen, this.mWorkSettingKey, false);
            this.setVisible(preferenceScreen, this.mWorkSettingCategoryKey, false);
        }
        this.mSettingObserver = new SettingObserver();
        this.initLockScreenNotificationPrefDisplay();
        this.initLockscreenNotificationPrefForProfile();
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return false;
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), false);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final String key = preference.getKey();
        final boolean equals = TextUtils.equals((CharSequence)this.mWorkSettingKey, (CharSequence)key);
        final boolean b = false;
        int n = 0;
        if (equals) {
            final int int1 = Integer.parseInt((String)o);
            if (int1 == this.mLockscreenSelectedValueProfile) {
                return false;
            }
            final boolean b2 = int1 == 2131888093;
            final ContentResolver contentResolver = this.mContext.getContentResolver();
            if (b2) {
                n = 1;
            }
            Settings.Secure.putIntForUser(contentResolver, "lock_screen_allow_private_notifications", n, this.mProfileUserId);
            this.mLockscreenSelectedValueProfile = int1;
            return true;
        }
        else {
            if (!TextUtils.equals((CharSequence)this.mSettingKey, (CharSequence)key)) {
                return false;
            }
            final int int2 = Integer.parseInt((String)o);
            if (int2 == this.mLockscreenSelectedValue) {
                return false;
            }
            final boolean b3 = int2 != 2131888089;
            final boolean b4 = int2 == 2131888092;
            final ContentResolver contentResolver2 = this.mContext.getContentResolver();
            int n2;
            if (b4) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            Settings.Secure.putInt(contentResolver2, "lock_screen_allow_private_notifications", n2);
            final ContentResolver contentResolver3 = this.mContext.getContentResolver();
            int n3;
            if (b3) {
                n3 = 1;
            }
            else {
                n3 = (b ? 1 : 0);
            }
            Settings.Secure.putInt(contentResolver3, "lock_screen_show_notifications", n3);
            this.mLockscreenSelectedValue = int2;
            return true;
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), true);
        }
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri LOCK_SCREEN_PRIVATE_URI;
        private final Uri LOCK_SCREEN_SHOW_URI;
        
        public SettingObserver() {
            super(new Handler());
            this.LOCK_SCREEN_PRIVATE_URI = Settings.Secure.getUriFor("lock_screen_allow_private_notifications");
            this.LOCK_SCREEN_SHOW_URI = Settings.Secure.getUriFor("lock_screen_show_notifications");
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.LOCK_SCREEN_PRIVATE_URI.equals((Object)uri) || this.LOCK_SCREEN_SHOW_URI.equals((Object)uri)) {
                LockScreenNotificationPreferenceController.this.updateLockscreenNotifications();
                if (LockScreenNotificationPreferenceController.this.mProfileUserId != -10000) {
                    LockScreenNotificationPreferenceController.this.updateLockscreenNotificationsForProfile();
                }
            }
        }
        
        public void register(final ContentResolver contentResolver, final boolean b) {
            if (b) {
                contentResolver.registerContentObserver(this.LOCK_SCREEN_PRIVATE_URI, false, (ContentObserver)this);
                contentResolver.registerContentObserver(this.LOCK_SCREEN_SHOW_URI, false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
