package com.android.settings.notification;

import android.text.TextUtils;
import android.content.Context;

public class MediaVolumePreferenceController extends VolumeSeekBarPreferenceController
{
    private static final String KEY_MEDIA_VOLUME = "media_volume";
    
    public MediaVolumePreferenceController(final Context context) {
        super(context, "media_volume");
    }
    
    public int getAudioStream() {
        return 3;
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034151)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    public int getMuteIcon() {
        return 2131231061;
    }
    
    @Override
    public String getPreferenceKey() {
        return "media_volume";
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"media_volume");
    }
}
