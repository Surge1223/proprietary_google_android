package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.app.NotificationManager$Policy;
import com.android.settings.core.SubSettingLauncher;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class ZenModeVisEffectsCustomPreferenceController extends AbstractZenModePreferenceController
{
    private final String KEY;
    private ZenCustomRadioButtonPreference mPreference;
    
    public ZenModeVisEffectsCustomPreferenceController(final Context context, final Lifecycle lifecycle, final String key) {
        super(context, key, lifecycle);
        this.KEY = key;
    }
    
    private void launchCustomSettings() {
        this.select();
        new SubSettingLauncher(this.mContext).setDestination(ZenModeBlockedEffectsSettings.class.getName()).setTitle(2131890394).setSourceMetricsCategory(1400).launch();
    }
    
    protected boolean areCustomOptionsSelected() {
        final boolean allVisualEffectsSuppressed = NotificationManager$Policy.areAllVisualEffectsSuppressed(this.mBackend.mPolicy.suppressedVisualEffects);
        final int suppressedVisualEffects = this.mBackend.mPolicy.suppressedVisualEffects;
        final boolean b = false;
        final boolean b2 = suppressedVisualEffects == 0;
        boolean b3 = b;
        if (!allVisualEffectsSuppressed) {
            b3 = b;
            if (!b2) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = (ZenCustomRadioButtonPreference)preferenceScreen.findPreference(this.KEY)).setOnGearClickListener((ZenCustomRadioButtonPreference.OnGearClickListener)new _$$Lambda$ZenModeVisEffectsCustomPreferenceController$hYHNs4_TKsGpjPSCluD3oYAyplI(this));
        this.mPreference.setOnRadioButtonClickListener((ZenCustomRadioButtonPreference.OnRadioButtonClickListener)new _$$Lambda$ZenModeVisEffectsCustomPreferenceController$anmhCczZGnQRUAoXVehKNMc66b4(this));
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    protected void select() {
        this.mMetricsFeatureProvider.action(this.mContext, 1399, true);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.mPreference.setChecked(this.areCustomOptionsSelected());
    }
}
