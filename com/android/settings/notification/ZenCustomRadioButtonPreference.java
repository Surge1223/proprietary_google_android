package com.android.settings.notification;

import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.RadioButton;
import android.view.View.OnClickListener;
import com.android.settingslib.TwoTargetPreference;

public class ZenCustomRadioButtonPreference extends TwoTargetPreference implements View.OnClickListener
{
    private RadioButton mButton;
    private boolean mChecked;
    private OnGearClickListener mOnGearClickListener;
    private OnRadioButtonClickListener mOnRadioButtonClickListener;
    
    public ZenCustomRadioButtonPreference(final Context context) {
        super(context);
        this.setLayoutResource(2131558675);
    }
    
    public ZenCustomRadioButtonPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setLayoutResource(2131558675);
    }
    
    public ZenCustomRadioButtonPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.setLayoutResource(2131558675);
    }
    
    public ZenCustomRadioButtonPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setLayoutResource(2131558675);
    }
    
    @Override
    protected int getSecondTargetResId() {
        return 2131558679;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131361983);
        if (viewById != null) {
            viewById.setOnClickListener((View.OnClickListener)this);
        }
        this.mButton = (RadioButton)preferenceViewHolder.findViewById(16908289);
        if (this.mButton != null) {
            this.mButton.setChecked(this.mChecked);
        }
        final View viewById2 = preferenceViewHolder.findViewById(16908312);
        final View viewById3 = preferenceViewHolder.findViewById(R.id.two_target_divider);
        if (this.mOnGearClickListener != null) {
            viewById3.setVisibility(0);
            viewById2.setVisibility(0);
            viewById2.setOnClickListener((View.OnClickListener)this);
        }
        else {
            viewById3.setVisibility(8);
            viewById2.setVisibility(8);
            viewById2.setOnClickListener((View.OnClickListener)null);
        }
    }
    
    public void onClick() {
        if (this.mOnRadioButtonClickListener != null) {
            this.mOnRadioButtonClickListener.onRadioButtonClick(this);
        }
    }
    
    public void onClick(final View view) {
        if (view.getId() == 16908312) {
            if (this.mOnGearClickListener != null) {
                this.mOnGearClickListener.onGearClick(this);
            }
        }
        else if (view.getId() == 2131361983 && this.mOnRadioButtonClickListener != null) {
            this.mOnRadioButtonClickListener.onRadioButtonClick(this);
        }
    }
    
    public void setChecked(final boolean b) {
        this.mChecked = b;
        if (this.mButton != null) {
            this.mButton.setChecked(b);
        }
    }
    
    public void setOnGearClickListener(final OnGearClickListener mOnGearClickListener) {
        this.mOnGearClickListener = mOnGearClickListener;
        this.notifyChanged();
    }
    
    public void setOnRadioButtonClickListener(final OnRadioButtonClickListener mOnRadioButtonClickListener) {
        this.mOnRadioButtonClickListener = mOnRadioButtonClickListener;
        this.notifyChanged();
    }
    
    public interface OnGearClickListener
    {
        void onGearClick(final ZenCustomRadioButtonPreference p0);
    }
    
    public interface OnRadioButtonClickListener
    {
        void onRadioButtonClick(final ZenCustomRadioButtonPreference p0);
    }
}
