package com.android.settings.notification;

import android.net.Uri;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.AbstractPreferenceController;
import android.widget.Toast;
import android.util.Log;
import android.app.NotificationManager;
import android.app.AutomaticZenRule;
import android.content.Context;

public abstract class ZenModeRuleSettingsBase extends ZenModeSettingsBase
{
    protected static final boolean DEBUG;
    protected Context mContext;
    protected boolean mDisableListeners;
    protected ZenAutomaticRuleHeaderPreferenceController mHeader;
    protected String mId;
    protected AutomaticZenRule mRule;
    protected ZenAutomaticRuleSwitchPreferenceController mSwitch;
    
    static {
        DEBUG = ZenModeSettingsBase.DEBUG;
    }
    
    private AutomaticZenRule getZenRule() {
        return NotificationManager.from(this.mContext).getAutomaticZenRule(this.mId);
    }
    
    private boolean refreshRuleOrFinish() {
        this.mRule = this.getZenRule();
        if (ZenModeRuleSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("mRule=");
            sb.append(this.mRule);
            Log.d("ZenModeSettings", sb.toString());
        }
        if (!this.setRule(this.mRule)) {
            this.toastAndFinish();
            return true;
        }
        return false;
    }
    
    private void toastAndFinish() {
        Toast.makeText(this.mContext, 2131890354, 0).show();
        this.getActivity().finish();
    }
    
    private void updateControls() {
        this.mDisableListeners = true;
        this.updateControlsInternal();
        this.updateHeader();
        this.mDisableListeners = false;
    }
    
    private void updatePreference(final AbstractPreferenceController abstractPreferenceController) {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (!abstractPreferenceController.isAvailable()) {
            return;
        }
        final String preferenceKey = abstractPreferenceController.getPreferenceKey();
        final Preference preference = preferenceScreen.findPreference(preferenceKey);
        if (preference == null) {
            Log.d("ZenModeSettings", String.format("Cannot find preference with key %s in Controller %s", preferenceKey, abstractPreferenceController.getClass().getSimpleName()));
            return;
        }
        abstractPreferenceController.updateState(preference);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887777;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.mContext = (Context)this.getActivity();
        final Intent intent = this.getActivity().getIntent();
        if (ZenModeRuleSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onCreate getIntent()=");
            sb.append(intent);
            Log.d("ZenModeSettings", sb.toString());
        }
        if (intent == null) {
            Log.w("ZenModeSettings", "No intent");
            this.toastAndFinish();
            return;
        }
        this.mId = intent.getStringExtra("android.service.notification.extra.RULE_ID");
        if (this.mId == null) {
            Log.w("ZenModeSettings", "rule id is null");
            this.toastAndFinish();
            return;
        }
        if (ZenModeRuleSettingsBase.DEBUG) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("mId=");
            sb2.append(this.mId);
            Log.d("ZenModeSettings", sb2.toString());
        }
        if (this.refreshRuleOrFinish()) {
            return;
        }
        super.onCreate(bundle);
        this.onCreateInternal();
    }
    
    protected abstract void onCreateInternal();
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.isUiRestricted()) {
            return;
        }
        this.updateControls();
    }
    
    @Override
    protected void onZenModeConfigChanged() {
        super.onZenModeConfigChanged();
        if (!this.refreshRuleOrFinish()) {
            this.updateControls();
        }
    }
    
    protected abstract boolean setRule(final AutomaticZenRule p0);
    
    protected abstract void updateControlsInternal();
    
    protected void updateHeader() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        this.mSwitch.onResume(this.mRule, this.mId);
        this.mSwitch.displayPreference(preferenceScreen);
        this.updatePreference(this.mSwitch);
        this.mHeader.onResume(this.mRule, this.mId);
        this.mHeader.displayPreference(preferenceScreen);
        this.updatePreference(this.mHeader);
    }
    
    protected void updateRule(final Uri conditionId) {
        this.mRule.setConditionId(conditionId);
        this.mBackend.setZenRule(this.mId, this.mRule);
    }
}
