package com.android.settings.notification;

import com.android.internal.app.AlertController;
import android.content.Context;
import com.android.internal.app.AlertController$AlertParams;
import android.os.Bundle;
import com.android.internal.app.AlertActivity;
import android.view.accessibility.AccessibilityEvent;
import android.content.pm.PackageManager;
import android.util.Slog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$NotificationAccessConfirmationActivity$hd7i7CSD_dVpjvK__hXE8eDM2I0 implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        NotificationAccessConfirmationActivity.lambda$onCreate$1(this.f$0, dialogInterface, n);
    }
}
