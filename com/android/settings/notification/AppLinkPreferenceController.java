package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;

public class AppLinkPreferenceController extends NotificationPreferenceController implements PreferenceControllerMixin
{
    public AppLinkPreferenceController(final Context context) {
        super(context, null);
    }
    
    @Override
    public String getPreferenceKey() {
        return "app_link";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        boolean b = false;
        if (!available) {
            return false;
        }
        if (this.mAppRow.settingsIntent != null) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null) {
            preference.setIntent(this.mAppRow.settingsIntent);
        }
    }
}
