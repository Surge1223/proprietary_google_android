package com.android.settings.notification;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.app.FragmentManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class ZenModeDurationPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceClickListener, PreferenceControllerMixin
{
    private FragmentManager mFragment;
    
    public ZenModeDurationPreferenceController(final Context context, final Lifecycle lifecycle, final FragmentManager mFragment) {
        super(context, "zen_mode_duration_settings", lifecycle);
        this.mFragment = mFragment;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        preferenceScreen.findPreference("zen_mode_duration_settings").setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_duration_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        new SettingsZenDurationDialog().show(this.mFragment, "ZenModeDurationDialog");
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final int zenDuration = this.getZenDuration();
        String summary;
        if (zenDuration < 0) {
            summary = this.mContext.getString(2131890302);
        }
        else if (zenDuration == 0) {
            summary = this.mContext.getString(2131890303);
        }
        else if (zenDuration >= 60) {
            final int n = zenDuration / 60;
            summary = this.mContext.getResources().getQuantityString(2131755075, n, new Object[] { n });
        }
        else {
            summary = this.mContext.getResources().getString(2131890304, new Object[] { zenDuration });
        }
        preference.setSummary(summary);
    }
}
