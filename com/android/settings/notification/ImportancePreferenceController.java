package com.android.settings.notification;

import com.android.settings.RestrictedListPreference;
import android.media.RingtoneManager;
import android.app.NotificationChannel;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class ImportancePreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private NotificationSettingsBase.ImportanceListener mImportanceListener;
    
    public ImportancePreferenceController(final Context context, final NotificationSettingsBase.ImportanceListener mImportanceListener, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
        this.mImportanceListener = mImportanceListener;
    }
    
    protected String getImportanceSummary(final NotificationChannel notificationChannel) {
        final int importance = notificationChannel.getImportance();
        String s = null;
        if (importance != -1000) {
            switch (importance) {
                default: {
                    return "";
                }
                case 4:
                case 5: {
                    if (SoundPreferenceController.hasValidSound(notificationChannel)) {
                        s = this.mContext.getString(2131888447);
                        break;
                    }
                    s = this.mContext.getString(2131888448);
                    break;
                }
                case 3: {
                    if (SoundPreferenceController.hasValidSound(notificationChannel)) {
                        s = this.mContext.getString(2131888444);
                        break;
                    }
                    s = this.mContext.getString(2131888450);
                    break;
                }
                case 2: {
                    s = this.mContext.getString(2131888450);
                    break;
                }
                case 1: {
                    s = this.mContext.getString(2131888452);
                    break;
                }
            }
        }
        else {
            s = this.mContext.getString(2131888455);
        }
        return s;
    }
    
    @Override
    public String getPreferenceKey() {
        return "importance";
    }
    
    @Override
    public boolean isAvailable() {
        return super.isAvailable() && this.mChannel != null && (this.isDefaultChannel() ^ true);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            final int int1 = Integer.parseInt((String)o);
            if (this.mChannel.getImportance() < 3 && !SoundPreferenceController.hasValidSound(this.mChannel) && int1 >= 3) {
                this.mChannel.setSound(RingtoneManager.getDefaultUri(2), this.mChannel.getAudioAttributes());
                this.mChannel.lockFields(32);
            }
            this.mChannel.setImportance(int1);
            this.mChannel.lockFields(4);
            this.saveChannel();
            this.mImportanceListener.onImportanceChanged();
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null && this.mChannel != null) {
            preference.setEnabled(this.mAdmin == null && this.isChannelConfigurable());
            preference.setSummary(this.getImportanceSummary(this.mChannel));
            final CharSequence[] entries = new CharSequence[4];
            final CharSequence[] entryValues = new CharSequence[4];
            int n = 0;
            for (int i = 4; i >= 1; --i) {
                entries[n] = this.getImportanceSummary(new NotificationChannel("", (CharSequence)"", i));
                entryValues[n] = String.valueOf(i);
                ++n;
            }
            final RestrictedListPreference restrictedListPreference = (RestrictedListPreference)preference;
            restrictedListPreference.setEntries(entries);
            restrictedListPreference.setEntryValues(entryValues);
            restrictedListPreference.setValue(String.valueOf(this.mChannel.getImportance()));
        }
    }
}
