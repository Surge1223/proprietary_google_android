package com.android.settings.notification;

import android.service.notification.ZenModeConfig;
import java.util.Collection;
import android.icu.text.ListFormatter;
import java.util.Iterator;
import java.util.Map;
import android.app.AutomaticZenRule;
import android.app.NotificationManager;
import java.util.function.Predicate;
import android.app.NotificationManager$Policy;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.app.FragmentManager;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;

public class ZenModeSettings extends ZenModeSettingsBase
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("zen_mode_duration_settings");
                nonIndexableKeys.add("zen_mode_settings_button_container");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082884;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle, final FragmentManager fragmentManager) {
        final ArrayList<ZenModeBehaviorSoundPreferenceController> list = (ArrayList<ZenModeBehaviorSoundPreferenceController>)new ArrayList<ZenModeSettingsFooterPreferenceController>();
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeBehaviorMsgEventReminderPreferenceController(context, lifecycle));
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeBehaviorSoundPreferenceController(context, lifecycle));
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeBehaviorCallsPreferenceController(context, lifecycle));
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeBlockedEffectsPreferenceController(context, lifecycle));
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeDurationPreferenceController(context, lifecycle, fragmentManager));
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeAutomationPreferenceController(context));
        list.add((ZenModeSettingsFooterPreferenceController)new ZenModeButtonPreferenceController(context, lifecycle, fragmentManager));
        list.add(new ZenModeSettingsFooterPreferenceController(context, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle(), this.getFragmentManager());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887777;
    }
    
    @Override
    public int getMetricsCategory() {
        return 76;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082884;
    }
    
    @Override
    public void onResume() {
        super.onResume();
    }
    
    public static class SummaryBuilder
    {
        private static final int[] ALL_PRIORITY_CATEGORIES;
        private Context mContext;
        
        static {
            ALL_PRIORITY_CATEGORIES = new int[] { 32, 64, 128, 4, 2, 1, 8, 16 };
        }
        
        public SummaryBuilder(final Context mContext) {
            this.mContext = mContext;
        }
        
        private List<String> getEnabledCategories(final NotificationManager$Policy notificationManager$Policy, final Predicate<Integer> predicate) {
            final ArrayList<String> list = new ArrayList<String>();
            for (final int n : SummaryBuilder.ALL_PRIORITY_CATEGORIES) {
                if (predicate.test(n) && this.isCategoryEnabled(notificationManager$Policy, n)) {
                    if (n == 32) {
                        list.add(this.mContext.getString(2131890254));
                    }
                    else if (n == 64) {
                        list.add(this.mContext.getString(2131890323));
                    }
                    else if (n == 128) {
                        list.add(this.mContext.getString(2131890389));
                    }
                    else if (n == 4) {
                        if (notificationManager$Policy.priorityMessageSenders == 0) {
                            list.add(this.mContext.getString(2131890256));
                        }
                        else {
                            list.add(this.mContext.getString(2131890369));
                        }
                    }
                    else if (n == 2) {
                        list.add(this.mContext.getString(2131890317));
                    }
                    else if (n == 1) {
                        list.add(this.mContext.getString(2131890332));
                    }
                    else if (n == 8) {
                        if (notificationManager$Policy.priorityCallSenders == 0) {
                            list.add(this.mContext.getString(2131890255));
                        }
                        else if (notificationManager$Policy.priorityCallSenders == 1) {
                            list.add(this.mContext.getString(2131890296));
                        }
                        else {
                            list.add(this.mContext.getString(2131890382));
                        }
                    }
                    else if (n == 16 && !list.contains(this.mContext.getString(2131890255))) {
                        list.add(this.mContext.getString(2131890334));
                    }
                }
            }
            return list;
        }
        
        private boolean isCategoryEnabled(final NotificationManager$Policy notificationManager$Policy, final int n) {
            return (notificationManager$Policy.priorityCategories & n) != 0x0;
        }
        
        String getAutomaticRulesSummary() {
            final int enabledAutomaticRulesCount = this.getEnabledAutomaticRulesCount();
            String s;
            if (enabledAutomaticRulesCount == 0) {
                s = this.mContext.getString(2131890375);
            }
            else {
                s = this.mContext.getResources().getQuantityString(2131755076, enabledAutomaticRulesCount, new Object[] { enabledAutomaticRulesCount });
            }
            return s;
        }
        
        String getBlockedEffectsSummary(final NotificationManager$Policy notificationManager$Policy) {
            if (notificationManager$Policy.suppressedVisualEffects == 0) {
                return this.mContext.getResources().getString(2131890348);
            }
            if (NotificationManager$Policy.areAllVisualEffectsSuppressed(notificationManager$Policy.suppressedVisualEffects)) {
                return this.mContext.getResources().getString(2131890347);
            }
            return this.mContext.getResources().getString(2131890346);
        }
        
        String getCallsSettingSummary(final NotificationManager$Policy notificationManager$Policy) {
            final List<String> enabledCategories = this.getEnabledCategories(notificationManager$Policy, (Predicate<Integer>)_$$Lambda$ZenModeSettings$SummaryBuilder$_Gea8GbwXN997GXaupRdGPPi1FA.INSTANCE);
            final int size = enabledCategories.size();
            if (size == 0) {
                return this.mContext.getString(2131890326);
            }
            if (size == 1) {
                return this.mContext.getString(2131890292, new Object[] { enabledCategories.get(0).toLowerCase() });
            }
            return this.mContext.getString(2131890293, new Object[] { enabledCategories.get(0).toLowerCase(), enabledCategories.get(1).toLowerCase() });
        }
        
        int getEnabledAutomaticRulesCount() {
            int n = 0;
            int n2 = 0;
            final Map automaticZenRules = NotificationManager.from(this.mContext).getAutomaticZenRules();
            if (automaticZenRules != null) {
                final Iterator<Map.Entry<K, AutomaticZenRule>> iterator = automaticZenRules.entrySet().iterator();
                while (true) {
                    n = n2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final AutomaticZenRule automaticZenRule = iterator.next().getValue();
                    int n3 = n2;
                    if (automaticZenRule != null) {
                        n3 = n2;
                        if (automaticZenRule.isEnabled()) {
                            n3 = n2 + 1;
                        }
                    }
                    n2 = n3;
                }
            }
            return n;
        }
        
        String getMsgEventReminderSettingSummary(final NotificationManager$Policy notificationManager$Policy) {
            final List<String> enabledCategories = this.getEnabledCategories(notificationManager$Policy, (Predicate<Integer>)_$$Lambda$ZenModeSettings$SummaryBuilder$Ydm8DmhkL6wV0O584_hfIH59p1A.INSTANCE);
            final int size = enabledCategories.size();
            if (size == 0) {
                return this.mContext.getString(2131890326);
            }
            if (size == 1) {
                return enabledCategories.get(0);
            }
            if (size == 2) {
                return this.mContext.getString(2131887917, new Object[] { enabledCategories.get(0), enabledCategories.get(1).toLowerCase() });
            }
            if (size == 3) {
                final ArrayList<String> list = new ArrayList<String>();
                list.add(enabledCategories.get(0));
                list.add(enabledCategories.get(1).toLowerCase());
                list.add(enabledCategories.get(2).toLowerCase());
                return ListFormatter.getInstance().format((Collection)list);
            }
            final ArrayList<String> list2 = new ArrayList<String>();
            list2.add(enabledCategories.get(0));
            list2.add(enabledCategories.get(1).toLowerCase());
            list2.add(enabledCategories.get(2).toLowerCase());
            list2.add(this.mContext.getString(2131890330));
            return ListFormatter.getInstance().format((Collection)list2);
        }
        
        String getSoundSettingSummary(final NotificationManager$Policy notificationManager$Policy) {
            final List<String> enabledCategories = this.getEnabledCategories(notificationManager$Policy, (Predicate<Integer>)_$$Lambda$ZenModeSettings$SummaryBuilder$_hUbn9epxyVxqc9qNo66a_LO5Ug.INSTANCE);
            final int size = enabledCategories.size();
            if (size == 0) {
                return this.mContext.getString(2131890416);
            }
            if (size == 1) {
                return this.mContext.getString(2131890420, new Object[] { enabledCategories.get(0).toLowerCase() });
            }
            if (size == 2) {
                return this.mContext.getString(2131890423, new Object[] { enabledCategories.get(0).toLowerCase(), enabledCategories.get(1).toLowerCase() });
            }
            if (size == 3) {
                return this.mContext.getString(2131890421, new Object[] { enabledCategories.get(0).toLowerCase(), enabledCategories.get(1).toLowerCase(), enabledCategories.get(2).toLowerCase() });
            }
            return this.mContext.getString(2131890419);
        }
        
        String getSoundSummary() {
            if (NotificationManager.from(this.mContext).getZenMode() != 0) {
                final String description = ZenModeConfig.getDescription(this.mContext, true, NotificationManager.from(this.mContext).getZenModeConfig(), false);
                if (description == null) {
                    return this.mContext.getString(2131890380);
                }
                return this.mContext.getString(2131890381, new Object[] { description });
            }
            else {
                final int enabledAutomaticRulesCount = this.getEnabledAutomaticRulesCount();
                if (enabledAutomaticRulesCount > 0) {
                    return this.mContext.getString(2131890379, new Object[] { this.mContext.getResources().getQuantityString(2131755077, enabledAutomaticRulesCount, new Object[] { enabledAutomaticRulesCount }) });
                }
                return this.mContext.getString(2131890378);
            }
        }
    }
}
