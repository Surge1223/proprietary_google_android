package com.android.settings.notification;

import android.content.DialogInterface;
import android.widget.CompoundButton;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.CheckBox;
import android.app.Dialog;
import android.provider.Settings;
import com.android.settings.Utils;
import android.os.UserManager;
import android.widget.ListAdapter;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.RestrictedListPreference;

public class NotificationLockscreenPreference extends RestrictedListPreference
{
    private RestrictedLockUtils.EnforcedAdmin mAdminRestrictingRemoteInput;
    private boolean mAllowRemoteInput;
    private Listener mListener;
    private boolean mRemoteInputCheckBoxEnabled;
    private boolean mShowRemoteInput;
    private int mUserId;
    
    public NotificationLockscreenPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRemoteInputCheckBoxEnabled = true;
        this.mUserId = UserHandle.myUserId();
    }
    
    private int checkboxVisibilityForSelectedIndex(int n, final boolean b) {
        if (n == 1 && b && this.mRemoteInputCheckBoxEnabled) {
            n = 0;
        }
        else {
            n = 8;
        }
        return n;
    }
    
    @Override
    protected ListAdapter createListAdapter() {
        return (ListAdapter)new RestrictedArrayAdapter(this.getContext(), this.getEntries(), -1);
    }
    
    @Override
    protected boolean isAutoClosePreference() {
        return false;
    }
    
    @Override
    protected void onClick() {
        final Context context = this.getContext();
        if (!Utils.startQuietModeDialogIfNecessary(context, UserManager.get(context), this.mUserId)) {
            super.onClick();
        }
    }
    
    @Override
    protected void onDialogClosed(final boolean b) {
        super.onDialogClosed(b);
        Settings.Secure.putInt(this.getContext().getContentResolver(), "lock_screen_allow_remote_input", (int)(this.mAllowRemoteInput ? 1 : 0));
    }
    
    @Override
    protected void onDialogCreated(final Dialog dialog) {
        super.onDialogCreated(dialog);
        dialog.create();
        final CheckBox checkBox = (CheckBox)dialog.findViewById(2131362349);
        final boolean mAllowRemoteInput = this.mAllowRemoteInput;
        boolean enabled = true;
        checkBox.setChecked(mAllowRemoteInput ^ true);
        checkBox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this.mListener);
        if (this.mAdminRestrictingRemoteInput != null) {
            enabled = false;
        }
        checkBox.setEnabled(enabled);
        final View viewById = dialog.findViewById(2131362527);
        int visibility;
        if (this.mAdminRestrictingRemoteInput == null) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        viewById.setVisibility(visibility);
        if (this.mAdminRestrictingRemoteInput != null) {
            checkBox.setClickable(false);
            dialog.findViewById(16908820).setOnClickListener((View.OnClickListener)this.mListener);
        }
    }
    
    @Override
    protected void onDialogStateRestored(final Dialog dialog, final Bundle bundle) {
        super.onDialogStateRestored(dialog, bundle);
        final int checkedItemPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
        final View viewById = dialog.findViewById(16908820);
        viewById.setVisibility(this.checkboxVisibilityForSelectedIndex(checkedItemPosition, this.mShowRemoteInput));
        this.mListener.setView(viewById);
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        this.mListener = new Listener(dialogInterface$OnClickListener);
        alertDialog$Builder.setSingleChoiceItems(this.createListAdapter(), this.getSelectedValuePos(), (DialogInterface$OnClickListener)this.mListener);
        final int length = this.getEntryValues().length;
        final boolean b = true;
        this.mShowRemoteInput = (length == 3);
        this.mAllowRemoteInput = (Settings.Secure.getInt(this.getContext().getContentResolver(), "lock_screen_allow_remote_input", 0) != 0 && b);
        alertDialog$Builder.setView(2131558604);
    }
    
    private class Listener implements DialogInterface$OnClickListener, View.OnClickListener, CompoundButton$OnCheckedChangeListener
    {
        private final DialogInterface$OnClickListener mInner;
        private View mView;
        
        public Listener(final DialogInterface$OnClickListener mInner) {
            this.mInner = mInner;
        }
        
        public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
            NotificationLockscreenPreference.this.mAllowRemoteInput = (b ^ true);
        }
        
        public void onClick(final DialogInterface dialogInterface, int checkedItemPosition) {
            this.mInner.onClick(dialogInterface, checkedItemPosition);
            checkedItemPosition = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();
            if (this.mView != null) {
                this.mView.setVisibility(NotificationLockscreenPreference.this.checkboxVisibilityForSelectedIndex(checkedItemPosition, NotificationLockscreenPreference.this.mShowRemoteInput));
            }
        }
        
        public void onClick(final View view) {
            if (view.getId() == 16908820) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent(NotificationLockscreenPreference.this.getContext(), NotificationLockscreenPreference.this.mAdminRestrictingRemoteInput);
            }
        }
        
        public void setView(final View mView) {
            this.mView = mView;
        }
    }
}
