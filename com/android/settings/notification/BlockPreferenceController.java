package com.android.settings.notification;

import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import android.app.NotificationChannel;
import android.widget.Switch;
import android.content.Context;
import com.android.settings.widget.SwitchBar;
import com.android.settings.core.PreferenceControllerMixin;

public class BlockPreferenceController extends NotificationPreferenceController implements PreferenceControllerMixin, OnSwitchChangeListener
{
    private NotificationSettingsBase.ImportanceListener mImportanceListener;
    
    public BlockPreferenceController(final Context context, final NotificationSettingsBase.ImportanceListener mImportanceListener, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
        this.mImportanceListener = mImportanceListener;
    }
    
    @Override
    public String getPreferenceKey() {
        return "block";
    }
    
    @Override
    public boolean isAvailable() {
        final NotificationBackend.AppRow mAppRow = this.mAppRow;
        boolean b = false;
        if (mAppRow == null) {
            return false;
        }
        if (this.mChannel != null) {
            return this.isChannelBlockable();
        }
        if (this.mChannelGroup != null) {
            return this.isChannelGroupBlockable();
        }
        if (!this.mAppRow.systemApp || (this.mAppRow.systemApp && this.mAppRow.banned)) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        final boolean banned = b ^ true;
        final NotificationChannel mChannel = this.mChannel;
        final boolean b2 = false;
        boolean b3 = false;
        if (mChannel != null) {
            final int importance = this.mChannel.getImportance();
            if (banned || importance == 0) {
                int importance2;
                if (banned) {
                    importance2 = 0;
                }
                else if (this.isDefaultChannel()) {
                    importance2 = -1000;
                }
                else {
                    importance2 = 3;
                }
                this.mChannel.setImportance(importance2);
                this.saveChannel();
            }
            if (this.mBackend.onlyHasDefaultChannel(this.mAppRow.pkg, this.mAppRow.uid) && this.mAppRow.banned != banned) {
                this.mAppRow.banned = banned;
                final NotificationBackend mBackend = this.mBackend;
                final String pkg = this.mAppRow.pkg;
                final int uid = this.mAppRow.uid;
                if (!banned) {
                    b3 = true;
                }
                mBackend.setNotificationsEnabledForPackage(pkg, uid, b3);
            }
        }
        else if (this.mChannelGroup != null) {
            this.mChannelGroup.setBlocked(banned);
            this.mBackend.updateChannelGroup(this.mAppRow.pkg, this.mAppRow.uid, this.mChannelGroup);
        }
        else if (this.mAppRow != null) {
            this.mAppRow.banned = banned;
            final NotificationBackend mBackend2 = this.mBackend;
            final String pkg2 = this.mAppRow.pkg;
            final int uid2 = this.mAppRow.uid;
            boolean b4 = b2;
            if (!banned) {
                b4 = true;
            }
            mBackend2.setNotificationsEnabledForPackage(pkg2, uid2, b4);
        }
        this.mImportanceListener.onImportanceChanged();
    }
    
    @Override
    public void updateState(Preference preference) {
        preference = (Preference)((LayoutPreference)preference).findViewById(2131362706);
        if (preference != null) {
            ((SwitchBar)preference).setSwitchBarText(2131888502, 2131888502);
            ((SwitchBar)preference).show();
            try {
                ((SwitchBar)preference).addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            }
            catch (IllegalStateException ex) {}
            ((SwitchBar)preference).setDisabledByAdmin(this.mAdmin);
            final NotificationChannel mChannel = this.mChannel;
            final boolean b = false;
            boolean checked = false;
            if (mChannel != null) {
                if (!this.mAppRow.banned && this.mChannel.getImportance() != 0) {
                    checked = true;
                }
                ((SwitchBar)preference).setChecked(checked);
            }
            else if (this.mChannelGroup != null) {
                boolean checked2 = b;
                if (!this.mAppRow.banned) {
                    checked2 = b;
                    if (!this.mChannelGroup.isBlocked()) {
                        checked2 = true;
                    }
                }
                ((SwitchBar)preference).setChecked(checked2);
            }
            else {
                ((SwitchBar)preference).setChecked(this.mAppRow.banned ^ true);
            }
        }
    }
}
