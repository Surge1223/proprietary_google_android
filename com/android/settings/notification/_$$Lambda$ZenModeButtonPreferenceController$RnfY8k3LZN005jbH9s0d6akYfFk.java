package com.android.settings.notification;

import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.widget.Button;
import android.app.FragmentManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$ZenModeButtonPreferenceController$RnfY8k3LZN005jbH9s0d6akYfFk implements View.OnClickListener
{
    public final void onClick(final View view) {
        ZenModeButtonPreferenceController.lambda$updateState$0(this.f$0, view);
    }
}
