package com.android.settings.notification;

import android.support.v7.preference.PreferenceScreen;
import android.widget.Toast;
import android.content.IntentFilter;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.widget.MasterCheckBoxPreference;
import android.support.v7.preference.PreferenceGroup;
import android.arch.lifecycle.LifecycleObserver;
import android.os.UserHandle;
import android.content.pm.ActivityInfo;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import java.util.ArrayList;
import android.util.Log;
import com.android.settingslib.RestrictedLockUtils;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.BroadcastReceiver;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.preference.Preference;
import java.util.List;
import android.content.Context;
import android.app.NotificationChannelGroup;
import java.util.Comparator;
import android.app.NotificationChannel;
import android.os.Bundle;
import com.android.settings.dashboard.DashboardFragment;

public abstract class NotificationSettingsBase extends DashboardFragment
{
    private static final boolean DEBUG;
    protected NotificationBackend.AppRow mAppRow;
    protected Bundle mArgs;
    protected NotificationBackend mBackend;
    protected NotificationChannel mChannel;
    protected Comparator<NotificationChannel> mChannelComparator;
    protected NotificationChannelGroup mChannelGroup;
    protected Context mContext;
    protected List<NotificationPreferenceController> mControllers;
    protected List<Preference> mDynamicPreferences;
    protected ImportanceListener mImportanceListener;
    protected Intent mIntent;
    protected boolean mListeningToPackageRemove;
    protected NotificationManager mNm;
    protected final BroadcastReceiver mPackageRemovedReceiver;
    protected String mPkg;
    protected PackageInfo mPkgInfo;
    protected PackageManager mPm;
    protected boolean mShowLegacyChannelConfig;
    protected RestrictedLockUtils.EnforcedAdmin mSuspendedAppsAdmin;
    protected int mUid;
    protected int mUserId;
    
    static {
        DEBUG = Log.isLoggable("NotifiSettingsBase", 3);
    }
    
    public NotificationSettingsBase() {
        this.mBackend = new NotificationBackend();
        this.mShowLegacyChannelConfig = false;
        this.mControllers = new ArrayList<NotificationPreferenceController>();
        this.mDynamicPreferences = new ArrayList<Preference>();
        this.mImportanceListener = new ImportanceListener();
        this.mPackageRemovedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
                if (NotificationSettingsBase.this.mPkgInfo == null || TextUtils.equals((CharSequence)NotificationSettingsBase.this.mPkgInfo.packageName, (CharSequence)schemeSpecificPart)) {
                    if (NotificationSettingsBase.DEBUG) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Package (");
                        sb.append(schemeSpecificPart);
                        sb.append(") removed. RemovingNotificationSettingsBase.");
                        Log.d("NotifiSettingsBase", sb.toString());
                    }
                    NotificationSettingsBase.this.onPackageRemoved();
                }
            }
        };
        this.mChannelComparator = (Comparator<NotificationChannel>)_$$Lambda$NotificationSettingsBase$_zFOM6q_03lCRFkOVmbrRVoBxkk.INSTANCE;
    }
    
    private PackageInfo findPackageInfo(final String s, int i) {
        if (s != null && i >= 0) {
            final String[] packagesForUid = this.mPm.getPackagesForUid(i);
            if (packagesForUid != null && s != null) {
                int length;
                StringBuilder sb;
                for (length = packagesForUid.length, i = 0; i < length; ++i) {
                    if (s.equals(packagesForUid[i])) {
                        try {
                            return this.mPm.getPackageInfo(s, 64);
                        }
                        catch (PackageManager$NameNotFoundException ex) {
                            sb = new StringBuilder();
                            sb.append("Failed to load package ");
                            sb.append(s);
                            Log.w("NotifiSettingsBase", sb.toString(), (Throwable)ex);
                        }
                    }
                }
            }
            return null;
        }
        return null;
    }
    
    private void loadAppRow() {
        this.mAppRow = this.mBackend.loadAppRow(this.mContext, this.mPm, this.mPkgInfo);
    }
    
    private void loadChannel() {
        final Intent intent = this.getActivity().getIntent();
        final String s = null;
        String stringExtra;
        if (intent != null) {
            stringExtra = intent.getStringExtra("android.provider.extra.CHANNEL_ID");
        }
        else {
            stringExtra = null;
        }
        String s2 = stringExtra;
        if (stringExtra == null) {
            s2 = stringExtra;
            if (intent != null) {
                final Bundle bundleExtra = intent.getBundleExtra(":settings:show_fragment_args");
                String string = s;
                if (bundleExtra != null) {
                    string = bundleExtra.getString("android.provider.extra.CHANNEL_ID");
                }
                s2 = string;
            }
        }
        this.mChannel = this.mBackend.getChannel(this.mPkg, this.mUid, s2);
    }
    
    private void loadChannelGroup() {
        this.mShowLegacyChannelConfig = (this.mBackend.onlyHasDefaultChannel(this.mAppRow.pkg, this.mAppRow.uid) || (this.mChannel != null && "miscellaneous".equals(this.mChannel.getId())));
        if (this.mShowLegacyChannelConfig) {
            this.mChannel = this.mBackend.getChannel(this.mAppRow.pkg, this.mAppRow.uid, "miscellaneous");
        }
        if (this.mChannel != null && !TextUtils.isEmpty((CharSequence)this.mChannel.getGroup())) {
            final NotificationChannelGroup group = this.mBackend.getGroup(this.mPkg, this.mUid, this.mChannel.getGroup());
            if (group != null) {
                this.mChannelGroup = group;
            }
        }
    }
    
    protected void collectConfigActivities() {
        final Intent setPackage = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.NOTIFICATION_PREFERENCES").setPackage(this.mAppRow.pkg);
        final List queryIntentActivities = this.mPm.queryIntentActivities(setPackage, 0);
        if (NotificationSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Found ");
            sb.append(queryIntentActivities.size());
            sb.append(" preference activities");
            String s;
            if (queryIntentActivities.size() == 0) {
                s = " ;_;";
            }
            else {
                s = "";
            }
            sb.append(s);
            Log.d("NotifiSettingsBase", sb.toString());
        }
        final Iterator<ResolveInfo> iterator = queryIntentActivities.iterator();
        while (iterator.hasNext()) {
            final ActivityInfo activityInfo = iterator.next().activityInfo;
            if (this.mAppRow.settingsIntent != null) {
                if (!NotificationSettingsBase.DEBUG) {
                    continue;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Ignoring duplicate notification preference activity (");
                sb2.append(activityInfo.name);
                sb2.append(") for package ");
                sb2.append(activityInfo.packageName);
                Log.d("NotifiSettingsBase", sb2.toString());
            }
            else {
                this.mAppRow.settingsIntent = setPackage.setPackage((String)null).setClassName(activityInfo.packageName, activityInfo.name);
                if (this.mChannel != null) {
                    this.mAppRow.settingsIntent.putExtra("android.intent.extra.CHANNEL_ID", this.mChannel.getId());
                }
                if (this.mChannelGroup == null) {
                    continue;
                }
                this.mAppRow.settingsIntent.putExtra("android.intent.extra.CHANNEL_GROUP_ID", this.mChannelGroup.getId());
            }
        }
    }
    
    protected boolean isChannelBlockable(final NotificationChannel notificationChannel) {
        boolean b = false;
        if (notificationChannel == null || this.mAppRow == null) {
            return false;
        }
        if (!this.mAppRow.systemApp) {
            return true;
        }
        if (notificationChannel.isBlockableSystem() || notificationChannel.getImportance() == 0) {
            b = true;
        }
        return b;
    }
    
    protected boolean isChannelConfigurable(final NotificationChannel notificationChannel) {
        return notificationChannel != null && this.mAppRow != null && (notificationChannel.getId().equals(this.mAppRow.lockedChannelId) ^ true);
    }
    
    protected boolean isChannelGroupBlockable(final NotificationChannelGroup notificationChannelGroup) {
        return notificationChannelGroup != null && this.mAppRow != null && (!this.mAppRow.systemApp || notificationChannelGroup.isBlocked());
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mContext = (Context)this.getActivity();
        this.mIntent = this.getActivity().getIntent();
        this.mArgs = this.getArguments();
        this.mPm = this.getPackageManager();
        this.mNm = NotificationManager.from(this.mContext);
        String mPkg;
        if (this.mArgs != null && this.mArgs.containsKey("package")) {
            mPkg = this.mArgs.getString("package");
        }
        else {
            mPkg = this.mIntent.getStringExtra("android.provider.extra.APP_PACKAGE");
        }
        this.mPkg = mPkg;
        int mUid;
        if (this.mArgs != null && this.mArgs.containsKey("uid")) {
            mUid = this.mArgs.getInt("uid");
        }
        else {
            mUid = this.mIntent.getIntExtra("app_uid", -1);
        }
        this.mUid = mUid;
        if (this.mUid < 0) {
            try {
                this.mUid = this.mPm.getPackageUid(this.mPkg, 0);
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        this.mPkgInfo = this.findPackageInfo(this.mPkg, this.mUid);
        this.mUserId = UserHandle.getUserId(this.mUid);
        this.mSuspendedAppsAdmin = RestrictedLockUtils.checkIfApplicationIsSuspended(this.mContext, this.mPkg, this.mUserId);
        this.loadChannel();
        this.loadAppRow();
        this.loadChannelGroup();
        this.collectConfigActivities();
        this.getLifecycle().addObserver(this.use(HeaderPreferenceController.class));
        final Iterator<NotificationPreferenceController> iterator = this.mControllers.iterator();
        while (iterator.hasNext()) {
            iterator.next().onResume(this.mAppRow, this.mChannel, this.mChannelGroup, this.mSuspendedAppsAdmin);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.mIntent == null && this.mArgs == null) {
            Log.w("NotifiSettingsBase", "No intent");
            this.toastAndFinish();
            return;
        }
        if (this.mUid >= 0 && !TextUtils.isEmpty((CharSequence)this.mPkg) && this.mPkgInfo != null) {
            this.startListeningToPackageRemove();
            return;
        }
        Log.w("NotifiSettingsBase", "Missing package or uid or packageinfo");
        this.toastAndFinish();
    }
    
    @Override
    public void onDestroy() {
        this.stopListeningToPackageRemove();
        super.onDestroy();
    }
    
    protected void onPackageRemoved() {
        this.getActivity().finishAndRemoveTask();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mUid < 0 || TextUtils.isEmpty((CharSequence)this.mPkg) || this.mPkgInfo == null || this.mAppRow == null) {
            Log.w("NotifiSettingsBase", "Missing package or uid or packageinfo");
            this.finish();
            return;
        }
        this.loadAppRow();
        if (this.mAppRow == null) {
            Log.w("NotifiSettingsBase", "Can't load package");
            this.finish();
            return;
        }
        this.loadChannel();
        this.loadChannelGroup();
        this.collectConfigActivities();
    }
    
    protected Preference populateSingleChannelPrefs(final PreferenceGroup preferenceGroup, final NotificationChannel notificationChannel, final boolean b) {
        final MasterCheckBoxPreference masterCheckBoxPreference = new MasterCheckBoxPreference(this.getPrefContext());
        final RestrictedLockUtils.EnforcedAdmin mSuspendedAppsAdmin = this.mSuspendedAppsAdmin;
        final boolean b2 = false;
        masterCheckBoxPreference.setCheckBoxEnabled(mSuspendedAppsAdmin == null && this.isChannelBlockable(notificationChannel) && this.isChannelConfigurable(notificationChannel) && !b);
        masterCheckBoxPreference.setKey(notificationChannel.getId());
        masterCheckBoxPreference.setTitle(notificationChannel.getName());
        boolean checked = b2;
        if (notificationChannel.getImportance() != 0) {
            checked = true;
        }
        masterCheckBoxPreference.setChecked(checked);
        final Bundle arguments = new Bundle();
        arguments.putInt("uid", this.mUid);
        arguments.putString("package", this.mPkg);
        arguments.putString("android.provider.extra.CHANNEL_ID", notificationChannel.getId());
        arguments.putBoolean("fromSettings", true);
        masterCheckBoxPreference.setIntent(new SubSettingLauncher((Context)this.getActivity()).setDestination(ChannelNotificationSettings.class.getName()).setArguments(arguments).setTitle(2131888436).setSourceMetricsCategory(this.getMetricsCategory()).toIntent());
        masterCheckBoxPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                int importance;
                if (o) {
                    importance = 2;
                }
                else {
                    importance = 0;
                }
                notificationChannel.setImportance(importance);
                notificationChannel.lockFields(4);
                NotificationSettingsBase.this.mBackend.updateChannel(NotificationSettingsBase.this.mPkg, NotificationSettingsBase.this.mUid, notificationChannel);
                return true;
            }
        });
        preferenceGroup.addPreference(masterCheckBoxPreference);
        return masterCheckBoxPreference;
    }
    
    protected void setVisible(final PreferenceGroup preferenceGroup, final Preference preference, final boolean b) {
        if (preferenceGroup.findPreference(preference.getKey()) != null == b) {
            return;
        }
        if (b) {
            preferenceGroup.addPreference(preference);
        }
        else {
            preferenceGroup.removePreference(preference);
        }
    }
    
    protected void startListeningToPackageRemove() {
        if (this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = true;
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.getContext().registerReceiver(this.mPackageRemovedReceiver, intentFilter);
    }
    
    protected void stopListeningToPackageRemove() {
        if (!this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = false;
        this.getContext().unregisterReceiver(this.mPackageRemovedReceiver);
    }
    
    protected void toastAndFinish() {
        Toast.makeText(this.mContext, 2131886373, 0).show();
        this.getActivity().finish();
    }
    
    protected class ImportanceListener
    {
        protected void onImportanceChanged() {
            final PreferenceScreen preferenceScreen = NotificationSettingsBase.this.getPreferenceScreen();
            final Iterator<NotificationPreferenceController> iterator = NotificationSettingsBase.this.mControllers.iterator();
            while (iterator.hasNext()) {
                iterator.next().displayPreference(preferenceScreen);
            }
            DashboardFragment.this.updatePreferenceStates();
            boolean blocked = false;
            if (NotificationSettingsBase.this.mAppRow != null && !NotificationSettingsBase.this.mAppRow.banned) {
                if (NotificationSettingsBase.this.mChannel != null) {
                    blocked = (NotificationSettingsBase.this.mChannel.getImportance() == 0);
                }
                else if (NotificationSettingsBase.this.mChannelGroup != null) {
                    blocked = NotificationSettingsBase.this.mChannelGroup.isBlocked();
                }
            }
            else {
                blocked = true;
            }
            final Iterator<Preference> iterator2 = NotificationSettingsBase.this.mDynamicPreferences.iterator();
            while (iterator2.hasNext()) {
                NotificationSettingsBase.this.setVisible(NotificationSettingsBase.this.getPreferenceScreen(), iterator2.next(), !blocked);
            }
        }
    }
}
