package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.app.NotificationManager$Policy;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class ZenFooterPreferenceController extends AbstractZenModePreferenceController
{
    public ZenFooterPreferenceController(final Context context, final Lifecycle lifecycle, final String s) {
        super(context, s, lifecycle);
    }
    
    @Override
    public boolean isAvailable() {
        return this.mBackend.mPolicy.suppressedVisualEffects == 0 || NotificationManager$Policy.areAllVisualEffectsSuppressed(this.mBackend.mPolicy.suppressedVisualEffects);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (this.mBackend.mPolicy.suppressedVisualEffects == 0) {
            preference.setTitle(2131890344);
        }
        else if (NotificationManager$Policy.areAllVisualEffectsSuppressed(this.mBackend.mPolicy.suppressedVisualEffects)) {
            preference.setTitle(2131890341);
        }
        else {
            preference.setTitle(null);
        }
    }
}
