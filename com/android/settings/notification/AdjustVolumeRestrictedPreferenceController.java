package com.android.settings.notification;

import android.os.UserHandle;
import com.android.settingslib.RestrictedPreference;
import android.support.v7.preference.Preference;
import android.content.IntentFilter;
import android.content.Context;
import com.android.settings.accounts.AccountRestrictionHelper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.SliderPreferenceController;

public abstract class AdjustVolumeRestrictedPreferenceController extends SliderPreferenceController implements PreferenceControllerMixin
{
    private AccountRestrictionHelper mHelper;
    
    AdjustVolumeRestrictedPreferenceController(final Context context, final AccountRestrictionHelper mHelper, final String s) {
        super(context, s);
        this.mHelper = mHelper;
    }
    
    public AdjustVolumeRestrictedPreferenceController(final Context context, final String s) {
        this(context, new AccountRestrictionHelper(context), s);
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.media.VOLUME_CHANGED_ACTION");
        intentFilter.addAction("android.media.STREAM_MUTE_CHANGED_ACTION");
        intentFilter.addAction("android.media.MASTER_MUTE_CHANGED_ACTION");
        return intentFilter;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof RestrictedPreference)) {
            return;
        }
        this.mHelper.enforceRestrictionOnPreference((RestrictedPreference)preference, "no_adjust_volume", UserHandle.myUserId());
    }
}
