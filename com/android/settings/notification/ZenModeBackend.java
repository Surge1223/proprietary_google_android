package com.android.settings.notification;

import android.app.ActivityManager;
import android.net.Uri;
import android.util.Log;
import android.service.notification.ZenModeConfig;
import android.provider.Settings;
import android.app.AutomaticZenRule;
import android.app.NotificationManager$Policy;
import android.app.NotificationManager;
import android.content.Context;

public class ZenModeBackend
{
    protected static final String ZEN_MODE_FROM_ANYONE = "zen_mode_from_anyone";
    protected static final String ZEN_MODE_FROM_CONTACTS = "zen_mode_from_contacts";
    protected static final String ZEN_MODE_FROM_NONE = "zen_mode_from_none";
    protected static final String ZEN_MODE_FROM_STARRED = "zen_mode_from_starred";
    private static ZenModeBackend sInstance;
    private String TAG;
    private final Context mContext;
    private final NotificationManager mNotificationManager;
    protected NotificationManager$Policy mPolicy;
    protected int mZenMode;
    
    public ZenModeBackend(final Context mContext) {
        this.TAG = "ZenModeSettingsBackend";
        this.mContext = mContext;
        this.mNotificationManager = (NotificationManager)mContext.getSystemService("notification");
        this.updateZenMode();
        this.updatePolicy();
    }
    
    private int clearDeprecatedEffects(final int n) {
        return n & 0xFFFFFFFC;
    }
    
    public static ZenModeBackend getInstance(final Context context) {
        if (ZenModeBackend.sInstance == null) {
            ZenModeBackend.sInstance = new ZenModeBackend(context);
        }
        return ZenModeBackend.sInstance;
    }
    
    protected static String getKeyFromSetting(final int n) {
        switch (n) {
            default: {
                return "zen_mode_from_none";
            }
            case 2: {
                return "zen_mode_from_starred";
            }
            case 1: {
                return "zen_mode_from_contacts";
            }
            case 0: {
                return "zen_mode_from_anyone";
            }
        }
    }
    
    private int getNewSuppressedEffects(final boolean b, int n) {
        final int suppressedVisualEffects = this.mPolicy.suppressedVisualEffects;
        if (b) {
            n |= suppressedVisualEffects;
        }
        else {
            n &= suppressedVisualEffects;
        }
        return this.clearDeprecatedEffects(n);
    }
    
    private int getPrioritySenders(final int n) {
        if (n == 8) {
            return this.getPriorityCallSenders();
        }
        if (n == 4) {
            return this.getPriorityMessageSenders();
        }
        return -1;
    }
    
    protected static int getSettingFromPrefKey(final String s) {
        final int hashCode = s.hashCode();
        int n = 0;
        Label_0090: {
            if (hashCode != -946901971) {
                if (hashCode != -423126328) {
                    if (hashCode != 187510959) {
                        if (hashCode == 462773226) {
                            if (s.equals("zen_mode_from_starred")) {
                                n = 2;
                                break Label_0090;
                            }
                        }
                    }
                    else if (s.equals("zen_mode_from_anyone")) {
                        n = 0;
                        break Label_0090;
                    }
                }
                else if (s.equals("zen_mode_from_contacts")) {
                    n = 1;
                    break Label_0090;
                }
            }
            else if (s.equals("zen_mode_from_none")) {
                n = 3;
                break Label_0090;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return -1;
            }
            case 2: {
                return 2;
            }
            case 1: {
                return 1;
            }
            case 0: {
                return 0;
            }
        }
    }
    
    protected String addZenRule(final AutomaticZenRule automaticZenRule) {
        try {
            final String addAutomaticZenRule = NotificationManager.from(this.mContext).addAutomaticZenRule(automaticZenRule);
            NotificationManager.from(this.mContext).getAutomaticZenRule(addAutomaticZenRule);
            return addAutomaticZenRule;
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    protected int getContactsSummary(final int n) {
        final int n2 = -1;
        if (n == -1) {
            return 2131890321;
        }
        int n3;
        if (n == 4) {
            n3 = n2;
            if (this.isPriorityCategoryEnabled(n)) {
                n3 = this.getPriorityMessageSenders();
            }
        }
        else {
            n3 = n2;
            if (n == 8) {
                n3 = n2;
                if (this.isPriorityCategoryEnabled(n)) {
                    n3 = this.getPriorityCallSenders();
                }
            }
        }
        switch (n3) {
            default: {
                return 2131890321;
            }
            case 2: {
                return 2131890322;
            }
            case 1: {
                return 2131890320;
            }
            case 0: {
                return 2131890319;
            }
        }
    }
    
    protected int getNewPriorityCategories(final boolean b, int n) {
        final int priorityCategories = this.mPolicy.priorityCategories;
        if (b) {
            n |= priorityCategories;
        }
        else {
            n &= priorityCategories;
        }
        return n;
    }
    
    protected int getPriorityCallSenders() {
        if (this.isPriorityCategoryEnabled(8)) {
            return this.mPolicy.priorityCallSenders;
        }
        return -1;
    }
    
    protected int getPriorityMessageSenders() {
        if (this.isPriorityCategoryEnabled(4)) {
            return this.mPolicy.priorityMessageSenders;
        }
        return -1;
    }
    
    protected int getZenMode() {
        return this.mZenMode = Settings.Global.getInt(this.mContext.getContentResolver(), "zen_mode", this.mZenMode);
    }
    
    protected boolean isPriorityCategoryEnabled(final int n) {
        return (this.mPolicy.priorityCategories & n) != 0x0;
    }
    
    protected boolean isVisualEffectSuppressed(final int n) {
        return (this.mPolicy.suppressedVisualEffects & n) != 0x0;
    }
    
    public boolean removeZenRule(final String s) {
        return NotificationManager.from(this.mContext).removeAutomaticZenRule(s);
    }
    
    protected void savePolicy(final int n, final int n2, final int n3, final int n4) {
        this.mPolicy = new NotificationManager$Policy(n, n2, n3, n4);
        this.mNotificationManager.setNotificationPolicy(this.mPolicy);
    }
    
    protected void saveSenders(final int n, int n2) {
        int priorityCallSenders = this.getPriorityCallSenders();
        int priorityMessageSenders = this.getPriorityMessageSenders();
        final int prioritySenders = this.getPrioritySenders(n);
        final boolean b = n2 != -1;
        if (n2 == -1) {
            n2 = prioritySenders;
        }
        String s = "";
        if (n == 8) {
            s = "Calls";
            priorityCallSenders = n2;
        }
        if (n == 4) {
            s = "Messages";
            priorityMessageSenders = n2;
        }
        this.savePolicy(this.getNewPriorityCategories(b, n), priorityCallSenders, priorityMessageSenders, this.mPolicy.suppressedVisualEffects);
        if (ZenModeSettingsBase.DEBUG) {
            final String tag = this.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("onPrefChange allow");
            sb.append(s);
            sb.append("=");
            sb.append(b);
            sb.append(" allow");
            sb.append(s);
            sb.append("From=");
            sb.append(ZenModeConfig.sourceToString(n2));
            Log.d(tag, sb.toString());
        }
    }
    
    protected void saveSoundPolicy(final int n, final boolean b) {
        this.savePolicy(this.getNewPriorityCategories(b, n), this.mPolicy.priorityCallSenders, this.mPolicy.priorityMessageSenders, this.mPolicy.suppressedVisualEffects);
    }
    
    protected void saveVisualEffectsPolicy(int newSuppressedEffects, final boolean b) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "zen_settings_updated", 1);
        newSuppressedEffects = this.getNewSuppressedEffects(b, newSuppressedEffects);
        this.savePolicy(this.mPolicy.priorityCategories, this.mPolicy.priorityCallSenders, this.mPolicy.priorityMessageSenders, newSuppressedEffects);
    }
    
    protected void setZenMode(final int n) {
        NotificationManager.from(this.mContext).setZenMode(n, (Uri)null, this.TAG);
        this.mZenMode = this.getZenMode();
    }
    
    protected void setZenModeForDuration(final int n) {
        this.mNotificationManager.setZenMode(1, ZenModeConfig.toTimeCondition(this.mContext, n, ActivityManager.getCurrentUser(), true).id, this.TAG);
        this.mZenMode = this.getZenMode();
    }
    
    protected boolean setZenRule(final String s, final AutomaticZenRule automaticZenRule) {
        return NotificationManager.from(this.mContext).updateAutomaticZenRule(s, automaticZenRule);
    }
    
    protected void updatePolicy() {
        if (this.mNotificationManager != null) {
            this.mPolicy = this.mNotificationManager.getNotificationPolicy();
        }
    }
    
    protected void updateZenMode() {
        this.mZenMode = Settings.Global.getInt(this.mContext.getContentResolver(), "zen_mode", this.mZenMode);
    }
}
