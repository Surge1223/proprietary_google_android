package com.android.settings.notification;

import android.view.View;
import android.content.Context;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.app.Dialog;
import android.os.Parcelable;
import android.os.Bundle;
import android.app.Fragment;
import android.service.notification.ZenModeConfig;
import android.net.Uri;
import android.widget.EditText;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ZenRuleNameDialog extends InstrumentedDialogFragment
{
    protected static PositiveClickListener mPositiveClickListener;
    
    private int getTitleResource(final Uri uri, final boolean b) {
        final boolean validEventConditionId = ZenModeConfig.isValidEventConditionId(uri);
        final boolean validScheduleConditionId = ZenModeConfig.isValidScheduleConditionId(uri);
        int n = 2131890351;
        if (b) {
            if (validEventConditionId) {
                n = 2131890251;
            }
            else {
                n = n;
                if (validScheduleConditionId) {
                    n = 2131890253;
                }
            }
        }
        return n;
    }
    
    public static void show(final Fragment fragment, final String s, final Uri uri, final PositiveClickListener mPositiveClickListener) {
        final Bundle arguments = new Bundle();
        arguments.putString("zen_rule_name", s);
        arguments.putParcelable("extra_zen_condition_id", (Parcelable)uri);
        ZenRuleNameDialog.mPositiveClickListener = mPositiveClickListener;
        final ZenRuleNameDialog zenRuleNameDialog = new ZenRuleNameDialog();
        zenRuleNameDialog.setArguments(arguments);
        zenRuleNameDialog.setTargetFragment(fragment, 0);
        zenRuleNameDialog.show(fragment.getFragmentManager(), "ZenRuleNameDialog");
    }
    
    private String trimmedText(final EditText editText) {
        String trim;
        if (editText.getText() == null) {
            trim = null;
        }
        else {
            trim = editText.getText().toString().trim();
        }
        return trim;
    }
    
    @Override
    public int getMetricsCategory() {
        return 1269;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Bundle arguments = this.getArguments();
        final Uri uri = (Uri)arguments.getParcelable("extra_zen_condition_id");
        final String string = arguments.getString("zen_rule_name");
        final boolean b = string == null;
        final Context context = this.getContext();
        final View inflate = LayoutInflater.from(context).inflate(2131558905, (ViewGroup)null, false);
        final EditText editText = (EditText)inflate.findViewById(2131362846);
        if (!b) {
            editText.setText((CharSequence)string);
            editText.setSelection(editText.getText().length());
        }
        editText.setSelectAllOnFocus(true);
        final AlertDialog$Builder setView = new AlertDialog$Builder(context).setTitle(this.getTitleResource(uri, b)).setView(inflate);
        int n;
        if (b) {
            n = 2131890250;
        }
        else {
            n = R.string.okay;
        }
        return (Dialog)setView.setPositiveButton(n, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String access$000 = ZenRuleNameDialog.this.trimmedText(editText);
                if (TextUtils.isEmpty((CharSequence)access$000)) {
                    return;
                }
                if (!b && string != null && string.equals(access$000)) {
                    return;
                }
                ZenRuleNameDialog.mPositiveClickListener.onOk(access$000, ZenRuleNameDialog.this.getTargetFragment());
            }
        }).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).create();
    }
    
    public interface PositiveClickListener
    {
        void onOk(final String p0, final Fragment p1);
    }
}
