package com.android.settings.notification;

import java.util.ArrayList;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.RestrictedListPreference;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Context;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class VisibilityPreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private LockPatternUtils mLockPatternUtils;
    
    public VisibilityPreferenceController(final Context context, final LockPatternUtils mLockPatternUtils, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
        this.mLockPatternUtils = mLockPatternUtils;
    }
    
    private int getGlobalVisibility() {
        int n = -1000;
        if (!this.getLockscreenNotificationsEnabled()) {
            n = -1;
        }
        else if (!this.getLockscreenAllowPrivateNotifications()) {
            n = 0;
        }
        return n;
    }
    
    private boolean getLockscreenAllowPrivateNotifications() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, "lock_screen_allow_private_notifications", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    private boolean getLockscreenNotificationsEnabled() {
        final UserInfo profileParent = this.mUm.getProfileParent(UserHandle.myUserId());
        int n;
        if (profileParent != null) {
            n = profileParent.id;
        }
        else {
            n = UserHandle.myUserId();
        }
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getIntForUser(contentResolver, "lock_screen_show_notifications", 0, n) != 0) {
            b = true;
        }
        return b;
    }
    
    private void setRestrictedIfNotificationFeaturesDisabled(final RestrictedListPreference restrictedListPreference, final CharSequence charSequence, final CharSequence charSequence2, final int n) {
        final RestrictedLockUtils.EnforcedAdmin checkIfKeyguardFeaturesDisabled = RestrictedLockUtils.checkIfKeyguardFeaturesDisabled(this.mContext, n, this.mAppRow.userId);
        if (checkIfKeyguardFeaturesDisabled != null) {
            restrictedListPreference.addRestrictedItem(new RestrictedListPreference.RestrictedItem(charSequence, charSequence2, checkIfKeyguardFeaturesDisabled));
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "visibility_override";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        final boolean b = false;
        if (!available) {
            return false;
        }
        if (this.mChannel != null && !this.mAppRow.banned) {
            boolean b2 = b;
            if (this.checkCanBeVisible(2)) {
                b2 = b;
                if (this.isLockScreenSecure()) {
                    b2 = true;
                }
            }
            return b2;
        }
        return false;
    }
    
    protected boolean isLockScreenSecure() {
        final boolean secure = this.mLockPatternUtils.isSecure(UserHandle.myUserId());
        final UserInfo profileParent = this.mUm.getProfileParent(UserHandle.myUserId());
        boolean b = secure;
        if (profileParent != null) {
            b = (secure | this.mLockPatternUtils.isSecure(profileParent.id));
        }
        return b;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            int int1;
            if ((int1 = Integer.parseInt((String)o)) == this.getGlobalVisibility()) {
                int1 = -1000;
            }
            this.mChannel.setLockscreenVisibility(int1);
            this.mChannel.lockFields(2);
            this.saveChannel();
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mChannel != null && this.mAppRow != null) {
            final RestrictedListPreference restrictedListPreference = (RestrictedListPreference)preference;
            final ArrayList<String> list = new ArrayList<String>();
            final ArrayList<String> list2 = new ArrayList<String>();
            restrictedListPreference.clearRestrictedItems();
            if (this.getLockscreenNotificationsEnabled() && this.getLockscreenAllowPrivateNotifications()) {
                final String string = this.mContext.getString(2131888092);
                final String string2 = Integer.toString(-1000);
                list.add(string);
                list2.add(string2);
                this.setRestrictedIfNotificationFeaturesDisabled(restrictedListPreference, string, string2, 12);
            }
            if (this.getLockscreenNotificationsEnabled()) {
                final String string3 = this.mContext.getString(2131888090);
                final String string4 = Integer.toString(0);
                list.add(string3);
                list2.add(string4);
                this.setRestrictedIfNotificationFeaturesDisabled(restrictedListPreference, string3, string4, 4);
            }
            list.add(this.mContext.getString(2131888089));
            list2.add(Integer.toString(-1));
            restrictedListPreference.setEntries(list.toArray(new CharSequence[list.size()]));
            restrictedListPreference.setEntryValues(list2.toArray(new CharSequence[list2.size()]));
            if (this.mChannel.getLockscreenVisibility() == -1000) {
                restrictedListPreference.setValue(Integer.toString(this.getGlobalVisibility()));
            }
            else {
                restrictedListPreference.setValue(Integer.toString(this.mChannel.getLockscreenVisibility()));
            }
            restrictedListPreference.setSummary("%s");
        }
    }
}
