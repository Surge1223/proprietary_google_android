package com.android.settings.notification;

import android.util.Log;
import com.android.settingslib.RestrictedSwitchPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class AllowSoundPreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private NotificationSettingsBase.ImportanceListener mImportanceListener;
    
    public AllowSoundPreferenceController(final Context context, final NotificationSettingsBase.ImportanceListener mImportanceListener, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
        this.mImportanceListener = mImportanceListener;
    }
    
    @Override
    public String getPreferenceKey() {
        return "allow_sound";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        final boolean b = false;
        if (!available) {
            return false;
        }
        boolean b2 = b;
        if (this.mChannel != null) {
            b2 = b;
            if ("miscellaneous".equals(this.mChannel.getId())) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            int importance;
            if (o) {
                importance = -1000;
            }
            else {
                importance = 2;
            }
            this.mChannel.setImportance(importance);
            this.mChannel.lockFields(4);
            this.saveChannel();
            this.mImportanceListener.onImportanceChanged();
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mChannel != null) {
            final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
            restrictedSwitchPreference.setDisabledByAdmin(this.mAdmin);
            final boolean channelConfigurable = this.isChannelConfigurable();
            final boolean b = false;
            restrictedSwitchPreference.setEnabled(channelConfigurable && !restrictedSwitchPreference.isDisabledByAdmin());
            restrictedSwitchPreference.setChecked(this.mChannel.getImportance() >= 3 || this.mChannel.getImportance() == -1000 || b);
        }
        else {
            Log.i("AllowSoundPrefContr", "tried to updatestate on a null channel?!");
        }
    }
}
