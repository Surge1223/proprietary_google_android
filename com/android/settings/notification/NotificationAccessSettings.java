package com.android.settings.notification;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.os.AsyncTask;
import android.content.ComponentName;
import android.app.NotificationManager;
import com.android.settings.utils.ManagedServiceSettings;

public class NotificationAccessSettings extends ManagedServiceSettings
{
    private static final Config CONFIG;
    private static final String TAG;
    private NotificationManager mNm;
    
    static {
        TAG = NotificationAccessSettings.class.getSimpleName();
        CONFIG = new Config.Builder().setTag(NotificationAccessSettings.TAG).setSetting("enabled_notification_listeners").setIntentAction("android.service.notification.NotificationListenerService").setPermission("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE").setNoun("notification listener").setWarningDialogTitle(2131888460).setWarningDialogSummary(2131888459).setEmptyText(2131888420).build();
    }
    
    private static void disable(final NotificationAccessSettings notificationAccessSettings, final ComponentName componentName) {
        notificationAccessSettings.mNm.setNotificationListenerAccessGranted(componentName, false);
        AsyncTask.execute((Runnable)new _$$Lambda$NotificationAccessSettings$5Getr2Y6VpjSaSB3qVPpmCZNr9A(notificationAccessSettings, componentName));
    }
    
    @Override
    protected void enable(final ComponentName componentName) {
        this.mNm.setNotificationListenerAccessGranted(componentName, true);
    }
    
    @Override
    protected Config getConfig() {
        return NotificationAccessSettings.CONFIG;
    }
    
    @Override
    public int getMetricsCategory() {
        return 179;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082795;
    }
    
    @Override
    protected boolean isServiceEnabled(final ComponentName componentName) {
        return this.mNm.isNotificationListenerAccessGranted(componentName);
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 776;
        }
        else {
            n = 777;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mNm = (NotificationManager)context.getSystemService((Class)NotificationManager.class);
    }
    
    @Override
    protected boolean setEnabled(final ComponentName componentName, final String s, final boolean b) {
        this.logSpecialPermissionChange(b, componentName.getPackageName());
        if (!b) {
            if (!this.isServiceEnabled(componentName)) {
                return true;
            }
            new FriendlyWarningDialogFragment().setServiceInfo(componentName, s, this).show(this.getFragmentManager(), "friendlydialog");
            return false;
        }
        else {
            if (this.isServiceEnabled(componentName)) {
                return true;
            }
            new ScaryWarningDialogFragment().setServiceInfo(componentName, s, this).show(this.getFragmentManager(), "dialog");
            return false;
        }
    }
    
    public static class FriendlyWarningDialogFragment extends InstrumentedDialogFragment
    {
        @Override
        public int getMetricsCategory() {
            return 552;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Bundle arguments = this.getArguments();
            return (Dialog)new AlertDialog$Builder(this.getContext()).setMessage((CharSequence)this.getResources().getString(2131888458, new Object[] { arguments.getString("l") })).setCancelable(true).setPositiveButton(2131888457, (DialogInterface$OnClickListener)new _$$Lambda$NotificationAccessSettings$FriendlyWarningDialogFragment$ND5PkKgvmxdEIdAr9gHIhLyAwTU((NotificationAccessSettings)this.getTargetFragment(), ComponentName.unflattenFromString(arguments.getString("c")))).setNegativeButton(2131888456, (DialogInterface$OnClickListener)_$$Lambda$NotificationAccessSettings$FriendlyWarningDialogFragment$dxECkfkY_zLrkSsUm1OLKJMeIiE.INSTANCE).create();
        }
        
        public FriendlyWarningDialogFragment setServiceInfo(final ComponentName componentName, final String s, final Fragment fragment) {
            final Bundle arguments = new Bundle();
            arguments.putString("c", componentName.flattenToString());
            arguments.putString("l", s);
            this.setArguments(arguments);
            this.setTargetFragment(fragment, 0);
            return this;
        }
    }
}
