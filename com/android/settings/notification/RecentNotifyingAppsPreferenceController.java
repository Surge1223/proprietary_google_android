package com.android.settings.notification;

import java.util.Map;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.applications.AppUtils;
import android.content.Intent;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import com.android.settingslib.utils.StringUtil;
import android.util.ArrayMap;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.app.Application;
import android.content.Context;
import java.util.Collection;
import java.util.Arrays;
import android.util.ArraySet;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.service.notification.NotifyingApp;
import java.util.List;
import com.android.settingslib.applications.ApplicationsState;
import java.util.Set;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class RecentNotifyingAppsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    static final String KEY_DIVIDER = "all_notifications_divider";
    static final String KEY_SEE_ALL = "all_notifications";
    private static final Set<String> SKIP_SYSTEM_PACKAGES;
    private final ApplicationsState mApplicationsState;
    private List<NotifyingApp> mApps;
    private PreferenceCategory mCategory;
    private Preference mDivider;
    private final Fragment mHost;
    private final IconDrawableFactory mIconDrawableFactory;
    private final NotificationBackend mNotificationBackend;
    private final PackageManager mPm;
    private Preference mSeeAllPref;
    private final int mUserId;
    
    static {
        (SKIP_SYSTEM_PACKAGES = (Set)new ArraySet()).addAll(Arrays.asList("android", "com.android.phone", "com.android.settings", "com.android.systemui", "com.android.providers.calendar", "com.android.providers.media"));
    }
    
    public RecentNotifyingAppsPreferenceController(final Context context, final NotificationBackend notificationBackend, final Application application, final Fragment fragment) {
        ApplicationsState instance;
        if (application == null) {
            instance = null;
        }
        else {
            instance = ApplicationsState.getInstance(application);
        }
        this(context, notificationBackend, instance, fragment);
    }
    
    RecentNotifyingAppsPreferenceController(final Context context, final NotificationBackend mNotificationBackend, final ApplicationsState mApplicationsState, final Fragment mHost) {
        super(context);
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(context);
        this.mUserId = UserHandle.myUserId();
        this.mPm = context.getPackageManager();
        this.mHost = mHost;
        this.mApplicationsState = mApplicationsState;
        this.mNotificationBackend = mNotificationBackend;
    }
    
    private void displayOnlyAllAppsLink() {
        this.mCategory.setTitle(null);
        this.mDivider.setVisible(false);
        this.mSeeAllPref.setTitle(2131888522);
        this.mSeeAllPref.setIcon(null);
        for (int i = this.mCategory.getPreferenceCount() - 1; i >= 0; --i) {
            final Preference preference = this.mCategory.getPreference(i);
            if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"all_notifications")) {
                this.mCategory.removePreference(preference);
            }
        }
    }
    
    private void displayRecentApps(final Context context, final List<NotifyingApp> list) {
        this.mCategory.setTitle(2131888795);
        this.mDivider.setVisible(true);
        this.mSeeAllPref.setSummary(null);
        this.mSeeAllPref.setIcon(2131230994);
        final ArrayMap arrayMap = new ArrayMap();
        final int preferenceCount = this.mCategory.getPreferenceCount();
        final int n = 0;
        for (int i = 0; i < preferenceCount; ++i) {
            final Preference preference = this.mCategory.getPreference(i);
            final String key = preference.getKey();
            if (!TextUtils.equals((CharSequence)key, (CharSequence)"all_notifications")) {
                ((Map<String, NotificationAppPreference>)arrayMap).put(key, (NotificationAppPreference)preference);
            }
        }
        for (int size = list.size(), j = n; j < size; ++j) {
            final NotifyingApp notifyingApp = list.get(j);
            final String package1 = notifyingApp.getPackage();
            final ApplicationsState.AppEntry entry = this.mApplicationsState.getEntry(notifyingApp.getPackage(), this.mUserId);
            if (entry != null) {
                boolean b = true;
                NotificationAppPreference notificationAppPreference = ((Map<String, NotificationAppPreference>)arrayMap).remove(package1);
                if (notificationAppPreference == null) {
                    notificationAppPreference = new NotificationAppPreference(context);
                    b = false;
                }
                notificationAppPreference.setKey(package1);
                notificationAppPreference.setTitle(entry.label);
                notificationAppPreference.setIcon(this.mIconDrawableFactory.getBadgedIcon(entry.info));
                notificationAppPreference.setIconSize(2);
                notificationAppPreference.setSummary(StringUtil.formatRelativeTime(this.mContext, System.currentTimeMillis() - notifyingApp.getLastNotified(), true));
                notificationAppPreference.setOrder(j);
                final Bundle arguments = new Bundle();
                arguments.putString("package", package1);
                arguments.putInt("uid", entry.info.uid);
                notificationAppPreference.setIntent(new SubSettingLauncher((Context)this.mHost.getActivity()).setDestination(AppNotificationSettings.class.getName()).setTitle(2131888522).setArguments(arguments).setSourceMetricsCategory(133).toIntent());
                notificationAppPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new _$$Lambda$RecentNotifyingAppsPreferenceController$7CmRKIepfLY9sZOWQrI97x_3AWA(this, package1, entry));
                notificationAppPreference.setChecked(this.mNotificationBackend.getNotificationsBanned(package1, entry.info.uid) ^ true);
                if (!b) {
                    this.mCategory.addPreference(notificationAppPreference);
                }
            }
        }
        final Iterator<NotificationAppPreference> iterator = ((Map<String, NotificationAppPreference>)arrayMap).values().iterator();
        while (iterator.hasNext()) {
            this.mCategory.removePreference(iterator.next());
        }
    }
    
    private List<NotifyingApp> getDisplayableRecentAppList() {
        Collections.sort(this.mApps);
        final ArrayList<NotifyingApp> list = new ArrayList<NotifyingApp>(5);
        int n = 0;
        for (final NotifyingApp notifyingApp : this.mApps) {
            if (this.mApplicationsState.getEntry(notifyingApp.getPackage(), this.mUserId) == null) {
                continue;
            }
            if (!this.shouldIncludePkgInRecents(notifyingApp.getPackage())) {
                continue;
            }
            list.add(notifyingApp);
            if (++n >= 5) {
                break;
            }
        }
        return list;
    }
    
    private boolean shouldIncludePkgInRecents(final String package1) {
        if (RecentNotifyingAppsPreferenceController.SKIP_SYSTEM_PACKAGES.contains(package1)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("System package, skipping ");
            sb.append(package1);
            Log.d("RecentNotisCtrl", sb.toString());
            return false;
        }
        if (this.mPm.resolveActivity(new Intent().addCategory("android.intent.category.LAUNCHER").setPackage(package1), 0) == null) {
            final ApplicationsState.AppEntry entry = this.mApplicationsState.getEntry(package1, this.mUserId);
            if (entry == null || entry.info == null || !AppUtils.isInstant(entry.info)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Not a user visible or instant app, skipping ");
                sb2.append(package1);
                Log.d("RecentNotisCtrl", sb2.toString());
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mCategory = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey());
        this.mSeeAllPref = preferenceScreen.findPreference("all_notifications");
        this.mDivider = preferenceScreen.findPreference("all_notifications_divider");
        super.displayPreference(preferenceScreen);
        this.refreshUi(this.mCategory.getContext());
    }
    
    @Override
    public String getPreferenceKey() {
        return "recent_notifications_category";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    void refreshUi(final Context context) {
        this.reloadData();
        final List<NotifyingApp> displayableRecentAppList = this.getDisplayableRecentAppList();
        if (displayableRecentAppList != null && !displayableRecentAppList.isEmpty()) {
            this.displayRecentApps(context, displayableRecentAppList);
        }
        else {
            this.displayOnlyAllAppsLink();
        }
    }
    
    void reloadData() {
        this.mApps = this.mNotificationBackend.getRecentApps();
    }
    
    @Override
    public void updateNonIndexableKeys(final List<String> list) {
        super.updateNonIndexableKeys(list);
        list.add("recent_notifications_category");
        list.add("all_notifications_divider");
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.refreshUi(this.mCategory.getContext());
        this.mSeeAllPref.setTitle(this.mContext.getString(2131888796));
    }
}
