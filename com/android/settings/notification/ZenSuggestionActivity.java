package com.android.settings.notification;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;

public class ZenSuggestionActivity extends Activity
{
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.startActivity(new Intent("android.settings.ZEN_MODE_SETTINGS"));
        this.startActivity(new Intent("android.settings.ZEN_MODE_ONBOARDING"));
        this.finish();
    }
}
