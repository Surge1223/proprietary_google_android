package com.android.settings.notification;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.os.AsyncTask;
import android.app.NotificationManager;
import com.android.settings.utils.ManagedServiceSettings;
import android.content.ComponentName;

public final class _$$Lambda$NotificationAccessSettings$5Getr2Y6VpjSaSB3qVPpmCZNr9A implements Runnable
{
    @Override
    public final void run() {
        NotificationAccessSettings.lambda$disable$0(this.f$0, this.f$1);
    }
}
