package com.android.settings.notification;

import com.android.settings.widget.DisabledCheckBoxPreference;
import android.support.v7.preference.CheckBoxPreference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class ZenModeVisEffectPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    protected final int mEffect;
    protected final String mKey;
    protected final int mMetricsCategory;
    protected final int[] mParentSuppressedEffects;
    private PreferenceScreen mScreen;
    
    public ZenModeVisEffectPreferenceController(final Context context, final Lifecycle lifecycle, final String mKey, final int mEffect, final int mMetricsCategory, final int[] mParentSuppressedEffects) {
        super(context, mKey, lifecycle);
        this.mKey = mKey;
        this.mEffect = mEffect;
        this.mMetricsCategory = mMetricsCategory;
        this.mParentSuppressedEffects = mParentSuppressedEffects;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        super.displayPreference(this.mScreen = mScreen);
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mKey;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mEffect != 8 || this.mContext.getResources().getBoolean(17956986);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        this.mMetricsFeatureProvider.action(this.mContext, this.mMetricsCategory, booleanValue);
        this.mBackend.saveVisualEffectsPolicy(this.mEffect, booleanValue);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final boolean visualEffectSuppressed = this.mBackend.isVisualEffectSuppressed(this.mEffect);
        boolean checked = false;
        if (this.mParentSuppressedEffects != null) {
            final int[] mParentSuppressedEffects = this.mParentSuppressedEffects;
            final int length = mParentSuppressedEffects.length;
            checked = false;
            for (int i = 0; i < length; ++i) {
                checked |= this.mBackend.isVisualEffectSuppressed(mParentSuppressedEffects[i]);
            }
        }
        if (checked) {
            ((CheckBoxPreference)preference).setChecked(checked);
            this.onPreferenceChange(preference, checked);
            ((DisabledCheckBoxPreference)preference).enableCheckbox(false);
        }
        else {
            ((DisabledCheckBoxPreference)preference).enableCheckbox(true);
            ((CheckBoxPreference)preference).setChecked(visualEffectSuppressed);
        }
    }
}
