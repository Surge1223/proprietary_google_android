package com.android.settings.notification;

import android.os.Bundle;
import android.os.UserHandle;
import android.support.v7.preference.Preference;
import android.content.Intent;
import android.arch.lifecycle.LifecycleObserver;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import android.app.Application;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.search.BaseSearchIndexProvider;
import android.content.Context;
import android.app.Activity;
import com.android.settings.RingtonePreference;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class ConfigureNotificationSettings extends DashboardFragment
{
    static final String KEY_LOCKSCREEN = "lock_screen_notifications";
    static final String KEY_LOCKSCREEN_WORK_PROFILE = "lock_screen_notifications_profile";
    static final String KEY_LOCKSCREEN_WORK_PROFILE_HEADER = "lock_screen_notifications_profile_header";
    static final String KEY_SWIPE_DOWN = "gesture_swipe_down_fingerprint_notifications";
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private RingtonePreference mRequestPreference;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new ConfigureNotificationSettings.SummaryProvider((Context)activity, summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("gesture_swipe_down_fingerprint_notifications");
                nonIndexableKeys.add("lock_screen_notifications");
                nonIndexableKeys.add("lock_screen_notifications_profile");
                nonIndexableKeys.add("lock_screen_notifications_profile_header");
                nonIndexableKeys.add("zen_mode_notifications");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082734;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle, final Application application, final Fragment fragment) {
        final ArrayList<ConfigureNotificationSettings$1> list = (ArrayList<ConfigureNotificationSettings$1>)new ArrayList<ZenModePreferenceController>();
        final PulseNotificationPreferenceController pulseNotificationPreferenceController = new PulseNotificationPreferenceController(context);
        final LockScreenNotificationPreferenceController lockScreenNotificationPreferenceController = new LockScreenNotificationPreferenceController(context, "lock_screen_notifications", "lock_screen_notifications_profile_header", "lock_screen_notifications_profile");
        if (lifecycle != null) {
            lifecycle.addObserver(pulseNotificationPreferenceController);
            lifecycle.addObserver(lockScreenNotificationPreferenceController);
        }
        list.add((ZenModePreferenceController)new RecentNotifyingAppsPreferenceController(context, new NotificationBackend(), application, fragment));
        list.add((ZenModePreferenceController)pulseNotificationPreferenceController);
        list.add((ZenModePreferenceController)lockScreenNotificationPreferenceController);
        list.add((ZenModePreferenceController)new NotificationRingtonePreferenceController(context) {
            @Override
            public String getPreferenceKey() {
                return "notification_default_ringtone";
            }
        });
        list.add(new ZenModePreferenceController(context, lifecycle, "zen_mode_notifications"));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final Activity activity = this.getActivity();
        Application application;
        if (activity != null) {
            application = activity.getApplication();
        }
        else {
            application = null;
        }
        return buildPreferenceControllers(context, this.getLifecycle(), application, this);
    }
    
    @Override
    protected String getLogTag() {
        return "ConfigNotiSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 337;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082734;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (this.mRequestPreference != null) {
            this.mRequestPreference.onActivityResult(n, n2, intent);
            this.mRequestPreference = null;
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof RingtonePreference) {
            (this.mRequestPreference = (RingtonePreference)preference).onPrepareRingtonePickerIntent(this.mRequestPreference.getIntent());
            this.startActivityForResultAsUser(this.mRequestPreference.getIntent(), 200, (Bundle)null, UserHandle.of(this.mRequestPreference.getUserId()));
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mRequestPreference != null) {
            bundle.putString("selected_preference", this.mRequestPreference.getKey());
        }
    }
    
    static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private NotificationBackend mBackend;
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
            this.mBackend = new NotificationBackend();
        }
        
        protected void setBackend(final NotificationBackend mBackend) {
            this.mBackend = mBackend;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (!b) {
                return;
            }
            final int blockedAppCount = this.mBackend.getBlockedAppCount();
            if (blockedAppCount == 0) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getText(2131886378));
            }
            else {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getResources().getQuantityString(2131755016, blockedAppCount, new Object[] { blockedAppCount }));
            }
        }
    }
}
