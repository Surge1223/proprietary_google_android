package com.android.settings.notification;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.applications.AppUtils;
import android.content.Intent;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import com.android.settingslib.utils.StringUtil;
import android.util.ArrayMap;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.app.Application;
import android.content.Context;
import java.util.Collection;
import java.util.Arrays;
import android.util.ArraySet;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.Fragment;
import android.support.v7.preference.PreferenceCategory;
import android.service.notification.NotifyingApp;
import java.util.List;
import java.util.Set;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settingslib.applications.ApplicationsState;
import android.support.v7.preference.Preference;

public final class _$$Lambda$RecentNotifyingAppsPreferenceController$7CmRKIepfLY9sZOWQrI97x_3AWA implements OnPreferenceChangeListener
{
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return RecentNotifyingAppsPreferenceController.lambda$displayRecentApps$0(this.f$0, this.f$1, this.f$2, preference, o);
    }
}
