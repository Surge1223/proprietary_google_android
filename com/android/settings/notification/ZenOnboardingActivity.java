package com.android.settings.notification;

import android.view.View.OnClickListener;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.android.settings.overlay.FeatureFactory;
import android.app.NotificationManager$Policy;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Context;
import android.app.NotificationManager;
import com.android.internal.logging.MetricsLogger;
import android.widget.RadioButton;
import android.view.View;
import com.android.internal.annotations.VisibleForTesting;
import android.app.Activity;

public class ZenOnboardingActivity extends Activity
{
    @VisibleForTesting
    static final long ALWAYS_SHOW_THRESHOLD = 1209600000L;
    @VisibleForTesting
    static final String PREF_KEY_SUGGESTION_FIRST_DISPLAY_TIME = "pref_zen_suggestion_first_display_time_ms";
    View mKeepCurrentSetting;
    RadioButton mKeepCurrentSettingButton;
    private MetricsLogger mMetrics;
    View mNewSetting;
    RadioButton mNewSettingButton;
    private NotificationManager mNm;
    
    public static boolean isSuggestionComplete(final Context context) {
        return wasZenUpdated(context) || (!showSuggestion(context) && !withinShowTimeThreshold(context));
    }
    
    private static boolean showSuggestion(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "show_zen_settings_suggestion", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    private static boolean wasZenUpdated(final Context context) {
        final boolean allVisualEffectsSuppressed = NotificationManager$Policy.areAllVisualEffectsSuppressed(((NotificationManager)context.getSystemService((Class)NotificationManager.class)).getNotificationPolicy().suppressedVisualEffects);
        boolean b = true;
        if (allVisualEffectsSuppressed) {
            Settings.Global.putInt(context.getContentResolver(), "zen_settings_updated", 1);
        }
        if (Settings.Global.getInt(context.getContentResolver(), "zen_settings_updated", 0) == 0) {
            b = false;
        }
        return b;
    }
    
    private static boolean withinShowTimeThreshold(final Context context) {
        final SharedPreferences sharedPrefs = FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).getSharedPrefs(context);
        final long currentTimeMillis = System.currentTimeMillis();
        long long1;
        if (!sharedPrefs.contains("pref_zen_suggestion_first_display_time_ms")) {
            long1 = currentTimeMillis;
            sharedPrefs.edit().putLong("pref_zen_suggestion_first_display_time_ms", currentTimeMillis).commit();
        }
        else {
            long1 = sharedPrefs.getLong("pref_zen_suggestion_first_display_time_ms", -1L);
        }
        final boolean b = currentTimeMillis < 1209600000L + long1;
        final StringBuilder sb = new StringBuilder();
        sb.append("still show zen suggestion based on time: ");
        sb.append(b);
        Log.d("ZenOnboardingActivity", sb.toString());
        return b;
    }
    
    public void launchSettings(final View view) {
        this.mMetrics.action(1379);
        final Intent intent = new Intent("android.settings.ZEN_MODE_SETTINGS");
        intent.addFlags(268468224);
        this.startActivity(intent);
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setNotificationManager((NotificationManager)this.getSystemService((Class)NotificationManager.class));
        this.setMetricsLogger(new MetricsLogger());
        Settings.Global.putInt(this.getApplicationContext().getContentResolver(), "zen_settings_suggestion_viewed", 1);
        this.setupUI();
    }
    
    public void save(final View view) {
        final NotificationManager$Policy notificationPolicy = this.mNm.getNotificationPolicy();
        if (this.mNewSettingButton.isChecked()) {
            this.mNm.setNotificationPolicy(new NotificationManager$Policy(0x10 | notificationPolicy.priorityCategories, 2, notificationPolicy.priorityMessageSenders, NotificationManager$Policy.getAllSuppressedVisualEffects()));
            this.mMetrics.action(1378);
        }
        else {
            this.mMetrics.action(1406);
        }
        Settings.Global.putInt(this.getApplicationContext().getContentResolver(), "zen_settings_updated", 1);
        this.finishAndRemoveTask();
    }
    
    @VisibleForTesting
    protected void setMetricsLogger(final MetricsLogger mMetrics) {
        this.mMetrics = mMetrics;
    }
    
    @VisibleForTesting
    protected void setNotificationManager(final NotificationManager mNm) {
        this.mNm = mNm;
    }
    
    @VisibleForTesting
    protected void setupUI() {
        this.setContentView(2131558904);
        this.mNewSetting = this.findViewById(2131362855);
        this.mKeepCurrentSetting = this.findViewById(2131362851);
        this.mNewSettingButton = (RadioButton)this.findViewById(2131362856);
        this.mKeepCurrentSettingButton = (RadioButton)this.findViewById(2131362852);
        final View.OnClickListener view$OnClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ZenOnboardingActivity.this.mKeepCurrentSettingButton.setChecked(false);
                ZenOnboardingActivity.this.mNewSettingButton.setChecked(true);
            }
        };
        final View.OnClickListener view$OnClickListener2 = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ZenOnboardingActivity.this.mKeepCurrentSettingButton.setChecked(true);
                ZenOnboardingActivity.this.mNewSettingButton.setChecked(false);
            }
        };
        this.mNewSetting.setOnClickListener((View.OnClickListener)view$OnClickListener);
        this.mNewSettingButton.setOnClickListener((View.OnClickListener)view$OnClickListener);
        this.mKeepCurrentSetting.setOnClickListener((View.OnClickListener)view$OnClickListener2);
        this.mKeepCurrentSettingButton.setOnClickListener((View.OnClickListener)view$OnClickListener2);
        this.mKeepCurrentSettingButton.setChecked(true);
        this.mMetrics.visible(1380);
    }
}
