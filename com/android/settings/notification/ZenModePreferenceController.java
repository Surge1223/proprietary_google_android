package com.android.settings.notification;

import android.content.ContentResolver;
import android.provider.Settings;
import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ZenModePreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final String mKey;
    private SettingObserver mSettingObserver;
    private ZenModeSettings.SummaryBuilder mSummaryBuilder;
    
    public ZenModePreferenceController(final Context context, final Lifecycle lifecycle, final String mKey) {
        super(context);
        this.mSummaryBuilder = new ZenModeSettings.SummaryBuilder(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
        this.mKey = mKey;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mSettingObserver = new SettingObserver(preferenceScreen.findPreference(this.mKey));
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mKey;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.unregister(this.mContext.getContentResolver());
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver());
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (preference.isEnabled()) {
            preference.setSummary(this.mSummaryBuilder.getSoundSummary());
        }
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri ZEN_MODE_CONFIG_ETAG_URI;
        private final Uri ZEN_MODE_URI;
        private final Preference mPreference;
        
        public SettingObserver(final Preference mPreference) {
            super(new Handler());
            this.ZEN_MODE_URI = Settings.Global.getUriFor("zen_mode");
            this.ZEN_MODE_CONFIG_ETAG_URI = Settings.Global.getUriFor("zen_mode_config_etag");
            this.mPreference = mPreference;
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.ZEN_MODE_URI.equals((Object)uri)) {
                ZenModePreferenceController.this.updateState(this.mPreference);
            }
            if (this.ZEN_MODE_CONFIG_ETAG_URI.equals((Object)uri)) {
                ZenModePreferenceController.this.updateState(this.mPreference);
            }
        }
        
        public void register(final ContentResolver contentResolver) {
            contentResolver.registerContentObserver(this.ZEN_MODE_URI, false, (ContentObserver)this, -1);
            contentResolver.registerContentObserver(this.ZEN_MODE_CONFIG_ETAG_URI, false, (ContentObserver)this, -1);
        }
        
        public void unregister(final ContentResolver contentResolver) {
            contentResolver.unregisterContentObserver((ContentObserver)this);
        }
    }
}
