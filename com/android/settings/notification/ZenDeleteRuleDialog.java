package com.android.settings.notification;

import android.view.View;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.app.Fragment;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ZenDeleteRuleDialog extends InstrumentedDialogFragment
{
    protected static PositiveClickListener mPositiveClickListener;
    
    public static void show(final Fragment fragment, final String s, final String s2, final PositiveClickListener mPositiveClickListener) {
        final Bundle arguments = new Bundle();
        arguments.putString("zen_rule_name", s);
        arguments.putString("zen_rule_id", s2);
        ZenDeleteRuleDialog.mPositiveClickListener = mPositiveClickListener;
        final ZenDeleteRuleDialog zenDeleteRuleDialog = new ZenDeleteRuleDialog();
        zenDeleteRuleDialog.setArguments(arguments);
        zenDeleteRuleDialog.setTargetFragment(fragment, 0);
        zenDeleteRuleDialog.show(fragment.getFragmentManager(), "ZenDeleteRuleDialog");
    }
    
    @Override
    public int getMetricsCategory() {
        return 1266;
    }
    
    public Dialog onCreateDialog(Bundle arguments) {
        arguments = this.getArguments();
        final AlertDialog create = new AlertDialog$Builder(this.getContext()).setMessage((CharSequence)this.getString(2131890299, new Object[] { arguments.getString("zen_rule_name") })).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setPositiveButton(2131890298, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            final /* synthetic */ String val$id = arguments.getString("zen_rule_id");
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (arguments != null) {
                    ZenDeleteRuleDialog.mPositiveClickListener.onOk(this.val$id);
                }
            }
        }).create();
        final View viewById = create.findViewById(16908299);
        if (viewById != null) {
            viewById.setTextDirection(5);
        }
        return (Dialog)create;
    }
    
    public interface PositiveClickListener
    {
        void onOk(final String p0);
    }
}
