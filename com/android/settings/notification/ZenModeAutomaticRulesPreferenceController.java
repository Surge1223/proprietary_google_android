package com.android.settings.notification;

import android.app.AutomaticZenRule;
import java.util.Map;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Fragment;
import android.content.Context;
import android.support.v7.preference.PreferenceCategory;

public class ZenModeAutomaticRulesPreferenceController extends AbstractZenModeAutomaticRulePreferenceController
{
    protected PreferenceCategory mPreferenceCategory;
    
    public ZenModeAutomaticRulesPreferenceController(final Context context, final Fragment fragment, final Lifecycle lifecycle) {
        super(context, "zen_mode_automatic_rules", fragment, lifecycle);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreferenceCategory = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey())).setPersistent(false);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_automatic_rules";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.mPreferenceCategory.removeAll();
        final Map.Entry<String, AutomaticZenRule>[] sortedRules = this.sortedRules();
        for (int length = sortedRules.length, i = 0; i < length; ++i) {
            this.mPreferenceCategory.addPreference(new ZenRulePreference(this.mPreferenceCategory.getContext(), sortedRules[i], this.mParent, this.mMetricsFeatureProvider));
        }
    }
}
