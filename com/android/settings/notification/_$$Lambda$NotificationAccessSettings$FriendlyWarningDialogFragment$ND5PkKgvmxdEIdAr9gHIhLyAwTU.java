package com.android.settings.notification;

import android.app.Fragment;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.ComponentName;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$NotificationAccessSettings$FriendlyWarningDialogFragment$ND5PkKgvmxdEIdAr9gHIhLyAwTU implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        NotificationAccessSettings.FriendlyWarningDialogFragment.lambda$onCreateDialog$0(this.f$0, this.f$1, dialogInterface, n);
    }
}
