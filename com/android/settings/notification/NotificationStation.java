package com.android.settings.notification;

import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.DateTimeView;
import android.widget.ImageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.ComponentName;
import android.app.INotificationManager$Stub;
import android.os.ServiceManager;
import android.app.Activity;
import android.view.View;
import com.android.settings.Utils;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import java.util.Collections;
import android.content.pm.ApplicationInfo;
import java.util.ArrayList;
import java.util.List;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.res.Resources;
import java.util.Iterator;
import android.app.Notification$Action;
import android.os.Parcel;
import android.text.SpannableStringBuilder;
import android.service.notification.NotificationListenerService$Ranking;
import android.content.IntentSender;
import android.os.RemoteException;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.text.style.StyleSpan;
import android.text.SpannableString;
import android.app.Notification;
import android.service.notification.StatusBarNotification;
import android.service.notification.NotificationListenerService$RankingMap;
import android.content.pm.PackageManager;
import java.util.Comparator;
import android.app.INotificationManager;
import android.service.notification.NotificationListenerService;
import android.os.Handler;
import android.content.Context;
import com.android.settings.SettingsPreferenceFragment;

public class NotificationStation extends SettingsPreferenceFragment
{
    private static final String TAG;
    private Context mContext;
    private Handler mHandler;
    private final NotificationListenerService mListener;
    private INotificationManager mNoMan;
    private final Comparator<HistoricalNotificationInfo> mNotificationSorter;
    private PackageManager mPm;
    private NotificationListenerService$RankingMap mRanking;
    private Runnable mRefreshListRunnable;
    
    static {
        TAG = NotificationStation.class.getSimpleName();
    }
    
    public NotificationStation() {
        this.mRefreshListRunnable = new Runnable() {
            @Override
            public void run() {
                NotificationStation.this.refreshList();
            }
        };
        this.mListener = new NotificationListenerService() {
            public void onListenerConnected() {
                NotificationStation.this.mRanking = this.getCurrentRanking();
                int length;
                if (NotificationStation.this.mRanking == null) {
                    length = 0;
                }
                else {
                    length = NotificationStation.this.mRanking.getOrderedKeys().length;
                }
                logd("onListenerConnected with update for %d", new Object[] { length });
                NotificationStation.this.scheduleRefreshList();
            }
            
            public void onNotificationPosted(final StatusBarNotification statusBarNotification, final NotificationListenerService$RankingMap notificationListenerService$RankingMap) {
                final Notification notification = statusBarNotification.getNotification();
                int length = 0;
                if (notificationListenerService$RankingMap != null) {
                    length = notificationListenerService$RankingMap.getOrderedKeys().length;
                }
                logd("onNotificationPosted: %s, with update for %d", new Object[] { notification, length });
                NotificationStation.this.mRanking = notificationListenerService$RankingMap;
                NotificationStation.this.scheduleRefreshList();
            }
            
            public void onNotificationRankingUpdate(final NotificationListenerService$RankingMap notificationListenerService$RankingMap) {
                int length;
                if (notificationListenerService$RankingMap == null) {
                    length = 0;
                }
                else {
                    length = notificationListenerService$RankingMap.getOrderedKeys().length;
                }
                logd("onNotificationRankingUpdate with update for %d", new Object[] { length });
                NotificationStation.this.mRanking = notificationListenerService$RankingMap;
                NotificationStation.this.scheduleRefreshList();
            }
            
            public void onNotificationRemoved(final StatusBarNotification statusBarNotification, final NotificationListenerService$RankingMap notificationListenerService$RankingMap) {
                int length;
                if (notificationListenerService$RankingMap == null) {
                    length = 0;
                }
                else {
                    length = notificationListenerService$RankingMap.getOrderedKeys().length;
                }
                logd("onNotificationRankingUpdate with update for %d", new Object[] { length });
                NotificationStation.this.mRanking = notificationListenerService$RankingMap;
                NotificationStation.this.scheduleRefreshList();
            }
        };
        this.mNotificationSorter = new Comparator<HistoricalNotificationInfo>() {
            @Override
            public int compare(final HistoricalNotificationInfo historicalNotificationInfo, final HistoricalNotificationInfo historicalNotificationInfo2) {
                return Long.compare(historicalNotificationInfo2.timestamp, historicalNotificationInfo.timestamp);
            }
        };
    }
    
    private static CharSequence bold(final CharSequence charSequence) {
        if (charSequence.length() == 0) {
            return charSequence;
        }
        final SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan((Object)new StyleSpan(1), 0, charSequence.length(), 0);
        return (CharSequence)spannableString;
    }
    
    private static String formatPendingIntent(final PendingIntent pendingIntent) {
        final StringBuilder sb = new StringBuilder();
        final IntentSender intentSender = pendingIntent.getIntentSender();
        sb.append("Intent(pkg=");
        sb.append(intentSender.getCreatorPackage());
        try {
            if (ActivityManager.getService().isIntentSenderAnActivity(intentSender.getTarget())) {
                sb.append(" (activity)");
            }
        }
        catch (RemoteException ex) {}
        sb.append(")");
        return sb.toString();
    }
    
    private CharSequence generateExtraText(final StatusBarNotification statusBarNotification, final HistoricalNotificationInfo historicalNotificationInfo) {
        final NotificationListenerService$Ranking notificationListenerService$Ranking = new NotificationListenerService$Ranking();
        final Notification notification = statusBarNotification.getNotification();
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        final String string = this.getString(2131888468);
        spannableStringBuilder.append(bold(this.getString(2131888478))).append((CharSequence)string).append((CharSequence)historicalNotificationInfo.pkg).append((CharSequence)"\n").append(bold(this.getString(2131888476))).append((CharSequence)string).append((CharSequence)statusBarNotification.getKey());
        spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888474))).append((CharSequence)string).append((CharSequence)String.valueOf(notification.getSmallIcon()));
        spannableStringBuilder.append((CharSequence)"\n").append(bold("channelId")).append((CharSequence)string).append((CharSequence)String.valueOf(notification.getChannelId()));
        spannableStringBuilder.append((CharSequence)"\n").append(bold("postTime")).append((CharSequence)string).append((CharSequence)String.valueOf(statusBarNotification.getPostTime()));
        if (notification.getTimeoutAfter() != 0L) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold("timeoutAfter")).append((CharSequence)string).append((CharSequence)String.valueOf(notification.getTimeoutAfter()));
        }
        if (statusBarNotification.isGroup()) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888472))).append((CharSequence)string).append((CharSequence)String.valueOf(statusBarNotification.getGroupKey()));
            if (notification.isGroupSummary()) {
                spannableStringBuilder.append(bold(this.getString(2131888473)));
            }
        }
        spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888485))).append((CharSequence)string);
        if ((notification.defaults & 0x1) != 0x0) {
            spannableStringBuilder.append((CharSequence)this.getString(2131888466));
        }
        else if (notification.sound != null) {
            spannableStringBuilder.append((CharSequence)notification.sound.toString());
        }
        else {
            spannableStringBuilder.append((CharSequence)this.getString(2131888477));
        }
        spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888487))).append((CharSequence)string);
        if ((notification.defaults & 0x2) != 0x0) {
            spannableStringBuilder.append((CharSequence)this.getString(2131888466));
        }
        else if (notification.vibrate != null) {
            for (int i = 0; i < notification.vibrate.length; ++i) {
                if (i > 0) {
                    spannableStringBuilder.append(',');
                }
                spannableStringBuilder.append((CharSequence)String.valueOf(notification.vibrate[i]));
            }
        }
        else {
            spannableStringBuilder.append((CharSequence)this.getString(2131888477));
        }
        spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888488))).append((CharSequence)string).append((CharSequence)Notification.visibilityToString(notification.visibility));
        if (notification.publicVersion != null) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888481))).append((CharSequence)string).append((CharSequence)getTitleString(notification.publicVersion));
        }
        spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888480))).append((CharSequence)string).append((CharSequence)Notification.priorityToString(notification.priority));
        if (historicalNotificationInfo.active) {
            if (this.mRanking != null && this.mRanking.getRanking(statusBarNotification.getKey(), notificationListenerService$Ranking)) {
                spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888475))).append((CharSequence)string).append((CharSequence)NotificationListenerService$Ranking.importanceToString(notificationListenerService$Ranking.getImportance()));
                if (notificationListenerService$Ranking.getImportanceExplanation() != null) {
                    spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888469))).append((CharSequence)string).append(notificationListenerService$Ranking.getImportanceExplanation());
                }
                spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888463))).append((CharSequence)string).append((CharSequence)Boolean.toString(notificationListenerService$Ranking.canShowBadge()));
            }
            else if (this.mRanking == null) {
                spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888483)));
            }
            else {
                spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888482)));
            }
        }
        if (notification.contentIntent != null) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888464))).append((CharSequence)string).append((CharSequence)formatPendingIntent(notification.contentIntent));
        }
        if (notification.deleteIntent != null) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888467))).append((CharSequence)string).append((CharSequence)formatPendingIntent(notification.deleteIntent));
        }
        if (notification.fullScreenIntent != null) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888471))).append((CharSequence)string).append((CharSequence)formatPendingIntent(notification.fullScreenIntent));
        }
        if (notification.actions != null && notification.actions.length > 0) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888461)));
            for (int j = 0; j < notification.actions.length; ++j) {
                final Notification$Action notification$Action = notification.actions[j];
                spannableStringBuilder.append((CharSequence)"\n  ").append((CharSequence)String.valueOf(j)).append(' ').append(bold(this.getString(2131888486))).append((CharSequence)string).append(notification$Action.title);
                if (notification$Action.actionIntent != null) {
                    spannableStringBuilder.append((CharSequence)"\n    ").append(bold(this.getString(2131888464))).append((CharSequence)string).append((CharSequence)formatPendingIntent(notification$Action.actionIntent));
                }
                if (notification$Action.getRemoteInputs() != null) {
                    spannableStringBuilder.append((CharSequence)"\n    ").append(bold(this.getString(2131888484))).append((CharSequence)string).append((CharSequence)String.valueOf(notification$Action.getRemoteInputs().length));
                }
            }
        }
        if (notification.contentView != null) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888465))).append((CharSequence)string).append((CharSequence)notification.contentView.toString());
        }
        if (notification.extras != null && notification.extras.size() > 0) {
            spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888470)));
            for (final String s : notification.extras.keySet()) {
                String s3;
                final String s2 = s3 = String.valueOf(notification.extras.get(s));
                if (s2.length() > 100) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(s2.substring(0, 100));
                    sb.append("...");
                    s3 = sb.toString();
                }
                spannableStringBuilder.append((CharSequence)"\n  ").append((CharSequence)s).append((CharSequence)string).append((CharSequence)s3);
            }
        }
        final Parcel obtain = Parcel.obtain();
        notification.writeToParcel(obtain, 0);
        spannableStringBuilder.append((CharSequence)"\n").append(bold(this.getString(2131888479))).append((CharSequence)string).append((CharSequence)String.valueOf(obtain.dataPosition())).append(' ').append(bold(this.getString(2131888462))).append((CharSequence)string).append((CharSequence)String.valueOf(obtain.getBlobAshmemSize())).append((CharSequence)"\n");
        return (CharSequence)spannableStringBuilder;
    }
    
    private Resources getResourcesForUserPackage(String s, final int n) {
        if (s != null) {
            int n2;
            if ((n2 = n) == -1) {
                n2 = 0;
            }
            try {
                s = (String)this.mPm.getResourcesForApplicationAsUser(s, n2);
                return (Resources)s;
            }
            catch (PackageManager$NameNotFoundException ex) {
                final String tag = NotificationStation.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Icon package not found: ");
                sb.append(s);
                Log.e(tag, sb.toString(), (Throwable)ex);
                return null;
            }
        }
        s = (String)this.mContext.getResources();
        return (Resources)s;
    }
    
    private static String getTitleString(final Notification notification) {
        CharSequence charSequence = null;
        if (notification.extras != null && TextUtils.isEmpty(charSequence = notification.extras.getCharSequence("android.title"))) {
            charSequence = notification.extras.getCharSequence("android.text");
        }
        CharSequence tickerText = charSequence;
        if (TextUtils.isEmpty(charSequence)) {
            tickerText = charSequence;
            if (!TextUtils.isEmpty(notification.tickerText)) {
                tickerText = notification.tickerText;
            }
        }
        return String.valueOf(tickerText);
    }
    
    private Drawable loadIconDrawable(String value, final int n, final int n2) {
        final Resources resourcesForUserPackage = this.getResourcesForUserPackage(value, n);
        if (n2 == 0) {
            return null;
        }
        try {
            return resourcesForUserPackage.getDrawable(n2, (Resources.Theme)null);
        }
        catch (RuntimeException ex) {
            final String tag = NotificationStation.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Icon not found in ");
            if (value != null) {
                value = (String)n2;
            }
            else {
                value = "<system>";
            }
            sb.append((Object)value);
            sb.append(": ");
            sb.append(Integer.toHexString(n2));
            Log.w(tag, sb.toString(), (Throwable)ex);
            return null;
        }
    }
    
    private List<HistoricalNotificationInfo> loadNotifications() {
        final int currentUser = ActivityManager.getCurrentUser();
        StatusBarNotification[] activeNotifications = null;
        Label_0504: {
            try {
                activeNotifications = this.mNoMan.getActiveNotifications(this.mContext.getPackageName());
                final StatusBarNotification[] historicalNotifications = this.mNoMan.getHistoricalNotifications(this.mContext.getPackageName(), 50);
                final ArrayList list = new ArrayList<HistoricalNotificationInfo>(((Throwable)(Object)activeNotifications).length + historicalNotifications.length);
                final StatusBarNotification[][] array = new StatusBarNotification[2][];
                int n = 0;
                array[0] = activeNotifications;
                array[1] = historicalNotifications;
                for (final StatusBarNotification[] array2 : array) {
                    final int length2 = array2.length;
                    int j = n;
                    while (j < length2) {
                        final StatusBarNotification statusBarNotification = array2[j];
                        int n2;
                        if (statusBarNotification.getUserId() != -1) {
                            n2 = 1;
                        }
                        else {
                            n2 = n;
                        }
                        int n3;
                        if (statusBarNotification.getUserId() != currentUser) {
                            n3 = 1;
                        }
                        else {
                            n3 = n;
                        }
                        Label_0476: {
                            if ((n2 & n3) != 0x0) {
                                break Label_0476;
                            }
                            final Notification notification = statusBarNotification.getNotification();
                            final HistoricalNotificationInfo historicalNotificationInfo = new HistoricalNotificationInfo();
                            historicalNotificationInfo.pkg = statusBarNotification.getPackageName();
                            historicalNotificationInfo.user = statusBarNotification.getUserId();
                            final String pkg = historicalNotificationInfo.pkg;
                            final int user = historicalNotificationInfo.user;
                            try {
                                historicalNotificationInfo.icon = this.loadIconDrawable(pkg, user, notification.icon);
                                historicalNotificationInfo.pkgicon = this.loadPackageIconDrawable(historicalNotificationInfo.pkg, historicalNotificationInfo.user);
                                historicalNotificationInfo.pkgname = this.loadPackageName(historicalNotificationInfo.pkg);
                                historicalNotificationInfo.title = getTitleString(notification);
                                if (TextUtils.isEmpty(historicalNotificationInfo.title)) {
                                    historicalNotificationInfo.title = this.getString(2131888489);
                                }
                                historicalNotificationInfo.timestamp = statusBarNotification.getPostTime();
                                historicalNotificationInfo.priority = notification.priority;
                                historicalNotificationInfo.channel = notification.getChannelId();
                                historicalNotificationInfo.key = statusBarNotification.getKey();
                                historicalNotificationInfo.active = (array2 == activeNotifications);
                                historicalNotificationInfo.extra = this.generateExtraText(statusBarNotification, historicalNotificationInfo);
                                final long timestamp = historicalNotificationInfo.timestamp;
                                n = 0;
                                logd("   [%d] %s: %s", timestamp, historicalNotificationInfo.pkg, historicalNotificationInfo.title);
                                list.add(historicalNotificationInfo);
                                ++j;
                                continue;
                            }
                            catch (RemoteException activeNotifications) {
                                break Label_0504;
                            }
                        }
                        break;
                    }
                }
                return (List<HistoricalNotificationInfo>)list;
            }
            catch (RemoteException ex) {}
        }
        Log.e(NotificationStation.TAG, "Cannot load Notifications: ", (Throwable)(Object)activeNotifications);
        return null;
    }
    
    private Drawable loadPackageIconDrawable(final String s, final int n) {
        final Drawable drawable = null;
        Drawable applicationIcon;
        try {
            applicationIcon = this.mPm.getApplicationIcon(s);
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e(NotificationStation.TAG, "Cannot get application icon", (Throwable)ex);
            applicationIcon = drawable;
        }
        return applicationIcon;
    }
    
    private CharSequence loadPackageName(final String s) {
        try {
            final ApplicationInfo applicationInfo = this.mPm.getApplicationInfo(s, 4194304);
            if (applicationInfo != null) {
                return this.mPm.getApplicationLabel(applicationInfo);
            }
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e(NotificationStation.TAG, "Cannot load package name", (Throwable)ex);
        }
        return s;
    }
    
    private static void logd(String format, final Object... array) {
        final String tag = NotificationStation.TAG;
        if (array != null) {
            if (array.length != 0) {
                format = String.format(format, array);
            }
        }
        Log.d(tag, format);
    }
    
    private void refreshList() {
        final List<HistoricalNotificationInfo> loadNotifications = this.loadNotifications();
        if (loadNotifications != null) {
            final int size = loadNotifications.size();
            int i = 0;
            logd("adding %d infos", size);
            Collections.sort((List<Object>)loadNotifications, (Comparator<? super Object>)this.mNotificationSorter);
            if (this.getPreferenceScreen() == null) {
                this.setPreferenceScreen(this.getPreferenceManager().createPreferenceScreen(this.getContext()));
            }
            this.getPreferenceScreen().removeAll();
            while (i < size) {
                this.getPreferenceScreen().addPreference(new HistoricalNotificationPreference(this.getPrefContext(), (HistoricalNotificationInfo)loadNotifications.get(i)));
                ++i;
            }
        }
    }
    
    private void scheduleRefreshList() {
        if (this.mHandler != null) {
            this.mHandler.removeCallbacks(this.mRefreshListRunnable);
            this.mHandler.postDelayed(this.mRefreshListRunnable, 100L);
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 75;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        logd("onActivityCreated(%s)", bundle);
        super.onActivityCreated(bundle);
        Utils.forceCustomPadding((View)this.getListView(), false);
    }
    
    public void onAttach(final Activity mContext) {
        logd("onAttach(%s)", mContext.getClass().getSimpleName());
        super.onAttach(mContext);
        this.mHandler = new Handler(mContext.getMainLooper());
        this.mContext = (Context)mContext;
        this.mPm = this.mContext.getPackageManager();
        this.mNoMan = INotificationManager$Stub.asInterface(ServiceManager.getService("notification"));
    }
    
    @Override
    public void onDetach() {
        logd("onDetach()", new Object[0]);
        this.mHandler.removeCallbacks(this.mRefreshListRunnable);
        this.mHandler = null;
        super.onDetach();
    }
    
    @Override
    public void onPause() {
        try {
            this.mListener.unregisterAsSystemService();
        }
        catch (RemoteException ex) {
            Log.e(NotificationStation.TAG, "Cannot unregister listener", (Throwable)ex);
        }
        super.onPause();
    }
    
    @Override
    public void onResume() {
        logd("onResume()", new Object[0]);
        super.onResume();
        try {
            this.mListener.registerAsSystemService(this.mContext, new ComponentName(this.mContext.getPackageName(), this.getClass().getCanonicalName()), ActivityManager.getCurrentUser());
        }
        catch (RemoteException ex) {
            Log.e(NotificationStation.TAG, "Cannot register listener", (Throwable)ex);
        }
        this.refreshList();
    }
    
    private static class HistoricalNotificationInfo
    {
        public boolean active;
        public String channel;
        public CharSequence extra;
        public Drawable icon;
        public String key;
        public String pkg;
        public Drawable pkgicon;
        public CharSequence pkgname;
        public int priority;
        public long timestamp;
        public CharSequence title;
        public int user;
    }
    
    private static class HistoricalNotificationPreference extends Preference
    {
        private static long sLastExpandedTimestamp;
        private final HistoricalNotificationInfo mInfo;
        
        public HistoricalNotificationPreference(final Context context, final HistoricalNotificationInfo mInfo) {
            super(context);
            this.setLayoutResource(2131558624);
            this.mInfo = mInfo;
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            super.onBindViewHolder(preferenceViewHolder);
            if (this.mInfo.icon != null) {
                ((ImageView)preferenceViewHolder.findViewById(R.id.icon)).setImageDrawable(this.mInfo.icon);
            }
            if (this.mInfo.pkgicon != null) {
                ((ImageView)preferenceViewHolder.findViewById(2131362455)).setImageDrawable(this.mInfo.pkgicon);
            }
            ((DateTimeView)preferenceViewHolder.findViewById(2131362737)).setTime(this.mInfo.timestamp);
            ((TextView)preferenceViewHolder.findViewById(R.id.title)).setText(this.mInfo.title);
            ((TextView)preferenceViewHolder.findViewById(2131362456)).setText(this.mInfo.pkgname);
            final TextView textView = (TextView)preferenceViewHolder.findViewById(2131362139);
            textView.setText(this.mInfo.extra);
            int visibility;
            if (this.mInfo.timestamp == HistoricalNotificationPreference.sLastExpandedTimestamp) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            textView.setVisibility(visibility);
            preferenceViewHolder.itemView.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    final TextView val$extra = textView;
                    int visibility;
                    if (textView.getVisibility() == 0) {
                        visibility = 8;
                    }
                    else {
                        visibility = 0;
                    }
                    val$extra.setVisibility(visibility);
                    HistoricalNotificationPreference.sLastExpandedTimestamp = HistoricalNotificationPreference.this.mInfo.timestamp;
                }
            });
            final View itemView = preferenceViewHolder.itemView;
            float alpha;
            if (this.mInfo.active) {
                alpha = 1.0f;
            }
            else {
                alpha = 0.5f;
            }
            itemView.setAlpha(alpha);
        }
        
        @Override
        public void performClick() {
        }
    }
}
