package com.android.settings.notification;

import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertController;
import android.content.Context;
import com.android.internal.app.AlertController$AlertParams;
import android.os.Bundle;
import com.android.internal.app.AlertActivity;
import android.view.accessibility.AccessibilityEvent;
import android.content.pm.PackageManager;
import android.util.Slog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.app.Activity;

public class NotificationAccessConfirmationActivity extends Activity implements DialogInterface
{
    private ComponentName mComponentName;
    private NotificationManager mNm;
    private int mUserId;
    
    private void onAllow() {
        try {
            if (!"android.permission.BIND_NOTIFICATION_LISTENER_SERVICE".equals(this.getPackageManager().getServiceInfo(this.mComponentName, 0).permission)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Service ");
                sb.append(this.mComponentName);
                sb.append(" lacks permission ");
                sb.append("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE");
                Slog.e("NotificationAccessConfirmationActivity", sb.toString());
                return;
            }
            this.mNm.setNotificationListenerAccessGranted(this.mComponentName, true);
            this.finish();
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to get service info for ");
            sb2.append(this.mComponentName);
            Slog.e("NotificationAccessConfirmationActivity", sb2.toString(), (Throwable)ex);
        }
    }
    
    public void cancel() {
        this.finish();
    }
    
    public void dismiss() {
        if (!this.isFinishing()) {
            this.finish();
        }
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        return AlertActivity.dispatchPopulateAccessibilityEvent((Activity)this, accessibilityEvent);
    }
    
    public void onBackPressed() {
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mNm = (NotificationManager)this.getSystemService("notification");
        this.mComponentName = (ComponentName)this.getIntent().getParcelableExtra("component_name");
        this.mUserId = this.getIntent().getIntExtra("user_id", -10000);
        final String stringExtra = this.getIntent().getStringExtra("package_title");
        final AlertController$AlertParams alertController$AlertParams = new AlertController$AlertParams((Context)this);
        alertController$AlertParams.mTitle = this.getString(2131888460, new Object[] { stringExtra });
        alertController$AlertParams.mMessage = this.getString(2131888459, new Object[] { stringExtra });
        alertController$AlertParams.mPositiveButtonText = this.getString(2131886285);
        alertController$AlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)new _$$Lambda$NotificationAccessConfirmationActivity$UvveyFMEwlZ6m4ViLmcVExulBE8(this);
        alertController$AlertParams.mNegativeButtonText = this.getString(2131887370);
        alertController$AlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)new _$$Lambda$NotificationAccessConfirmationActivity$hd7i7CSD_dVpjvK__hXE8eDM2I0(this);
        AlertController.create((Context)this, (DialogInterface)this, this.getWindow()).installContent(alertController$AlertParams);
        this.getWindow().setCloseOnTouchOutside(false);
    }
    
    public void onPause() {
        this.getWindow().clearFlags(524288);
        super.onPause();
    }
    
    public void onResume() {
        super.onResume();
        this.getWindow().addFlags(524288);
    }
}
