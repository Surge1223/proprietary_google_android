package com.android.settings.notification;

import android.app.Activity;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;
import android.content.res.Resources;
import android.provider.Settings;
import android.provider.Settings;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.DropDownPreference;

public class SettingPref
{
    protected final int mDefault;
    protected DropDownPreference mDropDown;
    private final String mKey;
    protected final String mSetting;
    protected TwoStatePreference mTwoState;
    protected final int mType;
    private final Uri mUri;
    private final int[] mValues;
    
    public SettingPref(final int mType, final String mKey, final String mSetting, final int mDefault, final int... mValues) {
        this.mType = mType;
        this.mKey = mKey;
        this.mSetting = mSetting;
        this.mDefault = mDefault;
        this.mValues = mValues;
        this.mUri = getUriFor(this.mType, this.mSetting);
    }
    
    protected static int getInt(final int n, final ContentResolver contentResolver, final String s, final int n2) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 2: {
                return Settings.System.getInt(contentResolver, s, n2);
            }
            case 1: {
                return Settings.Global.getInt(contentResolver, s, n2);
            }
        }
    }
    
    private static Uri getUriFor(final int n, final String s) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 2: {
                return Settings.System.getUriFor(s);
            }
            case 1: {
                return Settings.Global.getUriFor(s);
            }
        }
    }
    
    protected static boolean putInt(final int n, final ContentResolver contentResolver, final String s, final int n2) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 2: {
                return Settings.System.putInt(contentResolver, s, n2);
            }
            case 1: {
                return Settings.Global.putInt(contentResolver, s, n2);
            }
        }
    }
    
    protected String getCaption(final Resources resources, final int n) {
        throw new UnsupportedOperationException();
    }
    
    public String getKey() {
        return this.mKey;
    }
    
    public Uri getUri() {
        return this.mUri;
    }
    
    public Preference init(final SettingsPreferenceFragment settingsPreferenceFragment) {
        final Activity activity = settingsPreferenceFragment.getActivity();
        DropDownPreference preference2;
        final Preference preference = preference2 = (DropDownPreference)settingsPreferenceFragment.getPreferenceScreen().findPreference(this.mKey);
        if (preference != null) {
            preference2 = (DropDownPreference)preference;
            if (!this.isApplicable((Context)activity)) {
                settingsPreferenceFragment.getPreferenceScreen().removePreference(preference);
                preference2 = null;
            }
        }
        if (preference2 instanceof TwoStatePreference) {
            this.mTwoState = (TwoStatePreference)preference2;
        }
        else if (preference2 instanceof DropDownPreference) {
            this.mDropDown = preference2;
            final CharSequence[] entries = new CharSequence[this.mValues.length];
            final CharSequence[] entryValues = new CharSequence[this.mValues.length];
            for (int i = 0; i < this.mValues.length; ++i) {
                entries[i] = this.getCaption(((Context)activity).getResources(), this.mValues[i]);
                entryValues[i] = Integer.toString(this.mValues[i]);
            }
            this.mDropDown.setEntries(entries);
            this.mDropDown.setEntryValues(entryValues);
        }
        this.update((Context)activity);
        if (this.mTwoState != null) {
            preference2.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(final Preference preference, final Object o) {
                    SettingPref.this.setSetting(activity, (int)(((boolean)o) ? 1 : 0));
                    return true;
                }
            });
            return this.mTwoState;
        }
        if (this.mDropDown != null) {
            preference2.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(final Preference preference, final Object o) {
                    return SettingPref.this.setSetting(activity, Integer.parseInt((String)o));
                }
            });
            return this.mDropDown;
        }
        return null;
    }
    
    public boolean isApplicable(final Context context) {
        return true;
    }
    
    protected boolean setSetting(final Context context, final int n) {
        return putInt(this.mType, context.getContentResolver(), this.mSetting, n);
    }
    
    public void update(final Context context) {
        final int int1 = getInt(this.mType, context.getContentResolver(), this.mSetting, this.mDefault);
        if (this.mTwoState != null) {
            this.mTwoState.setChecked(int1 != 0);
        }
        else if (this.mDropDown != null) {
            this.mDropDown.setValue(Integer.toString(int1));
        }
    }
}
