package com.android.settings.notification;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.support.v7.preference.PreferenceViewHolder;
import android.service.notification.ZenModeConfig;
import android.util.Pair;
import android.view.View;
import android.app.AutomaticZenRule;
import java.util.Map;
import com.android.settings.utils.ZenServiceListing;
import android.support.v7.preference.Preference;
import android.content.pm.PackageManager;
import android.app.Fragment;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.view.View.OnClickListener;
import android.content.Context;
import com.android.settings.utils.ManagedServiceSettings;
import com.android.settingslib.TwoTargetPreference;

public class ZenRulePreference extends TwoTargetPreference
{
    private static final ManagedServiceSettings.Config CONFIG;
    boolean appExists;
    final ZenModeBackend mBackend;
    final Context mContext;
    private final View.OnClickListener mDeleteListener;
    final String mId;
    final MetricsFeatureProvider mMetricsFeatureProvider;
    final CharSequence mName;
    final Fragment mParent;
    final PackageManager mPm;
    final Preference mPref;
    final ZenServiceListing mServiceListing;
    
    static {
        CONFIG = ZenModeAutomationSettings.getConditionProviderConfig();
    }
    
    public ZenRulePreference(final Context mContext, final Map.Entry<String, AutomaticZenRule> entry, final Fragment mParent, final MetricsFeatureProvider mMetricsFeatureProvider) {
        super(mContext);
        this.mDeleteListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ZenRulePreference.this.showDeleteRuleDialog(ZenRulePreference.this.mParent, ZenRulePreference.this.mId, ZenRulePreference.this.mName.toString());
            }
        };
        this.mBackend = ZenModeBackend.getInstance(mContext);
        this.mContext = mContext;
        final AutomaticZenRule attributes = entry.getValue();
        this.mName = attributes.getName();
        this.mId = entry.getKey();
        this.mParent = mParent;
        this.mPm = this.mContext.getPackageManager();
        (this.mServiceListing = new ZenServiceListing(this.mContext, ZenRulePreference.CONFIG)).reloadApprovedServices();
        this.mPref = this;
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        this.setAttributes(attributes);
    }
    
    private String computeRuleSummary(final AutomaticZenRule automaticZenRule, final boolean b, final CharSequence charSequence) {
        String s;
        if (automaticZenRule != null && automaticZenRule.isEnabled()) {
            s = this.mContext.getResources().getString(2131889422);
        }
        else {
            s = this.mContext.getResources().getString(2131889421);
        }
        return s;
    }
    
    private void showDeleteRuleDialog(final Fragment fragment, final String s, final String s2) {
        ZenDeleteRuleDialog.show(fragment, s2, s, (ZenDeleteRuleDialog.PositiveClickListener)new ZenDeleteRuleDialog.PositiveClickListener() {
            @Override
            public void onOk(final String s) {
                ZenRulePreference.this.mMetricsFeatureProvider.action(ZenRulePreference.this.mContext, 175, (Pair<Integer, Object>[])new Pair[0]);
                ZenRulePreference.this.mBackend.removeZenRule(s);
            }
        });
    }
    
    @Override
    protected int getSecondTargetResId() {
        if (this.mId != null && ZenModeConfig.DEFAULT_RULE_IDS.contains(this.mId)) {
            return 0;
        }
        return 2131558908;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362063);
        if (viewById != null) {
            viewById.setOnClickListener(this.mDeleteListener);
        }
    }
    
    protected void setAttributes(final AutomaticZenRule automaticZenRule) {
        final boolean validScheduleConditionId = ZenModeConfig.isValidScheduleConditionId(automaticZenRule.getConditionId());
        final boolean validEventConditionId = ZenModeConfig.isValidEventConditionId(automaticZenRule.getConditionId());
        final boolean b = true;
        boolean b2;
        if (!validScheduleConditionId && !validEventConditionId) {
            b2 = false;
        }
        else {
            b2 = true;
        }
        try {
            this.setSummary(this.computeRuleSummary(automaticZenRule, b2, this.mPm.getApplicationInfo(automaticZenRule.getOwner().getPackageName(), 0).loadLabel(this.mPm)));
            this.appExists = true;
            this.setTitle(automaticZenRule.getName());
            this.setPersistent(false);
            String s;
            if (validScheduleConditionId) {
                s = "android.settings.ZEN_MODE_SCHEDULE_RULE_SETTINGS";
            }
            else if (validEventConditionId) {
                s = "android.settings.ZEN_MODE_EVENT_RULE_SETTINGS";
            }
            else {
                s = "";
            }
            final ComponentName settingsActivity = AbstractZenModeAutomaticRulePreferenceController.getSettingsActivity(this.mServiceListing.findService(automaticZenRule.getOwner()));
            this.setIntent(AbstractZenModeAutomaticRulePreferenceController.getRuleIntent(s, settingsActivity, this.mId));
            boolean selectable = b;
            if (settingsActivity == null) {
                selectable = (b2 && b);
            }
            this.setSelectable(selectable);
            this.setKey(this.mId);
        }
        catch (PackageManager$NameNotFoundException ex) {
            this.appExists = false;
        }
    }
}
