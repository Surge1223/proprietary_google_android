package com.android.settings.notification;

import android.support.v7.preference.Preference;
import java.util.Iterator;
import android.net.Uri;
import android.service.notification.ZenModeConfig;
import android.service.notification.ZenModeConfig$ZenRule;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class ZenModeSettingsFooterPreferenceController extends AbstractZenModePreferenceController
{
    public ZenModeSettingsFooterPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "footer_preference", lifecycle);
    }
    
    protected String getFooterText() {
        final ZenModeConfig zenModeConfig = this.getZenModeConfig();
        final String s = "";
        final long n = -1L;
        String s2 = s;
        long n2 = n;
        if (zenModeConfig.manualRule != null) {
            final Uri conditionId = zenModeConfig.manualRule.conditionId;
            if (zenModeConfig.manualRule.enabler != null) {
                final String ownerCaption = ZenModeSettingsFooterPreferenceController.mZenModeConfigWrapper.getOwnerCaption(zenModeConfig.manualRule.enabler);
                s2 = s;
                if (!ownerCaption.isEmpty()) {
                    s2 = this.mContext.getString(2131890372, new Object[] { ownerCaption });
                }
                n2 = n;
            }
            else {
                if (conditionId == null) {
                    return this.mContext.getString(2131890374);
                }
                final long manualRuleTime = ZenModeSettingsFooterPreferenceController.mZenModeConfigWrapper.parseManualRuleTime(conditionId);
                s2 = s;
                n2 = manualRuleTime;
                if (manualRuleTime > 0L) {
                    s2 = this.mContext.getString(2131890373, new Object[] { ZenModeSettingsFooterPreferenceController.mZenModeConfigWrapper.getFormattedTime(manualRuleTime, this.mContext.getUserId()) });
                    n2 = manualRuleTime;
                }
            }
        }
        final Iterator iterator = zenModeConfig.automaticRules.values().iterator();
        long n3 = n2;
        String s3 = s2;
        while (iterator.hasNext()) {
            final ZenModeConfig$ZenRule zenModeConfig$ZenRule = iterator.next();
            String string = s3;
            long n4 = n3;
            if (zenModeConfig$ZenRule.isAutomaticActive()) {
                if (!ZenModeSettingsFooterPreferenceController.mZenModeConfigWrapper.isTimeRule(zenModeConfig$ZenRule.conditionId)) {
                    return this.mContext.getString(2131890371, new Object[] { zenModeConfig$ZenRule.name });
                }
                final long automaticRuleEndTime = ZenModeSettingsFooterPreferenceController.mZenModeConfigWrapper.parseAutomaticRuleEndTime(zenModeConfig$ZenRule.conditionId);
                string = s3;
                n4 = n3;
                if (automaticRuleEndTime > n3) {
                    n4 = automaticRuleEndTime;
                    string = this.mContext.getString(2131890371, new Object[] { zenModeConfig$ZenRule.name });
                }
            }
            s3 = string;
            n3 = n4;
        }
        return s3;
    }
    
    @Override
    public String getPreferenceKey() {
        return "footer_preference";
    }
    
    @Override
    public boolean isAvailable() {
        switch (this.getZenMode()) {
            default: {
                return false;
            }
            case 1:
            case 2:
            case 3: {
                return true;
            }
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final boolean available = this.isAvailable();
        preference.setVisible(available);
        if (available) {
            preference.setTitle(this.getFooterText());
        }
    }
}
