package com.android.settings.notification;

import com.android.internal.annotations.VisibleForTesting;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;

public class ZenModeCallsPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceChangeListener
{
    private final ZenModeBackend mBackend;
    private final String[] mListValues;
    private ListPreference mPreference;
    
    public ZenModeCallsPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "zen_mode_calls", lifecycle);
        this.mBackend = ZenModeBackend.getInstance(context);
        this.mListValues = context.getResources().getStringArray(2130903206);
    }
    
    private void updateFromContactsValue(final Preference preference) {
        this.mPreference = (ListPreference)preference;
        switch (this.getZenMode()) {
            default: {
                preference.setEnabled(true);
                preference.setSummary(this.mBackend.getContactsSummary(8));
                this.mPreference.setValue(this.mListValues[this.getIndexOfSendersValue(ZenModeBackend.getKeyFromSetting(this.mBackend.getPriorityCallSenders()))]);
                break;
            }
            case 2:
            case 3: {
                this.mPreference.setEnabled(false);
                this.mPreference.setValue("zen_mode_from_none");
                this.mPreference.setSummary(this.mBackend.getContactsSummary(-1));
                break;
            }
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (ListPreference)preferenceScreen.findPreference("zen_mode_calls");
    }
    
    @VisibleForTesting
    protected int getIndexOfSendersValue(final String s) {
        for (int i = 0; i < this.mListValues.length; ++i) {
            if (TextUtils.equals((CharSequence)s, (CharSequence)this.mListValues[i])) {
                return i;
            }
        }
        return 3;
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_calls";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mBackend.saveSenders(8, ZenModeBackend.getSettingFromPrefKey(o.toString()));
        this.updateFromContactsValue(preference);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.updateFromContactsValue(preference);
    }
}
