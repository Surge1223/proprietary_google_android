package com.android.settings.notification;

import android.os.Vibrator;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class VibrateOnTouchPreferenceController extends SettingPrefController
{
    public VibrateOnTouchPreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(2, "vibrate_on_touch", "haptic_feedback_enabled", 0, new int[0]) {
            @Override
            public boolean isApplicable(final Context context) {
                return hasHaptic(context);
            }
        };
    }
    
    private static boolean hasHaptic(final Context context) {
        final Vibrator vibrator = (Vibrator)context.getSystemService("vibrator");
        return vibrator != null && vibrator.hasVibrator();
    }
}
