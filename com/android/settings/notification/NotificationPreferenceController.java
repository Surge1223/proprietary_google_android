package com.android.settings.notification;

import java.util.Objects;
import android.util.Log;
import android.os.UserManager;
import android.content.pm.PackageManager;
import android.app.NotificationManager;
import android.content.Context;
import android.app.NotificationChannelGroup;
import android.app.NotificationChannel;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class NotificationPreferenceController extends AbstractPreferenceController
{
    protected RestrictedLockUtils.EnforcedAdmin mAdmin;
    protected NotificationBackend.AppRow mAppRow;
    protected final NotificationBackend mBackend;
    protected NotificationChannel mChannel;
    protected NotificationChannelGroup mChannelGroup;
    protected final Context mContext;
    protected final NotificationManager mNm;
    protected final PackageManager mPm;
    protected final UserManager mUm;
    
    public NotificationPreferenceController(final Context mContext, final NotificationBackend mBackend) {
        super(mContext);
        this.mContext = mContext;
        this.mNm = (NotificationManager)this.mContext.getSystemService("notification");
        this.mBackend = mBackend;
        this.mUm = (UserManager)this.mContext.getSystemService("user");
        this.mPm = this.mContext.getPackageManager();
    }
    
    protected boolean checkCanBeVisible(final int n) {
        final NotificationChannel mChannel = this.mChannel;
        boolean b = false;
        if (mChannel == null) {
            Log.w("ChannelPrefContr", "No channel");
            return false;
        }
        final int importance = this.mChannel.getImportance();
        if (importance == -1000) {
            return true;
        }
        if (importance >= n) {
            b = true;
        }
        return b;
    }
    
    protected boolean hasValidGroup() {
        return this.mChannelGroup != null;
    }
    
    @Override
    public boolean isAvailable() {
        final NotificationBackend.AppRow mAppRow = this.mAppRow;
        boolean b = false;
        if (mAppRow == null) {
            return false;
        }
        if (this.mAppRow.banned) {
            return false;
        }
        if (this.mChannel != null) {
            if (this.mChannel.getImportance() != 0) {
                b = true;
            }
            return b;
        }
        return this.mChannelGroup == null || (this.mChannelGroup.isBlocked() ^ true);
    }
    
    protected boolean isChannelBlockable() {
        final NotificationChannel mChannel = this.mChannel;
        boolean b = false;
        if (mChannel == null || this.mAppRow == null) {
            return false;
        }
        if (!this.mAppRow.systemApp) {
            return true;
        }
        if (this.mChannel.isBlockableSystem() || this.mChannel.getImportance() == 0) {
            b = true;
        }
        return b;
    }
    
    protected boolean isChannelConfigurable() {
        return this.mChannel != null && this.mAppRow != null && (Objects.equals(this.mChannel.getId(), this.mAppRow.lockedChannelId) ^ true);
    }
    
    protected boolean isChannelGroupBlockable() {
        return this.mChannelGroup != null && this.mAppRow != null && (!this.mAppRow.systemApp || this.mChannelGroup.isBlocked());
    }
    
    protected final boolean isDefaultChannel() {
        return this.mChannel != null && Objects.equals("miscellaneous", this.mChannel.getId());
    }
    
    protected void onResume(final NotificationBackend.AppRow mAppRow, final NotificationChannel mChannel, final NotificationChannelGroup mChannelGroup, final RestrictedLockUtils.EnforcedAdmin mAdmin) {
        this.mAppRow = mAppRow;
        this.mChannel = mChannel;
        this.mChannelGroup = mChannelGroup;
        this.mAdmin = mAdmin;
    }
    
    protected void saveChannel() {
        if (this.mChannel != null && this.mAppRow != null) {
            this.mBackend.updateChannel(this.mAppRow.pkg, this.mAppRow.uid, this.mChannel);
        }
    }
}
