package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.widget.Switch;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Fragment;
import android.content.Context;
import android.app.AutomaticZenRule;
import com.android.settings.widget.SwitchBar;

public class ZenAutomaticRuleSwitchPreferenceController extends AbstractZenModeAutomaticRulePreferenceController implements OnSwitchChangeListener
{
    private String mId;
    private AutomaticZenRule mRule;
    private SwitchBar mSwitchBar;
    
    public ZenAutomaticRuleSwitchPreferenceController(final Context context, final Fragment fragment, final Lifecycle lifecycle) {
        super(context, "zen_automatic_rule_switch", fragment, lifecycle);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mSwitchBar = ((LayoutPreference)preferenceScreen.findPreference("zen_automatic_rule_switch")).findViewById(2131362706);
        if (this.mSwitchBar != null) {
            this.mSwitchBar.setSwitchBarText(2131890391, 2131890391);
            try {
                this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            }
            catch (IllegalStateException ex) {}
            this.mSwitchBar.show();
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_automatic_rule_switch";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mRule != null && this.mId != null;
    }
    
    public void onResume(final AutomaticZenRule mRule, final String mId) {
        this.mRule = mRule;
        this.mId = mId;
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean enabled) {
        if (enabled == this.mRule.isEnabled()) {
            return;
        }
        this.mRule.setEnabled(enabled);
        this.mBackend.setZenRule(this.mId, this.mRule);
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mRule != null) {
            this.mSwitchBar.setChecked(this.mRule.isEnabled());
        }
    }
}
