package com.android.settings.notification;

import com.android.settingslib.RestrictedSwitchPreference;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.android.settings.widget.MasterCheckBoxPreference;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import java.util.Collection;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.SettingsPreferenceFragment;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import java.util.Iterator;
import android.app.NotificationChannel;
import java.util.Collections;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceCategory;
import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import java.util.List;
import java.util.Comparator;
import android.app.NotificationChannelGroup;
import android.support.v7.preference.Preference;

public final class _$$Lambda$AppNotificationSettings$KKPiatF9s2jsC7BTjM3YfK_E8S4 implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return AppNotificationSettings.lambda$populateGroupToggle$0(this.f$0, this.f$1, preference);
    }
}
