package com.android.settings.notification;

import android.content.Context;

public class NotificationRingtonePreferenceController extends RingtonePreferenceControllerBase
{
    public NotificationRingtonePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "notification_ringtone";
    }
    
    @Override
    public int getRingtoneType() {
        return 2;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034153);
    }
}
