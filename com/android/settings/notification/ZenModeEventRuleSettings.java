package com.android.settings.notification;

import android.app.AutomaticZenRule;
import android.support.v7.preference.PreferenceScreen;
import android.service.notification.ZenModeConfig;
import android.support.v7.preference.Preference;
import android.app.Fragment;
import android.support.v14.preference.PreferenceFragment;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.pm.PackageManager;
import java.util.Iterator;
import java.util.Collections;
import android.os.UserHandle;
import android.os.UserManager;
import java.util.ArrayList;
import android.database.Cursor;
import android.provider.CalendarContract$Calendars;
import android.content.Context;
import android.service.notification.ZenModeConfig$EventInfo;
import java.util.List;
import android.support.v7.preference.DropDownPreference;
import java.util.Comparator;

public class ZenModeEventRuleSettings extends ZenModeRuleSettingsBase
{
    private static final Comparator<CalendarInfo> CALENDAR_NAME;
    private DropDownPreference mCalendar;
    private List<CalendarInfo> mCalendars;
    private boolean mCreate;
    private ZenModeConfig$EventInfo mEvent;
    private DropDownPreference mReply;
    
    static {
        CALENDAR_NAME = new Comparator<CalendarInfo>() {
            @Override
            public int compare(final CalendarInfo calendarInfo, final CalendarInfo calendarInfo2) {
                return calendarInfo.name.compareTo(calendarInfo2.name);
            }
        };
    }
    
    public static void addCalendars(final Context context, final List<CalendarInfo> list) {
        Cursor cursor = null;
        try {
            final Cursor query = context.getContentResolver().query(CalendarContract$Calendars.CONTENT_URI, new String[] { "_id", "calendar_displayName", "(account_name=ownerAccount) AS \"primary\"" }, "\"primary\" = 1", (String[])null, (String)null);
            if (query == null) {
                return;
            }
            while (true) {
                cursor = query;
                if (!query.moveToNext()) {
                    break;
                }
                cursor = query;
                cursor = query;
                final CalendarInfo calendarInfo = new CalendarInfo();
                cursor = query;
                calendarInfo.name = query.getString(1);
                cursor = query;
                calendarInfo.userId = context.getUserId();
                cursor = query;
                list.add(calendarInfo);
            }
        }
        finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    
    private static List<CalendarInfo> getCalendars(final Context context) {
        final ArrayList<Object> list = new ArrayList<Object>();
        final Iterator<UserHandle> iterator = UserManager.get(context).getUserProfiles().iterator();
        while (iterator.hasNext()) {
            final Context contextForUser = getContextForUser(context, iterator.next());
            if (contextForUser != null) {
                addCalendars(contextForUser, (List<CalendarInfo>)list);
            }
        }
        Collections.sort(list, (Comparator<? super Object>)ZenModeEventRuleSettings.CALENDAR_NAME);
        return (List<CalendarInfo>)list;
    }
    
    private static Context getContextForUser(Context packageContextAsUser, final UserHandle userHandle) {
        try {
            packageContextAsUser = packageContextAsUser.createPackageContextAsUser(packageContextAsUser.getPackageName(), 0, userHandle);
            return packageContextAsUser;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    private static String key(final int n, String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(ZenModeConfig$EventInfo.resolveUserId(n));
        sb.append(":");
        if (s == null) {
            s = "";
        }
        sb.append(s);
        return sb.toString();
    }
    
    private static String key(final ZenModeConfig$EventInfo zenModeConfig$EventInfo) {
        return key(zenModeConfig$EventInfo.userId, zenModeConfig$EventInfo.calendar);
    }
    
    private static String key(final CalendarInfo calendarInfo) {
        return key(calendarInfo.userId, calendarInfo.name);
    }
    
    private void reloadCalendar() {
        this.mCalendars = getCalendars(this.mContext);
        final ArrayList<String> list = new ArrayList<String>();
        final ArrayList<String> list2 = new ArrayList<String>();
        list.add(this.getString(2131890309));
        String calendar = null;
        list2.add(key(0, null));
        if (this.mEvent != null) {
            calendar = this.mEvent.calendar;
        }
        int n = 0;
        for (final CalendarInfo calendarInfo : this.mCalendars) {
            list.add(calendarInfo.name);
            list2.add(key(calendarInfo));
            int n2 = n;
            if (calendar != null) {
                n2 = n;
                if (calendar.equals(calendarInfo.name)) {
                    n2 = 1;
                }
            }
            n = n2;
        }
        if (calendar != null && n == 0) {
            list.add(calendar);
            list2.add(key(this.mEvent.userId, calendar));
        }
        this.mCalendar.setEntries(list.toArray(new CharSequence[list.size()]));
        this.mCalendar.setEntryValues(list2.toArray(new CharSequence[list2.size()]));
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<ZenAutomaticRuleHeaderPreferenceController> list = (ArrayList<ZenAutomaticRuleHeaderPreferenceController>)new ArrayList<ZenAutomaticRuleSwitchPreferenceController>();
        this.mHeader = new ZenAutomaticRuleHeaderPreferenceController(context, this, this.getLifecycle());
        this.mSwitch = new ZenAutomaticRuleSwitchPreferenceController(context, this, this.getLifecycle());
        list.add((ZenAutomaticRuleSwitchPreferenceController)this.mHeader);
        list.add(this.mSwitch);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getMetricsCategory() {
        return 146;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082880;
    }
    
    @Override
    protected void onCreateInternal() {
        this.mCreate = true;
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        (this.mCalendar = (DropDownPreference)preferenceScreen.findPreference("calendar")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                final String s = (String)o;
                if (s.equals(key(ZenModeEventRuleSettings.this.mEvent))) {
                    return false;
                }
                final int index = s.indexOf(58);
                ZenModeEventRuleSettings.this.mEvent.userId = Integer.parseInt(s.substring(0, index));
                ZenModeEventRuleSettings.this.mEvent.calendar = s.substring(index + 1);
                if (ZenModeEventRuleSettings.this.mEvent.calendar.isEmpty()) {
                    ZenModeEventRuleSettings.this.mEvent.calendar = null;
                }
                ZenModeEventRuleSettings.this.updateRule(ZenModeConfig.toEventConditionId(ZenModeEventRuleSettings.this.mEvent));
                return true;
            }
        });
        (this.mReply = (DropDownPreference)preferenceScreen.findPreference("reply")).setEntries(new CharSequence[] { this.getString(2131890311), this.getString(2131890313), this.getString(2131890312) });
        this.mReply.setEntryValues(new CharSequence[] { Integer.toString(0), Integer.toString(1), Integer.toString(2) });
        this.mReply.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                final int int1 = Integer.parseInt((String)o);
                if (int1 == ZenModeEventRuleSettings.this.mEvent.reply) {
                    return false;
                }
                ZenModeEventRuleSettings.this.mEvent.reply = int1;
                ZenModeEventRuleSettings.this.updateRule(ZenModeConfig.toEventConditionId(ZenModeEventRuleSettings.this.mEvent));
                return true;
            }
        });
        this.reloadCalendar();
        this.updateControlsInternal();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.isUiRestricted()) {
            return;
        }
        if (!this.mCreate) {
            this.reloadCalendar();
        }
        this.mCreate = false;
    }
    
    @Override
    protected boolean setRule(final AutomaticZenRule automaticZenRule) {
        ZenModeConfig$EventInfo tryParseEventConditionId;
        if (automaticZenRule != null) {
            tryParseEventConditionId = ZenModeConfig.tryParseEventConditionId(automaticZenRule.getConditionId());
        }
        else {
            tryParseEventConditionId = null;
        }
        this.mEvent = tryParseEventConditionId;
        return this.mEvent != null;
    }
    
    @Override
    protected void updateControlsInternal() {
        this.mCalendar.setValue(key(this.mEvent));
        this.mReply.setValue(Integer.toString(this.mEvent.reply));
    }
    
    public static class CalendarInfo
    {
        public String name;
        public int userId;
    }
}
