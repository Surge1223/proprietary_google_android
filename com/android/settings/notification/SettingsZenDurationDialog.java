package com.android.settings.notification;

import com.android.settingslib.notification.ZenDurationDialog;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class SettingsZenDurationDialog extends InstrumentedDialogFragment
{
    @Override
    public int getMetricsCategory() {
        return 1341;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return new ZenDurationDialog(this.getContext()).createDialog();
    }
}
