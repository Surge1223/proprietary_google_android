package com.android.settings.notification;

import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;

public class ZenModeBehaviorMsgEventReminderPreferenceController extends AbstractZenModePreferenceController implements PreferenceControllerMixin
{
    private final ZenModeSettings.SummaryBuilder mSummaryBuilder;
    
    public ZenModeBehaviorMsgEventReminderPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "zen_mode_msg_event_reminder_settings", lifecycle);
        this.mSummaryBuilder = new ZenModeSettings.SummaryBuilder(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_msg_event_reminder_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        preference.setSummary(this.mSummaryBuilder.getMsgEventReminderSettingSummary(this.getPolicy()));
    }
}
