package com.android.settings.notification;

import com.android.settingslib.RestrictedSwitchPreference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class BadgePreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public BadgePreferenceController(final Context context, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
    }
    
    @Override
    public String getPreferenceKey() {
        return "badge";
    }
    
    @Override
    public boolean isAvailable() {
        return super.isAvailable() && (this.mAppRow != null || this.mChannel != null) && Settings.Secure.getInt(this.mContext.getContentResolver(), "notification_badging", 1) != 0 && (this.mChannel == null || this.isDefaultChannel() || this.mAppRow.showBadge);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        if (this.mChannel != null) {
            this.mChannel.setShowBadge(booleanValue);
            this.saveChannel();
        }
        else if (this.mAppRow != null) {
            this.mAppRow.showBadge = booleanValue;
            this.mBackend.setShowBadge(this.mAppRow.pkg, this.mAppRow.uid, booleanValue);
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mAppRow != null) {
            final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
            restrictedSwitchPreference.setDisabledByAdmin(this.mAdmin);
            if (this.mChannel != null) {
                restrictedSwitchPreference.setChecked(this.mChannel.canShowBadge());
                restrictedSwitchPreference.setEnabled(this.isChannelConfigurable() && !restrictedSwitchPreference.isDisabledByAdmin());
            }
            else {
                restrictedSwitchPreference.setChecked(this.mAppRow.showBadge);
            }
        }
    }
}
