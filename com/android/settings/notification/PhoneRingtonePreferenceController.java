package com.android.settings.notification;

import com.android.settings.Utils;
import android.content.Context;

public class PhoneRingtonePreferenceController extends RingtonePreferenceControllerBase
{
    public PhoneRingtonePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "ringtone";
    }
    
    @Override
    public int getRingtoneType() {
        return 1;
    }
    
    @Override
    public boolean isAvailable() {
        return Utils.isVoiceCapable(this.mContext);
    }
}
