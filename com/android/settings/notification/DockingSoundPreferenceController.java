package com.android.settings.notification;

import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class DockingSoundPreferenceController extends SettingPrefController
{
    public DockingSoundPreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(1, "docking_sounds", "dock_sounds_enabled", 1, new int[0]) {
            @Override
            public boolean isApplicable(final Context context) {
                return context.getResources().getBoolean(2131034184);
            }
        };
    }
}
