package com.android.settings.notification;

import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.text.TextUtils;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.Utils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.TogglePreferenceController;

public class VibrateWhenRingPreferenceController extends TogglePreferenceController implements LifecycleObserver, OnPause, OnResume
{
    private static final String KEY_VIBRATE_WHEN_RINGING = "vibrate_when_ringing";
    private final int DEFAULT_VALUE;
    private final int NOTIFICATION_VIBRATE_WHEN_RINGING;
    private SettingObserver mSettingObserver;
    
    public VibrateWhenRingPreferenceController(final Context context, final String s) {
        super(context, s);
        this.DEFAULT_VALUE = 0;
        this.NOTIFICATION_VIBRATE_WHEN_RINGING = 1;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("vibrate_when_ringing");
        if (preference != null) {
            this.mSettingObserver = new SettingObserver(preference);
            preference.setPersistent(false);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (Utils.isVoiceCapable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.System.getInt(contentResolver, "vibrate_when_ringing", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"vibrate_when_ringing");
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(false);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(true);
        }
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.System.putInt(this.mContext.getContentResolver(), "vibrate_when_ringing", (int)(b ? 1 : 0));
    }
    
    private final class SettingObserver extends ContentObserver
    {
        private final Uri VIBRATE_WHEN_RINGING_URI;
        private final Preference mPreference;
        
        public SettingObserver(final Preference mPreference) {
            super(new Handler());
            this.VIBRATE_WHEN_RINGING_URI = Settings.System.getUriFor("vibrate_when_ringing");
            this.mPreference = mPreference;
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.VIBRATE_WHEN_RINGING_URI.equals((Object)uri)) {
                VibrateWhenRingPreferenceController.this.updateState(this.mPreference);
            }
        }
        
        public void register(final boolean b) {
            final ContentResolver contentResolver = VibrateWhenRingPreferenceController.this.mContext.getContentResolver();
            if (b) {
                contentResolver.registerContentObserver(this.VIBRATE_WHEN_RINGING_URI, false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
