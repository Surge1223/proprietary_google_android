package com.android.settings.notification;

import com.android.internal.annotations.VisibleForTesting;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.arch.lifecycle.LifecycleObserver;

public abstract class VolumeSeekBarPreferenceController extends AdjustVolumeRestrictedPreferenceController implements LifecycleObserver
{
    protected AudioHelper mHelper;
    protected VolumeSeekBarPreference mPreference;
    protected VolumeSeekBarPreference.Callback mVolumePreferenceCallback;
    
    public VolumeSeekBarPreferenceController(final Context context, final String s) {
        super(context, s);
        this.setAudioHelper(new AudioHelper(context));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            (this.mPreference = (VolumeSeekBarPreference)preferenceScreen.findPreference(this.getPreferenceKey())).setCallback(this.mVolumePreferenceCallback);
            this.mPreference.setStream(this.getAudioStream());
            this.mPreference.setMuteIcon(this.getMuteIcon());
        }
    }
    
    protected abstract int getAudioStream();
    
    @Override
    public int getMaxSteps() {
        if (this.mPreference != null) {
            return this.mPreference.getMax();
        }
        return this.mHelper.getMaxVolume(this.getAudioStream());
    }
    
    protected abstract int getMuteIcon();
    
    @Override
    public int getSliderPosition() {
        if (this.mPreference != null) {
            return this.mPreference.getProgress();
        }
        return this.mHelper.getStreamVolume(this.getAudioStream());
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {
        if (this.mPreference != null) {
            this.mPreference.onActivityPause();
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        if (this.mPreference != null) {
            this.mPreference.onActivityResume();
        }
    }
    
    @VisibleForTesting
    void setAudioHelper(final AudioHelper mHelper) {
        this.mHelper = mHelper;
    }
    
    public void setCallback(final VolumeSeekBarPreference.Callback mVolumePreferenceCallback) {
        this.mVolumePreferenceCallback = mVolumePreferenceCallback;
    }
    
    @Override
    public boolean setSliderPosition(final int progress) {
        if (this.mPreference != null) {
            this.mPreference.setProgress(progress);
        }
        return this.mHelper.setStreamVolume(this.getAudioStream(), progress);
    }
}
