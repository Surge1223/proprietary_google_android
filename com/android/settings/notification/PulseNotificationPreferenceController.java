package com.android.settings.notification;

import android.content.ContentResolver;
import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.provider.Settings$SettingNotFoundException;
import android.util.Log;
import android.support.v7.preference.TwoStatePreference;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class PulseNotificationPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private SettingObserver mSettingObserver;
    
    public PulseNotificationPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("notification_pulse");
        if (preference != null) {
            this.mSettingObserver = new SettingObserver(preference);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "notification_pulse";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(17956986);
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), false);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return Settings.System.putInt(this.mContext.getContentResolver(), "notification_light_pulse", (int)(((boolean)o) ? 1 : 0));
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), true);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        try {
            final int int1 = Settings.System.getInt(this.mContext.getContentResolver(), "notification_light_pulse");
            boolean checked = true;
            if (int1 != 1) {
                checked = false;
            }
            ((TwoStatePreference)preference).setChecked(checked);
        }
        catch (Settings$SettingNotFoundException ex) {
            Log.e("PulseNotifPrefContr", "notification_light_pulse not found");
        }
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri NOTIFICATION_LIGHT_PULSE_URI;
        private final Preference mPreference;
        
        public SettingObserver(final Preference mPreference) {
            super(new Handler());
            this.NOTIFICATION_LIGHT_PULSE_URI = Settings.System.getUriFor("notification_light_pulse");
            this.mPreference = mPreference;
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.NOTIFICATION_LIGHT_PULSE_URI.equals((Object)uri)) {
                PulseNotificationPreferenceController.this.updateState(this.mPreference);
            }
        }
        
        public void register(final ContentResolver contentResolver, final boolean b) {
            if (b) {
                contentResolver.registerContentObserver(this.NOTIFICATION_LIGHT_PULSE_URI, false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
