package com.android.settings.notification;

import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.widget.ToggleSwitch;
import com.android.settings.applications.LayoutPreference;

public class NotificationSwitchBarPreference extends LayoutPreference
{
    private boolean mChecked;
    private boolean mEnableSwitch;
    private ToggleSwitch mSwitch;
    
    public NotificationSwitchBarPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEnableSwitch = true;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.mSwitch = (ToggleSwitch)preferenceViewHolder.findViewById(16908352);
        if (this.mSwitch != null) {
            this.mSwitch.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    if (!NotificationSwitchBarPreference.this.mSwitch.isEnabled()) {
                        return;
                    }
                    NotificationSwitchBarPreference.this.mChecked ^= true;
                    NotificationSwitchBarPreference.this.setChecked(NotificationSwitchBarPreference.this.mChecked);
                    if (!NotificationSwitchBarPreference.this.callChangeListener(NotificationSwitchBarPreference.this.mChecked)) {
                        NotificationSwitchBarPreference.this.setChecked(NotificationSwitchBarPreference.this.mChecked ^ true);
                    }
                }
            });
            this.mSwitch.setChecked(this.mChecked);
            this.mSwitch.setEnabled(this.mEnableSwitch);
        }
    }
    
    public void setChecked(final boolean b) {
        this.mChecked = b;
        if (this.mSwitch != null) {
            this.mSwitch.setChecked(b);
        }
    }
}
