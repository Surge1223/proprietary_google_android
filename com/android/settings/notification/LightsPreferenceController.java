package com.android.settings.notification;

import com.android.settingslib.RestrictedSwitchPreference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;

public class LightsPreferenceController extends NotificationPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public LightsPreferenceController(final Context context, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
    }
    
    boolean canPulseLight() {
        final boolean boolean1 = this.mContext.getResources().getBoolean(17956986);
        boolean b = false;
        if (!boolean1) {
            return false;
        }
        if (Settings.System.getInt(this.mContext.getContentResolver(), "notification_light_pulse", 0) == 1) {
            b = true;
        }
        return b;
    }
    
    @Override
    public String getPreferenceKey() {
        return "lights";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        final boolean b = false;
        if (!available) {
            return false;
        }
        if (this.mChannel == null) {
            return false;
        }
        boolean b2 = b;
        if (this.checkCanBeVisible(3)) {
            b2 = b;
            if (this.canPulseLight()) {
                b2 = b;
                if (!this.isDefaultChannel()) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            this.mChannel.enableLights((boolean)o);
            this.saveChannel();
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mChannel != null) {
            final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
            restrictedSwitchPreference.setDisabledByAdmin(this.mAdmin);
            restrictedSwitchPreference.setEnabled(this.isChannelConfigurable() && !restrictedSwitchPreference.isDisabledByAdmin());
            restrictedSwitchPreference.setChecked(this.mChannel.shouldShowLights());
        }
    }
}
