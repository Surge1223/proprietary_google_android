package com.android.settings.notification;

import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.widget.Button;
import android.app.FragmentManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$ZenModeButtonPreferenceController$16_xvFNOTseGHNtlUJrmr4Oa8o8 implements View.OnClickListener
{
    public final void onClick(final View view) {
        ZenModeButtonPreferenceController.lambda$updateZenButtonOnClickListener$2(this.f$0, view);
    }
}
