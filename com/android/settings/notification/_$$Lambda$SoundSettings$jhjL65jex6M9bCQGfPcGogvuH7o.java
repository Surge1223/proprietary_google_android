package com.android.settings.notification;

import android.preference.SeekBarVolumizer;
import android.os.UserHandle;
import android.app.Fragment;
import android.support.v7.preference.Preference;
import android.text.TextUtils;
import android.os.Bundle;
import java.util.Iterator;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.sound.HandsFreeProfileOutputPreferenceController;
import com.android.settings.sound.MediaOutputPreferenceController;
import android.content.Intent;
import com.android.settings.widget.PreferenceCategoryController;
import com.android.settings.SettingsPreferenceFragment;
import java.util.ArrayList;
import android.os.Message;
import android.os.Looper;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.RingtonePreference;
import android.os.Handler;
import com.android.settings.widget.UpdatableListPreferenceDialogFragment;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.dashboard.DashboardFragment;
import android.support.v7.preference.ListPreference;
import com.android.settings.sound.AudioSwitchPreferenceController;

public final class _$$Lambda$SoundSettings$jhjL65jex6M9bCQGfPcGogvuH7o implements AudioSwitchCallback
{
    @Override
    public final void onPreferenceDataChanged(final ListPreference listPreference) {
        this.f$0.onPreferenceDataChanged(listPreference);
    }
}
