package com.android.settings.notification;

import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import android.net.Uri;
import android.app.NotificationChannel;
import android.content.Context;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import android.preference.PreferenceManager$OnActivityResultListener;

public class SoundPreferenceController extends NotificationPreferenceController implements PreferenceManager$OnActivityResultListener, OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final SettingsPreferenceFragment mFragment;
    private final NotificationSettingsBase.ImportanceListener mListener;
    private NotificationSoundPreference mPreference;
    
    public SoundPreferenceController(final Context context, final SettingsPreferenceFragment mFragment, final NotificationSettingsBase.ImportanceListener mListener, final NotificationBackend notificationBackend) {
        super(context, notificationBackend);
        this.mFragment = mFragment;
        this.mListener = mListener;
    }
    
    protected static boolean hasValidSound(final NotificationChannel notificationChannel) {
        return notificationChannel != null && notificationChannel.getSound() != null && !Uri.EMPTY.equals((Object)notificationChannel.getSound());
    }
    
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (NotificationSoundPreference)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    public String getPreferenceKey() {
        return "ringtone";
    }
    
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("ringtone".equals(preference.getKey()) && this.mFragment != null) {
            final NotificationSoundPreference notificationSoundPreference = (NotificationSoundPreference)preference;
            notificationSoundPreference.onPrepareRingtonePickerIntent(notificationSoundPreference.getIntent());
            this.mFragment.startActivityForResult(preference.getIntent(), 200);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        final boolean available = super.isAvailable();
        final boolean b = false;
        if (!available) {
            return false;
        }
        if (this.mChannel == null) {
            return false;
        }
        boolean b2 = b;
        if (this.checkCanBeVisible(3)) {
            b2 = b;
            if (!this.isDefaultChannel()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (200 == n) {
            if (this.mPreference != null) {
                this.mPreference.onActivityResult(n, n2, intent);
            }
            this.mListener.onImportanceChanged();
            return true;
        }
        return false;
    }
    
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mChannel != null) {
            this.mChannel.setSound((Uri)o, this.mChannel.getAudioAttributes());
            this.saveChannel();
        }
        return true;
    }
    
    public void updateState(final Preference preference) {
        if (this.mAppRow != null && this.mChannel != null) {
            final NotificationSoundPreference notificationSoundPreference = (NotificationSoundPreference)preference;
            notificationSoundPreference.setEnabled(this.mAdmin == null && this.isChannelConfigurable());
            notificationSoundPreference.setRingtone(this.mChannel.getSound());
        }
    }
}
