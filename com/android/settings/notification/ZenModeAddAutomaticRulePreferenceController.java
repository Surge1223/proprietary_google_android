package com.android.settings.notification;

import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Fragment;
import android.content.Context;
import com.android.settings.utils.ZenServiceListing;
import android.support.v7.preference.Preference;

public class ZenModeAddAutomaticRulePreferenceController extends AbstractZenModeAutomaticRulePreferenceController implements OnPreferenceClickListener
{
    private final ZenServiceListing mZenServiceListing;
    
    public ZenModeAddAutomaticRulePreferenceController(final Context context, final Fragment fragment, final ZenServiceListing mZenServiceListing, final Lifecycle lifecycle) {
        super(context, "zen_mode_add_automatic_rule", fragment, lifecycle);
        this.mZenServiceListing = mZenServiceListing;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("zen_mode_add_automatic_rule");
        preference.setPersistent(false);
        preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_add_automatic_rule";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        ZenRuleSelectionDialog.show(this.mContext, this.mParent, (ZenRuleSelectionDialog.PositiveClickListener)new RuleSelectionListener(), this.mZenServiceListing);
        return true;
    }
    
    public class RuleSelectionListener implements PositiveClickListener
    {
        @Override
        public void onExternalRuleSelected(final ZenRuleInfo zenRuleInfo, final Fragment fragment) {
            fragment.startActivity(new Intent().setComponent(zenRuleInfo.configurationActivity));
        }
        
        @Override
        public void onSystemRuleSelected(final ZenRuleInfo zenRuleInfo, final Fragment fragment) {
            ZenModeAddAutomaticRulePreferenceController.this.showNameRuleDialog(zenRuleInfo, fragment);
        }
    }
}
