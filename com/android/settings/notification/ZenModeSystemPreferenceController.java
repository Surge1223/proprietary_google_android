package com.android.settings.notification;

import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ZenModeSystemPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceChangeListener
{
    public ZenModeSystemPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "zen_mode_system", lifecycle);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_system";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        if (ZenModeSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onPrefChange allowSystem=");
            sb.append(booleanValue);
            Log.d("PrefControllerMixin", sb.toString());
        }
        this.mMetricsFeatureProvider.action(this.mContext, 1340, booleanValue);
        this.mBackend.saveSoundPolicy(128, booleanValue);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        switch (this.getZenMode()) {
            default: {
                switchPreference.setEnabled(true);
                switchPreference.setChecked(this.mBackend.isPriorityCategoryEnabled(128));
                break;
            }
            case 3: {
                switchPreference.setEnabled(false);
                switchPreference.setChecked(false);
                break;
            }
            case 2: {
                switchPreference.setEnabled(false);
                switchPreference.setChecked(false);
                break;
            }
        }
    }
}
