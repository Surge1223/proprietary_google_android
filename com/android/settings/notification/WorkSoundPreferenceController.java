package com.android.settings.notification;

import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.IntentFilter;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import android.media.Ringtone;
import com.android.settings.DefaultRingtonePreference;
import android.media.RingtoneManager;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.PreferenceGroup;
import android.os.UserManager;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class WorkSoundPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final AudioHelper mHelper;
    private int mManagedProfileId;
    private final BroadcastReceiver mManagedProfileReceiver;
    private final SoundSettings mParent;
    private final UserManager mUserManager;
    private final boolean mVoiceCapable;
    private Preference mWorkAlarmRingtonePreference;
    private Preference mWorkNotificationRingtonePreference;
    private Preference mWorkPhoneRingtonePreference;
    private PreferenceGroup mWorkPreferenceCategory;
    private TwoStatePreference mWorkUsePersonalSounds;
    
    public WorkSoundPreferenceController(final Context context, final SoundSettings soundSettings, final Lifecycle lifecycle) {
        this(context, soundSettings, lifecycle, new AudioHelper(context));
    }
    
    WorkSoundPreferenceController(final Context context, final SoundSettings mParent, final Lifecycle lifecycle, final AudioHelper mHelper) {
        super(context);
        this.mManagedProfileReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final int identifier = ((UserHandle)intent.getExtra("android.intent.extra.USER")).getIdentifier();
                final String action = intent.getAction();
                final int hashCode = action.hashCode();
                int n = 0;
                Label_0074: {
                    if (hashCode != -385593787) {
                        if (hashCode == 1051477093) {
                            if (action.equals("android.intent.action.MANAGED_PROFILE_REMOVED")) {
                                n = 1;
                                break Label_0074;
                            }
                        }
                    }
                    else if (action.equals("android.intent.action.MANAGED_PROFILE_ADDED")) {
                        n = 0;
                        break Label_0074;
                    }
                    n = -1;
                }
                switch (n) {
                    default: {}
                    case 1: {
                        WorkSoundPreferenceController.this.onManagedProfileRemoved(identifier);
                    }
                    case 0: {
                        WorkSoundPreferenceController.this.onManagedProfileAdded(identifier);
                    }
                }
            }
        };
        this.mUserManager = UserManager.get(context);
        this.mVoiceCapable = Utils.isVoiceCapable(this.mContext);
        this.mParent = mParent;
        this.mHelper = mHelper;
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private void disableWorkSync() {
        RingtoneManager.disableSyncFromParent(this.getManagedProfileContext());
        this.disableWorkSyncSettings();
    }
    
    private void disableWorkSyncSettings() {
        if (this.mWorkPhoneRingtonePreference != null) {
            this.mWorkPhoneRingtonePreference.setEnabled(true);
        }
        this.mWorkNotificationRingtonePreference.setEnabled(true);
        this.mWorkAlarmRingtonePreference.setEnabled(true);
        this.updateWorkRingtoneSummaries();
    }
    
    private void enableWorkSyncSettings() {
        this.mWorkUsePersonalSounds.setChecked(true);
        if (this.mWorkPhoneRingtonePreference != null) {
            this.mWorkPhoneRingtonePreference.setSummary(2131890217);
        }
        this.mWorkNotificationRingtonePreference.setSummary(2131890217);
        this.mWorkAlarmRingtonePreference.setSummary(2131890217);
    }
    
    private Context getManagedProfileContext() {
        if (this.mManagedProfileId == -10000) {
            return null;
        }
        return this.mHelper.createPackageContextAsUser(this.mManagedProfileId);
    }
    
    private DefaultRingtonePreference initWorkPreference(final PreferenceGroup preferenceGroup, final String s) {
        final DefaultRingtonePreference defaultRingtonePreference = (DefaultRingtonePreference)preferenceGroup.findPreference(s);
        defaultRingtonePreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        defaultRingtonePreference.setUserId(this.mManagedProfileId);
        return defaultRingtonePreference;
    }
    
    private boolean shouldShowRingtoneSettings() {
        return this.mHelper.isSingleVolume() ^ true;
    }
    
    private CharSequence updateRingtoneName(final Context context, final int n) {
        if (context != null && this.mHelper.isUserUnlocked(this.mUserManager, context.getUserId())) {
            return Ringtone.getTitle(context, RingtoneManager.getActualDefaultRingtoneUri(context, n), false, true);
        }
        return this.mContext.getString(2131888226);
    }
    
    private void updateWorkPreferences() {
        if (this.mWorkPreferenceCategory == null) {
            return;
        }
        final boolean available = this.isAvailable();
        this.mWorkPreferenceCategory.setVisible(available);
        if (!available) {
            return;
        }
        if (this.mWorkUsePersonalSounds == null) {
            (this.mWorkUsePersonalSounds = (TwoStatePreference)this.mWorkPreferenceCategory.findPreference("work_use_personal_sounds")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new _$$Lambda$WorkSoundPreferenceController$XBbO1oM_StZ54wAnUJEnnExa5OU(this));
        }
        if (this.mWorkPhoneRingtonePreference == null) {
            this.mWorkPhoneRingtonePreference = this.initWorkPreference(this.mWorkPreferenceCategory, "work_ringtone");
        }
        if (this.mWorkNotificationRingtonePreference == null) {
            this.mWorkNotificationRingtonePreference = this.initWorkPreference(this.mWorkPreferenceCategory, "work_notification_ringtone");
        }
        if (this.mWorkAlarmRingtonePreference == null) {
            this.mWorkAlarmRingtonePreference = this.initWorkPreference(this.mWorkPreferenceCategory, "work_alarm_ringtone");
        }
        if (!this.mVoiceCapable) {
            this.mWorkPhoneRingtonePreference.setVisible(false);
            this.mWorkPhoneRingtonePreference = null;
        }
        if (Settings.Secure.getIntForUser(this.getManagedProfileContext().getContentResolver(), "sync_parent_sounds", 0, this.mManagedProfileId) == 1) {
            this.enableWorkSyncSettings();
        }
        else {
            this.disableWorkSyncSettings();
        }
    }
    
    private void updateWorkRingtoneSummaries() {
        final Context managedProfileContext = this.getManagedProfileContext();
        if (this.mWorkPhoneRingtonePreference != null) {
            this.mWorkPhoneRingtonePreference.setSummary(this.updateRingtoneName(managedProfileContext, 1));
        }
        this.mWorkNotificationRingtonePreference.setSummary(this.updateRingtoneName(managedProfileContext, 2));
        this.mWorkAlarmRingtonePreference.setSummary(this.updateRingtoneName(managedProfileContext, 4));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mWorkPreferenceCategory = (PreferenceGroup)preferenceScreen.findPreference("sound_work_settings_section");
        if (this.mWorkPreferenceCategory != null) {
            this.mWorkPreferenceCategory.setVisible(this.isAvailable());
        }
    }
    
    void enableWorkSync() {
        RingtoneManager.enableSyncFromParent(this.getManagedProfileContext());
        this.enableWorkSyncSettings();
    }
    
    @Override
    public String getPreferenceKey() {
        return "sound_work_settings_section";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mHelper.getManagedProfileId(this.mUserManager) != -10000 && this.shouldShowRingtoneSettings();
    }
    
    public void onManagedProfileAdded(final int mManagedProfileId) {
        if (this.mManagedProfileId == -10000) {
            this.mManagedProfileId = mManagedProfileId;
            this.updateWorkPreferences();
        }
    }
    
    public void onManagedProfileRemoved(final int n) {
        if (this.mManagedProfileId == n) {
            this.mManagedProfileId = this.mHelper.getManagedProfileId(this.mUserManager);
            this.updateWorkPreferences();
        }
    }
    
    @Override
    public void onPause() {
        this.mContext.unregisterReceiver(this.mManagedProfileReceiver);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        int n;
        if ("work_ringtone".equals(preference.getKey())) {
            n = 1;
        }
        else if ("work_notification_ringtone".equals(preference.getKey())) {
            n = 2;
        }
        else {
            if (!"work_alarm_ringtone".equals(preference.getKey())) {
                return true;
            }
            n = 4;
        }
        preference.setSummary(this.updateRingtoneName(this.getManagedProfileContext(), n));
        return true;
    }
    
    @Override
    public void onResume() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_ADDED");
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_REMOVED");
        this.mContext.registerReceiver(this.mManagedProfileReceiver, intentFilter);
        this.mManagedProfileId = this.mHelper.getManagedProfileId(this.mUserManager);
        this.updateWorkPreferences();
    }
    
    public static class UnifyWorkDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        public static void show(final SoundSettings soundSettings) {
            final FragmentManager fragmentManager = soundSettings.getFragmentManager();
            if (fragmentManager.findFragmentByTag("UnifyWorkDialogFragment") == null) {
                final UnifyWorkDialogFragment unifyWorkDialogFragment = new UnifyWorkDialogFragment();
                unifyWorkDialogFragment.setTargetFragment((Fragment)soundSettings, 200);
                unifyWorkDialogFragment.show(fragmentManager, "UnifyWorkDialogFragment");
            }
        }
        
        public int getMetricsCategory() {
            return 553;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final SoundSettings soundSettings = (SoundSettings)this.getTargetFragment();
            if (soundSettings.isAdded()) {
                soundSettings.enableWorkSync();
            }
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131890219).setMessage(2131890218).setPositiveButton(2131890220, (DialogInterface$OnClickListener)this).setNegativeButton(17039369, (DialogInterface$OnClickListener)null).create();
        }
    }
}
