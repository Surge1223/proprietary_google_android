package com.android.settings.notification;

import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.util.TypedValue;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.android.settings.SettingsPreferenceFragment;

public abstract class EmptyTextSettings extends SettingsPreferenceFragment
{
    private TextView mEmpty;
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        (this.mEmpty = new TextView(this.getContext())).setGravity(17);
        final TypedValue typedValue = new TypedValue();
        this.getContext().getTheme().resolveAttribute(16842817, typedValue, true);
        this.mEmpty.setTextAppearance(typedValue.resourceId);
        ((ViewGroup)view.findViewById(16908351)).addView((View)this.mEmpty, new ViewGroup.LayoutParams(-1, -1));
        this.setEmptyView((View)this.mEmpty);
    }
    
    protected void setEmptyText(final int text) {
        this.mEmpty.setText(text);
    }
}
