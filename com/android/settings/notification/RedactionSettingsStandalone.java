package com.android.settings.notification;

import android.content.Intent;
import com.android.settings.SettingsActivity;

public class RedactionSettingsStandalone extends SettingsActivity
{
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", RedactionInterstitial.RedactionInterstitialFragment.class.getName()).putExtra("extra_prefs_show_button_bar", true).putExtra("extra_prefs_set_back_text", (String)null).putExtra("extra_prefs_set_next_text", this.getString(2131886386));
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return RedactionInterstitial.RedactionInterstitialFragment.class.getName().equals(s);
    }
}
