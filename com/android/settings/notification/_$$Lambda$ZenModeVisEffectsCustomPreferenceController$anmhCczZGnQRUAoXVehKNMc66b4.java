package com.android.settings.notification;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.app.NotificationManager$Policy;
import com.android.settings.core.SubSettingLauncher;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public final class _$$Lambda$ZenModeVisEffectsCustomPreferenceController$anmhCczZGnQRUAoXVehKNMc66b4 implements OnRadioButtonClickListener
{
    @Override
    public final void onRadioButtonClick(final ZenCustomRadioButtonPreference zenCustomRadioButtonPreference) {
        ZenModeVisEffectsCustomPreferenceController.lambda$displayPreference$1(this.f$0, zenCustomRadioButtonPreference);
    }
}
