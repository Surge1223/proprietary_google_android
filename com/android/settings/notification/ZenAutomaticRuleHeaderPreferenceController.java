package com.android.settings.notification;

import android.util.Pair;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.Fragment;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.Preference;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager;
import android.util.Slog;
import android.service.notification.ZenModeConfig;
import android.graphics.drawable.Drawable;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.app.AutomaticZenRule;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.widget.EntityHeaderController;
import com.android.settings.core.PreferenceControllerMixin;

public class ZenAutomaticRuleHeaderPreferenceController extends AbstractZenModePreferenceController implements PreferenceControllerMixin
{
    private final String KEY;
    private EntityHeaderController mController;
    private final PreferenceFragment mFragment;
    private String mId;
    private AutomaticZenRule mRule;
    
    public ZenAutomaticRuleHeaderPreferenceController(final Context context, final PreferenceFragment mFragment, final Lifecycle lifecycle) {
        super(context, "pref_app_header", lifecycle);
        this.KEY = "pref_app_header";
        this.mFragment = mFragment;
    }
    
    private Drawable getIcon() {
        try {
            final PackageManager packageManager = this.mContext.getPackageManager();
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.mRule.getOwner().getPackageName(), 0);
            if (applicationInfo.isSystemApp()) {
                if (ZenModeConfig.isValidScheduleConditionId(this.mRule.getConditionId())) {
                    return this.mContext.getDrawable(2131231184);
                }
                if (ZenModeConfig.isValidEventConditionId(this.mRule.getConditionId())) {
                    return this.mContext.getDrawable(2131231006);
                }
            }
            return applicationInfo.loadIcon(packageManager);
        }
        catch (PackageManager$NameNotFoundException ex) {
            Slog.w("PrefControllerMixin", "Unable to load icon - PackageManager.NameNotFoundException");
            return null;
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "pref_app_header";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mRule != null;
    }
    
    protected void onResume(final AutomaticZenRule mRule, final String mId) {
        this.mRule = mRule;
        this.mId = mId;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mRule == null) {
            return;
        }
        if (this.mFragment != null) {
            final LayoutPreference layoutPreference = (LayoutPreference)preference;
            if (this.mController == null) {
                (this.mController = EntityHeaderController.newInstance(this.mFragment.getActivity(), this.mFragment, layoutPreference.findViewById(2131362112))).setEditZenRuleNameListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        ZenRuleNameDialog.show(ZenAutomaticRuleHeaderPreferenceController.this.mFragment, ZenAutomaticRuleHeaderPreferenceController.this.mRule.getName(), null, (ZenRuleNameDialog.PositiveClickListener)new RuleNameChangeListener());
                    }
                });
            }
            this.mController.setIcon(this.getIcon()).setLabel(this.mRule.getName()).setPackageName(this.mRule.getOwner().getPackageName()).setUid(this.mContext.getUserId()).setHasAppInfoLink(false).setButtonActions(2, 0).done(this.mFragment.getActivity(), this.mContext).findViewById(2131362112).setVisibility(0);
        }
    }
    
    public class RuleNameChangeListener implements PositiveClickListener
    {
        @Override
        public void onOk(final String name, final Fragment fragment) {
            ZenAutomaticRuleHeaderPreferenceController.this.mMetricsFeatureProvider.action(ZenAutomaticRuleHeaderPreferenceController.this.mContext, 1267, (Pair<Integer, Object>[])new Pair[0]);
            ZenAutomaticRuleHeaderPreferenceController.this.mRule.setName(name);
            ZenAutomaticRuleHeaderPreferenceController.this.mBackend.setZenRule(ZenAutomaticRuleHeaderPreferenceController.this.mId, ZenAutomaticRuleHeaderPreferenceController.this.mRule);
        }
    }
}
