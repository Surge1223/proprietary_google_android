package com.android.settings.notification;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.content.Context;
import android.net.Uri;
import com.android.settings.RingtonePreference;

public class NotificationSoundPreference extends RingtonePreference
{
    private Uri mRingtone;
    
    public NotificationSoundPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    private void updateRingtoneName(final Uri uri) {
        new AsyncTask<Object, Void, CharSequence>() {
            protected CharSequence doInBackground(final Object... array) {
                if (uri == null) {
                    return NotificationSoundPreference.this.getContext().getString(17040789);
                }
                if (RingtoneManager.isDefault(uri)) {
                    return NotificationSoundPreference.this.getContext().getString(2131888494);
                }
                if ("android.resource".equals(uri.getScheme())) {
                    return NotificationSoundPreference.this.getContext().getString(2131888505);
                }
                return Ringtone.getTitle(NotificationSoundPreference.this.getContext(), uri, false, true);
            }
            
            protected void onPostExecute(final CharSequence summary) {
                NotificationSoundPreference.this.setSummary(summary);
            }
        }.execute(new Object[0]);
    }
    
    @Override
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (intent != null) {
            final Uri ringtone = (Uri)intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
            this.setRingtone(ringtone);
            this.callChangeListener(ringtone);
        }
        return true;
    }
    
    @Override
    protected Uri onRestoreRingtone() {
        return this.mRingtone;
    }
    
    public void setRingtone(final Uri mRingtone) {
        this.mRingtone = mRingtone;
        this.setSummary(" ");
        this.updateRingtoneName(this.mRingtone);
    }
}
