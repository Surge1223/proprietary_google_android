package com.android.settings.notification;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import com.android.settingslib.Utils;
import android.util.IconDrawableFactory;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.Context;
import java.util.ArrayList;
import android.service.notification.NotifyingApp;
import java.util.List;
import android.content.pm.ParceledListSlice;
import android.app.NotificationChannelGroup;
import android.app.NotificationChannel;
import android.os.UserHandle;
import android.util.Log;
import com.android.internal.annotations.VisibleForTesting;
import android.app.INotificationManager$Stub;
import android.os.ServiceManager;
import android.app.INotificationManager;

public class NotificationBackend
{
    static INotificationManager sINM;
    
    static {
        NotificationBackend.sINM = INotificationManager$Stub.asInterface(ServiceManager.getService("notification"));
    }
    
    @VisibleForTesting
    static void markAppRowWithBlockables(final String[] array, final AppRow appRow, final String s) {
        if (array != null) {
            for (int length = array.length, i = 0; i < length; ++i) {
                final String s2 = array[i];
                if (s2 != null) {
                    if (s2.contains(":")) {
                        if (s.equals(s2.split(":", 2)[0])) {
                            appRow.lockedChannelId = s2.split(":", 2)[1];
                        }
                    }
                    else if (s.equals(array[i])) {
                        appRow.lockedImportance = true;
                        appRow.systemApp = true;
                    }
                }
            }
        }
    }
    
    public boolean canShowBadge(final String s, final int n) {
        try {
            return NotificationBackend.sINM.canShowBadge(s, n);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return false;
        }
    }
    
    public int getBlockedAppCount() {
        try {
            return NotificationBackend.sINM.getBlockedAppCount(UserHandle.myUserId());
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return 0;
        }
    }
    
    public int getBlockedChannelCount(final String s, int blockedChannelCount) {
        try {
            blockedChannelCount = NotificationBackend.sINM.getBlockedChannelCount(s, blockedChannelCount);
            return blockedChannelCount;
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return 0;
        }
    }
    
    public NotificationChannel getChannel(final String s, final int n, final String s2) {
        if (s2 == null) {
            return null;
        }
        try {
            return NotificationBackend.sINM.getNotificationChannelForPackage(s, n, s2, true);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return null;
        }
    }
    
    public int getChannelCount(final String s, int numNotificationChannelsForPackage) {
        try {
            numNotificationChannelsForPackage = NotificationBackend.sINM.getNumNotificationChannelsForPackage(s, numNotificationChannelsForPackage, false);
            return numNotificationChannelsForPackage;
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return 0;
        }
    }
    
    public int getDeletedChannelCount(final String s, int deletedChannelCount) {
        try {
            deletedChannelCount = NotificationBackend.sINM.getDeletedChannelCount(s, deletedChannelCount);
            return deletedChannelCount;
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return 0;
        }
    }
    
    public NotificationChannelGroup getGroup(final String s, final int n, final String s2) {
        if (s2 == null) {
            return null;
        }
        try {
            return NotificationBackend.sINM.getNotificationChannelGroupForPackage(s2, s, n);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return null;
        }
    }
    
    public ParceledListSlice<NotificationChannelGroup> getGroups(final String s, final int n) {
        try {
            return (ParceledListSlice<NotificationChannelGroup>)NotificationBackend.sINM.getNotificationChannelGroupsForPackage(s, n, false);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return (ParceledListSlice<NotificationChannelGroup>)ParceledListSlice.emptyList();
        }
    }
    
    public boolean getNotificationsBanned(final String s, final int n) {
        boolean b = false;
        try {
            if (!NotificationBackend.sINM.areNotificationsEnabledForPackage(s, n)) {
                b = true;
            }
            return b;
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return false;
        }
    }
    
    public List<NotifyingApp> getRecentApps() {
        try {
            return (List<NotifyingApp>)NotificationBackend.sINM.getRecentNotifyingAppsForUser(UserHandle.myUserId()).getList();
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return new ArrayList<NotifyingApp>();
        }
    }
    
    public boolean isSystemApp(final Context context, final ApplicationInfo applicationInfo) {
        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(applicationInfo.packageName, 64);
            final AppRow appRow = new AppRow();
            this.recordCanBeBlocked(context, context.getPackageManager(), packageInfo, appRow);
            return appRow.systemApp;
        }
        catch (PackageManager$NameNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public AppRow loadAppRow(final Context context, final PackageManager packageManager, final ApplicationInfo applicationInfo) {
        final AppRow appRow = new AppRow();
        appRow.pkg = applicationInfo.packageName;
        appRow.uid = applicationInfo.uid;
        try {
            appRow.label = applicationInfo.loadLabel(packageManager);
        }
        catch (Throwable t) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error loading application label for ");
            sb.append(appRow.pkg);
            Log.e("NotificationBackend", sb.toString(), t);
            appRow.label = appRow.pkg;
        }
        appRow.icon = IconDrawableFactory.newInstance(context).getBadgedIcon(applicationInfo);
        appRow.banned = this.getNotificationsBanned(appRow.pkg, appRow.uid);
        appRow.showBadge = this.canShowBadge(appRow.pkg, appRow.uid);
        appRow.userId = UserHandle.getUserId(appRow.uid);
        appRow.blockedChannelCount = this.getBlockedChannelCount(appRow.pkg, appRow.uid);
        appRow.channelCount = this.getChannelCount(appRow.pkg, appRow.uid);
        return appRow;
    }
    
    public AppRow loadAppRow(final Context context, final PackageManager packageManager, final PackageInfo packageInfo) {
        final AppRow loadAppRow = this.loadAppRow(context, packageManager, packageInfo.applicationInfo);
        this.recordCanBeBlocked(context, packageManager, packageInfo, loadAppRow);
        return loadAppRow;
    }
    
    public boolean onlyHasDefaultChannel(final String s, final int n) {
        try {
            return NotificationBackend.sINM.onlyHasDefaultChannel(s, n);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return false;
        }
    }
    
    void recordCanBeBlocked(final Context context, final PackageManager packageManager, final PackageInfo packageInfo, final AppRow appRow) {
        appRow.systemApp = Utils.isSystemPackage(context.getResources(), packageManager, packageInfo);
        markAppRowWithBlockables(context.getResources().getStringArray(17236026), appRow, packageInfo.packageName);
    }
    
    public boolean setNotificationsEnabledForPackage(final String s, final int n, final boolean b) {
        try {
            if (this.onlyHasDefaultChannel(s, n)) {
                final NotificationChannel channel = this.getChannel(s, n, "miscellaneous");
                int importance;
                if (b) {
                    importance = -1000;
                }
                else {
                    importance = 0;
                }
                channel.setImportance(importance);
                this.updateChannel(s, n, channel);
            }
            NotificationBackend.sINM.setNotificationsEnabledForPackage(s, n, b);
            return true;
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return false;
        }
    }
    
    public boolean setShowBadge(final String s, final int n, final boolean b) {
        try {
            NotificationBackend.sINM.setShowBadge(s, n, b);
            return true;
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
            return false;
        }
    }
    
    public void updateChannel(final String s, final int n, final NotificationChannel notificationChannel) {
        try {
            NotificationBackend.sINM.updateNotificationChannelForPackage(s, n, notificationChannel);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
        }
    }
    
    public void updateChannelGroup(final String s, final int n, final NotificationChannelGroup notificationChannelGroup) {
        try {
            NotificationBackend.sINM.updateNotificationChannelGroupForPackage(s, n, notificationChannelGroup);
        }
        catch (Exception ex) {
            Log.w("NotificationBackend", "Error calling NoMan", (Throwable)ex);
        }
    }
    
    public static class AppRow extends Row
    {
        public boolean banned;
        public int blockedChannelCount;
        public int channelCount;
        public Drawable icon;
        public CharSequence label;
        public String lockedChannelId;
        public boolean lockedImportance;
        public String pkg;
        public Intent settingsIntent;
        public boolean showBadge;
        public boolean systemApp;
        public int uid;
        public int userId;
    }
    
    static class Row
    {
    }
}
