package com.android.settings.notification;

import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ZenModeRemindersPreferenceController extends AbstractZenModePreferenceController implements OnPreferenceChangeListener
{
    public ZenModeRemindersPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "zen_mode_reminders", lifecycle);
    }
    
    @Override
    public String getPreferenceKey() {
        return "zen_mode_reminders";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        if (ZenModeSettingsBase.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onPrefChange allowReminders=");
            sb.append(booleanValue);
            Log.d("PrefControllerMixin", sb.toString());
        }
        this.mMetricsFeatureProvider.action(this.mContext, 167, booleanValue);
        this.mBackend.saveSoundPolicy(1, booleanValue);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        switch (this.getZenMode()) {
            default: {
                switchPreference.setEnabled(true);
                switchPreference.setChecked(this.mBackend.isPriorityCategoryEnabled(1));
                break;
            }
            case 2:
            case 3: {
                switchPreference.setEnabled(false);
                switchPreference.setChecked(false);
                break;
            }
        }
    }
}
