package com.android.settings.notification;

import java.lang.ref.WeakReference;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.app.Fragment;
import android.service.notification.ZenModeConfig$ScheduleInfo;
import android.service.notification.ZenModeConfig$EventInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.service.notification.ZenModeConfig;
import android.content.pm.ApplicationInfo;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.util.Iterator;
import java.util.TreeSet;
import android.util.Log;
import android.content.pm.ServiceInfo;
import java.util.Set;
import java.text.Collator;
import android.widget.LinearLayout;
import com.android.settings.utils.ZenServiceListing;
import android.content.pm.PackageManager;
import android.app.NotificationManager;
import android.content.Context;
import java.util.Comparator;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ZenRuleSelectionDialog extends InstrumentedDialogFragment
{
    private static final boolean DEBUG;
    private static final Comparator<ZenRuleInfo> RULE_TYPE_COMPARATOR;
    private static Context mContext;
    private static NotificationManager mNm;
    private static PackageManager mPm;
    protected static PositiveClickListener mPositiveClickListener;
    private static ZenServiceListing mServiceListing;
    private LinearLayout mRuleContainer;
    private final ZenServiceListing.Callback mServiceListingCallback;
    
    static {
        DEBUG = ZenModeSettings.DEBUG;
        RULE_TYPE_COMPARATOR = new Comparator<ZenRuleInfo>() {
            private final Collator mCollator = Collator.getInstance();
            
            @Override
            public int compare(final ZenRuleInfo zenRuleInfo, final ZenRuleInfo zenRuleInfo2) {
                final int compare = this.mCollator.compare(zenRuleInfo.packageLabel, zenRuleInfo2.packageLabel);
                if (compare != 0) {
                    return compare;
                }
                return this.mCollator.compare(zenRuleInfo.title, zenRuleInfo2.title);
            }
        };
    }
    
    public ZenRuleSelectionDialog() {
        this.mServiceListingCallback = new ZenServiceListing.Callback() {
            @Override
            public void onServicesReloaded(final Set<ServiceInfo> set) {
                if (ZenRuleSelectionDialog.DEBUG) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Services reloaded: count=");
                    sb.append(set.size());
                    Log.d("ZenRuleSelectionDialog", sb.toString());
                }
                final TreeSet<ZenRuleInfo> set2 = new TreeSet<ZenRuleInfo>(ZenRuleSelectionDialog.RULE_TYPE_COMPARATOR);
                for (final ServiceInfo serviceInfo : set) {
                    final ZenRuleInfo ruleInfo = AbstractZenModeAutomaticRulePreferenceController.getRuleInfo(ZenRuleSelectionDialog.mPm, serviceInfo);
                    if (ruleInfo != null && ruleInfo.configurationActivity != null && ZenRuleSelectionDialog.mNm.isNotificationPolicyAccessGrantedForPackage(ruleInfo.packageName) && (ruleInfo.ruleInstanceLimit <= 0 || ruleInfo.ruleInstanceLimit >= ZenRuleSelectionDialog.mNm.getRuleInstanceCount(serviceInfo.getComponentName()) + 1)) {
                        set2.add(ruleInfo);
                    }
                }
                ZenRuleSelectionDialog.this.bindExternalRules(set2);
            }
        };
    }
    
    private void bindExternalRules(final Set<ZenRuleInfo> set) {
        final Iterator<ZenRuleInfo> iterator = set.iterator();
        while (iterator.hasNext()) {
            this.bindType(iterator.next());
        }
    }
    
    private void bindType(final ZenRuleInfo zenRuleInfo) {
        try {
            final ApplicationInfo applicationInfo = ZenRuleSelectionDialog.mPm.getApplicationInfo(zenRuleInfo.packageName, 0);
            final LinearLayout linearLayout = (LinearLayout)LayoutInflater.from(ZenRuleSelectionDialog.mContext).inflate(2131558906, (ViewGroup)null, false);
            final ImageView imageView = (ImageView)linearLayout.findViewById(R.id.icon);
            ((TextView)linearLayout.findViewById(R.id.title)).setText((CharSequence)zenRuleInfo.title);
            if (!zenRuleInfo.isSystem) {
                new LoadIconTask(imageView).execute((Object[])new ApplicationInfo[] { applicationInfo });
                final TextView textView = (TextView)linearLayout.findViewById(2131362665);
                textView.setText(applicationInfo.loadLabel(ZenRuleSelectionDialog.mPm));
                textView.setVisibility(0);
            }
            else if (ZenModeConfig.isValidScheduleConditionId(zenRuleInfo.defaultConditionId)) {
                imageView.setImageDrawable(ZenRuleSelectionDialog.mContext.getDrawable(2131231184));
            }
            else if (ZenModeConfig.isValidEventConditionId(zenRuleInfo.defaultConditionId)) {
                imageView.setImageDrawable(ZenRuleSelectionDialog.mContext.getDrawable(2131231006));
            }
            linearLayout.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    ZenRuleSelectionDialog.this.dismiss();
                    if (zenRuleInfo.isSystem) {
                        ZenRuleSelectionDialog.mPositiveClickListener.onSystemRuleSelected(zenRuleInfo, ZenRuleSelectionDialog.this.getTargetFragment());
                    }
                    else {
                        ZenRuleSelectionDialog.mPositiveClickListener.onExternalRuleSelected(zenRuleInfo, ZenRuleSelectionDialog.this.getTargetFragment());
                    }
                }
            });
            this.mRuleContainer.addView((View)linearLayout);
        }
        catch (PackageManager$NameNotFoundException ex) {}
    }
    
    private ZenRuleInfo defaultNewEvent() {
        final ZenModeConfig$EventInfo zenModeConfig$EventInfo = new ZenModeConfig$EventInfo();
        zenModeConfig$EventInfo.calendar = null;
        zenModeConfig$EventInfo.reply = 0;
        final ZenRuleInfo zenRuleInfo = new ZenRuleInfo();
        zenRuleInfo.settingsAction = "android.settings.ZEN_MODE_EVENT_RULE_SETTINGS";
        zenRuleInfo.title = ZenRuleSelectionDialog.mContext.getString(2131890248);
        zenRuleInfo.packageName = ZenModeConfig.getScheduleConditionProvider().getPackageName();
        zenRuleInfo.defaultConditionId = ZenModeConfig.toEventConditionId(zenModeConfig$EventInfo);
        zenRuleInfo.serviceComponent = ZenModeConfig.getEventConditionProvider();
        zenRuleInfo.isSystem = true;
        return zenRuleInfo;
    }
    
    private ZenRuleInfo defaultNewSchedule() {
        final ZenModeConfig$ScheduleInfo zenModeConfig$ScheduleInfo = new ZenModeConfig$ScheduleInfo();
        zenModeConfig$ScheduleInfo.days = ZenModeConfig.ALL_DAYS;
        zenModeConfig$ScheduleInfo.startHour = 22;
        zenModeConfig$ScheduleInfo.endHour = 7;
        final ZenRuleInfo zenRuleInfo = new ZenRuleInfo();
        zenRuleInfo.settingsAction = "android.settings.ZEN_MODE_SCHEDULE_RULE_SETTINGS";
        zenRuleInfo.title = ZenRuleSelectionDialog.mContext.getString(2131890415);
        zenRuleInfo.packageName = ZenModeConfig.getEventConditionProvider().getPackageName();
        zenRuleInfo.defaultConditionId = ZenModeConfig.toScheduleConditionId(zenModeConfig$ScheduleInfo);
        zenRuleInfo.serviceComponent = ZenModeConfig.getScheduleConditionProvider();
        zenRuleInfo.isSystem = true;
        return zenRuleInfo;
    }
    
    public static void show(final Context mContext, final Fragment fragment, final PositiveClickListener mPositiveClickListener, final ZenServiceListing mServiceListing) {
        ZenRuleSelectionDialog.mPositiveClickListener = mPositiveClickListener;
        ZenRuleSelectionDialog.mContext = mContext;
        ZenRuleSelectionDialog.mPm = ZenRuleSelectionDialog.mContext.getPackageManager();
        ZenRuleSelectionDialog.mNm = (NotificationManager)ZenRuleSelectionDialog.mContext.getSystemService("notification");
        ZenRuleSelectionDialog.mServiceListing = mServiceListing;
        final ZenRuleSelectionDialog zenRuleSelectionDialog = new ZenRuleSelectionDialog();
        zenRuleSelectionDialog.setTargetFragment(fragment, 0);
        zenRuleSelectionDialog.show(fragment.getFragmentManager(), "ZenRuleSelectionDialog");
    }
    
    @Override
    public int getMetricsCategory() {
        return 1270;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final View inflate = LayoutInflater.from(this.getContext()).inflate(2131558907, (ViewGroup)null, false);
        this.mRuleContainer = (LinearLayout)inflate.findViewById(2131362541);
        if (ZenRuleSelectionDialog.mServiceListing != null) {
            this.bindType(this.defaultNewEvent());
            this.bindType(this.defaultNewSchedule());
            ZenRuleSelectionDialog.mServiceListing.addZenCallback(this.mServiceListingCallback);
            ZenRuleSelectionDialog.mServiceListing.reloadApprovedServices();
        }
        return (Dialog)new AlertDialog$Builder(this.getContext()).setTitle(2131890295).setView(inflate).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).create();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (ZenRuleSelectionDialog.mServiceListing != null) {
            ZenRuleSelectionDialog.mServiceListing.removeZenCallback(this.mServiceListingCallback);
        }
    }
    
    private class LoadIconTask extends AsyncTask<ApplicationInfo, Void, Drawable>
    {
        private final WeakReference<ImageView> viewReference;
        
        public LoadIconTask(final ImageView imageView) {
            this.viewReference = new WeakReference<ImageView>(imageView);
        }
        
        protected Drawable doInBackground(final ApplicationInfo... array) {
            return array[0].loadIcon(ZenRuleSelectionDialog.mPm);
        }
        
        protected void onPostExecute(final Drawable imageDrawable) {
            if (imageDrawable != null) {
                final ImageView imageView = this.viewReference.get();
                if (imageView != null) {
                    imageView.setImageDrawable(imageDrawable);
                }
            }
        }
    }
    
    public interface PositiveClickListener
    {
        void onExternalRuleSelected(final ZenRuleInfo p0, final Fragment p1);
        
        void onSystemRuleSelected(final ZenRuleInfo p0, final Fragment p1);
    }
}
