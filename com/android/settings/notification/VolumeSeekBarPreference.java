package com.android.settings.notification;

import java.util.Objects;
import android.support.v7.preference.PreferenceViewHolder;
import android.text.TextUtils;
import android.preference.SeekBarVolumizer$Callback;
import android.net.Uri;
import android.util.AttributeSet;
import android.content.Context;
import android.preference.SeekBarVolumizer;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.ImageView;
import android.media.AudioManager;
import com.android.settings.widget.SeekBarPreference;

public class VolumeSeekBarPreference extends SeekBarPreference
{
    AudioManager mAudioManager;
    private Callback mCallback;
    private int mIconResId;
    private ImageView mIconView;
    private int mMuteIconResId;
    private boolean mMuted;
    private SeekBar mSeekBar;
    private boolean mStopped;
    private int mStream;
    private String mSuppressionText;
    private TextView mSuppressionTextView;
    private SeekBarVolumizer mVolumizer;
    private boolean mZenMuted;
    
    public VolumeSeekBarPreference(final Context context) {
        super(context);
        this.setLayoutResource(2131558676);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
    }
    
    public VolumeSeekBarPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setLayoutResource(2131558676);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
    }
    
    public VolumeSeekBarPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.setLayoutResource(2131558676);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
    }
    
    public VolumeSeekBarPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setLayoutResource(2131558676);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
    }
    
    private Uri getMediaVolumeUri() {
        final StringBuilder sb = new StringBuilder();
        sb.append("android.resource://");
        sb.append(this.getContext().getPackageName());
        sb.append("/");
        sb.append(2131820559);
        return Uri.parse(sb.toString());
    }
    
    private void init() {
        if (this.mSeekBar == null) {
            return;
        }
        final SeekBarVolumizer$Callback seekBarVolumizer$Callback = (SeekBarVolumizer$Callback)new SeekBarVolumizer$Callback() {
            public void onMuted(final boolean b, final boolean b2) {
                if (VolumeSeekBarPreference.this.mMuted == b && VolumeSeekBarPreference.this.mZenMuted == b2) {
                    return;
                }
                VolumeSeekBarPreference.this.mMuted = b;
                VolumeSeekBarPreference.this.mZenMuted = b2;
                VolumeSeekBarPreference.this.updateIconView();
            }
            
            public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
                if (VolumeSeekBarPreference.this.mCallback != null) {
                    VolumeSeekBarPreference.this.mCallback.onStreamValueChanged(VolumeSeekBarPreference.this.mStream, n);
                }
            }
            
            public void onSampleStarting(final SeekBarVolumizer seekBarVolumizer) {
                if (VolumeSeekBarPreference.this.mCallback != null) {
                    VolumeSeekBarPreference.this.mCallback.onSampleStarting(seekBarVolumizer);
                }
            }
        };
        Uri mediaVolumeUri;
        if (this.mStream == 3) {
            mediaVolumeUri = this.getMediaVolumeUri();
        }
        else {
            mediaVolumeUri = null;
        }
        if (this.mVolumizer == null) {
            this.mVolumizer = new SeekBarVolumizer(this.getContext(), this.mStream, mediaVolumeUri, (SeekBarVolumizer$Callback)seekBarVolumizer$Callback);
        }
        this.mVolumizer.start();
        this.mVolumizer.setSeekBar(this.mSeekBar);
        this.updateIconView();
        this.updateSuppressionText();
        if (!this.isEnabled()) {
            this.mSeekBar.setEnabled(false);
            this.mVolumizer.stop();
        }
    }
    
    private void updateIconView() {
        if (this.mIconView == null) {
            return;
        }
        if (this.mIconResId != 0) {
            this.mIconView.setImageResource(this.mIconResId);
        }
        else if (this.mMuteIconResId != 0 && this.mMuted && !this.mZenMuted) {
            this.mIconView.setImageResource(this.mMuteIconResId);
        }
        else {
            this.mIconView.setImageDrawable(this.getIcon());
        }
    }
    
    private void updateSuppressionText() {
        if (this.mSuppressionTextView != null && this.mSeekBar != null) {
            this.mSuppressionTextView.setText((CharSequence)this.mSuppressionText);
            final boolean empty = TextUtils.isEmpty((CharSequence)this.mSuppressionText);
            final TextView mSuppressionTextView = this.mSuppressionTextView;
            int visibility;
            if (empty ^ true) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            mSuppressionTextView.setVisibility(visibility);
        }
    }
    
    public void onActivityPause() {
        this.mStopped = true;
        if (this.mVolumizer != null) {
            this.mVolumizer.stop();
            this.mVolumizer = null;
        }
    }
    
    public void onActivityResume() {
        if (this.mStopped) {
            this.init();
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.mSeekBar = (SeekBar)preferenceViewHolder.findViewById(16909273);
        this.mIconView = (ImageView)preferenceViewHolder.findViewById(16908294);
        this.mSuppressionTextView = (TextView)preferenceViewHolder.findViewById(2131362675);
        this.init();
    }
    
    public void setCallback(final Callback mCallback) {
        this.mCallback = mCallback;
    }
    
    public void setMuteIcon(final int mMuteIconResId) {
        if (this.mMuteIconResId == mMuteIconResId) {
            return;
        }
        this.mMuteIconResId = mMuteIconResId;
        this.updateIconView();
    }
    
    public void setStream(final int mStream) {
        this.mStream = mStream;
        this.setMax(this.mAudioManager.getStreamMaxVolume(this.mStream));
        this.setMin(this.mAudioManager.getStreamMinVolumeInt(this.mStream));
        this.setProgress(this.mAudioManager.getStreamVolume(this.mStream));
    }
    
    public void setSuppressionText(final String mSuppressionText) {
        if (Objects.equals(mSuppressionText, this.mSuppressionText)) {
            return;
        }
        this.mSuppressionText = mSuppressionText;
        this.updateSuppressionText();
    }
    
    public void showIcon(final int mIconResId) {
        if (this.mIconResId == mIconResId) {
            return;
        }
        this.mIconResId = mIconResId;
        this.updateIconView();
    }
    
    public interface Callback
    {
        void onSampleStarting(final SeekBarVolumizer p0);
        
        void onStreamValueChanged(final int p0, final int p1);
    }
}
