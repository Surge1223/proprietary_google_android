package com.android.settings.notification;

import android.os.AsyncTask;
import android.media.AudioManager;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class TouchSoundPreferenceController extends SettingPrefController
{
    public TouchSoundPreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(2, "touch_sounds", "sound_effects_enabled", 1, new int[0]) {
            @Override
            protected boolean setSetting(final Context context, final int n) {
                AsyncTask.execute((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        final AudioManager audioManager = (AudioManager)context.getSystemService("audio");
                        if (n != 0) {
                            audioManager.loadSoundEffects();
                        }
                        else {
                            audioManager.unloadSoundEffects();
                        }
                    }
                });
                return super.setSetting(context, n);
            }
        };
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034167);
    }
}
