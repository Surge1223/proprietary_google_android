package com.android.settings.notification;

import android.app.NotificationManager;
import android.support.v4.util.Consumer;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.builders.SliceAction;
import com.android.settingslib.Utils;
import androidx.slice.builders.ListBuilder;
import androidx.slice.Slice;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;
import android.content.IntentFilter;

public class ZenModeSliceBuilder
{
    public static final IntentFilter INTENT_FILTER;
    public static final Uri ZEN_MODE_URI;
    
    static {
        ZEN_MODE_URI = new Uri.Builder().scheme("content").authority("com.android.settings.slices").appendPath("action").appendPath("zen_mode").build();
        (INTENT_FILTER = new IntentFilter()).addAction("android.app.action.NOTIFICATION_POLICY_CHANGED");
        ZenModeSliceBuilder.INTENT_FILTER.addAction("android.app.action.INTERRUPTION_FILTER_CHANGED");
        ZenModeSliceBuilder.INTENT_FILTER.addAction("android.app.action.INTERRUPTION_FILTER_CHANGED_INTERNAL");
    }
    
    private static PendingIntent getBroadcastIntent(final Context context) {
        return PendingIntent.getBroadcast(context, 0, new Intent("com.android.settings.notification.ZEN_MODE_CHANGED").setClass(context, (Class)SliceBroadcastReceiver.class), 268435456);
    }
    
    public static Intent getIntent(final Context context) {
        return DatabaseIndexingUtils.buildSearchResultPageIntent(context, ZenModeSettings.class.getName(), "zen_mode", context.getText(2131890376).toString(), 76).setClassName(context.getPackageName(), SubSettings.class.getName()).setData(new Uri.Builder().appendPath("zen_mode").build());
    }
    
    private static PendingIntent getPrimaryAction(final Context context) {
        return PendingIntent.getActivity(context, 0, getIntent(context), 0);
    }
    
    public static Slice getSlice(final Context context) {
        final boolean zenModeEnabled = isZenModeEnabled(context);
        final CharSequence text = context.getText(2131890376);
        return new ListBuilder(context, ZenModeSliceBuilder.ZEN_MODE_URI, -1L).setAccentColor(Utils.getColorAccent(context)).addRow(new _$$Lambda$ZenModeSliceBuilder$sz_ZmwW0wKJApaEBVBuTr2mkXrg(text, new SliceAction(getBroadcastIntent(context), null, zenModeEnabled), new SliceAction(getPrimaryAction(context), null, text))).build();
    }
    
    public static void handleUriChange(final Context context, final Intent intent) {
        int n = 0;
        if (intent.getBooleanExtra("android.app.slice.extra.TOGGLE_STATE", false)) {
            n = 1;
        }
        NotificationManager.from(context).setZenMode(n, (Uri)null, "ZenModeSliceBuilder");
    }
    
    private static boolean isZenModeEnabled(final Context context) {
        switch (((NotificationManager)context.getSystemService((Class)NotificationManager.class)).getZenMode()) {
            default: {
                return false;
            }
            case 1:
            case 2:
            case 3: {
                return true;
            }
        }
    }
}
