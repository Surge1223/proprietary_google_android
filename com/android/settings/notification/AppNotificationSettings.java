package com.android.settings.notification;

import com.android.settingslib.RestrictedSwitchPreference;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.android.settings.widget.MasterCheckBoxPreference;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import java.util.Collection;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.SettingsPreferenceFragment;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import java.util.Iterator;
import android.app.NotificationChannel;
import java.util.Collections;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceCategory;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.util.Log;
import java.util.List;
import android.app.NotificationChannelGroup;
import java.util.Comparator;

public class AppNotificationSettings extends NotificationSettingsBase
{
    private static final boolean DEBUG;
    private static String KEY_ADVANCED_CATEGORY;
    private static String KEY_APP_LINK;
    private static String KEY_BADGE;
    private static String KEY_GENERAL_CATEGORY;
    private Comparator<NotificationChannelGroup> mChannelGroupComparator;
    private List<NotificationChannelGroup> mChannelGroupList;
    
    static {
        DEBUG = Log.isLoggable("AppNotificationSettings", 3);
        AppNotificationSettings.KEY_GENERAL_CATEGORY = "categories";
        AppNotificationSettings.KEY_ADVANCED_CATEGORY = "app_advanced";
        AppNotificationSettings.KEY_BADGE = "badge";
        AppNotificationSettings.KEY_APP_LINK = "app_link";
    }
    
    public AppNotificationSettings() {
        this.mChannelGroupComparator = new Comparator<NotificationChannelGroup>() {
            @Override
            public int compare(final NotificationChannelGroup notificationChannelGroup, final NotificationChannelGroup notificationChannelGroup2) {
                if (notificationChannelGroup.getId() == null && notificationChannelGroup2.getId() != null) {
                    return 1;
                }
                if (notificationChannelGroup2.getId() == null && notificationChannelGroup.getId() != null) {
                    return -1;
                }
                return notificationChannelGroup.getId().compareTo(notificationChannelGroup2.getId());
            }
        };
    }
    
    private void populateGroupList() {
        for (final NotificationChannelGroup notificationChannelGroup : this.mChannelGroupList) {
            final PreferenceCategory preferenceCategory = new PreferenceCategory(this.getPrefContext());
            preferenceCategory.setOrderingAsAdded(true);
            this.getPreferenceScreen().addPreference(preferenceCategory);
            this.mDynamicPreferences.add(preferenceCategory);
            if (notificationChannelGroup.getId() == null) {
                if (this.mChannelGroupList.size() > 1) {
                    preferenceCategory.setTitle(2131888438);
                }
                preferenceCategory.setKey(AppNotificationSettings.KEY_GENERAL_CATEGORY);
            }
            else {
                preferenceCategory.setTitle(notificationChannelGroup.getName());
                preferenceCategory.setKey(notificationChannelGroup.getId());
                this.populateGroupToggle(preferenceCategory, notificationChannelGroup);
            }
            if (!notificationChannelGroup.isBlocked()) {
                final List channels = notificationChannelGroup.getChannels();
                Collections.sort((List<Object>)channels, (Comparator<? super Object>)this.mChannelComparator);
                for (int size = channels.size(), i = 0; i < size; ++i) {
                    this.populateSingleChannelPrefs(preferenceCategory, channels.get(i), notificationChannelGroup.isBlocked());
                }
            }
        }
    }
    
    private void populateList() {
        if (!this.mDynamicPreferences.isEmpty()) {
            final Iterator<Preference> iterator = this.mDynamicPreferences.iterator();
            while (iterator.hasNext()) {
                this.getPreferenceScreen().removePreference(iterator.next());
            }
            this.mDynamicPreferences.clear();
        }
        if (this.mChannelGroupList.isEmpty()) {
            final PreferenceCategory preferenceCategory = new PreferenceCategory(this.getPrefContext());
            preferenceCategory.setTitle(2131888437);
            preferenceCategory.setKey(AppNotificationSettings.KEY_GENERAL_CATEGORY);
            this.getPreferenceScreen().addPreference(preferenceCategory);
            this.mDynamicPreferences.add(preferenceCategory);
            final Preference preference = new Preference(this.getPrefContext());
            preference.setTitle(2131888412);
            preference.setEnabled(false);
            preferenceCategory.addPreference(preference);
        }
        else {
            this.populateGroupList();
            this.mImportanceListener.onImportanceChanged();
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        (this.mControllers = new ArrayList<NotificationPreferenceController>()).add(new HeaderPreferenceController(context, this));
        this.mControllers.add(new BlockPreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new BadgePreferenceController(context, this.mBackend));
        this.mControllers.add(new AllowSoundPreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new ImportancePreferenceController(context, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new SoundPreferenceController(context, this, this.mImportanceListener, this.mBackend));
        this.mControllers.add(new LightsPreferenceController(context, this.mBackend));
        this.mControllers.add(new VibrationPreferenceController(context, this.mBackend));
        this.mControllers.add(new VisibilityPreferenceController(context, new LockPatternUtils(context), this.mBackend));
        this.mControllers.add(new DndPreferenceController(context, this.mBackend));
        this.mControllers.add(new AppLinkPreferenceController(context));
        this.mControllers.add(new DescriptionPreferenceController(context));
        this.mControllers.add(new NotificationsOffPreferenceController(context));
        this.mControllers.add(new DeletedChannelsPreferenceController(context, this.mBackend));
        return new ArrayList<AbstractPreferenceController>(this.mControllers);
    }
    
    @Override
    protected String getLogTag() {
        return "AppNotificationSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 72;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082712;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (this.mShowLegacyChannelConfig && preferenceScreen != null) {
            final Preference preference = this.findPreference(AppNotificationSettings.KEY_BADGE);
            final Preference preference2 = this.findPreference(AppNotificationSettings.KEY_APP_LINK);
            this.removePreference(AppNotificationSettings.KEY_ADVANCED_CATEGORY);
            if (preference != null) {
                preferenceScreen.addPreference(preference);
            }
            if (preference2 != null) {
                preferenceScreen.addPreference(preference2);
            }
        }
    }
    
    protected void onGroupBlockStateChanged(final NotificationChannelGroup notificationChannelGroup) {
        if (notificationChannelGroup == null) {
            return;
        }
        final PreferenceGroup preferenceGroup = (PreferenceGroup)this.getPreferenceScreen().findPreference(notificationChannelGroup.getId());
        if (preferenceGroup != null) {
            final boolean blocked = notificationChannelGroup.isBlocked();
            final int n = 0;
            int i = 0;
            if (blocked) {
                final ArrayList<Preference> list = new ArrayList<Preference>();
                while (i < preferenceGroup.getPreferenceCount()) {
                    final Preference preference = preferenceGroup.getPreference(i);
                    if (preference instanceof MasterCheckBoxPreference) {
                        list.add(preference);
                    }
                    ++i;
                }
                final Iterator<Object> iterator = list.iterator();
                while (iterator.hasNext()) {
                    preferenceGroup.removePreference(iterator.next());
                }
            }
            else {
                final List channels = notificationChannelGroup.getChannels();
                Collections.sort((List<Object>)channels, (Comparator<? super Object>)this.mChannelComparator);
                for (int size = channels.size(), j = n; j < size; ++j) {
                    this.populateSingleChannelPrefs(preferenceGroup, channels.get(j), notificationChannelGroup.isBlocked());
                }
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mUid >= 0 && !TextUtils.isEmpty((CharSequence)this.mPkg) && this.mPkgInfo != null) {
            if (!this.mShowLegacyChannelConfig) {
                new AsyncTask<Void, Void, Void>() {
                    protected Void doInBackground(final Void... array) {
                        AppNotificationSettings.this.mChannelGroupList = (List<NotificationChannelGroup>)AppNotificationSettings.this.mBackend.getGroups(AppNotificationSettings.this.mPkg, AppNotificationSettings.this.mUid).getList();
                        Collections.sort((List<Object>)AppNotificationSettings.this.mChannelGroupList, AppNotificationSettings.this.mChannelGroupComparator);
                        return null;
                    }
                    
                    protected void onPostExecute(final Void void1) {
                        if (AppNotificationSettings.this.getHost() == null) {
                            return;
                        }
                        AppNotificationSettings.this.populateList();
                    }
                }.execute((Object[])new Void[0]);
            }
            for (final NotificationPreferenceController notificationPreferenceController : this.mControllers) {
                notificationPreferenceController.onResume(this.mAppRow, this.mChannel, this.mChannelGroup, this.mSuspendedAppsAdmin);
                notificationPreferenceController.displayPreference(this.getPreferenceScreen());
            }
            this.updatePreferenceStates();
            return;
        }
        Log.w("AppNotificationSettings", "Missing package or uid or packageinfo");
        this.finish();
    }
    
    protected void populateGroupToggle(final PreferenceGroup preferenceGroup, final NotificationChannelGroup notificationChannelGroup) {
        final RestrictedSwitchPreference restrictedSwitchPreference = new RestrictedSwitchPreference(this.getPrefContext());
        restrictedSwitchPreference.setTitle(2131888502);
        restrictedSwitchPreference.setEnabled(this.mSuspendedAppsAdmin == null && this.isChannelGroupBlockable(notificationChannelGroup));
        restrictedSwitchPreference.setChecked(notificationChannelGroup.isBlocked() ^ true);
        restrictedSwitchPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$AppNotificationSettings$KKPiatF9s2jsC7BTjM3YfK_E8S4(this, notificationChannelGroup));
        preferenceGroup.addPreference(restrictedSwitchPreference);
    }
}
