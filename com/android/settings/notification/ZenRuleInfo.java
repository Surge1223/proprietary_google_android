package com.android.settings.notification;

import android.net.Uri;
import android.content.ComponentName;

public class ZenRuleInfo
{
    public ComponentName configurationActivity;
    public Uri defaultConditionId;
    public boolean isSystem;
    public CharSequence packageLabel;
    public String packageName;
    public int ruleInstanceLimit;
    public ComponentName serviceComponent;
    public String settingsAction;
    public String title;
    
    public ZenRuleInfo() {
        this.ruleInstanceLimit = -1;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final ZenRuleInfo zenRuleInfo = (ZenRuleInfo)o;
        if (this.isSystem != zenRuleInfo.isSystem) {
            return false;
        }
        if (this.ruleInstanceLimit != zenRuleInfo.ruleInstanceLimit) {
            return false;
        }
        Label_0091: {
            if (this.packageName != null) {
                if (this.packageName.equals(zenRuleInfo.packageName)) {
                    break Label_0091;
                }
            }
            else if (zenRuleInfo.packageName == null) {
                break Label_0091;
            }
            return false;
        }
        Label_0124: {
            if (this.title != null) {
                if (this.title.equals(zenRuleInfo.title)) {
                    break Label_0124;
                }
            }
            else if (zenRuleInfo.title == null) {
                break Label_0124;
            }
            return false;
        }
        Label_0157: {
            if (this.settingsAction != null) {
                if (this.settingsAction.equals(zenRuleInfo.settingsAction)) {
                    break Label_0157;
                }
            }
            else if (zenRuleInfo.settingsAction == null) {
                break Label_0157;
            }
            return false;
        }
        Label_0190: {
            if (this.configurationActivity != null) {
                if (this.configurationActivity.equals((Object)zenRuleInfo.configurationActivity)) {
                    break Label_0190;
                }
            }
            else if (zenRuleInfo.configurationActivity == null) {
                break Label_0190;
            }
            return false;
        }
        Label_0223: {
            if (this.defaultConditionId != null) {
                if (this.defaultConditionId.equals((Object)zenRuleInfo.defaultConditionId)) {
                    break Label_0223;
                }
            }
            else if (zenRuleInfo.defaultConditionId == null) {
                break Label_0223;
            }
            return false;
        }
        Label_0256: {
            if (this.serviceComponent != null) {
                if (this.serviceComponent.equals((Object)zenRuleInfo.serviceComponent)) {
                    break Label_0256;
                }
            }
            else if (zenRuleInfo.serviceComponent == null) {
                break Label_0256;
            }
            return false;
        }
        if (this.packageLabel != null) {
            equals = this.packageLabel.equals(zenRuleInfo.packageLabel);
        }
        else if (zenRuleInfo.packageLabel != null) {
            equals = false;
        }
        return equals;
    }
}
