package com.android.settings.notification;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.widget.TimePicker;
import android.app.TimePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.app.TimePickerDialog$OnTimeSetListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.text.format.DateFormat;
import android.app.AutomaticZenRule;
import android.app.FragmentManager;
import android.support.v7.preference.PreferenceScreen;
import android.app.Fragment;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import java.util.Calendar;
import android.content.DialogInterface$OnClickListener;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import android.view.View;
import android.service.notification.ZenModeConfig;
import android.util.Log;
import java.util.Arrays;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.service.notification.ZenModeConfig$ScheduleInfo;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import java.text.SimpleDateFormat;

public class ZenModeScheduleRuleSettings extends ZenModeRuleSettingsBase
{
    private final SimpleDateFormat mDayFormat;
    private Preference mDays;
    private TimePickerPreference mEnd;
    private SwitchPreference mExitAtAlarm;
    private ZenModeConfig$ScheduleInfo mSchedule;
    private TimePickerPreference mStart;
    
    public ZenModeScheduleRuleSettings() {
        this.mDayFormat = new SimpleDateFormat("EEE");
    }
    
    private void showDaysDialog() {
        new AlertDialog$Builder(this.mContext).setTitle(2131890361).setView((View)new ZenModeScheduleDaysSelection(this.mContext, this.mSchedule.days) {
            @Override
            protected void onChanged(final int[] days) {
                if (ZenModeScheduleRuleSettings.this.mDisableListeners) {
                    return;
                }
                if (Arrays.equals(days, ZenModeScheduleRuleSettings.this.mSchedule.days)) {
                    return;
                }
                if (ZenModeRuleSettingsBase.DEBUG) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("days.onChanged days=");
                    sb.append(Arrays.asList(new int[][] { days }));
                    Log.d("ZenModeSettings", sb.toString());
                }
                ZenModeScheduleRuleSettings.this.mSchedule.days = days;
                ZenModeScheduleRuleSettings.this.updateRule(ZenModeConfig.toScheduleConditionId(ZenModeScheduleRuleSettings.this.mSchedule));
            }
        }).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                ZenModeScheduleRuleSettings.this.updateDays();
            }
        }).setPositiveButton(2131887494, (DialogInterface$OnClickListener)null).show();
    }
    
    private void updateDays() {
        final int[] days = this.mSchedule.days;
        if (days != null && days.length > 0) {
            final StringBuilder summary = new StringBuilder();
            final Calendar instance = Calendar.getInstance();
            final int[] daysOfWeekForLocale = ZenModeScheduleDaysSelection.getDaysOfWeekForLocale(instance);
            for (int i = 0; i < daysOfWeekForLocale.length; ++i) {
                final int n = daysOfWeekForLocale[i];
                for (int j = 0; j < days.length; ++j) {
                    if (n == days[j]) {
                        instance.set(7, n);
                        if (summary.length() > 0) {
                            summary.append(this.mContext.getString(2131889402));
                        }
                        summary.append(this.mDayFormat.format(instance.getTime()));
                        break;
                    }
                }
            }
            if (summary.length() > 0) {
                this.mDays.setSummary(summary);
                this.mDays.notifyDependencyChange(false);
                return;
            }
        }
        this.mDays.setSummary(2131890363);
        this.mDays.notifyDependencyChange(true);
    }
    
    private void updateEndSummary() {
        final int startHour = this.mSchedule.startHour;
        final int startMinute = this.mSchedule.startMinute;
        final int endHour = this.mSchedule.endHour;
        final int endMinute = this.mSchedule.endMinute;
        int summaryFormat = 0;
        if (startHour * 60 + startMinute >= 60 * endHour + endMinute) {
            summaryFormat = 2131890307;
        }
        this.mEnd.setSummaryFormat(summaryFormat);
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<ZenAutomaticRuleHeaderPreferenceController> list = (ArrayList<ZenAutomaticRuleHeaderPreferenceController>)new ArrayList<ZenAutomaticRuleSwitchPreferenceController>();
        this.mHeader = new ZenAutomaticRuleHeaderPreferenceController(context, this, this.getLifecycle());
        this.mSwitch = new ZenAutomaticRuleSwitchPreferenceController(context, this, this.getLifecycle());
        list.add((ZenAutomaticRuleSwitchPreferenceController)this.mHeader);
        list.add(this.mSwitch);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getMetricsCategory() {
        return 144;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082883;
    }
    
    @Override
    protected void onCreateInternal() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        (this.mDays = preferenceScreen.findPreference("days")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                ZenModeScheduleRuleSettings.this.showDaysDialog();
                return true;
            }
        });
        final FragmentManager fragmentManager = this.getFragmentManager();
        (this.mStart = new TimePickerPreference(this.getPrefContext(), fragmentManager)).setKey("start_time");
        this.mStart.setTitle(2131890384);
        this.mStart.setCallback((Callback)new Callback() {
            @Override
            public boolean onSetTime(final int startHour, final int startMinute) {
                if (ZenModeScheduleRuleSettings.this.mDisableListeners) {
                    return true;
                }
                if (!ZenModeConfig.isValidHour(startHour)) {
                    return false;
                }
                if (!ZenModeConfig.isValidMinute(startMinute)) {
                    return false;
                }
                if (startHour == ZenModeScheduleRuleSettings.this.mSchedule.startHour && startMinute == ZenModeScheduleRuleSettings.this.mSchedule.startMinute) {
                    return true;
                }
                if (ZenModeRuleSettingsBase.DEBUG) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onPrefChange start h=");
                    sb.append(startHour);
                    sb.append(" m=");
                    sb.append(startMinute);
                    Log.d("ZenModeSettings", sb.toString());
                }
                ZenModeScheduleRuleSettings.this.mSchedule.startHour = startHour;
                ZenModeScheduleRuleSettings.this.mSchedule.startMinute = startMinute;
                ZenModeScheduleRuleSettings.this.updateRule(ZenModeConfig.toScheduleConditionId(ZenModeScheduleRuleSettings.this.mSchedule));
                return true;
            }
        });
        preferenceScreen.addPreference(this.mStart);
        this.mStart.setDependency(this.mDays.getKey());
        (this.mEnd = new TimePickerPreference(this.getPrefContext(), fragmentManager)).setKey("end_time");
        this.mEnd.setTitle(2131890306);
        this.mEnd.setCallback((Callback)new Callback() {
            @Override
            public boolean onSetTime(final int endHour, final int endMinute) {
                if (ZenModeScheduleRuleSettings.this.mDisableListeners) {
                    return true;
                }
                if (!ZenModeConfig.isValidHour(endHour)) {
                    return false;
                }
                if (!ZenModeConfig.isValidMinute(endMinute)) {
                    return false;
                }
                if (endHour == ZenModeScheduleRuleSettings.this.mSchedule.endHour && endMinute == ZenModeScheduleRuleSettings.this.mSchedule.endMinute) {
                    return true;
                }
                if (ZenModeRuleSettingsBase.DEBUG) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onPrefChange end h=");
                    sb.append(endHour);
                    sb.append(" m=");
                    sb.append(endMinute);
                    Log.d("ZenModeSettings", sb.toString());
                }
                ZenModeScheduleRuleSettings.this.mSchedule.endHour = endHour;
                ZenModeScheduleRuleSettings.this.mSchedule.endMinute = endMinute;
                ZenModeScheduleRuleSettings.this.updateRule(ZenModeConfig.toScheduleConditionId(ZenModeScheduleRuleSettings.this.mSchedule));
                return true;
            }
        });
        preferenceScreen.addPreference(this.mEnd);
        this.mEnd.setDependency(this.mDays.getKey());
        (this.mExitAtAlarm = (SwitchPreference)preferenceScreen.findPreference("exit_at_alarm")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                ZenModeScheduleRuleSettings.this.mSchedule.exitAtAlarm = (boolean)o;
                ZenModeScheduleRuleSettings.this.updateRule(ZenModeConfig.toScheduleConditionId(ZenModeScheduleRuleSettings.this.mSchedule));
                return true;
            }
        });
    }
    
    @Override
    protected boolean setRule(final AutomaticZenRule automaticZenRule) {
        ZenModeConfig$ScheduleInfo tryParseScheduleConditionId;
        if (automaticZenRule != null) {
            tryParseScheduleConditionId = ZenModeConfig.tryParseScheduleConditionId(automaticZenRule.getConditionId());
        }
        else {
            tryParseScheduleConditionId = null;
        }
        this.mSchedule = tryParseScheduleConditionId;
        return this.mSchedule != null;
    }
    
    @Override
    protected void updateControlsInternal() {
        this.updateDays();
        this.mStart.setTime(this.mSchedule.startHour, this.mSchedule.startMinute);
        this.mEnd.setTime(this.mSchedule.endHour, this.mSchedule.endMinute);
        this.mExitAtAlarm.setChecked(this.mSchedule.exitAtAlarm);
        this.updateEndSummary();
    }
    
    private static class TimePickerPreference extends Preference
    {
        private Callback mCallback;
        private final Context mContext;
        private int mHourOfDay;
        private int mMinute;
        private int mSummaryFormat;
        
        public TimePickerPreference(final Context mContext, final FragmentManager fragmentManager) {
            super(mContext);
            this.mContext = mContext;
            this.setPersistent(false);
            this.setOnPreferenceClickListener((OnPreferenceClickListener)new OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(final Preference preference) {
                    final TimePickerFragment timePickerFragment = new TimePickerFragment();
                    timePickerFragment.pref = TimePickerPreference.this;
                    timePickerFragment.show(fragmentManager, TimePickerPreference.class.getName());
                    return true;
                }
            });
        }
        
        private void updateSummary() {
            final Calendar instance = Calendar.getInstance();
            instance.set(11, this.mHourOfDay);
            instance.set(12, this.mMinute);
            String summary = DateFormat.getTimeFormat(this.mContext).format(instance.getTime());
            if (this.mSummaryFormat != 0) {
                summary = this.mContext.getResources().getString(this.mSummaryFormat, new Object[] { summary });
            }
            this.setSummary(summary);
        }
        
        public void setCallback(final Callback mCallback) {
            this.mCallback = mCallback;
        }
        
        public void setSummaryFormat(final int mSummaryFormat) {
            this.mSummaryFormat = mSummaryFormat;
            this.updateSummary();
        }
        
        public void setTime(final int mHourOfDay, final int mMinute) {
            if (this.mCallback != null && !this.mCallback.onSetTime(mHourOfDay, mMinute)) {
                return;
            }
            this.mHourOfDay = mHourOfDay;
            this.mMinute = mMinute;
            this.updateSummary();
        }
        
        public interface Callback
        {
            boolean onSetTime(final int p0, final int p1);
        }
        
        public static class TimePickerFragment extends InstrumentedDialogFragment implements TimePickerDialog$OnTimeSetListener
        {
            public TimePickerPreference pref;
            
            public int getMetricsCategory() {
                return 556;
            }
            
            public Dialog onCreateDialog(final Bundle bundle) {
                final boolean b = this.pref != null && this.pref.mHourOfDay >= 0 && this.pref.mMinute >= 0;
                final Calendar instance = Calendar.getInstance();
                int n;
                if (b) {
                    n = this.pref.mHourOfDay;
                }
                else {
                    n = instance.get(11);
                }
                int n2;
                if (b) {
                    n2 = this.pref.mMinute;
                }
                else {
                    n2 = instance.get(12);
                }
                return (Dialog)new TimePickerDialog((Context)this.getActivity(), (TimePickerDialog$OnTimeSetListener)this, n, n2, DateFormat.is24HourFormat((Context)this.getActivity()));
            }
            
            public void onTimeSet(final TimePicker timePicker, final int n, final int n2) {
                if (this.pref != null) {
                    this.pref.setTime(n, n2);
                }
            }
        }
    }
}
