package com.android.settings.notification;

import android.os.Parcelable;
import android.content.Intent;
import android.util.AttributeSet;
import android.content.Context;
import android.net.Uri;
import com.android.settings.DefaultRingtonePreference;

public class DefaultNotificationTonePreference extends DefaultRingtonePreference
{
    private Uri mRingtone;
    
    public DefaultNotificationTonePreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public void onPrepareRingtonePickerIntent(final Intent intent) {
        super.onPrepareRingtonePickerIntent(intent);
        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", (Parcelable)this.mRingtone);
    }
    
    @Override
    protected Uri onRestoreRingtone() {
        return this.mRingtone;
    }
}
