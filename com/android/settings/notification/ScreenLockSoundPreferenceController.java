package com.android.settings.notification;

import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;

public class ScreenLockSoundPreferenceController extends SettingPrefController
{
    public ScreenLockSoundPreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final Lifecycle lifecycle) {
        super(context, settingsPreferenceFragment, lifecycle);
        this.mPreference = new SettingPref(2, "screen_locking_sounds", "lockscreen_sounds_enabled", 1, new int[0]);
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034161);
    }
}
