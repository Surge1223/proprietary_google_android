package com.android.settings.notification;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.view.View;
import android.provider.Settings;
import android.database.ContentObserver;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import com.android.settings.widget.AppSwitchPreference;
import java.util.Comparator;
import java.util.Collections;
import android.content.pm.PackageItemInfo$DisplayNameComparator;
import java.util.Collection;
import android.content.pm.ApplicationInfo;
import java.util.ArrayList;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import java.util.Iterator;
import java.util.List;
import android.os.RemoteException;
import android.util.Log;
import android.content.pm.PackageInfo;
import android.app.ActivityManager;
import android.app.AppGlobals;
import android.util.ArraySet;
import android.os.AsyncTask;
import android.content.pm.PackageManager;
import android.app.NotificationManager;
import android.content.Context;

public class ZenAccessSettings extends EmptyTextSettings
{
    private final String TAG;
    private Context mContext;
    private NotificationManager mNoMan;
    private final SettingObserver mObserver;
    private PackageManager mPkgMan;
    
    public ZenAccessSettings() {
        this.TAG = "ZenAccessSettings";
        this.mObserver = new SettingObserver();
    }
    
    private static void deleteRules(final Context context, final String s) {
        AsyncTask.execute((Runnable)new Runnable() {
            @Override
            public void run() {
                ((NotificationManager)context.getSystemService((Class)NotificationManager.class)).removeAutomaticZenRules(s);
            }
        });
    }
    
    private ArraySet<String> getPackagesRequestingNotificationPolicyAccess() {
        final ArraySet set = new ArraySet();
        try {
            final List list = AppGlobals.getPackageManager().getPackagesHoldingPermissions(new String[] { "android.permission.ACCESS_NOTIFICATION_POLICY" }, 0, ActivityManager.getCurrentUser()).getList();
            if (list != null) {
                final Iterator<PackageInfo> iterator = list.iterator();
                while (iterator.hasNext()) {
                    set.add((Object)iterator.next().packageName);
                }
            }
        }
        catch (RemoteException ex) {
            Log.e("ZenAccessSettings", "Cannot reach packagemanager", (Throwable)ex);
        }
        return (ArraySet<String>)set;
    }
    
    private boolean hasAccess(final String s) {
        return this.mNoMan.isNotificationPolicyAccessGrantedForPackage(s);
    }
    
    @VisibleForTesting
    static void logSpecialPermissionChange(final boolean b, final String s, final Context context) {
        int n;
        if (b) {
            n = 768;
        }
        else {
            n = 769;
        }
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    private void reloadList() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        final ArrayList<Object> list = new ArrayList<Object>();
        final ArraySet<String> packagesRequestingNotificationPolicyAccess = this.getPackagesRequestingNotificationPolicyAccess();
        if (!packagesRequestingNotificationPolicyAccess.isEmpty()) {
            final List installedApplications = this.mPkgMan.getInstalledApplications(0);
            if (installedApplications != null) {
                for (final ApplicationInfo applicationInfo : installedApplications) {
                    if (packagesRequestingNotificationPolicyAccess.contains((Object)applicationInfo.packageName)) {
                        list.add(applicationInfo);
                    }
                }
            }
        }
        final ArraySet set = new ArraySet();
        set.addAll((Collection)this.mNoMan.getEnabledNotificationListenerPackages());
        packagesRequestingNotificationPolicyAccess.addAll(set);
        Collections.sort(list, (Comparator<? super Object>)new PackageItemInfo$DisplayNameComparator(this.mPkgMan));
        for (final ApplicationInfo applicationInfo2 : list) {
            final String packageName = applicationInfo2.packageName;
            final CharSequence loadLabel = applicationInfo2.loadLabel(this.mPkgMan);
            final AppSwitchPreference appSwitchPreference = new AppSwitchPreference(this.getPrefContext());
            appSwitchPreference.setKey(packageName);
            appSwitchPreference.setPersistent(false);
            appSwitchPreference.setIcon(applicationInfo2.loadIcon(this.mPkgMan));
            appSwitchPreference.setTitle(loadLabel);
            appSwitchPreference.setChecked(this.hasAccess(packageName));
            if (set.contains((Object)packageName)) {
                appSwitchPreference.setEnabled(false);
                appSwitchPreference.setSummary(this.getString(2131890233));
            }
            appSwitchPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(final Preference preference, final Object o) {
                    if (o) {
                        new ScaryWarningDialogFragment().setPkgInfo(packageName, loadLabel).show(ZenAccessSettings.this.getFragmentManager(), "dialog");
                    }
                    else {
                        new FriendlyWarningDialogFragment().setPkgInfo(packageName, loadLabel).show(ZenAccessSettings.this.getFragmentManager(), "dialog");
                    }
                    return false;
                }
            });
            preferenceScreen.addPreference(appSwitchPreference);
        }
    }
    
    private static void setAccess(final Context context, final String s, final boolean b) {
        logSpecialPermissionChange(b, s, context);
        AsyncTask.execute((Runnable)new Runnable() {
            @Override
            public void run() {
                ((NotificationManager)context.getSystemService((Class)NotificationManager.class)).setNotificationPolicyAccessGranted(s, b);
            }
        });
    }
    
    @Override
    public int getMetricsCategory() {
        return 180;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082876;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Context)this.getActivity();
        this.mPkgMan = this.mContext.getPackageManager();
        this.mNoMan = (NotificationManager)this.mContext.getSystemService((Class)NotificationManager.class);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (!ActivityManager.isLowRamDeviceStatic()) {
            this.getContentResolver().unregisterContentObserver((ContentObserver)this.mObserver);
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (!ActivityManager.isLowRamDeviceStatic()) {
            this.reloadList();
            this.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("enabled_notification_policy_access_packages"), false, (ContentObserver)this.mObserver);
            this.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("enabled_notification_listeners"), false, (ContentObserver)this.mObserver);
        }
        else {
            this.setEmptyText(2131887437);
        }
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.setEmptyText(2131890234);
    }
    
    public static class FriendlyWarningDialogFragment extends InstrumentedDialogFragment
    {
        @Override
        public int getMetricsCategory() {
            return 555;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            super.onCreate(bundle);
            final Bundle arguments = this.getArguments();
            return (Dialog)new AlertDialog$Builder(this.getContext()).setMessage((CharSequence)this.getResources().getString(2131890235)).setTitle((CharSequence)this.getResources().getString(2131890236, new Object[] { arguments.getString("l") })).setCancelable(true).setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                final /* synthetic */ String val$pkg = arguments.getString("p");
                
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    deleteRules(FriendlyWarningDialogFragment.this.getContext(), this.val$pkg);
                    setAccess(FriendlyWarningDialogFragment.this.getContext(), this.val$pkg, false);
                }
            }).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                }
            }).create();
        }
        
        public FriendlyWarningDialogFragment setPkgInfo(String string, final CharSequence charSequence) {
            final Bundle arguments = new Bundle();
            arguments.putString("p", string);
            if (!TextUtils.isEmpty(charSequence)) {
                string = charSequence.toString();
            }
            arguments.putString("l", string);
            this.setArguments(arguments);
            return this;
        }
    }
    
    public static class ScaryWarningDialogFragment extends InstrumentedDialogFragment
    {
        @Override
        public int getMetricsCategory() {
            return 554;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            super.onCreate(bundle);
            final Bundle arguments = this.getArguments();
            return (Dialog)new AlertDialog$Builder(this.getContext()).setMessage((CharSequence)this.getResources().getString(2131890237)).setTitle((CharSequence)this.getResources().getString(2131890238, new Object[] { arguments.getString("l") })).setCancelable(true).setPositiveButton(2131886285, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                final /* synthetic */ String val$pkg = arguments.getString("p");
                
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    setAccess(ScaryWarningDialogFragment.this.getContext(), this.val$pkg, true);
                }
            }).setNegativeButton(2131887370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                }
            }).create();
        }
        
        public ScaryWarningDialogFragment setPkgInfo(String string, final CharSequence charSequence) {
            final Bundle arguments = new Bundle();
            arguments.putString("p", string);
            if (!TextUtils.isEmpty(charSequence)) {
                string = charSequence.toString();
            }
            arguments.putString("l", string);
            this.setArguments(arguments);
            return this;
        }
    }
    
    private final class SettingObserver extends ContentObserver
    {
        public SettingObserver() {
            super(new Handler(Looper.getMainLooper()));
        }
        
        public void onChange(final boolean b, final Uri uri) {
            ZenAccessSettings.this.reloadList();
        }
    }
}
