package com.android.settings;

import java.util.ArrayList;
import android.app.admin.DevicePolicyManager;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.Dialog;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.RestrictedLockUtils;

public class TimeoutListPreference extends RestrictedListPreference
{
    private RestrictedLockUtils.EnforcedAdmin mAdmin;
    private final CharSequence[] mInitialEntries;
    private final CharSequence[] mInitialValues;
    
    public TimeoutListPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mInitialEntries = this.getEntries();
        this.mInitialValues = this.getEntryValues();
    }
    
    @Override
    protected void onDialogCreated(final Dialog dialog) {
        super.onDialogCreated(dialog);
        dialog.create();
        if (this.mAdmin != null) {
            dialog.findViewById(2131361846).findViewById(2131361848).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    RestrictedLockUtils.sendShowAdminSupportDetailsIntent(TimeoutListPreference.this.getContext(), TimeoutListPreference.this.mAdmin);
                }
            });
        }
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        super.onPrepareDialogBuilder(alertDialog$Builder, dialogInterface$OnClickListener);
        if (this.mAdmin != null) {
            alertDialog$Builder.setView(2131558442);
        }
        else {
            alertDialog$Builder.setView((View)null);
        }
    }
    
    public void removeUnusableTimeouts(long n, final RestrictedLockUtils.EnforcedAdmin enforcedAdmin) {
        if (this.getContext().getSystemService("device_policy") == null) {
            return;
        }
        if (enforcedAdmin == null && this.mAdmin == null && !this.isDisabledByAdmin()) {
            return;
        }
        if (enforcedAdmin == null) {
            n = Long.MAX_VALUE;
        }
        final ArrayList<CharSequence> list = new ArrayList<CharSequence>();
        final ArrayList<CharSequence> list2 = new ArrayList<CharSequence>();
        for (int i = 0; i < this.mInitialValues.length; ++i) {
            if (Long.parseLong(this.mInitialValues[i].toString()) <= n) {
                list.add(this.mInitialEntries[i]);
                list2.add(this.mInitialValues[i]);
            }
        }
        if (list2.size() == 0) {
            this.setDisabledByAdmin(enforcedAdmin);
            return;
        }
        this.setDisabledByAdmin(null);
        if (list.size() != this.getEntries().length) {
            final int int1 = Integer.parseInt(this.getValue());
            this.setEntries(list.toArray(new CharSequence[0]));
            this.setEntryValues(list2.toArray(new CharSequence[0]));
            this.mAdmin = enforcedAdmin;
            if (int1 <= n) {
                this.setValue(String.valueOf(int1));
            }
            else if (list2.size() > 0 && Long.parseLong(list2.get(list2.size() - 1).toString()) == n) {
                this.setValue(String.valueOf(n));
            }
        }
    }
}
