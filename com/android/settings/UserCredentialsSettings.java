package com.android.settings;

import android.security.KeyChain$KeyChainConnection;
import android.os.RemoteException;
import android.security.KeyChain;
import android.content.DialogInterface;
import com.android.settingslib.RestrictedLockUtils;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import java.util.Iterator;
import android.os.Parcel;
import java.util.EnumSet;
import android.os.Parcelable.Creator;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import java.util.Collection;
import java.util.ArrayList;
import android.security.keymaster.KeymasterBlob;
import android.security.keymaster.KeyCharacteristics;
import android.util.Log;
import java.security.UnrecoverableKeyException;
import android.os.UserHandle;
import java.util.TreeMap;
import java.util.SortedMap;
import android.security.KeyStore;
import java.util.List;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.util.SparseArray;
import android.view.View.OnClickListener;

public class UserCredentialsSettings extends SettingsPreferenceFragment implements View.OnClickListener
{
    private static final SparseArray<Type> credentialViewTypes;
    
    static {
        (credentialViewTypes = new SparseArray()).put(2131362020, (Object)Type.USER_KEY);
        UserCredentialsSettings.credentialViewTypes.put(2131362019, (Object)Type.USER_CERTIFICATE);
        UserCredentialsSettings.credentialViewTypes.put(2131362017, (Object)Type.CA_CERTIFICATE);
    }
    
    protected static View getCredentialView(final Credential credential, int i, View view, final ViewGroup viewGroup, final boolean b) {
        View inflate = view;
        if (view == null) {
            inflate = LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false);
        }
        ((TextView)inflate.findViewById(2131361858)).setText((CharSequence)credential.alias);
        final TextView textView = (TextView)inflate.findViewById(2131362505);
        if (credential.isSystem()) {
            i = 2131887139;
        }
        else {
            i = 2131887140;
        }
        textView.setText(i);
        view = inflate.findViewById(2131362016);
        if (b) {
            i = 0;
        }
        else {
            i = 8;
        }
        view.setVisibility(i);
        if (b) {
            int visibility;
            for (i = 0; i < UserCredentialsSettings.credentialViewTypes.size(); ++i) {
                view = inflate.findViewById(UserCredentialsSettings.credentialViewTypes.keyAt(i));
                if (credential.storedTypes.contains(UserCredentialsSettings.credentialViewTypes.valueAt(i))) {
                    visibility = 0;
                }
                else {
                    visibility = 8;
                }
                view.setVisibility(visibility);
            }
        }
        return inflate;
    }
    
    protected void announceRemoval(final String s) {
        if (!this.isAdded()) {
            return;
        }
        this.getListView().announceForAccessibility((CharSequence)this.getString(2131889698, new Object[] { s }));
    }
    
    public int getMetricsCategory() {
        return 285;
    }
    
    public void onClick(final View view) {
        final Credential credential = (Credential)view.getTag();
        if (credential != null) {
            CredentialDialogFragment.show(this, credential);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131889700);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.refreshItems();
    }
    
    protected void refreshItems() {
        if (this.isAdded()) {
            new AliasLoader().execute((Object[])new Void[0]);
        }
    }
    
    private class AliasLoader extends AsyncTask<Void, Void, List<Credential>>
    {
        private SortedMap<String, Credential> getCredentialsForUid(final KeyStore keyStore, final int n) {
            final TreeMap<Object, Credential> treeMap = (TreeMap<Object, Credential>)new TreeMap<String, Credential>();
            for (final Type type : Type.values()) {
                for (final String s : type.prefix) {
                    for (final String s2 : keyStore.list(s, n)) {
                        Label_0316: {
                            if (UserHandle.getAppId(n) == 1000) {
                                if (s2.startsWith("profile_key_name_encrypt_")) {
                                    break Label_0316;
                                }
                                if (s2.startsWith("profile_key_name_decrypt_")) {
                                    break Label_0316;
                                }
                                if (s2.startsWith("synthetic_password_")) {
                                    break Label_0316;
                                }
                            }
                            Object string = null;
                            Label_0270: {
                                try {
                                    if (type == Type.USER_KEY) {
                                        string = new StringBuilder();
                                        ((StringBuilder)string).append(s);
                                        ((StringBuilder)string).append(s2);
                                        string = ((StringBuilder)string).toString();
                                        try {
                                            if (!this.isAsymmetric(keyStore, (String)string, n)) {
                                                break Label_0316;
                                            }
                                        }
                                        catch (UnrecoverableKeyException string) {
                                            break Label_0270;
                                        }
                                    }
                                    string = treeMap.get(s2);
                                    if (string == null) {
                                        string = new Credential(s2, n);
                                        treeMap.put(s2, (Credential)string);
                                    }
                                    ((Credential)string).storedTypes.add(type);
                                    break Label_0316;
                                }
                                catch (UnrecoverableKeyException ex) {}
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unable to determine algorithm of key: ");
                            sb.append(s);
                            sb.append(s2);
                            Log.e("UserCredentialsSettings", sb.toString(), (Throwable)string);
                        }
                    }
                }
            }
            return (SortedMap<String, Credential>)treeMap;
        }
        
        private boolean isAsymmetric(final KeyStore keyStore, final String s, int keyCharacteristics) throws UnrecoverableKeyException {
            final KeyCharacteristics keyCharacteristics2 = new KeyCharacteristics();
            keyCharacteristics = keyStore.getKeyCharacteristics(s, (KeymasterBlob)null, (KeymasterBlob)null, keyCharacteristics, keyCharacteristics2);
            final boolean b = true;
            if (keyCharacteristics != 1) {
                throw (UnrecoverableKeyException)new UnrecoverableKeyException("Failed to obtain information about key").initCause((Throwable)KeyStore.getKeyStoreException(keyCharacteristics));
            }
            final Integer enum1 = keyCharacteristics2.getEnum(268435458);
            if (enum1 != null) {
                boolean b2 = b;
                if (enum1 != 1) {
                    b2 = (enum1 == 3 && b);
                }
                return b2;
            }
            throw new UnrecoverableKeyException("Key algorithm unknown");
        }
        
        protected List<Credential> doInBackground(final Void... array) {
            final KeyStore instance = KeyStore.getInstance();
            final int myUserId = UserHandle.myUserId();
            final int uid = UserHandle.getUid(myUserId, 1000);
            final int uid2 = UserHandle.getUid(myUserId, 1010);
            final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<Credential>();
            list.addAll(this.getCredentialsForUid(instance, uid).values());
            list.addAll(this.getCredentialsForUid(instance, uid2).values());
            return (List<Credential>)list;
        }
        
        protected void onPostExecute(final List<Credential> list) {
            if (!UserCredentialsSettings.this.isAdded()) {
                return;
            }
            if (list != null && list.size() != 0) {
                UserCredentialsSettings.this.setEmptyView(null);
            }
            else {
                final TextView emptyView = (TextView)UserCredentialsSettings.this.getActivity().findViewById(16908292);
                emptyView.setText(2131889697);
                UserCredentialsSettings.this.setEmptyView((View)emptyView);
            }
            UserCredentialsSettings.this.getListView().setAdapter((RecyclerView.Adapter)new CredentialAdapter(list, (View.OnClickListener)UserCredentialsSettings.this));
        }
    }
    
    static class Credential implements Parcelable
    {
        public static final Parcelable.Creator<Credential> CREATOR;
        final String alias;
        final EnumSet<Type> storedTypes;
        final int uid;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<Credential>() {
                public Credential createFromParcel(final Parcel parcel) {
                    return new Credential(parcel);
                }
                
                public Credential[] newArray(final int n) {
                    return new Credential[n];
                }
            };
        }
        
        Credential(final Parcel parcel) {
            this(parcel.readString(), parcel.readInt());
            final long long1 = parcel.readLong();
            for (final Type type : Type.values()) {
                if ((1L << type.ordinal() & long1) != 0x0L) {
                    this.storedTypes.add(type);
                }
            }
        }
        
        Credential(final String alias, final int uid) {
            this.storedTypes = EnumSet.noneOf(Type.class);
            this.alias = alias;
            this.uid = uid;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public boolean isSystem() {
            return UserHandle.getAppId(this.uid) == 1000;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeString(this.alias);
            parcel.writeInt(this.uid);
            long n2 = 0L;
            final Iterator<Type> iterator = this.storedTypes.iterator();
            while (iterator.hasNext()) {
                n2 |= 1L << iterator.next().ordinal();
            }
            parcel.writeLong(n2);
        }
        
        enum Type
        {
            CA_CERTIFICATE(new String[] { "CACERT_" }), 
            USER_CERTIFICATE(new String[] { "USRCERT_" }), 
            USER_KEY(new String[] { "USRPKEY_", "USRSKEY_" });
            
            final String[] prefix;
            
            private Type(final String[] prefix) {
                this.prefix = prefix;
            }
        }
    }
    
    private static class CredentialAdapter extends Adapter<UserCredentialsSettings.ViewHolder>
    {
        private final List<Credential> mItems;
        private final View.OnClickListener mListener;
        
        public CredentialAdapter(final List<Credential> mItems, final View.OnClickListener mListener) {
            this.mItems = mItems;
            this.mListener = mListener;
        }
        
        @Override
        public int getItemCount() {
            return this.mItems.size();
        }
        
        public void onBindViewHolder(final UserCredentialsSettings.ViewHolder viewHolder, final int n) {
            UserCredentialsSettings.getCredentialView((Credential)this.mItems.get(n), 2131558869, viewHolder.itemView, null, false);
            viewHolder.itemView.setTag((Object)this.mItems.get(n));
            viewHolder.itemView.setOnClickListener(this.mListener);
        }
        
        public UserCredentialsSettings.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
            return new UserCredentialsSettings.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(2131558869, viewGroup, false));
        }
    }
    
    public static class CredentialDialogFragment extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment, final Credential credential) {
            final Bundle arguments = new Bundle();
            arguments.putParcelable("credential", (Parcelable)credential);
            if (fragment.getFragmentManager().findFragmentByTag("CredentialDialogFragment") == null) {
                final CredentialDialogFragment credentialDialogFragment = new CredentialDialogFragment();
                credentialDialogFragment.setTargetFragment(fragment, -1);
                credentialDialogFragment.setArguments(arguments);
                credentialDialogFragment.show(fragment.getFragmentManager(), "CredentialDialogFragment");
            }
        }
        
        @Override
        public int getMetricsCategory() {
            return 533;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final Credential credential = (Credential)this.getArguments().getParcelable("credential");
            final View inflate = this.getActivity().getLayoutInflater().inflate(2131558868, (ViewGroup)null);
            final ViewGroup viewGroup = (ViewGroup)inflate.findViewById(2131362024);
            viewGroup.addView(UserCredentialsSettings.getCredentialView(credential, 2131558867, null, viewGroup, true));
            final AlertDialog$Builder setPositiveButton = new AlertDialog$Builder((Context)this.getActivity()).setView(inflate).setTitle(2131889699).setPositiveButton(2131887493, (DialogInterface$OnClickListener)null);
            final int myUserId = UserHandle.myUserId();
            if (!RestrictedLockUtils.hasBaseUserRestriction(this.getContext(), "no_config_credentials", myUserId)) {
                final DialogInterface$OnClickListener dialogInterface$OnClickListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced(CredentialDialogFragment.this.getContext(), "no_config_credentials", myUserId);
                        if (checkIfRestrictionEnforced != null) {
                            RestrictedLockUtils.sendShowAdminSupportDetailsIntent(CredentialDialogFragment.this.getContext(), checkIfRestrictionEnforced);
                        }
                        else {
                            new RemoveCredentialsTask(CredentialDialogFragment.this.getContext(), CredentialDialogFragment.this.getTargetFragment()).execute((Object[])new Credential[] { credential });
                        }
                        dialogInterface.dismiss();
                    }
                };
                if (credential.isSystem()) {
                    setPositiveButton.setNegativeButton(2131889498, (DialogInterface$OnClickListener)dialogInterface$OnClickListener);
                }
            }
            return (Dialog)setPositiveButton.create();
        }
        
        private class RemoveCredentialsTask extends AsyncTask<Credential, Void, Credential[]>
        {
            private Context context;
            private Fragment targetFragment;
            
            public RemoveCredentialsTask(final Context context, final Fragment targetFragment) {
                this.context = context;
                this.targetFragment = targetFragment;
            }
            
            private void removeGrantsAndDelete(final Credential credential) {
                try {
                    final KeyChain$KeyChainConnection bind = KeyChain.bind(CredentialDialogFragment.this.getContext());
                    while (true) {
                        try {
                            try {
                                bind.getService().removeKeyPair(credential.alias);
                                bind.close();
                            }
                            finally {}
                        }
                        catch (RemoteException ex) {
                            Log.w("CredentialDialogFragment", "Removing credentials", (Throwable)ex);
                            continue;
                        }
                        break;
                    }
                    return;
                    bind.close();
                }
                catch (InterruptedException ex2) {
                    Log.w("CredentialDialogFragment", "Connecting to KeyChain", (Throwable)ex2);
                }
            }
            
            protected Credential[] doInBackground(final Credential... array) {
                for (final Credential credential : array) {
                    if (!credential.isSystem()) {
                        throw new UnsupportedOperationException("Not implemented for wifi certificates. This should not be reachable.");
                    }
                    this.removeGrantsAndDelete(credential);
                }
                return array;
            }
            
            protected void onPostExecute(final Credential... array) {
                if (this.targetFragment instanceof UserCredentialsSettings && this.targetFragment.isAdded()) {
                    final UserCredentialsSettings userCredentialsSettings = (UserCredentialsSettings)this.targetFragment;
                    for (int length = array.length, i = 0; i < length; ++i) {
                        userCredentialsSettings.announceRemoval(array[i].alias);
                    }
                    userCredentialsSettings.refreshItems();
                }
            }
        }
    }
    
    private static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ViewHolder(final View view) {
            super(view);
        }
    }
}
