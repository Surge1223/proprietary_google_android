package com.android.settings;

import android.widget.ListAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.content.pm.ApplicationInfo;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager;
import java.util.ArrayList;
import java.util.List;
import android.widget.BaseAdapter;
import android.view.WindowManager.LayoutParams;
import android.view.Window;
import android.util.EventLog;
import android.os.Bundle;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.content.ComponentName;
import com.android.internal.app.AlertController$AlertParams;
import android.content.Context;
import com.android.internal.telephony.SmsApplication;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.SmsApplication$SmsApplicationData;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public final class SmsDefaultDialog extends AlertActivity implements DialogInterface$OnClickListener
{
    private SmsApplication$SmsApplicationData mNewSmsApplicationData;
    
    private boolean buildDialog(final String s) {
        if (!((TelephonyManager)this.getSystemService("phone")).isSmsCapable()) {
            return false;
        }
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        mAlertParams.mTitle = this.getString(2131889183);
        this.mNewSmsApplicationData = SmsApplication.getSmsApplicationData(s, (Context)this);
        if (this.mNewSmsApplicationData != null) {
            SmsApplication$SmsApplicationData smsApplicationData = null;
            final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication((Context)this, true);
            if (defaultSmsApplication != null && (smsApplicationData = SmsApplication.getSmsApplicationData(defaultSmsApplication.getPackageName(), (Context)this)).mPackageName.equals(this.mNewSmsApplicationData.mPackageName)) {
                return false;
            }
            if (smsApplicationData != null) {
                mAlertParams.mMessage = this.getString(2131889182, new Object[] { this.mNewSmsApplicationData.getApplicationName((Context)this), smsApplicationData.getApplicationName((Context)this) });
            }
            else {
                mAlertParams.mMessage = this.getString(2131889184, new Object[] { this.mNewSmsApplicationData.getApplicationName((Context)this) });
            }
            mAlertParams.mPositiveButtonText = this.getString(2131890232);
            mAlertParams.mNegativeButtonText = this.getString(2131888406);
            mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
            mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
        }
        else {
            mAlertParams.mAdapter = (ListAdapter)new AppListAdapter();
            mAlertParams.mOnClickListener = (DialogInterface$OnClickListener)this;
            mAlertParams.mNegativeButtonText = this.getString(R.string.cancel);
            mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
            if (mAlertParams.mAdapter.isEmpty()) {
                return false;
            }
        }
        this.setupAlert();
        return true;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        switch (n) {
            default: {
                if (n < 0) {
                    return;
                }
                final AppListAdapter appListAdapter = (AppListAdapter)this.mAlertParams.mAdapter;
                if (appListAdapter.isSelected(n)) {
                    return;
                }
                final String packageName = appListAdapter.getPackageName(n);
                if (!TextUtils.isEmpty((CharSequence)packageName)) {
                    SmsApplication.setDefaultApplication(packageName, (Context)this);
                    this.setResult(-1);
                }
            }
            case -1: {
                SmsApplication.setDefaultApplication(this.mNewSmsApplicationData.mPackageName, (Context)this);
                this.setResult(-1);
            }
            case -2: {}
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String stringExtra = this.getIntent().getStringExtra("package");
        this.setResult(0);
        if (!this.buildDialog(stringExtra)) {
            this.finish();
        }
    }
    
    protected void onStart() {
        super.onStart();
        this.getWindow().addPrivateFlags(524288);
        EventLog.writeEvent(1397638484, new Object[] { "120484087", -1, "" });
    }
    
    protected void onStop() {
        super.onStop();
        final Window window = this.getWindow();
        final WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.privateFlags &= 0xFFF7FFFF;
        window.setAttributes(attributes);
    }
    
    private class AppListAdapter extends BaseAdapter
    {
        private final List<Item> mItems;
        private final int mSelectedIndex;
        
        public AppListAdapter() {
            this.mItems = this.getItems();
            int selectedIndex;
            final int n = selectedIndex = this.getSelectedIndex();
            if (n > 0) {
                this.mItems.add(0, this.mItems.remove(n));
                selectedIndex = 0;
            }
            this.mSelectedIndex = selectedIndex;
        }
        
        private List<Item> getItems() {
            final PackageManager packageManager = SmsDefaultDialog.this.getPackageManager();
            final ArrayList<Item> list = new ArrayList<Item>();
            for (final SmsApplication$SmsApplicationData smsApplication$SmsApplicationData : SmsApplication.getApplicationCollection((Context)SmsDefaultDialog.this)) {
                try {
                    final String mPackageName = smsApplication$SmsApplicationData.mPackageName;
                    final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(mPackageName, 0);
                    if (applicationInfo == null) {
                        continue;
                    }
                    list.add(new Item(applicationInfo.loadLabel(packageManager).toString(), applicationInfo.loadIcon(packageManager), mPackageName));
                }
                catch (PackageManager$NameNotFoundException ex) {}
            }
            return list;
        }
        
        private int getSelectedIndex() {
            final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication((Context)SmsDefaultDialog.this, true);
            if (defaultSmsApplication != null) {
                final String packageName = defaultSmsApplication.getPackageName();
                if (!TextUtils.isEmpty((CharSequence)packageName)) {
                    for (int i = 0; i < this.mItems.size(); ++i) {
                        if (TextUtils.equals((CharSequence)this.mItems.get(i).packgeName, (CharSequence)packageName)) {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }
        
        public int getCount() {
            int size;
            if (this.mItems != null) {
                size = this.mItems.size();
            }
            else {
                size = 0;
            }
            return size;
        }
        
        public Object getItem(final int n) {
            Object value;
            if (this.mItems != null && n < this.mItems.size()) {
                value = this.mItems.get(n);
            }
            else {
                value = null;
            }
            return value;
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public String getPackageName(final int n) {
            final Item item = (Item)this.getItem(n);
            if (item != null) {
                return item.packgeName;
            }
            return null;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final Item item = (Item)this.getItem(n);
            final View inflate = SmsDefaultDialog.this.getLayoutInflater().inflate(2131558451, viewGroup, false);
            ((TextView)inflate.findViewById(16908310)).setText((CharSequence)item.label);
            if (n == this.mSelectedIndex) {
                inflate.findViewById(2131362056).setVisibility(0);
            }
            else {
                inflate.findViewById(2131362056).setVisibility(8);
            }
            ((ImageView)inflate.findViewById(16908294)).setImageDrawable(item.icon);
            return inflate;
        }
        
        public boolean isSelected(final int n) {
            return n == this.mSelectedIndex;
        }
        
        private class Item
        {
            final Drawable icon;
            final String label;
            final String packgeName;
            
            public Item(final String label, final Drawable icon, final String packgeName) {
                this.label = label;
                this.icon = icon;
                this.packgeName = packgeName;
            }
        }
    }
}
