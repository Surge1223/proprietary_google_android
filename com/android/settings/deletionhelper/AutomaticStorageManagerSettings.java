package com.android.settings.deletionhelper;

import android.view.View;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.SettingsActivity;
import android.provider.Settings;
import com.android.settingslib.Utils;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.widget.SwitchBar;
import android.support.v7.preference.DropDownPreference;
import com.android.settings.search.Indexable;
import android.support.v7.preference.Preference;
import com.android.settings.dashboard.DashboardFragment;

public class AutomaticStorageManagerSettings extends DashboardFragment implements OnPreferenceChangeListener
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private DropDownPreference mDaysToRetain;
    private SwitchBar mSwitchBar;
    private AutomaticStorageManagerSwitchBarController mSwitchController;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context);
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return false;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context) {
        final ArrayList<AutomaticStorageManagerDescriptionPreferenceController> list = (ArrayList<AutomaticStorageManagerDescriptionPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new AutomaticStorageManagerDescriptionPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    private static int daysValueToIndex(final int n, final String[] array) {
        for (int i = 0; i < array.length; ++i) {
            if (n == Integer.parseInt(array[i])) {
                return i;
            }
        }
        return array.length - 1;
    }
    
    private void initializeDaysToRetainPreference() {
        (this.mDaysToRetain = (DropDownPreference)this.findPreference("days")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        final int int1 = Settings.Secure.getInt(this.getContentResolver(), "automatic_storage_manager_days_to_retain", Utils.getDefaultStorageManagerDaysToRetain(this.getResources()));
        final String[] stringArray = this.getResources().getStringArray(2130903054);
        this.mDaysToRetain.setValue(stringArray[daysValueToIndex(int1, stringArray)]);
    }
    
    private void initializeSwitchBar() {
        (this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar()).setSwitchBarText(2131886511, 2131886511);
        this.mSwitchBar.show();
        this.mSwitchController = new AutomaticStorageManagerSwitchBarController(this.getContext(), this.mSwitchBar, this.mMetricsFeatureProvider, this.mDaysToRetain, this.getFragmentManager());
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887787;
    }
    
    @Override
    protected String getLogTag() {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 458;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082719;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.initializeDaysToRetainPreference();
        this.initializeSwitchBar();
        return onCreateView;
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.mSwitchBar.hide();
        this.mSwitchController.tearDown();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if ("days".equals(preference.getKey())) {
            Settings.Secure.putInt(this.getContentResolver(), "automatic_storage_manager_days_to_retain", Integer.parseInt((String)o));
        }
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mDaysToRetain.setEnabled(Utils.isStorageManagerEnabled(this.getContext()));
    }
}
