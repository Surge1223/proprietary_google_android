package com.android.settings.deletionhelper;

import android.provider.Settings;
import android.widget.Switch;
import android.os.SystemProperties;
import com.android.settingslib.Utils;
import com.android.internal.util.Preconditions;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.app.FragmentManager;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.widget.SwitchBar;

public class AutomaticStorageManagerSwitchBarController implements OnSwitchChangeListener
{
    private Context mContext;
    private Preference mDaysToRetainPreference;
    private FragmentManager mFragmentManager;
    private MetricsFeatureProvider mMetrics;
    private SwitchBar mSwitchBar;
    
    public AutomaticStorageManagerSwitchBarController(final Context context, final SwitchBar switchBar, final MetricsFeatureProvider metricsFeatureProvider, final Preference preference, final FragmentManager fragmentManager) {
        this.mContext = (Context)Preconditions.checkNotNull((Object)context);
        this.mSwitchBar = (SwitchBar)Preconditions.checkNotNull((Object)switchBar);
        this.mMetrics = (MetricsFeatureProvider)Preconditions.checkNotNull((Object)metricsFeatureProvider);
        this.mDaysToRetainPreference = (Preference)Preconditions.checkNotNull((Object)preference);
        this.mFragmentManager = (FragmentManager)Preconditions.checkNotNull((Object)fragmentManager);
        this.initializeCheckedStatus();
    }
    
    private void initializeCheckedStatus() {
        this.mSwitchBar.setChecked(Utils.isStorageManagerEnabled(this.mContext));
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    private void maybeShowWarning() {
        if (SystemProperties.getBoolean("ro.storage_manager.enabled", false)) {
            return;
        }
        ActivationWarningFragment.newInstance().show(this.mFragmentManager, "ActivationWarningFragment");
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean enabled) {
        this.mMetrics.action(this.mContext, 489, enabled);
        this.mDaysToRetainPreference.setEnabled(enabled);
        Settings.Secure.putInt(this.mContext.getContentResolver(), "automatic_storage_manager_enabled", (int)(enabled ? 1 : 0));
        if ((enabled ? 1 : 0) != 0) {
            this.maybeShowWarning();
        }
    }
    
    public void tearDown() {
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
}
