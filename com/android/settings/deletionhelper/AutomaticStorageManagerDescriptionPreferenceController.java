package com.android.settings.deletionhelper;

import android.content.ContentResolver;
import android.support.v7.preference.Preference;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import com.android.settingslib.Utils;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AutomaticStorageManagerDescriptionPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public AutomaticStorageManagerDescriptionPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        final Context context = preference.getContext();
        final ContentResolver contentResolver = context.getContentResolver();
        final long long1 = Settings.Secure.getLong(contentResolver, "automatic_storage_manager_bytes_cleared", 0L);
        final long long2 = Settings.Secure.getLong(contentResolver, "automatic_storage_manager_last_run", 0L);
        if (long1 != 0L && long2 != 0L && Utils.isStorageManagerEnabled(context)) {
            preference.setSummary(context.getString(2131886510, new Object[] { Formatter.formatFileSize(context, long1), DateUtils.formatDateTime(context, long2, 16) }));
        }
        else {
            preference.setSummary(2131886514);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "freed_bytes";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
}
