package com.android.settings.deletionhelper;

import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;

public class ActivationWarningFragment extends DialogFragment
{
    public static ActivationWarningFragment newInstance() {
        return new ActivationWarningFragment();
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage(2131886506).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
    }
}
