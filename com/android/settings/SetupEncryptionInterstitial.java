package com.android.settings;

import android.widget.LinearLayout;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context;

public class SetupEncryptionInterstitial extends EncryptionInterstitial
{
    public static Intent createStartIntent(final Context context, final int n, final boolean b, Intent startIntent) {
        startIntent = EncryptionInterstitial.createStartIntent(context, n, b, startIntent);
        startIntent.setClass(context, (Class)SetupEncryptionInterstitial.class);
        startIntent.putExtra("extra_prefs_show_button_bar", false).putExtra(":settings:show_fragment_title_resid", -1);
        return startIntent;
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", SetupEncryptionInterstitialFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return SetupEncryptionInterstitialFragment.class.getName().equals(s);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public static class SetupEncryptionInterstitialFragment extends EncryptionInterstitialFragment
    {
    }
}
