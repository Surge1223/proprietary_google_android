package com.android.settings.accounts;

import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedPreference;
import com.android.settings.AccessiblePreferenceCategory;
import java.util.ArrayList;
import android.content.Context;

public class AccountRestrictionHelper
{
    private final Context mContext;
    
    public AccountRestrictionHelper(final Context mContext) {
        this.mContext = mContext;
    }
    
    public static boolean showAccount(final String[] array, final ArrayList<String> list) {
        boolean b = true;
        if (array != null) {
            b = b;
            if (list != null) {
                final boolean b2 = false;
                final int length = array.length;
                int n = 0;
                while (true) {
                    b = b2;
                    if (n >= length) {
                        break;
                    }
                    if (list.contains(array[n])) {
                        return true;
                    }
                    ++n;
                }
            }
        }
        return b;
    }
    
    public AccessiblePreferenceCategory createAccessiblePreferenceCategory(final Context context) {
        return new AccessiblePreferenceCategory(context);
    }
    
    public void enforceRestrictionOnPreference(final RestrictedPreference restrictedPreference, final String s, final int n) {
        if (restrictedPreference == null) {
            return;
        }
        if (this.hasBaseUserRestriction(s, n)) {
            restrictedPreference.setEnabled(false);
        }
        else {
            restrictedPreference.checkRestrictionAndSetDisabled(s, n);
        }
    }
    
    public boolean hasBaseUserRestriction(final String s, final int n) {
        return RestrictedLockUtils.hasBaseUserRestriction(this.mContext, s, n);
    }
}
