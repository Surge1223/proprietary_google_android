package com.android.settings.accounts;

import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Handler;
import android.app.Activity;
import android.accounts.OperationCanceledException;
import java.io.IOException;
import android.accounts.AuthenticatorException;
import android.accounts.AccountManagerFuture;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManager;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import com.android.settingslib.RestrictedLockUtils;
import android.view.View;
import com.android.settings.applications.LayoutPreference;
import android.widget.Button;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.os.UserHandle;
import android.app.Fragment;
import android.accounts.Account;
import com.android.settings.core.PreferenceControllerMixin;
import android.view.View.OnClickListener;
import com.android.settingslib.core.AbstractPreferenceController;

public class RemoveAccountPreferenceController extends AbstractPreferenceController implements View.OnClickListener, PreferenceControllerMixin
{
    private Account mAccount;
    private Fragment mParentFragment;
    private UserHandle mUserHandle;
    
    public RemoveAccountPreferenceController(final Context context, final Fragment mParentFragment) {
        super(context);
        this.mParentFragment = mParentFragment;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        ((LayoutPreference)preferenceScreen.findPreference("remove_account")).findViewById(2131361942).setOnClickListener((View.OnClickListener)this);
    }
    
    @Override
    public String getPreferenceKey() {
        return "remove_account";
    }
    
    public void init(final Account mAccount, final UserHandle mUserHandle) {
        this.mAccount = mAccount;
        this.mUserHandle = mUserHandle;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void onClick(final View view) {
        if (this.mUserHandle != null) {
            final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_modify_accounts", this.mUserHandle.getIdentifier());
            if (checkIfRestrictionEnforced != null) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, checkIfRestrictionEnforced);
                return;
            }
        }
        ConfirmRemoveAccountDialog.show(this.mParentFragment, this.mAccount, this.mUserHandle);
    }
    
    public static class ConfirmRemoveAccountDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        private Account mAccount;
        private UserHandle mUserHandle;
        
        public static ConfirmRemoveAccountDialog show(final Fragment fragment, final Account account, final UserHandle userHandle) {
            if (!fragment.isAdded()) {
                return null;
            }
            final ConfirmRemoveAccountDialog confirmRemoveAccountDialog = new ConfirmRemoveAccountDialog();
            final Bundle arguments = new Bundle();
            arguments.putParcelable("account", (Parcelable)account);
            arguments.putParcelable("android.intent.extra.USER", (Parcelable)userHandle);
            confirmRemoveAccountDialog.setArguments(arguments);
            confirmRemoveAccountDialog.setTargetFragment(fragment, 0);
            confirmRemoveAccountDialog.show(fragment.getFragmentManager(), "confirmRemoveAccount");
            return confirmRemoveAccountDialog;
        }
        
        public int getMetricsCategory() {
            return 585;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final Activity activity = this.getTargetFragment().getActivity();
            AccountManager.get((Context)activity).removeAccountAsUser(this.mAccount, activity, (AccountManagerCallback)new AccountManagerCallback<Bundle>() {
                public void run(final AccountManagerFuture<Bundle> accountManagerFuture) {
                    if (!ConfirmRemoveAccountDialog.this.getTargetFragment().isResumed()) {
                        return;
                    }
                    final boolean b = true;
                    boolean b2 = true;
                    try {
                        if (((Bundle)accountManagerFuture.getResult()).getBoolean("booleanResult")) {
                            b2 = false;
                        }
                    }
                    catch (AuthenticatorException ex) {
                        b2 = b;
                    }
                    catch (IOException ex2) {}
                    catch (OperationCanceledException ex3) {}
                    final Activity activity = ConfirmRemoveAccountDialog.this.getTargetFragment().getActivity();
                    if (b2 && activity != null && !activity.isFinishing()) {
                        RemoveAccountFailureDialog.show(ConfirmRemoveAccountDialog.this.getTargetFragment());
                    }
                    else {
                        activity.finish();
                    }
                }
            }, (Handler)null, this.mUserHandle);
        }
        
        public void onCreate(Bundle arguments) {
            super.onCreate(arguments);
            arguments = this.getArguments();
            this.mAccount = (Account)arguments.getParcelable("account");
            this.mUserHandle = (UserHandle)arguments.getParcelable("android.intent.extra.USER");
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888793).setMessage(2131888792).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).setPositiveButton(2131888802, (DialogInterface$OnClickListener)this).create();
        }
    }
    
    public static class RemoveAccountFailureDialog extends InstrumentedDialogFragment
    {
        public static void show(final Fragment fragment) {
            if (!fragment.isAdded()) {
                return;
            }
            final RemoveAccountFailureDialog removeAccountFailureDialog = new RemoveAccountFailureDialog();
            removeAccountFailureDialog.setTargetFragment(fragment, 0);
            removeAccountFailureDialog.show(fragment.getFragmentManager(), "removeAccountFailed");
        }
        
        @Override
        public int getMetricsCategory() {
            return 586;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888793).setMessage(2131888801).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
        }
    }
}
