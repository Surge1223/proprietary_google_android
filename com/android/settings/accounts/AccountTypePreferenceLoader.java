package com.android.settings.accounts;

import com.android.settings.core.SubSettingLauncher;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.os.Parcelable;
import android.support.v7.preference.Preference;
import com.android.settings.location.LocationSettings;
import android.text.TextUtils;
import android.accounts.Account;
import android.support.v7.preference.PreferenceGroup;
import android.content.res.Resources.Theme;
import android.content.res.Resources$NotFoundException;
import android.content.Context;
import com.android.settings.utils.LocalClassLoaderContextThemeWrapper;
import android.support.v7.preference.PreferenceScreen;
import android.content.pm.ApplicationInfo;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.accounts.AuthenticatorDescription;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.UserHandle;
import android.support.v14.preference.PreferenceFragment;
import com.android.settingslib.accounts.AuthenticatorHelper;

public class AccountTypePreferenceLoader
{
    private AuthenticatorHelper mAuthenticatorHelper;
    private PreferenceFragment mFragment;
    private UserHandle mUserHandle;
    
    public AccountTypePreferenceLoader(final PreferenceFragment mFragment, final AuthenticatorHelper mAuthenticatorHelper, final UserHandle mUserHandle) {
        this.mFragment = mFragment;
        this.mAuthenticatorHelper = mAuthenticatorHelper;
        this.mUserHandle = mUserHandle;
    }
    
    private boolean isSafeIntent(final PackageManager packageManager, final Intent intent, final String s) {
        final AuthenticatorDescription accountTypeDescription = this.mAuthenticatorHelper.getAccountTypeDescription(s);
        final int identifier = this.mUserHandle.getIdentifier();
        boolean b = false;
        final ResolveInfo resolveActivityAsUser = packageManager.resolveActivityAsUser(intent, 0, identifier);
        if (resolveActivityAsUser == null) {
            return false;
        }
        final ActivityInfo activityInfo = resolveActivityAsUser.activityInfo;
        final ApplicationInfo applicationInfo = activityInfo.applicationInfo;
        try {
            if (activityInfo.exported) {
                if (activityInfo.permission == null) {
                    return true;
                }
                if (packageManager.checkPermission(activityInfo.permission, accountTypeDescription.packageName) == 0) {
                    return true;
                }
            }
            if (applicationInfo.uid == packageManager.getApplicationInfo(accountTypeDescription.packageName, 0).uid) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e("AccountTypePrefLoader", "Intent considered unsafe due to exception.", (Throwable)ex);
            return false;
        }
    }
    
    public PreferenceScreen addPreferencesForType(final String s, final PreferenceScreen preferenceScreen) {
        final PreferenceScreen preferenceScreen2 = null;
        final PreferenceScreen preferenceScreen3 = null;
        Object o = preferenceScreen2;
        if (this.mAuthenticatorHelper.containsAccountType(s)) {
            Object o2 = null;
            o = null;
            while (true) {
                try {
                    final AuthenticatorDescription accountTypeDescription = this.mAuthenticatorHelper.getAccountTypeDescription(s);
                    PreferenceScreen inflateFromResource = preferenceScreen3;
                    if (accountTypeDescription != null) {
                        inflateFromResource = preferenceScreen3;
                        o = accountTypeDescription;
                        o2 = accountTypeDescription;
                        if (accountTypeDescription.accountPreferencesId != 0) {
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            final Context packageContextAsUser = this.mFragment.getActivity().createPackageContextAsUser(accountTypeDescription.packageName, 0, this.mUserHandle);
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            final Resources.Theme theme = this.mFragment.getResources().newTheme();
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            theme.applyStyle(2131952104, true);
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            final LocalClassLoaderContextThemeWrapper localClassLoaderContextThemeWrapper = new LocalClassLoaderContextThemeWrapper(this.getClass(), packageContextAsUser, 0);
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            ((Context)localClassLoaderContextThemeWrapper).getTheme().setTo(theme);
                            o = accountTypeDescription;
                            o2 = accountTypeDescription;
                            inflateFromResource = this.mFragment.getPreferenceManager().inflateFromResource((Context)localClassLoaderContextThemeWrapper, accountTypeDescription.accountPreferencesId, preferenceScreen);
                        }
                    }
                    o = inflateFromResource;
                }
                catch (Resources$NotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Couldn't load preferences.xml file from ");
                    sb.append(((AuthenticatorDescription)o).packageName);
                    Log.w("AccountTypePrefLoader", sb.toString());
                    o = preferenceScreen2;
                }
                catch (PackageManager$NameNotFoundException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Couldn't load preferences.xml file from ");
                    sb2.append(((AuthenticatorDescription)o2).packageName);
                    Log.w("AccountTypePrefLoader", sb2.toString());
                    final PreferenceScreen inflateFromResource = preferenceScreen3;
                    continue;
                }
                break;
            }
        }
        return (PreferenceScreen)o;
    }
    
    public void updatePreferenceIntents(final PreferenceGroup preferenceGroup, final String s, final Account account) {
        final PackageManager packageManager = this.mFragment.getActivity().getPackageManager();
        int i = 0;
        while (i < preferenceGroup.getPreferenceCount()) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (preference instanceof PreferenceGroup) {
                this.updatePreferenceIntents((PreferenceGroup)preference, s, account);
            }
            final Intent intent = preference.getIntent();
            if (intent != null) {
                if (TextUtils.equals((CharSequence)intent.getAction(), (CharSequence)"android.settings.LOCATION_SOURCE_SETTINGS")) {
                    preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new FragmentStarter(LocationSettings.class.getName(), 2131888059));
                }
                else {
                    if (packageManager.resolveActivityAsUser(intent, 65536, this.mUserHandle.getIdentifier()) == null) {
                        preferenceGroup.removePreference(preference);
                        continue;
                    }
                    intent.putExtra("account", (Parcelable)account);
                    intent.setFlags(intent.getFlags() | 0x10000000);
                    preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(final Preference preference) {
                            final Intent intent = preference.getIntent();
                            if (AccountTypePreferenceLoader.this.isSafeIntent(packageManager, intent, s)) {
                                AccountTypePreferenceLoader.this.mFragment.getActivity().startActivityAsUser(intent, AccountTypePreferenceLoader.this.mUserHandle);
                            }
                            else {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Refusing to launch authenticator intent becauseit exploits Settings permissions: ");
                                sb.append(intent);
                                Log.e("AccountTypePrefLoader", sb.toString());
                            }
                            return true;
                        }
                    });
                }
            }
            ++i;
        }
    }
    
    private class FragmentStarter implements OnPreferenceClickListener
    {
        private final String mClass;
        private final int mTitleRes;
        
        public FragmentStarter(final String mClass, final int mTitleRes) {
            this.mClass = mClass;
            this.mTitleRes = mTitleRes;
        }
        
        @Override
        public boolean onPreferenceClick(final Preference preference) {
            int metricsCategory;
            if (AccountTypePreferenceLoader.this.mFragment instanceof Instrumentable) {
                metricsCategory = ((Instrumentable)AccountTypePreferenceLoader.this.mFragment).getMetricsCategory();
            }
            else {
                metricsCategory = 0;
            }
            new SubSettingLauncher(preference.getContext()).setTitle(this.mTitleRes).setDestination(this.mClass).setSourceMetricsCategory(metricsCategory).launch();
            if (this.mClass.equals(LocationSettings.class.getName())) {
                AccountTypePreferenceLoader.this.mFragment.getActivity().sendBroadcast(new Intent("com.android.settings.accounts.LAUNCHING_LOCATION_SETTINGS"), "android.permission.WRITE_SECURE_SETTINGS");
            }
            return true;
        }
    }
}
