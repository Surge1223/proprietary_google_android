package com.android.settings.accounts;

import android.os.Binder;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.support.v7.preference.PreferenceScreen;
import android.os.UserHandle;
import android.view.View;
import android.support.v7.preference.PreferenceGroup;
import android.content.SyncStatusInfo;
import java.util.Date;
import android.content.pm.UserInfo;
import com.android.settings.Utils;
import android.os.UserManager;
import android.content.ContentResolver;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.IntentSender$SendIntentException;
import android.os.Bundle;
import android.content.Intent;
import java.util.Iterator;
import android.content.SyncInfo;
import java.util.List;
import android.app.Activity;
import android.content.pm.ProviderInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.accounts.AccountManager;
import com.google.android.collect.Lists;
import android.widget.ImageView;
import android.content.SyncAdapterType;
import java.util.ArrayList;
import android.widget.TextView;
import android.accounts.Account;

public class AccountSyncSettings extends AccountPreferenceBase
{
    private Account mAccount;
    private TextView mErrorInfoView;
    private ArrayList<SyncAdapterType> mInvisibleAdapters;
    private ImageView mProviderIcon;
    private TextView mProviderId;
    private TextView mUserId;
    
    public AccountSyncSettings() {
        this.mInvisibleAdapters = (ArrayList<SyncAdapterType>)Lists.newArrayList();
    }
    
    private boolean accountExists(final Account account) {
        if (account == null) {
            return false;
        }
        final Account[] accountsByTypeAsUser = AccountManager.get((Context)this.getActivity()).getAccountsByTypeAsUser(account.type, this.mUserHandle);
        for (int length = accountsByTypeAsUser.length, i = 0; i < length; ++i) {
            if (accountsByTypeAsUser[i].equals((Object)account)) {
                return true;
            }
        }
        return false;
    }
    
    private void addSyncStateSwitch(final Account account, final String key, final String s, final int n) {
        final SyncStateSwitchPreference syncStateSwitchPreference = (SyncStateSwitchPreference)this.getCachedPreference(key);
        SyncStateSwitchPreference syncStateSwitchPreference2;
        if (syncStateSwitchPreference == null) {
            syncStateSwitchPreference2 = new SyncStateSwitchPreference(this.getPrefContext(), account, key, s, n);
            this.getPreferenceScreen().addPreference(syncStateSwitchPreference2);
        }
        else {
            syncStateSwitchPreference.setup(account, key, s, n);
            syncStateSwitchPreference2 = syncStateSwitchPreference;
        }
        final PackageManager packageManager = this.getPackageManager();
        syncStateSwitchPreference2.setPersistent(false);
        final ProviderInfo resolveContentProviderAsUser = packageManager.resolveContentProviderAsUser(key, 0, this.mUserHandle.getIdentifier());
        if (resolveContentProviderAsUser == null) {
            return;
        }
        final CharSequence loadLabel = resolveContentProviderAsUser.loadLabel(packageManager);
        if (TextUtils.isEmpty(loadLabel)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Provider needs a label for authority '");
            sb.append(key);
            sb.append("'");
            Log.e("AccountPreferenceBase", sb.toString());
            return;
        }
        syncStateSwitchPreference2.setTitle(loadLabel);
        syncStateSwitchPreference2.setKey(key);
    }
    
    private void cancelSyncForEnabledProviders() {
        this.requestOrCancelSyncForEnabledProviders(false);
        final Activity activity = this.getActivity();
        if (activity != null) {
            activity.invalidateOptionsMenu();
        }
    }
    
    private boolean isSyncing(final List<SyncInfo> list, final Account account, final String s) {
        for (final SyncInfo syncInfo : list) {
            if (syncInfo.account.equals((Object)account) && syncInfo.authority.equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean requestAccountAccessIfNeeded(final String s) {
        if (s == null) {
            return false;
        }
        try {
            final int packageUidAsUser = this.getContext().getPackageManager().getPackageUidAsUser(s, this.mUserHandle.getIdentifier());
            final AccountManager accountManager = (AccountManager)this.getContext().getSystemService((Class)AccountManager.class);
            if (!accountManager.hasAccountAccess(this.mAccount, s, this.mUserHandle)) {
                final IntentSender requestAccountAccessIntentSenderAsUser = accountManager.createRequestAccountAccessIntentSenderAsUser(this.mAccount, s, this.mUserHandle);
                if (requestAccountAccessIntentSenderAsUser != null) {
                    try {
                        this.startIntentSenderForResult(requestAccountAccessIntentSenderAsUser, packageUidAsUser, (Intent)null, 0, 0, 0, (Bundle)null);
                        return true;
                    }
                    catch (IntentSender$SendIntentException ex) {
                        Log.e("AccountPreferenceBase", "Error requesting account access", (Throwable)ex);
                    }
                }
            }
            return false;
        }
        catch (PackageManager$NameNotFoundException ex2) {
            Log.e("AccountPreferenceBase", "Invalid sync ", (Throwable)ex2);
            return false;
        }
    }
    
    private void requestOrCancelSync(final Account account, final String s, final boolean b) {
        if (b) {
            final Bundle bundle = new Bundle();
            bundle.putBoolean("force", true);
            ContentResolver.requestSyncAsUser(account, s, this.mUserHandle.getIdentifier(), bundle);
        }
        else {
            ContentResolver.cancelSyncAsUser(account, s, this.mUserHandle.getIdentifier());
        }
    }
    
    private void requestOrCancelSyncForEnabledProviders(final boolean b) {
        for (int preferenceCount = this.getPreferenceScreen().getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = this.getPreferenceScreen().getPreference(i);
            if (preference instanceof SyncStateSwitchPreference) {
                final SyncStateSwitchPreference syncStateSwitchPreference = (SyncStateSwitchPreference)preference;
                if (syncStateSwitchPreference.isChecked()) {
                    this.requestOrCancelSync(syncStateSwitchPreference.getAccount(), syncStateSwitchPreference.getAuthority(), b);
                }
            }
        }
        if (this.mAccount != null) {
            final Iterator<SyncAdapterType> iterator = this.mInvisibleAdapters.iterator();
            while (iterator.hasNext()) {
                this.requestOrCancelSync(this.mAccount, iterator.next().authority, b);
            }
        }
    }
    
    private void setAccessibilityTitle() {
        final UserInfo userInfo = ((UserManager)this.getSystemService("user")).getUserInfo(this.mUserHandle.getIdentifier());
        final boolean b = userInfo != null && userInfo.isManagedProfile();
        final CharSequence title = this.getActivity().getTitle();
        int n;
        if (b) {
            n = 2131886223;
        }
        else {
            n = 2131886172;
        }
        this.getActivity().setTitle((CharSequence)Utils.createAccessibleSequence(title, this.getString(n, new Object[] { title })));
    }
    
    private void setFeedsState() {
        final Date date = new Date();
        final int identifier = this.mUserHandle.getIdentifier();
        final List currentSyncsAsUser = ContentResolver.getCurrentSyncsAsUser(identifier);
        int n = 0;
        this.updateAccountSwitches();
        for (int i = 0; i < this.getPreferenceScreen().getPreferenceCount(); ++i) {
            final Preference preference = this.getPreferenceScreen().getPreference(i);
            if (preference instanceof SyncStateSwitchPreference) {
                final SyncStateSwitchPreference syncStateSwitchPreference = (SyncStateSwitchPreference)preference;
                final String authority = syncStateSwitchPreference.getAuthority();
                final Account account = syncStateSwitchPreference.getAccount();
                final SyncStatusInfo syncStatusAsUser = ContentResolver.getSyncStatusAsUser(account, authority, identifier);
                final boolean syncAutomaticallyAsUser = ContentResolver.getSyncAutomaticallyAsUser(account, authority, identifier);
                final boolean b = syncStatusAsUser != null && syncStatusAsUser.pending;
                final boolean b2 = syncStatusAsUser != null && syncStatusAsUser.initialize;
                final boolean syncing = this.isSyncing(currentSyncsAsUser, account, authority);
                boolean failed = syncStatusAsUser != null && syncStatusAsUser.lastFailureTime != 0L && syncStatusAsUser.getLastFailureMesgAsInt(0) != 1;
                final int n2 = n;
                if (!syncAutomaticallyAsUser) {
                    failed = false;
                }
                n = n2;
                if (failed) {
                    n = n2;
                    if (!syncing) {
                        n = n2;
                        if (!b) {
                            n = 1;
                        }
                    }
                }
                if (Log.isLoggable("AccountPreferenceBase", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Update sync status: ");
                    sb.append(account);
                    sb.append(" ");
                    sb.append(authority);
                    sb.append(" active = ");
                    sb.append(syncing);
                    sb.append(" pend =");
                    sb.append(b);
                    Log.d("AccountPreferenceBase", sb.toString());
                }
                long lastSuccessTime;
                if (syncStatusAsUser == null) {
                    lastSuccessTime = 0L;
                }
                else {
                    lastSuccessTime = syncStatusAsUser.lastSuccessTime;
                }
                if (!syncAutomaticallyAsUser) {
                    syncStateSwitchPreference.setSummary(2131889427);
                }
                else if (syncing) {
                    syncStateSwitchPreference.setSummary(2131889432);
                }
                else if (lastSuccessTime != 0L) {
                    date.setTime(lastSuccessTime);
                    syncStateSwitchPreference.setSummary(this.getResources().getString(2131888014, new Object[] { this.formatSyncDate(date) }));
                }
                else {
                    syncStateSwitchPreference.setSummary("");
                }
                final boolean b3 = false;
                final int isSyncableAsUser = ContentResolver.getIsSyncableAsUser(account, authority, identifier);
                syncStateSwitchPreference.setActive(syncing && isSyncableAsUser >= 0 && !b2);
                syncStateSwitchPreference.setPending(b && isSyncableAsUser >= 0 && !b2);
                syncStateSwitchPreference.setFailed(failed);
                final boolean oneTimeSyncMode = ContentResolver.getMasterSyncAutomaticallyAsUser(identifier) ^ true;
                syncStateSwitchPreference.setOneTimeSyncMode(oneTimeSyncMode);
                syncStateSwitchPreference.setChecked(oneTimeSyncMode || syncAutomaticallyAsUser || b3);
            }
        }
        final boolean b4 = false;
        final TextView mErrorInfoView = this.mErrorInfoView;
        int visibility;
        if (n != 0) {
            visibility = (b4 ? 1 : 0);
        }
        else {
            visibility = 8;
        }
        mErrorInfoView.setVisibility(visibility);
    }
    
    private void startSyncForEnabledProviders() {
        this.requestOrCancelSyncForEnabledProviders(true);
        final Activity activity = this.getActivity();
        if (activity != null) {
            activity.invalidateOptionsMenu();
        }
    }
    
    private void updateAccountSwitches() {
        this.mInvisibleAdapters.clear();
        final SyncAdapterType[] syncAdapterTypesAsUser = ContentResolver.getSyncAdapterTypesAsUser(this.mUserHandle.getIdentifier());
        final ArrayList<SyncAdapterType> list = new ArrayList<SyncAdapterType>();
        for (int i = 0; i < syncAdapterTypesAsUser.length; ++i) {
            final SyncAdapterType syncAdapterType = syncAdapterTypesAsUser[i];
            if (syncAdapterType.accountType.equals(this.mAccount.type)) {
                if (syncAdapterType.isUserVisible()) {
                    if (Log.isLoggable("AccountPreferenceBase", 3)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("updateAccountSwitches: added authority ");
                        sb.append(syncAdapterType.authority);
                        sb.append(" to accountType ");
                        sb.append(syncAdapterType.accountType);
                        Log.d("AccountPreferenceBase", sb.toString());
                    }
                    list.add(syncAdapterType);
                }
                else {
                    this.mInvisibleAdapters.add(syncAdapterType);
                }
            }
        }
        if (Log.isLoggable("AccountPreferenceBase", 3)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("looking for sync adapters that match account ");
            sb2.append(this.mAccount);
            Log.d("AccountPreferenceBase", sb2.toString());
        }
        this.cacheRemoveAllPrefs(this.getPreferenceScreen());
        for (int j = 0; j < list.size(); ++j) {
            final SyncAdapterType syncAdapterType2 = list.get(j);
            final int isSyncableAsUser = ContentResolver.getIsSyncableAsUser(this.mAccount, syncAdapterType2.authority, this.mUserHandle.getIdentifier());
            if (Log.isLoggable("AccountPreferenceBase", 3)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("  found authority ");
                sb3.append(syncAdapterType2.authority);
                sb3.append(" ");
                sb3.append(isSyncableAsUser);
                Log.d("AccountPreferenceBase", sb3.toString());
            }
            if (isSyncableAsUser > 0) {
                try {
                    this.addSyncStateSwitch(this.mAccount, syncAdapterType2.authority, syncAdapterType2.getPackageName(), this.getContext().getPackageManager().getPackageUidAsUser(syncAdapterType2.getPackageName(), this.mUserHandle.getIdentifier()));
                }
                catch (PackageManager$NameNotFoundException ex) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("No uid for package");
                    sb4.append(syncAdapterType2.getPackageName());
                    Log.e("AccountPreferenceBase", sb4.toString(), (Throwable)ex);
                }
            }
        }
        this.removeCachedPrefs(this.getPreferenceScreen());
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        if (n != 102) {
            return 0;
        }
        return 587;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887793;
    }
    
    @Override
    public int getMetricsCategory() {
        return 9;
    }
    
    protected void initializeUi(final View view) {
        (this.mErrorInfoView = (TextView)view.findViewById(2131362712)).setVisibility(8);
        this.mUserId = (TextView)view.findViewById(2131362793);
        this.mProviderId = (TextView)view.findViewById(2131362494);
        this.mProviderIcon = (ImageView)view.findViewById(2131362493);
    }
    
    @Override
    public void onAccountsUpdate(final UserHandle userHandle) {
        super.onAccountsUpdate(userHandle);
        if (!this.accountExists(this.mAccount)) {
            this.finish();
            return;
        }
        this.updateAccountSwitches();
        this.onSyncStateUpdated();
    }
    
    @Override
    public void onActivityCreated(Bundle arguments) {
        super.onActivityCreated(arguments);
        arguments = this.getArguments();
        if (arguments == null) {
            Log.e("AccountPreferenceBase", "No arguments provided when starting intent. ACCOUNT_KEY needed.");
            this.finish();
            return;
        }
        this.mAccount = (Account)arguments.getParcelable("account");
        if (!this.accountExists(this.mAccount)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Account provided does not exist: ");
            sb.append(this.mAccount);
            Log.e("AccountPreferenceBase", sb.toString());
            this.finish();
            return;
        }
        if (Log.isLoggable("AccountPreferenceBase", 2)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Got account: ");
            sb2.append(this.mAccount);
            Log.v("AccountPreferenceBase", sb2.toString());
        }
        this.mUserId.setText((CharSequence)this.mAccount.name);
        this.mProviderId.setText((CharSequence)this.mAccount.type);
    }
    
    public void onActivityResult(final int n, int i, final Intent intent) {
        if (i == -1) {
            int preferenceCount;
            Preference preference;
            SyncStateSwitchPreference syncStateSwitchPreference;
            for (preferenceCount = this.getPreferenceScreen().getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
                preference = this.getPreferenceScreen().getPreference(i);
                if (preference instanceof SyncStateSwitchPreference) {
                    syncStateSwitchPreference = (SyncStateSwitchPreference)preference;
                    if (syncStateSwitchPreference.getUid() == n) {
                        this.onPreferenceTreeClick(syncStateSwitchPreference);
                        return;
                    }
                }
            }
        }
    }
    
    @Override
    protected void onAuthDescriptionsUpdated() {
        super.onAuthDescriptionsUpdated();
        if (this.mAccount != null) {
            this.mProviderIcon.setImageDrawable(this.getDrawableForType(this.mAccount.type));
            this.mProviderId.setText(this.getLabelForType(this.mAccount.type));
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setPreferenceScreen(null);
        this.addPreferencesFromResource(2132082699);
        this.getPreferenceScreen().setOrderingAsAdded(false);
        this.setAccessibilityTitle();
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        Object create = null;
        if (n == 102) {
            create = new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131886951).setMessage(2131886950).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
        }
        return (Dialog)create;
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final MenuItem setIcon = menu.add(0, 1, 0, (CharSequence)this.getString(2131889435)).setIcon(2131231067);
        final MenuItem setIcon2 = menu.add(0, 2, 0, (CharSequence)this.getString(2131889434)).setIcon(17301560);
        setIcon.setShowAsAction(4);
        setIcon2.setShowAsAction(4);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558440, viewGroup, false);
        final ViewGroup viewGroup2 = (ViewGroup)inflate.findViewById(2131362462);
        Utils.prepareCustomPreferencesList(viewGroup, inflate, (View)viewGroup2, false);
        viewGroup2.addView(super.onCreateView(layoutInflater, viewGroup2, bundle));
        this.initializeUi(inflate);
        return inflate;
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2: {
                this.cancelSyncForEnabledProviders();
                return true;
            }
            case 1: {
                this.startSyncForEnabledProviders();
                return true;
            }
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mAuthenticatorHelper.stopListeningToAccountUpdates();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (this.getActivity() == null) {
            return false;
        }
        if (preference instanceof SyncStateSwitchPreference) {
            final SyncStateSwitchPreference syncStateSwitchPreference = (SyncStateSwitchPreference)preference;
            final String authority = syncStateSwitchPreference.getAuthority();
            final Account account = syncStateSwitchPreference.getAccount();
            final int identifier = this.mUserHandle.getIdentifier();
            final String packageName = syncStateSwitchPreference.getPackageName();
            final boolean syncAutomaticallyAsUser = ContentResolver.getSyncAutomaticallyAsUser(account, authority, identifier);
            if (syncStateSwitchPreference.isOneTimeSyncMode()) {
                if (this.requestAccountAccessIfNeeded(packageName)) {
                    return true;
                }
                this.requestOrCancelSync(account, authority, true);
            }
            else {
                final boolean checked = syncStateSwitchPreference.isChecked();
                if (checked != syncAutomaticallyAsUser) {
                    if (checked && this.requestAccountAccessIfNeeded(packageName)) {
                        return true;
                    }
                    ContentResolver.setSyncAutomaticallyAsUser(account, authority, checked, identifier);
                    if (!ContentResolver.getMasterSyncAutomaticallyAsUser(identifier) || !checked) {
                        this.requestOrCancelSync(account, authority, checked);
                    }
                }
            }
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        super.onPrepareOptionsMenu(menu);
        final boolean empty = ContentResolver.getCurrentSyncsAsUser(this.mUserHandle.getIdentifier()).isEmpty();
        boolean visible = true;
        final boolean visible2 = empty ^ true;
        final MenuItem item = menu.findItem(1);
        if (visible2) {
            visible = false;
        }
        item.setVisible(visible);
        menu.findItem(2).setVisible(visible2);
    }
    
    @Override
    public void onResume() {
        this.removePreference("dummy");
        this.mAuthenticatorHelper.listenToAccountUpdates();
        this.updateAuthDescriptions();
        this.onAccountsUpdate(Binder.getCallingUserHandle());
        super.onResume();
    }
    
    @Override
    protected void onSyncStateUpdated() {
        if (!this.isResumed()) {
            return;
        }
        this.setFeedsState();
        final Activity activity = this.getActivity();
        if (activity != null) {
            activity.invalidateOptionsMenu();
        }
    }
}
