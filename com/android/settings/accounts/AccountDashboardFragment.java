package com.android.settings.accounts;

import android.text.TextUtils;
import android.text.BidiFormatter;
import com.android.settingslib.accounts.AuthenticatorHelper;
import android.os.UserHandle;
import com.android.settings.users.AutoSyncWorkDataPreferenceController;
import com.android.settings.users.AutoSyncPersonalDataPreferenceController;
import android.app.Fragment;
import com.android.settings.users.AutoSyncDataPreferenceController;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.SettingsPreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import com.android.settings.search.BaseSearchIndexProvider;
import android.content.Context;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class AccountDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new AccountDashboardFragment.SummaryProvider((Context)activity, summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082701;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<AutoSyncWorkDataPreferenceController> list = (ArrayList<AutoSyncWorkDataPreferenceController>)new ArrayList<AbstractPreferenceController>();
        final AccountPreferenceController accountPreferenceController = new AccountPreferenceController(context, this, this.getIntent().getStringArrayExtra("authorities"));
        this.getLifecycle().addObserver(accountPreferenceController);
        list.add(accountPreferenceController);
        list.add(new AutoSyncDataPreferenceController(context, this));
        list.add(new AutoSyncPersonalDataPreferenceController(context, this));
        list.add(new AutoSyncWorkDataPreferenceController(context, this));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887837;
    }
    
    @Override
    protected String getLogTag() {
        return "AccountDashboardFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 8;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082701;
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                final AuthenticatorHelper authenticatorHelper = new AuthenticatorHelper(this.mContext, UserHandle.of(UserHandle.myUserId()), null);
                final String[] enabledAccountTypes = authenticatorHelper.getEnabledAccountTypes();
                final BidiFormatter instance = BidiFormatter.getInstance();
                CharSequence string;
                if (enabledAccountTypes != null && enabledAccountTypes.length != 0) {
                    int min = Math.min(3, enabledAccountTypes.length);
                    CharSequence charSequence = null;
                    int n = 0;
                    while (true) {
                        string = charSequence;
                        if (n >= enabledAccountTypes.length) {
                            break;
                        }
                        string = charSequence;
                        if (min <= 0) {
                            break;
                        }
                        final CharSequence labelForType = authenticatorHelper.getLabelForType(this.mContext, enabledAccountTypes[n]);
                        if (!TextUtils.isEmpty(labelForType)) {
                            if (charSequence == null) {
                                charSequence = instance.unicodeWrap(labelForType);
                            }
                            else {
                                charSequence = this.mContext.getString(2131887916, new Object[] { charSequence, instance.unicodeWrap(labelForType) });
                            }
                            --min;
                        }
                        ++n;
                    }
                }
                else {
                    string = this.mContext.getString(2131886226);
                }
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, string);
            }
        }
    }
}
