package com.android.settings.accounts;

import com.android.settingslib.RestrictedLockUtils;
import android.graphics.drawable.Drawable;
import android.content.Context;
import com.android.settingslib.RestrictedPreference;

public class ProviderPreference extends RestrictedPreference
{
    private String mAccountType;
    
    public ProviderPreference(final Context context, final String mAccountType, final Drawable icon, final CharSequence title) {
        super(context);
        this.setIconSize(1);
        this.mAccountType = mAccountType;
        this.setIcon(icon);
        this.setPersistent(false);
        this.setTitle(title);
        this.useAdminDisabledSummary(true);
    }
    
    public void checkAccountManagementAndSetDisabled(final int n) {
        this.setDisabledByAdmin(RestrictedLockUtils.checkIfAccountManagementDisabled(this.getContext(), this.getAccountType(), n));
    }
    
    public String getAccountType() {
        return this.mAccountType;
    }
}
