package com.android.settings.accounts;

import android.content.ContentResolver;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.Utils;
import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.Date;
import com.android.settingslib.utils.ThreadUtils;
import android.util.Log;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.SyncStatusObserver;
import java.text.DateFormat;
import com.android.settingslib.accounts.AuthenticatorHelper;
import com.android.settings.SettingsPreferenceFragment;

public final class _$$Lambda$AccountPreferenceBase$7XBpqCguERDVZFsa_jC8V8rk8o8 implements Runnable
{
    @Override
    public final void run() {
        this.f$0.onSyncStateUpdated();
    }
}
