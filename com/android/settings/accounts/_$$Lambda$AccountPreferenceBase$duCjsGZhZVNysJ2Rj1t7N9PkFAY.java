package com.android.settings.accounts;

import android.content.ContentResolver;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.Utils;
import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.Date;
import com.android.settingslib.utils.ThreadUtils;
import android.util.Log;
import android.os.UserHandle;
import android.os.UserManager;
import java.text.DateFormat;
import com.android.settingslib.accounts.AuthenticatorHelper;
import com.android.settings.SettingsPreferenceFragment;
import android.content.SyncStatusObserver;

public final class _$$Lambda$AccountPreferenceBase$duCjsGZhZVNysJ2Rj1t7N9PkFAY implements SyncStatusObserver
{
    public final void onStatusChanged(final int n) {
        AccountPreferenceBase.lambda$new$1(this.f$0, n);
    }
}
