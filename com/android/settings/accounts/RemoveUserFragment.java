package com.android.settings.accounts;

import android.content.Context;
import com.android.settings.users.UserDialogs;
import android.os.UserManager;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class RemoveUserFragment extends InstrumentedDialogFragment
{
    static RemoveUserFragment newInstance(final int n) {
        final Bundle arguments = new Bundle();
        arguments.putInt("userId", n);
        final RemoveUserFragment removeUserFragment = new RemoveUserFragment();
        removeUserFragment.setArguments(arguments);
        return removeUserFragment;
    }
    
    @Override
    public int getMetricsCategory() {
        return 534;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final int int1 = this.getArguments().getInt("userId");
        return UserDialogs.createRemoveDialog((Context)this.getActivity(), int1, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                ((UserManager)RemoveUserFragment.this.getActivity().getSystemService("user")).removeUser(int1);
            }
        });
    }
}
