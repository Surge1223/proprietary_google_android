package com.android.settings.accounts;

import android.content.ContentResolver;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.Utils;
import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.Date;
import com.android.settingslib.utils.ThreadUtils;
import android.util.Log;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.SyncStatusObserver;
import java.text.DateFormat;
import com.android.settingslib.accounts.AuthenticatorHelper;
import com.android.settings.SettingsPreferenceFragment;

abstract class AccountPreferenceBase extends SettingsPreferenceFragment implements OnAccountsUpdateListener
{
    protected static final boolean VERBOSE;
    protected AccountTypePreferenceLoader mAccountTypePreferenceLoader;
    protected AuthenticatorHelper mAuthenticatorHelper;
    private DateFormat mDateFormat;
    private Object mStatusChangeListenerHandle;
    private SyncStatusObserver mSyncStatusObserver;
    private DateFormat mTimeFormat;
    private UserManager mUm;
    protected UserHandle mUserHandle;
    
    static {
        VERBOSE = Log.isLoggable("AccountPreferenceBase", 2);
    }
    
    AccountPreferenceBase() {
        this.mSyncStatusObserver = (SyncStatusObserver)new _$$Lambda$AccountPreferenceBase$duCjsGZhZVNysJ2Rj1t7N9PkFAY(this);
    }
    
    protected String formatSyncDate(final Date date) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mDateFormat.format(date));
        sb.append(" ");
        sb.append(this.mTimeFormat.format(date));
        return sb.toString();
    }
    
    protected Drawable getDrawableForType(final String s) {
        return this.mAuthenticatorHelper.getDrawableForType((Context)this.getActivity(), s);
    }
    
    protected CharSequence getLabelForType(final String s) {
        return this.mAuthenticatorHelper.getLabelForType((Context)this.getActivity(), s);
    }
    
    @Override
    public void onAccountsUpdate(final UserHandle userHandle) {
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final Activity activity = this.getActivity();
        this.mDateFormat = android.text.format.DateFormat.getDateFormat((Context)activity);
        this.mTimeFormat = android.text.format.DateFormat.getTimeFormat((Context)activity);
    }
    
    protected void onAuthDescriptionsUpdated() {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mUm = (UserManager)this.getSystemService("user");
        final Activity activity = this.getActivity();
        this.mUserHandle = Utils.getSecureTargetUser(activity.getActivityToken(), this.mUm, this.getArguments(), activity.getIntent().getExtras());
        this.mAuthenticatorHelper = new AuthenticatorHelper((Context)activity, this.mUserHandle, (AuthenticatorHelper.OnAccountsUpdateListener)this);
        this.mAccountTypePreferenceLoader = new AccountTypePreferenceLoader(this, this.mAuthenticatorHelper, this.mUserHandle);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        ContentResolver.removeStatusChangeListener(this.mStatusChangeListenerHandle);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mStatusChangeListenerHandle = ContentResolver.addStatusChangeListener(13, this.mSyncStatusObserver);
        this.onSyncStateUpdated();
    }
    
    protected void onSyncStateUpdated() {
    }
    
    public void updateAuthDescriptions() {
        this.mAuthenticatorHelper.updateAuthDescriptions((Context)this.getActivity());
        this.onAuthDescriptionsUpdated();
    }
}
