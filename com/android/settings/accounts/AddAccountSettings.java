package com.android.settings.accounts;

import com.android.settings.password.ChooseLockSettingsHelper;
import android.widget.Toast;
import android.os.UserManager;
import com.android.settings.Settings;
import android.os.Handler;
import android.accounts.AccountManager;
import android.content.ComponentName;
import android.accounts.OperationCanceledException;
import java.io.IOException;
import android.accounts.AuthenticatorException;
import android.util.Log;
import android.content.Context;
import com.android.settings.Utils;
import android.os.Parcelable;
import android.content.Intent;
import android.accounts.AccountManagerFuture;
import android.os.UserHandle;
import android.app.PendingIntent;
import android.os.Bundle;
import android.accounts.AccountManagerCallback;
import android.app.Activity;

public class AddAccountSettings extends Activity
{
    private boolean mAddAccountCalled;
    private final AccountManagerCallback<Bundle> mCallback;
    private PendingIntent mPendingIntent;
    private UserHandle mUserHandle;
    
    public AddAccountSettings() {
        this.mCallback = (AccountManagerCallback<Bundle>)new AccountManagerCallback<Bundle>() {
            public void run(final AccountManagerFuture<Bundle> accountManagerFuture) {
                final boolean b = true;
                final boolean b2 = true;
                final boolean b3 = true;
                final boolean b4 = true;
                final boolean b5 = true;
                boolean b6 = b;
                boolean b7 = b2;
                boolean b8 = b3;
                boolean b9 = b4;
                try {
                    while (true) {
                        try {
                            final Bundle bundle = (Bundle)accountManagerFuture.getResult();
                            b6 = b;
                            b7 = b2;
                            b8 = b3;
                            b9 = b4;
                            final Intent intent = (Intent)bundle.get("intent");
                            boolean b14;
                            if (intent != null) {
                                final boolean b10 = false;
                                final boolean b11 = false;
                                final boolean b12 = false;
                                final boolean b13 = false;
                                b14 = false;
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                final Bundle bundle2 = new Bundle();
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                bundle2.putParcelable("pendingIntent", (Parcelable)AddAccountSettings.this.mPendingIntent);
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                bundle2.putBoolean("hasMultipleUsers", Utils.hasMultipleUsers((Context)AddAccountSettings.this));
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                bundle2.putParcelable("android.intent.extra.USER", (Parcelable)AddAccountSettings.this.mUserHandle);
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                intent.putExtras(bundle2);
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                intent.addFlags(268435456);
                                b6 = b10;
                                b7 = b11;
                                b8 = b12;
                                b9 = b13;
                                AddAccountSettings.this.startActivityForResultAsUser(intent, 2, AddAccountSettings.this.mUserHandle);
                            }
                            else {
                                b6 = b;
                                b7 = b2;
                                b8 = b3;
                                b9 = b4;
                                AddAccountSettings.this.setResult(-1);
                                b14 = b5;
                                b6 = b;
                                b7 = b2;
                                b8 = b3;
                                b9 = b4;
                                if (AddAccountSettings.this.mPendingIntent != null) {
                                    b6 = b;
                                    b7 = b2;
                                    b8 = b3;
                                    b9 = b4;
                                    AddAccountSettings.this.mPendingIntent.cancel();
                                    b6 = b;
                                    b7 = b2;
                                    b8 = b3;
                                    b9 = b4;
                                    AddAccountSettings.this.mPendingIntent = null;
                                    b14 = b5;
                                }
                            }
                            b6 = b14;
                            b7 = b14;
                            b8 = b14;
                            b9 = b14;
                            if (Log.isLoggable("AddAccountSettings", 2)) {
                                b6 = b14;
                                b7 = b14;
                                b8 = b14;
                                b9 = b14;
                                b6 = b14;
                                b7 = b14;
                                b8 = b14;
                                b9 = b14;
                                final StringBuilder sb = new StringBuilder();
                                b6 = b14;
                                b7 = b14;
                                b8 = b14;
                                b9 = b14;
                                sb.append("account added: ");
                                b6 = b14;
                                b7 = b14;
                                b8 = b14;
                                b9 = b14;
                                sb.append(bundle);
                                b6 = b14;
                                b7 = b14;
                                b8 = b14;
                                b9 = b14;
                                Log.v("AddAccountSettings", sb.toString());
                            }
                            if (b14) {
                                AddAccountSettings.this.finish();
                            }
                        }
                        finally {
                            if (b6) {
                                AddAccountSettings.this.finish();
                            }
                            continue;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("addAccount failed: ");
                            final AuthenticatorException ex;
                            sb2.append(ex);
                            b6 = b7;
                            Log.v("AddAccountSettings", sb2.toString());
                            // iftrue(Label_0731:, b7 == false)
                            continue;
                            Label_0731: {
                                return;
                            }
                            b6 = b9;
                            Log.v("AddAccountSettings", "addAccount was canceled");
                            // iftrue(Label_0731:, b9 == false)
                            continue;
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("addAccount failed: ");
                            final IOException ex2;
                            sb3.append(ex2);
                            b6 = b8;
                            Log.v("AddAccountSettings", sb3.toString());
                        }
                        // iftrue(Label_0731:, b8 == false)
                        break;
                    }
                }
                catch (AuthenticatorException ex3) {}
                catch (IOException ex4) {}
                catch (OperationCanceledException ex5) {}
            }
        };
        this.mAddAccountCalled = false;
    }
    
    private void addAccount(final String s) {
        final Bundle bundle = new Bundle();
        final Intent intent = new Intent();
        intent.setComponent(new ComponentName("SHOULDN'T RESOLVE!", "SHOULDN'T RESOLVE!"));
        intent.setAction("SHOULDN'T RESOLVE!");
        intent.addCategory("SHOULDN'T RESOLVE!");
        bundle.putParcelable("pendingIntent", (Parcelable)(this.mPendingIntent = PendingIntent.getBroadcast((Context)this, 0, intent, 0)));
        bundle.putBoolean("hasMultipleUsers", Utils.hasMultipleUsers((Context)this));
        AccountManager.get((Context)this).addAccountAsUser(s, (String)null, (String[])null, bundle, (Activity)null, (AccountManagerCallback)this.mCallback, (Handler)null, this.mUserHandle);
        this.mAddAccountCalled = true;
    }
    
    private void requestChooseAccount() {
        final String[] stringArrayExtra = this.getIntent().getStringArrayExtra("authorities");
        final String[] stringArrayExtra2 = this.getIntent().getStringArrayExtra("account_types");
        final Intent intent = new Intent((Context)this, (Class)Settings.ChooseAccountActivity.class);
        if (stringArrayExtra != null) {
            intent.putExtra("authorities", stringArrayExtra);
        }
        if (stringArrayExtra2 != null) {
            intent.putExtra("account_types", stringArrayExtra2);
        }
        intent.putExtra("android.intent.extra.USER", (Parcelable)this.mUserHandle);
        this.startActivityForResult(intent, 1);
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        switch (n) {
            case 3: {
                if (n2 == -1) {
                    this.requestChooseAccount();
                    break;
                }
                this.finish();
                break;
            }
            case 2: {
                this.setResult(n2);
                if (this.mPendingIntent != null) {
                    this.mPendingIntent.cancel();
                    this.mPendingIntent = null;
                }
                this.finish();
                break;
            }
            case 1: {
                if (n2 == 0) {
                    if (intent != null) {
                        this.startActivityAsUser(intent, this.mUserHandle);
                    }
                    this.setResult(n2);
                    this.finish();
                    return;
                }
                this.addAccount(intent.getStringExtra("selected_account"));
                break;
            }
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.mAddAccountCalled = bundle.getBoolean("AddAccountCalled");
            if (Log.isLoggable("AddAccountSettings", 2)) {
                Log.v("AddAccountSettings", "restored");
            }
        }
        final UserManager userManager = (UserManager)this.getSystemService("user");
        this.mUserHandle = Utils.getSecureTargetUser(this.getActivityToken(), userManager, null, this.getIntent().getExtras());
        if (userManager.hasUserRestriction("no_modify_accounts", this.mUserHandle)) {
            Toast.makeText((Context)this, 2131889691, 1).show();
            this.finish();
            return;
        }
        if (this.mAddAccountCalled) {
            this.finish();
            return;
        }
        if (Utils.startQuietModeDialogIfNecessary((Context)this, userManager, this.mUserHandle.getIdentifier())) {
            this.finish();
            return;
        }
        if (userManager.isUserUnlocked(this.mUserHandle)) {
            this.requestChooseAccount();
        }
        else if (!new ChooseLockSettingsHelper(this).launchConfirmationActivity(3, this.getString(2131889578), false, this.mUserHandle.getIdentifier())) {
            this.requestChooseAccount();
        }
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("AddAccountCalled", this.mAddAccountCalled);
        if (Log.isLoggable("AddAccountSettings", 2)) {
            Log.v("AddAccountSettings", "saved");
        }
    }
}
