package com.android.settings.accounts;

import android.text.TextUtils;
import android.util.Log;
import android.app.ActivityManager;
import android.view.View;
import android.widget.TextView;
import com.android.settingslib.widget.AnimatedImageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.accounts.Account;
import android.support.v14.preference.SwitchPreference;

public class SyncStateSwitchPreference extends SwitchPreference
{
    private Account mAccount;
    private String mAuthority;
    private boolean mFailed;
    private boolean mIsActive;
    private boolean mIsPending;
    private boolean mOneTimeSyncMode;
    private String mPackageName;
    private int mUid;
    
    public SyncStateSwitchPreference(final Context context, final Account account, final String s, final String s2, final int n) {
        super(context, null, 0, 2131951973);
        this.mIsActive = false;
        this.mIsPending = false;
        this.mFailed = false;
        this.mOneTimeSyncMode = false;
        this.setup(account, s, s2, n);
    }
    
    public SyncStateSwitchPreference(final Context context, final AttributeSet set) {
        super(context, set, 0, 2131951973);
        this.mIsActive = false;
        this.mIsPending = false;
        this.mFailed = false;
        this.mOneTimeSyncMode = false;
        this.mAccount = null;
        this.mAuthority = null;
        this.mPackageName = null;
        this.mUid = 0;
    }
    
    public Account getAccount() {
        return this.mAccount;
    }
    
    public String getAuthority() {
        return this.mAuthority;
    }
    
    public String getPackageName() {
        return this.mPackageName;
    }
    
    public int getUid() {
        return this.mUid;
    }
    
    public boolean isOneTimeSyncMode() {
        return this.mOneTimeSyncMode;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final AnimatedImageView animatedImageView = (AnimatedImageView)preferenceViewHolder.findViewById(2131362710);
        final View viewById = preferenceViewHolder.findViewById(2131362711);
        final boolean b = this.mIsActive || this.mIsPending;
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        animatedImageView.setVisibility(visibility);
        animatedImageView.setAnimating(this.mIsActive);
        int visibility2;
        if (this.mFailed && !b) {
            visibility2 = 0;
        }
        else {
            visibility2 = 8;
        }
        viewById.setVisibility(visibility2);
        final View viewById2 = preferenceViewHolder.findViewById(16908352);
        if (this.mOneTimeSyncMode) {
            viewById2.setVisibility(8);
            ((TextView)preferenceViewHolder.findViewById(16908304)).setText((CharSequence)this.getContext().getString(2131889436, new Object[] { this.getSummary() }));
        }
        else {
            viewById2.setVisibility(0);
        }
    }
    
    @Override
    protected void onClick() {
        if (!this.mOneTimeSyncMode) {
            if (ActivityManager.isUserAMonkey()) {
                Log.d("SyncState", "ignoring monkey's attempt to flip sync state");
            }
            else {
                super.onClick();
            }
        }
    }
    
    public void setActive(final boolean mIsActive) {
        this.mIsActive = mIsActive;
        this.notifyChanged();
    }
    
    public void setFailed(final boolean mFailed) {
        this.mFailed = mFailed;
        this.notifyChanged();
    }
    
    public void setOneTimeSyncMode(final boolean mOneTimeSyncMode) {
        this.mOneTimeSyncMode = mOneTimeSyncMode;
        this.notifyChanged();
    }
    
    public void setPending(final boolean mIsPending) {
        this.mIsPending = mIsPending;
        this.notifyChanged();
    }
    
    public void setup(final Account mAccount, final String mAuthority, final String mPackageName, final int mUid) {
        this.mAccount = mAccount;
        this.mAuthority = mAuthority;
        this.mPackageName = mPackageName;
        this.mUid = mUid;
        this.setVisible(TextUtils.isEmpty((CharSequence)this.mAuthority) ^ true);
        this.notifyChanged();
    }
}
