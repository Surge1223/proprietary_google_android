package com.android.settings.accounts;

import android.os.UserHandle;
import android.os.UserManager;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class EmergencyInfoPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public EmergencyInfoPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "emergency_info";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("emergency_info".equals(preference.getKey())) {
            final Intent intent = new Intent("android.settings.EDIT_EMERGENCY_INFO");
            intent.setFlags(67108864);
            this.mContext.startActivity(intent);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        final Intent setPackage = new Intent("android.settings.EDIT_EMERGENCY_INFO").setPackage("com.android.emergency");
        final PackageManager packageManager = this.mContext.getPackageManager();
        final boolean b = false;
        final List queryIntentActivities = packageManager.queryIntentActivities(setPackage, 0);
        boolean b2 = b;
        if (queryIntentActivities != null) {
            b2 = b;
            if (!queryIntentActivities.isEmpty()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(this.mContext.getString(2131887511, new Object[] { ((UserManager)this.mContext.getSystemService((Class)UserManager.class)).getUserInfo(UserHandle.myUserId()).name }));
    }
}
