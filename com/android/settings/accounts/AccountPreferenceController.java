package com.android.settings.accounts;

import android.support.v7.preference.PreferenceGroup;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import com.android.settings.core.SubSettingLauncher;
import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.AccessiblePreferenceCategory;
import android.text.BidiFormatter;
import java.util.Iterator;
import com.android.settingslib.RestrictedPreference;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.android.settings.Utils;
import android.content.pm.UserInfo;
import android.graphics.drawable.Drawable;
import android.accounts.Account;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import android.os.Parcelable;
import android.os.Bundle;
import android.accounts.AccountManager;
import android.util.ArrayMap;
import android.os.UserHandle;
import java.util.ArrayList;
import android.util.Log;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.UserManager;
import android.util.SparseArray;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.accounts.AuthenticatorHelper;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class AccountPreferenceController extends AbstractPreferenceController implements OnPreferenceClickListener, PreferenceControllerMixin, OnAccountsUpdateListener, LifecycleObserver, OnPause, OnResume
{
    private int mAccountProfileOrder;
    private String[] mAuthorities;
    private int mAuthoritiesCount;
    private AccountRestrictionHelper mHelper;
    private ManagedProfileBroadcastReceiver mManagedProfileBroadcastReceiver;
    private MetricsFeatureProvider mMetricsFeatureProvider;
    private SettingsPreferenceFragment mParent;
    private Preference mProfileNotAvailablePreference;
    private SparseArray<ProfileData> mProfiles;
    private UserManager mUm;
    
    public AccountPreferenceController(final Context context, final SettingsPreferenceFragment settingsPreferenceFragment, final String[] array) {
        this(context, settingsPreferenceFragment, array, new AccountRestrictionHelper(context));
    }
    
    AccountPreferenceController(final Context context, final SettingsPreferenceFragment mParent, final String[] mAuthorities, final AccountRestrictionHelper mHelper) {
        super(context);
        this.mProfiles = (SparseArray<ProfileData>)new SparseArray();
        this.mManagedProfileBroadcastReceiver = new ManagedProfileBroadcastReceiver();
        this.mAuthoritiesCount = 0;
        this.mAccountProfileOrder = 1;
        this.mUm = (UserManager)context.getSystemService("user");
        this.mAuthorities = mAuthorities;
        this.mParent = mParent;
        if (this.mAuthorities != null) {
            this.mAuthoritiesCount = this.mAuthorities.length;
        }
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(this.mContext).getMetricsFeatureProvider();
        this.mHelper = mHelper;
    }
    
    private boolean accountTypeHasAnyRequestedAuthorities(final AuthenticatorHelper authenticatorHelper, final String s) {
        if (this.mAuthoritiesCount == 0) {
            return true;
        }
        final ArrayList<String> authoritiesForAccountType = authenticatorHelper.getAuthoritiesForAccountType(s);
        if (authoritiesForAccountType == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No sync authorities for account type: ");
            sb.append(s);
            Log.d("AccountPrefController", sb.toString());
            return false;
        }
        for (int i = 0; i < this.mAuthoritiesCount; ++i) {
            if (authoritiesForAccountType.contains(this.mAuthorities[i])) {
                return true;
            }
        }
        return false;
    }
    
    private ArrayList<AccountTypePreference> getAccountTypePreferences(final AuthenticatorHelper authenticatorHelper, final UserHandle userHandle, final ArrayMap<String, AccountTypePreference> arrayMap) {
        final String[] enabledAccountTypes = authenticatorHelper.getEnabledAccountTypes();
        final ArrayList list = new ArrayList<AccountTypePreference>(enabledAccountTypes.length);
        for (int i = 0; i < enabledAccountTypes.length; ++i) {
            final String s = enabledAccountTypes[i];
            if (this.accountTypeHasAnyRequestedAuthorities(authenticatorHelper, s)) {
                final CharSequence labelForType = authenticatorHelper.getLabelForType(this.mContext, s);
                if (labelForType != null) {
                    final String packageForType = authenticatorHelper.getPackageForType(s);
                    final int labelIdForType = authenticatorHelper.getLabelIdForType(s);
                    final Account[] accountsByTypeAsUser = AccountManager.get(this.mContext).getAccountsByTypeAsUser(s, userHandle);
                    final Drawable drawableForType = authenticatorHelper.getDrawableForType(this.mContext, s);
                    final Context context = this.mParent.getPreferenceManager().getContext();
                    for (final Account account : accountsByTypeAsUser) {
                        final AccountTypePreference accountTypePreference = (AccountTypePreference)arrayMap.remove((Object)AccountTypePreference.buildKey(account));
                        if (accountTypePreference != null) {
                            list.add(accountTypePreference);
                        }
                        else if (AccountRestrictionHelper.showAccount(this.mAuthorities, authenticatorHelper.getAuthoritiesForAccountType(account.type))) {
                            final Bundle bundle = new Bundle();
                            bundle.putParcelable("account", (Parcelable)account);
                            bundle.putParcelable("user_handle", (Parcelable)userHandle);
                            bundle.putString("account_type", s);
                            bundle.putString("account_label", labelForType.toString());
                            bundle.putInt("account_title_res", labelIdForType);
                            bundle.putParcelable("android.intent.extra.USER", (Parcelable)userHandle);
                            list.add(new AccountTypePreference(context, this.mMetricsFeatureProvider.getMetricsCategory(this.mParent), account, packageForType, labelIdForType, labelForType, AccountDetailDashboardFragment.class.getName(), bundle, drawableForType));
                        }
                    }
                    authenticatorHelper.preloadDrawableForType(this.mContext, s);
                }
            }
        }
        Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<AccountTypePreference>() {
            @Override
            public int compare(final AccountTypePreference accountTypePreference, final AccountTypePreference accountTypePreference2) {
                int n = accountTypePreference.getSummary().toString().compareTo(accountTypePreference2.getSummary().toString());
                if (n == 0) {
                    n = accountTypePreference.getTitle().toString().compareTo(accountTypePreference2.getTitle().toString());
                }
                return n;
            }
        });
        return (ArrayList<AccountTypePreference>)list;
    }
    
    private String getWorkGroupSummary(final Context context, final UserInfo userInfo) {
        final PackageManager packageManager = context.getPackageManager();
        final ApplicationInfo adminApplicationInfo = Utils.getAdminApplicationInfo(context, userInfo.id);
        if (adminApplicationInfo == null) {
            return null;
        }
        return this.mContext.getString(2131888229, new Object[] { packageManager.getApplicationLabel(adminApplicationInfo) });
    }
    
    private boolean isSingleProfile() {
        final boolean linkedUser = this.mUm.isLinkedUser();
        boolean b = true;
        if (!linkedUser) {
            b = (this.mUm.getProfiles(UserHandle.myUserId()).size() == 1 && b);
        }
        return b;
    }
    
    private void listenToAccountUpdates() {
        for (int size = this.mProfiles.size(), i = 0; i < size; ++i) {
            final AuthenticatorHelper authenticatorHelper = ((ProfileData)this.mProfiles.valueAt(i)).authenticatorHelper;
            if (authenticatorHelper != null) {
                authenticatorHelper.listenToAccountUpdates();
            }
        }
    }
    
    private RestrictedPreference newAddAccountPreference() {
        final RestrictedPreference restrictedPreference = new RestrictedPreference(this.mParent.getPreferenceManager().getContext());
        restrictedPreference.setTitle(2131886254);
        restrictedPreference.setIcon(2131231064);
        restrictedPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        restrictedPreference.setOrder(1000);
        return restrictedPreference;
    }
    
    private Preference newManagedProfileSettings() {
        final Preference preference = new Preference(this.mParent.getPreferenceManager().getContext());
        preference.setTitle(2131888227);
        preference.setIcon(2131231108);
        preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        preference.setOrder(1001);
        return preference;
    }
    
    private RestrictedPreference newRemoveWorkProfilePreference() {
        final RestrictedPreference restrictedPreference = new RestrictedPreference(this.mParent.getPreferenceManager().getContext());
        restrictedPreference.setTitle(2131888805);
        restrictedPreference.setIcon(2131230999);
        restrictedPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        restrictedPreference.setOrder(1002);
        return restrictedPreference;
    }
    
    private void stopListeningToAccountUpdates() {
        for (int size = this.mProfiles.size(), i = 0; i < size; ++i) {
            final AuthenticatorHelper authenticatorHelper = ((ProfileData)this.mProfiles.valueAt(i)).authenticatorHelper;
            if (authenticatorHelper != null) {
                authenticatorHelper.stopListeningToAccountUpdates();
            }
        }
    }
    
    private void updateAccountTypes(final ProfileData profileData) {
        if (this.mParent.getPreferenceManager() != null && profileData.preferenceGroup.getPreferenceManager() != null) {
            final boolean enabled = profileData.userInfo.isEnabled();
            int i = 0;
            if (enabled) {
                final ArrayMap arrayMap = new ArrayMap((ArrayMap)profileData.accountPreferences);
                for (ArrayList<AccountTypePreference> accountTypePreferences = this.getAccountTypePreferences(profileData.authenticatorHelper, profileData.userInfo.getUserHandle(), (ArrayMap<String, AccountTypePreference>)arrayMap); i < accountTypePreferences.size(); ++i) {
                    final AccountTypePreference accountTypePreference = accountTypePreferences.get(i);
                    accountTypePreference.setOrder(i);
                    final String key = accountTypePreference.getKey();
                    if (!profileData.accountPreferences.containsKey((Object)key)) {
                        profileData.preferenceGroup.addPreference(accountTypePreference);
                        profileData.accountPreferences.put((Object)key, (Object)accountTypePreference);
                    }
                }
                if (profileData.addAccountPreference != null) {
                    profileData.preferenceGroup.addPreference(profileData.addAccountPreference);
                }
                for (final String s : arrayMap.keySet()) {
                    profileData.preferenceGroup.removePreference((Preference)profileData.accountPreferences.get((Object)s));
                    profileData.accountPreferences.remove((Object)s);
                }
            }
            else {
                profileData.preferenceGroup.removeAll();
                if (this.mProfileNotAvailablePreference == null) {
                    this.mProfileNotAvailablePreference = new Preference(this.mParent.getPreferenceManager().getContext());
                }
                this.mProfileNotAvailablePreference.setEnabled(false);
                this.mProfileNotAvailablePreference.setIcon(2131230910);
                this.mProfileNotAvailablePreference.setTitle(null);
                this.mProfileNotAvailablePreference.setSummary(2131888226);
                profileData.preferenceGroup.addPreference(this.mProfileNotAvailablePreference);
            }
            if (profileData.removeWorkProfilePreference != null) {
                profileData.preferenceGroup.addPreference(profileData.removeWorkProfilePreference);
            }
            if (profileData.managedProfilePreference != null) {
                profileData.preferenceGroup.addPreference(profileData.managedProfilePreference);
            }
        }
    }
    
    private void updateProfileUi(final UserInfo userInfo) {
        if (this.mParent.getPreferenceManager() == null) {
            return;
        }
        final ProfileData profileData = (ProfileData)this.mProfiles.get(userInfo.id);
        if (profileData != null) {
            profileData.pendingRemoval = false;
            if (userInfo.isEnabled()) {
                profileData.authenticatorHelper = new AuthenticatorHelper(this.mContext, userInfo.getUserHandle(), (AuthenticatorHelper.OnAccountsUpdateListener)this);
            }
            return;
        }
        final Context mContext = this.mContext;
        final ProfileData profileData2 = new ProfileData();
        profileData2.userInfo = userInfo;
        final AccessiblePreferenceCategory accessiblePreferenceCategory = this.mHelper.createAccessiblePreferenceCategory(this.mParent.getPreferenceManager().getContext());
        accessiblePreferenceCategory.setOrder(this.mAccountProfileOrder++);
        if (this.isSingleProfile()) {
            accessiblePreferenceCategory.setTitle(mContext.getString(2131886228, new Object[] { BidiFormatter.getInstance().unicodeWrap(userInfo.name) }));
            accessiblePreferenceCategory.setContentDescription(this.mContext.getString(2131886229));
        }
        else if (userInfo.isManagedProfile()) {
            accessiblePreferenceCategory.setTitle(R.string.category_work);
            final String workGroupSummary = this.getWorkGroupSummary(mContext, userInfo);
            accessiblePreferenceCategory.setSummary(workGroupSummary);
            accessiblePreferenceCategory.setContentDescription(this.mContext.getString(2131886146, new Object[] { workGroupSummary }));
            profileData2.removeWorkProfilePreference = this.newRemoveWorkProfilePreference();
            this.mHelper.enforceRestrictionOnPreference(profileData2.removeWorkProfilePreference, "no_remove_managed_profile", UserHandle.myUserId());
            profileData2.managedProfilePreference = this.newManagedProfileSettings();
        }
        else {
            accessiblePreferenceCategory.setTitle(R.string.category_personal);
            accessiblePreferenceCategory.setContentDescription(this.mContext.getString(2131886145));
        }
        final PreferenceScreen preferenceScreen = this.mParent.getPreferenceScreen();
        if (preferenceScreen != null) {
            preferenceScreen.addPreference(accessiblePreferenceCategory);
        }
        profileData2.preferenceGroup = accessiblePreferenceCategory;
        if (userInfo.isEnabled()) {
            profileData2.authenticatorHelper = new AuthenticatorHelper(mContext, userInfo.getUserHandle(), (AuthenticatorHelper.OnAccountsUpdateListener)this);
            profileData2.addAccountPreference = this.newAddAccountPreference();
            this.mHelper.enforceRestrictionOnPreference(profileData2.addAccountPreference, "no_modify_accounts", userInfo.id);
        }
        this.mProfiles.put(userInfo.id, (Object)profileData2);
    }
    
    private void updateUi() {
        if (!this.isAvailable()) {
            Log.e("AccountPrefController", "We should not be showing settings for a managed profile");
            return;
        }
        for (int i = 0; i < this.mProfiles.size(); ++i) {
            ((ProfileData)this.mProfiles.valueAt(i)).pendingRemoval = true;
        }
        final boolean restrictedProfile = this.mUm.isRestrictedProfile();
        final int n = 0;
        if (restrictedProfile) {
            this.updateProfileUi(this.mUm.getUserInfo(UserHandle.myUserId()));
        }
        else {
            final List profiles = this.mUm.getProfiles(UserHandle.myUserId());
            for (int size = profiles.size(), j = 0; j < size; ++j) {
                this.updateProfileUi(profiles.get(j));
            }
        }
        this.cleanUpPreferences();
        for (int size2 = this.mProfiles.size(), k = n; k < size2; ++k) {
            this.updateAccountTypes((ProfileData)this.mProfiles.valueAt(k));
        }
    }
    
    void cleanUpPreferences() {
        final PreferenceScreen preferenceScreen = this.mParent.getPreferenceScreen();
        if (preferenceScreen == null) {
            return;
        }
        for (int i = this.mProfiles.size() - 1; i >= 0; --i) {
            final ProfileData profileData = (ProfileData)this.mProfiles.valueAt(i);
            if (profileData.pendingRemoval) {
                preferenceScreen.removePreference(profileData.preferenceGroup);
                this.mProfiles.removeAt(i);
            }
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.updateUi();
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUm.isManagedProfile() ^ true;
    }
    
    @Override
    public void onAccountsUpdate(final UserHandle userHandle) {
        final ProfileData profileData = (ProfileData)this.mProfiles.get(userHandle.getIdentifier());
        if (profileData != null) {
            this.updateAccountTypes(profileData);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Missing Settings screen for: ");
            sb.append(userHandle.getIdentifier());
            Log.w("AccountPrefController", sb.toString());
        }
    }
    
    @Override
    public void onPause() {
        this.stopListeningToAccountUpdates();
        this.mManagedProfileBroadcastReceiver.unregister(this.mContext);
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        for (int size = this.mProfiles.size(), i = 0; i < size; ++i) {
            final ProfileData profileData = (ProfileData)this.mProfiles.valueAt(i);
            if (preference == profileData.addAccountPreference) {
                final Intent intent = new Intent("android.settings.ADD_ACCOUNT_SETTINGS");
                intent.putExtra("android.intent.extra.USER", (Parcelable)profileData.userInfo.getUserHandle());
                intent.putExtra("authorities", this.mAuthorities);
                this.mContext.startActivity(intent);
                return true;
            }
            if (preference == profileData.removeWorkProfilePreference) {
                RemoveUserFragment.newInstance(profileData.userInfo.id).show(this.mParent.getFragmentManager(), "removeUser");
                return true;
            }
            if (preference == profileData.managedProfilePreference) {
                final Bundle arguments = new Bundle();
                arguments.putParcelable("android.intent.extra.USER", (Parcelable)profileData.userInfo.getUserHandle());
                new SubSettingLauncher(this.mContext).setSourceMetricsCategory(this.mParent.getMetricsCategory()).setDestination(ManagedProfileSettings.class.getName()).setTitle(2131888227).setArguments(arguments).launch();
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void onResume() {
        this.updateUi();
        this.mManagedProfileBroadcastReceiver.register(this.mContext);
        this.listenToAccountUpdates();
    }
    
    private class ManagedProfileBroadcastReceiver extends BroadcastReceiver
    {
        private boolean mListeningToManagedProfileEvents;
        
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            final StringBuilder sb = new StringBuilder();
            sb.append("Received broadcast: ");
            sb.append(action);
            Log.v("AccountPrefController", sb.toString());
            if (!action.equals("android.intent.action.MANAGED_PROFILE_REMOVED") && !action.equals("android.intent.action.MANAGED_PROFILE_ADDED")) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Cannot handle received broadcast: ");
                sb2.append(intent.getAction());
                Log.w("AccountPrefController", sb2.toString());
                return;
            }
            AccountPreferenceController.this.stopListeningToAccountUpdates();
            AccountPreferenceController.this.updateUi();
            AccountPreferenceController.this.listenToAccountUpdates();
        }
        
        public void register(final Context context) {
            if (!this.mListeningToManagedProfileEvents) {
                final IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.MANAGED_PROFILE_REMOVED");
                intentFilter.addAction("android.intent.action.MANAGED_PROFILE_ADDED");
                context.registerReceiver((BroadcastReceiver)this, intentFilter);
                this.mListeningToManagedProfileEvents = true;
            }
        }
        
        public void unregister(final Context context) {
            if (this.mListeningToManagedProfileEvents) {
                context.unregisterReceiver((BroadcastReceiver)this);
                this.mListeningToManagedProfileEvents = false;
            }
        }
    }
    
    public static class ProfileData
    {
        public ArrayMap<String, AccountTypePreference> accountPreferences;
        public RestrictedPreference addAccountPreference;
        public AuthenticatorHelper authenticatorHelper;
        public Preference managedProfilePreference;
        public boolean pendingRemoval;
        public PreferenceGroup preferenceGroup;
        public RestrictedPreference removeWorkProfilePreference;
        public UserInfo userInfo;
        
        public ProfileData() {
            this.accountPreferences = (ArrayMap<String, AccountTypePreference>)new ArrayMap();
        }
    }
}
