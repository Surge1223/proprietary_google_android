package com.android.settings.accounts;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.accounts.AuthenticatorHelper;
import android.os.UserHandle;
import android.app.Activity;
import com.android.settings.Utils;
import android.os.UserManager;
import android.support.v7.preference.PreferenceManager;
import android.os.Bundle;
import com.android.settingslib.drawer.Tile;
import android.support.v14.preference.PreferenceFragment;
import android.app.Fragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.accounts.Account;
import com.android.settings.dashboard.DashboardFragment;

public class AccountDetailDashboardFragment extends DashboardFragment
{
    Account mAccount;
    private String mAccountLabel;
    private AccountSyncPreferenceController mAccountSynController;
    String mAccountType;
    private RemoveAccountPreferenceController mRemoveAccountController;
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<AccountHeaderPreferenceController> list = (ArrayList<AccountHeaderPreferenceController>)new ArrayList<RemoveAccountPreferenceController>();
        list.add((RemoveAccountPreferenceController)(this.mAccountSynController = new AccountSyncPreferenceController(context)));
        list.add(this.mRemoveAccountController = new RemoveAccountPreferenceController(context, this));
        list.add(new AccountHeaderPreferenceController(context, this.getLifecycle(), this.getActivity(), this, this.getArguments()));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected boolean displayTile(final Tile tile) {
        if (this.mAccountType == null) {
            return false;
        }
        final Bundle metaData = tile.metaData;
        if (metaData == null) {
            return false;
        }
        final boolean equals = this.mAccountType.equals(metaData.getString("com.android.settings.ia.account"));
        if (equals && tile.intent != null) {
            tile.intent.putExtra("extra.accountName", this.mAccount.name);
        }
        return equals;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887792;
    }
    
    @Override
    protected String getLogTag() {
        return "AccountDetailDashboard";
    }
    
    @Override
    public int getMetricsCategory() {
        return 8;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082700;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        if (this.mAccountLabel != null) {
            this.getActivity().setTitle((CharSequence)this.mAccountLabel);
        }
        this.updateUi();
    }
    
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        this.getPreferenceManager().setPreferenceComparisonCallback(null);
        arguments = this.getArguments();
        final Activity activity = this.getActivity();
        final UserHandle secureTargetUser = Utils.getSecureTargetUser(activity.getActivityToken(), (UserManager)this.getSystemService("user"), arguments, activity.getIntent().getExtras());
        if (arguments != null) {
            if (arguments.containsKey("account")) {
                this.mAccount = (Account)arguments.getParcelable("account");
            }
            if (arguments.containsKey("account_label")) {
                this.mAccountLabel = arguments.getString("account_label");
            }
            if (arguments.containsKey("account_type")) {
                this.mAccountType = arguments.getString("account_type");
            }
        }
        this.mAccountSynController.init(this.mAccount, secureTargetUser);
        this.mRemoveAccountController.init(this.mAccount, secureTargetUser);
    }
    
    void updateUi() {
        final Context context = this.getContext();
        final UserHandle userHandle = null;
        final Bundle arguments = this.getArguments();
        UserHandle userHandle2 = userHandle;
        if (arguments != null) {
            userHandle2 = userHandle;
            if (arguments.containsKey("user_handle")) {
                userHandle2 = (UserHandle)arguments.getParcelable("user_handle");
            }
        }
        final AccountTypePreferenceLoader accountTypePreferenceLoader = new AccountTypePreferenceLoader(this, new AuthenticatorHelper(context, userHandle2, null), userHandle2);
        final PreferenceScreen addPreferencesForType = accountTypePreferenceLoader.addPreferencesForType(this.mAccountType, this.getPreferenceScreen());
        if (addPreferencesForType != null) {
            accountTypePreferenceLoader.updatePreferenceIntents(addPreferencesForType, this.mAccountType, this.mAccount);
        }
    }
}
