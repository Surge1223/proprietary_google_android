package com.android.settings.accounts;

import com.android.internal.util.CharSequences;
import android.app.Activity;
import com.android.settings.Utils;
import com.android.settings.overlay.FeatureFactory;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.content.res.Resources$NotFoundException;
import android.graphics.drawable.Drawable;
import android.content.SyncAdapterType;
import android.content.ContentResolver;
import com.google.android.collect.Maps;
import android.accounts.AccountManager;
import java.util.Iterator;
import android.content.Context;
import java.util.List;
import java.util.Collections;
import com.android.settingslib.RestrictedLockUtils;
import android.util.Log;
import android.os.Parcelable;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.os.UserHandle;
import android.os.UserManager;
import java.util.Map;
import com.android.settings.enterprise.EnterprisePrivacyFeatureProvider;
import com.android.settingslib.widget.FooterPreference;
import android.accounts.AuthenticatorDescription;
import android.support.v7.preference.PreferenceGroup;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;
import com.android.settings.SettingsPreferenceFragment;

public class ChooseAccountActivity extends SettingsPreferenceFragment
{
    private HashMap<String, ArrayList<String>> mAccountTypeToAuthorities;
    public HashSet<String> mAccountTypesFilter;
    private PreferenceGroup mAddAccountGroup;
    private AuthenticatorDescription[] mAuthDescs;
    private String[] mAuthorities;
    private FooterPreference mEnterpriseDisclosurePreference;
    private EnterprisePrivacyFeatureProvider mFeatureProvider;
    private final ArrayList<ProviderEntry> mProviderList;
    private Map<String, AuthenticatorDescription> mTypeToAuthDescription;
    private UserManager mUm;
    private UserHandle mUserHandle;
    
    public ChooseAccountActivity() {
        this.mEnterpriseDisclosurePreference = null;
        this.mProviderList = new ArrayList<ProviderEntry>();
        this.mAccountTypeToAuthorities = null;
        this.mTypeToAuthDescription = new HashMap<String, AuthenticatorDescription>();
    }
    
    private void addEnterpriseDisclosure() {
        final CharSequence deviceOwnerDisclosure = this.mFeatureProvider.getDeviceOwnerDisclosure();
        if (deviceOwnerDisclosure == null) {
            return;
        }
        if (this.mEnterpriseDisclosurePreference == null) {
            (this.mEnterpriseDisclosurePreference = this.mFooterPreferenceMixin.createFooterPreference()).setSelectable(false);
        }
        this.mEnterpriseDisclosurePreference.setTitle(deviceOwnerDisclosure);
        this.mAddAccountGroup.addPreference(this.mEnterpriseDisclosurePreference);
    }
    
    private void finishWithAccountType(final String s) {
        final Intent intent = new Intent();
        intent.putExtra("selected_account", s);
        intent.putExtra("android.intent.extra.USER", (Parcelable)this.mUserHandle);
        this.setResult(-1, intent);
        this.finish();
    }
    
    private void onAuthDescriptionsUpdated() {
        for (int i = 0; i < this.mAuthDescs.length; ++i) {
            final String type = this.mAuthDescs[i].type;
            final CharSequence labelForType = this.getLabelForType(type);
            final ArrayList<String> authoritiesForAccountType = this.getAuthoritiesForAccountType(type);
            boolean b2;
            final boolean b = b2 = true;
            if (this.mAuthorities != null) {
                b2 = b;
                if (this.mAuthorities.length > 0) {
                    b2 = b;
                    if (authoritiesForAccountType != null) {
                        final boolean b3 = false;
                        int n = 0;
                        while (true) {
                            b2 = b3;
                            if (n >= this.mAuthorities.length) {
                                break;
                            }
                            if (authoritiesForAccountType.contains(this.mAuthorities[n])) {
                                b2 = true;
                                break;
                            }
                            ++n;
                        }
                    }
                }
            }
            boolean b4;
            if (b4 = b2) {
                b4 = b2;
                if (this.mAccountTypesFilter != null) {
                    b4 = b2;
                    if (!this.mAccountTypesFilter.contains(type)) {
                        b4 = false;
                    }
                }
            }
            if (b4) {
                this.mProviderList.add(new ProviderEntry(labelForType, type));
            }
            else if (Log.isLoggable("ChooseAccountActivity", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipped pref ");
                sb.append((Object)labelForType);
                sb.append(": has no authority we need");
                Log.v("ChooseAccountActivity", sb.toString());
            }
        }
        final Context context = this.getPreferenceScreen().getContext();
        if (this.mProviderList.size() == 1) {
            final RestrictedLockUtils.EnforcedAdmin checkIfAccountManagementDisabled = RestrictedLockUtils.checkIfAccountManagementDisabled(context, this.mProviderList.get(0).type, this.mUserHandle.getIdentifier());
            if (checkIfAccountManagementDisabled != null) {
                this.setResult(0, RestrictedLockUtils.getShowAdminSupportDetailsIntent(context, checkIfAccountManagementDisabled));
                this.finish();
            }
            else {
                this.finishWithAccountType(this.mProviderList.get(0).type);
            }
        }
        else if (this.mProviderList.size() > 0) {
            Collections.sort(this.mProviderList);
            this.mAddAccountGroup.removeAll();
            for (final ProviderEntry providerEntry : this.mProviderList) {
                final ProviderPreference providerPreference = new ProviderPreference(this.getPreferenceScreen().getContext(), providerEntry.type, this.getDrawableForType(providerEntry.type), providerEntry.name);
                providerPreference.checkAccountManagementAndSetDisabled(this.mUserHandle.getIdentifier());
                this.mAddAccountGroup.addPreference(providerPreference);
            }
            this.addEnterpriseDisclosure();
        }
        else {
            if (Log.isLoggable("ChooseAccountActivity", 2)) {
                final StringBuilder sb2 = new StringBuilder();
                final String[] mAuthorities = this.mAuthorities;
                for (int length = mAuthorities.length, j = 0; j < length; ++j) {
                    sb2.append(mAuthorities[j]);
                    sb2.append(' ');
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("No providers found for authorities: ");
                sb3.append((Object)sb2);
                Log.v("ChooseAccountActivity", sb3.toString());
            }
            this.setResult(0);
            this.finish();
        }
    }
    
    private void updateAuthDescriptions() {
        this.mAuthDescs = AccountManager.get(this.getContext()).getAuthenticatorTypesAsUser(this.mUserHandle.getIdentifier());
        for (int i = 0; i < this.mAuthDescs.length; ++i) {
            this.mTypeToAuthDescription.put(this.mAuthDescs[i].type, this.mAuthDescs[i]);
        }
        this.onAuthDescriptionsUpdated();
    }
    
    public ArrayList<String> getAuthoritiesForAccountType(final String s) {
        if (this.mAccountTypeToAuthorities == null) {
            this.mAccountTypeToAuthorities = (HashMap<String, ArrayList<String>>)Maps.newHashMap();
            final SyncAdapterType[] syncAdapterTypesAsUser = ContentResolver.getSyncAdapterTypesAsUser(this.mUserHandle.getIdentifier());
            for (int i = 0; i < syncAdapterTypesAsUser.length; ++i) {
                final SyncAdapterType syncAdapterType = syncAdapterTypesAsUser[i];
                ArrayList<String> list;
                if ((list = this.mAccountTypeToAuthorities.get(syncAdapterType.accountType)) == null) {
                    list = new ArrayList<String>();
                    this.mAccountTypeToAuthorities.put(syncAdapterType.accountType, list);
                }
                if (Log.isLoggable("ChooseAccountActivity", 2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("added authority ");
                    sb.append(syncAdapterType.authority);
                    sb.append(" to accountType ");
                    sb.append(syncAdapterType.accountType);
                    Log.d("ChooseAccountActivity", sb.toString());
                }
                list.add(syncAdapterType.authority);
            }
        }
        return this.mAccountTypeToAuthorities.get(s);
    }
    
    protected Drawable getDrawableForType(String userBadgedIcon) {
        final Drawable drawable = null;
        final Object o = null;
        Object o2 = drawable;
        if (this.mTypeToAuthDescription.containsKey(userBadgedIcon)) {
            while (true) {
                try {
                    final AuthenticatorDescription authenticatorDescription = this.mTypeToAuthDescription.get(userBadgedIcon);
                    userBadgedIcon = this.getPackageManager().getUserBadgedIcon(this.getActivity().createPackageContextAsUser(authenticatorDescription.packageName, 0, this.mUserHandle).getDrawable(authenticatorDescription.iconId), this.mUserHandle);
                    o2 = userBadgedIcon;
                }
                catch (Resources$NotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No icon resource for account type ");
                    sb.append((String)userBadgedIcon);
                    Log.w("ChooseAccountActivity", sb.toString());
                    o2 = drawable;
                }
                catch (PackageManager$NameNotFoundException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("No icon name for account type ");
                    sb2.append((String)userBadgedIcon);
                    Log.w("ChooseAccountActivity", sb2.toString());
                    userBadgedIcon = o;
                    continue;
                }
                break;
            }
        }
        if (o2 != null) {
            return (Drawable)o2;
        }
        return this.getPackageManager().getDefaultActivityIcon();
    }
    
    protected CharSequence getLabelForType(String text) {
        final CharSequence charSequence = null;
        final CharSequence charSequence2 = null;
        CharSequence charSequence3 = charSequence;
        if (this.mTypeToAuthDescription.containsKey(text)) {
            while (true) {
                try {
                    final AuthenticatorDescription authenticatorDescription = this.mTypeToAuthDescription.get(text);
                    text = this.getActivity().createPackageContextAsUser(authenticatorDescription.packageName, 0, this.mUserHandle).getResources().getText(authenticatorDescription.labelId);
                    charSequence3 = text;
                }
                catch (Resources$NotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No label resource for account type ");
                    sb.append((String)text);
                    Log.w("ChooseAccountActivity", sb.toString());
                    charSequence3 = charSequence;
                }
                catch (PackageManager$NameNotFoundException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("No label name for account type ");
                    sb2.append((String)text);
                    Log.w("ChooseAccountActivity", sb2.toString());
                    text = charSequence2;
                    continue;
                }
                break;
            }
        }
        return charSequence3;
    }
    
    @Override
    public int getMetricsCategory() {
        return 10;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mFeatureProvider = FeatureFactory.getFactory((Context)activity).getEnterprisePrivacyFeatureProvider((Context)activity);
        this.addPreferencesFromResource(2132082702);
        this.mAuthorities = this.getIntent().getStringArrayExtra("authorities");
        final String[] stringArrayExtra = this.getIntent().getStringArrayExtra("account_types");
        if (stringArrayExtra != null) {
            this.mAccountTypesFilter = new HashSet<String>();
            for (int length = stringArrayExtra.length, i = 0; i < length; ++i) {
                this.mAccountTypesFilter.add(stringArrayExtra[i]);
            }
        }
        this.mAddAccountGroup = this.getPreferenceScreen();
        this.mUm = UserManager.get(this.getContext());
        this.mUserHandle = Utils.getSecureTargetUser(this.getActivity().getActivityToken(), this.mUm, null, this.getIntent().getExtras());
        this.updateAuthDescriptions();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof ProviderPreference) {
            final ProviderPreference providerPreference = (ProviderPreference)preference;
            if (Log.isLoggable("ChooseAccountActivity", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Attempting to add account of type ");
                sb.append(providerPreference.getAccountType());
                Log.v("ChooseAccountActivity", sb.toString());
            }
            this.finishWithAccountType(providerPreference.getAccountType());
        }
        return true;
    }
    
    private static class ProviderEntry implements Comparable<ProviderEntry>
    {
        private final CharSequence name;
        private final String type;
        
        ProviderEntry(final CharSequence name, final String type) {
            this.name = name;
            this.type = type;
        }
        
        @Override
        public int compareTo(final ProviderEntry providerEntry) {
            if (this.name == null) {
                return -1;
            }
            if (providerEntry.name == null) {
                return 1;
            }
            return CharSequences.compareToIgnoreCase(this.name, providerEntry.name);
        }
    }
}
