package com.android.settings.accounts;

import com.android.settingslib.accounts.AuthenticatorHelper;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserHandle;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.applications.LayoutPreference;
import android.app.Activity;
import android.accounts.Account;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AccountHeaderPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnResume
{
    private final Account mAccount;
    private final Activity mActivity;
    private LayoutPreference mHeaderPreference;
    private final PreferenceFragment mHost;
    private final UserHandle mUserHandle;
    
    public AccountHeaderPreferenceController(final Context context, final Lifecycle lifecycle, final Activity mActivity, final PreferenceFragment mHost, final Bundle bundle) {
        super(context);
        this.mActivity = mActivity;
        this.mHost = mHost;
        if (bundle != null && bundle.containsKey("account")) {
            this.mAccount = (Account)bundle.getParcelable("account");
        }
        else {
            this.mAccount = null;
        }
        if (bundle != null && bundle.containsKey("user_handle")) {
            this.mUserHandle = (UserHandle)bundle.getParcelable("user_handle");
        }
        else {
            this.mUserHandle = null;
        }
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mHeaderPreference = (LayoutPreference)preferenceScreen.findPreference("account_header");
    }
    
    @Override
    public String getPreferenceKey() {
        return "account_header";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mAccount != null && this.mUserHandle != null;
    }
    
    @Override
    public void onResume() {
        EntityHeaderController.newInstance(this.mActivity, this.mHost, this.mHeaderPreference.findViewById(2131362112)).setLabel(this.mAccount.name).setIcon(new AuthenticatorHelper(this.mContext, this.mUserHandle, null).getDrawableForType(this.mContext, this.mAccount.type)).done(this.mActivity, true);
    }
}
