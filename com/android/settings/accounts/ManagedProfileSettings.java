package com.android.settings.accounts;

import android.content.IntentFilter;
import android.util.Log;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import com.android.settingslib.RestrictedLockUtils;
import android.provider.Settings;
import android.os.Bundle;
import com.android.settings.Utils;
import android.support.v14.preference.SwitchPreference;
import android.os.UserManager;
import android.os.UserHandle;
import android.content.Context;
import com.android.settingslib.RestrictedSwitchPreference;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class ManagedProfileSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener
{
    private RestrictedSwitchPreference mContactPrefrence;
    private Context mContext;
    private ManagedProfileBroadcastReceiver mManagedProfileBroadcastReceiver;
    private UserHandle mManagedUser;
    private UserManager mUserManager;
    private SwitchPreference mWorkModePreference;
    
    private UserHandle getManagedUserFromArgument() {
        final Bundle arguments = this.getArguments();
        if (arguments != null) {
            final UserHandle userHandle = (UserHandle)arguments.getParcelable("android.intent.extra.USER");
            if (userHandle != null && this.mUserManager.isManagedProfile(userHandle.getIdentifier())) {
                return userHandle;
            }
        }
        return Utils.getManagedProfile(this.mUserManager);
    }
    
    private void loadDataAndPopulateUi() {
        if (this.mWorkModePreference != null) {
            this.updateWorkModePreference();
        }
        if (this.mContactPrefrence != null) {
            final ContentResolver contentResolver = this.getContentResolver();
            final int identifier = this.mManagedUser.getIdentifier();
            boolean checked = false;
            final int intForUser = Settings.Secure.getIntForUser(contentResolver, "managed_profile_contact_remote_search", 0, identifier);
            final RestrictedSwitchPreference mContactPrefrence = this.mContactPrefrence;
            if (intForUser != 0) {
                checked = true;
            }
            mContactPrefrence.setChecked(checked);
            this.mContactPrefrence.setDisabledByAdmin(RestrictedLockUtils.checkIfRemoteContactSearchDisallowed(this.mContext, this.mManagedUser.getIdentifier()));
        }
    }
    
    private void updateWorkModePreference() {
        final boolean checked = this.mUserManager.isQuietModeEnabled(this.mManagedUser) ^ true;
        this.mWorkModePreference.setChecked(checked);
        final SwitchPreference mWorkModePreference = this.mWorkModePreference;
        int summary;
        if (checked) {
            summary = 2131890209;
        }
        else {
            summary = 2131890208;
        }
        mWorkModePreference.setSummary(summary);
    }
    
    @Override
    public int getMetricsCategory() {
        return 401;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082789);
        (this.mWorkModePreference = (SwitchPreference)this.findPreference("work_mode")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        (this.mContactPrefrence = (RestrictedSwitchPreference)this.findPreference("contacts_search")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mContext = this.getActivity().getApplicationContext();
        this.mUserManager = (UserManager)this.getSystemService("user");
        this.mManagedUser = this.getManagedUserFromArgument();
        if (this.mManagedUser == null) {
            this.getActivity().finish();
        }
        (this.mManagedProfileBroadcastReceiver = new ManagedProfileBroadcastReceiver()).register((Context)this.getActivity());
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mManagedProfileBroadcastReceiver.unregister((Context)this.getActivity());
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mWorkModePreference) {
            this.mUserManager.requestQuietModeEnabled((boolean)o ^ true, this.mManagedUser);
            return true;
        }
        final RestrictedSwitchPreference mContactPrefrence = this.mContactPrefrence;
        int n = 0;
        if (preference == mContactPrefrence) {
            if (o) {
                n = 1;
            }
            Settings.Secure.putIntForUser(this.getContentResolver(), "managed_profile_contact_remote_search", n, this.mManagedUser.getIdentifier());
            return true;
        }
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.loadDataAndPopulateUi();
    }
    
    private class ManagedProfileBroadcastReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            final StringBuilder sb = new StringBuilder();
            sb.append("Received broadcast: ");
            sb.append(action);
            Log.v("ManagedProfileSettings", sb.toString());
            if (action.equals("android.intent.action.MANAGED_PROFILE_REMOVED")) {
                if (intent.getIntExtra("android.intent.extra.user_handle", -10000) == ManagedProfileSettings.this.mManagedUser.getIdentifier()) {
                    ManagedProfileSettings.this.getActivity().finish();
                }
                return;
            }
            if (!action.equals("android.intent.action.MANAGED_PROFILE_AVAILABLE") && !action.equals("android.intent.action.MANAGED_PROFILE_UNAVAILABLE")) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Cannot handle received broadcast: ");
                sb2.append(intent.getAction());
                Log.w("ManagedProfileSettings", sb2.toString());
                return;
            }
            if (intent.getIntExtra("android.intent.extra.user_handle", -10000) == ManagedProfileSettings.this.mManagedUser.getIdentifier()) {
                ManagedProfileSettings.this.updateWorkModePreference();
            }
        }
        
        public void register(final Context context) {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.MANAGED_PROFILE_REMOVED");
            intentFilter.addAction("android.intent.action.MANAGED_PROFILE_AVAILABLE");
            intentFilter.addAction("android.intent.action.MANAGED_PROFILE_UNAVAILABLE");
            context.registerReceiver((BroadcastReceiver)this, intentFilter);
        }
        
        public void unregister(final Context context) {
            context.unregisterReceiver((BroadcastReceiver)this);
        }
    }
}
