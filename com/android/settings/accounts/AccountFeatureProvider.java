package com.android.settings.accounts;

import android.util.FeatureFlagUtils;
import android.accounts.Account;
import android.content.Context;

public interface AccountFeatureProvider
{
    String getAccountType();
    
    Account[] getAccounts(final Context p0);
    
    default boolean isAboutPhoneV2Enabled(final Context context) {
        return FeatureFlagUtils.isEnabled(context, "settings_about_phone_v2");
    }
}
