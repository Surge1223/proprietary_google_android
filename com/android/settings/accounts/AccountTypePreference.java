package com.android.settings.accounts;

import com.android.settings.core.SubSettingLauncher;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.os.UserManager;
import android.graphics.drawable.Drawable;
import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import com.android.settings.widget.AppPreference;

public class AccountTypePreference extends AppPreference implements OnPreferenceClickListener
{
    private final String mFragment;
    private final Bundle mFragmentArguments;
    private final int mMetricsCategory;
    private final CharSequence mSummary;
    private final CharSequence mTitle;
    private final int mTitleResId;
    private final String mTitleResPackageName;
    
    public AccountTypePreference(final Context context, final int mMetricsCategory, final Account account, final String mTitleResPackageName, final int mTitleResId, final CharSequence charSequence, final String mFragment, final Bundle mFragmentArguments, final Drawable icon) {
        super(context);
        this.mTitle = account.name;
        this.mTitleResPackageName = mTitleResPackageName;
        this.mTitleResId = mTitleResId;
        this.mSummary = charSequence;
        this.mFragment = mFragment;
        this.mFragmentArguments = mFragmentArguments;
        this.mMetricsCategory = mMetricsCategory;
        this.setKey(buildKey(account));
        this.setTitle(this.mTitle);
        this.setSummary(charSequence);
        this.setIcon(icon);
        this.setOnPreferenceClickListener((OnPreferenceClickListener)this);
    }
    
    public static String buildKey(final Account account) {
        return String.valueOf(account.hashCode());
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mSummary;
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (this.mFragment == null) {
            return false;
        }
        final UserManager userManager = (UserManager)this.getContext().getSystemService("user");
        final UserHandle userHandle = (UserHandle)this.mFragmentArguments.getParcelable("android.intent.extra.USER");
        if (userHandle != null && Utils.startQuietModeDialogIfNecessary(this.getContext(), userManager, userHandle.getIdentifier())) {
            return true;
        }
        if (userHandle != null && Utils.unlockWorkProfileIfNecessary(this.getContext(), userHandle.getIdentifier())) {
            return true;
        }
        new SubSettingLauncher(this.getContext()).setDestination(this.mFragment).setArguments(this.mFragmentArguments).setTitle(this.mTitleResPackageName, this.mTitleResId).setSourceMetricsCategory(this.mMetricsCategory).launch();
        return true;
    }
}
