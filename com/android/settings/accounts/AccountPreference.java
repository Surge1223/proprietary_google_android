package com.android.settings.accounts;

import android.support.v7.preference.PreferenceViewHolder;
import android.util.Log;
import android.widget.ImageView;
import android.support.v7.preference.Preference;

public class AccountPreference extends Preference
{
    private boolean mShowTypeIcon;
    private int mStatus;
    private ImageView mSyncStatusIcon;
    
    private String getSyncContentDescription(final int n) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown sync status: ");
                sb.append(n);
                Log.e("AccountPreference", sb.toString());
                return this.getContext().getString(2131886197);
            }
            case 3: {
                return this.getContext().getString(2131886198);
            }
            case 2: {
                return this.getContext().getString(2131886197);
            }
            case 1: {
                return this.getContext().getString(2131886195);
            }
            case 0: {
                return this.getContext().getString(2131886196);
            }
        }
    }
    
    private int getSyncStatusIcon(int n) {
        switch (n) {
            default: {
                final int n2 = 2131231173;
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown sync status: ");
                sb.append(n);
                Log.e("AccountPreference", sb.toString());
                n = n2;
                break;
            }
            case 2: {
                n = 2131231173;
                break;
            }
            case 1: {
                n = 2131231171;
                break;
            }
            case 0:
            case 3: {
                n = 2131231142;
                break;
            }
        }
        return n;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        if (!this.mShowTypeIcon) {
            (this.mSyncStatusIcon = (ImageView)preferenceViewHolder.findViewById(16908294)).setImageResource(this.getSyncStatusIcon(this.mStatus));
            this.mSyncStatusIcon.setContentDescription((CharSequence)this.getSyncContentDescription(this.mStatus));
        }
    }
}
