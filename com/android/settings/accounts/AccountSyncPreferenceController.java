package com.android.settings.accounts;

import android.content.SyncAdapterType;
import android.content.ContentResolver;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.os.UserHandle;
import android.support.v7.preference.Preference;
import android.accounts.Account;
import com.android.settingslib.accounts.AuthenticatorHelper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AccountSyncPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, OnAccountsUpdateListener
{
    private Account mAccount;
    private Preference mPreference;
    private UserHandle mUserHandle;
    
    public AccountSyncPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "account_sync";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!"account_sync".equals(preference.getKey())) {
            return false;
        }
        final Bundle arguments = new Bundle();
        arguments.putParcelable("account", (Parcelable)this.mAccount);
        arguments.putParcelable("android.intent.extra.USER", (Parcelable)this.mUserHandle);
        new SubSettingLauncher(this.mContext).setDestination(AccountSyncSettings.class.getName()).setArguments(arguments).setSourceMetricsCategory(8).setTitle(2131886238).launch();
        return true;
    }
    
    public void init(final Account mAccount, final UserHandle mUserHandle) {
        this.mAccount = mAccount;
        this.mUserHandle = mUserHandle;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onAccountsUpdate(final UserHandle userHandle) {
        this.updateSummary(this.mPreference);
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateSummary(preference);
    }
    
    void updateSummary(final Preference preference) {
        if (this.mAccount == null) {
            return;
        }
        final int identifier = this.mUserHandle.getIdentifier();
        final SyncAdapterType[] syncAdapterTypesAsUser = ContentResolver.getSyncAdapterTypesAsUser(identifier);
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        if (syncAdapterTypesAsUser != null) {
            int n5 = 0;
            final int length = syncAdapterTypesAsUser.length;
            while (true) {
                n = n2;
                n3 = n4;
                if (n5 >= length) {
                    break;
                }
                final SyncAdapterType syncAdapterType = syncAdapterTypesAsUser[n5];
                int n6 = n2;
                int n7 = n4;
                Label_0184: {
                    if (syncAdapterType.accountType.equals(this.mAccount.type)) {
                        if (!syncAdapterType.isUserVisible()) {
                            n6 = n2;
                            n7 = n4;
                        }
                        else {
                            n6 = n2;
                            n7 = n4;
                            if (ContentResolver.getIsSyncableAsUser(this.mAccount, syncAdapterType.authority, identifier) > 0) {
                                ++n2;
                                final boolean syncAutomaticallyAsUser = ContentResolver.getSyncAutomaticallyAsUser(this.mAccount, syncAdapterType.authority, identifier);
                                if (!(ContentResolver.getMasterSyncAutomaticallyAsUser(identifier) ^ true)) {
                                    n6 = n2;
                                    n7 = n4;
                                    if (!syncAutomaticallyAsUser) {
                                        break Label_0184;
                                    }
                                }
                                n7 = n4 + 1;
                                n6 = n2;
                            }
                        }
                    }
                }
                ++n5;
                n2 = n6;
                n4 = n7;
            }
        }
        if (n3 == 0) {
            preference.setSummary(2131886235);
        }
        else if (n3 == n) {
            preference.setSummary(2131886236);
        }
        else {
            preference.setSummary(this.mContext.getString(2131886237, new Object[] { n3, n }));
        }
    }
}
