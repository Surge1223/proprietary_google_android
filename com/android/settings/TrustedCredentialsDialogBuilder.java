package com.android.settings;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.content.DialogInterface;
import android.content.pm.UserInfo;
import com.android.internal.widget.LockPatternUtils;
import java.util.Iterator;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.net.http.SslCertificate;
import java.util.ArrayList;
import android.animation.TimeInterpolator;
import android.view.animation.AnimationUtils;
import android.view.View$OnLayoutChangeListener;
import android.os.UserManager;
import android.widget.LinearLayout;
import android.widget.Button;
import android.app.admin.DevicePolicyManager;
import android.view.View.OnClickListener;
import java.util.function.IntConsumer;
import java.security.cert.X509Certificate;
import java.util.List;
import android.content.DialogInterface$OnShowListener;
import android.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import android.view.View;
import android.content.Context;
import android.app.Activity;
import android.app.AlertDialog$Builder;

class TrustedCredentialsDialogBuilder extends AlertDialog$Builder
{
    private final DialogEventHandler mDialogEventHandler;
    
    public TrustedCredentialsDialogBuilder(final Activity activity, final DelegateInterface delegateInterface) {
        super((Context)activity);
        this.mDialogEventHandler = new DialogEventHandler(activity, delegateInterface);
        this.initDefaultBuilderParams();
    }
    
    private void initDefaultBuilderParams() {
        this.setTitle(17040909);
        this.setView((View)this.mDialogEventHandler.mRootContainer);
        this.setPositiveButton(2131889501, (DialogInterface$OnClickListener)null);
        this.setNegativeButton(17039370, (DialogInterface$OnClickListener)null);
    }
    
    public AlertDialog create() {
        final AlertDialog create = super.create();
        create.setOnShowListener((DialogInterface$OnShowListener)this.mDialogEventHandler);
        this.mDialogEventHandler.setDialog(create);
        return create;
    }
    
    public TrustedCredentialsDialogBuilder setCertHolder(final TrustedCredentialsSettings.CertHolder certHolder) {
        TrustedCredentialsSettings.CertHolder[] certHolders;
        if (certHolder == null) {
            certHolders = new TrustedCredentialsSettings.CertHolder[0];
        }
        else {
            certHolders = new TrustedCredentialsSettings.CertHolder[] { certHolder };
        }
        return this.setCertHolders(certHolders);
    }
    
    public TrustedCredentialsDialogBuilder setCertHolders(final TrustedCredentialsSettings.CertHolder[] certHolders) {
        this.mDialogEventHandler.setCertHolders(certHolders);
        return this;
    }
    
    public interface DelegateInterface
    {
        List<X509Certificate> getX509CertsFromCertHolder(final TrustedCredentialsSettings.CertHolder p0);
        
        void removeOrInstallCert(final TrustedCredentialsSettings.CertHolder p0);
        
        boolean startConfirmCredentialIfNotConfirmed(final int p0, final IntConsumer p1);
    }
    
    private static class DialogEventHandler implements DialogInterface$OnShowListener, View.OnClickListener
    {
        private final Activity mActivity;
        private TrustedCredentialsSettings.CertHolder[] mCertHolders;
        private int mCurrentCertIndex;
        private View mCurrentCertLayout;
        private final DelegateInterface mDelegate;
        private AlertDialog mDialog;
        private final DevicePolicyManager mDpm;
        private boolean mNeedsApproval;
        private Button mNegativeButton;
        private Button mPositiveButton;
        private final LinearLayout mRootContainer;
        private final UserManager mUserManager;
        
        public DialogEventHandler(final Activity mActivity, final DelegateInterface mDelegate) {
            this.mCurrentCertIndex = -1;
            this.mCertHolders = new TrustedCredentialsSettings.CertHolder[0];
            this.mCurrentCertLayout = null;
            this.mActivity = mActivity;
            this.mDpm = (DevicePolicyManager)mActivity.getSystemService((Class)DevicePolicyManager.class);
            this.mUserManager = (UserManager)mActivity.getSystemService((Class)UserManager.class);
            this.mDelegate = mDelegate;
            (this.mRootContainer = new LinearLayout((Context)this.mActivity)).setOrientation(1);
        }
        
        private void addAndAnimateNewContent(final View mCurrentCertLayout) {
            this.mCurrentCertLayout = mCurrentCertLayout;
            this.mRootContainer.removeAllViews();
            this.mRootContainer.addView(mCurrentCertLayout);
            this.mRootContainer.addOnLayoutChangeListener((View$OnLayoutChangeListener)new View$OnLayoutChangeListener() {
                public void onLayoutChange(final View view, int width, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7) {
                    DialogEventHandler.this.mRootContainer.removeOnLayoutChangeListener((View$OnLayoutChangeListener)this);
                    width = DialogEventHandler.this.mRootContainer.getWidth();
                    DialogEventHandler.this.mCurrentCertLayout.setTranslationX((float)width);
                    DialogEventHandler.this.mCurrentCertLayout.animate().translationX(0.0f).setInterpolator((TimeInterpolator)AnimationUtils.loadInterpolator((Context)DialogEventHandler.this.mActivity, 17563662)).setDuration(200L).start();
                }
            });
        }
        
        private void animateOldContent(final Runnable runnable) {
            this.mCurrentCertLayout.animate().alpha(0.0f).setDuration(300L).setInterpolator((TimeInterpolator)AnimationUtils.loadInterpolator((Context)this.mActivity, 17563663)).withEndAction(runnable).start();
        }
        
        private void animateViewTransition(final View view) {
            this.animateOldContent(new Runnable() {
                @Override
                public void run() {
                    DialogEventHandler.this.addAndAnimateNewContent(view);
                }
            });
        }
        
        private static int getButtonLabel(final TrustedCredentialsSettings.CertHolder certHolder) {
            int n;
            if (certHolder.isSystemCert()) {
                if (certHolder.isDeleted()) {
                    n = 2131889496;
                }
                else {
                    n = 2131889494;
                }
            }
            else {
                n = 2131889498;
            }
            return n;
        }
        
        private LinearLayout getCertLayout(final TrustedCredentialsSettings.CertHolder certHolder) {
            final ArrayList<View> list = new ArrayList<View>();
            final ArrayList<String> list2 = new ArrayList<String>();
            final List<X509Certificate> x509CertsFromCertHolder = this.mDelegate.getX509CertsFromCertHolder(certHolder);
            if (x509CertsFromCertHolder != null) {
                final Iterator<X509Certificate> iterator = x509CertsFromCertHolder.iterator();
                while (iterator.hasNext()) {
                    final SslCertificate sslCertificate = new SslCertificate((X509Certificate)iterator.next());
                    list.add(sslCertificate.inflateCertificateView((Context)this.mActivity));
                    list2.add(sslCertificate.getIssuedTo().getCName());
                }
            }
            final ArrayAdapter adapter = new ArrayAdapter((Context)this.mActivity, 17367048, (List)list2);
            adapter.setDropDownViewResource(17367049);
            final Spinner spinner = new Spinner((Context)this.mActivity);
            spinner.setAdapter((SpinnerAdapter)adapter);
            spinner.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
                public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                    for (int i = 0; i < list.size(); ++i) {
                        final View view2 = list.get(i);
                        int visibility;
                        if (i == n) {
                            visibility = 0;
                        }
                        else {
                            visibility = 8;
                        }
                        view2.setVisibility(visibility);
                    }
                }
                
                public void onNothingSelected(final AdapterView<?> adapterView) {
                }
            });
            final LinearLayout linearLayout = new LinearLayout((Context)this.mActivity);
            linearLayout.setOrientation(1);
            linearLayout.addView((View)spinner);
            for (int i = 0; i < list.size(); ++i) {
                final View view = list.get(i);
                int visibility;
                if (i == 0) {
                    visibility = 0;
                }
                else {
                    visibility = 8;
                }
                view.setVisibility(visibility);
                linearLayout.addView(view);
            }
            return linearLayout;
        }
        
        private TrustedCredentialsSettings.CertHolder getCurrentCertInfo() {
            TrustedCredentialsSettings.CertHolder certHolder;
            if (this.mCurrentCertIndex < this.mCertHolders.length) {
                certHolder = this.mCertHolders[this.mCurrentCertIndex];
            }
            else {
                certHolder = null;
            }
            return certHolder;
        }
        
        private boolean isUserSecure(final int n) {
            final LockPatternUtils lockPatternUtils = new LockPatternUtils((Context)this.mActivity);
            if (lockPatternUtils.isSecure(n)) {
                return true;
            }
            final UserInfo profileParent = this.mUserManager.getProfileParent(n);
            return profileParent != null && lockPatternUtils.isSecure(profileParent.id);
        }
        
        private void nextOrDismiss() {
            ++this.mCurrentCertIndex;
            while (this.mCurrentCertIndex < this.mCertHolders.length && this.getCurrentCertInfo() == null) {
                ++this.mCurrentCertIndex;
            }
            if (this.mCurrentCertIndex >= this.mCertHolders.length) {
                this.mDialog.dismiss();
                return;
            }
            this.updateViewContainer();
            this.updatePositiveButton();
            this.updateNegativeButton();
        }
        
        private void onClickEnableOrDisable() {
            final TrustedCredentialsSettings.CertHolder currentCertInfo = this.getCurrentCertInfo();
            final DialogInterface$OnClickListener dialogInterface$OnClickListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    DialogEventHandler.this.mDelegate.removeOrInstallCert(currentCertInfo);
                    DialogEventHandler.this.nextOrDismiss();
                }
            };
            if (currentCertInfo.isSystemCert()) {
                ((DialogInterface$OnClickListener)dialogInterface$OnClickListener).onClick(null, -1);
            }
            else {
                new AlertDialog$Builder((Context)this.mActivity).setMessage(2131889497).setPositiveButton(17039379, (DialogInterface$OnClickListener)dialogInterface$OnClickListener).setNegativeButton(17039369, (DialogInterface$OnClickListener)null).show();
            }
        }
        
        private void onClickOk() {
            this.nextOrDismiss();
        }
        
        private void onClickTrust() {
            final TrustedCredentialsSettings.CertHolder currentCertInfo = this.getCurrentCertInfo();
            if (!this.mDelegate.startConfirmCredentialIfNotConfirmed(currentCertInfo.getUserId(), new _$$Lambda$TrustedCredentialsDialogBuilder$DialogEventHandler$l_T7FQ_tmXq7wOC1gAhDRR6fZzQ(this))) {
                this.mDpm.approveCaCert(currentCertInfo.getAlias(), currentCertInfo.getUserId(), true);
                this.nextOrDismiss();
            }
        }
        
        private void onCredentialConfirmed(final int n) {
            if (this.mDialog.isShowing() && this.mNeedsApproval && this.getCurrentCertInfo() != null && this.getCurrentCertInfo().getUserId() == n) {
                this.onClickTrust();
            }
        }
        
        private Button updateButton(final int n, final CharSequence text) {
            this.mDialog.setButton(n, text, (DialogInterface$OnClickListener)null);
            final Button button = this.mDialog.getButton(n);
            button.setText(text);
            button.setOnClickListener((View.OnClickListener)this);
            return button;
        }
        
        private void updateNegativeButton() {
            final TrustedCredentialsSettings.CertHolder currentCertInfo = this.getCurrentCertInfo();
            final boolean hasUserRestriction = this.mUserManager.hasUserRestriction("no_config_credentials", new UserHandle(currentCertInfo.getUserId()));
            this.mNegativeButton = this.updateButton(-2, this.mActivity.getText(getButtonLabel(currentCertInfo)));
            final Button mNegativeButton = this.mNegativeButton;
            int visibility;
            if (hasUserRestriction ^ true) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            mNegativeButton.setVisibility(visibility);
        }
        
        private void updatePositiveButton() {
            final TrustedCredentialsSettings.CertHolder currentCertInfo = this.getCurrentCertInfo();
            final boolean systemCert = currentCertInfo.isSystemCert();
            boolean b = false;
            this.mNeedsApproval = (!systemCert && this.isUserSecure(currentCertInfo.getUserId()) && !this.mDpm.isCaCertApproved(currentCertInfo.getAlias(), currentCertInfo.getUserId()));
            if (RestrictedLockUtils.getProfileOrDeviceOwner((Context)this.mActivity, currentCertInfo.getUserId()) != null) {
                b = true;
            }
            final Activity mActivity = this.mActivity;
            int n;
            if (!b && this.mNeedsApproval) {
                n = 2131889501;
            }
            else {
                n = 17039370;
            }
            this.mPositiveButton = this.updateButton(-1, mActivity.getText(n));
        }
        
        private void updateViewContainer() {
            final LinearLayout certLayout = this.getCertLayout(this.getCurrentCertInfo());
            if (this.mCurrentCertLayout == null) {
                this.mCurrentCertLayout = (View)certLayout;
                this.mRootContainer.addView(this.mCurrentCertLayout);
            }
            else {
                this.animateViewTransition((View)certLayout);
            }
        }
        
        public void onClick(final View view) {
            if (view == this.mPositiveButton) {
                if (this.mNeedsApproval) {
                    this.onClickTrust();
                }
                else {
                    this.onClickOk();
                }
            }
            else if (view == this.mNegativeButton) {
                this.onClickEnableOrDisable();
            }
        }
        
        public void onShow(final DialogInterface dialogInterface) {
            this.nextOrDismiss();
        }
        
        public void setCertHolders(final TrustedCredentialsSettings.CertHolder[] mCertHolders) {
            this.mCertHolders = mCertHolders;
        }
        
        public void setDialog(final AlertDialog mDialog) {
            this.mDialog = mDialog;
        }
    }
}
