package com.android.settings;

import android.text.TextPaint;
import android.view.View;
import android.text.style.ClickableSpan;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.widget.TextView$BufferType;
import android.widget.TextView;

public class LinkifyUtils
{
    public static boolean linkify(final TextView textView, final StringBuilder sb, final OnClickListener onClickListener) {
        final int index = sb.indexOf("LINK_BEGIN");
        if (index == -1) {
            textView.setText((CharSequence)sb);
            return false;
        }
        sb.delete(index, "LINK_BEGIN".length() + index);
        final int index2 = sb.indexOf("LINK_END");
        if (index2 == -1) {
            textView.setText((CharSequence)sb);
            return false;
        }
        sb.delete(index2, "LINK_END".length() + index2);
        textView.setText((CharSequence)sb.toString(), TextView$BufferType.SPANNABLE);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        ((Spannable)textView.getText()).setSpan((Object)new ClickableSpan() {
            public void onClick(final View view) {
                onClickListener.onClick();
            }
            
            public void updateDrawState(final TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setUnderlineText(false);
            }
        }, index, index2, 33);
        return true;
    }
    
    public interface OnClickListener
    {
        void onClick();
    }
}
