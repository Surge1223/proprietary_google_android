package com.android.settings.tts;

import android.view.View.OnClickListener;
import android.os.Bundle;
import java.util.Collections;
import java.util.Comparator;
import android.util.Pair;
import java.util.HashMap;
import android.speech.tts.UtteranceProgressListener;
import java.util.ArrayList;
import android.view.View;
import java.util.Set;
import android.speech.tts.TextToSpeech$EngineInfo;
import android.content.ContentResolver;
import com.android.settings.SettingsActivity;
import android.provider.Settings;
import java.util.Iterator;
import java.util.MissingResourceException;
import android.text.TextUtils;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import java.util.Objects;
import android.util.Log;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.speech.tts.TextToSpeech;
import android.support.v7.preference.ListPreference;
import android.speech.tts.TextToSpeech$OnInitListener;
import android.speech.tts.TtsEngines;
import com.android.settings.widget.SeekBarPreference;
import java.util.Locale;
import java.util.List;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settings.widget.GearPreference;
import com.android.settings.search.Indexable;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class TextToSpeechSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener, Indexable, OnGearClickListener
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private ActionButtonPreference mActionButtons;
    private List<String> mAvailableStrLocals;
    private Locale mCurrentDefaultLocale;
    private String mCurrentEngine;
    private int mDefaultPitch;
    private SeekBarPreference mDefaultPitchPref;
    private int mDefaultRate;
    private SeekBarPreference mDefaultRatePref;
    private TtsEngines mEnginesHelper;
    private final TextToSpeech$OnInitListener mInitListener;
    private ListPreference mLocalePreference;
    private String mSampleText;
    private int mSelectedLocaleIndex;
    private TextToSpeech mTts;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("tts_engine_preference");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082849;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    public TextToSpeechSettings() {
        this.mDefaultPitch = 100;
        this.mDefaultRate = 100;
        this.mSelectedLocaleIndex = -1;
        this.mTts = null;
        this.mEnginesHelper = null;
        this.mSampleText = null;
        this.mInitListener = (TextToSpeech$OnInitListener)new TextToSpeech$OnInitListener() {
            public void onInit(final int n) {
                TextToSpeechSettings.this.onInitEngine(n);
            }
        };
    }
    
    private void checkDefaultLocale() {
        final Locale defaultLanguage = this.mTts.getDefaultLanguage();
        if (defaultLanguage == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to get default language from engine ");
            sb.append(this.mCurrentEngine);
            Log.e("TextToSpeechSettings", sb.toString());
            this.updateWidgetState(false);
            return;
        }
        final Locale mCurrentDefaultLocale = this.mCurrentDefaultLocale;
        this.mCurrentDefaultLocale = this.mEnginesHelper.parseLocaleString(defaultLanguage.toString());
        if (!Objects.equals(mCurrentDefaultLocale, this.mCurrentDefaultLocale)) {
            this.mSampleText = null;
        }
        this.mTts.setLanguage(defaultLanguage);
        if (this.evaluateDefaultLocale() && this.mSampleText == null) {
            this.getSampleText();
        }
    }
    
    private void checkVoiceData(final String package1) {
        final Intent intent = new Intent("android.speech.tts.engine.CHECK_TTS_DATA");
        intent.setPackage(package1);
        try {
            this.startActivityForResult(intent, 1977);
        }
        catch (ActivityNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to check TTS data, no activity found for ");
            sb.append(intent);
            sb.append(")");
            Log.e("TextToSpeechSettings", sb.toString());
        }
    }
    
    private void displayNetworkAlert() {
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this.getActivity());
        alertDialog$Builder.setTitle(17039380).setMessage((CharSequence)this.getActivity().getString(2131889510)).setCancelable(false).setPositiveButton(17039370, (DialogInterface$OnClickListener)null);
        alertDialog$Builder.create().show();
    }
    
    private boolean evaluateDefaultLocale() {
        if (this.mCurrentDefaultLocale != null) {
            if (this.mAvailableStrLocals != null) {
                final boolean b = true;
                try {
                    String s2;
                    final String s = s2 = this.mCurrentDefaultLocale.getISO3Language();
                    if (!TextUtils.isEmpty((CharSequence)this.mCurrentDefaultLocale.getISO3Country())) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(s);
                        sb.append("-");
                        sb.append(this.mCurrentDefaultLocale.getISO3Country());
                        s2 = sb.toString();
                    }
                    String string = s2;
                    if (!TextUtils.isEmpty((CharSequence)this.mCurrentDefaultLocale.getVariant())) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(s2);
                        sb2.append("-");
                        sb2.append(this.mCurrentDefaultLocale.getVariant());
                        string = sb2.toString();
                    }
                    final Iterator<String> iterator = this.mAvailableStrLocals.iterator();
                    boolean b2;
                    while (true) {
                        b2 = b;
                        if (!iterator.hasNext()) {
                            break;
                        }
                        if (iterator.next().equalsIgnoreCase(string)) {
                            b2 = false;
                            break;
                        }
                    }
                    final int setLanguage = this.mTts.setLanguage(this.mCurrentDefaultLocale);
                    if (setLanguage != -2 && setLanguage != -1 && !b2) {
                        this.updateWidgetState(true);
                        return true;
                    }
                    this.updateWidgetState(false);
                    return false;
                }
                catch (MissingResourceException ex) {
                    this.updateWidgetState(false);
                    return false;
                }
            }
        }
        return false;
    }
    
    private String getDefaultSampleString() {
        if (this.mTts != null && this.mTts.getLanguage() != null) {
            try {
                final String iso3Language = this.mTts.getLanguage().getISO3Language();
                final String[] stringArray = this.getActivity().getResources().getStringArray(2130903160);
                final String[] stringArray2 = this.getActivity().getResources().getStringArray(2130903159);
                for (int i = 0; i < stringArray.length; ++i) {
                    if (stringArray2[i].equals(iso3Language)) {
                        return stringArray[i];
                    }
                }
            }
            catch (MissingResourceException ex) {}
        }
        return this.getString(2131889509);
    }
    
    private void getSampleText() {
        String package1;
        if (TextUtils.isEmpty((CharSequence)(package1 = this.mTts.getCurrentEngine()))) {
            package1 = this.mTts.getDefaultEngine();
        }
        final Intent intent = new Intent("android.speech.tts.engine.GET_SAMPLE_TEXT");
        intent.putExtra("language", this.mCurrentDefaultLocale.getLanguage());
        intent.putExtra("country", this.mCurrentDefaultLocale.getCountry());
        intent.putExtra("variant", this.mCurrentDefaultLocale.getVariant());
        intent.setPackage(package1);
        try {
            this.startActivityForResult(intent, 1983);
        }
        catch (ActivityNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to get sample text, no activity found for ");
            sb.append(intent);
            sb.append(")");
            Log.e("TextToSpeechSettings", sb.toString());
        }
    }
    
    private int getSeekBarProgressFromValue(final String s, final int n) {
        if (s.equals("tts_default_rate")) {
            return n - 10;
        }
        if (s.equals("tts_default_pitch")) {
            return n - 25;
        }
        return n;
    }
    
    private int getValueFromSeekBarProgress(final String s, final int n) {
        if (s.equals("tts_default_rate")) {
            return 10 + n;
        }
        if (s.equals("tts_default_pitch")) {
            return 25 + n;
        }
        return n;
    }
    
    private void initSettings() {
        final ContentResolver contentResolver = this.getContentResolver();
        this.mDefaultRate = Settings.Secure.getInt(contentResolver, "tts_default_rate", 100);
        this.mDefaultPitch = Settings.Secure.getInt(contentResolver, "tts_default_pitch", 100);
        this.mDefaultRatePref.setProgress(this.getSeekBarProgressFromValue("tts_default_rate", this.mDefaultRate));
        this.mDefaultRatePref.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mDefaultRatePref.setMax(this.getSeekBarProgressFromValue("tts_default_rate", 600));
        this.mDefaultPitchPref.setProgress(this.getSeekBarProgressFromValue("tts_default_pitch", this.mDefaultPitch));
        this.mDefaultPitchPref.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mDefaultPitchPref.setMax(this.getSeekBarProgressFromValue("tts_default_pitch", 400));
        if (this.mTts != null) {
            this.mCurrentEngine = this.mTts.getCurrentEngine();
            this.mTts.setSpeechRate(this.mDefaultRate / 100.0f);
            this.mTts.setPitch(this.mDefaultPitch / 100.0f);
        }
        if (this.getActivity() instanceof SettingsActivity) {
            this.getActivity();
            if (this.mCurrentEngine != null) {
                final TextToSpeech$EngineInfo engineInfo = this.mEnginesHelper.getEngineInfo(this.mCurrentEngine);
                final Preference preference = this.findPreference("tts_engine_preference");
                ((GearPreference)preference).setOnGearClickListener((GearPreference.OnGearClickListener)this);
                preference.setSummary(engineInfo.label);
            }
            this.checkVoiceData(this.mCurrentEngine);
            return;
        }
        throw new IllegalStateException("TextToSpeechSettings used outside a Settings");
    }
    
    private boolean isNetworkRequiredForSynthesis() {
        final Set features = this.mTts.getFeatures(this.mCurrentDefaultLocale);
        final boolean b = false;
        if (features == null) {
            return false;
        }
        boolean b2 = b;
        if (features.contains("networkTts")) {
            b2 = b;
            if (!features.contains("embeddedTts")) {
                b2 = true;
            }
        }
        return b2;
    }
    
    private void onSampleTextReceived(final int n, final Intent intent) {
        String mSampleText;
        final String s = mSampleText = this.getDefaultSampleString();
        if (n == 0) {
            mSampleText = s;
            if (intent != null) {
                mSampleText = s;
                if (intent != null) {
                    mSampleText = s;
                    if (intent.getStringExtra("sampleText") != null) {
                        mSampleText = intent.getStringExtra("sampleText");
                    }
                }
            }
        }
        this.mSampleText = mSampleText;
        if (this.mSampleText != null) {
            this.updateWidgetState(true);
        }
        else {
            Log.e("TextToSpeechSettings", "Did not have a sample string for the requested language. Using default");
        }
    }
    
    private void onVoiceDataIntegrityCheckDone(final Intent intent) {
        final String currentEngine = this.mTts.getCurrentEngine();
        if (currentEngine == null) {
            Log.e("TextToSpeechSettings", "Voice data check complete, but no engine bound");
            return;
        }
        if (intent == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Engine failed voice data integrity check (null return)");
            sb.append(this.mTts.getCurrentEngine());
            Log.e("TextToSpeechSettings", sb.toString());
            return;
        }
        Settings.Secure.putString(this.getContentResolver(), "tts_default_synth", currentEngine);
        this.mAvailableStrLocals = (List<String>)intent.getStringArrayListExtra("availableVoices");
        if (this.mAvailableStrLocals == null) {
            Log.e("TextToSpeechSettings", "Voice data check complete, but no available voices found");
            this.mAvailableStrLocals = new ArrayList<String>();
        }
        if (this.evaluateDefaultLocale()) {
            this.getSampleText();
        }
    }
    
    private void resetTts() {
        final int seekBarProgressFromValue = this.getSeekBarProgressFromValue("tts_default_rate", 100);
        this.mDefaultRatePref.setProgress(seekBarProgressFromValue);
        this.updateSpeechRate(seekBarProgressFromValue);
        final int seekBarProgressFromValue2 = this.getSeekBarProgressFromValue("tts_default_pitch", 100);
        this.mDefaultPitchPref.setProgress(seekBarProgressFromValue2);
        this.updateSpeechPitchValue(seekBarProgressFromValue2);
    }
    
    private void setLocalePreference(final int valueIndex) {
        if (valueIndex < 0) {
            this.mLocalePreference.setValue("");
            this.mLocalePreference.setSummary(2131889525);
        }
        else {
            this.mLocalePreference.setValueIndex(valueIndex);
            this.mLocalePreference.setSummary(this.mLocalePreference.getEntries()[valueIndex]);
        }
    }
    
    private void setTtsUtteranceProgressListener() {
        if (this.mTts == null) {
            return;
        }
        this.mTts.setOnUtteranceProgressListener((UtteranceProgressListener)new UtteranceProgressListener() {
            public void onDone(final String s) {
            }
            
            public void onError(final String s) {
                Log.e("TextToSpeechSettings", "Error while trying to synthesize sample text");
            }
            
            public void onStart(final String s) {
            }
        });
    }
    
    private void speakSampleText() {
        final boolean networkRequiredForSynthesis = this.isNetworkRequiredForSynthesis();
        if (networkRequiredForSynthesis && (!networkRequiredForSynthesis || this.mTts.isLanguageAvailable(this.mCurrentDefaultLocale) < 0)) {
            Log.w("TextToSpeechSettings", "Network required for sample synthesis for requested language");
            this.displayNetworkAlert();
        }
        else {
            final HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("utteranceId", "Sample");
            this.mTts.speak(this.mSampleText, 0, (HashMap)hashMap);
        }
    }
    
    private void updateDefaultLocalePref(final Intent intent) {
        final ArrayList stringArrayListExtra = intent.getStringArrayListExtra("availableVoices");
        intent.getStringArrayListExtra("unavailableVoices");
        if (stringArrayListExtra != null && stringArrayListExtra.size() != 0) {
            Object localePrefForEngine = null;
            if (!this.mEnginesHelper.isLocaleSetToDefaultForEngine(this.mTts.getCurrentEngine())) {
                localePrefForEngine = this.mEnginesHelper.getLocalePrefForEngine(this.mTts.getCurrentEngine());
            }
            final ArrayList list = new ArrayList<Pair>(stringArrayListExtra.size());
            for (int i = 0; i < stringArrayListExtra.size(); ++i) {
                final Locale localeString = this.mEnginesHelper.parseLocaleString((String)stringArrayListExtra.get(i));
                if (localeString != null) {
                    list.add(new Pair((Object)localeString.getDisplayName(), (Object)localeString));
                }
            }
            Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<Pair<String, Locale>>() {
                @Override
                public int compare(final Pair<String, Locale> pair, final Pair<String, Locale> pair2) {
                    return ((String)pair.first).compareToIgnoreCase((String)pair2.first);
                }
            });
            this.mSelectedLocaleIndex = 0;
            final CharSequence[] entries = new CharSequence[stringArrayListExtra.size() + 1];
            final CharSequence[] entryValues = new CharSequence[stringArrayListExtra.size() + 1];
            entries[0] = this.getActivity().getString(2131889526);
            entryValues[0] = "";
            int mSelectedLocaleIndex = 1;
            for (final Pair pair : list) {
                if (((Locale)pair.second).equals(localePrefForEngine)) {
                    this.mSelectedLocaleIndex = mSelectedLocaleIndex;
                }
                entries[mSelectedLocaleIndex] = (CharSequence)pair.first;
                entryValues[mSelectedLocaleIndex] = ((Locale)pair.second).toString();
                ++mSelectedLocaleIndex;
            }
            this.mLocalePreference.setEntries(entries);
            this.mLocalePreference.setEntryValues(entryValues);
            this.mLocalePreference.setEnabled(true);
            this.setLocalePreference(this.mSelectedLocaleIndex);
            return;
        }
        this.mLocalePreference.setEnabled(false);
    }
    
    private void updateLanguageTo(Locale default1) {
        final int n = -1;
        String string;
        if (default1 != null) {
            string = default1.toString();
        }
        else {
            string = "";
        }
        int n2 = 0;
        int mSelectedLocaleIndex;
        while (true) {
            mSelectedLocaleIndex = n;
            if (n2 >= this.mLocalePreference.getEntryValues().length) {
                break;
            }
            if (string.equalsIgnoreCase(this.mLocalePreference.getEntryValues()[n2].toString())) {
                mSelectedLocaleIndex = n2;
                break;
            }
            ++n2;
        }
        if (mSelectedLocaleIndex == -1) {
            Log.w("TextToSpeechSettings", "updateLanguageTo called with unknown locale argument");
            return;
        }
        this.mLocalePreference.setSummary(this.mLocalePreference.getEntries()[mSelectedLocaleIndex]);
        this.mSelectedLocaleIndex = mSelectedLocaleIndex;
        this.mEnginesHelper.updateLocalePrefForEngine(this.mTts.getCurrentEngine(), default1);
        final TextToSpeech mTts = this.mTts;
        if (default1 == null) {
            default1 = Locale.getDefault();
        }
        mTts.setLanguage(default1);
    }
    
    private void updateSpeechPitchValue(final int n) {
        this.mDefaultPitch = this.getValueFromSeekBarProgress("tts_default_pitch", n);
        try {
            Settings.Secure.putInt(this.getContentResolver(), "tts_default_pitch", this.mDefaultPitch);
            if (this.mTts != null) {
                this.mTts.setPitch(this.mDefaultPitch / 100.0f);
            }
        }
        catch (NumberFormatException ex) {
            Log.e("TextToSpeechSettings", "could not persist default TTS pitch setting", (Throwable)ex);
        }
    }
    
    private void updateSpeechRate(final int n) {
        this.mDefaultRate = this.getValueFromSeekBarProgress("tts_default_rate", n);
        try {
            Settings.Secure.putInt(this.getContentResolver(), "tts_default_rate", this.mDefaultRate);
            if (this.mTts != null) {
                this.mTts.setSpeechRate(this.mDefaultRate / 100.0f);
            }
        }
        catch (NumberFormatException ex) {
            Log.e("TextToSpeechSettings", "could not persist default TTS rate setting", (Throwable)ex);
        }
    }
    
    private void updateWidgetState(final boolean enabled) {
        this.mActionButtons.setButton1Enabled(enabled);
        this.mDefaultRatePref.setEnabled(enabled);
        this.mDefaultPitchPref.setEnabled(enabled);
    }
    
    @Override
    public int getMetricsCategory() {
        return 94;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1983) {
            this.onSampleTextReceived(n2, intent);
        }
        else if (n == 1977) {
            this.onVoiceDataIntegrityCheckDone(intent);
            if (n2 != 0) {
                this.updateDefaultLocalePref(intent);
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082849);
        this.getActivity().setVolumeControlStream(3);
        this.mEnginesHelper = new TtsEngines(this.getActivity().getApplicationContext());
        (this.mLocalePreference = (ListPreference)this.findPreference("tts_default_lang")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mDefaultPitchPref = (SeekBarPreference)this.findPreference("tts_default_pitch");
        this.mDefaultRatePref = (SeekBarPreference)this.findPreference("tts_default_rate");
        final ActionButtonPreference setButton1OnClickListener = ((ActionButtonPreference)this.findPreference("action_buttons")).setButton1Text(2131889527).setButton1Positive(true).setButton1OnClickListener((View.OnClickListener)new _$$Lambda$TextToSpeechSettings$_mqMfqhP2l_0b2lu0aliM8gSxIQ(this));
        boolean enabled = false;
        this.mActionButtons = setButton1OnClickListener.setButton1Enabled(false).setButton2Text(2131889530).setButton2Positive(false).setButton2OnClickListener((View.OnClickListener)new _$$Lambda$TextToSpeechSettings$_PSeoELUhAn9aTlkws2o7dPjqCc(this)).setButton1Enabled(true);
        if (bundle == null) {
            this.mLocalePreference.setEnabled(false);
            this.mLocalePreference.setEntries(new CharSequence[0]);
            this.mLocalePreference.setEntryValues(new CharSequence[0]);
        }
        else {
            final CharSequence[] charSequenceArray = bundle.getCharSequenceArray("locale_entries");
            final CharSequence[] charSequenceArray2 = bundle.getCharSequenceArray("locale_entry_values");
            final CharSequence charSequence = bundle.getCharSequence("locale_value");
            this.mLocalePreference.setEntries(charSequenceArray);
            this.mLocalePreference.setEntryValues(charSequenceArray2);
            final ListPreference mLocalePreference = this.mLocalePreference;
            String string;
            if (charSequence != null) {
                string = charSequence.toString();
            }
            else {
                string = null;
            }
            mLocalePreference.setValue(string);
            final ListPreference mLocalePreference2 = this.mLocalePreference;
            if (charSequenceArray.length > 0) {
                enabled = true;
            }
            mLocalePreference2.setEnabled(enabled);
        }
        this.mTts = new TextToSpeech(this.getActivity().getApplicationContext(), this.mInitListener);
        this.setTtsUtteranceProgressListener();
        this.initSettings();
        this.setRetainInstance(true);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mTts != null) {
            this.mTts.shutdown();
            this.mTts = null;
        }
    }
    
    @Override
    public void onGearClick(final GearPreference gearPreference) {
        if ("tts_engine_preference".equals(gearPreference.getKey())) {
            final Intent settingsIntent = this.mEnginesHelper.getSettingsIntent(this.mEnginesHelper.getEngineInfo(this.mCurrentEngine).name);
            if (settingsIntent != null) {
                this.startActivity(settingsIntent);
            }
            else {
                Log.e("TextToSpeechSettings", "settingsIntent is null");
            }
        }
    }
    
    public void onInitEngine(final int n) {
        if (n == 0) {
            this.checkDefaultLocale();
            this.getActivity().runOnUiThread((Runnable)new Runnable() {
                @Override
                public void run() {
                    TextToSpeechSettings.this.mLocalePreference.setEnabled(true);
                }
            });
        }
        else {
            this.updateWidgetState(false);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if ("tts_default_rate".equals(preference.getKey())) {
            this.updateSpeechRate((int)o);
        }
        else if ("tts_default_pitch".equals(preference.getKey())) {
            this.updateSpeechPitchValue((int)o);
        }
        else if (preference == this.mLocalePreference) {
            final String s = (String)o;
            Locale localeString;
            if (!TextUtils.isEmpty((CharSequence)s)) {
                localeString = this.mEnginesHelper.parseLocaleString(s);
            }
            else {
                localeString = null;
            }
            this.updateLanguageTo(localeString);
            this.checkDefaultLocale();
            return true;
        }
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mTts != null && this.mCurrentDefaultLocale != null) {
            if (!this.mTts.getDefaultEngine().equals(this.mTts.getCurrentEngine())) {
                try {
                    this.mTts.shutdown();
                    this.mTts = null;
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Error shutting down TTS engine");
                    sb.append(ex);
                    Log.e("TextToSpeechSettings", sb.toString());
                }
                this.mTts = new TextToSpeech(this.getActivity().getApplicationContext(), this.mInitListener);
                this.setTtsUtteranceProgressListener();
                this.initSettings();
            }
            else {
                this.mTts.setPitch(Settings.Secure.getInt(this.getContentResolver(), "tts_default_pitch", 100) / 100.0f);
            }
            final Locale defaultLanguage = this.mTts.getDefaultLanguage();
            if (this.mCurrentDefaultLocale != null && !this.mCurrentDefaultLocale.equals(defaultLanguage)) {
                this.updateWidgetState(false);
                this.checkDefaultLocale();
            }
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequenceArray("locale_entries", this.mLocalePreference.getEntries());
        bundle.putCharSequenceArray("locale_entry_values", this.mLocalePreference.getEntryValues());
        bundle.putCharSequence("locale_value", (CharSequence)this.mLocalePreference.getValue());
    }
}
