package com.android.settings.tts;

import android.os.Bundle;
import java.util.Collections;
import java.util.Comparator;
import android.util.Pair;
import java.util.HashMap;
import android.speech.tts.UtteranceProgressListener;
import java.util.ArrayList;
import java.util.Set;
import android.speech.tts.TextToSpeech$EngineInfo;
import android.content.ContentResolver;
import com.android.settings.SettingsActivity;
import android.provider.Settings;
import java.util.Iterator;
import java.util.MissingResourceException;
import android.text.TextUtils;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import java.util.Objects;
import android.util.Log;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.speech.tts.TextToSpeech;
import android.support.v7.preference.ListPreference;
import android.speech.tts.TextToSpeech$OnInitListener;
import android.speech.tts.TtsEngines;
import com.android.settings.widget.SeekBarPreference;
import java.util.Locale;
import java.util.List;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settings.widget.GearPreference;
import com.android.settings.search.Indexable;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$TextToSpeechSettings$_mqMfqhP2l_0b2lu0aliM8gSxIQ implements View.OnClickListener
{
    public final void onClick(final View view) {
        TextToSpeechSettings.lambda$onCreate$0(this.f$0, view);
    }
}
