package com.android.settings.tts;

import android.support.v7.preference.PreferenceViewHolder;
import android.content.DialogInterface;
import android.app.AlertDialog$Builder;
import android.util.Log;
import android.content.DialogInterface$OnClickListener;
import android.widget.Checkable;
import android.widget.CompoundButton;
import com.android.settings.SettingsActivity;
import android.content.Context;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.RadioButton;
import android.speech.tts.TextToSpeech$EngineInfo;
import android.support.v7.preference.Preference;

public class TtsEnginePreference extends Preference
{
    private final TextToSpeech$EngineInfo mEngineInfo;
    private volatile boolean mPreventRadioButtonCallbacks;
    private RadioButton mRadioButton;
    private final CompoundButton$OnCheckedChangeListener mRadioChangeListener;
    private final RadioButtonGroupState mSharedState;
    
    public TtsEnginePreference(final Context context, final TextToSpeech$EngineInfo mEngineInfo, final RadioButtonGroupState mSharedState, final SettingsActivity settingsActivity) {
        super(context);
        this.mRadioChangeListener = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                TtsEnginePreference.this.onRadioButtonClicked(compoundButton, b);
            }
        };
        this.setLayoutResource(2131558672);
        this.mSharedState = mSharedState;
        this.mEngineInfo = mEngineInfo;
        this.mPreventRadioButtonCallbacks = false;
        this.setKey(this.mEngineInfo.name);
        this.setTitle(this.mEngineInfo.label);
    }
    
    private void displayDataAlert(final DialogInterface$OnClickListener dialogInterface$OnClickListener, final DialogInterface$OnClickListener dialogInterface$OnClickListener2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Displaying data alert for :");
        sb.append(this.mEngineInfo.name);
        Log.i("TtsEnginePreference", sb.toString());
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(this.getContext());
        alertDialog$Builder.setTitle(17039380).setMessage((CharSequence)this.getContext().getString(2131889514, new Object[] { this.mEngineInfo.label })).setCancelable(true).setPositiveButton(17039370, dialogInterface$OnClickListener).setNegativeButton(17039360, dialogInterface$OnClickListener2);
        alertDialog$Builder.create().show();
    }
    
    private void makeCurrentEngine(final Checkable currentChecked) {
        if (this.mSharedState.getCurrentChecked() != null) {
            this.mSharedState.getCurrentChecked().setChecked(false);
        }
        this.mSharedState.setCurrentChecked(currentChecked);
        this.mSharedState.setCurrentKey(this.getKey());
        this.callChangeListener(this.mSharedState.getCurrentKey());
    }
    
    private void onRadioButtonClicked(final CompoundButton compoundButton, final boolean b) {
        if (!this.mPreventRadioButtonCallbacks && this.mSharedState.getCurrentChecked() != compoundButton) {
            if (b) {
                if (this.shouldDisplayDataAlert()) {
                    this.displayDataAlert((DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            TtsEnginePreference.this.makeCurrentEngine((Checkable)compoundButton);
                        }
                    }, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            compoundButton.setChecked(false);
                        }
                    });
                }
                else {
                    this.makeCurrentEngine((Checkable)compoundButton);
                }
            }
        }
    }
    
    private boolean shouldDisplayDataAlert() {
        return this.mEngineInfo.system ^ true;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        if (this.mSharedState != null) {
            final RadioButton radioButton = (RadioButton)preferenceViewHolder.findViewById(2131362756);
            radioButton.setOnCheckedChangeListener(this.mRadioChangeListener);
            radioButton.setText((CharSequence)this.mEngineInfo.label);
            final boolean equals = this.getKey().equals(this.mSharedState.getCurrentKey());
            if (equals) {
                this.mSharedState.setCurrentChecked((Checkable)radioButton);
            }
            this.mPreventRadioButtonCallbacks = true;
            radioButton.setChecked(equals);
            this.mPreventRadioButtonCallbacks = false;
            this.mRadioButton = radioButton;
            return;
        }
        throw new IllegalStateException("Call to getView() before a call tosetSharedState()");
    }
    
    public interface RadioButtonGroupState
    {
        Checkable getCurrentChecked();
        
        String getCurrentKey();
        
        void setCurrentChecked(final Checkable p0);
        
        void setCurrentKey(final String p0);
    }
}
