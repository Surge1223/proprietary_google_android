package com.android.settings.tts;

import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import java.util.Iterator;
import android.support.v7.preference.Preference;
import android.speech.tts.TextToSpeech$EngineInfo;
import com.android.settings.SettingsActivity;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.speech.tts.TextToSpeech$OnInitListener;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TtsEngines;
import android.support.v7.preference.PreferenceCategory;
import android.widget.Checkable;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public class TtsEnginePreferenceFragment extends SettingsPreferenceFragment implements Indexable, RadioButtonGroupState
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private Checkable mCurrentChecked;
    private String mCurrentEngine;
    private PreferenceCategory mEnginePreferenceCategory;
    private TtsEngines mEnginesHelper;
    private String mPreviousEngine;
    private TextToSpeech mTts;
    private final TextToSpeech$OnInitListener mUpdateListener;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082848;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    public TtsEnginePreferenceFragment() {
        this.mTts = null;
        this.mEnginesHelper = null;
        this.mUpdateListener = (TextToSpeech$OnInitListener)new TextToSpeech$OnInitListener() {
            public void onInit(final int n) {
                TtsEnginePreferenceFragment.this.onUpdateEngine(n);
            }
        };
    }
    
    private void initSettings() {
        if (this.mTts != null) {
            this.mCurrentEngine = this.mTts.getCurrentEngine();
        }
        this.mEnginePreferenceCategory.removeAll();
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        final Iterator<TextToSpeech$EngineInfo> iterator = this.mEnginesHelper.getEngines().iterator();
        while (iterator.hasNext()) {
            this.mEnginePreferenceCategory.addPreference(new TtsEnginePreference(this.getPrefContext(), iterator.next(), (TtsEnginePreference.RadioButtonGroupState)this, settingsActivity));
        }
    }
    
    private void updateDefaultEngine(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Updating default synth to : ");
        sb.append(s);
        Log.d("TtsEnginePrefFragment", sb.toString());
        this.mPreviousEngine = this.mTts.getCurrentEngine();
        Log.i("TtsEnginePrefFragment", "Shutting down current tts engine");
        if (this.mTts != null) {
            try {
                this.mTts.shutdown();
                this.mTts = null;
            }
            catch (Exception ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Error shutting down TTS engine");
                sb2.append(ex);
                Log.e("TtsEnginePrefFragment", sb2.toString());
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Updating engine : Attempting to connect to engine: ");
        sb3.append(s);
        Log.i("TtsEnginePrefFragment", sb3.toString());
        this.mTts = new TextToSpeech(this.getActivity().getApplicationContext(), this.mUpdateListener, s);
        Log.i("TtsEnginePrefFragment", "Success");
    }
    
    @Override
    public Checkable getCurrentChecked() {
        return this.mCurrentChecked;
    }
    
    @Override
    public String getCurrentKey() {
        return this.mCurrentEngine;
    }
    
    @Override
    public int getMetricsCategory() {
        return 93;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082848);
        this.mEnginePreferenceCategory = (PreferenceCategory)this.findPreference("tts_engine_preference_category");
        this.mEnginesHelper = new TtsEngines(this.getActivity().getApplicationContext());
        this.mTts = new TextToSpeech(this.getActivity().getApplicationContext(), (TextToSpeech$OnInitListener)null);
        this.initSettings();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mTts != null) {
            this.mTts.shutdown();
            this.mTts = null;
        }
    }
    
    public void onUpdateEngine(final int n) {
        if (n == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Updating engine: Successfully bound to the engine: ");
            sb.append(this.mTts.getCurrentEngine());
            Log.d("TtsEnginePrefFragment", sb.toString());
            Settings.Secure.putString(this.getContentResolver(), "tts_default_synth", this.mTts.getCurrentEngine());
        }
        else {
            Log.d("TtsEnginePrefFragment", "Updating engine: Failed to bind to engine, reverting.");
            if (this.mPreviousEngine != null) {
                this.mTts = new TextToSpeech(this.getActivity().getApplicationContext(), (TextToSpeech$OnInitListener)null, this.mPreviousEngine);
            }
            this.mPreviousEngine = null;
        }
    }
    
    @Override
    public void setCurrentChecked(final Checkable mCurrentChecked) {
        this.mCurrentChecked = mCurrentChecked;
    }
    
    @Override
    public void setCurrentKey(final String mCurrentEngine) {
        this.updateDefaultEngine(this.mCurrentEngine = mCurrentEngine);
    }
}
