package com.android.settings;

import android.app.ActivityManager$TaskDescription;
import android.content.IntentFilter;
import com.android.settingslib.core.instrumentation.Instrumentable;
import com.android.settings.core.SubSettingLauncher;
import android.support.v7.preference.Preference;
import android.support.v4.content.LocalBroadcastManager;
import android.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.Activity;
import android.widget.Toolbar;
import java.util.Collection;
import com.android.settings.dashboard.DashboardSummary;
import com.android.settingslib.core.instrumentation.SharedPreferencesLogger;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap$Config;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.overlay.FeatureFactory;
import android.app.FragmentTransaction;
import android.transition.TransitionManager;
import android.app.Fragment;
import android.os.Bundle;
import android.os.UserHandle;
import android.app.FragmentManager$BackStackEntry;
import com.android.internal.util.ArrayUtils;
import com.android.settings.core.gateway.SettingsGateway;
import android.content.ComponentName;
import com.android.settings.applications.manageapplications.ManageApplications;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import com.android.settings.widget.SwitchBar;
import android.widget.Button;
import com.android.settings.dashboard.DashboardFeatureProvider;
import android.view.ViewGroup;
import com.android.settingslib.drawer.DashboardCategory;
import java.util.ArrayList;
import android.content.BroadcastReceiver;
import android.support.v7.preference.PreferenceManager;
import android.support.v14.preference.PreferenceFragment;
import android.app.FragmentManager$OnBackStackChangedListener;
import com.android.settingslib.drawer.SettingsDrawerActivity;
import com.android.settings.search.DeviceIndexFeatureProvider;

public final class _$$Lambda$SettingsActivity$HXMcoLHGNmdxTucTgqvnp3fY_K8 implements Runnable
{
    @Override
    public final void run() {
        SettingsActivity.lambda$updateDeviceIndex$0(this.f$0, this.f$1);
    }
}
