package com.android.settings;

import android.os.SystemProperties;
import android.content.Intent;

public class SetupWizardUtils
{
    static final String SYSTEM_PROP_SETUPWIZARD_THEME = "setupwizard.theme";
    
    public static void copySetupExtras(final Intent intent, final Intent intent2) {
        intent2.putExtra("theme", intent.getStringExtra("theme"));
        intent2.putExtra("useImmersiveMode", intent.getBooleanExtra("useImmersiveMode", false));
    }
    
    public static int getTheme(final Intent intent) {
        String s;
        if ((s = intent.getStringExtra("theme")) == null) {
            s = SystemProperties.get("setupwizard.theme");
        }
        if (s != null) {
            switch (s) {
                case "glif": {
                    return 2131951797;
                }
                case "glif_light": {
                    return 2131951798;
                }
                case "glif_v2": {
                    return 2131951799;
                }
                case "glif_v2_light": {
                    return 2131951800;
                }
                case "glif_v3": {
                    return 2131951805;
                }
                case "glif_v3_light": {
                    return 2131951806;
                }
            }
        }
        return 2131951798;
    }
    
    public static int getTransparentTheme(final Intent intent) {
        final int theme = getTheme(intent);
        int n = 2131951801;
        if (theme == 2131951805) {
            n = 2131951808;
        }
        else if (theme == 2131951806) {
            n = 2131951807;
        }
        else if (theme == 2131951799) {
            n = 2131951802;
        }
        else if (theme == 2131951798) {
            n = 2131951913;
        }
        else if (theme == 2131951797) {
            n = 2131951914;
        }
        return n;
    }
}
