package com.android.settings.dashboard;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

class DashboardTilePlaceholderPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private int mOrder;
    
    public DashboardTilePlaceholderPreferenceController(final Context context) {
        super(context);
        this.mOrder = Integer.MAX_VALUE;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        if (preference != null) {
            this.mOrder = preference.getOrder();
            preferenceScreen.removePreference(preference);
        }
    }
    
    public int getOrder() {
        return this.mOrder;
    }
    
    @Override
    public String getPreferenceKey() {
        return "dashboard_tile_placeholder";
    }
    
    @Override
    public boolean isAvailable() {
        return false;
    }
}
