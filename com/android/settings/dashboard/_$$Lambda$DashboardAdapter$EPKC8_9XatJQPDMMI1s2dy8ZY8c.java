package com.android.settings.dashboard;

import android.widget.TextView;
import android.widget.LinearLayout;
import android.os.Parcelable;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.res.Resources.Theme;
import android.text.TextUtils;
import android.service.settings.suggestions.Suggestion;
import android.support.v7.widget.LinearLayoutManager;
import com.android.settings.dashboard.conditional.ConditionAdapter;
import android.support.v7.util.DiffUtil;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import com.android.settingslib.drawer.DashboardCategory;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.drawer.Tile;
import android.app.Activity;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settings.dashboard.conditional.Condition;
import java.util.List;
import android.os.Bundle;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import com.android.settingslib.utils.IconCache;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.dashboard.suggestions.SuggestionAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$DashboardAdapter$EPKC8_9XatJQPDMMI1s2dy8ZY8c implements View.OnClickListener
{
    public final void onClick(final View view) {
        DashboardAdapter.lambda$onBindViewHolder$0(this.f$0, view);
    }
}
