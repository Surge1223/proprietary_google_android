package com.android.settings.dashboard;

import android.support.v4.view.ViewCompat;
import com.android.settingslib.drawer.Tile;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.DefaultItemAnimator;

public class DashboardItemAnimator extends DefaultItemAnimator
{
    @Override
    public boolean animateChange(final ViewHolder viewHolder, final ViewHolder viewHolder2, final int n, final int n2, final int n3, final int n4) {
        int n5 = n;
        int n6 = n2;
        if (viewHolder.itemView.getTag() instanceof Tile) {
            n5 = n;
            n6 = n2;
            if (viewHolder == viewHolder2) {
                int n7 = n;
                int n8 = n2;
                if (!this.isRunning()) {
                    n7 = (int)(n + ViewCompat.getTranslationX(viewHolder.itemView));
                    n8 = (int)(n2 + ViewCompat.getTranslationY(viewHolder.itemView));
                }
                n5 = n7;
                n6 = n8;
                if (n7 == n3) {
                    n5 = n7;
                    if ((n6 = n8) == n4) {
                        this.dispatchMoveFinished(viewHolder);
                        return false;
                    }
                }
            }
        }
        return super.animateChange(viewHolder, viewHolder2, n5, n6, n3, n4);
    }
}
