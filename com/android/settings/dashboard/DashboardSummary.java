package com.android.settings.dashboard;

import com.android.settingslib.utils.ThreadUtils;
import java.util.Iterator;
import com.android.settings.dashboard.conditional.Condition;
import com.android.settings.widget.ActionBarShadowController;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.app.Activity;
import android.arch.lifecycle.LifecycleObserver;
import android.os.Bundle;
import com.android.settings.dashboard.suggestions.SuggestionFeatureProvider;
import com.android.settings.overlay.FeatureFactory;
import android.util.Log;
import android.content.Context;
import android.app.LoaderManager;
import android.service.settings.suggestions.Suggestion;
import java.util.List;
import com.android.settingslib.drawer.DashboardCategory;
import android.support.v7.widget.LinearLayoutManager;
import android.os.Handler;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settingslib.drawer.SettingsDrawerActivity;
import com.android.settings.dashboard.conditional.FocusRecyclerView;
import com.android.settings.dashboard.conditional.ConditionManager;
import com.android.settings.core.InstrumentedFragment;

public class DashboardSummary extends InstrumentedFragment implements ConditionListener, FocusListener, CategoryListener, SuggestionControllerHost
{
    private DashboardAdapter mAdapter;
    private ConditionManager mConditionManager;
    private FocusRecyclerView mDashboard;
    private DashboardFeatureProvider mDashboardFeatureProvider;
    private final Handler mHandler;
    boolean mIsOnCategoriesChangedCalled;
    private LinearLayoutManager mLayoutManager;
    private boolean mOnConditionsChangedCalled;
    private DashboardCategory mStagingCategory;
    private List<Suggestion> mStagingSuggestions;
    private SuggestionControllerMixin mSuggestionControllerMixin;
    private SummaryLoader mSummaryLoader;
    
    public DashboardSummary() {
        this.mHandler = new Handler();
    }
    
    @Override
    public LoaderManager getLoaderManager() {
        if (!this.isAdded()) {
            return null;
        }
        return super.getLoaderManager();
    }
    
    @Override
    public int getMetricsCategory() {
        return 35;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        Log.d("DashboardSummary", "Creating SuggestionControllerMixin");
        final SuggestionFeatureProvider suggestionFeatureProvider = FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context);
        if (suggestionFeatureProvider.isSuggestionEnabled(context)) {
            this.mSuggestionControllerMixin = new SuggestionControllerMixin(context, (SuggestionControllerMixin.SuggestionControllerHost)this, this.getLifecycle(), suggestionFeatureProvider.getSuggestionServiceComponent());
        }
    }
    
    @Override
    public void onCategoriesChanged() {
        if (this.mIsOnCategoriesChangedCalled) {
            this.rebuildUI();
        }
        this.mIsOnCategoriesChangedCalled = true;
    }
    
    @Override
    public void onConditionsChanged() {
        Log.d("DashboardSummary", "onConditionsChanged");
        final boolean mOnConditionsChangedCalled = this.mOnConditionsChangedCalled;
        boolean b = true;
        if (mOnConditionsChangedCalled) {
            if (this.mLayoutManager.findFirstCompletelyVisibleItemPosition() > 1) {
                b = false;
            }
            this.mAdapter.setConditions(this.mConditionManager.getConditions());
            if (b) {
                this.mDashboard.scrollToPosition(0);
            }
        }
        else {
            this.mOnConditionsChangedCalled = true;
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        System.currentTimeMillis();
        super.onCreate(bundle);
        Log.d("DashboardSummary", "Starting DashboardSummary");
        final Activity activity = this.getActivity();
        this.mDashboardFeatureProvider = FeatureFactory.getFactory((Context)activity).getDashboardFeatureProvider((Context)activity);
        this.mSummaryLoader = new SummaryLoader(activity, "com.android.settings.category.ia.homepage");
        this.mConditionManager = ConditionManager.get((Context)activity, false);
        this.getLifecycle().addObserver(this.mConditionManager);
        if (bundle != null) {
            this.mIsOnCategoriesChangedCalled = bundle.getBoolean("categories_change_called");
        }
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        System.currentTimeMillis();
        final View inflate = layoutInflater.inflate(2131558521, viewGroup, false);
        this.mDashboard = (FocusRecyclerView)inflate.findViewById(2131362036);
        (this.mLayoutManager = new LinearLayoutManager(this.getContext())).setOrientation(1);
        if (bundle != null) {
            this.mLayoutManager.scrollToPosition(bundle.getInt("scroll_position"));
        }
        this.mDashboard.setLayoutManager((RecyclerView.LayoutManager)this.mLayoutManager);
        this.mDashboard.setHasFixedSize(true);
        this.mDashboard.setListener((FocusRecyclerView.FocusListener)this);
        this.mDashboard.setItemAnimator((RecyclerView.ItemAnimator)new DashboardItemAnimator());
        this.mAdapter = new DashboardAdapter(this.getContext(), bundle, this.mConditionManager.getConditions(), this.mSuggestionControllerMixin, this.getLifecycle());
        this.mDashboard.setAdapter((RecyclerView.Adapter)this.mAdapter);
        this.mSummaryLoader.setSummaryConsumer((SummaryLoader.SummaryConsumer)this.mAdapter);
        ActionBarShadowController.attachToRecyclerView(this.getActivity().findViewById(2131362559), this.getLifecycle(), this.mDashboard);
        this.rebuildUI();
        return inflate;
    }
    
    @Override
    public void onDestroy() {
        this.mSummaryLoader.release();
        super.onDestroy();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        ((SettingsDrawerActivity)this.getActivity()).remCategoryListener((SettingsDrawerActivity.CategoryListener)this);
        this.mSummaryLoader.setListening(false);
        for (final Condition condition : this.mConditionManager.getConditions()) {
            if (condition.shouldShow()) {
                this.mMetricsFeatureProvider.hidden(this.getContext(), condition.getMetricsConstant());
            }
        }
    }
    
    @Override
    public void onResume() {
        System.currentTimeMillis();
        super.onResume();
        ((SettingsDrawerActivity)this.getActivity()).addCategoryListener((SettingsDrawerActivity.CategoryListener)this);
        this.mSummaryLoader.setListening(true);
        final int metricsCategory = this.getMetricsCategory();
        for (final Condition condition : this.mConditionManager.getConditions()) {
            if (condition.shouldShow()) {
                this.mMetricsFeatureProvider.visible(this.getContext(), metricsCategory, condition.getMetricsConstant());
            }
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mLayoutManager == null) {
            return;
        }
        bundle.putBoolean("categories_change_called", this.mIsOnCategoriesChangedCalled);
        bundle.putInt("scroll_position", this.mLayoutManager.findFirstVisibleItemPosition());
    }
    
    @Override
    public void onSuggestionReady(final List<Suggestion> list) {
        this.mStagingSuggestions = list;
        this.mAdapter.setSuggestions(list);
        if (this.mStagingCategory != null) {
            Log.d("DashboardSummary", "Category has loaded, setting category from suggestionReady");
            this.mHandler.removeCallbacksAndMessages((Object)null);
            this.mAdapter.setCategory(this.mStagingCategory);
        }
    }
    
    @Override
    public void onWindowFocusChanged(final boolean b) {
        System.currentTimeMillis();
        if (b) {
            Log.d("DashboardSummary", "Listening for condition changes");
            this.mConditionManager.addListener((ConditionManager.ConditionListener)this);
            Log.d("DashboardSummary", "conditions refreshed");
            this.mConditionManager.refreshAll();
        }
        else {
            Log.d("DashboardSummary", "Stopped listening for condition changes");
            this.mConditionManager.remListener((ConditionManager.ConditionListener)this);
        }
    }
    
    void rebuildUI() {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$DashboardSummary$8s9N5t2Nn47oUx2XbtJ_BLsLzIY(this));
    }
    
    void updateCategory() {
        final DashboardCategory tilesForCategory = this.mDashboardFeatureProvider.getTilesForCategory("com.android.settings.category.ia.homepage");
        this.mSummaryLoader.updateSummaryToCache(tilesForCategory);
        this.mStagingCategory = tilesForCategory;
        if (this.mSuggestionControllerMixin == null) {
            ThreadUtils.postOnMainThread(new _$$Lambda$DashboardSummary$IEZgZ97m6Eczh4OO9ztmxtZNqM8(this));
            return;
        }
        if (this.mSuggestionControllerMixin.isSuggestionLoaded()) {
            Log.d("DashboardSummary", "Suggestion has loaded, setting suggestion/category");
            ThreadUtils.postOnMainThread(new _$$Lambda$DashboardSummary$S4ZnJYAtpWnSKH5Ya_6PeP_43T4(this));
        }
        else {
            Log.d("DashboardSummary", "Suggestion NOT loaded, delaying setCategory by 3000ms");
            this.mHandler.postDelayed((Runnable)new _$$Lambda$DashboardSummary$kCUZowpTTsEozF_ygTzgGisYUiM(this), 3000L);
        }
    }
}
