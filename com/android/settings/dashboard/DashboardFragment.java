package com.android.settings.dashboard;

import android.graphics.drawable.Icon;
import android.content.res.TypedArray;
import com.android.settingslib.drawer.DashboardCategory;
import android.text.TextUtils;
import android.app.Activity;
import android.support.v7.preference.PreferenceManager;
import android.os.Bundle;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.Collection;
import com.android.settings.core.PreferenceControllerListHelper;
import com.android.settings.overlay.FeatureFactory;
import android.support.v7.preference.Preference;
import android.util.Log;
import com.android.settingslib.drawer.Tile;
import android.content.Context;
import java.util.ArrayList;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;
import android.support.v7.preference.PreferenceScreen;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.function.Function;
import android.util.ArraySet;
import android.util.ArrayMap;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.android.settingslib.drawer.SettingsDrawerActivity;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public abstract class DashboardFragment extends SettingsPreferenceFragment implements SummaryConsumer, Indexable, CategoryListener
{
    private DashboardFeatureProvider mDashboardFeatureProvider;
    private final Set<String> mDashboardTilePrefKeys;
    private boolean mListeningToCategoryChange;
    private DashboardTilePlaceholderPreferenceController mPlaceholderPreferenceController;
    private final Map<Class, List<AbstractPreferenceController>> mPreferenceControllers;
    private SummaryLoader mSummaryLoader;
    
    public DashboardFragment() {
        this.mPreferenceControllers = (Map<Class, List<AbstractPreferenceController>>)new ArrayMap();
        this.mDashboardTilePrefKeys = (Set<String>)new ArraySet();
    }
    
    private void displayResourceTiles() {
        final int preferenceScreenResId = this.getPreferenceScreenResId();
        if (preferenceScreenResId <= 0) {
            return;
        }
        this.addPreferencesFromResource(preferenceScreenResId);
        this.mPreferenceControllers.values().stream().flatMap((Function<? super List<AbstractPreferenceController>, ? extends Stream<?>>)_$$Lambda$seyL25CSW2NInOydsTbSDrNW6pM.INSTANCE).forEach(new _$$Lambda$DashboardFragment$wmCpqAavTrPCWLW0gqd6_3n9DOU(this.getPreferenceScreen()));
    }
    
    private void refreshAllPreferences(final String s) {
        if (this.getPreferenceScreen() != null) {
            this.getPreferenceScreen().removeAll();
        }
        this.displayResourceTiles();
        this.refreshDashboardTiles(s);
    }
    
    protected void addPreferenceController(final AbstractPreferenceController abstractPreferenceController) {
        if (this.mPreferenceControllers.get(abstractPreferenceController.getClass()) == null) {
            this.mPreferenceControllers.put(abstractPreferenceController.getClass(), new ArrayList<AbstractPreferenceController>());
        }
        this.mPreferenceControllers.get(abstractPreferenceController.getClass()).add(abstractPreferenceController);
    }
    
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return null;
    }
    
    protected boolean displayTile(final Tile tile) {
        return true;
    }
    
    public String getCategoryKey() {
        return DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.get(this.getClass().getName());
    }
    
    protected abstract String getLogTag();
    
    @Override
    protected abstract int getPreferenceScreenResId();
    
    @Override
    public void notifySummaryChanged(final Tile tile) {
        final String dashboardKeyForTile = this.mDashboardFeatureProvider.getDashboardKeyForTile(tile);
        final Preference preference = this.getPreferenceScreen().findPreference(dashboardKeyForTile);
        if (preference == null) {
            Log.d(this.getLogTag(), String.format("Can't find pref by key %s, skipping update summary %s/%s", dashboardKeyForTile, tile.title, tile.summary));
            return;
        }
        preference.setSummary(tile.summary);
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mDashboardFeatureProvider = FeatureFactory.getFactory(context).getDashboardFeatureProvider(context);
        final ArrayList<AbstractPreferenceController> list = new ArrayList<AbstractPreferenceController>();
        final List<AbstractPreferenceController> preferenceControllers = this.createPreferenceControllers(context);
        final List<BasePreferenceController> filterControllers = PreferenceControllerListHelper.filterControllers(PreferenceControllerListHelper.getPreferenceControllersFromXml(context, this.getPreferenceScreenResId()), preferenceControllers);
        if (preferenceControllers != null) {
            list.addAll((Collection<?>)preferenceControllers);
        }
        list.addAll((Collection<?>)filterControllers);
        filterControllers.stream().filter((Predicate<? super DashboardTilePlaceholderPreferenceController>)_$$Lambda$DashboardFragment$S_iRpeKDC_3jmfXOTbVaWpa8f5Y.INSTANCE).forEach(new _$$Lambda$DashboardFragment$iYpWkssUBFPuOKWOC_GeIjRUfdk(this.getLifecycle()));
        list.add(this.mPlaceholderPreferenceController = new DashboardTilePlaceholderPreferenceController(context));
        final Iterator<Object> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.addPreferenceController(iterator.next());
        }
    }
    
    @Override
    public void onCategoriesChanged() {
        if (this.mDashboardFeatureProvider.getTilesForCategory(this.getCategoryKey()) == null) {
            return;
        }
        this.refreshDashboardTiles(this.getLogTag());
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getPreferenceManager().setPreferenceComparisonCallback((PreferenceManager.PreferenceComparisonCallback)new PreferenceManager.SimplePreferenceComparisonCallback());
        if (bundle != null) {
            this.updatePreferenceStates();
        }
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        this.refreshAllPreferences(this.getLogTag());
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        final Collection<List<AbstractPreferenceController>> values = this.mPreferenceControllers.values();
        this.mMetricsFeatureProvider.logDashboardStartIntent(this.getContext(), preference.getIntent(), this.getMetricsCategory());
        final Iterator<List<AbstractPreferenceController>> iterator = values.iterator();
        while (iterator.hasNext()) {
            final Iterator<AbstractPreferenceController> iterator2 = iterator.next().iterator();
            while (iterator2.hasNext()) {
                if (iterator2.next().handlePreferenceTreeClick(preference)) {
                    return true;
                }
            }
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updatePreferenceStates();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (this.mDashboardFeatureProvider.getTilesForCategory(this.getCategoryKey()) == null) {
            return;
        }
        if (this.mSummaryLoader != null) {
            this.mSummaryLoader.setListening(true);
        }
        final Activity activity = this.getActivity();
        if (activity instanceof SettingsDrawerActivity) {
            this.mListeningToCategoryChange = true;
            ((SettingsDrawerActivity)activity).addCategoryListener((SettingsDrawerActivity.CategoryListener)this);
        }
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (this.mSummaryLoader != null) {
            this.mSummaryLoader.setListening(false);
        }
        if (this.mListeningToCategoryChange) {
            final Activity activity = this.getActivity();
            if (activity instanceof SettingsDrawerActivity) {
                ((SettingsDrawerActivity)activity).remCategoryListener((SettingsDrawerActivity.CategoryListener)this);
            }
            this.mListeningToCategoryChange = false;
        }
    }
    
    void refreshDashboardTiles(final String s) {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        final DashboardCategory tilesForCategory = this.mDashboardFeatureProvider.getTilesForCategory(this.getCategoryKey());
        if (tilesForCategory == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("NO dashboard tiles for ");
            sb.append(s);
            Log.d(s, sb.toString());
            return;
        }
        final List<Tile> tiles = tilesForCategory.getTiles();
        if (tiles == null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("tile list is empty, skipping category ");
            sb2.append((Object)tilesForCategory.title);
            Log.d(s, sb2.toString());
            return;
        }
        final ArrayList<String> list = new ArrayList<String>(this.mDashboardTilePrefKeys);
        if (this.mSummaryLoader != null) {
            this.mSummaryLoader.release();
        }
        final Context context = this.getContext();
        (this.mSummaryLoader = new SummaryLoader(this.getActivity(), this.getCategoryKey())).setSummaryConsumer((SummaryLoader.SummaryConsumer)this);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { 16843817 });
        final int color = obtainStyledAttributes.getColor(0, context.getColor(17170443));
        obtainStyledAttributes.recycle();
        for (final Tile tile : tiles) {
            final String dashboardKeyForTile = this.mDashboardFeatureProvider.getDashboardKeyForTile(tile);
            if (TextUtils.isEmpty((CharSequence)dashboardKeyForTile)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("tile does not contain a key, skipping ");
                sb3.append(tile);
                Log.d(s, sb3.toString());
            }
            else {
                if (!this.displayTile(tile)) {
                    continue;
                }
                if (this.tintTileIcon(tile)) {
                    tile.icon.setTint(color);
                }
                if (this.mDashboardTilePrefKeys.contains(dashboardKeyForTile)) {
                    this.mDashboardFeatureProvider.bindPreferenceToTile(this.getActivity(), this.getMetricsCategory(), preferenceScreen.findPreference(dashboardKeyForTile), tile, dashboardKeyForTile, this.mPlaceholderPreferenceController.getOrder());
                }
                else {
                    final String s2 = dashboardKeyForTile;
                    final Preference preference = new Preference(this.getPrefContext());
                    this.mDashboardFeatureProvider.bindPreferenceToTile(this.getActivity(), this.getMetricsCategory(), preference, tile, s2, this.mPlaceholderPreferenceController.getOrder());
                    preferenceScreen.addPreference(preference);
                    this.mDashboardTilePrefKeys.add(s2);
                }
                list.remove(dashboardKeyForTile);
            }
        }
        for (final String s3 : list) {
            this.mDashboardTilePrefKeys.remove(s3);
            final Preference preference2 = preferenceScreen.findPreference(s3);
            if (preference2 != null) {
                preferenceScreen.removePreference(preference2);
            }
        }
        this.mSummaryLoader.setListening(true);
    }
    
    boolean tintTileIcon(final Tile tile) {
        final Icon icon = tile.icon;
        final boolean b = false;
        if (icon == null) {
            return false;
        }
        final Bundle metaData = tile.metaData;
        if (metaData != null && metaData.containsKey("com.android.settings.icon_tintable")) {
            return metaData.getBoolean("com.android.settings.icon_tintable");
        }
        final String packageName = this.getContext().getPackageName();
        boolean b2 = b;
        if (packageName != null) {
            b2 = b;
            if (tile.intent != null) {
                b2 = b;
                if (!packageName.equals(tile.intent.getComponent().getPackageName())) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    protected void updatePreferenceStates() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        final Iterator<List<AbstractPreferenceController>> iterator = this.mPreferenceControllers.values().iterator();
        while (iterator.hasNext()) {
            for (final AbstractPreferenceController abstractPreferenceController : iterator.next()) {
                if (!abstractPreferenceController.isAvailable()) {
                    continue;
                }
                final String preferenceKey = abstractPreferenceController.getPreferenceKey();
                final Preference preference = preferenceScreen.findPreference(preferenceKey);
                if (preference == null) {
                    Log.d("DashboardFragment", String.format("Cannot find preference with key %s in Controller %s", preferenceKey, abstractPreferenceController.getClass().getSimpleName()));
                }
                else {
                    abstractPreferenceController.updateState(preference);
                }
            }
        }
    }
    
    protected <T extends AbstractPreferenceController> T use(final Class<T> clazz) {
        final List<AbstractPreferenceController> list = this.mPreferenceControllers.get(clazz);
        if (list != null) {
            if (list.size() > 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Multiple controllers of Class ");
                sb.append(clazz.getSimpleName());
                sb.append(" found, returning first one.");
                Log.w("DashboardFragment", sb.toString());
            }
            return (T)list.get(0);
        }
        return null;
    }
}
