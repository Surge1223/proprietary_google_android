package com.android.settings.dashboard;

import android.view.View;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.content.BroadcastReceiver;
import android.content.RestrictionsManager;
import com.android.settingslib.RestrictedLockUtils;
import android.widget.TextView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;

public final class _$$Lambda$RestrictedDashboardFragment$xeipMNP1JUFbhaWoStBNgM1y67g implements DialogInterface$OnDismissListener
{
    public final void onDismiss(final DialogInterface dialogInterface) {
        RestrictedDashboardFragment.lambda$onDataSetChanged$0(this.f$0, dialogInterface);
    }
}
