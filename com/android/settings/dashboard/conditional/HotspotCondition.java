package com.android.settings.dashboard.conditional;

import android.content.Intent;
import com.android.settings.TetherSettings;
import com.android.settings.core.SubSettingLauncher;
import android.net.ConnectivityManager;
import android.content.BroadcastReceiver;
import android.graphics.drawable.Drawable;
import android.content.Context;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.content.IntentFilter;

public class HotspotCondition extends Condition
{
    private static final IntentFilter WIFI_AP_STATE_FILTER;
    private final Receiver mReceiver;
    private final WifiManager mWifiManager;
    
    static {
        WIFI_AP_STATE_FILTER = new IntentFilter("android.net.wifi.WIFI_AP_STATE_CHANGED");
    }
    
    public HotspotCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mWifiManager = (WifiManager)this.mManager.getContext().getSystemService((Class)WifiManager.class);
        this.mReceiver = new Receiver();
    }
    
    private String getSsid() {
        final WifiConfiguration wifiApConfiguration = this.mWifiManager.getWifiApConfiguration();
        if (wifiApConfiguration == null) {
            return this.mManager.getContext().getString(17041124);
        }
        return wifiApConfiguration.SSID;
    }
    
    @Override
    public CharSequence[] getActions() {
        final Context context = this.mManager.getContext();
        if (RestrictedLockUtils.hasBaseUserRestriction(context, "no_config_tethering", UserHandle.myUserId())) {
            return new CharSequence[0];
        }
        return new CharSequence[] { context.getString(2131887088) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131231045);
    }
    
    @Override
    protected IntentFilter getIntentFilter() {
        return HotspotCondition.WIFI_AP_STATE_FILTER;
    }
    
    @Override
    public int getMetricsConstant() {
        return 382;
    }
    
    @Override
    protected BroadcastReceiver getReceiver() {
        return this.mReceiver;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887083, new Object[] { this.getSsid() });
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887084);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            final Context context = this.mManager.getContext();
            final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced(context, "no_config_tethering", UserHandle.myUserId());
            if (checkIfRestrictionEnforced != null) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent(context, checkIfRestrictionEnforced);
            }
            else {
                ((ConnectivityManager)context.getSystemService("connectivity")).stopTethering(0);
                this.setActive(false);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onPrimaryClick() {
        new SubSettingLauncher(this.mManager.getContext()).setDestination(TetherSettings.class.getName()).setSourceMetricsCategory(35).setTitle(R.string.tether_settings_title_all).addFlags(268435456).launch();
    }
    
    @Override
    public void refreshState() {
        this.setActive(this.mWifiManager.isWifiApEnabled());
    }
    
    public static class Receiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            if ("android.net.wifi.WIFI_AP_STATE_CHANGED".equals(intent.getAction())) {
                ConditionManager.get(context).getCondition(HotspotCondition.class).refreshState();
            }
        }
    }
}
