package com.android.settings.dashboard.conditional;

import com.android.settings.display.NightDisplaySettings;
import com.android.settings.core.SubSettingLauncher;
import android.graphics.drawable.Drawable;
import com.android.internal.app.ColorDisplayController;
import com.android.internal.app.ColorDisplayController$Callback;

public final class NightDisplayCondition extends Condition implements ColorDisplayController$Callback
{
    private ColorDisplayController mController;
    
    NightDisplayCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        (this.mController = new ColorDisplayController(conditionManager.getContext())).setListener((ColorDisplayController$Callback)this);
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887088) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131231135);
    }
    
    @Override
    public int getMetricsConstant() {
        return 492;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887085);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887086);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            this.mController.setActivated(false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void onActivated(final boolean b) {
        this.refreshState();
    }
    
    @Override
    public void onPrimaryClick() {
        new SubSettingLauncher(this.mManager.getContext()).setDestination(NightDisplaySettings.class.getName()).setSourceMetricsCategory(35).setTitle(2131888405).addFlags(268435456).launch();
    }
    
    @Override
    public void refreshState() {
        this.setActive(this.mController.isActivated());
    }
}
