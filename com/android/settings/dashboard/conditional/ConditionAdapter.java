package com.android.settings.dashboard.conditional;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.Iterator;
import java.util.Objects;
import android.widget.Button;
import android.util.Log;
import com.android.settingslib.WirelessUtils;
import com.android.settings.overlay.FeatureFactory;
import android.view.View;
import android.support.v7.widget.helper.ItemTouchHelper;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import java.util.List;
import android.view.View.OnClickListener;
import com.android.settings.dashboard.DashboardAdapter;
import android.support.v7.widget.RecyclerView;

public class ConditionAdapter extends Adapter<DashboardAdapter.DashboardItemHolder>
{
    private View.OnClickListener mConditionClickListener;
    private List<Condition> mConditions;
    private final Context mContext;
    private boolean mExpanded;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    ItemTouchHelper.SimpleCallback mSwipeCallback;
    
    public ConditionAdapter(final Context mContext, final List<Condition> mConditions, final boolean mExpanded) {
        this.mConditionClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Condition condition = (Condition)view.getTag();
                ConditionAdapter.this.mMetricsFeatureProvider.action(ConditionAdapter.this.mContext, 375, condition.getMetricsConstant());
                condition.onPrimaryClick();
            }
        };
        this.mSwipeCallback = new ItemTouchHelper.SimpleCallback(0, 48) {
            @Override
            public int getSwipeDirs(final RecyclerView recyclerView, final ViewHolder viewHolder) {
                int swipeDirs;
                if (viewHolder.getItemViewType() == 2131558494) {
                    swipeDirs = super.getSwipeDirs(recyclerView, viewHolder);
                }
                else {
                    swipeDirs = 0;
                }
                return swipeDirs;
            }
            
            @Override
            public boolean onMove(final RecyclerView recyclerView, final ViewHolder viewHolder, final ViewHolder viewHolder2) {
                return true;
            }
            
            @Override
            public void onSwiped(final ViewHolder viewHolder, final int n) {
                final Object item = ConditionAdapter.this.getItem(viewHolder.getItemId());
                if (item != null) {
                    ((Condition)item).silence();
                }
            }
        };
        this.mContext = mContext;
        this.mConditions = mConditions;
        this.mExpanded = mExpanded;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(mContext).getMetricsFeatureProvider();
        ((RecyclerView.Adapter)this).setHasStableIds(true);
    }
    
    private void bindViews(final Condition tag, final DashboardAdapter.DashboardItemHolder dashboardItemHolder, final boolean b, final View.OnClickListener onClickListener) {
        if (tag instanceof AirplaneModeCondition) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Airplane mode condition has been bound with isActive=");
            sb.append(tag.isActive());
            sb.append(". Airplane mode is currently ");
            sb.append(WirelessUtils.isAirplaneModeOn(tag.mManager.getContext()));
            Log.d("ConditionAdapter", sb.toString());
        }
        final View viewById = dashboardItemHolder.itemView.findViewById(R.id.content);
        viewById.setTag((Object)tag);
        viewById.setOnClickListener(onClickListener);
        dashboardItemHolder.icon.setImageDrawable(tag.getIcon());
        dashboardItemHolder.title.setText(tag.getTitle());
        final CharSequence[] actions = tag.getActions();
        this.setViewVisibility(dashboardItemHolder.itemView, 2131361947, actions.length > 0);
        dashboardItemHolder.summary.setText(tag.getSummary());
        for (int i = 0; i < 2; ++i) {
            final View itemView = dashboardItemHolder.itemView;
            int n;
            if (i == 0) {
                n = 2131362161;
            }
            else {
                n = 2131362569;
            }
            final Button button = (Button)itemView.findViewById(n);
            if (actions.length > i) {
                button.setVisibility(0);
                button.setText(actions[i]);
                button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        final Context context = view.getContext();
                        FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 376, tag.getMetricsConstant());
                        tag.onActionClick(i);
                    }
                });
            }
            else {
                button.setVisibility(8);
            }
        }
        this.setViewVisibility(dashboardItemHolder.itemView, R.id.divider, b ^ true);
    }
    
    private void setViewVisibility(View viewById, int visibility, final boolean b) {
        viewById = viewById.findViewById(visibility);
        if (viewById != null) {
            if (b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
        }
    }
    
    public void addDismissHandling(final RecyclerView recyclerView) {
        new ItemTouchHelper((ItemTouchHelper.Callback)this.mSwipeCallback).attachToRecyclerView(recyclerView);
    }
    
    public Object getItem(final long n) {
        for (final Condition condition : this.mConditions) {
            if (Objects.hash(condition.getTitle()) == n) {
                return condition;
            }
        }
        return null;
    }
    
    @Override
    public int getItemCount() {
        if (this.mExpanded) {
            return this.mConditions.size();
        }
        return 0;
    }
    
    @Override
    public long getItemId(final int n) {
        return Objects.hash(this.mConditions.get(n).getTitle());
    }
    
    @Override
    public int getItemViewType(final int n) {
        return 2131558494;
    }
    
    public void onBindViewHolder(final DashboardAdapter.DashboardItemHolder dashboardItemHolder, final int n) {
        final Condition condition = this.mConditions.get(n);
        final int size = this.mConditions.size();
        boolean b = true;
        if (n != size - 1) {
            b = false;
        }
        this.bindViews(condition, dashboardItemHolder, b, this.mConditionClickListener);
    }
    
    public DashboardAdapter.DashboardItemHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        return new DashboardAdapter.DashboardItemHolder(LayoutInflater.from(viewGroup.getContext()).inflate(n, viewGroup, false));
    }
}
