package com.android.settings.dashboard.conditional;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.media.AudioManager;

public abstract class AbnormalRingerConditionBase extends Condition
{
    protected final AudioManager mAudioManager;
    private final IntentFilter mFilter;
    private final RingerModeChangeReceiver mReceiver;
    
    AbnormalRingerConditionBase(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mAudioManager = (AudioManager)this.mManager.getContext().getSystemService("audio");
        this.mReceiver = new RingerModeChangeReceiver(this);
        this.mFilter = new IntentFilter("android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION");
        conditionManager.getContext().registerReceiver((BroadcastReceiver)this.mReceiver, this.mFilter);
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getText(2131887076) };
    }
    
    @Override
    public void onActionClick(final int n) {
        this.mAudioManager.setRingerModeInternal(2);
        this.mAudioManager.setStreamVolume(2, 1, 0);
        this.refreshState();
    }
    
    @Override
    public void onPrimaryClick() {
        this.mManager.getContext().startActivity(new Intent("android.settings.SOUND_SETTINGS").addFlags(268435456));
    }
    
    static class RingerModeChangeReceiver extends BroadcastReceiver
    {
        private final AbnormalRingerConditionBase mCondition;
        
        public RingerModeChangeReceiver(final AbnormalRingerConditionBase mCondition) {
            this.mCondition = mCondition;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            if ("android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION".equals(intent.getAction())) {
                this.mCondition.refreshState();
            }
        }
    }
}
