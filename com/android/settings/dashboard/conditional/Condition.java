package com.android.settings.dashboard.conditional;

import android.os.PersistableBundle;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;

public abstract class Condition
{
    private boolean mIsActive;
    private boolean mIsSilenced;
    private long mLastStateChange;
    protected final ConditionManager mManager;
    protected final MetricsFeatureProvider mMetricsFeatureProvider;
    protected boolean mReceiverRegistered;
    
    Condition(final ConditionManager conditionManager) {
        this(conditionManager, FeatureFactory.getFactory(conditionManager.getContext()).getMetricsFeatureProvider());
    }
    
    Condition(final ConditionManager mManager, final MetricsFeatureProvider mMetricsFeatureProvider) {
        this.mManager = mManager;
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
    }
    
    public abstract CharSequence[] getActions();
    
    public abstract Drawable getIcon();
    
    protected IntentFilter getIntentFilter() {
        return null;
    }
    
    long getLastChange() {
        return this.mLastStateChange;
    }
    
    public abstract int getMetricsConstant();
    
    protected BroadcastReceiver getReceiver() {
        return null;
    }
    
    public abstract CharSequence getSummary();
    
    public abstract CharSequence getTitle();
    
    public boolean isActive() {
        return this.mIsActive;
    }
    
    public boolean isSilenced() {
        return this.mIsSilenced;
    }
    
    protected void notifyChanged() {
        this.mManager.notifyChanged(this);
    }
    
    public abstract void onActionClick(final int p0);
    
    public void onPause() {
    }
    
    public abstract void onPrimaryClick();
    
    public void onResume() {
    }
    
    void onSilenceChanged(final boolean b) {
        final BroadcastReceiver receiver = this.getReceiver();
        if (receiver == null) {
            return;
        }
        if (b) {
            if (!this.mReceiverRegistered) {
                this.mManager.getContext().registerReceiver(receiver, this.getIntentFilter());
                this.mReceiverRegistered = true;
            }
        }
        else if (this.mReceiverRegistered) {
            this.mManager.getContext().unregisterReceiver(receiver);
            this.mReceiverRegistered = false;
        }
    }
    
    public abstract void refreshState();
    
    void restoreState(final PersistableBundle persistableBundle) {
        this.mIsSilenced = persistableBundle.getBoolean("silence");
        this.mIsActive = persistableBundle.getBoolean("active");
        this.mLastStateChange = persistableBundle.getLong("last_state");
    }
    
    boolean saveState(final PersistableBundle persistableBundle) {
        if (this.mIsSilenced) {
            persistableBundle.putBoolean("silence", this.mIsSilenced);
        }
        if (this.mIsActive) {
            persistableBundle.putBoolean("active", this.mIsActive);
            persistableBundle.putLong("last_state", this.mLastStateChange);
        }
        return this.mIsSilenced || this.mIsActive;
    }
    
    protected void setActive(final boolean mIsActive) {
        if (this.mIsActive == mIsActive) {
            return;
        }
        this.mIsActive = mIsActive;
        this.mLastStateChange = System.currentTimeMillis();
        if (this.mIsSilenced && !mIsActive) {
            this.onSilenceChanged(this.mIsSilenced = false);
        }
        this.notifyChanged();
    }
    
    public boolean shouldShow() {
        return this.isActive() && !this.isSilenced();
    }
    
    public void silence() {
        if (!this.mIsSilenced) {
            this.mIsSilenced = true;
            this.mMetricsFeatureProvider.action(this.mManager.getContext(), 372, this.getMetricsConstant());
            this.onSilenceChanged(this.mIsSilenced);
            this.notifyChanged();
        }
    }
}
