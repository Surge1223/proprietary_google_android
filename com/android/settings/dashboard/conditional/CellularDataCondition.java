package com.android.settings.dashboard.conditional;

import android.content.Context;
import android.net.ConnectivityManager;
import android.content.Intent;
import com.android.settings.Settings;
import android.telephony.TelephonyManager;
import android.content.BroadcastReceiver;
import android.graphics.drawable.Drawable;
import android.content.IntentFilter;

public class CellularDataCondition extends Condition
{
    private static final IntentFilter DATA_CONNECTION_FILTER;
    private final Receiver mReceiver;
    
    static {
        DATA_CONNECTION_FILTER = new IntentFilter("android.intent.action.ANY_DATA_STATE");
    }
    
    public CellularDataCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mReceiver = new Receiver();
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887089) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131230991);
    }
    
    @Override
    protected IntentFilter getIntentFilter() {
        return CellularDataCondition.DATA_CONNECTION_FILTER;
    }
    
    @Override
    public int getMetricsConstant() {
        return 380;
    }
    
    @Override
    protected BroadcastReceiver getReceiver() {
        return this.mReceiver;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887074);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887075);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            ((TelephonyManager)this.mManager.getContext().getSystemService((Class)TelephonyManager.class)).setDataEnabled(true);
            this.setActive(false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onPrimaryClick() {
        this.mManager.getContext().startActivity(new Intent(this.mManager.getContext(), (Class)Settings.DataUsageSummaryActivity.class).addFlags(268435456));
    }
    
    @Override
    public void refreshState() {
        final ConnectivityManager connectivityManager = (ConnectivityManager)this.mManager.getContext().getSystemService((Class)ConnectivityManager.class);
        final TelephonyManager telephonyManager = (TelephonyManager)this.mManager.getContext().getSystemService((Class)TelephonyManager.class);
        if (connectivityManager.isNetworkSupported(0) && telephonyManager.getSimState() == 5) {
            this.setActive(telephonyManager.isDataEnabled() ^ true);
            return;
        }
        this.setActive(false);
    }
    
    public static class Receiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            if ("android.intent.action.ANY_DATA_STATE".equals(intent.getAction())) {
                final CellularDataCondition cellularDataCondition = ConditionManager.get(context).getCondition(CellularDataCondition.class);
                if (cellularDataCondition != null) {
                    cellularDataCondition.refreshState();
                }
            }
        }
    }
}
