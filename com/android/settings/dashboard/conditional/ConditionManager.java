package com.android.settings.dashboard.conditional;

import java.util.Collection;
import android.os.AsyncTask;
import org.xmlpull.v1.XmlSerializer;
import java.io.Writer;
import java.io.FileWriter;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.os.PersistableBundle;
import java.io.Reader;
import java.io.FileReader;
import android.util.Xml;
import java.util.Collections;
import java.util.List;
import android.util.Log;
import java.io.File;
import android.content.Context;
import java.util.ArrayList;
import java.util.Comparator;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class ConditionManager implements LifecycleObserver, OnPause, OnResume
{
    private static final Comparator<Condition> CONDITION_COMPARATOR;
    private static ConditionManager sInstance;
    private final ArrayList<Condition> mConditions;
    private final Context mContext;
    private final ArrayList<ConditionListener> mListeners;
    private File mXmlFile;
    
    static {
        CONDITION_COMPARATOR = new Comparator<Condition>() {
            @Override
            public int compare(final Condition condition, final Condition condition2) {
                return Long.compare(condition.getLastChange(), condition2.getLastChange());
            }
        };
    }
    
    private ConditionManager(final Context mContext, final boolean b) {
        this.mListeners = new ArrayList<ConditionListener>();
        this.mContext = mContext;
        this.mConditions = new ArrayList<Condition>();
        if (b) {
            Log.d("ConditionManager", "conditions loading synchronously");
            final ConditionLoader conditionLoader = new ConditionLoader();
            conditionLoader.onPostExecute(conditionLoader.doInBackground(new Void[0]));
        }
        else {
            Log.d("ConditionManager", "conditions loading asychronously");
            new ConditionLoader().execute((Object[])new Void[0]);
        }
    }
    
    private void addIfMissing(final Class<? extends Condition> clazz, final ArrayList<Condition> list) {
        if (this.getCondition(clazz, list) == null) {
            final Condition condition = this.createCondition(clazz);
            if (condition != null) {
                list.add(condition);
            }
        }
    }
    
    private void addMissingConditions(final ArrayList<Condition> list) {
        this.addIfMissing(AirplaneModeCondition.class, list);
        this.addIfMissing(HotspotCondition.class, list);
        this.addIfMissing(DndCondition.class, list);
        this.addIfMissing(BatterySaverCondition.class, list);
        this.addIfMissing(CellularDataCondition.class, list);
        this.addIfMissing(BackgroundDataCondition.class, list);
        this.addIfMissing(WorkModeCondition.class, list);
        this.addIfMissing(NightDisplayCondition.class, list);
        this.addIfMissing(RingerMutedCondition.class, list);
        this.addIfMissing(RingerVibrateCondition.class, list);
        Collections.sort((List<Object>)list, (Comparator<? super Object>)ConditionManager.CONDITION_COMPARATOR);
    }
    
    private Condition createCondition(final Class<?> clazz) {
        if (AirplaneModeCondition.class == clazz) {
            return new AirplaneModeCondition(this);
        }
        if (HotspotCondition.class == clazz) {
            return new HotspotCondition(this);
        }
        if (DndCondition.class == clazz) {
            return new DndCondition(this);
        }
        if (BatterySaverCondition.class == clazz) {
            return new BatterySaverCondition(this);
        }
        if (CellularDataCondition.class == clazz) {
            return new CellularDataCondition(this);
        }
        if (BackgroundDataCondition.class == clazz) {
            return new BackgroundDataCondition(this);
        }
        if (WorkModeCondition.class == clazz) {
            return new WorkModeCondition(this);
        }
        if (NightDisplayCondition.class == clazz) {
            return new NightDisplayCondition(this);
        }
        if (RingerMutedCondition.class == clazz) {
            return new RingerMutedCondition(this);
        }
        if (RingerVibrateCondition.class == clazz) {
            return new RingerVibrateCondition(this);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown condition class: ");
        sb.append(clazz.getSimpleName());
        Log.e("ConditionManager", sb.toString());
        return null;
    }
    
    public static ConditionManager get(final Context context) {
        return get(context, true);
    }
    
    public static ConditionManager get(final Context context, final boolean b) {
        if (ConditionManager.sInstance == null) {
            ConditionManager.sInstance = new ConditionManager(context.getApplicationContext(), b);
        }
        return ConditionManager.sInstance;
    }
    
    private <T extends Condition> T getCondition(final Class<T> clazz, final List<Condition> list) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (clazz.equals(list.get(i).getClass())) {
                return list.get(i);
            }
        }
        return null;
    }
    
    private void readFromXml(final File file, final ArrayList<Condition> list) {
        try {
            final XmlPullParser pullParser = Xml.newPullParser();
            final FileReader input = new FileReader(file);
            pullParser.setInput((Reader)input);
            int n = pullParser.getEventType();
            while (true) {
                Label_0220: {
                    if (n == 1) {
                        break Label_0220;
                    }
                    Label_0209: {
                        if (!"c".equals(pullParser.getName())) {
                            break Label_0209;
                        }
                        final int depth = pullParser.getDepth();
                        String s2;
                        final String s = s2 = pullParser.getAttributeValue("", "cls");
                        if (!s.startsWith("com.android.settings.dashboard.conditional.")) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("com.android.settings.dashboard.conditional.");
                            sb.append(s);
                            s2 = sb.toString();
                        }
                        final Condition condition = this.createCondition(Class.forName(s2));
                        final PersistableBundle restoreFromXml = PersistableBundle.restoreFromXml(pullParser);
                        Label_0188: {
                            if (condition != null) {
                                condition.restoreState(restoreFromXml);
                                list.add(condition);
                                break Label_0188;
                            }
                            try {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("failed to add condition: ");
                                sb2.append(s2);
                                Log.e("ConditionManager", sb2.toString());
                                while (pullParser.getDepth() > depth) {
                                    pullParser.next();
                                }
                                try {
                                    n = pullParser.next();
                                    continue;
                                    try {
                                        input.close();
                                    }
                                    catch (IOException ex) {
                                        Log.w("ConditionManager", "Problem reading condition_state.xml", (Throwable)ex);
                                    }
                                }
                                catch (IOException ex2) {}
                            }
                            catch (IOException ex3) {}
                        }
                    }
                }
            }
        }
        catch (XmlPullParserException ex4) {}
        catch (IOException ex5) {}
        catch (ClassNotFoundException ex6) {}
    }
    
    private void saveToXml() {
        try {
            final XmlSerializer serializer = Xml.newSerializer();
            final FileWriter output = new FileWriter(this.mXmlFile);
            serializer.setOutput((Writer)output);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "cs");
            for (int size = this.mConditions.size(), i = 0; i < size; ++i) {
                final PersistableBundle persistableBundle = new PersistableBundle();
                if (this.mConditions.get(i).saveState(persistableBundle)) {
                    serializer.startTag("", "c");
                    serializer.attribute("", "cls", this.mConditions.get(i).getClass().getSimpleName());
                    persistableBundle.saveToXml(serializer);
                    serializer.endTag("", "c");
                }
            }
            serializer.endTag("", "cs");
            serializer.flush();
            output.close();
        }
        catch (XmlPullParserException | IOException ex) {
            final Throwable t;
            Log.w("ConditionManager", "Problem writing condition_state.xml", t);
        }
    }
    
    public void addListener(final ConditionListener conditionListener) {
        this.mListeners.add(conditionListener);
        conditionListener.onConditionsChanged();
    }
    
    public <T extends Condition> T getCondition(final Class<T> clazz) {
        return this.getCondition(clazz, this.mConditions);
    }
    
    public List<Condition> getConditions() {
        return this.mConditions;
    }
    
    Context getContext() {
        return this.mContext;
    }
    
    public void notifyChanged(final Condition condition) {
        this.saveToXml();
        Collections.sort(this.mConditions, ConditionManager.CONDITION_COMPARATOR);
        for (int size = this.mListeners.size(), i = 0; i < size; ++i) {
            this.mListeners.get(i).onConditionsChanged();
        }
    }
    
    @Override
    public void onPause() {
        for (int i = 0; i < this.mConditions.size(); ++i) {
            this.mConditions.get(i).onPause();
        }
    }
    
    @Override
    public void onResume() {
        for (int i = 0; i < this.mConditions.size(); ++i) {
            this.mConditions.get(i).onResume();
        }
    }
    
    public void refreshAll() {
        for (int size = this.mConditions.size(), i = 0; i < size; ++i) {
            this.mConditions.get(i).refreshState();
        }
    }
    
    public void remListener(final ConditionListener conditionListener) {
        this.mListeners.remove(conditionListener);
    }
    
    public interface ConditionListener
    {
        void onConditionsChanged();
    }
    
    private class ConditionLoader extends AsyncTask<Void, Void, ArrayList<Condition>>
    {
        protected ArrayList<Condition> doInBackground(final Void... array) {
            Log.d("ConditionManager", "loading conditions from xml");
            final ArrayList<Condition> list = new ArrayList<Condition>();
            ConditionManager.this.mXmlFile = new File(ConditionManager.this.mContext.getFilesDir(), "condition_state.xml");
            if (ConditionManager.this.mXmlFile.exists()) {
                ConditionManager.this.readFromXml(ConditionManager.this.mXmlFile, list);
            }
            ConditionManager.this.addMissingConditions(list);
            return list;
        }
        
        protected void onPostExecute(final ArrayList<Condition> list) {
            Log.d("ConditionManager", "conditions loaded from xml, refreshing conditions");
            ConditionManager.this.mConditions.clear();
            ConditionManager.this.mConditions.addAll(list);
            ConditionManager.this.refreshAll();
        }
    }
}
