package com.android.settings.dashboard.conditional;

import android.content.Context;
import com.android.settingslib.WirelessUtils;
import android.util.Log;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.content.BroadcastReceiver;
import android.graphics.drawable.Drawable;
import android.content.IntentFilter;

public class AirplaneModeCondition extends Condition
{
    private static final IntentFilter AIRPLANE_MODE_FILTER;
    public static String TAG;
    private final Receiver mReceiver;
    
    static {
        AirplaneModeCondition.TAG = "APM_Condition";
        AIRPLANE_MODE_FILTER = new IntentFilter("android.intent.action.AIRPLANE_MODE");
    }
    
    public AirplaneModeCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mReceiver = new Receiver();
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887088) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131230948);
    }
    
    @Override
    protected IntentFilter getIntentFilter() {
        return AirplaneModeCondition.AIRPLANE_MODE_FILTER;
    }
    
    @Override
    public int getMetricsConstant() {
        return 377;
    }
    
    @Override
    protected BroadcastReceiver getReceiver() {
        return this.mReceiver;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887068);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887069);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            ConnectivityManager.from(this.mManager.getContext()).setAirplaneMode(false);
            this.setActive(false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onPrimaryClick() {
        this.mManager.getContext().startActivity(new Intent("android.settings.WIRELESS_SETTINGS").addFlags(268435456));
    }
    
    @Override
    public void refreshState() {
        Log.d(AirplaneModeCondition.TAG, "APM condition refreshed");
        this.setActive(WirelessUtils.isAirplaneModeOn(this.mManager.getContext()));
    }
    
    @Override
    protected void setActive(final boolean active) {
        super.setActive(active);
        final String tag = AirplaneModeCondition.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("setActive was called with ");
        sb.append(active);
        Log.d(tag, sb.toString());
    }
    
    public static class Receiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            if ("android.intent.action.AIRPLANE_MODE".equals(intent.getAction())) {
                ConditionManager.get(context).getCondition(AirplaneModeCondition.class).refreshState();
            }
        }
    }
}
