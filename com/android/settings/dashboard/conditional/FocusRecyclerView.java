package com.android.settings.dashboard.conditional;

import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

public class FocusRecyclerView extends RecyclerView
{
    private FocusListener mListener;
    
    public FocusRecyclerView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        if (this.mListener != null) {
            this.mListener.onWindowFocusChanged(b);
        }
    }
    
    public void setListener(final FocusListener mListener) {
        this.mListener = mListener;
    }
    
    public interface FocusListener
    {
        void onWindowFocusChanged(final boolean p0);
    }
}
