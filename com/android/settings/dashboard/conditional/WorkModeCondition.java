package com.android.settings.dashboard.conditional;

import android.content.Intent;
import com.android.settings.Settings;
import android.graphics.drawable.Drawable;
import java.util.List;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import android.os.UserManager;

public class WorkModeCondition extends Condition
{
    private UserManager mUm;
    private UserHandle mUserHandle;
    
    public WorkModeCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mUm = (UserManager)this.mManager.getContext().getSystemService("user");
    }
    
    private void updateUserHandle() {
        final List profiles = this.mUm.getProfiles(UserHandle.myUserId());
        final int size = profiles.size();
        this.mUserHandle = null;
        for (int i = 0; i < size; ++i) {
            final UserInfo userInfo = profiles.get(i);
            if (userInfo.isManagedProfile()) {
                this.mUserHandle = userInfo.getUserHandle();
                break;
            }
        }
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887089) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131231152);
    }
    
    @Override
    public int getMetricsConstant() {
        return 383;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887090);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887091);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            if (this.mUserHandle != null) {
                this.mUm.requestQuietModeEnabled(false, this.mUserHandle);
            }
            this.setActive(false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onPrimaryClick() {
        this.mManager.getContext().startActivity(new Intent(this.mManager.getContext(), (Class)Settings.AccountDashboardActivity.class).addFlags(268435456));
    }
    
    @Override
    public void refreshState() {
        this.updateUserHandle();
        this.setActive(this.mUserHandle != null && this.mUm.isQuietModeEnabled(this.mUserHandle));
    }
}
