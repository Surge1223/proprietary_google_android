package com.android.settings.dashboard.conditional;

import android.graphics.drawable.Drawable;

public class RingerVibrateCondition extends AbnormalRingerConditionBase
{
    RingerVibrateCondition(final ConditionManager conditionManager) {
        super(conditionManager);
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131231189);
    }
    
    @Override
    public int getMetricsConstant() {
        return 1369;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getText(2131887079);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getText(2131887080);
    }
    
    @Override
    public void refreshState() {
        final int ringerModeInternal = this.mAudioManager.getRingerModeInternal();
        boolean active = true;
        if (ringerModeInternal != 1) {
            active = false;
        }
        this.setActive(active);
    }
}
