package com.android.settings.dashboard.conditional;

import android.graphics.drawable.Drawable;
import android.app.NotificationManager;

public class RingerMutedCondition extends AbnormalRingerConditionBase
{
    private final NotificationManager mNotificationManager;
    
    RingerMutedCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mNotificationManager = (NotificationManager)this.mManager.getContext().getSystemService("notification");
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131231079);
    }
    
    @Override
    public int getMetricsConstant() {
        return 1368;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getText(2131887077);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getText(2131887078);
    }
    
    @Override
    public void refreshState() {
        int zenMode = 0;
        if (this.mNotificationManager != null) {
            zenMode = this.mNotificationManager.getZenMode();
        }
        final boolean b = false;
        final boolean b2 = zenMode != 0;
        final boolean b3 = this.mAudioManager.getRingerModeInternal() == 0;
        boolean active = b;
        if (b3) {
            active = b;
            if (!b2) {
                active = true;
            }
        }
        this.setActive(active);
    }
}
