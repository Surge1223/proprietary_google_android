package com.android.settings.dashboard.conditional;

import android.os.PowerManager;
import com.android.settings.fuelgauge.batterysaver.BatterySaverSettings;
import com.android.settings.core.SubSettingLauncher;
import com.android.settingslib.fuelgauge.BatterySaverUtils;
import android.graphics.drawable.Drawable;
import com.android.settings.fuelgauge.BatterySaverReceiver;

public class BatterySaverCondition extends Condition implements BatterySaverListener
{
    private final BatterySaverReceiver mReceiver;
    
    public BatterySaverCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        (this.mReceiver = new BatterySaverReceiver(conditionManager.getContext())).setBatterySaverListener((BatterySaverReceiver.BatterySaverListener)this);
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887088) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131230975);
    }
    
    @Override
    public int getMetricsConstant() {
        return 379;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887070);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887071);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            BatterySaverUtils.setPowerSaveMode(this.mManager.getContext(), false, false);
            this.refreshState();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onBatteryChanged(final boolean b) {
    }
    
    @Override
    public void onPause() {
        this.mReceiver.setListening(false);
    }
    
    @Override
    public void onPowerSaveModeChanged() {
        ConditionManager.get(this.mManager.getContext()).getCondition(BatterySaverCondition.class).refreshState();
    }
    
    @Override
    public void onPrimaryClick() {
        new SubSettingLauncher(this.mManager.getContext()).setDestination(BatterySaverSettings.class.getName()).setSourceMetricsCategory(35).setTitle(2131886617).addFlags(268435456).launch();
    }
    
    @Override
    public void onResume() {
        this.mReceiver.setListening(true);
    }
    
    @Override
    public void refreshState() {
        this.setActive(((PowerManager)this.mManager.getContext().getSystemService((Class)PowerManager.class)).isPowerSaveMode());
    }
}
