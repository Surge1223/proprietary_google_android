package com.android.settings.dashboard.conditional;

import java.io.Serializable;
import android.content.Intent;
import com.android.settings.Settings;
import android.util.FeatureFlagUtils;
import android.net.NetworkPolicyManager;
import android.graphics.drawable.Drawable;

public class BackgroundDataCondition extends Condition
{
    public BackgroundDataCondition(final ConditionManager conditionManager) {
        super(conditionManager);
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887088) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131230998);
    }
    
    @Override
    public int getMetricsConstant() {
        return 378;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mManager.getContext().getString(2131887072);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887073);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            NetworkPolicyManager.from(this.mManager.getContext()).setRestrictBackground(false);
            this.setActive(false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onPrimaryClick() {
        Serializable s;
        if (FeatureFlagUtils.isEnabled(this.mManager.getContext(), "settings_data_usage_v2")) {
            s = Settings.DataUsageSummaryActivity.class;
        }
        else {
            s = Settings.DataUsageSummaryLegacyActivity.class;
        }
        this.mManager.getContext().startActivity(new Intent(this.mManager.getContext(), (Class)s).addFlags(268435456));
    }
    
    @Override
    public void refreshState() {
        this.setActive(NetworkPolicyManager.from(this.mManager.getContext()).getRestrictBackground());
    }
}
