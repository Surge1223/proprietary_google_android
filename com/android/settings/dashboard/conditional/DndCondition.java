package com.android.settings.dashboard.conditional;

import android.content.Intent;
import android.content.Context;
import android.os.PersistableBundle;
import com.android.settings.notification.ZenModeSettings;
import com.android.settings.core.SubSettingLauncher;
import android.net.Uri;
import android.app.NotificationManager;
import android.graphics.drawable.Drawable;
import android.content.BroadcastReceiver;
import android.service.notification.ZenModeConfig;
import android.content.IntentFilter;

public class DndCondition extends Condition
{
    static final IntentFilter DND_FILTER;
    protected ZenModeConfig mConfig;
    private final Receiver mReceiver;
    private boolean mRegistered;
    private int mZen;
    
    static {
        DND_FILTER = new IntentFilter("android.app.action.INTERRUPTION_FILTER_CHANGED_INTERNAL");
    }
    
    public DndCondition(final ConditionManager conditionManager) {
        super(conditionManager);
        this.mReceiver = new Receiver();
        this.mManager.getContext().registerReceiver((BroadcastReceiver)this.mReceiver, DndCondition.DND_FILTER);
        this.mRegistered = true;
    }
    
    @Override
    public CharSequence[] getActions() {
        return new CharSequence[] { this.mManager.getContext().getString(2131887088) };
    }
    
    @Override
    public Drawable getIcon() {
        return this.mManager.getContext().getDrawable(2131231002);
    }
    
    @Override
    public int getMetricsConstant() {
        return 381;
    }
    
    @Override
    public CharSequence getSummary() {
        return ZenModeConfig.getDescription(this.mManager.getContext(), this.mZen != 0, this.mConfig, true);
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mManager.getContext().getString(2131887092);
    }
    
    @Override
    public void onActionClick(final int n) {
        if (n == 0) {
            ((NotificationManager)this.mManager.getContext().getSystemService((Class)NotificationManager.class)).setZenMode(0, (Uri)null, "DndCondition");
            this.setActive(false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected index ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void onPause() {
        if (this.mRegistered) {
            this.mManager.getContext().unregisterReceiver((BroadcastReceiver)this.mReceiver);
            this.mRegistered = false;
        }
    }
    
    @Override
    public void onPrimaryClick() {
        new SubSettingLauncher(this.mManager.getContext()).setDestination(ZenModeSettings.class.getName()).setSourceMetricsCategory(35).setTitle(2131890376).addFlags(268435456).launch();
    }
    
    @Override
    public void onResume() {
        if (!this.mRegistered) {
            this.mManager.getContext().registerReceiver((BroadcastReceiver)this.mReceiver, DndCondition.DND_FILTER);
            this.mRegistered = true;
        }
    }
    
    @Override
    public void refreshState() {
        final NotificationManager notificationManager = (NotificationManager)this.mManager.getContext().getSystemService((Class)NotificationManager.class);
        this.mZen = notificationManager.getZenMode();
        final boolean active = this.mZen != 0;
        if (active) {
            this.mConfig = notificationManager.getZenModeConfig();
        }
        else {
            this.mConfig = null;
        }
        this.setActive(active);
    }
    
    @Override
    void restoreState(final PersistableBundle persistableBundle) {
        super.restoreState(persistableBundle);
        this.mZen = persistableBundle.getInt("state", 0);
    }
    
    @Override
    boolean saveState(final PersistableBundle persistableBundle) {
        persistableBundle.putInt("state", this.mZen);
        return super.saveState(persistableBundle);
    }
    
    public static class Receiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            if ("android.app.action.INTERRUPTION_FILTER_CHANGED_INTERNAL".equals(intent.getAction())) {
                final DndCondition condition = ConditionManager.get(context).getCondition(DndCondition.class);
                if (condition != null) {
                    condition.refreshState();
                }
            }
        }
    }
}
