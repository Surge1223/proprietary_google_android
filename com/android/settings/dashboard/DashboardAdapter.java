package com.android.settings.dashboard;

import android.widget.TextView;
import android.widget.LinearLayout;
import android.os.Parcelable;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.res.Resources.Theme;
import android.text.TextUtils;
import android.service.settings.suggestions.Suggestion;
import android.support.v7.widget.LinearLayoutManager;
import com.android.settings.dashboard.conditional.ConditionAdapter;
import android.support.v7.util.DiffUtil;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import com.android.settingslib.drawer.DashboardCategory;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.drawer.Tile;
import android.app.Activity;
import android.view.View;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settings.dashboard.conditional.Condition;
import java.util.List;
import android.os.Bundle;
import android.view.View.OnClickListener;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import com.android.settingslib.utils.IconCache;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.dashboard.suggestions.SuggestionAdapter;
import android.support.v7.widget.RecyclerView;

public class DashboardAdapter extends Adapter<DashboardItemHolder> implements SummaryConsumer, Callback, LifecycleObserver, OnSaveInstanceState
{
    static final String STATE_CONDITION_EXPANDED = "condition_expanded";
    private final IconCache mCache;
    private final Context mContext;
    DashboardData mDashboardData;
    private final DashboardFeatureProvider mDashboardFeatureProvider;
    private boolean mFirstFrameDrawn;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private RecyclerView mRecyclerView;
    private SuggestionAdapter mSuggestionAdapter;
    private View.OnClickListener mTileClickListener;
    
    public DashboardAdapter(final Context mContext, final Bundle bundle, final List<Condition> conditions, final SuggestionControllerMixin suggestionControllerMixin, final Lifecycle lifecycle) {
        this.mTileClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                DashboardAdapter.this.mDashboardFeatureProvider.openTileIntent((Activity)DashboardAdapter.this.mContext, (Tile)view.getTag());
            }
        };
        final DashboardCategory dashboardCategory = null;
        boolean boolean1 = false;
        this.mContext = mContext;
        final FeatureFactory factory = FeatureFactory.getFactory(mContext);
        this.mMetricsFeatureProvider = factory.getMetricsFeatureProvider();
        this.mDashboardFeatureProvider = factory.getDashboardFeatureProvider(mContext);
        this.mCache = new IconCache(mContext);
        this.mSuggestionAdapter = new SuggestionAdapter(this.mContext, suggestionControllerMixin, bundle, (SuggestionAdapter.Callback)this, lifecycle);
        ((RecyclerView.Adapter)this).setHasStableIds(true);
        DashboardCategory category = dashboardCategory;
        if (bundle != null) {
            category = (DashboardCategory)bundle.getParcelable("category_list");
            boolean1 = bundle.getBoolean("condition_expanded", false);
        }
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
        this.mDashboardData = new DashboardData.Builder().setConditions(conditions).setSuggestions(this.mSuggestionAdapter.getSuggestions()).setCategory(category).setConditionExpanded(boolean1).build();
    }
    
    private void scrollToTopOfConditions() {
        this.mRecyclerView.scrollToPosition(this.mDashboardData.hasSuggestion() ? 1 : 0);
    }
    
    private void updateConditionIcons(final List<Drawable> list, final ViewGroup viewGroup) {
        if (list != null && list.size() >= 2) {
            final LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
            viewGroup.removeAllViews();
            for (int i = 1; i < list.size(); ++i) {
                final ImageView imageView = (ImageView)from.inflate(2131558493, viewGroup, false);
                imageView.setImageDrawable((Drawable)list.get(i));
                viewGroup.addView((View)imageView);
            }
            viewGroup.setVisibility(0);
            return;
        }
        viewGroup.setVisibility(4);
    }
    
    @Override
    public int getItemCount() {
        return this.mDashboardData.size();
    }
    
    @Override
    public long getItemId(final int n) {
        return this.mDashboardData.getItemIdByPosition(n);
    }
    
    @Override
    public int getItemViewType(final int n) {
        return this.mDashboardData.getItemTypeByPosition(n);
    }
    
    void notifyDashboardDataChanged(final DashboardData dashboardData) {
        if (this.mFirstFrameDrawn && dashboardData != null) {
            DiffUtil.calculateDiff((DiffUtil.Callback)new DashboardData.ItemsDataDiffCallback(dashboardData.getItemList(), this.mDashboardData.getItemList())).dispatchUpdatesTo(this);
        }
        else {
            this.mFirstFrameDrawn = true;
            ((RecyclerView.Adapter)this).notifyDataSetChanged();
        }
    }
    
    @Override
    public void notifySummaryChanged(final Tile tile) {
        final int positionByTile = this.mDashboardData.getPositionByTile(tile);
        if (positionByTile != -1) {
            ((RecyclerView.Adapter)this).notifyItemChanged(positionByTile, this.mDashboardData.getItemTypeByPosition(positionByTile));
        }
    }
    
    @Override
    public void onAttachedToRecyclerView(final RecyclerView mRecyclerView) {
        super.onAttachedToRecyclerView(mRecyclerView);
        this.mRecyclerView = mRecyclerView;
    }
    
    void onBindCondition(final ConditionContainerHolder conditionContainerHolder, final int n) {
        final ConditionAdapter adapter = new ConditionAdapter(this.mContext, (List<Condition>)this.mDashboardData.getItemEntityByPosition(n), this.mDashboardData.isConditionExpanded());
        adapter.addDismissHandling(conditionContainerHolder.data);
        conditionContainerHolder.data.setAdapter((RecyclerView.Adapter)adapter);
        conditionContainerHolder.data.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(this.mContext));
    }
    
    void onBindConditionHeader(final ConditionHeaderHolder conditionHeaderHolder, final DashboardData.ConditionHeaderData conditionHeaderData) {
        conditionHeaderHolder.icon.setImageDrawable((Drawable)conditionHeaderData.conditionIcons.get(0));
        if (conditionHeaderData.conditionCount == 1) {
            conditionHeaderHolder.title.setText(conditionHeaderData.title);
            conditionHeaderHolder.summary.setText((CharSequence)null);
            conditionHeaderHolder.icons.setVisibility(4);
        }
        else {
            conditionHeaderHolder.title.setText((CharSequence)null);
            conditionHeaderHolder.summary.setText((CharSequence)this.mContext.getString(2131887087, new Object[] { conditionHeaderData.conditionCount }));
            this.updateConditionIcons(conditionHeaderData.conditionIcons, (ViewGroup)conditionHeaderHolder.icons);
            conditionHeaderHolder.icons.setVisibility(0);
        }
        conditionHeaderHolder.itemView.setOnClickListener((View.OnClickListener)new _$$Lambda$DashboardAdapter$rjSGP6Cnddhw6FvR740SjWmziQ4(this));
    }
    
    void onBindSuggestion(final SuggestionContainerHolder suggestionContainerHolder, final int n) {
        final List suggestions = (List)this.mDashboardData.getItemEntityByPosition(n);
        if (suggestions != null && suggestions.size() > 0) {
            this.mSuggestionAdapter.setSuggestions(suggestions);
            suggestionContainerHolder.data.setAdapter((RecyclerView.Adapter)this.mSuggestionAdapter);
        }
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this.mContext);
        layoutManager.setOrientation(0);
        suggestionContainerHolder.data.setLayoutManager((RecyclerView.LayoutManager)layoutManager);
    }
    
    void onBindTile(final DashboardItemHolder dashboardItemHolder, final Tile tile) {
        Object icon;
        final Drawable drawable = (Drawable)(icon = this.mCache.getIcon(tile.icon));
        if (!TextUtils.equals((CharSequence)tile.icon.getResPackage(), (CharSequence)this.mContext.getPackageName())) {
            icon = drawable;
            if (!(drawable instanceof RoundedHomepageIcon)) {
                icon = new RoundedHomepageIcon(this.mContext, drawable);
                try {
                    if (tile.metaData != null) {
                        final int int1 = tile.metaData.getInt("com.android.settings.bg.hint", 0);
                        if (int1 != 0) {
                            ((RoundedHomepageIcon)icon).setBackgroundColor(this.mContext.getPackageManager().getResourcesForApplication(tile.icon.getResPackage()).getColor(int1, (Resources.Theme)null));
                        }
                    }
                }
                catch (PackageManager$NameNotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to set background color for ");
                    sb.append(tile.intent.getPackage());
                    Log.e("DashboardAdapter", sb.toString());
                }
                this.mCache.updateIcon(tile.icon, (Drawable)icon);
            }
        }
        dashboardItemHolder.icon.setImageDrawable((Drawable)icon);
        dashboardItemHolder.title.setText(tile.title);
        if (!TextUtils.isEmpty(tile.summary)) {
            dashboardItemHolder.summary.setText(tile.summary);
            dashboardItemHolder.summary.setVisibility(0);
        }
        else {
            dashboardItemHolder.summary.setVisibility(8);
        }
    }
    
    public void onBindViewHolder(final DashboardItemHolder dashboardItemHolder, final int n) {
        final int itemTypeByPosition = this.mDashboardData.getItemTypeByPosition(n);
        if (itemTypeByPosition != 2131558523) {
            if (itemTypeByPosition != 2131558775) {
                switch (itemTypeByPosition) {
                    case 2131558492: {
                        this.onBindConditionHeader((ConditionHeaderHolder)dashboardItemHolder, (DashboardData.ConditionHeaderData)this.mDashboardData.getItemEntityByPosition(n));
                        break;
                    }
                    case 2131558491: {
                        dashboardItemHolder.itemView.setOnClickListener((View.OnClickListener)new _$$Lambda$DashboardAdapter$EPKC8_9XatJQPDMMI1s2dy8ZY8c(this));
                        break;
                    }
                    case 2131558490: {
                        this.onBindCondition((ConditionContainerHolder)dashboardItemHolder, n);
                        break;
                    }
                }
            }
            else {
                this.onBindSuggestion((SuggestionContainerHolder)dashboardItemHolder, n);
            }
        }
        else {
            final Tile tag = (Tile)this.mDashboardData.getItemEntityByPosition(n);
            this.onBindTile(dashboardItemHolder, tag);
            dashboardItemHolder.itemView.setTag((Object)tag);
            dashboardItemHolder.itemView.setOnClickListener(this.mTileClickListener);
        }
    }
    
    public DashboardItemHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        final View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(n, viewGroup, false);
        if (n == 2131558492) {
            return (DashboardItemHolder)new ConditionHeaderHolder(inflate);
        }
        if (n == 2131558490) {
            return (DashboardItemHolder)new ConditionContainerHolder(inflate);
        }
        if (n == 2131558775) {
            return (DashboardItemHolder)new SuggestionContainerHolder(inflate);
        }
        return new DashboardItemHolder(inflate);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        final DashboardCategory category = this.mDashboardData.getCategory();
        if (category != null) {
            bundle.putParcelable("category_list", (Parcelable)category);
        }
        bundle.putBoolean("condition_expanded", this.mDashboardData.isConditionExpanded());
    }
    
    @Override
    public void onSuggestionClosed(final Suggestion suggestion) {
        final List<Suggestion> suggestions = this.mDashboardData.getSuggestions();
        if (suggestions != null && suggestions.size() != 0) {
            if (suggestions.size() == 1) {
                this.setSuggestions(null);
            }
            else {
                suggestions.remove(suggestion);
                this.setSuggestions(suggestions);
            }
        }
    }
    
    public void setCategory(final DashboardCategory category) {
        final DashboardData mDashboardData = this.mDashboardData;
        Log.d("DashboardAdapter", "adapter setCategory called");
        this.mDashboardData = new DashboardData.Builder(mDashboardData).setCategory(category).build();
        this.notifyDashboardDataChanged(mDashboardData);
    }
    
    public void setConditions(final List<Condition> conditions) {
        final DashboardData mDashboardData = this.mDashboardData;
        Log.d("DashboardAdapter", "adapter setConditions called");
        this.mDashboardData = new DashboardData.Builder(mDashboardData).setConditions(conditions).build();
        this.notifyDashboardDataChanged(mDashboardData);
    }
    
    public void setSuggestions(final List<Suggestion> suggestions) {
        final DashboardData mDashboardData = this.mDashboardData;
        this.mDashboardData = new DashboardData.Builder(mDashboardData).setSuggestions(suggestions).build();
        this.notifyDashboardDataChanged(mDashboardData);
    }
    
    public static class ConditionContainerHolder extends DashboardItemHolder
    {
        public final RecyclerView data;
        
        public ConditionContainerHolder(final View view) {
            super(view);
            this.data = (RecyclerView)view.findViewById(2131362038);
        }
    }
    
    public static class ConditionHeaderHolder extends DashboardItemHolder
    {
        public final ImageView expandIndicator;
        public final LinearLayout icons;
        
        public ConditionHeaderHolder(final View view) {
            super(view);
            this.icons = (LinearLayout)view.findViewById(2131361841);
            this.expandIndicator = (ImageView)view.findViewById(2131362137);
        }
    }
    
    public static class DashboardItemHolder extends ViewHolder
    {
        public final ImageView icon;
        public final TextView summary;
        public final TextView title;
        
        public DashboardItemHolder(final View view) {
            super(view);
            this.icon = (ImageView)view.findViewById(16908294);
            this.title = (TextView)view.findViewById(16908310);
            this.summary = (TextView)view.findViewById(16908304);
        }
    }
    
    public static class SuggestionContainerHolder extends DashboardItemHolder
    {
        public final RecyclerView data;
        
        public SuggestionContainerHolder(final View view) {
            super(view);
            this.data = (RecyclerView)view.findViewById(2131362667);
        }
    }
}
