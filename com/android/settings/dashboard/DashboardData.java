package com.android.settings.dashboard;

import android.support.v7.util.DiffUtil;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import java.util.Objects;
import com.android.settingslib.drawer.Tile;
import java.util.ArrayList;
import android.service.settings.suggestions.Suggestion;
import com.android.settings.dashboard.conditional.Condition;
import java.util.List;
import com.android.settingslib.drawer.DashboardCategory;

public class DashboardData
{
    static final int STABLE_ID_CONDITION_CONTAINER = 4;
    static final int STABLE_ID_CONDITION_FOOTER = 3;
    static final int STABLE_ID_CONDITION_HEADER = 2;
    static final int STABLE_ID_SUGGESTION_CONTAINER = 0;
    private final DashboardCategory mCategory;
    private final boolean mConditionExpanded;
    private final List<Condition> mConditions;
    private final List<Item> mItems;
    private final List<Suggestion> mSuggestions;
    
    private DashboardData(final Builder builder) {
        this.mCategory = builder.mCategory;
        this.mConditions = builder.mConditions;
        this.mSuggestions = builder.mSuggestions;
        this.mConditionExpanded = builder.mConditionExpanded;
        this.mItems = new ArrayList<Item>();
        this.buildItemsData();
    }
    
    private void addToItemList(final Object o, final int n, final int n2, final boolean b) {
        if (b) {
            this.mItems.add(new Item(o, n, n2));
        }
    }
    
    private void buildItemsData() {
        final List<Condition> conditionsToShow = this.getConditionsToShow(this.mConditions);
        final boolean b = sizeOf(conditionsToShow) > 0;
        final List<Suggestion> suggestionsToShow = this.getSuggestionsToShow(this.mSuggestions);
        final boolean b2 = sizeOf(suggestionsToShow) > 0;
        this.addToItemList(suggestionsToShow, 2131558775, 0, b2);
        this.addToItemList(null, 2131558589, 1, b2 && b);
        this.addToItemList(new ConditionHeaderData(conditionsToShow), 2131558492, 2, b && !this.mConditionExpanded);
        this.addToItemList(conditionsToShow, 2131558490, 4, b && this.mConditionExpanded);
        this.addToItemList(null, 2131558491, 3, b && this.mConditionExpanded);
        if (this.mCategory != null) {
            final List<Tile> tiles = this.mCategory.getTiles();
            for (int i = 0; i < tiles.size(); ++i) {
                final Tile tile = tiles.get(i);
                this.addToItemList(tile, 2131558523, Objects.hash(tile.title), true);
            }
        }
    }
    
    private List<Condition> getConditionsToShow(final List<Condition> list) {
        if (list == null) {
            return null;
        }
        final ArrayList<Condition> list2 = new ArrayList<Condition>();
        int i = 0;
        int size;
        if (list == null) {
            size = 0;
        }
        else {
            size = list.size();
        }
        while (i < size) {
            final Condition condition = list.get(i);
            if (condition.shouldShow()) {
                list2.add(condition);
            }
            ++i;
        }
        return list2;
    }
    
    private List<Suggestion> getSuggestionsToShow(final List<Suggestion> list) {
        if (list == null) {
            return null;
        }
        if (list.size() <= 2) {
            return list;
        }
        final ArrayList<Suggestion> list2 = new ArrayList<Suggestion>(2);
        for (int i = 0; i < 2; ++i) {
            list2.add(list.get(i));
        }
        return list2;
    }
    
    private static int sizeOf(final List<?> list) {
        int size;
        if (list == null) {
            size = 0;
        }
        else {
            size = list.size();
        }
        return size;
    }
    
    public DashboardCategory getCategory() {
        return this.mCategory;
    }
    
    public Object getItemEntityByPosition(final int n) {
        return this.mItems.get(n).entity;
    }
    
    public int getItemIdByPosition(final int n) {
        return this.mItems.get(n).id;
    }
    
    public List<Item> getItemList() {
        return this.mItems;
    }
    
    public int getItemTypeByPosition(final int n) {
        return this.mItems.get(n).type;
    }
    
    public int getPositionByTile(final Tile tile) {
        for (int size = this.mItems.size(), i = 0; i < size; ++i) {
            final Object entity = this.mItems.get(i).entity;
            if (entity == tile) {
                return i;
            }
            if (entity instanceof Tile && tile.title.equals(((Tile)entity).title)) {
                return i;
            }
        }
        return -1;
    }
    
    public List<Suggestion> getSuggestions() {
        return this.mSuggestions;
    }
    
    public boolean hasSuggestion() {
        return sizeOf(this.mSuggestions) > 0;
    }
    
    public boolean isConditionExpanded() {
        return this.mConditionExpanded;
    }
    
    public int size() {
        return this.mItems.size();
    }
    
    public static class Builder
    {
        private DashboardCategory mCategory;
        private boolean mConditionExpanded;
        private List<Condition> mConditions;
        private List<Suggestion> mSuggestions;
        
        public Builder() {
        }
        
        public Builder(final DashboardData dashboardData) {
            this.mCategory = dashboardData.mCategory;
            this.mConditions = dashboardData.mConditions;
            this.mSuggestions = dashboardData.mSuggestions;
            this.mConditionExpanded = dashboardData.mConditionExpanded;
        }
        
        public DashboardData build() {
            return new DashboardData(this, null);
        }
        
        public Builder setCategory(final DashboardCategory mCategory) {
            this.mCategory = mCategory;
            return this;
        }
        
        public Builder setConditionExpanded(final boolean mConditionExpanded) {
            this.mConditionExpanded = mConditionExpanded;
            return this;
        }
        
        public Builder setConditions(final List<Condition> mConditions) {
            this.mConditions = mConditions;
            return this;
        }
        
        public Builder setSuggestions(final List<Suggestion> mSuggestions) {
            this.mSuggestions = mSuggestions;
            return this;
        }
    }
    
    public static class ConditionHeaderData
    {
        public final int conditionCount;
        public final List<Drawable> conditionIcons;
        public final CharSequence title;
        
        public ConditionHeaderData(final List<Condition> list) {
            this.conditionCount = sizeOf(list);
            final int conditionCount = this.conditionCount;
            int n = 0;
            CharSequence title;
            if (conditionCount > 0) {
                title = list.get(0).getTitle();
            }
            else {
                title = null;
            }
            this.title = title;
            this.conditionIcons = new ArrayList<Drawable>();
            while (list != null && n < list.size()) {
                this.conditionIcons.add(list.get(n).getIcon());
                ++n;
            }
        }
    }
    
    static class Item
    {
        public final Object entity;
        public final int id;
        public final int type;
        
        public Item(final Object entity, final int type, final int id) {
            this.entity = entity;
            this.type = type;
            this.id = id;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = true;
            boolean b2 = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Item)) {
                return false;
            }
            final Item item = (Item)o;
            if (this.type == item.type && this.id == item.id) {
                final int type = this.type;
                Label_0184: {
                    if (type != 2131558490) {
                        if (type == 2131558523) {
                            final Tile tile = (Tile)this.entity;
                            final Tile tile2 = (Tile)item.entity;
                            if (!TextUtils.equals(tile.title, tile2.title) || !TextUtils.equals(tile.summary, tile2.summary)) {
                                b2 = false;
                            }
                            return b2;
                        }
                        if (type != 2131558775) {
                            break Label_0184;
                        }
                    }
                    final List list = (List)this.entity;
                    if (!list.isEmpty()) {
                        final Tile value = list.get(0);
                        if (value instanceof Tile && value.remoteViews != null) {
                            return false;
                        }
                    }
                }
                boolean equals;
                if (this.entity == null) {
                    equals = (item.entity == null && b);
                }
                else {
                    equals = this.entity.equals(item.entity);
                }
                return equals;
            }
            return false;
        }
    }
    
    public static class ItemsDataDiffCallback extends Callback
    {
        private final List<Item> mNewItems;
        private final List<Item> mOldItems;
        
        public ItemsDataDiffCallback(final List<Item> mOldItems, final List<Item> mNewItems) {
            this.mOldItems = mOldItems;
            this.mNewItems = mNewItems;
        }
        
        @Override
        public boolean areContentsTheSame(final int n, final int n2) {
            return this.mOldItems.get(n).equals(this.mNewItems.get(n2));
        }
        
        @Override
        public boolean areItemsTheSame(final int n, final int n2) {
            return this.mOldItems.get(n).id == this.mNewItems.get(n2).id;
        }
        
        @Override
        public int getNewListSize() {
            return this.mNewItems.size();
        }
        
        @Override
        public int getOldListSize() {
            return this.mOldItems.size();
        }
    }
}
