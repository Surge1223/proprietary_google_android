package com.android.settings.dashboard;

import android.view.View;
import android.content.DialogInterface$OnDismissListener;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.UserHandle;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.content.BroadcastReceiver;
import android.content.RestrictionsManager;
import com.android.settingslib.RestrictedLockUtils;
import android.widget.TextView;
import android.app.AlertDialog;

public abstract class RestrictedDashboardFragment extends DashboardFragment
{
    private AlertDialog mActionDisabledDialog;
    private boolean mChallengeRequested;
    private boolean mChallengeSucceeded;
    private TextView mEmptyTextView;
    private RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    private boolean mIsAdminUser;
    private boolean mOnlyAvailableForAdmins;
    private final String mRestrictionKey;
    private RestrictionsManager mRestrictionsManager;
    private BroadcastReceiver mScreenOffReceiver;
    private UserManager mUserManager;
    
    public RestrictedDashboardFragment(final String mRestrictionKey) {
        this.mOnlyAvailableForAdmins = false;
        this.mScreenOffReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (!RestrictedDashboardFragment.this.mChallengeRequested) {
                    RestrictedDashboardFragment.this.mChallengeSucceeded = false;
                    RestrictedDashboardFragment.this.mChallengeRequested = false;
                }
            }
        };
        this.mRestrictionKey = mRestrictionKey;
    }
    
    private void ensurePin() {
        if (!this.mChallengeSucceeded && !this.mChallengeRequested && this.mRestrictionsManager.hasRestrictionsProvider()) {
            final Intent localApprovalIntent = this.mRestrictionsManager.createLocalApprovalIntent();
            if (localApprovalIntent != null) {
                this.mChallengeRequested = true;
                this.mChallengeSucceeded = false;
                final PersistableBundle persistableBundle = new PersistableBundle();
                persistableBundle.putString("android.request.mesg", this.getResources().getString(2131888828));
                localApprovalIntent.putExtra("android.content.extra.REQUEST_BUNDLE", (Parcelable)persistableBundle);
                this.startActivityForResult(localApprovalIntent, 12309);
            }
        }
    }
    
    public TextView getEmptyTextView() {
        return this.mEmptyTextView;
    }
    
    public RestrictedLockUtils.EnforcedAdmin getRestrictionEnforcedAdmin() {
        this.mEnforcedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.getActivity(), this.mRestrictionKey, UserHandle.myUserId());
        if (this.mEnforcedAdmin != null && this.mEnforcedAdmin.userId == -10000) {
            this.mEnforcedAdmin.userId = UserHandle.myUserId();
        }
        return this.mEnforcedAdmin;
    }
    
    protected boolean hasChallengeSucceeded() {
        return (this.mChallengeRequested && this.mChallengeSucceeded) || !this.mChallengeRequested;
    }
    
    protected TextView initEmptyTextView() {
        return (TextView)this.getActivity().findViewById(16908292);
    }
    
    protected boolean isRestrictedAndNotProviderProtected() {
        final String mRestrictionKey = this.mRestrictionKey;
        final boolean b = false;
        if (mRestrictionKey != null && !"restrict_if_overridable".equals(this.mRestrictionKey)) {
            boolean b2 = b;
            if (this.mUserManager.hasUserRestriction(this.mRestrictionKey)) {
                b2 = b;
                if (!this.mRestrictionsManager.hasRestrictionsProvider()) {
                    b2 = true;
                }
            }
            return b2;
        }
        return false;
    }
    
    protected boolean isUiRestricted() {
        return this.isRestrictedAndNotProviderProtected() || !this.hasChallengeSucceeded() || (!this.mIsAdminUser && this.mOnlyAvailableForAdmins);
    }
    
    protected boolean isUiRestrictedByOnlyAdmin() {
        return this.isUiRestricted() && !this.mUserManager.hasBaseUserRestriction(this.mRestrictionKey, UserHandle.of(UserHandle.myUserId())) && (this.mIsAdminUser || !this.mOnlyAvailableForAdmins);
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.mEmptyTextView = this.initEmptyTextView();
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 12309) {
            if (n2 == -1) {
                this.mChallengeSucceeded = true;
                this.mChallengeRequested = false;
            }
            else {
                this.mChallengeSucceeded = false;
            }
            return;
        }
        super.onActivityResult(n, n2, intent);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mRestrictionsManager = (RestrictionsManager)this.getSystemService("restrictions");
        this.mUserManager = (UserManager)this.getSystemService("user");
        this.mIsAdminUser = this.mUserManager.isAdminUser();
        if (bundle != null) {
            this.mChallengeSucceeded = bundle.getBoolean("chsc", false);
            this.mChallengeRequested = bundle.getBoolean("chrq", false);
        }
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.getActivity().registerReceiver(this.mScreenOffReceiver, intentFilter);
    }
    
    @Override
    protected void onDataSetChanged() {
        this.highlightPreferenceIfNeeded();
        if (this.isUiRestrictedByOnlyAdmin() && (this.mActionDisabledDialog == null || !this.mActionDisabledDialog.isShowing())) {
            this.mActionDisabledDialog = new ActionDisabledByAdminDialogHelper(this.getActivity()).prepareDialogBuilder(this.mRestrictionKey, this.getRestrictionEnforcedAdmin()).setOnDismissListener((DialogInterface$OnDismissListener)new _$$Lambda$RestrictedDashboardFragment$xeipMNP1JUFbhaWoStBNgM1y67g(this)).show();
            this.setEmptyView(new View(this.getContext()));
        }
        else if (this.mEmptyTextView != null) {
            this.setEmptyView((View)this.mEmptyTextView);
        }
        super.onDataSetChanged();
    }
    
    @Override
    public void onDestroy() {
        this.getActivity().unregisterReceiver(this.mScreenOffReceiver);
        super.onDestroy();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.shouldBeProviderProtected(this.mRestrictionKey)) {
            this.ensurePin();
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.getActivity().isChangingConfigurations()) {
            bundle.putBoolean("chrq", this.mChallengeRequested);
            bundle.putBoolean("chsc", this.mChallengeSucceeded);
        }
    }
    
    public void setIfOnlyAvailableForAdmins(final boolean mOnlyAvailableForAdmins) {
        this.mOnlyAvailableForAdmins = mOnlyAvailableForAdmins;
    }
    
    protected boolean shouldBeProviderProtected(final String s) {
        final boolean b = false;
        if (s == null) {
            return false;
        }
        final boolean b2 = "restrict_if_overridable".equals(s) || this.mUserManager.hasUserRestriction(this.mRestrictionKey);
        boolean b3 = b;
        if (b2) {
            b3 = b;
            if (this.mRestrictionsManager.hasRestrictionsProvider()) {
                b3 = true;
            }
        }
        return b3;
    }
}
