package com.android.settings.dashboard.suggestions;

import android.app.ActivityManager;
import com.android.settings.notification.ZenOnboardingActivity;
import com.android.settings.notification.ZenSuggestionActivity;
import com.android.settings.support.NewDeviceIntroSuggestionActivity;
import com.android.settings.display.NightDisplayPreferenceController;
import com.android.settings.Settings;
import com.android.settings.wifi.calling.WifiCallingSuggestionActivity;
import com.android.settings.password.ScreenLockSuggestionActivity;
import com.android.settings.fingerprint.FingerprintEnrollSuggestionActivity;
import com.android.settings.fingerprint.FingerprintSuggestionActivity;
import com.android.settings.wallpaper.WallpaperSuggestionActivity;
import android.content.ComponentName;
import android.content.SharedPreferences;
import android.util.Pair;
import android.service.settings.suggestions.Suggestion;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;

public class SuggestionFeatureProviderImpl implements SuggestionFeatureProvider
{
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    
    public SuggestionFeatureProviderImpl(final Context context) {
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context.getApplicationContext()).getMetricsFeatureProvider();
    }
    
    @Override
    public void dismissSuggestion(final Context context, final SuggestionControllerMixin suggestionControllerMixin, final Suggestion suggestion) {
        if (suggestionControllerMixin != null && suggestion != null && context != null) {
            this.mMetricsFeatureProvider.action(context, 387, suggestion.getId(), (Pair<Integer, Object>[])new Pair[0]);
            suggestionControllerMixin.dismissSuggestion(suggestion);
        }
    }
    
    @Override
    public SharedPreferences getSharedPrefs(final Context context) {
        return context.getSharedPreferences("suggestions", 0);
    }
    
    @Override
    public ComponentName getSuggestionServiceComponent() {
        return new ComponentName("com.android.settings.intelligence", "com.android.settings.intelligence.suggestions.SuggestionService");
    }
    
    @Override
    public boolean isSuggestionComplete(final Context context, final ComponentName componentName) {
        final String className = componentName.getClassName();
        if (className.equals(WallpaperSuggestionActivity.class.getName())) {
            return WallpaperSuggestionActivity.isSuggestionComplete(context);
        }
        if (className.equals(FingerprintSuggestionActivity.class.getName())) {
            return FingerprintSuggestionActivity.isSuggestionComplete(context);
        }
        if (className.equals(FingerprintEnrollSuggestionActivity.class.getName())) {
            return FingerprintEnrollSuggestionActivity.isSuggestionComplete(context);
        }
        if (className.equals(ScreenLockSuggestionActivity.class.getName())) {
            return ScreenLockSuggestionActivity.isSuggestionComplete(context);
        }
        if (className.equals(WifiCallingSuggestionActivity.class.getName())) {
            return WifiCallingSuggestionActivity.isSuggestionComplete(context);
        }
        if (className.equals(Settings.NightDisplaySuggestionActivity.class.getName())) {
            return NightDisplayPreferenceController.isSuggestionComplete(context);
        }
        if (className.equals(NewDeviceIntroSuggestionActivity.class.getName())) {
            return NewDeviceIntroSuggestionActivity.isSuggestionComplete(context);
        }
        return className.equals(ZenSuggestionActivity.class.getName()) && ZenOnboardingActivity.isSuggestionComplete(context);
    }
    
    @Override
    public boolean isSuggestionEnabled(final Context context) {
        return ((ActivityManager)context.getSystemService("activity")).isLowRamDevice() ^ true;
    }
}
