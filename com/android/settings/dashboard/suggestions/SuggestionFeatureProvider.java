package com.android.settings.dashboard.suggestions;

import android.content.ComponentName;
import android.content.SharedPreferences;
import android.service.settings.suggestions.Suggestion;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import android.content.Context;

public interface SuggestionFeatureProvider
{
    void dismissSuggestion(final Context p0, final SuggestionControllerMixin p1, final Suggestion p2);
    
    SharedPreferences getSharedPrefs(final Context p0);
    
    ComponentName getSuggestionServiceComponent();
    
    boolean isSuggestionComplete(final Context p0, final ComponentName p1);
    
    boolean isSuggestionEnabled(final Context p0);
}
