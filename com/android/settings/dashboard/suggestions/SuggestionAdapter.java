package com.android.settings.dashboard.suggestions;

import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.util.DisplayMetrics;
import android.content.res.Resources;
import android.view.WindowManager;
import java.util.Collection;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;
import android.text.TextUtils;
import android.graphics.Typeface;
import com.android.settingslib.Utils;
import java.util.Iterator;
import java.util.Objects;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.os.Bundle;
import java.util.ArrayList;
import android.service.settings.suggestions.Suggestion;
import java.util.List;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import com.android.settingslib.utils.IconCache;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.dashboard.DashboardAdapter;
import android.support.v7.widget.RecyclerView;

public class SuggestionAdapter extends Adapter<DashboardAdapter.DashboardItemHolder> implements LifecycleObserver, OnSaveInstanceState
{
    private final IconCache mCache;
    private final Callback mCallback;
    private final CardConfig mConfig;
    private final Context mContext;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final SuggestionControllerMixin mSuggestionControllerMixin;
    private final SuggestionFeatureProvider mSuggestionFeatureProvider;
    private List<Suggestion> mSuggestions;
    private final ArrayList<String> mSuggestionsShownLogged;
    
    public SuggestionAdapter(final Context mContext, final SuggestionControllerMixin mSuggestionControllerMixin, final Bundle bundle, final Callback mCallback, final Lifecycle lifecycle) {
        this.mContext = mContext;
        this.mSuggestionControllerMixin = mSuggestionControllerMixin;
        this.mCache = new IconCache(mContext);
        final FeatureFactory factory = FeatureFactory.getFactory(mContext);
        this.mMetricsFeatureProvider = factory.getMetricsFeatureProvider();
        this.mSuggestionFeatureProvider = factory.getSuggestionFeatureProvider(mContext);
        this.mCallback = mCallback;
        if (bundle != null) {
            this.mSuggestions = (List<Suggestion>)bundle.getParcelableArrayList("suggestion_list");
            this.mSuggestionsShownLogged = (ArrayList<String>)bundle.getStringArrayList("suggestions_shown_logged");
        }
        else {
            this.mSuggestionsShownLogged = new ArrayList<String>();
        }
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
        this.mConfig = CardConfig.get(mContext);
        ((RecyclerView.Adapter)this).setHasStableIds(true);
    }
    
    @Override
    public int getItemCount() {
        return this.mSuggestions.size();
    }
    
    @Override
    public long getItemId(final int n) {
        return Objects.hash(this.mSuggestions.get(n).getId());
    }
    
    @Override
    public int getItemViewType(final int n) {
        if ((this.getSuggestion(n).getFlags() & 0x1) != 0x0) {
            return 2131558778;
        }
        if (this.getItemCount() == 1) {
            return 2131558776;
        }
        return 2131558777;
    }
    
    public Suggestion getSuggestion(final int n) {
        final long itemId = this.getItemId(n);
        if (this.mSuggestions == null) {
            return null;
        }
        for (final Suggestion suggestion : this.mSuggestions) {
            if (Objects.hash(suggestion.getId()) == itemId) {
                return suggestion;
            }
        }
        return null;
    }
    
    public List<Suggestion> getSuggestions() {
        return this.mSuggestions;
    }
    
    public void onBindViewHolder(final DashboardAdapter.DashboardItemHolder dashboardItemHolder, final int n) {
        final Suggestion suggestion = this.mSuggestions.get(n);
        final String id = suggestion.getId();
        final int size = this.mSuggestions.size();
        if (!this.mSuggestionsShownLogged.contains(id)) {
            this.mMetricsFeatureProvider.action(this.mContext, 384, id, (Pair<Integer, Object>[])new Pair[0]);
            this.mSuggestionsShownLogged.add(id);
        }
        final Drawable icon = this.mCache.getIcon(suggestion.getIcon());
        if (icon != null && (suggestion.getFlags() & 0x2) != 0x0) {
            icon.setTint(Utils.getColorAccent(this.mContext));
        }
        dashboardItemHolder.icon.setImageDrawable(icon);
        dashboardItemHolder.title.setText(suggestion.getTitle());
        dashboardItemHolder.title.setTypeface(Typeface.create(this.mContext.getString(17039680), 0));
        if (size == 1) {
            final CharSequence summary = suggestion.getSummary();
            if (!TextUtils.isEmpty(summary)) {
                dashboardItemHolder.summary.setText(summary);
                dashboardItemHolder.summary.setVisibility(0);
            }
            else {
                dashboardItemHolder.summary.setVisibility(8);
            }
        }
        else {
            this.mConfig.setCardLayout(dashboardItemHolder, n);
        }
        final View viewById = dashboardItemHolder.itemView.findViewById(2131361992);
        if (viewById != null) {
            viewById.setOnClickListener((View.OnClickListener)new _$$Lambda$SuggestionAdapter$3YCJShAgHMZGvTmpJ4rD8V_2WkA(this, suggestion));
        }
        final View itemView = dashboardItemHolder.itemView;
        final View viewById2 = dashboardItemHolder.itemView.findViewById(16908300);
        View view = itemView;
        if (viewById2 != null) {
            view = viewById2;
        }
        view.setOnClickListener((View.OnClickListener)new _$$Lambda$SuggestionAdapter$o_nlX1JhE_RQCl3p5ch8A_R_uN0(this, id, suggestion));
    }
    
    public DashboardAdapter.DashboardItemHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        return new DashboardAdapter.DashboardItemHolder(LayoutInflater.from(viewGroup.getContext()).inflate(n, viewGroup, false));
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        if (this.mSuggestions != null) {
            bundle.putParcelableArrayList("suggestion_list", new ArrayList((Collection<? extends E>)this.mSuggestions));
        }
        bundle.putStringArrayList("suggestions_shown_logged", (ArrayList)this.mSuggestionsShownLogged);
    }
    
    public void setSuggestions(final List<Suggestion> mSuggestions) {
        this.mSuggestions = mSuggestions;
    }
    
    public interface Callback
    {
        void onSuggestionClosed(final Suggestion p0);
    }
    
    static class CardConfig
    {
        private static CardConfig sConfig;
        private final int mMarginInner;
        private final int mMarginOuter;
        private final WindowManager mWindowManager;
        
        private CardConfig(final Context context) {
            this.mWindowManager = (WindowManager)context.getSystemService("window");
            final Resources resources = context.getResources();
            this.mMarginInner = resources.getDimensionPixelOffset(2131165573);
            this.mMarginOuter = resources.getDimensionPixelOffset(2131165574);
        }
        
        public static CardConfig get(final Context context) {
            if (CardConfig.sConfig == null) {
                CardConfig.sConfig = new CardConfig(context);
            }
            return CardConfig.sConfig;
        }
        
        private int getWidthForTwoCrads() {
            return (this.getScreenWidth() - this.mMarginInner - this.mMarginOuter * 2) / 2;
        }
        
        int getScreenWidth() {
            final DisplayMetrics displayMetrics = new DisplayMetrics();
            this.mWindowManager.getDefaultDisplay().getMetrics(displayMetrics);
            return displayMetrics.widthPixels;
        }
        
        void setCardLayout(final DashboardAdapter.DashboardItemHolder dashboardItemHolder, int mMarginOuter) {
            final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(this.getWidthForTwoCrads(), -2);
            int marginStart;
            if (mMarginOuter == 0) {
                marginStart = this.mMarginOuter;
            }
            else {
                marginStart = this.mMarginInner;
            }
            layoutParams.setMarginStart(marginStart);
            if (mMarginOuter != 0) {
                mMarginOuter = this.mMarginOuter;
            }
            else {
                mMarginOuter = 0;
            }
            layoutParams.setMarginEnd(mMarginOuter);
            dashboardItemHolder.itemView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        }
    }
}
