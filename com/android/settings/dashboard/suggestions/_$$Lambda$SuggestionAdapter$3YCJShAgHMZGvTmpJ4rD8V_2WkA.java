package com.android.settings.dashboard.suggestions;

import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.util.DisplayMetrics;
import android.content.res.Resources;
import android.view.WindowManager;
import java.util.Collection;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.graphics.Typeface;
import com.android.settingslib.Utils;
import java.util.Iterator;
import java.util.Objects;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import com.android.settingslib.utils.IconCache;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.dashboard.DashboardAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.service.settings.suggestions.Suggestion;
import android.view.View.OnClickListener;

public final class _$$Lambda$SuggestionAdapter$3YCJShAgHMZGvTmpJ4rD8V_2WkA implements View.OnClickListener
{
    public final void onClick(final View view) {
        SuggestionAdapter.lambda$onBindViewHolder$0(this.f$0, this.f$1, view);
    }
}
