package com.android.settings.dashboard.suggestions;

import android.database.Cursor;
import android.content.ContentValues;
import android.net.Uri;
import android.content.Context;
import android.util.Log;
import com.android.settings.overlay.FeatureFactory;
import android.content.ComponentName;
import android.os.Bundle;
import android.content.ContentProvider;

public class SuggestionStateProvider extends ContentProvider
{
    static final String EXTRA_CANDIDATE_ID = "candidate_id";
    static final String METHOD_GET_SUGGESTION_STATE = "getSuggestionState";
    
    public Bundle call(String string, final String s, final Bundle bundle) {
        final Bundle bundle2 = new Bundle();
        if ("getSuggestionState".equals(string)) {
            string = bundle.getString("candidate_id");
            final ComponentName componentName = (ComponentName)bundle.getParcelable("android.intent.extra.COMPONENT_NAME");
            boolean suggestionComplete;
            if (componentName == null) {
                suggestionComplete = true;
            }
            else {
                final Context context = this.getContext();
                suggestionComplete = FeatureFactory.getFactory(context).getSuggestionFeatureProvider(context).isSuggestionComplete(context, componentName);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Suggestion ");
            sb.append(string);
            sb.append(" complete: ");
            sb.append(suggestionComplete);
            Log.d("SugstStatusProvider", sb.toString());
            bundle2.putBoolean("candidate_is_complete", suggestionComplete);
        }
        return bundle2;
    }
    
    public int delete(final Uri uri, final String s, final String[] array) {
        throw new UnsupportedOperationException("delete operation not supported currently.");
    }
    
    public String getType(final Uri uri) {
        throw new UnsupportedOperationException("getType operation not supported currently.");
    }
    
    public Uri insert(final Uri uri, final ContentValues contentValues) {
        throw new UnsupportedOperationException("insert operation not supported currently.");
    }
    
    public boolean onCreate() {
        return true;
    }
    
    public Cursor query(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        throw new UnsupportedOperationException("query operation not supported currently.");
    }
    
    public int update(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        throw new UnsupportedOperationException("update operation not supported currently.");
    }
}
