package com.android.settings.dashboard;

import java.util.Iterator;
import com.android.settings.display.NightDisplaySettings;
import com.android.settings.gestures.GestureSettings;
import com.android.settings.notification.ZenModeSettings;
import com.android.settings.security.LockscreenDashboardFragment;
import com.android.settings.notification.ConfigureNotificationSettings;
import com.android.settings.development.DevelopmentSettingsDashboardFragment;
import com.android.settings.language.LanguageAndInputSettings;
import com.android.settings.system.SystemDashboardFragment;
import com.android.settings.accounts.AccountDashboardFragment;
import com.android.settings.accounts.AccountDetailDashboardFragment;
import com.android.settings.security.SecuritySettings;
import com.android.settings.deviceinfo.StorageDashboardFragment;
import com.android.settings.notification.SoundSettings;
import com.android.settings.DisplaySettings;
import com.android.settings.applications.DefaultAppSettings;
import com.android.settings.fuelgauge.PowerUsageSummary;
import com.android.settings.applications.AppAndNotificationDashboardFragment;
import com.android.settings.connecteddevice.AdvancedConnectedDeviceDashboardFragment;
import com.android.settings.connecteddevice.ConnectedDeviceDashboardFragment;
import com.android.settings.network.NetworkDashboardFragment;
import android.util.ArrayMap;
import java.util.Map;

public class DashboardFragmentRegistry
{
    public static final Map<String, String> CATEGORY_KEY_TO_PARENT_MAP;
    public static final Map<String, String> PARENT_TO_CATEGORY_KEY_MAP;
    
    static {
        (PARENT_TO_CATEGORY_KEY_MAP = (Map)new ArrayMap()).put(NetworkDashboardFragment.class.getName(), "com.android.settings.category.ia.wireless");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(ConnectedDeviceDashboardFragment.class.getName(), "com.android.settings.category.ia.connect");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(AdvancedConnectedDeviceDashboardFragment.class.getName(), "com.android.settings.category.ia.device");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(AppAndNotificationDashboardFragment.class.getName(), "com.android.settings.category.ia.apps");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(PowerUsageSummary.class.getName(), "com.android.settings.category.ia.battery");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(DefaultAppSettings.class.getName(), "com.android.settings.category.ia.apps.default");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(DisplaySettings.class.getName(), "com.android.settings.category.ia.display");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(SoundSettings.class.getName(), "com.android.settings.category.ia.sound");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(StorageDashboardFragment.class.getName(), "com.android.settings.category.ia.storage");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(SecuritySettings.class.getName(), "com.android.settings.category.ia.security");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(AccountDetailDashboardFragment.class.getName(), "com.android.settings.category.ia.account_detail");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(AccountDashboardFragment.class.getName(), "com.android.settings.category.ia.accounts");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(SystemDashboardFragment.class.getName(), "com.android.settings.category.ia.system");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(LanguageAndInputSettings.class.getName(), "com.android.settings.category.ia.language");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(DevelopmentSettingsDashboardFragment.class.getName(), "com.android.settings.category.ia.development");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(ConfigureNotificationSettings.class.getName(), "com.android.settings.category.ia.notifications");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(LockscreenDashboardFragment.class.getName(), "com.android.settings.category.ia.lockscreen");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(ZenModeSettings.class.getName(), "com.android.settings.category.ia.dnd");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(GestureSettings.class.getName(), "com.android.settings.category.ia.gestures");
        DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.put(NightDisplaySettings.class.getName(), "com.android.settings.category.ia.night_display");
        CATEGORY_KEY_TO_PARENT_MAP = (Map)new ArrayMap(DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.size());
        for (final Map.Entry<String, String> entry : DashboardFragmentRegistry.PARENT_TO_CATEGORY_KEY_MAP.entrySet()) {
            DashboardFragmentRegistry.CATEGORY_KEY_TO_PARENT_MAP.put(entry.getValue(), entry.getKey());
        }
    }
}
