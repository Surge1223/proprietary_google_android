package com.android.settings.dashboard;

import com.android.settingslib.drawer.DashboardCategory;
import java.util.List;
import android.os.Bundle;
import android.os.UserHandle;
import com.android.settingslib.drawer.ProfileSelectDialog;
import android.util.Pair;
import android.util.Log;
import android.content.IContentProvider;
import java.util.Map;
import com.android.settingslib.drawer.TileUtils;
import android.util.ArrayMap;
import android.text.TextUtils;
import android.graphics.drawable.Icon;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.overlay.FeatureFactory;
import android.content.pm.PackageManager;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import com.android.settingslib.drawer.CategoryManager;
import android.content.Intent;
import com.android.settingslib.drawer.Tile;
import android.app.Activity;
import android.support.v7.preference.Preference;

public final class _$$Lambda$DashboardFeatureProviderImpl$EctMPOsKyfRtceDMH6yiU0UQS8U implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return DashboardFeatureProviderImpl.lambda$bindPreferenceToTile$0(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, preference);
    }
}
