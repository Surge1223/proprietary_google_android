package com.android.settings.dashboard;

import com.android.settingslib.drawer.DashboardCategory;
import java.util.List;
import com.android.settingslib.drawer.Tile;
import android.support.v7.preference.Preference;
import android.app.Activity;

public interface DashboardFeatureProvider
{
    void bindPreferenceToTile(final Activity p0, final int p1, final Preference p2, final Tile p3, final String p4, final int p5);
    
    List<DashboardCategory> getAllCategories();
    
    String getDashboardKeyForTile(final Tile p0);
    
    DashboardCategory getTilesForCategory(final String p0);
    
    void openTileIntent(final Activity p0, final Tile p1);
}
