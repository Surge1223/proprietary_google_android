package com.android.settings.dashboard;

import com.android.settingslib.drawer.DashboardCategory;
import java.util.List;
import android.os.Bundle;
import android.os.UserHandle;
import com.android.settingslib.drawer.ProfileSelectDialog;
import android.app.Activity;
import android.util.Pair;
import android.util.Log;
import android.content.IContentProvider;
import java.util.Map;
import com.android.settingslib.drawer.TileUtils;
import android.util.ArrayMap;
import android.text.TextUtils;
import android.graphics.drawable.Icon;
import android.content.Intent;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settingslib.drawer.Tile;
import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.pm.PackageManager;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import com.android.settingslib.drawer.CategoryManager;

public class DashboardFeatureProviderImpl implements DashboardFeatureProvider
{
    static final String META_DATA_KEY_ORDER = "com.android.settings.order";
    private final CategoryManager mCategoryManager;
    protected final Context mContext;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final PackageManager mPackageManager;
    
    public DashboardFeatureProviderImpl(final Context context) {
        this.mContext = context.getApplicationContext();
        this.mCategoryManager = CategoryManager.get(context, this.getExtraIntentAction());
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mPackageManager = context.getPackageManager();
    }
    
    private void bindSummary(final Preference preference, final Tile tile) {
        if (tile.summary != null) {
            preference.setSummary(tile.summary);
        }
        else if (tile.metaData != null && tile.metaData.containsKey("com.android.settings.summary_uri")) {
            preference.setSummary(2131889405);
            ThreadUtils.postOnBackgroundThread(new _$$Lambda$DashboardFeatureProviderImpl$eT0JYpovsB0_eUpWXkBH1qYJv_I(this, tile, preference));
        }
        else {
            preference.setSummary(2131889405);
        }
    }
    
    private boolean isIntentResolvable(final Intent intent) {
        final PackageManager mPackageManager = this.mPackageManager;
        boolean b = false;
        if (mPackageManager.resolveActivity(intent, 0) != null) {
            b = true;
        }
        return b;
    }
    
    private void launchIntentOrSelectProfile(final Activity activity, final Tile tile, final Intent intent, final int n) {
        if (!this.isIntentResolvable(intent)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot resolve intent, skipping. ");
            sb.append(intent);
            Log.w("DashboardFeatureImpl", sb.toString());
            return;
        }
        ProfileSelectDialog.updateUserHandlesIfNeeded(this.mContext, tile);
        if (tile.userHandle == null) {
            this.mMetricsFeatureProvider.logDashboardStartIntent(this.mContext, intent, n);
            activity.startActivityForResult(intent, 0);
        }
        else if (tile.userHandle.size() == 1) {
            this.mMetricsFeatureProvider.logDashboardStartIntent(this.mContext, intent, n);
            activity.startActivityForResultAsUser(intent, 0, (UserHandle)tile.userHandle.get(0));
        }
        else {
            ProfileSelectDialog.show(activity.getFragmentManager(), tile);
        }
    }
    
    void bindIcon(final Preference preference, final Tile tile) {
        if (tile.icon != null) {
            preference.setIcon(tile.icon.loadDrawable(preference.getContext()));
        }
        else if (tile.metaData != null && tile.metaData.containsKey("com.android.settings.icon_uri")) {
            ThreadUtils.postOnBackgroundThread(new _$$Lambda$DashboardFeatureProviderImpl$6nCUbNprlrw__1aNwFQYcoGh4Oc(this, tile, preference));
        }
    }
    
    @Override
    public void bindPreferenceToTile(final Activity activity, final int n, final Preference preference, final Tile tile, final String key, final int n2) {
        if (preference == null) {
            return;
        }
        preference.setTitle(tile.title);
        if (!TextUtils.isEmpty((CharSequence)key)) {
            preference.setKey(key);
        }
        else {
            preference.setKey(this.getDashboardKeyForTile(tile));
        }
        this.bindSummary(preference, tile);
        this.bindIcon(preference, tile);
        final Bundle metaData = tile.metaData;
        String fragment = null;
        String action = null;
        Integer value;
        final Integer n3 = value = null;
        if (metaData != null) {
            final String string = metaData.getString("com.android.settings.FRAGMENT_CLASS");
            final String string2 = metaData.getString("com.android.settings.intent.action");
            fragment = string;
            action = string2;
            value = n3;
            if (metaData.containsKey("com.android.settings.order")) {
                fragment = string;
                action = string2;
                value = n3;
                if (metaData.get("com.android.settings.order") instanceof Integer) {
                    value = metaData.getInt("com.android.settings.order");
                    action = string2;
                    fragment = string;
                }
            }
        }
        if (!TextUtils.isEmpty((CharSequence)fragment)) {
            preference.setFragment(fragment);
        }
        else if (tile.intent != null) {
            final Intent intent = new Intent(tile.intent);
            intent.putExtra(":settings:source_metrics", n);
            if (action != null) {
                intent.setAction(action);
            }
            preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$DashboardFeatureProviderImpl$EctMPOsKyfRtceDMH6yiU0UQS8U(this, activity, tile, intent, n));
        }
        final String packageName = activity.getPackageName();
        Integer value2;
        if ((value2 = value) == null) {
            value2 = value;
            if (tile.priority != 0) {
                value2 = -tile.priority;
            }
        }
        if (value2 != null) {
            boolean equals = false;
            if (tile.intent != null) {
                equals = TextUtils.equals((CharSequence)packageName, (CharSequence)tile.intent.getComponent().getPackageName());
            }
            if (!equals && n2 != Integer.MAX_VALUE) {
                preference.setOrder(value2 + n2);
            }
            else {
                preference.setOrder(value2);
            }
        }
    }
    
    @Override
    public List<DashboardCategory> getAllCategories() {
        return this.mCategoryManager.getCategories(this.mContext);
    }
    
    @Override
    public String getDashboardKeyForTile(final Tile tile) {
        if (tile == null || tile.intent == null) {
            return null;
        }
        if (!TextUtils.isEmpty((CharSequence)tile.key)) {
            return tile.key;
        }
        final StringBuilder sb = new StringBuilder("dashboard_tile_pref_");
        sb.append(tile.intent.getComponent().getClassName());
        return sb.toString();
    }
    
    public String getExtraIntentAction() {
        return null;
    }
    
    @Override
    public DashboardCategory getTilesForCategory(final String s) {
        return this.mCategoryManager.getTilesByCategory(this.mContext, s);
    }
    
    @Override
    public void openTileIntent(final Activity activity, final Tile tile) {
        if (tile == null) {
            this.mContext.startActivity(new Intent("android.settings.SETTINGS").addFlags(32768));
            return;
        }
        if (tile.intent == null) {
            return;
        }
        this.launchIntentOrSelectProfile(activity, tile, new Intent(tile.intent).putExtra(":settings:source_metrics", 35).addFlags(32768), 35);
    }
}
