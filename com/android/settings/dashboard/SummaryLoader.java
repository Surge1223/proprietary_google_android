package com.android.settings.dashboard;

import android.os.Message;
import android.os.Looper;
import android.os.Handler;
import android.text.TextUtils;
import com.android.settingslib.utils.ThreadUtils;
import java.util.Iterator;
import android.util.Log;
import java.util.List;
import com.android.settingslib.drawer.DashboardCategory;
import android.os.Bundle;
import com.android.settingslib.drawer.Tile;
import android.content.Context;
import com.android.settings.overlay.FeatureFactory;
import android.os.HandlerThread;
import android.content.ComponentName;
import android.util.ArrayMap;
import android.content.BroadcastReceiver;
import android.util.ArraySet;
import android.app.Activity;

public class SummaryLoader
{
    public static final String SUMMARY_PROVIDER_FACTORY = "SUMMARY_PROVIDER_FACTORY";
    private final Activity mActivity;
    private final String mCategoryKey;
    private final DashboardFeatureProvider mDashboardFeatureProvider;
    private boolean mListening;
    private ArraySet<BroadcastReceiver> mReceivers;
    private SummaryConsumer mSummaryConsumer;
    private final ArrayMap<SummaryProvider, ComponentName> mSummaryProviderMap;
    private final ArrayMap<String, CharSequence> mSummaryTextMap;
    private final Worker mWorker;
    private boolean mWorkerListening;
    private final HandlerThread mWorkerThread;
    
    public SummaryLoader(final Activity mActivity, final String mCategoryKey) {
        this.mSummaryProviderMap = (ArrayMap<SummaryProvider, ComponentName>)new ArrayMap();
        this.mSummaryTextMap = (ArrayMap<String, CharSequence>)new ArrayMap();
        this.mReceivers = (ArraySet<BroadcastReceiver>)new ArraySet();
        this.mDashboardFeatureProvider = FeatureFactory.getFactory((Context)mActivity).getDashboardFeatureProvider((Context)mActivity);
        this.mCategoryKey = mCategoryKey;
        (this.mWorkerThread = new HandlerThread("SummaryLoader", 10)).start();
        this.mWorker = new Worker(this.mWorkerThread.getLooper());
        this.mActivity = mActivity;
    }
    
    private Bundle getMetaData(final Tile tile) {
        return tile.metaData;
    }
    
    private SummaryProvider getSummaryProvider(final Tile tile) {
        if (!this.mActivity.getPackageName().equals(tile.intent.getComponent().getPackageName())) {
            return null;
        }
        final Bundle metaData = this.getMetaData(tile);
        if (metaData == null) {
            return null;
        }
        final String string = metaData.getString("com.android.settings.FRAGMENT_CLASS");
        if (string == null) {
            return null;
        }
        try {
            return ((SummaryProviderFactory)Class.forName(string).getField("SUMMARY_PROVIDER_FACTORY").get(null)).createSummaryProvider(this.mActivity, this);
        }
        catch (IllegalAccessException ex) {}
        catch (ClassCastException ex2) {}
        catch (NoSuchFieldException ex3) {}
        catch (ClassNotFoundException ex4) {}
        return null;
    }
    
    private Tile getTileFromCategory(final DashboardCategory dashboardCategory, final ComponentName componentName) {
        if (dashboardCategory != null && dashboardCategory.getTilesCount() != 0) {
            final List<Tile> tiles = dashboardCategory.getTiles();
            for (int size = tiles.size(), i = 0; i < size; ++i) {
                final Tile tile = tiles.get(i);
                if (componentName.equals((Object)tile.intent.getComponent())) {
                    return tile;
                }
            }
            return null;
        }
        return null;
    }
    
    private void makeProviderW(final Tile tile) {
        synchronized (this) {
            final SummaryProvider summaryProvider = this.getSummaryProvider(tile);
            if (summaryProvider != null) {
                this.mSummaryProviderMap.put((Object)summaryProvider, (Object)tile.intent.getComponent());
            }
        }
    }
    
    private void setListeningW(final boolean b) {
        synchronized (this) {
            if (this.mWorkerListening == b) {
                return;
            }
            this.mWorkerListening = b;
            for (final SummaryProvider summaryProvider : this.mSummaryProviderMap.keySet()) {
                try {
                    summaryProvider.setListening(b);
                }
                catch (Exception ex) {
                    Log.d("SummaryLoader", "Problem in setListening", (Throwable)ex);
                }
            }
        }
    }
    
    public void release() {
        this.mWorkerThread.quitSafely();
        this.setListeningW(false);
    }
    
    public void setListening(final boolean mListening) {
        if (this.mListening == mListening) {
            return;
        }
        this.mListening = mListening;
        for (int i = 0; i < this.mReceivers.size(); ++i) {
            this.mActivity.unregisterReceiver((BroadcastReceiver)this.mReceivers.valueAt(i));
        }
        this.mReceivers.clear();
        this.mWorker.removeMessages(3);
        if (!mListening) {
            this.mWorker.obtainMessage(3, (Object)0).sendToTarget();
        }
        else if (this.mSummaryProviderMap.isEmpty()) {
            if (!this.mWorker.hasMessages(1)) {
                this.mWorker.sendEmptyMessage(1);
            }
        }
        else {
            this.mWorker.obtainMessage(3, (Object)1).sendToTarget();
        }
    }
    
    public void setSummary(final SummaryProvider summaryProvider, final CharSequence charSequence) {
        ThreadUtils.postOnMainThread(new _$$Lambda$SummaryLoader$EirySW2ETuFFjqqH756jJXvHagg(this, (ComponentName)this.mSummaryProviderMap.get((Object)summaryProvider), charSequence));
    }
    
    public void setSummaryConsumer(final SummaryConsumer mSummaryConsumer) {
        this.mSummaryConsumer = mSummaryConsumer;
    }
    
    void updateSummaryIfNeeded(final Tile tile, final CharSequence summary) {
        if (TextUtils.equals(tile.summary, summary)) {
            return;
        }
        this.mSummaryTextMap.put((Object)this.mDashboardFeatureProvider.getDashboardKeyForTile(tile), (Object)summary);
        tile.summary = summary;
        if (this.mSummaryConsumer != null) {
            this.mSummaryConsumer.notifySummaryChanged(tile);
        }
    }
    
    public void updateSummaryToCache(final DashboardCategory dashboardCategory) {
        if (dashboardCategory == null) {
            return;
        }
        for (final Tile tile : dashboardCategory.getTiles()) {
            final String dashboardKeyForTile = this.mDashboardFeatureProvider.getDashboardKeyForTile(tile);
            if (this.mSummaryTextMap.containsKey((Object)dashboardKeyForTile)) {
                tile.summary = (CharSequence)this.mSummaryTextMap.get((Object)dashboardKeyForTile);
            }
        }
    }
    
    public interface SummaryConsumer
    {
        void notifySummaryChanged(final Tile p0);
    }
    
    public interface SummaryProvider
    {
        void setListening(final boolean p0);
    }
    
    public interface SummaryProviderFactory
    {
        SummaryProvider createSummaryProvider(final Activity p0, final SummaryLoader p1);
    }
    
    private class Worker extends Handler
    {
        public Worker(final Looper looper) {
            super(looper);
        }
        
        public void handleMessage(final Message message) {
            final int what = message.what;
            boolean b = true;
            switch (what) {
                case 3: {
                    if (message.obj == null || !message.obj.equals(1)) {
                        b = false;
                    }
                    SummaryLoader.this.setListeningW(b);
                    break;
                }
                case 2: {
                    SummaryLoader.this.makeProviderW((Tile)message.obj);
                    break;
                }
                case 1: {
                    final DashboardCategory tilesForCategory = SummaryLoader.this.mDashboardFeatureProvider.getTilesForCategory(SummaryLoader.this.mCategoryKey);
                    if (tilesForCategory != null && tilesForCategory.getTilesCount() != 0) {
                        final Iterator<Tile> iterator = tilesForCategory.getTiles().iterator();
                        while (iterator.hasNext()) {
                            SummaryLoader.this.makeProviderW(iterator.next());
                        }
                        SummaryLoader.this.setListeningW(true);
                        break;
                    }
                }
            }
        }
    }
}
