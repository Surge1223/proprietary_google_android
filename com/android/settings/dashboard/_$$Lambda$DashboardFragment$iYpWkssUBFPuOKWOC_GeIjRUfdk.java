package com.android.settings.dashboard;

import android.graphics.drawable.Icon;
import android.content.res.TypedArray;
import com.android.settingslib.drawer.DashboardCategory;
import android.text.TextUtils;
import android.app.Activity;
import android.support.v7.preference.PreferenceManager;
import android.os.Bundle;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.Collection;
import com.android.settings.core.PreferenceControllerListHelper;
import com.android.settings.overlay.FeatureFactory;
import android.support.v7.preference.Preference;
import android.util.Log;
import com.android.settingslib.drawer.Tile;
import android.content.Context;
import java.util.ArrayList;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.support.v7.preference.PreferenceScreen;
import java.util.stream.Stream;
import java.util.function.Function;
import android.util.ArraySet;
import android.util.ArrayMap;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.android.settingslib.drawer.SettingsDrawerActivity;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.core.BasePreferenceController;
import com.android.settingslib.core.lifecycle.Lifecycle;
import java.util.function.Consumer;

public final class _$$Lambda$DashboardFragment$iYpWkssUBFPuOKWOC_GeIjRUfdk implements Consumer
{
    @Override
    public final void accept(final Object o) {
        DashboardFragment.lambda$onAttach$1(this.f$0, (BasePreferenceController)o);
    }
}
