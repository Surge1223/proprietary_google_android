package com.android.settings.dashboard;

import android.os.Message;
import android.os.Looper;
import android.os.Handler;
import android.text.TextUtils;
import com.android.settingslib.utils.ThreadUtils;
import java.util.Iterator;
import android.util.Log;
import java.util.List;
import com.android.settingslib.drawer.DashboardCategory;
import android.os.Bundle;
import com.android.settingslib.drawer.Tile;
import android.content.Context;
import com.android.settings.overlay.FeatureFactory;
import android.os.HandlerThread;
import android.util.ArrayMap;
import android.content.BroadcastReceiver;
import android.util.ArraySet;
import android.app.Activity;
import android.content.ComponentName;

public final class _$$Lambda$SummaryLoader$EirySW2ETuFFjqqH756jJXvHagg implements Runnable
{
    @Override
    public final void run() {
        SummaryLoader.lambda$setSummary$0(this.f$0, this.f$1, this.f$2);
    }
}
