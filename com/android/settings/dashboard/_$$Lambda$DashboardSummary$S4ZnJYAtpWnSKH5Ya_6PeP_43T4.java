package com.android.settings.dashboard;

import com.android.settingslib.utils.ThreadUtils;
import java.util.Iterator;
import com.android.settings.dashboard.conditional.Condition;
import com.android.settings.widget.ActionBarShadowController;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.app.Activity;
import android.arch.lifecycle.LifecycleObserver;
import android.os.Bundle;
import com.android.settings.dashboard.suggestions.SuggestionFeatureProvider;
import com.android.settings.overlay.FeatureFactory;
import android.util.Log;
import android.content.Context;
import android.app.LoaderManager;
import android.service.settings.suggestions.Suggestion;
import java.util.List;
import com.android.settingslib.drawer.DashboardCategory;
import android.support.v7.widget.LinearLayoutManager;
import android.os.Handler;
import com.android.settingslib.suggestions.SuggestionControllerMixin;
import com.android.settingslib.drawer.SettingsDrawerActivity;
import com.android.settings.dashboard.conditional.FocusRecyclerView;
import com.android.settings.dashboard.conditional.ConditionManager;
import com.android.settings.core.InstrumentedFragment;

public final class _$$Lambda$DashboardSummary$S4ZnJYAtpWnSKH5Ya_6PeP_43T4 implements Runnable
{
    @Override
    public final void run() {
        DashboardSummary.lambda$updateCategory$2(this.f$0);
    }
}
