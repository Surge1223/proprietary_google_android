package com.android.settings.dashboard;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;

public class RoundedHomepageIcon extends LayerDrawable
{
    int mBackgroundColor;
    
    public RoundedHomepageIcon(final Context context, final Drawable drawable) {
        super(new Drawable[] { context.getDrawable(2131231038), drawable });
        this.mBackgroundColor = -1;
        final int dimensionPixelSize = context.getResources().getDimensionPixelSize(2131165372);
        this.setLayerInset(1, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
    }
    
    public void setBackgroundColor(final int mBackgroundColor) {
        this.mBackgroundColor = mBackgroundColor;
        this.getDrawable(0).setColorFilter(mBackgroundColor, PorterDuff.Mode.SRC_ATOP);
    }
}
