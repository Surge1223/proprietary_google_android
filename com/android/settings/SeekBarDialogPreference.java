package com.android.settings;

import android.widget.ImageView;
import android.widget.SeekBar;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import com.android.settingslib.CustomDialogPreference;

public class SeekBarDialogPreference extends CustomDialogPreference
{
    private final Drawable mMyIcon;
    
    public SeekBarDialogPreference(final Context context) {
        this(context, null);
    }
    
    public SeekBarDialogPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setDialogLayoutResource(2131558656);
        this.createActionButtons();
        this.mMyIcon = this.getDialogIcon();
        this.setDialogIcon(null);
    }
    
    protected static SeekBar getSeekBar(final View view) {
        return (SeekBar)view.findViewById(R.id.seekbar);
    }
    
    public void createActionButtons() {
        this.setPositiveButtonText(17039370);
        this.setNegativeButtonText(17039360);
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        final ImageView imageView = (ImageView)view.findViewById(16908294);
        if (this.mMyIcon != null) {
            imageView.setImageDrawable(this.mMyIcon);
        }
        else {
            imageView.setVisibility(8);
        }
    }
}
