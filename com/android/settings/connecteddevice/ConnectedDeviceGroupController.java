package com.android.settings.connecteddevice;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.bluetooth.ConnectedBluetoothDeviceUpdater;
import com.android.settings.dashboard.DashboardFragment;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.connecteddevice.usb.ConnectedUsbDeviceUpdater;
import com.android.settings.connecteddevice.dock.DockUpdater;
import com.android.settings.bluetooth.BluetoothDeviceUpdater;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.BasePreferenceController;

public class ConnectedDeviceGroupController extends BasePreferenceController implements DevicePreferenceCallback, PreferenceControllerMixin, LifecycleObserver, OnStart, OnStop
{
    private static final String KEY = "connected_device_list";
    private BluetoothDeviceUpdater mBluetoothDeviceUpdater;
    private DockUpdater mConnectedDockUpdater;
    private ConnectedUsbDeviceUpdater mConnectedUsbDeviceUpdater;
    PreferenceGroup mPreferenceGroup;
    
    public ConnectedDeviceGroupController(final Context context) {
        super(context, "connected_device_list");
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            (this.mPreferenceGroup = (PreferenceGroup)preferenceScreen.findPreference("connected_device_list")).setVisible(false);
            this.mBluetoothDeviceUpdater.setPrefContext(preferenceScreen.getContext());
            this.mBluetoothDeviceUpdater.forceUpdate();
            this.mConnectedUsbDeviceUpdater.initUsbPreference(preferenceScreen.getContext());
            this.mConnectedDockUpdater.forceUpdate();
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public String getPreferenceKey() {
        return "connected_device_list";
    }
    
    public void init(final BluetoothDeviceUpdater mBluetoothDeviceUpdater, final ConnectedUsbDeviceUpdater mConnectedUsbDeviceUpdater, final DockUpdater mConnectedDockUpdater) {
        this.mBluetoothDeviceUpdater = mBluetoothDeviceUpdater;
        this.mConnectedUsbDeviceUpdater = mConnectedUsbDeviceUpdater;
        this.mConnectedDockUpdater = mConnectedDockUpdater;
    }
    
    public void init(final DashboardFragment dashboardFragment) {
        final Context context = dashboardFragment.getContext();
        this.init(new ConnectedBluetoothDeviceUpdater(context, dashboardFragment, this), new ConnectedUsbDeviceUpdater(context, dashboardFragment, this), FeatureFactory.getFactory(context).getDockUpdaterFeatureProvider().getConnectedDockUpdater(context, this));
    }
    
    @Override
    public void onDeviceAdded(final Preference preference) {
        if (this.mPreferenceGroup.getPreferenceCount() == 0) {
            this.mPreferenceGroup.setVisible(true);
        }
        this.mPreferenceGroup.addPreference(preference);
    }
    
    @Override
    public void onDeviceRemoved(final Preference preference) {
        this.mPreferenceGroup.removePreference(preference);
        if (this.mPreferenceGroup.getPreferenceCount() == 0) {
            this.mPreferenceGroup.setVisible(false);
        }
    }
    
    @Override
    public void onStart() {
        this.mBluetoothDeviceUpdater.registerCallback();
        this.mConnectedUsbDeviceUpdater.registerCallback();
        this.mConnectedDockUpdater.registerCallback();
    }
    
    @Override
    public void onStop() {
        this.mConnectedUsbDeviceUpdater.unregisterCallback();
        this.mBluetoothDeviceUpdater.unregisterCallback();
        this.mConnectedDockUpdater.unregisterCallback();
    }
}
