package com.android.settings.connecteddevice;

import android.support.v7.preference.Preference;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settings.bluetooth.AvailableMediaBluetoothDeviceUpdater;
import com.android.settings.dashboard.DashboardFragment;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.bluetooth.Utils;
import android.content.Context;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.bluetooth.BluetoothDeviceUpdater;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.bluetooth.BluetoothCallback;
import com.android.settings.core.BasePreferenceController;

public class AvailableMediaDeviceGroupController extends BasePreferenceController implements DevicePreferenceCallback, BluetoothCallback, LifecycleObserver, OnStart, OnStop
{
    private static final String KEY = "available_device_list";
    private BluetoothDeviceUpdater mBluetoothDeviceUpdater;
    private final LocalBluetoothManager mLocalBluetoothManager;
    PreferenceGroup mPreferenceGroup;
    
    public AvailableMediaDeviceGroupController(final Context context) {
        super(context, "available_device_list");
        this.mLocalBluetoothManager = Utils.getLocalBtManager(this.mContext);
    }
    
    private void updateTitle() {
        if (com.android.settingslib.Utils.isAudioModeOngoingCall(this.mContext)) {
            this.mPreferenceGroup.setTitle(this.mContext.getString(2131887113));
        }
        else {
            this.mPreferenceGroup.setTitle(this.mContext.getString(2131887114));
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            (this.mPreferenceGroup = (PreferenceGroup)preferenceScreen.findPreference("available_device_list")).setVisible(false);
            this.updateTitle();
            this.mBluetoothDeviceUpdater.setPrefContext(preferenceScreen.getContext());
            this.mBluetoothDeviceUpdater.forceUpdate();
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public String getPreferenceKey() {
        return "available_device_list";
    }
    
    public void init(final DashboardFragment dashboardFragment) {
        this.mBluetoothDeviceUpdater = new AvailableMediaBluetoothDeviceUpdater(dashboardFragment.getContext(), dashboardFragment, this);
    }
    
    @Override
    public void onActiveDeviceChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onAudioModeChanged() {
        this.updateTitle();
    }
    
    @Override
    public void onBluetoothStateChanged(final int n) {
    }
    
    @Override
    public void onConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onDeviceAdded(final Preference preference) {
        if (this.mPreferenceGroup.getPreferenceCount() == 0) {
            this.mPreferenceGroup.setVisible(true);
        }
        this.mPreferenceGroup.addPreference(preference);
    }
    
    @Override
    public void onDeviceAdded(final CachedBluetoothDevice cachedBluetoothDevice) {
    }
    
    @Override
    public void onDeviceBondStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onDeviceDeleted(final CachedBluetoothDevice cachedBluetoothDevice) {
    }
    
    @Override
    public void onDeviceRemoved(final Preference preference) {
        this.mPreferenceGroup.removePreference(preference);
        if (this.mPreferenceGroup.getPreferenceCount() == 0) {
            this.mPreferenceGroup.setVisible(false);
        }
    }
    
    @Override
    public void onScanningStateChanged(final boolean b) {
    }
    
    @Override
    public void onStart() {
        this.mBluetoothDeviceUpdater.registerCallback();
        this.mLocalBluetoothManager.getEventManager().registerCallback(this);
    }
    
    @Override
    public void onStop() {
        this.mBluetoothDeviceUpdater.unregisterCallback();
        this.mLocalBluetoothManager.getEventManager().unregisterCallback(this);
    }
    
    public void setBluetoothDeviceUpdater(final BluetoothDeviceUpdater mBluetoothDeviceUpdater) {
        this.mBluetoothDeviceUpdater = mBluetoothDeviceUpdater;
    }
}
