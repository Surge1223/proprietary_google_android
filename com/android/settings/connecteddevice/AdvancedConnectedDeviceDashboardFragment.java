package com.android.settings.connecteddevice;

import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.print.PrintSettingPreferenceController;
import com.android.settings.bluetooth.BluetoothFilesPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class AdvancedConnectedDeviceDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (!context.getPackageManager().hasSystemFeature("android.hardware.nfc")) {
                    nonIndexableKeys.add("android_beam_settings");
                }
                nonIndexableKeys.add("bluetooth_settings");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082736;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<BluetoothOnWhileDrivingPreferenceController> list = (ArrayList<BluetoothOnWhileDrivingPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new BluetoothFilesPreferenceController(context));
        list.add(new BluetoothOnWhileDrivingPreferenceController(context));
        final PrintSettingPreferenceController printSettingPreferenceController = new PrintSettingPreferenceController(context);
        if (lifecycle != null) {
            lifecycle.addObserver(printSettingPreferenceController);
        }
        list.add((BluetoothFilesPreferenceController)printSettingPreferenceController);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildControllers(context, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887804;
    }
    
    @Override
    protected String getLogTag() {
        return "AdvancedConnectedDeviceFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1264;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082736;
    }
}
