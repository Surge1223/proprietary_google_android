package com.android.settings.connecteddevice;

import android.content.res.Resources;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class PreviouslyConnectedDeviceDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final Resources resources = context.getResources();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.key = "saved_device_list";
                searchIndexableRaw.title = resources.getString(2131887117);
                searchIndexableRaw.screenTitle = resources.getString(2131887117);
                list.add(searchIndexableRaw);
                return list;
            }
        };
    }
    
    @Override
    public int getHelpResource() {
        return 2131887824;
    }
    
    @Override
    protected String getLogTag() {
        return "PreConnectedDeviceFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1370;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082813;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(SavedDeviceGroupController.class).init(this);
    }
}
