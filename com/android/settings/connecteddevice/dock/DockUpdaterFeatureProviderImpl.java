package com.android.settings.connecteddevice.dock;

import com.android.settings.connecteddevice.DevicePreferenceCallback;
import android.content.Context;
import com.android.settings.overlay.DockUpdaterFeatureProvider;

public class DockUpdaterFeatureProviderImpl implements DockUpdaterFeatureProvider
{
    @Override
    public DockUpdater getConnectedDockUpdater(final Context context, final DevicePreferenceCallback devicePreferenceCallback) {
        return new DockUpdater() {};
    }
    
    @Override
    public DockUpdater getSavedDockUpdater(final Context context, final DevicePreferenceCallback devicePreferenceCallback) {
        return new DockUpdater() {};
    }
}
