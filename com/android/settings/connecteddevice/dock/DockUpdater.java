package com.android.settings.connecteddevice.dock;

public interface DockUpdater
{
    default void forceUpdate() {
    }
    
    default void registerCallback() {
    }
    
    default void unregisterCallback() {
    }
}
