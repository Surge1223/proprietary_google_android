package com.android.settings.connecteddevice;

import android.support.v7.preference.Preference;
import com.android.settings.bluetooth.SavedBluetoothDeviceUpdater;
import com.android.settings.dashboard.DashboardFragment;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.connecteddevice.dock.DockUpdater;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.bluetooth.BluetoothDeviceUpdater;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.BasePreferenceController;

public class SavedDeviceGroupController extends BasePreferenceController implements DevicePreferenceCallback, PreferenceControllerMixin, LifecycleObserver, OnStart, OnStop
{
    private static final String KEY = "saved_device_list";
    private BluetoothDeviceUpdater mBluetoothDeviceUpdater;
    PreferenceGroup mPreferenceGroup;
    private DockUpdater mSavedDockUpdater;
    
    public SavedDeviceGroupController(final Context context) {
        super(context, "saved_device_list");
        this.mSavedDockUpdater = FeatureFactory.getFactory(context).getDockUpdaterFeatureProvider().getSavedDockUpdater(context, this);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        if (this.isAvailable()) {
            (this.mPreferenceGroup = (PreferenceGroup)preferenceScreen.findPreference("saved_device_list")).setVisible(false);
            this.mBluetoothDeviceUpdater.setPrefContext(preferenceScreen.getContext());
            this.mBluetoothDeviceUpdater.forceUpdate();
            this.mSavedDockUpdater.forceUpdate();
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public String getPreferenceKey() {
        return "saved_device_list";
    }
    
    public void init(final DashboardFragment dashboardFragment) {
        this.mBluetoothDeviceUpdater = new SavedBluetoothDeviceUpdater(dashboardFragment.getContext(), dashboardFragment, this);
    }
    
    @Override
    public void onDeviceAdded(final Preference preference) {
        if (this.mPreferenceGroup.getPreferenceCount() == 0) {
            this.mPreferenceGroup.setVisible(true);
        }
        this.mPreferenceGroup.addPreference(preference);
    }
    
    @Override
    public void onDeviceRemoved(final Preference preference) {
        this.mPreferenceGroup.removePreference(preference);
        if (this.mPreferenceGroup.getPreferenceCount() == 0) {
            this.mPreferenceGroup.setVisible(false);
        }
    }
    
    @Override
    public void onStart() {
        this.mBluetoothDeviceUpdater.registerCallback();
        this.mSavedDockUpdater.registerCallback();
    }
    
    @Override
    public void onStop() {
        this.mBluetoothDeviceUpdater.unregisterCallback();
        this.mSavedDockUpdater.unregisterCallback();
    }
    
    public void setBluetoothDeviceUpdater(final BluetoothDeviceUpdater mBluetoothDeviceUpdater) {
        this.mBluetoothDeviceUpdater = mBluetoothDeviceUpdater;
    }
    
    public void setSavedDockUpdater(final DockUpdater mSavedDockUpdater) {
        this.mSavedDockUpdater = mSavedDockUpdater;
    }
}
