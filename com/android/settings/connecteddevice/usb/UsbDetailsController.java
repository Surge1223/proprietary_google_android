package com.android.settings.connecteddevice.usb;

import com.android.internal.annotations.VisibleForTesting;
import android.os.Handler;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class UsbDetailsController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    protected final Context mContext;
    protected final UsbDetailsFragment mFragment;
    @VisibleForTesting
    Handler mHandler;
    protected final UsbBackend mUsbBackend;
    
    public UsbDetailsController(final Context mContext, final UsbDetailsFragment mFragment, final UsbBackend mUsbBackend) {
        super(mContext);
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.mUsbBackend = mUsbBackend;
        this.mHandler = new Handler(mContext.getMainLooper());
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    protected abstract void refresh(final boolean p0, final long p1, final int p2, final int p3);
}
