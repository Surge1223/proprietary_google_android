package com.android.settings.connecteddevice.usb;

import android.content.IntentFilter;
import android.hardware.usb.UsbPortStatus;
import android.content.Intent;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.content.BroadcastReceiver;

public class UsbConnectionBroadcastReceiver extends BroadcastReceiver implements LifecycleObserver, OnPause, OnResume
{
    private boolean mConnected;
    private Context mContext;
    private int mDataRole;
    private long mFunctions;
    private boolean mListeningToUsbEvents;
    private int mPowerRole;
    private UsbBackend mUsbBackend;
    private UsbConnectionListener mUsbConnectionListener;
    
    public UsbConnectionBroadcastReceiver(final Context mContext, final UsbConnectionListener mUsbConnectionListener, final UsbBackend mUsbBackend) {
        this.mContext = mContext;
        this.mUsbConnectionListener = mUsbConnectionListener;
        this.mUsbBackend = mUsbBackend;
        this.mFunctions = 0L;
        this.mDataRole = 0;
        this.mPowerRole = 0;
    }
    
    public void onPause() {
        this.unregister();
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if ("android.hardware.usb.action.USB_STATE".equals(intent.getAction())) {
            this.mConnected = (intent.getExtras().getBoolean("connected") || intent.getExtras().getBoolean("host_connected"));
            if (this.mConnected) {
                long n2;
                final long n = n2 = 0L;
                if (intent.getExtras().getBoolean("mtp")) {
                    n2 = n;
                    if (intent.getExtras().getBoolean("unlocked", false)) {
                        n2 = (0x0L | 0x4L);
                    }
                }
                long n3 = n2;
                if (intent.getExtras().getBoolean("ptp")) {
                    n3 = n2;
                    if (intent.getExtras().getBoolean("unlocked", false)) {
                        n3 = (n2 | 0x10L);
                    }
                }
                long n4 = n3;
                if (intent.getExtras().getBoolean("midi")) {
                    n4 = (n3 | 0x8L);
                }
                long mFunctions = n4;
                if (intent.getExtras().getBoolean("rndis")) {
                    mFunctions = (n4 | 0x20L);
                }
                this.mFunctions = mFunctions;
                this.mDataRole = this.mUsbBackend.getDataRole();
                this.mPowerRole = this.mUsbBackend.getPowerRole();
            }
        }
        else if ("android.hardware.usb.action.USB_PORT_CHANGED".equals(intent.getAction())) {
            final UsbPortStatus usbPortStatus = (UsbPortStatus)intent.getExtras().getParcelable("portStatus");
            if (usbPortStatus != null) {
                this.mDataRole = usbPortStatus.getCurrentDataRole();
                this.mPowerRole = usbPortStatus.getCurrentPowerRole();
            }
        }
        if (this.mUsbConnectionListener != null) {
            this.mUsbConnectionListener.onUsbConnectionChanged(this.mConnected, this.mFunctions, this.mPowerRole, this.mDataRole);
        }
    }
    
    public void onResume() {
        this.register();
    }
    
    public void register() {
        if (!this.mListeningToUsbEvents) {
            this.mConnected = false;
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.hardware.usb.action.USB_STATE");
            intentFilter.addAction("android.hardware.usb.action.USB_PORT_CHANGED");
            final Intent registerReceiver = this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter);
            if (registerReceiver != null) {
                this.onReceive(this.mContext, registerReceiver);
            }
            this.mListeningToUsbEvents = true;
        }
    }
    
    public void unregister() {
        if (this.mListeningToUsbEvents) {
            this.mContext.unregisterReceiver((BroadcastReceiver)this);
            this.mListeningToUsbEvents = false;
        }
    }
    
    interface UsbConnectionListener
    {
        void onUsbConnectionChanged(final boolean p0, final long p1, final int p2, final int p3);
    }
}
