package com.android.settings.connecteddevice.usb;

import android.os.UserHandle;
import android.net.ConnectivityManager;
import android.os.UserManager;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbPortStatus;
import android.hardware.usb.UsbPort;

public class UsbBackend
{
    private final boolean mFileTransferRestricted;
    private final boolean mFileTransferRestrictedBySystem;
    private final boolean mMidiSupported;
    private UsbPort mPort;
    private UsbPortStatus mPortStatus;
    private final boolean mTetheringRestricted;
    private final boolean mTetheringRestrictedBySystem;
    private final boolean mTetheringSupported;
    private UsbManager mUsbManager;
    
    public UsbBackend(final Context context) {
        this(context, (UserManager)context.getSystemService("user"));
    }
    
    public UsbBackend(final Context context, final UserManager userManager) {
        this.mUsbManager = (UsbManager)context.getSystemService((Class)UsbManager.class);
        this.mFileTransferRestricted = isUsbFileTransferRestricted(userManager);
        this.mFileTransferRestrictedBySystem = isUsbFileTransferRestrictedBySystem(userManager);
        this.mTetheringRestricted = isUsbTetheringRestricted(userManager);
        this.mTetheringRestrictedBySystem = isUsbTetheringRestrictedBySystem(userManager);
        this.mMidiSupported = context.getPackageManager().hasSystemFeature("android.software.midi");
        this.mTetheringSupported = ((ConnectivityManager)context.getSystemService("connectivity")).isTetheringSupported();
        this.updatePorts();
    }
    
    private boolean areFunctionDisallowed(final long n) {
        return (this.mFileTransferRestricted && ((0x4L & n) != 0x0L || (0x10L & n) != 0x0L)) || (this.mTetheringRestricted && (0x20L & n) != 0x0L);
    }
    
    private boolean areFunctionsDisallowedBySystem(final long n) {
        return (this.mFileTransferRestrictedBySystem && ((0x4L & n) != 0x0L || (0x10L & n) != 0x0L)) || (this.mTetheringRestrictedBySystem && (0x20L & n) != 0x0L);
    }
    
    public static int dataRoleFromString(final String s) {
        return Integer.parseInt(s);
    }
    
    public static String dataRoleToString(final int n) {
        return Integer.toString(n);
    }
    
    private static boolean isUsbFileTransferRestricted(final UserManager userManager) {
        return userManager.hasUserRestriction("no_usb_file_transfer");
    }
    
    private static boolean isUsbFileTransferRestrictedBySystem(final UserManager userManager) {
        return userManager.hasBaseUserRestriction("no_usb_file_transfer", UserHandle.of(UserHandle.myUserId()));
    }
    
    private static boolean isUsbTetheringRestricted(final UserManager userManager) {
        return userManager.hasUserRestriction("no_config_tethering");
    }
    
    private static boolean isUsbTetheringRestrictedBySystem(final UserManager userManager) {
        return userManager.hasBaseUserRestriction("no_config_tethering", UserHandle.of(UserHandle.myUserId()));
    }
    
    private void updatePorts() {
        this.mPort = null;
        this.mPortStatus = null;
        final UsbPort[] ports = this.mUsbManager.getPorts();
        if (ports == null) {
            return;
        }
        for (int length = ports.length, i = 0; i < length; ++i) {
            final UsbPortStatus portStatus = this.mUsbManager.getPortStatus(ports[i]);
            if (portStatus.isConnected()) {
                this.mPort = ports[i];
                this.mPortStatus = portStatus;
                break;
            }
        }
    }
    
    public static long usbFunctionsFromString(final String s) {
        return Long.parseLong(s, 2);
    }
    
    public static String usbFunctionsToString(final long n) {
        return Long.toBinaryString(n);
    }
    
    public boolean areAllRolesSupported() {
        final UsbPort mPort = this.mPort;
        boolean b = true;
        if (mPort == null || this.mPortStatus == null || !this.mPortStatus.isRoleCombinationSupported(2, 2) || !this.mPortStatus.isRoleCombinationSupported(2, 1) || !this.mPortStatus.isRoleCombinationSupported(1, 2) || !this.mPortStatus.isRoleCombinationSupported(1, 1)) {
            b = false;
        }
        return b;
    }
    
    public boolean areFunctionsSupported(final long n) {
        final boolean mMidiSupported = this.mMidiSupported;
        final boolean b = false;
        if ((!mMidiSupported && (0x8L & n) != 0x0L) || (!this.mTetheringSupported && (0x20L & n) != 0x0L)) {
            return false;
        }
        boolean b2 = b;
        if (!this.areFunctionDisallowed(n)) {
            b2 = b;
            if (!this.areFunctionsDisallowedBySystem(n)) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public long getCurrentFunctions() {
        return this.mUsbManager.getCurrentFunctions();
    }
    
    public int getDataRole() {
        this.updatePorts();
        int currentDataRole;
        if (this.mPortStatus == null) {
            currentDataRole = 0;
        }
        else {
            currentDataRole = this.mPortStatus.getCurrentDataRole();
        }
        return currentDataRole;
    }
    
    public long getDefaultUsbFunctions() {
        return this.mUsbManager.getScreenUnlockedFunctions();
    }
    
    public int getPowerRole() {
        this.updatePorts();
        int currentPowerRole;
        if (this.mPortStatus == null) {
            currentPowerRole = 0;
        }
        else {
            currentPowerRole = this.mPortStatus.getCurrentPowerRole();
        }
        return currentPowerRole;
    }
    
    public void setCurrentFunctions(final long currentFunctions) {
        this.mUsbManager.setCurrentFunctions(currentFunctions);
    }
    
    public void setDataRole(final int n) {
        int powerRole = this.getPowerRole();
        if (!this.areAllRolesSupported()) {
            switch (n) {
                default: {
                    powerRole = 0;
                    break;
                }
                case 2: {
                    powerRole = 2;
                    break;
                }
                case 1: {
                    powerRole = 1;
                    break;
                }
            }
        }
        if (this.mPort != null) {
            this.mUsbManager.setPortRoles(this.mPort, powerRole, n);
        }
    }
    
    public void setDefaultUsbFunctions(final long screenUnlockedFunctions) {
        this.mUsbManager.setScreenUnlockedFunctions(screenUnlockedFunctions);
    }
    
    public void setPowerRole(final int n) {
        int dataRole = this.getDataRole();
        if (!this.areAllRolesSupported()) {
            switch (n) {
                default: {
                    dataRole = 0;
                    break;
                }
                case 2: {
                    dataRole = 2;
                    break;
                }
                case 1: {
                    dataRole = 1;
                    break;
                }
            }
        }
        if (this.mPort != null) {
            this.mUsbManager.setPortRoles(this.mPort, n, dataRole);
        }
    }
}
