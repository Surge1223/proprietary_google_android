package com.android.settings.connecteddevice.usb;

import android.app.Fragment;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.widget.EntityHeaderController;

public class UsbDetailsHeaderController extends UsbDetailsController
{
    private EntityHeaderController mHeaderController;
    
    public UsbDetailsHeaderController(final Context context, final UsbDetailsFragment usbDetailsFragment, final UsbBackend usbBackend) {
        super(context, usbDetailsFragment, usbBackend);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mHeaderController = EntityHeaderController.newInstance(this.mFragment.getActivity(), this.mFragment, ((LayoutPreference)preferenceScreen.findPreference("usb_device_header")).findViewById(2131362112));
    }
    
    @Override
    public String getPreferenceKey() {
        return "usb_device_header";
    }
    
    @Override
    protected void refresh(final boolean b, final long n, final int n2, final int n3) {
        this.mHeaderController.setLabel(this.mContext.getString(2131889639));
        this.mHeaderController.setIcon(this.mContext.getDrawable(2131231187));
        this.mHeaderController.done(this.mFragment.getActivity(), true);
    }
}
