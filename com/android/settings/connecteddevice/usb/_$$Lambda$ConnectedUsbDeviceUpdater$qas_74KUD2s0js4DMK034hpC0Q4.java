package com.android.settings.connecteddevice.usb;

import android.util.AttributeSet;
import com.android.settings.core.SubSettingLauncher;
import android.content.Context;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import android.support.v7.preference.Preference;

public final class _$$Lambda$ConnectedUsbDeviceUpdater$qas_74KUD2s0js4DMK034hpC0Q4 implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return ConnectedUsbDeviceUpdater.lambda$initUsbPreference$1(this.f$0, preference);
    }
}
