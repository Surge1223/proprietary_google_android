package com.android.settings.connecteddevice.usb;

import android.util.AttributeSet;
import com.android.settings.core.SubSettingLauncher;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.connecteddevice.DevicePreferenceCallback;

public class ConnectedUsbDeviceUpdater
{
    private DevicePreferenceCallback mDevicePreferenceCallback;
    private DashboardFragment mFragment;
    private UsbBackend mUsbBackend;
    UsbConnectionBroadcastReceiver.UsbConnectionListener mUsbConnectionListener;
    Preference mUsbPreference;
    UsbConnectionBroadcastReceiver mUsbReceiver;
    
    public ConnectedUsbDeviceUpdater(final Context context, final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback) {
        this(context, dashboardFragment, devicePreferenceCallback, new UsbBackend(context));
    }
    
    ConnectedUsbDeviceUpdater(final Context context, final DashboardFragment mFragment, final DevicePreferenceCallback mDevicePreferenceCallback, final UsbBackend mUsbBackend) {
        this.mUsbConnectionListener = new _$$Lambda$ConnectedUsbDeviceUpdater$8_8ZhYJMgn_zGVqi_7esENaXwOM(this);
        this.mFragment = mFragment;
        this.mDevicePreferenceCallback = mDevicePreferenceCallback;
        this.mUsbBackend = mUsbBackend;
        this.mUsbReceiver = new UsbConnectionBroadcastReceiver(context, this.mUsbConnectionListener, this.mUsbBackend);
    }
    
    private void forceUpdate() {
        this.mUsbReceiver.register();
    }
    
    public static int getSummary(final long n, final int n2) {
        switch (n2) {
            default: {
                return 2131889645;
            }
            case 2: {
                if (n == 4L) {
                    return 2131889646;
                }
                if (n == 32L) {
                    return 2131889651;
                }
                if (n == 16L) {
                    return 2131889648;
                }
                if (n == 8L) {
                    return 2131889643;
                }
                return 2131889645;
            }
            case 1: {
                if (n == 4L) {
                    return 2131889647;
                }
                if (n == 32L) {
                    return 2131889652;
                }
                if (n == 16L) {
                    return 2131889649;
                }
                if (n == 8L) {
                    return 2131889644;
                }
                return 2131889650;
            }
        }
    }
    
    public void initUsbPreference(final Context context) {
        (this.mUsbPreference = new Preference(context, null)).setTitle(2131889639);
        this.mUsbPreference.setIcon(2131231187);
        this.mUsbPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$ConnectedUsbDeviceUpdater$qas_74KUD2s0js4DMK034hpC0Q4(this));
        this.forceUpdate();
    }
    
    public void registerCallback() {
        this.mUsbReceiver.register();
    }
    
    public void unregisterCallback() {
        this.mUsbReceiver.unregister();
    }
}
