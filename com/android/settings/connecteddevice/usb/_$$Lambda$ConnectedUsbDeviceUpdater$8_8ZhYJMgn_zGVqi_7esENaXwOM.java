package com.android.settings.connecteddevice.usb;

import android.util.AttributeSet;
import com.android.settings.core.SubSettingLauncher;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.connecteddevice.DevicePreferenceCallback;

public final class _$$Lambda$ConnectedUsbDeviceUpdater$8_8ZhYJMgn_zGVqi_7esENaXwOM implements UsbConnectionListener
{
    @Override
    public final void onUsbConnectionChanged(final boolean b, final long n, final int n2, final int n3) {
        ConnectedUsbDeviceUpdater.lambda$new$0(this.f$0, b, n, n2, n3);
    }
}
