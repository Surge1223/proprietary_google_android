package com.android.settings.connecteddevice.usb;

import android.os.Handler;
import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;

public class UsbDetailsPowerRoleController extends UsbDetailsController implements OnPreferenceClickListener
{
    private final Runnable mFailureCallback;
    private int mNextPowerRole;
    private PreferenceCategory mPreferenceCategory;
    private SwitchPreference mSwitchPreference;
    
    public UsbDetailsPowerRoleController(final Context context, final UsbDetailsFragment usbDetailsFragment, final UsbBackend usbBackend) {
        super(context, usbDetailsFragment, usbBackend);
        this.mFailureCallback = new _$$Lambda$UsbDetailsPowerRoleController$jiVF0c0jApWPiJapsUjjyYudYlM(this);
        this.mNextPowerRole = 0;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreferenceCategory = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey());
        (this.mSwitchPreference = new SwitchPreference(this.mPreferenceCategory.getContext())).setTitle(2131889667);
        this.mSwitchPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        this.mPreferenceCategory.addPreference(this.mSwitchPreference);
    }
    
    @Override
    public String getPreferenceKey() {
        return "usb_details_power_role";
    }
    
    @Override
    public boolean isAvailable() {
        return Utils.isMonkeyRunning() ^ true;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        int n;
        if (this.mSwitchPreference.isChecked()) {
            n = 1;
        }
        else {
            n = 2;
        }
        if (this.mUsbBackend.getPowerRole() != n && this.mNextPowerRole == 0 && !Utils.isMonkeyRunning()) {
            this.mUsbBackend.setPowerRole(n);
            this.mNextPowerRole = n;
            this.mSwitchPreference.setSummary(2131889653);
            final Handler mHandler = this.mHandler;
            final Runnable mFailureCallback = this.mFailureCallback;
            long n2;
            if (this.mUsbBackend.areAllRolesSupported()) {
                n2 = 3000L;
            }
            else {
                n2 = 15000L;
            }
            mHandler.postDelayed(mFailureCallback, n2);
        }
        this.mSwitchPreference.setChecked(this.mSwitchPreference.isChecked() ^ true);
        return true;
    }
    
    @Override
    protected void refresh(final boolean b, final long n, final int n2, final int n3) {
        if (b && !this.mUsbBackend.areAllRolesSupported()) {
            this.mFragment.getPreferenceScreen().removePreference(this.mPreferenceCategory);
        }
        else if (b && this.mUsbBackend.areAllRolesSupported()) {
            this.mFragment.getPreferenceScreen().addPreference(this.mPreferenceCategory);
        }
        if (n2 == 1) {
            this.mSwitchPreference.setChecked(true);
            this.mPreferenceCategory.setEnabled(true);
        }
        else if (n2 == 2) {
            this.mSwitchPreference.setChecked(false);
            this.mPreferenceCategory.setEnabled(true);
        }
        else if (!b || n2 == 0) {
            this.mPreferenceCategory.setEnabled(false);
            if (this.mNextPowerRole == 0) {
                this.mSwitchPreference.setSummary("");
            }
        }
        if (this.mNextPowerRole != 0 && n2 != 0) {
            if (this.mNextPowerRole == n2) {
                this.mSwitchPreference.setSummary("");
            }
            else {
                this.mSwitchPreference.setSummary(2131889654);
            }
            this.mNextPowerRole = 0;
            this.mHandler.removeCallbacks(this.mFailureCallback);
        }
    }
}
