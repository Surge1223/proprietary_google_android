package com.android.settings.connecteddevice.usb;

import android.net.ConnectivityManager$OnStartTetheringCallback;
import com.android.settings.Utils;
import android.support.v14.preference.PreferenceFragment;
import com.android.settingslib.widget.FooterPreferenceMixin;
import android.os.Bundle;
import android.content.Context;
import java.util.Iterator;
import java.util.ArrayList;
import android.graphics.drawable.Drawable;
import com.google.android.collect.Lists;
import com.android.settingslib.widget.CandidateInfo;
import java.util.List;
import com.android.internal.annotations.VisibleForTesting;
import android.net.ConnectivityManager;
import com.android.settings.widget.RadioButtonPickerFragment;

public class UsbDefaultFragment extends RadioButtonPickerFragment
{
    @VisibleForTesting
    ConnectivityManager mConnectivityManager;
    @VisibleForTesting
    OnStartTetheringCallback mOnStartTetheringCallback;
    @VisibleForTesting
    long mPreviousFunctions;
    @VisibleForTesting
    UsbBackend mUsbBackend;
    
    public UsbDefaultFragment() {
        this.mOnStartTetheringCallback = new OnStartTetheringCallback();
    }
    
    @Override
    protected List<? extends CandidateInfo> getCandidates() {
        final ArrayList arrayList = Lists.newArrayList();
        for (final long longValue : UsbDetailsFunctionsController.FUNCTIONS_MAP.keySet()) {
            final String string = this.getContext().getString((int)UsbDetailsFunctionsController.FUNCTIONS_MAP.get(longValue));
            final String usbFunctionsToString = UsbBackend.usbFunctionsToString(longValue);
            if (this.mUsbBackend.areFunctionsSupported(longValue)) {
                arrayList.add(new CandidateInfo(true) {
                    @Override
                    public String getKey() {
                        return usbFunctionsToString;
                    }
                    
                    @Override
                    public Drawable loadIcon() {
                        return null;
                    }
                    
                    @Override
                    public CharSequence loadLabel() {
                        return string;
                    }
                });
            }
        }
        return (List<? extends CandidateInfo>)arrayList;
    }
    
    @Override
    protected String getDefaultKey() {
        return UsbBackend.usbFunctionsToString(this.mUsbBackend.getDefaultUsbFunctions());
    }
    
    @Override
    public int getMetricsCategory() {
        return 1312;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082852;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mUsbBackend = new UsbBackend(context);
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class);
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        super.onCreatePreferences(bundle, s);
        new FooterPreferenceMixin(this, this.getLifecycle()).createFooterPreference().setTitle(2131889633);
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        final long usbFunctionsFromString = UsbBackend.usbFunctionsFromString(s);
        this.mPreviousFunctions = this.mUsbBackend.getCurrentFunctions();
        if (!Utils.isMonkeyRunning()) {
            if (usbFunctionsFromString == 32L) {
                this.mConnectivityManager.startTethering(1, true, (ConnectivityManager$OnStartTetheringCallback)this.mOnStartTetheringCallback);
            }
            else {
                this.mUsbBackend.setDefaultUsbFunctions(usbFunctionsFromString);
            }
        }
        return true;
    }
    
    @VisibleForTesting
    final class OnStartTetheringCallback extends ConnectivityManager$OnStartTetheringCallback
    {
        public void onTetheringFailed() {
            super.onTetheringFailed();
            UsbDefaultFragment.this.mUsbBackend.setDefaultUsbFunctions(UsbDefaultFragment.this.mPreviousFunctions);
            UsbDefaultFragment.this.updateCandidates();
        }
        
        public void onTetheringStarted() {
            super.onTetheringStarted();
            UsbDefaultFragment.this.mUsbBackend.setDefaultUsbFunctions(32L);
        }
    }
}
