package com.android.settings.connecteddevice.usb;

import android.os.Handler;
import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.support.v7.preference.PreferenceCategory;
import com.android.settings.widget.RadioButtonPreference;

public class UsbDetailsDataRoleController extends UsbDetailsController implements OnClickListener
{
    private RadioButtonPreference mDevicePref;
    private final Runnable mFailureCallback;
    private RadioButtonPreference mHostPref;
    private RadioButtonPreference mNextRolePref;
    private PreferenceCategory mPreferenceCategory;
    
    public UsbDetailsDataRoleController(final Context context, final UsbDetailsFragment usbDetailsFragment, final UsbBackend usbBackend) {
        super(context, usbDetailsFragment, usbBackend);
        this.mFailureCallback = new _$$Lambda$UsbDetailsDataRoleController$cU_Vca_1LUjTmehDhPZv_qMdSP8(this);
    }
    
    private RadioButtonPreference makeRadioPreference(final String key, final int title) {
        final RadioButtonPreference radioButtonPreference = new RadioButtonPreference(this.mPreferenceCategory.getContext());
        radioButtonPreference.setKey(key);
        radioButtonPreference.setTitle(title);
        radioButtonPreference.setOnClickListener((RadioButtonPreference.OnClickListener)this);
        this.mPreferenceCategory.addPreference(radioButtonPreference);
        return radioButtonPreference;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreferenceCategory = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey());
        this.mHostPref = this.makeRadioPreference(UsbBackend.dataRoleToString(1), 2131889631);
        this.mDevicePref = this.makeRadioPreference(UsbBackend.dataRoleToString(2), 2131889630);
    }
    
    @Override
    public String getPreferenceKey() {
        return "usb_details_data_role";
    }
    
    @Override
    public boolean isAvailable() {
        return Utils.isMonkeyRunning() ^ true;
    }
    
    @Override
    public void onRadioButtonClicked(final RadioButtonPreference mNextRolePref) {
        final int dataRoleFromString = UsbBackend.dataRoleFromString(mNextRolePref.getKey());
        if (dataRoleFromString != this.mUsbBackend.getDataRole() && this.mNextRolePref == null && !Utils.isMonkeyRunning()) {
            this.mUsbBackend.setDataRole(dataRoleFromString);
            (this.mNextRolePref = mNextRolePref).setSummary(2131889653);
            final Handler mHandler = this.mHandler;
            final Runnable mFailureCallback = this.mFailureCallback;
            long n;
            if (this.mUsbBackend.areAllRolesSupported()) {
                n = 3000L;
            }
            else {
                n = 15000L;
            }
            mHandler.postDelayed(mFailureCallback, n);
        }
    }
    
    @Override
    protected void refresh(final boolean b, final long n, final int n2, final int n3) {
        if (n3 == 2) {
            this.mDevicePref.setChecked(true);
            this.mHostPref.setChecked(false);
            this.mPreferenceCategory.setEnabled(true);
        }
        else if (n3 == 1) {
            this.mDevicePref.setChecked(false);
            this.mHostPref.setChecked(true);
            this.mPreferenceCategory.setEnabled(true);
        }
        else if (!b || n3 == 0) {
            this.mPreferenceCategory.setEnabled(false);
            if (this.mNextRolePref == null) {
                this.mHostPref.setSummary("");
                this.mDevicePref.setSummary("");
            }
        }
        if (this.mNextRolePref != null && n3 != 0) {
            if (UsbBackend.dataRoleFromString(this.mNextRolePref.getKey()) == n3) {
                this.mNextRolePref.setSummary("");
            }
            else {
                this.mNextRolePref.setSummary(2131889654);
            }
            this.mNextRolePref = null;
            this.mHandler.removeCallbacks(this.mFailureCallback);
        }
    }
}
