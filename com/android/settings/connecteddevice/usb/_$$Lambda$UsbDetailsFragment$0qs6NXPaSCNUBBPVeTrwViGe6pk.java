package com.android.settings.connecteddevice.usb;

import android.os.Bundle;
import android.arch.lifecycle.LifecycleObserver;
import java.util.Iterator;
import com.google.android.collect.Lists;
import android.provider.SearchIndexableResource;
import java.util.Collection;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import java.util.List;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public final class _$$Lambda$UsbDetailsFragment$0qs6NXPaSCNUBBPVeTrwViGe6pk implements UsbConnectionListener
{
    @Override
    public final void onUsbConnectionChanged(final boolean b, final long n, final int n2, final int n3) {
        UsbDetailsFragment.lambda$new$0(this.f$0, b, n, n2, n3);
    }
}
