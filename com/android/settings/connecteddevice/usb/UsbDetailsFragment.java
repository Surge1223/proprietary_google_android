package com.android.settings.connecteddevice.usb;

import android.os.Bundle;
import android.arch.lifecycle.LifecycleObserver;
import java.util.Iterator;
import com.google.android.collect.Lists;
import android.provider.SearchIndexableResource;
import java.util.Collection;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import java.util.List;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class UsbDetailsFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private static final String TAG;
    private List<UsbDetailsController> mControllers;
    private UsbBackend mUsbBackend;
    private UsbConnectionBroadcastReceiver.UsbConnectionListener mUsbConnectionListener;
    UsbConnectionBroadcastReceiver mUsbReceiver;
    
    static {
        TAG = UsbDetailsFragment.class.getSimpleName();
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return new ArrayList<AbstractPreferenceController>(createControllerList(context, new UsbBackend(context), null));
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                return super.getNonIndexableKeys(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082853;
                return (List<SearchIndexableResource>)Lists.newArrayList((Object[])new SearchIndexableResource[] { searchIndexableResource });
            }
        };
    }
    
    public UsbDetailsFragment() {
        this.mUsbConnectionListener = new _$$Lambda$UsbDetailsFragment$0qs6NXPaSCNUBBPVeTrwViGe6pk(this);
    }
    
    private static List<UsbDetailsController> createControllerList(final Context context, final UsbBackend usbBackend, final UsbDetailsFragment usbDetailsFragment) {
        final ArrayList<UsbDetailsDataRoleController> list = (ArrayList<UsbDetailsDataRoleController>)new ArrayList<UsbDetailsPowerRoleController>();
        list.add((UsbDetailsPowerRoleController)new UsbDetailsHeaderController(context, usbDetailsFragment, usbBackend));
        list.add((UsbDetailsPowerRoleController)new UsbDetailsDataRoleController(context, usbDetailsFragment, usbBackend));
        list.add((UsbDetailsPowerRoleController)new UsbDetailsFunctionsController(context, usbDetailsFragment, usbBackend));
        list.add(new UsbDetailsPowerRoleController(context, usbDetailsFragment, usbBackend));
        return (List<UsbDetailsController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        this.mUsbBackend = new UsbBackend(context);
        this.mControllers = createControllerList(context, this.mUsbBackend, this);
        this.mUsbReceiver = new UsbConnectionBroadcastReceiver(context, this.mUsbConnectionListener, this.mUsbBackend);
        this.getLifecycle().addObserver(this.mUsbReceiver);
        return new ArrayList<AbstractPreferenceController>(this.mControllers);
    }
    
    @Override
    protected String getLogTag() {
        return UsbDetailsFragment.TAG;
    }
    
    @Override
    public int getMetricsCategory() {
        return 1291;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082853;
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        super.onCreatePreferences(bundle, s);
    }
}
