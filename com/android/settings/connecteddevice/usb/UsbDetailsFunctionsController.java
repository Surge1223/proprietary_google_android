package com.android.settings.connecteddevice.usb;

import java.util.Iterator;
import android.net.ConnectivityManager$OnStartTetheringCallback;
import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.content.Context;
import java.util.LinkedHashMap;
import android.support.v7.preference.PreferenceCategory;
import android.net.ConnectivityManager;
import java.util.Map;
import com.android.settings.widget.RadioButtonPreference;

public class UsbDetailsFunctionsController extends UsbDetailsController implements OnClickListener
{
    static final Map<Long, Integer> FUNCTIONS_MAP;
    private ConnectivityManager mConnectivityManager;
    OnStartTetheringCallback mOnStartTetheringCallback;
    long mPreviousFunction;
    private PreferenceCategory mProfilesContainer;
    
    static {
        (FUNCTIONS_MAP = new LinkedHashMap<Long, Integer>()).put(4L, 2131889663);
        UsbDetailsFunctionsController.FUNCTIONS_MAP.put(32L, 2131889668);
        UsbDetailsFunctionsController.FUNCTIONS_MAP.put(8L, 2131889659);
        UsbDetailsFunctionsController.FUNCTIONS_MAP.put(16L, 2131889665);
        UsbDetailsFunctionsController.FUNCTIONS_MAP.put(0L, 2131889661);
    }
    
    public UsbDetailsFunctionsController(final Context context, final UsbDetailsFragment usbDetailsFragment, final UsbBackend usbBackend) {
        super(context, usbDetailsFragment, usbBackend);
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class);
        this.mOnStartTetheringCallback = new OnStartTetheringCallback();
        this.mPreviousFunction = this.mUsbBackend.getCurrentFunctions();
    }
    
    private RadioButtonPreference getProfilePreference(final String key, final int title) {
        RadioButtonPreference radioButtonPreference;
        if ((radioButtonPreference = (RadioButtonPreference)this.mProfilesContainer.findPreference(key)) == null) {
            radioButtonPreference = new RadioButtonPreference(this.mProfilesContainer.getContext());
            radioButtonPreference.setKey(key);
            radioButtonPreference.setTitle(title);
            radioButtonPreference.setOnClickListener((RadioButtonPreference.OnClickListener)this);
            this.mProfilesContainer.addPreference(radioButtonPreference);
        }
        return radioButtonPreference;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mProfilesContainer = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "usb_details_functions";
    }
    
    @Override
    public boolean isAvailable() {
        return Utils.isMonkeyRunning() ^ true;
    }
    
    @Override
    public void onRadioButtonClicked(final RadioButtonPreference radioButtonPreference) {
        final long usbFunctionsFromString = UsbBackend.usbFunctionsFromString(radioButtonPreference.getKey());
        final long currentFunctions = this.mUsbBackend.getCurrentFunctions();
        if (usbFunctionsFromString != currentFunctions && !Utils.isMonkeyRunning()) {
            this.mPreviousFunction = currentFunctions;
            if (usbFunctionsFromString == 32L) {
                final RadioButtonPreference radioButtonPreference2 = (RadioButtonPreference)this.mProfilesContainer.findPreference(UsbBackend.usbFunctionsToString(this.mPreviousFunction));
                if (radioButtonPreference2 != null) {
                    radioButtonPreference2.setChecked(false);
                    radioButtonPreference.setChecked(true);
                }
                this.mConnectivityManager.startTethering(1, true, (ConnectivityManager$OnStartTetheringCallback)this.mOnStartTetheringCallback);
            }
            else {
                this.mUsbBackend.setCurrentFunctions(usbFunctionsFromString);
            }
        }
    }
    
    @Override
    protected void refresh(final boolean b, final long n, int intValue, final int n2) {
        if (b && n2 == 2) {
            this.mProfilesContainer.setEnabled(true);
        }
        else {
            this.mProfilesContainer.setEnabled(false);
        }
        for (final long longValue : UsbDetailsFunctionsController.FUNCTIONS_MAP.keySet()) {
            intValue = UsbDetailsFunctionsController.FUNCTIONS_MAP.get(longValue);
            final RadioButtonPreference profilePreference = this.getProfilePreference(UsbBackend.usbFunctionsToString(longValue), intValue);
            if (this.mUsbBackend.areFunctionsSupported(longValue)) {
                profilePreference.setChecked(n == longValue);
            }
            else {
                this.mProfilesContainer.removePreference(profilePreference);
            }
        }
    }
    
    final class OnStartTetheringCallback extends ConnectivityManager$OnStartTetheringCallback
    {
        public void onTetheringFailed() {
            super.onTetheringFailed();
            UsbDetailsFunctionsController.this.mUsbBackend.setCurrentFunctions(UsbDetailsFunctionsController.this.mPreviousFunction);
        }
    }
}
