package com.android.settings.connecteddevice;

import android.content.ContentResolver;
import android.provider.Settings;
import android.util.FeatureFlagUtils;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.TogglePreferenceController;

public class BluetoothOnWhileDrivingPreferenceController extends TogglePreferenceController implements PreferenceControllerMixin
{
    static final String KEY_BLUETOOTH_ON_DRIVING = "bluetooth_on_while_driving";
    
    public BluetoothOnWhileDrivingPreferenceController(final Context context) {
        super(context, "bluetooth_on_while_driving");
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (FeatureFlagUtils.isEnabled(this.mContext, "settings_bluetooth_while_driving")) {
            return 0;
        }
        return 1;
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, "bluetooth_on_while_driving", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.Secure.putInt(this.mContext.getContentResolver(), "bluetooth_on_while_driving", (int)(b ? 1 : 0));
    }
}
