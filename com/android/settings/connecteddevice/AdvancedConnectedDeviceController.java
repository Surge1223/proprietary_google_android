package com.android.settings.connecteddevice;

import android.provider.Settings;
import com.android.settings.nfc.NfcPreferenceController;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class AdvancedConnectedDeviceController extends BasePreferenceController
{
    private static final String DRIVING_MODE_SETTINGS_ENABLED = "gearhead:driving_mode_settings_enabled";
    
    public AdvancedConnectedDeviceController(final Context context, final String s) {
        super(context, s);
    }
    
    public static int getConnectedDevicesSummaryResourceId(final Context context) {
        return getConnectedDevicesSummaryResourceId(new NfcPreferenceController(context, "toggle_nfc"), isDrivingModeAvailable(context));
    }
    
    static int getConnectedDevicesSummaryResourceId(final NfcPreferenceController nfcPreferenceController, final boolean b) {
        int n;
        if (nfcPreferenceController.isAvailable()) {
            if (b) {
                n = 2131887122;
            }
            else {
                n = 2131887120;
            }
        }
        else if (b) {
            n = 2131887121;
        }
        else {
            n = 2131887119;
        }
        return n;
    }
    
    static boolean isDrivingModeAvailable(final Context context) {
        final int int1 = Settings.System.getInt(context.getContentResolver(), "gearhead:driving_mode_settings_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mContext.getText(getConnectedDevicesSummaryResourceId(this.mContext));
    }
}
