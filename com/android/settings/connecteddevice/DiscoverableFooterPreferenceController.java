package com.android.settings.connecteddevice;

import android.content.IntentFilter;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.dashboard.DashboardFragment;
import android.content.Intent;
import android.text.BidiFormatter;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.bluetooth.Utils;
import android.content.Context;
import com.android.settingslib.widget.FooterPreference;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import com.android.settingslib.widget.FooterPreferenceMixin;
import com.android.internal.annotations.VisibleForTesting;
import android.content.BroadcastReceiver;
import com.android.settings.bluetooth.AlwaysDiscoverable;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class DiscoverableFooterPreferenceController extends BasePreferenceController implements LifecycleObserver, OnPause, OnResume
{
    private static final String KEY = "discoverable_footer_preference";
    private AlwaysDiscoverable mAlwaysDiscoverable;
    @VisibleForTesting
    BroadcastReceiver mBluetoothChangedReceiver;
    private FooterPreferenceMixin mFooterPreferenceMixin;
    private LocalBluetoothAdapter mLocalAdapter;
    private LocalBluetoothManager mLocalManager;
    private FooterPreference mPreference;
    
    public DiscoverableFooterPreferenceController(final Context context) {
        super(context, "discoverable_footer_preference");
        this.mLocalManager = Utils.getLocalBtManager(context);
        if (this.mLocalManager == null) {
            return;
        }
        this.mLocalAdapter = this.mLocalManager.getBluetoothAdapter();
        this.mAlwaysDiscoverable = new AlwaysDiscoverable(context, this.mLocalAdapter);
        this.initReceiver();
    }
    
    private void addFooterPreference(final PreferenceScreen preferenceScreen) {
        (this.mPreference = this.mFooterPreferenceMixin.createFooterPreference()).setKey("discoverable_footer_preference");
        preferenceScreen.addPreference(this.mPreference);
    }
    
    private CharSequence getPreferenceTitle() {
        final String name = this.mLocalAdapter.getName();
        if (TextUtils.isEmpty((CharSequence)name)) {
            return null;
        }
        return TextUtils.expandTemplate(this.mContext.getText(2131886742), new CharSequence[] { BidiFormatter.getInstance().unicodeWrap(name) });
    }
    
    private void initReceiver() {
        this.mBluetoothChangedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    DiscoverableFooterPreferenceController.this.updateFooterPreferenceTitle(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE));
                }
            }
        };
    }
    
    private void updateFooterPreferenceTitle(final int n) {
        if (n == 12) {
            this.mPreference.setTitle(this.getPreferenceTitle());
        }
        else {
            this.mPreference.setTitle(2131886808);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.addFooterPreference(preferenceScreen);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    public void init(final DashboardFragment dashboardFragment) {
        this.mFooterPreferenceMixin = new FooterPreferenceMixin(dashboardFragment, dashboardFragment.getLifecycle());
    }
    
    @VisibleForTesting
    void init(final FooterPreferenceMixin mFooterPreferenceMixin, final FooterPreference mPreference, final AlwaysDiscoverable mAlwaysDiscoverable) {
        this.mFooterPreferenceMixin = mFooterPreferenceMixin;
        this.mPreference = mPreference;
        this.mAlwaysDiscoverable = mAlwaysDiscoverable;
    }
    
    @Override
    public void onPause() {
        this.mContext.unregisterReceiver(this.mBluetoothChangedReceiver);
        this.mAlwaysDiscoverable.stop();
    }
    
    @Override
    public void onResume() {
        this.mContext.registerReceiver(this.mBluetoothChangedReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.mAlwaysDiscoverable.start();
        this.updateFooterPreferenceTitle(this.mLocalAdapter.getState());
    }
}
