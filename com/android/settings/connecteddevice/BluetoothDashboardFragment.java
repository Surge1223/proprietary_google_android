package com.android.settings.connecteddevice;

import android.app.Fragment;
import com.android.settings.bluetooth.BluetoothDeviceRenamePreferenceController;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.widget.SwitchBarController;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import android.bluetooth.BluetoothManager;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.widget.SwitchBar;
import com.android.settingslib.widget.FooterPreference;
import com.android.settings.bluetooth.BluetoothSwitchPreferenceController;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class BluetoothDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private BluetoothSwitchPreferenceController mController;
    private FooterPreference mFooterPreference;
    private SwitchBar mSwitchBar;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                final BluetoothManager bluetoothManager = (BluetoothManager)context.getSystemService("bluetooth");
                if (bluetoothManager != null) {
                    int n;
                    if (bluetoothManager.getAdapter() != null) {
                        n = 0;
                    }
                    else {
                        n = 2;
                    }
                    if (n != 0) {
                        nonIndexableKeys.add("bluetooth_switchbar_screen");
                    }
                }
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.title = context.getString(2131886896);
                searchIndexableRaw.screenTitle = context.getString(2131886896);
                searchIndexableRaw.keywords = context.getString(2131887947);
                searchIndexableRaw.key = "bluetooth_switchbar_screen";
                list.add(searchIndexableRaw);
                return list;
            }
        };
    }
    
    @Override
    public int getHelpResource() {
        return 2131887774;
    }
    
    @Override
    protected String getLogTag() {
        return "BluetoothDashboardFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1390;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082727;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        this.mSwitchBar = settingsActivity.getSwitchBar();
        this.mController = new BluetoothSwitchPreferenceController((Context)settingsActivity, new SwitchBarController(this.mSwitchBar), this.mFooterPreference);
        final Lifecycle lifecycle = this.getLifecycle();
        if (lifecycle != null) {
            lifecycle.addObserver(this.mController);
        }
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(BluetoothDeviceRenamePreferenceController.class).setFragment(this);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFooterPreference = this.mFooterPreferenceMixin.createFooterPreference();
    }
}
