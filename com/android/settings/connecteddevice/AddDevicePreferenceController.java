package com.android.settings.connecteddevice;

import android.support.v7.preference.PreferenceScreen;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.support.v7.preference.Preference;
import android.content.IntentFilter;
import android.bluetooth.BluetoothAdapter;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class AddDevicePreferenceController extends BasePreferenceController implements LifecycleObserver, OnStart, OnStop
{
    private BluetoothAdapter mBluetoothAdapter;
    private IntentFilter mIntentFilter;
    private Preference mPreference;
    private final BroadcastReceiver mReceiver;
    
    public AddDevicePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                AddDevicePreferenceController.this.updateState();
            }
        };
        this.mIntentFilter = new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED");
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        String string;
        if (this.mBluetoothAdapter != null && this.mBluetoothAdapter.isEnabled()) {
            string = "";
        }
        else {
            string = this.mContext.getString(2131887111);
        }
        return string;
    }
    
    @Override
    public void onStart() {
        this.mContext.registerReceiver(this.mReceiver, this.mIntentFilter);
    }
    
    @Override
    public void onStop() {
        this.mContext.unregisterReceiver(this.mReceiver);
    }
    
    void updateState() {
        this.updateState(this.mPreference);
    }
}
