package com.android.settings.connecteddevice;

import android.support.v7.preference.Preference;

public interface DevicePreferenceCallback
{
    void onDeviceAdded(final Preference p0);
    
    void onDeviceRemoved(final Preference p0);
}
