package com.android.settings.connecteddevice;

import android.arch.lifecycle.LifecycleObserver;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.search.BaseSearchIndexProvider;
import android.content.Context;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class ConnectedDeviceDashboardFragment extends DashboardFragment
{
    static final String KEY_AVAILABLE_DEVICES = "available_device_list";
    static final String KEY_CONNECTED_DEVICES = "connected_device_list";
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new ConnectedDeviceDashboardFragment.SummaryProvider((Context)activity, summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("available_device_list");
                nonIndexableKeys.add("connected_device_list");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082735;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<DiscoverableFooterPreferenceController> list = (ArrayList<DiscoverableFooterPreferenceController>)new ArrayList<AbstractPreferenceController>();
        final DiscoverableFooterPreferenceController discoverableFooterPreferenceController = new DiscoverableFooterPreferenceController(context);
        list.add(discoverableFooterPreferenceController);
        if (lifecycle != null) {
            lifecycle.addObserver(discoverableFooterPreferenceController);
        }
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887804;
    }
    
    @Override
    protected String getLogTag() {
        return "ConnectedDeviceFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 747;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082735;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(AvailableMediaDeviceGroupController.class).init(this);
        this.use(ConnectedDeviceGroupController.class).init(this);
        this.use(PreviouslyConnectedDevicePreferenceController.class).init(this);
        this.use(DiscoverableFooterPreferenceController.class).init(this);
    }
    
    static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getText(AdvancedConnectedDeviceController.getConnectedDevicesSummaryResourceId(this.mContext)));
            }
        }
    }
}
