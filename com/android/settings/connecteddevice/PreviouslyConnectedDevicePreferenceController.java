package com.android.settings.connecteddevice;

import com.android.settings.bluetooth.SavedBluetoothDeviceUpdater;
import com.android.settings.dashboard.DashboardFragment;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.connecteddevice.dock.DockUpdater;
import android.support.v7.preference.Preference;
import com.android.settings.bluetooth.BluetoothDeviceUpdater;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class PreviouslyConnectedDevicePreferenceController extends BasePreferenceController implements DevicePreferenceCallback, LifecycleObserver, OnStart, OnStop
{
    private BluetoothDeviceUpdater mBluetoothDeviceUpdater;
    private Preference mPreference;
    private int mPreferenceSize;
    private DockUpdater mSavedDockUpdater;
    
    public PreviouslyConnectedDevicePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mSavedDockUpdater = FeatureFactory.getFactory(context).getDockUpdaterFeatureProvider().getSavedDockUpdater(context, this);
    }
    
    private void updatePreferenceOnSizeChanged() {
        if (this.isAvailable()) {
            this.mPreference.setEnabled(this.mPreferenceSize != 0);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
            this.mBluetoothDeviceUpdater.setPrefContext(preferenceScreen.getContext());
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        return this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth") ? 0 : 1;
    }
    
    public void init(final DashboardFragment dashboardFragment) {
        this.mBluetoothDeviceUpdater = new SavedBluetoothDeviceUpdater(dashboardFragment.getContext(), dashboardFragment, this);
    }
    
    @Override
    public void onDeviceAdded(final Preference preference) {
        ++this.mPreferenceSize;
        this.updatePreferenceOnSizeChanged();
    }
    
    @Override
    public void onDeviceRemoved(final Preference preference) {
        --this.mPreferenceSize;
        this.updatePreferenceOnSizeChanged();
    }
    
    @Override
    public void onStart() {
        this.mBluetoothDeviceUpdater.registerCallback();
        this.mSavedDockUpdater.registerCallback();
        this.updatePreferenceOnSizeChanged();
    }
    
    @Override
    public void onStop() {
        this.mBluetoothDeviceUpdater.unregisterCallback();
        this.mSavedDockUpdater.unregisterCallback();
    }
    
    void setBluetoothDeviceUpdater(final BluetoothDeviceUpdater mBluetoothDeviceUpdater) {
        this.mBluetoothDeviceUpdater = mBluetoothDeviceUpdater;
    }
    
    void setPreference(final Preference mPreference) {
        this.mPreference = mPreference;
    }
    
    void setPreferenceSize(final int mPreferenceSize) {
        this.mPreferenceSize = mPreferenceSize;
    }
    
    void setSavedDockUpdater(final DockUpdater mSavedDockUpdater) {
        this.mSavedDockUpdater = mSavedDockUpdater;
    }
}
