package com.android.settings;

import com.android.internal.app.AlertController$AlertParams;
import android.content.pm.PackageManager;
import android.content.Context;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Process;
import android.util.Log;
import android.content.Intent;
import android.content.DialogInterface;
import android.os.UserHandle;
import android.content.ComponentName;
import android.os.Bundle;
import android.appwidget.AppWidgetManager;
import android.widget.CheckBox;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public class AllowBindAppWidgetActivity extends AlertActivity implements DialogInterface$OnClickListener
{
    private CheckBox mAlwaysUse;
    private int mAppWidgetId;
    private AppWidgetManager mAppWidgetManager;
    private Bundle mBindOptions;
    private String mCallingPackage;
    private boolean mClicked;
    private ComponentName mComponentName;
    private UserHandle mProfile;
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        this.mClicked = true;
        if (n == -1 && this.mAppWidgetId != -1 && this.mComponentName != null && this.mCallingPackage != null) {
            try {
                if (this.mAppWidgetManager.bindAppWidgetIdIfAllowed(this.mAppWidgetId, this.mProfile, this.mComponentName, this.mBindOptions)) {
                    final Intent intent = new Intent();
                    intent.putExtra("appWidgetId", this.mAppWidgetId);
                    this.setResult(-1, intent);
                }
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error binding widget with id ");
                sb.append(this.mAppWidgetId);
                sb.append(" and component ");
                sb.append(this.mComponentName);
                Log.v("BIND_APPWIDGET", sb.toString());
            }
            final boolean checked = this.mAlwaysUse.isChecked();
            if (checked != this.mAppWidgetManager.hasBindAppWidgetPermission(this.mCallingPackage)) {
                this.mAppWidgetManager.setBindAppWidgetPermission(this.mCallingPackage, checked);
            }
        }
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setResult(0);
        final Intent intent = this.getIntent();
        CharSequence applicationLabel = "";
        if (intent != null) {
            try {
                this.mAppWidgetId = intent.getIntExtra("appWidgetId", -1);
                this.mProfile = (UserHandle)intent.getParcelableExtra("appWidgetProviderProfile");
                if (this.mProfile == null) {
                    this.mProfile = Process.myUserHandle();
                }
                this.mComponentName = (ComponentName)intent.getParcelableExtra("appWidgetProvider");
                this.mBindOptions = (Bundle)intent.getParcelableExtra("appWidgetOptions");
                this.mCallingPackage = this.getCallingPackage();
                final PackageManager packageManager = this.getPackageManager();
                applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.mCallingPackage, 0));
            }
            catch (Exception ex) {
                this.mAppWidgetId = -1;
                this.mComponentName = null;
                this.mCallingPackage = null;
                Log.v("BIND_APPWIDGET", "Error getting parameters");
                this.finish();
                return;
            }
        }
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        mAlertParams.mTitle = this.getString(2131886287);
        mAlertParams.mMessage = this.getString(2131886286, new Object[] { applicationLabel });
        mAlertParams.mPositiveButtonText = this.getString(2131887137);
        mAlertParams.mNegativeButtonText = this.getString(17039360);
        mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
        mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
        mAlertParams.mView = ((LayoutInflater)this.getSystemService("layout_inflater")).inflate(17367090, (ViewGroup)null);
        (this.mAlwaysUse = (CheckBox)mAlertParams.mView.findViewById(16908711)).setText((CharSequence)this.getString(2131886288, new Object[] { applicationLabel }));
        this.mAlwaysUse.setPadding(this.mAlwaysUse.getPaddingLeft(), this.mAlwaysUse.getPaddingTop(), this.mAlwaysUse.getPaddingRight(), (int)(this.mAlwaysUse.getPaddingBottom() + this.getResources().getDimension(2131165315)));
        this.mAppWidgetManager = AppWidgetManager.getInstance((Context)this);
        this.mAlwaysUse.setChecked(this.mAppWidgetManager.hasBindAppWidgetPermission(this.mCallingPackage, this.mProfile.getIdentifier()));
        this.setupAlert();
    }
    
    protected void onPause() {
        if (!this.mClicked) {
            this.finish();
        }
        super.onPause();
    }
}
