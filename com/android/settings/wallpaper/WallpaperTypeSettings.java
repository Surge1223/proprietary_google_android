package com.android.settings.wallpaper;

import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.content.ComponentName;
import android.support.v7.preference.Preference;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public class WallpaperTypeSettings extends SettingsPreferenceFragment implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final Intent intent = new Intent("android.intent.action.SET_WALLPAPER");
                final PackageManager packageManager = context.getPackageManager();
                final List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
                final String string = context.getString(2131887100);
                for (final ResolveInfo resolveInfo : queryIntentActivities) {
                    if (!string.equals(resolveInfo.activityInfo.packageName)) {
                        continue;
                    }
                    CharSequence charSequence;
                    if ((charSequence = resolveInfo.loadLabel(packageManager)) == null) {
                        charSequence = resolveInfo.activityInfo.packageName;
                    }
                    final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                    searchIndexableRaw.title = charSequence.toString();
                    searchIndexableRaw.key = "wallpaper_type_settings";
                    searchIndexableRaw.screenTitle = context.getResources().getString(2131889873);
                    searchIndexableRaw.intentAction = "android.intent.action.SET_WALLPAPER";
                    searchIndexableRaw.intentTargetPackage = resolveInfo.activityInfo.packageName;
                    searchIndexableRaw.intentTargetClass = resolveInfo.activityInfo.name;
                    searchIndexableRaw.keywords = context.getString(2131888001);
                    list.add(searchIndexableRaw);
                }
                return list;
            }
        };
    }
    
    private void populateWallpaperTypes() {
        final Intent intent = new Intent("android.intent.action.SET_WALLPAPER");
        final PackageManager packageManager = this.getPackageManager();
        final List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.setOrderingAsAdded(false);
        for (final ResolveInfo resolveInfo : queryIntentActivities) {
            final Preference preference = new Preference(this.getPrefContext());
            final Intent addFlags = new Intent(intent).addFlags(33554432);
            addFlags.setComponent(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name));
            preference.setIntent(addFlags);
            CharSequence title;
            if ((title = resolveInfo.loadLabel(packageManager)) == null) {
                title = resolveInfo.activityInfo.packageName;
            }
            preference.setTitle(title);
            preference.setIcon(resolveInfo.loadIcon(packageManager));
            preferenceScreen.addPreference(preference);
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887788;
    }
    
    @Override
    public int getMetricsCategory() {
        return 101;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082862);
        this.populateWallpaperTypes();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference.getIntent() == null) {
            return super.onPreferenceTreeClick(preference);
        }
        this.startActivity(preference.getIntent());
        this.finish();
        return true;
    }
}
