package com.android.settings.wallpaper;

import com.android.settings.core.SubSettingLauncher;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.os.Bundle;
import android.app.WallpaperManager;
import android.content.Context;
import android.app.Activity;

public class WallpaperSuggestionActivity extends Activity
{
    public static boolean isSuggestionComplete(final Context context) {
        final boolean wallpaperServiceEnabled = isWallpaperServiceEnabled(context);
        boolean b = true;
        if (!wallpaperServiceEnabled) {
            return true;
        }
        if (((WallpaperManager)context.getSystemService("wallpaper")).getWallpaperId(1) <= 0) {
            b = false;
        }
        return b;
    }
    
    private static boolean isWallpaperServiceEnabled(final Context context) {
        return context.getResources().getBoolean(17956970);
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final PackageManager packageManager = this.getPackageManager();
        final Intent addFlags = new Intent().setClassName(this.getString(2131887100), this.getString(2131887099)).addFlags(33554432);
        if (packageManager.resolveActivity(addFlags, 0) != null) {
            this.startActivity(addFlags);
        }
        else {
            this.startFallbackSuggestion();
        }
        this.finish();
    }
    
    void startFallbackSuggestion() {
        new SubSettingLauncher((Context)this).setDestination(WallpaperTypeSettings.class.getName()).setTitle(2131889878).setSourceMetricsCategory(35).addFlags(33554432).launch();
    }
}
