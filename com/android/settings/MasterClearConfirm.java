package com.android.settings;

import android.content.DialogInterface$OnDismissListener;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.widget.TextView;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.service.oemlock.OemLockManager;
import android.service.persistentdata.PersistentDataBlockManager;
import android.content.Context;
import android.app.ProgressDialog;
import android.view.View.OnClickListener;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;

public class MasterClearConfirm extends InstrumentedFragment
{
    private View mContentView;
    private boolean mEraseEsims;
    private boolean mEraseSdCard;
    private View.OnClickListener mFinalClickListener;
    
    public MasterClearConfirm() {
        this.mFinalClickListener = (View.OnClickListener)new View.OnClickListener() {
            private ProgressDialog getProgressDialog() {
                final ProgressDialog progressDialog = new ProgressDialog((Context)MasterClearConfirm.this.getActivity());
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setTitle((CharSequence)MasterClearConfirm.this.getActivity().getString(2131888245));
                progressDialog.setMessage((CharSequence)MasterClearConfirm.this.getActivity().getString(2131888244));
                return progressDialog;
            }
            
            public void onClick(final View view) {
                if (Utils.isMonkeyRunning()) {
                    return;
                }
                final PersistentDataBlockManager persistentDataBlockManager = (PersistentDataBlockManager)MasterClearConfirm.this.getActivity().getSystemService("persistent_data_block");
                final OemLockManager oemLockManager = (OemLockManager)MasterClearConfirm.this.getActivity().getSystemService("oem_lock");
                if (persistentDataBlockManager != null && !oemLockManager.isOemUnlockAllowed() && Utils.isDeviceProvisioned((Context)MasterClearConfirm.this.getActivity())) {
                    new AsyncTask<Void, Void, Void>() {
                        int mOldOrientation;
                        ProgressDialog mProgressDialog;
                        
                        protected Void doInBackground(final Void... array) {
                            persistentDataBlockManager.wipe();
                            return null;
                        }
                        
                        protected void onPostExecute(final Void void1) {
                            this.mProgressDialog.hide();
                            if (MasterClearConfirm.this.getActivity() != null) {
                                MasterClearConfirm.this.getActivity().setRequestedOrientation(this.mOldOrientation);
                                MasterClearConfirm.this.doMasterClear();
                            }
                        }
                        
                        protected void onPreExecute() {
                            (this.mProgressDialog = MasterClearConfirm$1.this.getProgressDialog()).show();
                            this.mOldOrientation = MasterClearConfirm.this.getActivity().getRequestedOrientation();
                            MasterClearConfirm.this.getActivity().setRequestedOrientation(14);
                        }
                    }.execute((Object[])new Void[0]);
                }
                else {
                    MasterClearConfirm.this.doMasterClear();
                }
            }
        };
    }
    
    private void doMasterClear() {
        final Intent intent = new Intent("android.intent.action.FACTORY_RESET");
        intent.setPackage("android");
        intent.addFlags(268435456);
        intent.putExtra("android.intent.extra.REASON", "MasterClearConfirm");
        intent.putExtra("android.intent.extra.WIPE_EXTERNAL_STORAGE", this.mEraseSdCard);
        intent.putExtra("com.android.internal.intent.extra.WIPE_ESIMS", this.mEraseEsims);
        this.getActivity().sendBroadcast(intent);
    }
    
    private void establishFinalConfirmationState() {
        this.mContentView.findViewById(2131362134).setOnClickListener(this.mFinalClickListener);
    }
    
    private void setAccessibilityTitle() {
        final CharSequence title = this.getActivity().getTitle();
        final TextView textView = (TextView)this.mContentView.findViewById(2131362355);
        if (textView != null) {
            final StringBuilder sb = new StringBuilder(title);
            sb.append(",");
            sb.append(textView.getText());
            this.getActivity().setTitle((CharSequence)Utils.createAccessibleSequence(title, sb.toString()));
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 67;
    }
    
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        arguments = this.getArguments();
        final boolean b = false;
        this.mEraseSdCard = (arguments != null && arguments.getBoolean("erase_sd"));
        boolean mEraseEsims = b;
        if (arguments != null) {
            mEraseEsims = b;
            if (arguments.getBoolean("erase_esim")) {
                mEraseEsims = true;
            }
        }
        this.mEraseEsims = mEraseEsims;
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.getActivity(), "no_factory_reset", UserHandle.myUserId());
        if (RestrictedLockUtils.hasBaseUserRestriction((Context)this.getActivity(), "no_factory_reset", UserHandle.myUserId())) {
            return layoutInflater.inflate(2131558612, (ViewGroup)null);
        }
        if (checkIfRestrictionEnforced != null) {
            new ActionDisabledByAdminDialogHelper(this.getActivity()).prepareDialogBuilder("no_factory_reset", checkIfRestrictionEnforced).setOnDismissListener((DialogInterface$OnDismissListener)new _$$Lambda$MasterClearConfirm$weRgiuD2TQnm7jx9NX__qHWwsHU(this)).show();
            return new View((Context)this.getActivity());
        }
        this.mContentView = layoutInflater.inflate(2131558611, (ViewGroup)null);
        this.establishFinalConfirmationState();
        this.setAccessibilityTitle();
        return this.mContentView;
    }
}
