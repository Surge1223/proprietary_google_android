package com.android.settings;

import android.app.AlertDialog$Builder;
import android.content.Context;
import com.android.settingslib.RestrictedLockUtils;
import android.app.admin.DevicePolicyManager;
import android.os.UserHandle;
import android.os.Bundle;
import android.content.Intent;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import android.app.Activity;

public class MonitoringCertInfoActivity extends Activity implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    private int mUserId;
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final Intent intent = new Intent("com.android.settings.TRUSTED_CREDENTIALS_USER");
        intent.setFlags(335544320);
        intent.putExtra("ARG_SHOW_NEW_FOR_USER", this.mUserId);
        this.startActivity(intent);
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mUserId = this.getIntent().getIntExtra("android.intent.extra.USER_ID", UserHandle.myUserId());
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)this.getSystemService((Class)DevicePolicyManager.class);
        final int intExtra = this.getIntent().getIntExtra("android.settings.extra.number_of_certificates", 1);
        int n;
        if (RestrictedLockUtils.getProfileOrDeviceOwner((Context)this, this.mUserId) != null) {
            n = 2131755068;
        }
        else {
            n = 2131755065;
        }
        final CharSequence quantityText = this.getResources().getQuantityText(n, intExtra);
        this.setTitle(quantityText);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this);
        alertDialog$Builder.setTitle(quantityText);
        alertDialog$Builder.setCancelable(true);
        alertDialog$Builder.setPositiveButton(this.getResources().getQuantityText(2131755068, intExtra), (DialogInterface$OnClickListener)this);
        alertDialog$Builder.setNeutralButton(R.string.cancel, (DialogInterface$OnClickListener)null);
        alertDialog$Builder.setOnDismissListener((DialogInterface$OnDismissListener)this);
        if (devicePolicyManager.getProfileOwnerAsUser(this.mUserId) != null) {
            alertDialog$Builder.setMessage((CharSequence)this.getResources().getQuantityString(2131755066, intExtra, new Object[] { devicePolicyManager.getProfileOwnerNameAsUser(this.mUserId) }));
        }
        else if (devicePolicyManager.getDeviceOwnerComponentOnCallingUser() != null) {
            alertDialog$Builder.setMessage((CharSequence)this.getResources().getQuantityString(2131755067, intExtra, new Object[] { devicePolicyManager.getDeviceOwnerNameOnAnyUser() }));
        }
        else {
            alertDialog$Builder.setIcon(17301624);
            alertDialog$Builder.setMessage(2131889219);
        }
        alertDialog$Builder.show();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        this.finish();
    }
}
