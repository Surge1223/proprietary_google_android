package com.android.settings;

import android.view.View;
import android.widget.ImageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;

public class CancellablePreference extends Preference implements View.OnClickListener
{
    private boolean mCancellable;
    private OnCancelListener mListener;
    
    public CancellablePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setWidgetLayoutResource(2131558479);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131361959);
        int visibility;
        if (this.mCancellable) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        imageView.setVisibility(visibility);
        imageView.setOnClickListener((View.OnClickListener)this);
    }
    
    public void onClick(final View view) {
        if (this.mListener != null) {
            this.mListener.onCancel(this);
        }
    }
    
    public void setCancellable(final boolean mCancellable) {
        this.mCancellable = mCancellable;
        this.notifyChanged();
    }
    
    public void setOnCancelListener(final OnCancelListener mListener) {
        this.mListener = mListener;
    }
    
    public interface OnCancelListener
    {
        void onCancel(final CancellablePreference p0);
    }
}
