package com.android.settings;

import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.Bundle;
import android.telephony.euicc.EuiccManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import android.view.View$OnScrollChangeListener;
import android.os.Environment;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.pm.PackageManager;
import android.accounts.AuthenticatorDescription;
import android.accounts.Account;
import java.util.List;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.content.pm.PackageManager;
import android.content.res.Resources$NotFoundException;
import android.util.Log;
import android.content.pm.UserInfo;
import android.view.LayoutInflater;
import android.accounts.AccountManager;
import android.os.UserHandle;
import android.widget.LinearLayout;
import android.os.UserManager;
import android.os.SystemProperties;
import android.widget.TextView;
import android.view.ViewGroup;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.ScrollView;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.view.View;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import com.android.settings.core.InstrumentedFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;

public final class _$$Lambda$MasterClear$Z9cDw51Fmk6mKl61MZqDIXg0_34 implements DialogInterface$OnDismissListener
{
    public final void onDismiss(final DialogInterface dialogInterface) {
        MasterClear.lambda$onCreateView$0(this.f$0, dialogInterface);
    }
}
