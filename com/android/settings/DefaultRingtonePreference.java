package com.android.settings;

import android.media.RingtoneManager;
import android.net.Uri;
import android.content.Intent;
import android.util.AttributeSet;
import android.content.Context;

public class DefaultRingtonePreference extends RingtonePreference
{
    public DefaultRingtonePreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public void onPrepareRingtonePickerIntent(final Intent intent) {
        super.onPrepareRingtonePickerIntent(intent);
        intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
    }
    
    @Override
    protected Uri onRestoreRingtone() {
        return RingtoneManager.getActualDefaultRingtoneUri(this.mUserContext, this.getRingtoneType());
    }
    
    @Override
    protected void onSaveRingtone(final Uri uri) {
        RingtoneManager.setActualDefaultRingtoneUri(this.mUserContext, this.getRingtoneType(), uri);
    }
}
