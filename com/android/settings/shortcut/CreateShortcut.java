package com.android.settings.shortcut;

import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.pm.ShortcutInfo;
import java.util.ArrayList;
import android.os.AsyncTask;
import com.android.settings.Settings;
import android.net.ConnectivityManager;
import java.util.List;
import android.app.LauncherActivity$ListItem;
import android.widget.ListView;
import android.content.pm.ActivityInfo;
import android.os.Parcelable;
import android.content.Intent$ShortcutIconResource;
import android.content.pm.ShortcutInfo$Builder;
import android.graphics.drawable.Icon;
import android.content.pm.ShortcutManager;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.view.View$MeasureSpec;
import android.widget.ImageView;
import android.graphics.drawable.LayerDrawable;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ContextThemeWrapper;
import android.graphics.Bitmap;
import android.app.LauncherActivity;

public class CreateShortcut extends LauncherActivity
{
    static final String SHORTCUT_ID_PREFIX = "component-shortcut-";
    
    private Bitmap createIcon(int measureSpec, final int n, final int n2) {
        final View inflate = LayoutInflater.from((Context)new ContextThemeWrapper((Context)this, 16974372)).inflate(n, (ViewGroup)null);
        Drawable imageDrawable;
        final Drawable drawable = imageDrawable = this.getDrawable(measureSpec);
        if (drawable instanceof LayerDrawable) {
            imageDrawable = ((LayerDrawable)drawable).getDrawable(1);
        }
        ((ImageView)inflate.findViewById(16908294)).setImageDrawable(imageDrawable);
        measureSpec = View$MeasureSpec.makeMeasureSpec(n2, 1073741824);
        inflate.measure(measureSpec, measureSpec);
        final Bitmap bitmap = Bitmap.createBitmap(inflate.getMeasuredWidth(), inflate.getMeasuredHeight(), Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
        inflate.draw(canvas);
        return bitmap;
    }
    
    static Intent getBaseIntent() {
        return new Intent("android.intent.action.MAIN").addCategory("com.android.settings.SHORTCUT");
    }
    
    private void logCreateShortcut(final ResolveInfo resolveInfo) {
        if (resolveInfo != null && resolveInfo.activityInfo != null) {
            FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, 829, resolveInfo.activityInfo.name, (Pair<Integer, Object>[])new Pair[0]);
        }
    }
    
    Intent createResultIntent(final Intent intent, final ResolveInfo resolveInfo, final CharSequence shortLabel) {
        intent.setFlags(335544320);
        final ShortcutManager shortcutManager = (ShortcutManager)this.getSystemService((Class)ShortcutManager.class);
        final ActivityInfo activityInfo = resolveInfo.activityInfo;
        Icon icon;
        if (activityInfo.icon != 0) {
            icon = Icon.createWithAdaptiveBitmap(this.createIcon(activityInfo.icon, 2131558753, this.getResources().getDimensionPixelSize(2131165549)));
        }
        else {
            icon = Icon.createWithResource((Context)this, 2131231051);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("component-shortcut-");
        sb.append(intent.getComponent().flattenToShortString());
        Intent shortcutResultIntent;
        if ((shortcutResultIntent = shortcutManager.createShortcutResultIntent(new ShortcutInfo$Builder((Context)this, sb.toString()).setShortLabel(shortLabel).setIntent(intent).setIcon(icon).build())) == null) {
            shortcutResultIntent = new Intent();
        }
        shortcutResultIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", (Parcelable)Intent$ShortcutIconResource.fromContext((Context)this, 2131689474));
        shortcutResultIntent.putExtra("android.intent.extra.shortcut.INTENT", (Parcelable)intent);
        shortcutResultIntent.putExtra("android.intent.extra.shortcut.NAME", shortLabel);
        if (activityInfo.icon != 0) {
            shortcutResultIntent.putExtra("android.intent.extra.shortcut.ICON", (Parcelable)this.createIcon(activityInfo.icon, 2131558752, this.getResources().getDimensionPixelSize(2131165548)));
        }
        return shortcutResultIntent;
    }
    
    protected Intent getTargetIntent() {
        return getBaseIntent().addFlags(268435456);
    }
    
    protected boolean onEvaluateShowIcons() {
        return false;
    }
    
    protected void onListItemClick(final ListView listView, final View view, final int n, final long n2) {
        final LauncherActivity$ListItem itemForPosition = this.itemForPosition(n);
        this.logCreateShortcut(itemForPosition.resolveInfo);
        this.setResult(-1, this.createResultIntent(this.intentForPosition(n), itemForPosition.resolveInfo, itemForPosition.label));
        this.finish();
    }
    
    protected List<ResolveInfo> onQueryPackageManager(final Intent intent) {
        final List queryIntentActivities = this.getPackageManager().queryIntentActivities(intent, 128);
        final ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService("connectivity");
        if (queryIntentActivities == null) {
            return null;
        }
        for (int i = queryIntentActivities.size() - 1; i >= 0; --i) {
            if (queryIntentActivities.get(i).activityInfo.name.endsWith(Settings.TetherSettingsActivity.class.getSimpleName()) && !connectivityManager.isTetheringSupported()) {
                queryIntentActivities.remove(i);
            }
        }
        return (List<ResolveInfo>)queryIntentActivities;
    }
    
    protected void onSetContentView() {
        this.setContentView(2131558441);
    }
    
    public static class ShortcutsUpdateTask extends AsyncTask<Void, Void, Void>
    {
        private final Context mContext;
        
        public ShortcutsUpdateTask(final Context mContext) {
            this.mContext = mContext;
        }
        
        public Void doInBackground(final Void... array) {
            final ShortcutManager shortcutManager = (ShortcutManager)this.mContext.getSystemService((Class)ShortcutManager.class);
            final PackageManager packageManager = this.mContext.getPackageManager();
            final ArrayList<ShortcutInfo> list = new ArrayList<ShortcutInfo>();
            for (final ShortcutInfo shortcutInfo : shortcutManager.getPinnedShortcuts()) {
                if (!shortcutInfo.getId().startsWith("component-shortcut-")) {
                    continue;
                }
                final ResolveInfo resolveActivity = packageManager.resolveActivity(CreateShortcut.getBaseIntent().setComponent(ComponentName.unflattenFromString(shortcutInfo.getId().substring("component-shortcut-".length()))), 0);
                if (resolveActivity == null) {
                    continue;
                }
                list.add(new ShortcutInfo$Builder(this.mContext, shortcutInfo.getId()).setShortLabel(resolveActivity.loadLabel(packageManager)).build());
            }
            if (!list.isEmpty()) {
                shortcutManager.updateShortcuts((List)list);
            }
            return null;
        }
    }
}
