package com.android.settings;

import android.app.backup.IBackupManager$Stub;
import android.os.ServiceManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.content.Context;
import android.widget.Toast;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.app.backup.IBackupManager;
import android.app.Activity;

public class SetFullBackupPassword extends Activity
{
    IBackupManager mBackupManager;
    View.OnClickListener mButtonListener;
    Button mCancel;
    TextView mConfirmNewPw;
    TextView mCurrentPw;
    TextView mNewPw;
    Button mSet;
    
    public SetFullBackupPassword() {
        this.mButtonListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (view == SetFullBackupPassword.this.mSet) {
                    final String string = SetFullBackupPassword.this.mCurrentPw.getText().toString();
                    final String string2 = SetFullBackupPassword.this.mNewPw.getText().toString();
                    if (!string2.equals(SetFullBackupPassword.this.mConfirmNewPw.getText().toString())) {
                        Log.i("SetFullBackupPassword", "password mismatch");
                        Toast.makeText((Context)SetFullBackupPassword.this, 2131888032, 1).show();
                        return;
                    }
                    if (SetFullBackupPassword.this.setBackupPassword(string, string2)) {
                        Log.i("SetFullBackupPassword", "password set successfully");
                        Toast.makeText((Context)SetFullBackupPassword.this, 2131888033, 1).show();
                        SetFullBackupPassword.this.finish();
                    }
                    else {
                        Log.i("SetFullBackupPassword", "failure; password mismatch?");
                        Toast.makeText((Context)SetFullBackupPassword.this, 2131888034, 1).show();
                    }
                }
                else if (view == SetFullBackupPassword.this.mCancel) {
                    SetFullBackupPassword.this.finish();
                }
                else {
                    Log.w("SetFullBackupPassword", "Click on unknown view");
                }
            }
        };
    }
    
    private boolean setBackupPassword(final String s, final String s2) {
        try {
            return this.mBackupManager.setBackupPassword(s, s2);
        }
        catch (RemoteException ex) {
            Log.e("SetFullBackupPassword", "Unable to communicate with backup manager");
            return false;
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mBackupManager = IBackupManager$Stub.asInterface(ServiceManager.getService("backup"));
        this.setContentView(2131558740);
        this.mCurrentPw = (TextView)this.findViewById(2131362026);
        this.mNewPw = (TextView)this.findViewById(2131362388);
        this.mConfirmNewPw = (TextView)this.findViewById(2131362003);
        this.mCancel = (Button)this.findViewById(2131361898);
        this.mSet = (Button)this.findViewById(2131361899);
        this.mCancel.setOnClickListener(this.mButtonListener);
        this.mSet.setOnClickListener(this.mButtonListener);
    }
}
