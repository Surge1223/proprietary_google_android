package com.android.settings.inputmethod;

import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import java.util.List;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;

public class UserDictionaryAddWordFragment extends InstrumentedFragment
{
    private UserDictionaryAddWordContents mContents;
    private boolean mIsDeleting;
    private View mRootView;
    
    public UserDictionaryAddWordFragment() {
        this.mIsDeleting = false;
    }
    
    private void updateSpinner() {
        new ArrayAdapter((Context)this.getActivity(), 17367048, (List)this.mContents.getLocalesList(this.getActivity())).setDropDownViewResource(17367049);
    }
    
    @Override
    public int getMetricsCategory() {
        return 62;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.setHasOptionsMenu(true);
        this.setRetainInstance(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        menu.add(0, 1, 0, 2131887364).setIcon(2131230999).setShowAsAction(5);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2131558872, (ViewGroup)null);
        this.mIsDeleting = false;
        if (this.mContents == null) {
            this.mContents = new UserDictionaryAddWordContents(this.mRootView, this.getArguments());
        }
        else {
            this.mContents = new UserDictionaryAddWordContents(this.mRootView, this.mContents);
        }
        this.getActivity().getActionBar().setSubtitle((CharSequence)UserDictionarySettingsUtils.getLocaleDisplayName((Context)this.getActivity(), this.mContents.getCurrentUserDictionaryLocale()));
        return this.mRootView;
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 1) {
            this.mContents.delete((Context)this.getActivity());
            this.mIsDeleting = true;
            this.getActivity().onBackPressed();
            return true;
        }
        return false;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (!this.mIsDeleting) {
            this.mContents.apply((Context)this.getActivity(), null);
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateSpinner();
    }
}
