package com.android.settings.inputmethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import android.content.AsyncTaskLoader;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.CheckedTextView;
import android.widget.ArrayAdapter;
import android.os.Parcelable;
import android.view.InputDevice;
import android.os.Handler;
import java.util.Collection;
import android.content.Loader;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ListAdapter;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.app.AlertDialog;
import android.hardware.input.KeyboardLayout;
import android.hardware.input.InputDeviceIdentifier;
import android.hardware.input.InputManager;
import android.hardware.input.InputManager$InputDeviceListener;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class KeyboardLayoutDialogFragment extends InstrumentedDialogFragment implements LoaderManager$LoaderCallbacks<Keyboards>, InputManager$InputDeviceListener
{
    private KeyboardLayoutAdapter mAdapter;
    private InputManager mIm;
    private int mInputDeviceId;
    private InputDeviceIdentifier mInputDeviceIdentifier;
    
    public KeyboardLayoutDialogFragment() {
        this.mInputDeviceId = -1;
    }
    
    public KeyboardLayoutDialogFragment(final InputDeviceIdentifier mInputDeviceIdentifier) {
        this.mInputDeviceId = -1;
        this.mInputDeviceIdentifier = mInputDeviceIdentifier;
    }
    
    private void onKeyboardLayoutClicked(final int n) {
        if (n >= 0 && n < this.mAdapter.getCount()) {
            final KeyboardLayout keyboardLayout = (KeyboardLayout)this.mAdapter.getItem(n);
            if (keyboardLayout != null) {
                this.mIm.setCurrentKeyboardLayoutForInputDevice(this.mInputDeviceIdentifier, keyboardLayout.getDescriptor());
            }
            this.dismiss();
        }
    }
    
    private void onSetupLayoutsButtonClicked() {
        ((OnSetupKeyboardLayoutsListener)this.getTargetFragment()).onSetupKeyboardLayouts(this.mInputDeviceIdentifier);
    }
    
    private void updateSwitchHintVisibility() {
        final AlertDialog alertDialog = (AlertDialog)this.getDialog();
        if (alertDialog != null) {
            final View viewById = alertDialog.findViewById(16908820);
            int visibility;
            if (this.mAdapter.getCount() > 1) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
        }
    }
    
    public int getMetricsCategory() {
        return 541;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        this.show(this.getActivity().getFragmentManager(), "layout");
    }
    
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        final Context baseContext = activity.getBaseContext();
        this.mIm = (InputManager)baseContext.getSystemService("input");
        this.mAdapter = new KeyboardLayoutAdapter(baseContext);
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        this.dismiss();
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.mInputDeviceIdentifier = (InputDeviceIdentifier)bundle.getParcelable("inputDeviceIdentifier");
        }
        this.getLoaderManager().initLoader(0, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Activity activity = this.getActivity();
        final AlertDialog$Builder setView = new AlertDialog$Builder((Context)activity).setTitle(2131887927).setPositiveButton(2131887925, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                KeyboardLayoutDialogFragment.this.onSetupLayoutsButtonClicked();
            }
        }).setSingleChoiceItems((ListAdapter)this.mAdapter, -1, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                KeyboardLayoutDialogFragment.this.onKeyboardLayoutClicked(n);
            }
        }).setView(LayoutInflater.from((Context)activity).inflate(2131558596, (ViewGroup)null));
        this.updateSwitchHintVisibility();
        return (Dialog)setView.create();
    }
    
    public Loader<Keyboards> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<Keyboards>)new KeyboardLayoutLoader(this.getActivity().getBaseContext(), this.mInputDeviceIdentifier);
    }
    
    public void onInputDeviceAdded(final int n) {
    }
    
    public void onInputDeviceChanged(final int n) {
        if (this.mInputDeviceId >= 0 && n == this.mInputDeviceId) {
            this.getLoaderManager().restartLoader(0, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
        }
    }
    
    public void onInputDeviceRemoved(final int n) {
        if (this.mInputDeviceId >= 0 && n == this.mInputDeviceId) {
            this.dismiss();
        }
    }
    
    public void onLoadFinished(final Loader<Keyboards> loader, final Keyboards keyboards) {
        this.mAdapter.clear();
        this.mAdapter.addAll((Collection)keyboards.keyboardLayouts);
        this.mAdapter.setCheckedItem(keyboards.current);
        final AlertDialog alertDialog = (AlertDialog)this.getDialog();
        if (alertDialog != null) {
            alertDialog.getListView().setItemChecked(keyboards.current, true);
        }
        this.updateSwitchHintVisibility();
    }
    
    public void onLoaderReset(final Loader<Keyboards> loader) {
        this.mAdapter.clear();
        this.updateSwitchHintVisibility();
    }
    
    public void onPause() {
        this.mIm.unregisterInputDeviceListener((InputManager$InputDeviceListener)this);
        this.mInputDeviceId = -1;
        super.onPause();
    }
    
    public void onResume() {
        super.onResume();
        this.mIm.registerInputDeviceListener((InputManager$InputDeviceListener)this, (Handler)null);
        final InputDevice inputDeviceByDescriptor = this.mIm.getInputDeviceByDescriptor(this.mInputDeviceIdentifier.getDescriptor());
        if (inputDeviceByDescriptor == null) {
            this.dismiss();
            return;
        }
        this.mInputDeviceId = inputDeviceByDescriptor.getId();
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("inputDeviceIdentifier", (Parcelable)this.mInputDeviceIdentifier);
    }
    
    private static final class KeyboardLayoutAdapter extends ArrayAdapter<KeyboardLayout>
    {
        private int mCheckedItem;
        private final LayoutInflater mInflater;
        
        public KeyboardLayoutAdapter(final Context context) {
            super(context, 17367282);
            this.mCheckedItem = -1;
            this.mInflater = (LayoutInflater)context.getSystemService("layout_inflater");
        }
        
        private View inflateOneLine(View inflate, final ViewGroup viewGroup, final String text, final boolean checked) {
            final View view = inflate;
            Label_0036: {
                if (view != null) {
                    inflate = view;
                    if (!isTwoLine(view)) {
                        break Label_0036;
                    }
                }
                inflate = this.mInflater.inflate(17367055, viewGroup, false);
                setTwoLine(inflate, false);
            }
            final CheckedTextView checkedTextView = (CheckedTextView)inflate.findViewById(16908308);
            checkedTextView.setText((CharSequence)text);
            checkedTextView.setChecked(checked);
            return inflate;
        }
        
        private View inflateTwoLine(View inflate, final ViewGroup viewGroup, final String text, final String text2, final boolean checked) {
            final View view = inflate;
            Label_0036: {
                if (view != null) {
                    inflate = view;
                    if (isTwoLine(view)) {
                        break Label_0036;
                    }
                }
                inflate = this.mInflater.inflate(17367282, viewGroup, false);
                setTwoLine(inflate, true);
            }
            final TextView textView = (TextView)inflate.findViewById(16908308);
            final TextView textView2 = (TextView)inflate.findViewById(16908309);
            final RadioButton radioButton = (RadioButton)inflate.findViewById(16909214);
            textView.setText((CharSequence)text);
            textView2.setText((CharSequence)text2);
            radioButton.setChecked(checked);
            return inflate;
        }
        
        private static boolean isTwoLine(final View view) {
            return view.getTag() == Boolean.TRUE;
        }
        
        private static void setTwoLine(final View view, final boolean b) {
            view.setTag((Object)b);
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final KeyboardLayout keyboardLayout = (KeyboardLayout)this.getItem(n);
            String s;
            String collection;
            if (keyboardLayout != null) {
                s = keyboardLayout.getLabel();
                collection = keyboardLayout.getCollection();
            }
            else {
                s = this.getContext().getString(2131887924);
                collection = "";
            }
            final boolean b = n == this.mCheckedItem;
            if (collection.isEmpty()) {
                return this.inflateOneLine(view, viewGroup, s, b);
            }
            return this.inflateTwoLine(view, viewGroup, s, collection, b);
        }
        
        public void setCheckedItem(final int mCheckedItem) {
            this.mCheckedItem = mCheckedItem;
        }
    }
    
    private static final class KeyboardLayoutLoader extends AsyncTaskLoader<Keyboards>
    {
        private final InputDeviceIdentifier mInputDeviceIdentifier;
        
        public KeyboardLayoutLoader(final Context context, final InputDeviceIdentifier mInputDeviceIdentifier) {
            super(context);
            this.mInputDeviceIdentifier = mInputDeviceIdentifier;
        }
        
        public Keyboards loadInBackground() {
            final Keyboards keyboards = new Keyboards();
            final InputManager inputManager = (InputManager)this.getContext().getSystemService("input");
            final String[] enabledKeyboardLayoutsForInputDevice = inputManager.getEnabledKeyboardLayoutsForInputDevice(this.mInputDeviceIdentifier);
            for (int length = enabledKeyboardLayoutsForInputDevice.length, i = 0; i < length; ++i) {
                final KeyboardLayout keyboardLayout = inputManager.getKeyboardLayout(enabledKeyboardLayoutsForInputDevice[i]);
                if (keyboardLayout != null) {
                    keyboards.keyboardLayouts.add(keyboardLayout);
                }
            }
            Collections.sort(keyboards.keyboardLayouts);
            final String currentKeyboardLayoutForInputDevice = inputManager.getCurrentKeyboardLayoutForInputDevice(this.mInputDeviceIdentifier);
            if (currentKeyboardLayoutForInputDevice != null) {
                for (int size = keyboards.keyboardLayouts.size(), j = 0; j < size; ++j) {
                    if (keyboards.keyboardLayouts.get(j).getDescriptor().equals(currentKeyboardLayoutForInputDevice)) {
                        keyboards.current = j;
                        break;
                    }
                }
            }
            if (keyboards.keyboardLayouts.isEmpty()) {
                keyboards.keyboardLayouts.add(null);
                keyboards.current = 0;
            }
            return keyboards;
        }
        
        protected void onStartLoading() {
            super.onStartLoading();
            this.forceLoad();
        }
        
        protected void onStopLoading() {
            super.onStopLoading();
            this.cancelLoad();
        }
    }
    
    public static final class Keyboards
    {
        public int current;
        public final ArrayList<KeyboardLayout> keyboardLayouts;
        
        public Keyboards() {
            this.keyboardLayouts = new ArrayList<KeyboardLayout>();
            this.current = -1;
        }
    }
    
    public interface OnSetupKeyboardLayoutsListener
    {
        void onSetupKeyboardLayouts(final InputDeviceIdentifier p0);
    }
}
