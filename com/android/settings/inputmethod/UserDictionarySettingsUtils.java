package com.android.settings.inputmethod;

import com.android.settings.Utils;
import android.text.TextUtils;
import android.content.Context;

public class UserDictionarySettingsUtils
{
    public static String getLocaleDisplayName(final Context context, final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return context.getResources().getString(2131889715);
        }
        return Utils.createLocaleFromString(s).getDisplayName(context.getResources().getConfiguration().locale);
    }
}
