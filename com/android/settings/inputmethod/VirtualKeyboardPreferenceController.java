package com.android.settings.inputmethod;

import java.util.Iterator;
import java.util.List;
import android.text.BidiFormatter;
import android.view.inputmethod.InputMethodInfo;
import java.util.ArrayList;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.content.pm.PackageManager;
import android.view.inputmethod.InputMethodManager;
import android.app.admin.DevicePolicyManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class VirtualKeyboardPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final DevicePolicyManager mDpm;
    private final InputMethodManager mImm;
    private final PackageManager mPm;
    
    public VirtualKeyboardPreferenceController(final Context context) {
        super(context);
        this.mPm = this.mContext.getPackageManager();
        this.mDpm = (DevicePolicyManager)context.getSystemService("device_policy");
        this.mImm = (InputMethodManager)this.mContext.getSystemService("input_method");
    }
    
    @Override
    public String getPreferenceKey() {
        return "virtual_keyboard_pref";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034172);
    }
    
    @Override
    public void updateState(final Preference preference) {
        final List enabledInputMethodList = this.mImm.getEnabledInputMethodList();
        if (enabledInputMethodList == null) {
            preference.setSummary(2131889403);
            return;
        }
        final List permittedInputMethodsForCurrentUser = this.mDpm.getPermittedInputMethodsForCurrentUser();
        final ArrayList<String> list = new ArrayList<String>();
        final Iterator<InputMethodInfo> iterator = enabledInputMethodList.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            final boolean b = true;
            if (!hasNext) {
                break;
            }
            final InputMethodInfo inputMethodInfo = iterator.next();
            boolean b2 = b;
            if (permittedInputMethodsForCurrentUser != null) {
                b2 = (permittedInputMethodsForCurrentUser.contains(inputMethodInfo.getPackageName()) && b);
            }
            if (!b2) {
                continue;
            }
            list.add(inputMethodInfo.loadLabel(this.mPm).toString());
        }
        if (list.isEmpty()) {
            preference.setSummary(2131889403);
            return;
        }
        final BidiFormatter instance = BidiFormatter.getInstance();
        CharSequence summary = null;
        for (final String s : list) {
            if (summary == null) {
                summary = instance.unicodeWrap(s);
            }
            else {
                summary = this.mContext.getString(2131887916, new Object[] { summary, instance.unicodeWrap(s) });
            }
        }
        preference.setSummary(summary);
    }
}
