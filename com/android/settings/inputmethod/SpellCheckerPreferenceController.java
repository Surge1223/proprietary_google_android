package com.android.settings.inputmethod;

import android.view.textservice.SpellCheckerInfo;
import android.support.v7.preference.Preference;
import com.android.settingslib.inputmethod.InputMethodAndSubtypeUtil;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.view.textservice.TextServicesManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class SpellCheckerPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final TextServicesManager mTextServicesManager;
    
    public SpellCheckerPreferenceController(final Context context) {
        super(context);
        this.mTextServicesManager = (TextServicesManager)context.getSystemService("textservices");
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("spellcheckers_settings");
        if (preference != null) {
            InputMethodAndSubtypeUtil.removeUnnecessaryNonPersistentPreference(preference);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "spellcheckers_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034164);
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (preference == null) {
            return;
        }
        if (!this.mTextServicesManager.isSpellCheckerEnabled()) {
            preference.setSummary(2131889421);
        }
        else {
            final SpellCheckerInfo currentSpellChecker = this.mTextServicesManager.getCurrentSpellChecker();
            if (currentSpellChecker != null) {
                preference.setSummary(currentSpellChecker.loadLabel(this.mContext.getPackageManager()));
            }
            else {
                preference.setSummary(2131889212);
            }
        }
    }
}
