package com.android.settings.inputmethod;

import android.database.Cursor;
import java.util.Iterator;
import java.util.TreeSet;
import android.app.Activity;
import java.util.Locale;
import android.content.ContentResolver;
import com.android.settings.Utils;
import android.text.TextUtils;
import android.provider.UserDictionary$Words;
import java.util.ArrayList;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class UserDictionaryAddWordContents
{
    private static final String[] HAS_WORD_PROJECTION;
    private String mLocale;
    private final int mMode;
    private final String mOldShortcut;
    private final String mOldWord;
    private String mSavedShortcut;
    private String mSavedWord;
    private final EditText mShortcutEditText;
    private final EditText mWordEditText;
    
    static {
        HAS_WORD_PROJECTION = new String[] { "word" };
    }
    
    UserDictionaryAddWordContents(final View view, final Bundle bundle) {
        this.mWordEditText = (EditText)view.findViewById(2131362792);
        this.mShortcutEditText = (EditText)view.findViewById(2131362789);
        final String string = bundle.getString("word");
        if (string != null) {
            this.mWordEditText.setText((CharSequence)string);
            this.mWordEditText.setSelection(this.mWordEditText.getText().length());
        }
        final String string2 = bundle.getString("shortcut");
        if (string2 != null && this.mShortcutEditText != null) {
            this.mShortcutEditText.setText((CharSequence)string2);
        }
        this.mMode = bundle.getInt("mode");
        this.mOldWord = bundle.getString("word");
        this.mOldShortcut = bundle.getString("shortcut");
        this.updateLocale(bundle.getString("locale"));
    }
    
    UserDictionaryAddWordContents(final View view, final UserDictionaryAddWordContents userDictionaryAddWordContents) {
        this.mWordEditText = (EditText)view.findViewById(2131362792);
        this.mShortcutEditText = (EditText)view.findViewById(2131362789);
        this.mMode = 0;
        this.mOldWord = userDictionaryAddWordContents.mSavedWord;
        this.mOldShortcut = userDictionaryAddWordContents.mSavedShortcut;
        this.updateLocale(userDictionaryAddWordContents.getCurrentUserDictionaryLocale());
    }
    
    private static void addLocaleDisplayNameToList(final Context context, final ArrayList<LocaleRenderer> list, final String s) {
        if (s != null) {
            list.add(new LocaleRenderer(context, s));
        }
    }
    
    private boolean hasWord(String o, final Context context) {
        final boolean equals = "".equals(this.mLocale);
        boolean b = true;
        if (equals) {
            o = context.getContentResolver().query(UserDictionary$Words.CONTENT_URI, UserDictionaryAddWordContents.HAS_WORD_PROJECTION, "word=? AND locale is null", new String[] { o }, (String)null);
        }
        else {
            o = context.getContentResolver().query(UserDictionary$Words.CONTENT_URI, UserDictionaryAddWordContents.HAS_WORD_PROJECTION, "word=? AND locale=?", new String[] { o, this.mLocale }, (String)null);
        }
        if (o == null) {
            if (o != null) {
                ((Cursor)o).close();
            }
            return false;
        }
        try {
            if (((Cursor)o).getCount() <= 0) {
                b = false;
            }
            return b;
        }
        finally {
            if (o != null) {
                ((Cursor)o).close();
            }
        }
    }
    
    int apply(final Context context, final Bundle bundle) {
        if (bundle != null) {
            this.saveStateIntoBundle(bundle);
        }
        final ContentResolver contentResolver = context.getContentResolver();
        if (this.mMode == 0 && !TextUtils.isEmpty((CharSequence)this.mOldWord)) {
            UserDictionarySettings.deleteWord(this.mOldWord, this.mOldShortcut, contentResolver);
        }
        final String string = this.mWordEditText.getText().toString();
        String string2;
        if (this.mShortcutEditText == null) {
            string2 = null;
        }
        else if (TextUtils.isEmpty((CharSequence)(string2 = this.mShortcutEditText.getText().toString()))) {
            string2 = null;
        }
        if (TextUtils.isEmpty((CharSequence)string)) {
            return 1;
        }
        this.mSavedWord = string;
        this.mSavedShortcut = string2;
        if (TextUtils.isEmpty((CharSequence)string2) && this.hasWord(string, context)) {
            return 2;
        }
        Locale localeFromString = null;
        UserDictionarySettings.deleteWord(string, null, contentResolver);
        if (!TextUtils.isEmpty((CharSequence)string2)) {
            UserDictionarySettings.deleteWord(string, string2, contentResolver);
        }
        final String string3 = string.toString();
        if (!TextUtils.isEmpty((CharSequence)this.mLocale)) {
            localeFromString = Utils.createLocaleFromString(this.mLocale);
        }
        UserDictionary$Words.addWord(context, string3, 250, string2, localeFromString);
        return 0;
    }
    
    void delete(final Context context) {
        if (this.mMode == 0 && !TextUtils.isEmpty((CharSequence)this.mOldWord)) {
            UserDictionarySettings.deleteWord(this.mOldWord, this.mOldShortcut, context.getContentResolver());
        }
    }
    
    public String getCurrentUserDictionaryLocale() {
        return this.mLocale;
    }
    
    public ArrayList<LocaleRenderer> getLocalesList(final Activity activity) {
        final TreeSet<String> userDictionaryLocalesSet = UserDictionaryList.getUserDictionaryLocalesSet((Context)activity);
        userDictionaryLocalesSet.remove(this.mLocale);
        final String string = Locale.getDefault().toString();
        userDictionaryLocalesSet.remove(string);
        userDictionaryLocalesSet.remove("");
        final ArrayList<LocaleRenderer> list = new ArrayList<LocaleRenderer>();
        addLocaleDisplayNameToList((Context)activity, list, this.mLocale);
        if (!string.equals(this.mLocale)) {
            addLocaleDisplayNameToList((Context)activity, list, string);
        }
        final Iterator<String> iterator = userDictionaryLocalesSet.iterator();
        while (iterator.hasNext()) {
            addLocaleDisplayNameToList((Context)activity, list, iterator.next());
        }
        if (!"".equals(this.mLocale)) {
            addLocaleDisplayNameToList((Context)activity, list, "");
        }
        list.add(new LocaleRenderer((Context)activity, null));
        return list;
    }
    
    void saveStateIntoBundle(final Bundle bundle) {
        bundle.putString("word", this.mWordEditText.getText().toString());
        bundle.putString("originalWord", this.mOldWord);
        if (this.mShortcutEditText != null) {
            bundle.putString("shortcut", this.mShortcutEditText.getText().toString());
        }
        if (this.mOldShortcut != null) {
            bundle.putString("originalShortcut", this.mOldShortcut);
        }
        bundle.putString("locale", this.mLocale);
    }
    
    void updateLocale(String string) {
        if (string == null) {
            string = Locale.getDefault().toString();
        }
        this.mLocale = string;
    }
    
    public static class LocaleRenderer
    {
        private final String mDescription;
        private final String mLocaleString;
        
        public LocaleRenderer(final Context context, final String mLocaleString) {
            this.mLocaleString = mLocaleString;
            if (mLocaleString == null) {
                this.mDescription = context.getString(2131889720);
            }
            else if ("".equals(mLocaleString)) {
                this.mDescription = context.getString(2131889715);
            }
            else {
                this.mDescription = Utils.createLocaleFromString(mLocaleString).getDisplayName();
            }
        }
        
        @Override
        public String toString() {
            return this.mDescription;
        }
    }
}
