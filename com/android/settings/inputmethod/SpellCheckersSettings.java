package com.android.settings.inputmethod;

import android.widget.Switch;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.Context;
import android.view.textservice.SpellCheckerSubtype;
import android.provider.Settings;
import android.content.ContentResolver;
import android.view.textservice.TextServicesManager;
import android.app.AlertDialog;
import android.view.textservice.SpellCheckerInfo;
import com.android.settings.widget.SwitchBar;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class SpellCheckersSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnPreferenceClickListener, OnSwitchChangeListener
{
    private static final String TAG;
    private SpellCheckerInfo mCurrentSci;
    private AlertDialog mDialog;
    private SpellCheckerInfo[] mEnabledScis;
    private Preference mSpellCheckerLanaguagePref;
    private SwitchBar mSwitchBar;
    private TextServicesManager mTsm;
    
    static {
        TAG = SpellCheckersSettings.class.getSimpleName();
    }
    
    public SpellCheckersSettings() {
        this.mDialog = null;
    }
    
    private void changeCurrentSpellChecker(final SpellCheckerInfo spellCheckerInfo) {
        Settings.Secure.putString(this.getContentResolver(), "selected_spell_checker", spellCheckerInfo.getId());
        Settings.Secure.putInt(this.getContentResolver(), "selected_spell_checker_subtype", 0);
        this.updatePreferenceScreen();
    }
    
    private static int convertDialogItemIdToSubtypeIndex(final int n) {
        return n - 1;
    }
    
    private static int convertSubtypeIndexToDialogItemId(final int n) {
        return n + 1;
    }
    
    private CharSequence getSpellCheckerSubtypeLabel(final SpellCheckerInfo spellCheckerInfo, final SpellCheckerSubtype spellCheckerSubtype) {
        if (spellCheckerInfo == null) {
            return this.getString(2131889212);
        }
        if (spellCheckerSubtype == null) {
            return this.getString(R.string.use_system_language_to_select_input_method_subtypes);
        }
        return spellCheckerSubtype.getDisplayName((Context)this.getActivity(), spellCheckerInfo.getPackageName(), spellCheckerInfo.getServiceInfo().applicationInfo);
    }
    
    private void populatePreferenceScreen() {
        final SpellCheckerPreference spellCheckerPreference = new SpellCheckerPreference(this.getPrefContext(), this.mEnabledScis);
        spellCheckerPreference.setTitle(2131887362);
        int length;
        if (this.mEnabledScis == null) {
            length = 0;
        }
        else {
            length = this.mEnabledScis.length;
        }
        if (length > 0) {
            spellCheckerPreference.setSummary("%s");
        }
        else {
            spellCheckerPreference.setSummary(2131889212);
        }
        spellCheckerPreference.setKey("default_spellchecker");
        spellCheckerPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.getPreferenceScreen().addPreference(spellCheckerPreference);
    }
    
    private void showChooseLanguageDialog() {
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        final SpellCheckerInfo currentSpellChecker = this.mTsm.getCurrentSpellChecker();
        if (currentSpellChecker == null) {
            return;
        }
        final TextServicesManager mTsm = this.mTsm;
        int i = 0;
        final SpellCheckerSubtype currentSpellCheckerSubtype = mTsm.getCurrentSpellCheckerSubtype(false);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this.getActivity());
        alertDialog$Builder.setTitle(2131888563);
        final int subtypeCount = currentSpellChecker.getSubtypeCount();
        final CharSequence[] array = new CharSequence[subtypeCount + 1];
        array[0] = this.getSpellCheckerSubtypeLabel(currentSpellChecker, null);
        int n = 0;
        while (i < subtypeCount) {
            final SpellCheckerSubtype subtype = currentSpellChecker.getSubtypeAt(i);
            final int convertSubtypeIndexToDialogItemId = convertSubtypeIndexToDialogItemId(i);
            array[convertSubtypeIndexToDialogItemId] = this.getSpellCheckerSubtypeLabel(currentSpellChecker, subtype);
            if (subtype.equals((Object)currentSpellCheckerSubtype)) {
                n = convertSubtypeIndexToDialogItemId;
            }
            ++i;
        }
        alertDialog$Builder.setSingleChoiceItems(array, n, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, int n) {
                if (n == 0) {
                    n = 0;
                }
                else {
                    n = convertDialogItemIdToSubtypeIndex(n);
                    n = currentSpellChecker.getSubtypeAt(n).hashCode();
                }
                Settings.Secure.putInt(SettingsPreferenceFragment.this.getContentResolver(), "selected_spell_checker_subtype", n);
                dialogInterface.dismiss();
                SpellCheckersSettings.this.updatePreferenceScreen();
            }
        });
        (this.mDialog = alertDialog$Builder.create()).show();
    }
    
    private void showSecurityWarnDialog(final SpellCheckerInfo spellCheckerInfo) {
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this.getActivity());
        alertDialog$Builder.setTitle(17039380);
        alertDialog$Builder.setMessage((CharSequence)this.getString(2131889215, new Object[] { spellCheckerInfo.loadLabel(this.getPackageManager()) }));
        alertDialog$Builder.setCancelable(true);
        alertDialog$Builder.setPositiveButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                SpellCheckersSettings.this.changeCurrentSpellChecker(spellCheckerInfo);
            }
        });
        alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        });
        (this.mDialog = alertDialog$Builder.create()).show();
    }
    
    private void updatePreferenceScreen() {
        this.mCurrentSci = this.mTsm.getCurrentSpellChecker();
        final boolean spellCheckerEnabled = this.mTsm.isSpellCheckerEnabled();
        this.mSwitchBar.setChecked(spellCheckerEnabled);
        final SpellCheckerInfo mCurrentSci = this.mCurrentSci;
        final boolean b = false;
        SpellCheckerSubtype currentSpellCheckerSubtype;
        if (mCurrentSci != null) {
            currentSpellCheckerSubtype = this.mTsm.getCurrentSpellCheckerSubtype(false);
        }
        else {
            currentSpellCheckerSubtype = null;
        }
        this.mSpellCheckerLanaguagePref.setSummary(this.getSpellCheckerSubtypeLabel(this.mCurrentSci, currentSpellCheckerSubtype));
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        for (int preferenceCount = preferenceScreen.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = preferenceScreen.getPreference(i);
            preference.setEnabled(spellCheckerEnabled);
            if (preference instanceof SpellCheckerPreference) {
                ((SpellCheckerPreference)preference).setSelected(this.mCurrentSci);
            }
        }
        final Preference mSpellCheckerLanaguagePref = this.mSpellCheckerLanaguagePref;
        boolean enabled = b;
        if (spellCheckerEnabled) {
            enabled = b;
            if (this.mCurrentSci != null) {
                enabled = true;
            }
        }
        mSpellCheckerLanaguagePref.setEnabled(enabled);
    }
    
    @Override
    public int getMetricsCategory() {
        return 59;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082835);
        (this.mSpellCheckerLanaguagePref = this.findPreference("spellchecker_language")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        this.mTsm = (TextServicesManager)this.getSystemService("textservices");
        this.mCurrentSci = this.mTsm.getCurrentSpellChecker();
        this.mEnabledScis = this.mTsm.getEnabledSpellCheckers();
        this.populatePreferenceScreen();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final SpellCheckerInfo spellCheckerInfo = (SpellCheckerInfo)o;
        if ((spellCheckerInfo.getServiceInfo().applicationInfo.flags & 0x1) != 0x0) {
            this.changeCurrentSpellChecker(spellCheckerInfo);
            return true;
        }
        this.showSecurityWarnDialog(spellCheckerInfo);
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (preference == this.mSpellCheckerLanaguagePref) {
            this.showChooseLanguageDialog();
            return true;
        }
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        (this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar()).setSwitchBarText(2131889211, 2131889211);
        this.mSwitchBar.show();
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        this.updatePreferenceScreen();
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        Settings.Secure.putInt(this.getContentResolver(), "spell_checker_enabled", (int)(b ? 1 : 0));
        this.updatePreferenceScreen();
    }
}
