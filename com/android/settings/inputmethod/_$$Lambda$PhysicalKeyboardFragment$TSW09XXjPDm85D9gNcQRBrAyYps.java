package com.android.settings.inputmethod;

import android.os.Parcelable;
import com.android.settings.Settings;
import java.util.HashMap;
import com.android.internal.util.Preconditions;
import android.app.Activity;
import android.os.Bundle;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import java.util.Collection;
import java.util.Objects;
import android.app.Fragment;
import android.hardware.input.InputDeviceIdentifier;
import android.os.UserHandle;
import android.provider.Settings;
import com.android.settingslib.utils.ThreadUtils;
import android.hardware.input.KeyboardLayout;
import android.text.TextUtils;
import java.util.Comparator;
import java.text.Collator;
import android.view.InputDevice;
import android.os.Handler;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import com.android.internal.inputmethod.InputMethodUtils$InputMethodSettings;
import java.util.ArrayList;
import android.support.v7.preference.PreferenceCategory;
import android.content.Intent;
import android.hardware.input.InputManager;
import android.database.ContentObserver;
import com.android.settings.search.Indexable;
import android.hardware.input.InputManager$InputDeviceListener;
import com.android.settings.SettingsPreferenceFragment;
import java.util.List;

public final class _$$Lambda$PhysicalKeyboardFragment$TSW09XXjPDm85D9gNcQRBrAyYps implements Runnable
{
    @Override
    public final void run() {
        this.f$0.updateHardKeyboards(this.f$1);
    }
}
