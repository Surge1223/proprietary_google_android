package com.android.settings.inputmethod;

import android.os.Handler;
import java.util.Iterator;
import java.util.List;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.hardware.input.InputManager;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.hardware.input.InputManager$InputDeviceListener;
import com.android.settingslib.core.AbstractPreferenceController;

public class PhysicalKeyboardPreferenceController extends AbstractPreferenceController implements InputManager$InputDeviceListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final InputManager mIm;
    private Preference mPreference;
    
    public PhysicalKeyboardPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mIm = (InputManager)context.getSystemService("input");
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private void updateSummary() {
        if (this.mPreference == null) {
            return;
        }
        final List<PhysicalKeyboardFragment.HardKeyboardDeviceInfo> hardKeyboards = PhysicalKeyboardFragment.getHardKeyboards(this.mContext);
        if (hardKeyboards.isEmpty()) {
            this.mPreference.setSummary(2131887439);
            return;
        }
        CharSequence summary = null;
        for (final PhysicalKeyboardFragment.HardKeyboardDeviceInfo hardKeyboardDeviceInfo : hardKeyboards) {
            if (summary == null) {
                summary = hardKeyboardDeviceInfo.mDeviceName;
            }
            else {
                summary = this.mContext.getString(2131887916, new Object[] { summary, hardKeyboardDeviceInfo.mDeviceName });
            }
        }
        this.mPreference.setSummary(summary);
    }
    
    @Override
    public String getPreferenceKey() {
        return "physical_keyboard_pref";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034156);
    }
    
    public void onInputDeviceAdded(final int n) {
        this.updateSummary();
    }
    
    public void onInputDeviceChanged(final int n) {
        this.updateSummary();
    }
    
    public void onInputDeviceRemoved(final int n) {
        this.updateSummary();
    }
    
    public void onPause() {
        this.mIm.unregisterInputDeviceListener((InputManager$InputDeviceListener)this);
    }
    
    public void onResume() {
        this.mIm.registerInputDeviceListener((InputManager$InputDeviceListener)this, (Handler)null);
    }
    
    @Override
    public void updateState(final Preference mPreference) {
        this.mPreference = mPreference;
        this.updateSummary();
    }
}
