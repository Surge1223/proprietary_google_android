package com.android.settings.inputmethod;

import android.support.v7.preference.PreferenceScreen;
import android.support.v14.preference.PreferenceFragment;
import android.text.TextUtils;
import android.os.Bundle;
import com.android.settingslib.inputmethod.InputMethodAndSubtypeEnablerManager;
import com.android.settings.SettingsPreferenceFragment;

public class InputMethodAndSubtypeEnabler extends SettingsPreferenceFragment
{
    private InputMethodAndSubtypeEnablerManager mManager;
    
    private String getStringExtraFromIntentOrArguments(String string) {
        final String stringExtra = this.getActivity().getIntent().getStringExtra(string);
        if (stringExtra != null) {
            return stringExtra;
        }
        final Bundle arguments = this.getArguments();
        if (arguments == null) {
            string = null;
        }
        else {
            string = arguments.getString(string);
        }
        return string;
    }
    
    @Override
    public int getMetricsCategory() {
        return 60;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final String stringExtraFromIntentOrArguments = this.getStringExtraFromIntentOrArguments("android.intent.extra.TITLE");
        if (!TextUtils.isEmpty((CharSequence)stringExtraFromIntentOrArguments)) {
            this.getActivity().setTitle((CharSequence)stringExtraFromIntentOrArguments);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String stringExtraFromIntentOrArguments = this.getStringExtraFromIntentOrArguments("input_method_id");
        final PreferenceScreen preferenceScreen = this.getPreferenceManager().createPreferenceScreen(this.getPrefContext());
        (this.mManager = new InputMethodAndSubtypeEnablerManager(this)).init(this, stringExtraFromIntentOrArguments, preferenceScreen);
        this.setPreferenceScreen(preferenceScreen);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mManager.save(this.getContext(), this);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mManager.refresh(this.getContext(), this);
    }
}
