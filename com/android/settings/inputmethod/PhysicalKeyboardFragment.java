package com.android.settings.inputmethod;

import android.os.Parcelable;
import com.android.settings.Settings;
import java.util.HashMap;
import com.android.internal.util.Preconditions;
import android.app.Activity;
import android.os.Bundle;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import java.util.Collection;
import java.util.Objects;
import android.app.Fragment;
import android.hardware.input.InputDeviceIdentifier;
import android.os.UserHandle;
import android.provider.Settings;
import com.android.settingslib.utils.ThreadUtils;
import android.hardware.input.KeyboardLayout;
import android.text.TextUtils;
import java.util.Comparator;
import java.text.Collator;
import android.view.InputDevice;
import android.os.Handler;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import com.android.internal.inputmethod.InputMethodUtils$InputMethodSettings;
import java.util.ArrayList;
import android.support.v7.preference.PreferenceCategory;
import android.content.Intent;
import android.hardware.input.InputManager;
import android.database.ContentObserver;
import com.android.settings.search.Indexable;
import android.hardware.input.InputManager$InputDeviceListener;
import com.android.settings.SettingsPreferenceFragment;

public final class PhysicalKeyboardFragment extends SettingsPreferenceFragment implements InputManager$InputDeviceListener, OnSetupKeyboardLayoutsListener, Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private final ContentObserver mContentObserver;
    private InputManager mIm;
    private Intent mIntentWaitingForResult;
    private PreferenceCategory mKeyboardAssistanceCategory;
    private final ArrayList<HardKeyboardDeviceInfo> mLastHardKeyboards;
    private InputMethodUtils$InputMethodSettings mSettings;
    private SwitchPreference mShowVirtualKeyboardSwitch;
    private final OnPreferenceChangeListener mShowVirtualKeyboardSwitchPreferenceChangeListener;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082801;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    public PhysicalKeyboardFragment() {
        this.mLastHardKeyboards = new ArrayList<HardKeyboardDeviceInfo>();
        this.mShowVirtualKeyboardSwitchPreferenceChangeListener = new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                PhysicalKeyboardFragment.this.mSettings.setShowImeWithHardKeyboard((boolean)o);
                return true;
            }
        };
        this.mContentObserver = new ContentObserver(new Handler(true)) {
            public void onChange(final boolean b) {
                PhysicalKeyboardFragment.this.updateShowVirtualKeyboardSwitch();
            }
        };
    }
    
    static List<HardKeyboardDeviceInfo> getHardKeyboards(final Context context) {
        final ArrayList<Object> list = new ArrayList<Object>();
        final InputManager inputManager = (InputManager)context.getSystemService((Class)InputManager.class);
        if (inputManager == null) {
            return new ArrayList<HardKeyboardDeviceInfo>();
        }
        final int[] deviceIds = InputDevice.getDeviceIds();
        for (int length = deviceIds.length, i = 0; i < length; ++i) {
            final InputDevice device = InputDevice.getDevice(deviceIds[i]);
            if (device != null && !device.isVirtual()) {
                if (device.isFullKeyboard()) {
                    list.add(new HardKeyboardDeviceInfo(device.getName(), device.getIdentifier(), getLayoutLabel(device, context, inputManager)));
                }
            }
        }
        list.sort(new _$$Lambda$PhysicalKeyboardFragment$E1Pa9yi7mSTmfiefFBHYeSOZEJQ(Collator.getInstance()));
        return (List<HardKeyboardDeviceInfo>)list;
    }
    
    private static String getLayoutLabel(final InputDevice inputDevice, final Context context, final InputManager inputManager) {
        final String currentKeyboardLayoutForInputDevice = inputManager.getCurrentKeyboardLayoutForInputDevice(inputDevice.getIdentifier());
        if (currentKeyboardLayoutForInputDevice == null) {
            return context.getString(2131887924);
        }
        final KeyboardLayout keyboardLayout = inputManager.getKeyboardLayout(currentKeyboardLayoutForInputDevice);
        if (keyboardLayout == null) {
            return context.getString(2131887924);
        }
        return TextUtils.emptyIfNull(keyboardLayout.getLabel());
    }
    
    private void registerShowVirtualKeyboardSettingsObserver() {
        this.unregisterShowVirtualKeyboardSettingsObserver();
        this.getActivity().getContentResolver().registerContentObserver(Settings.Secure.getUriFor("show_ime_with_hard_keyboard"), false, this.mContentObserver, UserHandle.myUserId());
        this.updateShowVirtualKeyboardSwitch();
    }
    
    private void scheduleUpdateHardKeyboards() {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$PhysicalKeyboardFragment$j2wn_SRBsrC7ziAxKgN6he5fFRk(this, this.getContext()));
    }
    
    private void showKeyboardLayoutDialog(final InputDeviceIdentifier inputDeviceIdentifier) {
        final KeyboardLayoutDialogFragment keyboardLayoutDialogFragment = new KeyboardLayoutDialogFragment(inputDeviceIdentifier);
        keyboardLayoutDialogFragment.setTargetFragment((Fragment)this, 0);
        keyboardLayoutDialogFragment.show(this.getActivity().getFragmentManager(), "keyboardLayout");
    }
    
    private void toggleKeyboardShortcutsMenu() {
        this.getActivity().requestShowKeyboardShortcuts();
    }
    
    private void unregisterShowVirtualKeyboardSettingsObserver() {
        this.getActivity().getContentResolver().unregisterContentObserver(this.mContentObserver);
    }
    
    private void updateHardKeyboards(final List<HardKeyboardDeviceInfo> list) {
        if (Objects.equals(this.mLastHardKeyboards, list)) {
            return;
        }
        this.mLastHardKeyboards.clear();
        this.mLastHardKeyboards.addAll(list);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        final PreferenceCategory preferenceCategory = new PreferenceCategory(this.getPrefContext());
        preferenceCategory.setTitle(2131886930);
        preferenceCategory.setOrder(0);
        preferenceScreen.addPreference(preferenceCategory);
        for (final HardKeyboardDeviceInfo hardKeyboardDeviceInfo : list) {
            final Preference preference = new Preference(this.getPrefContext());
            preference.setTitle(hardKeyboardDeviceInfo.mDeviceName);
            preference.setSummary(hardKeyboardDeviceInfo.mLayoutLabel);
            preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$PhysicalKeyboardFragment$GzAuWQoIrNRWOGdhye1KALY7EFw(this, hardKeyboardDeviceInfo));
            preferenceCategory.addPreference(preference);
        }
        this.mKeyboardAssistanceCategory.setOrder(1);
        preferenceScreen.addPreference(this.mKeyboardAssistanceCategory);
        this.updateShowVirtualKeyboardSwitch();
    }
    
    private void updateShowVirtualKeyboardSwitch() {
        this.mShowVirtualKeyboardSwitch.setChecked(this.mSettings.isShowImeWithHardKeyboardEnabled());
    }
    
    public int getMetricsCategory() {
        return 346;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (this.mIntentWaitingForResult != null) {
            final InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)this.mIntentWaitingForResult.getParcelableExtra("input_device_identifier");
            this.mIntentWaitingForResult = null;
            this.showKeyboardLayoutDialog(inputDeviceIdentifier);
        }
    }
    
    public void onCreatePreferences(final Bundle bundle, final String s) {
        final Activity activity = (Activity)Preconditions.checkNotNull((Object)this.getActivity());
        this.addPreferencesFromResource(2132082801);
        this.mIm = (InputManager)Preconditions.checkNotNull((Object)activity.getSystemService((Class)InputManager.class));
        this.mSettings = new InputMethodUtils$InputMethodSettings(activity.getResources(), this.getContentResolver(), new HashMap(), new ArrayList(), UserHandle.myUserId(), false);
        this.mKeyboardAssistanceCategory = (PreferenceCategory)Preconditions.checkNotNull((Object)this.findPreference("keyboard_assistance_category"));
        this.mShowVirtualKeyboardSwitch = (SwitchPreference)Preconditions.checkNotNull((Object)this.mKeyboardAssistanceCategory.findPreference("show_virtual_keyboard_switch"));
        this.findPreference("keyboard_shortcuts_helper").setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                PhysicalKeyboardFragment.this.toggleKeyboardShortcutsMenu();
                return true;
            }
        });
    }
    
    public void onInputDeviceAdded(final int n) {
        this.scheduleUpdateHardKeyboards();
    }
    
    public void onInputDeviceChanged(final int n) {
        this.scheduleUpdateHardKeyboards();
    }
    
    public void onInputDeviceRemoved(final int n) {
        this.scheduleUpdateHardKeyboards();
    }
    
    public void onPause() {
        super.onPause();
        this.mLastHardKeyboards.clear();
        this.mIm.unregisterInputDeviceListener((InputManager$InputDeviceListener)this);
        this.mShowVirtualKeyboardSwitch.setOnPreferenceChangeListener(null);
        this.unregisterShowVirtualKeyboardSettingsObserver();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mLastHardKeyboards.clear();
        this.scheduleUpdateHardKeyboards();
        this.mIm.registerInputDeviceListener((InputManager$InputDeviceListener)this, (Handler)null);
        this.mShowVirtualKeyboardSwitch.setOnPreferenceChangeListener(this.mShowVirtualKeyboardSwitchPreferenceChangeListener);
        this.registerShowVirtualKeyboardSettingsObserver();
    }
    
    public void onSetupKeyboardLayouts(final InputDeviceIdentifier inputDeviceIdentifier) {
        final Intent mIntentWaitingForResult = new Intent("android.intent.action.MAIN");
        mIntentWaitingForResult.setClass((Context)this.getActivity(), (Class)Settings.KeyboardLayoutPickerActivity.class);
        mIntentWaitingForResult.putExtra("input_device_identifier", (Parcelable)inputDeviceIdentifier);
        this.startActivityForResult(this.mIntentWaitingForResult = mIntentWaitingForResult, 0);
    }
    
    public static final class HardKeyboardDeviceInfo
    {
        public final InputDeviceIdentifier mDeviceIdentifier;
        public final String mDeviceName;
        public final String mLayoutLabel;
        
        public HardKeyboardDeviceInfo(final String s, final InputDeviceIdentifier mDeviceIdentifier, final String mLayoutLabel) {
            this.mDeviceName = TextUtils.emptyIfNull(s);
            this.mDeviceIdentifier = mDeviceIdentifier;
            this.mLayoutLabel = mLayoutLabel;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (!(o instanceof HardKeyboardDeviceInfo)) {
                return false;
            }
            final HardKeyboardDeviceInfo hardKeyboardDeviceInfo = (HardKeyboardDeviceInfo)o;
            return TextUtils.equals((CharSequence)this.mDeviceName, (CharSequence)hardKeyboardDeviceInfo.mDeviceName) && Objects.equals(this.mDeviceIdentifier, hardKeyboardDeviceInfo.mDeviceIdentifier) && TextUtils.equals((CharSequence)this.mLayoutLabel, (CharSequence)hardKeyboardDeviceInfo.mLayoutLabel);
        }
    }
}
