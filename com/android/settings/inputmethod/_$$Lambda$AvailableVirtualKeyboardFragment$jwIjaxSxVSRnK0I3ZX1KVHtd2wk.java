package com.android.settings.inputmethod;

import java.util.Iterator;
import android.support.v14.preference.PreferenceFragment;
import android.app.Activity;
import android.os.Bundle;
import com.android.settingslib.inputmethod.InputMethodAndSubtypeUtil;
import android.support.v7.preference.Preference;
import android.content.pm.ApplicationInfo;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputMethodInfo;
import android.content.pm.PackageManager;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settingslib.inputmethod.InputMethodSettingValuesWrapper;
import java.util.ArrayList;
import android.view.inputmethod.InputMethodManager;
import android.app.admin.DevicePolicyManager;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settingslib.inputmethod.InputMethodPreference;
import java.text.Collator;
import java.util.Comparator;

public final class _$$Lambda$AvailableVirtualKeyboardFragment$jwIjaxSxVSRnK0I3ZX1KVHtd2wk implements Comparator
{
    @Override
    public final int compare(final Object o, final Object o2) {
        return AvailableVirtualKeyboardFragment.lambda$updateInputMethodPreferenceViews$0(this.f$0, (InputMethodPreference)o, (InputMethodPreference)o2);
    }
}
