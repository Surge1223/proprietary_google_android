package com.android.settings.inputmethod;

import android.view.InputDevice;
import android.os.Handler;
import android.os.Bundle;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.CheckBoxPreference;
import java.util.HashMap;
import android.hardware.input.KeyboardLayout;
import android.hardware.input.InputDeviceIdentifier;
import android.hardware.input.InputManager;
import android.hardware.input.InputManager$InputDeviceListener;
import com.android.settings.SettingsPreferenceFragment;

public class KeyboardLayoutPickerFragment extends SettingsPreferenceFragment implements InputManager$InputDeviceListener
{
    private InputManager mIm;
    private int mInputDeviceId;
    private InputDeviceIdentifier mInputDeviceIdentifier;
    private KeyboardLayout[] mKeyboardLayouts;
    private HashMap<CheckBoxPreference, KeyboardLayout> mPreferenceMap;
    
    public KeyboardLayoutPickerFragment() {
        this.mInputDeviceId = -1;
        this.mPreferenceMap = new HashMap<CheckBoxPreference, KeyboardLayout>();
    }
    
    private PreferenceScreen createPreferenceHierarchy() {
        final PreferenceScreen preferenceScreen = this.getPreferenceManager().createPreferenceScreen((Context)this.getActivity());
        for (final KeyboardLayout keyboardLayout : this.mKeyboardLayouts) {
            final CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this.getPrefContext());
            checkBoxPreference.setTitle(keyboardLayout.getLabel());
            checkBoxPreference.setSummary(keyboardLayout.getCollection());
            preferenceScreen.addPreference(checkBoxPreference);
            this.mPreferenceMap.put(checkBoxPreference, keyboardLayout);
        }
        return preferenceScreen;
    }
    
    private void updateCheckedState() {
        final String[] enabledKeyboardLayoutsForInputDevice = this.mIm.getEnabledKeyboardLayoutsForInputDevice(this.mInputDeviceIdentifier);
        Arrays.sort(enabledKeyboardLayoutsForInputDevice);
        for (final Map.Entry<CheckBoxPreference, KeyboardLayout> entry : this.mPreferenceMap.entrySet()) {
            entry.getKey().setChecked(Arrays.binarySearch(enabledKeyboardLayoutsForInputDevice, entry.getValue().getDescriptor()) >= 0);
        }
    }
    
    public int getMetricsCategory() {
        return 58;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mInputDeviceIdentifier = (InputDeviceIdentifier)this.getActivity().getIntent().getParcelableExtra("input_device_identifier");
        if (this.mInputDeviceIdentifier == null) {
            this.getActivity().finish();
        }
        this.mIm = (InputManager)this.getSystemService("input");
        Arrays.sort(this.mKeyboardLayouts = this.mIm.getKeyboardLayoutsForInputDevice(this.mInputDeviceIdentifier));
        this.setPreferenceScreen(this.createPreferenceHierarchy());
    }
    
    public void onInputDeviceAdded(final int n) {
    }
    
    public void onInputDeviceChanged(final int n) {
        if (this.mInputDeviceId >= 0 && n == this.mInputDeviceId) {
            this.updateCheckedState();
        }
    }
    
    public void onInputDeviceRemoved(final int n) {
        if (this.mInputDeviceId >= 0 && n == this.mInputDeviceId) {
            this.getActivity().finish();
        }
    }
    
    public void onPause() {
        this.mIm.unregisterInputDeviceListener((InputManager$InputDeviceListener)this);
        this.mInputDeviceId = -1;
        super.onPause();
    }
    
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof CheckBoxPreference) {
            final CheckBoxPreference checkBoxPreference = (CheckBoxPreference)preference;
            final KeyboardLayout keyboardLayout = this.mPreferenceMap.get(checkBoxPreference);
            if (keyboardLayout != null) {
                if (checkBoxPreference.isChecked()) {
                    this.mIm.addKeyboardLayoutForInputDevice(this.mInputDeviceIdentifier, keyboardLayout.getDescriptor());
                }
                else {
                    this.mIm.removeKeyboardLayoutForInputDevice(this.mInputDeviceIdentifier, keyboardLayout.getDescriptor());
                }
                return true;
            }
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mIm.registerInputDeviceListener((InputManager$InputDeviceListener)this, (Handler)null);
        final InputDevice inputDeviceByDescriptor = this.mIm.getInputDeviceByDescriptor(this.mInputDeviceIdentifier.getDescriptor());
        if (inputDeviceByDescriptor == null) {
            this.getActivity().finish();
            return;
        }
        this.mInputDeviceId = inputDeviceByDescriptor.getId();
        this.updateCheckedState();
    }
}
