package com.android.settings.inputmethod;

import android.text.TextUtils;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.ActivityNotFoundException;
import android.util.AttributeSet;
import android.content.Context;
import android.view.textservice.SpellCheckerInfo;
import android.content.Intent;
import com.android.settings.CustomListPreference;

class SpellCheckerPreference extends CustomListPreference
{
    private Intent mIntent;
    private final SpellCheckerInfo[] mScis;
    
    public SpellCheckerPreference(final Context context, final SpellCheckerInfo[] mScis) {
        super(context, null);
        this.mScis = mScis;
        this.setWidgetLayoutResource(2131558679);
        final CharSequence[] entries = new CharSequence[mScis.length];
        final CharSequence[] entryValues = new CharSequence[mScis.length];
        for (int i = 0; i < mScis.length; ++i) {
            entries[i] = mScis[i].loadLabel(context.getPackageManager());
            entryValues[i] = String.valueOf(i);
        }
        this.setEntries(entries);
        this.setEntryValues(entryValues);
    }
    
    private void onSettingsButtonClicked() {
        final Context context = this.getContext();
        try {
            final Intent mIntent = this.mIntent;
            if (mIntent != null) {
                context.startActivity(mIntent);
            }
        }
        catch (ActivityNotFoundException ex) {}
    }
    
    @Override
    public boolean callChangeListener(final Object o) {
        SpellCheckerInfo spellCheckerInfo;
        if (o != null) {
            spellCheckerInfo = this.mScis[Integer.parseInt((String)o)];
        }
        else {
            spellCheckerInfo = null;
        }
        return super.callChangeListener(spellCheckerInfo);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362594);
        int visibility;
        if (this.mIntent != null) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        viewById.setVisibility(visibility);
        viewById.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                SpellCheckerPreference.this.onSettingsButtonClicked();
            }
        });
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        alertDialog$Builder.setTitle(2131887010);
        alertDialog$Builder.setSingleChoiceItems(this.getEntries(), this.findIndexOfValue(this.getValue()), dialogInterface$OnClickListener);
    }
    
    public void setSelected(final SpellCheckerInfo spellCheckerInfo) {
        if (spellCheckerInfo == null) {
            this.setValue(null);
            return;
        }
        for (int i = 0; i < this.mScis.length; ++i) {
            if (this.mScis[i].getId().equals(spellCheckerInfo.getId())) {
                this.setValueIndex(i);
                return;
            }
        }
    }
    
    @Override
    public void setValue(final String value) {
        super.setValue(value);
        int int1;
        if (value != null) {
            int1 = Integer.parseInt(value);
        }
        else {
            int1 = -1;
        }
        if (int1 == -1) {
            this.mIntent = null;
            return;
        }
        final SpellCheckerInfo spellCheckerInfo = this.mScis[int1];
        final String settingsActivity = spellCheckerInfo.getSettingsActivity();
        if (TextUtils.isEmpty((CharSequence)settingsActivity)) {
            this.mIntent = null;
        }
        else {
            (this.mIntent = new Intent("android.intent.action.MAIN")).setClassName(spellCheckerInfo.getPackageName(), settingsActivity);
        }
    }
}
