package com.android.settings.inputmethod;

import android.database.Cursor;
import android.os.Bundle;
import com.android.settings.Utils;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.app.Activity;
import android.support.v7.preference.PreferenceGroup;
import java.util.Iterator;
import java.util.Locale;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodSubtype;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.provider.UserDictionary$Words;
import java.util.TreeSet;
import android.content.Context;
import com.android.settings.SettingsPreferenceFragment;

public class UserDictionaryList extends SettingsPreferenceFragment
{
    private String mLocale;
    
    public static TreeSet<String> getUserDictionaryLocalesSet(final Context context) {
        Object query = context.getContentResolver().query(UserDictionary$Words.CONTENT_URI, new String[] { "locale" }, (String)null, (String[])null, (String)null);
        final TreeSet<String> set = new TreeSet<String>();
        if (query == null) {
            return set;
        }
        try {
            if (((Cursor)query).moveToFirst()) {
                final int columnIndex = ((Cursor)query).getColumnIndex("locale");
                do {
                    String string = ((Cursor)query).getString(columnIndex);
                    if (string == null) {
                        string = "";
                    }
                    set.add(string);
                } while (((Cursor)query).moveToNext());
            }
            ((Cursor)query).close();
            query = context.getSystemService("input_method");
            final Iterator iterator = ((InputMethodManager)query).getEnabledInputMethodList().iterator();
            while (iterator.hasNext()) {
                final Iterator iterator2 = ((InputMethodManager)query).getEnabledInputMethodSubtypeList((InputMethodInfo)iterator.next(), true).iterator();
                while (iterator2.hasNext()) {
                    final String locale = iterator2.next().getLocale();
                    if (!TextUtils.isEmpty((CharSequence)locale)) {
                        set.add(locale);
                    }
                }
            }
            if (!set.contains(Locale.getDefault().getLanguage().toString())) {
                set.add(Locale.getDefault().toString());
            }
            return set;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    protected void createUserDictSettings(final PreferenceGroup preferenceGroup) {
        final Activity activity = this.getActivity();
        preferenceGroup.removeAll();
        final TreeSet<String> userDictionaryLocalesSet = getUserDictionaryLocalesSet((Context)activity);
        if (this.mLocale != null) {
            userDictionaryLocalesSet.add(this.mLocale);
        }
        if (userDictionaryLocalesSet.size() > 1) {
            userDictionaryLocalesSet.add("");
        }
        if (userDictionaryLocalesSet.isEmpty()) {
            preferenceGroup.addPreference(this.createUserDictionaryPreference(null, activity));
        }
        else {
            final Iterator<String> iterator = userDictionaryLocalesSet.iterator();
            while (iterator.hasNext()) {
                preferenceGroup.addPreference(this.createUserDictionaryPreference(iterator.next(), activity));
            }
        }
    }
    
    protected Preference createUserDictionaryPreference(final String s, final Activity activity) {
        final Preference preference = new Preference(this.getPrefContext());
        final Intent intent = new Intent("android.settings.USER_DICTIONARY_SETTINGS");
        if (s == null) {
            preference.setTitle(Locale.getDefault().getDisplayName());
        }
        else {
            if ("".equals(s)) {
                preference.setTitle(this.getString(2131889715));
            }
            else {
                preference.setTitle(Utils.createLocaleFromString(s).getDisplayName());
            }
            intent.putExtra("locale", s);
            preference.getExtras().putString("locale", s);
        }
        preference.setIntent(intent);
        preference.setFragment(UserDictionarySettings.class.getName());
        return preference;
    }
    
    @Override
    public int getMetricsCategory() {
        return 61;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.getActivity().getActionBar().setTitle(2131889722);
        final Intent intent = this.getActivity().getIntent();
        final String s = null;
        String stringExtra;
        if (intent == null) {
            stringExtra = null;
        }
        else {
            stringExtra = intent.getStringExtra("locale");
        }
        final Bundle arguments = this.getArguments();
        String string;
        if (arguments == null) {
            string = null;
        }
        else {
            string = arguments.getString("locale");
        }
        if (string != null) {
            stringExtra = string;
        }
        else if (stringExtra == null) {
            stringExtra = s;
        }
        this.mLocale = stringExtra;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setPreferenceScreen(this.getPreferenceManager().createPreferenceScreen((Context)this.getActivity()));
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.createUserDictSettings(this.getPreferenceScreen());
    }
}
