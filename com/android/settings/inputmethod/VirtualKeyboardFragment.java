package com.android.settings.inputmethod;

import com.android.internal.util.Preconditions;
import android.app.Activity;
import android.os.Bundle;
import com.android.settingslib.inputmethod.InputMethodAndSubtypeUtil;
import java.util.Comparator;
import android.view.inputmethod.InputMethodInfo;
import java.text.Collator;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.graphics.drawable.ColorDrawable;
import com.android.settingslib.inputmethod.InputMethodPreference;
import java.util.ArrayList;
import android.view.inputmethod.InputMethodManager;
import android.app.admin.DevicePolicyManager;
import android.support.v7.preference.Preference;
import android.graphics.drawable.Drawable;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public final class VirtualKeyboardFragment extends SettingsPreferenceFragment implements Indexable
{
    private static final Drawable NO_ICON;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private Preference mAddVirtualKeyboardScreen;
    private DevicePolicyManager mDpm;
    private InputMethodManager mImm;
    private final ArrayList<InputMethodPreference> mInputMethodPreferenceList;
    
    static {
        NO_ICON = (Drawable)new ColorDrawable(0);
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("add_virtual_keyboard_screen");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082856;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    public VirtualKeyboardFragment() {
        this.mInputMethodPreferenceList = new ArrayList<InputMethodPreference>();
    }
    
    private void updateInputMethodPreferenceViews() {
        this.mInputMethodPreferenceList.clear();
        final List permittedInputMethodsForCurrentUser = this.mDpm.getPermittedInputMethodsForCurrentUser();
        final Context prefContext = this.getPrefContext();
        final List enabledInputMethodList = this.mImm.getEnabledInputMethodList();
        final int n = 0;
        int size;
        if (enabledInputMethodList == null) {
            size = 0;
        }
        else {
            size = enabledInputMethodList.size();
        }
        for (int i = 0; i < size; ++i) {
            final InputMethodInfo inputMethodInfo = enabledInputMethodList.get(i);
            boolean b;
            if (permittedInputMethodsForCurrentUser != null && !permittedInputMethodsForCurrentUser.contains(inputMethodInfo.getPackageName())) {
                b = false;
            }
            else {
                b = true;
            }
            Drawable icon;
            try {
                icon = this.getActivity().getPackageManager().getApplicationIcon(inputMethodInfo.getPackageName());
            }
            catch (Exception ex) {
                icon = VirtualKeyboardFragment.NO_ICON;
            }
            final InputMethodPreference inputMethodPreference = new InputMethodPreference(prefContext, inputMethodInfo, false, b, null);
            inputMethodPreference.setIcon(icon);
            this.mInputMethodPreferenceList.add(inputMethodPreference);
        }
        this.mInputMethodPreferenceList.sort(new _$$Lambda$VirtualKeyboardFragment$3eczHKaadmVH3sZXf9rlrdYqLjw(Collator.getInstance()));
        this.getPreferenceScreen().removeAll();
        for (int j = n; j < size; ++j) {
            final InputMethodPreference inputMethodPreference2 = this.mInputMethodPreferenceList.get(j);
            inputMethodPreference2.setOrder(j);
            this.getPreferenceScreen().addPreference(inputMethodPreference2);
            InputMethodAndSubtypeUtil.removeUnnecessaryNonPersistentPreference(inputMethodPreference2);
            inputMethodPreference2.updatePreferenceViews();
        }
        this.mAddVirtualKeyboardScreen.setIcon(2131230947);
        this.mAddVirtualKeyboardScreen.setOrder(size);
        this.getPreferenceScreen().addPreference(this.mAddVirtualKeyboardScreen);
    }
    
    @Override
    public int getMetricsCategory() {
        return 345;
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        final Activity activity = (Activity)Preconditions.checkNotNull((Object)this.getActivity());
        this.addPreferencesFromResource(2132082856);
        this.mImm = (InputMethodManager)Preconditions.checkNotNull((Object)activity.getSystemService((Class)InputMethodManager.class));
        this.mDpm = (DevicePolicyManager)Preconditions.checkNotNull((Object)activity.getSystemService((Class)DevicePolicyManager.class));
        this.mAddVirtualKeyboardScreen = (Preference)Preconditions.checkNotNull((Object)this.findPreference("add_virtual_keyboard_screen"));
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.updateInputMethodPreferenceViews();
    }
}
