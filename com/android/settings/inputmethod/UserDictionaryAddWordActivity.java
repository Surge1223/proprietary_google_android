package com.android.settings.inputmethod;

import android.content.Context;
import android.view.View;
import android.content.Intent;
import android.os.RemoteException;
import android.os.Message;
import android.os.Messenger;
import android.os.Bundle;
import android.app.Activity;

public class UserDictionaryAddWordActivity extends Activity
{
    private UserDictionaryAddWordContents mContents;
    
    private void reportBackToCaller(final int what, final Bundle obj) {
        final Intent intent = this.getIntent();
        if (intent.getExtras() == null) {
            return;
        }
        final Object value = intent.getExtras().get("listener");
        if (!(value instanceof Messenger)) {
            return;
        }
        final Messenger messenger = (Messenger)value;
        final Message obtain = Message.obtain();
        obtain.obj = obj;
        obtain.what = what;
        try {
            messenger.send(obtain);
        }
        catch (RemoteException ex) {}
    }
    
    public void onClickCancel(final View view) {
        this.reportBackToCaller(1, null);
        this.finish();
    }
    
    public void onClickConfirm(final View view) {
        final Bundle bundle = new Bundle();
        this.reportBackToCaller(this.mContents.apply((Context)this, bundle), bundle);
        this.finish();
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131558871);
        final Intent intent = this.getIntent();
        final String action = intent.getAction();
        int n;
        if ("com.android.settings.USER_DICTIONARY_EDIT".equals(action)) {
            n = 0;
        }
        else {
            if (!"com.android.settings.USER_DICTIONARY_INSERT".equals(action)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported action: ");
                sb.append(action);
                throw new RuntimeException(sb.toString());
            }
            n = 1;
        }
        Bundle extras;
        if ((extras = intent.getExtras()) == null) {
            extras = new Bundle();
        }
        extras.putInt("mode", n);
        if (bundle != null) {
            extras.putAll(bundle);
        }
        this.mContents = new UserDictionaryAddWordContents(this.getWindow().getDecorView(), extras);
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        this.mContents.saveStateIntoBundle(bundle);
    }
}
