package com.android.settings.inputmethod;

import com.android.internal.util.Preconditions;
import android.app.Activity;
import android.os.Bundle;
import com.android.settingslib.inputmethod.InputMethodAndSubtypeUtil;
import android.view.inputmethod.InputMethodInfo;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.graphics.drawable.ColorDrawable;
import java.util.ArrayList;
import android.view.inputmethod.InputMethodManager;
import android.app.admin.DevicePolicyManager;
import android.support.v7.preference.Preference;
import android.graphics.drawable.Drawable;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settingslib.inputmethod.InputMethodPreference;
import java.text.Collator;
import java.util.Comparator;

public final class _$$Lambda$VirtualKeyboardFragment$3eczHKaadmVH3sZXf9rlrdYqLjw implements Comparator
{
    @Override
    public final int compare(final Object o, final Object o2) {
        return VirtualKeyboardFragment.lambda$updateInputMethodPreferenceViews$0(this.f$0, (InputMethodPreference)o, (InputMethodPreference)o2);
    }
}
