package com.android.settings.inputmethod;

import android.app.ActionBar;
import android.os.Bundle;
import android.content.Intent;
import com.android.settings.SettingsActivity;

public class InputMethodAndSubtypeEnablerActivity extends SettingsActivity
{
    private static final String FRAGMENT_NAME;
    
    static {
        FRAGMENT_NAME = InputMethodAndSubtypeEnabler.class.getName();
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        if (!intent.hasExtra(":settings:show_fragment")) {
            intent.putExtra(":settings:show_fragment", InputMethodAndSubtypeEnablerActivity.FRAGMENT_NAME);
        }
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return InputMethodAndSubtypeEnablerActivity.FRAGMENT_NAME.equals(s);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final ActionBar actionBar = this.getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }
    
    @Override
    public boolean onNavigateUp() {
        this.finish();
        return true;
    }
}
