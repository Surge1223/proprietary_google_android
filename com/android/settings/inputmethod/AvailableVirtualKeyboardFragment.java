package com.android.settings.inputmethod;

import java.util.Iterator;
import android.support.v14.preference.PreferenceFragment;
import android.app.Activity;
import android.os.Bundle;
import com.android.settingslib.inputmethod.InputMethodAndSubtypeUtil;
import android.support.v7.preference.Preference;
import java.util.Comparator;
import java.text.Collator;
import android.content.pm.ApplicationInfo;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputMethodInfo;
import android.content.pm.PackageManager;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settingslib.inputmethod.InputMethodSettingValuesWrapper;
import java.util.ArrayList;
import android.view.inputmethod.InputMethodManager;
import android.app.admin.DevicePolicyManager;
import com.android.settingslib.inputmethod.InputMethodPreference;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public final class AvailableVirtualKeyboardFragment extends SettingsPreferenceFragment implements Indexable, OnSavePreferenceListener
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private DevicePolicyManager mDpm;
    private InputMethodManager mImm;
    private final ArrayList<InputMethodPreference> mInputMethodPreferenceList;
    private InputMethodSettingValuesWrapper mInputMethodSettingValues;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082720;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    public AvailableVirtualKeyboardFragment() {
        this.mInputMethodPreferenceList = new ArrayList<InputMethodPreference>();
    }
    
    private static Drawable getInputMethodIcon(final PackageManager packageManager, final InputMethodInfo inputMethodInfo) {
        final ServiceInfo serviceInfo = inputMethodInfo.getServiceInfo();
        ApplicationInfo applicationInfo;
        if (serviceInfo != null) {
            applicationInfo = serviceInfo.applicationInfo;
        }
        else {
            applicationInfo = null;
        }
        final String packageName = inputMethodInfo.getPackageName();
        if (serviceInfo == null || applicationInfo == null || packageName == null) {
            return (Drawable)new ColorDrawable(0);
        }
        final Drawable loadDrawable = loadDrawable(packageManager, packageName, serviceInfo.logo, applicationInfo);
        if (loadDrawable != null) {
            return loadDrawable;
        }
        final Drawable loadDrawable2 = loadDrawable(packageManager, packageName, serviceInfo.icon, applicationInfo);
        if (loadDrawable2 != null) {
            return loadDrawable2;
        }
        final Drawable loadDrawable3 = loadDrawable(packageManager, packageName, applicationInfo.logo, applicationInfo);
        if (loadDrawable3 != null) {
            return loadDrawable3;
        }
        final Drawable loadDrawable4 = loadDrawable(packageManager, packageName, applicationInfo.icon, applicationInfo);
        if (loadDrawable4 != null) {
            return loadDrawable4;
        }
        return (Drawable)new ColorDrawable(0);
    }
    
    private static Drawable loadDrawable(final PackageManager packageManager, final String s, final int n, final ApplicationInfo applicationInfo) {
        if (n == 0) {
            return null;
        }
        try {
            return packageManager.getDrawable(s, n, applicationInfo);
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    private void updateInputMethodPreferenceViews() {
        this.mInputMethodSettingValues.refreshAllInputMethodAndSubtypes();
        this.mInputMethodPreferenceList.clear();
        final List permittedInputMethodsForCurrentUser = this.mDpm.getPermittedInputMethodsForCurrentUser();
        final Context prefContext = this.getPrefContext();
        final PackageManager packageManager = this.getActivity().getPackageManager();
        final List<InputMethodInfo> inputMethodList = this.mInputMethodSettingValues.getInputMethodList();
        final int n = 0;
        int size;
        if (inputMethodList == null) {
            size = 0;
        }
        else {
            size = inputMethodList.size();
        }
        for (int i = 0; i < size; ++i) {
            final InputMethodInfo inputMethodInfo = inputMethodList.get(i);
            final InputMethodPreference inputMethodPreference = new InputMethodPreference(prefContext, inputMethodInfo, true, permittedInputMethodsForCurrentUser == null || permittedInputMethodsForCurrentUser.contains(inputMethodInfo.getPackageName()), (InputMethodPreference.OnSavePreferenceListener)this);
            inputMethodPreference.setIcon(getInputMethodIcon(packageManager, inputMethodInfo));
            this.mInputMethodPreferenceList.add(inputMethodPreference);
        }
        this.mInputMethodPreferenceList.sort(new _$$Lambda$AvailableVirtualKeyboardFragment$jwIjaxSxVSRnK0I3ZX1KVHtd2wk(Collator.getInstance()));
        this.getPreferenceScreen().removeAll();
        for (int j = n; j < size; ++j) {
            final InputMethodPreference inputMethodPreference2 = this.mInputMethodPreferenceList.get(j);
            inputMethodPreference2.setOrder(j);
            this.getPreferenceScreen().addPreference(inputMethodPreference2);
            InputMethodAndSubtypeUtil.removeUnnecessaryNonPersistentPreference(inputMethodPreference2);
            inputMethodPreference2.updatePreferenceViews();
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 347;
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        this.addPreferencesFromResource(2132082720);
        final Activity activity = this.getActivity();
        this.mInputMethodSettingValues = InputMethodSettingValuesWrapper.getInstance((Context)activity);
        this.mImm = (InputMethodManager)activity.getSystemService((Class)InputMethodManager.class);
        this.mDpm = (DevicePolicyManager)activity.getSystemService((Class)DevicePolicyManager.class);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mInputMethodSettingValues.refreshAllInputMethodAndSubtypes();
        this.updateInputMethodPreferenceViews();
    }
    
    @Override
    public void onSaveInputMethodPreference(final InputMethodPreference inputMethodPreference) {
        InputMethodAndSubtypeUtil.saveInputMethodSubtypeList(this, this.getContentResolver(), this.mImm.getInputMethodList(), this.getResources().getConfiguration().keyboard == 2);
        this.mInputMethodSettingValues.refreshAllInputMethodAndSubtypes();
        final Iterator<InputMethodPreference> iterator = this.mInputMethodPreferenceList.iterator();
        while (iterator.hasNext()) {
            iterator.next().updatePreferenceViews();
        }
    }
}
