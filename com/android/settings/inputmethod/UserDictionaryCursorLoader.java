package com.android.settings.inputmethod;

import java.util.Set;
import java.util.Objects;
import android.util.ArraySet;
import java.util.Locale;
import android.provider.UserDictionary$Words;
import android.database.MatrixCursor;
import android.database.Cursor;
import android.content.Context;
import android.content.CursorLoader;

public class UserDictionaryCursorLoader extends CursorLoader
{
    static final String[] QUERY_PROJECTION;
    private final String mLocale;
    
    static {
        QUERY_PROJECTION = new String[] { "_id", "word", "shortcut" };
    }
    
    public UserDictionaryCursorLoader(final Context context, final String mLocale) {
        super(context);
        this.mLocale = mLocale;
    }
    
    public Cursor loadInBackground() {
        final MatrixCursor matrixCursor = new MatrixCursor(UserDictionaryCursorLoader.QUERY_PROJECTION);
        Cursor cursor;
        if ("".equals(this.mLocale)) {
            cursor = this.getContext().getContentResolver().query(UserDictionary$Words.CONTENT_URI, UserDictionaryCursorLoader.QUERY_PROJECTION, "locale is null", (String[])null, "UPPER(word)");
        }
        else {
            String s;
            if (this.mLocale != null) {
                s = this.mLocale;
            }
            else {
                s = Locale.getDefault().toString();
            }
            cursor = this.getContext().getContentResolver().query(UserDictionary$Words.CONTENT_URI, UserDictionaryCursorLoader.QUERY_PROJECTION, "locale=?", new String[] { s }, "UPPER(word)");
        }
        final ArraySet set = new ArraySet();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            final int int1 = cursor.getInt(0);
            final String string = cursor.getString(1);
            final String string2 = cursor.getString(2);
            final int hash = Objects.hash(string, string2);
            if (!((Set)set).contains(hash)) {
                ((Set<Integer>)set).add(hash);
                matrixCursor.addRow(new Object[] { int1, string, string2 });
            }
            cursor.moveToNext();
        }
        return (Cursor)matrixCursor;
    }
}
