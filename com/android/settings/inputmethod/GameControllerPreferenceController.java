package com.android.settings.inputmethod;

import android.os.Handler;
import android.content.ContentResolver;
import android.provider.Settings;
import android.view.InputDevice;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.hardware.input.InputManager;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.hardware.input.InputManager$InputDeviceListener;
import com.android.settings.core.TogglePreferenceController;

public class GameControllerPreferenceController extends TogglePreferenceController implements InputManager$InputDeviceListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final InputManager mIm;
    private Preference mPreference;
    
    public GameControllerPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mIm = (InputManager)context.getSystemService("input");
    }
    
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    public int getAvailabilityStatus() {
        if (!this.mContext.getResources().getBoolean(2131034171)) {
            return 2;
        }
        final int[] inputDeviceIds = this.mIm.getInputDeviceIds();
        for (int length = inputDeviceIds.length, i = 0; i < length; ++i) {
            final InputDevice inputDevice = this.mIm.getInputDevice(inputDeviceIds[i]);
            if (inputDevice != null && !inputDevice.isVirtual() && inputDevice.getVibrator().hasVibrator()) {
                return 0;
            }
        }
        return 1;
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.System.getInt(contentResolver, "vibrate_input_devices", 1) <= 0) {
            b = false;
        }
        return b;
    }
    
    public void onInputDeviceAdded(final int n) {
        this.updateState(this.mPreference);
    }
    
    public void onInputDeviceChanged(final int n) {
        this.updateState(this.mPreference);
    }
    
    public void onInputDeviceRemoved(final int n) {
        this.updateState(this.mPreference);
    }
    
    public void onPause() {
        this.mIm.unregisterInputDeviceListener((InputManager$InputDeviceListener)this);
    }
    
    public void onResume() {
        this.mIm.registerInputDeviceListener((InputManager$InputDeviceListener)this, (Handler)null);
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return Settings.System.putInt(this.mContext.getContentResolver(), "vibrate_input_devices", (int)(b ? 1 : 0));
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (preference == null) {
            return;
        }
        this.mPreference.setVisible(this.isAvailable());
    }
}
