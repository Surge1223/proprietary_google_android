package com.android.settings.inputmethod;

import android.widget.SimpleCursorAdapter$ViewBinder;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.view.MenuItem;
import android.widget.ListView;
import android.app.ActionBar;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.Loader;
import android.content.Intent;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.provider.UserDictionary$Words;
import android.text.TextUtils;
import android.content.ContentResolver;
import android.content.Context;
import android.widget.ListAdapter;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.database.Cursor;
import android.app.LoaderManager$LoaderCallbacks;
import android.app.ListFragment;

public class UserDictionarySettings extends ListFragment implements LoaderManager$LoaderCallbacks<Cursor>, Instrumentable
{
    private Cursor mCursor;
    private String mLocale;
    private VisibilityLoggerMixin mVisibilityLoggerMixin;
    
    private ListAdapter createAdapter() {
        return (ListAdapter)new MyAdapter((Context)this.getActivity(), 2131558873, this.mCursor, new String[] { "word", "shortcut" }, new int[] { 16908308, 16908309 });
    }
    
    public static void deleteWord(final String s, final String s2, final ContentResolver contentResolver) {
        if (TextUtils.isEmpty((CharSequence)s2)) {
            contentResolver.delete(UserDictionary$Words.CONTENT_URI, "word=? AND shortcut is null OR shortcut=''", new String[] { s });
        }
        else {
            contentResolver.delete(UserDictionary$Words.CONTENT_URI, "word=? AND shortcut=?", new String[] { s, s2 });
        }
    }
    
    private String getShortcut(final int n) {
        if (this.mCursor == null) {
            return null;
        }
        this.mCursor.moveToPosition(n);
        if (this.mCursor.isAfterLast()) {
            return null;
        }
        return this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("shortcut"));
    }
    
    private String getWord(final int n) {
        if (this.mCursor == null) {
            return null;
        }
        this.mCursor.moveToPosition(n);
        if (this.mCursor.isAfterLast()) {
            return null;
        }
        return this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("word"));
    }
    
    private void showAddOrEditDialog(final String s, final String s2) {
        final Bundle arguments = new Bundle();
        int n;
        if (s == null) {
            n = 1;
        }
        else {
            n = 0;
        }
        arguments.putInt("mode", n);
        arguments.putString("word", s);
        arguments.putString("shortcut", s2);
        arguments.putString("locale", this.mLocale);
        new SubSettingLauncher(this.getContext()).setDestination(UserDictionaryAddWordFragment.class.getName()).setArguments(arguments).setTitle(2131889707).setSourceMetricsCategory(this.getMetricsCategory()).launch();
    }
    
    public int getMetricsCategory() {
        return 514;
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mVisibilityLoggerMixin = new VisibilityLoggerMixin(this.getMetricsCategory(), FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider());
        final Intent intent = this.getActivity().getIntent();
        String stringExtra;
        if (intent == null) {
            stringExtra = null;
        }
        else {
            stringExtra = intent.getStringExtra("locale");
        }
        final Bundle arguments = this.getArguments();
        String string;
        if (arguments == null) {
            string = null;
        }
        else {
            string = arguments.getString("locale");
        }
        if (string != null) {
            stringExtra = string;
        }
        else if (stringExtra == null) {
            stringExtra = null;
        }
        this.mLocale = stringExtra;
        this.setHasOptionsMenu(true);
        this.getLoaderManager().initLoader(1, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
    }
    
    public Loader<Cursor> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<Cursor>)new UserDictionaryCursorLoader(this.getContext(), this.mLocale);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        menu.add(0, 1, 0, 2131889709).setIcon(2131231066).setShowAsAction(5);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final ActionBar actionBar = this.getActivity().getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(2131889722);
            actionBar.setSubtitle((CharSequence)UserDictionarySettingsUtils.getLocaleDisplayName((Context)this.getActivity(), this.mLocale));
        }
        return layoutInflater.inflate(17367234, viewGroup, false);
    }
    
    public void onListItemClick(final ListView listView, final View view, final int n, final long n2) {
        final String word = this.getWord(n);
        final String shortcut = this.getShortcut(n);
        if (word != null) {
            this.showAddOrEditDialog(word, shortcut);
        }
    }
    
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor mCursor) {
        this.mCursor = mCursor;
        this.getListView().setAdapter(this.createAdapter());
    }
    
    public void onLoaderReset(final Loader<Cursor> loader) {
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 1) {
            this.showAddOrEditDialog(null, null);
            return true;
        }
        return false;
    }
    
    public void onPause() {
        super.onPause();
        this.mVisibilityLoggerMixin.onPause();
    }
    
    public void onResume() {
        super.onResume();
        this.mVisibilityLoggerMixin.onResume();
        this.getLoaderManager().restartLoader(1, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        final TextView emptyView = (TextView)this.getView().findViewById(16908292);
        emptyView.setText(2131889719);
        final ListView listView = this.getListView();
        listView.setFastScrollEnabled(true);
        listView.setEmptyView((View)emptyView);
    }
    
    private static class MyAdapter extends SimpleCursorAdapter implements SectionIndexer
    {
        private AlphabetIndexer mIndexer;
        private final SimpleCursorAdapter$ViewBinder mViewBinder;
        
        public MyAdapter(final Context context, final int n, final Cursor cursor, final String[] array, final int[] array2) {
            super(context, n, cursor, array, array2);
            this.mViewBinder = (SimpleCursorAdapter$ViewBinder)new SimpleCursorAdapter$ViewBinder() {
                public boolean setViewValue(final View view, final Cursor cursor, final int n) {
                    if (n == 2) {
                        final String string = cursor.getString(2);
                        if (TextUtils.isEmpty((CharSequence)string)) {
                            view.setVisibility(8);
                        }
                        else {
                            ((TextView)view).setText((CharSequence)string);
                            view.setVisibility(0);
                        }
                        view.invalidate();
                        return true;
                    }
                    return false;
                }
            };
            if (cursor != null) {
                this.mIndexer = new AlphabetIndexer(cursor, cursor.getColumnIndexOrThrow("word"), (CharSequence)context.getString(17039893));
            }
            this.setViewBinder(this.mViewBinder);
        }
        
        public int getPositionForSection(int positionForSection) {
            if (this.mIndexer == null) {
                positionForSection = 0;
            }
            else {
                positionForSection = this.mIndexer.getPositionForSection(positionForSection);
            }
            return positionForSection;
        }
        
        public int getSectionForPosition(int sectionForPosition) {
            if (this.mIndexer == null) {
                sectionForPosition = 0;
            }
            else {
                sectionForPosition = this.mIndexer.getSectionForPosition(sectionForPosition);
            }
            return sectionForPosition;
        }
        
        public Object[] getSections() {
            Object[] sections;
            if (this.mIndexer == null) {
                sections = null;
            }
            else {
                sections = this.mIndexer.getSections();
            }
            return sections;
        }
    }
}
