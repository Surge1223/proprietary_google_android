package com.android.settings;

import android.widget.EditText;
import android.view.View;
import android.app.Dialog;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.CustomEditTextPreference;

class EditPinPreference extends CustomEditTextPreference
{
    private OnPinEnteredListener mPinListener;
    
    public EditPinPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public EditPinPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    public boolean isDialogOpen() {
        final Dialog dialog = this.getDialog();
        return dialog != null && dialog.isShowing();
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        final EditText editText = (EditText)view.findViewById(16908291);
        if (editText != null) {
            editText.setInputType(18);
            editText.setTextAlignment(5);
        }
    }
    
    @Override
    protected void onDialogClosed(final boolean b) {
        super.onDialogClosed(b);
        if (this.mPinListener != null) {
            this.mPinListener.onPinEntered(this, b);
        }
    }
    
    public void setOnPinEnteredListener(final OnPinEnteredListener mPinListener) {
        this.mPinListener = mPinListener;
    }
    
    public void showPinDialog() {
        final Dialog dialog = this.getDialog();
        if (dialog == null || !dialog.isShowing()) {
            this.onClick();
        }
    }
    
    interface OnPinEnteredListener
    {
        void onPinEntered(final EditPinPreference p0, final boolean p1);
    }
}
