package com.android.settings;

import android.view.MotionEvent;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.FrameLayout;

public class TouchBlockingFrameLayout extends FrameLayout
{
    public TouchBlockingFrameLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        return false;
    }
}
