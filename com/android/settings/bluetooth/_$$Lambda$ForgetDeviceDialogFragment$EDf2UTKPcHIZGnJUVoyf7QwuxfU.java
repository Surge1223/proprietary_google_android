package com.android.settings.bluetooth;

import android.app.AlertDialog;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$ForgetDeviceDialogFragment$EDf2UTKPcHIZGnJUVoyf7QwuxfU implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        ForgetDeviceDialogFragment.lambda$onCreateDialog$0(this.f$0, dialogInterface, n);
    }
}
