package com.android.settings.bluetooth;

import android.content.IntentFilter;
import android.os.Bundle;
import android.content.DialogInterface;
import com.android.internal.app.AlertController$AlertParams;
import android.os.Parcelable;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.util.Log;
import android.view.ViewGroup;
import android.content.Intent;
import android.content.Context;
import android.widget.TextView;
import android.view.View;
import android.content.BroadcastReceiver;
import android.widget.Button;
import android.bluetooth.BluetoothDevice;
import android.support.v7.preference.Preference;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public class BluetoothPermissionActivity extends AlertActivity implements DialogInterface$OnClickListener, OnPreferenceChangeListener
{
    private BluetoothDevice mDevice;
    private Button mOkButton;
    private BroadcastReceiver mReceiver;
    private boolean mReceiverRegistered;
    private int mRequestType;
    private String mReturnClass;
    private String mReturnPackage;
    private View mView;
    private TextView messageView;
    
    public BluetoothPermissionActivity() {
        this.mReturnPackage = null;
        this.mReturnClass = null;
        this.mRequestType = 0;
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL")) {
                    if (intent.getIntExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", 2) != BluetoothPermissionActivity.this.mRequestType) {
                        return;
                    }
                    if (BluetoothPermissionActivity.this.mDevice.equals((Object)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE"))) {
                        BluetoothPermissionActivity.this.dismissDialog();
                    }
                }
            }
        };
        this.mReceiverRegistered = false;
    }
    
    private View createConnectionDialogView() {
        final String remoteName = Utils.createRemoteName((Context)this, this.mDevice);
        this.mView = this.getLayoutInflater().inflate(2131558472, (ViewGroup)null);
        (this.messageView = (TextView)this.mView.findViewById(R.id.message)).setText((CharSequence)this.getString(2131886723, new Object[] { remoteName }));
        return this.mView;
    }
    
    private View createMapDialogView() {
        final String remoteName = Utils.createRemoteName((Context)this, this.mDevice);
        this.mView = this.getLayoutInflater().inflate(2131558472, (ViewGroup)null);
        (this.messageView = (TextView)this.mView.findViewById(R.id.message)).setText((CharSequence)this.getString(2131886795, new Object[] { remoteName, remoteName }));
        return this.mView;
    }
    
    private View createPhonebookDialogView() {
        final String remoteName = Utils.createRemoteName((Context)this, this.mDevice);
        this.mView = this.getLayoutInflater().inflate(2131558472, (ViewGroup)null);
        (this.messageView = (TextView)this.mView.findViewById(R.id.message)).setText((CharSequence)this.getString(2131886838, new Object[] { remoteName, remoteName }));
        return this.mView;
    }
    
    private View createSapDialogView() {
        final String remoteName = Utils.createRemoteName((Context)this, this.mDevice);
        this.mView = this.getLayoutInflater().inflate(2131558472, (ViewGroup)null);
        (this.messageView = (TextView)this.mView.findViewById(R.id.message)).setText((CharSequence)this.getString(2131886873, new Object[] { remoteName, remoteName }));
        return this.mView;
    }
    
    private void dismissDialog() {
        this.dismiss();
    }
    
    private void onNegative() {
        Log.d("BluetoothPermissionActivity", "onNegative");
        boolean checkAndIncreaseMessageRejectionCount = true;
        if (this.mRequestType == 3) {
            final LocalBluetoothManager localBtManager = Utils.getLocalBtManager((Context)this);
            final CachedBluetoothDeviceManager cachedDeviceManager = localBtManager.getCachedDeviceManager();
            CachedBluetoothDevice cachedBluetoothDevice;
            if ((cachedBluetoothDevice = cachedDeviceManager.findDevice(this.mDevice)) == null) {
                cachedBluetoothDevice = cachedDeviceManager.addDevice(localBtManager.getBluetoothAdapter(), localBtManager.getProfileManager(), this.mDevice);
            }
            checkAndIncreaseMessageRejectionCount = cachedBluetoothDevice.checkAndIncreaseMessageRejectionCount();
        }
        this.sendReplyIntentToReceiver(false, checkAndIncreaseMessageRejectionCount);
    }
    
    private void onPositive() {
        Log.d("BluetoothPermissionActivity", "onPositive");
        this.sendReplyIntentToReceiver(true, true);
        this.finish();
    }
    
    private void sendReplyIntentToReceiver(final boolean b, final boolean b2) {
        final Intent intent = new Intent("android.bluetooth.device.action.CONNECTION_ACCESS_REPLY");
        if (this.mReturnPackage != null && this.mReturnClass != null) {
            intent.setClassName(this.mReturnPackage, this.mReturnClass);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("sendReplyIntentToReceiver() Request type: ");
        sb.append(this.mRequestType);
        sb.append(" mReturnPackage");
        sb.append(this.mReturnPackage);
        sb.append(" mReturnClass");
        sb.append(this.mReturnClass);
        Log.i("BluetoothPermissionActivity", sb.toString());
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 2;
        }
        intent.putExtra("android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT", n);
        intent.putExtra("android.bluetooth.device.extra.ALWAYS_ALLOWED", b2);
        intent.putExtra("android.bluetooth.device.extra.DEVICE", (Parcelable)this.mDevice);
        intent.putExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", this.mRequestType);
        this.sendBroadcast(intent, "android.permission.BLUETOOTH_ADMIN");
    }
    
    private void showDialog(final String mTitle, final int n) {
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        mAlertParams.mTitle = mTitle;
        final StringBuilder sb = new StringBuilder();
        sb.append("showDialog() Request type: ");
        sb.append(this.mRequestType);
        sb.append(" this: ");
        sb.append(this);
        Log.i("BluetoothPermissionActivity", sb.toString());
        switch (n) {
            case 4: {
                mAlertParams.mView = this.createSapDialogView();
                break;
            }
            case 3: {
                mAlertParams.mView = this.createMapDialogView();
                break;
            }
            case 2: {
                mAlertParams.mView = this.createPhonebookDialogView();
                break;
            }
            case 1: {
                mAlertParams.mView = this.createConnectionDialogView();
                break;
            }
        }
        mAlertParams.mPositiveButtonText = this.getString(2131890232);
        mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
        mAlertParams.mNegativeButtonText = this.getString(2131888406);
        mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
        this.mOkButton = this.mAlert.getButton(-1);
        this.setupAlert();
    }
    
    public void onBackPressed() {
        Log.i("BluetoothPermissionActivity", "Back button pressed! ignoring");
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        switch (n) {
            case -1: {
                this.onPositive();
                break;
            }
            case -2: {
                this.onNegative();
                break;
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent intent = this.getIntent();
        if (!intent.getAction().equals("android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST")) {
            Log.e("BluetoothPermissionActivity", "Error: this activity may be started only with intent ACTION_CONNECTION_ACCESS_REQUEST");
            this.finish();
            return;
        }
        this.mDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        this.mReturnPackage = intent.getStringExtra("android.bluetooth.device.extra.PACKAGE_NAME");
        this.mReturnClass = intent.getStringExtra("android.bluetooth.device.extra.CLASS_NAME");
        this.mRequestType = intent.getIntExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", 2);
        final StringBuilder sb = new StringBuilder();
        sb.append("onCreate() Request type: ");
        sb.append(this.mRequestType);
        Log.i("BluetoothPermissionActivity", sb.toString());
        if (this.mRequestType == 1) {
            this.showDialog(this.getString(2131886725), this.mRequestType);
        }
        else if (this.mRequestType == 2) {
            this.showDialog(this.getString(2131886840), this.mRequestType);
        }
        else if (this.mRequestType == 3) {
            this.showDialog(this.getString(2131886798), this.mRequestType);
        }
        else {
            if (this.mRequestType != 4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Error: bad request type: ");
                sb2.append(this.mRequestType);
                Log.e("BluetoothPermissionActivity", sb2.toString());
                this.finish();
                return;
            }
            this.showDialog(this.getString(2131886876), this.mRequestType);
        }
        this.registerReceiver(this.mReceiver, new IntentFilter("android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL"));
        this.mReceiverRegistered = true;
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (this.mReceiverRegistered) {
            this.unregisterReceiver(this.mReceiver);
            this.mReceiverRegistered = false;
        }
    }
    
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return true;
    }
}
