package com.android.settings.bluetooth;

import android.os.Bundle;
import android.content.IntentFilter;
import com.android.settingslib.bluetooth.BluetoothDiscoverableTimeoutReceiver;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface;
import android.content.BroadcastReceiver;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import android.app.Activity;

public class RequestPermissionActivity extends Activity implements DialogInterface$OnClickListener
{
    private CharSequence mAppLabel;
    private AlertDialog mDialog;
    private LocalBluetoothAdapter mLocalAdapter;
    private BroadcastReceiver mReceiver;
    private int mRequest;
    private int mTimeout;
    
    public RequestPermissionActivity() {
        this.mTimeout = 120;
    }
    
    private void cancelAndFinish() {
        this.setResult(0);
        this.finish();
    }
    
    private void createDialog() {
        if (this.getResources().getBoolean(2131034115)) {
            this.onClick(null, -1);
            return;
        }
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this);
        if (this.mReceiver != null) {
            switch (this.mRequest) {
                default: {
                    alertDialog$Builder.setMessage((CharSequence)this.getString(2131886911));
                    break;
                }
                case 1:
                case 2: {
                    alertDialog$Builder.setMessage((CharSequence)this.getString(2131886912));
                    break;
                }
            }
            alertDialog$Builder.setCancelable(false);
        }
        else {
            if (this.mTimeout == 0) {
                String message;
                if (this.mAppLabel != null) {
                    message = this.getString(2131886702, new Object[] { this.mAppLabel });
                }
                else {
                    message = this.getString(2131886703);
                }
                alertDialog$Builder.setMessage((CharSequence)message);
            }
            else {
                String message2;
                if (this.mAppLabel != null) {
                    message2 = this.getString(2131886694, new Object[] { this.mAppLabel, this.mTimeout });
                }
                else {
                    message2 = this.getString(2131886695, new Object[] { this.mTimeout });
                }
                alertDialog$Builder.setMessage((CharSequence)message2);
            }
            alertDialog$Builder.setPositiveButton((CharSequence)this.getString(2131886285), (DialogInterface$OnClickListener)this);
            alertDialog$Builder.setNegativeButton((CharSequence)this.getString(2131887370), (DialogInterface$OnClickListener)this);
        }
        (this.mDialog = alertDialog$Builder.create()).show();
    }
    
    private boolean parseIntent() {
        final Intent intent = this.getIntent();
        if (intent == null) {
            return true;
        }
        if (intent.getAction().equals("android.bluetooth.adapter.action.REQUEST_ENABLE")) {
            this.mRequest = 1;
        }
        else if (intent.getAction().equals("android.bluetooth.adapter.action.REQUEST_DISABLE")) {
            this.mRequest = 3;
        }
        else {
            if (!intent.getAction().equals("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE")) {
                Log.e("RequestPermissionActivity", "Error: this activity may be started only with intent android.bluetooth.adapter.action.REQUEST_ENABLE or android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
                this.setResult(0);
                return true;
            }
            this.mRequest = 2;
            this.mTimeout = intent.getIntExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 120);
            final StringBuilder sb = new StringBuilder();
            sb.append("Setting Bluetooth Discoverable Timeout = ");
            sb.append(this.mTimeout);
            Log.d("RequestPermissionActivity", sb.toString());
            if (this.mTimeout < 1 || this.mTimeout > 3600) {
                this.mTimeout = 120;
            }
        }
        final LocalBluetoothManager localBtManager = Utils.getLocalBtManager((Context)this);
        if (localBtManager == null) {
            Log.e("RequestPermissionActivity", "Error: there's a problem starting Bluetooth");
            this.setResult(0);
            return true;
        }
        String s;
        if (TextUtils.isEmpty((CharSequence)(s = this.getCallingPackage()))) {
            s = this.getIntent().getStringExtra("android.intent.extra.PACKAGE_NAME");
        }
        if (!TextUtils.isEmpty((CharSequence)s)) {
            try {
                this.mAppLabel = this.getPackageManager().getApplicationInfo(s, 0).loadSafeLabel(this.getPackageManager());
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Couldn't find app with package name ");
                sb2.append(s);
                Log.e("RequestPermissionActivity", sb2.toString());
                this.setResult(0);
                return true;
            }
        }
        this.mLocalAdapter = localBtManager.getBluetoothAdapter();
        return false;
    }
    
    private void proceedAndFinish() {
        int mTimeout;
        if (this.mRequest != 1 && this.mRequest != 3) {
            if (this.mLocalAdapter.setScanMode(23, this.mTimeout)) {
                final long n = System.currentTimeMillis() + this.mTimeout * 1000L;
                LocalBluetoothPreferences.persistDiscoverableEndTimestamp((Context)this, n);
                if (this.mTimeout > 0) {
                    BluetoothDiscoverableTimeoutReceiver.setDiscoverableAlarm((Context)this, n);
                }
                if ((mTimeout = this.mTimeout) < 1) {
                    mTimeout = 1;
                }
            }
            else {
                mTimeout = 0;
            }
        }
        else {
            mTimeout = -1;
        }
        if (this.mDialog != null) {
            this.mDialog.dismiss();
        }
        this.setResult(mTimeout);
        this.finish();
    }
    
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n2 != -1) {
            this.cancelAndFinish();
            return;
        }
        switch (this.mRequest) {
            default: {
                this.cancelAndFinish();
                break;
            }
            case 3: {
                if (this.mLocalAdapter.getBluetoothState() == 10) {
                    this.proceedAndFinish();
                    break;
                }
                this.registerReceiver(this.mReceiver = new StateChangeReceiver(), new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
                this.createDialog();
                break;
            }
            case 1:
            case 2: {
                if (this.mLocalAdapter.getBluetoothState() == 12) {
                    this.proceedAndFinish();
                    break;
                }
                this.registerReceiver(this.mReceiver = new StateChangeReceiver(), new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
                this.createDialog();
                break;
            }
        }
    }
    
    public void onBackPressed() {
        this.setResult(0);
        super.onBackPressed();
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        switch (n) {
            case -1: {
                this.proceedAndFinish();
                break;
            }
            case -2: {
                this.setResult(0);
                this.finish();
                break;
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setResult(0);
        if (this.parseIntent()) {
            this.finish();
            return;
        }
        final int state = this.mLocalAdapter.getState();
        if (this.mRequest == 3) {
            switch (state) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown adapter state: ");
                    sb.append(state);
                    Log.e("RequestPermissionActivity", sb.toString());
                    this.cancelAndFinish();
                    break;
                }
                case 11:
                case 12: {
                    final Intent intent = new Intent((Context)this, (Class)RequestPermissionHelperActivity.class);
                    intent.putExtra("com.android.settings.bluetooth.extra.APP_LABEL", this.mAppLabel);
                    intent.setAction("com.android.settings.bluetooth.ACTION_INTERNAL_REQUEST_BT_OFF");
                    this.startActivityForResult(intent, 0);
                    break;
                }
                case 10:
                case 13: {
                    this.proceedAndFinish();
                    break;
                }
            }
        }
        else {
            switch (state) {
                default: {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unknown adapter state: ");
                    sb2.append(state);
                    Log.e("RequestPermissionActivity", sb2.toString());
                    this.cancelAndFinish();
                    break;
                }
                case 12: {
                    if (this.mRequest == 1) {
                        this.proceedAndFinish();
                        break;
                    }
                    this.createDialog();
                    break;
                }
                case 10:
                case 11:
                case 13: {
                    final Intent intent2 = new Intent((Context)this, (Class)RequestPermissionHelperActivity.class);
                    intent2.setAction("com.android.settings.bluetooth.ACTION_INTERNAL_REQUEST_BT_ON");
                    intent2.putExtra("com.android.settings.bluetooth.extra.APP_LABEL", this.mAppLabel);
                    if (this.mRequest == 2) {
                        intent2.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", this.mTimeout);
                    }
                    this.startActivityForResult(intent2, 0);
                    break;
                }
            }
        }
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (this.mReceiver != null) {
            this.unregisterReceiver(this.mReceiver);
            this.mReceiver = null;
        }
    }
    
    private final class StateChangeReceiver extends BroadcastReceiver
    {
        public StateChangeReceiver() {
            RequestPermissionActivity.this.getWindow().getDecorView().postDelayed((Runnable)new _$$Lambda$RequestPermissionActivity$StateChangeReceiver$q4ZilZjRzY7SLoogXiJIIa__yMA(this), 10000L);
        }
        
        public void onReceive(final Context context, final Intent intent) {
            if (intent == null) {
                return;
            }
            final int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
            switch (RequestPermissionActivity.this.mRequest) {
                case 3: {
                    if (intExtra == 10) {
                        RequestPermissionActivity.this.proceedAndFinish();
                        break;
                    }
                    break;
                }
                case 1:
                case 2: {
                    if (intExtra == 12) {
                        RequestPermissionActivity.this.proceedAndFinish();
                        break;
                    }
                    break;
                }
            }
        }
    }
}
