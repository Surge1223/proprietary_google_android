package com.android.settings.bluetooth;

import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Context;

public class RestrictionUtils
{
    public RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced(final Context context, final String s) {
        return RestrictedLockUtils.checkIfRestrictionEnforced(context, s, UserHandle.myUserId());
    }
}
