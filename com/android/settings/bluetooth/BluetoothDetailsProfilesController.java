package com.android.settings.bluetooth;

import java.util.Iterator;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.bluetooth.PanProfile;
import com.android.settingslib.bluetooth.A2dpProfile;
import java.util.List;
import com.android.settingslib.bluetooth.MapProfile;
import com.android.settingslib.bluetooth.PbapServerProfile;
import android.bluetooth.BluetoothDevice;
import android.support.v14.preference.SwitchPreference;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import android.support.v7.preference.PreferenceCategory;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v7.preference.Preference;

public class BluetoothDetailsProfilesController extends BluetoothDetailsController implements OnPreferenceClickListener
{
    static final String HIGH_QUALITY_AUDIO_PREF_TAG = "A2dpProfileHighQualityAudio";
    private CachedBluetoothDevice mCachedDevice;
    private LocalBluetoothManager mManager;
    private LocalBluetoothProfileManager mProfileManager;
    private PreferenceCategory mProfilesContainer;
    
    public BluetoothDetailsProfilesController(final Context context, final PreferenceFragment preferenceFragment, final LocalBluetoothManager mManager, final CachedBluetoothDevice mCachedDevice, final Lifecycle lifecycle) {
        super(context, preferenceFragment, mCachedDevice, lifecycle);
        this.mManager = mManager;
        this.mProfileManager = this.mManager.getProfileManager();
        this.mCachedDevice = mCachedDevice;
        lifecycle.addObserver(this);
    }
    
    private SwitchPreference createProfilePreference(final Context context, final LocalBluetoothProfile localBluetoothProfile) {
        final SwitchPreference switchPreference = new SwitchPreference(context);
        switchPreference.setKey(localBluetoothProfile.toString());
        switchPreference.setTitle(localBluetoothProfile.getNameResource(this.mCachedDevice.getDevice()));
        switchPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        return switchPreference;
    }
    
    private void disableProfile(final LocalBluetoothProfile localBluetoothProfile, final BluetoothDevice bluetoothDevice, final SwitchPreference switchPreference) {
        if (localBluetoothProfile instanceof PbapServerProfile) {
            this.mCachedDevice.setPhonebookPermissionChoice(2);
            return;
        }
        this.mCachedDevice.disconnect(localBluetoothProfile);
        localBluetoothProfile.setPreferred(bluetoothDevice, false);
        if (localBluetoothProfile instanceof MapProfile) {
            this.mCachedDevice.setMessagePermissionChoice(2);
        }
    }
    
    private void enableProfile(final LocalBluetoothProfile localBluetoothProfile, final BluetoothDevice bluetoothDevice, final SwitchPreference switchPreference) {
        if (localBluetoothProfile instanceof PbapServerProfile) {
            this.mCachedDevice.setPhonebookPermissionChoice(1);
            return;
        }
        if (localBluetoothProfile instanceof MapProfile) {
            this.mCachedDevice.setMessagePermissionChoice(1);
        }
        localBluetoothProfile.setPreferred(bluetoothDevice, true);
        this.mCachedDevice.connectProfile(localBluetoothProfile);
    }
    
    private List<LocalBluetoothProfile> getProfiles() {
        final List<LocalBluetoothProfile> connectableProfiles = this.mCachedDevice.getConnectableProfiles();
        if (this.mCachedDevice.getPhonebookPermissionChoice() != 0) {
            connectableProfiles.add(this.mManager.getProfileManager().getPbapProfile());
        }
        final MapProfile mapProfile = this.mManager.getProfileManager().getMapProfile();
        if (this.mCachedDevice.getMessagePermissionChoice() != 0) {
            connectableProfiles.add(mapProfile);
        }
        return connectableProfiles;
    }
    
    private void maybeAddHighQualityAudioPref(final LocalBluetoothProfile localBluetoothProfile) {
        if (!(localBluetoothProfile instanceof A2dpProfile)) {
            return;
        }
        final BluetoothDevice device = this.mCachedDevice.getDevice();
        final A2dpProfile a2dpProfile = (A2dpProfile)localBluetoothProfile;
        if (a2dpProfile.supportsHighQualityAudio(device)) {
            final SwitchPreference switchPreference = new SwitchPreference(this.mProfilesContainer.getContext());
            switchPreference.setKey("A2dpProfileHighQualityAudio");
            switchPreference.setVisible(false);
            switchPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$BluetoothDetailsProfilesController$pv2kZi3KDLDrPBqbb1ECR74MeRo(this, a2dpProfile));
            this.mProfilesContainer.addPreference(switchPreference);
        }
    }
    
    private void refreshProfilePreference(SwitchPreference switchPreference, final LocalBluetoothProfile localBluetoothProfile) {
        final BluetoothDevice device = this.mCachedDevice.getDevice();
        switchPreference.setEnabled(this.mCachedDevice.isBusy() ^ true);
        if (localBluetoothProfile instanceof MapProfile) {
            switchPreference.setChecked(this.mCachedDevice.getMessagePermissionChoice() == 1);
        }
        else if (localBluetoothProfile instanceof PbapServerProfile) {
            switchPreference.setChecked(this.mCachedDevice.getPhonebookPermissionChoice() == 1);
        }
        else if (localBluetoothProfile instanceof PanProfile) {
            switchPreference.setChecked(localBluetoothProfile.getConnectionStatus(device) == 2);
        }
        else {
            switchPreference.setChecked(localBluetoothProfile.isPreferred(device));
        }
        if (localBluetoothProfile instanceof A2dpProfile) {
            final A2dpProfile a2dpProfile = (A2dpProfile)localBluetoothProfile;
            switchPreference = (SwitchPreference)this.mProfilesContainer.findPreference("A2dpProfileHighQualityAudio");
            if (switchPreference != null) {
                if (a2dpProfile.isPreferred(device) && a2dpProfile.supportsHighQualityAudio(device)) {
                    switchPreference.setVisible(true);
                    switchPreference.setTitle(a2dpProfile.getHighQualityAudioOptionLabel(device));
                    switchPreference.setChecked(a2dpProfile.isHighQualityAudioEnabled(device));
                    switchPreference.setEnabled(true ^ this.mCachedDevice.isBusy());
                }
                else {
                    switchPreference.setVisible(false);
                }
            }
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_profiles";
    }
    
    @Override
    protected void init(final PreferenceScreen preferenceScreen) {
        this.mProfilesContainer = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey());
        this.refresh();
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        LocalBluetoothProfile localBluetoothProfile;
        if ((localBluetoothProfile = this.mProfileManager.getProfileByName(preference.getKey())) == null) {
            localBluetoothProfile = this.mManager.getProfileManager().getPbapProfile();
            if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)((PbapServerProfile)localBluetoothProfile).toString())) {
                return false;
            }
        }
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        final BluetoothDevice device = this.mCachedDevice.getDevice();
        if (switchPreference.isChecked()) {
            this.enableProfile(localBluetoothProfile, device, switchPreference);
        }
        else {
            this.disableProfile(localBluetoothProfile, device, switchPreference);
        }
        this.refreshProfilePreference(switchPreference, localBluetoothProfile);
        return true;
    }
    
    @Override
    protected void refresh() {
        for (final LocalBluetoothProfile localBluetoothProfile : this.getProfiles()) {
            SwitchPreference profilePreference;
            if ((profilePreference = (SwitchPreference)this.mProfilesContainer.findPreference(localBluetoothProfile.toString())) == null) {
                profilePreference = this.createProfilePreference(this.mProfilesContainer.getContext(), localBluetoothProfile);
                this.mProfilesContainer.addPreference(profilePreference);
                this.maybeAddHighQualityAudioPref(localBluetoothProfile);
            }
            this.refreshProfilePreference(profilePreference, localBluetoothProfile);
        }
        final Iterator<LocalBluetoothProfile> iterator2 = this.mCachedDevice.getRemovedProfiles().iterator();
        while (iterator2.hasNext()) {
            final SwitchPreference switchPreference = (SwitchPreference)this.mProfilesContainer.findPreference(iterator2.next().toString());
            if (switchPreference != null) {
                this.mProfilesContainer.removePreference(switchPreference);
            }
        }
    }
}
