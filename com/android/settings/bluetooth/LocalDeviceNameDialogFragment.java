package com.android.settings.bluetooth;

import android.content.IntentFilter;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;

public class LocalDeviceNameDialogFragment extends BluetoothNameDialogFragment
{
    private LocalBluetoothAdapter mLocalAdapter;
    private final BroadcastReceiver mReceiver;
    
    public LocalDeviceNameDialogFragment() {
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.bluetooth.adapter.action.LOCAL_NAME_CHANGED".equals(action) || ("android.bluetooth.adapter.action.STATE_CHANGED".equals(action) && intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE) == 12)) {
                    LocalDeviceNameDialogFragment.this.updateDeviceName();
                }
            }
        };
    }
    
    public static LocalDeviceNameDialogFragment newInstance() {
        return new LocalDeviceNameDialogFragment();
    }
    
    @Override
    protected String getDeviceName() {
        if (this.mLocalAdapter != null && this.mLocalAdapter.isEnabled()) {
            return this.mLocalAdapter.getName();
        }
        return null;
    }
    
    @Override
    protected int getDialogTitle() {
        return 2131886872;
    }
    
    public int getMetricsCategory() {
        return 538;
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mLocalAdapter = Utils.getLocalBtManager((Context)this.getActivity()).getBluetoothAdapter();
    }
    
    public void onPause() {
        super.onPause();
        this.getActivity().unregisterReceiver(this.mReceiver);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.LOCAL_NAME_CHANGED");
        this.getActivity().registerReceiver(this.mReceiver, intentFilter);
    }
    
    @Override
    protected void setDeviceName(final String name) {
        this.mLocalAdapter.setName(name);
    }
}
