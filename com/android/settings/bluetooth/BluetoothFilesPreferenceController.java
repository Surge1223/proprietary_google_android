package com.android.settings.bluetooth;

import android.content.Intent;
import android.util.Pair;
import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.BasePreferenceController;

public class BluetoothFilesPreferenceController extends BasePreferenceController implements PreferenceControllerMixin
{
    static final String ACTION_OPEN_FILES = "com.android.bluetooth.action.TransferHistory";
    static final String EXTRA_DIRECTION = "direction";
    static final String EXTRA_SHOW_ALL_FILES = "android.btopp.intent.extra.SHOW_ALL";
    public static final String KEY_RECEIVED_FILES = "bt_received_files";
    private static final String TAG = "BluetoothFilesPrefCtrl";
    private MetricsFeatureProvider mMetricsFeatureProvider;
    
    public BluetoothFilesPreferenceController(final Context context) {
        super(context, "bt_received_files");
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth")) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public String getPreferenceKey() {
        return "bt_received_files";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("bt_received_files".equals(preference.getKey())) {
            this.mMetricsFeatureProvider.action(this.mContext, 162, (Pair<Integer, Object>[])new Pair[0]);
            final Intent intent = new Intent("com.android.bluetooth.action.TransferHistory");
            intent.setFlags(335544320);
            intent.putExtra("direction", 1);
            intent.putExtra("android.btopp.intent.extra.SHOW_ALL", true);
            this.mContext.startActivity(intent);
            return true;
        }
        return false;
    }
}
