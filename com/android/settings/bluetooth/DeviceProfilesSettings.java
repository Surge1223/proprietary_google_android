package com.android.settings.bluetooth;

import android.app.AlertDialog$Builder;
import android.widget.TextView$BufferType;
import android.view.LayoutInflater;
import android.app.Dialog;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;
import android.os.Bundle;
import android.widget.EditText;
import com.android.settingslib.bluetooth.PanProfile;
import android.text.Html;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.bluetooth.BluetoothDevice;
import java.util.Iterator;
import android.util.Log;
import android.content.Context;
import com.android.settingslib.bluetooth.A2dpProfile;
import com.android.settingslib.bluetooth.MapProfile;
import com.android.settingslib.bluetooth.PbapServerProfile;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import android.widget.CheckBox;
import android.view.View;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import android.widget.TextView;
import android.view.ViewGroup;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.app.AlertDialog;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.view.View.OnClickListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public final class DeviceProfilesSettings extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, View.OnClickListener, Callback
{
    static final String HIGH_QUALITY_AUDIO_PREF_TAG = "A2dpProfileHighQualityAudio";
    private CachedBluetoothDevice mCachedDevice;
    private AlertDialog mDisconnectDialog;
    private LocalBluetoothManager mManager;
    private ViewGroup mProfileContainer;
    private boolean mProfileGroupIsRemoved;
    private TextView mProfileLabel;
    private LocalBluetoothProfileManager mProfileManager;
    private View mRootView;
    
    private void addPreferencesForProfiles() {
        this.mProfileContainer.removeAllViews();
        for (final LocalBluetoothProfile localBluetoothProfile : this.mCachedDevice.getConnectableProfiles()) {
            final CheckBox profilePreference = this.createProfilePreference(localBluetoothProfile);
            if (!(localBluetoothProfile instanceof PbapServerProfile) && !(localBluetoothProfile instanceof MapProfile)) {
                this.mProfileContainer.addView((View)profilePreference);
            }
            if (localBluetoothProfile instanceof A2dpProfile) {
                final BluetoothDevice device = this.mCachedDevice.getDevice();
                final A2dpProfile a2dpProfile = (A2dpProfile)localBluetoothProfile;
                if (a2dpProfile.supportsHighQualityAudio(device)) {
                    final CheckBox checkBox = new CheckBox((Context)this.getActivity());
                    checkBox.setTag((Object)"A2dpProfileHighQualityAudio");
                    checkBox.setOnClickListener((View.OnClickListener)new _$$Lambda$DeviceProfilesSettings$qBNrFA8_Smm3qyHXDkezO_CS7tQ(a2dpProfile, device, checkBox));
                    checkBox.setVisibility(8);
                    this.mProfileContainer.addView((View)checkBox);
                }
                this.refreshProfilePreference(profilePreference, localBluetoothProfile);
            }
        }
        final int phonebookPermissionChoice = this.mCachedDevice.getPhonebookPermissionChoice();
        final StringBuilder sb = new StringBuilder();
        sb.append("addPreferencesForProfiles: pbapPermission = ");
        sb.append(phonebookPermissionChoice);
        Log.d("DeviceProfilesSettings", sb.toString());
        if (phonebookPermissionChoice != 0) {
            this.mProfileContainer.addView((View)this.createProfilePreference(this.mManager.getProfileManager().getPbapProfile()));
        }
        final MapProfile mapProfile = this.mManager.getProfileManager().getMapProfile();
        final int messagePermissionChoice = this.mCachedDevice.getMessagePermissionChoice();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("addPreferencesForProfiles: mapPermission = ");
        sb2.append(messagePermissionChoice);
        Log.d("DeviceProfilesSettings", sb2.toString());
        if (messagePermissionChoice != 0) {
            this.mProfileContainer.addView((View)this.createProfilePreference(mapProfile));
        }
        this.showOrHideProfileGroup();
    }
    
    private void askDisconnect(final Context context, final LocalBluetoothProfile localBluetoothProfile) {
        final CachedBluetoothDevice mCachedDevice = this.mCachedDevice;
        String s;
        if (TextUtils.isEmpty((CharSequence)(s = mCachedDevice.getName()))) {
            s = context.getString(2131886726);
        }
        this.mDisconnectDialog = Utils.showDisconnectDialog(context, this.mDisconnectDialog, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (n == -1) {
                    mCachedDevice.disconnect(localBluetoothProfile);
                    localBluetoothProfile.setPreferred(mCachedDevice.getDevice(), false);
                    if (localBluetoothProfile instanceof MapProfile) {
                        mCachedDevice.setMessagePermissionChoice(2);
                    }
                    if (localBluetoothProfile instanceof PbapServerProfile) {
                        mCachedDevice.setPhonebookPermissionChoice(2);
                    }
                }
                DeviceProfilesSettings.this.refreshProfilePreference(DeviceProfilesSettings.this.findProfile(localBluetoothProfile.toString()), localBluetoothProfile);
            }
        }, context.getString(2131886752), (CharSequence)Html.fromHtml(context.getString(2131886751, new Object[] { context.getString(localBluetoothProfile.getNameResource(mCachedDevice.getDevice())), s })));
    }
    
    private CheckBox createProfilePreference(final LocalBluetoothProfile localBluetoothProfile) {
        final CheckBox checkBox = new CheckBox((Context)this.getActivity());
        checkBox.setTag((Object)localBluetoothProfile.toString());
        checkBox.setText(localBluetoothProfile.getNameResource(this.mCachedDevice.getDevice()));
        checkBox.setOnClickListener((View.OnClickListener)this);
        this.refreshProfilePreference(checkBox, localBluetoothProfile);
        return checkBox;
    }
    
    private CheckBox findProfile(final String s) {
        return (CheckBox)this.mProfileContainer.findViewWithTag((Object)s);
    }
    
    private LocalBluetoothProfile getProfileOf(final View view) {
        if (!(view instanceof CheckBox)) {
            return null;
        }
        final String s = (String)view.getTag();
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            return this.mProfileManager.getProfileByName(s);
        }
        catch (IllegalArgumentException ex) {
            return null;
        }
    }
    
    private void onProfileClicked(final LocalBluetoothProfile localBluetoothProfile, final CheckBox checkBox) {
        final BluetoothDevice device = this.mCachedDevice.getDevice();
        if (!checkBox.isChecked()) {
            checkBox.setChecked(true);
            this.askDisconnect(this.mManager.getForegroundActivity(), localBluetoothProfile);
        }
        else {
            if (localBluetoothProfile instanceof MapProfile) {
                this.mCachedDevice.setMessagePermissionChoice(1);
            }
            if (localBluetoothProfile instanceof PbapServerProfile) {
                this.mCachedDevice.setPhonebookPermissionChoice(1);
                this.refreshProfilePreference(checkBox, localBluetoothProfile);
                return;
            }
            if (localBluetoothProfile.isPreferred(device)) {
                if (localBluetoothProfile instanceof PanProfile) {
                    this.mCachedDevice.connectProfile(localBluetoothProfile);
                }
                else {
                    localBluetoothProfile.setPreferred(device, false);
                }
            }
            else {
                localBluetoothProfile.setPreferred(device, true);
                this.mCachedDevice.connectProfile(localBluetoothProfile);
            }
            this.refreshProfilePreference(checkBox, localBluetoothProfile);
        }
    }
    
    private void refresh() {
        final EditText editTextCursorPosition = (EditText)this.mRootView.findViewById(2131362382);
        if (editTextCursorPosition != null) {
            editTextCursorPosition.setText((CharSequence)this.mCachedDevice.getName());
            com.android.settings.Utils.setEditTextCursorPosition(editTextCursorPosition);
        }
        this.refreshProfiles();
    }
    
    private void refreshProfilePreference(final CheckBox checkBox, final LocalBluetoothProfile localBluetoothProfile) {
        final BluetoothDevice device = this.mCachedDevice.getDevice();
        checkBox.setEnabled(this.mCachedDevice.isBusy() ^ true);
        if (localBluetoothProfile instanceof MapProfile) {
            checkBox.setChecked(this.mCachedDevice.getMessagePermissionChoice() == 1);
        }
        else if (localBluetoothProfile instanceof PbapServerProfile) {
            checkBox.setChecked(this.mCachedDevice.getPhonebookPermissionChoice() == 1);
        }
        else if (localBluetoothProfile instanceof PanProfile) {
            checkBox.setChecked(localBluetoothProfile.getConnectionStatus(device) == 2);
        }
        else {
            checkBox.setChecked(localBluetoothProfile.isPreferred(device));
        }
        if (localBluetoothProfile instanceof A2dpProfile) {
            final A2dpProfile a2dpProfile = (A2dpProfile)localBluetoothProfile;
            final View viewWithTag = this.mProfileContainer.findViewWithTag((Object)"A2dpProfileHighQualityAudio");
            if (viewWithTag instanceof CheckBox) {
                final CheckBox checkBox2 = (CheckBox)viewWithTag;
                checkBox2.setText((CharSequence)a2dpProfile.getHighQualityAudioOptionLabel(device));
                checkBox2.setChecked(a2dpProfile.isHighQualityAudioEnabled(device));
                if (a2dpProfile.isPreferred(device)) {
                    viewWithTag.setVisibility(0);
                    viewWithTag.setEnabled(true ^ this.mCachedDevice.isBusy());
                }
                else {
                    viewWithTag.setVisibility(8);
                }
            }
        }
    }
    
    private void refreshProfiles() {
        for (final LocalBluetoothProfile localBluetoothProfile : this.mCachedDevice.getConnectableProfiles()) {
            final CheckBox profile = this.findProfile(localBluetoothProfile.toString());
            if (profile == null) {
                this.mProfileContainer.addView((View)this.createProfilePreference(localBluetoothProfile));
            }
            else {
                this.refreshProfilePreference(profile, localBluetoothProfile);
            }
        }
        for (final LocalBluetoothProfile localBluetoothProfile2 : this.mCachedDevice.getRemovedProfiles()) {
            final CheckBox profile2 = this.findProfile(localBluetoothProfile2.toString());
            if (profile2 != null) {
                if (localBluetoothProfile2 instanceof PbapServerProfile) {
                    final int phonebookPermissionChoice = this.mCachedDevice.getPhonebookPermissionChoice();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("refreshProfiles: pbapPermission = ");
                    sb.append(phonebookPermissionChoice);
                    Log.d("DeviceProfilesSettings", sb.toString());
                    if (phonebookPermissionChoice != 0) {
                        continue;
                    }
                }
                if (localBluetoothProfile2 instanceof MapProfile) {
                    final int messagePermissionChoice = this.mCachedDevice.getMessagePermissionChoice();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("refreshProfiles: mapPermission = ");
                    sb2.append(messagePermissionChoice);
                    Log.d("DeviceProfilesSettings", sb2.toString());
                    if (messagePermissionChoice != 0) {
                        continue;
                    }
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Removing ");
                sb3.append(localBluetoothProfile2.toString());
                sb3.append(" from profile list");
                Log.d("DeviceProfilesSettings", sb3.toString());
                this.mProfileContainer.removeView((View)profile2);
            }
        }
        this.showOrHideProfileGroup();
    }
    
    private void showOrHideProfileGroup() {
        final int childCount = this.mProfileContainer.getChildCount();
        if (!this.mProfileGroupIsRemoved && childCount == 0) {
            this.mProfileContainer.setVisibility(8);
            this.mProfileLabel.setVisibility(8);
            this.mProfileGroupIsRemoved = true;
        }
        else if (this.mProfileGroupIsRemoved && childCount != 0) {
            this.mProfileContainer.setVisibility(0);
            this.mProfileLabel.setVisibility(0);
            this.mProfileGroupIsRemoved = false;
        }
    }
    
    public int getMetricsCategory() {
        return 539;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n != -3) {
            if (n == -1) {
                this.mCachedDevice.setName(((EditText)this.mRootView.findViewById(2131362382)).getText().toString());
            }
        }
        else {
            this.mCachedDevice.unpair();
        }
    }
    
    public void onClick(final View view) {
        if (view instanceof CheckBox) {
            this.onProfileClicked(this.getProfileOf(view), (CheckBox)view);
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mManager = Utils.getLocalBtManager((Context)this.getActivity());
        final CachedBluetoothDeviceManager cachedDeviceManager = this.mManager.getCachedDeviceManager();
        final BluetoothDevice remoteDevice = this.mManager.getBluetoothAdapter().getRemoteDevice(this.getArguments().getString("device_address"));
        this.mCachedDevice = cachedDeviceManager.findDevice(remoteDevice);
        if (this.mCachedDevice == null) {
            this.mCachedDevice = cachedDeviceManager.addDevice(this.mManager.getBluetoothAdapter(), this.mManager.getProfileManager(), remoteDevice);
        }
        this.mProfileManager = this.mManager.getProfileManager();
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        this.mRootView = LayoutInflater.from(this.getContext()).inflate(2131558539, (ViewGroup)null);
        this.mProfileContainer = (ViewGroup)this.mRootView.findViewById(2131362482);
        this.mProfileLabel = (TextView)this.mRootView.findViewById(2131362481);
        ((EditText)this.mRootView.findViewById(2131362382)).setText((CharSequence)this.mCachedDevice.getName(), TextView$BufferType.EDITABLE);
        return (Dialog)new AlertDialog$Builder(this.getContext()).setView(this.mRootView).setNeutralButton(2131887697, (DialogInterface$OnClickListener)this).setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)this).setTitle(2131886847).create();
    }
    
    public void onDestroy() {
        super.onDestroy();
        if (this.mDisconnectDialog != null) {
            this.mDisconnectDialog.dismiss();
            this.mDisconnectDialog = null;
        }
        if (this.mCachedDevice != null) {
            this.mCachedDevice.unregisterCallback((CachedBluetoothDevice.Callback)this);
        }
    }
    
    public void onDeviceAttributesChanged() {
        this.refresh();
    }
    
    public void onPause() {
        super.onPause();
        if (this.mCachedDevice != null) {
            this.mCachedDevice.unregisterCallback((CachedBluetoothDevice.Callback)this);
        }
        this.mManager.setForegroundActivity(null);
    }
    
    public void onResume() {
        super.onResume();
        this.mManager.setForegroundActivity((Context)this.getActivity());
        if (this.mCachedDevice != null) {
            this.mCachedDevice.registerCallback((CachedBluetoothDevice.Callback)this);
            if (this.mCachedDevice.getBondState() == 10) {
                this.dismiss();
                return;
            }
            this.addPreferencesForProfiles();
            this.refresh();
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
}
