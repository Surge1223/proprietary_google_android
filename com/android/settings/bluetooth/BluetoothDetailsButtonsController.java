package com.android.settings.bluetooth;

import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceScreen;
import android.view.View;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import com.android.settings.widget.ActionButtonPreference;

public class BluetoothDetailsButtonsController extends BluetoothDetailsController
{
    private ActionButtonPreference mActionButtons;
    private boolean mConnectButtonInitialized;
    private boolean mIsConnected;
    
    public BluetoothDetailsButtonsController(final Context context, final PreferenceFragment preferenceFragment, final CachedBluetoothDevice cachedBluetoothDevice, final Lifecycle lifecycle) {
        super(context, preferenceFragment, cachedBluetoothDevice, lifecycle);
        this.mIsConnected = cachedBluetoothDevice.isConnected();
    }
    
    private void onForgetButtonPressed() {
        ForgetDeviceDialogFragment.newInstance(this.mCachedDevice.getAddress()).show(this.mFragment.getFragmentManager(), "ForgetBluetoothDevice");
    }
    
    @Override
    public String getPreferenceKey() {
        return "action_buttons";
    }
    
    @Override
    protected void init(final PreferenceScreen preferenceScreen) {
        this.mActionButtons = ((ActionButtonPreference)preferenceScreen.findPreference(this.getPreferenceKey())).setButton1Text(2131887697).setButton1OnClickListener((View.OnClickListener)new _$$Lambda$BluetoothDetailsButtonsController$10mSfoM1rAEvasn6gc_o1iWQgIA(this)).setButton1Positive(false).setButton1Enabled(true);
    }
    
    @Override
    protected void refresh() {
        this.mActionButtons.setButton2Enabled(this.mCachedDevice.isBusy() ^ true);
        final boolean mIsConnected = this.mIsConnected;
        this.mIsConnected = this.mCachedDevice.isConnected();
        if (this.mIsConnected) {
            if (!this.mConnectButtonInitialized || !mIsConnected) {
                this.mActionButtons.setButton2Text(2131886735).setButton2OnClickListener((View.OnClickListener)new _$$Lambda$BluetoothDetailsButtonsController$AbsgPn9bfqFfvfi3BgeGPbSW3X0(this)).setButton2Positive(false);
                this.mConnectButtonInitialized = true;
            }
        }
        else if (!this.mConnectButtonInitialized || mIsConnected) {
            this.mActionButtons.setButton2Text(2131886733).setButton2OnClickListener((View.OnClickListener)new _$$Lambda$BluetoothDetailsButtonsController$eZ36ezumIpXzpP7dOOnqn_gI5Uk(this)).setButton2Positive(true);
            this.mConnectButtonInitialized = true;
        }
    }
}
