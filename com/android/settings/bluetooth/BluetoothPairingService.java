package com.android.settings.bluetooth;

import android.content.res.Resources;
import android.content.IntentFilter;
import android.app.Notification$Action$Builder;
import android.text.TextUtils;
import android.app.PendingIntent;
import android.app.Notification$Builder;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.app.Service;

public final class BluetoothPairingService extends Service
{
    private final BroadcastReceiver mCancelReceiver;
    private BluetoothDevice mDevice;
    private boolean mRegistered;
    
    public BluetoothPairingService() {
        this.mRegistered = false;
        this.mCancelReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                    final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                    if (intExtra != 10 && intExtra != 12) {
                        return;
                    }
                }
                else if (action.equals("com.android.settings.bluetooth.ACTION_DISMISS_PAIRING")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Notification cancel ");
                    sb.append(BluetoothPairingService.this.mDevice.getAddress());
                    sb.append(" (");
                    sb.append(BluetoothPairingService.this.mDevice.getName());
                    sb.append(")");
                    Log.d("BluetoothPairingService", sb.toString());
                    BluetoothPairingService.this.mDevice.cancelPairingUserInput();
                }
                else {
                    final int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Dismiss pairing for ");
                    sb2.append(BluetoothPairingService.this.mDevice.getAddress());
                    sb2.append(" (");
                    sb2.append(BluetoothPairingService.this.mDevice.getName());
                    sb2.append("), BondState: ");
                    sb2.append(intExtra2);
                    Log.d("BluetoothPairingService", sb2.toString());
                }
                BluetoothPairingService.this.stopForeground(true);
                BluetoothPairingService.this.stopSelf();
            }
        };
    }
    
    public static Intent getPairingDialogIntent(final Context context, final Intent intent) {
        final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", Integer.MIN_VALUE);
        final Intent intent2 = new Intent();
        intent2.setClass(context, (Class)BluetoothPairingDialog.class);
        intent2.putExtra("android.bluetooth.device.extra.DEVICE", (Parcelable)bluetoothDevice);
        intent2.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", intExtra);
        if (intExtra == 2 || intExtra == 4 || intExtra == 5) {
            intent2.putExtra("android.bluetooth.device.extra.PAIRING_KEY", intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", Integer.MIN_VALUE));
        }
        intent2.setAction("android.bluetooth.device.action.PAIRING_REQUEST");
        intent2.setFlags(268435456);
        return intent2;
    }
    
    public IBinder onBind(final Intent intent) {
        return null;
    }
    
    public void onCreate() {
        ((NotificationManager)this.getSystemService("notification")).createNotificationChannel(new NotificationChannel("bluetooth_notification_channel", (CharSequence)this.getString(2131886686), 4));
    }
    
    public void onDestroy() {
        if (this.mRegistered) {
            this.unregisterReceiver(this.mCancelReceiver);
            this.mRegistered = false;
        }
        this.stopForeground(true);
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        if (intent == null) {
            Log.e("BluetoothPairingService", "Can't start: null intent!");
            this.stopSelf();
            return 2;
        }
        final Resources resources = this.getResources();
        final Notification$Builder setLocalOnly = new Notification$Builder((Context)this, "bluetooth_notification_channel").setSmallIcon(17301632).setTicker((CharSequence)resources.getString(2131886806)).setLocalOnly(true);
        final PendingIntent activity = PendingIntent.getActivity((Context)this, 0, getPairingDialogIntent((Context)this, intent), 1073741824);
        final PendingIntent broadcast = PendingIntent.getBroadcast((Context)this, 0, new Intent("com.android.settings.bluetooth.ACTION_DISMISS_PAIRING"), 1073741824);
        this.mDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        if (this.mDevice != null && this.mDevice.getBondState() != 11) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Device ");
            sb.append(this.mDevice);
            sb.append(" not bonding: ");
            sb.append(this.mDevice.getBondState());
            Log.w("BluetoothPairingService", sb.toString());
            this.stopSelf();
            return 2;
        }
        String stringExtra;
        if (TextUtils.isEmpty((CharSequence)(stringExtra = intent.getStringExtra("android.bluetooth.device.extra.NAME")))) {
            final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            String s;
            if (bluetoothDevice != null) {
                s = bluetoothDevice.getAliasName();
            }
            else {
                s = resources.getString(17039374);
            }
            stringExtra = s;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Show pairing notification for ");
        sb2.append(this.mDevice.getAddress());
        sb2.append(" (");
        sb2.append(stringExtra);
        sb2.append(")");
        Log.d("BluetoothPairingService", sb2.toString());
        setLocalOnly.setContentTitle((CharSequence)resources.getString(2131886807)).setContentText((CharSequence)resources.getString(2131886805, new Object[] { stringExtra })).setContentIntent(activity).setDefaults(1).setColor(this.getColor(17170774)).addAction(new Notification$Action$Builder(0, (CharSequence)resources.getString(2131886737), activity).build()).addAction(new Notification$Action$Builder(0, (CharSequence)resources.getString(17039360), broadcast).build());
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_CANCEL");
        intentFilter.addAction("com.android.settings.bluetooth.ACTION_DISMISS_PAIRING");
        this.registerReceiver(this.mCancelReceiver, intentFilter);
        this.mRegistered = true;
        this.startForeground(17301632, setLocalOnly.getNotification());
        return 3;
    }
}
