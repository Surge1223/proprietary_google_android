package com.android.settings.bluetooth;

import android.bluetooth.BluetoothDevice;
import com.android.settings.widget.GearPreference;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settings.dashboard.DashboardFragment;
import android.content.Context;
import android.media.AudioManager;
import android.support.v7.preference.Preference;

public final class _$$Lambda$ConnectedBluetoothDeviceUpdater$cbcA1LEPXmJVOc_WhespijdA8X8 implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return ConnectedBluetoothDeviceUpdater.lambda$addPreference$0(this.f$0, preference);
    }
}
