package com.android.settings.bluetooth;

import android.text.Spanned;
import android.text.InputFilter;

public class Utf8ByteLengthFilter implements InputFilter
{
    private final int mMaxBytes;
    
    Utf8ByteLengthFilter(final int mMaxBytes) {
        this.mMaxBytes = mMaxBytes;
    }
    
    public CharSequence filter(final CharSequence charSequence, final int n, final int n2, final Spanned spanned, int char1, int n3) {
        int n4 = 0;
        int n5 = n;
        while (true) {
            int n6 = 1;
            if (n5 >= n2) {
                break;
            }
            final char char2 = charSequence.charAt(n5);
            if (char2 >= '\u0080') {
                if (char2 < '\u0800') {
                    n6 = 2;
                }
                else {
                    n6 = 3;
                }
            }
            n4 += n6;
            ++n5;
        }
        final int length = spanned.length();
        int n7 = 0;
        for (int i = 0; i < length; ++i) {
            if (i < char1 || i >= n3) {
                final char char3 = spanned.charAt(i);
                int n8;
                if (char3 < '\u0080') {
                    n8 = 1;
                }
                else if (char3 < '\u0800') {
                    n8 = 2;
                }
                else {
                    n8 = 3;
                }
                n7 += n8;
            }
        }
        n3 = this.mMaxBytes - n7;
        if (n3 <= 0) {
            return "";
        }
        if (n3 >= n4) {
            return null;
        }
        for (int j = n; j < n2; ++j) {
            char1 = charSequence.charAt(j);
            if (char1 < 128) {
                char1 = 1;
            }
            else if (char1 < 2048) {
                char1 = 2;
            }
            else {
                char1 = 3;
            }
            n3 -= char1;
            if (n3 < 0) {
                return charSequence.subSequence(n, j);
            }
        }
        return null;
    }
}
