package com.android.settings.bluetooth;

import android.bluetooth.BluetoothDevice;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settings.dashboard.DashboardFragment;
import android.content.Context;
import android.support.v7.preference.Preference;

public class SavedBluetoothDeviceUpdater extends BluetoothDeviceUpdater implements OnPreferenceClickListener
{
    public SavedBluetoothDeviceUpdater(final Context context, final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback) {
        super(context, dashboardFragment, devicePreferenceCallback);
    }
    
    SavedBluetoothDeviceUpdater(final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback, final LocalBluetoothManager localBluetoothManager) {
        super(dashboardFragment, devicePreferenceCallback, localBluetoothManager);
    }
    
    @Override
    public boolean isFilterMatched(final CachedBluetoothDevice cachedBluetoothDevice) {
        final BluetoothDevice device = cachedBluetoothDevice.getDevice();
        return device.getBondState() == 12 && !device.isConnected();
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        ((BluetoothDevicePreference)preference).getBluetoothDevice().connect(true);
        return true;
    }
    
    @Override
    public void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
        if (n == 2) {
            this.removePreference(cachedBluetoothDevice);
        }
        else if (n == 0) {
            this.addPreference(cachedBluetoothDevice);
        }
    }
}
