package com.android.settings.bluetooth;

import android.app.Dialog;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.content.Context;
import android.text.Editable;
import android.os.Bundle;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;

public class RemoteDeviceNameDialogFragment extends BluetoothNameDialogFragment
{
    private CachedBluetoothDevice mDevice;
    
    public static RemoteDeviceNameDialogFragment newInstance(final CachedBluetoothDevice cachedBluetoothDevice) {
        final Bundle arguments = new Bundle(1);
        arguments.putString("cached_device", cachedBluetoothDevice.getDevice().getAddress());
        final RemoteDeviceNameDialogFragment remoteDeviceNameDialogFragment = new RemoteDeviceNameDialogFragment();
        remoteDeviceNameDialogFragment.setArguments(arguments);
        return remoteDeviceNameDialogFragment;
    }
    
    @VisibleForTesting
    CachedBluetoothDevice getDevice(final Context context) {
        final String string = this.getArguments().getString("cached_device");
        final LocalBluetoothManager localBtManager = Utils.getLocalBtManager(context);
        return localBtManager.getCachedDeviceManager().findDevice(localBtManager.getBluetoothAdapter().getRemoteDevice(string));
    }
    
    @Override
    protected String getDeviceName() {
        if (this.mDevice != null) {
            return this.mDevice.getName();
        }
        return null;
    }
    
    @Override
    protected int getDialogTitle() {
        return 2131886741;
    }
    
    public int getMetricsCategory() {
        return 1015;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mDevice = this.getDevice(context);
    }
    
    @Override
    protected void setDeviceName(final String name) {
        if (this.mDevice != null) {
            this.mDevice.setName(name);
        }
    }
}
