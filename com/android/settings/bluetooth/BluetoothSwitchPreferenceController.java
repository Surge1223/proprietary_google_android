package com.android.settings.bluetooth;

import com.android.settings.utils.AnnotationSpan;
import com.android.settings.location.ScanningSettings;
import com.android.settings.core.SubSettingLauncher;
import android.view.View;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.widget.FooterPreference;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.widget.SwitchWidgetController;
import android.view.View.OnClickListener;

public class BluetoothSwitchPreferenceController implements View.OnClickListener, OnSwitchChangeListener, LifecycleObserver, OnStart, OnStop
{
    @VisibleForTesting
    LocalBluetoothAdapter mBluetoothAdapter;
    private BluetoothEnabler mBluetoothEnabler;
    private LocalBluetoothManager mBluetoothManager;
    private Context mContext;
    private FooterPreference mFooterPreference;
    private RestrictionUtils mRestrictionUtils;
    private SwitchWidgetController mSwitch;
    
    public BluetoothSwitchPreferenceController(final Context context, final SwitchWidgetController switchWidgetController, final FooterPreference footerPreference) {
        this(context, Utils.getLocalBtManager(context), new RestrictionUtils(), switchWidgetController, footerPreference);
    }
    
    public BluetoothSwitchPreferenceController(final Context mContext, final LocalBluetoothManager mBluetoothManager, final RestrictionUtils mRestrictionUtils, final SwitchWidgetController mSwitch, final FooterPreference mFooterPreference) {
        this.mBluetoothManager = mBluetoothManager;
        this.mRestrictionUtils = mRestrictionUtils;
        this.mSwitch = mSwitch;
        this.mContext = mContext;
        this.mFooterPreference = mFooterPreference;
        this.mSwitch.setupView();
        this.updateText(this.mSwitch.isChecked());
        if (this.mBluetoothManager != null) {
            this.mBluetoothAdapter = this.mBluetoothManager.getBluetoothAdapter();
        }
        (this.mBluetoothEnabler = new BluetoothEnabler(mContext, mSwitch, FeatureFactory.getFactory(mContext).getMetricsFeatureProvider(), this.mBluetoothManager, 870, this.mRestrictionUtils)).setToggleCallback(this);
    }
    
    public void onClick(final View view) {
        new SubSettingLauncher(this.mContext).setDestination(ScanningSettings.class.getName()).setSourceMetricsCategory(1390).launch();
    }
    
    public void onStart() {
        this.mBluetoothEnabler.resume(this.mContext);
        if (this.mSwitch != null) {
            this.updateText(this.mSwitch.isChecked());
        }
    }
    
    public void onStop() {
        this.mBluetoothEnabler.pause();
    }
    
    public boolean onSwitchToggled(final boolean b) {
        this.updateText(b);
        return true;
    }
    
    @VisibleForTesting
    void updateText(final boolean b) {
        if (!b && Utils.isBluetoothScanningEnabled(this.mContext)) {
            this.mFooterPreference.setTitle(AnnotationSpan.linkify(this.mContext.getText(2131886878), new AnnotationSpan.LinkInfo("link", (View.OnClickListener)this)));
        }
        else {
            this.mFooterPreference.setTitle(2131886770);
        }
    }
}
