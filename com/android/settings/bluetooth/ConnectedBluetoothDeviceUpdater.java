package com.android.settings.bluetooth;

import android.bluetooth.BluetoothDevice;
import com.android.settings.widget.GearPreference;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v7.preference.Preference;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settings.dashboard.DashboardFragment;
import android.content.Context;
import android.media.AudioManager;

public class ConnectedBluetoothDeviceUpdater extends BluetoothDeviceUpdater
{
    private final AudioManager mAudioManager;
    
    public ConnectedBluetoothDeviceUpdater(final Context context, final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback) {
        super(context, dashboardFragment, devicePreferenceCallback);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
    }
    
    ConnectedBluetoothDeviceUpdater(final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback, final LocalBluetoothManager localBluetoothManager) {
        super(dashboardFragment, devicePreferenceCallback, localBluetoothManager);
        this.mAudioManager = (AudioManager)dashboardFragment.getContext().getSystemService("audio");
    }
    
    @Override
    protected void addPreference(final CachedBluetoothDevice cachedBluetoothDevice) {
        super.addPreference(cachedBluetoothDevice);
        final BluetoothDevice device = cachedBluetoothDevice.getDevice();
        if (this.mPreferenceMap.containsKey(device)) {
            final BluetoothDevicePreference bluetoothDevicePreference = this.mPreferenceMap.get(device);
            bluetoothDevicePreference.setOnGearClickListener(null);
            bluetoothDevicePreference.hideSecondTarget(true);
            bluetoothDevicePreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$ConnectedBluetoothDeviceUpdater$cbcA1LEPXmJVOc_WhespijdA8X8(this));
        }
    }
    
    @Override
    public boolean isFilterMatched(final CachedBluetoothDevice cachedBluetoothDevice) {
        final int mode = this.mAudioManager.getMode();
        int n = 2;
        if (mode == 1 || mode == 2 || mode == 3) {
            n = 1;
        }
        boolean b = false;
        if (this.isDeviceConnected(cachedBluetoothDevice)) {
            if (cachedBluetoothDevice.isConnectedHearingAidDevice()) {
                return false;
            }
            switch (n) {
                default: {
                    b = b;
                    break;
                }
                case 2: {
                    b = (true ^ cachedBluetoothDevice.isA2dpDevice());
                    break;
                }
                case 1: {
                    b = (true ^ cachedBluetoothDevice.isHfpDevice());
                    break;
                }
            }
        }
        return b;
    }
    
    @Override
    public void onAudioModeChanged() {
        this.forceUpdate();
    }
    
    @Override
    public void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
        if (n == 2) {
            if (this.isFilterMatched(cachedBluetoothDevice)) {
                this.addPreference(cachedBluetoothDevice);
            }
            else {
                this.removePreference(cachedBluetoothDevice);
            }
        }
        else if (n == 0) {
            this.removePreference(cachedBluetoothDevice);
        }
    }
}
