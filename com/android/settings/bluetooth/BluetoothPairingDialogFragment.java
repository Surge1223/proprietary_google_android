package com.android.settings.bluetooth;

import android.content.Context;
import android.app.Dialog;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.widget.Button;
import android.text.Editable;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.widget.CompoundButton;
import android.content.DialogInterface$OnShowListener;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.CheckBox;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.widget.EditText;
import android.app.AlertDialog;
import android.app.AlertDialog$Builder;
import android.text.TextWatcher;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class BluetoothPairingDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, TextWatcher
{
    private AlertDialog$Builder mBuilder;
    private AlertDialog mDialog;
    private BluetoothPairingController mPairingController;
    private BluetoothPairingDialog mPairingDialogActivity;
    private EditText mPairingView;
    
    private AlertDialog createConfirmationDialog() {
        this.mBuilder.setTitle((CharSequence)this.getString(2131886832, new Object[] { this.mPairingController.getDeviceName() }));
        this.mBuilder.setView(this.createView());
        this.mBuilder.setPositiveButton((CharSequence)this.getString(2131886818), (DialogInterface$OnClickListener)this);
        this.mBuilder.setNegativeButton((CharSequence)this.getString(2131886820), (DialogInterface$OnClickListener)this);
        return this.mBuilder.create();
    }
    
    private AlertDialog createConsentDialog() {
        return this.createConfirmationDialog();
    }
    
    private AlertDialog createDisplayPasskeyOrPinDialog() {
        this.mBuilder.setTitle((CharSequence)this.getString(2131886832, new Object[] { this.mPairingController.getDeviceName() }));
        this.mBuilder.setView(this.createView());
        this.mBuilder.setNegativeButton((CharSequence)this.getString(17039360), (DialogInterface$OnClickListener)this);
        final AlertDialog create = this.mBuilder.create();
        this.mPairingController.notifyDialogDisplayed();
        return create;
    }
    
    private View createPinEntryView() {
        final View inflate = this.getActivity().getLayoutInflater().inflate(2131558475, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131362448);
        final TextView textView2 = (TextView)inflate.findViewById(2131362363);
        final CheckBox checkBox = (CheckBox)inflate.findViewById(2131361861);
        final CheckBox checkBox2 = (CheckBox)inflate.findViewById(2131362446);
        checkBox2.setText((CharSequence)this.getString(2131886833, new Object[] { this.mPairingController.getDeviceName() }));
        final EditText mPairingView = (EditText)inflate.findViewById(2131362725);
        int visibility;
        if (this.mPairingController.isProfileReady()) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        checkBox2.setVisibility(visibility);
        this.mPairingController.setContactSharingState();
        checkBox2.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this.mPairingController);
        checkBox2.setChecked(this.mPairingController.getContactSharingState());
        (this.mPairingView = mPairingView).setInputType(2);
        mPairingView.addTextChangedListener((TextWatcher)this);
        checkBox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new _$$Lambda$BluetoothPairingDialogFragment$r7iz4I0mbAZSn1y_rbFsqcyiwC0(this));
        final int deviceVariantMessageId = this.mPairingController.getDeviceVariantMessageId();
        final int deviceVariantMessageHintId = this.mPairingController.getDeviceVariantMessageHintId();
        final int deviceMaxPasskeyLength = this.mPairingController.getDeviceMaxPasskeyLength();
        int visibility2;
        if (this.mPairingController.pairingCodeIsAlphanumeric()) {
            visibility2 = 0;
        }
        else {
            visibility2 = 8;
        }
        checkBox.setVisibility(visibility2);
        if (deviceVariantMessageId != -1) {
            textView2.setText(deviceVariantMessageId);
        }
        else {
            textView2.setVisibility(8);
        }
        if (deviceVariantMessageHintId != -1) {
            textView.setText(deviceVariantMessageHintId);
        }
        else {
            textView.setVisibility(8);
        }
        mPairingView.setFilters(new InputFilter[] { new InputFilter$LengthFilter(deviceMaxPasskeyLength) });
        return inflate;
    }
    
    private AlertDialog createUserEntryDialog() {
        this.mBuilder.setTitle((CharSequence)this.getString(2131886832, new Object[] { this.mPairingController.getDeviceName() }));
        this.mBuilder.setView(this.createPinEntryView());
        this.mBuilder.setPositiveButton((CharSequence)this.getString(17039370), (DialogInterface$OnClickListener)this);
        this.mBuilder.setNegativeButton((CharSequence)this.getString(17039360), (DialogInterface$OnClickListener)this);
        final AlertDialog create = this.mBuilder.create();
        create.setOnShowListener((DialogInterface$OnShowListener)new _$$Lambda$BluetoothPairingDialogFragment$ItV61WjNe_T4YaZN6BYGTBHLdZc(this));
        return create;
    }
    
    private View createView() {
        final View inflate = this.getActivity().getLayoutInflater().inflate(2131558474, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131362432);
        final TextView textView2 = (TextView)inflate.findViewById(2131362434);
        final TextView textView3 = (TextView)inflate.findViewById(2131362433);
        final CheckBox checkBox = (CheckBox)inflate.findViewById(2131362445);
        checkBox.setText((CharSequence)this.getString(2131886833, new Object[] { this.mPairingController.getDeviceName() }));
        final boolean profileReady = this.mPairingController.isProfileReady();
        final int n = 8;
        int visibility;
        if (profileReady) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        checkBox.setVisibility(visibility);
        this.mPairingController.setContactSharingState();
        checkBox.setChecked(this.mPairingController.getContactSharingState());
        checkBox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this.mPairingController);
        int visibility2;
        if (this.mPairingController.isDisplayPairingKeyVariant()) {
            visibility2 = 0;
        }
        else {
            visibility2 = n;
        }
        textView3.setVisibility(visibility2);
        if (this.mPairingController.hasPairingContent()) {
            textView.setVisibility(0);
            textView2.setVisibility(0);
            textView2.setText((CharSequence)this.mPairingController.getPairingContent());
        }
        return inflate;
    }
    
    private AlertDialog setupDialog() {
        AlertDialog alertDialog = null;
        switch (this.mPairingController.getDialogType()) {
            default: {
                alertDialog = null;
                Log.e("BTPairingDialogFragment", "Incorrect pairing type received, not showing any dialog");
                break;
            }
            case 2: {
                alertDialog = this.createDisplayPasskeyOrPinDialog();
                break;
            }
            case 1: {
                alertDialog = this.createConsentDialog();
                break;
            }
            case 0: {
                alertDialog = this.createUserEntryDialog();
                break;
            }
        }
        return alertDialog;
    }
    
    public void afterTextChanged(final Editable editable) {
        final Button button = this.mDialog.getButton(-1);
        if (button != null) {
            button.setEnabled(this.mPairingController.isPasskeyValid(editable));
        }
        this.mPairingController.updateUserInput(editable.toString());
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    public int getMetricsCategory() {
        return 613;
    }
    
    @VisibleForTesting
    CharSequence getPairingViewText() {
        if (this.mPairingView != null) {
            return (CharSequence)this.mPairingView.getText();
        }
        return null;
    }
    
    boolean isPairingControllerSet() {
        return this.mPairingController != null;
    }
    
    boolean isPairingDialogActivitySet() {
        return this.mPairingDialogActivity != null;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            this.mPairingController.onDialogPositiveClick(this);
        }
        else if (n == -2) {
            this.mPairingController.onDialogNegativeClick(this);
        }
        this.mPairingDialogActivity.dismiss();
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        if (!this.isPairingControllerSet()) {
            throw new IllegalStateException("Must call setPairingController() before showing dialog");
        }
        if (this.isPairingDialogActivitySet()) {
            this.mBuilder = new AlertDialog$Builder((Context)this.getActivity());
            (this.mDialog = this.setupDialog()).setCanceledOnTouchOutside(false);
            return (Dialog)this.mDialog;
        }
        throw new IllegalStateException("Must call setPairingDialogActivity() before showing dialog");
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    void setPairingController(final BluetoothPairingController mPairingController) {
        if (!this.isPairingControllerSet()) {
            this.mPairingController = mPairingController;
            return;
        }
        throw new IllegalStateException("The controller can only be set once. Forcibly replacing it will lead to undefined behavior");
    }
    
    void setPairingDialogActivity(final BluetoothPairingDialog mPairingDialogActivity) {
        if (!this.isPairingDialogActivitySet()) {
            this.mPairingDialogActivity = mPairingDialogActivity;
            return;
        }
        throw new IllegalStateException("The pairing dialog activity can only be set once");
    }
}
