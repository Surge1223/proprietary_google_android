package com.android.settings.bluetooth;

import android.os.UserHandle;
import android.bluetooth.BluetoothDevice;
import android.os.PowerManager;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public final class BluetoothPairingRequest extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        if (!intent.getAction().equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
            return;
        }
        final Intent pairingDialogIntent = BluetoothPairingService.getPairingDialogIntent(context, intent);
        final PowerManager powerManager = (PowerManager)context.getSystemService("power");
        final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        String name = null;
        String address;
        if (bluetoothDevice != null) {
            address = bluetoothDevice.getAddress();
        }
        else {
            address = null;
        }
        if (bluetoothDevice != null) {
            name = bluetoothDevice.getName();
        }
        final boolean shouldShowDialogInForeground = LocalBluetoothPreferences.shouldShowDialogInForeground(context, address, name);
        if (powerManager.isInteractive() && shouldShowDialogInForeground) {
            context.startActivityAsUser(pairingDialogIntent, UserHandle.CURRENT);
        }
        else {
            intent.setClass(context, (Class)BluetoothPairingService.class);
            context.startServiceAsUser(intent, UserHandle.CURRENT);
        }
    }
}
