package com.android.settings.bluetooth;

import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.UserManager;
import android.os.Bundle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.os.Parcelable;
import android.content.Intent;
import android.bluetooth.BluetoothDevice;

public final class DevicePickerFragment extends DeviceListPreferenceFragment
{
    BluetoothProgressCategory mAvailableDevicesCategory;
    private String mLaunchClass;
    private String mLaunchPackage;
    private boolean mNeedAuth;
    private boolean mScanAllowed;
    
    public DevicePickerFragment() {
        super(null);
    }
    
    private void sendDevicePickedIntent(final BluetoothDevice bluetoothDevice) {
        final Intent intent = new Intent("android.bluetooth.devicepicker.action.DEVICE_SELECTED");
        intent.putExtra("android.bluetooth.device.extra.DEVICE", (Parcelable)bluetoothDevice);
        if (this.mLaunchPackage != null && this.mLaunchClass != null) {
            intent.setClassName(this.mLaunchPackage, this.mLaunchClass);
        }
        this.getActivity().sendBroadcast(intent);
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return null;
    }
    
    @Override
    public String getDeviceListKey() {
        return "bt_device_list";
    }
    
    @Override
    protected String getLogTag() {
        return "DevicePickerFragment";
    }
    
    @Override
    public int getMetricsCategory() {
        return 25;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082762;
    }
    
    @Override
    void initPreferencesFromPreferenceScreen() {
        final Intent intent = this.getActivity().getIntent();
        this.mNeedAuth = intent.getBooleanExtra("android.bluetooth.devicepicker.extra.NEED_AUTH", false);
        this.setFilter(intent.getIntExtra("android.bluetooth.devicepicker.extra.FILTER_TYPE", 0));
        this.mLaunchPackage = intent.getStringExtra("android.bluetooth.devicepicker.extra.LAUNCH_PACKAGE");
        this.mLaunchClass = intent.getStringExtra("android.bluetooth.devicepicker.extra.DEVICE_PICKER_LAUNCH_CLASS");
        this.mAvailableDevicesCategory = (BluetoothProgressCategory)this.findPreference("bt_device_list");
    }
    
    @Override
    public void onBluetoothStateChanged(final int n) {
        super.onBluetoothStateChanged(n);
        if (n == 12) {
            this.enableScanning();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle((CharSequence)this.getString(2131887393));
        this.mScanAllowed = (((UserManager)this.getSystemService("user")).hasUserRestriction("no_config_bluetooth") ^ true);
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mSelectedDevice == null) {
            this.sendDevicePickedIntent(null);
        }
    }
    
    @Override
    public void onDeviceBondStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        final BluetoothDevice device = cachedBluetoothDevice.getDevice();
        if (!device.equals((Object)this.mSelectedDevice)) {
            return;
        }
        if (n == 12) {
            this.sendDevicePickedIntent(device);
            this.finish();
        }
        else if (n == 10) {
            this.enableScanning();
        }
    }
    
    @Override
    void onDevicePreferenceClick(final BluetoothDevicePreference bluetoothDevicePreference) {
        this.disableScanning();
        LocalBluetoothPreferences.persistSelectedDeviceInPicker((Context)this.getActivity(), this.mSelectedDevice.getAddress());
        if (bluetoothDevicePreference.getCachedDevice().getBondState() != 12 && this.mNeedAuth) {
            super.onDevicePreferenceClick(bluetoothDevicePreference);
        }
        else {
            this.sendDevicePickedIntent(this.mSelectedDevice);
            this.finish();
        }
    }
    
    @Override
    public void onScanningStateChanged(final boolean b) {
        super.onScanningStateChanged(b);
        this.mAvailableDevicesCategory.setProgress(b | this.mScanEnabled);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.addCachedDevices();
        this.mSelectedDevice = null;
        if (this.mScanAllowed) {
            this.enableScanning();
            this.mAvailableDevicesCategory.setProgress(this.mLocalAdapter.isDiscovering());
        }
    }
    
    @Override
    public void onStop() {
        this.disableScanning();
        super.onStop();
    }
}
