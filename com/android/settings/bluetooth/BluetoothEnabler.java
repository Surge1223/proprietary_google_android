package com.android.settings.bluetooth;

import android.widget.Toast;
import com.android.settingslib.WirelessUtils;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Intent;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.content.IntentFilter;
import android.content.Context;
import com.android.settings.widget.SwitchWidgetController;

public final class BluetoothEnabler implements OnSwitchChangeListener
{
    private OnSwitchChangeListener mCallback;
    private Context mContext;
    private final IntentFilter mIntentFilter;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final int mMetricsEvent;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final BroadcastReceiver mReceiver;
    private final RestrictionUtils mRestrictionUtils;
    private final SwitchWidgetController mSwitchController;
    private boolean mValidListener;
    
    public BluetoothEnabler(final Context mContext, final SwitchWidgetController mSwitchController, final MetricsFeatureProvider mMetricsFeatureProvider, final LocalBluetoothManager localBluetoothManager, final int mMetricsEvent, final RestrictionUtils mRestrictionUtils) {
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                BluetoothEnabler.this.handleStateChanged(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE));
            }
        };
        this.mContext = mContext;
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        (this.mSwitchController = mSwitchController).setListener((SwitchWidgetController.OnSwitchChangeListener)this);
        this.mValidListener = false;
        this.mMetricsEvent = mMetricsEvent;
        if (localBluetoothManager == null) {
            this.mLocalAdapter = null;
            this.mSwitchController.setEnabled(false);
        }
        else {
            this.mLocalAdapter = localBluetoothManager.getBluetoothAdapter();
        }
        this.mIntentFilter = new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED");
        this.mRestrictionUtils = mRestrictionUtils;
    }
    
    public static RestrictedLockUtils.EnforcedAdmin getEnforcedAdmin(final RestrictionUtils restrictionUtils, final Context context) {
        RestrictedLockUtils.EnforcedAdmin enforcedAdmin;
        if ((enforcedAdmin = restrictionUtils.checkIfRestrictionEnforced(context, "no_bluetooth")) == null) {
            enforcedAdmin = restrictionUtils.checkIfRestrictionEnforced(context, "no_config_bluetooth");
        }
        return enforcedAdmin;
    }
    
    private void setChecked(final boolean checked) {
        if (checked != this.mSwitchController.isChecked()) {
            if (this.mValidListener) {
                this.mSwitchController.stopListening();
            }
            this.mSwitchController.setChecked(checked);
            if (this.mValidListener) {
                this.mSwitchController.startListening();
            }
        }
    }
    
    private void triggerParentPreferenceCallback(final boolean b) {
        if (this.mCallback != null) {
            this.mCallback.onSwitchToggled(b);
        }
    }
    
    void handleStateChanged(final int n) {
        switch (n) {
            default: {
                this.setChecked(false);
                this.mSwitchController.setEnabled(true);
                break;
            }
            case 13: {
                this.mSwitchController.setEnabled(false);
                break;
            }
            case 12: {
                this.setChecked(true);
                this.mSwitchController.setEnabled(true);
                break;
            }
            case 11: {
                this.mSwitchController.setEnabled(false);
                break;
            }
            case 10: {
                this.setChecked(false);
                this.mSwitchController.setEnabled(true);
                break;
            }
        }
    }
    
    @VisibleForTesting
    boolean maybeEnforceRestrictions() {
        final RestrictedLockUtils.EnforcedAdmin enforcedAdmin = getEnforcedAdmin(this.mRestrictionUtils, this.mContext);
        this.mSwitchController.setDisabledByAdmin(enforcedAdmin);
        boolean b = false;
        if (enforcedAdmin != null) {
            this.mSwitchController.setChecked(false);
            this.mSwitchController.setEnabled(false);
        }
        if (enforcedAdmin != null) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean onSwitchToggled(final boolean bluetoothEnabled) {
        if (this.maybeEnforceRestrictions()) {
            this.triggerParentPreferenceCallback(bluetoothEnabled);
            return true;
        }
        if (bluetoothEnabled && !WirelessUtils.isRadioAllowed(this.mContext, "bluetooth")) {
            Toast.makeText(this.mContext, 2131890025, 0).show();
            this.mSwitchController.setChecked(false);
            this.triggerParentPreferenceCallback(false);
            return false;
        }
        this.mMetricsFeatureProvider.action(this.mContext, this.mMetricsEvent, bluetoothEnabled);
        if (this.mLocalAdapter != null) {
            final boolean setBluetoothEnabled = this.mLocalAdapter.setBluetoothEnabled(bluetoothEnabled);
            if (bluetoothEnabled && !setBluetoothEnabled) {
                this.mSwitchController.setChecked(false);
                this.mSwitchController.setEnabled(true);
                this.mSwitchController.updateTitle(false);
                this.triggerParentPreferenceCallback(false);
                return false;
            }
        }
        this.mSwitchController.setEnabled(false);
        this.triggerParentPreferenceCallback(bluetoothEnabled);
        return true;
    }
    
    public void pause() {
        if (this.mLocalAdapter == null) {
            return;
        }
        if (this.mValidListener) {
            this.mSwitchController.stopListening();
            this.mContext.unregisterReceiver(this.mReceiver);
            this.mValidListener = false;
        }
    }
    
    public void resume(final Context mContext) {
        if (this.mContext != mContext) {
            this.mContext = mContext;
        }
        final boolean maybeEnforceRestrictions = this.maybeEnforceRestrictions();
        if (this.mLocalAdapter == null) {
            this.mSwitchController.setEnabled(false);
            return;
        }
        if (!maybeEnforceRestrictions) {
            this.handleStateChanged(this.mLocalAdapter.getBluetoothState());
        }
        this.mSwitchController.startListening();
        this.mContext.registerReceiver(this.mReceiver, this.mIntentFilter);
        this.mValidListener = true;
    }
    
    public void setToggleCallback(final OnSwitchChangeListener mCallback) {
        this.mCallback = mCallback;
    }
}
