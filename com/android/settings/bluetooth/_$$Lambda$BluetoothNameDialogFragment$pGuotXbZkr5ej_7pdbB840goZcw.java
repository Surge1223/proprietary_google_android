package com.android.settings.bluetooth;

import android.content.DialogInterface$OnShowListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.view.inputmethod.InputMethodManager;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView$OnEditorActionListener;
import com.android.settings.Utils;
import android.text.TextUtils;
import android.text.InputFilter;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.AlertDialog;
import android.text.TextWatcher;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$BluetoothNameDialogFragment$pGuotXbZkr5ej_7pdbB840goZcw implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        BluetoothNameDialogFragment.lambda$onCreateDialog$0(this.f$0, dialogInterface, n);
    }
}
