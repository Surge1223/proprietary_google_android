package com.android.settings.bluetooth;

import android.content.IntentFilter;
import android.os.Bundle;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.app.Activity;

public class BluetoothPairingDialog extends Activity
{
    private BluetoothPairingController mBluetoothPairingController;
    private final BroadcastReceiver mReceiver;
    private boolean mReceiverRegistered;
    
    public BluetoothPairingDialog() {
        this.mReceiverRegistered = false;
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.bluetooth.device.action.BOND_STATE_CHANGED".equals(action)) {
                    final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                    if (intExtra == 12 || intExtra == 10) {
                        BluetoothPairingDialog.this.dismiss();
                    }
                }
                else if ("android.bluetooth.device.action.PAIRING_CANCEL".equals(action)) {
                    final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    if (bluetoothDevice == null || BluetoothPairingDialog.this.mBluetoothPairingController.deviceEquals(bluetoothDevice)) {
                        BluetoothPairingDialog.this.dismiss();
                    }
                }
            }
        };
    }
    
    void dismiss() {
        if (!this.isFinishing()) {
            this.finish();
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mBluetoothPairingController = new BluetoothPairingController(this.getIntent(), (Context)this);
        boolean b = true;
        BluetoothPairingDialogFragment bluetoothPairingDialogFragment2;
        final BluetoothPairingDialogFragment bluetoothPairingDialogFragment = bluetoothPairingDialogFragment2 = (BluetoothPairingDialogFragment)this.getFragmentManager().findFragmentByTag("bluetooth.pairing.fragment");
        Label_0064: {
            if (bluetoothPairingDialogFragment != null) {
                if (!bluetoothPairingDialogFragment.isPairingControllerSet()) {
                    bluetoothPairingDialogFragment2 = bluetoothPairingDialogFragment;
                    if (!bluetoothPairingDialogFragment.isPairingDialogActivitySet()) {
                        break Label_0064;
                    }
                }
                bluetoothPairingDialogFragment.dismiss();
                bluetoothPairingDialogFragment2 = null;
            }
        }
        BluetoothPairingDialogFragment bluetoothPairingDialogFragment3;
        if ((bluetoothPairingDialogFragment3 = bluetoothPairingDialogFragment2) == null) {
            b = false;
            bluetoothPairingDialogFragment3 = new BluetoothPairingDialogFragment();
        }
        bluetoothPairingDialogFragment3.setPairingController(this.mBluetoothPairingController);
        bluetoothPairingDialogFragment3.setPairingDialogActivity(this);
        if (!b) {
            bluetoothPairingDialogFragment3.show(this.getFragmentManager(), "bluetooth.pairing.fragment");
        }
        this.registerReceiver(this.mReceiver, new IntentFilter("android.bluetooth.device.action.PAIRING_CANCEL"));
        this.registerReceiver(this.mReceiver, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
        this.mReceiverRegistered = true;
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (this.mReceiverRegistered) {
            this.mReceiverRegistered = false;
            this.unregisterReceiver(this.mReceiver);
        }
    }
}
