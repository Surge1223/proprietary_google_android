package com.android.settings.bluetooth;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Notification$Builder;
import android.app.NotificationManager;
import android.os.PowerManager;
import android.os.UserManager;
import android.os.Parcelable;
import android.content.Intent;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.util.Log;
import android.app.NotificationChannel;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.BroadcastReceiver;

public final class BluetoothPermissionRequest extends BroadcastReceiver
{
    Context mContext;
    BluetoothDevice mDevice;
    private NotificationChannel mNotificationChannel;
    int mRequestType;
    String mReturnClass;
    String mReturnPackage;
    
    public BluetoothPermissionRequest() {
        this.mNotificationChannel = null;
        this.mReturnPackage = null;
        this.mReturnClass = null;
    }
    
    private boolean checkUserChoice() {
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = false;
        if (this.mRequestType != 2 && this.mRequestType != 3 && this.mRequestType != 4) {
            return false;
        }
        final LocalBluetoothManager localBtManager = Utils.getLocalBtManager(this.mContext);
        final CachedBluetoothDeviceManager cachedDeviceManager = localBtManager.getCachedDeviceManager();
        CachedBluetoothDevice cachedBluetoothDevice;
        if ((cachedBluetoothDevice = cachedDeviceManager.findDevice(this.mDevice)) == null) {
            cachedBluetoothDevice = cachedDeviceManager.addDevice(localBtManager.getBluetoothAdapter(), localBtManager.getProfileManager(), this.mDevice);
        }
        if (this.mRequestType == 2) {
            final int phonebookPermissionChoice = cachedBluetoothDevice.getPhonebookPermissionChoice();
            if (phonebookPermissionChoice != 0) {
                if (phonebookPermissionChoice == 1) {
                    this.sendReplyIntentToReceiver(true);
                    b3 = true;
                }
                else if (phonebookPermissionChoice == 2) {
                    this.sendReplyIntentToReceiver(false);
                    b3 = true;
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Bad phonebookPermission: ");
                    sb.append(phonebookPermissionChoice);
                    Log.e("BluetoothPermissionRequest", sb.toString());
                }
            }
        }
        else if (this.mRequestType == 3) {
            final int messagePermissionChoice = cachedBluetoothDevice.getMessagePermissionChoice();
            if (messagePermissionChoice == 0) {
                b3 = b;
            }
            else if (messagePermissionChoice == 1) {
                this.sendReplyIntentToReceiver(true);
                b3 = true;
            }
            else if (messagePermissionChoice == 2) {
                this.sendReplyIntentToReceiver(false);
                b3 = true;
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Bad messagePermission: ");
                sb2.append(messagePermissionChoice);
                Log.e("BluetoothPermissionRequest", sb2.toString());
                b3 = b;
            }
        }
        else {
            b3 = b2;
            if (this.mRequestType == 4) {
                final int simPermissionChoice = cachedBluetoothDevice.getSimPermissionChoice();
                if (simPermissionChoice == 0) {
                    b3 = b2;
                }
                else if (simPermissionChoice == 1) {
                    this.sendReplyIntentToReceiver(true);
                    b3 = true;
                }
                else if (simPermissionChoice == 2) {
                    this.sendReplyIntentToReceiver(false);
                    b3 = true;
                }
                else {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Bad simPermission: ");
                    sb3.append(simPermissionChoice);
                    Log.e("BluetoothPermissionRequest", sb3.toString());
                    b3 = b2;
                }
            }
        }
        return b3;
    }
    
    private String getNotificationTag(final int n) {
        if (n == 2) {
            return "Phonebook Access";
        }
        if (this.mRequestType == 3) {
            return "Message Access";
        }
        if (this.mRequestType == 4) {
            return "SIM Access";
        }
        return null;
    }
    
    private void sendReplyIntentToReceiver(final boolean b) {
        final Intent intent = new Intent("android.bluetooth.device.action.CONNECTION_ACCESS_REPLY");
        if (this.mReturnPackage != null && this.mReturnClass != null) {
            intent.setClassName(this.mReturnPackage, this.mReturnClass);
        }
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 2;
        }
        intent.putExtra("android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT", n);
        intent.putExtra("android.bluetooth.device.extra.DEVICE", (Parcelable)this.mDevice);
        intent.putExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", this.mRequestType);
        this.mContext.sendBroadcast(intent, "android.permission.BLUETOOTH_ADMIN");
    }
    
    public void onReceive(final Context mContext, final Intent intent) {
        this.mContext = mContext;
        final String action = intent.getAction();
        if (action.equals("android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST")) {
            if (((UserManager)mContext.getSystemService("user")).isManagedProfile()) {
                return;
            }
            this.mDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            this.mRequestType = intent.getIntExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", 1);
            this.mReturnPackage = intent.getStringExtra("android.bluetooth.device.extra.PACKAGE_NAME");
            this.mReturnClass = intent.getStringExtra("android.bluetooth.device.extra.CLASS_NAME");
            if (this.checkUserChoice()) {
                return;
            }
            final Intent intent2 = new Intent(action);
            intent2.setClass(mContext, (Class)BluetoothPermissionActivity.class);
            intent2.setFlags(402653184);
            intent2.setType(Integer.toString(this.mRequestType));
            intent2.putExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", this.mRequestType);
            intent2.putExtra("android.bluetooth.device.extra.DEVICE", (Parcelable)this.mDevice);
            intent2.putExtra("android.bluetooth.device.extra.PACKAGE_NAME", this.mReturnPackage);
            intent2.putExtra("android.bluetooth.device.extra.CLASS_NAME", this.mReturnClass);
            final BluetoothDevice mDevice = this.mDevice;
            String name = null;
            String address;
            if (mDevice != null) {
                address = this.mDevice.getAddress();
            }
            else {
                address = null;
            }
            if (this.mDevice != null) {
                name = this.mDevice.getName();
            }
            if (((PowerManager)mContext.getSystemService("power")).isScreenOn() && LocalBluetoothPreferences.shouldShowDialogInForeground(mContext, address, name)) {
                mContext.startActivity(intent2);
            }
            else {
                final Intent intent3 = new Intent("android.bluetooth.device.action.CONNECTION_ACCESS_REPLY");
                intent3.putExtra("android.bluetooth.device.extra.DEVICE", (Parcelable)this.mDevice);
                intent3.putExtra("android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT", 2);
                intent3.putExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", this.mRequestType);
                final String remoteName = Utils.createRemoteName(mContext, this.mDevice);
                String contentTitle = null;
                String s = null;
                switch (this.mRequestType) {
                    default: {
                        contentTitle = mContext.getString(2131886725);
                        s = mContext.getString(2131886723, new Object[] { remoteName, remoteName });
                        break;
                    }
                    case 4: {
                        contentTitle = mContext.getString(2131886876);
                        s = mContext.getString(2131886873, new Object[] { remoteName, remoteName });
                        break;
                    }
                    case 3: {
                        contentTitle = mContext.getString(2131886798);
                        s = mContext.getString(2131886795, new Object[] { remoteName, remoteName });
                        break;
                    }
                    case 2: {
                        contentTitle = mContext.getString(2131886840);
                        s = mContext.getString(2131886838, new Object[] { remoteName, remoteName });
                        break;
                    }
                }
                final NotificationManager notificationManager = (NotificationManager)mContext.getSystemService("notification");
                if (this.mNotificationChannel == null) {
                    notificationManager.createNotificationChannel(this.mNotificationChannel = new NotificationChannel("bluetooth_notification_channel", (CharSequence)mContext.getString(2131886686), 4));
                }
                final Notification build = new Notification$Builder(mContext, "bluetooth_notification_channel").setContentTitle((CharSequence)contentTitle).setTicker((CharSequence)s).setContentText((CharSequence)s).setSmallIcon(17301632).setAutoCancel(true).setPriority(2).setOnlyAlertOnce(false).setDefaults(-1).setContentIntent(PendingIntent.getActivity(mContext, 0, intent2, 0)).setDeleteIntent(PendingIntent.getBroadcast(mContext, 0, intent3, 0)).setColor(mContext.getColor(17170774)).setLocalOnly(true).build();
                build.flags |= 0x20;
                notificationManager.notify(this.getNotificationTag(this.mRequestType), 17301632, build);
            }
        }
        else if (action.equals("android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL")) {
            final NotificationManager notificationManager2 = (NotificationManager)mContext.getSystemService("notification");
            this.mRequestType = intent.getIntExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", 2);
            notificationManager2.cancel(this.getNotificationTag(this.mRequestType), 17301632);
        }
    }
}
