package com.android.settings.bluetooth;

import android.widget.Toast;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog;
import com.android.settings.overlay.FeatureFactory;
import android.provider.Settings;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothManager;

public final class Utils
{
    private static final com.android.settingslib.bluetooth.Utils.ErrorListener mErrorListener;
    private static final LocalBluetoothManager.BluetoothManagerCallback mOnInitCallback;
    
    static {
        mErrorListener = new com.android.settingslib.bluetooth.Utils.ErrorListener() {
            @Override
            public void onShowError(final Context context, final String s, final int n) {
                Utils.showError(context, s, n);
            }
        };
        mOnInitCallback = new LocalBluetoothManager.BluetoothManagerCallback() {
            @Override
            public void onBluetoothManagerInitialized(final Context context, final LocalBluetoothManager localBluetoothManager) {
                com.android.settingslib.bluetooth.Utils.setErrorListener(Utils.mErrorListener);
            }
        };
    }
    
    public static String createRemoteName(final Context context, final BluetoothDevice bluetoothDevice) {
        String aliasName;
        if (bluetoothDevice != null) {
            aliasName = bluetoothDevice.getAliasName();
        }
        else {
            aliasName = null;
        }
        String string = aliasName;
        if (aliasName == null) {
            string = context.getString(R.string.unknown);
        }
        return string;
    }
    
    public static LocalBluetoothManager getLocalBtManager(final Context context) {
        return LocalBluetoothManager.getInstance(context, Utils.mOnInitCallback);
    }
    
    public static boolean isBluetoothScanningEnabled(final Context context) {
        final int int1 = Settings.Global.getInt(context.getContentResolver(), "ble_scan_always_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b;
    }
    
    static void showConnectingError(final Context context, final String s, final LocalBluetoothManager localBluetoothManager) {
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().visible(context, 0, 869);
        showError(context, s, 2131886722, localBluetoothManager);
    }
    
    static AlertDialog showDisconnectDialog(final Context context, AlertDialog create, final DialogInterface$OnClickListener dialogInterface$OnClickListener, final CharSequence title, final CharSequence message) {
        if (create == null) {
            create = new AlertDialog$Builder(context).setPositiveButton(17039370, dialogInterface$OnClickListener).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
        }
        else {
            if (create.isShowing()) {
                create.dismiss();
            }
            create.setButton(-1, context.getText(17039370), dialogInterface$OnClickListener);
        }
        create.setTitle(title);
        create.setMessage(message);
        create.show();
        return create;
    }
    
    static void showError(final Context context, final String s, final int n) {
        showError(context, s, n, getLocalBtManager(context));
    }
    
    private static void showError(final Context context, String string, final int n, final LocalBluetoothManager localBluetoothManager) {
        string = context.getString(n, new Object[] { string });
        final Context foregroundActivity = localBluetoothManager.getForegroundActivity();
        if (localBluetoothManager.isForegroundActivity()) {
            new AlertDialog$Builder(foregroundActivity).setTitle(2131886776).setMessage((CharSequence)string).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).show();
        }
        else {
            Toast.makeText(context, (CharSequence)string, 0).show();
        }
    }
}
