package com.android.settings.bluetooth;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import com.android.settings.widget.ActionButtonPreference;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$BluetoothDetailsButtonsController$eZ36ezumIpXzpP7dOOnqn_gI5Uk implements View.OnClickListener
{
    public final void onClick(final View view) {
        BluetoothDetailsButtonsController.lambda$refresh$2(this.f$0, view);
    }
}
