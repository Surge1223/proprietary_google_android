package com.android.settings.bluetooth;

import android.content.Intent;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.content.IntentFilter;
import android.content.Context;
import android.content.BroadcastReceiver;

public class AlwaysDiscoverable extends BroadcastReceiver
{
    private Context mContext;
    private IntentFilter mIntentFilter;
    private LocalBluetoothAdapter mLocalAdapter;
    boolean mStarted;
    
    public AlwaysDiscoverable(final Context mContext, final LocalBluetoothAdapter mLocalAdapter) {
        this.mContext = mContext;
        this.mLocalAdapter = mLocalAdapter;
        (this.mIntentFilter = new IntentFilter()).addAction("android.bluetooth.adapter.action.SCAN_MODE_CHANGED");
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (intent.getAction() != "android.bluetooth.adapter.action.SCAN_MODE_CHANGED") {
            return;
        }
        if (this.mLocalAdapter.getScanMode() != 23) {
            this.mLocalAdapter.setScanMode(23);
        }
    }
    
    public void start() {
        if (this.mStarted) {
            return;
        }
        this.mContext.registerReceiver((BroadcastReceiver)this, this.mIntentFilter);
        this.mStarted = true;
        if (this.mLocalAdapter.getScanMode() != 23) {
            this.mLocalAdapter.setScanMode(23);
        }
    }
    
    public void stop() {
        if (!this.mStarted) {
            return;
        }
        this.mContext.unregisterReceiver((BroadcastReceiver)this);
        this.mStarted = false;
        this.mLocalAdapter.setScanMode(21);
    }
}
