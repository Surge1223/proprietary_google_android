package com.android.settings.bluetooth;

import android.bluetooth.BluetoothAdapter;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.support.v4.util.Consumer;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.builders.SliceAction;
import com.android.settingslib.Utils;
import androidx.slice.builders.ListBuilder;
import androidx.slice.Slice;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.connecteddevice.BluetoothDashboardFragment;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri.Builder;
import android.content.IntentFilter;
import android.net.Uri;

public class BluetoothSliceBuilder
{
    public static final Uri BLUETOOTH_URI;
    public static final IntentFilter INTENT_FILTER;
    
    static {
        BLUETOOTH_URI = new Uri.Builder().scheme("content").authority("android.settings.slices").appendPath("action").appendPath("bluetooth").build();
        (INTENT_FILTER = new IntentFilter()).addAction("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED");
        BluetoothSliceBuilder.INTENT_FILTER.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
    }
    
    private static PendingIntent getBroadcastIntent(final Context context) {
        return PendingIntent.getBroadcast(context, 0, new Intent("com.android.settings.bluetooth.action.BLUETOOTH_MODE_CHANGED").setClass(context, (Class)SliceBroadcastReceiver.class), 268435456);
    }
    
    public static Intent getIntent(final Context context) {
        return DatabaseIndexingUtils.buildSearchResultPageIntent(context, BluetoothDashboardFragment.class.getName(), null, context.getText(2131886896).toString(), 747).setClassName(context.getPackageName(), SubSettings.class.getName()).setData(new Uri.Builder().appendPath("bluetooth").build());
    }
    
    private static PendingIntent getPrimaryAction(final Context context) {
        return PendingIntent.getActivity(context, 0, getIntent(context), 0);
    }
    
    public static Slice getSlice(final Context context) {
        final boolean bluetoothEnabled = isBluetoothEnabled();
        final CharSequence text = context.getText(2131886894);
        return new ListBuilder(context, BluetoothSliceBuilder.BLUETOOTH_URI, -1L).setAccentColor(Utils.getColorAccent(context)).addRow(new _$$Lambda$BluetoothSliceBuilder$t1meuX4HmFYfZCMXndFcRlc9eII(text, new SliceAction(getBroadcastIntent(context), null, bluetoothEnabled), new SliceAction(getPrimaryAction(context), IconCompat.createWithResource(context, R.drawable.ic_settings_bluetooth), text))).build();
    }
    
    public static void handleUriChange(final Context context, final Intent intent) {
        LocalBluetoothManager.getInstance(context, null).getBluetoothAdapter().setBluetoothEnabled(intent.getBooleanExtra("android.app.slice.extra.TOGGLE_STATE", false));
    }
    
    private static boolean isBluetoothEnabled() {
        return BluetoothAdapter.getDefaultAdapter().isEnabled();
    }
}
