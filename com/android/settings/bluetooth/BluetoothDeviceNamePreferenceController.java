package com.android.settings.bluetooth;

import android.content.IntentFilter;
import android.text.BidiFormatter;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;
import android.text.TextUtils;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.support.v7.preference.Preference;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class BluetoothDeviceNamePreferenceController extends BasePreferenceController implements LifecycleObserver, OnStart, OnStop
{
    private static final String TAG = "BluetoothNamePrefCtrl";
    protected LocalBluetoothAdapter mLocalAdapter;
    private LocalBluetoothManager mLocalManager;
    Preference mPreference;
    final BroadcastReceiver mReceiver;
    
    BluetoothDeviceNamePreferenceController(final Context context, final LocalBluetoothAdapter mLocalAdapter, final String s) {
        super(context, s);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (TextUtils.equals((CharSequence)action, (CharSequence)"android.bluetooth.adapter.action.LOCAL_NAME_CHANGED")) {
                    if (BluetoothDeviceNamePreferenceController.this.mPreference != null && BluetoothDeviceNamePreferenceController.this.mLocalAdapter != null && BluetoothDeviceNamePreferenceController.this.mLocalAdapter.isEnabled()) {
                        BluetoothDeviceNamePreferenceController.this.updatePreferenceState(BluetoothDeviceNamePreferenceController.this.mPreference);
                    }
                }
                else if (TextUtils.equals((CharSequence)action, (CharSequence)"android.bluetooth.adapter.action.STATE_CHANGED")) {
                    BluetoothDeviceNamePreferenceController.this.updatePreferenceState(BluetoothDeviceNamePreferenceController.this.mPreference);
                }
            }
        };
        this.mLocalAdapter = mLocalAdapter;
    }
    
    public BluetoothDeviceNamePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (TextUtils.equals((CharSequence)action, (CharSequence)"android.bluetooth.adapter.action.LOCAL_NAME_CHANGED")) {
                    if (BluetoothDeviceNamePreferenceController.this.mPreference != null && BluetoothDeviceNamePreferenceController.this.mLocalAdapter != null && BluetoothDeviceNamePreferenceController.this.mLocalAdapter.isEnabled()) {
                        BluetoothDeviceNamePreferenceController.this.updatePreferenceState(BluetoothDeviceNamePreferenceController.this.mPreference);
                    }
                }
                else if (TextUtils.equals((CharSequence)action, (CharSequence)"android.bluetooth.adapter.action.STATE_CHANGED")) {
                    BluetoothDeviceNamePreferenceController.this.updatePreferenceState(BluetoothDeviceNamePreferenceController.this.mPreference);
                }
            }
        };
        this.mLocalManager = Utils.getLocalBtManager(context);
        if (this.mLocalManager == null) {
            Log.e("BluetoothNamePrefCtrl", "Bluetooth is not supported on this device");
            return;
        }
        this.mLocalAdapter = this.mLocalManager.getBluetoothAdapter();
    }
    
    public Preference createBluetoothDeviceNamePreference(final PreferenceScreen preferenceScreen, final int order) {
        (this.mPreference = new Preference(preferenceScreen.getContext())).setOrder(order);
        this.mPreference.setKey(this.getPreferenceKey());
        preferenceScreen.addPreference(this.mPreference);
        return this.mPreference;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
        super.displayPreference(preferenceScreen);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mLocalAdapter != null) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    protected String getDeviceName() {
        return this.mLocalAdapter.getName();
    }
    
    @Override
    public CharSequence getSummary() {
        final String deviceName = this.getDeviceName();
        if (TextUtils.isEmpty((CharSequence)deviceName)) {
            return super.getSummary();
        }
        return TextUtils.expandTemplate(this.mContext.getText(2131886742), new CharSequence[] { BidiFormatter.getInstance().unicodeWrap(deviceName) }).toString();
    }
    
    @Override
    public void onStart() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.adapter.action.LOCAL_NAME_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        this.mContext.registerReceiver(this.mReceiver, intentFilter);
    }
    
    @Override
    public void onStop() {
        this.mContext.unregisterReceiver(this.mReceiver);
    }
    
    protected void updatePreferenceState(final Preference preference) {
        preference.setSelectable(false);
        preference.setSummary(this.getSummary());
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updatePreferenceState(preference);
    }
}
