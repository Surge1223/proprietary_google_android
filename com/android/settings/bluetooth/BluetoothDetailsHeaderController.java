package com.android.settings.bluetooth;

import android.util.Pair;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.android.settingslib.bluetooth.Utils;
import android.support.v7.preference.Preference;
import android.app.Fragment;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.widget.EntityHeaderController;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;

public class BluetoothDetailsHeaderController extends BluetoothDetailsController
{
    private CachedBluetoothDeviceManager mDeviceManager;
    private EntityHeaderController mHeaderController;
    private LocalBluetoothManager mLocalManager;
    
    public BluetoothDetailsHeaderController(final Context context, final PreferenceFragment preferenceFragment, final CachedBluetoothDevice cachedBluetoothDevice, final Lifecycle lifecycle, final LocalBluetoothManager mLocalManager) {
        super(context, preferenceFragment, cachedBluetoothDevice, lifecycle);
        this.mLocalManager = mLocalManager;
        this.mDeviceManager = this.mLocalManager.getCachedDeviceManager();
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_device_header";
    }
    
    @Override
    protected void init(final PreferenceScreen preferenceScreen) {
        final LayoutPreference layoutPreference = (LayoutPreference)preferenceScreen.findPreference("bluetooth_device_header");
        this.mHeaderController = EntityHeaderController.newInstance(this.mFragment.getActivity(), this.mFragment, layoutPreference.findViewById(2131362112));
        preferenceScreen.addPreference(layoutPreference);
    }
    
    @Override
    protected void refresh() {
        this.setHeaderProperties();
        this.mHeaderController.done(this.mFragment.getActivity(), true);
    }
    
    protected void setHeaderProperties() {
        final Pair<Drawable, String> btClassDrawableWithDescription = Utils.getBtClassDrawableWithDescription(this.mContext, this.mCachedDevice, this.mContext.getResources().getFraction(2131296261, 1, 1));
        final String connectionSummary = this.mCachedDevice.getConnectionSummary();
        if (this.mCachedDevice.isHearingAidDevice()) {
            final String hearingAidPairDeviceSummary = this.mDeviceManager.getHearingAidPairDeviceSummary(this.mCachedDevice);
            final StringBuilder sb = new StringBuilder();
            sb.append("setHeaderProperties: HearingAid: summaryText=");
            sb.append(connectionSummary);
            sb.append(", pairDeviceSummary=");
            sb.append(hearingAidPairDeviceSummary);
            Log.d("BluetoothDetailsHeaderController", sb.toString());
            this.mHeaderController.setSecondSummary(hearingAidPairDeviceSummary);
        }
        this.mHeaderController.setLabel(this.mCachedDevice.getName());
        this.mHeaderController.setIcon((Drawable)btClassDrawableWithDescription.first);
        this.mHeaderController.setIconContentDescription((String)btClassDrawableWithDescription.second);
        this.mHeaderController.setSummary(connectionSummary);
    }
}
