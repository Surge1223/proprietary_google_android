package com.android.settings.bluetooth;

import android.content.Context;
import android.app.Dialog;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.widget.Button;
import android.text.Editable;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.text.TextUtils;
import android.widget.CompoundButton;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.CheckBox;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.widget.EditText;
import android.app.AlertDialog;
import android.app.AlertDialog$Builder;
import android.text.TextWatcher;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnShowListener;

public final class _$$Lambda$BluetoothPairingDialogFragment$ItV61WjNe_T4YaZN6BYGTBHLdZc implements DialogInterface$OnShowListener
{
    public final void onShow(final DialogInterface dialogInterface) {
        BluetoothPairingDialogFragment.lambda$createUserEntryDialog$0(this.f$0, dialogInterface);
    }
}
