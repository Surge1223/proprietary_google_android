package com.android.settings.bluetooth;

import android.os.Bundle;
import android.app.admin.DevicePolicyManager;
import android.os.UserManager;
import android.content.DialogInterface;
import com.android.internal.app.AlertController$AlertParams;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.content.Intent;
import android.util.Log;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public class RequestPermissionHelperActivity extends AlertActivity implements DialogInterface$OnClickListener
{
    private CharSequence mAppLabel;
    private LocalBluetoothAdapter mLocalAdapter;
    private int mRequest;
    private int mTimeout;
    
    public RequestPermissionHelperActivity() {
        this.mTimeout = -1;
    }
    
    private boolean parseIntent() {
        final Intent intent = this.getIntent();
        if (intent == null) {
            return false;
        }
        final String action = intent.getAction();
        if ("com.android.settings.bluetooth.ACTION_INTERNAL_REQUEST_BT_ON".equals(action)) {
            this.mRequest = 1;
            if (intent.hasExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION")) {
                this.mTimeout = intent.getIntExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 120);
            }
        }
        else {
            if (!"com.android.settings.bluetooth.ACTION_INTERNAL_REQUEST_BT_OFF".equals(action)) {
                return false;
            }
            this.mRequest = 3;
        }
        final LocalBluetoothManager localBtManager = Utils.getLocalBtManager((Context)this);
        if (localBtManager == null) {
            Log.e("RequestPermissionHelperActivity", "Error: there's a problem starting Bluetooth");
            return false;
        }
        this.mAppLabel = this.getIntent().getCharSequenceExtra("com.android.settings.bluetooth.extra.APP_LABEL");
        this.mLocalAdapter = localBtManager.getBluetoothAdapter();
        return true;
    }
    
    void createDialog() {
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        final int mRequest = this.mRequest;
        if (mRequest != 1) {
            if (mRequest == 3) {
                String mMessage;
                if (this.mAppLabel != null) {
                    mMessage = this.getString(2131886692, new Object[] { this.mAppLabel });
                }
                else {
                    mMessage = this.getString(2131886693);
                }
                mAlertParams.mMessage = mMessage;
            }
        }
        else if (this.mTimeout < 0) {
            String mMessage2;
            if (this.mAppLabel != null) {
                mMessage2 = this.getString(2131886696, new Object[] { this.mAppLabel });
            }
            else {
                mMessage2 = this.getString(2131886701);
            }
            mAlertParams.mMessage = mMessage2;
        }
        else if (this.mTimeout == 0) {
            String mMessage3;
            if (this.mAppLabel != null) {
                mMessage3 = this.getString(2131886699, new Object[] { this.mAppLabel });
            }
            else {
                mMessage3 = this.getString(2131886700);
            }
            mAlertParams.mMessage = mMessage3;
        }
        else {
            String mMessage4;
            if (this.mAppLabel != null) {
                mMessage4 = this.getString(2131886697, new Object[] { this.mAppLabel, this.mTimeout });
            }
            else {
                mMessage4 = this.getString(2131886698, new Object[] { this.mTimeout });
            }
            mAlertParams.mMessage = mMessage4;
        }
        mAlertParams.mPositiveButtonText = this.getString(2131886285);
        mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
        mAlertParams.mNegativeButtonText = this.getString(2131887370);
        this.setupAlert();
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        switch (this.mRequest) {
            case 3: {
                this.mLocalAdapter.disable();
                this.setResult(-1);
                break;
            }
            case 1:
            case 2: {
                if (((UserManager)this.getSystemService((Class)UserManager.class)).hasUserRestriction("no_bluetooth")) {
                    final Intent adminSupportIntent = ((DevicePolicyManager)this.getSystemService((Class)DevicePolicyManager.class)).createAdminSupportIntent("no_bluetooth");
                    if (adminSupportIntent != null) {
                        this.startActivity(adminSupportIntent);
                    }
                    break;
                }
                this.mLocalAdapter.enable();
                this.setResult(-1);
                break;
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setResult(0);
        if (!this.parseIntent()) {
            this.finish();
            return;
        }
        if (this.getResources().getBoolean(2131034115)) {
            this.onClick(null, -1);
            this.dismiss();
        }
        this.createDialog();
    }
}
