package com.android.settings.bluetooth;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import com.android.settings.widget.ActionButtonPreference;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$BluetoothDetailsButtonsController$AbsgPn9bfqFfvfi3BgeGPbSW3X0 implements View.OnClickListener
{
    public final void onClick(final View view) {
        BluetoothDetailsButtonsController.lambda$refresh$1(this.f$0, view);
    }
}
