package com.android.settings.bluetooth;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.bluetooth.BluetoothDeviceFilter;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import com.android.settingslib.widget.FooterPreference;
import com.android.settings.search.Indexable;

public class BluetoothPairingDetail extends DeviceListPreferenceFragment implements Indexable
{
    static final String KEY_AVAIL_DEVICES = "available_devices";
    static final String KEY_FOOTER_PREF = "footer_preference";
    AlwaysDiscoverable mAlwaysDiscoverable;
    BluetoothProgressCategory mAvailableDevicesCategory;
    FooterPreference mFooterPreference;
    private boolean mInitialScanStarted;
    
    public BluetoothPairingDetail() {
        super("no_config_bluetooth");
    }
    
    @Override
    void enableScanning() {
        if (!this.mInitialScanStarted) {
            if (this.mAvailableDevicesCategory != null) {
                this.removeAllDevices();
            }
            this.mLocalManager.getCachedDeviceManager().clearNonBondedDevices();
            this.mInitialScanStarted = true;
        }
        super.enableScanning();
    }
    
    @Override
    public String getDeviceListKey() {
        return "available_devices";
    }
    
    @Override
    public int getHelpResource() {
        return 2131887801;
    }
    
    @Override
    protected String getLogTag() {
        return "BluetoothPairingDetail";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1018;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082726;
    }
    
    @Override
    void initPreferencesFromPreferenceScreen() {
        this.mAvailableDevicesCategory = (BluetoothProgressCategory)this.findPreference("available_devices");
        (this.mFooterPreference = (FooterPreference)this.findPreference("footer_preference")).setSelectable(false);
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.mInitialScanStarted = false;
        this.mAlwaysDiscoverable = new AlwaysDiscoverable(this.getContext(), this.mLocalAdapter);
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(BluetoothDeviceRenamePreferenceController.class).setFragment(this);
    }
    
    @Override
    public void onBluetoothStateChanged(final int n) {
        super.onBluetoothStateChanged(n);
        this.updateContent(n);
    }
    
    @Override
    public void onDeviceBondStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        if (n == 12) {
            this.finish();
            return;
        }
        if (this.mSelectedDevice != null && cachedBluetoothDevice != null) {
            final BluetoothDevice device = cachedBluetoothDevice.getDevice();
            if (device != null && this.mSelectedDevice.equals((Object)device) && n == 10) {
                this.enableScanning();
            }
        }
    }
    
    @Override
    void onDevicePreferenceClick(final BluetoothDevicePreference bluetoothDevicePreference) {
        this.disableScanning();
        super.onDevicePreferenceClick(bluetoothDevicePreference);
    }
    
    @Override
    public void onScanningStateChanged(final boolean b) {
        super.onScanningStateChanged(b);
        this.mAvailableDevicesCategory.setProgress(b | this.mScanEnabled);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (this.mLocalManager == null) {
            Log.e("BluetoothPairingDetail", "Bluetooth is not supported on this device");
            return;
        }
        this.updateBluetooth();
        this.mAvailableDevicesCategory.setProgress(this.mLocalAdapter.isDiscovering());
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (this.mLocalManager == null) {
            Log.e("BluetoothPairingDetail", "Bluetooth is not supported on this device");
            return;
        }
        this.mAlwaysDiscoverable.stop();
        this.disableScanning();
    }
    
    void updateBluetooth() {
        if (this.mLocalAdapter.isEnabled()) {
            this.updateContent(this.mLocalAdapter.getBluetoothState());
        }
        else {
            this.mLocalAdapter.enable();
        }
    }
    
    void updateContent(final int n) {
        if (n != 10) {
            if (n == 12) {
                this.mDevicePreferenceMap.clear();
                this.mLocalAdapter.setBluetoothEnabled(true);
                this.addDeviceCategory(this.mAvailableDevicesCategory, 2131886845, BluetoothDeviceFilter.UNBONDED_DEVICE_FILTER, this.mInitialScanStarted);
                this.updateFooterPreference(this.mFooterPreference);
                this.mAlwaysDiscoverable.start();
                this.enableScanning();
            }
        }
        else {
            this.finish();
        }
    }
}
