package com.android.settings.bluetooth;

import java.util.Iterator;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.bluetooth.PanProfile;
import java.util.List;
import com.android.settingslib.bluetooth.MapProfile;
import com.android.settingslib.bluetooth.PbapServerProfile;
import android.bluetooth.BluetoothDevice;
import android.support.v14.preference.SwitchPreference;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import android.support.v7.preference.PreferenceCategory;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.A2dpProfile;
import android.support.v7.preference.Preference;

public final class _$$Lambda$BluetoothDetailsProfilesController$pv2kZi3KDLDrPBqbb1ECR74MeRo implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return BluetoothDetailsProfilesController.lambda$maybeAddHighQualityAudioPref$0(this.f$0, this.f$1, preference);
    }
}
