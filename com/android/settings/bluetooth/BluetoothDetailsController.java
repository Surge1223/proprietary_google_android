package com.android.settings.bluetooth;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class BluetoothDetailsController extends AbstractPreferenceController implements PreferenceControllerMixin, Callback, LifecycleObserver, OnPause, OnResume
{
    protected final CachedBluetoothDevice mCachedDevice;
    protected final Context mContext;
    protected final PreferenceFragment mFragment;
    
    public BluetoothDetailsController(final Context mContext, final PreferenceFragment mFragment, final CachedBluetoothDevice mCachedDevice, final Lifecycle lifecycle) {
        super(mContext);
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.mCachedDevice = mCachedDevice;
        lifecycle.addObserver(this);
    }
    
    @Override
    public final void displayPreference(final PreferenceScreen preferenceScreen) {
        this.init(preferenceScreen);
        super.displayPreference(preferenceScreen);
    }
    
    protected abstract void init(final PreferenceScreen p0);
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onDeviceAttributesChanged() {
        this.refresh();
    }
    
    @Override
    public void onPause() {
        this.mCachedDevice.unregisterCallback((CachedBluetoothDevice.Callback)this);
    }
    
    @Override
    public void onResume() {
        this.mCachedDevice.registerCallback((CachedBluetoothDevice.Callback)this);
        this.refresh();
    }
    
    protected abstract void refresh();
}
