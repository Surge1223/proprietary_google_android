package com.android.settings.bluetooth;

import android.widget.CompoundButton;
import android.text.Editable;
import android.util.Log;
import java.util.Locale;
import android.content.Context;
import android.content.Intent;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import android.bluetooth.BluetoothDevice;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.widget.CompoundButton$OnCheckedChangeListener;

public class BluetoothPairingController implements CompoundButton$OnCheckedChangeListener
{
    private LocalBluetoothManager mBluetoothManager;
    BluetoothDevice mDevice;
    private String mDeviceName;
    private int mPasskey;
    private String mPasskeyFormatted;
    private boolean mPbapAllowed;
    private LocalBluetoothProfile mPbapClientProfile;
    int mType;
    private String mUserInput;
    
    public BluetoothPairingController(final Intent intent, final Context context) {
        this.mBluetoothManager = Utils.getLocalBtManager(context);
        this.mDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        if (this.mBluetoothManager == null) {
            throw new IllegalStateException("Could not obtain LocalBluetoothManager");
        }
        if (this.mDevice != null) {
            this.mType = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", Integer.MIN_VALUE);
            this.mPasskey = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", Integer.MIN_VALUE);
            this.mDeviceName = this.mBluetoothManager.getCachedDeviceManager().getName(this.mDevice);
            this.mPbapClientProfile = this.mBluetoothManager.getProfileManager().getPbapClientProfile();
            this.mPasskeyFormatted = this.formatKey(this.mPasskey);
            return;
        }
        throw new IllegalStateException("Could not find BluetoothDevice");
    }
    
    private String formatKey(final int n) {
        final int mType = this.mType;
        if (mType != 2) {
            switch (mType) {
                default: {
                    return null;
                }
                case 5: {
                    return String.format("%04d", n);
                }
                case 4: {
                    break;
                }
            }
        }
        return String.format(Locale.US, "%06d", n);
    }
    
    private void onPair(final String s) {
        Log.d("BTPairingController", "Pairing dialog accepted");
        switch (this.mType) {
            default: {
                Log.e("BTPairingController", "Incorrect pairing type received");
                break;
            }
            case 6: {
                this.mDevice.setRemoteOutOfBandData();
                break;
            }
            case 4:
            case 5: {
                break;
            }
            case 2:
            case 3: {
                this.mDevice.setPairingConfirmation(true);
                break;
            }
            case 1: {
                this.mDevice.setPasskey(Integer.parseInt(s));
                break;
            }
            case 0:
            case 7: {
                final byte[] convertPinToBytes = BluetoothDevice.convertPinToBytes(s);
                if (convertPinToBytes == null) {
                    return;
                }
                this.mDevice.setPin(convertPinToBytes);
                break;
            }
        }
    }
    
    public boolean deviceEquals(final BluetoothDevice bluetoothDevice) {
        return this.mDevice == bluetoothDevice;
    }
    
    public boolean getContactSharingState() {
        switch (this.mDevice.getPhonebookAccessPermission()) {
            default: {
                return this.mDevice.getBluetoothClass().getDeviceClass() == 1032;
            }
            case 2: {
                return false;
            }
            case 1: {
                return true;
            }
        }
    }
    
    public int getDeviceMaxPasskeyLength() {
        final int mType = this.mType;
        if (mType != 7) {
            switch (mType) {
                default: {
                    return 0;
                }
                case 1: {
                    return 6;
                }
                case 0: {
                    break;
                }
            }
        }
        return 16;
    }
    
    public String getDeviceName() {
        return this.mDeviceName;
    }
    
    public int getDeviceVariantMessageHintId() {
        final int mType = this.mType;
        if (mType == 7) {
            return 2131886842;
        }
        switch (mType) {
            default: {
                return -1;
            }
            case 0:
            case 1: {
                return 2131886841;
            }
        }
    }
    
    public int getDeviceVariantMessageId() {
        final int mType = this.mType;
        if (mType != 7) {
            switch (mType) {
                default: {
                    return -1;
                }
                case 1: {
                    return 2131886774;
                }
                case 0: {
                    break;
                }
            }
        }
        return 2131886775;
    }
    
    public int getDialogType() {
        switch (this.mType) {
            default: {
                return -1;
            }
            case 4:
            case 5: {
                return 2;
            }
            case 2:
            case 3:
            case 6: {
                return 1;
            }
            case 0:
            case 1:
            case 7: {
                return 0;
            }
        }
    }
    
    public String getPairingContent() {
        if (this.hasPairingContent()) {
            return this.mPasskeyFormatted;
        }
        return null;
    }
    
    public boolean hasPairingContent() {
        final int mType = this.mType;
        if (mType != 2) {
            switch (mType) {
                default: {
                    return false;
                }
                case 4:
                case 5: {
                    break;
                }
            }
        }
        return true;
    }
    
    public boolean isDisplayPairingKeyVariant() {
        switch (this.mType) {
            default: {
                return false;
            }
            case 4:
            case 5:
            case 6: {
                return true;
            }
        }
    }
    
    public boolean isPasskeyValid(final Editable editable) {
        final int mType = this.mType;
        final boolean b = false;
        final boolean b2 = mType == 7;
        if (editable.length() < 16 || !b2) {
            boolean b3 = b;
            if (editable.length() <= 0) {
                return b3;
            }
            b3 = b;
            if (b2) {
                return b3;
            }
        }
        return true;
    }
    
    public boolean isProfileReady() {
        return this.mPbapClientProfile != null && this.mPbapClientProfile.isProfileReady();
    }
    
    protected void notifyDialogDisplayed() {
        if (this.mType == 4) {
            this.mDevice.setPairingConfirmation(true);
        }
        else if (this.mType == 5) {
            this.mDevice.setPin(BluetoothDevice.convertPinToBytes(this.mPasskeyFormatted));
        }
    }
    
    public void onCancel() {
        Log.d("BTPairingController", "Pairing dialog canceled");
        this.mDevice.cancelPairingUserInput();
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        if (b) {
            this.mPbapAllowed = true;
        }
        else {
            this.mPbapAllowed = false;
        }
    }
    
    public void onDialogNegativeClick(final BluetoothPairingDialogFragment bluetoothPairingDialogFragment) {
        this.mDevice.setPhonebookAccessPermission(2);
        this.onCancel();
    }
    
    public void onDialogPositiveClick(final BluetoothPairingDialogFragment bluetoothPairingDialogFragment) {
        if (this.mPbapAllowed) {
            this.mDevice.setPhonebookAccessPermission(1);
        }
        else {
            this.mDevice.setPhonebookAccessPermission(2);
        }
        if (this.getDialogType() == 0) {
            this.onPair(this.mUserInput);
        }
        else {
            this.onPair(null);
        }
    }
    
    public boolean pairingCodeIsAlphanumeric() {
        return this.mType != 1;
    }
    
    public void setContactSharingState() {
        if (this.mDevice.getPhonebookAccessPermission() != 1 && this.mDevice.getPhonebookAccessPermission() != 2) {
            if (this.mDevice.getBluetoothClass().getDeviceClass() == 1032) {
                this.onCheckedChanged(null, true);
            }
            else {
                this.onCheckedChanged(null, false);
            }
        }
    }
    
    protected void updateUserInput(final String mUserInput) {
        this.mUserInput = mUserInput;
    }
}
