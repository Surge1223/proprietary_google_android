package com.android.settings.bluetooth;

import android.app.AlertDialog$Builder;
import android.widget.TextView$BufferType;
import android.view.LayoutInflater;
import android.app.Dialog;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;
import android.os.Bundle;
import android.widget.EditText;
import com.android.settingslib.bluetooth.PanProfile;
import android.text.Html;
import android.content.DialogInterface;
import android.text.TextUtils;
import java.util.Iterator;
import android.util.Log;
import android.content.Context;
import com.android.settingslib.bluetooth.MapProfile;
import com.android.settingslib.bluetooth.PbapServerProfile;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import android.widget.TextView;
import android.view.ViewGroup;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.app.AlertDialog;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.view.View;
import android.widget.CheckBox;
import android.bluetooth.BluetoothDevice;
import com.android.settingslib.bluetooth.A2dpProfile;
import android.view.View.OnClickListener;

public final class _$$Lambda$DeviceProfilesSettings$qBNrFA8_Smm3qyHXDkezO_CS7tQ implements View.OnClickListener
{
    public final void onClick(final View view) {
        DeviceProfilesSettings.lambda$addPreferencesForProfiles$0(this.f$0, this.f$1, this.f$2, view);
    }
}
