package com.android.settings.bluetooth;

import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.ProgressCategory;

public class BluetoothProgressCategory extends ProgressCategory
{
    public BluetoothProgressCategory(final Context context) {
        super(context);
        this.init();
    }
    
    public BluetoothProgressCategory(final Context context, final AttributeSet set) {
        super(context, set);
        this.init();
    }
    
    public BluetoothProgressCategory(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init();
    }
    
    public BluetoothProgressCategory(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.init();
    }
    
    private void init() {
        this.setEmptyTextRes(2131886803);
    }
}
