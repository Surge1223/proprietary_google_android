package com.android.settings.bluetooth;

import android.text.BidiFormatter;
import android.support.v7.preference.PreferenceCategory;
import android.os.SystemProperties;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.util.Log;
import java.util.Iterator;
import android.bluetooth.BluetoothDevice;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import com.android.settingslib.bluetooth.BluetoothDeviceFilter;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import java.util.WeakHashMap;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.bluetooth.BluetoothCallback;
import com.android.settings.dashboard.RestrictedDashboardFragment;

public abstract class DeviceListPreferenceFragment extends RestrictedDashboardFragment implements BluetoothCallback
{
    PreferenceGroup mDeviceListGroup;
    final WeakHashMap<CachedBluetoothDevice, BluetoothDevicePreference> mDevicePreferenceMap;
    private BluetoothDeviceFilter.Filter mFilter;
    LocalBluetoothAdapter mLocalAdapter;
    LocalBluetoothManager mLocalManager;
    boolean mScanEnabled;
    BluetoothDevice mSelectedDevice;
    boolean mShowDevicesWithoutNames;
    
    DeviceListPreferenceFragment(final String s) {
        super(s);
        this.mDevicePreferenceMap = new WeakHashMap<CachedBluetoothDevice, BluetoothDevicePreference>();
        this.mFilter = BluetoothDeviceFilter.ALL_FILTER;
    }
    
    void addCachedDevices() {
        final Iterator<CachedBluetoothDevice> iterator = this.mLocalManager.getCachedDeviceManager().getCachedDevicesCopy().iterator();
        while (iterator.hasNext()) {
            this.onDeviceAdded(iterator.next());
        }
    }
    
    public void addDeviceCategory(final PreferenceGroup mDeviceListGroup, final int title, final BluetoothDeviceFilter.Filter filter, final boolean b) {
        this.cacheRemoveAllPrefs(mDeviceListGroup);
        mDeviceListGroup.setTitle(title);
        this.mDeviceListGroup = mDeviceListGroup;
        this.setFilter(filter);
        if (b) {
            this.addCachedDevices();
        }
        mDeviceListGroup.setEnabled(true);
        this.removeCachedPrefs(mDeviceListGroup);
    }
    
    void createDevicePreference(final CachedBluetoothDevice cachedBluetoothDevice) {
        if (this.mDeviceListGroup == null) {
            Log.w("DeviceListPreferenceFragment", "Trying to create a device preference before the list group/category exists!");
            return;
        }
        final String address = cachedBluetoothDevice.getDevice().getAddress();
        BluetoothDevicePreference bluetoothDevicePreference = (BluetoothDevicePreference)this.getCachedPreference(address);
        if (bluetoothDevicePreference == null) {
            bluetoothDevicePreference = new BluetoothDevicePreference(this.getPrefContext(), cachedBluetoothDevice, this.mShowDevicesWithoutNames);
            bluetoothDevicePreference.setKey(address);
            this.mDeviceListGroup.addPreference(bluetoothDevicePreference);
        }
        else {
            bluetoothDevicePreference.rebind();
        }
        this.initDevicePreference(bluetoothDevicePreference);
        this.mDevicePreferenceMap.put(cachedBluetoothDevice, bluetoothDevicePreference);
    }
    
    void disableScanning() {
        this.mLocalAdapter.stopScanning();
        this.mScanEnabled = false;
    }
    
    void enableScanning() {
        this.mLocalAdapter.startScanning(true);
        this.mScanEnabled = true;
    }
    
    public abstract String getDeviceListKey();
    
    void initDevicePreference(final BluetoothDevicePreference bluetoothDevicePreference) {
    }
    
    abstract void initPreferencesFromPreferenceScreen();
    
    @Override
    public void onActiveDeviceChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onAudioModeChanged() {
    }
    
    @Override
    public void onBluetoothStateChanged(final int n) {
    }
    
    @Override
    public void onConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mLocalManager = Utils.getLocalBtManager((Context)this.getActivity());
        if (this.mLocalManager == null) {
            Log.e("DeviceListPreferenceFragment", "Bluetooth is not supported on this device");
            return;
        }
        this.mLocalAdapter = this.mLocalManager.getBluetoothAdapter();
        this.mShowDevicesWithoutNames = SystemProperties.getBoolean("persist.bluetooth.showdeviceswithoutnames", false);
        this.initPreferencesFromPreferenceScreen();
        this.mDeviceListGroup = (PreferenceCategory)this.findPreference(this.getDeviceListKey());
    }
    
    @Override
    public void onDeviceAdded(final CachedBluetoothDevice cachedBluetoothDevice) {
        if (this.mDevicePreferenceMap.get(cachedBluetoothDevice) != null) {
            return;
        }
        if (this.mLocalAdapter.getBluetoothState() != 12) {
            return;
        }
        if (this.mFilter.matches(cachedBluetoothDevice.getDevice())) {
            this.createDevicePreference(cachedBluetoothDevice);
        }
    }
    
    @Override
    public void onDeviceDeleted(final CachedBluetoothDevice cachedBluetoothDevice) {
        final BluetoothDevicePreference bluetoothDevicePreference = this.mDevicePreferenceMap.remove(cachedBluetoothDevice);
        if (bluetoothDevicePreference != null) {
            this.mDeviceListGroup.removePreference(bluetoothDevicePreference);
        }
    }
    
    void onDevicePreferenceClick(final BluetoothDevicePreference bluetoothDevicePreference) {
        bluetoothDevicePreference.onClicked();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if ("bt_scan".equals(preference.getKey())) {
            this.mLocalAdapter.startScanning(true);
            return true;
        }
        if (preference instanceof BluetoothDevicePreference) {
            final BluetoothDevicePreference bluetoothDevicePreference = (BluetoothDevicePreference)preference;
            this.mSelectedDevice = bluetoothDevicePreference.getCachedDevice().getDevice();
            this.onDevicePreferenceClick(bluetoothDevicePreference);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onScanningStateChanged(final boolean b) {
        if (!b && this.mScanEnabled) {
            this.mLocalAdapter.startScanning(true);
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (this.mLocalManager != null && !this.isUiRestricted()) {
            this.mLocalManager.setForegroundActivity((Context)this.getActivity());
            this.mLocalManager.getEventManager().registerCallback(this);
        }
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (this.mLocalManager != null && !this.isUiRestricted()) {
            this.removeAllDevices();
            this.mLocalManager.setForegroundActivity(null);
            this.mLocalManager.getEventManager().unregisterCallback(this);
        }
    }
    
    void removeAllDevices() {
        this.mDevicePreferenceMap.clear();
        this.mDeviceListGroup.removeAll();
    }
    
    final void setFilter(final int n) {
        this.mFilter = BluetoothDeviceFilter.getFilter(n);
    }
    
    final void setFilter(final BluetoothDeviceFilter.Filter mFilter) {
        this.mFilter = mFilter;
    }
    
    void updateFooterPreference(final Preference preference) {
        preference.setTitle(this.getString(2131886777, new Object[] { BidiFormatter.getInstance().unicodeWrap(this.mLocalAdapter.getAddress()) }));
    }
}
