package com.android.settings.bluetooth;

import android.content.DialogInterface$OnShowListener;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.view.inputmethod.InputMethodManager;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView$OnEditorActionListener;
import com.android.settings.Utils;
import android.text.TextUtils;
import android.text.InputFilter;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.AlertDialog;
import android.text.TextWatcher;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

abstract class BluetoothNameDialogFragment extends InstrumentedDialogFragment implements TextWatcher
{
    private AlertDialog mAlertDialog;
    private boolean mDeviceNameEdited;
    private boolean mDeviceNameUpdated;
    EditText mDeviceNameView;
    private Button mOkButton;
    
    private View createDialogView(final String text) {
        final View inflate = ((LayoutInflater)this.getActivity().getSystemService("layout_inflater")).inflate(2131558540, (ViewGroup)null);
        (this.mDeviceNameView = (EditText)inflate.findViewById(2131362098)).setFilters(new InputFilter[] { new BluetoothLengthDeviceNameFilter() });
        this.mDeviceNameView.setText((CharSequence)text);
        if (!TextUtils.isEmpty((CharSequence)text)) {
            this.mDeviceNameView.setSelection(text.length());
        }
        this.mDeviceNameView.addTextChangedListener((TextWatcher)this);
        Utils.setEditTextCursorPosition(this.mDeviceNameView);
        this.mDeviceNameView.setOnEditorActionListener((TextView$OnEditorActionListener)new TextView$OnEditorActionListener() {
            public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
                if (n == 6) {
                    BluetoothNameDialogFragment.this.setDeviceName(textView.getText().toString());
                    BluetoothNameDialogFragment.this.mAlertDialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        return inflate;
    }
    
    public void afterTextChanged(final Editable editable) {
        if (this.mDeviceNameUpdated) {
            this.mDeviceNameUpdated = false;
            this.mOkButton.setEnabled(false);
        }
        else {
            boolean enabled = true;
            this.mDeviceNameEdited = true;
            if (this.mOkButton != null) {
                final Button mOkButton = this.mOkButton;
                if (editable.toString().trim().length() == 0) {
                    enabled = false;
                }
                mOkButton.setEnabled(enabled);
            }
        }
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    protected abstract String getDeviceName();
    
    protected abstract int getDialogTitle();
    
    public Dialog onCreateDialog(final Bundle bundle) {
        String s = this.getDeviceName();
        if (bundle != null) {
            s = bundle.getString("device_name", s);
            this.mDeviceNameEdited = bundle.getBoolean("device_name_edited", false);
        }
        (this.mAlertDialog = new AlertDialog$Builder((Context)this.getActivity()).setTitle(this.getDialogTitle()).setView(this.createDialogView(s)).setPositiveButton(2131886871, (DialogInterface$OnClickListener)new _$$Lambda$BluetoothNameDialogFragment$pGuotXbZkr5ej_7pdbB840goZcw(this)).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create()).setOnShowListener((DialogInterface$OnShowListener)new _$$Lambda$BluetoothNameDialogFragment$UwiP0mVIJKgl9XIciCSL5BjtkO4(this));
        return (Dialog)this.mAlertDialog;
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.mAlertDialog = null;
        this.mDeviceNameView = null;
        this.mOkButton = null;
    }
    
    public void onResume() {
        super.onResume();
        if (this.mOkButton == null) {
            (this.mOkButton = this.mAlertDialog.getButton(-1)).setEnabled(this.mDeviceNameEdited);
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putString("device_name", this.mDeviceNameView.getText().toString());
        bundle.putBoolean("device_name_edited", this.mDeviceNameEdited);
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    protected abstract void setDeviceName(final String p0);
    
    void updateDeviceName() {
        final String deviceName = this.getDeviceName();
        if (deviceName != null) {
            this.mDeviceNameUpdated = true;
            this.mDeviceNameEdited = false;
            this.mDeviceNameView.setText((CharSequence)deviceName);
        }
    }
}
