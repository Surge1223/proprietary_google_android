package com.android.settings.bluetooth;

import android.util.Pair;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.content.Context;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.app.Fragment;

public class BluetoothDeviceRenamePreferenceController extends BluetoothDeviceNamePreferenceController
{
    private Fragment mFragment;
    private MetricsFeatureProvider mMetricsFeatureProvider;
    
    BluetoothDeviceRenamePreferenceController(final Context context, final LocalBluetoothAdapter localBluetoothAdapter, final String s) {
        super(context, localBluetoothAdapter, s);
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    public BluetoothDeviceRenamePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    @Override
    public CharSequence getSummary() {
        return this.getDeviceName();
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)preference.getKey()) && this.mFragment != null) {
            this.mMetricsFeatureProvider.action(this.mContext, 161, (Pair<Integer, Object>[])new Pair[0]);
            LocalDeviceNameDialogFragment.newInstance().show(this.mFragment.getFragmentManager(), "LocalAdapterName");
            return true;
        }
        return false;
    }
    
    public void setFragment(final Fragment mFragment) {
        this.mFragment = mFragment;
    }
    
    @Override
    protected void updatePreferenceState(final Preference preference) {
        preference.setSummary(this.getSummary());
        preference.setVisible(this.mLocalAdapter != null && this.mLocalAdapter.isEnabled());
    }
}
