package com.android.settings.bluetooth;

import android.content.Context;
import android.app.Dialog;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.widget.Button;
import android.text.Editable;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.content.DialogInterface$OnShowListener;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.widget.EditText;
import android.app.AlertDialog;
import android.app.AlertDialog$Builder;
import android.text.TextWatcher;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;

public final class _$$Lambda$BluetoothPairingDialogFragment$r7iz4I0mbAZSn1y_rbFsqcyiwC0 implements CompoundButton$OnCheckedChangeListener
{
    public final void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        BluetoothPairingDialogFragment.lambda$createPinEntryView$1(this.f$0, compoundButton, b);
    }
}
