package com.android.settings.bluetooth;

import android.bluetooth.BluetoothAdapter;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.support.v4.graphics.drawable.IconCompat;
import com.android.settingslib.Utils;
import androidx.slice.Slice;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.connecteddevice.BluetoothDashboardFragment;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri.Builder;
import android.content.IntentFilter;
import android.net.Uri;
import androidx.slice.builders.ListBuilder;
import androidx.slice.builders.SliceAction;
import android.support.v4.util.Consumer;

public final class _$$Lambda$BluetoothSliceBuilder$t1meuX4HmFYfZCMXndFcRlc9eII implements Consumer
{
    @Override
    public final void accept(final Object o) {
        BluetoothSliceBuilder.lambda$getSlice$0(this.f$0, this.f$1, this.f$2, (ListBuilder.RowBuilder)o);
    }
}
