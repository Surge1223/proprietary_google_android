package com.android.settings.bluetooth;

import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settings.dashboard.DashboardFragment;
import android.content.Context;
import android.media.AudioManager;
import android.support.v7.preference.Preference;

public class AvailableMediaBluetoothDeviceUpdater extends BluetoothDeviceUpdater implements OnPreferenceClickListener
{
    private final AudioManager mAudioManager;
    
    public AvailableMediaBluetoothDeviceUpdater(final Context context, final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback) {
        super(context, dashboardFragment, devicePreferenceCallback);
        this.mAudioManager = (AudioManager)context.getSystemService("audio");
    }
    
    AvailableMediaBluetoothDeviceUpdater(final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback, final LocalBluetoothManager localBluetoothManager) {
        super(dashboardFragment, devicePreferenceCallback, localBluetoothManager);
        this.mAudioManager = (AudioManager)dashboardFragment.getContext().getSystemService("audio");
    }
    
    @Override
    public boolean isFilterMatched(final CachedBluetoothDevice cachedBluetoothDevice) {
        final int mode = this.mAudioManager.getMode();
        int n = 2;
        if (mode == 1 || mode == 2 || mode == 3) {
            n = 1;
        }
        boolean b = false;
        if (this.isDeviceConnected(cachedBluetoothDevice)) {
            if (cachedBluetoothDevice.isConnectedHearingAidDevice()) {
                return true;
            }
            switch (n) {
                default: {
                    b = b;
                    break;
                }
                case 2: {
                    b = cachedBluetoothDevice.isA2dpDevice();
                    break;
                }
                case 1: {
                    b = cachedBluetoothDevice.isHfpDevice();
                    break;
                }
            }
        }
        return b;
    }
    
    @Override
    public void onAudioModeChanged() {
        this.forceUpdate();
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        return ((BluetoothDevicePreference)preference).getBluetoothDevice().setActive();
    }
    
    @Override
    public void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
        if (n == 2) {
            if (this.isFilterMatched(cachedBluetoothDevice)) {
                this.addPreference(cachedBluetoothDevice);
            }
            else {
                this.removePreference(cachedBluetoothDevice);
            }
        }
        else if (n == 0) {
            this.removePreference(cachedBluetoothDevice);
        }
    }
}
