package com.android.settings.bluetooth;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settings.dashboard.RestrictedDashboardFragment;

public class BluetoothDeviceDetailsFragment extends RestrictedDashboardFragment
{
    static int EDIT_DEVICE_NAME_ITEM_ID;
    static TestDataFactory sTestDataFactory;
    private CachedBluetoothDevice mCachedDevice;
    private String mDeviceAddress;
    private LocalBluetoothManager mManager;
    
    static {
        BluetoothDeviceDetailsFragment.EDIT_DEVICE_NAME_ITEM_ID = 1;
    }
    
    public BluetoothDeviceDetailsFragment() {
        super("no_config_bluetooth");
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<BluetoothDetailsHeaderController> list = (ArrayList<BluetoothDetailsHeaderController>)new ArrayList<AbstractPreferenceController>();
        if (this.mCachedDevice != null) {
            final Lifecycle lifecycle = this.getLifecycle();
            list.add(new BluetoothDetailsHeaderController(context, this, this.mCachedDevice, lifecycle, this.mManager));
            list.add((BluetoothDetailsHeaderController)new BluetoothDetailsButtonsController(context, this, this.mCachedDevice, lifecycle));
            list.add((BluetoothDetailsHeaderController)new BluetoothDetailsProfilesController(context, this, this.mManager, this.mCachedDevice, lifecycle));
            list.add((BluetoothDetailsHeaderController)new BluetoothDetailsMacAddressController(context, this, this.mCachedDevice, lifecycle));
        }
        return (List<AbstractPreferenceController>)list;
    }
    
    CachedBluetoothDevice getCachedDevice(final String s) {
        if (BluetoothDeviceDetailsFragment.sTestDataFactory != null) {
            return BluetoothDeviceDetailsFragment.sTestDataFactory.getDevice(s);
        }
        return this.mManager.getCachedDeviceManager().findDevice(this.mManager.getBluetoothAdapter().getRemoteDevice(s));
    }
    
    LocalBluetoothManager getLocalBluetoothManager(final Context context) {
        if (BluetoothDeviceDetailsFragment.sTestDataFactory != null) {
            return BluetoothDeviceDetailsFragment.sTestDataFactory.getManager(context);
        }
        return Utils.getLocalBtManager(context);
    }
    
    @Override
    protected String getLogTag() {
        return "BTDeviceDetailsFrg";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1009;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082725;
    }
    
    @Override
    public void onAttach(final Context context) {
        this.mDeviceAddress = this.getArguments().getString("device_address");
        this.mManager = this.getLocalBluetoothManager(context);
        this.mCachedDevice = this.getCachedDevice(this.mDeviceAddress);
        super.onAttach(context);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final MenuItem add = menu.add(0, BluetoothDeviceDetailsFragment.EDIT_DEVICE_NAME_ITEM_ID, 0, 2131886871);
        add.setIcon(2131231069);
        add.setShowAsAction(2);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == BluetoothDeviceDetailsFragment.EDIT_DEVICE_NAME_ITEM_ID) {
            RemoteDeviceNameDialogFragment.newInstance(this.mCachedDevice).show(this.getFragmentManager(), "RemoteDeviceName");
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
    
    interface TestDataFactory
    {
        CachedBluetoothDevice getDevice(final String p0);
        
        LocalBluetoothManager getManager(final Context p0);
    }
}
