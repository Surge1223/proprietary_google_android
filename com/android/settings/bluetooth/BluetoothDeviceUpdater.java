package com.android.settings.bluetooth;

import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import java.util.Iterator;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import java.util.HashMap;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.bluetooth.BluetoothDevice;
import java.util.Map;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.widget.GearPreference;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import com.android.settingslib.bluetooth.BluetoothCallback;

public abstract class BluetoothDeviceUpdater implements BluetoothCallback, ServiceListener
{
    protected final DevicePreferenceCallback mDevicePreferenceCallback;
    final GearPreference.OnGearClickListener mDeviceProfilesListener;
    protected DashboardFragment mFragment;
    protected final LocalBluetoothManager mLocalManager;
    protected Context mPrefContext;
    protected final Map<BluetoothDevice, Preference> mPreferenceMap;
    private final boolean mShowDeviceWithoutNames;
    
    public BluetoothDeviceUpdater(final Context context, final DashboardFragment dashboardFragment, final DevicePreferenceCallback devicePreferenceCallback) {
        this(dashboardFragment, devicePreferenceCallback, Utils.getLocalBtManager(context));
    }
    
    BluetoothDeviceUpdater(final DashboardFragment mFragment, final DevicePreferenceCallback mDevicePreferenceCallback, final LocalBluetoothManager mLocalManager) {
        this.mDeviceProfilesListener = new _$$Lambda$BluetoothDeviceUpdater$9cHgqnTeqRHSfH6f9TvykmwcB28(this);
        this.mFragment = mFragment;
        this.mDevicePreferenceCallback = mDevicePreferenceCallback;
        this.mShowDeviceWithoutNames = SystemProperties.getBoolean("persist.bluetooth.showdeviceswithoutnames", false);
        this.mPreferenceMap = new HashMap<BluetoothDevice, Preference>();
        this.mLocalManager = mLocalManager;
    }
    
    protected void addPreference(final CachedBluetoothDevice cachedBluetoothDevice) {
        final BluetoothDevice device = cachedBluetoothDevice.getDevice();
        if (!this.mPreferenceMap.containsKey(device)) {
            final BluetoothDevicePreference bluetoothDevicePreference = new BluetoothDevicePreference(this.mPrefContext, cachedBluetoothDevice, this.mShowDeviceWithoutNames);
            bluetoothDevicePreference.setOnGearClickListener(this.mDeviceProfilesListener);
            if (this instanceof Preference.OnPreferenceClickListener) {
                bluetoothDevicePreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
            }
            this.mPreferenceMap.put(device, bluetoothDevicePreference);
            this.mDevicePreferenceCallback.onDeviceAdded(bluetoothDevicePreference);
        }
    }
    
    public void forceUpdate() {
        final Iterator<CachedBluetoothDevice> iterator = this.mLocalManager.getCachedDeviceManager().getCachedDevicesCopy().iterator();
        while (iterator.hasNext()) {
            this.update(iterator.next());
        }
    }
    
    public boolean isDeviceConnected(final CachedBluetoothDevice cachedBluetoothDevice) {
        final boolean b = false;
        if (cachedBluetoothDevice == null) {
            return false;
        }
        final BluetoothDevice device = cachedBluetoothDevice.getDevice();
        boolean b2 = b;
        if (device.getBondState() == 12) {
            b2 = b;
            if (device.isConnected()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public abstract boolean isFilterMatched(final CachedBluetoothDevice p0);
    
    protected void launchDeviceDetails(final Preference preference) {
        final CachedBluetoothDevice bluetoothDevice = ((BluetoothDevicePreference)preference).getBluetoothDevice();
        if (bluetoothDevice == null) {
            return;
        }
        final Bundle arguments = new Bundle();
        arguments.putString("device_address", bluetoothDevice.getDevice().getAddress());
        new SubSettingLauncher(this.mFragment.getContext()).setDestination(BluetoothDeviceDetailsFragment.class.getName()).setArguments(arguments).setTitle(2131887387).setSourceMetricsCategory(this.mFragment.getMetricsCategory()).launch();
    }
    
    @Override
    public void onActiveDeviceChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onAudioModeChanged() {
    }
    
    @Override
    public void onBluetoothStateChanged(final int n) {
        this.forceUpdate();
    }
    
    @Override
    public void onConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
    }
    
    @Override
    public void onDeviceAdded(final CachedBluetoothDevice cachedBluetoothDevice) {
        this.update(cachedBluetoothDevice);
    }
    
    @Override
    public void onDeviceBondStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        this.update(cachedBluetoothDevice);
    }
    
    @Override
    public void onDeviceDeleted(final CachedBluetoothDevice cachedBluetoothDevice) {
        this.removePreference(cachedBluetoothDevice);
    }
    
    @Override
    public void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
    }
    
    @Override
    public void onScanningStateChanged(final boolean b) {
    }
    
    @Override
    public void onServiceConnected() {
        this.forceUpdate();
    }
    
    @Override
    public void onServiceDisconnected() {
    }
    
    public void registerCallback() {
        this.mLocalManager.setForegroundActivity(this.mFragment.getContext());
        this.mLocalManager.getEventManager().registerCallback(this);
        this.mLocalManager.getProfileManager().addServiceListener((LocalBluetoothProfileManager.ServiceListener)this);
        this.forceUpdate();
    }
    
    protected void removePreference(final CachedBluetoothDevice cachedBluetoothDevice) {
        final BluetoothDevice device = cachedBluetoothDevice.getDevice();
        if (this.mPreferenceMap.containsKey(device)) {
            this.mDevicePreferenceCallback.onDeviceRemoved(this.mPreferenceMap.get(device));
            this.mPreferenceMap.remove(device);
        }
    }
    
    public void setPrefContext(final Context mPrefContext) {
        this.mPrefContext = mPrefContext;
    }
    
    public void unregisterCallback() {
        this.mLocalManager.setForegroundActivity(null);
        this.mLocalManager.getEventManager().unregisterCallback(this);
        this.mLocalManager.getProfileManager().removeServiceListener((LocalBluetoothProfileManager.ServiceListener)this);
    }
    
    protected void update(final CachedBluetoothDevice cachedBluetoothDevice) {
        if (this.isFilterMatched(cachedBluetoothDevice)) {
            this.addPreference(cachedBluetoothDevice);
        }
        else {
            this.removePreference(cachedBluetoothDevice);
        }
    }
}
