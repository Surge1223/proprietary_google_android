package com.android.settings.bluetooth;

import android.graphics.drawable.Drawable;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.Preference;
import android.text.Html;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.text.TextUtils;
import android.util.TypedValue;
import android.util.AttributeSet;
import android.content.Context;
import android.os.UserManager;
import android.content.res.Resources;
import android.app.AlertDialog;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settings.widget.GearPreference;

public final class BluetoothDevicePreference extends GearPreference implements Callback
{
    private static int sDimAlpha;
    private String contentDescription;
    private final CachedBluetoothDevice mCachedDevice;
    private AlertDialog mDisconnectDialog;
    private boolean mHideSecondTarget;
    Resources mResources;
    private final boolean mShowDevicesWithoutNames;
    private final UserManager mUserManager;
    
    static {
        BluetoothDevicePreference.sDimAlpha = Integer.MIN_VALUE;
    }
    
    public BluetoothDevicePreference(final Context context, final CachedBluetoothDevice mCachedDevice, final boolean mShowDevicesWithoutNames) {
        super(context, null);
        this.contentDescription = null;
        this.mHideSecondTarget = false;
        this.mResources = this.getContext().getResources();
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mShowDevicesWithoutNames = mShowDevicesWithoutNames;
        if (BluetoothDevicePreference.sDimAlpha == Integer.MIN_VALUE) {
            final TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(16842803, typedValue, true);
            BluetoothDevicePreference.sDimAlpha = (int)(typedValue.getFloat() * 255.0f);
        }
        (this.mCachedDevice = mCachedDevice).registerCallback((CachedBluetoothDevice.Callback)this);
        this.onDeviceAttributesChanged();
    }
    
    private void askDisconnect() {
        final Context context = this.getContext();
        String s;
        if (TextUtils.isEmpty((CharSequence)(s = this.mCachedDevice.getName()))) {
            s = context.getString(2131886726);
        }
        this.mDisconnectDialog = Utils.showDisconnectDialog(context, this.mDisconnectDialog, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                BluetoothDevicePreference.this.mCachedDevice.disconnect();
            }
        }, context.getString(2131886761), (CharSequence)Html.fromHtml(context.getString(2131886755, new Object[] { s })));
    }
    
    private void pair() {
        if (!this.mCachedDevice.startPairing()) {
            Utils.showError(this.getContext(), this.mCachedDevice.getName(), R.string.bluetooth_pairing_error_message);
        }
    }
    
    public int compareTo(final Preference preference) {
        if (!(preference instanceof BluetoothDevicePreference)) {
            return super.compareTo(preference);
        }
        return this.mCachedDevice.compareTo(((BluetoothDevicePreference)preference).mCachedDevice);
    }
    
    public boolean equals(final Object o) {
        return o != null && o instanceof BluetoothDevicePreference && this.mCachedDevice.equals(((BluetoothDevicePreference)o).mCachedDevice);
    }
    
    public CachedBluetoothDevice getBluetoothDevice() {
        return this.mCachedDevice;
    }
    
    CachedBluetoothDevice getCachedDevice() {
        return this.mCachedDevice;
    }
    
    @Override
    protected int getSecondTargetResId() {
        return 2131558679;
    }
    
    public int hashCode() {
        return this.mCachedDevice.hashCode();
    }
    
    public void hideSecondTarget(final boolean mHideSecondTarget) {
        this.mHideSecondTarget = mHideSecondTarget;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        if (this.findPreferenceInHierarchy("bt_checkbox") != null) {
            this.setDependency("bt_checkbox");
        }
        if (this.mCachedDevice.getBondState() == 12) {
            final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131362594);
            if (imageView != null) {
                imageView.setOnClickListener((View.OnClickListener)this);
            }
        }
        final ImageView imageView2 = (ImageView)preferenceViewHolder.findViewById(16908294);
        if (imageView2 != null) {
            imageView2.setContentDescription((CharSequence)this.contentDescription);
        }
        super.onBindViewHolder(preferenceViewHolder);
    }
    
    void onClicked() {
        final Context context = this.getContext();
        final int bondState = this.mCachedDevice.getBondState();
        final MetricsFeatureProvider metricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        if (this.mCachedDevice.isConnected()) {
            metricsFeatureProvider.action(context, 868, (Pair<Integer, Object>[])new Pair[0]);
            this.askDisconnect();
        }
        else if (bondState == 12) {
            metricsFeatureProvider.action(context, 867, (Pair<Integer, Object>[])new Pair[0]);
            this.mCachedDevice.connect(true);
        }
        else if (bondState == 10) {
            metricsFeatureProvider.action(context, 866, (Pair<Integer, Object>[])new Pair[0]);
            if (!this.mCachedDevice.hasHumanReadableName()) {
                metricsFeatureProvider.action(context, 1096, (Pair<Integer, Object>[])new Pair[0]);
            }
            this.pair();
        }
    }
    
    @Override
    public void onDeviceAttributesChanged() {
        this.setTitle(this.mCachedDevice.getName());
        this.setSummary(this.mCachedDevice.getConnectionSummary());
        final Pair<Drawable, String> btClassDrawableWithDescription = com.android.settingslib.bluetooth.Utils.getBtClassDrawableWithDescription(this.getContext(), this.mCachedDevice);
        if (btClassDrawableWithDescription.first != null) {
            this.setIcon((Drawable)btClassDrawableWithDescription.first);
            this.contentDescription = (String)btClassDrawableWithDescription.second;
        }
        final boolean busy = this.mCachedDevice.isBusy();
        final boolean b = true;
        this.setEnabled(busy ^ true);
        boolean visible = b;
        if (!this.mShowDevicesWithoutNames) {
            visible = (this.mCachedDevice.hasHumanReadableName() && b);
        }
        this.setVisible(visible);
        this.notifyHierarchyChanged();
    }
    
    protected void onPrepareForRemoval() {
        super.onPrepareForRemoval();
        this.mCachedDevice.unregisterCallback((CachedBluetoothDevice.Callback)this);
        if (this.mDisconnectDialog != null) {
            this.mDisconnectDialog.dismiss();
            this.mDisconnectDialog = null;
        }
    }
    
    void rebind() {
        this.notifyChanged();
    }
    
    @Override
    protected boolean shouldHideSecondTarget() {
        return this.mCachedDevice == null || this.mCachedDevice.getBondState() != 12 || this.mUserManager.hasUserRestriction("no_config_bluetooth") || this.mHideSecondTarget;
    }
}
