package com.android.settings.bluetooth;

import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import java.util.Iterator;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import java.util.HashMap;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.bluetooth.BluetoothDevice;
import java.util.Map;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import com.android.settingslib.bluetooth.BluetoothCallback;
import com.android.settings.widget.GearPreference;

public final class _$$Lambda$BluetoothDeviceUpdater$9cHgqnTeqRHSfH6f9TvykmwcB28 implements OnGearClickListener
{
    @Override
    public final void onGearClick(final GearPreference gearPreference) {
        this.f$0.launchDeviceDetails(gearPreference);
    }
}
