package com.android.settings.bluetooth;

import android.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ForgetDeviceDialogFragment extends InstrumentedDialogFragment
{
    private CachedBluetoothDevice mDevice;
    
    public static ForgetDeviceDialogFragment newInstance(final String s) {
        final Bundle arguments = new Bundle(1);
        arguments.putString("device_address", s);
        final ForgetDeviceDialogFragment forgetDeviceDialogFragment = new ForgetDeviceDialogFragment();
        forgetDeviceDialogFragment.setArguments(arguments);
        return forgetDeviceDialogFragment;
    }
    
    @VisibleForTesting
    CachedBluetoothDevice getDevice(final Context context) {
        final String string = this.getArguments().getString("device_address");
        final LocalBluetoothManager localBtManager = Utils.getLocalBtManager(context);
        return localBtManager.getCachedDeviceManager().findDevice(localBtManager.getBluetoothAdapter().getRemoteDevice(string));
    }
    
    @Override
    public int getMetricsCategory() {
        return 1031;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final _$$Lambda$ForgetDeviceDialogFragment$EDf2UTKPcHIZGnJUVoyf7QwuxfU $$Lambda$ForgetDeviceDialogFragment$EDf2UTKPcHIZGnJUVoyf7QwuxfU = new _$$Lambda$ForgetDeviceDialogFragment$EDf2UTKPcHIZGnJUVoyf7QwuxfU(this);
        final Context context = this.getContext();
        this.mDevice = this.getDevice(context);
        final AlertDialog create = new AlertDialog$Builder(context).setPositiveButton(2131886915, (DialogInterface$OnClickListener)$$Lambda$ForgetDeviceDialogFragment$EDf2UTKPcHIZGnJUVoyf7QwuxfU).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
        create.setTitle(2131886916);
        create.setMessage((CharSequence)context.getString(2131886914, new Object[] { this.mDevice.getName() }));
        return (Dialog)create;
    }
}
