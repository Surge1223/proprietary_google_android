package com.android.settings.bluetooth;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import android.support.v14.preference.PreferenceFragment;
import android.content.Context;
import com.android.settingslib.widget.FooterPreferenceMixin;
import com.android.settingslib.widget.FooterPreference;

public class BluetoothDetailsMacAddressController extends BluetoothDetailsController
{
    FooterPreference mFooterPreference;
    FooterPreferenceMixin mFooterPreferenceMixin;
    
    public BluetoothDetailsMacAddressController(final Context context, final PreferenceFragment preferenceFragment, final CachedBluetoothDevice cachedBluetoothDevice, final Lifecycle lifecycle) {
        super(context, preferenceFragment, cachedBluetoothDevice, lifecycle);
        this.mFooterPreferenceMixin = new FooterPreferenceMixin(preferenceFragment, lifecycle);
    }
    
    @Override
    public String getPreferenceKey() {
        if (this.mFooterPreference == null) {
            return null;
        }
        return this.mFooterPreference.getKey();
    }
    
    @Override
    protected void init(final PreferenceScreen preferenceScreen) {
        (this.mFooterPreference = this.mFooterPreferenceMixin.createFooterPreference()).setTitle(this.mContext.getString(2131886740, new Object[] { this.mCachedDevice.getAddress() }));
    }
    
    @Override
    protected void refresh() {
    }
}
