package com.android.settings.bluetooth;

import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import android.text.TextUtils;
import android.util.Log;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.content.Context;

final class LocalBluetoothPreferences
{
    private static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences("bluetooth_settings", 0);
    }
    
    static void persistDiscoverableEndTimestamp(final Context context, final long n) {
        final SharedPreferences$Editor edit = getSharedPreferences(context).edit();
        edit.putLong("discoverable_end_timestamp", n);
        edit.apply();
    }
    
    static void persistSelectedDeviceInPicker(final Context context, final String s) {
        final SharedPreferences$Editor edit = getSharedPreferences(context).edit();
        edit.putString("last_selected_device", s);
        edit.putLong("last_selected_device_time", System.currentTimeMillis());
        edit.apply();
    }
    
    static boolean shouldShowDialogInForeground(final Context context, final String s, final String s2) {
        final LocalBluetoothManager localBtManager = Utils.getLocalBtManager(context);
        if (localBtManager == null) {
            Log.v("LocalBluetoothPreferences", "manager == null - do not show dialog.");
            return false;
        }
        if (localBtManager.isForegroundActivity()) {
            return true;
        }
        if ((context.getResources().getConfiguration().uiMode & 0x5) == 0x5) {
            Log.v("LocalBluetoothPreferences", "in appliance mode - do not show dialog.");
            return false;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        final SharedPreferences sharedPreferences = getSharedPreferences(context);
        if (sharedPreferences.getLong("discoverable_end_timestamp", 0L) + 60000L > currentTimeMillis) {
            return true;
        }
        final LocalBluetoothAdapter bluetoothAdapter = localBtManager.getBluetoothAdapter();
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                return true;
            }
            if (bluetoothAdapter.getDiscoveryEndMillis() + 60000L > currentTimeMillis) {
                return true;
            }
        }
        if (s != null && s.equals(sharedPreferences.getString("last_selected_device", (String)null)) && 60000L + sharedPreferences.getLong("last_selected_device_time", 0L) > currentTimeMillis) {
            return true;
        }
        if (!TextUtils.isEmpty((CharSequence)s2) && s2.equals(context.getString(17039708))) {
            Log.v("LocalBluetoothPreferences", "showing dialog for packaged keyboard");
            return true;
        }
        Log.v("LocalBluetoothPreferences", "Found no reason to show the dialog - do not show dialog.");
        return false;
    }
}
