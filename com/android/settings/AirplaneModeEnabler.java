package com.android.settings;

import android.os.SystemProperties;
import com.android.settingslib.WirelessUtils;
import android.os.UserHandle;
import android.content.Intent;
import android.provider.Settings;
import android.os.Message;
import android.os.Looper;
import com.android.internal.telephony.PhoneStateIntentReceiver;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.os.Handler;
import android.content.Context;
import android.database.ContentObserver;

public class AirplaneModeEnabler
{
    private ContentObserver mAirplaneModeObserver;
    private final Context mContext;
    private Handler mHandler;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private OnAirplaneModeChangedListener mOnAirplaneModeChangedListener;
    private PhoneStateIntentReceiver mPhoneStateReceiver;
    
    public AirplaneModeEnabler(final Context mContext, final MetricsFeatureProvider mMetricsFeatureProvider, final OnAirplaneModeChangedListener mOnAirplaneModeChangedListener) {
        this.mHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(final Message message) {
                if (message.what == 3) {
                    AirplaneModeEnabler.this.onAirplaneModeChanged();
                }
            }
        };
        this.mAirplaneModeObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
            public void onChange(final boolean b) {
                AirplaneModeEnabler.this.onAirplaneModeChanged();
            }
        };
        this.mContext = mContext;
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        this.mOnAirplaneModeChangedListener = mOnAirplaneModeChangedListener;
        (this.mPhoneStateReceiver = new PhoneStateIntentReceiver(this.mContext, this.mHandler)).notifyServiceState(3);
    }
    
    private void onAirplaneModeChanged() {
        if (this.mOnAirplaneModeChangedListener != null) {
            this.mOnAirplaneModeChangedListener.onAirplaneModeChanged(this.isAirplaneModeOn());
        }
    }
    
    private void setAirplaneModeOn(final boolean b) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "airplane_mode_on", (int)(b ? 1 : 0));
        if (this.mOnAirplaneModeChangedListener != null) {
            this.mOnAirplaneModeChangedListener.onAirplaneModeChanged(b);
        }
        final Intent intent = new Intent("android.intent.action.AIRPLANE_MODE");
        intent.putExtra("state", b);
        this.mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
    }
    
    public boolean isAirplaneModeOn() {
        return WirelessUtils.isAirplaneModeOn(this.mContext);
    }
    
    public void pause() {
        this.mPhoneStateReceiver.unregisterIntent();
        this.mContext.getContentResolver().unregisterContentObserver(this.mAirplaneModeObserver);
    }
    
    public void resume() {
        this.mPhoneStateReceiver.registerIntent();
        this.mContext.getContentResolver().registerContentObserver(Settings.Global.getUriFor("airplane_mode_on"), true, this.mAirplaneModeObserver);
    }
    
    public void setAirplaneMode(final boolean airplaneModeOn) {
        if (!Boolean.parseBoolean(SystemProperties.get("ril.cdma.inecmmode"))) {
            this.mMetricsFeatureProvider.action(this.mContext, 177, airplaneModeOn);
            this.setAirplaneModeOn(airplaneModeOn);
        }
    }
    
    public void setAirplaneModeInECM(final boolean b, final boolean airplaneModeOn) {
        if (b) {
            this.setAirplaneModeOn(airplaneModeOn);
        }
        else {
            this.onAirplaneModeChanged();
        }
    }
    
    public interface OnAirplaneModeChangedListener
    {
        void onAirplaneModeChanged(final boolean p0);
    }
}
