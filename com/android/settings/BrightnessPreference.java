package com.android.settings;

import android.os.UserHandle;
import android.content.Intent;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class BrightnessPreference extends Preference
{
    public BrightnessPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    protected void onClick() {
        this.getContext().startActivityAsUser(new Intent("com.android.intent.action.SHOW_BRIGHTNESS_DIALOG"), UserHandle.CURRENT_OR_SELF);
    }
}
