package com.android.settings;

import android.util.Log;
import android.os.Parcelable;
import android.os.Bundle;
import android.appwidget.AppWidgetProviderInfo;
import java.util.Collection;
import java.util.Collections;
import java.text.Collator;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;
import android.content.Intent;
import android.content.Context;
import android.appwidget.AppWidgetManager;

public class AppWidgetLoader<Item extends LabelledItem>
{
    private AppWidgetManager mAppWidgetManager;
    private Context mContext;
    ItemConstructor<Item> mItemConstructor;
    
    public AppWidgetLoader(final Context mContext, final AppWidgetManager mAppWidgetManager, final ItemConstructor<Item> mItemConstructor) {
        this.mContext = mContext;
        this.mAppWidgetManager = mAppWidgetManager;
        this.mItemConstructor = mItemConstructor;
    }
    
    protected List<Item> getItems(final Intent intent) {
        final boolean booleanExtra = intent.getBooleanExtra("customSort", true);
        final ArrayList<Item> list = new ArrayList<Item>();
        this.putInstalledAppWidgets(list, intent.getIntExtra("categoryFilter", 1));
        if (booleanExtra) {
            this.putCustomAppWidgets(list, intent);
        }
        Collections.sort(list, new Comparator<Item>() {
            Collator mCollator = Collator.getInstance();
            
            @Override
            public int compare(final Item item, final Item item2) {
                return this.mCollator.compare(item.getLabel(), item2.getLabel());
            }
        });
        if (!booleanExtra) {
            final ArrayList<Item> list2 = new ArrayList<Item>();
            this.putCustomAppWidgets(list2, intent);
            list.addAll((Collection<?>)list2);
        }
        return list;
    }
    
    void putAppWidgetItems(final List<AppWidgetProviderInfo> list, final List<Bundle> list2, final List<Item> list3, final int n, final boolean b) {
        if (list == null) {
            return;
        }
        for (int size = list.size(), i = 0; i < size; ++i) {
            final AppWidgetProviderInfo appWidgetProviderInfo = list.get(i);
            if (b || (appWidgetProviderInfo.widgetCategory & n) != 0x0) {
                final ItemConstructor<Item> mItemConstructor = this.mItemConstructor;
                final Context mContext = this.mContext;
                Bundle bundle;
                if (list2 != null) {
                    bundle = list2.get(i);
                }
                else {
                    bundle = null;
                }
                list3.add(mItemConstructor.createItem(mContext, appWidgetProviderInfo, bundle));
            }
        }
    }
    
    void putCustomAppWidgets(final List<Item> list, final Intent intent) {
        List<Bundle> list2 = null;
        final ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("customInfo");
        List<AppWidgetProviderInfo> list3 = null;
        Label_0325: {
            if (parcelableArrayListExtra != null && parcelableArrayListExtra.size() != 0) {
                final int size = parcelableArrayListExtra.size();
                final int n = 0;
                for (int i = 0; i < size; ++i) {
                    final Parcelable parcelable = (Parcelable)parcelableArrayListExtra.get(i);
                    if (parcelable == null || !(parcelable instanceof AppWidgetProviderInfo)) {
                        list3 = null;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("error using EXTRA_CUSTOM_INFO index=");
                        sb.append(i);
                        Log.e("AppWidgetAdapter", sb.toString());
                        break Label_0325;
                    }
                }
                final ArrayList parcelableArrayListExtra2 = intent.getParcelableArrayListExtra("customExtras");
                if (parcelableArrayListExtra2 == null) {
                    list3 = null;
                    Log.e("AppWidgetAdapter", "EXTRA_CUSTOM_INFO without EXTRA_CUSTOM_EXTRAS");
                    list2 = (List<Bundle>)parcelableArrayListExtra2;
                }
                else {
                    final int size2 = parcelableArrayListExtra2.size();
                    if (size != size2) {
                        list3 = null;
                        list2 = null;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("list size mismatch: EXTRA_CUSTOM_INFO: ");
                        sb2.append(size);
                        sb2.append(" EXTRA_CUSTOM_EXTRAS: ");
                        sb2.append(size2);
                        Log.e("AppWidgetAdapter", sb2.toString());
                    }
                    else {
                        int n2 = n;
                        while (true) {
                            list3 = (List<AppWidgetProviderInfo>)parcelableArrayListExtra;
                            list2 = (List<Bundle>)parcelableArrayListExtra2;
                            if (n2 >= size2) {
                                break;
                            }
                            final Parcelable parcelable2 = (Parcelable)parcelableArrayListExtra2.get(n2);
                            if (parcelable2 == null || !(parcelable2 instanceof Bundle)) {
                                list3 = null;
                                list2 = null;
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("error using EXTRA_CUSTOM_EXTRAS index=");
                                sb3.append(n2);
                                Log.e("AppWidgetAdapter", sb3.toString());
                                break;
                            }
                            ++n2;
                        }
                    }
                }
            }
            else {
                Log.i("AppWidgetAdapter", "EXTRA_CUSTOM_INFO not present.");
                list3 = (List<AppWidgetProviderInfo>)parcelableArrayListExtra;
            }
        }
        this.putAppWidgetItems(list3, list2, list, 0, true);
    }
    
    void putInstalledAppWidgets(final List<Item> list, final int n) {
        this.putAppWidgetItems(this.mAppWidgetManager.getInstalledProviders(n), null, list, n, false);
    }
    
    public interface ItemConstructor<Item>
    {
        Item createItem(final Context p0, final AppWidgetProviderInfo p1, final Bundle p2);
    }
    
    interface LabelledItem
    {
        CharSequence getLabel();
    }
}
