package com.android.settings.core;

import android.content.Context;
import java.util.Iterator;
import android.util.Log;
import java.util.ArrayList;
import java.util.TreeSet;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;

public class PreferenceControllerListHelper
{
    public static List<BasePreferenceController> filterControllers(final List<BasePreferenceController> list, final List<AbstractPreferenceController> list2) {
        if (list != null && list2 != null) {
            final TreeSet<String> set = new TreeSet<String>();
            final ArrayList<BasePreferenceController> list3 = new ArrayList<BasePreferenceController>();
            final Iterator<AbstractPreferenceController> iterator = list2.iterator();
            while (iterator.hasNext()) {
                final String preferenceKey = iterator.next().getPreferenceKey();
                if (preferenceKey != null) {
                    set.add(preferenceKey);
                }
            }
            for (final BasePreferenceController basePreferenceController : list) {
                if (set.contains(basePreferenceController.getPreferenceKey())) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(basePreferenceController.getPreferenceKey());
                    sb.append(" already has a controller");
                    Log.w("PrefCtrlListHelper", sb.toString());
                }
                else {
                    list3.add(basePreferenceController);
                }
            }
            return list3;
        }
        return list;
    }
    
    public static List<BasePreferenceController> getPreferenceControllersFromXml(final Context p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/util/ArrayList.<init>:()V
        //     7: astore_2       
        //     8: aload_0        
        //     9: iload_1        
        //    10: bipush          10
        //    12: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.extractMetadata:(Landroid/content/Context;II)Ljava/util/List;
        //    15: astore_3       
        //    16: aload_3        
        //    17: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    22: astore          4
        //    24: aload           4
        //    26: invokeinterface java/util/Iterator.hasNext:()Z
        //    31: ifeq            214
        //    34: aload           4
        //    36: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    41: checkcast       Landroid/os/Bundle;
        //    44: astore          5
        //    46: aload           5
        //    48: ldc             "controller"
        //    50: invokevirtual   android/os/Bundle.getString:(Ljava/lang/String;)Ljava/lang/String;
        //    53: astore          6
        //    55: aload           6
        //    57: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    60: ifeq            66
        //    63: goto            24
        //    66: aload_0        
        //    67: aload           6
        //    69: invokestatic    com/android/settings/core/BasePreferenceController.createInstance:(Landroid/content/Context;Ljava/lang/String;)Lcom/android/settings/core/BasePreferenceController;
        //    72: astore_3       
        //    73: goto            167
        //    76: astore_3       
        //    77: new             Ljava/lang/StringBuilder;
        //    80: dup            
        //    81: invokespecial   java/lang/StringBuilder.<init>:()V
        //    84: astore_3       
        //    85: aload_3        
        //    86: ldc             "Could not find Context-only controller for pref: "
        //    88: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    91: pop            
        //    92: aload_3        
        //    93: aload           6
        //    95: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    98: pop            
        //    99: ldc             "PrefCtrlListHelper"
        //   101: aload_3        
        //   102: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   105: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   108: pop            
        //   109: aload           5
        //   111: ldc             "key"
        //   113: invokevirtual   android/os/Bundle.getString:(Ljava/lang/String;)Ljava/lang/String;
        //   116: astore_3       
        //   117: aload_3        
        //   118: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   121: ifeq            159
        //   124: new             Ljava/lang/StringBuilder;
        //   127: dup            
        //   128: invokespecial   java/lang/StringBuilder.<init>:()V
        //   131: astore_3       
        //   132: aload_3        
        //   133: ldc             "Controller requires key but it's not defined in xml: "
        //   135: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   138: pop            
        //   139: aload_3        
        //   140: aload           6
        //   142: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   145: pop            
        //   146: ldc             "PrefCtrlListHelper"
        //   148: aload_3        
        //   149: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   152: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   155: pop            
        //   156: goto            24
        //   159: aload_0        
        //   160: aload           6
        //   162: aload_3        
        //   163: invokestatic    com/android/settings/core/BasePreferenceController.createInstance:(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/core/BasePreferenceController;
        //   166: astore_3       
        //   167: aload_2        
        //   168: aload_3        
        //   169: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   174: pop            
        //   175: goto            24
        //   178: astore_3       
        //   179: new             Ljava/lang/StringBuilder;
        //   182: dup            
        //   183: invokespecial   java/lang/StringBuilder.<init>:()V
        //   186: astore_3       
        //   187: aload_3        
        //   188: ldc             "Cannot instantiate controller from reflection: "
        //   190: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   193: pop            
        //   194: aload_3        
        //   195: aload           6
        //   197: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   200: pop            
        //   201: ldc             "PrefCtrlListHelper"
        //   203: aload_3        
        //   204: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   207: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   210: pop            
        //   211: goto            24
        //   214: aload_2        
        //   215: areturn        
        //   216: astore_0       
        //   217: ldc             "PrefCtrlListHelper"
        //   219: ldc             "Failed to parse preference xml for getting controllers"
        //   221: aload_0        
        //   222: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   225: pop            
        //   226: aload_2        
        //   227: areturn        
        //    Signature:
        //  (Landroid/content/Context;I)Ljava/util/List<Lcom/android/settings/core/BasePreferenceController;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  8      16     216    228    Ljava/io/IOException;
        //  8      16     216    228    Lorg/xmlpull/v1/XmlPullParserException;
        //  66     73     76     167    Ljava/lang/IllegalStateException;
        //  159    167    178    214    Ljava/lang/IllegalStateException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0159:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Thread.java:745)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
