package com.android.settings.core;

import android.util.TypedValue;
import com.android.internal.R$styleable;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import com.android.settings.R;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import android.os.Bundle;
import android.content.Context;
import java.util.Arrays;
import java.util.List;

public class PreferenceXmlParserUtils
{
    static final String PREF_SCREEN_TAG = "PreferenceScreen";
    private static final List<String> SUPPORTED_PREF_TYPES;
    
    static {
        SUPPORTED_PREF_TYPES = Arrays.asList("Preference", "PreferenceCategory", "PreferenceScreen");
    }
    
    public static List<Bundle> extractMetadata(final Context context, int n, final int n2) throws IOException, XmlPullParserException {
        final ArrayList<Bundle> list = new ArrayList<Bundle>();
        if (n <= 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(n);
            sb.append(" is invalid.");
            Log.d("PreferenceXmlParserUtil", sb.toString());
            return list;
        }
        final XmlResourceParser xml = context.getResources().getXml(n);
        while ((n = xml.next()) != 1 && n != 2) {}
        final int depth = xml.getDepth();
        int n3 = n;
        while (true) {
            if (n3 == 2) {
                final String name = xml.getName();
                if (hasFlag(n2, 1) || !TextUtils.equals((CharSequence)"PreferenceScreen", (CharSequence)name)) {
                    if (PreferenceXmlParserUtils.SUPPORTED_PREF_TYPES.contains(name) || name.endsWith("Preference")) {
                        final Bundle bundle = new Bundle();
                        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet((XmlPullParser)xml), R.styleable.Preference);
                        if (hasFlag(n2, 4)) {
                            bundle.putString("type", name);
                        }
                        if (hasFlag(n2, 2)) {
                            bundle.putString("key", getKey(obtainStyledAttributes));
                        }
                        if (hasFlag(n2, 8)) {
                            bundle.putString("controller", getController(obtainStyledAttributes));
                        }
                        if (hasFlag(n2, 16)) {
                            bundle.putString("title", getTitle(obtainStyledAttributes));
                        }
                        if (hasFlag(n2, 32)) {
                            bundle.putString("summary", getSummary(obtainStyledAttributes));
                        }
                        if (hasFlag(n2, 64)) {
                            bundle.putInt("icon", getIcon(obtainStyledAttributes));
                        }
                        if (hasFlag(n2, 128)) {
                            bundle.putBoolean("platform_slice", getPlatformSlice(obtainStyledAttributes));
                        }
                        if (hasFlag(n2, 256)) {
                            bundle.putString("keywords", getKeywords(obtainStyledAttributes));
                        }
                        list.add(bundle);
                        obtainStyledAttributes.recycle();
                    }
                }
            }
            if ((n = xml.next()) == 1) {
                break;
            }
            if ((n3 = n) != 3) {
                continue;
            }
            n3 = n;
            if (xml.getDepth() <= depth) {
                break;
            }
        }
        xml.close();
        return list;
    }
    
    private static String getController(final TypedArray typedArray) {
        return typedArray.getString(18);
    }
    
    public static String getDataChildFragment(final Context context, final AttributeSet set) {
        return getStringData(context, set, R.styleable.Preference, 13);
    }
    
    public static String getDataEntries(final Context context, final AttributeSet set) {
        return getDataEntries(context, set, R$styleable.ListPreference, 0);
    }
    
    private static String getDataEntries(final Context context, final AttributeSet set, final int[] array, int length) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, array);
        final TypedValue peekValue = obtainStyledAttributes.peekValue(length);
        obtainStyledAttributes.recycle();
        String[] stringArray;
        final String[] array2 = stringArray = null;
        if (peekValue != null) {
            stringArray = array2;
            if (peekValue.type == 1) {
                stringArray = array2;
                if (peekValue.resourceId != 0) {
                    stringArray = context.getResources().getStringArray(peekValue.resourceId);
                }
            }
        }
        int i = 0;
        if (stringArray == null) {
            length = 0;
        }
        else {
            length = stringArray.length;
        }
        if (length == 0) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        while (i < length) {
            sb.append(stringArray[i]);
            sb.append("|");
            ++i;
        }
        return sb.toString();
    }
    
    @Deprecated
    public static int getDataIcon(final Context context, final AttributeSet set) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R$styleable.Preference);
        final int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        return resourceId;
    }
    
    @Deprecated
    public static String getDataKey(final Context context, final AttributeSet set) {
        return getStringData(context, set, R$styleable.Preference, 6);
    }
    
    public static String getDataKeywords(final Context context, final AttributeSet set) {
        return getStringData(context, set, R.styleable.Preference, 27);
    }
    
    @Deprecated
    public static String getDataSummary(final Context context, final AttributeSet set) {
        return getStringData(context, set, R$styleable.Preference, 7);
    }
    
    public static String getDataSummaryOff(final Context context, final AttributeSet set) {
        return getStringData(context, set, R$styleable.CheckBoxPreference, 1);
    }
    
    public static String getDataSummaryOn(final Context context, final AttributeSet set) {
        return getStringData(context, set, R$styleable.CheckBoxPreference, 0);
    }
    
    @Deprecated
    public static String getDataTitle(final Context context, final AttributeSet set) {
        return getStringData(context, set, R$styleable.Preference, 4);
    }
    
    private static int getIcon(final TypedArray typedArray) {
        return typedArray.getResourceId(0, 0);
    }
    
    private static String getKey(final TypedArray typedArray) {
        return typedArray.getString(6);
    }
    
    private static String getKeywords(final TypedArray typedArray) {
        return typedArray.getString(27);
    }
    
    private static boolean getPlatformSlice(final TypedArray typedArray) {
        return typedArray.getBoolean(31, false);
    }
    
    @Deprecated
    private static String getStringData(final Context context, final AttributeSet set, final int[] array, final int n) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, array);
        final String string = obtainStyledAttributes.getString(n);
        obtainStyledAttributes.recycle();
        return string;
    }
    
    private static String getSummary(final TypedArray typedArray) {
        return typedArray.getString(7);
    }
    
    private static String getTitle(final TypedArray typedArray) {
        return typedArray.getString(4);
    }
    
    private static boolean hasFlag(final int n, final int n2) {
        return (n & n2) != 0x0;
    }
}
