package com.android.settings.core;

import com.android.settings.widget.SeekBarPreference;
import android.content.Context;
import android.support.v7.preference.Preference;

public abstract class SliderPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    public SliderPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    public abstract int getMaxSteps();
    
    @Override
    public int getSliceType() {
        return 2;
    }
    
    public abstract int getSliderPosition();
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return this.setSliderPosition((int)o);
    }
    
    public abstract boolean setSliderPosition(final int p0);
    
    @Override
    public void updateState(final Preference preference) {
        if (preference instanceof SeekBarPreference) {
            ((SeekBarPreference)preference).setProgress(this.getSliderPosition());
        }
        else if (preference instanceof android.support.v7.preference.SeekBarPreference) {
            ((android.support.v7.preference.SeekBarPreference)preference).setValue(this.getSliderPosition());
        }
    }
}
