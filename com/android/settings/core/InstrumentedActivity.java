package com.android.settings.core;

import android.arch.lifecycle.LifecycleObserver;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import android.content.Context;
import com.android.settings.overlay.FeatureFactory;
import android.os.Bundle;
import com.android.settingslib.core.instrumentation.Instrumentable;
import com.android.settingslib.core.lifecycle.ObservableActivity;

public abstract class InstrumentedActivity extends ObservableActivity implements Instrumentable
{
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getLifecycle().addObserver(new VisibilityLoggerMixin(this.getMetricsCategory(), FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider()));
    }
}
