package com.android.settings.core.instrumentation;

import android.arch.lifecycle.LifecycleObserver;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.DialogCreatable;
import com.android.settingslib.core.instrumentation.Instrumentable;
import com.android.settingslib.core.lifecycle.ObservableDialogFragment;

public abstract class InstrumentedDialogFragment extends ObservableDialogFragment implements Instrumentable
{
    protected final DialogCreatable mDialogCreatable;
    protected int mDialogId;
    protected MetricsFeatureProvider mMetricsFeatureProvider;
    
    public InstrumentedDialogFragment() {
        this(null, 0);
    }
    
    public InstrumentedDialogFragment(final DialogCreatable mDialogCreatable, final int mDialogId) {
        this.mDialogCreatable = mDialogCreatable;
        this.mDialogId = mDialogId;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mLifecycle.addObserver(new VisibilityLoggerMixin(this.getMetricsCategory(), this.mMetricsFeatureProvider));
        this.mLifecycle.onAttach(context);
    }
}
