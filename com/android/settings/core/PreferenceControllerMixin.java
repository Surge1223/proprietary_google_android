package com.android.settings.core;

import android.util.Log;
import android.text.TextUtils;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.search.ResultPayload;

public interface PreferenceControllerMixin
{
    @Deprecated
    default ResultPayload getResultPayload() {
        return null;
    }
    
    default void updateNonIndexableKeys(final List<String> list) {
        if (this instanceof AbstractPreferenceController && !((AbstractPreferenceController)this).isAvailable()) {
            final String preferenceKey = ((AbstractPreferenceController)this).getPreferenceKey();
            if (TextUtils.isEmpty((CharSequence)preferenceKey)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping updateNonIndexableKeys due to empty key ");
                sb.append(this.toString());
                Log.w("PrefControllerMixin", sb.toString());
                return;
            }
            list.add(preferenceKey);
        }
    }
}
