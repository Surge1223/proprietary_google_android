package com.android.settings.core;

import android.text.TextUtils;
import com.android.settings.SubSettings;
import android.os.Bundle;
import android.os.UserHandle;
import android.content.Intent;
import android.app.Fragment;
import android.content.Context;

public class SubSettingLauncher
{
    private final Context mContext;
    private final LaunchRequest mLaunchRequest;
    private boolean mLaunched;
    
    public SubSettingLauncher(final Context mContext) {
        if (mContext != null) {
            this.mContext = mContext;
            this.mLaunchRequest = new LaunchRequest();
            return;
        }
        throw new IllegalArgumentException("Context must be non-null.");
    }
    
    private void launchForResult(final Fragment fragment, final Intent intent, final int n) {
        fragment.startActivityForResult(intent, n);
    }
    
    public SubSettingLauncher addFlags(final int n) {
        final LaunchRequest mLaunchRequest = this.mLaunchRequest;
        mLaunchRequest.flags |= n;
        return this;
    }
    
    public void launch() {
        if (!this.mLaunched) {
            boolean b = true;
            this.mLaunched = true;
            final Intent intent = this.toIntent();
            final boolean b2 = this.mLaunchRequest.userHandle != null && this.mLaunchRequest.userHandle.getIdentifier() != UserHandle.myUserId();
            if (this.mLaunchRequest.mResultListener == null) {
                b = false;
            }
            if (b2 && b) {
                this.launchForResultAsUser(intent, this.mLaunchRequest.userHandle, this.mLaunchRequest.mResultListener, this.mLaunchRequest.mRequestCode);
            }
            else if (b2 && !b) {
                this.launchAsUser(intent, this.mLaunchRequest.userHandle);
            }
            else if (!b2 && b) {
                this.launchForResult(this.mLaunchRequest.mResultListener, intent, this.mLaunchRequest.mRequestCode);
            }
            else {
                this.launch(intent);
            }
            return;
        }
        throw new IllegalStateException("This launcher has already been executed. Do not reuse");
    }
    
    void launch(final Intent intent) {
        this.mContext.startActivity(intent);
    }
    
    void launchAsUser(final Intent intent, final UserHandle userHandle) {
        intent.addFlags(268435456);
        intent.addFlags(32768);
        this.mContext.startActivityAsUser(intent, userHandle);
    }
    
    void launchForResultAsUser(final Intent intent, final UserHandle userHandle, final Fragment fragment, final int n) {
        fragment.getActivity().startActivityForResultAsUser(intent, n, userHandle);
    }
    
    public SubSettingLauncher setArguments(final Bundle arguments) {
        this.mLaunchRequest.arguments = arguments;
        return this;
    }
    
    public SubSettingLauncher setDestination(final String destinationName) {
        this.mLaunchRequest.destinationName = destinationName;
        return this;
    }
    
    public SubSettingLauncher setResultListener(final Fragment mResultListener, final int mRequestCode) {
        this.mLaunchRequest.mRequestCode = mRequestCode;
        this.mLaunchRequest.mResultListener = mResultListener;
        return this;
    }
    
    public SubSettingLauncher setSourceMetricsCategory(final int sourceMetricsCategory) {
        this.mLaunchRequest.sourceMetricsCategory = sourceMetricsCategory;
        return this;
    }
    
    public SubSettingLauncher setTitle(final int n) {
        return this.setTitle(null, n);
    }
    
    public SubSettingLauncher setTitle(final CharSequence title) {
        this.mLaunchRequest.title = title;
        return this;
    }
    
    public SubSettingLauncher setTitle(final String titleResPackageName, final int titleResId) {
        this.mLaunchRequest.titleResPackageName = titleResPackageName;
        this.mLaunchRequest.titleResId = titleResId;
        this.mLaunchRequest.title = null;
        return this;
    }
    
    public SubSettingLauncher setUserHandle(final UserHandle userHandle) {
        this.mLaunchRequest.userHandle = userHandle;
        return this;
    }
    
    public Intent toIntent() {
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.setClass(this.mContext, (Class)SubSettings.class);
        if (TextUtils.isEmpty((CharSequence)this.mLaunchRequest.destinationName)) {
            throw new IllegalArgumentException("Destination fragment must be set");
        }
        intent.putExtra(":settings:show_fragment", this.mLaunchRequest.destinationName);
        if (this.mLaunchRequest.sourceMetricsCategory >= 0) {
            intent.putExtra(":settings:source_metrics", this.mLaunchRequest.sourceMetricsCategory);
            intent.putExtra(":settings:show_fragment_args", this.mLaunchRequest.arguments);
            intent.putExtra(":settings:show_fragment_title_res_package_name", this.mLaunchRequest.titleResPackageName);
            intent.putExtra(":settings:show_fragment_title_resid", this.mLaunchRequest.titleResId);
            intent.putExtra(":settings:show_fragment_title", this.mLaunchRequest.title);
            intent.putExtra(":settings:show_fragment_as_shortcut", this.mLaunchRequest.isShortCut);
            intent.addFlags(this.mLaunchRequest.flags);
            return intent;
        }
        throw new IllegalArgumentException("Source metrics category must be set");
    }
    
    static class LaunchRequest
    {
        Bundle arguments;
        String destinationName;
        int flags;
        boolean isShortCut;
        int mRequestCode;
        Fragment mResultListener;
        int sourceMetricsCategory;
        CharSequence title;
        int titleResId;
        String titleResPackageName;
        UserHandle userHandle;
        
        LaunchRequest() {
            this.sourceMetricsCategory = -100;
        }
    }
}
