package com.android.settings.core;

import com.android.settings.widget.MasterSwitchPreference;
import android.support.v7.preference.TwoStatePreference;
import android.content.Context;
import android.support.v7.preference.Preference;

public abstract class TogglePreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    private static final String TAG = "TogglePrefController";
    
    public TogglePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getSliceType() {
        return 1;
    }
    
    public abstract boolean isChecked();
    
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return this.setChecked((boolean)o);
    }
    
    public abstract boolean setChecked(final boolean p0);
    
    @Override
    public void updateState(final Preference preference) {
        if (preference instanceof TwoStatePreference) {
            ((TwoStatePreference)preference).setChecked(this.isChecked());
        }
        else if (preference instanceof MasterSwitchPreference) {
            ((MasterSwitchPreference)preference).setChecked(this.isChecked());
        }
        else {
            this.refreshSummary(preference);
        }
    }
}
