package com.android.settings.core;

import com.android.settings.search.SearchIndexableRaw;
import android.util.Log;
import java.util.List;
import com.android.settings.search.ResultPayload;
import android.content.IntentFilter;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import java.lang.reflect.InvocationTargetException;
import android.text.TextUtils;
import android.content.Context;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class BasePreferenceController extends AbstractPreferenceController
{
    public static final int AVAILABLE = 0;
    public static final int CONDITIONALLY_UNAVAILABLE = 1;
    public static final int DISABLED_DEPENDENT_SETTING = 4;
    public static final int DISABLED_FOR_USER = 3;
    private static final String TAG = "SettingsPrefController";
    public static final int UNSUPPORTED_ON_DEVICE = 2;
    protected final String mPreferenceKey;
    
    public BasePreferenceController(final Context context, final String mPreferenceKey) {
        super(context);
        this.mPreferenceKey = mPreferenceKey;
        if (!TextUtils.isEmpty((CharSequence)this.mPreferenceKey)) {
            return;
        }
        throw new IllegalArgumentException("Preference key must be set");
    }
    
    public static BasePreferenceController createInstance(final Context context, final String s) {
        try {
            return (BasePreferenceController)Class.forName(s).getConstructor(Context.class).newInstance(context);
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalArgumentException | InvocationTargetException | IllegalAccessException ex3) {
            final IllegalAccessException ex2;
            final IllegalAccessException ex = ex2;
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid preference controller: ");
            sb.append(s);
            throw new IllegalStateException(sb.toString(), ex);
        }
    }
    
    public static BasePreferenceController createInstance(final Context context, final String s, final String s2) {
        try {
            return (BasePreferenceController)Class.forName(s).getConstructor(Context.class, String.class).newInstance(context, s2);
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalArgumentException | InvocationTargetException | IllegalAccessException ex3) {
            final IllegalAccessException ex2;
            final IllegalAccessException ex = ex2;
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid preference controller: ");
            sb.append(s);
            throw new IllegalStateException(sb.toString(), ex);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.getAvailabilityStatus() == 4) {
            final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
            if (preference != null) {
                preference.setEnabled(false);
            }
        }
    }
    
    public abstract int getAvailabilityStatus();
    
    public IntentFilter getIntentFilter() {
        return null;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPreferenceKey;
    }
    
    @Deprecated
    public ResultPayload getResultPayload() {
        return null;
    }
    
    public int getSliceType() {
        return 0;
    }
    
    public boolean hasAsyncUpdate() {
        return false;
    }
    
    @Override
    public final boolean isAvailable() {
        final int availabilityStatus = this.getAvailabilityStatus();
        return availabilityStatus == 0 || availabilityStatus == 4;
    }
    
    public boolean isSliceable() {
        return false;
    }
    
    public final boolean isSupported() {
        return this.getAvailabilityStatus() != 2;
    }
    
    public void updateNonIndexableKeys(final List<String> list) {
        if (this instanceof AbstractPreferenceController && !this.isAvailable()) {
            final String preferenceKey = this.getPreferenceKey();
            if (TextUtils.isEmpty((CharSequence)preferenceKey)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping updateNonIndexableKeys due to empty key ");
                sb.append(this.toString());
                Log.w("SettingsPrefController", sb.toString());
                return;
            }
            list.add(preferenceKey);
        }
    }
    
    public void updateRawDataToIndex(final List<SearchIndexableRaw> list) {
    }
}
