package com.android.settings.core;

import android.app.Fragment;
import com.android.settings.survey.SurveyMixin;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.instrumentation.Instrumentable;
import com.android.settingslib.core.lifecycle.ObservableFragment;

public abstract class InstrumentedFragment extends ObservableFragment implements Instrumentable
{
    protected MetricsFeatureProvider mMetricsFeatureProvider;
    private VisibilityLoggerMixin mVisibilityLoggerMixin;
    
    @Override
    public void onAttach(final Context context) {
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mVisibilityLoggerMixin = new VisibilityLoggerMixin(this.getMetricsCategory(), this.mMetricsFeatureProvider);
        this.getLifecycle().addObserver(this.mVisibilityLoggerMixin);
        this.getLifecycle().addObserver(new SurveyMixin(this, this.getClass().getSimpleName()));
        super.onAttach(context);
    }
    
    @Override
    public void onResume() {
        this.mVisibilityLoggerMixin.setSourceMetricsCategory(this.getActivity());
        super.onResume();
    }
}
