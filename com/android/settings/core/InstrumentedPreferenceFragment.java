package com.android.settings.core;

import android.os.Bundle;
import android.app.Fragment;
import com.android.settings.survey.SurveyMixin;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.util.Log;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.instrumentation.Instrumentable;
import com.android.settingslib.core.lifecycle.ObservablePreferenceFragment;

public abstract class InstrumentedPreferenceFragment extends ObservablePreferenceFragment implements Instrumentable
{
    protected final int PLACEHOLDER_METRIC;
    protected MetricsFeatureProvider mMetricsFeatureProvider;
    private VisibilityLoggerMixin mVisibilityLoggerMixin;
    
    public InstrumentedPreferenceFragment() {
        this.PLACEHOLDER_METRIC = 10000;
    }
    
    private void updateActivityTitleWithScreenTitle(final PreferenceScreen preferenceScreen) {
        if (preferenceScreen != null) {
            final CharSequence title = preferenceScreen.getTitle();
            if (!TextUtils.isEmpty(title)) {
                this.getActivity().setTitle(title);
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Screen title missing for fragment ");
                sb.append(this.getClass().getName());
                Log.w("InstrumentedPrefFrag", sb.toString());
            }
        }
    }
    
    @Override
    public void addPreferencesFromResource(final int n) {
        super.addPreferencesFromResource(n);
        this.updateActivityTitleWithScreenTitle(this.getPreferenceScreen());
    }
    
    protected final Context getPrefContext() {
        return this.getPreferenceManager().getContext();
    }
    
    protected int getPreferenceScreenResId() {
        return -1;
    }
    
    protected final VisibilityLoggerMixin getVisibilityLogger() {
        return this.mVisibilityLoggerMixin;
    }
    
    @Override
    public void onAttach(final Context context) {
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mVisibilityLoggerMixin = new VisibilityLoggerMixin(this.getMetricsCategory(), this.mMetricsFeatureProvider);
        this.getLifecycle().addObserver(this.mVisibilityLoggerMixin);
        this.getLifecycle().addObserver(new SurveyMixin(this, this.getClass().getSimpleName()));
        super.onAttach(context);
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        final int preferenceScreenResId = this.getPreferenceScreenResId();
        if (preferenceScreenResId > 0) {
            this.addPreferencesFromResource(preferenceScreenResId);
        }
    }
    
    @Override
    public void onResume() {
        this.mVisibilityLoggerMixin.setSourceMetricsCategory(this.getActivity());
        super.onResume();
    }
}
