package com.android.settings.fingerprint;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.android.settings.SetupWizardUtils;
import com.android.settings.password.StorageManagerWrapper;
import com.android.settings.password.SetupChooseLockGeneric;
import android.app.KeyguardManager;
import android.os.UserHandle;
import android.content.Context;
import com.android.internal.widget.LockPatternUtils;
import android.content.Intent;

public class SetupFingerprintEnrollIntroduction extends FingerprintEnrollIntroduction
{
    private boolean mAlreadyHadLockScreenSetup;
    
    public SetupFingerprintEnrollIntroduction() {
        this.mAlreadyHadLockScreenSetup = false;
    }
    
    private Intent getMetricIntent(final Intent intent) {
        Intent intent2 = intent;
        if (intent == null) {
            intent2 = new Intent();
        }
        intent2.putExtra(":settings:password_quality", new LockPatternUtils((Context)this).getKeyguardStoredPasswordQuality(UserHandle.myUserId()));
        return intent2;
    }
    
    private boolean isKeyguardSecure() {
        return ((KeyguardManager)this.getSystemService((Class)KeyguardManager.class)).isKeyguardSecure();
    }
    
    @Override
    protected Intent getChooseLockIntent() {
        final Intent intent = new Intent((Context)this, (Class)SetupChooseLockGeneric.class);
        if (StorageManagerWrapper.isFileEncryptedNativeOrEmulated()) {
            intent.putExtra("lockscreen.password_type", 131072);
            intent.putExtra("show_options_button", true);
        }
        SetupWizardUtils.copySetupExtras(this.getIntent(), intent);
        return intent;
    }
    
    @Override
    protected Intent getFindSensorIntent() {
        final Intent intent = new Intent((Context)this, (Class)SetupFingerprintEnrollFindSensor.class);
        SetupWizardUtils.copySetupExtras(this.getIntent(), intent);
        return intent;
    }
    
    @Override
    public int getMetricsCategory() {
        return 249;
    }
    
    @Override
    protected void initViews() {
        super.initViews();
        ((TextView)this.findViewById(2131362068)).setText(2131889002);
        this.getNextButton().setText(2131889000);
        ((Button)this.findViewById(2131362149)).setText(2131888998);
    }
    
    @Override
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        Intent metricIntent = intent;
        if (n == 2) {
            metricIntent = intent;
            if (this.isKeyguardSecure()) {
                metricIntent = intent;
                if (!this.mAlreadyHadLockScreenSetup) {
                    metricIntent = this.getMetricIntent(intent);
                }
            }
        }
        super.onActivityResult(n, n2, metricIntent);
    }
    
    public void onBackPressed() {
        if (!this.mAlreadyHadLockScreenSetup && this.isKeyguardSecure()) {
            this.setResult(0, this.getMetricIntent(null));
        }
        super.onBackPressed();
    }
    
    @Override
    protected void onCancelButtonClick() {
        if (this.isKeyguardSecure()) {
            final boolean mAlreadyHadLockScreenSetup = this.mAlreadyHadLockScreenSetup;
            Intent metricIntent = null;
            if (!mAlreadyHadLockScreenSetup) {
                metricIntent = this.getMetricIntent(null);
            }
            this.setResult(2, metricIntent);
            this.finish();
        }
        else {
            this.setResult(11);
            this.finish();
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            this.mAlreadyHadLockScreenSetup = this.isKeyguardSecure();
        }
        else {
            this.mAlreadyHadLockScreenSetup = bundle.getBoolean("wasLockScreenPresent", false);
        }
    }
    
    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("wasLockScreenPresent", this.mAlreadyHadLockScreenSetup);
    }
}
