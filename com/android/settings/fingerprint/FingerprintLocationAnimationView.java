package com.android.settings.fingerprint;

import android.animation.TimeInterpolator;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.graphics.Canvas;
import android.view.animation.AnimationUtils;
import com.android.settingslib.Utils;
import android.util.AttributeSet;
import android.content.Context;
import android.view.animation.Interpolator;
import android.graphics.Paint;
import android.animation.ValueAnimator;
import android.view.View;

public class FingerprintLocationAnimationView extends View implements FingerprintFindSensorAnimation
{
    private ValueAnimator mAlphaAnimator;
    private final Paint mDotPaint;
    private final int mDotRadius;
    private final Interpolator mFastOutSlowInInterpolator;
    private final float mFractionCenterX;
    private final float mFractionCenterY;
    private final Interpolator mLinearOutSlowInInterpolator;
    private final int mMaxPulseRadius;
    private final Paint mPulsePaint;
    private float mPulseRadius;
    private ValueAnimator mRadiusAnimator;
    private final Runnable mStartPhaseRunnable;
    
    public FingerprintLocationAnimationView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mDotPaint = new Paint();
        this.mPulsePaint = new Paint();
        this.mStartPhaseRunnable = new Runnable() {
            @Override
            public void run() {
                FingerprintLocationAnimationView.this.startPhase();
            }
        };
        this.mDotRadius = this.getResources().getDimensionPixelSize(2131165398);
        this.mMaxPulseRadius = this.getResources().getDimensionPixelSize(2131165407);
        this.mFractionCenterX = this.getResources().getFraction(2131296262, 1, 1);
        this.mFractionCenterY = this.getResources().getFraction(2131296263, 1, 1);
        final int colorAccent = Utils.getColorAccent(context);
        this.mDotPaint.setAntiAlias(true);
        this.mPulsePaint.setAntiAlias(true);
        this.mDotPaint.setColor(colorAccent);
        this.mPulsePaint.setColor(colorAccent);
        this.mLinearOutSlowInInterpolator = AnimationUtils.loadInterpolator(context, 17563662);
        this.mFastOutSlowInInterpolator = AnimationUtils.loadInterpolator(context, 17563662);
    }
    
    private void drawDot(final Canvas canvas) {
        canvas.drawCircle(this.getCenterX(), this.getCenterY(), (float)this.mDotRadius, this.mDotPaint);
    }
    
    private void drawPulse(final Canvas canvas) {
        canvas.drawCircle(this.getCenterX(), this.getCenterY(), this.mPulseRadius, this.mPulsePaint);
    }
    
    private float getCenterX() {
        return this.getWidth() * this.mFractionCenterX;
    }
    
    private float getCenterY() {
        return this.getHeight() * this.mFractionCenterY;
    }
    
    private void startAlphaAnimation() {
        this.mPulsePaint.setAlpha(38);
        final ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[] { 0.15f, 0.0f });
        ofFloat.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                FingerprintLocationAnimationView.this.mPulsePaint.setAlpha((int)(255.0f * (float)valueAnimator.getAnimatedValue()));
                FingerprintLocationAnimationView.this.invalidate();
            }
        });
        ofFloat.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            public void onAnimationEnd(final Animator animator) {
                FingerprintLocationAnimationView.this.mAlphaAnimator = null;
            }
        });
        ofFloat.setDuration(750L);
        ofFloat.setInterpolator((TimeInterpolator)this.mFastOutSlowInInterpolator);
        ofFloat.setStartDelay(250L);
        ofFloat.start();
        this.mAlphaAnimator = ofFloat;
    }
    
    private void startPhase() {
        this.startRadiusAnimation();
        this.startAlphaAnimation();
    }
    
    private void startRadiusAnimation() {
        final ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[] { 0.0f, this.mMaxPulseRadius });
        ofFloat.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                FingerprintLocationAnimationView.this.mPulseRadius = (float)valueAnimator.getAnimatedValue();
                FingerprintLocationAnimationView.this.invalidate();
            }
        });
        ofFloat.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            boolean mCancelled;
            
            public void onAnimationCancel(final Animator animator) {
                this.mCancelled = true;
            }
            
            public void onAnimationEnd(final Animator animator) {
                FingerprintLocationAnimationView.this.mRadiusAnimator = null;
                if (!this.mCancelled) {
                    FingerprintLocationAnimationView.this.postDelayed(FingerprintLocationAnimationView.this.mStartPhaseRunnable, 1000L);
                }
            }
        });
        ofFloat.setDuration(1000L);
        ofFloat.setInterpolator((TimeInterpolator)this.mLinearOutSlowInInterpolator);
        ofFloat.start();
        this.mRadiusAnimator = ofFloat;
    }
    
    protected void onDraw(final Canvas canvas) {
        this.drawPulse(canvas);
        this.drawDot(canvas);
    }
    
    public void pauseAnimation() {
        this.stopAnimation();
    }
    
    public void startAnimation() {
        this.startPhase();
    }
    
    public void stopAnimation() {
        this.removeCallbacks(this.mStartPhaseRunnable);
        if (this.mRadiusAnimator != null) {
            this.mRadiusAnimator.cancel();
        }
        if (this.mAlphaAnimator != null) {
            this.mAlphaAnimator.cancel();
        }
    }
}
