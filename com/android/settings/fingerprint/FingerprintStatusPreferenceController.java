package com.android.settings.fingerprint;

import java.util.List;
import android.content.Intent;
import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import android.hardware.fingerprint.FingerprintManager;
import com.android.settings.core.BasePreferenceController;

public class FingerprintStatusPreferenceController extends BasePreferenceController
{
    private static final String KEY_FINGERPRINT_SETTINGS = "fingerprint_settings";
    protected final FingerprintManager mFingerprintManager;
    protected final LockPatternUtils mLockPatternUtils;
    protected final int mProfileChallengeUserId;
    protected final UserManager mUm;
    protected final int mUserId;
    
    public FingerprintStatusPreferenceController(final Context context) {
        this(context, "fingerprint_settings");
    }
    
    public FingerprintStatusPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mUserId = UserHandle.myUserId();
        this.mFingerprintManager = Utils.getFingerprintManagerOrNull(context);
        this.mUm = (UserManager)context.getSystemService("user");
        this.mLockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
        this.mProfileChallengeUserId = Utils.getManagedProfileId(this.mUm, this.mUserId);
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mFingerprintManager == null || !this.mFingerprintManager.isHardwareDetected()) {
            return 2;
        }
        if (this.isUserSupported()) {
            return 0;
        }
        return 3;
    }
    
    protected int getUserId() {
        return this.mUserId;
    }
    
    protected boolean isUserSupported() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!this.isAvailable()) {
            if (preference != null) {
                preference.setVisible(false);
            }
            return;
        }
        preference.setVisible(true);
        final int userId = this.getUserId();
        final List enrolledFingerprints = this.mFingerprintManager.getEnrolledFingerprints(userId);
        int size;
        if (enrolledFingerprints != null) {
            size = enrolledFingerprints.size();
        }
        else {
            size = 0;
        }
        String s;
        if (size > 0) {
            preference.setSummary(this.mContext.getResources().getQuantityString(2131755060, size, new Object[] { size }));
            s = FingerprintSettings.class.getName();
        }
        else {
            preference.setSummary(2131889014);
            s = FingerprintEnrollIntroduction.class.getName();
        }
        preference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$FingerprintStatusPreferenceController$KyR_IBe4qHxX_KvME9NBxSJFxFQ(userId, s));
    }
}
