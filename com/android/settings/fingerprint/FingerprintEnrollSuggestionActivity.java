package com.android.settings.fingerprint;

import com.android.settings.Utils;
import android.content.Context;

public class FingerprintEnrollSuggestionActivity extends FingerprintEnrollIntroduction
{
    public static boolean isSuggestionComplete(final Context context) {
        return !Utils.hasFingerprintHardware(context) || !FingerprintSuggestionActivity.isFingerprintEnabled(context) || !Utils.hasFingerprintHardware(context) || Utils.getFingerprintManagerOrNull(context).hasEnrolledFingerprints();
    }
}
