package com.android.settings.fingerprint;

import android.content.DialogInterface$OnShowListener;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.widget.EditText;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import java.io.Serializable;
import android.os.UserManager;
import android.util.Log;
import com.android.settingslib.widget.FooterPreference;
import com.android.settingslib.HelpUtils;
import com.android.settings.utils.AnnotationSpan;
import android.os.UserHandle;
import com.android.settings.Utils;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.settings.password.ChooseLockGeneric;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.support.v7.preference.PreferenceScreen;
import java.util.List;
import android.support.v7.preference.PreferenceGroup;
import android.os.Message;
import android.app.Activity;
import android.widget.Toast;
import android.hardware.fingerprint.FingerprintManager$AuthenticationResult;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import java.util.HashMap;
import android.hardware.fingerprint.FingerprintManager;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import android.hardware.fingerprint.Fingerprint;
import android.view.View;
import com.android.settingslib.TwoTargetPreference;
import android.os.Bundle;
import android.content.Intent;
import com.android.settings.SubSettings;

public class FingerprintSettings extends SubSettings
{
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", FingerprintSettingsFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return FingerprintSettingsFragment.class.getName().equals(s);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setTitle(this.getText(2131889015));
    }
    
    public static class FingerprintPreference extends TwoTargetPreference
    {
        private View mDeleteView;
        private Fingerprint mFingerprint;
        private final OnDeleteClickListener mOnDeleteClickListener;
        private View mView;
        
        public FingerprintPreference(final Context context, final OnDeleteClickListener mOnDeleteClickListener) {
            super(context);
            this.mOnDeleteClickListener = mOnDeleteClickListener;
        }
        
        public Fingerprint getFingerprint() {
            return this.mFingerprint;
        }
        
        @Override
        protected int getSecondTargetResId() {
            return 2131558678;
        }
        
        public View getView() {
            return this.mView;
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            super.onBindViewHolder(preferenceViewHolder);
            this.mView = preferenceViewHolder.itemView;
            (this.mDeleteView = preferenceViewHolder.itemView.findViewById(2131362058)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    if (FingerprintPreference.this.mOnDeleteClickListener != null) {
                        FingerprintPreference.this.mOnDeleteClickListener.onDeleteClick(FingerprintPreference.this);
                    }
                }
            });
        }
        
        public void setFingerprint(final Fingerprint mFingerprint) {
            this.mFingerprint = mFingerprint;
        }
        
        public interface OnDeleteClickListener
        {
            void onDeleteClick(final FingerprintPreference p0);
        }
    }
    
    public static class FingerprintSettingsFragment extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnDeleteClickListener
    {
        FingerprintAuthenticateSidecar.Listener mAuthenticateListener;
        private FingerprintAuthenticateSidecar mAuthenticateSidecar;
        private final Runnable mFingerprintLockoutReset;
        private FingerprintManager mFingerprintManager;
        private HashMap<Integer, String> mFingerprintsRenaming;
        private final Handler mHandler;
        private Drawable mHighlightDrawable;
        private boolean mInFingerprintLockout;
        private boolean mLaunchedConfirm;
        FingerprintRemoveSidecar.Listener mRemovalListener;
        private FingerprintRemoveSidecar mRemovalSidecar;
        private byte[] mToken;
        private int mUserId;
        
        public FingerprintSettingsFragment() {
            this.mAuthenticateListener = new FingerprintAuthenticateSidecar.Listener() {
                @Override
                public void onAuthenticationError(final int n, final CharSequence charSequence) {
                    FingerprintSettingsFragment.this.mHandler.obtainMessage(1003, n, 0, (Object)charSequence).sendToTarget();
                }
                
                @Override
                public void onAuthenticationFailed() {
                    FingerprintSettingsFragment.this.mHandler.obtainMessage(1002).sendToTarget();
                }
                
                @Override
                public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
                    FingerprintSettingsFragment.this.mHandler.obtainMessage(1004, n, 0, (Object)charSequence).sendToTarget();
                }
                
                @Override
                public void onAuthenticationSucceeded(final FingerprintManager$AuthenticationResult fingerprintManager$AuthenticationResult) {
                    FingerprintSettingsFragment.this.mHandler.obtainMessage(1001, fingerprintManager$AuthenticationResult.getFingerprint().getFingerId(), 0).sendToTarget();
                }
            };
            this.mRemovalListener = new FingerprintRemoveSidecar.Listener() {
                private void updateDialog() {
                    final RenameDialog renameDialog = (RenameDialog)FingerprintSettingsFragment.this.getFragmentManager().findFragmentByTag(RenameDialog.class.getName());
                    if (renameDialog != null) {
                        renameDialog.enableDelete();
                    }
                }
                
                @Override
                public void onRemovalError(final Fingerprint fingerprint, final int n, final CharSequence charSequence) {
                    final Activity activity = FingerprintSettingsFragment.this.getActivity();
                    if (activity != null) {
                        Toast.makeText((Context)activity, charSequence, 0);
                    }
                    this.updateDialog();
                }
                
                @Override
                public void onRemovalSucceeded(final Fingerprint fingerprint) {
                    FingerprintSettingsFragment.this.mHandler.obtainMessage(1000, fingerprint.getFingerId(), 0).sendToTarget();
                    this.updateDialog();
                }
            };
            this.mHandler = new Handler() {
                public void handleMessage(final Message message) {
                    switch (message.what) {
                        case 1003: {
                            FingerprintSettingsFragment.this.handleError(message.arg1, (CharSequence)message.obj);
                            break;
                        }
                        case 1001: {
                            FingerprintSettingsFragment.this.highlightFingerprintItem(message.arg1);
                            FingerprintSettingsFragment.this.retryFingerprint();
                            break;
                        }
                        case 1000: {
                            FingerprintSettingsFragment.this.removeFingerprintPreference(message.arg1);
                            FingerprintSettingsFragment.this.updateAddPreference();
                            FingerprintSettingsFragment.this.retryFingerprint();
                            break;
                        }
                    }
                }
            };
            this.mFingerprintLockoutReset = new Runnable() {
                @Override
                public void run() {
                    FingerprintSettingsFragment.this.mInFingerprintLockout = false;
                    FingerprintSettingsFragment.this.retryFingerprint();
                }
            };
        }
        
        private void addFingerprintItemPreferences(final PreferenceGroup preferenceGroup) {
            preferenceGroup.removeAll();
            final List enrolledFingerprints = this.mFingerprintManager.getEnrolledFingerprints(this.mUserId);
            for (int size = enrolledFingerprints.size(), i = 0; i < size; ++i) {
                final Fingerprint fingerprint = enrolledFingerprints.get(i);
                final FingerprintPreference fingerprintPreference = new FingerprintPreference(preferenceGroup.getContext(), (OnDeleteClickListener)this);
                fingerprintPreference.setKey(genKey(fingerprint.getFingerId()));
                fingerprintPreference.setTitle(fingerprint.getName());
                fingerprintPreference.setFingerprint(fingerprint);
                fingerprintPreference.setPersistent(false);
                fingerprintPreference.setIcon(2131231015);
                if (this.mRemovalSidecar.isRemovingFingerprint(fingerprint.getFingerId())) {
                    fingerprintPreference.setEnabled(false);
                }
                if (this.mFingerprintsRenaming.containsKey(fingerprint.getFingerId())) {
                    fingerprintPreference.setTitle(this.mFingerprintsRenaming.get(fingerprint.getFingerId()));
                }
                preferenceGroup.addPreference(fingerprintPreference);
                fingerprintPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            }
            final Preference preference = new Preference(preferenceGroup.getContext());
            preference.setKey("key_fingerprint_add");
            preference.setTitle(2131887648);
            preference.setIcon(2131231064);
            preferenceGroup.addPreference(preference);
            preference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            this.updateAddPreference();
        }
        
        private PreferenceScreen createPreferenceHierarchy() {
            final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
            if (preferenceScreen != null) {
                preferenceScreen.removeAll();
            }
            this.addPreferencesFromResource(2132082826);
            final PreferenceScreen preferenceScreen2 = this.getPreferenceScreen();
            this.addFingerprintItemPreferences(preferenceScreen2);
            this.setPreferenceScreen(preferenceScreen2);
            return preferenceScreen2;
        }
        
        private static String genKey(final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("key_fingerprint_item_");
            sb.append(n);
            return sb.toString();
        }
        
        private Drawable getHighlightDrawable() {
            if (this.mHighlightDrawable == null) {
                final Activity activity = this.getActivity();
                if (activity != null) {
                    this.mHighlightDrawable = activity.getDrawable(2131231250);
                }
            }
            return this.mHighlightDrawable;
        }
        
        private void highlightFingerprintItem(int n) {
            final FingerprintPreference fingerprintPreference = (FingerprintPreference)this.findPreference(genKey(n));
            final Drawable highlightDrawable = this.getHighlightDrawable();
            if (highlightDrawable != null && fingerprintPreference != null) {
                final View view = fingerprintPreference.getView();
                n = view.getWidth() / 2;
                highlightDrawable.setHotspot((float)n, (float)(view.getHeight() / 2));
                view.setBackground(highlightDrawable);
                view.setPressed(true);
                view.setPressed(false);
                this.mHandler.postDelayed((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        view.setBackground((Drawable)null);
                    }
                }, 500L);
            }
        }
        
        private void launchChooseOrConfirmLock() {
            final Intent intent = new Intent();
            final long preEnroll = this.mFingerprintManager.preEnroll();
            if (!new ChooseLockSettingsHelper(this.getActivity(), this).launchConfirmationActivity(101, this.getString(2131889015), null, null, preEnroll, this.mUserId)) {
                intent.setClassName("com.android.settings", ChooseLockGeneric.class.getName());
                intent.putExtra("minimum_quality", 65536);
                intent.putExtra("hide_disabled_prefs", true);
                intent.putExtra("has_challenge", true);
                intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
                intent.putExtra("challenge", preEnroll);
                intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
                this.startActivityForResult(intent, 102);
            }
        }
        
        private void renameFingerPrint(final int n, final String s) {
            this.mFingerprintManager.rename(n, this.mUserId, s);
            if (!TextUtils.isEmpty((CharSequence)s)) {
                this.mFingerprintsRenaming.put(n, s);
            }
            this.updatePreferences();
        }
        
        private void retryFingerprint() {
            if (this.mRemovalSidecar.inProgress() || this.mFingerprintManager.getEnrolledFingerprints(this.mUserId).size() == 0) {
                return;
            }
            if (this.mLaunchedConfirm) {
                return;
            }
            if (!this.mInFingerprintLockout) {
                this.mAuthenticateSidecar.startAuthentication(this.mUserId);
                this.mAuthenticateSidecar.setListener(this.mAuthenticateListener);
            }
        }
        
        private void showRenameDialog(final Fingerprint fingerprint) {
            final RenameDialog renameDialog = new RenameDialog();
            final Bundle arguments = new Bundle();
            if (this.mFingerprintsRenaming.containsKey(fingerprint.getFingerId())) {
                arguments.putParcelable("fingerprint", (Parcelable)new Fingerprint((CharSequence)this.mFingerprintsRenaming.get(fingerprint.getFingerId()), fingerprint.getGroupId(), fingerprint.getFingerId(), fingerprint.getDeviceId()));
            }
            else {
                arguments.putParcelable("fingerprint", (Parcelable)fingerprint);
            }
            renameDialog.setDeleteInProgress(this.mRemovalSidecar.inProgress());
            renameDialog.setArguments(arguments);
            renameDialog.setTargetFragment((Fragment)this, 0);
            renameDialog.show(this.getFragmentManager(), RenameDialog.class.getName());
        }
        
        private void updateAddPreference() {
            if (this.getActivity() == null) {
                return;
            }
            final int integer = this.getContext().getResources().getInteger(17694789);
            final int size = this.mFingerprintManager.getEnrolledFingerprints(this.mUserId).size();
            final boolean b = false;
            final boolean b2 = size >= integer;
            final boolean inProgress = this.mRemovalSidecar.inProgress();
            String string;
            if (b2) {
                string = this.getContext().getString(2131887647, new Object[] { integer });
            }
            else {
                string = "";
            }
            final Preference preference = this.findPreference("key_fingerprint_add");
            preference.setSummary(string);
            boolean enabled = b;
            if (!b2) {
                enabled = b;
                if (!inProgress) {
                    enabled = true;
                }
            }
            preference.setEnabled(enabled);
        }
        
        private void updatePreferences() {
            this.createPreferenceHierarchy();
            this.retryFingerprint();
        }
        
        void deleteFingerPrint(final Fingerprint fingerprint) {
            this.mRemovalSidecar.startRemove(fingerprint, this.mUserId);
            this.findPreference(genKey(fingerprint.getFingerId())).setEnabled(false);
            this.updateAddPreference();
        }
        
        @Override
        public int getHelpResource() {
            return 2131887811;
        }
        
        @Override
        public int getMetricsCategory() {
            return 49;
        }
        
        protected void handleError(final int n, final CharSequence charSequence) {
            if (n != 5) {
                if (n != 7) {
                    if (n == 9) {
                        this.mInFingerprintLockout = true;
                    }
                }
                else {
                    this.mInFingerprintLockout = true;
                    if (!this.mHandler.hasCallbacks(this.mFingerprintLockoutReset)) {
                        this.mHandler.postDelayed(this.mFingerprintLockoutReset, 30000L);
                    }
                }
                if (this.mInFingerprintLockout) {
                    final Activity activity = this.getActivity();
                    if (activity != null) {
                        Toast.makeText((Context)activity, charSequence, 0).show();
                    }
                }
                this.retryFingerprint();
            }
        }
        
        public void onActivityResult(final int n, final int n2, final Intent intent) {
            super.onActivityResult(n, n2, intent);
            if (n != 102 && n != 101) {
                if (n == 10 && n2 == 3) {
                    final Activity activity = this.getActivity();
                    activity.setResult(3);
                    activity.finish();
                }
            }
            else {
                this.mLaunchedConfirm = false;
                if ((n2 == 1 || n2 == -1) && intent != null) {
                    this.mToken = intent.getByteArrayExtra("hw_auth_token");
                }
            }
            if (this.mToken == null) {
                this.getActivity().finish();
            }
        }
        
        @Override
        public void onCreate(final Bundle bundle) {
            super.onCreate(bundle);
            final Activity activity = this.getActivity();
            this.mFingerprintManager = Utils.getFingerprintManagerOrNull((Context)activity);
            this.mAuthenticateSidecar = (FingerprintAuthenticateSidecar)this.getFragmentManager().findFragmentByTag("authenticate_sidecar");
            if (this.mAuthenticateSidecar == null) {
                this.mAuthenticateSidecar = new FingerprintAuthenticateSidecar();
                this.getFragmentManager().beginTransaction().add((Fragment)this.mAuthenticateSidecar, "authenticate_sidecar").commit();
            }
            this.mAuthenticateSidecar.setFingerprintManager(this.mFingerprintManager);
            this.mRemovalSidecar = (FingerprintRemoveSidecar)this.getFragmentManager().findFragmentByTag("removal_sidecar");
            if (this.mRemovalSidecar == null) {
                this.mRemovalSidecar = new FingerprintRemoveSidecar();
                this.getFragmentManager().beginTransaction().add((Fragment)this.mRemovalSidecar, "removal_sidecar").commit();
            }
            this.mRemovalSidecar.setFingerprintManager(this.mFingerprintManager);
            this.mRemovalSidecar.setListener(this.mRemovalListener);
            final RenameDialog renameDialog = (RenameDialog)this.getFragmentManager().findFragmentByTag(RenameDialog.class.getName());
            if (renameDialog != null) {
                renameDialog.setDeleteInProgress(this.mRemovalSidecar.inProgress());
            }
            this.mFingerprintsRenaming = new HashMap<Integer, String>();
            if (bundle != null) {
                this.mFingerprintsRenaming = (HashMap<Integer, String>)bundle.getSerializable("mFingerprintsRenaming");
                this.mToken = bundle.getByteArray("hw_auth_token");
                this.mLaunchedConfirm = bundle.getBoolean("launched_confirm", false);
            }
            this.mUserId = this.getActivity().getIntent().getIntExtra("android.intent.extra.USER_ID", UserHandle.myUserId());
            if (this.mToken == null && !this.mLaunchedConfirm) {
                this.mLaunchedConfirm = true;
                this.launchChooseOrConfirmLock();
            }
            final FooterPreference footerPreference = this.mFooterPreferenceMixin.createFooterPreference();
            final RestrictedLockUtils.EnforcedAdmin checkIfKeyguardFeaturesDisabled = RestrictedLockUtils.checkIfKeyguardFeaturesDisabled((Context)activity, 32, this.mUserId);
            final AnnotationSpan.LinkInfo linkInfo = new AnnotationSpan.LinkInfo("admin_details", (View.OnClickListener)new _$$Lambda$FingerprintSettings$FingerprintSettingsFragment$yE_lJ_MtxexMYsEgD8_Zrh5Z2iY(activity, checkIfKeyguardFeaturesDisabled));
            final AnnotationSpan.LinkInfo linkInfo2 = new AnnotationSpan.LinkInfo((Context)activity, "url", HelpUtils.getHelpIntent((Context)activity, this.getString(this.getHelpResource()), ((Context)activity).getClass().getName()));
            int n;
            if (checkIfKeyguardFeaturesDisabled != null) {
                n = 2131888986;
            }
            else {
                n = 2131888985;
            }
            footerPreference.setTitle(AnnotationSpan.linkify(this.getText(n), linkInfo2, linkInfo));
        }
        
        @Override
        public void onDeleteClick(final FingerprintPreference fingerprintPreference) {
            final int size = this.mFingerprintManager.getEnrolledFingerprints(this.mUserId).size();
            boolean b = true;
            if (size <= 1) {
                b = false;
            }
            final Fingerprint fingerprint = fingerprintPreference.getFingerprint();
            if (b) {
                if (this.mRemovalSidecar.inProgress()) {
                    Log.d("FingerprintSettings", "Fingerprint delete in progress, skipping");
                    return;
                }
                DeleteFingerprintDialog.newInstance(fingerprint, this).show(this.getFragmentManager(), DeleteFingerprintDialog.class.getName());
            }
            else {
                final ConfirmLastDeleteDialog confirmLastDeleteDialog = new ConfirmLastDeleteDialog();
                final boolean managedProfile = UserManager.get(this.getContext()).isManagedProfile(this.mUserId);
                final Bundle arguments = new Bundle();
                arguments.putParcelable("fingerprint", (Parcelable)fingerprint);
                arguments.putBoolean("isProfileChallengeUser", managedProfile);
                confirmLastDeleteDialog.setArguments(arguments);
                confirmLastDeleteDialog.setTargetFragment((Fragment)this, 0);
                confirmLastDeleteDialog.show(this.getFragmentManager(), ConfirmLastDeleteDialog.class.getName());
            }
        }
        
        @Override
        public void onDestroy() {
            super.onDestroy();
            if (this.getActivity().isFinishing()) {
                final int postEnroll = this.mFingerprintManager.postEnroll();
                if (postEnroll < 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("postEnroll failed: result = ");
                    sb.append(postEnroll);
                    Log.w("FingerprintSettings", sb.toString());
                }
            }
        }
        
        @Override
        public void onPause() {
            super.onPause();
            if (this.mRemovalSidecar != null) {
                this.mRemovalSidecar.setListener(null);
            }
            if (this.mAuthenticateSidecar != null) {
                this.mAuthenticateSidecar.setListener(null);
                this.mAuthenticateSidecar.stopAuthentication();
            }
        }
        
        @Override
        public boolean onPreferenceChange(final Preference preference, final Object o) {
            final String key = preference.getKey();
            if (!"fingerprint_enable_keyguard_toggle".equals(key)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown key:");
                sb.append(key);
                Log.v("FingerprintSettings", sb.toString());
            }
            return true;
        }
        
        @Override
        public boolean onPreferenceTreeClick(final Preference preference) {
            if ("key_fingerprint_add".equals(preference.getKey())) {
                final Intent intent = new Intent();
                intent.setClassName("com.android.settings", FingerprintEnrollEnrolling.class.getName());
                intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
                intent.putExtra("hw_auth_token", this.mToken);
                this.startActivityForResult(intent, 10);
            }
            else if (preference instanceof FingerprintPreference) {
                this.showRenameDialog(((FingerprintPreference)preference).getFingerprint());
            }
            return super.onPreferenceTreeClick(preference);
        }
        
        @Override
        public void onResume() {
            super.onResume();
            this.mInFingerprintLockout = false;
            this.updatePreferences();
            if (this.mRemovalSidecar != null) {
                this.mRemovalSidecar.setListener(this.mRemovalListener);
            }
        }
        
        @Override
        public void onSaveInstanceState(final Bundle bundle) {
            bundle.putByteArray("hw_auth_token", this.mToken);
            bundle.putBoolean("launched_confirm", this.mLaunchedConfirm);
            bundle.putSerializable("mFingerprintsRenaming", (Serializable)this.mFingerprintsRenaming);
        }
        
        protected void removeFingerprintPreference(final int n) {
            final String genKey = genKey(n);
            final Preference preference = this.findPreference(genKey);
            if (preference != null) {
                if (!this.getPreferenceScreen().removePreference(preference)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to remove preference with key ");
                    sb.append(genKey);
                    Log.w("FingerprintSettings", sb.toString());
                }
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Can't find preference to remove: ");
                sb2.append(genKey);
                Log.w("FingerprintSettings", sb2.toString());
            }
        }
        
        public static class ConfirmLastDeleteDialog extends InstrumentedDialogFragment
        {
            private Fingerprint mFp;
            
            @Override
            public int getMetricsCategory() {
                return 571;
            }
            
            public Dialog onCreateDialog(final Bundle bundle) {
                this.mFp = (Fingerprint)this.getArguments().getParcelable("fingerprint");
                final boolean boolean1 = this.getArguments().getBoolean("isProfileChallengeUser");
                final AlertDialog$Builder setTitle = new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887660);
                int message;
                if (boolean1) {
                    message = 2131887659;
                }
                else {
                    message = 2131887658;
                }
                return (Dialog)setTitle.setMessage(message).setPositiveButton(2131887657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        ((FingerprintSettingsFragment)ConfirmLastDeleteDialog.this.getTargetFragment()).deleteFingerPrint(ConfirmLastDeleteDialog.this.mFp);
                        dialogInterface.dismiss();
                    }
                }).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        dialogInterface.dismiss();
                    }
                }).create();
            }
        }
        
        public static class DeleteFingerprintDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
        {
            private AlertDialog mAlertDialog;
            private Fingerprint mFp;
            
            public static DeleteFingerprintDialog newInstance(final Fingerprint fingerprint, final FingerprintSettingsFragment fingerprintSettingsFragment) {
                final DeleteFingerprintDialog deleteFingerprintDialog = new DeleteFingerprintDialog();
                final Bundle arguments = new Bundle();
                arguments.putParcelable("fingerprint", (Parcelable)fingerprint);
                deleteFingerprintDialog.setArguments(arguments);
                deleteFingerprintDialog.setTargetFragment((Fragment)fingerprintSettingsFragment, 0);
                return deleteFingerprintDialog;
            }
            
            public int getMetricsCategory() {
                return 570;
            }
            
            public void onClick(final DialogInterface dialogInterface, int fingerId) {
                if (fingerId == -1) {
                    fingerId = this.mFp.getFingerId();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Removing fpId=");
                    sb.append(fingerId);
                    Log.v("FingerprintSettings", sb.toString());
                    this.mMetricsFeatureProvider.action(this.getContext(), 253, fingerId);
                    ((FingerprintSettingsFragment)this.getTargetFragment()).deleteFingerPrint(this.mFp);
                }
            }
            
            public Dialog onCreateDialog(final Bundle bundle) {
                this.mFp = (Fingerprint)this.getArguments().getParcelable("fingerprint");
                return (Dialog)(this.mAlertDialog = new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)this.getString(2131887650, new Object[] { this.mFp.getName() })).setMessage(2131887649).setPositiveButton(2131888982, (DialogInterface$OnClickListener)this).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).create());
            }
        }
        
        public static class RenameDialog extends InstrumentedDialogFragment
        {
            private AlertDialog mAlertDialog;
            private boolean mDeleteInProgress;
            private EditText mDialogTextField;
            private String mFingerName;
            private Fingerprint mFp;
            private Boolean mTextHadFocus;
            private int mTextSelectionEnd;
            private int mTextSelectionStart;
            
            public void enableDelete() {
                this.mDeleteInProgress = false;
                if (this.mAlertDialog != null) {
                    this.mAlertDialog.getButton(-2).setEnabled(true);
                }
            }
            
            @Override
            public int getMetricsCategory() {
                return 570;
            }
            
            public Dialog onCreateDialog(final Bundle bundle) {
                this.mFp = (Fingerprint)this.getArguments().getParcelable("fingerprint");
                if (bundle != null) {
                    this.mFingerName = bundle.getString("fingerName");
                    this.mTextHadFocus = bundle.getBoolean("textHadFocus");
                    this.mTextSelectionStart = bundle.getInt("startSelection");
                    this.mTextSelectionEnd = bundle.getInt("endSelection");
                }
                (this.mAlertDialog = new AlertDialog$Builder((Context)this.getActivity()).setView(2131558569).setPositiveButton(2131888984, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        final String string = RenameDialog.this.mDialogTextField.getText().toString();
                        final CharSequence name = RenameDialog.this.mFp.getName();
                        if (!TextUtils.equals((CharSequence)string, name)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("rename ");
                            sb.append((Object)name);
                            sb.append(" to ");
                            sb.append(string);
                            Log.d("FingerprintSettings", sb.toString());
                            RenameDialog.this.mMetricsFeatureProvider.action(RenameDialog.this.getContext(), 254, RenameDialog.this.mFp.getFingerId());
                            ((FingerprintSettingsFragment)RenameDialog.this.getTargetFragment()).renameFingerPrint(RenameDialog.this.mFp.getFingerId(), string);
                        }
                        dialogInterface.dismiss();
                    }
                }).create()).setOnShowListener((DialogInterface$OnShowListener)new DialogInterface$OnShowListener() {
                    public void onShow(final DialogInterface dialogInterface) {
                        RenameDialog.this.mDialogTextField = (EditText)RenameDialog.this.mAlertDialog.findViewById(2131362154);
                        CharSequence text;
                        if (RenameDialog.this.mFingerName == null) {
                            text = RenameDialog.this.mFp.getName();
                        }
                        else {
                            text = RenameDialog.this.mFingerName;
                        }
                        RenameDialog.this.mDialogTextField.setText(text);
                        if (RenameDialog.this.mTextHadFocus == null) {
                            RenameDialog.this.mDialogTextField.selectAll();
                        }
                        else {
                            RenameDialog.this.mDialogTextField.setSelection(RenameDialog.this.mTextSelectionStart, RenameDialog.this.mTextSelectionEnd);
                        }
                        if (RenameDialog.this.mDeleteInProgress) {
                            RenameDialog.this.mAlertDialog.getButton(-2).setEnabled(false);
                        }
                        RenameDialog.this.mDialogTextField.requestFocus();
                    }
                });
                if (this.mTextHadFocus == null || this.mTextHadFocus) {
                    this.mAlertDialog.getWindow().setSoftInputMode(5);
                }
                return (Dialog)this.mAlertDialog;
            }
            
            public void onSaveInstanceState(final Bundle bundle) {
                super.onSaveInstanceState(bundle);
                if (this.mDialogTextField != null) {
                    bundle.putString("fingerName", this.mDialogTextField.getText().toString());
                    bundle.putBoolean("textHadFocus", this.mDialogTextField.hasFocus());
                    bundle.putInt("startSelection", this.mDialogTextField.getSelectionStart());
                    bundle.putInt("endSelection", this.mDialogTextField.getSelectionEnd());
                }
            }
            
            public void setDeleteInProgress(final boolean mDeleteInProgress) {
                this.mDeleteInProgress = mDeleteInProgress;
            }
        }
    }
}
