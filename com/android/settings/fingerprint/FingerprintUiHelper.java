package com.android.settings.fingerprint;

import android.os.Handler;
import android.hardware.fingerprint.FingerprintManager$CryptoObject;
import android.hardware.fingerprint.FingerprintManager$AuthenticationResult;
import com.android.settings.Utils;
import android.widget.ImageView;
import android.hardware.fingerprint.FingerprintManager;
import android.widget.TextView;
import android.os.CancellationSignal;
import android.hardware.fingerprint.FingerprintManager$AuthenticationCallback;

public class FingerprintUiHelper extends FingerprintManager$AuthenticationCallback
{
    private Callback mCallback;
    private CancellationSignal mCancellationSignal;
    private TextView mErrorTextView;
    private FingerprintManager mFingerprintManager;
    private ImageView mIcon;
    private Runnable mResetErrorTextRunnable;
    private int mUserId;
    
    public FingerprintUiHelper(final ImageView mIcon, final TextView mErrorTextView, final Callback mCallback, final int mUserId) {
        this.mResetErrorTextRunnable = new Runnable() {
            @Override
            public void run() {
                FingerprintUiHelper.this.mErrorTextView.setText((CharSequence)"");
                FingerprintUiHelper.this.mIcon.setImageResource(2131231014);
            }
        };
        this.mFingerprintManager = Utils.getFingerprintManagerOrNull(mIcon.getContext());
        this.mIcon = mIcon;
        this.mErrorTextView = mErrorTextView;
        this.mCallback = mCallback;
        this.mUserId = mUserId;
    }
    
    private void setFingerprintIconVisibility(final boolean b) {
        final ImageView mIcon = this.mIcon;
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        mIcon.setVisibility(visibility);
        this.mCallback.onFingerprintIconVisibilityChanged(b);
    }
    
    private void showError(final CharSequence text) {
        if (!this.isListening()) {
            return;
        }
        this.mIcon.setImageResource(2131231016);
        this.mErrorTextView.setText(text);
        this.mErrorTextView.removeCallbacks(this.mResetErrorTextRunnable);
        this.mErrorTextView.postDelayed(this.mResetErrorTextRunnable, 1300L);
    }
    
    public boolean isListening() {
        return this.mCancellationSignal != null && !this.mCancellationSignal.isCanceled();
    }
    
    public void onAuthenticationError(final int n, final CharSequence charSequence) {
        if (n == 5) {
            return;
        }
        this.showError(charSequence);
        this.setFingerprintIconVisibility(false);
    }
    
    public void onAuthenticationFailed() {
        this.showError(this.mIcon.getResources().getString(2131887662));
    }
    
    public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
        this.showError(charSequence);
    }
    
    public void onAuthenticationSucceeded(final FingerprintManager$AuthenticationResult fingerprintManager$AuthenticationResult) {
        this.mIcon.setImageResource(2131231019);
        this.mCallback.onAuthenticated();
    }
    
    public void startListening() {
        if (this.mFingerprintManager != null && this.mFingerprintManager.isHardwareDetected() && this.mFingerprintManager.getEnrolledFingerprints(this.mUserId).size() > 0) {
            this.mCancellationSignal = new CancellationSignal();
            this.mFingerprintManager.setActiveUser(this.mUserId);
            this.mFingerprintManager.authenticate((FingerprintManager$CryptoObject)null, this.mCancellationSignal, 0, (FingerprintManager$AuthenticationCallback)this, (Handler)null, this.mUserId);
            this.setFingerprintIconVisibility(true);
            this.mIcon.setImageResource(2131231014);
        }
    }
    
    public void stopListening() {
        if (this.mCancellationSignal != null) {
            this.mCancellationSignal.cancel();
            this.mCancellationSignal = null;
        }
    }
    
    public interface Callback
    {
        void onAuthenticated();
        
        void onFingerprintIconVisibilityChanged(final boolean p0);
    }
}
