package com.android.settings.fingerprint;

import android.os.Bundle;
import android.content.Context;
import com.android.settings.Utils;
import android.app.Activity;
import java.util.ArrayList;
import android.os.Handler;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.hardware.fingerprint.FingerprintManager$EnrollmentCallback;
import com.android.settings.core.InstrumentedFragment;

public class FingerprintEnrollSidecar extends InstrumentedFragment
{
    private boolean mDone;
    private boolean mEnrolling;
    private FingerprintManager$EnrollmentCallback mEnrollmentCallback;
    private CancellationSignal mEnrollmentCancel;
    private int mEnrollmentRemaining;
    private int mEnrollmentSteps;
    private FingerprintManager mFingerprintManager;
    private Handler mHandler;
    private Listener mListener;
    private ArrayList<QueuedEvent> mQueuedEvents;
    private final Runnable mTimeoutRunnable;
    private byte[] mToken;
    private int mUserId;
    
    public FingerprintEnrollSidecar() {
        this.mEnrollmentSteps = -1;
        this.mEnrollmentRemaining = 0;
        this.mHandler = new Handler();
        this.mEnrollmentCallback = new FingerprintManager$EnrollmentCallback() {
            public void onEnrollmentError(final int n, final CharSequence charSequence) {
                if (FingerprintEnrollSidecar.this.mListener != null) {
                    FingerprintEnrollSidecar.this.mListener.onEnrollmentError(n, charSequence);
                }
                else {
                    FingerprintEnrollSidecar.this.mQueuedEvents.add(new QueuedEnrollmentError(n, charSequence));
                }
                FingerprintEnrollSidecar.this.mEnrolling = false;
            }
            
            public void onEnrollmentHelp(final int n, final CharSequence charSequence) {
                if (FingerprintEnrollSidecar.this.mListener != null) {
                    FingerprintEnrollSidecar.this.mListener.onEnrollmentHelp(charSequence);
                }
                else {
                    FingerprintEnrollSidecar.this.mQueuedEvents.add(new QueuedEnrollmentHelp(n, charSequence));
                }
            }
            
            public void onEnrollmentProgress(final int n) {
                if (FingerprintEnrollSidecar.this.mEnrollmentSteps == -1) {
                    FingerprintEnrollSidecar.this.mEnrollmentSteps = n;
                }
                FingerprintEnrollSidecar.this.mEnrollmentRemaining = n;
                FingerprintEnrollSidecar.this.mDone = (n == 0);
                if (FingerprintEnrollSidecar.this.mListener != null) {
                    FingerprintEnrollSidecar.this.mListener.onEnrollmentProgressChange(FingerprintEnrollSidecar.this.mEnrollmentSteps, n);
                }
                else {
                    FingerprintEnrollSidecar.this.mQueuedEvents.add(new QueuedEnrollmentProgress(FingerprintEnrollSidecar.this.mEnrollmentSteps, n));
                }
            }
        };
        this.mTimeoutRunnable = new Runnable() {
            @Override
            public void run() {
                FingerprintEnrollSidecar.this.cancelEnrollment();
            }
        };
        this.mQueuedEvents = new ArrayList<QueuedEvent>();
    }
    
    private void startEnrollment() {
        this.mHandler.removeCallbacks(this.mTimeoutRunnable);
        this.mEnrollmentSteps = -1;
        this.mEnrollmentCancel = new CancellationSignal();
        if (this.mUserId != -10000) {
            this.mFingerprintManager.setActiveUser(this.mUserId);
        }
        this.mFingerprintManager.enroll(this.mToken, this.mEnrollmentCancel, 0, this.mUserId, this.mEnrollmentCallback);
        this.mEnrolling = true;
    }
    
    boolean cancelEnrollment() {
        this.mHandler.removeCallbacks(this.mTimeoutRunnable);
        if (this.mEnrolling) {
            this.mEnrollmentCancel.cancel();
            this.mEnrolling = false;
            this.mEnrollmentSteps = -1;
            return true;
        }
        return false;
    }
    
    public int getEnrollmentRemaining() {
        return this.mEnrollmentRemaining;
    }
    
    public int getEnrollmentSteps() {
        return this.mEnrollmentSteps;
    }
    
    @Override
    public int getMetricsCategory() {
        return 245;
    }
    
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        this.mFingerprintManager = Utils.getFingerprintManagerOrNull((Context)activity);
        this.mToken = activity.getIntent().getByteArrayExtra("hw_auth_token");
        this.mUserId = activity.getIntent().getIntExtra("android.intent.extra.USER_ID", -10000);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setRetainInstance(true);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (!this.mEnrolling) {
            this.startEnrollment();
        }
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (!this.getActivity().isChangingConfigurations()) {
            this.cancelEnrollment();
        }
    }
    
    public void setListener(final Listener mListener) {
        this.mListener = mListener;
        if (this.mListener != null) {
            for (int i = 0; i < this.mQueuedEvents.size(); ++i) {
                this.mQueuedEvents.get(i).send(this.mListener);
            }
            this.mQueuedEvents.clear();
        }
    }
    
    public interface Listener
    {
        void onEnrollmentError(final int p0, final CharSequence p1);
        
        void onEnrollmentHelp(final CharSequence p0);
        
        void onEnrollmentProgressChange(final int p0, final int p1);
    }
    
    private class QueuedEnrollmentError extends QueuedEvent
    {
        int errMsgId;
        CharSequence errString;
        
        public QueuedEnrollmentError(final int errMsgId, final CharSequence errString) {
            this.errMsgId = errMsgId;
            this.errString = errString;
        }
        
        @Override
        public void send(final Listener listener) {
            listener.onEnrollmentError(this.errMsgId, this.errString);
        }
    }
    
    private class QueuedEnrollmentHelp extends QueuedEvent
    {
        int helpMsgId;
        CharSequence helpString;
        
        public QueuedEnrollmentHelp(final int helpMsgId, final CharSequence helpString) {
            this.helpMsgId = helpMsgId;
            this.helpString = helpString;
        }
        
        @Override
        public void send(final Listener listener) {
            listener.onEnrollmentHelp(this.helpString);
        }
    }
    
    private class QueuedEnrollmentProgress extends QueuedEvent
    {
        int enrollmentSteps;
        int remaining;
        
        public QueuedEnrollmentProgress(final int enrollmentSteps, final int remaining) {
            this.enrollmentSteps = enrollmentSteps;
            this.remaining = remaining;
        }
        
        @Override
        public void send(final Listener listener) {
            listener.onEnrollmentProgressChange(this.enrollmentSteps, this.remaining);
        }
    }
    
    private abstract class QueuedEvent
    {
        public abstract void send(final Listener p0);
    }
}
