package com.android.settings.fingerprint;

import android.content.DialogInterface$OnShowListener;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.widget.EditText;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import java.io.Serializable;
import android.os.UserManager;
import android.util.Log;
import com.android.settingslib.widget.FooterPreference;
import com.android.settingslib.HelpUtils;
import com.android.settings.utils.AnnotationSpan;
import android.os.UserHandle;
import com.android.settings.Utils;
import android.os.Parcelable;
import android.os.Bundle;
import android.text.TextUtils;
import com.android.settings.password.ChooseLockGeneric;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import java.util.List;
import android.support.v7.preference.PreferenceGroup;
import android.os.Message;
import android.content.Context;
import android.widget.Toast;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager$AuthenticationResult;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import java.util.HashMap;
import android.hardware.fingerprint.FingerprintManager;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;
import android.view.View;
import com.android.settingslib.RestrictedLockUtils;
import android.app.Activity;
import android.view.View.OnClickListener;

public final class _$$Lambda$FingerprintSettings$FingerprintSettingsFragment$yE_lJ_MtxexMYsEgD8_Zrh5Z2iY implements View.OnClickListener
{
    public final void onClick(final View view) {
        FingerprintSettings.FingerprintSettingsFragment.lambda$onCreate$0(this.f$0, this.f$1, view);
    }
}
