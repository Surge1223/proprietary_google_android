package com.android.settings.fingerprint;

import android.app.FragmentManager;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import com.android.settings.SetupWizardUtils;
import android.content.Context;
import android.content.Intent;

public class SetupFingerprintEnrollFindSensor extends FingerprintEnrollFindSensor
{
    @Override
    protected int getContentView() {
        return 2131558560;
    }
    
    @Override
    protected Intent getEnrollingIntent() {
        final Intent intent = new Intent((Context)this, (Class)SetupFingerprintEnrollEnrolling.class);
        intent.putExtra("hw_auth_token", this.mToken);
        if (this.mUserId != -10000) {
            intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
        }
        SetupWizardUtils.copySetupExtras(this.getIntent(), intent);
        return intent;
    }
    
    @Override
    public int getMetricsCategory() {
        return 247;
    }
    
    @Override
    protected void onSkipButtonClick() {
        new SkipFingerprintDialog().show(this.getFragmentManager());
    }
    
    public static class SkipFingerprintDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        public int getMetricsCategory() {
            return 573;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            if (n == -1) {
                final Activity activity = this.getActivity();
                if (activity != null) {
                    activity.setResult(2);
                    activity.finish();
                }
            }
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)this.onCreateDialogBuilder().create();
        }
        
        public AlertDialog$Builder onCreateDialogBuilder() {
            return new AlertDialog$Builder(this.getContext()).setTitle(2131889071).setPositiveButton(2131889174, (DialogInterface$OnClickListener)this).setNegativeButton(2131887735, (DialogInterface$OnClickListener)this).setMessage(2131889070);
        }
        
        public void show(final FragmentManager fragmentManager) {
            this.show(fragmentManager, "skip_dialog");
        }
    }
}
