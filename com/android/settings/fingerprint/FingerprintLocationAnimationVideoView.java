package com.android.settings.fingerprint;

import android.view.View$MeasureSpec;
import android.media.MediaPlayer$OnInfoListener;
import android.media.MediaPlayer$OnPreparedListener;
import android.view.Surface;
import android.graphics.SurfaceTexture;
import android.view.TextureView$SurfaceTextureListener;
import android.content.res.Resources;
import android.net.Uri;
import android.util.AttributeSet;
import android.content.Context;
import android.media.MediaPlayer;
import android.view.TextureView;

public class FingerprintLocationAnimationVideoView extends TextureView implements FingerprintFindSensorAnimation
{
    protected float mAspect;
    protected MediaPlayer mMediaPlayer;
    
    public FingerprintLocationAnimationVideoView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mAspect = 1.0f;
    }
    
    protected static Uri resourceEntryToUri(final Context context, final int n) {
        final Resources resources = context.getResources();
        final StringBuilder sb = new StringBuilder();
        sb.append("android.resource://");
        sb.append(resources.getResourcePackageName(n));
        sb.append('/');
        sb.append(resources.getResourceTypeName(n));
        sb.append('/');
        sb.append(resources.getResourceEntryName(n));
        return Uri.parse(sb.toString());
    }
    
    MediaPlayer createMediaPlayer(final Context context, final Uri uri) {
        return MediaPlayer.create(this.mContext, uri);
    }
    
    protected Uri getFingerprintLocationAnimation() {
        return resourceEntryToUri(this.getContext(), 2131820549);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.setSurfaceTextureListener((TextureView$SurfaceTextureListener)new TextureView$SurfaceTextureListener() {
            private SurfaceTexture mTextureToDestroy = null;
            
            public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, final int n, final int n2) {
                FingerprintLocationAnimationVideoView.this.setVisibility(4);
                final Uri fingerprintLocationAnimation = FingerprintLocationAnimationVideoView.this.getFingerprintLocationAnimation();
                if (FingerprintLocationAnimationVideoView.this.mMediaPlayer != null) {
                    FingerprintLocationAnimationVideoView.this.mMediaPlayer.release();
                }
                if (this.mTextureToDestroy != null) {
                    this.mTextureToDestroy.release();
                    this.mTextureToDestroy = null;
                }
                FingerprintLocationAnimationVideoView.this.mMediaPlayer = FingerprintLocationAnimationVideoView.this.createMediaPlayer(FingerprintLocationAnimationVideoView.this.mContext, fingerprintLocationAnimation);
                if (FingerprintLocationAnimationVideoView.this.mMediaPlayer == null) {
                    return;
                }
                FingerprintLocationAnimationVideoView.this.mMediaPlayer.setSurface(new Surface(surfaceTexture));
                FingerprintLocationAnimationVideoView.this.mMediaPlayer.setOnPreparedListener((MediaPlayer$OnPreparedListener)new MediaPlayer$OnPreparedListener() {
                    public void onPrepared(final MediaPlayer mediaPlayer) {
                        mediaPlayer.setLooping(true);
                    }
                });
                FingerprintLocationAnimationVideoView.this.mMediaPlayer.setOnInfoListener((MediaPlayer$OnInfoListener)new MediaPlayer$OnInfoListener() {
                    public boolean onInfo(final MediaPlayer mediaPlayer, final int n, final int n2) {
                        if (n == 3) {
                            FingerprintLocationAnimationVideoView.this.setVisibility(0);
                        }
                        return false;
                    }
                });
                FingerprintLocationAnimationVideoView.this.mAspect = FingerprintLocationAnimationVideoView.this.mMediaPlayer.getVideoHeight() / FingerprintLocationAnimationVideoView.this.mMediaPlayer.getVideoWidth();
                FingerprintLocationAnimationVideoView.this.requestLayout();
                FingerprintLocationAnimationVideoView.this.startAnimation();
            }
            
            public boolean onSurfaceTextureDestroyed(final SurfaceTexture mTextureToDestroy) {
                this.mTextureToDestroy = mTextureToDestroy;
                return false;
            }
            
            public void onSurfaceTextureSizeChanged(final SurfaceTexture surfaceTexture, final int n, final int n2) {
            }
            
            public void onSurfaceTextureUpdated(final SurfaceTexture surfaceTexture) {
            }
        });
    }
    
    protected void onMeasure(final int n, int size) {
        size = View$MeasureSpec.getSize(n);
        super.onMeasure(n, View$MeasureSpec.makeMeasureSpec(Math.round(this.mAspect * size), 1073741824));
    }
    
    public void pauseAnimation() {
        if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
        }
    }
    
    public void startAnimation() {
        if (this.mMediaPlayer != null && !this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.start();
        }
    }
    
    public void stopAnimation() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
        }
    }
}
