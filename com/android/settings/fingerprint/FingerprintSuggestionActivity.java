package com.android.settings.fingerprint;

import android.widget.Button;
import android.hardware.fingerprint.FingerprintManager;
import com.android.settings.Utils;
import android.content.ComponentName;
import android.app.admin.DevicePolicyManager;
import android.content.Context;

public class FingerprintSuggestionActivity extends SetupFingerprintEnrollIntroduction
{
    static boolean isFingerprintEnabled(final Context context) {
        return (((DevicePolicyManager)context.getSystemService("device_policy")).getKeyguardDisabledFeatures((ComponentName)null, context.getUserId()) & 0x20) == 0x0;
    }
    
    private static boolean isNotSingleFingerprintEnrolled(final Context context) {
        final FingerprintManager fingerprintManagerOrNull = Utils.getFingerprintManagerOrNull(context);
        boolean b = true;
        if (fingerprintManagerOrNull != null) {
            b = (fingerprintManagerOrNull.getEnrolledFingerprints().size() != 1 && b);
        }
        return b;
    }
    
    public static boolean isSuggestionComplete(final Context context) {
        return !Utils.hasFingerprintHardware(context) || !isFingerprintEnabled(context) || isNotSingleFingerprintEnrolled(context);
    }
    
    public void finish() {
        this.setResult(0);
        super.finish();
    }
    
    @Override
    protected void initViews() {
        super.initViews();
        ((Button)this.findViewById(2131362149)).setText(2131888997);
    }
}
