package com.android.settings.fingerprint;

import android.content.Context;

public class FingerprintProfileStatusPreferenceController extends FingerprintStatusPreferenceController
{
    public static final String KEY_FINGERPRINT_SETTINGS = "fingerprint_settings_profile";
    
    public FingerprintProfileStatusPreferenceController(final Context context) {
        super(context, "fingerprint_settings_profile");
    }
    
    @Override
    protected int getUserId() {
        return this.mProfileChallengeUserId;
    }
    
    @Override
    protected boolean isUserSupported() {
        return this.mProfileChallengeUserId != -10000 && this.mLockPatternUtils.isSeparateProfileChallengeAllowed(this.mProfileChallengeUserId);
    }
}
