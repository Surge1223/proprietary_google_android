package com.android.settings.fingerprint;

import android.os.Handler;
import android.hardware.fingerprint.FingerprintManager$CryptoObject;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.hardware.fingerprint.FingerprintManager$AuthenticationResult;
import android.hardware.fingerprint.FingerprintManager$AuthenticationCallback;
import com.android.settings.core.InstrumentedFragment;

public class FingerprintAuthenticateSidecar extends InstrumentedFragment
{
    private FingerprintManager$AuthenticationCallback mAuthenticationCallback;
    private AuthenticationError mAuthenticationError;
    private FingerprintManager$AuthenticationResult mAuthenticationResult;
    private CancellationSignal mCancellationSignal;
    private FingerprintManager mFingerprintManager;
    private Listener mListener;
    
    public FingerprintAuthenticateSidecar() {
        this.mAuthenticationCallback = new FingerprintManager$AuthenticationCallback() {
            public void onAuthenticationError(final int n, final CharSequence charSequence) {
                FingerprintAuthenticateSidecar.this.mCancellationSignal = null;
                if (FingerprintAuthenticateSidecar.this.mListener != null) {
                    FingerprintAuthenticateSidecar.this.mListener.onAuthenticationError(n, charSequence);
                }
                else {
                    FingerprintAuthenticateSidecar.this.mAuthenticationError = new AuthenticationError(n, charSequence);
                    FingerprintAuthenticateSidecar.this.mAuthenticationResult = null;
                }
            }
            
            public void onAuthenticationFailed() {
                if (FingerprintAuthenticateSidecar.this.mListener != null) {
                    FingerprintAuthenticateSidecar.this.mListener.onAuthenticationFailed();
                }
            }
            
            public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
                if (FingerprintAuthenticateSidecar.this.mListener != null) {
                    FingerprintAuthenticateSidecar.this.mListener.onAuthenticationHelp(n, charSequence);
                }
            }
            
            public void onAuthenticationSucceeded(final FingerprintManager$AuthenticationResult fingerprintManager$AuthenticationResult) {
                FingerprintAuthenticateSidecar.this.mCancellationSignal = null;
                if (FingerprintAuthenticateSidecar.this.mListener != null) {
                    FingerprintAuthenticateSidecar.this.mListener.onAuthenticationSucceeded(fingerprintManager$AuthenticationResult);
                }
                else {
                    FingerprintAuthenticateSidecar.this.mAuthenticationResult = fingerprintManager$AuthenticationResult;
                    FingerprintAuthenticateSidecar.this.mAuthenticationError = null;
                }
            }
        };
    }
    
    @Override
    public int getMetricsCategory() {
        return 1221;
    }
    
    public void setFingerprintManager(final FingerprintManager mFingerprintManager) {
        this.mFingerprintManager = mFingerprintManager;
    }
    
    public void setListener(final Listener mListener) {
        if (this.mListener == null && mListener != null) {
            if (this.mAuthenticationResult != null) {
                mListener.onAuthenticationSucceeded(this.mAuthenticationResult);
                this.mAuthenticationResult = null;
            }
            if (this.mAuthenticationError != null && this.mAuthenticationError.error != 5) {
                mListener.onAuthenticationError(this.mAuthenticationError.error, this.mAuthenticationError.errorString);
                this.mAuthenticationError = null;
            }
        }
        this.mListener = mListener;
    }
    
    public void startAuthentication(final int n) {
        this.mCancellationSignal = new CancellationSignal();
        this.mFingerprintManager.authenticate((FingerprintManager$CryptoObject)null, this.mCancellationSignal, 0, this.mAuthenticationCallback, (Handler)null, n);
    }
    
    public void stopAuthentication() {
        if (this.mCancellationSignal != null && !this.mCancellationSignal.isCanceled()) {
            this.mCancellationSignal.cancel();
        }
        this.mCancellationSignal = null;
    }
    
    private class AuthenticationError
    {
        int error;
        CharSequence errorString;
        
        public AuthenticationError(final int error, final CharSequence errorString) {
            this.error = error;
            this.errorString = errorString;
        }
    }
    
    public interface Listener
    {
        void onAuthenticationError(final int p0, final CharSequence p1);
        
        void onAuthenticationFailed();
        
        void onAuthenticationHelp(final int p0, final CharSequence p1);
        
        void onAuthenticationSucceeded(final FingerprintManager$AuthenticationResult p0);
    }
}
