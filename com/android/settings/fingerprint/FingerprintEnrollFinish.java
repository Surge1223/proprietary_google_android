package com.android.settings.fingerprint;

import android.hardware.fingerprint.FingerprintManager;
import android.view.View.OnClickListener;
import android.content.Context;
import com.android.settings.Utils;
import android.widget.Button;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;

public class FingerprintEnrollFinish extends FingerprintEnrollBase
{
    public int getMetricsCategory() {
        return 242;
    }
    
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1 && n2 != 0) {
            this.setResult(n2, intent);
            this.finish();
        }
        else {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Override
    public void onClick(final View view) {
        if (view.getId() == 2131361836) {
            this.startActivityForResult(this.getEnrollingIntent(), 1);
        }
        super.onClick(view);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131558564);
        this.setHeaderText(2131888996);
    }
    
    @Override
    protected void onNextButtonClick() {
        this.setResult(1);
        this.finish();
    }
    
    protected void onResume() {
        super.onResume();
        final Button button = (Button)this.findViewById(2131361836);
        final FingerprintManager fingerprintManagerOrNull = Utils.getFingerprintManagerOrNull((Context)this);
        boolean b = false;
        if (fingerprintManagerOrNull != null) {
            b = (fingerprintManagerOrNull.getEnrolledFingerprints(this.mUserId).size() >= this.getResources().getInteger(17694789));
        }
        if (b) {
            button.setVisibility(4);
        }
        else {
            button.setOnClickListener((View.OnClickListener)this);
        }
    }
}
