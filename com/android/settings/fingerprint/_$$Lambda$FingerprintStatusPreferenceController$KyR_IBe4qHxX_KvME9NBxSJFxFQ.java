package com.android.settings.fingerprint;

import java.util.List;
import android.content.Intent;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import android.hardware.fingerprint.FingerprintManager;
import com.android.settings.core.BasePreferenceController;
import android.support.v7.preference.Preference;

public final class _$$Lambda$FingerprintStatusPreferenceController$KyR_IBe4qHxX_KvME9NBxSJFxFQ implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return FingerprintStatusPreferenceController.lambda$updateState$0(this.f$0, this.f$1, preference);
    }
}
