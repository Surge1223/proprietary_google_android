package com.android.settings.fingerprint;

import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.app.Fragment;
import android.app.Activity;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.Context;
import com.android.settings.Utils;

public class FingerprintEnrollFindSensor extends FingerprintEnrollBase
{
    private FingerprintFindSensorAnimation mAnimation;
    private boolean mLaunchedConfirmLock;
    private boolean mNextClicked;
    private FingerprintEnrollSidecar mSidecar;
    
    private void launchConfirmLock() {
        final long preEnroll = Utils.getFingerprintManagerOrNull((Context)this).preEnroll();
        final ChooseLockSettingsHelper chooseLockSettingsHelper = new ChooseLockSettingsHelper(this);
        boolean b;
        if (this.mUserId == -10000) {
            b = chooseLockSettingsHelper.launchConfirmationActivity(1, this.getString(2131889015), null, null, preEnroll);
        }
        else {
            b = chooseLockSettingsHelper.launchConfirmationActivity(1, this.getString(2131889015), null, null, preEnroll, this.mUserId);
        }
        if (!b) {
            this.finish();
        }
        else {
            this.mLaunchedConfirmLock = true;
        }
    }
    
    private void proceedToEnrolling(final boolean b) {
        if (this.mSidecar != null) {
            if (b && this.mSidecar.cancelEnrollment()) {
                return;
            }
            this.getFragmentManager().beginTransaction().remove((Fragment)this.mSidecar).commitAllowingStateLoss();
            this.mSidecar = null;
            this.startActivityForResult(this.getEnrollingIntent(), 2);
        }
    }
    
    private void startLookingForFingerprint() {
        this.mSidecar = (FingerprintEnrollSidecar)this.getFragmentManager().findFragmentByTag("sidecar");
        if (this.mSidecar == null) {
            this.mSidecar = new FingerprintEnrollSidecar();
            this.getFragmentManager().beginTransaction().add((Fragment)this.mSidecar, "sidecar").commit();
        }
        this.mSidecar.setListener((FingerprintEnrollSidecar.Listener)new FingerprintEnrollSidecar.Listener() {
            @Override
            public void onEnrollmentError(final int n, final CharSequence charSequence) {
                if (FingerprintEnrollFindSensor.this.mNextClicked && n == 5) {
                    FingerprintEnrollFindSensor.this.mNextClicked = false;
                    FingerprintEnrollFindSensor.this.proceedToEnrolling(false);
                }
            }
            
            @Override
            public void onEnrollmentHelp(final CharSequence charSequence) {
            }
            
            @Override
            public void onEnrollmentProgressChange(final int n, final int n2) {
                FingerprintEnrollFindSensor.this.mNextClicked = true;
                FingerprintEnrollFindSensor.this.proceedToEnrolling(true);
            }
        });
    }
    
    protected int getContentView() {
        return 2131558560;
    }
    
    public int getMetricsCategory() {
        return 241;
    }
    
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1) {
            if (n2 == -1) {
                this.mToken = intent.getByteArrayExtra("hw_auth_token");
                this.overridePendingTransition(2130772030, 2130772031);
                this.getIntent().putExtra("hw_auth_token", this.mToken);
                this.startLookingForFingerprint();
            }
            else {
                this.finish();
            }
        }
        else if (n == 2) {
            if (n2 == 1) {
                this.setResult(1);
                this.finish();
            }
            else if (n2 == 2) {
                this.setResult(2);
                this.finish();
            }
            else if (n2 == 3) {
                this.setResult(3);
                this.finish();
            }
            else if (Utils.getFingerprintManagerOrNull((Context)this).getEnrolledFingerprints().size() >= this.getResources().getInteger(17694789)) {
                this.finish();
            }
            else {
                this.startLookingForFingerprint();
            }
        }
        else {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Override
    public void onClick(final View view) {
        if (view.getId() != 2131362617) {
            super.onClick(view);
        }
        else {
            this.onSkipButtonClick();
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(this.getContentView());
        ((Button)this.findViewById(2131362617)).setOnClickListener((View.OnClickListener)this);
        this.setHeaderText(2131888994);
        if (bundle != null) {
            this.mLaunchedConfirmLock = bundle.getBoolean("launched_confirm_lock");
            this.mToken = bundle.getByteArray("hw_auth_token");
        }
        if (this.mToken == null && !this.mLaunchedConfirmLock) {
            this.launchConfirmLock();
        }
        else if (this.mToken != null) {
            this.startLookingForFingerprint();
        }
        final View viewById = this.findViewById(2131362156);
        if (viewById instanceof FingerprintFindSensorAnimation) {
            this.mAnimation = (FingerprintFindSensorAnimation)viewById;
        }
        else {
            this.mAnimation = null;
        }
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (this.mAnimation != null) {
            this.mAnimation.stopAnimation();
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("launched_confirm_lock", this.mLaunchedConfirmLock);
        bundle.putByteArray("hw_auth_token", this.mToken);
    }
    
    protected void onSkipButtonClick() {
        this.setResult(2);
        this.finish();
    }
    
    protected void onStart() {
        super.onStart();
        if (this.mAnimation != null) {
            this.mAnimation.startAnimation();
        }
    }
    
    protected void onStop() {
        super.onStop();
        if (this.mAnimation != null) {
            this.mAnimation.pauseAnimation();
        }
    }
}
