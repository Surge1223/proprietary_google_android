package com.android.settings.fingerprint;

import android.hardware.fingerprint.FingerprintManager;
import com.android.settingslib.RestrictedLockUtils;
import android.os.Bundle;
import android.content.ActivityNotFoundException;
import android.util.Log;
import com.android.settingslib.HelpUtils;
import android.view.View;
import android.widget.Button;
import com.android.settings.password.ChooseLockGeneric;
import android.app.Activity;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.Intent;
import android.content.Context;
import com.android.settings.Utils;
import android.os.UserManager;
import android.widget.TextView;
import com.android.setupwizardlib.span.LinkSpan;
import android.view.View.OnClickListener;

public class FingerprintEnrollIntroduction extends FingerprintEnrollBase implements View.OnClickListener, OnClickListener
{
    private TextView mErrorText;
    private boolean mFingerprintUnlockDisabledByAdmin;
    private boolean mHasPassword;
    private UserManager mUserManager;
    
    private void launchChooseLock() {
        final Intent chooseLockIntent = this.getChooseLockIntent();
        final long preEnroll = Utils.getFingerprintManagerOrNull((Context)this).preEnroll();
        chooseLockIntent.putExtra("minimum_quality", 65536);
        chooseLockIntent.putExtra("hide_disabled_prefs", true);
        chooseLockIntent.putExtra("has_challenge", true);
        chooseLockIntent.putExtra("challenge", preEnroll);
        chooseLockIntent.putExtra("for_fingerprint", true);
        if (this.mUserId != -10000) {
            chooseLockIntent.putExtra("android.intent.extra.USER_ID", this.mUserId);
        }
        this.startActivityForResult(chooseLockIntent, 1);
    }
    
    private void launchFindSensor(final byte[] array) {
        final Intent findSensorIntent = this.getFindSensorIntent();
        if (array != null) {
            findSensorIntent.putExtra("hw_auth_token", array);
        }
        if (this.mUserId != -10000) {
            findSensorIntent.putExtra("android.intent.extra.USER_ID", this.mUserId);
        }
        this.startActivityForResult(findSensorIntent, 2);
    }
    
    private void updatePasswordQuality() {
        this.mHasPassword = (new ChooseLockSettingsHelper(this).utils().getActivePasswordQuality(this.mUserManager.getCredentialOwnerProfile(this.mUserId)) != 0);
    }
    
    protected Intent getChooseLockIntent() {
        return new Intent((Context)this, (Class)ChooseLockGeneric.class);
    }
    
    protected Intent getFindSensorIntent() {
        return new Intent((Context)this, (Class)FingerprintEnrollFindSensor.class);
    }
    
    public int getMetricsCategory() {
        return 243;
    }
    
    @Override
    protected Button getNextButton() {
        return (Button)this.findViewById(2131362152);
    }
    
    @Override
    protected void initViews() {
        super.initViews();
        final TextView textView = (TextView)this.findViewById(2131362068);
        if (this.mFingerprintUnlockDisabledByAdmin) {
            textView.setText(2131889003);
        }
    }
    
    protected void onActivityResult(int n, final int n2, final Intent intent) {
        final boolean b = n2 == 1;
        final int n3 = 2;
        if (n == 2) {
            if (b || n2 == 2) {
                n = n3;
                if (b) {
                    n = -1;
                }
                this.setResult(n, intent);
                this.finish();
                return;
            }
        }
        else if (n == 1) {
            if (b) {
                this.updatePasswordQuality();
                this.launchFindSensor(intent.getByteArrayExtra("hw_auth_token"));
                return;
            }
        }
        else if (n == 3) {
            this.overridePendingTransition(2130772028, 2130772029);
        }
        super.onActivityResult(n, n2, intent);
    }
    
    protected void onCancelButtonClick() {
        this.finish();
    }
    
    @Override
    public void onClick(final View view) {
        if (view.getId() == 2131362149) {
            this.onCancelButtonClick();
        }
        else {
            super.onClick(view);
        }
    }
    
    public void onClick(final LinkSpan linkSpan) {
        if ("url".equals(linkSpan.getId())) {
            final Intent helpIntent = HelpUtils.getHelpIntent((Context)this, this.getString(2131887811), this.getClass().getName());
            if (helpIntent == null) {
                Log.w("FingerprintIntro", "Null help intent.");
                return;
            }
            try {
                this.startActivityForResult(helpIntent, 3);
            }
            catch (ActivityNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Activity was not found for intent, ");
                sb.append(ex);
                Log.w("FingerprintIntro", sb.toString());
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFingerprintUnlockDisabledByAdmin = (RestrictedLockUtils.checkIfKeyguardFeaturesDisabled((Context)this, 32, this.mUserId) != null);
        this.setContentView(2131558567);
        if (this.mFingerprintUnlockDisabledByAdmin) {
            this.setHeaderText(2131889005);
        }
        else {
            this.setHeaderText(2131889004);
        }
        ((Button)this.findViewById(2131362149)).setOnClickListener((View.OnClickListener)this);
        this.mErrorText = (TextView)this.findViewById(2131362128);
        this.mUserManager = UserManager.get((Context)this);
        this.updatePasswordQuality();
    }
    
    @Override
    protected void onNextButtonClick() {
        if (!this.mHasPassword) {
            this.launchChooseLock();
        }
        else {
            this.launchFindSensor(null);
        }
    }
    
    protected void onResume() {
        super.onResume();
        final FingerprintManager fingerprintManagerOrNull = Utils.getFingerprintManagerOrNull((Context)this);
        int text = 0;
        if (fingerprintManagerOrNull != null) {
            if (fingerprintManagerOrNull.getEnrolledFingerprints(this.mUserId).size() >= this.getResources().getInteger(17694789)) {
                text = 2131887655;
            }
        }
        else {
            text = 2131887656;
        }
        if (text == 0) {
            this.mErrorText.setText((CharSequence)null);
            this.getNextButton().setVisibility(0);
        }
        else {
            this.mErrorText.setText(text);
            this.getNextButton().setVisibility(8);
        }
    }
}
