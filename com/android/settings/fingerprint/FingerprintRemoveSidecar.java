package com.android.settings.fingerprint;

import android.util.Log;
import android.os.Bundle;
import java.util.LinkedList;
import android.hardware.fingerprint.FingerprintManager$RemovalCallback;
import java.util.Queue;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import com.android.settings.core.InstrumentedFragment;

public class FingerprintRemoveSidecar extends InstrumentedFragment
{
    FingerprintManager mFingerprintManager;
    private Fingerprint mFingerprintRemoving;
    private Queue<Object> mFingerprintsRemoved;
    private Listener mListener;
    private FingerprintManager$RemovalCallback mRemoveCallback;
    
    public FingerprintRemoveSidecar() {
        this.mRemoveCallback = new FingerprintManager$RemovalCallback() {
            public void onRemovalError(final Fingerprint fingerprint, final int n, final CharSequence charSequence) {
                if (FingerprintRemoveSidecar.this.mListener != null) {
                    FingerprintRemoveSidecar.this.mListener.onRemovalError(fingerprint, n, charSequence);
                }
                else {
                    FingerprintRemoveSidecar.this.mFingerprintsRemoved.add(new RemovalError(fingerprint, n, charSequence));
                }
                FingerprintRemoveSidecar.this.mFingerprintRemoving = null;
            }
            
            public void onRemovalSucceeded(final Fingerprint fingerprint, final int n) {
                if (FingerprintRemoveSidecar.this.mListener != null) {
                    FingerprintRemoveSidecar.this.mListener.onRemovalSucceeded(fingerprint);
                }
                else {
                    FingerprintRemoveSidecar.this.mFingerprintsRemoved.add(fingerprint);
                }
                FingerprintRemoveSidecar.this.mFingerprintRemoving = null;
            }
        };
        this.mFingerprintsRemoved = new LinkedList<Object>();
    }
    
    @Override
    public int getMetricsCategory() {
        return 934;
    }
    
    final boolean inProgress() {
        return this.mFingerprintRemoving != null;
    }
    
    final boolean isRemovingFingerprint(final int n) {
        return this.inProgress() && this.mFingerprintRemoving.getFingerId() == n;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setRetainInstance(true);
    }
    
    public void setFingerprintManager(final FingerprintManager mFingerprintManager) {
        this.mFingerprintManager = mFingerprintManager;
    }
    
    public void setListener(final Listener mListener) {
        if (this.mListener == null && mListener != null) {
            while (!this.mFingerprintsRemoved.isEmpty()) {
                final RemovalError poll = this.mFingerprintsRemoved.poll();
                if (poll instanceof Fingerprint) {
                    mListener.onRemovalSucceeded((Fingerprint)poll);
                }
                else {
                    if (!(poll instanceof RemovalError)) {
                        continue;
                    }
                    final RemovalError removalError = poll;
                    mListener.onRemovalError(removalError.fingerprint, removalError.errMsgId, removalError.errString);
                }
            }
        }
        this.mListener = mListener;
    }
    
    public void startRemove(final Fingerprint mFingerprintRemoving, final int activeUser) {
        if (this.mFingerprintRemoving != null) {
            Log.e("FingerprintRemoveSidecar", "Remove already in progress");
            return;
        }
        if (activeUser != -10000) {
            this.mFingerprintManager.setActiveUser(activeUser);
        }
        this.mFingerprintRemoving = mFingerprintRemoving;
        this.mFingerprintManager.remove(mFingerprintRemoving, activeUser, this.mRemoveCallback);
    }
    
    public interface Listener
    {
        void onRemovalError(final Fingerprint p0, final int p1, final CharSequence p2);
        
        void onRemovalSucceeded(final Fingerprint p0);
    }
    
    private class RemovalError
    {
        int errMsgId;
        CharSequence errString;
        Fingerprint fingerprint;
        
        public RemovalError(final Fingerprint fingerprint, final int errMsgId, final CharSequence errString) {
            this.fingerprint = fingerprint;
            this.errMsgId = errMsgId;
            this.errString = errString;
        }
    }
}
