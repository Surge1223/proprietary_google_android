package com.android.settings.fingerprint;

import android.app.AlertDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.graphics.drawable.LayerDrawable;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;
import android.view.View;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.animation.TimeInterpolator;
import android.graphics.drawable.Drawable;
import android.animation.Animator;
import android.media.AudioAttributes$Builder;
import android.os.Vibrator;
import android.widget.ProgressBar;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Animatable2$AnimationCallback;
import android.view.animation.Interpolator;
import android.widget.TextView;
import android.os.VibrationEffect;
import android.media.AudioAttributes;

public class FingerprintEnrollEnrolling extends FingerprintEnrollBase implements Listener
{
    private static final AudioAttributes FINGERPRINT_ENROLLING_SONFICATION_ATTRIBUTES;
    private static final VibrationEffect VIBRATE_EFFECT_ERROR;
    private boolean mAnimationCancelled;
    private final Runnable mDelayedFinishRunnable;
    private TextView mErrorText;
    private Interpolator mFastOutLinearInInterpolator;
    private Interpolator mFastOutSlowInInterpolator;
    private final Animatable2$AnimationCallback mIconAnimationCallback;
    private AnimatedVectorDrawable mIconAnimationDrawable;
    private AnimatedVectorDrawable mIconBackgroundBlinksDrawable;
    private int mIconTouchCount;
    private Interpolator mLinearOutSlowInInterpolator;
    private ObjectAnimator mProgressAnim;
    private final Animator.AnimatorListener mProgressAnimationListener;
    private ProgressBar mProgressBar;
    private TextView mRepeatMessage;
    private boolean mRestoring;
    private final Runnable mShowDialogRunnable;
    private FingerprintEnrollSidecar mSidecar;
    private TextView mStartMessage;
    private final Runnable mTouchAgainRunnable;
    private Vibrator mVibrator;
    
    static {
        VIBRATE_EFFECT_ERROR = VibrationEffect.createWaveform(new long[] { 0L, 5L, 55L, 60L }, -1);
        FINGERPRINT_ENROLLING_SONFICATION_ATTRIBUTES = new AudioAttributes$Builder().setContentType(4).setUsage(13).build();
    }
    
    public FingerprintEnrollEnrolling() {
        this.mProgressAnimationListener = (Animator.AnimatorListener)new Animator.AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                if (FingerprintEnrollEnrolling.this.mProgressBar.getProgress() >= 10000) {
                    FingerprintEnrollEnrolling.this.mProgressBar.postDelayed(FingerprintEnrollEnrolling.this.mDelayedFinishRunnable, 250L);
                }
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
            }
        };
        this.mDelayedFinishRunnable = new Runnable() {
            @Override
            public void run() {
                FingerprintEnrollEnrolling.this.launchFinish(FingerprintEnrollEnrolling.this.mToken);
            }
        };
        this.mIconAnimationCallback = new Animatable2$AnimationCallback() {
            public void onAnimationEnd(final Drawable drawable) {
                if (FingerprintEnrollEnrolling.this.mAnimationCancelled) {
                    return;
                }
                FingerprintEnrollEnrolling.this.mProgressBar.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        FingerprintEnrollEnrolling.this.startIconAnimation();
                    }
                });
            }
        };
        this.mShowDialogRunnable = new Runnable() {
            @Override
            public void run() {
                FingerprintEnrollEnrolling.this.showIconTouchDialog();
            }
        };
        this.mTouchAgainRunnable = new Runnable() {
            @Override
            public void run() {
                FingerprintEnrollEnrolling.this.showError(FingerprintEnrollEnrolling.this.getString(2131889006));
            }
        };
    }
    
    private void animateFlash() {
        this.mIconBackgroundBlinksDrawable.start();
    }
    
    private void animateProgress(final int n) {
        if (this.mProgressAnim != null) {
            this.mProgressAnim.cancel();
        }
        final ObjectAnimator ofInt = ObjectAnimator.ofInt((Object)this.mProgressBar, "progress", new int[] { this.mProgressBar.getProgress(), n });
        ofInt.addListener(this.mProgressAnimationListener);
        ofInt.setInterpolator((TimeInterpolator)this.mFastOutSlowInInterpolator);
        ofInt.setDuration(250L);
        ofInt.start();
        this.mProgressAnim = ofInt;
    }
    
    private void clearError() {
        if (this.mErrorText.getVisibility() == 0) {
            this.mErrorText.animate().alpha(0.0f).translationY((float)this.getResources().getDimensionPixelSize(2131165401)).setDuration(100L).setInterpolator((TimeInterpolator)this.mFastOutLinearInInterpolator).withEndAction((Runnable)new _$$Lambda$FingerprintEnrollEnrolling$aLk12WuaBTV2piitP3fdnB0w_eM(this)).start();
        }
    }
    
    private int getProgress(final int n, final int n2) {
        if (n == -1) {
            return 0;
        }
        return 10000 * Math.max(0, n + 1 - n2) / (n + 1);
    }
    
    private void launchFinish(final byte[] array) {
        final Intent finishIntent = this.getFinishIntent();
        finishIntent.addFlags(637534208);
        finishIntent.putExtra("hw_auth_token", array);
        if (this.mUserId != -10000) {
            finishIntent.putExtra("android.intent.extra.USER_ID", this.mUserId);
        }
        this.startActivity(finishIntent);
        this.overridePendingTransition(2130772030, 2130772031);
        this.finish();
    }
    
    private void showError(final CharSequence text) {
        this.mErrorText.setText(text);
        if (this.mErrorText.getVisibility() == 4) {
            this.mErrorText.setVisibility(0);
            this.mErrorText.setTranslationY((float)this.getResources().getDimensionPixelSize(2131165400));
            this.mErrorText.setAlpha(0.0f);
            this.mErrorText.animate().alpha(1.0f).translationY(0.0f).setDuration(200L).setInterpolator((TimeInterpolator)this.mLinearOutSlowInInterpolator).start();
        }
        else {
            this.mErrorText.animate().cancel();
            this.mErrorText.setAlpha(1.0f);
            this.mErrorText.setTranslationY(0.0f);
        }
        if (this.isResumed()) {
            this.mVibrator.vibrate(FingerprintEnrollEnrolling.VIBRATE_EFFECT_ERROR, FingerprintEnrollEnrolling.FINGERPRINT_ENROLLING_SONFICATION_ATTRIBUTES);
        }
    }
    
    private void showErrorDialog(final CharSequence charSequence, final int n) {
        ErrorDialog.newInstance(charSequence, n).show(this.getFragmentManager(), ErrorDialog.class.getName());
    }
    
    private void showIconTouchDialog() {
        this.mIconTouchCount = 0;
        new IconTouchDialog().show(this.getFragmentManager(), (String)null);
    }
    
    private void startIconAnimation() {
        this.mIconAnimationDrawable.start();
    }
    
    private void stopIconAnimation() {
        this.mAnimationCancelled = true;
        this.mIconAnimationDrawable.stop();
    }
    
    private void updateDescription() {
        if (this.mSidecar.getEnrollmentSteps() == -1) {
            this.mStartMessage.setVisibility(0);
            this.mRepeatMessage.setVisibility(4);
        }
        else {
            this.mStartMessage.setVisibility(4);
            this.mRepeatMessage.setVisibility(0);
        }
    }
    
    private void updateProgress(final boolean b) {
        final int progress = this.getProgress(this.mSidecar.getEnrollmentSteps(), this.mSidecar.getEnrollmentRemaining());
        if (b) {
            this.animateProgress(progress);
        }
        else {
            this.mProgressBar.setProgress(progress);
            if (progress >= 10000) {
                this.mDelayedFinishRunnable.run();
            }
        }
    }
    
    protected Intent getFinishIntent() {
        return new Intent((Context)this, (Class)FingerprintEnrollFinish.class);
    }
    
    public int getMetricsCategory() {
        return 240;
    }
    
    public void onBackPressed() {
        if (this.mSidecar != null) {
            this.mSidecar.setListener(null);
            this.mSidecar.cancelEnrollment();
            this.getFragmentManager().beginTransaction().remove((Fragment)this.mSidecar).commitAllowingStateLoss();
            this.mSidecar = null;
        }
        super.onBackPressed();
    }
    
    @Override
    public void onClick(final View view) {
        if (view.getId() != 2131362617) {
            super.onClick(view);
        }
        else {
            this.setResult(2);
            this.finish();
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131558556);
        this.setHeaderText(2131889008);
        this.mStartMessage = (TextView)this.findViewById(2131362640);
        this.mRepeatMessage = (TextView)this.findViewById(2131362520);
        this.mErrorText = (TextView)this.findViewById(2131362128);
        this.mProgressBar = (ProgressBar)this.findViewById(2131362153);
        this.mVibrator = (Vibrator)this.getSystemService((Class)Vibrator.class);
        ((Button)this.findViewById(2131362617)).setOnClickListener((View.OnClickListener)this);
        final LayerDrawable layerDrawable = (LayerDrawable)this.mProgressBar.getBackground();
        this.mIconAnimationDrawable = (AnimatedVectorDrawable)layerDrawable.findDrawableByLayerId(2131362147);
        this.mIconBackgroundBlinksDrawable = (AnimatedVectorDrawable)layerDrawable.findDrawableByLayerId(2131362148);
        this.mIconAnimationDrawable.registerAnimationCallback(this.mIconAnimationCallback);
        this.mFastOutSlowInInterpolator = AnimationUtils.loadInterpolator((Context)this, 17563661);
        this.mLinearOutSlowInInterpolator = AnimationUtils.loadInterpolator((Context)this, 17563662);
        this.mFastOutLinearInInterpolator = AnimationUtils.loadInterpolator((Context)this, 17563663);
        this.mProgressBar.setOnTouchListener((View.OnTouchListener)new View.OnTouchListener() {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == 0) {
                    FingerprintEnrollEnrolling.this.mIconTouchCount++;
                    if (FingerprintEnrollEnrolling.this.mIconTouchCount == 3) {
                        FingerprintEnrollEnrolling.this.showIconTouchDialog();
                    }
                    else {
                        FingerprintEnrollEnrolling.this.mProgressBar.postDelayed(FingerprintEnrollEnrolling.this.mShowDialogRunnable, 500L);
                    }
                }
                else if (motionEvent.getActionMasked() == 3 || motionEvent.getActionMasked() == 1) {
                    FingerprintEnrollEnrolling.this.mProgressBar.removeCallbacks(FingerprintEnrollEnrolling.this.mShowDialogRunnable);
                }
                return true;
            }
        });
        this.mRestoring = (bundle != null);
    }
    
    @Override
    public void onEnrollmentError(final int n, final CharSequence charSequence) {
        int n2;
        if (n != 3) {
            n2 = 2131888990;
        }
        else {
            n2 = 2131888991;
        }
        this.showErrorDialog(this.getText(n2), n);
        this.stopIconAnimation();
        this.mErrorText.removeCallbacks(this.mTouchAgainRunnable);
    }
    
    @Override
    public void onEnrollmentHelp(final CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            this.mErrorText.removeCallbacks(this.mTouchAgainRunnable);
            this.showError(charSequence);
        }
    }
    
    @Override
    public void onEnrollmentProgressChange(final int n, final int n2) {
        this.updateProgress(true);
        this.updateDescription();
        this.clearError();
        this.animateFlash();
        this.mErrorText.removeCallbacks(this.mTouchAgainRunnable);
        this.mErrorText.postDelayed(this.mTouchAgainRunnable, 2500L);
    }
    
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        this.mAnimationCancelled = false;
        this.startIconAnimation();
    }
    
    protected void onStart() {
        super.onStart();
        this.mSidecar = (FingerprintEnrollSidecar)this.getFragmentManager().findFragmentByTag("sidecar");
        if (this.mSidecar == null) {
            this.mSidecar = new FingerprintEnrollSidecar();
            this.getFragmentManager().beginTransaction().add((Fragment)this.mSidecar, "sidecar").commit();
        }
        this.mSidecar.setListener((FingerprintEnrollSidecar.Listener)this);
        this.updateProgress(false);
        this.updateDescription();
        if (this.mRestoring) {
            this.startIconAnimation();
        }
    }
    
    protected void onStop() {
        super.onStop();
        if (this.mSidecar != null) {
            this.mSidecar.setListener(null);
        }
        this.stopIconAnimation();
        if (!this.isChangingConfigurations()) {
            if (this.mSidecar != null) {
                this.mSidecar.cancelEnrollment();
                this.getFragmentManager().beginTransaction().remove((Fragment)this.mSidecar).commitAllowingStateLoss();
            }
            this.finish();
        }
    }
    
    public static class ErrorDialog extends InstrumentedDialogFragment
    {
        static ErrorDialog newInstance(final CharSequence charSequence, final int n) {
            final ErrorDialog errorDialog = new ErrorDialog();
            final Bundle arguments = new Bundle();
            arguments.putCharSequence("error_msg", charSequence);
            arguments.putInt("error_id", n);
            errorDialog.setArguments(arguments);
            return errorDialog;
        }
        
        @Override
        public int getMetricsCategory() {
            return 569;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this.getActivity());
            alertDialog$Builder.setTitle(2131888989).setMessage(this.getArguments().getCharSequence("error_msg")).setCancelable(false).setPositiveButton(2131888984, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                final /* synthetic */ int val$errMsgId = ErrorDialog.this.getArguments().getInt("error_id");
                
                public void onClick(final DialogInterface dialogInterface, int val$errMsgId) {
                    dialogInterface.dismiss();
                    val$errMsgId = this.val$errMsgId;
                    final int n = 1;
                    if (val$errMsgId == 3) {
                        val$errMsgId = 1;
                    }
                    else {
                        val$errMsgId = 0;
                    }
                    final Activity activity = ErrorDialog.this.getActivity();
                    if (val$errMsgId != 0) {
                        val$errMsgId = 3;
                    }
                    else {
                        val$errMsgId = n;
                    }
                    activity.setResult(val$errMsgId);
                    activity.finish();
                }
            });
            final AlertDialog create = alertDialog$Builder.create();
            create.setCanceledOnTouchOutside(false);
            return (Dialog)create;
        }
    }
    
    public static class IconTouchDialog extends InstrumentedDialogFragment
    {
        @Override
        public int getMetricsCategory() {
            return 568;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this.getActivity());
            alertDialog$Builder.setTitle(2131889013).setMessage(2131889012).setPositiveButton(2131888984, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    dialogInterface.dismiss();
                }
            });
            return (Dialog)alertDialog$Builder.create();
        }
    }
}
