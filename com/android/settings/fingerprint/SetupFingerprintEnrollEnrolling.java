package com.android.settings.fingerprint;

import com.android.settings.SetupWizardUtils;
import android.content.Context;
import android.content.Intent;

public class SetupFingerprintEnrollEnrolling extends FingerprintEnrollEnrolling
{
    @Override
    protected Intent getFinishIntent() {
        final Intent intent = new Intent((Context)this, (Class)SetupFingerprintEnrollFinish.class);
        SetupWizardUtils.copySetupExtras(this.getIntent(), intent);
        return intent;
    }
    
    @Override
    public int getMetricsCategory() {
        return 246;
    }
}
