package com.android.settings.fingerprint;

import android.app.AlertDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.graphics.drawable.LayerDrawable;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;
import android.view.View;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.animation.TimeInterpolator;
import android.graphics.drawable.Drawable;
import android.animation.Animator;
import android.media.AudioAttributes$Builder;
import android.os.Vibrator;
import android.widget.ProgressBar;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Animatable2$AnimationCallback;
import android.view.animation.Interpolator;
import android.widget.TextView;
import android.os.VibrationEffect;
import android.media.AudioAttributes;

public final class _$$Lambda$FingerprintEnrollEnrolling$aLk12WuaBTV2piitP3fdnB0w_eM implements Runnable
{
    @Override
    public final void run() {
        FingerprintEnrollEnrolling.lambda$clearError$0(this.f$0);
    }
}
