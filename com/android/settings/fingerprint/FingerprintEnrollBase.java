package com.android.settings.fingerprint;

import android.widget.TextView;
import android.text.TextUtils;
import android.os.UserHandle;
import android.os.Bundle;
import android.view.View;
import com.android.settings.SetupWizardUtils;
import android.content.res.Resources.Theme;
import android.widget.Button;
import com.android.setupwizardlib.GlifLayout;
import android.content.Intent;
import android.view.View.OnClickListener;
import com.android.settings.core.InstrumentedActivity;

public abstract class FingerprintEnrollBase extends InstrumentedActivity implements View.OnClickListener
{
    protected byte[] mToken;
    protected int mUserId;
    
    protected Intent getEnrollingIntent() {
        final Intent intent = new Intent();
        intent.setClassName("com.android.settings", FingerprintEnrollEnrolling.class.getName());
        intent.putExtra("hw_auth_token", this.mToken);
        if (this.mUserId != -10000) {
            intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
        }
        return intent;
    }
    
    protected GlifLayout getLayout() {
        return (GlifLayout)this.findViewById(2131362597);
    }
    
    protected Button getNextButton() {
        return (Button)this.findViewById(2131362389);
    }
    
    protected void initViews() {
        this.getWindow().setStatusBarColor(0);
        final Button nextButton = this.getNextButton();
        if (nextButton != null) {
            nextButton.setOnClickListener((View.OnClickListener)this);
        }
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        super.onApplyThemeResource(resources$Theme, SetupWizardUtils.getTheme(this.getIntent()), b);
    }
    
    public void onClick(final View view) {
        if (view == this.getNextButton()) {
            this.onNextButtonClick();
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mToken = this.getIntent().getByteArrayExtra("hw_auth_token");
        if (bundle != null && this.mToken == null) {
            this.mToken = bundle.getByteArray("hw_auth_token");
        }
        this.mUserId = this.getIntent().getIntExtra("android.intent.extra.USER_ID", UserHandle.myUserId());
    }
    
    protected void onNextButtonClick() {
    }
    
    protected void onPostCreate(final Bundle bundle) {
        super.onPostCreate(bundle);
        this.initViews();
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putByteArray("hw_auth_token", this.mToken);
    }
    
    protected void setHeaderText(final int n) {
        this.setHeaderText(n, false);
    }
    
    protected void setHeaderText(final int n, final boolean b) {
        final TextView headerTextView = this.getLayout().getHeaderTextView();
        final CharSequence text = headerTextView.getText();
        final CharSequence text2 = this.getText(n);
        if (text != text2 || b) {
            if (!TextUtils.isEmpty(text)) {
                headerTextView.setAccessibilityLiveRegion(1);
            }
            this.getLayout().setHeaderText(text2);
            this.setTitle(text2);
        }
    }
}
