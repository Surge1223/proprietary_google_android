package com.android.settings.fingerprint;

import android.widget.Button;
import com.android.settings.SetupWizardUtils;
import android.content.Context;
import android.content.Intent;

public class SetupFingerprintEnrollFinish extends FingerprintEnrollFinish
{
    @Override
    protected Intent getEnrollingIntent() {
        final Intent intent = new Intent((Context)this, (Class)SetupFingerprintEnrollEnrolling.class);
        intent.putExtra("hw_auth_token", this.mToken);
        if (this.mUserId != -10000) {
            intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
        }
        SetupWizardUtils.copySetupExtras(this.getIntent(), intent);
        return intent;
    }
    
    @Override
    public int getMetricsCategory() {
        return 248;
    }
    
    @Override
    protected void initViews() {
        super.initViews();
        ((Button)this.findViewById(2131362389)).setText(2131888358);
    }
}
