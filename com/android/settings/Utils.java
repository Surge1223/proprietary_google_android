package com.android.settings;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.content.pm.ResolveInfo;
import android.support.v7.preference.PreferenceGroup;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.app.UnlaunchableAppActivity;
import android.graphics.drawable.VectorDrawable;
import android.support.v7.preference.Preference;
import android.widget.EditText;
import android.preference.PreferenceFrameLayout$LayoutParams;
import android.preference.PreferenceFrameLayout;
import android.os.storage.StorageManager;
import android.content.ActivityNotFoundException;
import android.app.Fragment;
import android.os.storage.VolumeInfo;
import android.telephony.TelephonyManager;
import android.provider.Settings;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService$Stub;
import android.os.ServiceManager;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R$styleable;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.net.Network;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.app.IActivityManager;
import android.app.ActivityManager;
import android.os.Bundle;
import android.os.IBinder;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.database.Cursor;
import android.content.ContentResolver;
import android.text.TextUtils;
import android.net.Uri;
import android.provider.ContactsContract$Profile;
import java.util.Collection;
import android.content.IntentFilter;
import android.content.pm.IntentFilterVerificationInfo;
import android.util.ArraySet;
import android.hardware.fingerprint.FingerprintManager;
import java.util.List;
import android.content.pm.UserInfo;
import android.os.UserManager;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.content.ComponentName;
import android.os.RemoteException;
import android.app.AppGlobals;
import android.app.admin.DevicePolicyManager;
import android.content.pm.ApplicationInfo;
import java.util.Iterator;
import java.net.InetAddress;
import android.net.LinkProperties;
import android.text.format.DateUtils;
import android.content.res.Resources;
import android.view.View;
import com.android.internal.util.ArrayUtils;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.UserHandle;
import android.text.style.TtsSpan$TextBuilder;
import android.text.SpannableString;
import android.content.Intent;
import android.app.KeyguardManager;
import android.content.Context;
import java.util.Locale;
import java.util.Formatter;

public final class Utils extends com.android.settingslib.Utils
{
    public static final int[] BADNESS_COLORS;
    private static final StringBuilder sBuilder;
    private static final Formatter sFormatter;
    
    static {
        BADNESS_COLORS = new int[] { 0, -3917784, -1750760, -754944, -344276, -9986505, -16089278 };
        sBuilder = new StringBuilder(50);
        sFormatter = new Formatter(Utils.sBuilder, Locale.getDefault());
    }
    
    private static boolean confirmWorkProfileCredentials(final Context context, final int n) {
        final Intent confirmDeviceCredentialIntent = ((KeyguardManager)context.getSystemService("keyguard")).createConfirmDeviceCredentialIntent((CharSequence)null, (CharSequence)null, n);
        if (confirmDeviceCredentialIntent != null) {
            context.startActivity(confirmDeviceCredentialIntent);
            return true;
        }
        return false;
    }
    
    public static SpannableString createAccessibleSequence(final CharSequence charSequence, final String s) {
        final SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan((Object)new TtsSpan$TextBuilder(s).build(), 0, charSequence.length(), 18);
        return spannableString;
    }
    
    public static Locale createLocaleFromString(final String s) {
        if (s == null) {
            return Locale.getDefault();
        }
        final String[] split = s.split("_", 3);
        if (1 == split.length) {
            return new Locale(split[0]);
        }
        if (2 == split.length) {
            return new Locale(split[0], split[1]);
        }
        return new Locale(split[0], split[1], split[2]);
    }
    
    public static Context createPackageContextAsUser(Context packageContextAsUser, final int n) {
        try {
            packageContextAsUser = packageContextAsUser.createPackageContextAsUser(packageContextAsUser.getPackageName(), 0, UserHandle.of(n));
            return packageContextAsUser;
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e("Settings", "Failed to create user context", (Throwable)ex);
            return null;
        }
    }
    
    public static int enforceSameOwner(final Context context, final int n) {
        if (ArrayUtils.contains(getUserManager(context).getProfileIdsWithDisabled(UserHandle.myUserId()), n)) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Given user id ");
        sb.append(n);
        sb.append(" does not belong to user ");
        sb.append(UserHandle.myUserId());
        throw new SecurityException(sb.toString());
    }
    
    public static int enforceSystemUser(final Context context, final int n) {
        if (UserHandle.myUserId() == 0) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Given user id ");
        sb.append(n);
        sb.append(" must only be used from USER_SYSTEM, but current user is ");
        sb.append(UserHandle.myUserId());
        throw new SecurityException(sb.toString());
    }
    
    public static void forceCustomPadding(final View view, final boolean b) {
        final Resources resources = view.getResources();
        final int dimensionPixelSize = resources.getDimensionPixelSize(2131165546);
        int paddingStart;
        if (b) {
            paddingStart = view.getPaddingStart();
        }
        else {
            paddingStart = 0;
        }
        int paddingEnd;
        if (b) {
            paddingEnd = view.getPaddingEnd();
        }
        else {
            paddingEnd = 0;
        }
        view.setPaddingRelative(paddingStart + dimensionPixelSize, 0, paddingEnd + dimensionPixelSize, resources.getDimensionPixelSize(17105260));
    }
    
    public static String formatDateRange(final Context context, final long n, final long n2) {
        synchronized (Utils.sBuilder) {
            Utils.sBuilder.setLength(0);
            return DateUtils.formatDateRange(context, Utils.sFormatter, n, n2, 65552, (String)null).toString();
        }
    }
    
    private static String formatIpAddresses(final LinkProperties linkProperties) {
        if (linkProperties == null) {
            return null;
        }
        final Iterator iterator = linkProperties.getAllAddresses().iterator();
        if (!iterator.hasNext()) {
            return null;
        }
        String s = "";
        while (iterator.hasNext()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(iterator.next().getHostAddress());
            final String s2 = s = sb.toString();
            if (iterator.hasNext()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s2);
                sb2.append("\n");
                s = sb2.toString();
            }
        }
        return s;
    }
    
    public static ApplicationInfo getAdminApplicationInfo(Context packageName, final int n) {
        final ComponentName profileOwnerAsUser = ((DevicePolicyManager)packageName.getSystemService("device_policy")).getProfileOwnerAsUser(n);
        if (profileOwnerAsUser == null) {
            return null;
        }
        packageName = (Context)profileOwnerAsUser.getPackageName();
        try {
            return AppGlobals.getPackageManager().getApplicationInfo((String)packageName, 0, n);
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error while retrieving application info for package ");
            sb.append((String)packageName);
            sb.append(", userId ");
            sb.append(n);
            Log.e("Settings", sb.toString(), (Throwable)ex);
            return null;
        }
    }
    
    public static CharSequence getApplicationLabel(final Context context, final String s) {
        try {
            return context.getPackageManager().getApplicationInfo(s, 4194816).loadLabel(context.getPackageManager());
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to find info for package: ");
            sb.append(s);
            Log.w("Settings", sb.toString());
            return null;
        }
    }
    
    public static Drawable getBadgedIcon(final IconDrawableFactory iconDrawableFactory, final PackageManager packageManager, final String s, final int n) {
        try {
            return iconDrawableFactory.getBadgedIcon(packageManager.getApplicationInfoAsUser(s, 128, n), n);
        }
        catch (PackageManager$NameNotFoundException ex) {
            return packageManager.getDefaultActivityIcon();
        }
    }
    
    public static String getBatteryPercentage(final Intent intent) {
        return com.android.settingslib.Utils.formatPercentage(com.android.settingslib.Utils.getBatteryLevel(intent));
    }
    
    public static int getCredentialOwnerUserId(final Context context) {
        return getCredentialOwnerUserId(context, UserHandle.myUserId());
    }
    
    public static int getCredentialOwnerUserId(final Context context, final int n) {
        return getUserManager(context).getCredentialOwnerProfile(n);
    }
    
    public static ComponentName getDeviceOwnerComponent(final Context context) {
        return ((DevicePolicyManager)context.getSystemService("device_policy")).getDeviceOwnerComponentOnAnyUser();
    }
    
    public static UserInfo getExistingUser(final UserManager userManager, final UserHandle userHandle) {
        final List users = userManager.getUsers(true);
        final int identifier = userHandle.getIdentifier();
        for (final UserInfo userInfo : users) {
            if (userInfo.id == identifier) {
                return userInfo;
            }
        }
        return null;
    }
    
    public static FingerprintManager getFingerprintManagerOrNull(final Context context) {
        if (context.getPackageManager().hasSystemFeature("android.hardware.fingerprint")) {
            return (FingerprintManager)context.getSystemService("fingerprint");
        }
        return null;
    }
    
    public static ArraySet<String> getHandledDomains(final PackageManager packageManager, final String s) {
        final List intentFilterVerifications = packageManager.getIntentFilterVerifications(s);
        final List allIntentFilters = packageManager.getAllIntentFilters(s);
        final ArraySet set = new ArraySet();
        if (intentFilterVerifications != null && intentFilterVerifications.size() > 0) {
            final Iterator<IntentFilterVerificationInfo> iterator = intentFilterVerifications.iterator();
            while (iterator.hasNext()) {
                final Iterator iterator2 = iterator.next().getDomains().iterator();
                while (iterator2.hasNext()) {
                    set.add((Object)iterator2.next());
                }
            }
        }
        if (allIntentFilters != null && allIntentFilters.size() > 0) {
            for (final IntentFilter intentFilter : allIntentFilters) {
                if (intentFilter.hasCategory("android.intent.category.BROWSABLE") && (intentFilter.hasDataScheme("http") || intentFilter.hasDataScheme("https"))) {
                    set.addAll((Collection)intentFilter.getHostsList());
                }
            }
        }
        return (ArraySet<String>)set;
    }
    
    public static int getInstallationStatus(final ApplicationInfo applicationInfo) {
        if ((applicationInfo.flags & 0x800000) == 0x0) {
            return 2131888426;
        }
        int n;
        if (applicationInfo.enabled) {
            n = 2131887897;
        }
        else {
            n = 2131887422;
        }
        return n;
    }
    
    private static String getLocalProfileGivenName(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        Object o = contentResolver.query(ContactsContract$Profile.CONTENT_RAW_CONTACTS_URI, new String[] { "_id" }, "account_type IS NULL AND account_name IS NULL", (String[])null, (String)null);
        if (o == null) {
            return null;
        }
        try {
            if (!((Cursor)o).moveToFirst()) {
                return null;
            }
            final long long1 = ((Cursor)o).getLong(0);
            ((Cursor)o).close();
            o = ContactsContract$Profile.CONTENT_URI.buildUpon().appendPath("data").build();
            final StringBuilder sb = new StringBuilder();
            sb.append("raw_contact_id=");
            sb.append(long1);
            final Cursor query = contentResolver.query((Uri)o, new String[] { "data2", "data3" }, sb.toString(), (String[])null, (String)null);
            if (query == null) {
                return null;
            }
            try {
                if (!query.moveToFirst()) {
                    return null;
                }
                String s;
                o = (s = query.getString(0));
                if (TextUtils.isEmpty((CharSequence)o)) {
                    s = query.getString(1);
                }
                return s;
            }
            finally {
                query.close();
            }
        }
        finally {
            ((Cursor)o).close();
        }
    }
    
    public static UserHandle getManagedProfile(final UserManager userManager) {
        final List userProfiles = userManager.getUserProfiles();
        for (int size = userProfiles.size(), i = 0; i < size; ++i) {
            final UserHandle userHandle = userProfiles.get(i);
            if (userHandle.getIdentifier() != userManager.getUserHandle()) {
                if (userManager.getUserInfo(userHandle.getIdentifier()).isManagedProfile()) {
                    return userHandle;
                }
            }
        }
        return null;
    }
    
    public static int getManagedProfileId(final UserManager userManager, final int n) {
        for (final int n2 : userManager.getProfileIdsWithDisabled(n)) {
            if (n2 != n) {
                return n2;
            }
        }
        return -10000;
    }
    
    public static UserHandle getManagedProfileWithDisabled(final UserManager userManager) {
        final int myUserId = UserHandle.myUserId();
        final List profiles = userManager.getProfiles(myUserId);
        for (int size = profiles.size(), i = 0; i < size; ++i) {
            final UserInfo userInfo = profiles.get(i);
            if (userInfo.isManagedProfile() && userInfo.getUserHandle().getIdentifier() != myUserId) {
                return userInfo.getUserHandle();
            }
        }
        return null;
    }
    
    public static String getMeProfileName(final Context context, final boolean b) {
        if (b) {
            return getProfileDisplayName(context);
        }
        return getShorterNameIfPossible(context);
    }
    
    private static final String getProfileDisplayName(Context query) {
        query = (Context)query.getContentResolver().query(ContactsContract$Profile.CONTENT_URI, new String[] { "display_name" }, (String)null, (String[])null, (String)null);
        if (query == null) {
            return null;
        }
        try {
            if (!((Cursor)query).moveToFirst()) {
                return null;
            }
            return ((Cursor)query).getString(0);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    public static Drawable getSafeDrawable(final Drawable drawable, int n, int n2) {
        final int minimumWidth = drawable.getMinimumWidth();
        final int minimumHeight = drawable.getMinimumHeight();
        if (minimumWidth <= n && minimumHeight <= n2) {
            return drawable;
        }
        final float min = Math.min(n / minimumWidth, n2 / minimumHeight);
        n = (int)(minimumWidth * min);
        n2 = (int)(minimumHeight * min);
        Bitmap scaledBitmap;
        if (drawable instanceof BitmapDrawable) {
            scaledBitmap = Bitmap.createScaledBitmap(((BitmapDrawable)drawable).getBitmap(), n, n2, false);
        }
        else {
            final Bitmap bitmap = Bitmap.createBitmap(n, n2, Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            scaledBitmap = bitmap;
        }
        return (Drawable)new BitmapDrawable((Resources)null, scaledBitmap);
    }
    
    public static UserHandle getSecureTargetUser(final IBinder binder, final UserManager userManager, final Bundle bundle, final Bundle bundle2) {
        final UserHandle userHandle = new UserHandle(UserHandle.myUserId());
        final IActivityManager service = ActivityManager.getService();
        try {
            final boolean equals = "com.android.settings".equals(service.getLaunchedFromPackage(binder));
            final UserHandle userHandle2 = new UserHandle(UserHandle.getUserId(service.getLaunchedFromUid(binder)));
            if (!userHandle2.equals((Object)userHandle) && isProfileOf(userManager, userHandle2)) {
                return userHandle2;
            }
            final UserHandle userHandleFromBundle = getUserHandleFromBundle(bundle2);
            if (userHandleFromBundle != null && !userHandleFromBundle.equals((Object)userHandle) && equals && isProfileOf(userManager, userHandleFromBundle)) {
                return userHandleFromBundle;
            }
            final UserHandle userHandleFromBundle2 = getUserHandleFromBundle(bundle);
            if (userHandleFromBundle2 != null && !userHandleFromBundle2.equals((Object)userHandle) && equals && isProfileOf(userManager, userHandleFromBundle2)) {
                return userHandleFromBundle2;
            }
        }
        catch (RemoteException ex) {
            Log.v("Settings", "Could not talk to activity manager.", (Throwable)ex);
        }
        return userHandle;
    }
    
    private static String getShorterNameIfPossible(final Context context) {
        final String localProfileGivenName = getLocalProfileGivenName(context);
        String profileDisplayName;
        if (!TextUtils.isEmpty((CharSequence)localProfileGivenName)) {
            profileDisplayName = localProfileGivenName;
        }
        else {
            profileDisplayName = getProfileDisplayName(context);
        }
        return profileDisplayName;
    }
    
    private static UserHandle getUserHandleFromBundle(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        final UserHandle userHandle = (UserHandle)bundle.getParcelable("android.intent.extra.USER");
        if (userHandle != null) {
            return userHandle;
        }
        final int int1 = bundle.getInt("android.intent.extra.USER_ID", -1);
        if (int1 != -1) {
            return UserHandle.of(int1);
        }
        return null;
    }
    
    public static int getUserIdFromBundle(final Context context, final Bundle bundle) {
        return getUserIdFromBundle(context, bundle, false);
    }
    
    public static int getUserIdFromBundle(final Context context, final Bundle bundle, final boolean b) {
        if (bundle == null) {
            return getCredentialOwnerUserId(context);
        }
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            b3 = b2;
            if (bundle.getBoolean("allow_any_user", false)) {
                b3 = true;
            }
        }
        final int int1 = bundle.getInt("android.intent.extra.USER_ID", UserHandle.myUserId());
        if (int1 == -9999) {
            int enforceSystemUser;
            if (b3) {
                enforceSystemUser = int1;
            }
            else {
                enforceSystemUser = enforceSystemUser(context, int1);
            }
            return enforceSystemUser;
        }
        int enforceSameOwner;
        if (b3) {
            enforceSameOwner = int1;
        }
        else {
            enforceSameOwner = enforceSameOwner(context, int1);
        }
        return enforceSameOwner;
    }
    
    public static UserManager getUserManager(final Context context) {
        final UserManager value = UserManager.get(context);
        if (value != null) {
            return value;
        }
        throw new IllegalStateException("Unable to load UserManager");
    }
    
    public static String getWifiIpAddresses(final Context context) {
        final Network currentNetwork = ((WifiManager)context.getSystemService((Class)WifiManager.class)).getCurrentNetwork();
        if (currentNetwork != null) {
            return formatIpAddresses(((ConnectivityManager)context.getSystemService("connectivity")).getLinkProperties(currentNetwork));
        }
        return null;
    }
    
    public static boolean hasFingerprintHardware(final Context context) {
        final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(context);
        return fingerprintManagerOrNull != null && fingerprintManagerOrNull.isHardwareDetected();
    }
    
    public static boolean hasMultipleUsers(final Context context) {
        final int size = ((UserManager)context.getSystemService("user")).getUsers().size();
        boolean b = true;
        if (size <= 1) {
            b = false;
        }
        return b;
    }
    
    public static View inflateCategoryHeader(final LayoutInflater layoutInflater, final ViewGroup viewGroup) {
        final TypedArray obtainStyledAttributes = layoutInflater.getContext().obtainStyledAttributes((AttributeSet)null, R$styleable.Preference, 16842892, 0);
        final int resourceId = obtainStyledAttributes.getResourceId(3, 0);
        obtainStyledAttributes.recycle();
        return layoutInflater.inflate(resourceId, viewGroup, false);
    }
    
    public static boolean isBandwidthControlEnabled() {
        final INetworkManagementService interface1 = INetworkManagementService$Stub.asInterface(ServiceManager.getService("network_management"));
        try {
            return interface1.isBandwidthControlEnabled();
        }
        catch (RemoteException ex) {
            return false;
        }
    }
    
    public static boolean isBatteryPresent(final Intent intent) {
        return intent.getBooleanExtra("present", true);
    }
    
    public static boolean isDemoUser(final Context context) {
        return UserManager.isDeviceInDemoMode(context) && getUserManager(context).isDemoUser();
    }
    
    public static boolean isDeviceProvisioned(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "device_provisioned", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    public static boolean isMonkeyRunning() {
        return ActivityManager.isUserAMonkey();
    }
    
    public static boolean isPackageDirectBootAware(final Context context, final String s) {
        boolean b = false;
        try {
            final ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(s, 0);
            if (applicationInfo.isDirectBootAware() || applicationInfo.isPartiallyDirectBootAware()) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    public static boolean isProfileOf(final UserInfo userInfo, final UserInfo userInfo2) {
        return userInfo.id == userInfo2.id || (userInfo.profileGroupId != -10000 && userInfo.profileGroupId == userInfo2.profileGroupId);
    }
    
    private static boolean isProfileOf(final UserManager userManager, final UserHandle userHandle) {
        boolean b = false;
        if (userManager != null && userHandle != null) {
            if (UserHandle.myUserId() == userHandle.getIdentifier() || userManager.getUserProfiles().contains(userHandle)) {
                b = true;
            }
            return b;
        }
        return false;
    }
    
    public static boolean isProfileOrDeviceOwner(final UserManager userManager, final DevicePolicyManager devicePolicyManager, final String s) {
        final List users = userManager.getUsers();
        if (devicePolicyManager.isDeviceOwnerAppOnAnyUser(s)) {
            return true;
        }
        for (int i = 0; i < users.size(); ++i) {
            final ComponentName profileOwnerAsUser = devicePolicyManager.getProfileOwnerAsUser(users.get(i).id);
            if (profileOwnerAsUser != null && profileOwnerAsUser.getPackageName().equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isVoiceCapable(final Context context) {
        final TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
        return telephonyManager != null && telephonyManager.isVoiceCapable();
    }
    
    private static boolean isVolumeValid(final VolumeInfo volumeInfo) {
        boolean b = true;
        if (volumeInfo == null || volumeInfo.getType() != 1 || !volumeInfo.isMountedReadable()) {
            b = false;
        }
        return b;
    }
    
    public static void launchIntent(final Fragment fragment, final Intent intent) {
        try {
            final int intExtra = intent.getIntExtra("android.intent.extra.USER_ID", -1);
            if (intExtra == -1) {
                fragment.startActivity(intent);
            }
            else {
                fragment.getActivity().startActivityAsUser(intent, new UserHandle(intExtra));
            }
        }
        catch (ActivityNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No activity found for ");
            sb.append(intent);
            Log.w("Settings", sb.toString());
        }
    }
    
    public static VolumeInfo maybeInitializeVolume(final StorageManager storageManager, final Bundle bundle) {
        VolumeInfo volumeById = storageManager.findVolumeById(bundle.getString("android.os.storage.extra.VOLUME_ID", "private"));
        if (!isVolumeValid(volumeById)) {
            volumeById = null;
        }
        return volumeById;
    }
    
    public static void prepareCustomPreferencesList(final ViewGroup viewGroup, final View view, final View view2, final boolean b) {
        if (view2.getScrollBarStyle() == 33554432) {
            final Resources resources = view2.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(2131165546);
            final int dimensionPixelSize2 = resources.getDimensionPixelSize(17105260);
            if (viewGroup instanceof PreferenceFrameLayout) {
                ((PreferenceFrameLayout$LayoutParams)view.getLayoutParams()).removeBorders = true;
                if (b) {
                    dimensionPixelSize = 0;
                }
                view2.setPaddingRelative(dimensionPixelSize, 0, dimensionPixelSize, dimensionPixelSize2);
            }
            else {
                view2.setPaddingRelative(dimensionPixelSize, 0, dimensionPixelSize, dimensionPixelSize2);
            }
        }
    }
    
    public static void setEditTextCursorPosition(final EditText editText) {
        editText.setSelection(editText.getText().length());
    }
    
    public static void setSafeIcon(final Preference preference, final Drawable drawable) {
        Drawable safeDrawable = drawable;
        if (drawable != null) {
            safeDrawable = drawable;
            if (!(drawable instanceof VectorDrawable)) {
                safeDrawable = getSafeDrawable(drawable, 500, 500);
            }
        }
        preference.setIcon(safeDrawable);
    }
    
    public static boolean showSimCardTile(final Context context) {
        final int simCount = ((TelephonyManager)context.getSystemService("phone")).getSimCount();
        boolean b = true;
        if (simCount <= 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean startQuietModeDialogIfNecessary(final Context context, final UserManager userManager, final int n) {
        if (userManager.isQuietModeEnabled(UserHandle.of(n))) {
            context.startActivity(UnlaunchableAppActivity.createInQuietModeDialogIntent(n));
            return true;
        }
        return false;
    }
    
    public static boolean unlockWorkProfileIfNecessary(final Context context, final int n) {
        try {
            return ActivityManager.getService().isUserRunning(n, 2) && new LockPatternUtils(context).isSecure(n) && confirmWorkProfileCredentials(context, n);
        }
        catch (RemoteException ex) {
            return false;
        }
    }
    
    public static boolean updatePreferenceToSpecificActivityOrRemove(final Context context, final PreferenceGroup preferenceGroup, final String s, final int n) {
        final Preference preference = preferenceGroup.findPreference(s);
        if (preference == null) {
            return false;
        }
        final Intent intent = preference.getIntent();
        if (intent != null) {
            final PackageManager packageManager = context.getPackageManager();
            final List queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            for (int size = queryIntentActivities.size(), i = 0; i < size; ++i) {
                final ResolveInfo resolveInfo = queryIntentActivities.get(i);
                if ((resolveInfo.activityInfo.applicationInfo.flags & 0x1) != 0x0) {
                    preference.setIntent(new Intent().setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name));
                    if ((n & 0x1) != 0x0) {
                        preference.setTitle(resolveInfo.loadLabel(packageManager));
                    }
                    return true;
                }
            }
        }
        preferenceGroup.removePreference(preference);
        return false;
    }
}
