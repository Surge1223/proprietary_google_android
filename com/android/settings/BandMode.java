package com.android.settings;

import android.widget.ListAdapter;
import com.android.internal.telephony.PhoneFactory;
import android.os.Bundle;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.util.Log;
import android.os.AsyncResult;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.content.DialogInterface;
import com.android.internal.telephony.Phone;
import android.os.Handler;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.app.Activity;

public class BandMode extends Activity
{
    private static final String[] BAND_NAMES;
    private ListView mBandList;
    private ArrayAdapter mBandListAdapter;
    private AdapterView$OnItemClickListener mBandSelectionHandler;
    private Handler mHandler;
    private Phone mPhone;
    private DialogInterface mProgressPanel;
    private BandListItem mTargetBand;
    
    static {
        BAND_NAMES = new String[] { "Automatic", "Europe", "United States", "Japan", "Australia", "Australia 2", "Cellular 800", "PCS", "Class 3 (JTACS)", "Class 4 (Korea-PCS)", "Class 5", "Class 6 (IMT2000)", "Class 7 (700Mhz-Upper)", "Class 8 (1800Mhz-Upper)", "Class 9 (900Mhz)", "Class 10 (800Mhz-Secondary)", "Class 11 (Europe PAMR 400Mhz)", "Class 15 (US-AWS)", "Class 16 (US-2500Mhz)" };
    }
    
    public BandMode() {
        this.mTargetBand = null;
        this.mPhone = null;
        this.mBandSelectionHandler = (AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                BandMode.this.getWindow().setFeatureInt(5, -1);
                BandMode.this.mTargetBand = (BandListItem)adapterView.getAdapter().getItem(n);
                BandMode.this.mPhone.setBandMode(BandMode.this.mTargetBand.getBand(), BandMode.this.mHandler.obtainMessage(200));
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                final int what = message.what;
                if (what != 100) {
                    if (what == 200) {
                        final AsyncResult asyncResult = (AsyncResult)message.obj;
                        BandMode.this.getWindow().setFeatureInt(5, -2);
                        if (!BandMode.this.isFinishing()) {
                            BandMode.this.displayBandSelectionResult(asyncResult.exception);
                        }
                    }
                }
                else {
                    BandMode.this.bandListLoaded((AsyncResult)message.obj);
                }
            }
        };
    }
    
    private void bandListLoaded(final AsyncResult asyncResult) {
        if (this.mProgressPanel != null) {
            this.mProgressPanel.dismiss();
        }
        this.clearList();
        final boolean b = false;
        final Object result = asyncResult.result;
        final int n = 0;
        boolean b2 = b;
        if (result != null) {
            final int[] array = (int[])asyncResult.result;
            if (array.length == 0) {
                Log.wtf("phone", "No Supported Band Modes");
                return;
            }
            final int n2 = array[0];
            b2 = b;
            if (n2 > 0) {
                this.mBandListAdapter.add((Object)new BandListItem(0));
                for (int i = 1; i <= n2; ++i) {
                    if (array[i] != 0) {
                        this.mBandListAdapter.add((Object)new BandListItem(array[i]));
                    }
                }
                b2 = true;
            }
        }
        if (!b2) {
            for (int j = n; j < 19; ++j) {
                this.mBandListAdapter.add((Object)new BandListItem(j));
            }
        }
        this.mBandList.requestFocus();
    }
    
    private void clearList() {
        while (this.mBandListAdapter.getCount() > 0) {
            this.mBandListAdapter.remove(this.mBandListAdapter.getItem(0));
        }
    }
    
    private void displayBandSelectionResult(final Throwable t) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getString(2131886562));
        sb.append(" [");
        sb.append(this.mTargetBand.toString());
        sb.append("] ");
        final String string = sb.toString();
        String message;
        if (t != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append(this.getString(2131886560));
            message = sb2.toString();
        }
        else {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(this.getString(2131886563));
            message = sb3.toString();
        }
        this.mProgressPanel = (DialogInterface)new AlertDialog$Builder((Context)this).setMessage((CharSequence)message).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).show();
    }
    
    private void loadBandList() {
        this.mProgressPanel = (DialogInterface)new AlertDialog$Builder((Context)this).setMessage((CharSequence)this.getString(2131886561)).show();
        this.mPhone.queryAvailableBandMode(this.mHandler.obtainMessage(100));
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.requestWindowFeature(5);
        this.setContentView(2131558466);
        this.setTitle((CharSequence)this.getString(2131886564));
        this.getWindow().setLayout(-1, -2);
        this.mPhone = PhoneFactory.getDefaultPhone();
        this.mBandList = (ListView)this.findViewById(2131361900);
        this.mBandListAdapter = new ArrayAdapter((Context)this, 17367043);
        this.mBandList.setAdapter((ListAdapter)this.mBandListAdapter);
        this.mBandList.setOnItemClickListener(this.mBandSelectionHandler);
        this.loadBandList();
    }
    
    private static class BandListItem
    {
        private int mBandMode;
        
        public BandListItem(final int mBandMode) {
            this.mBandMode = 0;
            this.mBandMode = mBandMode;
        }
        
        public int getBand() {
            return this.mBandMode;
        }
        
        @Override
        public String toString() {
            if (this.mBandMode >= BandMode.BAND_NAMES.length) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Band mode ");
                sb.append(this.mBandMode);
                return sb.toString();
            }
            return BandMode.BAND_NAMES[this.mBandMode];
        }
    }
}
