package com.android.settings;

import android.text.Editable;
import android.view.ViewGroup;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.text.TextWatcher;
import android.content.DialogInterface;
import android.app.AlertDialog;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import android.security.KeyChain$KeyChainConnection;
import android.os.AsyncTask;
import android.security.KeyStore$State;
import java.io.IOException;
import android.security.KeyChain;
import sun.security.x509.AlgorithmId;
import sun.security.util.ObjectIdentifier;
import com.android.org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.io.InputStream;
import com.android.org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import android.content.Intent;
import com.android.settings.security.ConfigureKeyGuardDialog;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.widget.Toast;
import com.android.settings.vpn2.VpnUtils;
import android.content.Context;
import com.android.internal.widget.LockPatternUtils;
import android.content.pm.UserInfo;
import android.os.RemoteException;
import android.os.UserManager;
import android.os.UserHandle;
import android.os.Process;
import android.util.Log;
import android.app.ActivityManager;
import android.text.TextUtils;
import android.security.KeyStore;
import android.os.Bundle;
import android.app.Activity;

public final class CredentialStorage extends Activity
{
    private Bundle mInstallBundle;
    private final KeyStore mKeyStore;
    private int mRetriesRemaining;
    
    public CredentialStorage() {
        this.mKeyStore = KeyStore.getInstance();
        this.mRetriesRemaining = -1;
    }
    
    private boolean checkCallerIsCertInstallerOrSelfInProfile() {
        final boolean equals = TextUtils.equals((CharSequence)"com.android.certinstaller", (CharSequence)this.getCallingPackage());
        boolean b = true;
        if (equals) {
            if (this.getPackageManager().checkSignatures(this.getCallingPackage(), this.getPackageName()) != 0) {
                b = false;
            }
            return b;
        }
        try {
            final int launchedFromUid = ActivityManager.getService().getLaunchedFromUid(this.getActivityToken());
            if (launchedFromUid == -1) {
                Log.e("CredentialStorage", "com.android.credentials.INSTALL must be started with startActivityForResult");
                return false;
            }
            if (!UserHandle.isSameApp(launchedFromUid, Process.myUid())) {
                return false;
            }
            final UserInfo profileParent = ((UserManager)this.getSystemService("user")).getProfileParent(UserHandle.getUserId(launchedFromUid));
            return profileParent != null && profileParent.id == UserHandle.myUserId();
        }
        catch (RemoteException ex) {
            return false;
        }
    }
    
    private boolean checkKeyGuardQuality() {
        return new LockPatternUtils((Context)this).getActivePasswordQuality(UserManager.get((Context)this).getCredentialOwnerProfile(UserHandle.myUserId())) >= 65536;
    }
    
    private void clearLegacyVpnIfEstablished() {
        if (VpnUtils.disconnectLegacyVpn(this.getApplicationContext())) {
            Toast.makeText((Context)this, 2131889815, 0).show();
        }
    }
    
    private boolean confirmKeyGuard(final int n) {
        return new ChooseLockSettingsHelper(this).launchConfirmationActivity(n, this.getResources().getText(2131887158), true);
    }
    
    private void ensureKeyGuard() {
        if (!this.checkKeyGuardQuality()) {
            new ConfigureKeyGuardDialog().show(this.getFragmentManager(), "ConfigureKeyGuardDialog");
            return;
        }
        if (this.confirmKeyGuard(1)) {
            return;
        }
        this.finish();
    }
    
    private void handleUnlockOrInstall() {
        if (this.isFinishing()) {
            return;
        }
        switch (this.mKeyStore.state()) {
            default: {}
            case UNLOCKED: {
                if (!this.checkKeyGuardQuality()) {
                    new ConfigureKeyGuardDialog().show(this.getFragmentManager(), "ConfigureKeyGuardDialog");
                    return;
                }
                this.installIfAvailable();
                this.finish();
            }
            case LOCKED: {
                new UnlockDialog();
            }
            case UNINITIALIZED: {
                this.ensureKeyGuard();
            }
        }
    }
    
    private void installIfAvailable() {
        if (this.mInstallBundle == null || this.mInstallBundle.isEmpty()) {
            return;
        }
        final Bundle mInstallBundle = this.mInstallBundle;
        this.mInstallBundle = null;
        final int int1 = mInstallBundle.getInt("install_as_uid", -1);
        if (int1 == -1 || UserHandle.isSameUser(int1, Process.myUid())) {
            if (mInstallBundle.containsKey("user_private_key_name")) {
                final String string = mInstallBundle.getString("user_private_key_name");
                final byte[] byteArray = mInstallBundle.getByteArray("user_private_key_data");
                int n = 1;
                if (int1 == 1010) {
                    n = n;
                    if (this.isHardwareBackedKey(byteArray)) {
                        Log.d("CredentialStorage", "Saving private key with FLAG_NONE for WIFI_UID");
                        n = 0;
                    }
                }
                if (!this.mKeyStore.importKey(string, byteArray, int1, n)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to install ");
                    sb.append(string);
                    sb.append(" as uid ");
                    sb.append(int1);
                    Log.e("CredentialStorage", sb.toString());
                    return;
                }
                if (int1 == 1000 || int1 == -1) {
                    new MarkKeyAsUserSelectable(string.replaceFirst("^USRPKEY_", "")).execute((Object[])new Void[0]);
                }
            }
            if (mInstallBundle.containsKey("user_certificate_name")) {
                final String string2 = mInstallBundle.getString("user_certificate_name");
                if (!this.mKeyStore.put(string2, mInstallBundle.getByteArray("user_certificate_data"), int1, 0)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to install ");
                    sb2.append(string2);
                    sb2.append(" as uid ");
                    sb2.append(int1);
                    Log.e("CredentialStorage", sb2.toString());
                    return;
                }
            }
            if (mInstallBundle.containsKey("ca_certificates_name")) {
                final String string3 = mInstallBundle.getString("ca_certificates_name");
                if (!this.mKeyStore.put(string3, mInstallBundle.getByteArray("ca_certificates_data"), int1, 0)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Failed to install ");
                    sb3.append(string3);
                    sb3.append(" as uid ");
                    sb3.append(int1);
                    Log.e("CredentialStorage", sb3.toString());
                    return;
                }
            }
            this.sendBroadcast(new Intent("android.security.action.KEYCHAIN_CHANGED"));
            this.setResult(-1);
            return;
        }
        final int userId = UserHandle.getUserId(int1);
        UserHandle.myUserId();
        if (int1 != 1010) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Failed to install credentials as uid ");
            sb4.append(int1);
            sb4.append(": cross-user installs may only target wifi uids");
            Log.e("CredentialStorage", sb4.toString());
            return;
        }
        this.startActivityAsUser(new Intent("com.android.credentials.INSTALL").setFlags(33554432).putExtras(mInstallBundle), new UserHandle(userId));
    }
    
    private boolean isHardwareBackedKey(final byte[] array) {
        try {
            return KeyChain.isBoundKeyAlgorithm(new AlgorithmId(new ObjectIdentifier(PrivateKeyInfo.getInstance((Object)new ASN1InputStream((InputStream)new ByteArrayInputStream(array)).readObject()).getAlgorithmId().getAlgorithm().getId())).getName());
        }
        catch (IOException ex) {
            Log.e("CredentialStorage", "Failed to parse key data");
            return false;
        }
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 1) {
            if (n2 == -1) {
                final String stringExtra = intent.getStringExtra("password");
                if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
                    this.mKeyStore.unlock(stringExtra);
                    return;
                }
            }
            this.finish();
        }
        else if (n == 2) {
            if (n2 == -1) {
                new ResetKeyStoreAndKeyChain().execute((Object[])new Void[0]);
                return;
            }
            this.finish();
        }
    }
    
    protected void onResume() {
        super.onResume();
        final Intent intent = this.getIntent();
        final String action = intent.getAction();
        if (!((UserManager)this.getSystemService("user")).hasUserRestriction("no_config_credentials")) {
            if ("com.android.credentials.RESET".equals(action)) {
                new ResetDialog();
            }
            else {
                if ("com.android.credentials.INSTALL".equals(action) && this.checkCallerIsCertInstallerOrSelfInProfile()) {
                    this.mInstallBundle = intent.getExtras();
                }
                this.handleUnlockOrInstall();
            }
        }
        else if ("com.android.credentials.UNLOCK".equals(action) && this.mKeyStore.state() == KeyStore$State.UNINITIALIZED) {
            this.ensureKeyGuard();
        }
        else {
            this.finish();
        }
    }
    
    private class MarkKeyAsUserSelectable extends AsyncTask<Void, Void, Boolean>
    {
        final String mAlias;
        
        public MarkKeyAsUserSelectable(final String mAlias) {
            this.mAlias = mAlias;
        }
        
        protected Boolean doInBackground(Void... array) {
            try {
                final KeyChain$KeyChainConnection bind = KeyChain.bind((Context)CredentialStorage.this);
                array = null;
                try {
                    try {
                        bind.getService().setUserSelectable(this.mAlias, true);
                        if (bind != null) {
                            bind.close();
                        }
                        return true;
                    }
                    finally {
                        if (bind != null) {
                            if (array != null) {
                                final KeyChain$KeyChainConnection keyChain$KeyChainConnection = bind;
                                keyChain$KeyChainConnection.close();
                            }
                            else {
                                bind.close();
                            }
                        }
                    }
                }
                catch (Throwable t) {}
                try {
                    final KeyChain$KeyChainConnection keyChain$KeyChainConnection = bind;
                    keyChain$KeyChainConnection.close();
                }
                catch (Throwable t2) {}
            }
            catch (InterruptedException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to mark key ");
                sb.append(this.mAlias);
                sb.append(" as user-selectable.");
                Log.w("CredentialStorage", sb.toString());
                Thread.currentThread().interrupt();
                return false;
            }
            catch (RemoteException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to mark key ");
                sb2.append(this.mAlias);
                sb2.append(" as user-selectable.");
                Log.w("CredentialStorage", sb2.toString());
                return false;
            }
        }
    }
    
    private class ResetDialog implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
    {
        private boolean mResetConfirmed;
        
        private ResetDialog() {
            final AlertDialog create = new AlertDialog$Builder((Context)CredentialStorage.this).setTitle(17039380).setMessage(2131887153).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
            create.setOnDismissListener((DialogInterface$OnDismissListener)this);
            create.show();
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            this.mResetConfirmed = (n == -1);
        }
        
        public void onDismiss(final DialogInterface dialogInterface) {
            if (this.mResetConfirmed) {
                this.mResetConfirmed = false;
                if (CredentialStorage.this.confirmKeyGuard(2)) {
                    return;
                }
            }
            CredentialStorage.this.finish();
        }
    }
    
    private class ResetKeyStoreAndKeyChain extends AsyncTask<Void, Void, Boolean>
    {
        protected Boolean doInBackground(Void... bind) {
            new LockPatternUtils((Context)CredentialStorage.this).resetKeyStore(UserHandle.myUserId());
            try {
                bind = (Void[])(Object)KeyChain.bind((Context)CredentialStorage.this);
                try {
                    final boolean reset = ((KeyChain$KeyChainConnection)(Object)bind).getService().reset();
                    ((KeyChain$KeyChainConnection)(Object)bind).close();
                    return reset;
                }
                catch (RemoteException ex) {
                    ((KeyChain$KeyChainConnection)(Object)bind).close();
                    return false;
                }
                ((KeyChain$KeyChainConnection)(Object)bind).close();
            }
            catch (InterruptedException ex2) {
                Thread.currentThread().interrupt();
                return false;
            }
        }
        
        protected void onPostExecute(final Boolean b) {
            if (b) {
                Toast.makeText((Context)CredentialStorage.this, 2131887147, 0).show();
                CredentialStorage.this.clearLegacyVpnIfEstablished();
            }
            else {
                Toast.makeText((Context)CredentialStorage.this, 2131887150, 0).show();
            }
            CredentialStorage.this.finish();
        }
    }
    
    private class UnlockDialog implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener, TextWatcher
    {
        private final Button mButton;
        private final TextView mError;
        private final TextView mOldPassword;
        private boolean mUnlockConfirmed;
        
        private UnlockDialog() {
            final View inflate = View.inflate((Context)CredentialStorage.this, 2131558507, (ViewGroup)null);
            CharSequence text;
            if (CredentialStorage.this.mRetriesRemaining == -1) {
                text = CredentialStorage.this.getResources().getText(2131887160);
            }
            else if (CredentialStorage.this.mRetriesRemaining > 3) {
                text = CredentialStorage.this.getResources().getText(2131887161);
            }
            else if (CredentialStorage.this.mRetriesRemaining == 1) {
                text = CredentialStorage.this.getResources().getText(2131887155);
            }
            else {
                text = CredentialStorage.this.getString(2131887156, new Object[] { CredentialStorage.this.mRetriesRemaining });
            }
            ((TextView)inflate.findViewById(2131362227)).setText(text);
            (this.mOldPassword = (TextView)inflate.findViewById(2131362414)).setVisibility(0);
            this.mOldPassword.addTextChangedListener((TextWatcher)this);
            this.mError = (TextView)inflate.findViewById(2131362125);
            final AlertDialog create = new AlertDialog$Builder((Context)CredentialStorage.this).setView(inflate).setTitle(2131887159).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
            create.setOnDismissListener((DialogInterface$OnDismissListener)this);
            create.show();
            (this.mButton = create.getButton(-1)).setEnabled(false);
        }
        
        public void afterTextChanged(final Editable editable) {
            this.mButton.setEnabled(this.mOldPassword == null || this.mOldPassword.getText().length() > 0);
        }
        
        public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            this.mUnlockConfirmed = (n == -1);
        }
        
        public void onDismiss(final DialogInterface dialogInterface) {
            if (this.mUnlockConfirmed) {
                this.mUnlockConfirmed = false;
                this.mError.setVisibility(0);
                CredentialStorage.this.mKeyStore.unlock(this.mOldPassword.getText().toString());
                final int lastError = CredentialStorage.this.mKeyStore.getLastError();
                if (lastError == 1) {
                    CredentialStorage.this.mRetriesRemaining = -1;
                    Toast.makeText((Context)CredentialStorage.this, 2131887146, 0).show();
                    CredentialStorage.this.ensureKeyGuard();
                }
                else if (lastError == 3) {
                    CredentialStorage.this.mRetriesRemaining = -1;
                    Toast.makeText((Context)CredentialStorage.this, 2131887147, 0).show();
                    CredentialStorage.this.handleUnlockOrInstall();
                }
                else if (lastError >= 10) {
                    CredentialStorage.this.mRetriesRemaining = lastError - 10 + 1;
                    CredentialStorage.this.handleUnlockOrInstall();
                }
                return;
            }
            CredentialStorage.this.finish();
        }
        
        public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        }
    }
}
