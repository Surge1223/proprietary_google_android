package com.android.settings;

import android.widget.Switch;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.res.Resources$NotFoundException;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.os.Handler;
import android.content.IntentFilter;
import android.app.Activity;
import android.os.Parcelable;
import android.widget.ListView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.overlay.FeatureFactory;
import android.view.View;
import android.os.Bundle;
import android.os.UserHandle;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import java.util.Collections;
import java.util.Collection;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.util.Log;
import android.app.AppGlobals;
import java.util.List;
import android.app.admin.DeviceAdminInfo;
import android.content.Intent;
import android.content.Context;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import android.os.UserManager;
import android.content.ComponentName;
import android.util.SparseArray;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import java.util.ArrayList;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.app.ListFragment;

public class DeviceAdminSettings extends ListFragment implements Instrumentable
{
    private final ArrayList<DeviceAdminListItem> mAdmins;
    private final BroadcastReceiver mBroadcastReceiver;
    private DevicePolicyManager mDPM;
    private String mDeviceOwnerPkg;
    private SparseArray<ComponentName> mProfileOwnerComponents;
    private UserManager mUm;
    private VisibilityLoggerMixin mVisibilityLoggerMixin;
    
    public DeviceAdminSettings() {
        this.mAdmins = new ArrayList<DeviceAdminListItem>();
        this.mProfileOwnerComponents = (SparseArray<ComponentName>)new SparseArray();
        this.mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if ("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED".equals(intent.getAction())) {
                    DeviceAdminSettings.this.updateList();
                }
            }
        };
    }
    
    private void addActiveAdminsForProfile(final List<ComponentName> list, final int n) {
        if (list != null) {
            final PackageManager packageManager = this.getActivity().getPackageManager();
            final IPackageManager packageManager2 = AppGlobals.getPackageManager();
            for (int size = list.size(), i = 0; i < size; ++i) {
                Object o = list.get(i);
                try {
                    final DeviceAdminInfo deviceAdminInfo = this.createDeviceAdminInfo(packageManager2.getReceiverInfo((ComponentName)o, 819328, n));
                    if (deviceAdminInfo != null) {
                        o = new DeviceAdminListItem();
                        ((DeviceAdminListItem)o).info = deviceAdminInfo;
                        ((DeviceAdminListItem)o).name = deviceAdminInfo.loadLabel(packageManager).toString();
                        ((DeviceAdminListItem)o).active = true;
                        this.mAdmins.add((DeviceAdminListItem)o);
                    }
                }
                catch (RemoteException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to load component: ");
                    sb.append(o);
                    Log.w("DeviceAdminSettings", sb.toString());
                }
            }
        }
    }
    
    private void addDeviceAdminBroadcastReceiversForProfile(final Collection<ComponentName> collection, int i) {
        final PackageManager packageManager = this.getActivity().getPackageManager();
        List<ResolveInfo> list;
        if ((list = (List<ResolveInfo>)packageManager.queryBroadcastReceiversAsUser(new Intent("android.app.action.DEVICE_ADMIN_ENABLED"), 32896, i)) == null) {
            list = Collections.emptyList();
        }
        int size;
        ResolveInfo resolveInfo;
        ComponentName componentName;
        DeviceAdminInfo deviceAdminInfo;
        DeviceAdminListItem deviceAdminListItem;
        for (size = list.size(), i = 0; i < size; ++i) {
            resolveInfo = list.get(i);
            componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
            if (collection == null || !collection.contains(componentName)) {
                deviceAdminInfo = this.createDeviceAdminInfo(resolveInfo.activityInfo);
                if (deviceAdminInfo != null && deviceAdminInfo.isVisible()) {
                    if (deviceAdminInfo.getActivityInfo().applicationInfo.isInternal()) {
                        deviceAdminListItem = new DeviceAdminListItem();
                        deviceAdminListItem.info = deviceAdminInfo;
                        deviceAdminListItem.name = deviceAdminInfo.loadLabel(packageManager).toString();
                        deviceAdminListItem.active = false;
                        this.mAdmins.add(deviceAdminListItem);
                    }
                }
            }
        }
    }
    
    private DeviceAdminInfo createDeviceAdminInfo(final ActivityInfo activityInfo) {
        try {
            return new DeviceAdminInfo((Context)this.getActivity(), activityInfo);
        }
        catch (XmlPullParserException | IOException ex) {
            final Object o2;
            final Object o = o2;
            final StringBuilder sb = new StringBuilder();
            sb.append("Skipping ");
            sb.append(activityInfo);
            Log.w("DeviceAdminSettings", sb.toString(), (Throwable)o);
            return null;
        }
    }
    
    private int getUserId(final DeviceAdminInfo deviceAdminInfo) {
        return UserHandle.getUserId(deviceAdminInfo.getActivityInfo().applicationInfo.uid);
    }
    
    private boolean isActiveAdmin(final DeviceAdminInfo deviceAdminInfo) {
        return this.mDPM.isAdminActiveAsUser(deviceAdminInfo.getComponent(), this.getUserId(deviceAdminInfo));
    }
    
    private boolean isRemovingAdmin(final DeviceAdminInfo deviceAdminInfo) {
        return this.mDPM.isRemovingAdmin(deviceAdminInfo.getComponent(), this.getUserId(deviceAdminInfo));
    }
    
    private void updateAvailableAdminsForProfile(final int n) {
        final List activeAdminsAsUser = this.mDPM.getActiveAdminsAsUser(n);
        this.addActiveAdminsForProfile(activeAdminsAsUser, n);
        this.addDeviceAdminBroadcastReceiversForProfile(activeAdminsAsUser, n);
    }
    
    public int getMetricsCategory() {
        return 516;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.setHasOptionsMenu(true);
        Utils.forceCustomPadding((View)this.getListView(), true);
        this.getActivity().setTitle(2131888212);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mVisibilityLoggerMixin = new VisibilityLoggerMixin(this.getMetricsCategory(), FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider());
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mDPM = (DevicePolicyManager)this.getActivity().getSystemService("device_policy");
        this.mUm = (UserManager)this.getActivity().getSystemService("user");
        return layoutInflater.inflate(2131558538, viewGroup, false);
    }
    
    public void onListItemClick(final ListView listView, final View view, final int n, final long n2) {
        final DeviceAdminInfo deviceAdminInfo = (DeviceAdminInfo)listView.getAdapter().getItem(n);
        final UserHandle userHandle = new UserHandle(this.getUserId(deviceAdminInfo));
        final Activity activity = this.getActivity();
        final Intent intent = new Intent((Context)activity, (Class)DeviceAdminAdd.class);
        intent.putExtra("android.app.extra.DEVICE_ADMIN", (Parcelable)deviceAdminInfo.getComponent());
        activity.startActivityAsUser(intent, userHandle);
    }
    
    public void onPause() {
        this.getActivity().unregisterReceiver(this.mBroadcastReceiver);
        this.mVisibilityLoggerMixin.onPause();
        super.onPause();
    }
    
    public void onResume() {
        super.onResume();
        final Activity activity = this.getActivity();
        this.mVisibilityLoggerMixin.onResume();
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED");
        activity.registerReceiverAsUser(this.mBroadcastReceiver, UserHandle.ALL, intentFilter, (String)null, (Handler)null);
        final ComponentName deviceOwnerComponentOnAnyUser = this.mDPM.getDeviceOwnerComponentOnAnyUser();
        String packageName;
        if (deviceOwnerComponentOnAnyUser != null) {
            packageName = deviceOwnerComponentOnAnyUser.getPackageName();
        }
        else {
            packageName = null;
        }
        this.mDeviceOwnerPkg = packageName;
        this.mProfileOwnerComponents.clear();
        final List userProfiles = this.mUm.getUserProfiles();
        for (int size = userProfiles.size(), i = 0; i < size; ++i) {
            final int identifier = userProfiles.get(i).getIdentifier();
            this.mProfileOwnerComponents.put(identifier, (Object)this.mDPM.getProfileOwnerAsUser(identifier));
        }
        this.updateList();
    }
    
    void updateList() {
        this.mAdmins.clear();
        final List userProfiles = this.mUm.getUserProfiles();
        for (int size = userProfiles.size(), i = 0; i < size; ++i) {
            this.updateAvailableAdminsForProfile(userProfiles.get(i).getIdentifier());
        }
        Collections.sort(this.mAdmins);
        this.getListView().setAdapter((ListAdapter)new PolicyListAdapter());
    }
    
    private static class DeviceAdminListItem implements Comparable<DeviceAdminListItem>
    {
        public boolean active;
        public DeviceAdminInfo info;
        public String name;
        
        @Override
        public int compareTo(final DeviceAdminListItem deviceAdminListItem) {
            if (this.active != deviceAdminListItem.active) {
                int n;
                if (this.active) {
                    n = -1;
                }
                else {
                    n = 1;
                }
                return n;
            }
            return this.name.compareTo(deviceAdminListItem.name);
        }
    }
    
    class PolicyListAdapter extends BaseAdapter
    {
        final LayoutInflater mInflater;
        
        PolicyListAdapter() {
            this.mInflater = (LayoutInflater)DeviceAdminSettings.this.getActivity().getSystemService("layout_inflater");
        }
        
        private void bindView(View view, final DeviceAdminInfo deviceAdminInfo) {
            final Activity activity = DeviceAdminSettings.this.getActivity();
            view = (View)view.getTag();
            ((ViewHolder)view).icon.setImageDrawable(activity.getPackageManager().getUserBadgedIcon(deviceAdminInfo.loadIcon(activity.getPackageManager()), new UserHandle(DeviceAdminSettings.this.getUserId(deviceAdminInfo))));
            ((ViewHolder)view).name.setText(deviceAdminInfo.loadLabel(activity.getPackageManager()));
            ((ViewHolder)view).checkbox.setChecked(DeviceAdminSettings.this.isActiveAdmin(deviceAdminInfo));
            final boolean enabled = this.isEnabled(deviceAdminInfo);
            try {
                ((ViewHolder)view).description.setText(deviceAdminInfo.loadDescription(activity.getPackageManager()));
            }
            catch (Resources$NotFoundException ex) {}
            ((ViewHolder)view).checkbox.setEnabled(enabled);
            ((ViewHolder)view).name.setEnabled(enabled);
            ((ViewHolder)view).description.setEnabled(enabled);
            ((ViewHolder)view).icon.setEnabled(enabled);
        }
        
        private boolean isEnabled(final Object o) {
            return !DeviceAdminSettings.this.isRemovingAdmin((DeviceAdminInfo)o);
        }
        
        private View newDeviceAdminView(final ViewGroup viewGroup) {
            final View inflate = this.mInflater.inflate(2131558537, viewGroup, false);
            final ViewHolder tag = new ViewHolder();
            tag.icon = (ImageView)inflate.findViewById(R.id.icon);
            tag.name = (TextView)inflate.findViewById(2131362382);
            tag.checkbox = (Switch)inflate.findViewById(2131361981);
            tag.description = (TextView)inflate.findViewById(2131362067);
            inflate.setTag((Object)tag);
            return inflate;
        }
        
        public boolean areAllItemsEnabled() {
            return false;
        }
        
        public int getCount() {
            return DeviceAdminSettings.this.mAdmins.size();
        }
        
        public Object getItem(final int n) {
            return DeviceAdminSettings.this.mAdmins.get(n).info;
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public int getItemViewType(final int n) {
            return 0;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final Object item = this.getItem(n);
            View deviceAdminView = view;
            if (view == null) {
                deviceAdminView = this.newDeviceAdminView(viewGroup);
            }
            this.bindView(deviceAdminView, (DeviceAdminInfo)item);
            return deviceAdminView;
        }
        
        public int getViewTypeCount() {
            return 1;
        }
        
        public boolean hasStableIds() {
            return false;
        }
        
        public boolean isEnabled(final int n) {
            return this.isEnabled(this.getItem(n));
        }
    }
    
    static class ViewHolder
    {
        Switch checkbox;
        TextView description;
        ImageView icon;
        TextView name;
    }
}
