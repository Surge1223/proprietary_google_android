package com.android.settings.network;

import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.Context;

public class NetworkResetRestrictionChecker
{
    private final Context mContext;
    private final UserManager mUserManager;
    
    public NetworkResetRestrictionChecker(final Context mContext) {
        this.mContext = mContext;
        this.mUserManager = (UserManager)mContext.getSystemService("user");
    }
    
    boolean hasUserBaseRestriction() {
        return RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_network_reset", UserHandle.myUserId());
    }
    
    boolean hasUserRestriction() {
        return !this.mUserManager.isAdminUser() || this.hasUserBaseRestriction();
    }
    
    boolean isRestrictionEnforcedByAdmin() {
        return RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_network_reset", UserHandle.myUserId()) != null;
    }
}
