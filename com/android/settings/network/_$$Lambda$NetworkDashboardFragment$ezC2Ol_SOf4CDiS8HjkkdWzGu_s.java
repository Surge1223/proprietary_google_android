package com.android.settings.network;

import android.text.BidiFormatter;
import android.app.AlertDialog$Builder;
import android.util.Log;
import android.app.Dialog;
import java.util.ArrayList;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.wifi.WifiMasterSwitchPreferenceController;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.search.BaseSearchIndexProvider;
import android.content.Context;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$NetworkDashboardFragment$ezC2Ol_SOf4CDiS8HjkkdWzGu_s implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        NetworkDashboardFragment.lambda$onCreateDialog$0(this.f$0, dialogInterface, n);
    }
}
