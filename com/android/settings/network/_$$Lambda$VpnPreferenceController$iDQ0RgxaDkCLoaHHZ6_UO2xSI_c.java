package com.android.settings.network;

import com.android.internal.net.LegacyVpnInfo;
import java.util.Iterator;
import android.os.RemoteException;
import com.android.settingslib.utils.ThreadUtils;
import android.content.pm.UserInfo;
import android.util.SparseArray;
import com.android.settingslib.RestrictedLockUtils;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.UserHandle;
import com.android.internal.net.VpnConfig;
import android.support.v7.preference.PreferenceScreen;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import android.provider.Settings;
import android.net.Network;
import android.content.Context;
import android.net.NetworkRequest$Builder;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.IConnectivityManager;
import android.net.ConnectivityManager;
import android.net.NetworkRequest;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public final class _$$Lambda$VpnPreferenceController$iDQ0RgxaDkCLoaHHZ6_UO2xSI_c implements Runnable
{
    @Override
    public final void run() {
        VpnPreferenceController.lambda$updateSummary$0(this.f$0, this.f$1);
    }
}
