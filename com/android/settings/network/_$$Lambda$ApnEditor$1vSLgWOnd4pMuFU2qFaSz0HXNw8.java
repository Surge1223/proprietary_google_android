package com.android.settings.network;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.app.Fragment;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import java.util.Iterator;
import android.telephony.ServiceState;
import android.view.MenuItem;
import android.view.KeyEvent;
import android.view.View;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.PersistableBundle;
import android.content.Intent;
import android.provider.Telephony$Carriers;
import android.telephony.CarrierConfigManager;
import android.os.Bundle;
import android.database.Cursor;
import java.util.HashSet;
import com.android.settingslib.utils.ThreadUtils;
import com.android.internal.telephony.PhoneConstants;
import java.util.Set;
import java.util.List;
import android.util.Log;
import java.util.Arrays;
import android.text.TextUtils;
import com.android.internal.util.ArrayUtils;
import android.telephony.TelephonyManager;
import android.support.v14.preference.SwitchPreference;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.EditTextPreference;
import android.view.View$OnKeyListener;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;
import android.content.ContentValues;
import android.net.Uri;

public final class _$$Lambda$ApnEditor$1vSLgWOnd4pMuFU2qFaSz0HXNw8 implements Runnable
{
    @Override
    public final void run() {
        ApnEditor.lambda$updateApnDataToDatabase$0(this.f$0, this.f$1, this.f$2);
    }
}
