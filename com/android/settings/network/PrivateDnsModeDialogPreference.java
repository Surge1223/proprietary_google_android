package com.android.settings.network;

import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.content.DialogInterface;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.text.Editable;
import android.content.Intent;
import android.content.ActivityNotFoundException;
import android.util.Log;
import com.android.settingslib.HelpUtils;
import android.view.View;
import android.system.Os;
import android.app.AlertDialog;
import android.widget.Button;
import android.provider.Settings;
import android.content.ContentResolver;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.content.Context;
import android.system.OsConstants;
import java.util.HashMap;
import com.android.settings.utils.AnnotationSpan;
import android.widget.RadioGroup;
import android.widget.EditText;
import java.util.Map;
import android.widget.RadioGroup$OnCheckedChangeListener;
import android.text.TextWatcher;
import android.content.DialogInterface$OnClickListener;
import com.android.settingslib.CustomDialogPreference;

public class PrivateDnsModeDialogPreference extends CustomDialogPreference implements DialogInterface$OnClickListener, TextWatcher, RadioGroup$OnCheckedChangeListener
{
    private static final int[] ADDRESS_FAMILIES;
    static final String HOSTNAME_KEY = "private_dns_specifier";
    static final String MODE_KEY = "private_dns_mode";
    private static final Map<String, Integer> PRIVATE_DNS_MAP;
    EditText mEditText;
    String mMode;
    RadioGroup mRadioGroup;
    private final AnnotationSpan.LinkInfo mUrlLinkInfo;
    
    static {
        (PRIVATE_DNS_MAP = new HashMap<String, Integer>()).put("off", 2131362472);
        PrivateDnsModeDialogPreference.PRIVATE_DNS_MAP.put("opportunistic", 2131362473);
        PrivateDnsModeDialogPreference.PRIVATE_DNS_MAP.put("hostname", 2131362474);
        ADDRESS_FAMILIES = new int[] { OsConstants.AF_INET, OsConstants.AF_INET6 };
    }
    
    public PrivateDnsModeDialogPreference(final Context context) {
        super(context);
        this.mUrlLinkInfo = new AnnotationSpan.LinkInfo("url", (View.OnClickListener)_$$Lambda$PrivateDnsModeDialogPreference$I1bK8FTmQSNCc_qXqZ0usMONEsU.INSTANCE);
    }
    
    public PrivateDnsModeDialogPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mUrlLinkInfo = new AnnotationSpan.LinkInfo("url", (View.OnClickListener)_$$Lambda$PrivateDnsModeDialogPreference$I1bK8FTmQSNCc_qXqZ0usMONEsU.INSTANCE);
    }
    
    public PrivateDnsModeDialogPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mUrlLinkInfo = new AnnotationSpan.LinkInfo("url", (View.OnClickListener)_$$Lambda$PrivateDnsModeDialogPreference$I1bK8FTmQSNCc_qXqZ0usMONEsU.INSTANCE);
    }
    
    public PrivateDnsModeDialogPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mUrlLinkInfo = new AnnotationSpan.LinkInfo("url", (View.OnClickListener)_$$Lambda$PrivateDnsModeDialogPreference$I1bK8FTmQSNCc_qXqZ0usMONEsU.INSTANCE);
    }
    
    public static String getHostnameFromSettings(final ContentResolver contentResolver) {
        return Settings.Global.getString(contentResolver, "private_dns_specifier");
    }
    
    public static String getModeFromSettings(final ContentResolver contentResolver) {
        String s;
        if (!PrivateDnsModeDialogPreference.PRIVATE_DNS_MAP.containsKey(s = Settings.Global.getString(contentResolver, "private_dns_mode"))) {
            s = Settings.Global.getString(contentResolver, "private_dns_default_mode");
        }
        String s2;
        if (PrivateDnsModeDialogPreference.PRIVATE_DNS_MAP.containsKey(s)) {
            s2 = s;
        }
        else {
            s2 = "opportunistic";
        }
        return s2;
    }
    
    private Button getSaveButton() {
        final AlertDialog alertDialog = (AlertDialog)this.getDialog();
        if (alertDialog == null) {
            return null;
        }
        return alertDialog.getButton(-1);
    }
    
    private boolean isWeaklyValidatedHostname(final String s) {
        if (!s.matches("^[a-zA-Z0-9_.-]+$")) {
            return false;
        }
        final int[] address_FAMILIES = PrivateDnsModeDialogPreference.ADDRESS_FAMILIES;
        for (int length = address_FAMILIES.length, i = 0; i < length; ++i) {
            if (Os.inet_pton(address_FAMILIES[i], s) != null) {
                return false;
            }
        }
        return true;
    }
    
    private void updateDialogInfo() {
        final boolean equals = "hostname".equals(this.mMode);
        if (this.mEditText != null) {
            this.mEditText.setEnabled(equals);
        }
        final Button saveButton = this.getSaveButton();
        if (saveButton != null) {
            saveButton.setEnabled(!equals || this.isWeaklyValidatedHostname(this.mEditText.getText().toString()));
        }
    }
    
    public void afterTextChanged(final Editable editable) {
        this.updateDialogInfo();
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        final Context context = this.getContext();
        final ContentResolver contentResolver = context.getContentResolver();
        this.mMode = getModeFromSettings(context.getContentResolver());
        (this.mEditText = (EditText)view.findViewById(2131362475)).addTextChangedListener((TextWatcher)this);
        this.mEditText.setText((CharSequence)getHostnameFromSettings(contentResolver));
        (this.mRadioGroup = (RadioGroup)view.findViewById(2131362476)).setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)this);
        this.mRadioGroup.check((int)PrivateDnsModeDialogPreference.PRIVATE_DNS_MAP.getOrDefault(this.mMode, 2131362473));
        final TextView textView = (TextView)view.findViewById(2131362471);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        final AnnotationSpan.LinkInfo linkInfo = new AnnotationSpan.LinkInfo(context, "url", HelpUtils.getHelpIntent(context, context.getString(2131887782), context.getClass().getName()));
        if (linkInfo.isActionable()) {
            textView.setText(AnnotationSpan.linkify(context.getText(2131888669), linkInfo));
        }
    }
    
    public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
        switch (n) {
            case 2131362474: {
                this.mMode = "hostname";
                break;
            }
            case 2131362473: {
                this.mMode = "opportunistic";
                break;
            }
            case 2131362472: {
                this.mMode = "off";
                break;
            }
        }
        this.updateDialogInfo();
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            final Context context = this.getContext();
            if (this.mMode.equals("hostname")) {
                Settings.Global.putString(context.getContentResolver(), "private_dns_specifier", this.mEditText.getText().toString());
            }
            FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 1249, this.mMode, (Pair<Integer, Object>[])new Pair[0]);
            Settings.Global.putString(context.getContentResolver(), "private_dns_mode", this.mMode);
        }
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
}
