package com.android.settings.network;

import android.text.TextUtils;
import android.net.Uri;
import android.content.Intent;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.app.Fragment;
import android.support.v14.preference.SwitchPreference;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.AirplaneModeEnabler;
import com.android.settings.core.TogglePreferenceController;

public class AirplaneModePreferenceController extends TogglePreferenceController implements OnAirplaneModeChangedListener, LifecycleObserver, OnPause, OnResume
{
    private static final String EXIT_ECM_RESULT = "exit_ecm_result";
    public static final int REQUEST_CODE_EXIT_ECM = 1;
    private AirplaneModeEnabler mAirplaneModeEnabler;
    private SwitchPreference mAirplaneModePreference;
    private Fragment mFragment;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    
    public AirplaneModePreferenceController(final Context context, final String s) {
        super(context, s);
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mAirplaneModeEnabler = new AirplaneModeEnabler(this.mContext, this.mMetricsFeatureProvider, (AirplaneModeEnabler.OnAirplaneModeChangedListener)this);
    }
    
    public static boolean isAvailable(final Context context) {
        return context.getResources().getBoolean(2131034166) && !context.getPackageManager().hasSystemFeature("android.software.leanback");
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mAirplaneModePreference = (SwitchPreference)preferenceScreen.findPreference(this.getPreferenceKey());
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (isAvailable(this.mContext)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("airplane_mode".equals(preference.getKey()) && Boolean.parseBoolean(SystemProperties.get("ril.cdma.inecmmode"))) {
            if (this.mFragment != null) {
                this.mFragment.startActivityForResult(new Intent("com.android.internal.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS", (Uri)null), 1);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isChecked() {
        return this.mAirplaneModeEnabler.isAirplaneModeOn();
    }
    
    @Override
    public boolean isSliceable() {
        return TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"toggle_airplane");
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1) {
            this.mAirplaneModeEnabler.setAirplaneModeInECM(Boolean.valueOf(intent.getBooleanExtra("exit_ecm_result", false)), this.mAirplaneModePreference.isChecked());
        }
    }
    
    @Override
    public void onAirplaneModeChanged(final boolean checked) {
        if (this.mAirplaneModePreference != null) {
            this.mAirplaneModePreference.setChecked(checked);
        }
    }
    
    @Override
    public void onPause() {
        if (this.isAvailable()) {
            this.mAirplaneModeEnabler.pause();
        }
    }
    
    @Override
    public void onResume() {
        if (this.isAvailable()) {
            this.mAirplaneModeEnabler.resume();
        }
    }
    
    @Override
    public boolean setChecked(final boolean airplaneMode) {
        if (this.isChecked() == airplaneMode) {
            return false;
        }
        this.mAirplaneModeEnabler.setAirplaneMode(airplaneMode);
        return true;
    }
    
    public void setFragment(final Fragment mFragment) {
        this.mFragment = mFragment;
    }
}
