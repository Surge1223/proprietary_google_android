package com.android.settings.network;

import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settingslib.RestrictedPreference;
import android.content.IntentFilter;
import android.telephony.ServiceState;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import com.android.settingslib.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.support.v7.preference.Preference;
import android.telephony.PhoneStateListener;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class MobileNetworkPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart, OnStop
{
    private BroadcastReceiver mAirplanModeChangedReceiver;
    private final boolean mIsSecondaryUser;
    PhoneStateListener mPhoneStateListener;
    private Preference mPreference;
    private final TelephonyManager mTelephonyManager;
    private final UserManager mUserManager;
    
    public MobileNetworkPreferenceController(final Context context) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
        this.mIsSecondaryUser = (this.mUserManager.isAdminUser() ^ true);
        this.mAirplanModeChangedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                MobileNetworkPreferenceController.this.updateState(MobileNetworkPreferenceController.this.mPreference);
            }
        };
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "mobile_network_settings";
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mTelephonyManager.getNetworkOperatorName();
    }
    
    @Override
    public boolean isAvailable() {
        return !this.isUserRestricted() && !Utils.isWifiOnly(this.mContext);
    }
    
    public boolean isUserRestricted() {
        return this.mIsSecondaryUser || RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_config_mobile_networks", UserHandle.myUserId());
    }
    
    @Override
    public void onStart() {
        if (this.isAvailable()) {
            if (this.mPhoneStateListener == null) {
                this.mPhoneStateListener = new PhoneStateListener() {
                    public void onServiceStateChanged(final ServiceState serviceState) {
                        MobileNetworkPreferenceController.this.updateState(MobileNetworkPreferenceController.this.mPreference);
                    }
                };
            }
            this.mTelephonyManager.listen(this.mPhoneStateListener, 1);
        }
        if (this.mAirplanModeChangedReceiver != null) {
            this.mContext.registerReceiver(this.mAirplanModeChangedReceiver, new IntentFilter("android.intent.action.AIRPLANE_MODE"));
        }
    }
    
    @Override
    public void onStop() {
        if (this.mPhoneStateListener != null) {
            this.mTelephonyManager.listen(this.mPhoneStateListener, 0);
        }
        if (this.mAirplanModeChangedReceiver != null) {
            this.mContext.unregisterReceiver(this.mAirplanModeChangedReceiver);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (preference instanceof RestrictedPreference && ((RestrictedPreference)preference).isDisabledByAdmin()) {
            return;
        }
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean enabled = false;
        if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) == 0) {
            enabled = true;
        }
        preference.setEnabled(enabled);
    }
}
