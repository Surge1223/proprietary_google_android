package com.android.settings.network;

import android.content.Intent;
import android.content.ContentResolver;
import android.provider.Settings;
import android.os.Handler;
import android.net.Uri;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.database.ContentObserver;
import android.os.Bundle;
import com.android.settings.TetherSettings;
import com.android.settingslib.Utils;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.net.ConnectivityManager;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.bluetooth.BluetoothPan;
import java.util.concurrent.atomic.AtomicReference;
import android.bluetooth.BluetoothAdapter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class TetherPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnCreate, OnDestroy, OnPause, OnResume
{
    private final boolean mAdminDisallowedTetherConfig;
    private SettingObserver mAirplaneModeObserver;
    private final BluetoothAdapter mBluetoothAdapter;
    private final AtomicReference<BluetoothPan> mBluetoothPan;
    final BluetoothProfile$ServiceListener mBtProfileServiceListener;
    private final ConnectivityManager mConnectivityManager;
    private Preference mPreference;
    private TetherBroadcastReceiver mTetherReceiver;
    
    TetherPreferenceController() {
        super(null);
        this.mBtProfileServiceListener = (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
            public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                TetherPreferenceController.this.mBluetoothPan.set(bluetoothProfile);
                TetherPreferenceController.this.updateSummary();
            }
            
            public void onServiceDisconnected(final int n) {
                TetherPreferenceController.this.mBluetoothPan.set(null);
            }
        };
        this.mAdminDisallowedTetherConfig = false;
        this.mBluetoothPan = new AtomicReference<BluetoothPan>();
        this.mConnectivityManager = null;
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }
    
    public TetherPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mBtProfileServiceListener = (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
            public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                TetherPreferenceController.this.mBluetoothPan.set(bluetoothProfile);
                TetherPreferenceController.this.updateSummary();
            }
            
            public void onServiceDisconnected(final int n) {
                TetherPreferenceController.this.mBluetoothPan.set(null);
            }
        };
        this.mBluetoothPan = new AtomicReference<BluetoothPan>();
        this.mAdminDisallowedTetherConfig = isTetherConfigDisallowed(context);
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    public static boolean isTetherConfigDisallowed(final Context context) {
        return RestrictedLockUtils.checkIfRestrictionEnforced(context, "no_config_tethering", UserHandle.myUserId()) != null;
    }
    
    private void updateSummaryToOff() {
        if (this.mPreference == null) {
            return;
        }
        this.mPreference.setSummary(2131889421);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference("tether_settings");
        if (this.mPreference != null && !this.mAdminDisallowedTetherConfig) {
            this.mPreference.setTitle(Utils.getTetheringLabel(this.mConnectivityManager));
            this.mPreference.setEnabled(TetherSettings.isProvisioningNeededButUnavailable(this.mContext) ^ true);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "tether_settings";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean tetheringSupported = this.mConnectivityManager.isTetheringSupported();
        boolean b = false;
        if ((tetheringSupported || this.mAdminDisallowedTetherConfig) && !RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_config_tethering", UserHandle.myUserId())) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        if (this.mBluetoothAdapter != null && this.mBluetoothAdapter.getState() == 12) {
            this.mBluetoothAdapter.getProfileProxy(this.mContext, this.mBtProfileServiceListener, 5);
        }
    }
    
    @Override
    public void onDestroy() {
        final BluetoothProfile bluetoothProfile = (BluetoothProfile)this.mBluetoothPan.getAndSet(null);
        if (bluetoothProfile != null && this.mBluetoothAdapter != null) {
            this.mBluetoothAdapter.closeProfileProxy(5, bluetoothProfile);
        }
    }
    
    @Override
    public void onPause() {
        if (this.mAirplaneModeObserver != null) {
            this.mContext.getContentResolver().unregisterContentObserver((ContentObserver)this.mAirplaneModeObserver);
        }
        if (this.mTetherReceiver != null) {
            this.mContext.unregisterReceiver((BroadcastReceiver)this.mTetherReceiver);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mAirplaneModeObserver == null) {
            this.mAirplaneModeObserver = new SettingObserver();
        }
        if (this.mTetherReceiver == null) {
            this.mTetherReceiver = new TetherBroadcastReceiver();
        }
        this.mContext.registerReceiver((BroadcastReceiver)this.mTetherReceiver, new IntentFilter("android.net.conn.TETHER_STATE_CHANGED"));
        this.mContext.getContentResolver().registerContentObserver(this.mAirplaneModeObserver.uri, false, (ContentObserver)this.mAirplaneModeObserver);
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateSummary();
    }
    
    void updateSummary() {
        if (this.mPreference == null) {
            return;
        }
        final String[] tetheredIfaces = this.mConnectivityManager.getTetheredIfaces();
        final String[] tetherableWifiRegexs = this.mConnectivityManager.getTetherableWifiRegexs();
        final String[] tetherableBluetoothRegexs = this.mConnectivityManager.getTetherableBluetoothRegexs();
        int n = 0;
        final boolean b = false;
        boolean b2 = false;
        final boolean b3 = false;
        if (tetheredIfaces != null) {
            n = (b ? 1 : 0);
            if (tetherableWifiRegexs != null) {
                final int length = tetheredIfaces.length;
                int n2 = 0;
                int n4;
                for (int i = 0; i < length; ++i, n2 = n4) {
                    final String s = tetheredIfaces[i];
                    final int length2 = tetherableWifiRegexs.length;
                    int n3 = 0;
                    while (true) {
                        n4 = n2;
                        if (n3 >= length2) {
                            break;
                        }
                        if (s.matches(tetherableWifiRegexs[n3])) {
                            n4 = 1;
                            break;
                        }
                        ++n3;
                    }
                }
                n = n2;
            }
            b2 = (tetheredIfaces.length > 1 || (tetheredIfaces.length == 1 && n == 0));
        }
        int n5 = b2 ? 1 : 0;
        if (!b2) {
            n5 = (b2 ? 1 : 0);
            if (tetherableBluetoothRegexs != null) {
                n5 = (b2 ? 1 : 0);
                if (tetherableBluetoothRegexs.length > 0) {
                    n5 = (b2 ? 1 : 0);
                    if (this.mBluetoothAdapter != null) {
                        n5 = (b2 ? 1 : 0);
                        if (this.mBluetoothAdapter.getState() == 12) {
                            final BluetoothPan bluetoothPan = this.mBluetoothPan.get();
                            int n6 = b3 ? 1 : 0;
                            if (bluetoothPan != null) {
                                n6 = (b3 ? 1 : 0);
                                if (bluetoothPan.isTetheringOn()) {
                                    n6 = 1;
                                }
                            }
                            n5 = n6;
                        }
                    }
                }
            }
        }
        if (n == 0 && n5 == 0) {
            this.mPreference.setSummary(2131889421);
        }
        else if (n != 0 && n5 != 0) {
            this.mPreference.setSummary(2131889466);
        }
        else if (n != 0) {
            this.mPreference.setSummary(2131889465);
        }
        else {
            this.mPreference.setSummary(2131889464);
        }
    }
    
    class SettingObserver extends ContentObserver
    {
        public final Uri uri;
        
        public SettingObserver() {
            super(new Handler());
            this.uri = Settings.Global.getUriFor("airplane_mode_on");
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.uri.equals((Object)uri)) {
                final ContentResolver contentResolver = TetherPreferenceController.this.mContext.getContentResolver();
                boolean b2 = false;
                if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 0) {
                    b2 = true;
                }
                if (b2) {
                    TetherPreferenceController.this.updateSummaryToOff();
                }
            }
        }
    }
    
    class TetherBroadcastReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            TetherPreferenceController.this.updateSummary();
        }
    }
}
