package com.android.settings.network;

import android.widget.Toast;
import android.os.Message;
import android.os.Looper;
import android.content.ContentUris;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.app.ProgressDialog;
import android.app.Dialog;
import android.os.PersistableBundle;
import android.app.Activity;
import android.telephony.CarrierConfigManager;
import android.os.Bundle;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.content.ContentValues;
import android.os.Handler;
import java.util.Iterator;
import android.database.Cursor;
import android.support.v7.preference.PreferenceGroup;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.telephony.TelephonyManager;
import android.provider.Telephony$Carriers;
import com.android.internal.telephony.dataconnection.ApnSetting;
import android.text.TextUtils;
import com.android.internal.telephony.uicc.IccRecords;
import java.util.ArrayList;
import android.content.ContentResolver;
import com.android.internal.telephony.PhoneConstants$DataState;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.telephony.uicc.UiccController;
import android.telephony.SubscriptionInfo;
import android.os.HandlerThread;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v7.preference.Preference;
import com.android.settings.RestrictedSettingsFragment;

public class ApnSettings extends RestrictedSettingsFragment implements OnPreferenceChangeListener
{
    private static final Uri DEFAULTAPN_URI;
    private static final Uri PREFERAPN_URI;
    private static boolean mRestoreDefaultApnMode;
    private boolean mAllowAddingApns;
    private boolean mHideImsApn;
    private IntentFilter mMobileStateFilter;
    private final BroadcastReceiver mMobileStateReceiver;
    private String mMvnoMatchData;
    private String mMvnoType;
    private RestoreApnProcessHandler mRestoreApnProcessHandler;
    private RestoreApnUiHandler mRestoreApnUiHandler;
    private HandlerThread mRestoreDefaultApnThread;
    private String mSelectedKey;
    private int mSubId;
    private SubscriptionInfo mSubscriptionInfo;
    private UiccController mUiccController;
    private boolean mUnavailable;
    private UserManager mUserManager;
    
    static {
        DEFAULTAPN_URI = Uri.parse("content://telephony/carriers/restore");
        PREFERAPN_URI = Uri.parse("content://telephony/carriers/preferapn");
    }
    
    public ApnSettings() {
        super("no_config_mobile_networks");
        this.mMobileStateReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.intent.action.ANY_DATA_STATE")) {
                    if (ApnSettings$3.$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState[getMobileDataState(intent).ordinal()] == 1) {
                        if (!ApnSettings.mRestoreDefaultApnMode) {
                            ApnSettings.this.fillList();
                        }
                        else {
                            SettingsPreferenceFragment.this.showDialog(1001);
                        }
                    }
                }
                else if (intent.getAction().equals("android.telephony.action.SUBSCRIPTION_CARRIER_IDENTITY_CHANGED") && !ApnSettings.mRestoreDefaultApnMode) {
                    final int intExtra = intent.getIntExtra("android.telephony.extra.SUBSCRIPTION_ID", -1);
                    if (intExtra != ApnSettings.this.mSubId) {
                        ApnSettings.this.mSubId = intExtra;
                        ApnSettings.this.mSubscriptionInfo = ApnSettings.this.getSubscriptionInfo(ApnSettings.this.mSubId);
                    }
                    ApnSettings.this.fillList();
                }
            }
        };
    }
    
    private void addApnToList(final ApnPreference apnPreference, final ArrayList<ApnPreference> list, final ArrayList<ApnPreference> list2, final IccRecords iccRecords, final String mMvnoType, final String mMvnoMatchData) {
        if (iccRecords != null && !TextUtils.isEmpty((CharSequence)mMvnoType) && !TextUtils.isEmpty((CharSequence)mMvnoMatchData)) {
            if (ApnSetting.mvnoMatches(iccRecords, mMvnoType, mMvnoMatchData)) {
                list2.add(apnPreference);
                this.mMvnoType = mMvnoType;
                this.mMvnoMatchData = mMvnoMatchData;
            }
        }
        else {
            list.add(apnPreference);
        }
    }
    
    private void addNewApn() {
        final Intent intent = new Intent("android.intent.action.INSERT", Telephony$Carriers.CONTENT_URI);
        int subscriptionId;
        if (this.mSubscriptionInfo != null) {
            subscriptionId = this.mSubscriptionInfo.getSubscriptionId();
        }
        else {
            subscriptionId = -1;
        }
        intent.putExtra("sub_id", subscriptionId);
        if (!TextUtils.isEmpty((CharSequence)this.mMvnoType) && !TextUtils.isEmpty((CharSequence)this.mMvnoMatchData)) {
            intent.putExtra("mvno_type", this.mMvnoType);
            intent.putExtra("mvno_match_data", this.mMvnoMatchData);
        }
        this.startActivity(intent);
    }
    
    private void fillList() {
        final TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService("phone");
        int subscriptionId;
        if (this.mSubscriptionInfo != null) {
            subscriptionId = this.mSubscriptionInfo.getSubscriptionId();
        }
        else {
            subscriptionId = -1;
        }
        String simOperator;
        if (this.mSubscriptionInfo == null) {
            simOperator = "";
        }
        else {
            simOperator = telephonyManager.getSimOperator(subscriptionId);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("mccmnc = ");
        sb.append(simOperator);
        Log.d("ApnSettings", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("numeric=\"");
        sb2.append(simOperator);
        sb2.append("\" AND NOT (type='ia' AND (apn=\"\" OR apn IS NULL)) AND user_visible!=0");
        final StringBuilder sb3 = new StringBuilder(sb2.toString());
        if (this.mHideImsApn) {
            sb3.append(" AND NOT (type='ims')");
        }
        final Cursor query = this.getContentResolver().query(Telephony$Carriers.CONTENT_URI, new String[] { "_id", "name", "apn", "type", "mvno_type", "mvno_match_data" }, sb3.toString(), (String[])null, "name ASC");
        if (query != null) {
            final IccRecords iccRecords = null;
            final UiccController mUiccController = this.mUiccController;
            int n = 1;
            IccRecords iccRecords2 = iccRecords;
            if (mUiccController != null) {
                iccRecords2 = iccRecords;
                if (this.mSubscriptionInfo != null) {
                    iccRecords2 = this.mUiccController.getIccRecords(SubscriptionManager.getPhoneId(subscriptionId), 1);
                }
            }
            final PreferenceGroup preferenceGroup = (PreferenceGroup)this.findPreference("apn_list");
            preferenceGroup.removeAll();
            final ArrayList<ApnPreference> list = new ArrayList<ApnPreference>();
            final ArrayList<ApnPreference> list2 = new ArrayList<ApnPreference>();
            final ArrayList<ApnPreference> list3 = new ArrayList<ApnPreference>();
            final ArrayList<ApnPreference> list4 = new ArrayList<ApnPreference>();
            this.mSelectedKey = this.getSelectedApnKey();
            query.moveToFirst();
            ArrayList<ApnPreference> list5 = list2;
            while (!query.isAfterLast()) {
                final String string = query.getString(n);
                final String string2 = query.getString(2);
                final String string3 = query.getString(0);
                final String string4 = query.getString(3);
                final String string5 = query.getString(4);
                final String string6 = query.getString(5);
                final ApnPreference apnPreference = new ApnPreference(this.getPrefContext());
                apnPreference.setKey(string3);
                apnPreference.setTitle(string);
                apnPreference.setSummary(string2);
                apnPreference.setPersistent(false);
                apnPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                apnPreference.setSubId(subscriptionId);
                final boolean selectable = string4 == null || !string4.equals("mms");
                apnPreference.setSelectable(selectable);
                if (selectable) {
                    if (this.mSelectedKey != null && this.mSelectedKey.equals(string3)) {
                        apnPreference.setChecked();
                    }
                    this.addApnToList(apnPreference, list, list5, iccRecords2, string5, string6);
                }
                else {
                    this.addApnToList(apnPreference, list3, list4, iccRecords2, string5, string6);
                }
                query.moveToNext();
                n = 1;
            }
            query.close();
            ArrayList<ApnPreference> list6;
            if (!list5.isEmpty()) {
                list6 = list4;
            }
            else {
                list5 = list;
                list6 = list3;
            }
            final Iterator<ApnPreference> iterator = list5.iterator();
            while (iterator.hasNext()) {
                preferenceGroup.addPreference(iterator.next());
            }
            final Iterator<ApnPreference> iterator2 = list6.iterator();
            while (iterator2.hasNext()) {
                preferenceGroup.addPreference(iterator2.next());
            }
        }
    }
    
    private static PhoneConstants$DataState getMobileDataState(final Intent intent) {
        final String stringExtra = intent.getStringExtra("state");
        if (stringExtra != null) {
            return Enum.valueOf(PhoneConstants$DataState.class, stringExtra);
        }
        return PhoneConstants$DataState.DISCONNECTED;
    }
    
    private String getSelectedApnKey() {
        String string = null;
        final Cursor query = this.getContentResolver().query(this.getUriForCurrSubId(ApnSettings.PREFERAPN_URI), new String[] { "_id" }, (String)null, (String[])null, "name ASC");
        if (query.getCount() > 0) {
            query.moveToFirst();
            string = query.getString(0);
        }
        query.close();
        return string;
    }
    
    private SubscriptionInfo getSubscriptionInfo(final int n) {
        return SubscriptionManager.from((Context)this.getActivity()).getActiveSubscriptionInfo(n);
    }
    
    private Uri getUriForCurrSubId(final Uri uri) {
        int subscriptionId;
        if (this.mSubscriptionInfo != null) {
            subscriptionId = this.mSubscriptionInfo.getSubscriptionId();
        }
        else {
            subscriptionId = -1;
        }
        if (SubscriptionManager.isValidSubscriptionId(subscriptionId)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("subId/");
            sb.append(String.valueOf(subscriptionId));
            return Uri.withAppendedPath(uri, sb.toString());
        }
        return uri;
    }
    
    private boolean restoreDefaultApn() {
        this.showDialog(1001);
        ApnSettings.mRestoreDefaultApnMode = true;
        if (this.mRestoreApnUiHandler == null) {
            this.mRestoreApnUiHandler = new RestoreApnUiHandler();
        }
        if (this.mRestoreApnProcessHandler == null || this.mRestoreDefaultApnThread == null) {
            (this.mRestoreDefaultApnThread = new HandlerThread("Restore default APN Handler: Process Thread")).start();
            this.mRestoreApnProcessHandler = new RestoreApnProcessHandler(this.mRestoreDefaultApnThread.getLooper(), this.mRestoreApnUiHandler);
        }
        this.mRestoreApnProcessHandler.sendEmptyMessage(1);
        return true;
    }
    
    private void setSelectedApnKey(final String mSelectedKey) {
        this.mSelectedKey = mSelectedKey;
        final ContentResolver contentResolver = this.getContentResolver();
        final ContentValues contentValues = new ContentValues();
        contentValues.put("apn_id", this.mSelectedKey);
        contentResolver.update(this.getUriForCurrSubId(ApnSettings.PREFERAPN_URI), contentValues, (String)null, (String[])null);
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        if (n == 1001) {
            return 579;
        }
        return 0;
    }
    
    @Override
    public int getMetricsCategory() {
        return 12;
    }
    
    @Override
    public RestrictedLockUtils.EnforcedAdmin getRestrictionEnforcedAdmin() {
        final UserHandle of = UserHandle.of(this.mUserManager.getUserHandle());
        if (this.mUserManager.hasUserRestriction("no_config_mobile_networks", of) && !this.mUserManager.hasBaseUserRestriction("no_config_mobile_networks", of)) {
            return RestrictedLockUtils.EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
        }
        return null;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.getEmptyTextView().setText(2131886338);
        this.mUnavailable = this.isUiRestricted();
        this.setHasOptionsMenu(this.mUnavailable ^ true);
        if (this.mUnavailable) {
            this.addPreferencesFromResource(2132082805);
            return;
        }
        this.addPreferencesFromResource(2132082705);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mSubId = activity.getIntent().getIntExtra("sub_id", -1);
        this.mMobileStateFilter = new IntentFilter("android.intent.action.ANY_DATA_STATE");
        this.setIfOnlyAvailableForAdmins(true);
        this.mSubscriptionInfo = this.getSubscriptionInfo(this.mSubId);
        this.mUiccController = UiccController.getInstance();
        final PersistableBundle config = ((CarrierConfigManager)this.getSystemService("carrier_config")).getConfig();
        this.mHideImsApn = config.getBoolean("hide_ims_apn_bool");
        this.mAllowAddingApns = config.getBoolean("allow_adding_apns_bool");
        if (this.mAllowAddingApns && ApnEditor.hasAllApns(config.getStringArray("read_only_apn_types_string_array"))) {
            Log.d("ApnSettings", "not allowing adding APN because all APN types are read only");
            this.mAllowAddingApns = false;
        }
        this.mUserManager = UserManager.get((Context)activity);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (n == 1001) {
            final ProgressDialog progressDialog = new ProgressDialog(this.getActivity()) {
                public boolean onTouchEvent(final MotionEvent motionEvent) {
                    return true;
                }
            };
            progressDialog.setMessage((CharSequence)this.getResources().getString(2131888826));
            progressDialog.setCancelable(false);
            return (Dialog)progressDialog;
        }
        return null;
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        if (!this.mUnavailable) {
            if (this.mAllowAddingApns) {
                menu.add(0, 1, 0, (CharSequence)this.getResources().getString(2131888296)).setIcon(2131231066).setShowAsAction(1);
            }
            menu.add(0, 2, 0, (CharSequence)this.getResources().getString(2131888302)).setIcon(17301589);
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mRestoreDefaultApnThread != null) {
            this.mRestoreDefaultApnThread.quit();
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2: {
                this.restoreDefaultApn();
                return true;
            }
            case 1: {
                this.addNewApn();
                return true;
            }
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (this.mUnavailable) {
            return;
        }
        this.getActivity().unregisterReceiver(this.mMobileStateReceiver);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onPreferenceChange(): Preference - ");
        sb.append(preference);
        sb.append(", newValue - ");
        sb.append(o);
        sb.append(", newValue type - ");
        sb.append(o.getClass());
        Log.d("ApnSettings", sb.toString());
        if (o instanceof String) {
            this.setSelectedApnKey((String)o);
        }
        return true;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        this.startActivity(new Intent("android.intent.action.EDIT", ContentUris.withAppendedId(Telephony$Carriers.CONTENT_URI, (long)Integer.parseInt(preference.getKey()))));
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mUnavailable) {
            return;
        }
        this.getActivity().registerReceiver(this.mMobileStateReceiver, this.mMobileStateFilter);
        if (!ApnSettings.mRestoreDefaultApnMode) {
            this.fillList();
        }
    }
    
    private class RestoreApnProcessHandler extends Handler
    {
        private Handler mRestoreApnUiHandler;
        
        public RestoreApnProcessHandler(final Looper looper, final Handler mRestoreApnUiHandler) {
            super(looper);
            this.mRestoreApnUiHandler = mRestoreApnUiHandler;
        }
        
        public void handleMessage(final Message message) {
            if (message.what == 1) {
                SettingsPreferenceFragment.this.getContentResolver().delete(ApnSettings.this.getUriForCurrSubId(ApnSettings.DEFAULTAPN_URI), (String)null, (String[])null);
                this.mRestoreApnUiHandler.sendEmptyMessage(2);
            }
        }
    }
    
    private class RestoreApnUiHandler extends Handler
    {
        public void handleMessage(final Message message) {
            if (message.what == 2) {
                final Activity activity = ApnSettings.this.getActivity();
                if (activity == null) {
                    ApnSettings.mRestoreDefaultApnMode = false;
                    return;
                }
                ApnSettings.this.fillList();
                ApnSettings.this.getPreferenceScreen().setEnabled(true);
                ApnSettings.mRestoreDefaultApnMode = false;
                SettingsPreferenceFragment.this.removeDialog(1001);
                Toast.makeText((Context)activity, (CharSequence)ApnSettings.this.getResources().getString(2131888827), 1).show();
            }
        }
    }
}
