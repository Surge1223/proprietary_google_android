package com.android.settings.network;

import android.net.NetworkScorerAppData;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.net.NetworkScoreManager;
import com.android.settings.core.BasePreferenceController;

public class NetworkScorerPickerPreferenceController extends BasePreferenceController
{
    private final NetworkScoreManager mNetworkScoreManager;
    
    public NetworkScorerPickerPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mNetworkScoreManager = (NetworkScoreManager)this.mContext.getSystemService("network_score");
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final boolean enabled = this.mNetworkScoreManager.getAllValidScorers().isEmpty() ^ true;
        preference.setEnabled(enabled);
        if (!enabled) {
            preference.setSummary(null);
            return;
        }
        final NetworkScorerAppData activeScorer = this.mNetworkScoreManager.getActiveScorer();
        if (activeScorer == null) {
            preference.setSummary(this.mContext.getString(2131888349));
        }
        else {
            preference.setSummary(activeScorer.getRecommendationServiceLabel());
        }
    }
}
