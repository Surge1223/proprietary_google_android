package com.android.settings.network;

import java.util.List;
import android.net.NetworkScorerAppData;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import android.net.NetworkScoreManager;
import com.android.settings.widget.RadioButtonPreference;
import com.android.settings.core.InstrumentedPreferenceFragment;

public class NetworkScorerPicker extends InstrumentedPreferenceFragment implements OnClickListener
{
    private NetworkScoreManager mNetworkScoreManager;
    
    private String getActiveScorerPackage() {
        return this.mNetworkScoreManager.getActiveScorerPackage();
    }
    
    private boolean setActiveScorer(final String activeScorer) {
        return !TextUtils.equals((CharSequence)activeScorer, (CharSequence)this.getActiveScorerPackage()) && this.mNetworkScoreManager.setActiveScorer(activeScorer);
    }
    
    private void updateCheckedState(final String s) {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        for (int preferenceCount = preferenceScreen.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = preferenceScreen.getPreference(i);
            if (preference instanceof RadioButtonPreference) {
                ((RadioButtonPreference)preference).setChecked(TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)s));
            }
        }
    }
    
    NetworkScoreManager createNetworkScorerManager(final Context context) {
        return (NetworkScoreManager)context.getSystemService("network_score");
    }
    
    @Override
    public int getMetricsCategory() {
        return 861;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082792;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mNetworkScoreManager = this.createNetworkScorerManager(context);
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        super.onCreatePreferences(bundle, s);
        this.updateCandidates();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.setHasOptionsMenu(true);
        return onCreateView;
    }
    
    @Override
    public void onRadioButtonClicked(final RadioButtonPreference radioButtonPreference) {
        final String key = radioButtonPreference.getKey();
        if (this.setActiveScorer(key)) {
            this.updateCheckedState(key);
        }
    }
    
    public void updateCandidates() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        final List allValidScorers = this.mNetworkScoreManager.getAllValidScorers();
        final String activeScorerPackage = this.getActiveScorerPackage();
        final RadioButtonPreference radioButtonPreference = new RadioButtonPreference(this.getPrefContext());
        radioButtonPreference.setTitle(2131888349);
        if (allValidScorers.isEmpty()) {
            radioButtonPreference.setChecked(true);
        }
        else {
            radioButtonPreference.setKey(null);
            radioButtonPreference.setChecked(TextUtils.isEmpty((CharSequence)activeScorerPackage));
            radioButtonPreference.setOnClickListener((RadioButtonPreference.OnClickListener)this);
        }
        preferenceScreen.addPreference(radioButtonPreference);
        for (int size = allValidScorers.size(), i = 0; i < size; ++i) {
            final RadioButtonPreference radioButtonPreference2 = new RadioButtonPreference(this.getPrefContext());
            final NetworkScorerAppData networkScorerAppData = allValidScorers.get(i);
            final String recommendationServicePackageName = networkScorerAppData.getRecommendationServicePackageName();
            radioButtonPreference2.setTitle(networkScorerAppData.getRecommendationServiceLabel());
            radioButtonPreference2.setKey(recommendationServicePackageName);
            radioButtonPreference2.setChecked(TextUtils.equals((CharSequence)activeScorerPackage, (CharSequence)recommendationServicePackageName));
            radioButtonPreference2.setOnClickListener((RadioButtonPreference.OnClickListener)this);
            preferenceScreen.addPreference(radioButtonPreference2);
        }
    }
}
