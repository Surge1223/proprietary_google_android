package com.android.settings.network;

import android.os.Bundle;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import com.android.settingslib.Utils;
import android.support.v7.preference.Preference;
import java.util.List;
import android.net.NetworkInfo;
import android.content.res.Resources;
import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.net.ConnectivityManager;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class MobilePlanPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnCreate, OnSaveInstanceState
{
    private ConnectivityManager mCm;
    private final MobilePlanPreferenceHost mHost;
    private final boolean mIsSecondaryUser;
    private String mMobilePlanDialogMessage;
    private TelephonyManager mTm;
    private final UserManager mUserManager;
    
    public MobilePlanPreferenceController(final Context context, final MobilePlanPreferenceHost mHost) {
        super(context);
        this.mHost = mHost;
        this.mCm = (ConnectivityManager)context.getSystemService("connectivity");
        this.mTm = (TelephonyManager)context.getSystemService("phone");
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mIsSecondaryUser = (this.mUserManager.isAdminUser() ^ true);
    }
    
    private void onManageMobilePlanClick() {
        final Resources resources = this.mContext.getResources();
        final NetworkInfo activeNetworkInfo = this.mCm.getActiveNetworkInfo();
        if (this.mTm.hasIccCard() && activeNetworkInfo != null) {
            final Intent intent = new Intent("android.intent.action.CARRIER_SETUP");
            final List carrierPackageNamesForIntent = this.mTm.getCarrierPackageNamesForIntent(intent);
            if (carrierPackageNamesForIntent != null && !carrierPackageNamesForIntent.isEmpty()) {
                if (carrierPackageNamesForIntent.size() != 1) {
                    Log.w("MobilePlanPrefContr", "Multiple matching carrier apps found, launching the first.");
                }
                intent.setPackage((String)carrierPackageNamesForIntent.get(0));
                this.mContext.startActivity(intent);
                return;
            }
            final String mobileProvisioningUrl = this.mCm.getMobileProvisioningUrl();
            if (!TextUtils.isEmpty((CharSequence)mobileProvisioningUrl)) {
                final Intent mainSelectorActivity = Intent.makeMainSelectorActivity("android.intent.action.MAIN", "android.intent.category.APP_BROWSER");
                mainSelectorActivity.setData(Uri.parse(mobileProvisioningUrl));
                mainSelectorActivity.setFlags(272629760);
                try {
                    this.mContext.startActivity(mainSelectorActivity);
                }
                catch (ActivityNotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onManageMobilePlanClick: startActivity failed");
                    sb.append(ex);
                    Log.w("MobilePlanPrefContr", sb.toString());
                }
            }
            else {
                final String simOperatorName = this.mTm.getSimOperatorName();
                if (TextUtils.isEmpty((CharSequence)simOperatorName)) {
                    final String networkOperatorName = this.mTm.getNetworkOperatorName();
                    if (TextUtils.isEmpty((CharSequence)networkOperatorName)) {
                        this.mMobilePlanDialogMessage = resources.getString(2131888319);
                    }
                    else {
                        this.mMobilePlanDialogMessage = resources.getString(2131888318, new Object[] { networkOperatorName });
                    }
                }
                else {
                    this.mMobilePlanDialogMessage = resources.getString(2131888318, new Object[] { simOperatorName });
                }
            }
        }
        else if (!this.mTm.hasIccCard()) {
            this.mMobilePlanDialogMessage = resources.getString(2131888317);
        }
        else {
            this.mMobilePlanDialogMessage = resources.getString(2131888314);
        }
        if (!TextUtils.isEmpty((CharSequence)this.mMobilePlanDialogMessage)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("onManageMobilePlanClick: message=");
            sb2.append(this.mMobilePlanDialogMessage);
            Log.d("MobilePlanPrefContr", sb2.toString());
            if (this.mHost != null) {
                this.mHost.showMobilePlanMessageDialog();
            }
            else {
                Log.d("MobilePlanPrefContr", "Missing host fragment, cannot show message dialog.");
            }
        }
    }
    
    public String getMobilePlanDialogMessage() {
        return this.mMobilePlanDialogMessage;
    }
    
    @Override
    public String getPreferenceKey() {
        return "manage_mobile_plan";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.mHost != null && "manage_mobile_plan".equals(preference.getKey())) {
            this.mMobilePlanDialogMessage = null;
            this.onManageMobilePlanClick();
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        final boolean boolean1 = this.mContext.getResources().getBoolean(2131034152);
        final boolean mIsSecondaryUser = this.mIsSecondaryUser;
        final boolean b = false;
        final boolean b2 = !mIsSecondaryUser && !Utils.isWifiOnly(this.mContext) && !RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_config_mobile_networks", UserHandle.myUserId());
        boolean b3 = b;
        if (b2) {
            b3 = b;
            if (boolean1) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        if (bundle != null) {
            this.mMobilePlanDialogMessage = bundle.getString("mManageMobilePlanMessage");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("onCreate: mMobilePlanDialogMessage=");
        sb.append(this.mMobilePlanDialogMessage);
        Log.d("MobilePlanPrefContr", sb.toString());
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        if (!TextUtils.isEmpty((CharSequence)this.mMobilePlanDialogMessage)) {
            bundle.putString("mManageMobilePlanMessage", this.mMobilePlanDialogMessage);
        }
    }
    
    public void setMobilePlanDialogMessage(final String mMobilePlanDialogMessage) {
        this.mMobilePlanDialogMessage = mMobilePlanDialogMessage;
    }
    
    public interface MobilePlanPreferenceHost
    {
        void showMobilePlanMessageDialog();
    }
}
