package com.android.settings.network;

import android.content.Intent;
import android.content.ContentUris;
import android.provider.Telephony$Carriers;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RadioButton;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;

public class ApnPreference extends Preference implements View.OnClickListener, CompoundButton$OnCheckedChangeListener
{
    private static CompoundButton mCurrentChecked;
    private static String mSelectedKey;
    private boolean mProtectFromCheckedChange;
    private boolean mSelectable;
    private int mSubId;
    
    static {
        ApnPreference.mSelectedKey = null;
        ApnPreference.mCurrentChecked = null;
    }
    
    public ApnPreference(final Context context) {
        this(context, null);
    }
    
    public ApnPreference(final Context context, final AttributeSet set) {
        this(context, set, 2130968627);
    }
    
    public ApnPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mSubId = -1;
        this.mProtectFromCheckedChange = false;
        this.mSelectable = true;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131361870);
        if (viewById != null && viewById instanceof RadioButton) {
            final RadioButton mCurrentChecked = (RadioButton)viewById;
            if (this.mSelectable) {
                mCurrentChecked.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
                final boolean equals = this.getKey().equals(ApnPreference.mSelectedKey);
                if (equals) {
                    ApnPreference.mCurrentChecked = (CompoundButton)mCurrentChecked;
                    ApnPreference.mSelectedKey = this.getKey();
                }
                this.mProtectFromCheckedChange = true;
                mCurrentChecked.setChecked(equals);
                this.mProtectFromCheckedChange = false;
                mCurrentChecked.setVisibility(0);
            }
            else {
                mCurrentChecked.setVisibility(8);
            }
        }
        final View viewById2 = preferenceViewHolder.findViewById(2131362730);
        if (viewById2 != null && viewById2 instanceof RelativeLayout) {
            viewById2.setOnClickListener((View.OnClickListener)this);
        }
    }
    
    public void onCheckedChanged(final CompoundButton mCurrentChecked, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("ID: ");
        sb.append(this.getKey());
        sb.append(" :");
        sb.append(b);
        Log.i("ApnPreference", sb.toString());
        if (this.mProtectFromCheckedChange) {
            return;
        }
        if (b) {
            if (ApnPreference.mCurrentChecked != null) {
                ApnPreference.mCurrentChecked.setChecked(false);
            }
            ApnPreference.mCurrentChecked = mCurrentChecked;
            this.callChangeListener(ApnPreference.mSelectedKey = this.getKey());
        }
        else {
            ApnPreference.mCurrentChecked = null;
            ApnPreference.mSelectedKey = null;
        }
    }
    
    public void onClick(final View view) {
        if (view != null && 2131362730 == view.getId()) {
            final Context context = this.getContext();
            if (context != null) {
                final Intent intent = new Intent("android.intent.action.EDIT", ContentUris.withAppendedId(Telephony$Carriers.CONTENT_URI, (long)Integer.parseInt(this.getKey())));
                intent.putExtra("sub_id", this.mSubId);
                context.startActivity(intent);
            }
        }
    }
    
    public void setChecked() {
        ApnPreference.mSelectedKey = this.getKey();
    }
    
    @Override
    public void setSelectable(final boolean mSelectable) {
        this.mSelectable = mSelectable;
    }
    
    public void setSubId(final int mSubId) {
        this.mSubId = mSubId;
    }
}
