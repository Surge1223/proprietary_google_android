package com.android.settings.network;

import android.text.BidiFormatter;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.util.Log;
import android.app.Dialog;
import android.content.DialogInterface;
import java.util.ArrayList;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.wifi.WifiMasterSwitchPreferenceController;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.search.BaseSearchIndexProvider;
import android.content.Context;
import android.app.Activity;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class NetworkDashboardFragment extends DashboardFragment implements MobilePlanPreferenceHost
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new NetworkDashboardFragment.SummaryProvider((Context)activity, summaryLoader);
            }
        };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("toggle_wifi");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082791;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle, final MetricsFeatureProvider metricsFeatureProvider, final Fragment fragment, final MobilePlanPreferenceHost mobilePlanPreferenceHost) {
        final MobilePlanPreferenceController mobilePlanPreferenceController = new MobilePlanPreferenceController(context, mobilePlanPreferenceHost);
        final WifiMasterSwitchPreferenceController wifiMasterSwitchPreferenceController = new WifiMasterSwitchPreferenceController(context, metricsFeatureProvider);
        final MobileNetworkPreferenceController mobileNetworkPreferenceController = new MobileNetworkPreferenceController(context);
        final VpnPreferenceController vpnPreferenceController = new VpnPreferenceController(context);
        final PrivateDnsPreferenceController privateDnsPreferenceController = new PrivateDnsPreferenceController(context);
        if (lifecycle != null) {
            lifecycle.addObserver(mobilePlanPreferenceController);
            lifecycle.addObserver(wifiMasterSwitchPreferenceController);
            lifecycle.addObserver(mobileNetworkPreferenceController);
            lifecycle.addObserver(vpnPreferenceController);
            lifecycle.addObserver(privateDnsPreferenceController);
        }
        final ArrayList<MobileNetworkPreferenceController> list = new ArrayList<MobileNetworkPreferenceController>();
        list.add(mobileNetworkPreferenceController);
        list.add((MobileNetworkPreferenceController)new TetherPreferenceController(context, lifecycle));
        list.add((MobileNetworkPreferenceController)vpnPreferenceController);
        list.add((MobileNetworkPreferenceController)new ProxyPreferenceController(context));
        list.add((MobileNetworkPreferenceController)mobilePlanPreferenceController);
        list.add((MobileNetworkPreferenceController)wifiMasterSwitchPreferenceController);
        list.add((MobileNetworkPreferenceController)privateDnsPreferenceController);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle(), this.mMetricsFeatureProvider, this, this);
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        if (1 == n) {
            return 609;
        }
        return 0;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887820;
    }
    
    @Override
    protected String getLogTag() {
        return "NetworkDashboardFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 746;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082791;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(AirplaneModePreferenceController.class).setFragment(this);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onCreateDialog: dialogId=");
        sb.append(n);
        Log.d("NetworkDashboardFrag", sb.toString());
        if (n != 1) {
            return super.onCreateDialog(n);
        }
        final MobilePlanPreferenceController mobilePlanPreferenceController = this.use(MobilePlanPreferenceController.class);
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage((CharSequence)mobilePlanPreferenceController.getMobilePlanDialogMessage()).setCancelable(false).setPositiveButton(17039370, (DialogInterface$OnClickListener)new _$$Lambda$NetworkDashboardFragment$ezC2Ol_SOf4CDiS8HjkkdWzGu_s(mobilePlanPreferenceController)).create();
    }
    
    @Override
    public void showMobilePlanMessageDialog() {
        this.showDialog(1);
    }
    
    static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final MobileNetworkPreferenceController mMobileNetworkPreferenceController;
        private final SummaryLoader mSummaryLoader;
        private final TetherPreferenceController mTetherPreferenceController;
        
        public SummaryProvider(final Context context, final SummaryLoader summaryLoader) {
            this(context, summaryLoader, new MobileNetworkPreferenceController(context), new TetherPreferenceController(context, null));
        }
        
        SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader, final MobileNetworkPreferenceController mMobileNetworkPreferenceController, final TetherPreferenceController mTetherPreferenceController) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
            this.mMobileNetworkPreferenceController = mMobileNetworkPreferenceController;
            this.mTetherPreferenceController = mTetherPreferenceController;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                String s = BidiFormatter.getInstance().unicodeWrap(this.mContext.getString(2131890121));
                if (this.mMobileNetworkPreferenceController.isAvailable()) {
                    s = this.mContext.getString(2131887916, new Object[] { s, this.mContext.getString(2131888341) });
                }
                String s2 = this.mContext.getString(2131887916, new Object[] { s, this.mContext.getString(2131888339) });
                if (this.mTetherPreferenceController.isAvailable()) {
                    s2 = this.mContext.getString(2131887916, new Object[] { s2, this.mContext.getString(2131888340) });
                }
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, s2);
            }
        }
    }
}
