package com.android.settings.network;

import java.util.Collection;
import android.content.ContentResolver;
import android.content.res.Resources;
import com.android.internal.util.ArrayUtils;
import android.support.v7.preference.PreferenceScreen;
import android.os.Looper;
import android.net.Network;
import android.content.Context;
import android.provider.Settings;
import android.database.ContentObserver;
import android.support.v7.preference.Preference;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.LinkProperties;
import android.os.Handler;
import android.net.ConnectivityManager;
import android.net.Uri;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.core.BasePreferenceController;

public class PrivateDnsPreferenceController extends BasePreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart, OnStop
{
    private static final String KEY_PRIVATE_DNS_SETTINGS = "private_dns_settings";
    private static final Uri[] SETTINGS_URIS;
    private final ConnectivityManager mConnectivityManager;
    private final Handler mHandler;
    private LinkProperties mLatestLinkProperties;
    private final ConnectivityManager$NetworkCallback mNetworkCallback;
    private Preference mPreference;
    private final ContentObserver mSettingsObserver;
    
    static {
        SETTINGS_URIS = new Uri[] { Settings.Global.getUriFor("private_dns_mode"), Settings.Global.getUriFor("private_dns_default_mode"), Settings.Global.getUriFor("private_dns_specifier") };
    }
    
    public PrivateDnsPreferenceController(final Context context) {
        super(context, "private_dns_settings");
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            public void onLinkPropertiesChanged(final Network network, final LinkProperties linkProperties) {
                PrivateDnsPreferenceController.this.mLatestLinkProperties = linkProperties;
                if (PrivateDnsPreferenceController.this.mPreference != null) {
                    PrivateDnsPreferenceController.this.updateState(PrivateDnsPreferenceController.this.mPreference);
                }
            }
            
            public void onLost(final Network network) {
                PrivateDnsPreferenceController.this.mLatestLinkProperties = null;
                if (PrivateDnsPreferenceController.this.mPreference != null) {
                    PrivateDnsPreferenceController.this.updateState(PrivateDnsPreferenceController.this.mPreference);
                }
            }
        };
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mSettingsObserver = new PrivateDnsSettingsObserver(this.mHandler);
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public String getPreferenceKey() {
        return "private_dns_settings";
    }
    
    @Override
    public CharSequence getSummary() {
        final Resources resources = this.mContext.getResources();
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        final String modeFromSettings = PrivateDnsModeDialogPreference.getModeFromSettings(contentResolver);
        final LinkProperties mLatestLinkProperties = this.mLatestLinkProperties;
        Collection validatedPrivateDnsServers;
        if (mLatestLinkProperties == null) {
            validatedPrivateDnsServers = null;
        }
        else {
            validatedPrivateDnsServers = mLatestLinkProperties.getValidatedPrivateDnsServers();
        }
        final boolean empty = ArrayUtils.isEmpty(validatedPrivateDnsServers);
        int n = 1;
        final boolean b = empty ^ true;
        final int hashCode = modeFromSettings.hashCode();
        Label_0136: {
            if (hashCode != -539229175) {
                if (hashCode != -299803597) {
                    if (hashCode == 109935) {
                        if (modeFromSettings.equals("off")) {
                            n = 0;
                            break Label_0136;
                        }
                    }
                }
                else if (modeFromSettings.equals("hostname")) {
                    n = 2;
                    break Label_0136;
                }
            }
            else if (modeFromSettings.equals("opportunistic")) {
                break Label_0136;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return "";
            }
            case 2: {
                String s;
                if (b) {
                    s = PrivateDnsModeDialogPreference.getHostnameFromSettings(contentResolver);
                }
                else {
                    s = resources.getString(2131888673);
                }
                return s;
            }
            case 1: {
                String s2;
                if (b) {
                    s2 = resources.getString(2131889422);
                }
                else {
                    s2 = resources.getString(2131888671);
                }
                return s2;
            }
            case 0: {
                return resources.getString(2131888670);
            }
        }
    }
    
    @Override
    public void onStart() {
        final Uri[] settings_URIS = PrivateDnsPreferenceController.SETTINGS_URIS;
        for (int length = settings_URIS.length, i = 0; i < length; ++i) {
            this.mContext.getContentResolver().registerContentObserver(settings_URIS[i], false, this.mSettingsObserver);
        }
        final Network activeNetwork = this.mConnectivityManager.getActiveNetwork();
        if (activeNetwork != null) {
            this.mLatestLinkProperties = this.mConnectivityManager.getLinkProperties(activeNetwork);
        }
        this.mConnectivityManager.registerDefaultNetworkCallback(this.mNetworkCallback, this.mHandler);
    }
    
    @Override
    public void onStop() {
        this.mContext.getContentResolver().unregisterContentObserver(this.mSettingsObserver);
        this.mConnectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
    }
    
    private class PrivateDnsSettingsObserver extends ContentObserver
    {
        public PrivateDnsSettingsObserver(final Handler handler) {
            super(handler);
        }
        
        public void onChange(final boolean b) {
            if (PrivateDnsPreferenceController.this.mPreference != null) {
                PrivateDnsPreferenceController.this.updateState(PrivateDnsPreferenceController.this.mPreference);
            }
        }
    }
}
