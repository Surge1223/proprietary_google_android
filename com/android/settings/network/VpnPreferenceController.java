package com.android.settings.network;

import com.android.internal.net.LegacyVpnInfo;
import java.util.Iterator;
import android.os.RemoteException;
import com.android.settingslib.utils.ThreadUtils;
import android.content.pm.UserInfo;
import android.util.SparseArray;
import com.android.settingslib.RestrictedLockUtils;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.UserHandle;
import com.android.internal.net.VpnConfig;
import android.support.v7.preference.PreferenceScreen;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import android.provider.Settings;
import android.net.Network;
import android.content.Context;
import android.net.NetworkRequest$Builder;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.IConnectivityManager;
import android.net.ConnectivityManager;
import android.net.NetworkRequest;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class VpnPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private static final NetworkRequest REQUEST;
    private final ConnectivityManager mConnectivityManager;
    private final IConnectivityManager mConnectivityManagerService;
    private final ConnectivityManager$NetworkCallback mNetworkCallback;
    private Preference mPreference;
    private final String mToggleable;
    private final UserManager mUserManager;
    
    static {
        REQUEST = new NetworkRequest$Builder().removeCapability(15).removeCapability(13).removeCapability(14).build();
    }
    
    public VpnPreferenceController(final Context context) {
        super(context);
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            public void onAvailable(final Network network) {
                VpnPreferenceController.this.updateSummary();
            }
            
            public void onLost(final Network network) {
                VpnPreferenceController.this.updateSummary();
            }
        };
        this.mToggleable = Settings.Global.getString(context.getContentResolver(), "airplane_mode_toggleable_radios");
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
        this.mConnectivityManagerService = IConnectivityManager$Stub.asInterface(ServiceManager.getService("connectivity"));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference("vpn_settings");
        if ((this.mToggleable == null || !this.mToggleable.contains("wifi")) && this.mPreference != null) {
            this.mPreference.setDependency("airplane_mode");
        }
    }
    
    String getNameForVpnConfig(VpnConfig user, final UserHandle userHandle) {
        if (user.legacy) {
            return this.mContext.getString(2131889972);
        }
        user = (VpnConfig)user.user;
        try {
            return VpnConfig.getVpnLabel(this.mContext.createPackageContextAsUser(this.mContext.getPackageName(), 0, userHandle), (String)user).toString();
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Package ");
            sb.append((String)user);
            sb.append(" is not present");
            Log.e("VpnPreferenceController", sb.toString(), (Throwable)ex);
            return null;
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "vpn_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_config_vpn", UserHandle.myUserId()) ^ true;
    }
    
    @Override
    public void onPause() {
        if (this.isAvailable()) {
            this.mConnectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
        }
    }
    
    @Override
    public void onResume() {
        if (this.isAvailable()) {
            this.mConnectivityManager.registerNetworkCallback(VpnPreferenceController.REQUEST, this.mNetworkCallback);
        }
    }
    
    void updateSummary() {
        if (this.mPreference == null) {
            return;
        }
        final SparseArray sparseArray = new SparseArray();
        try {
            for (final UserInfo userInfo : this.mUserManager.getUsers()) {
                final VpnConfig vpnConfig = this.mConnectivityManagerService.getVpnConfig(userInfo.id);
                if (vpnConfig == null) {
                    continue;
                }
                if (vpnConfig.legacy) {
                    final LegacyVpnInfo legacyVpnInfo = this.mConnectivityManagerService.getLegacyVpnInfo(userInfo.id);
                    if (legacyVpnInfo == null) {
                        continue;
                    }
                    if (legacyVpnInfo.state != 3) {
                        continue;
                    }
                }
                sparseArray.put(userInfo.id, (Object)vpnConfig);
            }
            final UserInfo userInfo2 = this.mUserManager.getUserInfo(UserHandle.myUserId());
            int n;
            if (userInfo2.isRestricted()) {
                n = userInfo2.restrictedProfileParentId;
            }
            else {
                n = userInfo2.id;
            }
            final VpnConfig vpnConfig2 = (VpnConfig)sparseArray.get(n);
            String s;
            if (vpnConfig2 == null) {
                s = this.mContext.getString(2131889816);
            }
            else {
                s = this.getNameForVpnConfig(vpnConfig2, UserHandle.of(n));
            }
            ThreadUtils.postOnMainThread(new _$$Lambda$VpnPreferenceController$iDQ0RgxaDkCLoaHHZ6_UO2xSI_c(this, s));
        }
        catch (RemoteException ex) {
            Log.e("VpnPreferenceController", "Unable to list active VPNs", (Throwable)ex);
        }
    }
}
