package com.android.settings.network;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.app.Fragment;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import java.util.Iterator;
import android.telephony.ServiceState;
import android.view.MenuItem;
import android.view.KeyEvent;
import android.view.View;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.PersistableBundle;
import android.content.Intent;
import android.provider.Telephony$Carriers;
import android.telephony.CarrierConfigManager;
import android.os.Bundle;
import android.database.Cursor;
import java.util.HashSet;
import com.android.settingslib.utils.ThreadUtils;
import android.content.ContentValues;
import com.android.internal.telephony.PhoneConstants;
import java.util.Set;
import java.util.List;
import android.util.Log;
import java.util.Arrays;
import android.text.TextUtils;
import com.android.internal.util.ArrayUtils;
import android.telephony.TelephonyManager;
import android.net.Uri;
import android.support.v14.preference.SwitchPreference;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.EditTextPreference;
import android.view.View$OnKeyListener;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class ApnEditor extends SettingsPreferenceFragment implements OnPreferenceChangeListener, View$OnKeyListener
{
    static final int APN_INDEX = 2;
    static final int CARRIER_ENABLED_INDEX = 17;
    static final int MCC_INDEX = 9;
    static final int MNC_INDEX = 10;
    static final int NAME_INDEX = 1;
    private static final String TAG;
    static String sNotSet;
    private static final String[] sProjection;
    EditTextPreference mApn;
    ApnData mApnData;
    EditTextPreference mApnType;
    ListPreference mAuthType;
    private int mBearerInitialVal;
    MultiSelectListPreference mBearerMulti;
    SwitchPreference mCarrierEnabled;
    private Uri mCarrierUri;
    private String mCurMcc;
    private String mCurMnc;
    EditTextPreference mMcc;
    EditTextPreference mMmsPort;
    EditTextPreference mMmsProxy;
    EditTextPreference mMmsc;
    EditTextPreference mMnc;
    EditTextPreference mMvnoMatchData;
    private String mMvnoMatchDataStr;
    ListPreference mMvnoType;
    private String mMvnoTypeStr;
    EditTextPreference mName;
    private boolean mNewApn;
    EditTextPreference mPassword;
    EditTextPreference mPort;
    ListPreference mProtocol;
    EditTextPreference mProxy;
    private boolean mReadOnlyApn;
    private String[] mReadOnlyApnFields;
    private String[] mReadOnlyApnTypes;
    ListPreference mRoamingProtocol;
    EditTextPreference mServer;
    private int mSubId;
    private TelephonyManager mTelephonyManager;
    EditTextPreference mUser;
    
    static {
        TAG = ApnEditor.class.getSimpleName();
        sProjection = new String[] { "_id", "name", "apn", "proxy", "port", "user", "server", "password", "mmsc", "mcc", "mnc", "numeric", "mmsproxy", "mmsport", "authtype", "type", "protocol", "carrier_enabled", "bearer", "bearer_bitmask", "roaming_protocol", "mvno_type", "mvno_match_data", "edited", "user_editable" };
    }
    
    public ApnEditor() {
        this.mBearerInitialVal = 0;
    }
    
    private boolean apnTypesMatch(final String[] array, String tag) {
        if (ArrayUtils.isEmpty((Object[])array)) {
            return false;
        }
        if (!hasAllApns(array) && !TextUtils.isEmpty((CharSequence)tag)) {
            final List<String> list = Arrays.asList(array);
            for (final String s : tag.split(",")) {
                if (list.contains(s.trim())) {
                    tag = ApnEditor.TAG;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("apnTypesMatch: true because match found for ");
                    sb.append(s.trim());
                    Log.d(tag, sb.toString());
                    return true;
                }
            }
            Log.d(ApnEditor.TAG, "apnTypesMatch: false");
            return false;
        }
        return true;
    }
    
    private String bearerMultiDescription(Set<String> iterator) {
        final String[] stringArray = this.getResources().getStringArray(2130903061);
        final StringBuilder sb = new StringBuilder();
        int n = 1;
        iterator = ((Set<String>)iterator).iterator();
        while (iterator.hasNext()) {
            final int indexOfValue = this.mBearerMulti.findIndexOfValue(iterator.next());
            if (n != 0) {
                try {
                    sb.append(stringArray[indexOfValue]);
                    n = 0;
                    continue;
                }
                catch (ArrayIndexOutOfBoundsException ex) {
                    continue;
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(", ");
            sb2.append(stringArray[indexOfValue]);
            sb.append(sb2.toString());
        }
        final String string = sb.toString();
        if (!TextUtils.isEmpty((CharSequence)string)) {
            return string;
        }
        return null;
    }
    
    private String checkNotSet(String s) {
        if (ApnEditor.sNotSet.equals(s)) {
            s = null;
        }
        return s;
    }
    
    private String checkNull(String sNotSet) {
        if (TextUtils.isEmpty((CharSequence)sNotSet)) {
            sNotSet = ApnEditor.sNotSet;
        }
        return sNotSet;
    }
    
    private void deleteApn() {
        if (this.mApnData.getUri() != null) {
            this.getContentResolver().delete(this.mApnData.getUri(), (String)null, (String[])null);
            this.mApnData = new ApnData(ApnEditor.sProjection.length);
        }
    }
    
    private void disableAllFields() {
        this.mName.setEnabled(false);
        this.mApn.setEnabled(false);
        this.mProxy.setEnabled(false);
        this.mPort.setEnabled(false);
        this.mUser.setEnabled(false);
        this.mServer.setEnabled(false);
        this.mPassword.setEnabled(false);
        this.mMmsProxy.setEnabled(false);
        this.mMmsPort.setEnabled(false);
        this.mMmsc.setEnabled(false);
        this.mMcc.setEnabled(false);
        this.mMnc.setEnabled(false);
        this.mApnType.setEnabled(false);
        this.mAuthType.setEnabled(false);
        this.mProtocol.setEnabled(false);
        this.mRoamingProtocol.setEnabled(false);
        this.mCarrierEnabled.setEnabled(false);
        this.mBearerMulti.setEnabled(false);
        this.mMvnoType.setEnabled(false);
        this.mMvnoMatchData.setEnabled(false);
    }
    
    private void disableFields(final String[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            final Preference preferenceFromFieldName = this.getPreferenceFromFieldName(array[i]);
            if (preferenceFromFieldName != null) {
                preferenceFromFieldName.setEnabled(false);
            }
        }
    }
    
    static String formatInteger(final String s) {
        try {
            return String.format("%d", Integer.parseInt(s));
        }
        catch (NumberFormatException ex) {
            return s;
        }
    }
    
    private Preference getPreferenceFromFieldName(final String s) {
        int n = 0;
        Label_0498: {
            switch (s.hashCode()) {
                case 1433229538: {
                    if (s.equals("authtype")) {
                        n = 13;
                        break Label_0498;
                    }
                    break;
                }
                case 1216985755: {
                    if (s.equals("password")) {
                        n = 6;
                        break Label_0498;
                    }
                    break;
                }
                case 1183882708: {
                    if (s.equals("mmsport")) {
                        n = 8;
                        break Label_0498;
                    }
                    break;
                }
                case 106941038: {
                    if (s.equals("proxy")) {
                        n = 2;
                        break Label_0498;
                    }
                    break;
                }
                case 3599307: {
                    if (s.equals("user")) {
                        n = 4;
                        break Label_0498;
                    }
                    break;
                }
                case 3575610: {
                    if (s.equals("type")) {
                        n = 12;
                        break Label_0498;
                    }
                    break;
                }
                case 3446913: {
                    if (s.equals("port")) {
                        n = 3;
                        break Label_0498;
                    }
                    break;
                }
                case 3373707: {
                    if (s.equals("name")) {
                        n = 0;
                        break Label_0498;
                    }
                    break;
                }
                case 3355632: {
                    if (s.equals("mmsc")) {
                        n = 9;
                        break Label_0498;
                    }
                    break;
                }
                case 108258: {
                    if (s.equals("mnc")) {
                        n = 11;
                        break Label_0498;
                    }
                    break;
                }
                case 107917: {
                    if (s.equals("mcc")) {
                        n = 10;
                        break Label_0498;
                    }
                    break;
                }
                case 96799: {
                    if (s.equals("apn")) {
                        n = 1;
                        break Label_0498;
                    }
                    break;
                }
                case -520149991: {
                    if (s.equals("mvno_match_data")) {
                        n = 20;
                        break Label_0498;
                    }
                    break;
                }
                case -905826493: {
                    if (s.equals("server")) {
                        n = 5;
                        break Label_0498;
                    }
                    break;
                }
                case -989163880: {
                    if (s.equals("protocol")) {
                        n = 14;
                        break Label_0498;
                    }
                    break;
                }
                case -1039601666: {
                    if (s.equals("roaming_protocol")) {
                        n = 15;
                        break Label_0498;
                    }
                    break;
                }
                case -1230508389: {
                    if (s.equals("bearer_bitmask")) {
                        n = 18;
                        break Label_0498;
                    }
                    break;
                }
                case -1393032351: {
                    if (s.equals("bearer")) {
                        n = 17;
                        break Label_0498;
                    }
                    break;
                }
                case -1640523526: {
                    if (s.equals("carrier_enabled")) {
                        n = 16;
                        break Label_0498;
                    }
                    break;
                }
                case -1954254981: {
                    if (s.equals("mmsproxy")) {
                        n = 7;
                        break Label_0498;
                    }
                    break;
                }
                case -2135515857: {
                    if (s.equals("mvno_type")) {
                        n = 19;
                        break Label_0498;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                return null;
            }
            case 20: {
                return this.mMvnoMatchData;
            }
            case 19: {
                return this.mMvnoType;
            }
            case 17:
            case 18: {
                return this.mBearerMulti;
            }
            case 16: {
                return this.mCarrierEnabled;
            }
            case 15: {
                return this.mRoamingProtocol;
            }
            case 14: {
                return this.mProtocol;
            }
            case 13: {
                return this.mAuthType;
            }
            case 12: {
                return this.mApnType;
            }
            case 11: {
                return this.mMnc;
            }
            case 10: {
                return this.mMcc;
            }
            case 9: {
                return this.mMmsc;
            }
            case 8: {
                return this.mMmsPort;
            }
            case 7: {
                return this.mMmsProxy;
            }
            case 6: {
                return this.mPassword;
            }
            case 5: {
                return this.mServer;
            }
            case 4: {
                return this.mUser;
            }
            case 3: {
                return this.mPort;
            }
            case 2: {
                return this.mProxy;
            }
            case 1: {
                return this.mApn;
            }
            case 0: {
                return this.mName;
            }
        }
    }
    
    private String getUserEnteredApnType() {
        String s2;
        final String s = s2 = this.mApnType.getText();
        if (s != null) {
            s2 = s.trim();
        }
        if (!TextUtils.isEmpty((CharSequence)s2)) {
            final String string = s2;
            if (!"*".equals(s2)) {
                return string;
            }
        }
        String string = s2;
        if (!ArrayUtils.isEmpty((Object[])this.mReadOnlyApnTypes)) {
            final StringBuilder sb = new StringBuilder();
            final List<String> list = Arrays.asList(this.mReadOnlyApnTypes);
            int n = 1;
            final String[] apn_TYPES = PhoneConstants.APN_TYPES;
            int n2;
            for (int length = apn_TYPES.length, i = 0; i < length; ++i, n = n2) {
                final String s3 = apn_TYPES[i];
                n2 = n;
                if (!list.contains(s3)) {
                    n2 = n;
                    if (!s3.equals("ia")) {
                        n2 = n;
                        if (!s3.equals("emergency")) {
                            if (n != 0) {
                                n = 0;
                            }
                            else {
                                sb.append(",");
                            }
                            sb.append(s3);
                            n2 = n;
                        }
                    }
                }
            }
            string = sb.toString();
            final String tag = ApnEditor.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("getUserEnteredApnType: changed apn type to editable apn types: ");
            sb2.append(string);
            Log.d(tag, sb2.toString());
        }
        return string;
    }
    
    static boolean hasAllApns(final String[] array) {
        if (ArrayUtils.isEmpty((Object[])array)) {
            return false;
        }
        final List<String> list = Arrays.asList(array);
        if (list.contains("*")) {
            Log.d(ApnEditor.TAG, "hasAllApns: true because apnList.contains(PhoneConstants.APN_TYPE_ALL)");
            return true;
        }
        final String[] apn_TYPES = PhoneConstants.APN_TYPES;
        for (int length = apn_TYPES.length, i = 0; i < length; ++i) {
            if (!list.contains(apn_TYPES[i])) {
                return false;
            }
        }
        Log.d(ApnEditor.TAG, "hasAllApns: true");
        return true;
    }
    
    private String mvnoDescription(String simOperator) {
        final int indexOfValue = this.mMvnoType.findIndexOfValue(simOperator);
        final String value = this.mMvnoType.getValue();
        if (indexOfValue == -1) {
            return null;
        }
        final String[] stringArray = this.getResources().getStringArray(2130903131);
        final boolean mReadOnlyApn = this.mReadOnlyApn;
        boolean enabled = true;
        final boolean b = mReadOnlyApn || (this.mReadOnlyApnFields != null && Arrays.asList(this.mReadOnlyApnFields).contains("mvno_match_data"));
        final EditTextPreference mMvnoMatchData = this.mMvnoMatchData;
        if (b || indexOfValue == 0) {
            enabled = false;
        }
        mMvnoMatchData.setEnabled(enabled);
        if (simOperator != null && !simOperator.equals(value)) {
            if (stringArray[indexOfValue].equals("SPN")) {
                this.mMvnoMatchData.setText(this.mTelephonyManager.getSimOperatorName());
            }
            else if (stringArray[indexOfValue].equals("IMSI")) {
                simOperator = this.mTelephonyManager.getSimOperator(this.mSubId);
                final EditTextPreference mMvnoMatchData2 = this.mMvnoMatchData;
                final StringBuilder sb = new StringBuilder();
                sb.append(simOperator);
                sb.append("x");
                mMvnoMatchData2.setText(sb.toString());
            }
            else if (stringArray[indexOfValue].equals("GID")) {
                this.mMvnoMatchData.setText(this.mTelephonyManager.getGroupIdLevel1());
            }
        }
        simOperator = stringArray[indexOfValue];
        return simOperator;
    }
    
    private String protocolDescription(String s, final ListPreference listPreference) {
        final int indexOfValue = listPreference.findIndexOfValue(s);
        if (indexOfValue == -1) {
            return null;
        }
        s = this.getResources().getStringArray(2130903044)[indexOfValue];
        return s;
    }
    
    private String starify(final String s) {
        if (s != null && s.length() != 0) {
            final char[] array = new char[s.length()];
            for (int i = 0; i < array.length; ++i) {
                array[i] = 42;
            }
            return new String(array);
        }
        return ApnEditor.sNotSet;
    }
    
    private void updateApnDataToDatabase(final Uri uri, final ContentValues contentValues) {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$ApnEditor$1vSLgWOnd4pMuFU2qFaSz0HXNw8(this, uri, contentValues));
    }
    
    void fillUI(final boolean b) {
        if (b) {
            this.mName.setText(this.mApnData.getString(1));
            this.mApn.setText(this.mApnData.getString(2));
            this.mProxy.setText(this.mApnData.getString(3));
            this.mPort.setText(this.mApnData.getString(4));
            this.mUser.setText(this.mApnData.getString(5));
            this.mServer.setText(this.mApnData.getString(6));
            this.mPassword.setText(this.mApnData.getString(7));
            this.mMmsProxy.setText(this.mApnData.getString(12));
            this.mMmsPort.setText(this.mApnData.getString(13));
            this.mMmsc.setText(this.mApnData.getString(8));
            this.mMcc.setText(this.mApnData.getString(9));
            this.mMnc.setText(this.mApnData.getString(10));
            this.mApnType.setText(this.mApnData.getString(15));
            if (this.mNewApn) {
                final String simOperator = this.mTelephonyManager.getSimOperator(this.mSubId);
                if (simOperator != null && simOperator.length() > 4) {
                    final String substring = simOperator.substring(0, 3);
                    final String substring2 = simOperator.substring(3);
                    this.mMcc.setText(substring);
                    this.mMnc.setText(substring2);
                    this.mCurMnc = substring2;
                    this.mCurMcc = substring;
                }
            }
            final int intValue = this.mApnData.getInteger(14, -1);
            if (intValue != -1) {
                this.mAuthType.setValueIndex(intValue);
            }
            else {
                this.mAuthType.setValue(null);
            }
            this.mProtocol.setValue(this.mApnData.getString(16));
            this.mRoamingProtocol.setValue(this.mApnData.getString(20));
            this.mCarrierEnabled.setChecked(this.mApnData.getInteger(17, 1) == 1);
            this.mBearerInitialVal = this.mApnData.getInteger(18, 0);
            final HashSet<String> values = new HashSet<String>();
            int i = this.mApnData.getInteger(19, 0);
            if (i == 0) {
                if (this.mBearerInitialVal == 0) {
                    values.add("0");
                }
            }
            else {
                for (int n = 1; i != 0; i >>= 1, ++n) {
                    if ((i & 0x1) == 0x1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("");
                        sb.append(n);
                        values.add(sb.toString());
                    }
                }
            }
            if (this.mBearerInitialVal != 0) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("");
                sb2.append(this.mBearerInitialVal);
                if (!values.contains(sb2.toString())) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("");
                    sb3.append(this.mBearerInitialVal);
                    values.add(sb3.toString());
                }
            }
            this.mBearerMulti.setValues(values);
            this.mMvnoType.setValue(this.mApnData.getString(21));
            this.mMvnoMatchData.setEnabled(false);
            this.mMvnoMatchData.setText(this.mApnData.getString(22));
            if (this.mNewApn && this.mMvnoTypeStr != null && this.mMvnoMatchDataStr != null) {
                this.mMvnoType.setValue(this.mMvnoTypeStr);
                this.mMvnoMatchData.setText(this.mMvnoMatchDataStr);
            }
        }
        this.mName.setSummary(this.checkNull(this.mName.getText()));
        this.mApn.setSummary(this.checkNull(this.mApn.getText()));
        this.mProxy.setSummary(this.checkNull(this.mProxy.getText()));
        this.mPort.setSummary(this.checkNull(this.mPort.getText()));
        this.mUser.setSummary(this.checkNull(this.mUser.getText()));
        this.mServer.setSummary(this.checkNull(this.mServer.getText()));
        this.mPassword.setSummary(this.starify(this.mPassword.getText()));
        this.mMmsProxy.setSummary(this.checkNull(this.mMmsProxy.getText()));
        this.mMmsPort.setSummary(this.checkNull(this.mMmsPort.getText()));
        this.mMmsc.setSummary(this.checkNull(this.mMmsc.getText()));
        this.mMcc.setSummary(formatInteger(this.checkNull(this.mMcc.getText())));
        this.mMnc.setSummary(formatInteger(this.checkNull(this.mMnc.getText())));
        this.mApnType.setSummary(this.checkNull(this.mApnType.getText()));
        final String value = this.mAuthType.getValue();
        if (value != null) {
            final int int1 = Integer.parseInt(value);
            this.mAuthType.setValueIndex(int1);
            this.mAuthType.setSummary(this.getResources().getStringArray(2130903042)[int1]);
        }
        else {
            this.mAuthType.setSummary(ApnEditor.sNotSet);
        }
        this.mProtocol.setSummary(this.checkNull(this.protocolDescription(this.mProtocol.getValue(), this.mProtocol)));
        this.mRoamingProtocol.setSummary(this.checkNull(this.protocolDescription(this.mRoamingProtocol.getValue(), this.mRoamingProtocol)));
        this.mBearerMulti.setSummary(this.checkNull(this.bearerMultiDescription(this.mBearerMulti.getValues())));
        this.mMvnoType.setSummary(this.checkNull(this.mvnoDescription(this.mMvnoType.getValue())));
        this.mMvnoMatchData.setSummary(this.checkNull(this.mMvnoMatchData.getText()));
        if (this.getResources().getBoolean(2131034117)) {
            this.mCarrierEnabled.setEnabled(true);
        }
        else {
            this.mCarrierEnabled.setEnabled(false);
        }
    }
    
    ApnData getApnDataFromUri(final Uri uri) {
        ApnData apnData = null;
        final Cursor query = this.getContentResolver().query(uri, ApnEditor.sProjection, (String)null, (String[])null, (String)null);
        if (query != null) {
            final ApnData apnData2 = apnData = null;
            while (true) {
                try {
                    try {
                        query.moveToFirst();
                        apnData = apnData2;
                        apnData = new ApnData(uri, query);
                    }
                    finally {
                        if (query != null) {
                            if (apnData != null) {
                                final Cursor cursor = query;
                                cursor.close();
                            }
                            else {
                                query.close();
                            }
                        }
                    }
                }
                catch (Throwable t) {}
                try {
                    final Cursor cursor = query;
                    cursor.close();
                    continue;
                }
                catch (Throwable t2) {}
                break;
            }
        }
        if (query != null) {
            query.close();
        }
        if (apnData == null) {
            final String tag = ApnEditor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Can't get apnData from Uri ");
            sb.append(uri);
            Log.d(tag, sb.toString());
        }
        return apnData;
    }
    
    public int getMetricsCategory() {
        return 13;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082704);
        ApnEditor.sNotSet = this.getResources().getString(2131886332);
        this.mName = (EditTextPreference)this.findPreference("apn_name");
        this.mApn = (EditTextPreference)this.findPreference("apn_apn");
        this.mProxy = (EditTextPreference)this.findPreference("apn_http_proxy");
        this.mPort = (EditTextPreference)this.findPreference("apn_http_port");
        this.mUser = (EditTextPreference)this.findPreference("apn_user");
        this.mServer = (EditTextPreference)this.findPreference("apn_server");
        this.mPassword = (EditTextPreference)this.findPreference("apn_password");
        this.mMmsProxy = (EditTextPreference)this.findPreference("apn_mms_proxy");
        this.mMmsPort = (EditTextPreference)this.findPreference("apn_mms_port");
        this.mMmsc = (EditTextPreference)this.findPreference("apn_mmsc");
        this.mMcc = (EditTextPreference)this.findPreference("apn_mcc");
        this.mMnc = (EditTextPreference)this.findPreference("apn_mnc");
        this.mApnType = (EditTextPreference)this.findPreference("apn_type");
        this.mAuthType = (ListPreference)this.findPreference("auth_type");
        this.mProtocol = (ListPreference)this.findPreference("apn_protocol");
        this.mRoamingProtocol = (ListPreference)this.findPreference("apn_roaming_protocol");
        this.mCarrierEnabled = (SwitchPreference)this.findPreference("carrier_enabled");
        this.mBearerMulti = (MultiSelectListPreference)this.findPreference("bearer_multi");
        this.mMvnoType = (ListPreference)this.findPreference("mvno_type");
        this.mMvnoMatchData = (EditTextPreference)this.findPreference("mvno_match_data");
        final Intent intent = this.getIntent();
        final String action = intent.getAction();
        this.mSubId = intent.getIntExtra("sub_id", -1);
        final boolean b = false;
        this.mReadOnlyApn = false;
        this.mReadOnlyApnTypes = null;
        this.mReadOnlyApnFields = null;
        final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)this.getSystemService("carrier_config");
        if (carrierConfigManager != null) {
            final PersistableBundle config = carrierConfigManager.getConfig();
            if (config != null) {
                this.mReadOnlyApnTypes = config.getStringArray("read_only_apn_types_string_array");
                if (!ArrayUtils.isEmpty((Object[])this.mReadOnlyApnTypes)) {
                    for (final String s : this.mReadOnlyApnTypes) {
                        final String tag = ApnEditor.TAG;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("onCreate: read only APN type: ");
                        sb.append(s);
                        Log.d(tag, sb.toString());
                    }
                }
                this.mReadOnlyApnFields = config.getStringArray("read_only_apn_fields_string_array");
            }
        }
        Uri data = null;
        if (action.equals("android.intent.action.EDIT")) {
            final Uri uri = data = intent.getData();
            if (!uri.isPathPrefixMatch(Telephony$Carriers.CONTENT_URI)) {
                final String tag2 = ApnEditor.TAG;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Edit request not for carrier table. Uri: ");
                sb2.append(uri);
                Log.e(tag2, sb2.toString());
                this.finish();
                return;
            }
        }
        else {
            if (!action.equals("android.intent.action.INSERT")) {
                this.finish();
                return;
            }
            this.mCarrierUri = intent.getData();
            if (!this.mCarrierUri.isPathPrefixMatch(Telephony$Carriers.CONTENT_URI)) {
                final String tag3 = ApnEditor.TAG;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Insert request not for carrier table. Uri: ");
                sb3.append(this.mCarrierUri);
                Log.e(tag3, sb3.toString());
                this.finish();
                return;
            }
            this.mNewApn = true;
            this.mMvnoTypeStr = intent.getStringExtra("mvno_type");
            this.mMvnoMatchDataStr = intent.getStringExtra("mvno_match_data");
        }
        if (data != null) {
            this.mApnData = this.getApnDataFromUri(data);
        }
        else {
            this.mApnData = new ApnData(ApnEditor.sProjection.length);
        }
        this.mTelephonyManager = (TelephonyManager)this.getSystemService("phone");
        final boolean b2 = this.mApnData.getInteger(23, 1) == 1;
        final String tag4 = ApnEditor.TAG;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("onCreate: EDITED ");
        sb4.append(b2);
        Log.d(tag4, sb4.toString());
        if (!b2 && (this.mApnData.getInteger(24, 1) == 0 || this.apnTypesMatch(this.mReadOnlyApnTypes, this.mApnData.getString(15)))) {
            Log.d(ApnEditor.TAG, "onCreate: apnTypesMatch; read-only APN");
            this.mReadOnlyApn = true;
            this.disableAllFields();
        }
        else if (!ArrayUtils.isEmpty((Object[])this.mReadOnlyApnFields)) {
            this.disableFields(this.mReadOnlyApnFields);
        }
        for (int j = 0; j < this.getPreferenceScreen().getPreferenceCount(); ++j) {
            this.getPreferenceScreen().getPreference(j).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        }
        boolean b3 = b;
        if (bundle == null) {
            b3 = true;
        }
        this.fillUI(b3);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        if (!this.mNewApn && !this.mReadOnlyApn) {
            menu.add(0, 1, 0, 2131888290).setIcon(2131230999);
        }
        menu.add(0, 2, 0, 2131888303).setIcon(17301582);
        menu.add(0, 3, 0, 2131888289).setIcon(17301560);
    }
    
    public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        if (n != 4) {
            return false;
        }
        if (this.validateAndSaveApnData()) {
            this.finish();
        }
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 3: {
                this.finish();
                return true;
            }
            case 2: {
                if (this.validateAndSaveApnData()) {
                    this.finish();
                }
                return true;
            }
            case 1: {
                this.deleteApn();
                this.finish();
                return true;
            }
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final String key = preference.getKey();
        if ("auth_type".equals(key)) {
            try {
                final int int1 = Integer.parseInt((String)o);
                this.mAuthType.setValueIndex(int1);
                this.mAuthType.setSummary(this.getResources().getStringArray(2130903042)[int1]);
                return true;
            }
            catch (NumberFormatException ex) {
                return false;
            }
        }
        if ("apn_protocol".equals(key)) {
            final String protocolDescription = this.protocolDescription((String)o, this.mProtocol);
            if (protocolDescription == null) {
                return false;
            }
            this.mProtocol.setSummary(protocolDescription);
            this.mProtocol.setValue((String)o);
        }
        else if ("apn_roaming_protocol".equals(key)) {
            final String protocolDescription2 = this.protocolDescription((String)o, this.mRoamingProtocol);
            if (protocolDescription2 == null) {
                return false;
            }
            this.mRoamingProtocol.setSummary(protocolDescription2);
            this.mRoamingProtocol.setValue((String)o);
        }
        else if ("bearer_multi".equals(key)) {
            final String bearerMultiDescription = this.bearerMultiDescription((Set<String>)o);
            if (bearerMultiDescription == null) {
                return false;
            }
            this.mBearerMulti.setValues((Set<String>)o);
            this.mBearerMulti.setSummary(bearerMultiDescription);
        }
        else if ("mvno_type".equals(key)) {
            final String mvnoDescription = this.mvnoDescription((String)o);
            if (mvnoDescription == null) {
                return false;
            }
            this.mMvnoType.setValue((String)o);
            this.mMvnoType.setSummary(mvnoDescription);
        }
        else if ("apn_password".equals(key)) {
            final EditTextPreference mPassword = this.mPassword;
            String value;
            if (o != null) {
                value = String.valueOf(o);
            }
            else {
                value = "";
            }
            mPassword.setSummary(this.starify(value));
        }
        else if (!"carrier_enabled".equals(key)) {
            String value2;
            if (o != null) {
                value2 = String.valueOf(o);
            }
            else {
                value2 = null;
            }
            preference.setSummary(this.checkNull(value2));
        }
        return true;
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        view.setOnKeyListener((View$OnKeyListener)this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }
    
    boolean setIntValueAndCheckIfDiff(final ContentValues contentValues, final String s, final int n, final boolean b, final int n2) {
        final Integer integer = this.mApnData.getInteger(n2);
        final boolean b2 = b || n != integer;
        if (b2) {
            contentValues.put(s, n);
        }
        return b2;
    }
    
    boolean setStringValueAndCheckIfDiff(final ContentValues contentValues, final String s, final String s2, final boolean b, final int n) {
        final String string = this.mApnData.getString(n);
        boolean b2 = false;
        Label_0056: {
            Label_0053: {
                if (!b) {
                    if (!TextUtils.isEmpty((CharSequence)s2) || !TextUtils.isEmpty((CharSequence)string)) {
                        if (s2 == null) {
                            break Label_0053;
                        }
                        if (!s2.equals(string)) {
                            break Label_0053;
                        }
                    }
                    b2 = false;
                    break Label_0056;
                }
            }
            b2 = true;
        }
        if (b2 && s2 != null) {
            contentValues.put(s, s2);
        }
        return b2;
    }
    
    void showError() {
        ErrorDialog.showError(this);
    }
    
    boolean validateAndSaveApnData() {
        if (this.mReadOnlyApn) {
            return true;
        }
        final String checkNotSet = this.checkNotSet(this.mName.getText());
        final String checkNotSet2 = this.checkNotSet(this.mApn.getText());
        final String checkNotSet3 = this.checkNotSet(this.mMcc.getText());
        final String checkNotSet4 = this.checkNotSet(this.mMnc.getText());
        if (this.validateApnData() != null) {
            this.showError();
            return false;
        }
        final ContentValues contentValues = new ContentValues();
        final boolean setStringValueAndCheckIfDiff = this.setStringValueAndCheckIfDiff(contentValues, "mmsc", this.checkNotSet(this.mMmsc.getText()), this.setStringValueAndCheckIfDiff(contentValues, "password", this.checkNotSet(this.mPassword.getText()), this.setStringValueAndCheckIfDiff(contentValues, "server", this.checkNotSet(this.mServer.getText()), this.setStringValueAndCheckIfDiff(contentValues, "user", this.checkNotSet(this.mUser.getText()), this.setStringValueAndCheckIfDiff(contentValues, "mmsport", this.checkNotSet(this.mMmsPort.getText()), this.setStringValueAndCheckIfDiff(contentValues, "mmsproxy", this.checkNotSet(this.mMmsProxy.getText()), this.setStringValueAndCheckIfDiff(contentValues, "port", this.checkNotSet(this.mPort.getText()), this.setStringValueAndCheckIfDiff(contentValues, "proxy", this.checkNotSet(this.mProxy.getText()), this.setStringValueAndCheckIfDiff(contentValues, "apn", checkNotSet2, this.setStringValueAndCheckIfDiff(contentValues, "name", checkNotSet, this.mNewApn, 1), 2), 3), 4), 12), 13), 5), 6), 7), 8);
        final String value = this.mAuthType.getValue();
        boolean setIntValueAndCheckIfDiff = setStringValueAndCheckIfDiff;
        if (value != null) {
            setIntValueAndCheckIfDiff = this.setIntValueAndCheckIfDiff(contentValues, "authtype", Integer.parseInt(value), setStringValueAndCheckIfDiff, 14);
        }
        final boolean setStringValueAndCheckIfDiff2 = this.setStringValueAndCheckIfDiff(contentValues, "mnc", checkNotSet4, this.setStringValueAndCheckIfDiff(contentValues, "mcc", checkNotSet3, this.setStringValueAndCheckIfDiff(contentValues, "type", this.checkNotSet(this.getUserEnteredApnType()), this.setStringValueAndCheckIfDiff(contentValues, "roaming_protocol", this.checkNotSet(this.mRoamingProtocol.getValue()), this.setStringValueAndCheckIfDiff(contentValues, "protocol", this.checkNotSet(this.mProtocol.getValue()), setIntValueAndCheckIfDiff, 16), 20), 15), 9), 10);
        final StringBuilder sb = new StringBuilder();
        sb.append(checkNotSet3);
        sb.append(checkNotSet4);
        contentValues.put("numeric", sb.toString());
        if (this.mCurMnc != null && this.mCurMcc != null && this.mCurMnc.equals(checkNotSet4) && this.mCurMcc.equals(checkNotSet3)) {
            contentValues.put("current", 1);
        }
        final Set<String> values = this.mBearerMulti.getValues();
        int n = 0;
        final Iterator<String> iterator = values.iterator();
        int n2;
        while (true) {
            n2 = n;
            if (!iterator.hasNext()) {
                break;
            }
            final String s = iterator.next();
            if (Integer.parseInt(s) == 0) {
                n2 = 0;
                break;
            }
            n |= ServiceState.getBitmaskForTech(Integer.parseInt(s));
        }
        final boolean setIntValueAndCheckIfDiff2 = this.setIntValueAndCheckIfDiff(contentValues, "bearer_bitmask", n2, setStringValueAndCheckIfDiff2, 19);
        int mBearerInitialVal;
        if (n2 != 0 && this.mBearerInitialVal != 0) {
            if (ServiceState.bitmaskHasTech(n2, this.mBearerInitialVal)) {
                mBearerInitialVal = this.mBearerInitialVal;
            }
            else {
                mBearerInitialVal = 0;
            }
        }
        else {
            mBearerInitialVal = 0;
        }
        final boolean setIntValueAndCheckIfDiff3 = this.setIntValueAndCheckIfDiff(contentValues, "carrier_enabled", this.mCarrierEnabled.isChecked() ? 1 : 0, this.setStringValueAndCheckIfDiff(contentValues, "mvno_match_data", this.checkNotSet(this.mMvnoMatchData.getText()), this.setStringValueAndCheckIfDiff(contentValues, "mvno_type", this.checkNotSet(this.mMvnoType.getValue()), this.setIntValueAndCheckIfDiff(contentValues, "bearer", mBearerInitialVal, setIntValueAndCheckIfDiff2, 18), 21), 22), 17);
        contentValues.put("edited", 1);
        if (setIntValueAndCheckIfDiff3) {
            Uri uri;
            if (this.mApnData.getUri() == null) {
                uri = this.mCarrierUri;
            }
            else {
                uri = this.mApnData.getUri();
            }
            this.updateApnDataToDatabase(uri, contentValues);
        }
        return true;
    }
    
    String validateApnData() {
        String s = null;
        final String checkNotSet = this.checkNotSet(this.mName.getText());
        final String checkNotSet2 = this.checkNotSet(this.mApn.getText());
        final String checkNotSet3 = this.checkNotSet(this.mMcc.getText());
        final String checkNotSet4 = this.checkNotSet(this.mMnc.getText());
        if (TextUtils.isEmpty((CharSequence)checkNotSet)) {
            s = this.getResources().getString(2131887605);
        }
        else if (TextUtils.isEmpty((CharSequence)checkNotSet2)) {
            s = this.getResources().getString(2131887602);
        }
        else if (checkNotSet3 != null && checkNotSet3.length() == 3) {
            if (checkNotSet4 == null || (checkNotSet4.length() & 0xFFFE) != 0x2) {
                s = this.getResources().getString(2131887604);
            }
        }
        else {
            s = this.getResources().getString(2131887603);
        }
        String format;
        if ((format = s) == null) {
            format = s;
            if (!ArrayUtils.isEmpty((Object[])this.mReadOnlyApnTypes)) {
                format = s;
                if (this.apnTypesMatch(this.mReadOnlyApnTypes, this.getUserEnteredApnType())) {
                    final StringBuilder sb = new StringBuilder();
                    for (final String s2 : this.mReadOnlyApnTypes) {
                        sb.append(s2);
                        sb.append(", ");
                        final String tag = ApnEditor.TAG;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("validateApnData: appending type: ");
                        sb2.append(s2);
                        Log.d(tag, sb2.toString());
                    }
                    if (sb.length() >= 2) {
                        sb.delete(sb.length() - 2, sb.length());
                    }
                    format = String.format(this.getResources().getString(2131887601), sb);
                }
            }
        }
        return format;
    }
    
    static class ApnData
    {
        Object[] mData;
        Uri mUri;
        
        ApnData(final int n) {
            this.mData = new Object[n];
        }
        
        ApnData(final Uri mUri, final Cursor cursor) {
            this.mUri = mUri;
            this.mData = new Object[cursor.getColumnCount()];
            for (int i = 0; i < this.mData.length; ++i) {
                switch (cursor.getType(i)) {
                    default: {
                        this.mData[i] = null;
                        break;
                    }
                    case 4: {
                        this.mData[i] = cursor.getBlob(i);
                        break;
                    }
                    case 3: {
                        this.mData[i] = cursor.getString(i);
                        break;
                    }
                    case 2: {
                        this.mData[i] = cursor.getFloat(i);
                        break;
                    }
                    case 1: {
                        this.mData[i] = cursor.getInt(i);
                        break;
                    }
                }
            }
        }
        
        Integer getInteger(final int n) {
            return (Integer)this.mData[n];
        }
        
        Integer getInteger(final int n, Integer n2) {
            final Integer integer = this.getInteger(n);
            if (integer != null) {
                n2 = integer;
            }
            return n2;
        }
        
        String getString(final int n) {
            return (String)this.mData[n];
        }
        
        Uri getUri() {
            return this.mUri;
        }
    }
    
    public static class ErrorDialog extends InstrumentedDialogFragment
    {
        public static void showError(final ApnEditor apnEditor) {
            final ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.setTargetFragment((Fragment)apnEditor, 0);
            errorDialog.show(apnEditor.getFragmentManager(), "error");
        }
        
        @Override
        public int getMetricsCategory() {
            return 530;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder(this.getContext()).setTitle(2131887606).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).setMessage((CharSequence)((ApnEditor)this.getTargetFragment()).validateApnData()).create();
        }
    }
}
