package com.android.settings.network;

import android.support.v7.preference.Preference;
import android.app.admin.DevicePolicyManager;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ProxyPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public ProxyPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference("proxy_settings");
        if (preference != null) {
            preference.setEnabled(((DevicePolicyManager)this.mContext.getSystemService("device_policy")).getGlobalProxyAdmin() == null);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "proxy_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return false;
    }
}
