package com.android.settings.backup;

import java.net.URISyntaxException;
import android.os.UserHandle;
import android.os.RemoteException;
import android.util.Log;
import com.android.settings.Settings;
import android.content.Intent;
import android.app.backup.IBackupManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.app.backup.IBackupManager;

public class BackupSettingsHelper
{
    private IBackupManager mBackupManager;
    private Context mContext;
    
    public BackupSettingsHelper(final Context mContext) {
        this.mBackupManager = IBackupManager$Stub.asInterface(ServiceManager.getService("backup"));
        this.mContext = mContext;
    }
    
    private Intent getIntentForDefaultBackupSettings() {
        return new Intent(this.mContext, (Class)Settings.PrivacySettingsActivity.class);
    }
    
    private Intent getIntentFromBackupTransport() {
        try {
            final Intent dataManagementIntent = this.mBackupManager.getDataManagementIntent(this.mBackupManager.getCurrentTransport());
            if (Log.isLoggable("BackupSettingsHelper", 3)) {
                if (dataManagementIntent != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Parsed intent from backup transport: ");
                    sb.append(dataManagementIntent.toString());
                    Log.d("BackupSettingsHelper", sb.toString());
                }
                else {
                    Log.d("BackupSettingsHelper", "Received a null intent from backup transport");
                }
            }
            return dataManagementIntent;
        }
        catch (RemoteException ex) {
            Log.e("BackupSettingsHelper", "Error getting data management intent", (Throwable)ex);
            return null;
        }
    }
    
    private boolean isBackupServiceActive() {
        boolean backupServiceActive;
        try {
            backupServiceActive = this.mBackupManager.isBackupServiceActive(UserHandle.myUserId());
        }
        catch (Exception ex) {
            backupServiceActive = false;
        }
        return backupServiceActive;
    }
    
    public Intent getIntentForBackupSettings() {
        Intent intent;
        if (this.isIntentProvidedByTransport()) {
            intent = this.getIntentForBackupSettingsFromTransport();
        }
        else {
            Log.e("BackupSettingsHelper", "Backup transport has not provided an intent or the component for the intent is not found!");
            intent = this.getIntentForDefaultBackupSettings();
        }
        return intent;
    }
    
    Intent getIntentForBackupSettingsFromTransport() {
        final Intent intentFromBackupTransport = this.getIntentFromBackupTransport();
        if (intentFromBackupTransport != null) {
            intentFromBackupTransport.putExtra("backup_services_available", this.isBackupServiceActive());
        }
        return intentFromBackupTransport;
    }
    
    public Intent getIntentProvidedByManufacturer() {
        if (Log.isLoggable("BackupSettingsHelper", 3)) {
            Log.d("BackupSettingsHelper", "Getting a backup settings intent provided by manufacturer");
        }
        final String string = this.mContext.getResources().getString(2131887093);
        if (string != null && !string.isEmpty()) {
            try {
                return Intent.parseUri(string, 0);
            }
            catch (URISyntaxException ex) {
                Log.e("BackupSettingsHelper", "Invalid intent provided by the manufacturer.", (Throwable)ex);
            }
        }
        return null;
    }
    
    public String getLabelForBackupSettings() {
        final String labelFromBackupTransport = this.getLabelFromBackupTransport();
        if (labelFromBackupTransport != null) {
            final String string = labelFromBackupTransport;
            if (!labelFromBackupTransport.isEmpty()) {
                return string;
            }
        }
        return this.mContext.getString(2131888668);
    }
    
    String getLabelFromBackupTransport() {
        try {
            final String dataManagementLabel = this.mBackupManager.getDataManagementLabel(this.mBackupManager.getCurrentTransport());
            if (Log.isLoggable("BackupSettingsHelper", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Received the backup settings label from backup transport: ");
                sb.append(dataManagementLabel);
                Log.d("BackupSettingsHelper", sb.toString());
            }
            return dataManagementLabel;
        }
        catch (RemoteException ex) {
            Log.e("BackupSettingsHelper", "Error getting data management label", (Throwable)ex);
            return null;
        }
    }
    
    public String getLabelProvidedByManufacturer() {
        return this.mContext.getResources().getString(2131887094);
    }
    
    public String getSummaryForBackupSettings() {
        String s;
        if ((s = this.getSummaryFromBackupTransport()) == null) {
            s = this.mContext.getString(2131886535);
        }
        return s;
    }
    
    String getSummaryFromBackupTransport() {
        try {
            final String destinationString = this.mBackupManager.getDestinationString(this.mBackupManager.getCurrentTransport());
            if (Log.isLoggable("BackupSettingsHelper", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Received the backup settings summary from backup transport: ");
                sb.append(destinationString);
                Log.d("BackupSettingsHelper", sb.toString());
            }
            return destinationString;
        }
        catch (RemoteException ex) {
            Log.e("BackupSettingsHelper", "Error getting data management summary", (Throwable)ex);
            return null;
        }
    }
    
    public boolean isBackupProvidedByManufacturer() {
        if (Log.isLoggable("BackupSettingsHelper", 3)) {
            Log.d("BackupSettingsHelper", "Checking if intent provided by manufacturer");
        }
        final String string = this.mContext.getResources().getString(2131887093);
        return string != null && !string.isEmpty();
    }
    
    boolean isIntentProvidedByTransport() {
        final Intent intentFromBackupTransport = this.getIntentFromBackupTransport();
        return intentFromBackupTransport != null && intentFromBackupTransport.resolveActivity(this.mContext.getPackageManager()) != null;
    }
}
