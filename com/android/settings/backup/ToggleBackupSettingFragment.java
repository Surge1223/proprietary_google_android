package com.android.settings.backup;

import com.android.settings.SettingsActivity;
import android.view.View;
import android.support.v7.preference.PreferenceScreen;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.app.backup.IBackupManager$Stub;
import android.os.ServiceManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.provider.Settings;
import android.os.RemoteException;
import android.util.Log;
import com.android.settings.widget.ToggleSwitch;
import com.android.settings.widget.SwitchBar;
import android.support.v7.preference.Preference;
import android.app.Dialog;
import android.app.backup.IBackupManager;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.SettingsPreferenceFragment;

public class ToggleBackupSettingFragment extends SettingsPreferenceFragment implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    private IBackupManager mBackupManager;
    private Dialog mConfirmDialog;
    private Preference mSummaryPreference;
    protected SwitchBar mSwitchBar;
    protected ToggleSwitch mToggleSwitch;
    private boolean mWaitingForConfirmationDialog;
    
    public ToggleBackupSettingFragment() {
        this.mWaitingForConfirmationDialog = false;
    }
    
    private void setBackupEnabled(final boolean backupEnabled) {
        if (this.mBackupManager != null) {
            try {
                this.mBackupManager.setBackupEnabled(backupEnabled);
            }
            catch (RemoteException ex) {
                Log.e("ToggleBackupSettingFragment", "Error communicating with BackupManager", (Throwable)ex);
            }
        }
    }
    
    private void showEraseBackupDialog() {
        CharSequence message;
        if (Settings.Secure.getInt(this.getContentResolver(), "user_full_data_backup_aware", 0) != 0) {
            message = this.getResources().getText(2131887700);
        }
        else {
            message = this.getResources().getText(2131886540);
        }
        this.mWaitingForConfirmationDialog = true;
        this.mConfirmDialog = (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage(message).setTitle(2131886541).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).setOnDismissListener((DialogInterface$OnDismissListener)this).show();
    }
    
    public int getMetricsCategory() {
        return 81;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.mToggleSwitch.setOnBeforeCheckedChangeListener((ToggleSwitch.OnBeforeCheckedChangeListener)new ToggleSwitch.OnBeforeCheckedChangeListener() {
            @Override
            public boolean onBeforeCheckedChanged(final ToggleSwitch toggleSwitch, final boolean b) {
                if (!b) {
                    ToggleBackupSettingFragment.this.showEraseBackupDialog();
                    return true;
                }
                ToggleBackupSettingFragment.this.setBackupEnabled(true);
                ToggleBackupSettingFragment.this.mSwitchBar.setCheckedInternal(true);
                return true;
            }
        });
        this.mSwitchBar.show();
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            this.setBackupEnabled(this.mWaitingForConfirmationDialog = false);
            this.mSwitchBar.setCheckedInternal(false);
        }
        else if (n == -2) {
            this.mWaitingForConfirmationDialog = false;
            this.setBackupEnabled(true);
            this.mSwitchBar.setCheckedInternal(true);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mBackupManager = IBackupManager$Stub.asInterface(ServiceManager.getService("backup"));
        final PreferenceScreen preferenceScreen = this.getPreferenceManager().createPreferenceScreen((Context)this.getActivity());
        this.setPreferenceScreen(preferenceScreen);
        (this.mSummaryPreference = new Preference(this.getPrefContext()) {
            @Override
            public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
                super.onBindViewHolder(preferenceViewHolder);
                ((TextView)preferenceViewHolder.findViewById(16908304)).setText(this.getSummary());
            }
        }).setPersistent(false);
        this.mSummaryPreference.setLayoutResource(2131558846);
        preferenceScreen.addPreference(this.mSummaryPreference);
    }
    
    public void onDestroyView() {
        super.onDestroyView();
        this.mToggleSwitch.setOnBeforeCheckedChangeListener(null);
        this.mSwitchBar.hide();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        if (this.mWaitingForConfirmationDialog) {
            this.setBackupEnabled(true);
            this.mSwitchBar.setCheckedInternal(true);
        }
    }
    
    public void onStop() {
        if (this.mConfirmDialog != null && this.mConfirmDialog.isShowing()) {
            this.mConfirmDialog.dismiss();
        }
        this.mConfirmDialog = null;
        super.onStop();
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar();
        this.mToggleSwitch = this.mSwitchBar.getSwitch();
        if (Settings.Secure.getInt(this.getContentResolver(), "user_full_data_backup_aware", 0) != 0) {
            this.mSummaryPreference.setSummary(2131887699);
        }
        else {
            this.mSummaryPreference.setSummary(2131886537);
        }
        try {
            this.mSwitchBar.setCheckedInternal(this.mBackupManager != null && this.mBackupManager.isBackupEnabled());
        }
        catch (RemoteException ex) {
            this.mSwitchBar.setEnabled(false);
        }
        this.getActivity().setTitle(2131886538);
    }
}
