package com.android.settings.backup;

import android.content.Intent;
import android.app.Fragment;
import android.os.Bundle;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import android.util.Log;
import android.os.UserHandle;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.app.FragmentManager;
import com.android.settings.search.Indexable;
import android.app.Activity;

public class BackupSettingsActivity extends Activity implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private FragmentManager mFragmentManager;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                if (UserHandle.myUserId() != 0) {
                    if (Log.isLoggable("BackupSettingsActivity", 3)) {
                        Log.d("BackupSettingsActivity", "Not a system user, not indexing the screen");
                    }
                    nonIndexableKeys.add("backup");
                }
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                searchIndexableRaw.title = context.getString(2131888668);
                searchIndexableRaw.screenTitle = context.getString(2131889054);
                searchIndexableRaw.keywords = context.getString(2131887945);
                searchIndexableRaw.intentTargetPackage = context.getPackageName();
                searchIndexableRaw.intentTargetClass = BackupSettingsActivity.class.getName();
                searchIndexableRaw.intentAction = "android.intent.action.MAIN";
                searchIndexableRaw.key = "backup";
                list.add(searchIndexableRaw);
                return list;
            }
        };
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final BackupSettingsHelper backupSettingsHelper = new BackupSettingsHelper((Context)this);
        if (!backupSettingsHelper.isBackupProvidedByManufacturer()) {
            if (Log.isLoggable("BackupSettingsActivity", 3)) {
                Log.d("BackupSettingsActivity", "No manufacturer settings found, launching the backup settings directly");
            }
            final Intent intentForBackupSettings = backupSettingsHelper.getIntentForBackupSettings();
            try {
                this.getPackageManager().setComponentEnabledSetting(intentForBackupSettings.getComponent(), 1, 1);
            }
            catch (SecurityException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Trying to enable activity ");
                sb.append(intentForBackupSettings.getComponent());
                sb.append(" but couldn't: ");
                sb.append(ex.getMessage());
                Log.w("BackupSettingsActivity", sb.toString());
            }
            this.startActivityForResult(intentForBackupSettings, 1);
            this.finish();
        }
        else {
            if (Log.isLoggable("BackupSettingsActivity", 3)) {
                Log.d("BackupSettingsActivity", "Manufacturer provided backup settings, showing the preference screen");
            }
            if (this.mFragmentManager == null) {
                this.mFragmentManager = this.getFragmentManager();
            }
            this.mFragmentManager.beginTransaction().replace(16908290, (Fragment)new BackupSettingsFragment()).commit();
        }
    }
    
    void setFragmentManager(final FragmentManager mFragmentManager) {
        this.mFragmentManager = mFragmentManager;
    }
}
