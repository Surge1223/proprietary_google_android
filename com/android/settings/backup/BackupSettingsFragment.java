package com.android.settings.backup;

import android.os.Bundle;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class BackupSettingsFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {};
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<BackupSettingsPreferenceController> list = (ArrayList<BackupSettingsPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new BackupSettingsPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "BackupSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 818;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082721;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
}
