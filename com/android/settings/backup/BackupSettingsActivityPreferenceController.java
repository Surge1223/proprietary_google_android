package com.android.settings.backup;

import android.content.Context;
import android.os.UserManager;
import android.app.backup.BackupManager;
import com.android.settings.core.BasePreferenceController;

public class BackupSettingsActivityPreferenceController extends BasePreferenceController
{
    private static final String KEY_BACKUP_SETTINGS = "backup_settings";
    private static final String TAG = "BackupSettingActivityPC";
    private final BackupManager mBackupManager;
    private final UserManager mUm;
    
    public BackupSettingsActivityPreferenceController(final Context context) {
        super(context, "backup_settings");
        this.mUm = (UserManager)context.getSystemService("user");
        this.mBackupManager = new BackupManager(context);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mUm.isAdminUser()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        CharSequence charSequence;
        if (this.mBackupManager.isBackupEnabled()) {
            charSequence = this.mContext.getText(2131886155);
        }
        else {
            charSequence = this.mContext.getText(2131886154);
        }
        return charSequence;
    }
}
