package com.android.settings.backup;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.content.Intent;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BackupSettingsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private Intent mBackupSettingsIntent;
    private String mBackupSettingsSummary;
    private String mBackupSettingsTitle;
    private Intent mManufacturerIntent;
    private String mManufacturerLabel;
    
    public BackupSettingsPreferenceController(final Context context) {
        super(context);
        final BackupSettingsHelper backupSettingsHelper = new BackupSettingsHelper(context);
        this.mBackupSettingsIntent = backupSettingsHelper.getIntentForBackupSettings();
        this.mBackupSettingsTitle = backupSettingsHelper.getLabelForBackupSettings();
        this.mBackupSettingsSummary = backupSettingsHelper.getSummaryForBackupSettings();
        this.mManufacturerIntent = backupSettingsHelper.getIntentProvidedByManufacturer();
        this.mManufacturerLabel = backupSettingsHelper.getLabelProvidedByManufacturer();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        final Preference preference = preferenceScreen.findPreference("backup_settings");
        final Preference preference2 = preferenceScreen.findPreference("manufacturer_backup");
        preference.setIntent(this.mBackupSettingsIntent);
        preference.setTitle(this.mBackupSettingsTitle);
        preference.setSummary(this.mBackupSettingsSummary);
        preference2.setIntent(this.mManufacturerIntent);
        preference2.setTitle(this.mManufacturerLabel);
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
}
