package com.android.settings;

import android.content.DialogInterface$OnDismissListener;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.os.UserManager;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Intent;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.content.ContentResolver;
import android.provider.Settings;
import android.telephony.euicc.EuiccManager;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.content.DialogInterface;
import java.util.Iterator;
import android.widget.TextView;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.text.TextUtils;
import java.util.ArrayList;
import android.content.Context;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionInfo;
import java.util.List;
import android.widget.Spinner;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;

public class ResetNetwork extends InstrumentedFragment
{
    private View mContentView;
    private CheckBox mEsimCheckbox;
    private View mEsimContainer;
    private Button mInitiateButton;
    private final View.OnClickListener mInitiateListener;
    private Spinner mSubscriptionSpinner;
    private List<SubscriptionInfo> mSubscriptions;
    
    public ResetNetwork() {
        this.mInitiateListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (!ResetNetwork.this.runKeyguardConfirmation(55)) {
                    ResetNetwork.this.showFinalConfirmation();
                }
            }
        };
    }
    
    private void establishInitialState() {
        this.mSubscriptionSpinner = (Spinner)this.mContentView.findViewById(2131362522);
        this.mEsimContainer = this.mContentView.findViewById(2131362120);
        this.mEsimCheckbox = (CheckBox)this.mContentView.findViewById(2131362119);
        this.mSubscriptions = (List<SubscriptionInfo>)SubscriptionManager.from((Context)this.getActivity()).getActiveSubscriptionInfoList();
        if (this.mSubscriptions != null && this.mSubscriptions.size() > 0) {
            int n;
            if (!SubscriptionManager.isUsableSubIdValue(n = SubscriptionManager.getDefaultDataSubscriptionId())) {
                n = SubscriptionManager.getDefaultVoiceSubscriptionId();
            }
            int defaultSmsSubscriptionId = n;
            if (!SubscriptionManager.isUsableSubIdValue(n)) {
                defaultSmsSubscriptionId = SubscriptionManager.getDefaultSmsSubscriptionId();
            }
            int defaultSubscriptionId = defaultSmsSubscriptionId;
            if (!SubscriptionManager.isUsableSubIdValue(defaultSmsSubscriptionId)) {
                defaultSubscriptionId = SubscriptionManager.getDefaultSubscriptionId();
            }
            int size = 0;
            this.mSubscriptions.size();
            final ArrayList<String> list = new ArrayList<String>();
            for (final SubscriptionInfo subscriptionInfo : this.mSubscriptions) {
                if (subscriptionInfo.getSubscriptionId() == defaultSubscriptionId) {
                    size = list.size();
                }
                String s;
                if (TextUtils.isEmpty((CharSequence)(s = subscriptionInfo.getDisplayName().toString()))) {
                    s = subscriptionInfo.getNumber();
                }
                String string = s;
                if (TextUtils.isEmpty((CharSequence)s)) {
                    string = subscriptionInfo.getCarrierName().toString();
                }
                String format = string;
                if (TextUtils.isEmpty((CharSequence)string)) {
                    format = String.format("MCC:%s MNC:%s Slot:%s Id:%s", subscriptionInfo.getMcc(), subscriptionInfo.getMnc(), subscriptionInfo.getSimSlotIndex(), subscriptionInfo.getSubscriptionId());
                }
                list.add(format);
            }
            final ArrayAdapter adapter = new ArrayAdapter((Context)this.getActivity(), 17367048, (List)list);
            adapter.setDropDownViewResource(17367049);
            this.mSubscriptionSpinner.setAdapter((SpinnerAdapter)adapter);
            this.mSubscriptionSpinner.setSelection(size);
            if (this.mSubscriptions.size() > 1) {
                this.mSubscriptionSpinner.setVisibility(0);
            }
            else {
                this.mSubscriptionSpinner.setVisibility(4);
            }
        }
        else {
            this.mSubscriptionSpinner.setVisibility(4);
        }
        (this.mInitiateButton = (Button)this.mContentView.findViewById(2131362272)).setOnClickListener(this.mInitiateListener);
        if (this.showEuiccSettings(this.getContext())) {
            this.mEsimContainer.setVisibility(0);
            ((TextView)this.mContentView.findViewById(2131362121)).setText(2131888816);
            this.mEsimContainer.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    ResetNetwork.this.mEsimCheckbox.toggle();
                }
            });
        }
        else {
            this.mEsimCheckbox.setChecked(false);
        }
    }
    
    private boolean runKeyguardConfirmation(final int n) {
        return new ChooseLockSettingsHelper(this.getActivity(), this).launchConfirmationActivity(n, this.getActivity().getResources().getText(2131888823));
    }
    
    private boolean showEuiccSettings(final Context context) {
        final boolean enabled = ((EuiccManager)context.getSystemService("euicc")).isEnabled();
        boolean b = false;
        if (!enabled) {
            return false;
        }
        final ContentResolver contentResolver = context.getContentResolver();
        if (Settings.Global.getInt(contentResolver, "euicc_provisioned", 0) != 0 || Settings.Global.getInt(contentResolver, "development_settings_enabled", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    private void showFinalConfirmation() {
        final Bundle arguments = new Bundle();
        if (this.mSubscriptions != null && this.mSubscriptions.size() > 0) {
            arguments.putInt("subscription", this.mSubscriptions.get(this.mSubscriptionSpinner.getSelectedItemPosition()).getSubscriptionId());
        }
        arguments.putBoolean("erase_esim", this.mEsimCheckbox.isChecked());
        new SubSettingLauncher(this.getContext()).setDestination(ResetNetworkConfirm.class.getName()).setArguments(arguments).setTitle(2131888819).setSourceMetricsCategory(this.getMetricsCategory()).launch();
    }
    
    @Override
    public int getMetricsCategory() {
        return 83;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n != 55) {
            return;
        }
        if (n2 == -1) {
            this.showFinalConfirmation();
        }
        else {
            this.establishInitialState();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131888823);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final UserManager value = UserManager.get((Context)this.getActivity());
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.getActivity(), "no_network_reset", UserHandle.myUserId());
        if (!value.isAdminUser() || RestrictedLockUtils.hasBaseUserRestriction((Context)this.getActivity(), "no_network_reset", UserHandle.myUserId())) {
            return layoutInflater.inflate(2131558614, (ViewGroup)null);
        }
        if (checkIfRestrictionEnforced != null) {
            new ActionDisabledByAdminDialogHelper(this.getActivity()).prepareDialogBuilder("no_network_reset", checkIfRestrictionEnforced).setOnDismissListener((DialogInterface$OnDismissListener)new _$$Lambda$ResetNetwork$sNSFVrhYYO7NxbKY35cdb4I6sYI(this)).show();
            return new View(this.getContext());
        }
        this.mContentView = layoutInflater.inflate(2131558710, (ViewGroup)null);
        this.establishInitialState();
        return this.mContentView;
    }
}
