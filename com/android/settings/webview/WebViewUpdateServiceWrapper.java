package com.android.settings.webview;

import android.widget.Toast;
import android.webkit.WebViewProviderInfo;
import android.content.pm.PackageManager;
import java.util.ArrayList;
import android.content.pm.ApplicationInfo;
import android.webkit.UserPackage;
import java.util.List;
import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import android.webkit.WebViewFactory;
import android.content.pm.PackageInfo;

public class WebViewUpdateServiceWrapper
{
    public PackageInfo getCurrentWebViewPackage() {
        try {
            return WebViewFactory.getUpdateService().getCurrentWebViewPackage();
        }
        catch (RemoteException ex) {
            Log.e("WVUSWrapper", ex.toString());
            return null;
        }
    }
    
    public List<UserPackage> getPackageInfosAllUsers(final Context context, final String s) {
        return (List<UserPackage>)UserPackage.getPackageInfosAllUsers(context, s, 4194304);
    }
    
    public List<ApplicationInfo> getValidWebViewApplicationInfos(final Context context) {
        WebViewProviderInfo[] validWebViewPackages = null;
        try {
            validWebViewPackages = WebViewFactory.getUpdateService().getValidWebViewPackages();
        }
        catch (RemoteException ex) {}
        final ArrayList<ApplicationInfo> list = new ArrayList<ApplicationInfo>();
        for (final WebViewProviderInfo webViewProviderInfo : validWebViewPackages) {
            try {
                list.add(context.getPackageManager().getApplicationInfo(webViewProviderInfo.packageName, 4194304));
            }
            catch (PackageManager$NameNotFoundException ex2) {}
        }
        return list;
    }
    
    public boolean setWebViewProvider(final String s) {
        try {
            return s.equals(WebViewFactory.getUpdateService().changeProviderAndSetting(s));
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("RemoteException when trying to change provider to ");
            sb.append(s);
            Log.e("WVUSWrapper", sb.toString(), (Throwable)ex);
            return false;
        }
    }
    
    public void showInvalidChoiceToast(final Context context) {
        Toast.makeText(context, 2131889037, 0).show();
    }
}
