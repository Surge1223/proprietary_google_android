package com.android.settings.webview;

import android.content.pm.PackageManager;
import android.content.Intent;
import android.app.Activity;
import android.webkit.UserPackage;
import android.content.pm.PackageInfo;
import java.util.Iterator;
import android.content.pm.ApplicationInfo;
import java.util.ArrayList;
import java.util.List;
import android.text.TextUtils;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;
import com.android.settings.applications.defaultapps.DefaultAppPickerFragment;

public class WebViewAppPicker extends DefaultAppPickerFragment
{
    private WebViewUpdateServiceWrapper mWebViewUpdateServiceWrapper;
    
    private WebViewUpdateServiceWrapper createDefaultWebViewUpdateServiceWrapper() {
        return new WebViewUpdateServiceWrapper();
    }
    
    private WebViewUpdateServiceWrapper getWebViewUpdateServiceWrapper() {
        if (this.mWebViewUpdateServiceWrapper == null) {
            this.setWebViewUpdateServiceWrapper(this.createDefaultWebViewUpdateServiceWrapper());
        }
        return this.mWebViewUpdateServiceWrapper;
    }
    
    DefaultAppInfo createDefaultAppInfo(final Context context, final PackageManagerWrapper packageManagerWrapper, final PackageItemInfo packageItemInfo, final String s) {
        return new WebViewAppInfo(context, packageManagerWrapper, packageItemInfo, s, TextUtils.isEmpty((CharSequence)s));
    }
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final ArrayList<DefaultAppInfo> list = new ArrayList<DefaultAppInfo>();
        final Context context = this.getContext();
        final WebViewUpdateServiceWrapper webViewUpdateServiceWrapper = this.getWebViewUpdateServiceWrapper();
        for (final ApplicationInfo applicationInfo : webViewUpdateServiceWrapper.getValidWebViewApplicationInfos(context)) {
            list.add(this.createDefaultAppInfo(context, this.mPm, (PackageItemInfo)applicationInfo, this.getDisabledReason(webViewUpdateServiceWrapper, context, applicationInfo.packageName)));
        }
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        final PackageInfo currentWebViewPackage = this.getWebViewUpdateServiceWrapper().getCurrentWebViewPackage();
        String packageName;
        if (currentWebViewPackage == null) {
            packageName = null;
        }
        else {
            packageName = currentWebViewPackage.packageName;
        }
        return packageName;
    }
    
    String getDisabledReason(final WebViewUpdateServiceWrapper webViewUpdateServiceWrapper, final Context context, final String s) {
        for (final UserPackage userPackage : webViewUpdateServiceWrapper.getPackageInfosAllUsers(context, s)) {
            if (!userPackage.isInstalledPackage()) {
                return context.getString(2131889884, new Object[] { userPackage.getUserInfo().name });
            }
            if (!userPackage.isEnabledPackage()) {
                return context.getString(2131889882, new Object[] { userPackage.getUserInfo().name });
            }
        }
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 405;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082863;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (!this.mUserManager.isAdminUser()) {
            this.getActivity().finish();
        }
    }
    
    @Override
    protected void onSelectionPerformed(final boolean b) {
        if (b) {
            final Activity activity = this.getActivity();
            Intent intent;
            if (activity == null) {
                intent = null;
            }
            else {
                intent = activity.getIntent();
            }
            if (intent != null && "android.settings.WEBVIEW_SETTINGS".equals(intent.getAction())) {
                this.getActivity().finish();
            }
        }
        else {
            this.getWebViewUpdateServiceWrapper().showInvalidChoiceToast((Context)this.getActivity());
            this.updateCandidates();
        }
    }
    
    @Override
    protected boolean setDefaultKey(final String webViewProvider) {
        return this.getWebViewUpdateServiceWrapper().setWebViewProvider(webViewProvider);
    }
    
    void setWebViewUpdateServiceWrapper(final WebViewUpdateServiceWrapper mWebViewUpdateServiceWrapper) {
        this.mWebViewUpdateServiceWrapper = mWebViewUpdateServiceWrapper;
    }
    
    private static class WebViewAppInfo extends DefaultAppInfo
    {
        public WebViewAppInfo(final Context context, final PackageManagerWrapper packageManagerWrapper, final PackageItemInfo packageItemInfo, final String s, final boolean b) {
            super(context, packageManagerWrapper, packageItemInfo, s, b);
        }
        
        @Override
        public CharSequence loadLabel() {
            String versionName = "";
            try {
                versionName = this.mPm.getPackageManager().getPackageInfo(this.packageItemInfo.packageName, 0).versionName;
            }
            catch (PackageManager$NameNotFoundException ex) {}
            return String.format("%s %s", super.loadLabel(), versionName);
        }
    }
}
