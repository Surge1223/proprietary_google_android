package com.android.settings;

import android.text.TextUtils;
import android.content.res.Resources$NotFoundException;
import android.text.TextUtils$TruncateAt;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import java.util.ArrayList;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.RemoteCallback;
import android.os.RemoteCallback$OnResultListener;
import com.android.settings.users.UserDialogs;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Process;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.view.Display;
import android.view.WindowManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.app.ActivityManager;
import android.util.Log;
import com.android.settings.fuelgauge.BatteryUtils;
import android.util.EventLog;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import java.util.List;
import java.util.function.Predicate;
import android.content.ComponentName;
import java.util.Optional;
import java.util.Iterator;
import android.widget.AppSecurityPermissions;
import android.app.admin.DeviceAdminInfo$PolicyInfo;
import android.content.Context;
import android.os.UserManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Handler;
import android.app.admin.DeviceAdminInfo;
import android.app.admin.DevicePolicyManager;
import android.app.AppOpsManager;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.app.Activity;

public class DeviceAdminAdd extends Activity
{
    Button mActionButton;
    TextView mAddMsg;
    boolean mAddMsgEllipsized;
    ImageView mAddMsgExpander;
    CharSequence mAddMsgText;
    boolean mAdding;
    boolean mAddingProfileOwner;
    TextView mAdminDescription;
    ImageView mAdminIcon;
    TextView mAdminName;
    ViewGroup mAdminPolicies;
    boolean mAdminPoliciesInitialized;
    TextView mAdminWarning;
    AppOpsManager mAppOps;
    Button mCancelButton;
    DevicePolicyManager mDPM;
    DeviceAdminInfo mDeviceAdmin;
    Handler mHandler;
    boolean mIsCalledFromSupportDialog;
    String mProfileOwnerName;
    TextView mProfileOwnerWarning;
    boolean mRefreshing;
    TextView mSupportMessage;
    private final IBinder mToken;
    Button mUninstallButton;
    boolean mUninstalling;
    boolean mWaitingForRemoveMsg;
    
    public DeviceAdminAdd() {
        this.mToken = (IBinder)new Binder();
        this.mAddMsgEllipsized = true;
        this.mUninstalling = false;
        this.mIsCalledFromSupportDialog = false;
    }
    
    private void addDeviceAdminPolicies(final boolean b) {
        if (!this.mAdminPoliciesInitialized) {
            final boolean adminUser = UserManager.get((Context)this).isAdminUser();
            for (final DeviceAdminInfo$PolicyInfo deviceAdminInfo$PolicyInfo : this.mDeviceAdmin.getUsedPolicies()) {
                int n;
                if (adminUser) {
                    n = deviceAdminInfo$PolicyInfo.description;
                }
                else {
                    n = deviceAdminInfo$PolicyInfo.descriptionForSecondaryUsers;
                }
                int n2;
                if (adminUser) {
                    n2 = deviceAdminInfo$PolicyInfo.label;
                }
                else {
                    n2 = deviceAdminInfo$PolicyInfo.labelForSecondaryUsers;
                }
                final CharSequence text = this.getText(n2);
                CharSequence text2;
                if (b) {
                    text2 = this.getText(n);
                }
                else {
                    text2 = "";
                }
                this.mAdminPolicies.addView(AppSecurityPermissions.getPermissionItemView((Context)this, text, text2, true));
            }
            this.mAdminPoliciesInitialized = true;
        }
    }
    
    private Optional<ComponentName> findAdminWithPackageName(final String s) {
        final List activeAdmins = this.mDPM.getActiveAdmins();
        if (activeAdmins == null) {
            return Optional.empty();
        }
        return activeAdmins.stream().filter(new _$$Lambda$DeviceAdminAdd$3kbf0VppdPbIFmWVVpDZ5dj27E4(s)).findAny();
    }
    
    private RestrictedLockUtils.EnforcedAdmin getAdminEnforcingCantRemoveProfile() {
        return RestrictedLockUtils.checkIfRestrictionEnforced((Context)this, "no_remove_managed_profile", this.getParentUserId());
    }
    
    private int getParentUserId() {
        return UserManager.get((Context)this).getProfileParent(UserHandle.myUserId()).id;
    }
    
    private boolean hasBaseCantRemoveProfileRestriction() {
        return RestrictedLockUtils.hasBaseUserRestriction((Context)this, "no_remove_managed_profile", this.getParentUserId());
    }
    
    private boolean isAdminUninstallable() {
        return this.mDeviceAdmin.getActivityInfo().applicationInfo.isSystemApp() ^ true;
    }
    
    private boolean isManagedProfile(final DeviceAdminInfo deviceAdminInfo) {
        final UserInfo userInfo = UserManager.get((Context)this).getUserInfo(UserHandle.getUserId(deviceAdminInfo.getActivityInfo().applicationInfo.uid));
        return userInfo != null && userInfo.isManagedProfile();
    }
    
    private void showPolicyTransparencyDialogIfRequired() {
        if (this.isManagedProfile(this.mDeviceAdmin) && this.mDeviceAdmin.getComponent().equals((Object)this.mDPM.getProfileOwner())) {
            if (this.hasBaseCantRemoveProfileRestriction()) {
                return;
            }
            final RestrictedLockUtils.EnforcedAdmin adminEnforcingCantRemoveProfile = this.getAdminEnforcingCantRemoveProfile();
            if (adminEnforcingCantRemoveProfile != null) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this, adminEnforcingCantRemoveProfile);
            }
        }
    }
    
    void addAndFinish() {
        try {
            this.logSpecialPermissionChange(true, this.mDeviceAdmin.getComponent().getPackageName());
            this.mDPM.setActiveAdmin(this.mDeviceAdmin.getComponent(), this.mRefreshing);
            EventLog.writeEvent(90201, this.mDeviceAdmin.getActivityInfo().applicationInfo.uid);
            this.unrestrictAppIfPossible(BatteryUtils.getInstance((Context)this));
            this.setResult(-1);
        }
        catch (RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception trying to activate admin ");
            sb.append(this.mDeviceAdmin.getComponent());
            Log.w("DeviceAdminAdd", sb.toString(), (Throwable)ex);
            if (this.mDPM.isAdminActive(this.mDeviceAdmin.getComponent())) {
                this.setResult(-1);
            }
        }
        if (this.mAddingProfileOwner) {
            try {
                this.mDPM.setProfileOwner(this.mDeviceAdmin.getComponent(), this.mProfileOwnerName, UserHandle.myUserId());
            }
            catch (RuntimeException ex2) {
                this.setResult(0);
            }
        }
        this.finish();
    }
    
    void continueRemoveAction(final CharSequence charSequence) {
        if (!this.mWaitingForRemoveMsg) {
            return;
        }
        this.mWaitingForRemoveMsg = false;
        if (charSequence == null) {
            try {
                ActivityManager.getService().resumeAppSwitches();
            }
            catch (RemoteException ex) {}
            this.logSpecialPermissionChange(false, this.mDeviceAdmin.getComponent().getPackageName());
            this.mDPM.removeActiveAdmin(this.mDeviceAdmin.getComponent());
            this.finish();
        }
        else {
            try {
                ActivityManager.getService().stopAppSwitches();
            }
            catch (RemoteException ex2) {}
            final Bundle bundle = new Bundle();
            bundle.putCharSequence("android.app.extra.DISABLE_WARNING", charSequence);
            this.showDialog(1, bundle);
        }
    }
    
    int getEllipsizedLines() {
        final Display defaultDisplay = ((WindowManager)this.getSystemService("window")).getDefaultDisplay();
        int n;
        if (defaultDisplay.getHeight() > defaultDisplay.getWidth()) {
            n = 5;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 766;
        }
        else {
            n = 767;
        }
        FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider().action((Context)this, n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    protected void onCreate(Bundle viewById) {
        super.onCreate(viewById);
        this.mHandler = new Handler(this.getMainLooper());
        this.mDPM = (DevicePolicyManager)this.getSystemService("device_policy");
        this.mAppOps = (AppOpsManager)this.getSystemService("appops");
        final PackageManager packageManager = this.getPackageManager();
        if ((this.getIntent().getFlags() & 0x10000000) != 0x0) {
            Log.w("DeviceAdminAdd", "Cannot start ADD_DEVICE_ADMIN as a new task");
            this.finish();
            return;
        }
        final Intent intent = this.getIntent();
        final int n = 0;
        this.mIsCalledFromSupportDialog = intent.getBooleanExtra("android.app.extra.CALLED_FROM_SUPPORT_DIALOG", false);
        final String action = this.getIntent().getAction();
        if ((viewById = (Bundle)this.getIntent().getParcelableExtra("android.app.extra.DEVICE_ADMIN")) == null) {
            final Optional<ComponentName> adminWithPackageName = this.findAdminWithPackageName(this.getIntent().getStringExtra("android.app.extra.DEVICE_ADMIN_PACKAGE_NAME"));
            if (!adminWithPackageName.isPresent()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("No component specified in ");
                sb.append(action);
                Log.w("DeviceAdminAdd", sb.toString());
                this.finish();
                return;
            }
            viewById = (Bundle)adminWithPackageName.get();
            this.mUninstalling = true;
        }
        Label_0369: {
            if (action != null && action.equals("android.app.action.SET_PROFILE_OWNER")) {
                this.setResult(0);
                this.setFinishOnTouchOutside(true);
                this.mAddingProfileOwner = true;
                this.mProfileOwnerName = this.getIntent().getStringExtra("android.app.extra.PROFILE_OWNER_NAME");
                final String callingPackage = this.getCallingPackage();
                if (callingPackage != null) {
                    if (callingPackage.equals(((ComponentName)viewById).getPackageName())) {
                        try {
                            if ((packageManager.getPackageInfo(callingPackage, 0).applicationInfo.flags & 0x1) == 0x0) {
                                Log.e("DeviceAdminAdd", "Cannot set a non-system app as a profile owner");
                                this.finish();
                                return;
                            }
                            break Label_0369;
                        }
                        catch (PackageManager$NameNotFoundException ex6) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Cannot find the package ");
                            sb2.append(callingPackage);
                            Log.e("DeviceAdminAdd", sb2.toString());
                            this.finish();
                            return;
                        }
                    }
                }
                Log.e("DeviceAdminAdd", "Unknown or incorrect caller");
                this.finish();
                return;
            }
            try {
                final ActivityInfo receiverInfo = packageManager.getReceiverInfo((ComponentName)viewById, 128);
                if (!this.mDPM.isAdminActive((ComponentName)viewById)) {
                    final List queryBroadcastReceivers = packageManager.queryBroadcastReceivers(new Intent("android.app.action.DEVICE_ADMIN_ENABLED"), 32768);
                    int size;
                    if (queryBroadcastReceivers == null) {
                        size = 0;
                    }
                    else {
                        size = queryBroadcastReceivers.size();
                    }
                    final boolean b = false;
                    final boolean b2 = false;
                    int n2 = 0;
                    boolean b3;
                    while (true) {
                        b3 = b;
                        if (n2 >= size) {
                            break;
                        }
                        final ResolveInfo resolveInfo = queryBroadcastReceivers.get(n2);
                        if (receiverInfo.packageName.equals(resolveInfo.activityInfo.packageName) && receiverInfo.name.equals(resolveInfo.activityInfo.name)) {
                            while (true) {
                                try {
                                    resolveInfo.activityInfo = receiverInfo;
                                    new DeviceAdminInfo((Context)this, resolveInfo);
                                    final boolean b4 = true;
                                    b3 = b4;
                                    break;
                                }
                                catch (IOException ex) {
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Bad ");
                                    sb3.append(resolveInfo.activityInfo);
                                    Log.w("DeviceAdminAdd", sb3.toString(), (Throwable)ex);
                                    b3 = b;
                                    break;
                                }
                                catch (XmlPullParserException ex2) {
                                    final StringBuilder sb4 = new StringBuilder();
                                    sb4.append("Bad ");
                                    sb4.append(resolveInfo.activityInfo);
                                    Log.w("DeviceAdminAdd", sb4.toString(), (Throwable)ex2);
                                    final boolean b4 = b2;
                                    continue;
                                }
                                break;
                            }
                        }
                        ++n2;
                    }
                    if (!b3) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("Request to add invalid device admin: ");
                        sb5.append(viewById);
                        Log.w("DeviceAdminAdd", sb5.toString());
                        this.finish();
                        return;
                    }
                }
                final ResolveInfo resolveInfo2 = new ResolveInfo();
                resolveInfo2.activityInfo = receiverInfo;
                try {
                    this.mDeviceAdmin = new DeviceAdminInfo((Context)this, resolveInfo2);
                    if ("android.app.action.ADD_DEVICE_ADMIN".equals(this.getIntent().getAction())) {
                        this.mRefreshing = false;
                        if (this.mDPM.isAdminActive((ComponentName)viewById)) {
                            if (this.mDPM.isRemovingAdmin((ComponentName)viewById, Process.myUserHandle().getIdentifier())) {
                                final StringBuilder sb6 = new StringBuilder();
                                sb6.append("Requested admin is already being removed: ");
                                sb6.append(viewById);
                                Log.w("DeviceAdminAdd", sb6.toString());
                                this.finish();
                                return;
                            }
                            final ArrayList usedPolicies = this.mDeviceAdmin.getUsedPolicies();
                            for (int i = n; i < usedPolicies.size(); ++i) {
                                if (!this.mDPM.hasGrantedPolicy((ComponentName)viewById, usedPolicies.get(i).ident)) {
                                    this.mRefreshing = true;
                                    break;
                                }
                            }
                            if (!this.mRefreshing) {
                                this.setResult(-1);
                                this.finish();
                                return;
                            }
                        }
                    }
                    if (this.mAddingProfileOwner && !this.mDPM.hasUserSetupCompleted()) {
                        this.addAndFinish();
                        return;
                    }
                    this.mAddMsgText = this.getIntent().getCharSequenceExtra("android.app.extra.ADD_EXPLANATION");
                    this.setContentView(2131558536);
                    this.mAdminIcon = (ImageView)this.findViewById(2131361847);
                    this.mAdminName = (TextView)this.findViewById(2131361849);
                    this.mAdminDescription = (TextView)this.findViewById(2131361845);
                    this.mProfileOwnerWarning = (TextView)this.findViewById(2131362479);
                    this.mAddMsg = (TextView)this.findViewById(2131361838);
                    this.mAddMsgExpander = (ImageView)this.findViewById(2131361839);
                    viewById = (Bundle)new View.OnClickListener() {
                        public void onClick(final View view) {
                            DeviceAdminAdd.this.toggleMessageEllipsis((View)DeviceAdminAdd.this.mAddMsg);
                        }
                    };
                    this.mAddMsgExpander.setOnClickListener((View.OnClickListener)viewById);
                    this.mAddMsg.setOnClickListener((View.OnClickListener)viewById);
                    this.mAddMsg.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            final int ellipsizedLines = DeviceAdminAdd.this.getEllipsizedLines();
                            final int lineCount = DeviceAdminAdd.this.mAddMsg.getLineCount();
                            int visibility = 0;
                            final boolean b = lineCount <= ellipsizedLines;
                            final ImageView mAddMsgExpander = DeviceAdminAdd.this.mAddMsgExpander;
                            if (b) {
                                visibility = 8;
                            }
                            mAddMsgExpander.setVisibility(visibility);
                            if (b) {
                                DeviceAdminAdd.this.mAddMsg.setOnClickListener((View.OnClickListener)null);
                                ((View)DeviceAdminAdd.this.mAddMsgExpander.getParent()).invalidate();
                            }
                            DeviceAdminAdd.this.mAddMsg.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
                        }
                    });
                    this.toggleMessageEllipsis((View)this.mAddMsg);
                    this.mAdminWarning = (TextView)this.findViewById(2131361855);
                    this.mAdminPolicies = (ViewGroup)this.findViewById(2131361850);
                    this.mSupportMessage = (TextView)this.findViewById(2131361853);
                    (this.mCancelButton = (Button)this.findViewById(2131361962)).setFilterTouchesWhenObscured(true);
                    this.mCancelButton.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                        public void onClick(final View view) {
                            EventLog.writeEvent(90202, DeviceAdminAdd.this.mDeviceAdmin.getActivityInfo().applicationInfo.uid);
                            DeviceAdminAdd.this.finish();
                        }
                    });
                    (this.mUninstallButton = (Button)this.findViewById(2131362767)).setFilterTouchesWhenObscured(true);
                    this.mUninstallButton.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                        public void onClick(final View view) {
                            EventLog.writeEvent(90203, DeviceAdminAdd.this.mDeviceAdmin.getActivityInfo().applicationInfo.uid);
                            DeviceAdminAdd.this.mDPM.uninstallPackageWithActiveAdmins(DeviceAdminAdd.this.mDeviceAdmin.getPackageName());
                            DeviceAdminAdd.this.finish();
                        }
                    });
                    this.mActionButton = (Button)this.findViewById(2131361814);
                    viewById = (Bundle)this.findViewById(2131362524);
                    ((View)viewById).setFilterTouchesWhenObscured(true);
                    ((View)viewById).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                        public void onClick(final View view) {
                            if (!DeviceAdminAdd.this.mActionButton.isEnabled()) {
                                DeviceAdminAdd.this.showPolicyTransparencyDialogIfRequired();
                                return;
                            }
                            if (DeviceAdminAdd.this.mAdding) {
                                DeviceAdminAdd.this.addAndFinish();
                            }
                            else if (DeviceAdminAdd.this.isManagedProfile(DeviceAdminAdd.this.mDeviceAdmin) && DeviceAdminAdd.this.mDeviceAdmin.getComponent().equals((Object)DeviceAdminAdd.this.mDPM.getProfileOwner())) {
                                final int myUserId = UserHandle.myUserId();
                                UserDialogs.createRemoveDialog((Context)DeviceAdminAdd.this, myUserId, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                                    public void onClick(final DialogInterface dialogInterface, final int n) {
                                        UserManager.get((Context)DeviceAdminAdd.this).removeUser(myUserId);
                                        DeviceAdminAdd.this.finish();
                                    }
                                }).show();
                            }
                            else if (DeviceAdminAdd.this.mUninstalling) {
                                DeviceAdminAdd.this.mDPM.uninstallPackageWithActiveAdmins(DeviceAdminAdd.this.mDeviceAdmin.getPackageName());
                                DeviceAdminAdd.this.finish();
                            }
                            else if (!DeviceAdminAdd.this.mWaitingForRemoveMsg) {
                                try {
                                    ActivityManager.getService().stopAppSwitches();
                                }
                                catch (RemoteException ex) {}
                                DeviceAdminAdd.this.mWaitingForRemoveMsg = true;
                                DeviceAdminAdd.this.mDPM.getRemoveWarning(DeviceAdminAdd.this.mDeviceAdmin.getComponent(), new RemoteCallback((RemoteCallback$OnResultListener)new RemoteCallback$OnResultListener() {
                                    public void onResult(final Bundle bundle) {
                                        CharSequence charSequence;
                                        if (bundle != null) {
                                            charSequence = bundle.getCharSequence("android.app.extra.DISABLE_WARNING");
                                        }
                                        else {
                                            charSequence = null;
                                        }
                                        DeviceAdminAdd.this.continueRemoveAction(charSequence);
                                    }
                                }, DeviceAdminAdd.this.mHandler));
                                DeviceAdminAdd.this.getWindow().getDecorView().getHandler().postDelayed((Runnable)new Runnable() {
                                    @Override
                                    public void run() {
                                        DeviceAdminAdd.this.continueRemoveAction(null);
                                    }
                                }, 2000L);
                            }
                        }
                    });
                }
                catch (IOException ex3) {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append("Unable to retrieve device policy ");
                    sb7.append(viewById);
                    Log.w("DeviceAdminAdd", sb7.toString(), (Throwable)ex3);
                    this.finish();
                }
                catch (XmlPullParserException ex4) {
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append("Unable to retrieve device policy ");
                    sb8.append(viewById);
                    Log.w("DeviceAdminAdd", sb8.toString(), (Throwable)ex4);
                    this.finish();
                }
            }
            catch (PackageManager$NameNotFoundException ex5) {
                final StringBuilder sb9 = new StringBuilder();
                sb9.append("Unable to retrieve device policy ");
                sb9.append(viewById);
                Log.w("DeviceAdminAdd", sb9.toString(), (Throwable)ex5);
                this.finish();
            }
        }
    }
    
    protected Dialog onCreateDialog(final int n, final Bundle bundle) {
        if (n != 1) {
            return super.onCreateDialog(n, bundle);
        }
        final CharSequence charSequence = bundle.getCharSequence("android.app.extra.DISABLE_WARNING");
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this);
        alertDialog$Builder.setMessage(charSequence);
        alertDialog$Builder.setPositiveButton(2131887460, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                try {
                    ActivityManager.getService().resumeAppSwitches();
                }
                catch (RemoteException ex) {}
                DeviceAdminAdd.this.mDPM.removeActiveAdmin(DeviceAdminAdd.this.mDeviceAdmin.getComponent());
                DeviceAdminAdd.this.finish();
            }
        });
        alertDialog$Builder.setNegativeButton(2131887454, (DialogInterface$OnClickListener)null);
        return (Dialog)alertDialog$Builder.create();
    }
    
    protected void onPause() {
        super.onPause();
        this.mActionButton.setEnabled(false);
        this.mAppOps.setUserRestriction(24, false, this.mToken);
        this.mAppOps.setUserRestriction(45, false, this.mToken);
        try {
            ActivityManager.getService().resumeAppSwitches();
        }
        catch (RemoteException ex) {}
    }
    
    protected void onResume() {
        super.onResume();
        this.mActionButton.setEnabled(true);
        this.updateInterface();
        this.mAppOps.setUserRestriction(24, true, this.mToken);
        this.mAppOps.setUserRestriction(45, true, this.mToken);
    }
    
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        if (this.mIsCalledFromSupportDialog) {
            this.finish();
        }
    }
    
    void toggleMessageEllipsis(final View view) {
        final TextView textView = (TextView)view;
        this.mAddMsgEllipsized ^= true;
        TextUtils$TruncateAt end;
        if (this.mAddMsgEllipsized) {
            end = TextUtils$TruncateAt.END;
        }
        else {
            end = null;
        }
        textView.setEllipsize(end);
        int ellipsizedLines;
        if (this.mAddMsgEllipsized) {
            ellipsizedLines = this.getEllipsizedLines();
        }
        else {
            ellipsizedLines = 15;
        }
        textView.setMaxLines(ellipsizedLines);
        final ImageView mAddMsgExpander = this.mAddMsgExpander;
        int imageResource;
        if (this.mAddMsgEllipsized) {
            imageResource = 17302205;
        }
        else {
            imageResource = 17302204;
        }
        mAddMsgExpander.setImageResource(imageResource);
    }
    
    void unrestrictAppIfPossible(final BatteryUtils batteryUtils) {
        final String packageName = this.mDeviceAdmin.getComponent().getPackageName();
        final int packageUid = batteryUtils.getPackageUid(packageName);
        if (batteryUtils.isForceAppStandbyEnabled(packageUid, packageName)) {
            batteryUtils.setForceAppStandby(packageUid, packageName, 0);
        }
    }
    
    void updateInterface() {
        this.findViewById(R.id.restricted_icon).setVisibility(8);
        this.mAdminIcon.setImageDrawable(this.mDeviceAdmin.loadIcon(this.getPackageManager()));
        this.mAdminName.setText(this.mDeviceAdmin.loadLabel(this.getPackageManager()));
        try {
            this.mAdminDescription.setText(this.mDeviceAdmin.loadDescription(this.getPackageManager()));
            this.mAdminDescription.setVisibility(0);
        }
        catch (Resources$NotFoundException ex) {
            this.mAdminDescription.setVisibility(8);
        }
        if (this.mAddingProfileOwner) {
            this.mProfileOwnerWarning.setVisibility(0);
        }
        if (this.mAddMsgText != null) {
            this.mAddMsg.setText(this.mAddMsgText);
            this.mAddMsg.setVisibility(0);
        }
        else {
            this.mAddMsg.setVisibility(8);
            this.mAddMsgExpander.setVisibility(8);
        }
        final boolean mRefreshing = this.mRefreshing;
        boolean enabled = true;
        if (!mRefreshing && !this.mAddingProfileOwner && this.mDPM.isAdminActive(this.mDeviceAdmin.getComponent())) {
            this.mAdding = false;
            final boolean equals = this.mDeviceAdmin.getComponent().equals((Object)this.mDPM.getProfileOwner());
            final boolean managedProfile = this.isManagedProfile(this.mDeviceAdmin);
            if (equals && managedProfile) {
                this.mAdminWarning.setText(2131886268);
                this.mActionButton.setText(2131888805);
                final RestrictedLockUtils.EnforcedAdmin adminEnforcingCantRemoveProfile = this.getAdminEnforcingCantRemoveProfile();
                final boolean hasBaseCantRemoveProfileRestriction = this.hasBaseCantRemoveProfileRestriction();
                if (adminEnforcingCantRemoveProfile != null && !hasBaseCantRemoveProfileRestriction) {
                    this.findViewById(R.id.restricted_icon).setVisibility(0);
                }
                final Button mActionButton = this.mActionButton;
                if (adminEnforcingCantRemoveProfile != null || hasBaseCantRemoveProfileRestriction) {
                    enabled = false;
                }
                mActionButton.setEnabled(enabled);
            }
            else if (!equals && !this.mDeviceAdmin.getComponent().equals((Object)this.mDPM.getDeviceOwnerComponentOnCallingUser())) {
                this.addDeviceAdminPolicies(false);
                this.mAdminWarning.setText((CharSequence)this.getString(2131887384, new Object[] { this.mDeviceAdmin.getActivityInfo().applicationInfo.loadLabel(this.getPackageManager()) }));
                this.setTitle(2131886245);
                if (this.mUninstalling) {
                    this.mActionButton.setText(2131888803);
                }
                else {
                    this.mActionButton.setText(2131888804);
                }
            }
            else {
                if (equals) {
                    this.mAdminWarning.setText(2131886269);
                }
                else {
                    this.mAdminWarning.setText(2131886265);
                }
                this.mActionButton.setText(2131888804);
                this.mActionButton.setEnabled(false);
            }
            final CharSequence longSupportMessageForUser = this.mDPM.getLongSupportMessageForUser(this.mDeviceAdmin.getComponent(), UserHandle.myUserId());
            if (!TextUtils.isEmpty(longSupportMessageForUser)) {
                this.mSupportMessage.setText(longSupportMessageForUser);
                this.mSupportMessage.setVisibility(0);
            }
            else {
                this.mSupportMessage.setVisibility(8);
            }
        }
        else {
            this.addDeviceAdminPolicies(true);
            this.mAdminWarning.setText((CharSequence)this.getString(2131887385, new Object[] { this.mDeviceAdmin.getActivityInfo().applicationInfo.loadLabel(this.getPackageManager()) }));
            if (this.mAddingProfileOwner) {
                this.setTitle(this.getText(2131888698));
            }
            else {
                this.setTitle(this.getText(2131886256));
            }
            this.mActionButton.setText(this.getText(2131886255));
            if (this.isAdminUninstallable()) {
                this.mUninstallButton.setVisibility(0);
            }
            this.mSupportMessage.setVisibility(8);
            this.mAdding = true;
        }
    }
}
