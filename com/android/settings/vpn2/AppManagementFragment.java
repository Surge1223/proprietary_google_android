package com.android.settings.vpn2;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import com.android.settings.core.SubSettingLauncher;
import android.app.Fragment;
import android.os.Bundle;
import android.content.pm.PackageManager;
import com.android.internal.net.VpnConfig;
import android.os.RemoteException;
import android.util.Log;
import android.text.TextUtils;
import com.android.internal.annotations.VisibleForTesting;
import java.util.Collection;
import com.android.internal.util.ArrayUtils;
import android.app.AppOpsManager;
import android.content.pm.ApplicationInfo;
import android.content.Context;
import android.os.UserHandle;
import com.android.settingslib.RestrictedPreference;
import com.android.settingslib.RestrictedSwitchPreference;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.net.IConnectivityManager;
import android.net.ConnectivityManager;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class AppManagementFragment extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnPreferenceClickListener, ConfirmLockdownListener
{
    private ConnectivityManager mConnectivityManager;
    private IConnectivityManager mConnectivityService;
    private final AppDialogFragment.Listener mForgetVpnDialogFragmentListener;
    private PackageInfo mPackageInfo;
    private PackageManager mPackageManager;
    private String mPackageName;
    private RestrictedSwitchPreference mPreferenceAlwaysOn;
    private RestrictedPreference mPreferenceForget;
    private RestrictedSwitchPreference mPreferenceLockdown;
    private Preference mPreferenceVersion;
    private final int mUserId;
    private String mVpnLabel;
    
    public AppManagementFragment() {
        this.mUserId = UserHandle.myUserId();
        this.mForgetVpnDialogFragmentListener = new AppDialogFragment.Listener() {
            @Override
            public void onCancel() {
            }
            
            @Override
            public void onForget() {
                if (AppManagementFragment.this.isVpnAlwaysOn()) {
                    AppManagementFragment.this.setAlwaysOnVpn(false, false);
                }
                AppManagementFragment.this.finish();
            }
        };
    }
    
    @VisibleForTesting
    static boolean appHasVpnPermission(final Context context, final ApplicationInfo applicationInfo) {
        return ArrayUtils.isEmpty((Collection)((AppOpsManager)context.getSystemService("appops")).getOpsForPackage(applicationInfo.uid, applicationInfo.packageName, new int[] { 47 })) ^ true;
    }
    
    private String getAlwaysOnVpnPackage() {
        return this.mConnectivityManager.getAlwaysOnVpnPackageForUser(this.mUserId);
    }
    
    private boolean isAnotherVpnActive() {
        final boolean b = false;
        try {
            final VpnConfig vpnConfig = this.mConnectivityService.getVpnConfig(this.mUserId);
            boolean b2 = b;
            if (vpnConfig != null) {
                final boolean equals = TextUtils.equals((CharSequence)vpnConfig.user, (CharSequence)this.mPackageName);
                b2 = b;
                if (!equals) {
                    b2 = true;
                }
            }
            return b2;
        }
        catch (RemoteException ex) {
            Log.w("AppManagementFragment", "Failure to look up active VPN", (Throwable)ex);
            return false;
        }
    }
    
    private boolean isVpnAlwaysOn() {
        return this.mPackageName.equals(this.getAlwaysOnVpnPackage());
    }
    
    private boolean loadInfo() {
        final Bundle arguments = this.getArguments();
        if (arguments == null) {
            Log.e("AppManagementFragment", "empty bundle");
            return false;
        }
        this.mPackageName = arguments.getString("package");
        if (this.mPackageName == null) {
            Log.e("AppManagementFragment", "empty package name");
            return false;
        }
        try {
            this.mPackageInfo = this.mPackageManager.getPackageInfo(this.mPackageName, 0);
            this.mVpnLabel = VpnConfig.getVpnLabel(this.getPrefContext(), this.mPackageName).toString();
            if (this.mPackageInfo.applicationInfo == null) {
                Log.e("AppManagementFragment", "package does not include an application");
                return false;
            }
            if (!appHasVpnPermission(this.getContext(), this.mPackageInfo.applicationInfo)) {
                Log.e("AppManagementFragment", "package didn't register VPN profile");
                return false;
            }
            return true;
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e("AppManagementFragment", "package not found", (Throwable)ex);
            return false;
        }
    }
    
    private boolean onAlwaysOnVpnClick(final boolean b, final boolean b2) {
        final boolean anotherVpnActive = this.isAnotherVpnActive();
        final boolean anyLockdownActive = VpnUtils.isAnyLockdownActive((Context)this.getActivity());
        if (ConfirmLockdownFragment.shouldShow(anotherVpnActive, anyLockdownActive, b2)) {
            ConfirmLockdownFragment.show(this, anotherVpnActive, b, anyLockdownActive, b2, null);
            return false;
        }
        return this.setAlwaysOnVpnByUI(b, b2);
    }
    
    private boolean onForgetVpnClick() {
        this.updateRestrictedViews();
        if (!this.mPreferenceForget.isEnabled()) {
            return false;
        }
        AppDialogFragment.show(this, this.mForgetVpnDialogFragmentListener, this.mPackageInfo, this.mVpnLabel, true, true);
        return true;
    }
    
    private boolean setAlwaysOnVpn(final boolean b, final boolean b2) {
        final ConnectivityManager mConnectivityManager = this.mConnectivityManager;
        final int mUserId = this.mUserId;
        String mPackageName;
        if (b) {
            mPackageName = this.mPackageName;
        }
        else {
            mPackageName = null;
        }
        return mConnectivityManager.setAlwaysOnVpnPackageForUser(mUserId, mPackageName, b2);
    }
    
    private boolean setAlwaysOnVpnByUI(final boolean b, final boolean b2) {
        this.updateRestrictedViews();
        if (!this.mPreferenceAlwaysOn.isEnabled()) {
            return false;
        }
        if (this.mUserId == 0) {
            VpnUtils.clearLockdownVpn(this.getContext());
        }
        final boolean setAlwaysOnVpn = this.setAlwaysOnVpn(b, b2);
        if (b && (!setAlwaysOnVpn || !this.isVpnAlwaysOn())) {
            CannotConnectFragment.show(this, this.mVpnLabel);
        }
        else {
            this.updateUI();
        }
        return setAlwaysOnVpn;
    }
    
    public static void show(final Context context, final AppPreference appPreference, final int sourceMetricsCategory) {
        final Bundle arguments = new Bundle();
        arguments.putString("package", appPreference.getPackageName());
        new SubSettingLauncher(context).setDestination(AppManagementFragment.class.getName()).setArguments(arguments).setTitle(appPreference.getLabel()).setSourceMetricsCategory(sourceMetricsCategory).setUserHandle(new UserHandle(appPreference.getUserId())).launch();
    }
    
    private void updateRestrictedViews() {
        if (this.isAdded()) {
            this.mPreferenceAlwaysOn.checkRestrictionAndSetDisabled("no_config_vpn", this.mUserId);
            this.mPreferenceLockdown.checkRestrictionAndSetDisabled("no_config_vpn", this.mUserId);
            this.mPreferenceForget.checkRestrictionAndSetDisabled("no_config_vpn", this.mUserId);
            if (this.mConnectivityManager.isAlwaysOnVpnPackageSupportedForUser(this.mUserId, this.mPackageName)) {
                this.mPreferenceAlwaysOn.setSummary(2131889804);
            }
            else {
                this.mPreferenceAlwaysOn.setEnabled(false);
                this.mPreferenceLockdown.setEnabled(false);
                this.mPreferenceAlwaysOn.setSummary(2131889806);
            }
        }
    }
    
    private void updateUI() {
        if (this.isAdded()) {
            final boolean vpnAlwaysOn = this.isVpnAlwaysOn();
            final boolean checked = vpnAlwaysOn && VpnUtils.isAnyLockdownActive((Context)this.getActivity());
            this.mPreferenceAlwaysOn.setChecked(vpnAlwaysOn);
            this.mPreferenceLockdown.setChecked(checked);
            this.updateRestrictedViews();
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 100;
    }
    
    @Override
    public void onConfirmLockdown(final Bundle bundle, final boolean b, final boolean b2) {
        this.setAlwaysOnVpnByUI(b, b2);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082858);
        this.mPackageManager = this.getContext().getPackageManager();
        this.mConnectivityManager = (ConnectivityManager)this.getContext().getSystemService((Class)ConnectivityManager.class);
        this.mConnectivityService = IConnectivityManager$Stub.asInterface(ServiceManager.getService("connectivity"));
        this.mPreferenceVersion = this.findPreference("version");
        this.mPreferenceAlwaysOn = (RestrictedSwitchPreference)this.findPreference("always_on_vpn");
        this.mPreferenceLockdown = (RestrictedSwitchPreference)this.findPreference("lockdown_vpn");
        this.mPreferenceForget = (RestrictedPreference)this.findPreference("forget_vpn");
        this.mPreferenceAlwaysOn.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mPreferenceLockdown.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mPreferenceForget.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, Object o) {
        final String key = preference.getKey();
        final int hashCode = key.hashCode();
        int n = 0;
        Label_0065: {
            if (hashCode != -2008102204) {
                if (hashCode == -1808701950) {
                    if (key.equals("lockdown_vpn")) {
                        n = 1;
                        break Label_0065;
                    }
                }
            }
            else if (key.equals("always_on_vpn")) {
                n = 0;
                break Label_0065;
            }
            n = -1;
        }
        switch (n) {
            default: {
                o = new StringBuilder();
                ((StringBuilder)o).append("unknown key is clicked: ");
                ((StringBuilder)o).append(preference.getKey());
                Log.w("AppManagementFragment", ((StringBuilder)o).toString());
                return false;
            }
            case 1: {
                return this.onAlwaysOnVpnClick(this.mPreferenceAlwaysOn.isChecked(), (boolean)o);
            }
            case 0: {
                return this.onAlwaysOnVpnClick((boolean)o, this.mPreferenceLockdown.isChecked());
            }
        }
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        final String key = preference.getKey();
        int n = 0;
        Label_0035: {
            if (key.hashCode() == -591389790) {
                if (key.equals("forget_vpn")) {
                    n = 0;
                    break Label_0035;
                }
            }
            n = -1;
        }
        if (n != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unknown key is clicked: ");
            sb.append(key);
            Log.w("AppManagementFragment", sb.toString());
            return false;
        }
        return this.onForgetVpnClick();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.loadInfo()) {
            this.mPreferenceVersion.setTitle(this.getPrefContext().getString(2131889864, new Object[] { this.mPackageInfo.versionName }));
            this.updateUI();
        }
        else {
            this.finish();
        }
    }
    
    public static class CannotConnectFragment extends InstrumentedDialogFragment
    {
        public static void show(final AppManagementFragment appManagementFragment, final String s) {
            if (appManagementFragment.getFragmentManager().findFragmentByTag("CannotConnect") == null) {
                final Bundle arguments = new Bundle();
                arguments.putString("label", s);
                final CannotConnectFragment cannotConnectFragment = new CannotConnectFragment();
                cannotConnectFragment.setArguments(arguments);
                cannotConnectFragment.show(appManagementFragment.getFragmentManager(), "CannotConnect");
            }
        }
        
        @Override
        public int getMetricsCategory() {
            return 547;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)this.getActivity().getString(2131889809, new Object[] { this.getArguments().getString("label") })).setMessage((CharSequence)this.getActivity().getString(2131889808)).setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)null).create();
        }
    }
}
