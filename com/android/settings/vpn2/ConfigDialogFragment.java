package com.android.settings.vpn2;

import android.app.AlertDialog;
import android.security.Credentials;
import android.app.Dialog;
import android.view.View;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.app.Fragment;
import android.os.Parcelable;
import android.os.Bundle;
import android.security.KeyStore;
import com.android.internal.net.LegacyVpnInfo;
import android.os.UserHandle;
import android.widget.Toast;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.net.VpnProfile;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import android.net.IConnectivityManager;
import android.content.Context;
import android.view.View.OnClickListener;
import android.content.DialogInterface$OnShowListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ConfigDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, DialogInterface$OnShowListener, View.OnClickListener, ConfirmLockdownListener
{
    private Context mContext;
    private final IConnectivityManager mService;
    private boolean mUnlocking;
    
    public ConfigDialogFragment() {
        this.mService = IConnectivityManager$Stub.asInterface(ServiceManager.getService("connectivity"));
        this.mUnlocking = false;
    }
    
    private void connect(final VpnProfile vpnProfile, final boolean b) {
        this.save(vpnProfile, b);
        if (!VpnUtils.isVpnLockdown(vpnProfile.key)) {
            VpnUtils.clearLockdownVpn(this.mContext);
            try {
                this.mService.startLegacyVpn(vpnProfile);
            }
            catch (RemoteException ex) {
                Log.e("ConfigDialogFragment", "Failed to connect", (Throwable)ex);
            }
            catch (IllegalStateException ex2) {
                Toast.makeText(this.mContext, 2131889839, 1).show();
            }
        }
    }
    
    private boolean disconnect(final VpnProfile vpnProfile) {
        try {
            return !this.isConnected(vpnProfile) || VpnUtils.disconnectLegacyVpn(this.getContext());
        }
        catch (RemoteException ex) {
            Log.e("ConfigDialogFragment", "Failed to disconnect", (Throwable)ex);
            return false;
        }
    }
    
    private boolean isConnected(final VpnProfile vpnProfile) throws RemoteException {
        final LegacyVpnInfo legacyVpnInfo = this.mService.getLegacyVpnInfo(UserHandle.myUserId());
        return legacyVpnInfo != null && vpnProfile.key.equals(legacyVpnInfo.key);
    }
    
    private void save(final VpnProfile vpnProfile, final boolean b) {
        final KeyStore instance = KeyStore.getInstance();
        final StringBuilder sb = new StringBuilder();
        sb.append("VPN_");
        sb.append(vpnProfile.key);
        instance.put(sb.toString(), vpnProfile.encode(), -1, 0);
        this.disconnect(vpnProfile);
        this.updateLockdownVpn(b, vpnProfile);
    }
    
    public static void show(final VpnSettings vpnSettings, final VpnProfile vpnProfile, final boolean b, final boolean b2) {
        if (!vpnSettings.isAdded()) {
            return;
        }
        final Bundle arguments = new Bundle();
        arguments.putParcelable("profile", (Parcelable)vpnProfile);
        arguments.putBoolean("editing", b);
        arguments.putBoolean("exists", b2);
        final ConfigDialogFragment configDialogFragment = new ConfigDialogFragment();
        configDialogFragment.setArguments(arguments);
        configDialogFragment.setTargetFragment((Fragment)vpnSettings, 0);
        configDialogFragment.show(vpnSettings.getFragmentManager(), "vpnconfigdialog");
    }
    
    private void updateLockdownVpn(final boolean b, final VpnProfile vpnProfile) {
        if (b) {
            if (!vpnProfile.isValidLockdownProfile()) {
                Toast.makeText(this.mContext, 2131889829, 1).show();
                return;
            }
            ConnectivityManager.from(this.mContext).setAlwaysOnVpnPackageForUser(UserHandle.myUserId(), (String)null, false);
            VpnUtils.setLockdownVpn(this.mContext, vpnProfile.key);
        }
        else if (VpnUtils.isVpnLockdown(vpnProfile.key)) {
            VpnUtils.clearLockdownVpn(this.mContext);
        }
    }
    
    public int getMetricsCategory() {
        return 545;
    }
    
    @Override
    public void onAttach(final Context mContext) {
        super.onAttach(mContext);
        this.mContext = mContext;
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        this.dismiss();
        super.onCancel(dialogInterface);
    }
    
    public void onClick(final DialogInterface dialogInterface, int n) {
        final ConfigDialog configDialog = (ConfigDialog)this.getDialog();
        final VpnProfile profile = configDialog.getProfile();
        if (n == -1) {
            final boolean vpnAlwaysOn = configDialog.isVpnAlwaysOn();
            if (!vpnAlwaysOn && configDialog.isEditing()) {
                n = 0;
            }
            else {
                n = 1;
            }
            final boolean anyLockdownActive = VpnUtils.isAnyLockdownActive(this.mContext);
            try {
                final boolean vpnActive = VpnUtils.isVpnActive(this.mContext);
                if (n != 0 && !this.isConnected(profile) && ConfirmLockdownFragment.shouldShow(vpnActive, anyLockdownActive, vpnAlwaysOn)) {
                    final Bundle bundle = new Bundle();
                    bundle.putParcelable("profile", (Parcelable)profile);
                    ConfirmLockdownFragment.show((Fragment)this, vpnActive, vpnAlwaysOn, anyLockdownActive, vpnAlwaysOn, bundle);
                }
                else if (n != 0) {
                    this.connect(profile, vpnAlwaysOn);
                }
                else {
                    this.save(profile, false);
                }
            }
            catch (RemoteException ex) {
                Log.w("ConfigDialogFragment", "Failed to check active VPN state. Skipping.", (Throwable)ex);
            }
        }
        else if (n == -3) {
            if (!this.disconnect(profile)) {
                Log.e("ConfigDialogFragment", "Failed to disconnect VPN. Leaving profile in keystore.");
                return;
            }
            final KeyStore instance = KeyStore.getInstance();
            final StringBuilder sb = new StringBuilder();
            sb.append("VPN_");
            sb.append(profile.key);
            instance.delete(sb.toString(), -1);
            this.updateLockdownVpn(false, profile);
        }
        this.dismiss();
    }
    
    public void onClick(final View view) {
        this.onClick((DialogInterface)this.getDialog(), -1);
    }
    
    public void onConfirmLockdown(final Bundle bundle, final boolean b, final boolean b2) {
        this.connect((VpnProfile)bundle.getParcelable("profile"), b);
        this.dismiss();
    }
    
    public Dialog onCreateDialog(Bundle arguments) {
        arguments = this.getArguments();
        final ConfigDialog configDialog = new ConfigDialog((Context)this.getActivity(), (DialogInterface$OnClickListener)this, (VpnProfile)arguments.getParcelable("profile"), arguments.getBoolean("editing"), arguments.getBoolean("exists"));
        ((Dialog)configDialog).setOnShowListener((DialogInterface$OnShowListener)this);
        return (Dialog)configDialog;
    }
    
    public void onResume() {
        super.onResume();
        if (!KeyStore.getInstance().isUnlocked()) {
            if (!this.mUnlocking) {
                Credentials.getInstance().unlock(this.mContext);
            }
            else {
                this.dismiss();
            }
            this.mUnlocking ^= true;
            return;
        }
        this.mUnlocking = false;
    }
    
    public void onShow(final DialogInterface dialogInterface) {
        ((AlertDialog)this.getDialog()).getButton(-1).setOnClickListener((View.OnClickListener)this);
    }
}
