package com.android.settings.vpn2;

import java.util.Objects;
import com.android.internal.util.Preconditions;

class AppVpnInfo implements Comparable
{
    public final String packageName;
    public final int userId;
    
    public AppVpnInfo(final int userId, final String s) {
        this.userId = userId;
        this.packageName = (String)Preconditions.checkNotNull((Object)s);
    }
    
    @Override
    public int compareTo(final Object o) {
        final AppVpnInfo appVpnInfo = (AppVpnInfo)o;
        int compareTo;
        if ((compareTo = this.packageName.compareTo(appVpnInfo.packageName)) == 0) {
            compareTo = appVpnInfo.userId - this.userId;
        }
        return compareTo;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof AppVpnInfo;
        final boolean b2 = false;
        if (b) {
            final AppVpnInfo appVpnInfo = (AppVpnInfo)o;
            boolean b3 = b2;
            if (this.userId == appVpnInfo.userId) {
                b3 = b2;
                if (Objects.equals(this.packageName, appVpnInfo.packageName)) {
                    b3 = true;
                }
            }
            return b3;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.packageName, this.userId);
    }
}
