package com.android.settings.vpn2;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.os.Bundle;
import android.app.Fragment;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ConfirmLockdownFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    public static boolean shouldShow(final boolean b, final boolean b2, final boolean b3) {
        return b || (b3 && !b2);
    }
    
    public static void show(final Fragment fragment, final boolean b, final boolean b2, final boolean b3, final boolean b4, final Bundle bundle) {
        if (fragment.getFragmentManager().findFragmentByTag("ConfirmLockdown") != null) {
            return;
        }
        final Bundle arguments = new Bundle();
        arguments.putBoolean("replacing", b);
        arguments.putBoolean("always_on", b2);
        arguments.putBoolean("lockdown_old", b3);
        arguments.putBoolean("lockdown_new", b4);
        arguments.putParcelable("options", (Parcelable)bundle);
        final ConfirmLockdownFragment confirmLockdownFragment = new ConfirmLockdownFragment();
        confirmLockdownFragment.setArguments(arguments);
        confirmLockdownFragment.setTargetFragment(fragment, 0);
        confirmLockdownFragment.show(fragment.getFragmentManager(), "ConfirmLockdown");
    }
    
    public int getMetricsCategory() {
        return 548;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (this.getTargetFragment() instanceof ConfirmLockdownListener) {
            ((ConfirmLockdownListener)this.getTargetFragment()).onConfirmLockdown((Bundle)this.getArguments().getParcelable("options"), this.getArguments().getBoolean("always_on"), this.getArguments().getBoolean("lockdown_new"));
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final boolean boolean1 = this.getArguments().getBoolean("replacing");
        this.getArguments().getBoolean("always_on");
        final boolean boolean2 = this.getArguments().getBoolean("lockdown_old");
        final boolean boolean3 = this.getArguments().getBoolean("lockdown_new");
        int title;
        if (boolean3) {
            title = 2131889850;
        }
        else if (boolean1) {
            title = 2131889848;
        }
        else {
            title = 2131889856;
        }
        int n;
        if (boolean1) {
            n = 2131889844;
        }
        else if (boolean3) {
            n = 2131889861;
        }
        else {
            n = R.string.okay;
        }
        int message;
        if (boolean3) {
            if (boolean1) {
                message = 2131889846;
            }
            else {
                message = 2131889820;
            }
        }
        else if (boolean2) {
            message = 2131889845;
        }
        else {
            message = 2131889847;
        }
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(title).setMessage(message).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setPositiveButton(n, (DialogInterface$OnClickListener)this).create();
    }
    
    public interface ConfirmLockdownListener
    {
        void onConfirmLockdown(final Bundle p0, final boolean p1, final boolean p2);
    }
}
