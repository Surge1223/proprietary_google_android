package com.android.settings.vpn2;

import android.text.TextUtils;
import android.view.View;
import android.support.v7.preference.Preference;
import android.util.AttributeSet;
import android.content.Context;
import com.android.internal.net.VpnProfile;

public class LegacyVpnPreference extends ManageablePreference
{
    private VpnProfile mProfile;
    
    LegacyVpnPreference(final Context context) {
        super(context, null);
        this.setIcon(2131231191);
        this.setIconSize(2);
    }
    
    public int compareTo(final Preference preference) {
        if (preference instanceof LegacyVpnPreference) {
            final LegacyVpnPreference legacyVpnPreference = (LegacyVpnPreference)preference;
            int n;
            if ((n = legacyVpnPreference.mState - this.mState) == 0 && (n = this.mProfile.name.compareToIgnoreCase(legacyVpnPreference.mProfile.name)) == 0 && (n = this.mProfile.type - legacyVpnPreference.mProfile.type) == 0) {
                n = this.mProfile.key.compareTo(legacyVpnPreference.mProfile.key);
            }
            return n;
        }
        if (!(preference instanceof AppPreference)) {
            return super.compareTo(preference);
        }
        final AppPreference appPreference = (AppPreference)preference;
        if (this.mState != 3 && appPreference.getState() == 3) {
            return 1;
        }
        return -1;
    }
    
    public VpnProfile getProfile() {
        return this.mProfile;
    }
    
    @Override
    public void onClick(final View view) {
        if (view.getId() == 2131362594 && this.isDisabledByAdmin()) {
            this.performClick();
            return;
        }
        super.onClick(view);
    }
    
    public void setProfile(final VpnProfile mProfile) {
        final VpnProfile mProfile2 = this.mProfile;
        CharSequence name = null;
        String name2;
        if (mProfile2 != null) {
            name2 = this.mProfile.name;
        }
        else {
            name2 = null;
        }
        if (mProfile != null) {
            name = mProfile.name;
        }
        if (!TextUtils.equals((CharSequence)name2, name)) {
            this.setTitle(name);
            this.notifyHierarchyChanged();
        }
        this.mProfile = mProfile;
    }
}
