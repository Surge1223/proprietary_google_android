package com.android.settings.vpn2;

import android.content.res.Resources;
import android.text.TextUtils;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.widget.GearPreference;

public abstract class ManageablePreference extends GearPreference
{
    public static int STATE_NONE;
    boolean mIsAlwaysOn;
    int mState;
    int mUserId;
    
    static {
        ManageablePreference.STATE_NONE = -1;
    }
    
    public ManageablePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mIsAlwaysOn = false;
        this.mState = ManageablePreference.STATE_NONE;
        this.setPersistent(false);
        this.setOrder(0);
        this.setUserId(UserHandle.myUserId());
    }
    
    public int getState() {
        return this.mState;
    }
    
    public int getUserId() {
        return this.mUserId;
    }
    
    public void setAlwaysOn(final boolean mIsAlwaysOn) {
        if (this.mIsAlwaysOn != mIsAlwaysOn) {
            this.mIsAlwaysOn = mIsAlwaysOn;
            this.updateSummary();
        }
    }
    
    public void setState(final int mState) {
        if (this.mState != mState) {
            this.mState = mState;
            this.updateSummary();
            this.notifyHierarchyChanged();
        }
    }
    
    public void setUserId(final int mUserId) {
        this.checkRestrictionAndSetDisabled("no_config_vpn", this.mUserId = mUserId);
    }
    
    protected void updateSummary() {
        final Resources resources = this.getContext().getResources();
        final String[] stringArray = resources.getStringArray(2130903168);
        String s;
        if (this.mState == ManageablePreference.STATE_NONE) {
            s = "";
        }
        else {
            s = stringArray[this.mState];
        }
        String summary = s;
        if (this.mIsAlwaysOn) {
            final String string = resources.getString(2131889805);
            String string2;
            if (TextUtils.isEmpty((CharSequence)s)) {
                string2 = string;
            }
            else {
                string2 = resources.getString(2131887918, new Object[] { s, string });
            }
            summary = string2;
        }
        this.setSummary(summary);
    }
}
