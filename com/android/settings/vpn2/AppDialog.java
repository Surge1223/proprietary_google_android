package com.android.settings.vpn2;

import android.os.Bundle;
import android.content.DialogInterface;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog;

class AppDialog extends AlertDialog implements DialogInterface$OnClickListener
{
    private final String mLabel;
    private final Listener mListener;
    private final PackageInfo mPackageInfo;
    
    AppDialog(final Context context, final Listener mListener, final PackageInfo mPackageInfo, final String mLabel) {
        super(context);
        this.mListener = mListener;
        this.mPackageInfo = mPackageInfo;
        this.mLabel = mLabel;
    }
    
    protected void createButtons() {
        final Context context = this.getContext();
        this.setButton(-2, (CharSequence)context.getString(2131889821), (DialogInterface$OnClickListener)this);
        this.setButton(-1, (CharSequence)context.getString(2131889818), (DialogInterface$OnClickListener)this);
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -2) {
            this.mListener.onForget(dialogInterface);
        }
        this.dismiss();
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setTitle((CharSequence)this.mLabel);
        this.setMessage((CharSequence)this.getContext().getString(2131889864, new Object[] { this.mPackageInfo.versionName }));
        this.createButtons();
        super.onCreate(bundle);
    }
    
    public interface Listener
    {
        void onForget(final DialogInterface p0);
    }
}
