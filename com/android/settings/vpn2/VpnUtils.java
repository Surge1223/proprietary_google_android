package com.android.settings.vpn2;

import android.provider.Settings;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import android.net.ConnectivityManager;
import com.android.internal.net.VpnConfig;
import android.net.IConnectivityManager;
import android.os.RemoteException;
import android.util.Log;
import android.security.KeyStore;
import android.content.Context;

public class VpnUtils
{
    public static void clearLockdownVpn(final Context context) {
        KeyStore.getInstance().delete("LOCKDOWN_VPN");
        getConnectivityManager(context).updateLockdownVpn();
    }
    
    public static boolean disconnectLegacyVpn(final Context context) {
        try {
            final int userId = context.getUserId();
            final IConnectivityManager iConnectivityManager = getIConnectivityManager();
            if (iConnectivityManager.getLegacyVpnInfo(userId) != null) {
                clearLockdownVpn(context);
                iConnectivityManager.prepareVpn((String)null, "[Legacy VPN]", userId);
                return true;
            }
        }
        catch (RemoteException ex) {
            Log.e("VpnUtils", "Legacy VPN could not be disconnected", (Throwable)ex);
        }
        return false;
    }
    
    public static String getConnectedPackage(final IConnectivityManager connectivityManager, final int n) throws RemoteException {
        final VpnConfig vpnConfig = connectivityManager.getVpnConfig(n);
        String user;
        if (vpnConfig != null) {
            user = vpnConfig.user;
        }
        else {
            user = null;
        }
        return user;
    }
    
    private static ConnectivityManager getConnectivityManager(final Context context) {
        return (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class);
    }
    
    private static IConnectivityManager getIConnectivityManager() {
        return IConnectivityManager$Stub.asInterface(ServiceManager.getService("connectivity"));
    }
    
    public static String getLockdownVpn() {
        final byte[] value = KeyStore.getInstance().get("LOCKDOWN_VPN");
        String s;
        if (value == null) {
            s = null;
        }
        else {
            s = new String(value);
        }
        return s;
    }
    
    public static boolean isAlwaysOnVpnSet(final ConnectivityManager connectivityManager, final int n) {
        return connectivityManager.getAlwaysOnVpnPackageForUser(n) != null;
    }
    
    public static boolean isAnyLockdownActive(final Context context) {
        final int userId = context.getUserId();
        final String lockdownVpn = getLockdownVpn();
        boolean b = true;
        if (lockdownVpn != null) {
            return true;
        }
        if (getConnectivityManager(context).getAlwaysOnVpnPackageForUser(userId) == null || Settings.Secure.getIntForUser(context.getContentResolver(), "always_on_vpn_lockdown", 0, userId) == 0) {
            b = false;
        }
        return b;
    }
    
    public static boolean isVpnActive(final Context context) throws RemoteException {
        return getIConnectivityManager().getVpnConfig(context.getUserId()) != null;
    }
    
    public static boolean isVpnLockdown(final String s) {
        return s.equals(getLockdownVpn());
    }
    
    public static void setLockdownVpn(final Context context, final String s) {
        KeyStore.getInstance().put("LOCKDOWN_VPN", s.getBytes(), -1, 0);
        getConnectivityManager(context).updateLockdownVpn();
    }
}
