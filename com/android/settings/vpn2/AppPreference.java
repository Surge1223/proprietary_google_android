package com.android.settings.vpn2;

import android.support.v7.preference.Preference;
import android.os.UserHandle;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager;
import com.android.internal.net.VpnConfig;
import android.util.AttributeSet;
import android.content.Context;

public class AppPreference extends ManageablePreference
{
    public static final int STATE_DISCONNECTED;
    private final String mName;
    private final String mPackageName;
    
    static {
        STATE_DISCONNECTED = AppPreference.STATE_NONE;
    }
    
    public AppPreference(Context loadIcon, final int userId, String s) {
        final Context context = null;
        final Context context2 = null;
        final Context context3 = null;
        super(loadIcon, null);
        super.setUserId(userId);
        this.mPackageName = s;
        loadIcon = context2;
        Object o = s;
        try {
            final Context userContext = this.getUserContext();
            loadIcon = context2;
            o = s;
            final PackageManager packageManager = userContext.getPackageManager();
            loadIcon = context;
            try {
                final PackageInfo packageInfo = packageManager.getPackageInfo(this.mPackageName, 0);
                loadIcon = context3;
                o = s;
                if (packageInfo != null) {
                    loadIcon = context;
                    final Object o2 = loadIcon = (Context)packageInfo.applicationInfo.loadIcon(packageManager);
                    o = VpnConfig.getVpnLabel(userContext, this.mPackageName).toString();
                    loadIcon = (Context)o2;
                }
                s = (String)o;
            }
            catch (PackageManager$NameNotFoundException ex) {}
            o = loadIcon;
            if (loadIcon == null) {
                o = s;
                o = packageManager.getDefaultActivityIcon();
            }
            loadIcon = (Context)o;
        }
        catch (PackageManager$NameNotFoundException ex2) {
            s = (String)o;
        }
        this.setTitle(this.mName = s);
        this.setIcon((Drawable)loadIcon);
    }
    
    private Context getUserContext() throws PackageManager$NameNotFoundException {
        return this.getContext().createPackageContextAsUser(this.getContext().getPackageName(), 0, UserHandle.of(this.mUserId));
    }
    
    public int compareTo(final Preference preference) {
        if (preference instanceof AppPreference) {
            final AppPreference appPreference = (AppPreference)preference;
            int n;
            if ((n = appPreference.mState - this.mState) == 0 && (n = this.mName.compareToIgnoreCase(appPreference.mName)) == 0 && (n = this.mPackageName.compareTo(appPreference.mPackageName)) == 0) {
                n = this.mUserId - appPreference.mUserId;
            }
            return n;
        }
        if (preference instanceof LegacyVpnPreference) {
            return -((LegacyVpnPreference)preference).compareTo(this);
        }
        return super.compareTo(preference);
    }
    
    public String getLabel() {
        return this.mName;
    }
    
    public PackageInfo getPackageInfo() {
        try {
            return this.getUserContext().getPackageManager().getPackageInfo(this.mPackageName, 0);
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    public String getPackageName() {
        return this.mPackageName;
    }
}
