package com.android.settings.vpn2;

import android.widget.AdapterView;
import android.os.SystemProperties;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.text.Editable;
import java.net.InetAddress;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.content.Context;
import android.view.View;
import com.android.internal.net.VpnProfile;
import android.content.DialogInterface$OnClickListener;
import android.security.KeyStore;
import android.widget.Spinner;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.AdapterView$OnItemSelectedListener;
import android.view.View.OnClickListener;
import android.text.TextWatcher;
import android.app.AlertDialog;

class ConfigDialog extends AlertDialog implements TextWatcher, View.OnClickListener, AdapterView$OnItemSelectedListener, CompoundButton$OnCheckedChangeListener
{
    private TextView mAlwaysOnInvalidReason;
    private CheckBox mAlwaysOnVpn;
    private TextView mDnsServers;
    private boolean mEditing;
    private boolean mExists;
    private Spinner mIpsecCaCert;
    private TextView mIpsecIdentifier;
    private TextView mIpsecSecret;
    private Spinner mIpsecServerCert;
    private Spinner mIpsecUserCert;
    private final KeyStore mKeyStore;
    private TextView mL2tpSecret;
    private final DialogInterface$OnClickListener mListener;
    private CheckBox mMppe;
    private TextView mName;
    private TextView mPassword;
    private final VpnProfile mProfile;
    private TextView mRoutes;
    private CheckBox mSaveLogin;
    private TextView mSearchDomains;
    private TextView mServer;
    private CheckBox mShowOptions;
    private Spinner mType;
    private TextView mUsername;
    private View mView;
    
    ConfigDialog(final Context context, final DialogInterface$OnClickListener mListener, final VpnProfile mProfile, final boolean mEditing, final boolean mExists) {
        super(context);
        this.mKeyStore = KeyStore.getInstance();
        this.mListener = mListener;
        this.mProfile = mProfile;
        this.mEditing = mEditing;
        this.mExists = mExists;
    }
    
    private void changeType(final int n) {
        this.mMppe.setVisibility(8);
        this.mView.findViewById(2131362301).setVisibility(8);
        this.mView.findViewById(2131362291).setVisibility(8);
        this.mView.findViewById(2131362294).setVisibility(8);
        this.mView.findViewById(2131362290).setVisibility(8);
        switch (n) {
            case 2: {
                this.mView.findViewById(2131362301).setVisibility(0);
            }
            case 4: {
                this.mView.findViewById(2131362294).setVisibility(0);
            }
            case 5: {
                this.mView.findViewById(2131362290).setVisibility(0);
                break;
            }
            case 1: {
                this.mView.findViewById(2131362301).setVisibility(0);
            }
            case 3: {
                this.mView.findViewById(2131362291).setVisibility(0);
                break;
            }
            case 0: {
                this.mMppe.setVisibility(0);
                break;
            }
        }
    }
    
    private void loadCertificates(final Spinner spinner, final String s, int i, final String s2) {
        final Context context = this.getContext();
        String string;
        if (i == 0) {
            string = "";
        }
        else {
            string = context.getString(i);
        }
        final String[] list = this.mKeyStore.list(s);
        i = 1;
        String[] array;
        if (list != null && list.length != 0) {
            array = new String[list.length + 1];
            array[0] = string;
            System.arraycopy(list, 0, array, 1, list.length);
        }
        else {
            array = new String[] { string };
        }
        final ArrayAdapter adapter = new ArrayAdapter(context, 17367048, (Object[])array);
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter)adapter);
        while (i < array.length) {
            if (array[i].equals(s2)) {
                spinner.setSelection(i);
                break;
            }
            ++i;
        }
    }
    
    private void showAdvancedOptions() {
        this.mView.findViewById(2131362426).setVisibility(0);
        this.mShowOptions.setVisibility(8);
    }
    
    private void updateUiControls() {
        final VpnProfile profile = this.getProfile();
        if (profile.isValidLockdownProfile()) {
            this.mAlwaysOnVpn.setEnabled(true);
            this.mAlwaysOnInvalidReason.setVisibility(8);
        }
        else {
            this.mAlwaysOnVpn.setChecked(false);
            this.mAlwaysOnVpn.setEnabled(false);
            if (!profile.isTypeValidForLockdown()) {
                this.mAlwaysOnInvalidReason.setText(2131889803);
            }
            else if (!profile.isServerAddressNumeric()) {
                this.mAlwaysOnInvalidReason.setText(2131889802);
            }
            else if (!profile.hasDns()) {
                this.mAlwaysOnInvalidReason.setText(2131889800);
            }
            else if (!profile.areDnsAddressesNumeric()) {
                this.mAlwaysOnInvalidReason.setText(2131889799);
            }
            else {
                this.mAlwaysOnInvalidReason.setText(2131889801);
            }
            this.mAlwaysOnInvalidReason.setVisibility(0);
        }
        if (this.mAlwaysOnVpn.isChecked()) {
            this.mSaveLogin.setChecked(true);
            this.mSaveLogin.setEnabled(false);
        }
        else {
            this.mSaveLogin.setChecked(this.mProfile.saveLogin);
            this.mSaveLogin.setEnabled(true);
        }
        this.getButton(-1).setEnabled(this.validate(this.mEditing));
    }
    
    private boolean validate(final boolean b) {
        if (this.mAlwaysOnVpn.isChecked() && !this.getProfile().isValidLockdownProfile()) {
            return false;
        }
        final boolean b2 = true;
        final boolean b3 = true;
        final boolean b4 = true;
        if (!b) {
            return this.mUsername.getText().length() != 0 && this.mPassword.getText().length() != 0 && b4;
        }
        if (this.mName.getText().length() == 0 || this.mServer.getText().length() == 0 || !this.validateAddresses(this.mDnsServers.getText().toString(), false) || !this.validateAddresses(this.mRoutes.getText().toString(), true)) {
            return false;
        }
        switch (this.mType.getSelectedItemPosition()) {
            default: {
                return false;
            }
            case 2:
            case 4: {
                return this.mIpsecUserCert.getSelectedItemPosition() != 0 && b2;
            }
            case 1:
            case 3: {
                return this.mIpsecSecret.getText().length() != 0 && b3;
            }
            case 0:
            case 5: {
                return true;
            }
        }
    }
    
    private boolean validateAddresses(String s, final boolean b) {
        try {
            for (final String s2 : s.split(" ")) {
                if (!s2.isEmpty()) {
                    int int1 = 32;
                    s = s2;
                    if (b) {
                        final String[] split2 = s2.split("/", 2);
                        s = split2[0];
                        int1 = Integer.parseInt(split2[1]);
                    }
                    final byte[] address = InetAddress.parseNumericAddress(s).getAddress();
                    final byte b2 = address[3];
                    final byte b3 = address[2];
                    final byte b4 = address[1];
                    final byte b5 = address[0];
                    if (address.length != 4 || int1 < 0 || int1 > 32 || (int1 < 32 && ((b4 & 0xFF) << 16 | ((b3 & 0xFF) << 8 | (b2 & 0xFF)) | (b5 & 0xFF) << 24) << int1 != 0)) {
                        return false;
                    }
                }
            }
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    public void afterTextChanged(final Editable editable) {
        this.updateUiControls();
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    VpnProfile getProfile() {
        final VpnProfile vpnProfile = new VpnProfile(this.mProfile.key);
        vpnProfile.name = this.mName.getText().toString();
        vpnProfile.type = this.mType.getSelectedItemPosition();
        vpnProfile.server = this.mServer.getText().toString().trim();
        vpnProfile.username = this.mUsername.getText().toString();
        vpnProfile.password = this.mPassword.getText().toString();
        vpnProfile.searchDomains = this.mSearchDomains.getText().toString().trim();
        vpnProfile.dnsServers = this.mDnsServers.getText().toString().trim();
        vpnProfile.routes = this.mRoutes.getText().toString().trim();
        switch (vpnProfile.type) {
            case 2: {
                vpnProfile.l2tpSecret = this.mL2tpSecret.getText().toString();
            }
            case 4: {
                if (this.mIpsecUserCert.getSelectedItemPosition() != 0) {
                    vpnProfile.ipsecUserCert = (String)this.mIpsecUserCert.getSelectedItem();
                }
            }
            case 5: {
                if (this.mIpsecCaCert.getSelectedItemPosition() != 0) {
                    vpnProfile.ipsecCaCert = (String)this.mIpsecCaCert.getSelectedItem();
                }
                if (this.mIpsecServerCert.getSelectedItemPosition() != 0) {
                    vpnProfile.ipsecServerCert = (String)this.mIpsecServerCert.getSelectedItem();
                    break;
                }
                break;
            }
            case 1: {
                vpnProfile.l2tpSecret = this.mL2tpSecret.getText().toString();
            }
            case 3: {
                vpnProfile.ipsecIdentifier = this.mIpsecIdentifier.getText().toString();
                vpnProfile.ipsecSecret = this.mIpsecSecret.getText().toString();
                break;
            }
            case 0: {
                vpnProfile.mppe = this.mMppe.isChecked();
                break;
            }
        }
        final boolean empty = vpnProfile.username.isEmpty();
        final boolean b = true;
        final boolean b2 = !empty || !vpnProfile.password.isEmpty();
        boolean saveLogin = b;
        if (!this.mSaveLogin.isChecked()) {
            saveLogin = (this.mEditing && b2 && b);
        }
        vpnProfile.saveLogin = saveLogin;
        return vpnProfile;
    }
    
    boolean isEditing() {
        return this.mEditing;
    }
    
    public boolean isVpnAlwaysOn() {
        return this.mAlwaysOnVpn.isChecked();
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        if (compoundButton == this.mAlwaysOnVpn) {
            this.updateUiControls();
        }
    }
    
    public void onClick(final View view) {
        if (view == this.mShowOptions) {
            this.showAdvancedOptions();
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setView(this.mView = this.getLayoutInflater().inflate(2131558878, (ViewGroup)null));
        final Context context = this.getContext();
        this.mName = (TextView)this.mView.findViewById(2131362382);
        this.mType = (Spinner)this.mView.findViewById(2131362759);
        this.mServer = (TextView)this.mView.findViewById(2131362589);
        this.mUsername = (TextView)this.mView.findViewById(2131362799);
        this.mPassword = (TextView)this.mView.findViewById(2131362436);
        this.mSearchDomains = (TextView)this.mView.findViewById(2131362562);
        this.mDnsServers = (TextView)this.mView.findViewById(2131362084);
        this.mRoutes = (TextView)this.mView.findViewById(2131362537);
        this.mMppe = (CheckBox)this.mView.findViewById(2131362379);
        this.mL2tpSecret = (TextView)this.mView.findViewById(2131362302);
        this.mIpsecIdentifier = (TextView)this.mView.findViewById(2131362289);
        this.mIpsecSecret = (TextView)this.mView.findViewById(2131362292);
        this.mIpsecUserCert = (Spinner)this.mView.findViewById(2131362295);
        this.mIpsecCaCert = (Spinner)this.mView.findViewById(2131362288);
        this.mIpsecServerCert = (Spinner)this.mView.findViewById(2131362293);
        this.mSaveLogin = (CheckBox)this.mView.findViewById(2131362543);
        this.mShowOptions = (CheckBox)this.mView.findViewById(2131362605);
        this.mAlwaysOnVpn = (CheckBox)this.mView.findViewById(2131361866);
        this.mAlwaysOnInvalidReason = (TextView)this.mView.findViewById(2131361865);
        this.mName.setText((CharSequence)this.mProfile.name);
        this.mType.setSelection(this.mProfile.type);
        this.mServer.setText((CharSequence)this.mProfile.server);
        if (this.mProfile.saveLogin) {
            this.mUsername.setText((CharSequence)this.mProfile.username);
            this.mPassword.setText((CharSequence)this.mProfile.password);
        }
        this.mSearchDomains.setText((CharSequence)this.mProfile.searchDomains);
        this.mDnsServers.setText((CharSequence)this.mProfile.dnsServers);
        this.mRoutes.setText((CharSequence)this.mProfile.routes);
        this.mMppe.setChecked(this.mProfile.mppe);
        this.mL2tpSecret.setText((CharSequence)this.mProfile.l2tpSecret);
        this.mIpsecIdentifier.setText((CharSequence)this.mProfile.ipsecIdentifier);
        this.mIpsecSecret.setText((CharSequence)this.mProfile.ipsecSecret);
        this.loadCertificates(this.mIpsecUserCert, "USRPKEY_", 0, this.mProfile.ipsecUserCert);
        this.loadCertificates(this.mIpsecCaCert, "CACERT_", 2131889838, this.mProfile.ipsecCaCert);
        this.loadCertificates(this.mIpsecServerCert, "USRCERT_", 2131889840, this.mProfile.ipsecServerCert);
        this.mSaveLogin.setChecked(this.mProfile.saveLogin);
        this.mAlwaysOnVpn.setChecked(this.mProfile.key.equals(VpnUtils.getLockdownVpn()));
        if (SystemProperties.getBoolean("persist.radio.imsregrequired", false)) {
            this.mAlwaysOnVpn.setVisibility(8);
        }
        this.mName.addTextChangedListener((TextWatcher)this);
        this.mType.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        this.mServer.addTextChangedListener((TextWatcher)this);
        this.mUsername.addTextChangedListener((TextWatcher)this);
        this.mPassword.addTextChangedListener((TextWatcher)this);
        this.mDnsServers.addTextChangedListener((TextWatcher)this);
        this.mRoutes.addTextChangedListener((TextWatcher)this);
        this.mIpsecSecret.addTextChangedListener((TextWatcher)this);
        this.mIpsecUserCert.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        this.mShowOptions.setOnClickListener((View.OnClickListener)this);
        this.mAlwaysOnVpn.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
        this.mEditing = (this.mEditing || !this.validate(true));
        if (this.mEditing) {
            this.setTitle(2131889819);
            this.mView.findViewById(2131362097).setVisibility(0);
            this.changeType(this.mProfile.type);
            this.mSaveLogin.setVisibility(8);
            if (!this.mProfile.searchDomains.isEmpty() || !this.mProfile.dnsServers.isEmpty() || !this.mProfile.routes.isEmpty()) {
                this.showAdvancedOptions();
            }
            if (this.mExists) {
                this.setButton(-3, (CharSequence)context.getString(2131889821), this.mListener);
            }
            this.setButton(-1, (CharSequence)context.getString(2131889852), this.mListener);
        }
        else {
            this.setTitle((CharSequence)context.getString(2131889811, new Object[] { this.mProfile.name }));
            this.setButton(-1, (CharSequence)context.getString(2131889810), this.mListener);
        }
        this.setButton(-2, (CharSequence)context.getString(2131889807), this.mListener);
        super.onCreate(bundle);
        this.updateUiControls();
        this.getWindow().setSoftInputMode(20);
    }
    
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        if (adapterView == this.mType) {
            this.changeType(n);
        }
        this.updateUiControls();
    }
    
    public void onNothingSelected(final AdapterView<?> adapterView) {
    }
    
    public void onRestoreInstanceState(final Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (this.mShowOptions.isChecked()) {
            this.showAdvancedOptions();
        }
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
}
