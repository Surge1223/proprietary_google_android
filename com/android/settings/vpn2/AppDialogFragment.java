package com.android.settings.vpn2;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.Context;
import android.app.Dialog;
import android.os.Parcelable;
import android.os.Bundle;
import android.app.Fragment;
import android.os.RemoteException;
import android.util.Log;
import android.os.UserHandle;
import android.content.DialogInterface;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import android.os.UserManager;
import android.net.IConnectivityManager;
import android.content.pm.PackageInfo;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class AppDialogFragment extends InstrumentedDialogFragment implements AppDialog.Listener
{
    private Listener mListener;
    private PackageInfo mPackageInfo;
    private final IConnectivityManager mService;
    private UserManager mUserManager;
    
    public AppDialogFragment() {
        this.mService = IConnectivityManager$Stub.asInterface(ServiceManager.getService("connectivity"));
    }
    
    private int getUserId() {
        return UserHandle.getUserId(this.mPackageInfo.applicationInfo.uid);
    }
    
    private boolean isUiRestricted() {
        return this.mUserManager.hasUserRestriction("no_config_vpn", UserHandle.of(this.getUserId()));
    }
    
    private void onDisconnect(final DialogInterface dialogInterface) {
        if (this.isUiRestricted()) {
            return;
        }
        final int userId = this.getUserId();
        try {
            if (this.mPackageInfo.packageName.equals(VpnUtils.getConnectedPackage(this.mService, userId))) {
                this.mService.setAlwaysOnVpnPackage(userId, (String)null, false);
                this.mService.prepareVpn(this.mPackageInfo.packageName, "[Legacy VPN]", userId);
            }
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to disconnect package ");
            sb.append(this.mPackageInfo.packageName);
            sb.append(" for user ");
            sb.append(userId);
            Log.e("AppDialogFragment", sb.toString(), (Throwable)ex);
        }
    }
    
    public static void show(final Fragment fragment, final PackageInfo packageInfo, final String s, final boolean b, final boolean b2) {
        if (!b && !b2) {
            return;
        }
        show(fragment, null, packageInfo, s, b, b2);
    }
    
    public static void show(final Fragment fragment, final Listener mListener, final PackageInfo packageInfo, final String s, final boolean b, final boolean b2) {
        if (!fragment.isAdded()) {
            return;
        }
        final Bundle arguments = new Bundle();
        arguments.putParcelable("package", (Parcelable)packageInfo);
        arguments.putString("label", s);
        arguments.putBoolean("managing", b);
        arguments.putBoolean("connected", b2);
        final AppDialogFragment appDialogFragment = new AppDialogFragment();
        appDialogFragment.mListener = mListener;
        appDialogFragment.setArguments(arguments);
        appDialogFragment.setTargetFragment(fragment, 0);
        appDialogFragment.show(fragment.getFragmentManager(), "vpnappdialog");
    }
    
    @Override
    public int getMetricsCategory() {
        return 546;
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        this.dismiss();
        if (this.mListener != null) {
            this.mListener.onCancel();
        }
        super.onCancel(dialogInterface);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mUserManager = UserManager.get(this.getContext());
    }
    
    public Dialog onCreateDialog(Bundle arguments) {
        arguments = this.getArguments();
        final String string = arguments.getString("label");
        final boolean boolean1 = arguments.getBoolean("managing");
        final boolean boolean2 = arguments.getBoolean("connected");
        this.mPackageInfo = (PackageInfo)arguments.getParcelable("package");
        if (boolean1) {
            return (Dialog)new AppDialog((Context)this.getActivity(), (AppDialog.Listener)this, this.mPackageInfo, string);
        }
        final AlertDialog$Builder setNegativeButton = new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)string).setMessage((CharSequence)this.getActivity().getString(2131889814)).setNegativeButton((CharSequence)this.getActivity().getString(2131889807), (DialogInterface$OnClickListener)null);
        if (boolean2 && !this.isUiRestricted()) {
            setNegativeButton.setPositiveButton((CharSequence)this.getActivity().getString(2131889813), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    AppDialogFragment.this.onDisconnect(dialogInterface);
                }
            });
        }
        return (Dialog)setNegativeButton.create();
    }
    
    @Override
    public void onForget(final DialogInterface dialogInterface) {
        if (this.isUiRestricted()) {
            return;
        }
        final int userId = this.getUserId();
        try {
            this.mService.setVpnPackageAuthorization(this.mPackageInfo.packageName, userId, false);
            this.onDisconnect(dialogInterface);
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to forget authorization of ");
            sb.append(this.mPackageInfo.packageName);
            sb.append(" for user ");
            sb.append(userId);
            Log.e("AppDialogFragment", sb.toString(), (Throwable)ex);
        }
        if (this.mListener != null) {
            this.mListener.onForget();
        }
    }
    
    public interface Listener
    {
        void onCancel();
        
        void onForget();
    }
}
