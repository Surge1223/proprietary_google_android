package com.android.settings.vpn2;

import android.support.v7.preference.PreferenceScreen;
import java.util.Collection;
import android.view.View;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Intent;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import android.app.Activity;
import android.os.Message;
import com.android.internal.annotations.VisibleForTesting;
import com.android.internal.util.ArrayUtils;
import com.android.internal.net.VpnProfile;
import java.util.ArrayList;
import android.app.AppOpsManager$OpEntry;
import android.app.AppOpsManager$PackageOps;
import android.app.AppOpsManager;
import com.google.android.collect.Lists;
import java.util.List;
import java.util.Collections;
import com.android.internal.net.VpnConfig;
import android.os.RemoteException;
import android.util.Log;
import java.util.Iterator;
import android.os.UserHandle;
import android.util.ArraySet;
import java.util.Set;
import android.content.Context;
import android.net.Network;
import android.util.ArrayMap;
import android.net.IConnectivityManager$Stub;
import android.os.ServiceManager;
import android.net.NetworkRequest$Builder;
import android.os.UserManager;
import android.os.HandlerThread;
import com.android.internal.annotations.GuardedBy;
import android.os.Handler;
import android.net.ConnectivityManager$NetworkCallback;
import android.security.KeyStore;
import com.android.settings.widget.GearPreference;
import android.net.IConnectivityManager;
import android.net.ConnectivityManager;
import com.android.internal.net.LegacyVpnInfo;
import java.util.Map;
import android.net.NetworkRequest;
import android.support.v7.preference.Preference;
import android.os.Handler$Callback;
import com.android.settings.RestrictedSettingsFragment;

public class VpnSettings extends RestrictedSettingsFragment implements Handler$Callback, OnPreferenceClickListener
{
    private static final NetworkRequest VPN_REQUEST;
    private Map<AppVpnInfo, AppPreference> mAppPreferences;
    private LegacyVpnInfo mConnectedLegacyVpn;
    private ConnectivityManager mConnectivityManager;
    private final IConnectivityManager mConnectivityService;
    private GearPreference.OnGearClickListener mGearListener;
    private final KeyStore mKeyStore;
    private Map<String, LegacyVpnPreference> mLegacyVpnPreferences;
    private ConnectivityManager$NetworkCallback mNetworkCallback;
    private boolean mUnavailable;
    @GuardedBy("this")
    private Handler mUpdater;
    private HandlerThread mUpdaterThread;
    private UserManager mUserManager;
    
    static {
        VPN_REQUEST = new NetworkRequest$Builder().removeCapability(15).removeCapability(13).removeCapability(14).build();
    }
    
    public VpnSettings() {
        super("no_config_vpn");
        this.mConnectivityService = IConnectivityManager$Stub.asInterface(ServiceManager.getService("connectivity"));
        this.mKeyStore = KeyStore.getInstance();
        this.mLegacyVpnPreferences = (Map<String, LegacyVpnPreference>)new ArrayMap();
        this.mAppPreferences = (Map<AppVpnInfo, AppPreference>)new ArrayMap();
        this.mGearListener = new GearPreference.OnGearClickListener() {
            @Override
            public void onGearClick(final GearPreference gearPreference) {
                if (gearPreference instanceof LegacyVpnPreference) {
                    ConfigDialogFragment.show(VpnSettings.this, ((LegacyVpnPreference)gearPreference).getProfile(), true, true);
                }
                else if (gearPreference instanceof AppPreference) {
                    AppManagementFragment.show(InstrumentedPreferenceFragment.this.getPrefContext(), (AppPreference)gearPreference, VpnSettings.this.getMetricsCategory());
                }
            }
        };
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            public void onAvailable(final Network network) {
                if (VpnSettings.this.mUpdater != null) {
                    VpnSettings.this.mUpdater.sendEmptyMessage(0);
                }
            }
            
            public void onLost(final Network network) {
                if (VpnSettings.this.mUpdater != null) {
                    VpnSettings.this.mUpdater.sendEmptyMessage(0);
                }
            }
        };
    }
    
    private Set<AppVpnInfo> getAlwaysOnAppVpnInfos() {
        final ArraySet set = new ArraySet();
        final Iterator<UserHandle> iterator = this.mUserManager.getUserProfiles().iterator();
        while (iterator.hasNext()) {
            final int identifier = iterator.next().getIdentifier();
            final String alwaysOnVpnPackageForUser = this.mConnectivityManager.getAlwaysOnVpnPackageForUser(identifier);
            if (alwaysOnVpnPackageForUser != null) {
                ((Set<AppVpnInfo>)set).add(new AppVpnInfo(identifier, alwaysOnVpnPackageForUser));
            }
        }
        return (Set<AppVpnInfo>)set;
    }
    
    private Set<AppVpnInfo> getConnectedAppVpns() {
        final ArraySet set = new ArraySet();
        try {
            for (final UserHandle userHandle : this.mUserManager.getUserProfiles()) {
                final VpnConfig vpnConfig = this.mConnectivityService.getVpnConfig(userHandle.getIdentifier());
                if (vpnConfig != null && !vpnConfig.legacy) {
                    ((Set<AppVpnInfo>)set).add(new AppVpnInfo(userHandle.getIdentifier(), vpnConfig.user));
                }
            }
        }
        catch (RemoteException ex) {
            Log.e("VpnSettings", "Failure updating VPN list with connected app VPNs", (Throwable)ex);
        }
        return (Set<AppVpnInfo>)set;
    }
    
    private Map<String, LegacyVpnInfo> getConnectedLegacyVpns() {
        try {
            this.mConnectedLegacyVpn = this.mConnectivityService.getLegacyVpnInfo(UserHandle.myUserId());
            if (this.mConnectedLegacyVpn != null) {
                return Collections.singletonMap(this.mConnectedLegacyVpn.key, this.mConnectedLegacyVpn);
            }
        }
        catch (RemoteException ex) {
            Log.e("VpnSettings", "Failure updating VPN list with connected legacy VPNs", (Throwable)ex);
        }
        return Collections.emptyMap();
    }
    
    static List<AppVpnInfo> getVpnApps(final Context context, final boolean b) {
        final ArrayList arrayList = Lists.newArrayList();
        Object singleton;
        if (b) {
            final ArraySet set = new ArraySet();
            final Iterator iterator = UserManager.get(context).getUserProfiles().iterator();
            while (true) {
                singleton = set;
                if (!iterator.hasNext()) {
                    break;
                }
                ((Set<Integer>)set).add(iterator.next().getIdentifier());
            }
        }
        else {
            singleton = Collections.singleton(UserHandle.myUserId());
        }
        final List packagesForOps = ((AppOpsManager)context.getSystemService("appops")).getPackagesForOps(new int[] { 47 });
        if (packagesForOps != null) {
            for (final AppOpsManager$PackageOps appOpsManager$PackageOps : packagesForOps) {
                final int userId = UserHandle.getUserId(appOpsManager$PackageOps.getUid());
                if (!((Set)singleton).contains(userId)) {
                    continue;
                }
                int n = 0;
                for (final AppOpsManager$OpEntry appOpsManager$OpEntry : appOpsManager$PackageOps.getOps()) {
                    int n2 = n;
                    if (appOpsManager$OpEntry.getOp() == 47) {
                        n2 = n;
                        if (appOpsManager$OpEntry.getMode() == 0) {
                            n2 = 1;
                        }
                    }
                    n = n2;
                }
                if (n == 0) {
                    continue;
                }
                arrayList.add(new AppVpnInfo(userId, appOpsManager$PackageOps.getPackageName()));
            }
        }
        Collections.sort((List<Comparable>)arrayList);
        return (List<AppVpnInfo>)arrayList;
    }
    
    static List<VpnProfile> loadVpnProfiles(final KeyStore keyStore, final int... array) {
        final ArrayList arrayList = Lists.newArrayList();
        for (final String s : keyStore.list("VPN_")) {
            final StringBuilder sb = new StringBuilder();
            sb.append("VPN_");
            sb.append(s);
            final VpnProfile decode = VpnProfile.decode(s, keyStore.get(sb.toString()));
            if (decode != null && !ArrayUtils.contains(array, decode.type)) {
                arrayList.add(decode);
            }
        }
        return (List<VpnProfile>)arrayList;
    }
    
    @VisibleForTesting
    public boolean canAddPreferences() {
        return this.isAdded();
    }
    
    @VisibleForTesting
    public AppPreference findOrCreatePreference(final AppVpnInfo appVpnInfo) {
        AppPreference appPreference;
        if ((appPreference = this.mAppPreferences.get(appVpnInfo)) == null) {
            appPreference = new AppPreference(this.getPrefContext(), appVpnInfo.userId, appVpnInfo.packageName);
            appPreference.setOnGearClickListener(this.mGearListener);
            appPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
            this.mAppPreferences.put(appVpnInfo, appPreference);
        }
        return appPreference;
    }
    
    @VisibleForTesting
    public LegacyVpnPreference findOrCreatePreference(final VpnProfile profile, final boolean b) {
        final LegacyVpnPreference legacyVpnPreference = this.mLegacyVpnPreferences.get(profile.key);
        boolean b2 = false;
        LegacyVpnPreference legacyVpnPreference2 = legacyVpnPreference;
        if (legacyVpnPreference == null) {
            legacyVpnPreference2 = new LegacyVpnPreference(this.getPrefContext());
            legacyVpnPreference2.setOnGearClickListener(this.mGearListener);
            legacyVpnPreference2.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
            this.mLegacyVpnPreferences.put(profile.key, legacyVpnPreference2);
            b2 = true;
        }
        if (b2 || b) {
            legacyVpnPreference2.setProfile(profile);
        }
        return legacyVpnPreference2;
    }
    
    public int getHelpResource() {
        return 2131887839;
    }
    
    public int getMetricsCategory() {
        return 100;
    }
    
    public boolean handleMessage(final Message message) {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return true;
        }
        activity.runOnUiThread((Runnable)new UpdatePreferences(this).legacyVpns(loadVpnProfiles(this.mKeyStore, new int[0]), this.getConnectedLegacyVpns(), VpnUtils.getLockdownVpn()).appVpns(getVpnApps(activity.getApplicationContext(), true), this.getConnectedAppVpns(), this.getAlwaysOnAppVpnInfos()));
        synchronized (this) {
            if (this.mUpdater != null) {
                this.mUpdater.removeMessages(0);
                this.mUpdater.sendEmptyMessageDelayed(0, 1000L);
            }
            return true;
        }
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.mUserManager = (UserManager)this.getSystemService("user");
        this.mConnectivityManager = (ConnectivityManager)this.getSystemService("connectivity");
        this.mUnavailable = this.isUiRestricted();
        this.setHasOptionsMenu(this.mUnavailable ^ true);
        this.addPreferencesFromResource(2132082859);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(2131623943, menu);
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 2131362813) {
            return super.onOptionsItemSelected(menuItem);
        }
        long currentTimeMillis;
        for (currentTimeMillis = System.currentTimeMillis(); this.mLegacyVpnPreferences.containsKey(Long.toHexString(currentTimeMillis)); ++currentTimeMillis) {}
        ConfigDialogFragment.show(this, new VpnProfile(Long.toHexString(currentTimeMillis)), true, false);
        return true;
    }
    
    public void onPause() {
        if (this.mUnavailable) {
            super.onPause();
            return;
        }
        this.mConnectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
        synchronized (this) {
            this.mUpdater.removeCallbacksAndMessages((Object)null);
            this.mUpdater = null;
            this.mUpdaterThread.quit();
            this.mUpdaterThread = null;
            // monitorexit(this)
            super.onPause();
        }
    }
    
    public boolean onPreferenceClick(Preference profile) {
        if (profile instanceof LegacyVpnPreference) {
            profile = (Preference)((LegacyVpnPreference)profile).getProfile();
            if (this.mConnectedLegacyVpn != null && ((VpnProfile)profile).key.equals(this.mConnectedLegacyVpn.key) && this.mConnectedLegacyVpn.state == 3) {
                try {
                    this.mConnectedLegacyVpn.intent.send();
                    return true;
                }
                catch (Exception ex) {
                    Log.w("VpnSettings", "Starting config intent failed", (Throwable)ex);
                }
            }
            ConfigDialogFragment.show(this, (VpnProfile)profile, false, true);
            return true;
        }
        if (profile instanceof AppPreference) {
            profile = profile;
            final boolean b = ((ManageablePreference)profile).getState() == 3;
            if (!b) {
                try {
                    final UserHandle of = UserHandle.of(((ManageablePreference)profile).getUserId());
                    final Context packageContextAsUser = this.getActivity().createPackageContextAsUser(this.getActivity().getPackageName(), 0, of);
                    final Intent launchIntentForPackage = packageContextAsUser.getPackageManager().getLaunchIntentForPackage(((AppPreference)profile).getPackageName());
                    if (launchIntentForPackage != null) {
                        packageContextAsUser.startActivityAsUser(launchIntentForPackage, of);
                        return true;
                    }
                }
                catch (PackageManager$NameNotFoundException ex2) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("VPN provider does not exist: ");
                    sb.append(((AppPreference)profile).getPackageName());
                    Log.w("VpnSettings", sb.toString(), (Throwable)ex2);
                }
            }
            AppDialogFragment.show(this, ((AppPreference)profile).getPackageInfo(), ((AppPreference)profile).getLabel(), false, b);
            return true;
        }
        return false;
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        super.onPrepareOptionsMenu(menu);
        for (int i = 0; i < menu.size(); ++i) {
            if (this.isUiRestrictedByOnlyAdmin()) {
                RestrictedLockUtils.setMenuItemAsDisabledByAdmin(this.getPrefContext(), menu.getItem(i), this.getRestrictionEnforcedAdmin());
            }
            else {
                menu.getItem(i).setEnabled(this.mUnavailable ^ true);
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mUnavailable = this.mUserManager.hasUserRestriction("no_config_vpn");
        if (this.mUnavailable) {
            if (!this.isUiRestrictedByOnlyAdmin()) {
                this.getEmptyTextView().setText(2131889857);
            }
            this.getPreferenceScreen().removeAll();
            return;
        }
        this.setEmptyView((View)this.getEmptyTextView());
        this.getEmptyTextView().setText(2131889841);
        this.mConnectivityManager.registerNetworkCallback(VpnSettings.VPN_REQUEST, this.mNetworkCallback);
        (this.mUpdaterThread = new HandlerThread("Refresh VPN list in background")).start();
        (this.mUpdater = new Handler(this.mUpdaterThread.getLooper(), (Handler$Callback)this)).sendEmptyMessage(0);
    }
    
    @VisibleForTesting
    public void setShownPreferences(final Collection<Preference> collection) {
        this.mLegacyVpnPreferences.values().retainAll(collection);
        this.mAppPreferences.values().retainAll(collection);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        for (int i = preferenceScreen.getPreferenceCount() - 1; i >= 0; --i) {
            final Preference preference = preferenceScreen.getPreference(i);
            if (collection.contains(preference)) {
                collection.remove(preference);
            }
            else {
                preferenceScreen.removePreference(preference);
            }
        }
        final Iterator<?> iterator = collection.iterator();
        while (iterator.hasNext()) {
            preferenceScreen.addPreference((Preference)iterator.next());
        }
    }
    
    @VisibleForTesting
    static class UpdatePreferences implements Runnable
    {
        private Set<AppVpnInfo> alwaysOnAppVpnInfos;
        private Set<AppVpnInfo> connectedAppVpns;
        private Map<String, LegacyVpnInfo> connectedLegacyVpns;
        private String lockdownVpnKey;
        private final VpnSettings mSettings;
        private List<AppVpnInfo> vpnApps;
        private List<VpnProfile> vpnProfiles;
        
        public UpdatePreferences(final VpnSettings mSettings) {
            this.vpnProfiles = Collections.emptyList();
            this.vpnApps = Collections.emptyList();
            this.connectedLegacyVpns = Collections.emptyMap();
            this.connectedAppVpns = Collections.emptySet();
            this.alwaysOnAppVpnInfos = Collections.emptySet();
            this.lockdownVpnKey = null;
            this.mSettings = mSettings;
        }
        
        public final UpdatePreferences appVpns(final List<AppVpnInfo> vpnApps, final Set<AppVpnInfo> connectedAppVpns, final Set<AppVpnInfo> alwaysOnAppVpnInfos) {
            this.vpnApps = vpnApps;
            this.connectedAppVpns = connectedAppVpns;
            this.alwaysOnAppVpnInfos = alwaysOnAppVpnInfos;
            return this;
        }
        
        public final UpdatePreferences legacyVpns(final List<VpnProfile> vpnProfiles, final Map<String, LegacyVpnInfo> connectedLegacyVpns, final String lockdownVpnKey) {
            this.vpnProfiles = vpnProfiles;
            this.connectedLegacyVpns = connectedLegacyVpns;
            this.lockdownVpnKey = lockdownVpnKey;
            return this;
        }
        
        @Override
        public void run() {
            if (!this.mSettings.canAddPreferences()) {
                return;
            }
            final ArraySet shownPreferences = new ArraySet();
            final Iterator<VpnProfile> iterator = this.vpnProfiles.iterator();
            while (true) {
                final boolean hasNext = iterator.hasNext();
                final boolean b = false;
                if (!hasNext) {
                    break;
                }
                final VpnProfile vpnProfile = iterator.next();
                final LegacyVpnPreference orCreatePreference = this.mSettings.findOrCreatePreference(vpnProfile, true);
                if (this.connectedLegacyVpns.containsKey(vpnProfile.key)) {
                    orCreatePreference.setState(this.connectedLegacyVpns.get(vpnProfile.key).state);
                }
                else {
                    orCreatePreference.setState(LegacyVpnPreference.STATE_NONE);
                }
                boolean alwaysOn = b;
                if (this.lockdownVpnKey != null) {
                    alwaysOn = b;
                    if (this.lockdownVpnKey.equals(vpnProfile.key)) {
                        alwaysOn = true;
                    }
                }
                orCreatePreference.setAlwaysOn(alwaysOn);
                ((Set<LegacyVpnPreference>)shownPreferences).add(orCreatePreference);
            }
            for (final LegacyVpnInfo legacyVpnInfo : this.connectedLegacyVpns.values()) {
                final LegacyVpnPreference orCreatePreference2 = this.mSettings.findOrCreatePreference(new VpnProfile(legacyVpnInfo.key), false);
                orCreatePreference2.setState(legacyVpnInfo.state);
                orCreatePreference2.setAlwaysOn(this.lockdownVpnKey != null && this.lockdownVpnKey.equals(legacyVpnInfo.key));
                ((Set<LegacyVpnPreference>)shownPreferences).add(orCreatePreference2);
            }
            for (final AppVpnInfo appVpnInfo : this.vpnApps) {
                final AppPreference orCreatePreference3 = this.mSettings.findOrCreatePreference(appVpnInfo);
                if (this.connectedAppVpns.contains(appVpnInfo)) {
                    orCreatePreference3.setState(3);
                }
                else {
                    orCreatePreference3.setState(AppPreference.STATE_DISCONNECTED);
                }
                orCreatePreference3.setAlwaysOn(this.alwaysOnAppVpnInfos.contains(appVpnInfo));
                ((Set<LegacyVpnPreference>)shownPreferences).add((LegacyVpnPreference)orCreatePreference3);
            }
            this.mSettings.setShownPreferences((Collection<Preference>)shownPreferences);
        }
    }
}
