package com.android.settings;

import android.os.storage.IStorageManager;
import android.os.IBinder;
import android.app.StatusBarManager;
import android.os.Handler;
import android.app.Activity;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.content.ContentResolver;
import android.util.Log;
import java.util.Locale;
import android.os.storage.IStorageManager$Stub;
import android.os.ServiceManager;
import android.content.Intent;
import android.provider.Settings;
import android.content.Context;
import com.android.internal.widget.LockPatternUtils;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;

public class CryptKeeperConfirm extends InstrumentedFragment
{
    private View mContentView;
    private Button mFinalButton;
    private View.OnClickListener mFinalClickListener;
    
    public CryptKeeperConfirm() {
        this.mFinalClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (Utils.isMonkeyRunning()) {
                    return;
                }
                final LockPatternUtils lockPatternUtils = new LockPatternUtils((Context)CryptKeeperConfirm.this.getActivity());
                lockPatternUtils.setVisiblePatternEnabled(lockPatternUtils.isVisiblePatternEnabled(0), 0);
                if (lockPatternUtils.isOwnerInfoEnabled(0)) {
                    lockPatternUtils.setOwnerInfo(lockPatternUtils.getOwnerInfo(0), 0);
                }
                final ContentResolver contentResolver = CryptKeeperConfirm.this.getContext().getContentResolver();
                boolean b = true;
                if (Settings.System.getInt(contentResolver, "show_password", 1) == 0) {
                    b = false;
                }
                lockPatternUtils.setVisiblePasswordEnabled(b, 0);
                final Intent intent = new Intent((Context)CryptKeeperConfirm.this.getActivity(), (Class)Blank.class);
                intent.putExtras(CryptKeeperConfirm.this.getArguments());
                CryptKeeperConfirm.this.startActivity(intent);
                try {
                    IStorageManager$Stub.asInterface(ServiceManager.getService("mount")).setField("SystemLocale", Locale.getDefault().toLanguageTag());
                }
                catch (Exception ex) {
                    Log.e("CryptKeeperConfirm", "Error storing locale for decryption UI", (Throwable)ex);
                }
            }
        };
    }
    
    private void establishFinalConfirmationState() {
        (this.mFinalButton = (Button)this.mContentView.findViewById(2131362133)).setOnClickListener(this.mFinalClickListener);
    }
    
    @Override
    public int getMetricsCategory() {
        return 33;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131887163);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mContentView = layoutInflater.inflate(2131558509, (ViewGroup)null);
        this.establishFinalConfirmationState();
        return this.mContentView;
    }
    
    public static class Blank extends Activity
    {
        private Handler mHandler;
        
        public Blank() {
            this.mHandler = new Handler();
        }
        
        public void onCreate(final Bundle bundle) {
            super.onCreate(bundle);
            this.setContentView(2131558508);
            if (Utils.isMonkeyRunning()) {
                this.finish();
            }
            ((StatusBarManager)this.getSystemService("statusbar")).disable(58130432);
            this.mHandler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    final IBinder service = ServiceManager.getService("mount");
                    if (service == null) {
                        Log.e("CryptKeeper", "Failed to find the mount service");
                        Blank.this.finish();
                        return;
                    }
                    final IStorageManager interface1 = IStorageManager$Stub.asInterface(service);
                    try {
                        final Bundle extras = Blank.this.getIntent().getExtras();
                        interface1.encryptStorage(extras.getInt("type", -1), extras.getString("password"));
                    }
                    catch (Exception ex) {
                        Log.e("CryptKeeper", "Error while encrypting...", (Throwable)ex);
                    }
                }
            }, 700L);
        }
    }
}
