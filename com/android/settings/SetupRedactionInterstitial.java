package com.android.settings;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.Context;
import com.android.settings.notification.RedactionInterstitial;

public class SetupRedactionInterstitial extends RedactionInterstitial
{
    public static void setEnabled(final Context context, final boolean b) {
        final PackageManager packageManager = context.getPackageManager();
        final ComponentName componentName = new ComponentName(context, (Class)SetupRedactionInterstitial.class);
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 2;
        }
        packageManager.setComponentEnabledSetting(componentName, n, 1);
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", SetupRedactionInterstitialFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return SetupRedactionInterstitialFragment.class.getName().equals(s);
    }
    
    public static class SetupRedactionInterstitialFragment extends RedactionInterstitialFragment
    {
    }
}
