package com.android.settings;

import android.os.IBinder;
import android.content.pm.ResolveInfo;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import java.io.File;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageManager;
import com.android.settings.applications.ProcStatsData;
import org.json.JSONException;
import java.util.Iterator;
import android.net.NetworkTemplate;
import android.telephony.SubscriptionInfo;
import org.json.JSONArray;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.net.ConnectivityManager;
import android.content.Context;
import com.android.settingslib.net.DataUsageController;
import org.json.JSONObject;
import android.net.Uri;
import com.android.internal.annotations.VisibleForTesting;
import android.content.Intent;
import android.app.Service;

public class SettingsDumpService extends Service
{
    @VisibleForTesting
    static final Intent BROWSER_INTENT;
    @VisibleForTesting
    static final String KEY_ANOMALY_DETECTION = "anomaly_detection";
    @VisibleForTesting
    static final String KEY_DATAUSAGE = "datausage";
    @VisibleForTesting
    static final String KEY_DEFAULT_BROWSER_APP = "default_browser_app";
    @VisibleForTesting
    static final String KEY_MEMORY = "memory";
    @VisibleForTesting
    static final String KEY_SERVICE = "service";
    @VisibleForTesting
    static final String KEY_STORAGE = "storage";
    
    static {
        BROWSER_INTENT = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
    }
    
    private JSONObject dumpDataUsage() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        final DataUsageController dataUsageController = new DataUsageController((Context)this);
        final ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService((Class)ConnectivityManager.class);
        final SubscriptionManager from = SubscriptionManager.from((Context)this);
        final TelephonyManager from2 = TelephonyManager.from((Context)this);
        if (connectivityManager.isNetworkSupported(0)) {
            final JSONArray jsonArray = new JSONArray();
            for (final SubscriptionInfo subscriptionInfo : from.getAllSubscriptionInfoList()) {
                final JSONObject dumpDataUsage = this.dumpDataUsage(NetworkTemplate.buildTemplateMobileAll(from2.getSubscriberId(subscriptionInfo.getSubscriptionId())), dataUsageController);
                dumpDataUsage.put("subId", subscriptionInfo.getSubscriptionId());
                jsonArray.put((Object)dumpDataUsage);
            }
            jsonObject.put("cell", (Object)jsonArray);
        }
        if (connectivityManager.isNetworkSupported(1)) {
            jsonObject.put("wifi", (Object)this.dumpDataUsage(NetworkTemplate.buildTemplateWifiWildcard(), dataUsageController));
        }
        if (connectivityManager.isNetworkSupported(9)) {
            jsonObject.put("ethernet", (Object)this.dumpDataUsage(NetworkTemplate.buildTemplateEthernet(), dataUsageController));
        }
        return jsonObject;
    }
    
    private JSONObject dumpDataUsage(final NetworkTemplate networkTemplate, final DataUsageController dataUsageController) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        final DataUsageController.DataUsageInfo dataUsageInfo = dataUsageController.getDataUsageInfo(networkTemplate);
        jsonObject.put("carrier", (Object)dataUsageInfo.carrier);
        jsonObject.put("start", dataUsageInfo.startDate);
        jsonObject.put("usage", dataUsageInfo.usageLevel);
        jsonObject.put("warning", dataUsageInfo.warningLevel);
        jsonObject.put("limit", dataUsageInfo.limitLevel);
        return jsonObject;
    }
    
    private JSONObject dumpMemory() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        final ProcStatsData procStatsData = new ProcStatsData((Context)this, false);
        procStatsData.refreshStats(true);
        final ProcStatsData.MemInfo memInfo = procStatsData.getMemInfo();
        jsonObject.put("used", (Object)String.valueOf(memInfo.realUsedRam));
        jsonObject.put("free", (Object)String.valueOf(memInfo.realFreeRam));
        jsonObject.put("total", (Object)String.valueOf(memInfo.realTotalRam));
        jsonObject.put("state", procStatsData.getMemState());
        return jsonObject;
    }
    
    private JSONObject dumpStorage() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        for (final VolumeInfo volumeInfo : ((StorageManager)this.getSystemService((Class)StorageManager.class)).getVolumes()) {
            final JSONObject jsonObject2 = new JSONObject();
            if (volumeInfo.isMountedReadable()) {
                final File path = volumeInfo.getPath();
                jsonObject2.put("used", (Object)String.valueOf(path.getTotalSpace() - path.getFreeSpace()));
                jsonObject2.put("total", (Object)String.valueOf(path.getTotalSpace()));
            }
            jsonObject2.put("path", (Object)volumeInfo.getInternalPath());
            jsonObject2.put("state", volumeInfo.getState());
            jsonObject2.put("stateDesc", volumeInfo.getStateDescription());
            jsonObject2.put("description", (Object)volumeInfo.getDescription());
            jsonObject.put(volumeInfo.getId(), (Object)jsonObject2);
        }
        return jsonObject;
    }
    
    protected void dump(final FileDescriptor fileDescriptor, final PrintWriter printWriter, String[] array) {
        array = (String[])(Object)new JSONObject();
        try {
            ((JSONObject)(Object)array).put("service", (Object)"Settings State");
            ((JSONObject)(Object)array).put("storage", (Object)this.dumpStorage());
            ((JSONObject)(Object)array).put("datausage", (Object)this.dumpDataUsage());
            ((JSONObject)(Object)array).put("memory", (Object)this.dumpMemory());
            ((JSONObject)(Object)array).put("default_browser_app", (Object)this.dumpDefaultBrowser());
            ((JSONObject)(Object)array).put("anomaly_detection", (Object)this.dumpAnomalyDetection());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        printWriter.println(array);
    }
    
    @VisibleForTesting
    JSONObject dumpAnomalyDetection() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("anomaly_config_version", (Object)String.valueOf(this.getSharedPreferences("anomaly_pref", 0).getInt("anomaly_config_version", 0)));
        return jsonObject;
    }
    
    @VisibleForTesting
    String dumpDefaultBrowser() {
        final ResolveInfo resolveActivity = this.getPackageManager().resolveActivity(SettingsDumpService.BROWSER_INTENT, 65536);
        if (resolveActivity != null && !resolveActivity.activityInfo.packageName.equals("android")) {
            return resolveActivity.activityInfo.packageName;
        }
        return null;
    }
    
    public IBinder onBind(final Intent intent) {
        return null;
    }
}
