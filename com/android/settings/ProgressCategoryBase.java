package com.android.settings;

import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.PreferenceCategory;

public abstract class ProgressCategoryBase extends PreferenceCategory
{
    public ProgressCategoryBase(final Context context) {
        super(context);
    }
    
    public ProgressCategoryBase(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public ProgressCategoryBase(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public ProgressCategoryBase(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
}
