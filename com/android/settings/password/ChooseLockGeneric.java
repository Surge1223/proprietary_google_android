package com.android.settings.password;

import android.app.FragmentManager;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import com.android.settings.Utils;
import android.os.Bundle;
import android.text.TextUtils;
import com.android.settings.fingerprint.FingerprintEnrollFindSensor;
import com.android.settings.EncryptionInterstitial;
import com.android.settingslib.RestrictedPreference;
import com.android.settingslib.RestrictedLockUtils;
import android.widget.TextView;
import android.support.v7.preference.PreferenceScreen;
import android.os.storage.StorageManager;
import android.util.EventLog;
import android.support.v7.preference.Preference;
import java.util.List;
import android.content.pm.UserInfo;
import android.util.Log;
import android.hardware.fingerprint.FingerprintManager$RemovalCallback;
import android.hardware.fingerprint.Fingerprint;
import android.app.Activity;
import android.view.accessibility.AccessibilityManager;
import android.os.UserHandle;
import android.content.Context;
import android.content.ComponentName;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import android.security.KeyStore;
import android.hardware.fingerprint.FingerprintManager;
import android.app.admin.DevicePolicyManager;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Intent;
import android.app.Fragment;
import com.android.settings.SettingsActivity;

public class ChooseLockGeneric extends SettingsActivity
{
    Class<? extends Fragment> getFragmentClass() {
        return ChooseLockGenericFragment.class;
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", this.getFragmentClass().getName());
        final String action = intent.getAction();
        if ("android.app.action.SET_NEW_PASSWORD".equals(action) || "android.app.action.SET_NEW_PARENT_PROFILE_PASSWORD".equals(action)) {
            intent.putExtra(":settings:hide_drawer", true);
        }
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return ChooseLockGenericFragment.class.getName().equals(s);
    }
    
    public static class ChooseLockGenericFragment extends SettingsPreferenceFragment
    {
        private long mChallenge;
        private ChooseLockSettingsHelper mChooseLockSettingsHelper;
        private ChooseLockGenericController mController;
        private DevicePolicyManager mDPM;
        private boolean mEncryptionRequestDisabled;
        private int mEncryptionRequestQuality;
        private FingerprintManager mFingerprintManager;
        private boolean mForChangeCredRequiredForBoot;
        protected boolean mForFingerprint;
        private boolean mHasChallenge;
        private boolean mHideDrawer;
        private boolean mIsSetNewPassword;
        private KeyStore mKeyStore;
        private LockPatternUtils mLockPatternUtils;
        private ManagedLockPasswordProvider mManagedPasswordProvider;
        private boolean mPasswordConfirmed;
        private int mUserId;
        private UserManager mUserManager;
        private String mUserPassword;
        private boolean mWaitingForConfirmation;
        
        public ChooseLockGenericFragment() {
            this.mHasChallenge = false;
            this.mPasswordConfirmed = false;
            this.mWaitingForConfirmation = false;
            this.mForChangeCredRequiredForBoot = false;
            this.mHideDrawer = false;
            this.mIsSetNewPassword = false;
            this.mForFingerprint = false;
        }
        
        private Intent getIntentForUnlockMethod(final int n) {
            Intent intent = null;
            if (n >= 524288) {
                intent = this.getLockManagedPasswordIntent(this.mUserPassword);
            }
            else if (n >= 131072) {
                int passwordMinimumLength;
                if ((passwordMinimumLength = this.mDPM.getPasswordMinimumLength((ComponentName)null, this.mUserId)) < 4) {
                    passwordMinimumLength = 4;
                }
                intent = this.getLockPasswordIntent(n, passwordMinimumLength, this.mDPM.getPasswordMaximumLength(n));
            }
            else if (n == 65536) {
                intent = this.getLockPatternIntent();
            }
            if (intent != null) {
                intent.putExtra(":settings:hide_drawer", this.mHideDrawer);
            }
            return intent;
        }
        
        private String getKeyForCurrent() {
            final int credentialOwnerProfile = UserManager.get(this.getContext()).getCredentialOwnerProfile(this.mUserId);
            if (this.mLockPatternUtils.isLockScreenDisabled(credentialOwnerProfile)) {
                return ScreenLockType.NONE.preferenceKey;
            }
            final ScreenLockType fromQuality = ScreenLockType.fromQuality(this.mLockPatternUtils.getKeyguardStoredPasswordQuality(credentialOwnerProfile));
            String preferenceKey;
            if (fromQuality != null) {
                preferenceKey = fromQuality.preferenceKey;
            }
            else {
                preferenceKey = null;
            }
            return preferenceKey;
        }
        
        private int getResIdForFactoryResetProtectionWarningMessage() {
            final boolean b = this.mFingerprintManager != null && this.mFingerprintManager.isHardwareDetected() && this.mFingerprintManager.hasEnrolledFingerprints(this.mUserId);
            final boolean managedProfile = UserManager.get((Context)this.getActivity()).isManagedProfile(this.mUserId);
            final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mUserId);
            if (keyguardStoredPasswordQuality != 65536) {
                if (keyguardStoredPasswordQuality != 131072 && keyguardStoredPasswordQuality != 196608) {
                    if (keyguardStoredPasswordQuality != 262144 && keyguardStoredPasswordQuality != 327680 && keyguardStoredPasswordQuality != 393216 && keyguardStoredPasswordQuality != 524288) {
                        if (b && managedProfile) {
                            return 2131889565;
                        }
                        if (b && !managedProfile) {
                            return 2131889564;
                        }
                        if (managedProfile) {
                            return 2131889566;
                        }
                        return 2131889563;
                    }
                    else {
                        if (b && managedProfile) {
                            return 2131889553;
                        }
                        if (b && !managedProfile) {
                            return 2131889552;
                        }
                        if (managedProfile) {
                            return 2131889554;
                        }
                        return 2131889551;
                    }
                }
                else {
                    if (b && managedProfile) {
                        return 2131889561;
                    }
                    if (b && !managedProfile) {
                        return 2131889560;
                    }
                    if (managedProfile) {
                        return 2131889562;
                    }
                    return 2131889559;
                }
            }
            else {
                if (b && managedProfile) {
                    return 2131889557;
                }
                if (b && !managedProfile) {
                    return 2131889556;
                }
                if (managedProfile) {
                    return 2131889558;
                }
                return 2131889555;
            }
        }
        
        private int getResIdForFactoryResetProtectionWarningTitle() {
            int n;
            if (UserManager.get((Context)this.getActivity()).isManagedProfile(this.mUserId)) {
                n = 2131889569;
            }
            else {
                n = 2131889568;
            }
            return n;
        }
        
        private boolean isUnlockMethodSecure(final String s) {
            return !ScreenLockType.SWIPE.preferenceKey.equals(s) && !ScreenLockType.NONE.preferenceKey.equals(s);
        }
        
        private void maybeEnableEncryption(int mEncryptionRequestQuality, final boolean mEncryptionRequestDisabled) {
            final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)this.getSystemService("device_policy");
            if (UserManager.get((Context)this.getActivity()).isAdminUser() && this.mUserId == UserHandle.myUserId() && LockPatternUtils.isDeviceEncryptionEnabled() && !LockPatternUtils.isFileEncryptionEnabled() && !devicePolicyManager.getDoNotAskCredentialsOnBoot()) {
                this.mEncryptionRequestQuality = mEncryptionRequestQuality;
                this.mEncryptionRequestDisabled = mEncryptionRequestDisabled;
                final Intent intentForUnlockMethod = this.getIntentForUnlockMethod(mEncryptionRequestQuality);
                intentForUnlockMethod.putExtra("for_cred_req_boot", this.mForChangeCredRequiredForBoot);
                final Activity activity = this.getActivity();
                final Intent encryptionInterstitialIntent = this.getEncryptionInterstitialIntent((Context)activity, mEncryptionRequestQuality, this.mLockPatternUtils.isCredentialRequiredToDecrypt(AccessibilityManager.getInstance((Context)activity).isEnabled() ^ true), intentForUnlockMethod);
                encryptionInterstitialIntent.putExtra("for_fingerprint", this.mForFingerprint);
                encryptionInterstitialIntent.putExtra(":settings:hide_drawer", this.mHideDrawer);
                if (this.mIsSetNewPassword && this.mHasChallenge) {
                    mEncryptionRequestQuality = 103;
                }
                else {
                    mEncryptionRequestQuality = 101;
                }
                this.startActivityForResult(encryptionInterstitialIntent, mEncryptionRequestQuality);
            }
            else {
                if (this.mForChangeCredRequiredForBoot) {
                    this.finish();
                    return;
                }
                this.updateUnlockMethodAndFinish(mEncryptionRequestQuality, mEncryptionRequestDisabled, false);
            }
        }
        
        private void removeAllFingerprintForUserAndFinish(final int activeUser) {
            if (this.mFingerprintManager != null && this.mFingerprintManager.isHardwareDetected()) {
                if (this.mFingerprintManager.hasEnrolledFingerprints(activeUser)) {
                    this.mFingerprintManager.setActiveUser(activeUser);
                    this.mFingerprintManager.remove(new Fingerprint((CharSequence)null, activeUser, 0, 0L), activeUser, (FingerprintManager$RemovalCallback)new FingerprintManager$RemovalCallback() {
                        public void onRemovalError(final Fingerprint fingerprint, final int n, final CharSequence charSequence) {
                            Log.e("ChooseLockGenericFragment", String.format("Can't remove fingerprint %d in group %d. Reason: %s", fingerprint.getFingerId(), fingerprint.getGroupId(), charSequence));
                        }
                        
                        public void onRemovalSucceeded(final Fingerprint fingerprint, final int n) {
                            if (n == 0) {
                                ChooseLockGenericFragment.this.removeManagedProfileFingerprintsAndFinishIfNecessary(activeUser);
                            }
                        }
                    });
                }
                else {
                    this.removeManagedProfileFingerprintsAndFinishIfNecessary(activeUser);
                }
            }
            else {
                this.finish();
            }
        }
        
        private void removeManagedProfileFingerprintsAndFinishIfNecessary(int n) {
            if (this.mFingerprintManager != null && this.mFingerprintManager.isHardwareDetected()) {
                this.mFingerprintManager.setActiveUser(UserHandle.myUserId());
            }
            boolean b = false;
            if (!this.mUserManager.getUserInfo(n).isManagedProfile()) {
                final List profiles = this.mUserManager.getProfiles(n);
                final int size = profiles.size();
                n = 0;
                while (true) {
                    b = b;
                    if (n >= size) {
                        break;
                    }
                    final UserInfo userInfo = profiles.get(n);
                    if (userInfo.isManagedProfile() && !this.mLockPatternUtils.isSeparateProfileChallengeEnabled(userInfo.id)) {
                        this.removeAllFingerprintForUserAndFinish(userInfo.id);
                        b = true;
                        break;
                    }
                    ++n;
                }
            }
            if (!b) {
                this.finish();
            }
        }
        
        private void setPreferenceSummary(final ScreenLockType screenLockType, final int summary) {
            final Preference preference = this.findPreference(screenLockType.preferenceKey);
            if (preference != null) {
                preference.setSummary(summary);
            }
        }
        
        private void setPreferenceTitle(final ScreenLockType screenLockType, final int title) {
            final Preference preference = this.findPreference(screenLockType.preferenceKey);
            if (preference != null) {
                preference.setTitle(title);
            }
        }
        
        private void setPreferenceTitle(final ScreenLockType screenLockType, final CharSequence title) {
            final Preference preference = this.findPreference(screenLockType.preferenceKey);
            if (preference != null) {
                preference.setTitle(title);
            }
        }
        
        private boolean setUnlockMethod(final String s) {
            EventLog.writeEvent(90200, s);
            final ScreenLockType fromKey = ScreenLockType.fromKey(s);
            if (fromKey != null) {
                switch (fromKey) {
                    case PATTERN:
                    case PIN:
                    case PASSWORD:
                    case MANAGED: {
                        this.maybeEnableEncryption(fromKey.defaultQuality, false);
                        return true;
                    }
                    case NONE:
                    case SWIPE: {
                        this.updateUnlockMethodAndFinish(fromKey.defaultQuality, fromKey == ScreenLockType.NONE, false);
                        return true;
                    }
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Encountered unknown unlock method to set: ");
            sb.append(s);
            Log.e("ChooseLockGenericFragment", sb.toString());
            return false;
        }
        
        private void showFactoryResetProtectionWarningDialog(final String s) {
            FactoryResetProtectionWarningDialog.newInstance(this.getResIdForFactoryResetProtectionWarningTitle(), this.getResIdForFactoryResetProtectionWarningMessage(), s).show(this.getChildFragmentManager(), "frp_warning_dialog");
        }
        
        private void updateCurrentPreference() {
            final Preference preference = this.findPreference(this.getKeyForCurrent());
            if (preference != null) {
                preference.setSummary(2131887191);
            }
        }
        
        private void updatePreferenceSummaryIfNeeded() {
            if (!StorageManager.isBlockEncrypted()) {
                return;
            }
            if (StorageManager.isNonDefaultBlockEncrypted()) {
                return;
            }
            if (AccessibilityManager.getInstance((Context)this.getActivity()).getEnabledAccessibilityServiceList(-1).isEmpty()) {
                return;
            }
            this.setPreferenceSummary(ScreenLockType.PATTERN, 2131888972);
            this.setPreferenceSummary(ScreenLockType.PIN, 2131888972);
            this.setPreferenceSummary(ScreenLockType.PASSWORD, 2131888972);
            this.setPreferenceSummary(ScreenLockType.MANAGED, 2131888972);
        }
        
        private void updatePreferenceText() {
            if (this.mForFingerprint) {
                this.setPreferenceTitle(ScreenLockType.PATTERN, 2131887667);
                this.setPreferenceTitle(ScreenLockType.PIN, 2131887668);
                this.setPreferenceTitle(ScreenLockType.PASSWORD, 2131887666);
            }
            if (this.mManagedPasswordProvider.isSettingManagedPasswordSupported()) {
                this.setPreferenceTitle(ScreenLockType.MANAGED, this.mManagedPasswordProvider.getPickerOptionTitle(this.mForFingerprint));
            }
            else {
                this.removePreference(ScreenLockType.MANAGED.preferenceKey);
            }
            if (!this.mForFingerprint || !this.mIsSetNewPassword) {
                this.removePreference("unlock_skip_fingerprint");
            }
        }
        
        private void updatePreferencesOrFinish(final boolean b) {
            final Intent intent = this.getActivity().getIntent();
            final int intExtra = intent.getIntExtra("lockscreen.password_type", -1);
            if (intExtra == -1) {
                final int upgradeQuality = this.mController.upgradeQuality(intent.getIntExtra("minimum_quality", -1));
                final boolean booleanExtra = intent.getBooleanExtra("hide_disabled_prefs", false);
                final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
                if (preferenceScreen != null) {
                    preferenceScreen.removeAll();
                }
                this.addPreferences();
                this.disableUnusablePreferences(upgradeQuality, booleanExtra);
                this.updatePreferenceText();
                this.updateCurrentPreference();
                this.updatePreferenceSummaryIfNeeded();
            }
            else if (!b) {
                this.updateUnlockMethodAndFinish(intExtra, false, true);
            }
        }
        
        protected void addHeaderView() {
            if (this.mForFingerprint) {
                this.setHeaderView(2131558482);
                if (this.mIsSetNewPassword) {
                    this.getHeaderView().findViewById(2131362150).setText(2131887670);
                }
            }
        }
        
        protected void addPreferences() {
            this.addPreferencesFromResource(2132082827);
            this.findPreference(ScreenLockType.NONE.preferenceKey).setViewId(2131362346);
            this.findPreference("unlock_skip_fingerprint").setViewId(2131362346);
            this.findPreference(ScreenLockType.PIN.preferenceKey).setViewId(2131362348);
            this.findPreference(ScreenLockType.PASSWORD.preferenceKey).setViewId(2131362347);
        }
        
        protected boolean canRunBeforeDeviceProvisioned() {
            return false;
        }
        
        protected void disableUnusablePreferences(final int n, final boolean b) {
            this.disableUnusablePreferencesImpl(n, b);
        }
        
        protected void disableUnusablePreferencesImpl(final int n, final boolean b) {
            final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
            final int passwordQuality = this.mDPM.getPasswordQuality((ComponentName)null, this.mUserId);
            final RestrictedLockUtils.EnforcedAdmin checkIfPasswordQualityIsSet = RestrictedLockUtils.checkIfPasswordQualityIsSet((Context)this.getActivity(), this.mUserId);
            for (final ScreenLockType screenLockType : ScreenLockType.values()) {
                final Preference preference = this.findPreference(screenLockType.preferenceKey);
                if (preference instanceof RestrictedPreference) {
                    final boolean screenLockVisible = this.mController.isScreenLockVisible(screenLockType);
                    final boolean screenLockEnabled = this.mController.isScreenLockEnabled(screenLockType, n);
                    final boolean screenLockDisabledByAdmin = this.mController.isScreenLockDisabledByAdmin(screenLockType, passwordQuality);
                    boolean b2 = screenLockVisible;
                    if (b) {
                        b2 = (screenLockVisible && screenLockEnabled);
                    }
                    if (!b2) {
                        preferenceScreen.removePreference(preference);
                    }
                    else if (screenLockDisabledByAdmin && checkIfPasswordQualityIsSet != null) {
                        ((RestrictedPreference)preference).setDisabledByAdmin(checkIfPasswordQualityIsSet);
                    }
                    else if (!screenLockEnabled) {
                        ((RestrictedPreference)preference).setDisabledByAdmin(null);
                        preference.setSummary(2131889572);
                        preference.setEnabled(false);
                    }
                    else {
                        ((RestrictedPreference)preference).setDisabledByAdmin(null);
                    }
                }
            }
        }
        
        protected Intent getEncryptionInterstitialIntent(final Context context, final int n, final boolean b, final Intent intent) {
            return EncryptionInterstitial.createStartIntent(context, n, b, intent);
        }
        
        protected Intent getFindSensorIntent(final Context context) {
            return new Intent(context, (Class)FingerprintEnrollFindSensor.class);
        }
        
        @Override
        public int getHelpResource() {
            return 2131887802;
        }
        
        protected Intent getLockManagedPasswordIntent(final String s) {
            return this.mManagedPasswordProvider.createIntent(false, s);
        }
        
        protected Intent getLockPasswordIntent(final int passwordQuality, final int n, final int n2) {
            final ChooseLockPassword.IntentBuilder setUserId = new ChooseLockPassword.IntentBuilder(this.getContext()).setPasswordQuality(passwordQuality).setPasswordLengthRange(n, n2).setForFingerprint(this.mForFingerprint).setUserId(this.mUserId);
            if (this.mHasChallenge) {
                setUserId.setChallenge(this.mChallenge);
            }
            if (this.mUserPassword != null) {
                setUserId.setPassword(this.mUserPassword);
            }
            return setUserId.build();
        }
        
        protected Intent getLockPatternIntent() {
            final ChooseLockPattern.IntentBuilder setUserId = new ChooseLockPattern.IntentBuilder(this.getContext()).setForFingerprint(this.mForFingerprint).setUserId(this.mUserId);
            if (this.mHasChallenge) {
                setUserId.setChallenge(this.mChallenge);
            }
            if (this.mUserPassword != null) {
                setUserId.setPattern(this.mUserPassword);
            }
            return setUserId.build();
        }
        
        @Override
        public int getMetricsCategory() {
            return 27;
        }
        
        public void onActivityResult(final int n, int n2, final Intent intent) {
            super.onActivityResult(n, n2, intent);
            this.mWaitingForConfirmation = false;
            final int n3 = -1;
            if (n == 100 && n2 == -1) {
                this.mPasswordConfirmed = true;
                this.mUserPassword = intent.getStringExtra("password");
                this.updatePreferencesOrFinish(false);
                if (this.mForChangeCredRequiredForBoot) {
                    if (!TextUtils.isEmpty((CharSequence)this.mUserPassword)) {
                        this.maybeEnableEncryption(this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mUserId), false);
                    }
                    else {
                        this.finish();
                    }
                }
            }
            else if (n != 102 && n != 101) {
                if (n == 103 && n2 == 1) {
                    final Intent findSensorIntent = this.getFindSensorIntent((Context)this.getActivity());
                    if (intent != null) {
                        findSensorIntent.putExtras(intent.getExtras());
                    }
                    findSensorIntent.putExtra("android.intent.extra.USER_ID", this.mUserId);
                    this.startActivity(findSensorIntent);
                    this.finish();
                }
                else if (n == 104) {
                    if (n2 != 0) {
                        final Activity activity = this.getActivity();
                        if (n2 == 1) {
                            n2 = n3;
                        }
                        activity.setResult(n2, intent);
                        this.finish();
                    }
                }
                else {
                    this.getActivity().setResult(0);
                    this.finish();
                }
            }
            else if (n2 == 0 && !this.mForChangeCredRequiredForBoot) {
                if (this.getIntent().getIntExtra("lockscreen.password_type", -1) != -1) {
                    this.getActivity().setResult(0, intent);
                    this.finish();
                }
            }
            else {
                this.getActivity().setResult(n2, intent);
                this.finish();
            }
            if (n == 0 && this.mForChangeCredRequiredForBoot) {
                this.finish();
            }
        }
        
        @Override
        public void onCreate(final Bundle bundle) {
            super.onCreate(bundle);
            final Activity activity = this.getActivity();
            if (!Utils.isDeviceProvisioned((Context)activity) && !this.canRunBeforeDeviceProvisioned()) {
                activity.finish();
                return;
            }
            final String action = this.getActivity().getIntent().getAction();
            this.mFingerprintManager = Utils.getFingerprintManagerOrNull((Context)this.getActivity());
            this.mDPM = (DevicePolicyManager)this.getSystemService("device_policy");
            this.mKeyStore = KeyStore.getInstance();
            this.mChooseLockSettingsHelper = new ChooseLockSettingsHelper(this.getActivity());
            this.mLockPatternUtils = new LockPatternUtils((Context)this.getActivity());
            final boolean equals = "android.app.action.SET_NEW_PARENT_PROFILE_PASSWORD".equals(action);
            final boolean b = false;
            final boolean b2 = true;
            this.mIsSetNewPassword = (equals || "android.app.action.SET_NEW_PASSWORD".equals(action));
            final boolean booleanExtra = this.getActivity().getIntent().getBooleanExtra("confirm_credentials", true);
            if (this.getActivity() instanceof InternalActivity) {
                this.mPasswordConfirmed = (booleanExtra ^ true);
                this.mUserPassword = this.getActivity().getIntent().getStringExtra("password");
            }
            this.mHideDrawer = this.getActivity().getIntent().getBooleanExtra(":settings:hide_drawer", false);
            this.mHasChallenge = this.getActivity().getIntent().getBooleanExtra("has_challenge", false);
            this.mChallenge = this.getActivity().getIntent().getLongExtra("challenge", 0L);
            this.mForFingerprint = this.getActivity().getIntent().getBooleanExtra("for_fingerprint", false);
            this.mForChangeCredRequiredForBoot = (this.getArguments() != null && this.getArguments().getBoolean("for_cred_req_boot"));
            this.mUserManager = UserManager.get((Context)this.getActivity());
            if (bundle != null) {
                this.mPasswordConfirmed = bundle.getBoolean("password_confirmed");
                this.mWaitingForConfirmation = bundle.getBoolean("waiting_for_confirmation");
                this.mEncryptionRequestQuality = bundle.getInt("encrypt_requested_quality");
                this.mEncryptionRequestDisabled = bundle.getBoolean("encrypt_requested_disabled");
                if (this.mUserPassword == null) {
                    this.mUserPassword = bundle.getString("password");
                }
            }
            this.mUserId = Utils.getSecureTargetUser(this.getActivity().getActivityToken(), UserManager.get((Context)this.getActivity()), this.getArguments(), this.getActivity().getIntent().getExtras()).getIdentifier();
            this.mController = new ChooseLockGenericController(this.getContext(), this.mUserId);
            if ("android.app.action.SET_NEW_PASSWORD".equals(action) && UserManager.get((Context)this.getActivity()).isManagedProfile(this.mUserId) && this.mLockPatternUtils.isSeparateProfileChallengeEnabled(this.mUserId)) {
                this.getActivity().setTitle(2131888098);
            }
            this.mManagedPasswordProvider = ManagedLockPasswordProvider.get((Context)this.getActivity(), this.mUserId);
            if (this.mPasswordConfirmed) {
                this.updatePreferencesOrFinish(bundle != null && b2);
                if (this.mForChangeCredRequiredForBoot) {
                    this.maybeEnableEncryption(this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mUserId), false);
                }
            }
            else if (!this.mWaitingForConfirmation) {
                final ChooseLockSettingsHelper chooseLockSettingsHelper = new ChooseLockSettingsHelper(this.getActivity(), this);
                if ((!UserManager.get((Context)this.getActivity()).isManagedProfile(this.mUserId) || this.mLockPatternUtils.isSeparateProfileChallengeEnabled(this.mUserId) || this.mIsSetNewPassword) && chooseLockSettingsHelper.launchConfirmationActivity(100, this.getString(2131889578), true, this.mUserId)) {
                    this.mWaitingForConfirmation = true;
                }
                else {
                    this.mPasswordConfirmed = true;
                    boolean b3 = b;
                    if (bundle != null) {
                        b3 = true;
                    }
                    this.updatePreferencesOrFinish(b3);
                }
            }
            this.addHeaderView();
        }
        
        @Override
        public void onDestroy() {
            super.onDestroy();
        }
        
        @Override
        public boolean onPreferenceTreeClick(final Preference preference) {
            final String key = preference.getKey();
            if (!this.isUnlockMethodSecure(key) && this.mLockPatternUtils.isSecure(this.mUserId)) {
                this.showFactoryResetProtectionWarningDialog(key);
                return true;
            }
            if ("unlock_skip_fingerprint".equals(key)) {
                final Intent intent = new Intent((Context)this.getActivity(), (Class)InternalActivity.class);
                intent.setAction(this.getIntent().getAction());
                intent.putExtra("android.intent.extra.USER_ID", this.mUserId);
                intent.putExtra("confirm_credentials", this.mPasswordConfirmed ^ true);
                if (this.mUserPassword != null) {
                    intent.putExtra("password", this.mUserPassword);
                }
                this.startActivityForResult(intent, 104);
                return true;
            }
            return this.setUnlockMethod(key);
        }
        
        @Override
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putBoolean("password_confirmed", this.mPasswordConfirmed);
            bundle.putBoolean("waiting_for_confirmation", this.mWaitingForConfirmation);
            bundle.putInt("encrypt_requested_quality", this.mEncryptionRequestQuality);
            bundle.putBoolean("encrypt_requested_disabled", this.mEncryptionRequestDisabled);
            if (this.mUserPassword != null) {
                bundle.putString("password", this.mUserPassword);
            }
        }
        
        void updateUnlockMethodAndFinish(int upgradeQuality, final boolean b, final boolean b2) {
            if (!this.mPasswordConfirmed) {
                throw new IllegalStateException("Tried to update password without confirming it");
            }
            upgradeQuality = this.mController.upgradeQuality(upgradeQuality);
            final Intent intentForUnlockMethod = this.getIntentForUnlockMethod(upgradeQuality);
            if (intentForUnlockMethod != null) {
                if (this.getIntent().getBooleanExtra("show_options_button", false)) {
                    intentForUnlockMethod.putExtra("show_options_button", b2);
                }
                intentForUnlockMethod.putExtra("choose_lock_generic_extras", this.getIntent().getExtras());
                if (this.mIsSetNewPassword && this.mHasChallenge) {
                    upgradeQuality = 103;
                }
                else {
                    upgradeQuality = 102;
                }
                this.startActivityForResult(intentForUnlockMethod, upgradeQuality);
                return;
            }
            if (upgradeQuality == 0) {
                this.mChooseLockSettingsHelper.utils().clearLock(this.mUserPassword, this.mUserId);
                this.mChooseLockSettingsHelper.utils().setLockScreenDisabled(b, this.mUserId);
                this.getActivity().setResult(-1);
                this.removeAllFingerprintForUserAndFinish(this.mUserId);
            }
            else {
                this.removeAllFingerprintForUserAndFinish(this.mUserId);
            }
        }
        
        public static class FactoryResetProtectionWarningDialog extends InstrumentedDialogFragment
        {
            public static FactoryResetProtectionWarningDialog newInstance(final int n, final int n2, final String s) {
                final FactoryResetProtectionWarningDialog factoryResetProtectionWarningDialog = new FactoryResetProtectionWarningDialog();
                final Bundle arguments = new Bundle();
                arguments.putInt("titleRes", n);
                arguments.putInt("messageRes", n2);
                arguments.putString("unlockMethodToSet", s);
                factoryResetProtectionWarningDialog.setArguments(arguments);
                return factoryResetProtectionWarningDialog;
            }
            
            @Override
            public int getMetricsCategory() {
                return 528;
            }
            
            public Dialog onCreateDialog(Bundle arguments) {
                arguments = this.getArguments();
                return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(arguments.getInt("titleRes")).setMessage(arguments.getInt("messageRes")).setPositiveButton(2131889567, (DialogInterface$OnClickListener)new _$$Lambda$ChooseLockGeneric$ChooseLockGenericFragment$FactoryResetProtectionWarningDialog$Abdb_f1FnDmiVy0c3RZHU7n2B2k(this, arguments)).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)new _$$Lambda$ChooseLockGeneric$ChooseLockGenericFragment$FactoryResetProtectionWarningDialog$YUiXVX_8NlQHl0UI000UMbpVL0U(this)).create();
            }
            
            public void show(final FragmentManager fragmentManager, final String s) {
                if (fragmentManager.findFragmentByTag(s) == null) {
                    super.show(fragmentManager, s);
                }
            }
        }
    }
    
    public static class InternalActivity extends ChooseLockGeneric
    {
    }
}
