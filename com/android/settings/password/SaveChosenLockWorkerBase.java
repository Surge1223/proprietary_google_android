package com.android.settings.password;

import android.os.AsyncTask;
import android.content.Context;
import android.os.UserManager;
import android.os.Bundle;
import com.android.internal.widget.LockPatternUtils;
import android.content.Intent;
import android.app.Fragment;

abstract class SaveChosenLockWorkerBase extends Fragment
{
    private boolean mBlocking;
    protected long mChallenge;
    private boolean mFinished;
    protected boolean mHasChallenge;
    private Listener mListener;
    private Intent mResultData;
    protected int mUserId;
    protected LockPatternUtils mUtils;
    protected boolean mWasSecureBefore;
    
    protected void finish(final Intent mResultData) {
        this.mFinished = true;
        this.mResultData = mResultData;
        if (this.mListener != null) {
            this.mListener.onChosenLockSaveFinished(this.mWasSecureBefore, this.mResultData);
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setRetainInstance(true);
    }
    
    protected void prepare(final LockPatternUtils mUtils, final boolean credentialRequiredToDecrypt, final boolean mHasChallenge, final long mChallenge, final int mUserId) {
        this.mUtils = mUtils;
        this.mUserId = mUserId;
        this.mHasChallenge = mHasChallenge;
        this.mChallenge = mChallenge;
        this.mWasSecureBefore = this.mUtils.isSecure(this.mUserId);
        final Context context = this.getContext();
        if (context == null || UserManager.get(context).getUserInfo(this.mUserId).isPrimary()) {
            this.mUtils.setCredentialRequiredToDecrypt(credentialRequiredToDecrypt);
        }
        this.mFinished = false;
        this.mResultData = null;
    }
    
    protected abstract Intent saveAndVerifyInBackground();
    
    public void setBlocking(final boolean mBlocking) {
        this.mBlocking = mBlocking;
    }
    
    public void setListener(final Listener mListener) {
        if (this.mListener == mListener) {
            return;
        }
        this.mListener = mListener;
        if (this.mFinished && this.mListener != null) {
            this.mListener.onChosenLockSaveFinished(this.mWasSecureBefore, this.mResultData);
        }
    }
    
    protected void start() {
        if (this.mBlocking) {
            this.finish(this.saveAndVerifyInBackground());
        }
        else {
            new Task().execute((Object[])new Void[0]);
        }
    }
    
    interface Listener
    {
        void onChosenLockSaveFinished(final boolean p0, final Intent p1);
    }
    
    private class Task extends AsyncTask<Void, Void, Intent>
    {
        protected Intent doInBackground(final Void... array) {
            return SaveChosenLockWorkerBase.this.saveAndVerifyInBackground();
        }
        
        protected void onPostExecute(final Intent intent) {
            SaveChosenLockWorkerBase.this.finish(intent);
        }
    }
}
