package com.android.settings.password;

import android.view.View.OnClickListener;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.SetupRedactionInterstitial;
import android.view.View;
import android.widget.Button;
import android.app.Fragment;
import android.content.Intent;
import android.content.Context;

public class SetupChooseLockPattern extends ChooseLockPattern
{
    public static Intent modifyIntentForSetup(final Context context, final Intent intent) {
        intent.setClass(context, (Class)SetupChooseLockPattern.class);
        return intent;
    }
    
    @Override
    Class<? extends Fragment> getFragmentClass() {
        return SetupChooseLockPatternFragment.class;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return SetupChooseLockPatternFragment.class.getName().equals(s);
    }
    
    public static class SetupChooseLockPatternFragment extends ChooseLockPatternFragment implements OnLockTypeSelectedListener
    {
        private Button mOptionsButton;
        
        @Override
        protected Intent getRedactionInterstitialIntent(final Context context) {
            SetupRedactionInterstitial.setEnabled(context, true);
            return null;
        }
        
        @Override
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
            if (!this.getResources().getBoolean(2131034126)) {
                (this.mOptionsButton = (Button)onCreateView.findViewById(2131362548)).setOnClickListener((View.OnClickListener)new _$$Lambda$SetupChooseLockPattern$SetupChooseLockPatternFragment$oe1sL_LLbUw3chjlv8P3cpGYEWs(this));
            }
            if (!this.mForFingerprint) {
                final Button button = (Button)onCreateView.findViewById(2131362617);
                button.setVisibility(0);
                button.setOnClickListener((View.OnClickListener)new _$$Lambda$SetupChooseLockPattern$SetupChooseLockPatternFragment$klleXh_HZ7yoRQxNNtN_WzAt_fY(this));
            }
            return onCreateView;
        }
        
        @Override
        public void onLockTypeSelected(final ScreenLockType screenLockType) {
            if (ScreenLockType.PATTERN == screenLockType) {
                return;
            }
            ((ChooseLockTypeDialogFragment.OnLockTypeSelectedListener)this).startChooseLockActivity(screenLockType, this.getActivity());
        }
        
        @Override
        protected void updateStage(final Stage stage) {
            super.updateStage(stage);
            if (!this.getResources().getBoolean(2131034126) && this.mOptionsButton != null) {
                final Button mOptionsButton = this.mOptionsButton;
                int visibility;
                if (stage == Stage.Introduction) {
                    visibility = 0;
                }
                else {
                    visibility = 4;
                }
                mOptionsButton.setVisibility(visibility);
            }
        }
    }
}
