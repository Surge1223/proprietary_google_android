package com.android.settings.password;

import android.app.FragmentManager;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$ChooseLockGeneric$ChooseLockGenericFragment$FactoryResetProtectionWarningDialog$YUiXVX_8NlQHl0UI000UMbpVL0U implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        ChooseLockGeneric.ChooseLockGenericFragment.FactoryResetProtectionWarningDialog.lambda$onCreateDialog$1(this.f$0, dialogInterface, n);
    }
}
