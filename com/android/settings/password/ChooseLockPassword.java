package com.android.settings.password;

import com.android.internal.widget.LockPatternUtils$RequestThrottledException;
import android.os.Message;
import android.os.Handler;
import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.Insets;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.Utils;
import android.text.Selection;
import android.text.Spannable;
import com.android.settings.notification.RedactionInterstitial;
import android.text.Editable;
import android.app.admin.PasswordMetrics;
import android.util.Log;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import java.util.function.ToIntFunction;
import java.util.ArrayList;
import android.view.View;
import android.text.TextUtils;
import android.support.v7.widget.RecyclerView;
import com.android.internal.widget.TextViewInputDisabler;
import com.android.settings.widget.ImeAwareEditText;
import android.widget.TextView;
import com.android.internal.widget.LockPatternUtils;
import com.android.setupwizardlib.GlifLayout;
import android.widget.Button;
import android.widget.TextView$OnEditorActionListener;
import android.view.View.OnClickListener;
import android.text.TextWatcher;
import com.android.settings.core.InstrumentedFragment;
import android.widget.LinearLayout;
import android.os.Bundle;
import com.android.settings.SetupWizardUtils;
import android.content.res.Resources.Theme;
import android.content.Intent;
import android.app.Fragment;
import com.android.settings.SettingsActivity;

public class ChooseLockPassword extends SettingsActivity
{
    Class<? extends Fragment> getFragmentClass() {
        return ChooseLockPasswordFragment.class;
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", this.getFragmentClass().getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return ChooseLockPasswordFragment.class.getName().equals(s);
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        super.onApplyThemeResource(resources$Theme, SetupWizardUtils.getTheme(this.getIntent()), b);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        int n;
        if (this.getIntent().getBooleanExtra("for_fingerprint", false)) {
            n = 2131888117;
        }
        else {
            n = 2131888123;
        }
        this.setTitle(this.getText(n));
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public static class ChooseLockPasswordFragment extends InstrumentedFragment implements TextWatcher, View.OnClickListener, TextView$OnEditorActionListener, Listener
    {
        private long mChallenge;
        private ChooseLockSettingsHelper mChooseLockSettingsHelper;
        private String mChosenPassword;
        private Button mClearButton;
        private String mCurrentPassword;
        private String mFirstPin;
        protected boolean mForFingerprint;
        private boolean mHasChallenge;
        private boolean mHideDrawer;
        protected boolean mIsAlphaMode;
        private GlifLayout mLayout;
        private LockPatternUtils mLockPatternUtils;
        private TextView mMessage;
        private Button mNextButton;
        private ImeAwareEditText mPasswordEntry;
        private TextViewInputDisabler mPasswordEntryInputDisabler;
        private byte[] mPasswordHistoryHashFactor;
        private int mPasswordMaxLength;
        private int mPasswordMinLength;
        private int mPasswordMinLengthToFulfillAllPolicies;
        private int mPasswordMinLetters;
        private int mPasswordMinLowerCase;
        private int mPasswordMinNonLetter;
        private int mPasswordMinNumeric;
        private int mPasswordMinSymbols;
        private int mPasswordMinUpperCase;
        private PasswordRequirementAdapter mPasswordRequirementAdapter;
        private int[] mPasswordRequirements;
        private RecyclerView mPasswordRestrictionView;
        private int mRequestedQuality;
        private SaveAndFinishWorker mSaveAndFinishWorker;
        protected Button mSkipButton;
        private TextChangedHandler mTextChangedHandler;
        protected Stage mUiStage;
        protected int mUserId;
        
        public ChooseLockPasswordFragment() {
            this.mPasswordMinLength = 4;
            this.mPasswordMaxLength = 16;
            this.mPasswordMinLetters = 0;
            this.mPasswordMinUpperCase = 0;
            this.mPasswordMinLowerCase = 0;
            this.mPasswordMinSymbols = 0;
            this.mPasswordMinNumeric = 0;
            this.mPasswordMinNonLetter = 0;
            this.mPasswordMinLengthToFulfillAllPolicies = 0;
            this.mHideDrawer = false;
            this.mRequestedQuality = 131072;
            this.mUiStage = Stage.Introduction;
        }
        
        private int getMinLengthToFulfillAllPolicies() {
            return Math.max(this.mPasswordMinLetters, this.mPasswordMinUpperCase + this.mPasswordMinLowerCase) + Math.max(this.mPasswordMinNonLetter, this.mPasswordMinSymbols + this.mPasswordMinNumeric);
        }
        
        private byte[] getPasswordHistoryHashFactor() {
            if (this.mPasswordHistoryHashFactor == null) {
                this.mPasswordHistoryHashFactor = this.mLockPatternUtils.getPasswordHistoryHashFactor(this.mCurrentPassword, this.mUserId);
            }
            return this.mPasswordHistoryHashFactor;
        }
        
        private void processPasswordRequirements(final Intent intent) {
            final int requestedPasswordQuality = this.mLockPatternUtils.getRequestedPasswordQuality(this.mUserId);
            this.mRequestedQuality = Math.max(intent.getIntExtra("lockscreen.password_type", this.mRequestedQuality), requestedPasswordQuality);
            this.mPasswordMinLength = Math.max(Math.max(4, intent.getIntExtra("lockscreen.password_min", this.mPasswordMinLength)), this.mLockPatternUtils.getRequestedMinimumPasswordLength(this.mUserId));
            this.mPasswordMaxLength = intent.getIntExtra("lockscreen.password_max", this.mPasswordMaxLength);
            this.mPasswordMinLetters = Math.max(intent.getIntExtra("lockscreen.password_min_letters", this.mPasswordMinLetters), this.mLockPatternUtils.getRequestedPasswordMinimumLetters(this.mUserId));
            this.mPasswordMinUpperCase = Math.max(intent.getIntExtra("lockscreen.password_min_uppercase", this.mPasswordMinUpperCase), this.mLockPatternUtils.getRequestedPasswordMinimumUpperCase(this.mUserId));
            this.mPasswordMinLowerCase = Math.max(intent.getIntExtra("lockscreen.password_min_lowercase", this.mPasswordMinLowerCase), this.mLockPatternUtils.getRequestedPasswordMinimumLowerCase(this.mUserId));
            this.mPasswordMinNumeric = Math.max(intent.getIntExtra("lockscreen.password_min_numeric", this.mPasswordMinNumeric), this.mLockPatternUtils.getRequestedPasswordMinimumNumeric(this.mUserId));
            this.mPasswordMinSymbols = Math.max(intent.getIntExtra("lockscreen.password_min_symbols", this.mPasswordMinSymbols), this.mLockPatternUtils.getRequestedPasswordMinimumSymbols(this.mUserId));
            this.mPasswordMinNonLetter = Math.max(intent.getIntExtra("lockscreen.password_min_nonletter", this.mPasswordMinNonLetter), this.mLockPatternUtils.getRequestedPasswordMinimumNonLetter(this.mUserId));
            if (requestedPasswordQuality != 262144) {
                if (requestedPasswordQuality != 327680) {
                    if (requestedPasswordQuality != 393216) {
                        this.mPasswordMinNumeric = 0;
                        this.mPasswordMinLetters = 0;
                        this.mPasswordMinUpperCase = 0;
                        this.mPasswordMinLowerCase = 0;
                        this.mPasswordMinSymbols = 0;
                        this.mPasswordMinNonLetter = 0;
                    }
                }
                else {
                    if (this.mPasswordMinLetters == 0) {
                        this.mPasswordMinLetters = 1;
                    }
                    if (this.mPasswordMinNumeric == 0) {
                        this.mPasswordMinNumeric = 1;
                    }
                }
            }
            else if (this.mPasswordMinLetters == 0) {
                this.mPasswordMinLetters = 1;
            }
            this.mPasswordMinLengthToFulfillAllPolicies = this.getMinLengthToFulfillAllPolicies();
        }
        
        private void setHeaderText(final String headerText) {
            if (!TextUtils.isEmpty(this.mLayout.getHeaderText()) && this.mLayout.getHeaderText().toString().equals(headerText)) {
                return;
            }
            this.mLayout.setHeaderText(headerText);
        }
        
        private void setupPasswordRequirementsView(final View view) {
            final ArrayList<Integer> list = new ArrayList<Integer>();
            final ArrayList<String> list2 = new ArrayList<String>();
            if (this.mPasswordMinUpperCase > 0) {
                list.add(1);
                list2.add(this.getResources().getQuantityString(2131755041, this.mPasswordMinUpperCase, new Object[] { this.mPasswordMinUpperCase }));
            }
            if (this.mPasswordMinLowerCase > 0) {
                list.add(2);
                list2.add(this.getResources().getQuantityString(2131755037, this.mPasswordMinLowerCase, new Object[] { this.mPasswordMinLowerCase }));
            }
            if (this.mPasswordMinLetters > 0 && this.mPasswordMinLetters > this.mPasswordMinUpperCase + this.mPasswordMinLowerCase) {
                list.add(0);
                list2.add(this.getResources().getQuantityString(2131755036, this.mPasswordMinLetters, new Object[] { this.mPasswordMinLetters }));
            }
            if (this.mPasswordMinNumeric > 0) {
                list.add(4);
                list2.add(this.getResources().getQuantityString(2131755039, this.mPasswordMinNumeric, new Object[] { this.mPasswordMinNumeric }));
            }
            if (this.mPasswordMinSymbols > 0) {
                list.add(3);
                list2.add(this.getResources().getQuantityString(2131755040, this.mPasswordMinSymbols, new Object[] { this.mPasswordMinSymbols }));
            }
            if (this.mPasswordMinNonLetter > 0 && this.mPasswordMinNonLetter > this.mPasswordMinNumeric + this.mPasswordMinSymbols) {
                list.add(5);
                list2.add(this.getResources().getQuantityString(2131755038, this.mPasswordMinNonLetter, new Object[] { this.mPasswordMinNonLetter }));
            }
            this.mPasswordRequirements = list.stream().mapToInt((ToIntFunction<? super Object>)_$$Lambda$ChooseLockPassword$ChooseLockPasswordFragment$WFCgmpRIhPOiOzVHNaBhMh5zoJI.INSTANCE).toArray();
            (this.mPasswordRestrictionView = (RecyclerView)view.findViewById(2131362442)).setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager((Context)this.getActivity()));
            this.mPasswordRequirementAdapter = new PasswordRequirementAdapter();
            this.mPasswordRestrictionView.setAdapter((RecyclerView.Adapter)this.mPasswordRequirementAdapter);
        }
        
        private void startSaveAndFinish() {
            if (this.mSaveAndFinishWorker != null) {
                Log.w("ChooseLockPassword", "startSaveAndFinish with an existing SaveAndFinishWorker.");
                return;
            }
            this.mPasswordEntryInputDisabler.setInputEnabled(false);
            this.setNextEnabled(false);
            (this.mSaveAndFinishWorker = new SaveAndFinishWorker()).setListener((Listener)this);
            this.getFragmentManager().beginTransaction().add((Fragment)this.mSaveAndFinishWorker, "save_and_finish_worker").commit();
            this.getFragmentManager().executePendingTransactions();
            this.mSaveAndFinishWorker.start(this.mLockPatternUtils, this.getActivity().getIntent().getBooleanExtra("extra_require_password", true), this.mHasChallenge, this.mChallenge, this.mChosenPassword, this.mCurrentPassword, this.mRequestedQuality, this.mUserId);
        }
        
        private int toVisibility(final boolean b) {
            int n;
            if (b) {
                n = 0;
            }
            else {
                n = 8;
            }
            return n;
        }
        
        private int validatePassword(final String s) {
            final boolean b = false;
            final boolean b2 = false;
            final PasswordMetrics computeForPassword = PasswordMetrics.computeForPassword(s);
            int n;
            if (s.length() < this.mPasswordMinLength) {
                n = (b ? 1 : 0);
                if (this.mPasswordMinLength > this.mPasswordMinLengthToFulfillAllPolicies) {
                    n = (0x0 | 0x2);
                }
            }
            else if (s.length() > this.mPasswordMaxLength) {
                n = (0x0 | 0x4);
            }
            else {
                int n2 = b2 ? 1 : 0;
                if (this.mLockPatternUtils.getRequestedPasswordQuality(this.mUserId) == 196608) {
                    n2 = (b2 ? 1 : 0);
                    if (computeForPassword.numeric == s.length()) {
                        n2 = (b2 ? 1 : 0);
                        if (PasswordMetrics.maxLengthSequence(s) > 3) {
                            n2 = (0x0 | 0x10);
                        }
                    }
                }
                n = n2;
                if (this.mLockPatternUtils.checkPasswordHistory(s, this.getPasswordHistoryHashFactor(), this.mUserId)) {
                    n = (n2 | 0x20);
                }
            }
            final int n3 = 0;
            int n4 = 0;
            int n5;
            while (true) {
                n5 = n;
                if (n4 >= s.length()) {
                    break;
                }
                final char char1 = s.charAt(n4);
                if (char1 < ' ' || char1 > '\u007f') {
                    n5 = (n | 0x1);
                    break;
                }
                ++n4;
            }
            int n6 = 0;
            Label_0226: {
                if (this.mRequestedQuality != 131072) {
                    n6 = n5;
                    if (this.mRequestedQuality != 196608) {
                        break Label_0226;
                    }
                }
                if (computeForPassword.letters <= 0) {
                    n6 = n5;
                    if (computeForPassword.symbols <= 0) {
                        break Label_0226;
                    }
                }
                n6 = (n5 | 0x8);
            }
            int i = n3;
            int n7 = n6;
            while (i < this.mPasswordRequirements.length) {
                int n8 = 0;
                switch (this.mPasswordRequirements[i]) {
                    default: {
                        n8 = n7;
                        break;
                    }
                    case 5: {
                        n8 = n7;
                        if (computeForPassword.nonLetter < this.mPasswordMinNonLetter) {
                            n8 = (n7 | 0x800);
                            break;
                        }
                        break;
                    }
                    case 4: {
                        n8 = n7;
                        if (computeForPassword.numeric < this.mPasswordMinNumeric) {
                            n8 = (n7 | 0x200);
                            break;
                        }
                        break;
                    }
                    case 3: {
                        n8 = n7;
                        if (computeForPassword.symbols < this.mPasswordMinSymbols) {
                            n8 = (n7 | 0x400);
                            break;
                        }
                        break;
                    }
                    case 2: {
                        n8 = n7;
                        if (computeForPassword.lowerCase < this.mPasswordMinLowerCase) {
                            n8 = (n7 | 0x100);
                            break;
                        }
                        break;
                    }
                    case 1: {
                        n8 = n7;
                        if (computeForPassword.upperCase < this.mPasswordMinUpperCase) {
                            n8 = (n7 | 0x80);
                            break;
                        }
                        break;
                    }
                    case 0: {
                        n8 = n7;
                        if (computeForPassword.letters < this.mPasswordMinLetters) {
                            n8 = (n7 | 0x40);
                            break;
                        }
                        break;
                    }
                }
                ++i;
                n7 = n8;
            }
            return n7;
        }
        
        public void afterTextChanged(final Editable editable) {
            if (this.mUiStage == Stage.ConfirmWrong) {
                this.mUiStage = Stage.NeedToConfirm;
            }
            this.mTextChangedHandler.notifyAfterTextChanged();
        }
        
        public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        }
        
        String[] convertErrorCodeToMessages(int n) {
            final ArrayList<String> list = new ArrayList<String>();
            if ((n & 0x1) > 0) {
                list.add(this.getString(2131888145));
            }
            if ((n & 0x8) > 0) {
                list.add(this.getString(2131888158));
            }
            if ((n & 0x80) > 0) {
                list.add(this.getResources().getQuantityString(2131755041, this.mPasswordMinUpperCase, new Object[] { this.mPasswordMinUpperCase }));
            }
            if ((n & 0x100) > 0) {
                list.add(this.getResources().getQuantityString(2131755037, this.mPasswordMinLowerCase, new Object[] { this.mPasswordMinLowerCase }));
            }
            if ((n & 0x40) > 0) {
                list.add(this.getResources().getQuantityString(2131755036, this.mPasswordMinLetters, new Object[] { this.mPasswordMinLetters }));
            }
            if ((n & 0x200) > 0) {
                list.add(this.getResources().getQuantityString(2131755039, this.mPasswordMinNumeric, new Object[] { this.mPasswordMinNumeric }));
            }
            if ((n & 0x400) > 0) {
                list.add(this.getResources().getQuantityString(2131755040, this.mPasswordMinSymbols, new Object[] { this.mPasswordMinSymbols }));
            }
            if ((n & 0x800) > 0) {
                list.add(this.getResources().getQuantityString(2131755038, this.mPasswordMinNonLetter, new Object[] { this.mPasswordMinNonLetter }));
            }
            if ((n & 0x2) > 0) {
                int n2;
                if (this.mIsAlphaMode) {
                    n2 = 2131888155;
                }
                else {
                    n2 = 2131888163;
                }
                list.add(this.getString(n2, new Object[] { this.mPasswordMinLength }));
            }
            if ((n & 0x4) > 0) {
                int n3;
                if (this.mIsAlphaMode) {
                    n3 = 2131888154;
                }
                else {
                    n3 = 2131888162;
                }
                list.add(this.getString(n3, new Object[] { this.mPasswordMaxLength + 1 }));
            }
            if ((n & 0x10) > 0) {
                list.add(this.getString(2131888159));
            }
            if ((n & 0x20) > 0) {
                if (this.mIsAlphaMode) {
                    n = 2131888149;
                }
                else {
                    n = 2131888160;
                }
                list.add(this.getString(n));
            }
            return list.toArray(new String[0]);
        }
        
        public int getMetricsCategory() {
            return 28;
        }
        
        protected Intent getRedactionInterstitialIntent(final Context context) {
            return RedactionInterstitial.createStartIntent(context, this.mUserId);
        }
        
        public void handleNext() {
            if (this.mSaveAndFinishWorker != null) {
                return;
            }
            this.mChosenPassword = this.mPasswordEntry.getText().toString();
            if (TextUtils.isEmpty((CharSequence)this.mChosenPassword)) {
                return;
            }
            if (this.mUiStage == Stage.Introduction) {
                if (this.validatePassword(this.mChosenPassword) == 0) {
                    this.mFirstPin = this.mChosenPassword;
                    this.mPasswordEntry.setText((CharSequence)"");
                    this.updateStage(Stage.NeedToConfirm);
                }
            }
            else if (this.mUiStage == Stage.NeedToConfirm) {
                if (this.mFirstPin.equals(this.mChosenPassword)) {
                    this.startSaveAndFinish();
                }
                else {
                    final Editable text = this.mPasswordEntry.getText();
                    if (text != null) {
                        Selection.setSelection((Spannable)text, 0, ((CharSequence)text).length());
                    }
                    this.updateStage(Stage.ConfirmWrong);
                }
            }
        }
        
        public void onActivityResult(final int n, final int n2, final Intent intent) {
            super.onActivityResult(n, n2, intent);
            if (n == 58) {
                if (n2 != -1) {
                    this.getActivity().setResult(1);
                    this.getActivity().finish();
                }
                else {
                    this.mCurrentPassword = intent.getStringExtra("password");
                }
            }
        }
        
        public void onChosenLockSaveFinished(final boolean b, Intent redactionInterstitialIntent) {
            this.getActivity().setResult(1, redactionInterstitialIntent);
            if (!b) {
                redactionInterstitialIntent = this.getRedactionInterstitialIntent((Context)this.getActivity());
                if (redactionInterstitialIntent != null) {
                    redactionInterstitialIntent.putExtra(":settings:hide_drawer", this.mHideDrawer);
                    this.startActivity(redactionInterstitialIntent);
                }
            }
            this.getActivity().finish();
        }
        
        public void onClick(final View view) {
            final int id = view.getId();
            if (id != 2131361987) {
                if (id == 2131362389) {
                    this.handleNext();
                }
            }
            else {
                this.mPasswordEntry.setText((CharSequence)"");
            }
        }
        
        public void onCreate(final Bundle bundle) {
            super.onCreate(bundle);
            this.mLockPatternUtils = new LockPatternUtils((Context)this.getActivity());
            final Intent intent = this.getActivity().getIntent();
            if (this.getActivity() instanceof ChooseLockPassword) {
                this.mUserId = Utils.getUserIdFromBundle((Context)this.getActivity(), intent.getExtras());
                this.mForFingerprint = intent.getBooleanExtra("for_fingerprint", false);
                this.processPasswordRequirements(intent);
                this.mChooseLockSettingsHelper = new ChooseLockSettingsHelper(this.getActivity());
                this.mHideDrawer = this.getActivity().getIntent().getBooleanExtra(":settings:hide_drawer", false);
                if (intent.getBooleanExtra("for_cred_req_boot", false)) {
                    final SaveAndFinishWorker saveAndFinishWorker = new SaveAndFinishWorker();
                    final boolean booleanExtra = this.getActivity().getIntent().getBooleanExtra("extra_require_password", true);
                    final String stringExtra = intent.getStringExtra("password");
                    saveAndFinishWorker.setBlocking(true);
                    saveAndFinishWorker.setListener((Listener)this);
                    saveAndFinishWorker.start(this.mChooseLockSettingsHelper.utils(), booleanExtra, false, 0L, stringExtra, stringExtra, this.mRequestedQuality, this.mUserId);
                }
                this.mTextChangedHandler = new TextChangedHandler();
                return;
            }
            throw new SecurityException("Fragment contained in wrong activity");
        }
        
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            return layoutInflater.inflate(2131558483, viewGroup, false);
        }
        
        public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
            if (n != 0 && n != 6 && n != 5) {
                return false;
            }
            this.handleNext();
            return true;
        }
        
        public void onPause() {
            if (this.mSaveAndFinishWorker != null) {
                this.mSaveAndFinishWorker.setListener((Listener)null);
            }
            super.onPause();
        }
        
        @Override
        public void onResume() {
            super.onResume();
            this.updateStage(this.mUiStage);
            if (this.mSaveAndFinishWorker != null) {
                this.mSaveAndFinishWorker.setListener((Listener)this);
            }
            else {
                this.mPasswordEntry.requestFocus();
                this.mPasswordEntry.scheduleShowSoftInput();
            }
        }
        
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putString("ui_stage", this.mUiStage.name());
            bundle.putString("first_pin", this.mFirstPin);
            bundle.putString("current_password", this.mCurrentPassword);
        }
        
        public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        }
        
        public void onViewCreated(final View view, final Bundle bundle) {
            super.onViewCreated(view, bundle);
            this.mLayout = (GlifLayout)view;
            ((ViewGroup)view.findViewById(2131362438)).setOpticalInsets(Insets.NONE);
            (this.mSkipButton = (Button)view.findViewById(2131362617)).setOnClickListener((View.OnClickListener)this);
            (this.mNextButton = (Button)view.findViewById(2131362389)).setOnClickListener((View.OnClickListener)this);
            (this.mClearButton = (Button)view.findViewById(2131361987)).setOnClickListener((View.OnClickListener)this);
            this.mMessage = (TextView)view.findViewById(R.id.message);
            if (this.mForFingerprint) {
                this.mLayout.setIcon(this.getActivity().getDrawable(2131231017));
            }
            this.mIsAlphaMode = (262144 == this.mRequestedQuality || 327680 == this.mRequestedQuality || 393216 == this.mRequestedQuality);
            this.setupPasswordRequirementsView(view);
            this.mPasswordRestrictionView.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager((Context)this.getActivity()));
            (this.mPasswordEntry = (ImeAwareEditText)view.findViewById(2131362439)).setOnEditorActionListener((TextView$OnEditorActionListener)this);
            this.mPasswordEntry.addTextChangedListener((TextWatcher)this);
            this.mPasswordEntry.requestFocus();
            this.mPasswordEntryInputDisabler = new TextViewInputDisabler((TextView)this.mPasswordEntry);
            final Activity activity = this.getActivity();
            int inputType = this.mPasswordEntry.getInputType();
            final ImeAwareEditText mPasswordEntry = this.mPasswordEntry;
            if (!this.mIsAlphaMode) {
                inputType = 18;
            }
            mPasswordEntry.setInputType(inputType);
            this.mPasswordEntry.setTypeface(Typeface.create(this.getContext().getString(17039680), 0));
            final Intent intent = this.getActivity().getIntent();
            final boolean booleanExtra = intent.getBooleanExtra("confirm_credentials", true);
            this.mCurrentPassword = intent.getStringExtra("password");
            this.mHasChallenge = intent.getBooleanExtra("has_challenge", false);
            this.mChallenge = intent.getLongExtra("challenge", 0L);
            if (bundle == null) {
                this.updateStage(Stage.Introduction);
                if (booleanExtra) {
                    this.mChooseLockSettingsHelper.launchConfirmationActivity(58, this.getString(2131889578), true, this.mUserId);
                }
            }
            else {
                this.mFirstPin = bundle.getString("first_pin");
                final String string = bundle.getString("ui_stage");
                if (string != null) {
                    this.updateStage(this.mUiStage = Stage.valueOf(string));
                }
                if (this.mCurrentPassword == null) {
                    this.mCurrentPassword = bundle.getString("current_password");
                }
                this.mSaveAndFinishWorker = (SaveAndFinishWorker)this.getFragmentManager().findFragmentByTag("save_and_finish_worker");
            }
            if (activity instanceof SettingsActivity) {
                final SettingsActivity settingsActivity = (SettingsActivity)activity;
                final int hint = Stage.Introduction.getHint(this.mIsAlphaMode, this.mForFingerprint);
                settingsActivity.setTitle(hint);
                this.mLayout.setHeaderText(hint);
            }
        }
        
        protected void setNextEnabled(final boolean enabled) {
            this.mNextButton.setEnabled(enabled);
        }
        
        protected void setNextText(final int text) {
            this.mNextButton.setText(text);
        }
        
        protected void updateStage(final Stage mUiStage) {
            final Stage mUiStage2 = this.mUiStage;
            this.mUiStage = mUiStage;
            this.updateUi();
            if (mUiStage2 != mUiStage) {
                this.mLayout.announceForAccessibility(this.mLayout.getHeaderText());
            }
        }
        
        protected void updateUi() {
            final SaveAndFinishWorker mSaveAndFinishWorker = this.mSaveAndFinishWorker;
            final boolean b = true;
            final boolean inputEnabled = mSaveAndFinishWorker == null;
            final String string = this.mPasswordEntry.getText().toString();
            final int length = string.length();
            if (this.mUiStage == Stage.Introduction) {
                this.mPasswordRestrictionView.setVisibility(0);
                final int validatePassword = this.validatePassword(string);
                this.mPasswordRequirementAdapter.setRequirements(this.convertErrorCodeToMessages(validatePassword));
                this.setNextEnabled(validatePassword == 0);
            }
            else {
                this.mPasswordRestrictionView.setVisibility(8);
                this.setHeaderText(this.getString(this.mUiStage.getHint(this.mIsAlphaMode, this.mForFingerprint)));
                this.setNextEnabled(inputEnabled && length >= this.mPasswordMinLength);
                this.mClearButton.setEnabled(inputEnabled && length > 0);
            }
            final int message = this.mUiStage.getMessage(this.mIsAlphaMode, this.mForFingerprint);
            if (message != 0) {
                this.mMessage.setVisibility(0);
                this.mMessage.setText(message);
            }
            else {
                this.mMessage.setVisibility(4);
            }
            this.mClearButton.setVisibility(this.toVisibility(this.mUiStage != Stage.Introduction && b));
            this.setNextText(this.mUiStage.buttonText);
            this.mPasswordEntryInputDisabler.setInputEnabled(inputEnabled);
        }
        
        protected enum Stage
        {
            ConfirmWrong(2131888126, 2131888126, 2131888127, 2131888127, 0, 0, 0, 0, 2131888125), 
            Introduction(2131888123, 2131888117, 2131888123, 2131888121, 2131888118, 2131888095, 2131888122, 2131888095, 2131888358), 
            NeedToConfirm(2131888131, 2131888131, 2131888141, 2131888141, 0, 0, 0, 0, 2131888125);
            
            public final int alphaHint;
            public final int alphaHintForFingerprint;
            public final int alphaMessage;
            public final int alphaMessageForFingerprint;
            public final int buttonText;
            public final int numericHint;
            public final int numericHintForFingerprint;
            public final int numericMessage;
            public final int numericMessageForFingerprint;
            
            private Stage(final int alphaHint, final int alphaHintForFingerprint, final int numericHint, final int numericHintForFingerprint, final int alphaMessage, final int alphaMessageForFingerprint, final int numericMessage, final int numericMessageForFingerprint, final int buttonText) {
                this.alphaHint = alphaHint;
                this.alphaHintForFingerprint = alphaHintForFingerprint;
                this.numericHint = numericHint;
                this.numericHintForFingerprint = numericHintForFingerprint;
                this.alphaMessage = alphaMessage;
                this.alphaMessageForFingerprint = alphaMessageForFingerprint;
                this.numericMessage = numericMessage;
                this.numericMessageForFingerprint = numericMessageForFingerprint;
                this.buttonText = buttonText;
            }
            
            public int getHint(final boolean b, final boolean b2) {
                if (b) {
                    int n;
                    if (b2) {
                        n = this.alphaHintForFingerprint;
                    }
                    else {
                        n = this.alphaHint;
                    }
                    return n;
                }
                int n2;
                if (b2) {
                    n2 = this.numericHintForFingerprint;
                }
                else {
                    n2 = this.numericHint;
                }
                return n2;
            }
            
            public int getMessage(final boolean b, final boolean b2) {
                if (b) {
                    int n;
                    if (b2) {
                        n = this.alphaMessageForFingerprint;
                    }
                    else {
                        n = this.alphaMessage;
                    }
                    return n;
                }
                int n2;
                if (b2) {
                    n2 = this.numericMessageForFingerprint;
                }
                else {
                    n2 = this.numericMessage;
                }
                return n2;
            }
        }
        
        class TextChangedHandler extends Handler
        {
            private void notifyAfterTextChanged() {
                this.removeMessages(1);
                this.sendEmptyMessageDelayed(1, 100L);
            }
            
            public void handleMessage(final Message message) {
                if (ChooseLockPasswordFragment.this.getActivity() == null) {
                    return;
                }
                if (message.what == 1) {
                    ChooseLockPasswordFragment.this.updateUi();
                }
            }
        }
    }
    
    public static class IntentBuilder
    {
        private final Intent mIntent;
        
        public IntentBuilder(final Context context) {
            (this.mIntent = new Intent(context, (Class)ChooseLockPassword.class)).putExtra("confirm_credentials", false);
            this.mIntent.putExtra("extra_require_password", false);
            this.mIntent.putExtra("has_challenge", false);
        }
        
        public Intent build() {
            return this.mIntent;
        }
        
        public IntentBuilder setChallenge(final long n) {
            this.mIntent.putExtra("has_challenge", true);
            this.mIntent.putExtra("challenge", n);
            return this;
        }
        
        public IntentBuilder setForFingerprint(final boolean b) {
            this.mIntent.putExtra("for_fingerprint", b);
            return this;
        }
        
        public IntentBuilder setPassword(final String s) {
            this.mIntent.putExtra("password", s);
            return this;
        }
        
        public IntentBuilder setPasswordLengthRange(final int n, final int n2) {
            this.mIntent.putExtra("lockscreen.password_min", n);
            this.mIntent.putExtra("lockscreen.password_max", n2);
            return this;
        }
        
        public IntentBuilder setPasswordQuality(final int n) {
            this.mIntent.putExtra("lockscreen.password_type", n);
            return this;
        }
        
        public IntentBuilder setUserId(final int n) {
            this.mIntent.putExtra("android.intent.extra.USER_ID", n);
            return this;
        }
    }
    
    public static class SaveAndFinishWorker extends SaveChosenLockWorkerBase
    {
        private String mChosenPassword;
        private String mCurrentPassword;
        private int mRequestedQuality;
        
        @Override
        protected Intent saveAndVerifyInBackground() {
            Intent intent = null;
            this.mUtils.saveLockPassword(this.mChosenPassword, this.mCurrentPassword, this.mRequestedQuality, this.mUserId);
            if (this.mHasChallenge) {
                byte[] verifyPassword;
                try {
                    verifyPassword = this.mUtils.verifyPassword(this.mChosenPassword, this.mChallenge, this.mUserId);
                }
                catch (LockPatternUtils$RequestThrottledException ex) {
                    verifyPassword = null;
                }
                if (verifyPassword == null) {
                    Log.e("ChooseLockPassword", "critical: no token returned for known good password.");
                }
                final Intent intent2 = new Intent();
                intent2.putExtra("hw_auth_token", verifyPassword);
                intent = intent2;
            }
            return intent;
        }
        
        public void start(final LockPatternUtils lockPatternUtils, final boolean b, final boolean b2, final long n, final String mChosenPassword, final String mCurrentPassword, final int mRequestedQuality, final int mUserId) {
            this.prepare(lockPatternUtils, b, b2, n, mUserId);
            this.mChosenPassword = mChosenPassword;
            this.mCurrentPassword = mCurrentPassword;
            this.mRequestedQuality = mRequestedQuality;
            this.mUserId = mUserId;
            this.start();
        }
    }
}
