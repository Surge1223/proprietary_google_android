package com.android.settings.password;

import android.content.ComponentName;
import android.os.UserHandle;
import java.util.ArrayList;
import java.util.List;
import android.app.admin.DevicePolicyManager;
import android.content.Context;

public class ChooseLockGenericController
{
    private final Context mContext;
    private DevicePolicyManager mDpm;
    private ManagedLockPasswordProvider mManagedPasswordProvider;
    private final int mUserId;
    
    public ChooseLockGenericController(final Context context, final int n) {
        this(context, n, (DevicePolicyManager)context.getSystemService((Class)DevicePolicyManager.class), ManagedLockPasswordProvider.get(context, n));
    }
    
    ChooseLockGenericController(final Context mContext, final int mUserId, final DevicePolicyManager mDpm, final ManagedLockPasswordProvider mManagedPasswordProvider) {
        this.mContext = mContext;
        this.mUserId = mUserId;
        this.mManagedPasswordProvider = mManagedPasswordProvider;
        this.mDpm = mDpm;
    }
    
    public CharSequence getTitle(final ScreenLockType screenLockType) {
        switch (screenLockType) {
            default: {
                return null;
            }
            case PASSWORD: {
                return this.mContext.getText(2131889590);
            }
            case PIN: {
                return this.mContext.getText(2131889594);
            }
            case PATTERN: {
                return this.mContext.getText(2131889592);
            }
            case MANAGED: {
                return this.mManagedPasswordProvider.getPickerOptionTitle(false);
            }
            case SWIPE: {
                return this.mContext.getText(2131889586);
            }
            case NONE: {
                return this.mContext.getText(2131889588);
            }
        }
    }
    
    public List<ScreenLockType> getVisibleScreenLockTypes(int i, final boolean b) {
        final int upgradeQuality = this.upgradeQuality(i);
        final ArrayList<ScreenLockType> list = new ArrayList<ScreenLockType>();
        final ScreenLockType[] values = ScreenLockType.values();
        int length;
        ScreenLockType screenLockType;
        for (length = values.length, i = 0; i < length; ++i) {
            screenLockType = values[i];
            if (this.isScreenLockVisible(screenLockType) && (b || this.isScreenLockEnabled(screenLockType, upgradeQuality))) {
                list.add(screenLockType);
            }
        }
        return list;
    }
    
    public boolean isScreenLockDisabledByAdmin(final ScreenLockType screenLockType, final int n) {
        final int maxQuality = screenLockType.maxQuality;
        final boolean b = false;
        boolean b2 = maxQuality < n;
        if (screenLockType == ScreenLockType.MANAGED) {
            b2 = (b2 || !this.mManagedPasswordProvider.isManagedPasswordChoosable() || b);
        }
        return b2;
    }
    
    public boolean isScreenLockEnabled(final ScreenLockType screenLockType, final int n) {
        return screenLockType.maxQuality >= n;
    }
    
    public boolean isScreenLockVisible(final ScreenLockType screenLockType) {
        final int n = ChooseLockGenericController$1.$SwitchMap$com$android$settings$password$ScreenLockType[screenLockType.ordinal()];
        boolean b = true;
        switch (n) {
            default: {
                return true;
            }
            case 3: {
                return this.mManagedPasswordProvider.isManagedPasswordChoosable();
            }
            case 2: {
                if (this.mContext.getResources().getBoolean(2131034124) || this.mUserId != UserHandle.myUserId()) {
                    b = false;
                }
                return b;
            }
            case 1: {
                return this.mContext.getResources().getBoolean(2131034123) ^ true;
            }
        }
    }
    
    public int upgradeQuality(final int n) {
        return Math.max(n, this.mDpm.getPasswordQuality((ComponentName)null, this.mUserId));
    }
}
