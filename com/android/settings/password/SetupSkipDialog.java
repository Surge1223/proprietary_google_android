package com.android.settings.password;

import android.app.FragmentManager;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class SetupSkipDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    public static SetupSkipDialog newInstance(final boolean b) {
        final SetupSkipDialog setupSkipDialog = new SetupSkipDialog();
        final Bundle arguments = new Bundle();
        arguments.putBoolean("frp_supported", b);
        setupSkipDialog.setArguments(arguments);
        return setupSkipDialog;
    }
    
    public int getMetricsCategory() {
        return 573;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            final Activity activity = this.getActivity();
            activity.setResult(11);
            activity.finish();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)this.onCreateDialogBuilder().create();
    }
    
    public AlertDialog$Builder onCreateDialogBuilder() {
        final Bundle arguments = this.getArguments();
        final AlertDialog$Builder setTitle = new AlertDialog$Builder(this.getContext()).setPositiveButton(2131889174, (DialogInterface$OnClickListener)this).setNegativeButton(2131887735, (DialogInterface$OnClickListener)this).setTitle(2131888084);
        int message;
        if (arguments.getBoolean("frp_supported")) {
            message = 2131888083;
        }
        else {
            message = 2131888082;
        }
        return setTitle.setMessage(message);
    }
    
    public void show(final FragmentManager fragmentManager) {
        this.show(fragmentManager, "skip_dialog");
    }
}
