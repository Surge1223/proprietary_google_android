package com.android.settings.password;

import android.content.ComponentName;
import com.android.internal.widget.LockPatternUtils;
import android.os.Bundle;
import com.android.settings.Utils;
import android.os.UserManager;
import android.app.ActivityManager;
import android.os.IBinder;
import android.content.Intent;
import android.content.Context;
import com.android.internal.util.Preconditions;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.app.admin.DevicePolicyManager;

final class SetNewPasswordController
{
    private final DevicePolicyManager mDevicePolicyManager;
    private final FingerprintManager mFingerprintManager;
    private final PackageManager mPackageManager;
    private final int mTargetUserId;
    private final Ui mUi;
    
    SetNewPasswordController(final int mTargetUserId, final PackageManager packageManager, final FingerprintManager mFingerprintManager, final DevicePolicyManager devicePolicyManager, final Ui ui) {
        this.mTargetUserId = mTargetUserId;
        this.mPackageManager = (PackageManager)Preconditions.checkNotNull((Object)packageManager);
        this.mFingerprintManager = mFingerprintManager;
        this.mDevicePolicyManager = (DevicePolicyManager)Preconditions.checkNotNull((Object)devicePolicyManager);
        this.mUi = (Ui)Preconditions.checkNotNull((Object)ui);
    }
    
    public static SetNewPasswordController create(final Context context, final Ui ui, final Intent intent, final IBinder binder) {
        int currentUser = ActivityManager.getCurrentUser();
        if ("android.app.action.SET_NEW_PASSWORD".equals(intent.getAction())) {
            final int identifier = Utils.getSecureTargetUser(binder, UserManager.get(context), null, intent.getExtras()).getIdentifier();
            currentUser = currentUser;
            if (new LockPatternUtils(context).isSeparateProfileChallengeAllowed(identifier)) {
                currentUser = identifier;
            }
        }
        return new SetNewPasswordController(currentUser, context.getPackageManager(), Utils.getFingerprintManagerOrNull(context), (DevicePolicyManager)context.getSystemService("device_policy"), ui);
    }
    
    private Bundle getFingerprintChooseLockExtras() {
        final Bundle bundle = new Bundle();
        final long preEnroll = this.mFingerprintManager.preEnroll();
        bundle.putInt("minimum_quality", 65536);
        bundle.putBoolean("hide_disabled_prefs", true);
        bundle.putBoolean("has_challenge", true);
        bundle.putLong("challenge", preEnroll);
        bundle.putBoolean("for_fingerprint", true);
        return bundle;
    }
    
    private boolean isFingerprintDisabledByAdmin() {
        return (this.mDevicePolicyManager.getKeyguardDisabledFeatures((ComponentName)null, this.mTargetUserId) & 0x20) != 0x0;
    }
    
    public void dispatchSetNewPasswordIntent() {
        Bundle fingerprintChooseLockExtras;
        if (this.mPackageManager.hasSystemFeature("android.hardware.fingerprint") && this.mFingerprintManager != null && this.mFingerprintManager.isHardwareDetected() && !this.mFingerprintManager.hasEnrolledFingerprints(this.mTargetUserId) && !this.isFingerprintDisabledByAdmin()) {
            fingerprintChooseLockExtras = this.getFingerprintChooseLockExtras();
        }
        else {
            fingerprintChooseLockExtras = new Bundle();
        }
        fingerprintChooseLockExtras.putInt("android.intent.extra.USER_ID", this.mTargetUserId);
        this.mUi.launchChooseLock(fingerprintChooseLockExtras);
    }
    
    interface Ui
    {
        void launchChooseLock(final Bundle p0);
    }
}
