package com.android.settings.password;

import com.android.internal.widget.LockPatternUtils$RequestThrottledException;
import android.util.TypedValue;
import com.android.internal.widget.LinearLayoutWithDefaultTouchRecepient;
import com.android.setupwizardlib.GlifLayout;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.Utils;
import android.view.View;
import com.android.internal.widget.LockPatternView$DisplayMode;
import com.android.settings.notification.RedactionInterstitial;
import android.content.Context;
import android.util.Log;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import com.google.android.collect.Lists;
import android.widget.ScrollView;
import com.android.internal.widget.LockPatternView;
import android.widget.TextView;
import android.content.res.ColorStateList;
import com.android.internal.widget.LockPatternView$OnPatternListener;
import com.android.internal.widget.LockPatternView$Cell;
import java.util.List;
import android.view.View.OnClickListener;
import com.android.settings.core.InstrumentedFragment;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.os.Bundle;
import com.android.settings.SetupWizardUtils;
import android.content.res.Resources.Theme;
import android.content.Intent;
import android.app.Fragment;
import com.android.settings.SettingsActivity;

public class ChooseLockPattern extends SettingsActivity
{
    Class<? extends Fragment> getFragmentClass() {
        return ChooseLockPatternFragment.class;
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", this.getFragmentClass().getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return ChooseLockPatternFragment.class.getName().equals(s);
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        super.onApplyThemeResource(resources$Theme, SetupWizardUtils.getTheme(this.getIntent()), b);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        int title;
        if (this.getIntent().getBooleanExtra("for_fingerprint", false)) {
            title = 2131888119;
        }
        else {
            title = 2131888123;
        }
        this.setTitle(title);
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        return super.onKeyDown(n, keyEvent);
    }
    
    public static class ChooseLockPatternFragment extends InstrumentedFragment implements View.OnClickListener, Listener
    {
        private final List<LockPatternView$Cell> mAnimatePattern;
        private long mChallenge;
        private ChooseLockSettingsHelper mChooseLockSettingsHelper;
        protected LockPatternView$OnPatternListener mChooseNewLockPatternListener;
        protected List<LockPatternView$Cell> mChosenPattern;
        private Runnable mClearPatternRunnable;
        private String mCurrentPattern;
        private ColorStateList mDefaultHeaderColorList;
        private TextView mFooterLeftButton;
        private TextView mFooterRightButton;
        protected TextView mFooterText;
        protected boolean mForFingerprint;
        private boolean mHasChallenge;
        protected TextView mHeaderText;
        private boolean mHideDrawer;
        protected LockPatternView mLockPatternView;
        protected TextView mMessageText;
        private SaveAndFinishWorker mSaveAndFinishWorker;
        private ScrollView mTitleHeaderScrollView;
        protected TextView mTitleText;
        private Stage mUiStage;
        protected int mUserId;
        
        public ChooseLockPatternFragment() {
            this.mChosenPattern = null;
            this.mHideDrawer = false;
            this.mAnimatePattern = Collections.unmodifiableList((List<? extends LockPatternView$Cell>)Lists.newArrayList((Object[])new LockPatternView$Cell[] { LockPatternView$Cell.of(0, 0), LockPatternView$Cell.of(0, 1), LockPatternView$Cell.of(1, 1), LockPatternView$Cell.of(2, 1) }));
            this.mChooseNewLockPatternListener = (LockPatternView$OnPatternListener)new LockPatternView$OnPatternListener() {
                private void patternInProgress() {
                    ChooseLockPatternFragment.this.mHeaderText.setText(2131888179);
                    if (ChooseLockPatternFragment.this.mDefaultHeaderColorList != null) {
                        ChooseLockPatternFragment.this.mHeaderText.setTextColor(ChooseLockPatternFragment.this.mDefaultHeaderColorList);
                    }
                    ChooseLockPatternFragment.this.mFooterText.setText((CharSequence)"");
                    ChooseLockPatternFragment.this.mFooterLeftButton.setEnabled(false);
                    ChooseLockPatternFragment.this.mFooterRightButton.setEnabled(false);
                    if (ChooseLockPatternFragment.this.mTitleHeaderScrollView != null) {
                        ChooseLockPatternFragment.this.mTitleHeaderScrollView.post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                ChooseLockPatternFragment.this.mTitleHeaderScrollView.fullScroll(130);
                            }
                        });
                    }
                }
                
                public void onPatternCellAdded(final List<LockPatternView$Cell> list) {
                }
                
                public void onPatternCleared() {
                    ChooseLockPatternFragment.this.mLockPatternView.removeCallbacks(ChooseLockPatternFragment.this.mClearPatternRunnable);
                }
                
                public void onPatternDetected(final List<LockPatternView$Cell> list) {
                    if (ChooseLockPatternFragment.this.mUiStage != Stage.NeedToConfirm && ChooseLockPatternFragment.this.mUiStage != Stage.ConfirmWrong) {
                        if (ChooseLockPatternFragment.this.mUiStage != Stage.Introduction && ChooseLockPatternFragment.this.mUiStage != Stage.ChoiceTooShort) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unexpected stage ");
                            sb.append(ChooseLockPatternFragment.this.mUiStage);
                            sb.append(" when entering the pattern.");
                            throw new IllegalStateException(sb.toString());
                        }
                        if (list.size() < 4) {
                            ChooseLockPatternFragment.this.updateStage(Stage.ChoiceTooShort);
                        }
                        else {
                            ChooseLockPatternFragment.this.mChosenPattern = new ArrayList<LockPatternView$Cell>(list);
                            ChooseLockPatternFragment.this.updateStage(Stage.FirstChoiceValid);
                        }
                    }
                    else {
                        if (ChooseLockPatternFragment.this.mChosenPattern == null) {
                            throw new IllegalStateException("null chosen pattern in stage 'need to confirm");
                        }
                        if (ChooseLockPatternFragment.this.mChosenPattern.equals(list)) {
                            ChooseLockPatternFragment.this.updateStage(Stage.ChoiceConfirmed);
                        }
                        else {
                            ChooseLockPatternFragment.this.updateStage(Stage.ConfirmWrong);
                        }
                    }
                }
                
                public void onPatternStart() {
                    ChooseLockPatternFragment.this.mLockPatternView.removeCallbacks(ChooseLockPatternFragment.this.mClearPatternRunnable);
                    this.patternInProgress();
                }
            };
            this.mUiStage = Stage.Introduction;
            this.mClearPatternRunnable = new Runnable() {
                @Override
                public void run() {
                    ChooseLockPatternFragment.this.mLockPatternView.clearPattern();
                }
            };
        }
        
        private void postClearPatternRunnable() {
            this.mLockPatternView.removeCallbacks(this.mClearPatternRunnable);
            this.mLockPatternView.postDelayed(this.mClearPatternRunnable, 2000L);
        }
        
        private void startSaveAndFinish() {
            if (this.mSaveAndFinishWorker != null) {
                Log.w("ChooseLockPattern", "startSaveAndFinish with an existing SaveAndFinishWorker.");
                return;
            }
            this.setRightButtonEnabled(false);
            (this.mSaveAndFinishWorker = new SaveAndFinishWorker()).setListener((Listener)this);
            this.getFragmentManager().beginTransaction().add((Fragment)this.mSaveAndFinishWorker, "save_and_finish_worker").commit();
            this.getFragmentManager().executePendingTransactions();
            this.mSaveAndFinishWorker.start(this.mChooseLockSettingsHelper.utils(), this.getActivity().getIntent().getBooleanExtra("extra_require_password", true), this.mHasChallenge, this.mChallenge, this.mChosenPattern, this.mCurrentPattern, this.mUserId);
        }
        
        public int getMetricsCategory() {
            return 29;
        }
        
        protected Intent getRedactionInterstitialIntent(final Context context) {
            return RedactionInterstitial.createStartIntent(context, this.mUserId);
        }
        
        public void handleLeftButton() {
            if (this.mUiStage.leftMode == LeftButtonMode.Retry) {
                this.mChosenPattern = null;
                this.mLockPatternView.clearPattern();
                this.updateStage(Stage.Introduction);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("left footer button pressed, but stage of ");
            sb.append(this.mUiStage);
            sb.append(" doesn't make sense");
            throw new IllegalStateException(sb.toString());
        }
        
        public void handleRightButton() {
            if (this.mUiStage.rightMode == RightButtonMode.Continue) {
                if (this.mUiStage != Stage.FirstChoiceValid) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("expected ui stage ");
                    sb.append(Stage.FirstChoiceValid);
                    sb.append(" when button is ");
                    sb.append(RightButtonMode.Continue);
                    throw new IllegalStateException(sb.toString());
                }
                this.updateStage(Stage.NeedToConfirm);
            }
            else if (this.mUiStage.rightMode == RightButtonMode.Confirm) {
                if (this.mUiStage != Stage.ChoiceConfirmed) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("expected ui stage ");
                    sb2.append(Stage.ChoiceConfirmed);
                    sb2.append(" when button is ");
                    sb2.append(RightButtonMode.Confirm);
                    throw new IllegalStateException(sb2.toString());
                }
                this.startSaveAndFinish();
            }
            else if (this.mUiStage.rightMode == RightButtonMode.Ok) {
                if (this.mUiStage != Stage.HelpScreen) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Help screen is only mode with ok button, but stage is ");
                    sb3.append(this.mUiStage);
                    throw new IllegalStateException(sb3.toString());
                }
                this.mLockPatternView.clearPattern();
                this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Correct);
                this.updateStage(Stage.Introduction);
            }
        }
        
        public void onActivityResult(final int n, final int n2, final Intent intent) {
            super.onActivityResult(n, n2, intent);
            if (n == 55) {
                if (n2 != -1) {
                    this.getActivity().setResult(1);
                    this.getActivity().finish();
                }
                else {
                    this.mCurrentPattern = intent.getStringExtra("password");
                }
                this.updateStage(Stage.Introduction);
            }
        }
        
        public void onChosenLockSaveFinished(final boolean b, Intent redactionInterstitialIntent) {
            this.getActivity().setResult(1, redactionInterstitialIntent);
            if (!b) {
                redactionInterstitialIntent = this.getRedactionInterstitialIntent((Context)this.getActivity());
                if (redactionInterstitialIntent != null) {
                    redactionInterstitialIntent.putExtra(":settings:hide_drawer", this.mHideDrawer);
                    this.startActivity(redactionInterstitialIntent);
                }
            }
            this.getActivity().finish();
        }
        
        public void onClick(final View view) {
            if (view == this.mFooterLeftButton) {
                this.handleLeftButton();
            }
            else if (view == this.mFooterRightButton) {
                this.handleRightButton();
            }
        }
        
        public void onCreate(final Bundle bundle) {
            super.onCreate(bundle);
            this.mChooseLockSettingsHelper = new ChooseLockSettingsHelper(this.getActivity());
            if (this.getActivity() instanceof ChooseLockPattern) {
                final Intent intent = this.getActivity().getIntent();
                this.mUserId = Utils.getUserIdFromBundle((Context)this.getActivity(), intent.getExtras());
                if (intent.getBooleanExtra("for_cred_req_boot", false)) {
                    final SaveAndFinishWorker saveAndFinishWorker = new SaveAndFinishWorker();
                    final boolean booleanExtra = this.getActivity().getIntent().getBooleanExtra("extra_require_password", true);
                    final String stringExtra = intent.getStringExtra("password");
                    saveAndFinishWorker.setBlocking(true);
                    saveAndFinishWorker.setListener((Listener)this);
                    saveAndFinishWorker.start(this.mChooseLockSettingsHelper.utils(), booleanExtra, false, 0L, LockPatternUtils.stringToPattern(stringExtra), stringExtra, this.mUserId);
                }
                this.mHideDrawer = this.getActivity().getIntent().getBooleanExtra(":settings:hide_drawer", false);
                this.mForFingerprint = intent.getBooleanExtra("for_fingerprint", false);
                return;
            }
            throw new SecurityException("Fragment contained in wrong activity");
        }
        
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            final GlifLayout glifLayout = (GlifLayout)layoutInflater.inflate(2131558485, viewGroup, false);
            glifLayout.setHeaderText(this.getActivity().getTitle());
            if (this.getResources().getBoolean(2131034126)) {
                final View viewById = glifLayout.findViewById(R.id.suw_layout_icon);
                if (viewById != null) {
                    viewById.setVisibility(8);
                }
            }
            else if (this.mForFingerprint) {
                glifLayout.setIcon(this.getActivity().getDrawable(2131231017));
            }
            return (View)glifLayout;
        }
        
        public void onPause() {
            super.onPause();
            if (this.mSaveAndFinishWorker != null) {
                this.mSaveAndFinishWorker.setListener((Listener)null);
            }
        }
        
        @Override
        public void onResume() {
            super.onResume();
            this.updateStage(this.mUiStage);
            if (this.mSaveAndFinishWorker != null) {
                this.setRightButtonEnabled(false);
                this.mSaveAndFinishWorker.setListener((Listener)this);
            }
        }
        
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putInt("uiStage", this.mUiStage.ordinal());
            if (this.mChosenPattern != null) {
                bundle.putString("chosenPattern", LockPatternUtils.patternToString((List)this.mChosenPattern));
            }
            if (this.mCurrentPattern != null) {
                bundle.putString("currentPattern", this.mCurrentPattern);
            }
        }
        
        public void onViewCreated(final View view, final Bundle bundle) {
            super.onViewCreated(view, bundle);
            this.mTitleText = (TextView)view.findViewById(R.id.suw_layout_title);
            this.mHeaderText = (TextView)view.findViewById(2131362216);
            this.mDefaultHeaderColorList = this.mHeaderText.getTextColors();
            this.mMessageText = (TextView)view.findViewById(R.id.message);
            (this.mLockPatternView = (LockPatternView)view.findViewById(2131362345)).setOnPatternListener(this.mChooseNewLockPatternListener);
            this.mLockPatternView.setTactileFeedbackEnabled(this.mChooseLockSettingsHelper.utils().isTactileFeedbackEnabled());
            this.mLockPatternView.setFadePattern(false);
            this.mFooterText = (TextView)view.findViewById(2131362166);
            this.mFooterLeftButton = (TextView)view.findViewById(2131362164);
            this.mFooterRightButton = (TextView)view.findViewById(2131362165);
            this.mTitleHeaderScrollView = (ScrollView)view.findViewById(2131362554);
            this.mFooterLeftButton.setOnClickListener((View.OnClickListener)this);
            this.mFooterRightButton.setOnClickListener((View.OnClickListener)this);
            ((LinearLayoutWithDefaultTouchRecepient)view.findViewById(2131362744)).setDefaultTouchRecepient((View)this.mLockPatternView);
            final boolean booleanExtra = this.getActivity().getIntent().getBooleanExtra("confirm_credentials", true);
            final Intent intent = this.getActivity().getIntent();
            this.mCurrentPattern = intent.getStringExtra("password");
            this.mHasChallenge = intent.getBooleanExtra("has_challenge", false);
            this.mChallenge = intent.getLongExtra("challenge", 0L);
            if (bundle == null) {
                if (booleanExtra) {
                    this.updateStage(Stage.NeedToConfirm);
                    if (!this.mChooseLockSettingsHelper.launchConfirmationActivity(55, this.getString(2131889578), true, this.mUserId)) {
                        this.updateStage(Stage.Introduction);
                    }
                }
                else {
                    this.updateStage(Stage.Introduction);
                }
            }
            else {
                final String string = bundle.getString("chosenPattern");
                if (string != null) {
                    this.mChosenPattern = (List<LockPatternView$Cell>)LockPatternUtils.stringToPattern(string);
                }
                if (this.mCurrentPattern == null) {
                    this.mCurrentPattern = bundle.getString("currentPattern");
                }
                this.updateStage(Stage.values()[bundle.getInt("uiStage")]);
                this.mSaveAndFinishWorker = (SaveAndFinishWorker)this.getFragmentManager().findFragmentByTag("save_and_finish_worker");
            }
        }
        
        protected void setRightButtonEnabled(final boolean enabled) {
            this.mFooterRightButton.setEnabled(enabled);
        }
        
        protected void setRightButtonText(final int text) {
            this.mFooterRightButton.setText(text);
        }
        
        protected void updateFooterLeftButton(final Stage stage, final TextView textView) {
            if (stage.leftMode == LeftButtonMode.Gone) {
                textView.setVisibility(8);
            }
            else {
                textView.setVisibility(0);
                textView.setText(stage.leftMode.text);
                textView.setEnabled(stage.leftMode.enabled);
            }
        }
        
        protected void updateStage(final Stage mUiStage) {
            final Stage mUiStage2 = this.mUiStage;
            this.mUiStage = mUiStage;
            if (mUiStage == Stage.ChoiceTooShort) {
                this.mHeaderText.setText((CharSequence)this.getResources().getString(mUiStage.headerMessage, new Object[] { 4 }));
            }
            else {
                this.mHeaderText.setText(mUiStage.headerMessage);
            }
            int text;
            if (this.mForFingerprint) {
                text = mUiStage.messageForFingerprint;
            }
            else {
                text = mUiStage.message;
            }
            if (text == -1) {
                this.mMessageText.setText((CharSequence)"");
            }
            else {
                this.mMessageText.setText(text);
            }
            if (mUiStage.footerMessage == -1) {
                this.mFooterText.setText((CharSequence)"");
            }
            else {
                this.mFooterText.setText(mUiStage.footerMessage);
            }
            if (mUiStage != Stage.ConfirmWrong && mUiStage != Stage.ChoiceTooShort) {
                if (this.mDefaultHeaderColorList != null) {
                    this.mHeaderText.setTextColor(this.mDefaultHeaderColorList);
                }
                if (mUiStage == Stage.NeedToConfirm && this.mForFingerprint) {
                    this.mHeaderText.setText((CharSequence)"");
                    this.mTitleText.setText(2131888144);
                }
            }
            else {
                final TypedValue typedValue = new TypedValue();
                this.getActivity().getTheme().resolveAttribute(2130968690, typedValue, true);
                this.mHeaderText.setTextColor(typedValue.data);
            }
            this.updateFooterLeftButton(mUiStage, this.mFooterLeftButton);
            this.setRightButtonText(mUiStage.rightMode.text);
            this.setRightButtonEnabled(mUiStage.rightMode.enabled);
            if (mUiStage.patternEnabled) {
                this.mLockPatternView.enableInput();
            }
            else {
                this.mLockPatternView.disableInput();
            }
            this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Correct);
            boolean b = false;
            switch (this.mUiStage) {
                case ConfirmWrong: {
                    this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Wrong);
                    this.postClearPatternRunnable();
                    b = true;
                    break;
                }
                case NeedToConfirm: {
                    this.mLockPatternView.clearPattern();
                    break;
                }
                case ChoiceTooShort: {
                    this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Wrong);
                    this.postClearPatternRunnable();
                    b = true;
                    break;
                }
                case HelpScreen: {
                    this.mLockPatternView.setPattern(LockPatternView$DisplayMode.Animate, (List)this.mAnimatePattern);
                    break;
                }
                case Introduction: {
                    this.mLockPatternView.clearPattern();
                    break;
                }
            }
            if (mUiStage2 != mUiStage || b) {
                this.mHeaderText.announceForAccessibility(this.mHeaderText.getText());
            }
        }
        
        enum LeftButtonMode
        {
            Gone(-1, false), 
            Retry(2131888183, true), 
            RetryDisabled(2131888183, false);
            
            final boolean enabled;
            final int text;
            
            private LeftButtonMode(final int text, final boolean enabled) {
                this.text = text;
                this.enabled = enabled;
            }
        }
        
        enum RightButtonMode
        {
            Confirm(2131888172, true), 
            ConfirmDisabled(2131888172, false), 
            Continue(2131888358, true), 
            ContinueDisabled(2131888358, false), 
            Ok(17039370, true);
            
            final boolean enabled;
            final int text;
            
            private RightButtonMode(final int text, final boolean enabled) {
                this.text = text;
                this.enabled = enabled;
            }
        }
        
        protected enum Stage
        {
            ChoiceConfirmed(-1, -1, 2131888176, LeftButtonMode.Gone, RightButtonMode.Confirm, -1, false), 
            ChoiceTooShort(2131888095, 2131888120, 2131888178, LeftButtonMode.Retry, RightButtonMode.ContinueDisabled, -1, true), 
            ConfirmWrong(-1, -1, 2131888175, LeftButtonMode.Gone, RightButtonMode.ConfirmDisabled, -1, true), 
            FirstChoiceValid(2131888095, 2131888120, 2131888177, LeftButtonMode.Retry, RightButtonMode.Continue, -1, false), 
            HelpScreen(-1, -1, 2131888192, LeftButtonMode.Gone, RightButtonMode.Ok, -1, false), 
            Introduction(2131888095, 2131888120, 2131888181, LeftButtonMode.Gone, RightButtonMode.ContinueDisabled, -1, true), 
            NeedToConfirm(-1, -1, 2131888174, LeftButtonMode.Gone, RightButtonMode.ConfirmDisabled, -1, true);
            
            final int footerMessage;
            final int headerMessage;
            final LeftButtonMode leftMode;
            final int message;
            final int messageForFingerprint;
            final boolean patternEnabled;
            final RightButtonMode rightMode;
            
            private Stage(final int messageForFingerprint, final int message, final int headerMessage, final LeftButtonMode leftMode, final RightButtonMode rightMode, final int footerMessage, final boolean patternEnabled) {
                this.headerMessage = headerMessage;
                this.messageForFingerprint = messageForFingerprint;
                this.message = message;
                this.leftMode = leftMode;
                this.rightMode = rightMode;
                this.footerMessage = footerMessage;
                this.patternEnabled = patternEnabled;
            }
        }
    }
    
    public static class IntentBuilder
    {
        private final Intent mIntent;
        
        public IntentBuilder(final Context context) {
            (this.mIntent = new Intent(context, (Class)ChooseLockPattern.class)).putExtra("extra_require_password", false);
            this.mIntent.putExtra("confirm_credentials", false);
            this.mIntent.putExtra("has_challenge", false);
        }
        
        public Intent build() {
            return this.mIntent;
        }
        
        public IntentBuilder setChallenge(final long n) {
            this.mIntent.putExtra("has_challenge", true);
            this.mIntent.putExtra("challenge", n);
            return this;
        }
        
        public IntentBuilder setForFingerprint(final boolean b) {
            this.mIntent.putExtra("for_fingerprint", b);
            return this;
        }
        
        public IntentBuilder setPattern(final String s) {
            this.mIntent.putExtra("password", s);
            return this;
        }
        
        public IntentBuilder setUserId(final int n) {
            this.mIntent.putExtra("android.intent.extra.USER_ID", n);
            return this;
        }
    }
    
    public static class SaveAndFinishWorker extends SaveChosenLockWorkerBase
    {
        private List<LockPatternView$Cell> mChosenPattern;
        private String mCurrentPattern;
        private boolean mLockVirgin;
        
        @Override
        protected void finish(final Intent intent) {
            if (this.mLockVirgin) {
                this.mUtils.setVisiblePatternEnabled(true, this.mUserId);
            }
            super.finish(intent);
        }
        
        @Override
        protected Intent saveAndVerifyInBackground() {
            Intent intent = null;
            final int mUserId = this.mUserId;
            this.mUtils.saveLockPattern((List)this.mChosenPattern, this.mCurrentPattern, mUserId);
            if (this.mHasChallenge) {
                byte[] verifyPattern;
                try {
                    verifyPattern = this.mUtils.verifyPattern((List)this.mChosenPattern, this.mChallenge, mUserId);
                }
                catch (LockPatternUtils$RequestThrottledException ex) {
                    verifyPattern = null;
                }
                if (verifyPattern == null) {
                    Log.e("ChooseLockPattern", "critical: no token returned for known good pattern");
                }
                final Intent intent2 = new Intent();
                intent2.putExtra("hw_auth_token", verifyPattern);
                intent = intent2;
            }
            return intent;
        }
        
        public void start(final LockPatternUtils lockPatternUtils, final boolean b, final boolean b2, final long n, final List<LockPatternView$Cell> mChosenPattern, final String mCurrentPattern, final int mUserId) {
            this.prepare(lockPatternUtils, b, b2, n, mUserId);
            this.mCurrentPattern = mCurrentPattern;
            this.mChosenPattern = mChosenPattern;
            this.mUserId = mUserId;
            this.mLockVirgin = (this.mUtils.isPatternEverChosen(this.mUserId) ^ true);
            this.start();
        }
    }
}
