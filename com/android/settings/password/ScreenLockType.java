package com.android.settings.password;

public enum ScreenLockType
{
    MANAGED(524288, "unlock_set_managed");
    
    private static final ScreenLockType MAX_QUALITY;
    private static final ScreenLockType MIN_QUALITY;
    
    NONE(0, "unlock_set_off"), 
    PASSWORD(262144, 393216, "unlock_set_password"), 
    PATTERN(65536, "unlock_set_pattern"), 
    PIN(131072, 196608, "unlock_set_pin"), 
    SWIPE(0, "unlock_set_none");
    
    public final int defaultQuality;
    public final int maxQuality;
    public final String preferenceKey;
    
    static {
        MIN_QUALITY = ScreenLockType.NONE;
        MAX_QUALITY = ScreenLockType.MANAGED;
    }
    
    private ScreenLockType(final int defaultQuality, final int maxQuality, final String preferenceKey) {
        this.defaultQuality = defaultQuality;
        this.maxQuality = maxQuality;
        this.preferenceKey = preferenceKey;
    }
    
    private ScreenLockType(final int n2, final String s2) {
        this(n2, n2, s2);
    }
    
    public static ScreenLockType fromKey(final String s) {
        for (final ScreenLockType screenLockType : values()) {
            if (screenLockType.preferenceKey.equals(s)) {
                return screenLockType;
            }
        }
        return null;
    }
    
    public static ScreenLockType fromQuality(final int n) {
        if (n == 0) {
            return ScreenLockType.SWIPE;
        }
        if (n == 65536) {
            return ScreenLockType.PATTERN;
        }
        if (n == 131072 || n == 196608) {
            return ScreenLockType.PIN;
        }
        if (n == 262144 || n == 327680 || n == 393216) {
            return ScreenLockType.PASSWORD;
        }
        if (n != 524288) {
            return null;
        }
        return ScreenLockType.MANAGED;
    }
}
