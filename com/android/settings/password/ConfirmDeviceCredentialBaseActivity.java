package com.android.settings.password;

import android.view.MenuItem;
import android.app.KeyguardManager;
import android.widget.LinearLayout;
import com.android.settings.SetupWizardUtils;
import com.android.settings.Utils;
import android.content.Context;
import android.os.UserManager;
import android.os.Bundle;
import android.app.Fragment;
import com.android.settings.SettingsActivity;

public abstract class ConfirmDeviceCredentialBaseActivity extends SettingsActivity
{
    private ConfirmCredentialTheme mConfirmCredentialTheme;
    private boolean mEnterAnimationPending;
    private boolean mFirstTimeVisible;
    private boolean mIsKeyguardLocked;
    private boolean mRestoring;
    
    public ConfirmDeviceCredentialBaseActivity() {
        this.mFirstTimeVisible = true;
        this.mIsKeyguardLocked = false;
    }
    
    private ConfirmDeviceCredentialBaseFragment getFragment() {
        final Fragment fragmentById = this.getFragmentManager().findFragmentById(2131362353);
        if (fragmentById != null && fragmentById instanceof ConfirmDeviceCredentialBaseFragment) {
            return (ConfirmDeviceCredentialBaseFragment)fragmentById;
        }
        return null;
    }
    
    private boolean isInternalActivity() {
        return this instanceof ConfirmLockPassword.InternalActivity || this instanceof ConfirmLockPattern.InternalActivity;
    }
    
    public ConfirmCredentialTheme getConfirmCredentialTheme() {
        return this.mConfirmCredentialTheme;
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        final boolean managedProfile = UserManager.get((Context)this).isManagedProfile(Utils.getCredentialOwnerUserId((Context)this, Utils.getUserIdFromBundle((Context)this, this.getIntent().getExtras(), this.isInternalActivity())));
        final boolean b = false;
        if (managedProfile) {
            this.setTheme(2131952097);
            this.mConfirmCredentialTheme = ConfirmCredentialTheme.WORK;
        }
        else if (this.getIntent().getBooleanExtra("com.android.settings.ConfirmCredentials.darkTheme", false)) {
            this.setTheme(2131952096);
            this.mConfirmCredentialTheme = ConfirmCredentialTheme.DARK;
        }
        else {
            this.setTheme(SetupWizardUtils.getTheme(this.getIntent()));
            this.mConfirmCredentialTheme = ConfirmCredentialTheme.NORMAL;
        }
        super.onCreate(bundle);
        if (this.mConfirmCredentialTheme == ConfirmCredentialTheme.NORMAL) {
            ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
        }
        this.getWindow().addFlags(8192);
        boolean mIsKeyguardLocked;
        if (bundle == null) {
            mIsKeyguardLocked = ((KeyguardManager)this.getSystemService((Class)KeyguardManager.class)).isKeyguardLocked();
        }
        else {
            mIsKeyguardLocked = bundle.getBoolean("STATE_IS_KEYGUARD_LOCKED", false);
        }
        this.mIsKeyguardLocked = mIsKeyguardLocked;
        if (this.mIsKeyguardLocked && this.getIntent().getBooleanExtra("com.android.settings.ConfirmCredentials.showWhenLocked", false)) {
            this.getWindow().addFlags(524288);
        }
        this.setTitle((CharSequence)this.getIntent().getStringExtra("com.android.settings.ConfirmCredentials.title"));
        if (this.getActionBar() != null) {
            this.getActionBar().setDisplayHomeAsUpEnabled(true);
            this.getActionBar().setHomeButtonEnabled(true);
        }
        boolean mRestoring = b;
        if (bundle != null) {
            mRestoring = true;
        }
        this.mRestoring = mRestoring;
    }
    
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        if (this.mEnterAnimationPending) {
            this.startEnterAnimation();
            this.mEnterAnimationPending = false;
        }
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
    
    public void onResume() {
        super.onResume();
        if (!this.isChangingConfigurations() && !this.mRestoring && this.mConfirmCredentialTheme == ConfirmCredentialTheme.DARK && this.mFirstTimeVisible) {
            this.mFirstTimeVisible = false;
            this.prepareEnterAnimation();
            this.mEnterAnimationPending = true;
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("STATE_IS_KEYGUARD_LOCKED", this.mIsKeyguardLocked);
    }
    
    public void prepareEnterAnimation() {
        this.getFragment().prepareEnterAnimation();
    }
    
    public void startEnterAnimation() {
        this.getFragment().startEnterAnimation();
    }
    
    enum ConfirmCredentialTheme
    {
        DARK, 
        NORMAL, 
        WORK;
    }
}
