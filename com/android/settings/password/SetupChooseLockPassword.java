package com.android.settings.password;

import android.app.Activity;
import android.view.View.OnClickListener;
import android.util.Log;
import android.view.View;
import com.android.settings.SetupRedactionInterstitial;
import android.widget.Button;
import android.widget.LinearLayout;
import android.os.Bundle;
import android.app.Fragment;
import android.content.Intent;
import android.content.Context;

public class SetupChooseLockPassword extends ChooseLockPassword
{
    public static Intent modifyIntentForSetup(final Context context, final Intent intent) {
        intent.setClass(context, (Class)SetupChooseLockPassword.class);
        intent.putExtra("extra_prefs_show_button_bar", false);
        return intent;
    }
    
    @Override
    Class<? extends Fragment> getFragmentClass() {
        return SetupChooseLockPasswordFragment.class;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return SetupChooseLockPasswordFragment.class.getName().equals(s);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public static class SetupChooseLockPasswordFragment extends ChooseLockPasswordFragment implements OnLockTypeSelectedListener
    {
        private Button mOptionsButton;
        
        @Override
        protected Intent getRedactionInterstitialIntent(final Context context) {
            SetupRedactionInterstitial.setEnabled(context, true);
            return null;
        }
        
        @Override
        public void onClick(final View view) {
            final int id = view.getId();
            if (id != 2131362548) {
                if (id != 2131362617) {
                    super.onClick(view);
                }
                else {
                    SetupSkipDialog.newInstance(this.getActivity().getIntent().getBooleanExtra(":settings:frp_supported", false)).show(this.getFragmentManager());
                }
            }
            else {
                ChooseLockTypeDialogFragment.newInstance(this.mUserId).show(this.getChildFragmentManager(), (String)null);
            }
        }
        
        @Override
        public void onLockTypeSelected(final ScreenLockType screenLockType) {
            ScreenLockType screenLockType2;
            if (this.mIsAlphaMode) {
                screenLockType2 = ScreenLockType.PASSWORD;
            }
            else {
                screenLockType2 = ScreenLockType.PIN;
            }
            if (screenLockType == screenLockType2) {
                return;
            }
            ((ChooseLockTypeDialogFragment.OnLockTypeSelectedListener)this).startChooseLockActivity(screenLockType, this.getActivity());
        }
        
        @Override
        public void onViewCreated(final View view, final Bundle bundle) {
            super.onViewCreated(view, bundle);
            final Activity activity = this.getActivity();
            final boolean b = new ChooseLockGenericController((Context)activity, this.mUserId).getVisibleScreenLockTypes(65536, false).size() > 0;
            final boolean booleanExtra = activity.getIntent().getBooleanExtra("show_options_button", false);
            if (!b) {
                Log.w("SetupChooseLockPassword", "Visible screen lock types is empty!");
            }
            if (booleanExtra && b) {
                (this.mOptionsButton = (Button)view.findViewById(2131362548)).setVisibility(0);
                this.mOptionsButton.setOnClickListener((View.OnClickListener)this);
            }
        }
        
        @Override
        protected void updateUi() {
            super.updateUi();
            final Button mSkipButton = this.mSkipButton;
            final boolean mForFingerprint = this.mForFingerprint;
            final boolean b = false;
            int visibility;
            if (mForFingerprint) {
                visibility = 8;
            }
            else {
                visibility = 0;
            }
            mSkipButton.setVisibility(visibility);
            if (this.mOptionsButton != null) {
                final Button mOptionsButton = this.mOptionsButton;
                int visibility2;
                if (this.mUiStage == Stage.Introduction) {
                    visibility2 = (b ? 1 : 0);
                }
                else {
                    visibility2 = 8;
                }
                mOptionsButton.setVisibility(visibility2);
            }
        }
    }
}
