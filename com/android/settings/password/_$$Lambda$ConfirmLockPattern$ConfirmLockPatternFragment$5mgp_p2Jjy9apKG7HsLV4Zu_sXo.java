package com.android.settings.password;

import android.app.Fragment;
import android.view.animation.AnimationUtils;
import com.android.internal.widget.LinearLayoutWithDefaultTouchRecepient;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.animation.Interpolator;
import com.android.internal.widget.LockPatternView$DisplayMode;
import android.os.SystemClock;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.widget.LockPatternView$CellState;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import com.android.internal.widget.LockPatternChecker$OnVerifyCallback;
import com.android.internal.widget.LockPatternChecker;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternChecker$OnCheckCallback;
import com.android.internal.widget.LockPatternView$Cell;
import java.util.List;
import android.os.AsyncTask;
import com.android.internal.widget.LockPatternView;
import android.view.View;
import com.android.settingslib.animation.DisappearAnimationUtils;
import android.widget.TextView;
import android.os.CountDownTimer;
import com.android.internal.widget.LockPatternView$OnPatternListener;
import com.android.settingslib.animation.AppearAnimationUtils;
import com.android.settingslib.animation.AppearAnimationCreator;
import android.content.Intent;

public final class _$$Lambda$ConfirmLockPattern$ConfirmLockPatternFragment$5mgp_p2Jjy9apKG7HsLV4Zu_sXo implements Runnable
{
    @Override
    public final void run() {
        ConfirmLockPattern.ConfirmLockPatternFragment.lambda$startDisappearAnimation$0(this.f$0, this.f$1);
    }
}
