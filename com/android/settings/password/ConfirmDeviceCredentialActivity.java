package com.android.settings.password;

import com.android.internal.widget.LockPatternUtils;
import android.os.UserManager;
import android.util.Log;
import android.content.Context;
import com.android.settings.Utils;
import android.os.Bundle;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.app.Activity;

public class ConfirmDeviceCredentialActivity extends Activity
{
    public static final String TAG;
    
    static {
        TAG = ConfirmDeviceCredentialActivity.class.getSimpleName();
    }
    
    public static Intent createIntent(final CharSequence charSequence, final CharSequence charSequence2) {
        final Intent intent = new Intent();
        intent.setClassName("com.android.settings", ConfirmDeviceCredentialActivity.class.getName());
        intent.putExtra("android.app.extra.TITLE", charSequence);
        intent.putExtra("android.app.extra.DESCRIPTION", charSequence2);
        return intent;
    }
    
    private String getTitleFromOrganizationName(final int n) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)this.getSystemService("device_policy");
        String string = null;
        CharSequence organizationNameForUser;
        if (devicePolicyManager != null) {
            organizationNameForUser = devicePolicyManager.getOrganizationNameForUser(n);
        }
        else {
            organizationNameForUser = null;
        }
        if (organizationNameForUser != null) {
            string = organizationNameForUser.toString();
        }
        return string;
    }
    
    private boolean isInternalActivity() {
        return this instanceof InternalActivity;
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent intent = this.getIntent();
        final String stringExtra = intent.getStringExtra("android.app.extra.TITLE");
        final String stringExtra2 = intent.getStringExtra("android.app.extra.DESCRIPTION");
        final String stringExtra3 = intent.getStringExtra("android.app.extra.ALTERNATE_BUTTON_LABEL");
        final boolean equals = "android.app.action.CONFIRM_FRP_CREDENTIAL".equals(intent.getAction());
        int n = Utils.getCredentialOwnerUserId((Context)this);
        if (this.isInternalActivity()) {
            try {
                n = Utils.getUserIdFromBundle((Context)this, intent.getExtras());
            }
            catch (SecurityException ex) {
                Log.e(ConfirmDeviceCredentialActivity.TAG, "Invalid intent extra", (Throwable)ex);
            }
        }
        final boolean managedProfile = UserManager.get((Context)this).isManagedProfile(n);
        String titleFromOrganizationName;
        if ((titleFromOrganizationName = stringExtra) == null) {
            titleFromOrganizationName = stringExtra;
            if (managedProfile) {
                titleFromOrganizationName = this.getTitleFromOrganizationName(n);
            }
        }
        final ChooseLockSettingsHelper chooseLockSettingsHelper = new ChooseLockSettingsHelper(this);
        final LockPatternUtils lockPatternUtils = new LockPatternUtils((Context)this);
        boolean b;
        if (equals) {
            b = chooseLockSettingsHelper.launchFrpConfirmationActivity(0, titleFromOrganizationName, stringExtra2, stringExtra3);
        }
        else if (managedProfile && this.isInternalActivity() && !lockPatternUtils.isSeparateProfileChallengeEnabled(n)) {
            b = chooseLockSettingsHelper.launchConfirmationActivityWithExternalAndChallenge(0, null, titleFromOrganizationName, stringExtra2, true, 0L, n);
        }
        else {
            b = chooseLockSettingsHelper.launchConfirmationActivity(0, null, titleFromOrganizationName, stringExtra2, false, true, n);
        }
        if (!b) {
            Log.d(ConfirmDeviceCredentialActivity.TAG, "No pattern, password or PIN set.");
            this.setResult(-1);
        }
        this.finish();
    }
    
    public static class InternalActivity extends ConfirmDeviceCredentialActivity
    {
    }
}
