package com.android.settings.password;

import com.android.settings.Utils;
import java.io.Serializable;
import android.os.UserManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.content.IntentSender;
import android.content.Intent;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import com.android.internal.widget.LockPatternUtils;
import android.app.Fragment;
import android.app.Activity;

public final class ChooseLockSettingsHelper
{
    private Activity mActivity;
    private Fragment mFragment;
    @VisibleForTesting
    LockPatternUtils mLockPatternUtils;
    
    public ChooseLockSettingsHelper(final Activity mActivity) {
        this.mActivity = mActivity;
        this.mLockPatternUtils = new LockPatternUtils((Context)mActivity);
    }
    
    public ChooseLockSettingsHelper(final Activity activity, final Fragment mFragment) {
        this(activity);
        this.mFragment = mFragment;
    }
    
    private void copyInternalExtras(final Intent intent, final Intent intent2) {
        final String stringExtra = intent.getStringExtra("theme");
        if (stringExtra != null) {
            intent2.putExtra("theme", stringExtra);
        }
    }
    
    private void copyOptionalExtras(final Intent intent, final Intent intent2) {
        final IntentSender intentSender = (IntentSender)intent.getParcelableExtra("android.intent.extra.INTENT");
        if (intentSender != null) {
            intent2.putExtra("android.intent.extra.INTENT", (Parcelable)intentSender);
        }
        final int intExtra = intent.getIntExtra("android.intent.extra.TASK_ID", -1);
        if (intExtra != -1) {
            intent2.putExtra("android.intent.extra.TASK_ID", intExtra);
        }
        if (intentSender != null || intExtra != -1) {
            intent2.addFlags(8388608);
            intent2.addFlags(1073741824);
        }
    }
    
    private boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final Class<?> clazz, final boolean b, final boolean b2, final boolean b3, final long n2, final int n3, final CharSequence charSequence4, final Bundle bundle) {
        final Intent intent = new Intent();
        intent.putExtra("com.android.settings.ConfirmCredentials.title", charSequence);
        intent.putExtra("com.android.settings.ConfirmCredentials.header", charSequence2);
        intent.putExtra("com.android.settings.ConfirmCredentials.details", charSequence3);
        intent.putExtra("com.android.settings.ConfirmCredentials.allowFpAuthentication", b2);
        intent.putExtra("com.android.settings.ConfirmCredentials.darkTheme", false);
        intent.putExtra("com.android.settings.ConfirmCredentials.showCancelButton", false);
        intent.putExtra("com.android.settings.ConfirmCredentials.showWhenLocked", b2);
        intent.putExtra("return_credentials", b);
        intent.putExtra("has_challenge", b3);
        intent.putExtra("challenge", n2);
        intent.putExtra(":settings:hide_drawer", true);
        intent.putExtra("android.intent.extra.USER_ID", n3);
        intent.putExtra("android.app.extra.ALTERNATE_BUTTON_LABEL", charSequence4);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setClassName("com.android.settings", clazz.getName());
        if (b2) {
            intent.addFlags(33554432);
            if (this.mFragment != null) {
                this.copyOptionalExtras(this.mFragment.getActivity().getIntent(), intent);
                this.mFragment.startActivity(intent);
            }
            else {
                this.copyOptionalExtras(this.mActivity.getIntent(), intent);
                this.mActivity.startActivity(intent);
            }
        }
        else if (this.mFragment != null) {
            this.copyInternalExtras(this.mFragment.getActivity().getIntent(), intent);
            this.mFragment.startActivityForResult(intent, n);
        }
        else {
            this.copyInternalExtras(this.mActivity.getIntent(), intent);
            this.mActivity.startActivityForResult(intent, n);
        }
        return true;
    }
    
    private boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final boolean b, final boolean b2, final boolean b3, final long n2, final int n3) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, b, b2, b3, n2, n3, null, null);
    }
    
    private boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final boolean b, final boolean b2, final boolean b3, final long n2, final int n3, final Bundle bundle) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, b, b2, b3, n2, n3, null, bundle);
    }
    
    private boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final boolean b, final boolean b2, final boolean b3, final long n2, final int n3, final CharSequence charSequence4, final Bundle bundle) {
        final int credentialOwnerProfile = UserManager.get((Context)this.mActivity).getCredentialOwnerProfile(n3);
        final boolean b4 = false;
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(credentialOwnerProfile);
        boolean b5;
        if (keyguardStoredPasswordQuality != 65536) {
            if (keyguardStoredPasswordQuality != 131072 && keyguardStoredPasswordQuality != 196608 && keyguardStoredPasswordQuality != 262144 && keyguardStoredPasswordQuality != 327680 && keyguardStoredPasswordQuality != 393216 && keyguardStoredPasswordQuality != 524288) {
                b5 = b4;
            }
            else {
                Serializable s;
                if (!b && !b3) {
                    s = ConfirmLockPassword.class;
                }
                else {
                    s = ConfirmLockPassword.InternalActivity.class;
                }
                b5 = this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, (Class<?>)s, b, b2, b3, n2, n3, charSequence4, bundle);
            }
        }
        else {
            Serializable s2;
            if (!b && !b3) {
                s2 = ConfirmLockPattern.class;
            }
            else {
                s2 = ConfirmLockPattern.InternalActivity.class;
            }
            b5 = this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, (Class<?>)s2, b, b2, b3, n2, n3, charSequence4, bundle);
        }
        return b5;
    }
    
    public boolean launchConfirmationActivity(final int n, final CharSequence charSequence) {
        return this.launchConfirmationActivity(n, charSequence, null, null, false, false);
    }
    
    public boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final long n2) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, true, false, true, n2, Utils.getCredentialOwnerUserId((Context)this.mActivity));
    }
    
    public boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final long n2, final int n3) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, true, false, true, n2, Utils.enforceSameOwner((Context)this.mActivity, n3));
    }
    
    boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final boolean b, final boolean b2) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, b, b2, false, 0L, Utils.getCredentialOwnerUserId((Context)this.mActivity));
    }
    
    boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final boolean b, final boolean b2, final int n2) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, b, b2, false, 0L, Utils.enforceSameOwner((Context)this.mActivity, n2));
    }
    
    public boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final boolean b) {
        return this.launchConfirmationActivity(n, charSequence, null, null, b, false);
    }
    
    public boolean launchConfirmationActivity(final int n, final CharSequence charSequence, final boolean b, final int n2) {
        return this.launchConfirmationActivity(n, charSequence, null, null, b, false, false, 0L, Utils.enforceSameOwner((Context)this.mActivity, n2));
    }
    
    public boolean launchConfirmationActivityForAnyUser(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final int n2) {
        final Bundle bundle = new Bundle();
        bundle.putBoolean("allow_any_user", true);
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, false, false, true, 0L, n2, bundle);
    }
    
    public boolean launchConfirmationActivityWithExternalAndChallenge(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final boolean b, final long n2, final int n3) {
        return this.launchConfirmationActivity(n, charSequence, charSequence2, charSequence3, false, b, true, n2, Utils.enforceSameOwner((Context)this.mActivity, n3));
    }
    
    public boolean launchFrpConfirmationActivity(final int n, final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3) {
        return this.launchConfirmationActivity(n, null, charSequence, charSequence2, false, true, false, 0L, -9999, charSequence3, null);
    }
    
    public LockPatternUtils utils() {
        return this.mLockPatternUtils;
    }
}
