package com.android.settings.password;

import android.os.Message;
import android.os.Handler;
import android.app.Activity;
import com.android.settings.SettingsActivity;
import android.graphics.Typeface;
import android.graphics.Insets;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.Utils;
import android.os.Bundle;
import android.text.Selection;
import android.text.Spannable;
import com.android.settings.notification.RedactionInterstitial;
import android.text.Editable;
import android.app.admin.PasswordMetrics;
import android.app.Fragment;
import android.util.Log;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import java.util.ArrayList;
import android.view.View;
import android.text.TextUtils;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import com.android.internal.widget.TextViewInputDisabler;
import com.android.settings.widget.ImeAwareEditText;
import android.widget.TextView;
import com.android.internal.widget.LockPatternUtils;
import com.android.setupwizardlib.GlifLayout;
import android.widget.Button;
import android.widget.TextView$OnEditorActionListener;
import android.view.View.OnClickListener;
import android.text.TextWatcher;
import com.android.settings.core.InstrumentedFragment;
import java.util.function.ToIntFunction;

public final class _$$Lambda$ChooseLockPassword$ChooseLockPasswordFragment$WFCgmpRIhPOiOzVHNaBhMh5zoJI implements ToIntFunction
{
    @Override
    public final int applyAsInt(final Object o) {
        return ((Integer)o).intValue();
    }
}
