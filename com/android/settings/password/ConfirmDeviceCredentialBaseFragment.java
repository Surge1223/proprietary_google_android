package com.android.settings.password;

import android.content.DialogInterface;
import android.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.view.View.OnClickListener;
import android.text.TextUtils;
import android.content.Context;
import com.android.settings.Utils;
import android.os.Bundle;
import android.app.trust.TrustManager;
import android.content.IntentSender$SendIntentException;
import android.content.Intent;
import android.content.IntentSender;
import android.os.RemoteException;
import android.app.ActivityOptions;
import android.app.ActivityManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.content.ComponentName;
import android.content.pm.UserInfo;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.admin.DevicePolicyManager;
import android.widget.Button;
import com.android.settings.fingerprint.FingerprintUiHelper;
import com.android.settings.core.InstrumentedFragment;

public abstract class ConfirmDeviceCredentialBaseFragment extends InstrumentedFragment implements Callback
{
    protected Button mCancelButton;
    protected DevicePolicyManager mDevicePolicyManager;
    protected int mEffectiveUserId;
    protected TextView mErrorTextView;
    private FingerprintUiHelper mFingerprintHelper;
    protected ImageView mFingerprintIcon;
    protected boolean mFrp;
    private CharSequence mFrpAlternateButtonText;
    protected final Handler mHandler;
    protected LockPatternUtils mLockPatternUtils;
    private final Runnable mResetErrorRunnable;
    protected boolean mReturnCredentials;
    protected int mUserId;
    protected UserManager mUserManager;
    
    public ConfirmDeviceCredentialBaseFragment() {
        this.mReturnCredentials = false;
        this.mHandler = new Handler();
        this.mResetErrorRunnable = new Runnable() {
            @Override
            public void run() {
                ConfirmDeviceCredentialBaseFragment.this.mErrorTextView.setText((CharSequence)"");
            }
        };
    }
    
    private int getUserTypeForWipe() {
        final UserInfo userInfo = this.mUserManager.getUserInfo(this.mDevicePolicyManager.getProfileWithMinimumFailedPasswordsForWipe(this.mEffectiveUserId));
        if (userInfo == null || userInfo.isPrimary()) {
            return 1;
        }
        if (userInfo.isManagedProfile()) {
            return 2;
        }
        return 3;
    }
    
    private int getWipeMessage(final int n) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unrecognized user type:");
                sb.append(n);
                throw new IllegalArgumentException(sb.toString());
            }
            case 3: {
                return 2131888070;
            }
            case 2: {
                return 2131888069;
            }
            case 1: {
                return 2131888067;
            }
        }
    }
    
    private boolean isFingerprintAllowed() {
        final boolean mReturnCredentials = this.mReturnCredentials;
        boolean b2;
        final boolean b = b2 = false;
        if (!mReturnCredentials) {
            b2 = b;
            if (this.getActivity().getIntent().getBooleanExtra("com.android.settings.ConfirmCredentials.allowFpAuthentication", false)) {
                b2 = b;
                if (!this.isStrongAuthRequired()) {
                    b2 = b;
                    if (!this.isFingerprintDisabledByAdmin()) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    private boolean isFingerprintDisabledByAdmin() {
        return (this.mDevicePolicyManager.getKeyguardDisabledFeatures((ComponentName)null, this.mEffectiveUserId) & 0x20) != 0x0;
    }
    
    private boolean isInternalActivity() {
        return this.getActivity() instanceof ConfirmLockPassword.InternalActivity || this.getActivity() instanceof ConfirmLockPattern.InternalActivity;
    }
    
    private void setWorkChallengeBackground(final View view, final int n) {
        final View viewById = this.getActivity().findViewById(2131362353);
        if (viewById != null) {
            viewById.setPadding(0, 0, 0, 0);
        }
        view.setBackground((Drawable)new ColorDrawable(this.mDevicePolicyManager.getOrganizationColorForUser(n)));
        final ImageView imageView = (ImageView)view.findViewById(2131361896);
        if (imageView != null) {
            final Drawable drawable = this.getResources().getDrawable(2131231307);
            drawable.setColorFilter(this.getResources().getColor(2131099719), PorterDuff.Mode.DARKEN);
            imageView.setImageDrawable(drawable);
            final Point point = new Point();
            this.getActivity().getWindowManager().getDefaultDisplay().getSize(point);
            imageView.setLayoutParams((ViewGroup.LayoutParams)new FrameLayout$LayoutParams(-1, point.y));
        }
    }
    
    protected abstract void authenticationSucceeded();
    
    protected void checkForPendingIntent() {
        final int intExtra = this.getActivity().getIntent().getIntExtra("android.intent.extra.TASK_ID", -1);
        if (intExtra != -1) {
            try {
                ActivityManager.getService().startActivityFromRecents(intExtra, ActivityOptions.makeBasic().toBundle());
                return;
            }
            catch (RemoteException ex) {}
        }
        final IntentSender intentSender = (IntentSender)this.getActivity().getIntent().getParcelableExtra("android.intent.extra.INTENT");
        if (intentSender != null) {
            try {
                this.getActivity().startIntentSenderForResult(intentSender, -1, (Intent)null, 0, 0, 0);
            }
            catch (IntentSender$SendIntentException ex2) {}
        }
    }
    
    protected abstract int getLastTryErrorMessage(final int p0);
    
    protected boolean isStrongAuthRequired() {
        return this.mFrp || !this.mLockPatternUtils.isFingerprintAllowedForUser(this.mEffectiveUserId) || !this.mUserManager.isUserUnlocked(this.mUserId);
    }
    
    @Override
    public void onAuthenticated() {
        if (this.getActivity() != null && this.getActivity().isResumed()) {
            ((TrustManager)this.getActivity().getSystemService("trust")).setDeviceLockedForUser(this.mEffectiveUserId, false);
            this.authenticationSucceeded();
            this.checkForPendingIntent();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFrpAlternateButtonText = this.getActivity().getIntent().getCharSequenceExtra("android.app.extra.ALTERNATE_BUTTON_LABEL");
        final Intent intent = this.getActivity().getIntent();
        boolean mFrp = false;
        this.mReturnCredentials = intent.getBooleanExtra("return_credentials", false);
        this.mUserId = Utils.getUserIdFromBundle((Context)this.getActivity(), this.getActivity().getIntent().getExtras(), this.isInternalActivity());
        if (this.mUserId == -9999) {
            mFrp = true;
        }
        this.mFrp = mFrp;
        this.mUserManager = UserManager.get((Context)this.getActivity());
        this.mEffectiveUserId = this.mUserManager.getCredentialOwnerProfile(this.mUserId);
        this.mLockPatternUtils = new LockPatternUtils((Context)this.getActivity());
        this.mDevicePolicyManager = (DevicePolicyManager)this.getActivity().getSystemService("device_policy");
    }
    
    @Override
    public void onFingerprintIconVisibilityChanged(final boolean b) {
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (this.mFingerprintHelper.isListening()) {
            this.mFingerprintHelper.stopListening();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.refreshLockScreen();
    }
    
    protected abstract void onShowError();
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.mCancelButton = (Button)view.findViewById(2131361960);
        this.mFingerprintIcon = (ImageView)view.findViewById(2131362146);
        this.mFingerprintHelper = new FingerprintUiHelper(this.mFingerprintIcon, (TextView)view.findViewById(2131362126), (FingerprintUiHelper.Callback)this, this.mEffectiveUserId);
        final Intent intent = this.getActivity().getIntent();
        final int n = 0;
        final boolean booleanExtra = intent.getBooleanExtra("com.android.settings.ConfirmCredentials.showCancelButton", false);
        final boolean b = this.mFrp && !TextUtils.isEmpty(this.mFrpAlternateButtonText);
        final Button mCancelButton = this.mCancelButton;
        int visibility = n;
        if (!booleanExtra) {
            if (b) {
                visibility = n;
            }
            else {
                visibility = 8;
            }
        }
        mCancelButton.setVisibility(visibility);
        if (b) {
            this.mCancelButton.setText(this.mFrpAlternateButtonText);
        }
        this.mCancelButton.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (b) {
                    ConfirmDeviceCredentialBaseFragment.this.getActivity().setResult(1);
                }
                ConfirmDeviceCredentialBaseFragment.this.getActivity().finish();
            }
        });
        final int credentialOwnerUserId = Utils.getCredentialOwnerUserId((Context)this.getActivity(), Utils.getUserIdFromBundle((Context)this.getActivity(), this.getActivity().getIntent().getExtras(), this.isInternalActivity()));
        if (this.mUserManager.isManagedProfile(credentialOwnerUserId)) {
            this.setWorkChallengeBackground(view, credentialOwnerUserId);
        }
    }
    
    public void prepareEnterAnimation() {
    }
    
    protected void refreshLockScreen() {
        if (this.isFingerprintAllowed()) {
            this.mFingerprintHelper.startListening();
        }
        else if (this.mFingerprintHelper.isListening()) {
            this.mFingerprintHelper.stopListening();
        }
        this.updateErrorMessage(this.mLockPatternUtils.getCurrentFailedPasswordAttempts(this.mEffectiveUserId));
    }
    
    protected void reportFailedAttempt() {
        this.updateErrorMessage(this.mLockPatternUtils.getCurrentFailedPasswordAttempts(this.mEffectiveUserId) + 1);
        this.mLockPatternUtils.reportFailedPasswordAttempt(this.mEffectiveUserId);
    }
    
    protected void reportSuccessfulAttempt() {
        this.mLockPatternUtils.reportSuccessfulPasswordAttempt(this.mEffectiveUserId);
        if (this.mUserManager.isManagedProfile(this.mEffectiveUserId)) {
            this.mLockPatternUtils.userPresent(this.mEffectiveUserId);
        }
    }
    
    protected void setAccessibilityTitle(final CharSequence title) {
        final Intent intent = this.getActivity().getIntent();
        if (intent != null) {
            final CharSequence charSequenceExtra = intent.getCharSequenceExtra("com.android.settings.ConfirmCredentials.title");
            if (title == null) {
                return;
            }
            if (charSequenceExtra == null) {
                this.getActivity().setTitle(title);
            }
            else {
                final StringBuilder sb = new StringBuilder(charSequenceExtra);
                sb.append(",");
                sb.append(title);
                this.getActivity().setTitle((CharSequence)Utils.createAccessibleSequence(charSequenceExtra, sb.toString()));
            }
        }
    }
    
    protected void showError(final int n, final long n2) {
        this.showError(this.getText(n), n2);
    }
    
    protected void showError(final CharSequence text, final long n) {
        this.mErrorTextView.setText(text);
        this.onShowError();
        this.mHandler.removeCallbacks(this.mResetErrorRunnable);
        if (n != 0L) {
            this.mHandler.postDelayed(this.mResetErrorRunnable, n);
        }
    }
    
    public void startEnterAnimation() {
    }
    
    protected void updateErrorMessage(int userTypeForWipe) {
        final int maximumFailedPasswordsForWipe = this.mLockPatternUtils.getMaximumFailedPasswordsForWipe(this.mEffectiveUserId);
        if (maximumFailedPasswordsForWipe <= 0 || userTypeForWipe <= 0) {
            return;
        }
        if (this.mErrorTextView != null) {
            this.showError(this.getActivity().getString(2131888066, new Object[] { userTypeForWipe, maximumFailedPasswordsForWipe }), 0L);
        }
        final int n = maximumFailedPasswordsForWipe - userTypeForWipe;
        if (n > 1) {
            return;
        }
        final FragmentManager childFragmentManager = this.getChildFragmentManager();
        userTypeForWipe = this.getUserTypeForWipe();
        if (n == 1) {
            LastTryDialog.show(childFragmentManager, this.getActivity().getString(2131888072), this.getLastTryErrorMessage(userTypeForWipe), 17039370, false);
        }
        else {
            LastTryDialog.show(childFragmentManager, null, this.getWipeMessage(userTypeForWipe), 2131888068, true);
        }
    }
    
    public static class LastTryDialog extends DialogFragment
    {
        private static final String TAG;
        
        static {
            TAG = LastTryDialog.class.getSimpleName();
        }
        
        static boolean show(final FragmentManager fragmentManager, final String s, final int n, final int n2, final boolean b) {
            final LastTryDialog lastTryDialog = (LastTryDialog)fragmentManager.findFragmentByTag(LastTryDialog.TAG);
            if (lastTryDialog != null && !lastTryDialog.isRemoving()) {
                return false;
            }
            final Bundle arguments = new Bundle();
            arguments.putString("title", s);
            arguments.putInt("message", n);
            arguments.putInt("button", n2);
            arguments.putBoolean("dismiss", b);
            final LastTryDialog lastTryDialog2 = new LastTryDialog();
            lastTryDialog2.setArguments(arguments);
            lastTryDialog2.show(fragmentManager, LastTryDialog.TAG);
            fragmentManager.executePendingTransactions();
            return true;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final AlertDialog create = new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)this.getArguments().getString("title")).setMessage(this.getArguments().getInt("message")).setPositiveButton(this.getArguments().getInt("button"), (DialogInterface$OnClickListener)null).create();
            ((Dialog)create).setCanceledOnTouchOutside(false);
            return (Dialog)create;
        }
        
        public void onDismiss(final DialogInterface dialogInterface) {
            super.onDismiss(dialogInterface);
            if (this.getActivity() != null && this.getArguments().getBoolean("dismiss")) {
                this.getActivity().finish();
            }
        }
    }
}
