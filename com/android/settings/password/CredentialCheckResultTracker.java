package com.android.settings.password;

import android.os.Bundle;
import android.content.Intent;
import android.app.Fragment;

public class CredentialCheckResultTracker extends Fragment
{
    private boolean mHasResult;
    private Listener mListener;
    private Intent mResultData;
    private int mResultEffectiveUserId;
    private boolean mResultMatched;
    private int mResultTimeoutMs;
    
    public CredentialCheckResultTracker() {
        this.mHasResult = false;
    }
    
    public void clearResult() {
        this.mHasResult = false;
        this.mResultMatched = false;
        this.mResultData = null;
        this.mResultTimeoutMs = 0;
        this.mResultEffectiveUserId = 0;
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setRetainInstance(true);
    }
    
    public void setListener(final Listener mListener) {
        if (this.mListener == mListener) {
            return;
        }
        this.mListener = mListener;
        if (this.mListener != null && this.mHasResult) {
            this.mListener.onCredentialChecked(this.mResultMatched, this.mResultData, this.mResultTimeoutMs, this.mResultEffectiveUserId, false);
        }
    }
    
    public void setResult(final boolean mResultMatched, final Intent mResultData, final int mResultTimeoutMs, final int mResultEffectiveUserId) {
        this.mResultMatched = mResultMatched;
        this.mResultData = mResultData;
        this.mResultTimeoutMs = mResultTimeoutMs;
        this.mResultEffectiveUserId = mResultEffectiveUserId;
        this.mHasResult = true;
        if (this.mListener != null) {
            this.mListener.onCredentialChecked(this.mResultMatched, this.mResultData, this.mResultTimeoutMs, this.mResultEffectiveUserId, true);
            this.mHasResult = false;
        }
    }
    
    interface Listener
    {
        void onCredentialChecked(final boolean p0, final Intent p1, final int p2, final int p3, final boolean p4);
    }
}
