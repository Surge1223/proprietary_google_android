package com.android.settings.password;

import android.graphics.drawable.Drawable;
import com.android.setupwizardlib.DividerItemDecoration;
import com.android.settings.utils.SettingsDividerItemDecoration;
import android.view.View;
import android.support.v7.preference.Preference;
import com.android.setupwizardlib.GlifPreferenceLayout;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.UserHandle;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.fingerprint.SetupFingerprintEnrollFindSensor;
import com.android.settings.SetupEncryptionInterstitial;
import android.content.Intent;
import android.content.Context;
import android.widget.LinearLayout;
import android.os.Bundle;
import com.android.settings.SetupWizardUtils;
import android.content.res.Resources.Theme;
import android.support.v14.preference.PreferenceFragment;

public class SetupChooseLockGeneric extends ChooseLockGeneric
{
    @Override
    Class<? extends PreferenceFragment> getFragmentClass() {
        return SetupChooseLockGenericFragment.class;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return SetupChooseLockGenericFragment.class.getName().equals(s);
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        super.onApplyThemeResource(resources$Theme, SetupWizardUtils.getTheme(this.getIntent()), b);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public static class SetupChooseLockGenericFragment extends ChooseLockGenericFragment
    {
        @Override
        protected void addHeaderView() {
            if (this.mForFingerprint) {
                this.setHeaderView(2131558748);
            }
            else {
                this.setHeaderView(2131558749);
            }
        }
        
        @Override
        protected void addPreferences() {
            if (this.mForFingerprint) {
                super.addPreferences();
            }
            else {
                this.addPreferencesFromResource(2132082828);
            }
        }
        
        @Override
        protected boolean canRunBeforeDeviceProvisioned() {
            return true;
        }
        
        @Override
        protected void disableUnusablePreferences(final int n, final boolean b) {
            super.disableUnusablePreferencesImpl(Math.max(n, 65536), true);
        }
        
        @Override
        protected Intent getEncryptionInterstitialIntent(final Context context, final int n, final boolean b, final Intent intent) {
            final Intent startIntent = SetupEncryptionInterstitial.createStartIntent(context, n, b, intent);
            SetupWizardUtils.copySetupExtras(this.getActivity().getIntent(), startIntent);
            return startIntent;
        }
        
        @Override
        protected Intent getFindSensorIntent(final Context context) {
            final Intent intent = new Intent(context, (Class)SetupFingerprintEnrollFindSensor.class);
            SetupWizardUtils.copySetupExtras(this.getActivity().getIntent(), intent);
            return intent;
        }
        
        @Override
        protected Intent getLockPasswordIntent(final int n, final int n2, final int n3) {
            final Intent modifyIntentForSetup = SetupChooseLockPassword.modifyIntentForSetup(this.getContext(), super.getLockPasswordIntent(n, n2, n3));
            SetupWizardUtils.copySetupExtras(this.getActivity().getIntent(), modifyIntentForSetup);
            return modifyIntentForSetup;
        }
        
        @Override
        protected Intent getLockPatternIntent() {
            final Intent modifyIntentForSetup = SetupChooseLockPattern.modifyIntentForSetup(this.getContext(), super.getLockPatternIntent());
            SetupWizardUtils.copySetupExtras(this.getActivity().getIntent(), modifyIntentForSetup);
            return modifyIntentForSetup;
        }
        
        @Override
        public void onActivityResult(final int n, final int n2, final Intent intent) {
            Intent intent2 = intent;
            if (intent == null) {
                intent2 = new Intent();
            }
            intent2.putExtra(":settings:password_quality", new LockPatternUtils((Context)this.getActivity()).getKeyguardStoredPasswordQuality(UserHandle.myUserId()));
            super.onActivityResult(n, n2, intent2);
        }
        
        @Override
        public RecyclerView onCreateRecyclerView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            return ((GlifPreferenceLayout)viewGroup).onCreateRecyclerView(layoutInflater, viewGroup, bundle);
        }
        
        @Override
        public boolean onPreferenceTreeClick(final Preference preference) {
            if ("unlock_set_do_later".equals(preference.getKey())) {
                SetupSkipDialog.newInstance(this.getActivity().getIntent().getBooleanExtra(":settings:frp_supported", false)).show(this.getFragmentManager());
                return true;
            }
            return super.onPreferenceTreeClick(preference);
        }
        
        @Override
        public void onViewCreated(final View view, final Bundle bundle) {
            super.onViewCreated(view, bundle);
            final GlifPreferenceLayout glifPreferenceLayout = (GlifPreferenceLayout)view;
            glifPreferenceLayout.setDividerItemDecoration(new SettingsDividerItemDecoration(this.getContext()));
            glifPreferenceLayout.setDividerInset(this.getContext().getResources().getDimensionPixelSize(2131165636));
            glifPreferenceLayout.setIcon(this.getContext().getDrawable(2131231057));
            int n;
            if (this.mForFingerprint) {
                n = 2131888097;
            }
            else {
                n = 2131889075;
            }
            if (this.getActivity() != null) {
                this.getActivity().setTitle(n);
            }
            glifPreferenceLayout.setHeaderText(n);
            this.setDivider(null);
        }
    }
}
