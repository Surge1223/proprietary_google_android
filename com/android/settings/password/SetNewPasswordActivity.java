package com.android.settings.password;

import android.util.Log;
import android.content.Intent;
import android.content.Context;
import com.android.settings.Utils;
import android.os.Bundle;
import android.app.Activity;

public class SetNewPasswordActivity extends Activity implements Ui
{
    private String mNewPasswordAction;
    private SetNewPasswordController mSetNewPasswordController;
    
    public void launchChooseLock(final Bundle bundle) {
        Intent intent;
        if (Utils.isDeviceProvisioned((Context)this) ^ true) {
            intent = new Intent((Context)this, (Class)SetupChooseLockGeneric.class);
        }
        else {
            intent = new Intent((Context)this, (Class)ChooseLockGeneric.class);
        }
        intent.setAction(this.mNewPasswordAction);
        intent.putExtras(bundle);
        this.startActivity(intent);
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mNewPasswordAction = this.getIntent().getAction();
        if (!"android.app.action.SET_NEW_PASSWORD".equals(this.mNewPasswordAction) && !"android.app.action.SET_NEW_PARENT_PROFILE_PASSWORD".equals(this.mNewPasswordAction)) {
            Log.e("SetNewPasswordActivity", "Unexpected action to launch this activity");
            this.finish();
            return;
        }
        (this.mSetNewPasswordController = SetNewPasswordController.create((Context)this, (SetNewPasswordController.Ui)this, this.getIntent(), this.getActivityToken())).dispatchSetNewPasswordIntent();
    }
}
