package com.android.settings.password;

import android.app.FragmentManager;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$ChooseLockGeneric$ChooseLockGenericFragment$FactoryResetProtectionWarningDialog$Abdb_f1FnDmiVy0c3RZHU7n2B2k implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        ChooseLockGeneric.ChooseLockGenericFragment.FactoryResetProtectionWarningDialog.lambda$onCreateDialog$0(this.f$0, this.f$1, dialogInterface, n);
    }
}
