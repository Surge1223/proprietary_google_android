package com.android.settings.password;

import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.graphics.drawable.Drawable;
import java.util.List;
import android.widget.ArrayAdapter;
import com.android.setupwizardlib.util.WizardManagerHelper;
import android.app.Activity;
import android.widget.ListAdapter;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.Context;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.content.Intent;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ChooseLockTypeDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    private ScreenLockAdapter mAdapter;
    private ChooseLockGenericController mController;
    
    private static void copyBooleanExtra(final Intent intent, final Intent intent2, final String s, final boolean b) {
        intent2.putExtra(s, intent.getBooleanExtra(s, b));
    }
    
    public static ChooseLockTypeDialogFragment newInstance(final int n) {
        final Bundle arguments = new Bundle();
        arguments.putInt("userId", n);
        final ChooseLockTypeDialogFragment chooseLockTypeDialogFragment = new ChooseLockTypeDialogFragment();
        chooseLockTypeDialogFragment.setArguments(arguments);
        return chooseLockTypeDialogFragment;
    }
    
    public int getMetricsCategory() {
        return 990;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        OnLockTypeSelectedListener onLockTypeSelectedListener = null;
        final Fragment parentFragment = this.getParentFragment();
        if (parentFragment instanceof OnLockTypeSelectedListener) {
            onLockTypeSelectedListener = (OnLockTypeSelectedListener)parentFragment;
        }
        else {
            final Context context = this.getContext();
            if (context instanceof OnLockTypeSelectedListener) {
                onLockTypeSelectedListener = (OnLockTypeSelectedListener)context;
            }
        }
        if (onLockTypeSelectedListener != null) {
            onLockTypeSelectedListener.onLockTypeSelected((ScreenLockType)this.mAdapter.getItem(n));
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mController = new ChooseLockGenericController(this.getContext(), this.getArguments().getInt("userId"));
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Context context = this.getContext();
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(context);
        alertDialog$Builder.setAdapter((ListAdapter)(this.mAdapter = new ScreenLockAdapter(context, this.mController.getVisibleScreenLockTypes(65536, false), this.mController)), (DialogInterface$OnClickListener)this);
        alertDialog$Builder.setTitle(2131889073);
        return (Dialog)alertDialog$Builder.create();
    }
    
    public interface OnLockTypeSelectedListener
    {
        void onLockTypeSelected(final ScreenLockType p0);
        
        default void startChooseLockActivity(final ScreenLockType screenLockType, final Activity activity) {
            final Intent intent = activity.getIntent();
            final Intent intent2 = new Intent((Context)activity, (Class)SetupChooseLockGeneric.class);
            intent2.addFlags(33554432);
            copyBooleanExtra(intent, intent2, "has_challenge", false);
            copyBooleanExtra(intent, intent2, "show_options_button", false);
            if (intent.hasExtra("choose_lock_generic_extras")) {
                intent2.putExtras(intent.getBundleExtra("choose_lock_generic_extras"));
            }
            intent2.putExtra("lockscreen.password_type", screenLockType.defaultQuality);
            intent2.putExtra("challenge", intent.getLongExtra("challenge", 0L));
            WizardManagerHelper.copyWizardManagerExtras(intent, intent2);
            activity.startActivity(intent2);
            activity.finish();
        }
    }
    
    private static class ScreenLockAdapter extends ArrayAdapter<ScreenLockType>
    {
        private final ChooseLockGenericController mController;
        
        ScreenLockAdapter(final Context context, final List<ScreenLockType> list, final ChooseLockGenericController mController) {
            super(context, 2131558481, (List)list);
            this.mController = mController;
        }
        
        private static Drawable getIconForScreenLock(final Context context, final ScreenLockType screenLockType) {
            switch (screenLockType) {
                default: {
                    return null;
                }
                case PASSWORD: {
                    return context.getDrawable(2131231089);
                }
                case PIN: {
                    return context.getDrawable(2131231095);
                }
                case PATTERN: {
                    return context.getDrawable(2131231090);
                }
            }
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final Context context = viewGroup.getContext();
            View inflate = view;
            if (view == null) {
                inflate = LayoutInflater.from(context).inflate(2131558481, viewGroup, false);
            }
            final ScreenLockType screenLockType = (ScreenLockType)this.getItem(n);
            final TextView textView = (TextView)inflate;
            textView.setText(this.mController.getTitle(screenLockType));
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(getIconForScreenLock(context, screenLockType), (Drawable)null, (Drawable)null, (Drawable)null);
            return inflate;
        }
    }
}
