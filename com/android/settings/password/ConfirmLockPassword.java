package com.android.settings.password;

import android.view.KeyEvent;
import android.view.animation.AnimationUtils;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.internal.widget.LockPatternChecker$OnVerifyCallback;
import com.android.internal.widget.LockPatternChecker;
import com.android.internal.widget.LockPatternChecker$OnCheckCallback;
import android.text.TextUtils;
import android.os.SystemClock;
import android.content.Context;
import android.os.UserManager;
import java.util.ArrayList;
import android.view.View;
import android.os.AsyncTask;
import com.android.internal.widget.TextViewInputDisabler;
import com.android.settings.widget.ImeAwareEditText;
import android.view.inputmethod.InputMethodManager;
import com.android.settingslib.animation.DisappearAnimationUtils;
import android.widget.TextView;
import android.os.CountDownTimer;
import com.android.settingslib.animation.AppearAnimationUtils;
import android.widget.TextView$OnEditorActionListener;
import android.view.View.OnClickListener;
import android.app.Fragment;
import android.content.Intent;

public class ConfirmLockPassword extends ConfirmDeviceCredentialBaseActivity
{
    private static final int[] DETAIL_TEXTS;
    
    static {
        DETAIL_TEXTS = new int[] { 2131888139, 2131888129, 2131888140, 2131888130, 2131888166, 2131888164, 2131888169, 2131888167 };
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", ConfirmLockPasswordFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return ConfirmLockPasswordFragment.class.getName().equals(s);
    }
    
    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        final Fragment fragmentById = this.getFragmentManager().findFragmentById(2131362353);
        if (fragmentById != null && fragmentById instanceof ConfirmLockPasswordFragment) {
            ((ConfirmLockPasswordFragment)fragmentById).onWindowFocusChanged(b);
        }
    }
    
    public static class ConfirmLockPasswordFragment extends ConfirmDeviceCredentialBaseFragment implements View.OnClickListener, TextView$OnEditorActionListener, Listener
    {
        private AppearAnimationUtils mAppearAnimationUtils;
        private CountDownTimer mCountdownTimer;
        private CredentialCheckResultTracker mCredentialCheckResultTracker;
        private TextView mDetailsTextView;
        private DisappearAnimationUtils mDisappearAnimationUtils;
        private boolean mDisappearing;
        private TextView mHeaderTextView;
        private InputMethodManager mImm;
        private boolean mIsAlpha;
        private ImeAwareEditText mPasswordEntry;
        private TextViewInputDisabler mPasswordEntryInputDisabler;
        private AsyncTask<?, ?, ?> mPendingLockCheck;
        private boolean mUsingFingerprint;
        
        public ConfirmLockPasswordFragment() {
            this.mDisappearing = false;
            this.mUsingFingerprint = false;
        }
        
        private View[] getActiveViews() {
            final ArrayList<TextView> list = new ArrayList<TextView>();
            list.add(this.mHeaderTextView);
            list.add(this.mDetailsTextView);
            if (this.mCancelButton.getVisibility() == 0) {
                list.add((TextView)this.mCancelButton);
            }
            list.add((TextView)this.mPasswordEntry);
            list.add(this.mErrorTextView);
            if (this.mFingerprintIcon.getVisibility() == 0) {
                list.add((TextView)this.mFingerprintIcon);
            }
            return list.toArray(new View[0]);
        }
        
        private int getDefaultDetails() {
            if (this.mFrp) {
                int n;
                if (this.mIsAlpha) {
                    n = 2131888128;
                }
                else {
                    n = 2131888138;
                }
                return n;
            }
            return ConfirmLockPassword.DETAIL_TEXTS[((this.isStrongAuthRequired() ? 1 : 0) << 2) + ((UserManager.get((Context)this.getActivity()).isManagedProfile(this.mEffectiveUserId) ? 1 : 0) << 1) + (this.mIsAlpha ? 1 : 0)];
        }
        
        private int getDefaultHeader() {
            if (this.mFrp) {
                int n;
                if (this.mIsAlpha) {
                    n = 2131888132;
                }
                else {
                    n = 2131888142;
                }
                return n;
            }
            int n2;
            if (this.mIsAlpha) {
                n2 = 2131888131;
            }
            else {
                n2 = 2131888141;
            }
            return n2;
        }
        
        private int getErrorMessage() {
            int n;
            if (this.mIsAlpha) {
                n = 2131888146;
            }
            else {
                n = 2131888147;
            }
            return n;
        }
        
        private void handleAttemptLockout(final long n) {
            this.mCountdownTimer = new CountDownTimer(n - SystemClock.elapsedRealtime(), 1000L) {
                public void onFinish() {
                    ConfirmLockPasswordFragment.this.updatePasswordEntry();
                    ConfirmLockPasswordFragment.this.mErrorTextView.setText((CharSequence)"");
                    ConfirmLockPasswordFragment.this.updateErrorMessage(ConfirmLockPasswordFragment.this.mLockPatternUtils.getCurrentFailedPasswordAttempts(ConfirmLockPasswordFragment.this.mEffectiveUserId));
                }
                
                public void onTick(final long n) {
                    ConfirmLockPasswordFragment.this.showError(ConfirmLockPasswordFragment.this.getString(2131888199, new Object[] { (int)(n / 1000L) }), 0L);
                }
            }.start();
            this.updatePasswordEntry();
        }
        
        private void handleNext() {
            if (this.mPendingLockCheck != null || this.mDisappearing) {
                return;
            }
            final String string = this.mPasswordEntry.getText().toString();
            if (TextUtils.isEmpty((CharSequence)string)) {
                return;
            }
            this.mPasswordEntryInputDisabler.setInputEnabled(false);
            final boolean booleanExtra = this.getActivity().getIntent().getBooleanExtra("has_challenge", false);
            final Intent intent = new Intent();
            if (!booleanExtra) {
                this.startCheckPassword(string, intent);
                return;
            }
            if (this.isInternalActivity()) {
                this.startVerifyPassword(string, intent);
                return;
            }
            this.mCredentialCheckResultTracker.setResult(false, intent, 0, this.mEffectiveUserId);
        }
        
        private boolean isInternalActivity() {
            return this.getActivity() instanceof InternalActivity;
        }
        
        private void onPasswordChecked(final boolean b, final Intent intent, final int n, final int n2, final boolean b2) {
            this.mPasswordEntryInputDisabler.setInputEnabled(true);
            if (b) {
                if (b2) {
                    this.reportSuccessfulAttempt();
                }
                this.startDisappearAnimation(intent);
                this.checkForPendingIntent();
            }
            else {
                if (n > 0) {
                    this.refreshLockScreen();
                    this.handleAttemptLockout(this.mLockPatternUtils.setLockoutAttemptDeadline(n2, n));
                }
                else {
                    this.showError(this.getErrorMessage(), 3000L);
                }
                if (b2) {
                    this.reportFailedAttempt();
                }
            }
        }
        
        private void startCheckPassword(final String s, final Intent intent) {
            final int mEffectiveUserId = this.mEffectiveUserId;
            this.mPendingLockCheck = (AsyncTask<?, ?, ?>)LockPatternChecker.checkPassword(this.mLockPatternUtils, s, mEffectiveUserId, (LockPatternChecker$OnCheckCallback)new LockPatternChecker$OnCheckCallback() {
                public void onChecked(final boolean b, final int n) {
                    ConfirmLockPasswordFragment.this.mPendingLockCheck = null;
                    if (b && ConfirmLockPasswordFragment.this.isInternalActivity() && ConfirmLockPasswordFragment.this.mReturnCredentials) {
                        final Intent val$intent = intent;
                        int n2;
                        if (ConfirmLockPasswordFragment.this.mIsAlpha) {
                            n2 = 0;
                        }
                        else {
                            n2 = 2;
                        }
                        val$intent.putExtra("type", n2);
                        intent.putExtra("password", s);
                    }
                    ConfirmLockPasswordFragment.this.mCredentialCheckResultTracker.setResult(b, intent, n, mEffectiveUserId);
                }
            });
        }
        
        private void startDisappearAnimation(final Intent intent) {
            if (this.mDisappearing) {
                return;
            }
            this.mDisappearing = true;
            final ConfirmLockPassword confirmLockPassword = (ConfirmLockPassword)this.getActivity();
            if (confirmLockPassword != null && !confirmLockPassword.isFinishing()) {
                if (confirmLockPassword.getConfirmCredentialTheme() == ConfirmCredentialTheme.DARK) {
                    this.mDisappearAnimationUtils.startAnimation(this.getActiveViews(), new _$$Lambda$ConfirmLockPassword$ConfirmLockPasswordFragment$hwD4uLqRx_u_wyU3V7MV_afxC5o(confirmLockPassword, intent));
                }
                else {
                    confirmLockPassword.setResult(-1, intent);
                    confirmLockPassword.finish();
                }
            }
        }
        
        private void startVerifyPassword(final String s, final Intent intent) {
            final long longExtra = this.getActivity().getIntent().getLongExtra("challenge", 0L);
            final int mEffectiveUserId = this.mEffectiveUserId;
            final int mUserId = this.mUserId;
            final LockPatternChecker$OnVerifyCallback lockPatternChecker$OnVerifyCallback = (LockPatternChecker$OnVerifyCallback)new LockPatternChecker$OnVerifyCallback() {
                public void onVerified(final byte[] array, final int n) {
                    ConfirmLockPasswordFragment.this.mPendingLockCheck = null;
                    boolean b = false;
                    if (array != null) {
                        b = true;
                        if (ConfirmLockPasswordFragment.this.mReturnCredentials) {
                            intent.putExtra("hw_auth_token", array);
                            b = b;
                        }
                    }
                    ConfirmLockPasswordFragment.this.mCredentialCheckResultTracker.setResult(b, intent, n, mEffectiveUserId);
                }
            };
            AsyncTask mPendingLockCheck;
            if (mEffectiveUserId == mUserId) {
                mPendingLockCheck = LockPatternChecker.verifyPassword(this.mLockPatternUtils, s, longExtra, mUserId, (LockPatternChecker$OnVerifyCallback)lockPatternChecker$OnVerifyCallback);
            }
            else {
                mPendingLockCheck = LockPatternChecker.verifyTiedProfileChallenge(this.mLockPatternUtils, s, false, longExtra, mUserId, (LockPatternChecker$OnVerifyCallback)lockPatternChecker$OnVerifyCallback);
            }
            this.mPendingLockCheck = (AsyncTask<?, ?, ?>)mPendingLockCheck;
        }
        
        private void updatePasswordEntry() {
            final long lockoutAttemptDeadline = this.mLockPatternUtils.getLockoutAttemptDeadline(this.mEffectiveUserId);
            final boolean b = true;
            final boolean b2 = lockoutAttemptDeadline != 0L;
            this.mPasswordEntry.setEnabled(!b2);
            this.mPasswordEntryInputDisabler.setInputEnabled(!b2 && b);
            if (!b2 && !this.mUsingFingerprint) {
                this.mPasswordEntry.scheduleShowSoftInput();
            }
            else {
                this.mImm.hideSoftInputFromWindow(this.mPasswordEntry.getWindowToken(), 0);
            }
        }
        
        @Override
        protected void authenticationSucceeded() {
            this.mCredentialCheckResultTracker.setResult(true, new Intent(), 0, this.mEffectiveUserId);
        }
        
        @Override
        protected int getLastTryErrorMessage(int n) {
            switch (n) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unrecognized user type:");
                    sb.append(n);
                    throw new IllegalArgumentException(sb.toString());
                }
                case 3: {
                    if (this.mIsAlpha) {
                        n = 2131888075;
                    }
                    else {
                        n = 2131888081;
                    }
                    return n;
                }
                case 2: {
                    if (this.mIsAlpha) {
                        n = 2131888074;
                    }
                    else {
                        n = 2131888080;
                    }
                    return n;
                }
                case 1: {
                    if (this.mIsAlpha) {
                        n = 2131888073;
                    }
                    else {
                        n = 2131888079;
                    }
                    return n;
                }
            }
        }
        
        public int getMetricsCategory() {
            return 30;
        }
        
        public void onClick(final View view) {
            final int id = view.getId();
            if (id != 2131361962) {
                if (id == 2131362389) {
                    this.handleNext();
                }
            }
            else {
                this.getActivity().setResult(0);
                this.getActivity().finish();
            }
        }
        
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mEffectiveUserId);
            int n;
            if (((ConfirmLockPassword)this.getActivity()).getConfirmCredentialTheme() == ConfirmCredentialTheme.NORMAL) {
                n = 2131558499;
            }
            else {
                n = 2131558497;
            }
            final View inflate = layoutInflater.inflate(n, viewGroup, false);
            (this.mPasswordEntry = (ImeAwareEditText)inflate.findViewById(2131362439)).setOnEditorActionListener((TextView$OnEditorActionListener)this);
            this.mPasswordEntry.requestFocus();
            this.mPasswordEntryInputDisabler = new TextViewInputDisabler((TextView)this.mPasswordEntry);
            this.mHeaderTextView = (TextView)inflate.findViewById(2131362216);
            if (this.mHeaderTextView == null) {
                this.mHeaderTextView = (TextView)inflate.findViewById(R.id.suw_layout_title);
            }
            this.mDetailsTextView = (TextView)inflate.findViewById(2131362070);
            this.mErrorTextView = (TextView)inflate.findViewById(2131362126);
            this.mIsAlpha = (262144 == keyguardStoredPasswordQuality || 327680 == keyguardStoredPasswordQuality || 393216 == keyguardStoredPasswordQuality || 524288 == keyguardStoredPasswordQuality);
            this.mImm = (InputMethodManager)this.getActivity().getSystemService("input_method");
            final Intent intent = this.getActivity().getIntent();
            if (intent != null) {
                final CharSequence charSequenceExtra = intent.getCharSequenceExtra("com.android.settings.ConfirmCredentials.header");
                final CharSequence charSequenceExtra2 = intent.getCharSequenceExtra("com.android.settings.ConfirmCredentials.details");
                CharSequence string = charSequenceExtra;
                if (TextUtils.isEmpty(charSequenceExtra)) {
                    string = this.getString(this.getDefaultHeader());
                }
                CharSequence string2 = charSequenceExtra2;
                if (TextUtils.isEmpty(charSequenceExtra2)) {
                    string2 = this.getString(this.getDefaultDetails());
                }
                this.mHeaderTextView.setText(string);
                this.mDetailsTextView.setText(string2);
            }
            int inputType = this.mPasswordEntry.getInputType();
            final ImeAwareEditText mPasswordEntry = this.mPasswordEntry;
            if (!this.mIsAlpha) {
                inputType = 18;
            }
            mPasswordEntry.setInputType(inputType);
            this.mPasswordEntry.setTypeface(Typeface.create(this.getContext().getString(17039680), 0));
            this.mAppearAnimationUtils = new AppearAnimationUtils(this.getContext(), 220L, 2.0f, 1.0f, AnimationUtils.loadInterpolator(this.getContext(), 17563662));
            this.mDisappearAnimationUtils = new DisappearAnimationUtils(this.getContext(), 110L, 1.0f, 0.5f, AnimationUtils.loadInterpolator(this.getContext(), 17563663));
            this.setAccessibilityTitle(this.mHeaderTextView.getText());
            this.mCredentialCheckResultTracker = (CredentialCheckResultTracker)this.getFragmentManager().findFragmentByTag("check_lock_result");
            if (this.mCredentialCheckResultTracker == null) {
                this.mCredentialCheckResultTracker = new CredentialCheckResultTracker();
                this.getFragmentManager().beginTransaction().add((Fragment)this.mCredentialCheckResultTracker, "check_lock_result").commit();
            }
            return inflate;
        }
        
        public void onCredentialChecked(final boolean b, final Intent intent, final int n, final int n2, final boolean b2) {
            this.onPasswordChecked(b, intent, n, n2, b2);
        }
        
        public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
            if (n != 0 && n != 6 && n != 5) {
                return false;
            }
            this.handleNext();
            return true;
        }
        
        @Override
        public void onFingerprintIconVisibilityChanged(final boolean mUsingFingerprint) {
            this.mUsingFingerprint = mUsingFingerprint;
        }
        
        @Override
        public void onPause() {
            super.onPause();
            if (this.mCountdownTimer != null) {
                this.mCountdownTimer.cancel();
                this.mCountdownTimer = null;
            }
            this.mCredentialCheckResultTracker.setListener(null);
        }
        
        @Override
        public void onResume() {
            super.onResume();
            final long lockoutAttemptDeadline = this.mLockPatternUtils.getLockoutAttemptDeadline(this.mEffectiveUserId);
            if (lockoutAttemptDeadline != 0L) {
                this.mCredentialCheckResultTracker.clearResult();
                this.handleAttemptLockout(lockoutAttemptDeadline);
            }
            else {
                this.updatePasswordEntry();
                this.mErrorTextView.setText((CharSequence)"");
                this.updateErrorMessage(this.mLockPatternUtils.getCurrentFailedPasswordAttempts(this.mEffectiveUserId));
            }
            this.mCredentialCheckResultTracker.setListener((CredentialCheckResultTracker.Listener)this);
        }
        
        @Override
        protected void onShowError() {
            this.mPasswordEntry.setText((CharSequence)null);
        }
        
        public void onWindowFocusChanged(final boolean b) {
            if (!b) {
                return;
            }
            this.mPasswordEntry.post((Runnable)new _$$Lambda$ConfirmLockPassword$ConfirmLockPasswordFragment$Myp25CGN_sn9Gs6wDwuZ61aKfg8(this));
        }
        
        @Override
        public void prepareEnterAnimation() {
            super.prepareEnterAnimation();
            this.mHeaderTextView.setAlpha(0.0f);
            this.mDetailsTextView.setAlpha(0.0f);
            this.mCancelButton.setAlpha(0.0f);
            this.mPasswordEntry.setAlpha(0.0f);
            this.mErrorTextView.setAlpha(0.0f);
            this.mFingerprintIcon.setAlpha(0.0f);
        }
        
        @Override
        public void startEnterAnimation() {
            super.startEnterAnimation();
            this.mAppearAnimationUtils.startAnimation(this.getActiveViews(), new _$$Lambda$ConfirmLockPassword$ConfirmLockPasswordFragment$Myp25CGN_sn9Gs6wDwuZ61aKfg8(this));
        }
    }
    
    public static class InternalActivity extends ConfirmLockPassword
    {
    }
}
