package com.android.settings.password;

import android.view.KeyEvent;
import android.app.Fragment;
import android.view.animation.AnimationUtils;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.internal.widget.LockPatternChecker$OnVerifyCallback;
import com.android.internal.widget.LockPatternChecker;
import com.android.internal.widget.LockPatternChecker$OnCheckCallback;
import android.text.TextUtils;
import android.os.SystemClock;
import android.content.Context;
import android.os.UserManager;
import java.util.ArrayList;
import android.view.View;
import android.os.AsyncTask;
import com.android.internal.widget.TextViewInputDisabler;
import com.android.settings.widget.ImeAwareEditText;
import android.view.inputmethod.InputMethodManager;
import com.android.settingslib.animation.DisappearAnimationUtils;
import android.widget.TextView;
import android.os.CountDownTimer;
import com.android.settingslib.animation.AppearAnimationUtils;
import android.widget.TextView$OnEditorActionListener;
import android.view.View.OnClickListener;
import android.content.Intent;

public final class _$$Lambda$ConfirmLockPassword$ConfirmLockPasswordFragment$hwD4uLqRx_u_wyU3V7MV_afxC5o implements Runnable
{
    @Override
    public final void run() {
        ConfirmLockPassword.ConfirmLockPasswordFragment.lambda$startDisappearAnimation$0(this.f$0, this.f$1);
    }
}
