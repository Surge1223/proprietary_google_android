package com.android.settings.password;

import android.view.View;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;

public class PasswordRequirementAdapter extends Adapter<PasswordRequirementViewHolder>
{
    private String[] mRequirements;
    
    public PasswordRequirementAdapter() {
        ((RecyclerView.Adapter)this).setHasStableIds(true);
    }
    
    @Override
    public int getItemCount() {
        return this.mRequirements.length;
    }
    
    @Override
    public long getItemId(final int n) {
        return this.mRequirements[n].hashCode();
    }
    
    public void onBindViewHolder(final PasswordRequirementViewHolder passwordRequirementViewHolder, final int n) {
        passwordRequirementViewHolder.mDescriptionText.setText((CharSequence)this.mRequirements[n]);
    }
    
    public PasswordRequirementViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        return new PasswordRequirementViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(2131558639, viewGroup, false));
    }
    
    public void setRequirements(final String[] mRequirements) {
        this.mRequirements = mRequirements;
        ((RecyclerView.Adapter)this).notifyDataSetChanged();
    }
    
    public static class PasswordRequirementViewHolder extends ViewHolder
    {
        private TextView mDescriptionText;
        
        public PasswordRequirementViewHolder(final View view) {
            super(view);
            this.mDescriptionText = (TextView)view;
        }
    }
}
