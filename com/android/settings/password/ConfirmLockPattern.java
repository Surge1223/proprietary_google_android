package com.android.settings.password;

import android.app.Fragment;
import android.view.animation.AnimationUtils;
import com.android.internal.widget.LinearLayoutWithDefaultTouchRecepient;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.animation.Interpolator;
import com.android.internal.widget.LockPatternView$DisplayMode;
import android.os.SystemClock;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.widget.LockPatternView$CellState;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import com.android.internal.widget.LockPatternChecker$OnVerifyCallback;
import com.android.internal.widget.LockPatternChecker;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternChecker$OnCheckCallback;
import com.android.internal.widget.LockPatternView$Cell;
import java.util.List;
import android.os.AsyncTask;
import com.android.internal.widget.LockPatternView;
import android.view.View;
import com.android.settingslib.animation.DisappearAnimationUtils;
import android.widget.TextView;
import android.os.CountDownTimer;
import com.android.internal.widget.LockPatternView$OnPatternListener;
import com.android.settingslib.animation.AppearAnimationUtils;
import com.android.settingslib.animation.AppearAnimationCreator;
import android.content.Intent;

public class ConfirmLockPattern extends ConfirmDeviceCredentialBaseActivity
{
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", ConfirmLockPatternFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return ConfirmLockPatternFragment.class.getName().equals(s);
    }
    
    public static class ConfirmLockPatternFragment extends ConfirmDeviceCredentialBaseFragment implements Listener, AppearAnimationCreator<Object>
    {
        private AppearAnimationUtils mAppearAnimationUtils;
        private Runnable mClearPatternRunnable;
        private LockPatternView$OnPatternListener mConfirmExistingLockPatternListener;
        private CountDownTimer mCountdownTimer;
        private CredentialCheckResultTracker mCredentialCheckResultTracker;
        private CharSequence mDetailsText;
        private TextView mDetailsTextView;
        private DisappearAnimationUtils mDisappearAnimationUtils;
        private boolean mDisappearing;
        private CharSequence mHeaderText;
        private TextView mHeaderTextView;
        private View mLeftSpacerLandscape;
        private LockPatternView mLockPatternView;
        private AsyncTask<?, ?, ?> mPendingLockCheck;
        private View mRightSpacerLandscape;
        
        public ConfirmLockPatternFragment() {
            this.mDisappearing = false;
            this.mClearPatternRunnable = new Runnable() {
                @Override
                public void run() {
                    ConfirmLockPatternFragment.this.mLockPatternView.clearPattern();
                }
            };
            this.mConfirmExistingLockPatternListener = (LockPatternView$OnPatternListener)new LockPatternView$OnPatternListener() {
                private boolean isInternalActivity() {
                    return ConfirmLockPatternFragment.this.getActivity() instanceof InternalActivity;
                }
                
                private void startCheckPattern(final List<LockPatternView$Cell> list, final Intent intent) {
                    if (list.size() < 4) {
                        ConfirmLockPatternFragment.this.onPatternChecked(false, intent, 0, ConfirmLockPatternFragment.this.mEffectiveUserId, false);
                        return;
                    }
                    final int mEffectiveUserId = ConfirmLockPatternFragment.this.mEffectiveUserId;
                    ConfirmLockPatternFragment.this.mPendingLockCheck = (AsyncTask<?, ?, ?>)LockPatternChecker.checkPattern(ConfirmLockPatternFragment.this.mLockPatternUtils, (List)list, mEffectiveUserId, (LockPatternChecker$OnCheckCallback)new LockPatternChecker$OnCheckCallback() {
                        public void onChecked(final boolean b, final int n) {
                            ConfirmLockPatternFragment.this.mPendingLockCheck = null;
                            if (b && ConfirmLockPattern$ConfirmLockPatternFragment$3.this.isInternalActivity() && ConfirmLockPatternFragment.this.mReturnCredentials) {
                                intent.putExtra("type", 3);
                                intent.putExtra("password", LockPatternUtils.patternToString(list));
                            }
                            ConfirmLockPatternFragment.this.mCredentialCheckResultTracker.setResult(b, intent, n, mEffectiveUserId);
                        }
                    });
                }
                
                private void startVerifyPattern(final List<LockPatternView$Cell> list, final Intent intent) {
                    final int mEffectiveUserId = ConfirmLockPatternFragment.this.mEffectiveUserId;
                    final int mUserId = ConfirmLockPatternFragment.this.mUserId;
                    final long longExtra = ConfirmLockPatternFragment.this.getActivity().getIntent().getLongExtra("challenge", 0L);
                    final LockPatternChecker$OnVerifyCallback lockPatternChecker$OnVerifyCallback = (LockPatternChecker$OnVerifyCallback)new LockPatternChecker$OnVerifyCallback() {
                        public void onVerified(final byte[] array, final int n) {
                            ConfirmLockPatternFragment.this.mPendingLockCheck = null;
                            boolean b = false;
                            if (array != null) {
                                b = true;
                                if (ConfirmLockPatternFragment.this.mReturnCredentials) {
                                    intent.putExtra("hw_auth_token", array);
                                    b = b;
                                }
                            }
                            ConfirmLockPatternFragment.this.mCredentialCheckResultTracker.setResult(b, intent, n, mEffectiveUserId);
                        }
                    };
                    final ConfirmLockPatternFragment this$0 = ConfirmLockPatternFragment.this;
                    AsyncTask asyncTask;
                    if (mEffectiveUserId == mUserId) {
                        asyncTask = LockPatternChecker.verifyPattern(ConfirmLockPatternFragment.this.mLockPatternUtils, (List)list, longExtra, mUserId, (LockPatternChecker$OnVerifyCallback)lockPatternChecker$OnVerifyCallback);
                    }
                    else {
                        asyncTask = LockPatternChecker.verifyTiedProfileChallenge(ConfirmLockPatternFragment.this.mLockPatternUtils, LockPatternUtils.patternToString((List)list), true, longExtra, mUserId, (LockPatternChecker$OnVerifyCallback)lockPatternChecker$OnVerifyCallback);
                    }
                    this$0.mPendingLockCheck = (AsyncTask<?, ?, ?>)asyncTask;
                }
                
                public void onPatternCellAdded(final List<LockPatternView$Cell> list) {
                }
                
                public void onPatternCleared() {
                    ConfirmLockPatternFragment.this.mLockPatternView.removeCallbacks(ConfirmLockPatternFragment.this.mClearPatternRunnable);
                }
                
                public void onPatternDetected(final List<LockPatternView$Cell> list) {
                    if (ConfirmLockPatternFragment.this.mPendingLockCheck != null || ConfirmLockPatternFragment.this.mDisappearing) {
                        return;
                    }
                    ConfirmLockPatternFragment.this.mLockPatternView.setEnabled(false);
                    final boolean booleanExtra = ConfirmLockPatternFragment.this.getActivity().getIntent().getBooleanExtra("has_challenge", false);
                    final Intent intent = new Intent();
                    if (!booleanExtra) {
                        this.startCheckPattern(list, intent);
                        return;
                    }
                    if (this.isInternalActivity()) {
                        this.startVerifyPattern(list, intent);
                        return;
                    }
                    ConfirmLockPatternFragment.this.mCredentialCheckResultTracker.setResult(false, intent, 0, ConfirmLockPatternFragment.this.mEffectiveUserId);
                }
                
                public void onPatternStart() {
                    ConfirmLockPatternFragment.this.mLockPatternView.removeCallbacks(ConfirmLockPatternFragment.this.mClearPatternRunnable);
                }
            };
        }
        
        private Object[][] getActiveViews() {
            final ArrayList<ArrayList<Object>> list = new ArrayList<ArrayList<Object>>();
            list.add(new ArrayList<Object>(Collections.singletonList(this.mHeaderTextView)));
            list.add(new ArrayList<Object>(Collections.singletonList(this.mDetailsTextView)));
            if (this.mCancelButton.getVisibility() == 0) {
                list.add(new ArrayList<Object>(Collections.singletonList(this.mCancelButton)));
            }
            final LockPatternView$CellState[][] cellStates = this.mLockPatternView.getCellStates();
            for (int i = 0; i < cellStates.length; ++i) {
                final ArrayList<Object> list2 = new ArrayList<Object>();
                for (int j = 0; j < cellStates[i].length; ++j) {
                    list2.add(cellStates[i][j]);
                }
                list.add(list2);
            }
            if (this.mFingerprintIcon.getVisibility() == 0) {
                list.add(new ArrayList<Object>(Collections.singletonList(this.mFingerprintIcon)));
            }
            final Object[][] array = new Object[list.size()][cellStates[0].length];
            for (int k = 0; k < list.size(); ++k) {
                final ArrayList<Object> list3 = list.get(k);
                for (int l = 0; l < list3.size(); ++l) {
                    array[k][l] = list3.get(l);
                }
            }
            return array;
        }
        
        private int getDefaultDetails() {
            if (this.mFrp) {
                return 2131888133;
            }
            final boolean strongAuthRequired = this.isStrongAuthRequired();
            if (UserManager.get((Context)this.getActivity()).isManagedProfile(this.mEffectiveUserId)) {
                int n;
                if (strongAuthRequired) {
                    n = 2131888168;
                }
                else {
                    n = 2131888135;
                }
                return n;
            }
            int n2;
            if (strongAuthRequired) {
                n2 = 2131888165;
            }
            else {
                n2 = 2131888134;
            }
            return n2;
        }
        
        private int getDefaultHeader() {
            int n;
            if (this.mFrp) {
                n = 2131888137;
            }
            else {
                n = 2131888136;
            }
            return n;
        }
        
        private void handleAttemptLockout(final long n) {
            this.updateStage(Stage.LockedOut);
            this.mCountdownTimer = new CountDownTimer(n - SystemClock.elapsedRealtime(), 1000L) {
                public void onFinish() {
                    ConfirmLockPatternFragment.this.updateStage(Stage.NeedToUnlock);
                }
                
                public void onTick(final long n) {
                    ConfirmLockPatternFragment.this.mErrorTextView.setText((CharSequence)ConfirmLockPatternFragment.this.getString(2131888199, new Object[] { (int)(n / 1000L) }));
                }
            }.start();
        }
        
        private void onPatternChecked(final boolean b, final Intent intent, final int n, final int n2, final boolean b2) {
            this.mLockPatternView.setEnabled(true);
            if (b) {
                if (b2) {
                    this.reportSuccessfulAttempt();
                }
                this.startDisappearAnimation(intent);
                this.checkForPendingIntent();
            }
            else {
                if (n > 0) {
                    this.refreshLockScreen();
                    this.handleAttemptLockout(this.mLockPatternUtils.setLockoutAttemptDeadline(n2, n));
                }
                else {
                    this.updateStage(Stage.NeedToUnlockWrong);
                    this.postClearPatternRunnable();
                }
                if (b2) {
                    this.reportFailedAttempt();
                }
            }
        }
        
        private void postClearPatternRunnable() {
            this.mLockPatternView.removeCallbacks(this.mClearPatternRunnable);
            this.mLockPatternView.postDelayed(this.mClearPatternRunnable, 3000L);
        }
        
        private void startDisappearAnimation(final Intent intent) {
            if (this.mDisappearing) {
                return;
            }
            this.mDisappearing = true;
            final ConfirmLockPattern confirmLockPattern = (ConfirmLockPattern)this.getActivity();
            if (confirmLockPattern != null && !confirmLockPattern.isFinishing()) {
                if (confirmLockPattern.getConfirmCredentialTheme() == ConfirmCredentialTheme.DARK) {
                    this.mLockPatternView.clearPattern();
                    this.mDisappearAnimationUtils.startAnimation2d(this.getActiveViews(), new _$$Lambda$ConfirmLockPattern$ConfirmLockPatternFragment$5mgp_p2Jjy9apKG7HsLV4Zu_sXo(confirmLockPattern, intent), this);
                }
                else {
                    confirmLockPattern.setResult(-1, intent);
                    confirmLockPattern.finish();
                }
            }
        }
        
        private void updateStage(final Stage stage) {
            switch (stage) {
                case LockedOut: {
                    this.mLockPatternView.clearPattern();
                    this.mLockPatternView.setEnabled(false);
                    break;
                }
                case NeedToUnlockWrong: {
                    this.showError(2131888175, 3000L);
                    this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Wrong);
                    this.mLockPatternView.setEnabled(true);
                    this.mLockPatternView.enableInput();
                    break;
                }
                case NeedToUnlock: {
                    if (this.mHeaderText != null) {
                        this.mHeaderTextView.setText(this.mHeaderText);
                    }
                    else {
                        this.mHeaderTextView.setText(this.getDefaultHeader());
                    }
                    if (this.mDetailsText != null) {
                        this.mDetailsTextView.setText(this.mDetailsText);
                    }
                    else {
                        this.mDetailsTextView.setText(this.getDefaultDetails());
                    }
                    this.mErrorTextView.setText((CharSequence)"");
                    this.updateErrorMessage(this.mLockPatternUtils.getCurrentFailedPasswordAttempts(this.mEffectiveUserId));
                    this.mLockPatternView.setEnabled(true);
                    this.mLockPatternView.enableInput();
                    this.mLockPatternView.clearPattern();
                    break;
                }
            }
            this.mHeaderTextView.announceForAccessibility(this.mHeaderTextView.getText());
        }
        
        @Override
        protected void authenticationSucceeded() {
            this.mCredentialCheckResultTracker.setResult(true, new Intent(), 0, this.mEffectiveUserId);
        }
        
        @Override
        public void createAnimation(final Object o, final long n, final long n2, float n3, final boolean b, final Interpolator interpolator, final Runnable runnable) {
            if (o instanceof LockPatternView$CellState) {
                final LockPatternView$CellState lockPatternView$CellState = (LockPatternView$CellState)o;
                final LockPatternView mLockPatternView = this.mLockPatternView;
                float n4;
                if (b) {
                    n4 = 1.0f;
                }
                else {
                    n4 = 0.0f;
                }
                float n5;
                if (b) {
                    n5 = n3;
                }
                else {
                    n5 = 0.0f;
                }
                if (b) {
                    n3 = 0.0f;
                }
                float n6;
                if (b) {
                    n6 = 0.0f;
                }
                else {
                    n6 = 1.0f;
                }
                mLockPatternView.startCellStateAnimation(lockPatternView$CellState, 1.0f, n4, n5, n3, n6, 1.0f, n, n2, interpolator, runnable);
            }
            else {
                this.mAppearAnimationUtils.createAnimation((View)o, n, n2, n3, b, interpolator, runnable);
            }
        }
        
        @Override
        protected int getLastTryErrorMessage(final int n) {
            switch (n) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unrecognized user type:");
                    sb.append(n);
                    throw new IllegalArgumentException(sb.toString());
                }
                case 3: {
                    return 2131888078;
                }
                case 2: {
                    return 2131888077;
                }
                case 1: {
                    return 2131888076;
                }
            }
        }
        
        @Override
        public int getMetricsCategory() {
            return 31;
        }
        
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            int n;
            if (((ConfirmLockPattern)this.getActivity()).getConfirmCredentialTheme() == ConfirmCredentialTheme.NORMAL) {
                n = 2131558502;
            }
            else {
                n = 2131558500;
            }
            final View inflate = layoutInflater.inflate(n, viewGroup, false);
            this.mHeaderTextView = (TextView)inflate.findViewById(2131362216);
            this.mLockPatternView = (LockPatternView)inflate.findViewById(2131362345);
            this.mDetailsTextView = (TextView)inflate.findViewById(2131362070);
            this.mErrorTextView = (TextView)inflate.findViewById(2131362126);
            this.mLeftSpacerLandscape = inflate.findViewById(2131362330);
            this.mRightSpacerLandscape = inflate.findViewById(2131362530);
            ((LinearLayoutWithDefaultTouchRecepient)inflate.findViewById(2131362744)).setDefaultTouchRecepient((View)this.mLockPatternView);
            final Intent intent = this.getActivity().getIntent();
            if (intent != null) {
                this.mHeaderText = intent.getCharSequenceExtra("com.android.settings.ConfirmCredentials.header");
                this.mDetailsText = intent.getCharSequenceExtra("com.android.settings.ConfirmCredentials.details");
            }
            this.mLockPatternView.setTactileFeedbackEnabled(this.mLockPatternUtils.isTactileFeedbackEnabled());
            this.mLockPatternView.setInStealthMode(this.mLockPatternUtils.isVisiblePatternEnabled(this.mEffectiveUserId) ^ true);
            this.mLockPatternView.setOnPatternListener(this.mConfirmExistingLockPatternListener);
            this.updateStage(Stage.NeedToUnlock);
            if (bundle == null && !this.mFrp && !this.mLockPatternUtils.isLockPatternEnabled(this.mEffectiveUserId)) {
                this.getActivity().setResult(-1);
                this.getActivity().finish();
            }
            this.mAppearAnimationUtils = new AppearAnimationUtils(this.getContext(), 220L, 2.0f, 1.3f, AnimationUtils.loadInterpolator(this.getContext(), 17563662));
            this.mDisappearAnimationUtils = new DisappearAnimationUtils(this.getContext(), 125L, 4.0f, 0.3f, AnimationUtils.loadInterpolator(this.getContext(), 17563663), new AppearAnimationUtils.RowTranslationScaler() {
                @Override
                public float getRowTranslationScale(final int n, final int n2) {
                    return (n2 - n) / n2;
                }
            });
            this.setAccessibilityTitle(this.mHeaderTextView.getText());
            this.mCredentialCheckResultTracker = (CredentialCheckResultTracker)this.getFragmentManager().findFragmentByTag("check_lock_result");
            if (this.mCredentialCheckResultTracker == null) {
                this.mCredentialCheckResultTracker = new CredentialCheckResultTracker();
                this.getFragmentManager().beginTransaction().add((Fragment)this.mCredentialCheckResultTracker, "check_lock_result").commit();
            }
            return inflate;
        }
        
        @Override
        public void onCredentialChecked(final boolean b, final Intent intent, final int n, final int n2, final boolean b2) {
            this.onPatternChecked(b, intent, n, n2, b2);
        }
        
        @Override
        public void onFingerprintIconVisibilityChanged(final boolean b) {
            if (this.mLeftSpacerLandscape != null && this.mRightSpacerLandscape != null) {
                final View mLeftSpacerLandscape = this.mLeftSpacerLandscape;
                final boolean b2 = false;
                int visibility;
                if (b) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                mLeftSpacerLandscape.setVisibility(visibility);
                final View mRightSpacerLandscape = this.mRightSpacerLandscape;
                int visibility2 = b2 ? 1 : 0;
                if (b) {
                    visibility2 = 8;
                }
                mRightSpacerLandscape.setVisibility(visibility2);
            }
        }
        
        @Override
        public void onPause() {
            super.onPause();
            if (this.mCountdownTimer != null) {
                this.mCountdownTimer.cancel();
            }
            this.mCredentialCheckResultTracker.setListener(null);
        }
        
        @Override
        public void onResume() {
            super.onResume();
            final long lockoutAttemptDeadline = this.mLockPatternUtils.getLockoutAttemptDeadline(this.mEffectiveUserId);
            if (lockoutAttemptDeadline != 0L) {
                this.mCredentialCheckResultTracker.clearResult();
                this.handleAttemptLockout(lockoutAttemptDeadline);
            }
            else if (!this.mLockPatternView.isEnabled()) {
                this.updateStage(Stage.NeedToUnlock);
            }
            this.mCredentialCheckResultTracker.setListener((CredentialCheckResultTracker.Listener)this);
        }
        
        @Override
        public void onSaveInstanceState(final Bundle bundle) {
        }
        
        @Override
        protected void onShowError() {
        }
        
        @Override
        public void prepareEnterAnimation() {
            super.prepareEnterAnimation();
            this.mHeaderTextView.setAlpha(0.0f);
            this.mCancelButton.setAlpha(0.0f);
            this.mLockPatternView.setAlpha(0.0f);
            this.mDetailsTextView.setAlpha(0.0f);
            this.mFingerprintIcon.setAlpha(0.0f);
        }
        
        @Override
        public void startEnterAnimation() {
            super.startEnterAnimation();
            this.mLockPatternView.setAlpha(1.0f);
            this.mAppearAnimationUtils.startAnimation2d(this.getActiveViews(), null, this);
        }
    }
    
    public static class InternalActivity extends ConfirmLockPattern
    {
    }
    
    private enum Stage
    {
        LockedOut, 
        NeedToUnlock, 
        NeedToUnlockWrong;
    }
}
