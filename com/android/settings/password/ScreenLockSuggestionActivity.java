package com.android.settings.password;

import android.app.KeyguardManager;
import android.content.Context;

public class ScreenLockSuggestionActivity extends ChooseLockGeneric
{
    public static boolean isSuggestionComplete(final Context context) {
        return ((KeyguardManager)context.getSystemService((Class)KeyguardManager.class)).isKeyguardSecure();
    }
}
