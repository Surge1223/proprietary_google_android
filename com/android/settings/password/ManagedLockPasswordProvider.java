package com.android.settings.password;

import android.content.Intent;
import android.content.Context;

public class ManagedLockPasswordProvider
{
    public static ManagedLockPasswordProvider get(final Context context, final int n) {
        return new ManagedLockPasswordProvider();
    }
    
    Intent createIntent(final boolean b, final String s) {
        return null;
    }
    
    CharSequence getPickerOptionTitle(final boolean b) {
        return "";
    }
    
    boolean isManagedPasswordChoosable() {
        return false;
    }
    
    boolean isSettingManagedPasswordSupported() {
        return false;
    }
}
