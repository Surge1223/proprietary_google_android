package com.android.settings;

import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import android.support.v7.preference.PreferenceCategory;

public class AccessiblePreferenceCategory extends PreferenceCategory
{
    private String mContentDescription;
    
    public AccessiblePreferenceCategory(final Context context) {
        super(context);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        preferenceViewHolder.itemView.setContentDescription((CharSequence)this.mContentDescription);
    }
    
    public void setContentDescription(final String mContentDescription) {
        this.mContentDescription = mContentDescription;
    }
}
