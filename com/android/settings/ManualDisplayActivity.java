package com.android.settings;

import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.content.Intent;
import java.io.File;
import android.util.Log;
import android.text.TextUtils;
import android.os.SystemProperties;
import android.os.Bundle;
import android.content.Context;
import android.widget.Toast;
import android.app.Activity;

public class ManualDisplayActivity extends Activity
{
    private void showErrorAndFinish() {
        Toast.makeText((Context)this, 2131889060, 1).show();
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (!this.getResources().getBoolean(2131034150)) {
            this.finish();
        }
        final String value = SystemProperties.get("ro.config.manual_path", "/system/etc/MANUAL.html.gz");
        if (TextUtils.isEmpty((CharSequence)value)) {
            Log.e("SettingsManualActivity", "The system property for the manual is empty");
            this.showErrorAndFinish();
            return;
        }
        final File file = new File(value);
        if (file.exists() && file.length() != 0L) {
            final Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), "text/html");
            intent.putExtra("android.intent.extra.TITLE", this.getString(2131889059));
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setPackage("com.android.htmlviewer");
            try {
                this.startActivity(intent);
                this.finish();
            }
            catch (ActivityNotFoundException ex) {
                Log.e("SettingsManualActivity", "Failed to find viewer", (Throwable)ex);
                this.showErrorAndFinish();
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Manual file ");
        sb.append(value);
        sb.append(" does not exist");
        Log.e("SettingsManualActivity", sb.toString());
        this.showErrorAndFinish();
    }
}
