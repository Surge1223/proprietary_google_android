package com.android.settings.widget;

import android.support.v4.view.ViewPager;
import android.view.View$MeasureSpec;
import android.support.v4.view.PagerAdapter;
import android.widget.TextView;
import android.view.ViewGroup;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

public final class SlidingTabLayout extends FrameLayout implements View.OnClickListener
{
    private final View mIndicatorView;
    private final LayoutInflater mLayoutInflater;
    private int mSelectedPosition;
    private float mSelectionOffset;
    private final LinearLayout mTitleView;
    private RtlCompatibleViewPager mViewPager;
    
    public SlidingTabLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mLayoutInflater = LayoutInflater.from(context);
        (this.mTitleView = new LinearLayout(context)).setGravity(1);
        this.mIndicatorView = this.mLayoutInflater.inflate(2131558755, (ViewGroup)this, false);
        this.addView((View)this.mTitleView, -1, -2);
        this.addView(this.mIndicatorView, this.mIndicatorView.getLayoutParams());
    }
    
    private int getIndicatorLeft() {
        int left;
        final int n = left = this.mTitleView.getChildAt(this.mSelectedPosition).getLeft();
        if (this.mSelectionOffset > 0.0f) {
            left = n;
            if (this.mSelectedPosition < this.getChildCount() - 1) {
                left = (int)(this.mSelectionOffset * this.mTitleView.getChildAt(this.mSelectedPosition + 1).getLeft() + (1.0f - this.mSelectionOffset) * n);
            }
        }
        return left;
    }
    
    private boolean isRtlMode() {
        final int layoutDirection = this.getLayoutDirection();
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    private void onViewPagerPageChanged(int indicatorLeft, final float mSelectionOffset) {
        this.mSelectedPosition = indicatorLeft;
        this.mSelectionOffset = mSelectionOffset;
        if (this.isRtlMode()) {
            indicatorLeft = -this.getIndicatorLeft();
        }
        else {
            indicatorLeft = this.getIndicatorLeft();
        }
        this.mIndicatorView.setTranslationX((float)indicatorLeft);
    }
    
    private void populateTabStrip() {
        final PagerAdapter adapter = this.mViewPager.getAdapter();
        for (int i = 0; i < adapter.getCount(); ++i) {
            final TextView textView = (TextView)this.mLayoutInflater.inflate(2131558756, (ViewGroup)this.mTitleView, false);
            textView.setText(adapter.getPageTitle(i));
            textView.setOnClickListener((View.OnClickListener)this);
            this.mTitleView.addView((View)textView);
            textView.setSelected(i == this.mViewPager.getCurrentItem());
        }
    }
    
    public void onClick(final View view) {
        for (int childCount = this.mTitleView.getChildCount(), i = 0; i < childCount; ++i) {
            if (view == this.mTitleView.getChildAt(i)) {
                this.mViewPager.setCurrentItem(i);
                return;
            }
        }
    }
    
    protected void onLayout(final boolean b, int measuredHeight, int measuredWidth, int measuredHeight2, int paddingRight) {
        if (this.mTitleView.getChildCount() > 0) {
            measuredHeight2 = this.getMeasuredHeight();
            measuredHeight = this.mIndicatorView.getMeasuredHeight();
            final int measuredWidth2 = this.mIndicatorView.getMeasuredWidth();
            measuredWidth = this.getMeasuredWidth();
            final int paddingLeft = this.getPaddingLeft();
            paddingRight = this.getPaddingRight();
            this.mTitleView.layout(paddingLeft, 0, this.mTitleView.getMeasuredWidth() + paddingRight, this.mTitleView.getMeasuredHeight());
            if (this.isRtlMode()) {
                this.mIndicatorView.layout(measuredWidth - measuredWidth2, measuredHeight2 - measuredHeight, measuredWidth, measuredHeight2);
            }
            else {
                this.mIndicatorView.layout(0, measuredHeight2 - measuredHeight, measuredWidth2, measuredHeight2);
            }
        }
    }
    
    protected void onMeasure(int n, int measureSpec) {
        super.onMeasure(n, measureSpec);
        n = this.mTitleView.getChildCount();
        if (n > 0) {
            n = View$MeasureSpec.makeMeasureSpec(this.mTitleView.getMeasuredWidth() / n, 1073741824);
            measureSpec = View$MeasureSpec.makeMeasureSpec(this.mIndicatorView.getMeasuredHeight(), 1073741824);
            this.mIndicatorView.measure(n, measureSpec);
        }
    }
    
    public void setViewPager(final RtlCompatibleViewPager mViewPager) {
        this.mTitleView.removeAllViews();
        this.mViewPager = mViewPager;
        if (mViewPager != null) {
            mViewPager.addOnPageChangeListener((ViewPager.OnPageChangeListener)new InternalViewPagerListener());
            this.populateTabStrip();
        }
    }
    
    private final class InternalViewPagerListener implements OnPageChangeListener
    {
        private int mScrollState;
        
        @Override
        public void onPageScrollStateChanged(final int mScrollState) {
            this.mScrollState = mScrollState;
        }
        
        @Override
        public void onPageScrolled(final int n, final float n2, int childCount) {
            childCount = SlidingTabLayout.this.mTitleView.getChildCount();
            if (childCount != 0 && n >= 0 && n < childCount) {
                SlidingTabLayout.this.onViewPagerPageChanged(n, n2);
            }
        }
        
        @Override
        public void onPageSelected(int i) {
            final int rtlAwareIndex = SlidingTabLayout.this.mViewPager.getRtlAwareIndex(i);
            if (this.mScrollState == 0) {
                SlidingTabLayout.this.onViewPagerPageChanged(rtlAwareIndex, 0.0f);
            }
            int childCount;
            for (childCount = SlidingTabLayout.this.mTitleView.getChildCount(), i = 0; i < childCount; ++i) {
                SlidingTabLayout.this.mTitleView.getChildAt(i).setSelected(rtlAwareIndex == i);
            }
        }
    }
}
