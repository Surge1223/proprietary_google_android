package com.android.settings.widget;

import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.EditText;

public class ImeAwareEditText extends EditText
{
    private boolean mHasPendingShowSoftInputRequest;
    final Runnable mRunShowSoftInputIfNecessary;
    
    public ImeAwareEditText(final Context context) {
        super(context, (AttributeSet)null);
        this.mRunShowSoftInputIfNecessary = new _$$Lambda$ImeAwareEditText$jSRw3KSZxc80AfkP8GTCtV5_bRY(this);
    }
    
    public ImeAwareEditText(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRunShowSoftInputIfNecessary = new _$$Lambda$ImeAwareEditText$jSRw3KSZxc80AfkP8GTCtV5_bRY(this);
    }
    
    public ImeAwareEditText(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mRunShowSoftInputIfNecessary = new _$$Lambda$ImeAwareEditText$jSRw3KSZxc80AfkP8GTCtV5_bRY(this);
    }
    
    public ImeAwareEditText(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mRunShowSoftInputIfNecessary = new _$$Lambda$ImeAwareEditText$jSRw3KSZxc80AfkP8GTCtV5_bRY(this);
    }
    
    private void showSoftInputIfNecessary() {
        if (this.mHasPendingShowSoftInputRequest) {
            ((InputMethodManager)this.getContext().getSystemService((Class)InputMethodManager.class)).showSoftInput((View)this, 0);
            this.mHasPendingShowSoftInputRequest = false;
        }
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        if (this.mHasPendingShowSoftInputRequest) {
            this.removeCallbacks(this.mRunShowSoftInputIfNecessary);
            this.post(this.mRunShowSoftInputIfNecessary);
        }
        return onCreateInputConnection;
    }
    
    public void scheduleShowSoftInput() {
        final InputMethodManager inputMethodManager = (InputMethodManager)this.getContext().getSystemService((Class)InputMethodManager.class);
        if (inputMethodManager.isActive((View)this)) {
            this.mHasPendingShowSoftInputRequest = false;
            this.removeCallbacks(this.mRunShowSoftInputIfNecessary);
            inputMethodManager.showSoftInput((View)this, 0);
            return;
        }
        this.mHasPendingShowSoftInputRequest = true;
    }
}
