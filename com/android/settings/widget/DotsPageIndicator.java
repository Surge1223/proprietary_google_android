package com.android.settings.widget;

import android.database.DataSetObserver;
import android.view.View$MeasureSpec;
import android.graphics.Path$Direction;
import android.graphics.Path$Op;
import android.graphics.Canvas;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import java.util.Arrays;
import android.content.res.TypedArray;
import android.view.View$OnAttachStateChangeListener;
import android.view.animation.AnimationUtils;
import android.os.Build.VERSION;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.animation.ValueAnimator;
import android.animation.AnimatorSet;
import android.view.animation.Interpolator;
import android.graphics.Path;
import android.support.v4.view.ViewPager;
import android.view.View;

public class DotsPageIndicator extends View implements OnPageChangeListener
{
    public static final String TAG;
    private long animDuration;
    private long animHalfDuration;
    private boolean attachedState;
    private final Path combinedUnselectedPath;
    float controlX1;
    float controlX2;
    float controlY1;
    float controlY2;
    private int currentPage;
    private float dotBottomY;
    private float[] dotCenterX;
    private float dotCenterY;
    private int dotDiameter;
    private float dotRadius;
    private float[] dotRevealFractions;
    private float dotTopY;
    float endX1;
    float endX2;
    float endY1;
    float endY2;
    private int gap;
    private float halfDotRadius;
    private final Interpolator interpolator;
    private AnimatorSet joiningAnimationSet;
    private ValueAnimator[] joiningAnimations;
    private float[] joiningFractions;
    private ValueAnimator moveAnimation;
    private OnPageChangeListener pageChangeListener;
    private int pageCount;
    private final RectF rectF;
    private PendingRetreatAnimator retreatAnimation;
    private float retreatingJoinX1;
    private float retreatingJoinX2;
    private PendingRevealAnimator[] revealAnimations;
    private int selectedColour;
    private boolean selectedDotInPosition;
    private float selectedDotX;
    private final Paint selectedPaint;
    private int unselectedColour;
    private final Path unselectedDotLeftPath;
    private final Path unselectedDotPath;
    private final Path unselectedDotRightPath;
    private final Paint unselectedPaint;
    private ViewPager viewPager;
    
    static {
        TAG = DotsPageIndicator.class.getSimpleName();
    }
    
    public DotsPageIndicator(final Context context) {
        this(context, null, 0);
    }
    
    public DotsPageIndicator(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public DotsPageIndicator(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final int n2 = (int)context.getResources().getDisplayMetrics().scaledDensity;
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.DotsPageIndicator, n, 0);
        this.dotDiameter = obtainStyledAttributes.getDimensionPixelSize(2, 8 * n2);
        this.dotRadius = this.dotDiameter / 2;
        this.halfDotRadius = this.dotRadius / 2.0f;
        this.gap = obtainStyledAttributes.getDimensionPixelSize(3, 12 * n2);
        this.animDuration = obtainStyledAttributes.getInteger(0, 400);
        this.animHalfDuration = this.animDuration / 2L;
        this.unselectedColour = obtainStyledAttributes.getColor(4, -2130706433);
        this.selectedColour = obtainStyledAttributes.getColor(1, -1);
        obtainStyledAttributes.recycle();
        (this.unselectedPaint = new Paint(1)).setColor(this.unselectedColour);
        (this.selectedPaint = new Paint(1)).setColor(this.selectedColour);
        if (Build.VERSION.SDK_INT >= 21) {
            this.interpolator = AnimationUtils.loadInterpolator(context, 17563661);
        }
        else {
            this.interpolator = AnimationUtils.loadInterpolator(context, 17432580);
        }
        this.combinedUnselectedPath = new Path();
        this.unselectedDotPath = new Path();
        this.unselectedDotLeftPath = new Path();
        this.unselectedDotRightPath = new Path();
        this.rectF = new RectF();
        this.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(final View view) {
                DotsPageIndicator.this.attachedState = true;
            }
            
            public void onViewDetachedFromWindow(final View view) {
                DotsPageIndicator.this.attachedState = false;
            }
        });
    }
    
    private void calculateDotPositions() {
        final int paddingLeft = this.getPaddingLeft();
        final int paddingTop = this.getPaddingTop();
        final float n = (this.getWidth() - this.getPaddingRight() - paddingLeft - this.getRequiredWidth()) / 2 + paddingLeft;
        final float dotRadius = this.dotRadius;
        this.dotCenterX = new float[this.pageCount];
        for (int i = 0; i < this.pageCount; ++i) {
            this.dotCenterX[i] = (this.dotDiameter + this.gap) * i + (n + dotRadius);
        }
        this.dotTopY = paddingTop;
        this.dotCenterY = paddingTop + this.dotRadius;
        this.dotBottomY = this.dotDiameter + paddingTop;
        this.setCurrentPageImmediate();
    }
    
    private void cancelJoiningAnimations() {
        if (this.joiningAnimationSet != null && this.joiningAnimationSet.isRunning()) {
            this.joiningAnimationSet.cancel();
        }
    }
    
    private void cancelMoveAnimation() {
        if (this.moveAnimation != null && this.moveAnimation.isRunning()) {
            this.moveAnimation.cancel();
        }
    }
    
    private void cancelRetreatAnimation() {
        if (this.retreatAnimation != null && this.retreatAnimation.isRunning()) {
            this.retreatAnimation.cancel();
        }
    }
    
    private void cancelRevealAnimations() {
        if (this.revealAnimations != null) {
            final PendingRevealAnimator[] revealAnimations = this.revealAnimations;
            for (int length = revealAnimations.length, i = 0; i < length; ++i) {
                revealAnimations[i].cancel();
            }
        }
    }
    
    private void cancelRunningAnimations() {
        this.cancelMoveAnimation();
        this.cancelJoiningAnimations();
        this.cancelRetreatAnimation();
        this.cancelRevealAnimations();
        this.resetState();
    }
    
    private void clearJoiningFractions() {
        Arrays.fill(this.joiningFractions, 0.0f);
        this.postInvalidateOnAnimation();
    }
    
    private ValueAnimator createJoiningAnimator(final int n, final long startDelay) {
        final ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f });
        ofFloat.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                DotsPageIndicator.this.setJoiningFraction(n, valueAnimator.getAnimatedFraction());
            }
        });
        ofFloat.setDuration(this.animHalfDuration);
        ofFloat.setStartDelay(startDelay);
        ofFloat.setInterpolator((TimeInterpolator)this.interpolator);
        return ofFloat;
    }
    
    private ValueAnimator createMoveSelectedAnimator(final float n, final int n2, final int n3, final int n4) {
        final ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[] { this.selectedDotX, n });
        StartPredicate startPredicate;
        if (n3 > n2) {
            startPredicate = new RightwardStartPredicate(n - (n - this.selectedDotX) * 0.25f);
        }
        else {
            startPredicate = new LeftwardStartPredicate((this.selectedDotX - n) * 0.25f + n);
        }
        this.retreatAnimation = new PendingRetreatAnimator(n2, n3, n4, startPredicate);
        ofFloat.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                DotsPageIndicator.this.selectedDotX = (float)valueAnimator.getAnimatedValue();
                ((PendingStartAnimator)DotsPageIndicator.this.retreatAnimation).startIfNecessary(DotsPageIndicator.this.selectedDotX);
                DotsPageIndicator.this.postInvalidateOnAnimation();
            }
        });
        ofFloat.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            public void onAnimationEnd(final Animator animator) {
                DotsPageIndicator.this.selectedDotInPosition = true;
            }
            
            public void onAnimationStart(final Animator animator) {
                DotsPageIndicator.this.selectedDotInPosition = false;
            }
        });
        long startDelay;
        if (this.selectedDotInPosition) {
            startDelay = this.animDuration / 4L;
        }
        else {
            startDelay = 0L;
        }
        ofFloat.setStartDelay(startDelay);
        ofFloat.setDuration(this.animDuration * 3L / 4L);
        ofFloat.setInterpolator((TimeInterpolator)this.interpolator);
        return ofFloat;
    }
    
    private void drawSelected(final Canvas canvas) {
        canvas.drawCircle(this.selectedDotX, this.dotCenterY, this.dotRadius, this.selectedPaint);
    }
    
    private void drawUnselected(final Canvas canvas) {
        this.combinedUnselectedPath.rewind();
        for (int i = 0; i < this.pageCount; ++i) {
            int n;
            if (i == this.pageCount - 1) {
                n = i;
            }
            else {
                n = i + 1;
            }
            if (Build.VERSION.SDK_INT >= 21) {
                final float n2 = this.dotCenterX[i];
                final float n3 = this.dotCenterX[n];
                float n4;
                if (i == this.pageCount - 1) {
                    n4 = -1.0f;
                }
                else {
                    n4 = this.joiningFractions[i];
                }
                this.combinedUnselectedPath.op(this.getUnselectedPath(i, n2, n3, n4, this.dotRevealFractions[i]), Path$Op.UNION);
            }
            else {
                canvas.drawCircle(this.dotCenterX[i], this.dotCenterY, this.dotRadius, this.unselectedPaint);
            }
        }
        if (this.retreatingJoinX1 != -1.0f && Build.VERSION.SDK_INT >= 21) {
            this.combinedUnselectedPath.op(this.getRetreatingJoinPath(), Path$Op.UNION);
        }
        canvas.drawPath(this.combinedUnselectedPath, this.unselectedPaint);
    }
    
    private int getDesiredHeight() {
        return this.getPaddingTop() + this.dotDiameter + this.getPaddingBottom();
    }
    
    private int getDesiredWidth() {
        return this.getPaddingLeft() + this.getRequiredWidth() + this.getPaddingRight();
    }
    
    private int getRequiredWidth() {
        return this.pageCount * this.dotDiameter + (this.pageCount - 1) * this.gap;
    }
    
    private Path getRetreatingJoinPath() {
        this.unselectedDotPath.rewind();
        this.rectF.set(this.retreatingJoinX1, this.dotTopY, this.retreatingJoinX2, this.dotBottomY);
        this.unselectedDotPath.addRoundRect(this.rectF, this.dotRadius, this.dotRadius, Path$Direction.CW);
        return this.unselectedDotPath;
    }
    
    private Path getUnselectedPath(final int n, final float n2, final float n3, final float n4, final float n5) {
        this.unselectedDotPath.rewind();
        if ((n4 == 0.0f || n4 == -1.0f) && n5 == 0.0f && (n != this.currentPage || !this.selectedDotInPosition)) {
            this.unselectedDotPath.addCircle(this.dotCenterX[n], this.dotCenterY, this.dotRadius, Path$Direction.CW);
        }
        if (n4 > 0.0f && n4 < 0.5f && this.retreatingJoinX1 == -1.0f) {
            this.unselectedDotLeftPath.rewind();
            this.unselectedDotLeftPath.moveTo(n2, this.dotBottomY);
            this.rectF.set(n2 - this.dotRadius, this.dotTopY, this.dotRadius + n2, this.dotBottomY);
            this.unselectedDotLeftPath.arcTo(this.rectF, 90.0f, 180.0f, true);
            this.endX1 = this.dotRadius + n2 + this.gap * n4;
            this.endY1 = this.dotCenterY;
            this.controlX1 = this.halfDotRadius + n2;
            this.controlY1 = this.dotTopY;
            this.controlX2 = this.endX1;
            this.controlY2 = this.endY1 - this.halfDotRadius;
            this.unselectedDotLeftPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX1, this.endY1);
            this.endX2 = n2;
            this.endY2 = this.dotBottomY;
            this.controlX1 = this.endX1;
            this.controlY1 = this.endY1 + this.halfDotRadius;
            this.controlX2 = this.halfDotRadius + n2;
            this.controlY2 = this.dotBottomY;
            this.unselectedDotLeftPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX2, this.endY2);
            if (Build.VERSION.SDK_INT >= 21) {
                this.unselectedDotPath.op(this.unselectedDotLeftPath, Path$Op.UNION);
            }
            this.unselectedDotRightPath.rewind();
            this.unselectedDotRightPath.moveTo(n3, this.dotBottomY);
            this.rectF.set(n3 - this.dotRadius, this.dotTopY, this.dotRadius + n3, this.dotBottomY);
            this.unselectedDotRightPath.arcTo(this.rectF, 90.0f, -180.0f, true);
            this.endX1 = n3 - this.dotRadius - this.gap * n4;
            this.endY1 = this.dotCenterY;
            this.controlX1 = n3 - this.halfDotRadius;
            this.controlY1 = this.dotTopY;
            this.controlX2 = this.endX1;
            this.controlY2 = this.endY1 - this.halfDotRadius;
            this.unselectedDotRightPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX1, this.endY1);
            this.endX2 = n3;
            this.endY2 = this.dotBottomY;
            this.controlX1 = this.endX1;
            this.controlY1 = this.endY1 + this.halfDotRadius;
            this.controlX2 = this.endX2 - this.halfDotRadius;
            this.controlY2 = this.dotBottomY;
            this.unselectedDotRightPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX2, this.endY2);
            if (Build.VERSION.SDK_INT >= 21) {
                this.unselectedDotPath.op(this.unselectedDotRightPath, Path$Op.UNION);
            }
        }
        if (n4 > 0.5f && n4 < 1.0f && this.retreatingJoinX1 == -1.0f) {
            this.unselectedDotPath.moveTo(n2, this.dotBottomY);
            this.rectF.set(n2 - this.dotRadius, this.dotTopY, this.dotRadius + n2, this.dotBottomY);
            this.unselectedDotPath.arcTo(this.rectF, 90.0f, 180.0f, true);
            this.endX1 = this.dotRadius + n2 + this.gap / 2;
            this.endY1 = this.dotCenterY - this.dotRadius * n4;
            this.controlX1 = this.endX1 - this.dotRadius * n4;
            this.controlY1 = this.dotTopY;
            this.controlX2 = this.endX1 - (1.0f - n4) * this.dotRadius;
            this.controlY2 = this.endY1;
            this.unselectedDotPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX1, this.endY1);
            this.endX2 = n3;
            this.endY2 = this.dotTopY;
            this.controlX1 = this.endX1 + (1.0f - n4) * this.dotRadius;
            this.controlY1 = this.endY1;
            this.controlX2 = this.endX1 + this.dotRadius * n4;
            this.controlY2 = this.dotTopY;
            this.unselectedDotPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX2, this.endY2);
            this.rectF.set(n3 - this.dotRadius, this.dotTopY, this.dotRadius + n3, this.dotBottomY);
            this.unselectedDotPath.arcTo(this.rectF, 270.0f, 180.0f, true);
            this.endY1 = this.dotCenterY + this.dotRadius * n4;
            this.controlX1 = this.endX1 + this.dotRadius * n4;
            this.controlY1 = this.dotBottomY;
            this.controlX2 = this.endX1 + (1.0f - n4) * this.dotRadius;
            this.controlY2 = this.endY1;
            this.unselectedDotPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX1, this.endY1);
            this.endX2 = n2;
            this.endY2 = this.dotBottomY;
            this.controlX1 = this.endX1 - (1.0f - n4) * this.dotRadius;
            this.controlY1 = this.endY1;
            this.controlX2 = this.endX1 - this.dotRadius * n4;
            this.controlY2 = this.endY2;
            this.unselectedDotPath.cubicTo(this.controlX1, this.controlY1, this.controlX2, this.controlY2, this.endX2, this.endY2);
        }
        if (n4 == 1.0f && this.retreatingJoinX1 == -1.0f) {
            this.rectF.set(n2 - this.dotRadius, this.dotTopY, this.dotRadius + n3, this.dotBottomY);
            this.unselectedDotPath.addRoundRect(this.rectF, this.dotRadius, this.dotRadius, Path$Direction.CW);
        }
        if (n5 > 1.0E-5f) {
            this.unselectedDotPath.addCircle(n2, this.dotCenterY, this.dotRadius * n5, Path$Direction.CW);
        }
        return this.unselectedDotPath;
    }
    
    private void resetState() {
        if (this.pageCount > 0) {
            Arrays.fill(this.joiningFractions = new float[this.pageCount - 1], 0.0f);
            Arrays.fill(this.dotRevealFractions = new float[this.pageCount], 0.0f);
            this.retreatingJoinX1 = -1.0f;
            this.retreatingJoinX2 = -1.0f;
            this.selectedDotInPosition = true;
        }
    }
    
    private void setCurrentPageImmediate() {
        if (this.viewPager != null) {
            this.currentPage = this.viewPager.getCurrentItem();
        }
        else {
            this.currentPage = 0;
        }
        if (this.pageCount > 0) {
            this.selectedDotX = this.dotCenterX[this.currentPage];
        }
    }
    
    private void setDotRevealFraction(final int n, final float n2) {
        this.dotRevealFractions[n] = n2;
        this.postInvalidateOnAnimation();
    }
    
    private void setJoiningFraction(final int n, final float n2) {
        this.joiningFractions[n] = n2;
        this.postInvalidateOnAnimation();
    }
    
    private void setPageCount(final int pageCount) {
        this.pageCount = pageCount;
        this.calculateDotPositions();
        this.resetState();
    }
    
    private void setSelectedPage(final int currentPage) {
        if (currentPage != this.currentPage && this.pageCount != 0) {
            final int currentPage2 = this.currentPage;
            this.currentPage = currentPage;
            if (Build.VERSION.SDK_INT >= 16) {
                this.cancelRunningAnimations();
                final int abs = Math.abs(currentPage - currentPage2);
                this.moveAnimation = this.createMoveSelectedAnimator(this.dotCenterX[currentPage], currentPage2, currentPage, abs);
                this.joiningAnimations = new ValueAnimator[abs];
                for (int i = 0; i < abs; ++i) {
                    final ValueAnimator[] joiningAnimations = this.joiningAnimations;
                    int n;
                    if (currentPage > currentPage2) {
                        n = currentPage2 + i;
                    }
                    else {
                        n = currentPage2 - 1 - i;
                    }
                    joiningAnimations[i] = this.createJoiningAnimator(n, i * (this.animDuration / 8L));
                }
                this.moveAnimation.start();
                this.startJoiningAnimations();
            }
            else {
                this.setCurrentPageImmediate();
                this.invalidate();
            }
        }
    }
    
    private void startJoiningAnimations() {
        (this.joiningAnimationSet = new AnimatorSet()).playTogether((Animator[])this.joiningAnimations);
        this.joiningAnimationSet.start();
    }
    
    public void clearAnimation() {
        super.clearAnimation();
        if (Build.VERSION.SDK_INT >= 16) {
            this.cancelRunningAnimations();
        }
    }
    
    int getCurrentPage() {
        return this.currentPage;
    }
    
    float getDotCenterY() {
        return this.dotCenterY;
    }
    
    int getSelectedColour() {
        return this.selectedColour;
    }
    
    float getSelectedDotX() {
        return this.selectedDotX;
    }
    
    int getUnselectedColour() {
        return this.unselectedColour;
    }
    
    protected void onDraw(final Canvas canvas) {
        if (this.viewPager != null && this.pageCount != 0) {
            this.drawUnselected(canvas);
            this.drawSelected(canvas);
        }
    }
    
    protected void onMeasure(int n, int n2) {
        final int desiredHeight = this.getDesiredHeight();
        final int mode = View$MeasureSpec.getMode(n2);
        if (mode != Integer.MIN_VALUE) {
            if (mode != 1073741824) {
                n2 = desiredHeight;
            }
            else {
                n2 = View$MeasureSpec.getSize(n2);
            }
        }
        else {
            n2 = Math.min(desiredHeight, View$MeasureSpec.getSize(n2));
        }
        final int desiredWidth = this.getDesiredWidth();
        final int mode2 = View$MeasureSpec.getMode(n);
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 != 1073741824) {
                n = desiredWidth;
            }
            else {
                n = View$MeasureSpec.getSize(n);
            }
        }
        else {
            n = Math.min(desiredWidth, View$MeasureSpec.getSize(n));
        }
        this.setMeasuredDimension(n, n2);
        this.calculateDotPositions();
    }
    
    public void onPageScrollStateChanged(final int n) {
        if (this.pageChangeListener != null) {
            this.pageChangeListener.onPageScrollStateChanged(n);
        }
    }
    
    public void onPageScrolled(final int n, final float n2, final int n3) {
        if (this.pageChangeListener != null) {
            this.pageChangeListener.onPageScrolled(n, n2, n3);
        }
    }
    
    public void onPageSelected(final int selectedPage) {
        if (this.attachedState) {
            this.setSelectedPage(selectedPage);
        }
        else {
            this.setCurrentPageImmediate();
        }
        if (this.pageChangeListener != null) {
            this.pageChangeListener.onPageSelected(selectedPage);
        }
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        this.setMeasuredDimension(n, n2);
        this.calculateDotPositions();
    }
    
    public void setOnPageChangeListener(final OnPageChangeListener pageChangeListener) {
        this.pageChangeListener = pageChangeListener;
    }
    
    public void setViewPager(final ViewPager viewPager) {
        (this.viewPager = viewPager).setOnPageChangeListener((ViewPager.OnPageChangeListener)this);
        this.setPageCount(viewPager.getAdapter().getCount());
        viewPager.getAdapter().registerDataSetObserver(new DataSetObserver() {
            public void onChanged() {
                DotsPageIndicator.this.setPageCount(DotsPageIndicator.this.viewPager.getAdapter().getCount());
            }
        });
        this.setCurrentPageImmediate();
    }
    
    public class LeftwardStartPredicate extends StartPredicate
    {
        public LeftwardStartPredicate(final float n) {
            super(n);
        }
        
        @Override
        boolean shouldStart(final float n) {
            return n < this.thresholdValue;
        }
    }
    
    public class PendingRetreatAnimator extends PendingStartAnimator
    {
        public PendingRetreatAnimator(final int n, int i, final int n2, final StartPredicate startPredicate) {
            super(startPredicate);
            this.setDuration(DotsPageIndicator.this.animHalfDuration);
            this.setInterpolator((TimeInterpolator)DotsPageIndicator.this.interpolator);
            float n3;
            if (i > n) {
                n3 = Math.min(DotsPageIndicator.this.dotCenterX[n], DotsPageIndicator.this.selectedDotX) - DotsPageIndicator.this.dotRadius;
            }
            else {
                n3 = DotsPageIndicator.this.dotCenterX[i] - DotsPageIndicator.this.dotRadius;
            }
            float n4;
            if (i > n) {
                n4 = DotsPageIndicator.this.dotCenterX[i] - DotsPageIndicator.this.dotRadius;
            }
            else {
                n4 = DotsPageIndicator.this.dotCenterX[i] - DotsPageIndicator.this.dotRadius;
            }
            float n5;
            if (i > n) {
                n5 = DotsPageIndicator.this.dotCenterX[i] + DotsPageIndicator.this.dotRadius;
            }
            else {
                n5 = Math.max(DotsPageIndicator.this.dotCenterX[n], DotsPageIndicator.this.selectedDotX) + DotsPageIndicator.this.dotRadius;
            }
            float n6;
            if (i > n) {
                n6 = DotsPageIndicator.this.dotCenterX[i] + DotsPageIndicator.this.dotRadius;
            }
            else {
                n6 = DotsPageIndicator.this.dotCenterX[i] + DotsPageIndicator.this.dotRadius;
            }
            DotsPageIndicator.this.revealAnimations = new PendingRevealAnimator[n2];
            final int[] array = new int[n2];
            final int n7 = 0;
            i = 0;
            if (n3 != n4) {
                this.setFloatValues(new float[] { n3, n4 });
                while (i < n2) {
                    DotsPageIndicator.this.revealAnimations[i] = new PendingRevealAnimator(n + i, new RightwardStartPredicate(DotsPageIndicator.this.dotCenterX[n + i]));
                    array[i] = n + i;
                    ++i;
                }
                this.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        DotsPageIndicator.this.retreatingJoinX1 = (float)valueAnimator.getAnimatedValue();
                        DotsPageIndicator.this.postInvalidateOnAnimation();
                        final PendingRevealAnimator[] access$1100 = DotsPageIndicator.this.revealAnimations;
                        for (int length = access$1100.length, i = 0; i < length; ++i) {
                            ((PendingStartAnimator)access$1100[i]).startIfNecessary(DotsPageIndicator.this.retreatingJoinX1);
                        }
                    }
                });
            }
            else {
                this.setFloatValues(new float[] { n5, n6 });
                for (i = n7; i < n2; ++i) {
                    DotsPageIndicator.this.revealAnimations[i] = new PendingRevealAnimator(n - i, new LeftwardStartPredicate(DotsPageIndicator.this.dotCenterX[n - i]));
                    array[i] = n - i;
                }
                this.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        DotsPageIndicator.this.retreatingJoinX2 = (float)valueAnimator.getAnimatedValue();
                        DotsPageIndicator.this.postInvalidateOnAnimation();
                        final PendingRevealAnimator[] access$1100 = DotsPageIndicator.this.revealAnimations;
                        for (int length = access$1100.length, i = 0; i < length; ++i) {
                            ((PendingStartAnimator)access$1100[i]).startIfNecessary(DotsPageIndicator.this.retreatingJoinX2);
                        }
                    }
                });
            }
            this.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
                public void onAnimationEnd(final Animator animator) {
                    DotsPageIndicator.this.retreatingJoinX1 = -1.0f;
                    DotsPageIndicator.this.retreatingJoinX2 = -1.0f;
                    DotsPageIndicator.this.postInvalidateOnAnimation();
                }
                
                public void onAnimationStart(final Animator animator) {
                    DotsPageIndicator.this.cancelJoiningAnimations();
                    DotsPageIndicator.this.clearJoiningFractions();
                    final int[] val$dotsToHide = array;
                    for (int length = val$dotsToHide.length, i = 0; i < length; ++i) {
                        DotsPageIndicator.this.setDotRevealFraction(val$dotsToHide[i], 1.0E-5f);
                    }
                    DotsPageIndicator.this.retreatingJoinX1 = n3;
                    DotsPageIndicator.this.retreatingJoinX2 = n5;
                    DotsPageIndicator.this.postInvalidateOnAnimation();
                }
            });
        }
    }
    
    public class PendingRevealAnimator extends PendingStartAnimator
    {
        private final int dot;
        
        public PendingRevealAnimator(final int dot, final StartPredicate startPredicate) {
            super(startPredicate);
            this.dot = dot;
            this.setFloatValues(new float[] { 1.0E-5f, 1.0f });
            this.setDuration(DotsPageIndicator.this.animHalfDuration);
            this.setInterpolator((TimeInterpolator)DotsPageIndicator.this.interpolator);
            this.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
                public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                    DotsPageIndicator.this.setDotRevealFraction(PendingRevealAnimator.this.dot, (float)valueAnimator.getAnimatedValue());
                }
            });
            this.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
                public void onAnimationEnd(final Animator animator) {
                    DotsPageIndicator.this.setDotRevealFraction(PendingRevealAnimator.this.dot, 0.0f);
                    DotsPageIndicator.this.postInvalidateOnAnimation();
                }
            });
        }
    }
    
    public abstract class PendingStartAnimator extends ValueAnimator
    {
        protected boolean hasStarted;
        protected StartPredicate predicate;
        
        public PendingStartAnimator(final StartPredicate predicate) {
            this.predicate = predicate;
            this.hasStarted = false;
        }
        
        public void startIfNecessary(final float n) {
            if (!this.hasStarted && this.predicate.shouldStart(n)) {
                this.start();
                this.hasStarted = true;
            }
        }
    }
    
    public class RightwardStartPredicate extends StartPredicate
    {
        public RightwardStartPredicate(final float n) {
            super(n);
        }
        
        @Override
        boolean shouldStart(final float n) {
            return n > this.thresholdValue;
        }
    }
    
    public abstract class StartPredicate
    {
        protected float thresholdValue;
        
        public StartPredicate(final float thresholdValue) {
            this.thresholdValue = thresholdValue;
        }
        
        abstract boolean shouldStart(final float p0);
    }
}
