package com.android.settings.widget;

import android.view.View;
import android.widget.ProgressBar;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class AppPreference extends Preference
{
    private int mProgress;
    private boolean mProgressVisible;
    
    public AppPreference(final Context context) {
        super(context);
        this.setLayoutResource(2131558644);
    }
    
    public AppPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setLayoutResource(2131558644);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362671);
        int visibility;
        if (TextUtils.isEmpty(this.getSummary())) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        viewById.setVisibility(visibility);
        final ProgressBar progressBar = (ProgressBar)preferenceViewHolder.findViewById(16908301);
        if (this.mProgressVisible) {
            progressBar.setProgress(this.mProgress);
            progressBar.setVisibility(0);
        }
        else {
            progressBar.setVisibility(8);
        }
    }
    
    public void setProgress(final int mProgress) {
        this.mProgress = mProgress;
        this.mProgressVisible = true;
        this.notifyChanged();
    }
}
