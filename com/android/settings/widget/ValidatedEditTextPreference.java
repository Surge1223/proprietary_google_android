package com.android.settings.widget;

import android.app.AlertDialog;
import android.text.Editable;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.text.TextWatcher;
import android.text.TextUtils;
import android.widget.EditText;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.CustomEditTextPreference;

public class ValidatedEditTextPreference extends CustomEditTextPreference
{
    private boolean mIsPassword;
    private boolean mIsSummaryPassword;
    private final EditTextWatcher mTextWatcher;
    private Validator mValidator;
    
    public ValidatedEditTextPreference(final Context context) {
        super(context);
        this.mTextWatcher = new EditTextWatcher();
    }
    
    public ValidatedEditTextPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mTextWatcher = new EditTextWatcher();
    }
    
    public ValidatedEditTextPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mTextWatcher = new EditTextWatcher();
    }
    
    public ValidatedEditTextPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mTextWatcher = new EditTextWatcher();
    }
    
    public boolean isPassword() {
        return this.mIsPassword;
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        final EditText editText = (EditText)view.findViewById(16908291);
        if (editText != null && !TextUtils.isEmpty((CharSequence)editText.getText())) {
            editText.setSelection(editText.getText().length());
        }
        if (this.mValidator != null && editText != null) {
            editText.removeTextChangedListener((TextWatcher)this.mTextWatcher);
            if (this.mIsPassword) {
                editText.setInputType(145);
                editText.setMaxLines(1);
            }
            editText.addTextChangedListener((TextWatcher)this.mTextWatcher);
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(16908304);
        if (textView == null) {
            return;
        }
        if (this.mIsSummaryPassword) {
            textView.setInputType(129);
        }
        else {
            textView.setInputType(1);
        }
    }
    
    public void setIsPassword(final boolean mIsPassword) {
        this.mIsPassword = mIsPassword;
    }
    
    public void setIsSummaryPassword(final boolean mIsSummaryPassword) {
        this.mIsSummaryPassword = mIsSummaryPassword;
    }
    
    public void setValidator(final Validator mValidator) {
        this.mValidator = mValidator;
    }
    
    private class EditTextWatcher implements TextWatcher
    {
        public void afterTextChanged(final Editable editable) {
            final EditText editText = ValidatedEditTextPreference.this.getEditText();
            if (ValidatedEditTextPreference.this.mValidator != null && editText != null) {
                ((AlertDialog)ValidatedEditTextPreference.this.getDialog()).getButton(-1).setEnabled(ValidatedEditTextPreference.this.mValidator.isTextValid(editText.getText().toString()));
            }
        }
        
        public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        }
        
        public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        }
    }
    
    public interface Validator
    {
        boolean isTextValid(final String p0);
    }
}
