package com.android.settings.widget;

import android.text.SpannableStringBuilder;
import android.content.res.Resources;

public class InvertedChartAxis implements ChartAxis
{
    private float mSize;
    private final ChartAxis mWrapped;
    
    public InvertedChartAxis(final ChartAxis mWrapped) {
        this.mWrapped = mWrapped;
    }
    
    @Override
    public long buildLabel(final Resources resources, final SpannableStringBuilder spannableStringBuilder, final long n) {
        return this.mWrapped.buildLabel(resources, spannableStringBuilder, n);
    }
    
    @Override
    public float convertToPoint(final long n) {
        return this.mSize - this.mWrapped.convertToPoint(n);
    }
    
    @Override
    public long convertToValue(final float n) {
        return this.mWrapped.convertToValue(this.mSize - n);
    }
    
    @Override
    public float[] getTickPoints() {
        final float[] tickPoints = this.mWrapped.getTickPoints();
        for (int i = 0; i < tickPoints.length; ++i) {
            tickPoints[i] = this.mSize - tickPoints[i];
        }
        return tickPoints;
    }
    
    @Override
    public boolean setBounds(final long n, final long n2) {
        return this.mWrapped.setBounds(n, n2);
    }
    
    @Override
    public boolean setSize(final float n) {
        this.mSize = n;
        return this.mWrapped.setSize(n);
    }
    
    @Override
    public int shouldAdjustAxis(final long n) {
        return this.mWrapped.shouldAdjustAxis(n);
    }
}
