package com.android.settings.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;
import android.content.res.Resources;
import android.os.Parcelable;
import android.widget.CompoundButton;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.TouchDelegate;
import android.graphics.Rect;
import android.content.res.TypedArray;
import com.android.settings.overlay.FeatureFactory;
import android.view.View.OnClickListener;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import java.util.List;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.RestrictedLockUtils;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Switch;

public final class _$$Lambda$SwitchBar$xcPsCGGwUScwZOtx6bxg2zuPXc8 implements OnSwitchChangeListener
{
    @Override
    public final void onSwitchChanged(final Switch switch1, final boolean b) {
        SwitchBar.lambda$new$0(this.f$0, switch1, b);
    }
}
