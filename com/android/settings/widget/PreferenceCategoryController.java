package com.android.settings.widget;

import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import android.content.Context;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settings.core.BasePreferenceController;

public class PreferenceCategoryController extends BasePreferenceController
{
    private final List<AbstractPreferenceController> mChildren;
    private final String mKey;
    
    public PreferenceCategoryController(final Context context, final String mKey) {
        super(context, mKey);
        this.mKey = mKey;
        this.mChildren = new ArrayList<AbstractPreferenceController>();
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mChildren != null && !this.mChildren.isEmpty()) {
            final Iterator<AbstractPreferenceController> iterator = this.mChildren.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().isAvailable()) {
                    return 0;
                }
            }
            return 1;
        }
        return 2;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mKey;
    }
    
    public PreferenceCategoryController setChildren(final List<AbstractPreferenceController> list) {
        this.mChildren.clear();
        if (list != null) {
            this.mChildren.addAll(list);
        }
        return this;
    }
}
