package com.android.settings.widget;

import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.SeekBar;

public class DefaultIndicatorSeekBar extends SeekBar
{
    private int mDefaultProgress;
    
    public DefaultIndicatorSeekBar(final Context context) {
        super(context);
        this.mDefaultProgress = -1;
    }
    
    public DefaultIndicatorSeekBar(final Context context, final AttributeSet set) {
        super(context, set);
        this.mDefaultProgress = -1;
    }
    
    public DefaultIndicatorSeekBar(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mDefaultProgress = -1;
    }
    
    public DefaultIndicatorSeekBar(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mDefaultProgress = -1;
    }
    
    protected void drawTickMarks(final Canvas canvas) {
        if (this.isEnabled() && this.mDefaultProgress <= this.getMax() && this.mDefaultProgress >= this.getMin()) {
            final Drawable tickMark = this.getTickMark();
            final int intrinsicWidth = tickMark.getIntrinsicWidth();
            final int intrinsicHeight = tickMark.getIntrinsicHeight();
            int n = 1;
            int n2;
            if (intrinsicWidth >= 0) {
                n2 = intrinsicWidth / 2;
            }
            else {
                n2 = 1;
            }
            if (intrinsicHeight >= 0) {
                n = intrinsicHeight / 2;
            }
            tickMark.setBounds(-n2, -n, n2, n);
            final int n3 = this.getWidth() - this.mPaddingLeft - this.mPaddingRight;
            final int n4 = this.getMax() - this.getMin();
            final float n5 = n4;
            float n6 = 0.0f;
            if (n5 > 0.0f) {
                n6 = this.mDefaultProgress / n4;
            }
            final int n7 = (int)(n3 * n6 + 0.5f);
            int n8;
            if (this.isLayoutRtl() && this.getMirrorForRtl()) {
                n8 = n3 - n7 + this.mPaddingRight;
            }
            else {
                n8 = this.mPaddingLeft + n7;
            }
            final int save = canvas.save();
            canvas.translate((float)n8, (float)(this.getHeight() / 2));
            tickMark.draw(canvas);
            canvas.restoreToCount(save);
        }
    }
    
    public int getDefaultProgress() {
        return this.mDefaultProgress;
    }
    
    public void setDefaultProgress(final int mDefaultProgress) {
        if (this.mDefaultProgress != mDefaultProgress) {
            this.mDefaultProgress = mDefaultProgress;
            this.invalidate();
        }
    }
}
