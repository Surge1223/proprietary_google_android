package com.android.settings.widget;

import android.graphics.Canvas;
import com.android.internal.util.Preconditions;
import android.content.res.TypedArray;
import com.android.internal.R$styleable;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.text.Layout;
import android.graphics.drawable.Drawable;
import android.view.View;

public class ChartGridView extends View
{
    private Drawable mBorder;
    private ChartAxis mHoriz;
    private int mLabelColor;
    private Layout mLabelEnd;
    private Layout mLabelMid;
    private int mLabelSize;
    private Layout mLabelStart;
    private Drawable mPrimary;
    private Drawable mSecondary;
    private ChartAxis mVert;
    
    public ChartGridView(final Context context) {
        this(context, null, 0);
    }
    
    public ChartGridView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChartGridView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.setWillNotDraw(false);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ChartGridView, n, 0);
        this.mPrimary = obtainStyledAttributes.getDrawable(3);
        this.mSecondary = obtainStyledAttributes.getDrawable(4);
        this.mBorder = obtainStyledAttributes.getDrawable(2);
        final TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(obtainStyledAttributes.getResourceId(0, -1), R$styleable.TextAppearance);
        this.mLabelSize = obtainStyledAttributes2.getDimensionPixelSize(0, 0);
        obtainStyledAttributes2.recycle();
        this.mLabelColor = obtainStyledAttributes.getColorStateList(1).getDefaultColor();
        obtainStyledAttributes.recycle();
    }
    
    void init(final ChartAxis chartAxis, final ChartAxis chartAxis2) {
        this.mHoriz = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis, (Object)"missing horiz");
        this.mVert = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis2, (Object)"missing vert");
    }
    
    protected void onDraw(final Canvas canvas) {
        final int width = this.getWidth();
        final int n = this.getHeight() - this.getPaddingBottom();
        final Drawable mSecondary = this.mSecondary;
        final int n2 = 0;
        if (mSecondary != null) {
            final int intrinsicHeight = mSecondary.getIntrinsicHeight();
            for (final float n3 : this.mVert.getTickPoints()) {
                mSecondary.setBounds(0, (int)n3, width, (int)Math.min(intrinsicHeight + n3, n));
                mSecondary.draw(canvas);
            }
        }
        final Drawable mPrimary = this.mPrimary;
        if (mPrimary != null) {
            final int intrinsicWidth = mPrimary.getIntrinsicWidth();
            mPrimary.getIntrinsicHeight();
            for (final float n4 : this.mHoriz.getTickPoints()) {
                mPrimary.setBounds((int)n4, 0, (int)Math.min(intrinsicWidth + n4, width), n);
                mPrimary.draw(canvas);
            }
        }
        this.mBorder.setBounds(0, 0, width, n);
        this.mBorder.draw(canvas);
        int n5 = n2;
        if (this.mLabelStart != null) {
            n5 = this.mLabelStart.getHeight() / 8;
        }
        final Layout mLabelStart = this.mLabelStart;
        if (mLabelStart != null) {
            final int save = canvas.save();
            canvas.translate(0.0f, (float)(n + n5));
            mLabelStart.draw(canvas);
            canvas.restoreToCount(save);
        }
        final Layout mLabelMid = this.mLabelMid;
        if (mLabelMid != null) {
            final int save2 = canvas.save();
            canvas.translate((float)((width - mLabelMid.getWidth()) / 2), (float)(n + n5));
            mLabelMid.draw(canvas);
            canvas.restoreToCount(save2);
        }
        final Layout mLabelEnd = this.mLabelEnd;
        if (mLabelEnd != null) {
            final int save3 = canvas.save();
            canvas.translate((float)(width - mLabelEnd.getWidth()), (float)(n + n5));
            mLabelEnd.draw(canvas);
            canvas.restoreToCount(save3);
        }
    }
}
