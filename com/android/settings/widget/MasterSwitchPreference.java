package com.android.settings.widget;

import com.android.settingslib.RestrictedLockUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Switch;
import com.android.settingslib.TwoTargetPreference;

public class MasterSwitchPreference extends TwoTargetPreference
{
    private boolean mChecked;
    private boolean mEnableSwitch;
    private Switch mSwitch;
    
    public MasterSwitchPreference(final Context context) {
        super(context);
        this.mEnableSwitch = true;
    }
    
    public MasterSwitchPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEnableSwitch = true;
    }
    
    public MasterSwitchPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mEnableSwitch = true;
    }
    
    public MasterSwitchPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mEnableSwitch = true;
    }
    
    @Override
    protected int getSecondTargetResId() {
        return 2131558683;
    }
    
    public boolean isChecked() {
        return this.mSwitch != null && this.mChecked;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(16908312);
        if (viewById != null) {
            viewById.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    if (MasterSwitchPreference.this.mSwitch != null && !MasterSwitchPreference.this.mSwitch.isEnabled()) {
                        return;
                    }
                    MasterSwitchPreference.this.setChecked(MasterSwitchPreference.this.mChecked ^ true);
                    if (!MasterSwitchPreference.this.callChangeListener(MasterSwitchPreference.this.mChecked)) {
                        MasterSwitchPreference.this.setChecked(MasterSwitchPreference.this.mChecked ^ true);
                    }
                    else {
                        Preference.this.persistBoolean(MasterSwitchPreference.this.mChecked);
                    }
                }
            });
        }
        this.mSwitch = (Switch)preferenceViewHolder.findViewById(R.id.switchWidget);
        if (this.mSwitch != null) {
            this.mSwitch.setContentDescription(this.getTitle());
            this.mSwitch.setChecked(this.mChecked);
            this.mSwitch.setEnabled(this.mEnableSwitch);
        }
    }
    
    public void setChecked(final boolean b) {
        this.mChecked = b;
        if (this.mSwitch != null) {
            this.mSwitch.setChecked(b);
        }
    }
    
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin enforcedAdmin) {
        this.setSwitchEnabled(enforcedAdmin == null);
    }
    
    public void setSwitchEnabled(final boolean b) {
        this.mEnableSwitch = b;
        if (this.mSwitch != null) {
            this.mSwitch.setEnabled(b);
        }
    }
}
