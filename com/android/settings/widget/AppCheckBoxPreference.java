package com.android.settings.widget;

import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.CheckBoxPreference;

public class AppCheckBoxPreference extends CheckBoxPreference
{
    public AppCheckBoxPreference(final Context context) {
        super(context);
        this.setLayoutResource(2131558644);
    }
    
    public AppCheckBoxPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setLayoutResource(2131558644);
    }
}
