package com.android.settings.widget;

import android.graphics.drawable.Drawable$Callback;
import android.util.MathUtils;
import android.graphics.Canvas;
import com.android.internal.util.Preconditions;
import android.view.View$OnLayoutChangeListener;
import android.text.Layout.Alignment;
import android.text.DynamicLayout$Builder;
import android.text.TextPaint;
import android.text.Layout;
import android.content.res.TypedArray;
import android.graphics.Paint.Style;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.MotionEvent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.Paint;
import android.text.SpannableStringBuilder;
import android.text.DynamicLayout;
import android.graphics.Rect;
import android.view.View.OnClickListener;
import android.view.View;

public class ChartSweepView extends View
{
    private ChartAxis mAxis;
    private View.OnClickListener mClickListener;
    private Rect mContentOffset;
    private long mDragInterval;
    private int mFollowAxis;
    private int mLabelColor;
    private DynamicLayout mLabelLayout;
    private int mLabelMinSize;
    private float mLabelOffset;
    private float mLabelSize;
    private SpannableStringBuilder mLabelTemplate;
    private int mLabelTemplateRes;
    private long mLabelValue;
    private OnSweepListener mListener;
    private Rect mMargins;
    private float mNeighborMargin;
    private ChartSweepView[] mNeighbors;
    private Paint mOutlinePaint;
    private int mSafeRegion;
    private Drawable mSweep;
    private Point mSweepOffset;
    private Rect mSweepPadding;
    private int mTouchMode;
    private MotionEvent mTracking;
    private float mTrackingStart;
    private long mValidAfter;
    private ChartSweepView mValidAfterDynamic;
    private long mValidBefore;
    private ChartSweepView mValidBeforeDynamic;
    private long mValue;
    
    public ChartSweepView(final Context context) {
        this(context, null);
    }
    
    public ChartSweepView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChartSweepView(final Context context, final AttributeSet set, int color) {
        super(context, set, color);
        this.mSweepPadding = new Rect();
        this.mContentOffset = new Rect();
        this.mSweepOffset = new Point();
        this.mMargins = new Rect();
        this.mOutlinePaint = new Paint();
        this.mTouchMode = 0;
        this.mDragInterval = 1L;
        this.mNeighbors = new ChartSweepView[0];
        this.mClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ChartSweepView.this.dispatchRequestEdit();
            }
        };
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ChartSweepView, color, 0);
        color = obtainStyledAttributes.getColor(1, -16776961);
        this.setSweepDrawable(obtainStyledAttributes.getDrawable(6), color);
        this.setFollowAxis(obtainStyledAttributes.getInt(0, -1));
        this.setNeighborMargin(obtainStyledAttributes.getDimensionPixelSize(4, 0));
        this.setSafeRegion(obtainStyledAttributes.getDimensionPixelSize(5, 0));
        this.setLabelMinSize(obtainStyledAttributes.getDimensionPixelSize(2, 0));
        this.setLabelTemplate(obtainStyledAttributes.getResourceId(3, 0));
        this.setLabelColor(color);
        this.setBackgroundResource(2131230906);
        this.mOutlinePaint.setColor(-65536);
        this.mOutlinePaint.setStrokeWidth(1.0f);
        this.mOutlinePaint.setStyle(Paint.Style.STROKE);
        obtainStyledAttributes.recycle();
        this.setClickable(true);
        this.setOnClickListener(this.mClickListener);
        this.setWillNotDraw(false);
    }
    
    private Rect buildClampRect(final Rect rect, final long n, final long n2, float n3) {
        long n4 = n;
        long n5 = n2;
        if (this.mAxis instanceof InvertedChartAxis) {
            n5 = n;
            n4 = n2;
        }
        final boolean b = false;
        final boolean b2 = n4 != Long.MIN_VALUE && n4 != Long.MAX_VALUE;
        boolean b3 = b;
        if (n5 != Long.MIN_VALUE) {
            b3 = b;
            if (n5 != Long.MAX_VALUE) {
                b3 = true;
            }
        }
        final float n6 = this.mAxis.convertToPoint(n4) + n3;
        n3 = this.mAxis.convertToPoint(n5) - n3;
        final Rect rect2 = new Rect(rect);
        if (this.mFollowAxis == 1) {
            if (b3) {
                rect2.bottom = rect2.top + (int)n3;
            }
            if (b2) {
                rect2.top += (int)n6;
            }
        }
        else {
            if (b3) {
                rect2.right = rect2.left + (int)n3;
            }
            if (b2) {
                rect2.left += (int)n6;
            }
        }
        return rect2;
    }
    
    private Rect computeClampRect(final Rect rect) {
        final Rect buildClampRect = this.buildClampRect(rect, this.mValidAfter, this.mValidBefore, 0.0f);
        if (!buildClampRect.intersect(this.buildClampRect(rect, this.getValidAfterDynamic(), this.getValidBeforeDynamic(), this.mNeighborMargin))) {
            buildClampRect.setEmpty();
        }
        return buildClampRect;
    }
    
    private void dispatchOnSweep(final boolean b) {
        if (this.mListener != null) {
            this.mListener.onSweep(this, b);
        }
    }
    
    private void dispatchRequestEdit() {
        if (this.mListener != null) {
            this.mListener.requestEdit(this);
        }
    }
    
    public static float getLabelBottom(final ChartSweepView chartSweepView) {
        return getLabelTop(chartSweepView) + chartSweepView.mLabelLayout.getHeight();
    }
    
    public static float getLabelTop(final ChartSweepView chartSweepView) {
        return chartSweepView.getY() + chartSweepView.mContentOffset.top;
    }
    
    public static float getLabelWidth(final ChartSweepView chartSweepView) {
        return Layout.getDesiredWidth(chartSweepView.mLabelLayout.getText(), chartSweepView.mLabelLayout.getPaint());
    }
    
    private Rect getParentContentRect() {
        final View view = (View)this.getParent();
        return new Rect(view.getPaddingLeft(), view.getPaddingTop(), view.getWidth() - view.getPaddingRight(), view.getHeight() - view.getPaddingBottom());
    }
    
    private float getTargetInset() {
        if (this.mFollowAxis == 1) {
            return this.mSweepPadding.top + (this.mSweep.getIntrinsicHeight() - this.mSweepPadding.top - this.mSweepPadding.bottom) / 2.0f + this.mSweepOffset.y;
        }
        return this.mSweepPadding.left + (this.mSweep.getIntrinsicWidth() - this.mSweepPadding.left - this.mSweepPadding.right) / 2.0f + this.mSweepOffset.x;
    }
    
    private float getTouchDistanceFromTarget(final MotionEvent motionEvent) {
        if (this.mFollowAxis == 0) {
            return Math.abs(motionEvent.getX() - (this.getX() + this.getTargetInset()));
        }
        return Math.abs(motionEvent.getY() - (this.getY() + this.getTargetInset()));
    }
    
    private long getValidAfterDynamic() {
        final ChartSweepView mValidAfterDynamic = this.mValidAfterDynamic;
        long value;
        if (mValidAfterDynamic != null && mValidAfterDynamic.isEnabled()) {
            value = mValidAfterDynamic.getValue();
        }
        else {
            value = Long.MIN_VALUE;
        }
        return value;
    }
    
    private long getValidBeforeDynamic() {
        final ChartSweepView mValidBeforeDynamic = this.mValidBeforeDynamic;
        long value;
        if (mValidBeforeDynamic != null && mValidBeforeDynamic.isEnabled()) {
            value = mValidBeforeDynamic.getValue();
        }
        else {
            value = Long.MAX_VALUE;
        }
        return value;
    }
    
    private void invalidateLabel() {
        if (this.mLabelTemplate != null && this.mAxis != null) {
            this.mLabelValue = this.mAxis.buildLabel(this.getResources(), this.mLabelTemplate, this.mValue);
            this.setContentDescription((CharSequence)this.mLabelTemplate);
            this.invalidateLabelOffset();
            this.invalidate();
        }
        else {
            this.mLabelValue = this.mValue;
        }
    }
    
    private void invalidateLabelTemplate() {
        if (this.mLabelTemplateRes != 0) {
            final CharSequence text = this.getResources().getText(this.mLabelTemplateRes);
            final TextPaint textPaint = new TextPaint(1);
            textPaint.density = this.getResources().getDisplayMetrics().density;
            textPaint.setCompatibilityScaling(this.getResources().getCompatibilityInfo().applicationScale);
            textPaint.setColor(this.mLabelColor);
            this.mLabelTemplate = new SpannableStringBuilder(text);
            this.mLabelLayout = DynamicLayout$Builder.obtain((CharSequence)this.mLabelTemplate, textPaint, 1024).setAlignment(Layout.Alignment.ALIGN_RIGHT).setIncludePad(false).setUseLineSpacingFromFallbacks(true).build();
            this.invalidateLabel();
        }
        else {
            this.mLabelTemplate = null;
            this.mLabelLayout = null;
        }
        this.invalidate();
        this.requestLayout();
    }
    
    public void addOnLayoutChangeListener(final View$OnLayoutChangeListener view$OnLayoutChangeListener) {
    }
    
    public void addOnSweepListener(final OnSweepListener mListener) {
        this.mListener = mListener;
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mSweep.isStateful()) {
            this.mSweep.setState(this.getDrawableState());
        }
    }
    
    public ChartAxis getAxis() {
        return this.mAxis;
    }
    
    public int getFollowAxis() {
        return this.mFollowAxis;
    }
    
    public long getLabelValue() {
        return this.mLabelValue;
    }
    
    public Rect getMargins() {
        return this.mMargins;
    }
    
    public float getPoint() {
        if (this.isEnabled()) {
            return this.mAxis.convertToPoint(this.mValue);
        }
        return 0.0f;
    }
    
    public long getValue() {
        return this.mValue;
    }
    
    void init(final ChartAxis chartAxis) {
        this.mAxis = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis, (Object)"missing axis");
    }
    
    public void invalidateLabelOffset() {
        float mLabelOffset;
        final float n = mLabelOffset = 0.0f;
        if (this.mFollowAxis == 1) {
            if (this.mValidAfterDynamic != null) {
                this.mLabelSize = Math.max(getLabelWidth(this), getLabelWidth(this.mValidAfterDynamic));
                final float n2 = getLabelTop(this.mValidAfterDynamic) - getLabelBottom(this);
                mLabelOffset = n;
                if (n2 < 0.0f) {
                    mLabelOffset = n2 / 2.0f;
                }
            }
            else if (this.mValidBeforeDynamic != null) {
                this.mLabelSize = Math.max(getLabelWidth(this), getLabelWidth(this.mValidBeforeDynamic));
                final float n3 = getLabelTop(this) - getLabelBottom(this.mValidBeforeDynamic);
                mLabelOffset = n;
                if (n3 < 0.0f) {
                    mLabelOffset = -n3 / 2.0f;
                }
            }
            else {
                this.mLabelSize = getLabelWidth(this);
                mLabelOffset = n;
            }
        }
        this.mLabelSize = Math.max(this.mLabelSize, this.mLabelMinSize);
        if (mLabelOffset != this.mLabelOffset) {
            this.mLabelOffset = mLabelOffset;
            this.invalidate();
            if (this.mValidAfterDynamic != null) {
                this.mValidAfterDynamic.invalidateLabelOffset();
            }
            if (this.mValidBeforeDynamic != null) {
                this.mValidBeforeDynamic.invalidateLabelOffset();
            }
        }
    }
    
    public boolean isTouchCloserTo(final MotionEvent motionEvent, final ChartSweepView chartSweepView) {
        return chartSweepView.getTouchDistanceFromTarget(motionEvent) < this.getTouchDistanceFromTarget(motionEvent);
    }
    
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        if (this.mSweep != null) {
            this.mSweep.jumpToCurrentState();
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final int width = this.getWidth();
        final int height = this.getHeight();
        int n;
        if (this.isEnabled() && this.mLabelLayout != null) {
            final int save = canvas.save();
            canvas.translate(this.mContentOffset.left + (this.mLabelSize - 1024.0f), this.mContentOffset.top + this.mLabelOffset);
            this.mLabelLayout.draw(canvas);
            canvas.restoreToCount(save);
            n = (int)this.mLabelSize + this.mSafeRegion;
        }
        else {
            n = 0;
        }
        if (this.mFollowAxis == 1) {
            this.mSweep.setBounds(n, this.mSweepOffset.y, this.mContentOffset.right + width, this.mSweepOffset.y + this.mSweep.getIntrinsicHeight());
        }
        else {
            this.mSweep.setBounds(this.mSweepOffset.x, n, this.mSweepOffset.x + this.mSweep.getIntrinsicWidth(), this.mContentOffset.bottom + height);
        }
        this.mSweep.draw(canvas);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.invalidateLabelOffset();
    }
    
    protected void onMeasure(int n, int n2) {
        if (this.isEnabled() && this.mLabelLayout != null) {
            n2 = this.mSweep.getIntrinsicHeight();
            n = this.mLabelLayout.getHeight();
            this.mSweepOffset.x = 0;
            this.mSweepOffset.y = 0;
            this.mSweepOffset.y = (int)(n / 2 - this.getTargetInset());
            this.setMeasuredDimension(this.mSweep.getIntrinsicWidth(), Math.max(n2, n));
        }
        else {
            this.mSweepOffset.x = 0;
            this.mSweepOffset.y = 0;
            this.setMeasuredDimension(this.mSweep.getIntrinsicWidth(), this.mSweep.getIntrinsicHeight());
        }
        if (this.mFollowAxis == 1) {
            n2 = this.mSweep.getIntrinsicHeight();
            n = this.mSweepPadding.top;
            this.mMargins.top = -(this.mSweepPadding.top + (n2 - n - this.mSweepPadding.bottom) / 2);
            this.mMargins.bottom = 0;
            this.mMargins.left = -this.mSweepPadding.left;
            this.mMargins.right = this.mSweepPadding.right;
        }
        else {
            final int intrinsicWidth = this.mSweep.getIntrinsicWidth();
            n2 = this.mSweepPadding.left;
            n = this.mSweepPadding.right;
            this.mMargins.left = -(this.mSweepPadding.left + (intrinsicWidth - n2 - n) / 2);
            this.mMargins.right = 0;
            this.mMargins.top = -this.mSweepPadding.top;
            this.mMargins.bottom = this.mSweepPadding.bottom;
        }
        this.mContentOffset.set(0, 0, 0, 0);
        n2 = this.getMeasuredWidth();
        n = this.getMeasuredHeight();
        if (this.mFollowAxis == 0) {
            final int n3 = n2 * 3;
            this.setMeasuredDimension(n3, n);
            this.mContentOffset.left = (n3 - n2) / 2;
            n = this.mSweepPadding.bottom * 2;
            final Rect mContentOffset = this.mContentOffset;
            mContentOffset.bottom -= n;
            final Rect mMargins = this.mMargins;
            mMargins.bottom += n;
        }
        else {
            final int n4 = n * 2;
            this.setMeasuredDimension(n2, n4);
            this.mContentOffset.offset(0, (n4 - n) / 2);
            n = this.mSweepPadding.right * 2;
            final Rect mContentOffset2 = this.mContentOffset;
            mContentOffset2.right -= n;
            final Rect mMargins2 = this.mMargins;
            mMargins2.right += n;
        }
        this.mSweepOffset.offset(this.mContentOffset.left, this.mContentOffset.top);
        this.mMargins.offset(-this.mSweepOffset.x, -this.mSweepOffset.y);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (!this.isEnabled()) {
            return false;
        }
        final View view = (View)this.getParent();
        switch (motionEvent.getAction()) {
            default: {
                return false;
            }
            case 2: {
                if (this.mTouchMode == 2) {
                    return true;
                }
                this.getParent().requestDisallowInterceptTouchEvent(true);
                final Rect parentContentRect = this.getParentContentRect();
                final Rect computeClampRect = this.computeClampRect(parentContentRect);
                if (computeClampRect.isEmpty()) {
                    return true;
                }
                long n2;
                if (this.mFollowAxis == 1) {
                    final float n = this.getTop() - this.mMargins.top;
                    final float constrain = MathUtils.constrain(this.mTrackingStart + (motionEvent.getRawY() - this.mTracking.getRawY()), (float)computeClampRect.top, (float)computeClampRect.bottom);
                    this.setTranslationY(constrain - n);
                    n2 = this.mAxis.convertToValue(constrain - parentContentRect.top);
                }
                else {
                    final float n3 = this.getLeft() - this.mMargins.left;
                    final float constrain2 = MathUtils.constrain(this.mTrackingStart + (motionEvent.getRawX() - this.mTracking.getRawX()), (float)computeClampRect.left, (float)computeClampRect.right);
                    this.setTranslationX(constrain2 - n3);
                    n2 = this.mAxis.convertToValue(constrain2 - parentContentRect.left);
                }
                this.setValue(n2 - n2 % this.mDragInterval);
                this.dispatchOnSweep(false);
                return true;
            }
            case 1: {
                if (this.mTouchMode == 2) {
                    this.performClick();
                }
                else if (this.mTouchMode == 1) {
                    this.mTrackingStart = 0.0f;
                    this.mTracking = null;
                    this.mValue = this.mLabelValue;
                    this.dispatchOnSweep(true);
                    this.setTranslationX(0.0f);
                    this.setTranslationY(0.0f);
                    this.requestLayout();
                }
                this.mTouchMode = 0;
                return true;
            }
            case 0: {
                boolean b3;
                boolean b4;
                if (this.mFollowAxis == 1) {
                    final boolean b = motionEvent.getX() > this.getWidth() - this.mSweepPadding.right * 8;
                    final boolean b2 = this.mLabelLayout != null && motionEvent.getX() < this.mLabelLayout.getWidth();
                    b3 = b;
                    b4 = b2;
                }
                else {
                    final boolean b5 = motionEvent.getY() > this.getHeight() - this.mSweepPadding.bottom * 8;
                    if (this.mLabelLayout != null && motionEvent.getY() < this.mLabelLayout.getHeight()) {
                        b4 = true;
                        b3 = b5;
                    }
                    else {
                        b4 = false;
                        b3 = b5;
                    }
                }
                final MotionEvent copy = motionEvent.copy();
                copy.offsetLocation((float)this.getLeft(), (float)this.getTop());
                final ChartSweepView[] mNeighbors = this.mNeighbors;
                for (int length = mNeighbors.length, i = 0; i < length; ++i) {
                    if (this.isTouchCloserTo(copy, mNeighbors[i])) {
                        return false;
                    }
                }
                if (b3) {
                    if (this.mFollowAxis == 1) {
                        this.mTrackingStart = this.getTop() - this.mMargins.top;
                    }
                    else {
                        this.mTrackingStart = this.getLeft() - this.mMargins.left;
                    }
                    this.mTracking = motionEvent.copy();
                    this.mTouchMode = 1;
                    if (!view.isActivated()) {
                        view.setActivated(true);
                    }
                    return true;
                }
                if (b4) {
                    this.mTouchMode = 2;
                    return true;
                }
                this.mTouchMode = 0;
                return false;
            }
        }
    }
    
    public void removeOnLayoutChangeListener(final View$OnLayoutChangeListener view$OnLayoutChangeListener) {
    }
    
    public void setDragInterval(final long mDragInterval) {
        this.mDragInterval = mDragInterval;
    }
    
    public void setEnabled(final boolean b) {
        super.setEnabled(b);
        this.setFocusable(b);
        this.requestLayout();
    }
    
    public void setFollowAxis(final int mFollowAxis) {
        this.mFollowAxis = mFollowAxis;
    }
    
    public void setLabelColor(final int mLabelColor) {
        this.mLabelColor = mLabelColor;
        this.invalidateLabelTemplate();
    }
    
    public void setLabelMinSize(final int mLabelMinSize) {
        this.mLabelMinSize = mLabelMinSize;
        this.invalidateLabelTemplate();
    }
    
    public void setLabelTemplate(final int mLabelTemplateRes) {
        this.mLabelTemplateRes = mLabelTemplateRes;
        this.invalidateLabelTemplate();
    }
    
    public void setNeighborMargin(final float mNeighborMargin) {
        this.mNeighborMargin = mNeighborMargin;
    }
    
    public void setNeighbors(final ChartSweepView... mNeighbors) {
        this.mNeighbors = mNeighbors;
    }
    
    public void setSafeRegion(final int mSafeRegion) {
        this.mSafeRegion = mSafeRegion;
    }
    
    public void setSweepDrawable(final Drawable mSweep, final int tint) {
        if (this.mSweep != null) {
            this.mSweep.setCallback((Drawable$Callback)null);
            this.unscheduleDrawable(this.mSweep);
        }
        if (mSweep != null) {
            mSweep.setCallback((Drawable$Callback)this);
            if (mSweep.isStateful()) {
                mSweep.setState(this.getDrawableState());
            }
            mSweep.setVisible(this.getVisibility() == 0, false);
            (this.mSweep = mSweep).setTint(tint);
            mSweep.getPadding(this.mSweepPadding);
        }
        else {
            this.mSweep = null;
        }
        this.invalidate();
    }
    
    public void setValidRange(final long mValidAfter, final long mValidBefore) {
        this.mValidAfter = mValidAfter;
        this.mValidBefore = mValidBefore;
    }
    
    public void setValidRangeDynamic(final ChartSweepView mValidAfterDynamic, final ChartSweepView mValidBeforeDynamic) {
        this.mValidAfterDynamic = mValidAfterDynamic;
        this.mValidBeforeDynamic = mValidBeforeDynamic;
    }
    
    public void setValue(final long mValue) {
        this.mValue = mValue;
        this.invalidateLabel();
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        if (this.mSweep != null) {
            this.mSweep.setVisible(visibility == 0, false);
        }
    }
    
    public int shouldAdjustAxis() {
        return this.mAxis.shouldAdjustAxis(this.getValue());
    }
    
    public void updateValueFromPosition() {
        final Rect parentContentRect = this.getParentContentRect();
        if (this.mFollowAxis == 1) {
            this.setValue(this.mAxis.convertToValue(this.getY() - this.mMargins.top - parentContentRect.top));
        }
        else {
            this.setValue(this.mAxis.convertToValue(this.getX() - this.mMargins.left - parentContentRect.left));
        }
    }
    
    protected boolean verifyDrawable(final Drawable drawable) {
        return drawable == this.mSweep || super.verifyDrawable(drawable);
    }
    
    public interface OnSweepListener
    {
        void onSweep(final ChartSweepView p0, final boolean p1);
        
        void requestEdit(final ChartSweepView p0);
    }
}
