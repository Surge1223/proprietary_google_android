package com.android.settings.widget;

import com.android.internal.annotations.VisibleForTesting;
import android.text.style.RelativeSizeSpan;
import android.text.SpannableString;
import android.text.Spannable;
import android.text.StaticLayout;
import android.text.Layout.Alignment;
import android.icu.text.DecimalFormatSymbols;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.Paint$Align;
import android.text.TextUtils;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Paint.Style;
import android.graphics.Paint$Cap;
import com.android.settings.R;
import com.android.settingslib.Utils;
import android.util.AttributeSet;
import android.content.Context;
import android.text.TextPaint;
import android.graphics.Paint;
import android.view.View;

public class DonutView extends View
{
    private Paint mBackgroundCircle;
    private TextPaint mBigNumberPaint;
    private Paint mFilledArc;
    private String mFullString;
    private int mMeterBackgroundColor;
    private int mMeterConsumedColor;
    private double mPercent;
    private String mPercentString;
    private boolean mShowPercentString;
    private float mStrokeWidth;
    private TextPaint mTextPaint;
    
    public DonutView(final Context context) {
        super(context);
        this.mShowPercentString = true;
    }
    
    public DonutView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mShowPercentString = true;
        this.mMeterBackgroundColor = context.getColor(R.color.meter_background_color);
        this.mMeterConsumedColor = Utils.getDefaultColor(this.mContext, R.color.meter_consumed_color);
        boolean boolean1 = true;
        final Resources resources = context.getResources();
        this.mStrokeWidth = resources.getDimension(2131165557);
        if (set != null) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.DonutView);
            this.mMeterBackgroundColor = obtainStyledAttributes.getColor(1, this.mMeterBackgroundColor);
            this.mMeterConsumedColor = obtainStyledAttributes.getColor(2, this.mMeterConsumedColor);
            boolean1 = obtainStyledAttributes.getBoolean(0, true);
            this.mShowPercentString = obtainStyledAttributes.getBoolean(3, true);
            this.mStrokeWidth = obtainStyledAttributes.getDimensionPixelSize(4, (int)this.mStrokeWidth);
            obtainStyledAttributes.recycle();
        }
        (this.mBackgroundCircle = new Paint()).setAntiAlias(true);
        this.mBackgroundCircle.setStrokeCap(Paint$Cap.BUTT);
        this.mBackgroundCircle.setStyle(Paint.Style.STROKE);
        this.mBackgroundCircle.setStrokeWidth(this.mStrokeWidth);
        this.mBackgroundCircle.setColor(this.mMeterBackgroundColor);
        (this.mFilledArc = new Paint()).setAntiAlias(true);
        this.mFilledArc.setStrokeCap(Paint$Cap.BUTT);
        this.mFilledArc.setStyle(Paint.Style.STROKE);
        this.mFilledArc.setStrokeWidth(this.mStrokeWidth);
        this.mFilledArc.setColor(this.mMeterConsumedColor);
        if (boolean1) {
            final PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Utils.getColorAttr(context, 16843829), PorterDuff.Mode.SRC_IN);
            this.mBackgroundCircle.setColorFilter((ColorFilter)porterDuffColorFilter);
            this.mFilledArc.setColorFilter((ColorFilter)porterDuffColorFilter);
        }
        int n;
        if (TextUtils.getLayoutDirectionFromLocale(resources.getConfiguration().locale) == 0) {
            n = 0;
        }
        else {
            n = 1;
        }
        (this.mTextPaint = new TextPaint()).setColor(Utils.getColorAccent(this.getContext()));
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(resources.getDimension(2131165558));
        this.mTextPaint.setTextAlign(Paint$Align.CENTER);
        this.mTextPaint.setBidiFlags(n);
        (this.mBigNumberPaint = new TextPaint()).setColor(Utils.getColorAccent(this.getContext()));
        this.mBigNumberPaint.setAntiAlias(true);
        this.mBigNumberPaint.setTextSize(resources.getDimension(2131165560));
        this.mBigNumberPaint.setTypeface(Typeface.create(context.getString(17039680), 0));
        this.mBigNumberPaint.setBidiFlags(n);
    }
    
    private void drawDonut(final Canvas canvas) {
        canvas.drawArc(0.0f + this.mStrokeWidth, 0.0f + this.mStrokeWidth, this.getWidth() - this.mStrokeWidth, this.getHeight() - this.mStrokeWidth, -90.0f, 360.0f, false, this.mBackgroundCircle);
        canvas.drawArc(0.0f + this.mStrokeWidth, 0.0f + this.mStrokeWidth, this.getWidth() - this.mStrokeWidth, this.getHeight() - this.mStrokeWidth, -90.0f, 360.0f * (float)this.mPercent, false, this.mFilledArc);
    }
    
    private void drawInnerText(final Canvas canvas) {
        final float n = this.getWidth() / 2;
        final float n2 = this.getHeight() / 2;
        final float n3 = this.getTextHeight(this.mTextPaint) + this.getTextHeight(this.mBigNumberPaint);
        final float n4 = n3 / 2.0f;
        final String percentString = new DecimalFormatSymbols().getPercentString();
        canvas.save();
        final StaticLayout staticLayout = new StaticLayout((CharSequence)getPercentageStringSpannable(this.getResources(), this.mPercentString, percentString), this.mBigNumberPaint, this.getWidth(), Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
        canvas.translate(0.0f, (this.getHeight() - n3) / 2.0f);
        staticLayout.draw(canvas);
        canvas.restore();
        canvas.drawText(this.mFullString, n, n4 + n2 - this.mTextPaint.descent(), (Paint)this.mTextPaint);
    }
    
    @VisibleForTesting
    static Spannable getPercentageStringSpannable(final Resources resources, final String s, final String s2) {
        final float n = resources.getDimension(2131165559) / resources.getDimension(2131165560);
        final SpannableString spannableString = new SpannableString((CharSequence)s);
        final int index = s.indexOf(s2);
        int length = s2.length() + index;
        int n2 = index;
        if (index < 0) {
            n2 = 0;
            length = s.length();
        }
        ((Spannable)spannableString).setSpan((Object)new RelativeSizeSpan(n), n2, length, 34);
        return (Spannable)spannableString;
    }
    
    private float getTextHeight(final TextPaint textPaint) {
        return textPaint.descent() - textPaint.ascent();
    }
    
    public int getMeterBackgroundColor() {
        return this.mMeterBackgroundColor;
    }
    
    public int getMeterConsumedColor() {
        return this.mMeterConsumedColor;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        this.drawDonut(canvas);
        if (this.mShowPercentString) {
            this.drawInnerText(canvas);
        }
    }
    
    public void setMeterBackgroundColor(final int n) {
        this.mMeterBackgroundColor = n;
        this.mBackgroundCircle.setColor(n);
        this.invalidate();
    }
    
    public void setMeterConsumedColor(final int n) {
        this.mMeterConsumedColor = n;
        this.mFilledArc.setColor(n);
        this.invalidate();
    }
    
    public void setPercentage(final double mPercent) {
        this.mPercent = mPercent;
        this.mPercentString = Utils.formatPercentage(this.mPercent);
        this.mFullString = this.getContext().getString(2131889303);
        if (this.mFullString.length() > 10) {
            this.mTextPaint.setTextSize(this.getContext().getResources().getDimension(2131165561));
        }
        this.setContentDescription((CharSequence)this.getContext().getString(2131887916, new Object[] { this.mPercentString, this.mFullString }));
        this.invalidate();
    }
}
