package com.android.settings.widget;

import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.database.ContentObserver;
import android.os.Process;
import android.location.LocationManager;
import android.os.UserManager;
import android.provider.Settings;
import android.os.AsyncTask;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settings.bluetooth.Utils;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.provider.Settings$SettingNotFoundException;
import android.os.PowerManager;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.content.Intent;
import android.app.PendingIntent;
import android.util.Log;
import android.provider.Settings;
import android.os.Handler;
import android.widget.RemoteViews;
import android.content.Context;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import android.content.ComponentName;
import android.appwidget.AppWidgetProvider;

public class SettingsAppWidgetProvider extends AppWidgetProvider
{
    private static final int[] IND_DRAWABLE_MID;
    private static final int[] IND_DRAWABLE_OFF;
    private static final int[] IND_DRAWABLE_ON;
    static final ComponentName THIS_APPWIDGET;
    private static final StateTracker sBluetoothState;
    private static LocalBluetoothAdapter sLocalBluetoothAdapter;
    private static final StateTracker sLocationState;
    private static SettingsObserver sSettingsObserver;
    private static final StateTracker sSyncState;
    private static final StateTracker sWifiState;
    
    static {
        THIS_APPWIDGET = new ComponentName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        SettingsAppWidgetProvider.sLocalBluetoothAdapter = null;
        IND_DRAWABLE_OFF = new int[] { 2131230862, 2131230861, 2131230863 };
        IND_DRAWABLE_MID = new int[] { 2131230859, 2131230858, 2131230860 };
        IND_DRAWABLE_ON = new int[] { 2131230865, 2131230864, 2131230866 };
        sWifiState = (StateTracker)new WifiStateTracker();
        sBluetoothState = (StateTracker)new BluetoothStateTracker();
        sLocationState = (StateTracker)new LocationStateTracker();
        sSyncState = (StateTracker)new SyncStateTracker();
    }
    
    static RemoteViews buildUpdate(final Context context) {
        final RemoteViews remoteViews = new RemoteViews(context.getPackageName(), 2131558880);
        remoteViews.setOnClickPendingIntent(2131361935, getLaunchPendingIntent(context, 0));
        remoteViews.setOnClickPendingIntent(2131361932, getLaunchPendingIntent(context, 1));
        remoteViews.setOnClickPendingIntent(2131361934, getLaunchPendingIntent(context, 2));
        remoteViews.setOnClickPendingIntent(2131361933, getLaunchPendingIntent(context, 3));
        remoteViews.setOnClickPendingIntent(2131361931, getLaunchPendingIntent(context, 4));
        updateButtons(remoteViews, context);
        return remoteViews;
    }
    
    private static void checkObserver(final Context context) {
        if (SettingsAppWidgetProvider.sSettingsObserver == null) {
            (SettingsAppWidgetProvider.sSettingsObserver = new SettingsObserver(new Handler(), context.getApplicationContext())).startObserving();
        }
    }
    
    private static int getBrightness(final Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "screen_brightness");
        }
        catch (Exception ex) {
            return 0;
        }
    }
    
    private static boolean getBrightnessMode(final Context context) {
        boolean b = false;
        try {
            if (Settings.System.getInt(context.getContentResolver(), "screen_brightness_mode") == 1) {
                b = true;
            }
            return b;
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getBrightnessMode: ");
            sb.append(ex);
            Log.d("SettingsAppWidgetProvider", sb.toString());
            return false;
        }
    }
    
    private static PendingIntent getLaunchPendingIntent(final Context context, final int n) {
        final Intent intent = new Intent();
        intent.setClass(context, (Class)SettingsAppWidgetProvider.class);
        intent.addCategory("android.intent.category.ALTERNATIVE");
        final StringBuilder sb = new StringBuilder();
        sb.append("custom:");
        sb.append(n);
        intent.setData(Uri.parse(sb.toString()));
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }
    
    private void toggleBrightness(final Context context) {
        try {
            final DisplayManager displayManager = (DisplayManager)context.getSystemService((Class)DisplayManager.class);
            final PowerManager powerManager = (PowerManager)context.getSystemService((Class)PowerManager.class);
            final ContentResolver contentResolver = context.getContentResolver();
            final int int1 = Settings.System.getInt(contentResolver, "screen_brightness");
            int int2 = 0;
            if (context.getResources().getBoolean(17956895)) {
                int2 = Settings.System.getInt(contentResolver, "screen_brightness_mode");
            }
            int temporaryBrightness;
            if (int2 == 1) {
                temporaryBrightness = powerManager.getMinimumScreenBrightnessSetting();
                int2 = 0;
            }
            else if (int1 < powerManager.getDefaultScreenBrightnessSetting()) {
                temporaryBrightness = powerManager.getDefaultScreenBrightnessSetting();
            }
            else if (int1 < powerManager.getMaximumScreenBrightnessSetting()) {
                temporaryBrightness = powerManager.getMaximumScreenBrightnessSetting();
            }
            else {
                int2 = 1;
                temporaryBrightness = powerManager.getMinimumScreenBrightnessSetting();
            }
            if (context.getResources().getBoolean(17956895)) {
                Settings.System.putInt(context.getContentResolver(), "screen_brightness_mode", int2);
            }
            else {
                int2 = 0;
            }
            if (int2 == 0) {
                displayManager.setTemporaryBrightness(temporaryBrightness);
                Settings.System.putInt(contentResolver, "screen_brightness", temporaryBrightness);
            }
        }
        catch (Settings$SettingNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("toggleBrightness: ");
            sb.append(ex);
            Log.d("SettingsAppWidgetProvider", sb.toString());
        }
    }
    
    private static void updateButtons(final RemoteViews remoteViews, final Context context) {
        SettingsAppWidgetProvider.sWifiState.setImageViewResources(context, remoteViews);
        SettingsAppWidgetProvider.sBluetoothState.setImageViewResources(context, remoteViews);
        SettingsAppWidgetProvider.sLocationState.setImageViewResources(context, remoteViews);
        SettingsAppWidgetProvider.sSyncState.setImageViewResources(context, remoteViews);
        if (getBrightnessMode(context)) {
            remoteViews.setContentDescription(2131361932, (CharSequence)context.getString(2131887706, new Object[] { context.getString(2131887702) }));
            remoteViews.setImageViewResource(2131362254, 2131230956);
            remoteViews.setImageViewResource(2131362262, 2131230866);
        }
        else {
            final int brightness = getBrightness(context);
            final PowerManager powerManager = (PowerManager)context.getSystemService((Class)PowerManager.class);
            final int n = (int)(powerManager.getMaximumScreenBrightnessSetting() * 0.8f);
            final int n2 = (int)(powerManager.getMaximumScreenBrightnessSetting() * 0.3f);
            if (brightness > n) {
                remoteViews.setContentDescription(2131361932, (CharSequence)context.getString(2131887706, new Object[] { context.getString(2131887703) }));
                remoteViews.setImageViewResource(2131362254, 2131230957);
            }
            else if (brightness > n2) {
                remoteViews.setContentDescription(2131361932, (CharSequence)context.getString(2131887706, new Object[] { context.getString(2131887704) }));
                remoteViews.setImageViewResource(2131362254, 2131230958);
            }
            else {
                remoteViews.setContentDescription(2131361932, (CharSequence)context.getString(2131887706, new Object[] { context.getString(2131887705) }));
                remoteViews.setImageViewResource(2131362254, 2131230959);
            }
            if (brightness > n2) {
                remoteViews.setImageViewResource(2131362262, 2131230866);
            }
            else {
                remoteViews.setImageViewResource(2131362262, 2131230863);
            }
        }
    }
    
    public static void updateWidget(final Context context) {
        AppWidgetManager.getInstance(context).updateAppWidget(SettingsAppWidgetProvider.THIS_APPWIDGET, buildUpdate(context));
        checkObserver(context);
    }
    
    public void onDisabled(final Context context) {
        if (SettingsAppWidgetProvider.sSettingsObserver != null) {
            SettingsAppWidgetProvider.sSettingsObserver.stopObserving();
            SettingsAppWidgetProvider.sSettingsObserver = null;
        }
    }
    
    public void onEnabled(final Context context) {
        checkObserver(context);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        super.onReceive(context, intent);
        final String action = intent.getAction();
        if ("android.net.wifi.WIFI_STATE_CHANGED".equals(action)) {
            SettingsAppWidgetProvider.sWifiState.onActualStateChange(context, intent);
        }
        else if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(action)) {
            SettingsAppWidgetProvider.sBluetoothState.onActualStateChange(context, intent);
        }
        else if ("android.location.MODE_CHANGED".equals(action)) {
            SettingsAppWidgetProvider.sLocationState.onActualStateChange(context, intent);
        }
        else if (ContentResolver.ACTION_SYNC_CONN_STATUS_CHANGED.equals(action)) {
            SettingsAppWidgetProvider.sSyncState.onActualStateChange(context, intent);
        }
        else {
            if (!intent.hasCategory("android.intent.category.ALTERNATIVE")) {
                return;
            }
            final int int1 = Integer.parseInt(intent.getData().getSchemeSpecificPart());
            if (int1 == 0) {
                SettingsAppWidgetProvider.sWifiState.toggleState(context);
            }
            else if (int1 == 1) {
                this.toggleBrightness(context);
            }
            else if (int1 == 2) {
                SettingsAppWidgetProvider.sSyncState.toggleState(context);
            }
            else if (int1 == 3) {
                SettingsAppWidgetProvider.sLocationState.toggleState(context);
            }
            else if (int1 == 4) {
                SettingsAppWidgetProvider.sBluetoothState.toggleState(context);
            }
        }
        updateWidget(context);
    }
    
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] array) {
        final RemoteViews buildUpdate = buildUpdate(context);
        for (int i = 0; i < array.length; ++i) {
            appWidgetManager.updateAppWidget(array[i], buildUpdate);
        }
    }
    
    private static final class BluetoothStateTracker extends StateTracker
    {
        private static int bluetoothStateToFiveState(final int n) {
            switch (n) {
                default: {
                    return 4;
                }
                case 13: {
                    return 3;
                }
                case 12: {
                    return 1;
                }
                case 11: {
                    return 2;
                }
                case 10: {
                    return 0;
                }
            }
        }
        
        @Override
        public int getActualState(final Context context) {
            if (SettingsAppWidgetProvider.sLocalBluetoothAdapter == null) {
                final LocalBluetoothManager localBtManager = Utils.getLocalBtManager(context);
                if (localBtManager == null) {
                    return 4;
                }
                SettingsAppWidgetProvider.sLocalBluetoothAdapter = localBtManager.getBluetoothAdapter();
                if (SettingsAppWidgetProvider.sLocalBluetoothAdapter == null) {
                    return 4;
                }
            }
            return bluetoothStateToFiveState(SettingsAppWidgetProvider.sLocalBluetoothAdapter.getBluetoothState());
        }
        
        @Override
        public int getButtonDescription() {
            return 2131887701;
        }
        
        @Override
        public int getButtonId() {
            return 2131362253;
        }
        
        @Override
        public int getButtonImageId(final boolean b) {
            int n;
            if (b) {
                n = 2131230955;
            }
            else {
                n = 2131230954;
            }
            return n;
        }
        
        @Override
        public int getContainerId() {
            return 2131361931;
        }
        
        @Override
        public int getIndicatorId() {
            return 2131362261;
        }
        
        @Override
        public void onActualStateChange(final Context context, final Intent intent) {
            if (!"android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction())) {
                return;
            }
            ((StateTracker)this).setCurrentState(context, bluetoothStateToFiveState(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1)));
        }
        
        @Override
        protected void requestStateChange(final Context context, final boolean b) {
            if (SettingsAppWidgetProvider.sLocalBluetoothAdapter == null) {
                Log.d("SettingsAppWidgetProvider", "No LocalBluetoothManager");
                return;
            }
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(final Void... array) {
                    SettingsAppWidgetProvider.sLocalBluetoothAdapter.setBluetoothEnabled(b);
                    return null;
                }
            }.execute((Object[])new Void[0]);
        }
    }
    
    private static final class LocationStateTracker extends StateTracker
    {
        private int mCurrentLocationMode;
        
        private LocationStateTracker() {
            this.mCurrentLocationMode = 0;
        }
        
        @Override
        public int getActualState(final Context context) {
            final ContentResolver contentResolver = context.getContentResolver();
            int n = 0;
            this.mCurrentLocationMode = Settings.Secure.getInt(contentResolver, "location_mode", 0);
            if (this.mCurrentLocationMode != 0) {
                n = 1;
            }
            return n;
        }
        
        @Override
        public int getButtonDescription() {
            return 2131887707;
        }
        
        @Override
        public int getButtonId() {
            return 2131362255;
        }
        
        @Override
        public int getButtonImageId(final boolean b) {
            if (!b) {
                return 2131230960;
            }
            final int mCurrentLocationMode = this.mCurrentLocationMode;
            if (mCurrentLocationMode != 1 && mCurrentLocationMode != 3) {
                return 2131230962;
            }
            return 2131230961;
        }
        
        @Override
        public int getContainerId() {
            return 2131361933;
        }
        
        @Override
        public int getIndicatorId() {
            return 2131362263;
        }
        
        @Override
        public void onActualStateChange(final Context context, final Intent intent) {
            ((StateTracker)this).setCurrentState(context, this.getActualState(context));
        }
        
        public void requestStateChange(final Context context, final boolean b) {
            context.getContentResolver();
            new AsyncTask<Void, Void, Boolean>() {
                protected Boolean doInBackground(final Void... array) {
                    if (!((UserManager)context.getSystemService("user")).hasUserRestriction("no_share_location")) {
                        final LocationManager locationManager = (LocationManager)context.getSystemService("location");
                        locationManager.setLocationEnabledForUser(locationManager.isLocationEnabled() ^ true, Process.myUserHandle());
                        return locationManager.isLocationEnabled();
                    }
                    final int actualState = LocationStateTracker.this.getActualState(context);
                    boolean b = true;
                    if (actualState != 1) {
                        b = false;
                    }
                    return b;
                }
                
                protected void onPostExecute(final Boolean b) {
                    ((StateTracker)LocationStateTracker.this).setCurrentState(context, ((boolean)b) ? 1 : 0);
                    SettingsAppWidgetProvider.updateWidget(context);
                }
            }.execute((Object[])new Void[0]);
        }
    }
    
    private static class SettingsObserver extends ContentObserver
    {
        private Context mContext;
        
        SettingsObserver(final Handler handler, final Context mContext) {
            super(handler);
            this.mContext = mContext;
        }
        
        public void onChange(final boolean b) {
            SettingsAppWidgetProvider.updateWidget(this.mContext);
        }
        
        void startObserving() {
            final ContentResolver contentResolver = this.mContext.getContentResolver();
            contentResolver.registerContentObserver(Settings.System.getUriFor("screen_brightness"), false, (ContentObserver)this);
            contentResolver.registerContentObserver(Settings.System.getUriFor("screen_brightness_mode"), false, (ContentObserver)this);
            contentResolver.registerContentObserver(Settings.System.getUriFor("screen_auto_brightness_adj"), false, (ContentObserver)this);
        }
        
        void stopObserving() {
            this.mContext.getContentResolver().unregisterContentObserver((ContentObserver)this);
        }
    }
    
    private abstract static class StateTracker
    {
        private Boolean mActualState;
        private boolean mDeferredStateChangeRequestNeeded;
        private boolean mInTransition;
        private Boolean mIntendedState;
        
        private StateTracker() {
            this.mInTransition = false;
            this.mActualState = null;
            this.mIntendedState = null;
            this.mDeferredStateChangeRequestNeeded = false;
        }
        
        private final String getContentDescription(final Context context, final int n) {
            return context.getString(2131887711, new Object[] { context.getString(this.getButtonDescription()), context.getString(n) });
        }
        
        public abstract int getActualState(final Context p0);
        
        public abstract int getButtonDescription();
        
        public abstract int getButtonId();
        
        public abstract int getButtonImageId(final boolean p0);
        
        public abstract int getContainerId();
        
        public abstract int getIndicatorId();
        
        public int getPosition() {
            return 1;
        }
        
        public final int getTriState(final Context context) {
            if (this.mInTransition) {
                return 5;
            }
            switch (this.getActualState(context)) {
                default: {
                    return 5;
                }
                case 1: {
                    return 1;
                }
                case 0: {
                    return 0;
                }
            }
        }
        
        public final boolean isTurningOn() {
            return this.mIntendedState != null && this.mIntendedState;
        }
        
        public abstract void onActualStateChange(final Context p0, final Intent p1);
        
        protected abstract void requestStateChange(final Context p0, final boolean p1);
        
        protected final void setCurrentState(final Context context, final int n) {
            final boolean mInTransition = this.mInTransition;
            switch (n) {
                case 3: {
                    this.mInTransition = true;
                    this.mActualState = true;
                    break;
                }
                case 2: {
                    this.mInTransition = true;
                    this.mActualState = false;
                    break;
                }
                case 1: {
                    this.mInTransition = false;
                    this.mActualState = true;
                    break;
                }
                case 0: {
                    this.mInTransition = false;
                    this.mActualState = false;
                    break;
                }
            }
            if (mInTransition && !this.mInTransition && this.mDeferredStateChangeRequestNeeded) {
                Log.v("SettingsAppWidgetProvider", "processing deferred state change");
                if (this.mActualState != null && this.mIntendedState != null && this.mIntendedState.equals(this.mActualState)) {
                    Log.v("SettingsAppWidgetProvider", "... but intended state matches, so no changes.");
                }
                else if (this.mIntendedState != null) {
                    this.mInTransition = true;
                    this.requestStateChange(context, this.mIntendedState);
                }
                this.mDeferredStateChangeRequestNeeded = false;
            }
        }
        
        public final void setImageViewResources(final Context context, final RemoteViews remoteViews) {
            final int containerId = this.getContainerId();
            final int buttonId = this.getButtonId();
            final int indicatorId = this.getIndicatorId();
            final int position = this.getPosition();
            final int triState = this.getTriState(context);
            if (triState != 5) {
                switch (triState) {
                    case 1: {
                        remoteViews.setContentDescription(containerId, (CharSequence)this.getContentDescription(context, 2131887710));
                        remoteViews.setImageViewResource(buttonId, this.getButtonImageId(true));
                        remoteViews.setImageViewResource(indicatorId, SettingsAppWidgetProvider.IND_DRAWABLE_ON[position]);
                        break;
                    }
                    case 0: {
                        remoteViews.setContentDescription(containerId, (CharSequence)this.getContentDescription(context, 2131887709));
                        remoteViews.setImageViewResource(buttonId, this.getButtonImageId(false));
                        remoteViews.setImageViewResource(indicatorId, SettingsAppWidgetProvider.IND_DRAWABLE_OFF[position]);
                        break;
                    }
                }
            }
            else if (this.isTurningOn()) {
                remoteViews.setContentDescription(containerId, (CharSequence)this.getContentDescription(context, 2131887713));
                remoteViews.setImageViewResource(buttonId, this.getButtonImageId(true));
                remoteViews.setImageViewResource(indicatorId, SettingsAppWidgetProvider.IND_DRAWABLE_MID[position]);
            }
            else {
                remoteViews.setContentDescription(containerId, (CharSequence)this.getContentDescription(context, 2131887712));
                remoteViews.setImageViewResource(buttonId, this.getButtonImageId(false));
                remoteViews.setImageViewResource(indicatorId, SettingsAppWidgetProvider.IND_DRAWABLE_OFF[position]);
            }
        }
        
        public final void toggleState(final Context context) {
            final int triState = this.getTriState(context);
            boolean b = false;
            if (triState != 5) {
                switch (triState) {
                    case 1: {
                        b = false;
                        break;
                    }
                    case 0: {
                        b = true;
                        break;
                    }
                }
            }
            else if (this.mIntendedState != null) {
                b = (this.mIntendedState ^ true);
            }
            this.mIntendedState = b;
            if (this.mInTransition) {
                this.mDeferredStateChangeRequestNeeded = true;
            }
            else {
                this.mInTransition = true;
                this.requestStateChange(context, b);
            }
        }
    }
    
    private static final class SyncStateTracker extends StateTracker
    {
        @Override
        public int getActualState(final Context context) {
            return ContentResolver.getMasterSyncAutomatically() ? 1 : 0;
        }
        
        @Override
        public int getButtonDescription() {
            return 2131887714;
        }
        
        @Override
        public int getButtonId() {
            return 2131362256;
        }
        
        @Override
        public int getButtonImageId(final boolean b) {
            int n;
            if (b) {
                n = 2131230964;
            }
            else {
                n = 2131230963;
            }
            return n;
        }
        
        @Override
        public int getContainerId() {
            return 2131361934;
        }
        
        @Override
        public int getIndicatorId() {
            return 2131362264;
        }
        
        @Override
        public void onActualStateChange(final Context context, final Intent intent) {
            ((StateTracker)this).setCurrentState(context, this.getActualState(context));
        }
        
        public void requestStateChange(final Context context, final boolean b) {
            context.getSystemService("connectivity");
            new AsyncTask<Void, Void, Boolean>() {
                final /* synthetic */ boolean val$sync = ContentResolver.getMasterSyncAutomatically();
                
                protected Boolean doInBackground(final Void... array) {
                    if (b) {
                        if (!this.val$sync) {
                            ContentResolver.setMasterSyncAutomatically(true);
                        }
                        return true;
                    }
                    if (this.val$sync) {
                        ContentResolver.setMasterSyncAutomatically(false);
                    }
                    return false;
                }
                
                protected void onPostExecute(final Boolean b) {
                    ((StateTracker)SyncStateTracker.this).setCurrentState(context, ((boolean)b) ? 1 : 0);
                    SettingsAppWidgetProvider.updateWidget(context);
                }
            }.execute((Object[])new Void[0]);
        }
    }
    
    private static final class WifiStateTracker extends StateTracker
    {
        private static int wifiStateToFiveState(final int n) {
            switch (n) {
                default: {
                    return 4;
                }
                case 3: {
                    return 1;
                }
                case 2: {
                    return 2;
                }
                case 1: {
                    return 0;
                }
                case 0: {
                    return 3;
                }
            }
        }
        
        @Override
        public int getActualState(final Context context) {
            final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
            if (wifiManager != null) {
                return wifiStateToFiveState(wifiManager.getWifiState());
            }
            return 4;
        }
        
        @Override
        public int getButtonDescription() {
            return 2131887718;
        }
        
        @Override
        public int getButtonId() {
            return 2131362257;
        }
        
        @Override
        public int getButtonImageId(final boolean b) {
            int n;
            if (b) {
                n = 2131230966;
            }
            else {
                n = 2131230965;
            }
            return n;
        }
        
        @Override
        public int getContainerId() {
            return 2131361935;
        }
        
        @Override
        public int getIndicatorId() {
            return 2131362265;
        }
        
        @Override
        public int getPosition() {
            return 0;
        }
        
        @Override
        public void onActualStateChange(final Context context, final Intent intent) {
            if (!"android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) {
                return;
            }
            ((StateTracker)this).setCurrentState(context, wifiStateToFiveState(intent.getIntExtra("wifi_state", -1)));
        }
        
        @Override
        protected void requestStateChange(final Context context, final boolean b) {
            final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
            if (wifiManager == null) {
                Log.d("SettingsAppWidgetProvider", "No wifiManager.");
                return;
            }
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(final Void... array) {
                    final int wifiApState = wifiManager.getWifiApState();
                    if (b && (wifiApState == 12 || wifiApState == 13)) {
                        ((ConnectivityManager)context.getSystemService("connectivity")).stopTethering(0);
                    }
                    wifiManager.setWifiEnabled(b);
                    return null;
                }
            }.execute((Object[])new Void[0]);
        }
    }
}
