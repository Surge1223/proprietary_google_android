package com.android.settings.widget;

import android.view.Surface;
import android.graphics.SurfaceTexture;
import android.view.TextureView$SurfaceTextureListener;
import android.view.TextureView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.Log;
import android.media.MediaPlayer$OnPreparedListener;
import android.media.MediaPlayer$OnSeekCompleteListener;
import android.net.Uri.Builder;
import com.android.settings.R;
import android.util.AttributeSet;
import android.net.Uri;
import android.media.MediaPlayer;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.view.View;
import android.widget.ImageView;
import android.view.View.OnClickListener;

public final class _$$Lambda$VideoPreference$n3lVCTPDzJxvnNXXv__BWcO0YKM implements View.OnClickListener
{
    public final void onClick(final View view) {
        VideoPreference.lambda$onBindViewHolder$2(this.f$0, this.f$1, view);
    }
}
