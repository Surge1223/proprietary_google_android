package com.android.settings.widget;

import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CheckBox;
import com.android.settingslib.TwoTargetPreference;

public class MasterCheckBoxPreference extends TwoTargetPreference
{
    private CheckBox mCheckBox;
    private boolean mChecked;
    private boolean mEnableCheckBox;
    
    public MasterCheckBoxPreference(final Context context) {
        super(context);
        this.mEnableCheckBox = true;
    }
    
    public MasterCheckBoxPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEnableCheckBox = true;
    }
    
    public MasterCheckBoxPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mEnableCheckBox = true;
    }
    
    public MasterCheckBoxPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mEnableCheckBox = true;
    }
    
    @Override
    protected int getSecondTargetResId() {
        return 2131558682;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(16908312);
        if (viewById != null) {
            viewById.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    if (MasterCheckBoxPreference.this.mCheckBox != null && !MasterCheckBoxPreference.this.mCheckBox.isEnabled()) {
                        return;
                    }
                    MasterCheckBoxPreference.this.setChecked(MasterCheckBoxPreference.this.mChecked ^ true);
                    if (!MasterCheckBoxPreference.this.callChangeListener(MasterCheckBoxPreference.this.mChecked)) {
                        MasterCheckBoxPreference.this.setChecked(MasterCheckBoxPreference.this.mChecked ^ true);
                    }
                    else {
                        Preference.this.persistBoolean(MasterCheckBoxPreference.this.mChecked);
                    }
                }
            });
        }
        this.mCheckBox = (CheckBox)preferenceViewHolder.findViewById(2131361982);
        if (this.mCheckBox != null) {
            this.mCheckBox.setContentDescription(this.getTitle());
            this.mCheckBox.setChecked(this.mChecked);
            this.mCheckBox.setEnabled(this.mEnableCheckBox);
        }
    }
    
    public void setCheckBoxEnabled(final boolean b) {
        this.mEnableCheckBox = b;
        if (this.mCheckBox != null) {
            this.mCheckBox.setEnabled(b);
        }
    }
    
    public void setChecked(final boolean b) {
        this.mChecked = b;
        if (this.mCheckBox != null) {
            this.mCheckBox.setChecked(b);
        }
    }
    
    @Override
    public void setEnabled(final boolean b) {
        super.setEnabled(b);
        this.setCheckBoxEnabled(b);
    }
}
