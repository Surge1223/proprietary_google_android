package com.android.settings.widget;

import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.PreferenceManager;
import android.os.UserHandle;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.RestrictedPreferenceHelper;

public class RestrictedAppPreference extends AppPreference
{
    private RestrictedPreferenceHelper mHelper;
    private String userRestriction;
    
    public RestrictedAppPreference(final Context context, final String s) {
        super(context);
        this.initialize(null, s);
    }
    
    private void initialize(final AttributeSet set, final String userRestriction) {
        this.setWidgetLayoutResource(R.layout.restricted_icon);
        this.mHelper = new RestrictedPreferenceHelper(this.getContext(), this, set);
        this.userRestriction = userRestriction;
    }
    
    public void checkRestrictionAndSetDisabled() {
        if (TextUtils.isEmpty((CharSequence)this.userRestriction)) {
            return;
        }
        this.mHelper.checkRestrictionAndSetDisabled(this.userRestriction, UserHandle.myUserId());
    }
    
    public boolean isDisabledByAdmin() {
        return this.mHelper.isDisabledByAdmin();
    }
    
    @Override
    protected void onAttachedToHierarchy(final PreferenceManager preferenceManager) {
        this.mHelper.onAttachedToHierarchy();
        super.onAttachedToHierarchy(preferenceManager);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.mHelper.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(R.id.restricted_icon);
        if (viewById != null) {
            int visibility;
            if (this.isDisabledByAdmin()) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
        }
    }
    
    @Override
    public void performClick() {
        if (!this.mHelper.performClick()) {
            super.performClick();
        }
    }
    
    @Override
    public void setEnabled(final boolean enabled) {
        if (this.isDisabledByAdmin() && enabled) {
            return;
        }
        super.setEnabled(enabled);
    }
}
