package com.android.settings.widget;

import com.android.settingslib.RestrictedLockUtils;
import android.support.v7.preference.Preference;

public class MasterSwitchController extends SwitchWidgetController implements OnPreferenceChangeListener
{
    private final MasterSwitchPreference mPreference;
    
    public MasterSwitchController(final MasterSwitchPreference mPreference) {
        this.mPreference = mPreference;
    }
    
    @Override
    public boolean isChecked() {
        return this.mPreference.isChecked();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return this.mListener != null && this.mListener.onSwitchToggled((boolean)o);
    }
    
    @Override
    public void setChecked(final boolean checked) {
        this.mPreference.setChecked(checked);
    }
    
    @Override
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin disabledByAdmin) {
        this.mPreference.setDisabledByAdmin(disabledByAdmin);
    }
    
    @Override
    public void setEnabled(final boolean switchEnabled) {
        this.mPreference.setSwitchEnabled(switchEnabled);
    }
    
    @Override
    public void startListening() {
        this.mPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
    }
    
    @Override
    public void stopListening() {
        this.mPreference.setOnPreferenceChangeListener(null);
    }
    
    @Override
    public void updateTitle(final boolean b) {
    }
}
