package com.android.settings.widget;

import java.util.Arrays;
import android.text.format.Time;
import java.util.Calendar;
import java.util.Objects;
import android.text.format.Formatter$BytesResult;
import android.text.format.Formatter;
import android.util.MathUtils;
import android.content.res.Resources;
import android.view.MotionEvent;
import android.text.TextUtils;
import android.text.SpannableStringBuilder;
import android.os.Message;
import android.util.AttributeSet;
import android.content.Context;
import android.os.Handler;

public class ChartDataUsageView extends ChartView
{
    private ChartNetworkSeriesView mDetailSeries;
    private ChartGridView mGrid;
    private Handler mHandler;
    private long mInspectEnd;
    private long mInspectStart;
    private DataUsageChartListener mListener;
    private ChartNetworkSeriesView mSeries;
    private ChartSweepView mSweepLimit;
    private ChartSweepView mSweepWarning;
    private ChartSweepView.OnSweepListener mVertListener;
    private long mVertMax;
    
    public ChartDataUsageView(final Context context) {
        this(context, null, 0);
    }
    
    public ChartDataUsageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChartDataUsageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mVertListener = new ChartSweepView.OnSweepListener() {
            @Override
            public void onSweep(final ChartSweepView chartSweepView, final boolean b) {
                if (b) {
                    ChartDataUsageView.this.clearUpdateAxisDelayed(chartSweepView);
                    ChartDataUsageView.this.updateEstimateVisible();
                    if (chartSweepView == ChartDataUsageView.this.mSweepWarning && ChartDataUsageView.this.mListener != null) {
                        ChartDataUsageView.this.mListener.onWarningChanged();
                    }
                    else if (chartSweepView == ChartDataUsageView.this.mSweepLimit && ChartDataUsageView.this.mListener != null) {
                        ChartDataUsageView.this.mListener.onLimitChanged();
                    }
                }
                else {
                    ChartDataUsageView.this.sendUpdateAxisDelayed(chartSweepView, false);
                }
            }
            
            @Override
            public void requestEdit(final ChartSweepView chartSweepView) {
                if (chartSweepView == ChartDataUsageView.this.mSweepWarning && ChartDataUsageView.this.mListener != null) {
                    ChartDataUsageView.this.mListener.requestWarningEdit();
                }
                else if (chartSweepView == ChartDataUsageView.this.mSweepLimit && ChartDataUsageView.this.mListener != null) {
                    ChartDataUsageView.this.mListener.requestLimitEdit();
                }
            }
        };
        this.init(new TimeAxis(), new InvertedChartAxis(new DataAxis()));
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                final ChartSweepView chartSweepView = (ChartSweepView)message.obj;
                ChartDataUsageView.this.updateVertAxisBounds(chartSweepView);
                ChartDataUsageView.this.updateEstimateVisible();
                ChartDataUsageView.this.sendUpdateAxisDelayed(chartSweepView, true);
            }
        };
    }
    
    private void clearUpdateAxisDelayed(final ChartSweepView chartSweepView) {
        this.mHandler.removeMessages(100, (Object)chartSweepView);
    }
    
    private static long roundUpToPowerOfTwo(long n) {
        --n;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        n = (n | n >>> 32) + 1L;
        if (n <= 0L) {
            n = Long.MAX_VALUE;
        }
        return n;
    }
    
    private void sendUpdateAxisDelayed(final ChartSweepView chartSweepView, final boolean b) {
        if (b || !this.mHandler.hasMessages(100, (Object)chartSweepView)) {
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(100, (Object)chartSweepView), 250L);
        }
    }
    
    private static void setText(final SpannableStringBuilder spannableStringBuilder, final Object o, final CharSequence charSequence, final String s) {
        final int spanStart = spannableStringBuilder.getSpanStart(o);
        int spanEnd = spannableStringBuilder.getSpanEnd(o);
        int index = spanStart;
        if (spanStart == -1) {
            index = TextUtils.indexOf((CharSequence)spannableStringBuilder, (CharSequence)s);
            spanEnd = index + s.length();
            spannableStringBuilder.setSpan(o, index, spanEnd, 18);
        }
        spannableStringBuilder.replace(index, spanEnd, charSequence);
    }
    
    private void updateEstimateVisible() {
        final long maxEstimate = this.mSeries.getMaxEstimate();
        long n = Long.MAX_VALUE;
        if (this.mSweepWarning.isEnabled()) {
            n = this.mSweepWarning.getValue();
        }
        else if (this.mSweepLimit.isEnabled()) {
            n = this.mSweepLimit.getValue();
        }
        long n2 = n;
        if (n < 0L) {
            n2 = Long.MAX_VALUE;
        }
        this.mSeries.setEstimateVisible(maxEstimate >= 7L * n2 / 10L);
    }
    
    private void updateVertAxisBounds(final ChartSweepView chartSweepView) {
        final long mVertMax = this.mVertMax;
        long n = 0L;
        if (chartSweepView != null) {
            final int shouldAdjustAxis = chartSweepView.shouldAdjustAxis();
            if (shouldAdjustAxis > 0) {
                n = 11L * mVertMax / 10L;
            }
            else if (shouldAdjustAxis < 0) {
                n = 9L * mVertMax / 10L;
            }
            else {
                n = mVertMax;
            }
        }
        final long max = Math.max(Math.max(Math.max(Math.max(this.mSeries.getMaxVisible(), this.mDetailSeries.getMaxVisible()), Math.max(this.mSweepWarning.getValue(), this.mSweepLimit.getValue())) * 12L / 10L, 52428800L), n);
        if (max != this.mVertMax) {
            this.mVertMax = max;
            final boolean setBounds = this.mVert.setBounds(0L, max);
            this.mSweepWarning.setValidRange(0L, max);
            this.mSweepLimit.setValidRange(0L, max);
            if (setBounds) {
                this.mSeries.invalidatePath();
                this.mDetailSeries.invalidatePath();
            }
            this.mGrid.invalidate();
            if (chartSweepView != null) {
                chartSweepView.updateValueFromPosition();
            }
            if (this.mSweepLimit != chartSweepView) {
                this.layoutSweep(this.mSweepLimit);
            }
            if (this.mSweepWarning != chartSweepView) {
                this.layoutSweep(this.mSweepWarning);
            }
        }
    }
    
    public long getInspectEnd() {
        return this.mInspectEnd;
    }
    
    public long getInspectStart() {
        return this.mInspectStart;
    }
    
    public long getLimitBytes() {
        return this.mSweepLimit.getLabelValue();
    }
    
    public long getWarningBytes() {
        return this.mSweepWarning.getLabelValue();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mGrid = (ChartGridView)this.findViewById(2131362180);
        this.mSeries = (ChartNetworkSeriesView)this.findViewById(2131362588);
        (this.mDetailSeries = (ChartNetworkSeriesView)this.findViewById(2131362069)).setVisibility(8);
        this.mSweepLimit = (ChartSweepView)this.findViewById(2131362703);
        (this.mSweepWarning = (ChartSweepView)this.findViewById(2131362704)).setValidRangeDynamic(null, this.mSweepLimit);
        this.mSweepLimit.setValidRangeDynamic(this.mSweepWarning, null);
        this.mSweepLimit.setNeighbors(this.mSweepWarning);
        this.mSweepWarning.setNeighbors(this.mSweepLimit);
        this.mSweepWarning.addOnSweepListener(this.mVertListener);
        this.mSweepLimit.addOnSweepListener(this.mVertListener);
        this.mSweepWarning.setDragInterval(5242880L);
        this.mSweepLimit.setDragInterval(5242880L);
        this.mGrid.init(this.mHoriz, this.mVert);
        this.mSeries.init(this.mHoriz, this.mVert);
        this.mDetailSeries.init(this.mHoriz, this.mVert);
        this.mSweepWarning.init(this.mVert);
        this.mSweepLimit.init(this.mVert);
        this.setActivated(false);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.isActivated()) {
            return false;
        }
        switch (motionEvent.getAction()) {
            default: {
                return false;
            }
            case 1: {
                this.setActivated(true);
                return true;
            }
            case 0: {
                return true;
            }
        }
    }
    
    public void setListener(final DataUsageChartListener mListener) {
        this.mListener = mListener;
    }
    
    public static class DataAxis implements ChartAxis
    {
        private static final Object sSpanSize;
        private static final Object sSpanUnit;
        private long mMax;
        private long mMin;
        private float mSize;
        
        static {
            sSpanSize = new Object();
            sSpanUnit = new Object();
        }
        
        @Override
        public long buildLabel(final Resources resources, final SpannableStringBuilder spannableStringBuilder, final long n) {
            final Formatter$BytesResult formatBytes = Formatter.formatBytes(resources, MathUtils.constrain(n, 0L, 1099511627776L), 3);
            setText(spannableStringBuilder, DataAxis.sSpanSize, formatBytes.value, "^1");
            setText(spannableStringBuilder, DataAxis.sSpanUnit, formatBytes.units, "^2");
            return formatBytes.roundedBytes;
        }
        
        @Override
        public float convertToPoint(final long n) {
            return this.mSize * (n - this.mMin) / (this.mMax - this.mMin);
        }
        
        @Override
        public long convertToValue(final float n) {
            return (long)(this.mMin + (this.mMax - this.mMin) * n / this.mSize);
        }
        
        @Override
        public float[] getTickPoints() {
            final long n = this.mMax - this.mMin;
            final long access$800 = roundUpToPowerOfTwo(n / 16L);
            final float[] array = new float[(int)(n / access$800)];
            long mMin = this.mMin;
            for (int i = 0; i < array.length; ++i) {
                array[i] = this.convertToPoint(mMin);
                mMin += access$800;
            }
            return array;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(this.mMin, this.mMax, this.mSize);
        }
        
        @Override
        public boolean setBounds(final long mMin, final long mMax) {
            if (this.mMin == mMin && this.mMax == mMax) {
                return false;
            }
            this.mMin = mMin;
            this.mMax = mMax;
            return true;
        }
        
        @Override
        public boolean setSize(final float mSize) {
            if (this.mSize != mSize) {
                this.mSize = mSize;
                return true;
            }
            return false;
        }
        
        @Override
        public int shouldAdjustAxis(final long n) {
            final float convertToPoint = this.convertToPoint(n);
            if (convertToPoint < this.mSize * 0.1) {
                return -1;
            }
            if (convertToPoint > this.mSize * 0.85) {
                return 1;
            }
            return 0;
        }
    }
    
    public interface DataUsageChartListener
    {
        void onLimitChanged();
        
        void onWarningChanged();
        
        void requestLimitEdit();
        
        void requestWarningEdit();
    }
    
    public static class TimeAxis implements ChartAxis
    {
        private static final int FIRST_DAY_OF_WEEK;
        private long mMax;
        private long mMin;
        private float mSize;
        
        static {
            FIRST_DAY_OF_WEEK = Calendar.getInstance().getFirstDayOfWeek() - 1;
        }
        
        public TimeAxis() {
            final long currentTimeMillis = System.currentTimeMillis();
            this.setBounds(currentTimeMillis - 2592000000L, currentTimeMillis);
        }
        
        @Override
        public long buildLabel(final Resources resources, final SpannableStringBuilder spannableStringBuilder, final long n) {
            spannableStringBuilder.replace(0, spannableStringBuilder.length(), (CharSequence)Long.toString(n));
            return n;
        }
        
        @Override
        public float convertToPoint(final long n) {
            return this.mSize * (n - this.mMin) / (this.mMax - this.mMin);
        }
        
        @Override
        public long convertToValue(final float n) {
            return (long)(this.mMin + (this.mMax - this.mMin) * n / this.mSize);
        }
        
        @Override
        public float[] getTickPoints() {
            final float[] array = new float[32];
            int n = 0;
            final Time time = new Time();
            time.set(this.mMax);
            time.monthDay -= time.weekDay - TimeAxis.FIRST_DAY_OF_WEEK;
            time.second = 0;
            time.minute = 0;
            time.hour = 0;
            time.normalize(true);
            int n3;
            for (long n2 = time.toMillis(true); n2 > this.mMin; n2 = time.toMillis(true), n = n3) {
                n3 = n;
                if (n2 <= this.mMax) {
                    array[n] = this.convertToPoint(n2);
                    n3 = n + 1;
                }
                time.monthDay -= 7;
                time.normalize(true);
            }
            return Arrays.copyOf(array, n);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(this.mMin, this.mMax, this.mSize);
        }
        
        @Override
        public boolean setBounds(final long mMin, final long mMax) {
            if (this.mMin == mMin && this.mMax == mMax) {
                return false;
            }
            this.mMin = mMin;
            this.mMax = mMax;
            return true;
        }
        
        @Override
        public boolean setSize(final float mSize) {
            if (this.mSize != mSize) {
                this.mSize = mSize;
                return true;
            }
            return false;
        }
        
        @Override
        public int shouldAdjustAxis(final long n) {
            return 0;
        }
    }
}
