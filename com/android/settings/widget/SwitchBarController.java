package com.android.settings.widget;

import com.android.settingslib.RestrictedLockUtils;
import android.widget.Switch;

public class SwitchBarController extends SwitchWidgetController implements SwitchBar.OnSwitchChangeListener
{
    private final SwitchBar mSwitchBar;
    
    public SwitchBarController(final SwitchBar mSwitchBar) {
        this.mSwitchBar = mSwitchBar;
    }
    
    @Override
    public boolean isChecked() {
        return this.mSwitchBar.isChecked();
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        if (this.mListener != null) {
            this.mListener.onSwitchToggled(b);
        }
    }
    
    @Override
    public void setChecked(final boolean checked) {
        this.mSwitchBar.setChecked(checked);
    }
    
    @Override
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin disabledByAdmin) {
        this.mSwitchBar.setDisabledByAdmin(disabledByAdmin);
    }
    
    @Override
    public void setEnabled(final boolean enabled) {
        this.mSwitchBar.setEnabled(enabled);
    }
    
    @Override
    public void setupView() {
        this.mSwitchBar.show();
    }
    
    @Override
    public void startListening() {
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public void stopListening() {
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
    }
    
    @Override
    public void teardownView() {
        this.mSwitchBar.hide();
    }
    
    @Override
    public void updateTitle(final boolean textViewLabelAndBackground) {
        this.mSwitchBar.setTextViewLabelAndBackground(textViewLabelAndBackground);
    }
}
