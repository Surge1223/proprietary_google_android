package com.android.settings.widget;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.view.View.BaseSavedState;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Locale;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v4.view.ViewPager;

public final class RtlCompatibleViewPager extends ViewPager
{
    public RtlCompatibleViewPager(final Context context) {
        this(context, null);
    }
    
    public RtlCompatibleViewPager(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public int getCurrentItem() {
        return this.getRtlAwareIndex(super.getCurrentItem());
    }
    
    public int getRtlAwareIndex(final int n) {
        if (TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) == 1) {
            return this.getAdapter().getCount() - n - 1;
        }
        return n;
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        final RtlSavedState rtlSavedState = (RtlSavedState)parcelable;
        super.onRestoreInstanceState(rtlSavedState.getSuperState());
        this.setCurrentItem(rtlSavedState.position);
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        final RtlSavedState rtlSavedState = new RtlSavedState(super.onSaveInstanceState());
        rtlSavedState.position = this.getCurrentItem();
        return (Parcelable)rtlSavedState;
    }
    
    @Override
    public void setCurrentItem(final int n) {
        super.setCurrentItem(this.getRtlAwareIndex(n));
    }
    
    static class RtlSavedState extends View.BaseSavedState
    {
        public static final Parcelable$ClassLoaderCreator<RtlSavedState> CREATOR;
        int position;
        
        static {
            CREATOR = (Parcelable$ClassLoaderCreator)new Parcelable$ClassLoaderCreator<RtlSavedState>() {
                public RtlSavedState createFromParcel(final Parcel parcel) {
                    return new RtlSavedState(parcel, (ClassLoader)null);
                }
                
                public RtlSavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    return new RtlSavedState(parcel, classLoader);
                }
                
                public RtlSavedState[] newArray(final int n) {
                    return new RtlSavedState[n];
                }
            };
        }
        
        private RtlSavedState(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.position = parcel.readInt();
        }
        
        public RtlSavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.position);
        }
    }
}
