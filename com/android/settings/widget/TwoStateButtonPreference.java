package com.android.settings.widget;

import android.view.View;
import android.content.res.TypedArray;
import com.android.settings.R;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Button;
import android.view.View.OnClickListener;
import com.android.settings.applications.LayoutPreference;

public class TwoStateButtonPreference extends LayoutPreference implements View.OnClickListener
{
    private final Button mButtonOff;
    private final Button mButtonOn;
    private boolean mIsChecked;
    
    public TwoStateButtonPreference(final Context context, final AttributeSet set) {
        super(context, set, TypedArrayUtils.getAttr(context, 2130969100, 16842894));
        if (set == null) {
            this.mButtonOn = null;
            this.mButtonOff = null;
        }
        else {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.TwoStateButtonPreference);
            final int resourceId = obtainStyledAttributes.getResourceId(1, 2131889405);
            final int resourceId2 = obtainStyledAttributes.getResourceId(0, 2131889405);
            obtainStyledAttributes.recycle();
            (this.mButtonOn = this.findViewById(2131362643)).setText(resourceId);
            this.mButtonOn.setOnClickListener((View.OnClickListener)this);
            (this.mButtonOff = this.findViewById(2131362642)).setText(resourceId2);
            this.mButtonOff.setOnClickListener((View.OnClickListener)this);
            this.setChecked(this.isChecked());
        }
    }
    
    public Button getStateOffButton() {
        return this.mButtonOff;
    }
    
    public Button getStateOnButton() {
        return this.mButtonOn;
    }
    
    public boolean isChecked() {
        return this.mIsChecked;
    }
    
    public void onClick(final View view) {
        final boolean checked = view.getId() == 2131362643;
        this.setChecked(checked);
        this.callChangeListener(checked);
    }
    
    public void setButtonEnabled(final boolean b) {
        this.mButtonOn.setEnabled(b);
        this.mButtonOff.setEnabled(b);
    }
    
    public void setChecked(final boolean mIsChecked) {
        this.mIsChecked = mIsChecked;
        if (mIsChecked) {
            this.mButtonOn.setVisibility(8);
            this.mButtonOff.setVisibility(0);
        }
        else {
            this.mButtonOn.setVisibility(0);
            this.mButtonOff.setVisibility(8);
        }
    }
}
