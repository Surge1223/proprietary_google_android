package com.android.settings.widget;

import android.content.res.TypedArray;
import android.widget.ListAdapter;
import java.util.List;
import android.util.AttributeSet;
import com.android.internal.R$styleable;
import android.app.AlertDialog$Builder;
import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.support.v14.preference.PreferenceDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$UpdatableListPreferenceDialogFragment$yZRmvmWflT3ytJ4m_nzXQtpejcQ implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        UpdatableListPreferenceDialogFragment.lambda$onPrepareDialogBuilder$0(this.f$0, dialogInterface, n);
    }
}
