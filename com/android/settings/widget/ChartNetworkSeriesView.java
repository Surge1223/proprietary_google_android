package com.android.settings.widget;

import android.graphics.PathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint.Style;
import android.graphics.Canvas;
import com.android.internal.util.Preconditions;
import android.net.NetworkStatsHistory$Entry;
import android.content.res.TypedArray;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.net.NetworkStatsHistory;
import android.graphics.Path;
import android.graphics.Paint;
import android.view.View;

public class ChartNetworkSeriesView extends View
{
    private long mEnd;
    private long mEndTime;
    private boolean mEstimateVisible;
    private ChartAxis mHoriz;
    private long mMax;
    private long mMaxEstimate;
    private Paint mPaintEstimate;
    private Paint mPaintFill;
    private Paint mPaintFillSecondary;
    private Paint mPaintStroke;
    private Path mPathEstimate;
    private Path mPathFill;
    private Path mPathStroke;
    private boolean mPathValid;
    private int mSafeRegion;
    private boolean mSecondary;
    private long mStart;
    private NetworkStatsHistory mStats;
    private ChartAxis mVert;
    
    public ChartNetworkSeriesView(final Context context) {
        this(context, null, 0);
    }
    
    public ChartNetworkSeriesView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChartNetworkSeriesView(final Context context, final AttributeSet set, int color) {
        super(context, set, color);
        this.mEndTime = Long.MIN_VALUE;
        this.mPathValid = false;
        this.mEstimateVisible = false;
        this.mSecondary = false;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ChartNetworkSeriesView, color, 0);
        final int color2 = obtainStyledAttributes.getColor(3, -65536);
        color = obtainStyledAttributes.getColor(0, -65536);
        final int color3 = obtainStyledAttributes.getColor(1, -65536);
        final int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.setChartColor(color2, color, color3);
        this.setSafeRegion(dimensionPixelSize);
        this.setWillNotDraw(false);
        obtainStyledAttributes.recycle();
        this.mPathStroke = new Path();
        this.mPathFill = new Path();
        this.mPathEstimate = new Path();
    }
    
    private void generatePath() {
        this.mMax = 0L;
        this.mPathStroke.reset();
        this.mPathFill.reset();
        this.mPathEstimate.reset();
        this.mPathValid = true;
        if (this.mStats != null && this.mStats.size() >= 2) {
            this.getWidth();
            final int height = this.getHeight();
            float n = height;
            long convertToValue = this.mHoriz.convertToValue(0.0f);
            this.mPathStroke.moveTo(0.0f, n);
            this.mPathFill.moveTo(0.0f, n);
            long mMax = 0L;
            NetworkStatsHistory$Entry values = null;
            final int indexBefore = this.mStats.getIndexBefore(this.mStart);
            final int indexAfter = this.mStats.getIndexAfter(this.mEnd);
            float convertToPoint = 0.0f;
            for (int i = indexBefore; i <= indexAfter; ++i) {
                values = this.mStats.getValues(i, values);
                final long bucketStart = values.bucketStart;
                final long n2 = values.bucketDuration + bucketStart;
                final float convertToPoint2 = this.mHoriz.convertToPoint(bucketStart);
                final float convertToPoint3 = this.mHoriz.convertToPoint(n2);
                if (convertToPoint3 >= 0.0f) {
                    mMax += values.rxBytes + values.txBytes;
                    final float convertToPoint4 = this.mVert.convertToPoint(mMax);
                    if (convertToValue != bucketStart) {
                        this.mPathStroke.lineTo(convertToPoint2, n);
                        this.mPathFill.lineTo(convertToPoint2, n);
                    }
                    this.mPathStroke.lineTo(convertToPoint3, convertToPoint4);
                    this.mPathFill.lineTo(convertToPoint3, convertToPoint4);
                    n = convertToPoint4;
                    convertToValue = n2;
                    convertToPoint = convertToPoint3;
                }
            }
            if (convertToValue < this.mEndTime) {
                convertToPoint = this.mHoriz.convertToPoint(this.mEndTime);
                this.mPathStroke.lineTo(convertToPoint, n);
                this.mPathFill.lineTo(convertToPoint, n);
            }
            this.mPathFill.lineTo(convertToPoint, (float)height);
            this.mPathFill.lineTo(0.0f, (float)height);
            this.mMax = mMax;
            this.invalidate();
        }
    }
    
    public long getMaxEstimate() {
        return this.mMaxEstimate;
    }
    
    public long getMaxVisible() {
        long n;
        if (this.mEstimateVisible) {
            n = this.mMaxEstimate;
        }
        else {
            n = this.mMax;
        }
        if (n <= 0L && this.mStats != null) {
            final NetworkStatsHistory$Entry values = this.mStats.getValues(this.mStart, this.mEnd, (NetworkStatsHistory$Entry)null);
            return values.rxBytes + values.txBytes;
        }
        return n;
    }
    
    void init(final ChartAxis chartAxis, final ChartAxis chartAxis2) {
        this.mHoriz = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis, (Object)"missing horiz");
        this.mVert = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis2, (Object)"missing vert");
    }
    
    public void invalidatePath() {
        this.mPathValid = false;
        this.mMax = 0L;
        this.invalidate();
    }
    
    protected void onDraw(final Canvas canvas) {
        if (!this.mPathValid) {
            this.generatePath();
        }
        if (this.mEstimateVisible) {
            final int save = canvas.save();
            canvas.clipRect(0, 0, this.getWidth(), this.getHeight());
            canvas.drawPath(this.mPathEstimate, this.mPaintEstimate);
            canvas.restoreToCount(save);
        }
        Paint paint;
        if (this.mSecondary) {
            paint = this.mPaintFillSecondary;
        }
        else {
            paint = this.mPaintFill;
        }
        final int save2 = canvas.save();
        canvas.clipRect(this.mSafeRegion, 0, this.getWidth(), this.getHeight() - this.mSafeRegion);
        canvas.drawPath(this.mPathFill, paint);
        canvas.restoreToCount(save2);
    }
    
    public void setChartColor(final int color, final int color2, final int n) {
        (this.mPaintStroke = new Paint()).setStrokeWidth(4.0f * this.getResources().getDisplayMetrics().density);
        this.mPaintStroke.setColor(color);
        this.mPaintStroke.setStyle(Paint.Style.STROKE);
        this.mPaintStroke.setAntiAlias(true);
        (this.mPaintFill = new Paint()).setColor(color2);
        this.mPaintFill.setStyle(Paint.Style.FILL);
        this.mPaintFill.setAntiAlias(true);
        (this.mPaintFillSecondary = new Paint()).setColor(n);
        this.mPaintFillSecondary.setStyle(Paint.Style.FILL);
        this.mPaintFillSecondary.setAntiAlias(true);
        (this.mPaintEstimate = new Paint()).setStrokeWidth(3.0f);
        this.mPaintEstimate.setColor(n);
        this.mPaintEstimate.setStyle(Paint.Style.STROKE);
        this.mPaintEstimate.setAntiAlias(true);
        this.mPaintEstimate.setPathEffect((PathEffect)new DashPathEffect(new float[] { 10.0f, 10.0f }, 1.0f));
    }
    
    public void setEndTime(final long mEndTime) {
        this.mEndTime = mEndTime;
    }
    
    public void setEstimateVisible(final boolean b) {
        this.mEstimateVisible = false;
        this.invalidate();
    }
    
    public void setSafeRegion(final int mSafeRegion) {
        this.mSafeRegion = mSafeRegion;
    }
    
    public void setSecondary(final boolean mSecondary) {
        this.mSecondary = mSecondary;
    }
}
