package com.android.settings.widget;

import android.support.v7.preference.PreferenceViewHolder;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v7.widget.RecyclerView;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import com.android.settings.SettingsPreferenceFragment;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.animation.ArgbEvaluator;
import android.util.Log;
import android.view.View;
import android.content.Context;
import android.util.TypedValue;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceGroupAdapter;

public class HighlightablePreferenceGroupAdapter extends PreferenceGroupAdapter
{
    static final long DELAY_HIGHLIGHT_DURATION_MILLIS = 600L;
    boolean mFadeInAnimated;
    final int mHighlightColor;
    private final String mHighlightKey;
    private int mHighlightPosition;
    private boolean mHighlightRequested;
    private final int mNormalBackgroundRes;
    
    public HighlightablePreferenceGroupAdapter(final PreferenceGroup preferenceGroup, final String mHighlightKey, final boolean mHighlightRequested) {
        super(preferenceGroup);
        this.mHighlightPosition = -1;
        this.mHighlightKey = mHighlightKey;
        this.mHighlightRequested = mHighlightRequested;
        final Context context = preferenceGroup.getContext();
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843534, typedValue, true);
        this.mNormalBackgroundRes = typedValue.resourceId;
        this.mHighlightColor = context.getColor(2131099818);
    }
    
    private void addHighlightBackground(final View view, final boolean b) {
        view.setTag(2131362460, (Object)true);
        if (!b) {
            view.setBackgroundColor(this.mHighlightColor);
            Log.d("HighlightableAdapter", "AddHighlight: Not animation requested - setting highlight background");
            this.requestRemoveHighlightDelayed(view);
            return;
        }
        this.mFadeInAnimated = true;
        final ValueAnimator ofObject = ValueAnimator.ofObject((TypeEvaluator)new ArgbEvaluator(), new Object[] { -1, this.mHighlightColor });
        ofObject.setDuration(200L);
        ofObject.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new _$$Lambda$HighlightablePreferenceGroupAdapter$piymLpeUf2m74Yz5ep7jpdxw2ho(view));
        ofObject.setRepeatMode(2);
        ofObject.setRepeatCount(4);
        ofObject.start();
        Log.d("HighlightableAdapter", "AddHighlight: starting fade in animation");
        this.requestRemoveHighlightDelayed(view);
    }
    
    public static void adjustInitialExpandedChildCount(final SettingsPreferenceFragment settingsPreferenceFragment) {
        if (settingsPreferenceFragment == null) {
            return;
        }
        final PreferenceScreen preferenceScreen = settingsPreferenceFragment.getPreferenceScreen();
        if (preferenceScreen == null) {
            return;
        }
        final Bundle arguments = settingsPreferenceFragment.getArguments();
        if (arguments != null && !TextUtils.isEmpty((CharSequence)arguments.getString(":settings:fragment_args_key"))) {
            preferenceScreen.setInitialExpandedChildrenCount(Integer.MAX_VALUE);
            return;
        }
        final int initialExpandedChildCount = settingsPreferenceFragment.getInitialExpandedChildCount();
        if (initialExpandedChildCount <= 0) {
            return;
        }
        preferenceScreen.setInitialExpandedChildrenCount(initialExpandedChildCount);
    }
    
    private void removeHighlightBackground(final View view, final boolean b) {
        if (!b) {
            view.setTag(2131362460, (Object)false);
            view.setBackgroundResource(this.mNormalBackgroundRes);
            Log.d("HighlightableAdapter", "RemoveHighlight: No animation requested - setting normal background");
            return;
        }
        if (!Boolean.TRUE.equals(view.getTag(2131362460))) {
            Log.d("HighlightableAdapter", "RemoveHighlight: Not highlighted - skipping");
            return;
        }
        final int mHighlightColor = this.mHighlightColor;
        view.setTag(2131362460, (Object)false);
        final ValueAnimator ofObject = ValueAnimator.ofObject((TypeEvaluator)new ArgbEvaluator(), new Object[] { mHighlightColor, -1 });
        ofObject.setDuration(500L);
        ofObject.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new _$$Lambda$HighlightablePreferenceGroupAdapter$HMY634RMu5R2WoggcFMdrEe8uA0(view));
        ofObject.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            public void onAnimationEnd(final Animator animator) {
                view.setBackgroundResource(HighlightablePreferenceGroupAdapter.this.mNormalBackgroundRes);
            }
        });
        ofObject.start();
        Log.d("HighlightableAdapter", "Starting fade out animation");
    }
    
    public boolean isHighlightRequested() {
        return this.mHighlightRequested;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder, final int n) {
        super.onBindViewHolder(preferenceViewHolder, n);
        this.updateBackground(preferenceViewHolder, n);
    }
    
    public void requestHighlight(final View view, final RecyclerView recyclerView) {
        if (!this.mHighlightRequested && recyclerView != null && !TextUtils.isEmpty((CharSequence)this.mHighlightKey)) {
            view.postDelayed((Runnable)new _$$Lambda$HighlightablePreferenceGroupAdapter$Xc5BA2nCks8YuSzn7LsPZS7EmPA(this, recyclerView), 600L);
        }
    }
    
    void requestRemoveHighlightDelayed(final View view) {
        view.postDelayed((Runnable)new _$$Lambda$HighlightablePreferenceGroupAdapter$CKVsKq8Jy7vb9RpitMwq8ps1ehE(this, view), 15000L);
    }
    
    void updateBackground(final PreferenceViewHolder preferenceViewHolder, final int n) {
        final View itemView = preferenceViewHolder.itemView;
        if (n == this.mHighlightPosition) {
            this.addHighlightBackground(itemView, this.mFadeInAnimated ^ true);
        }
        else if (Boolean.TRUE.equals(itemView.getTag(2131362460))) {
            this.removeHighlightBackground(itemView, false);
        }
    }
}
