package com.android.settings.widget;

import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.accessibility.AccessibilityEvent;
import android.os.Bundle;
import java.util.List;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.view.View;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.SeekBar$OnSeekBarChangeListener;
import android.support.v4.widget.ExploreByTouchHelper;
import android.widget.SeekBar;

public class LabeledSeekBar extends SeekBar
{
    private final ExploreByTouchHelper mAccessHelper;
    private String[] mLabels;
    private SeekBar$OnSeekBarChangeListener mOnSeekBarChangeListener;
    private final SeekBar$OnSeekBarChangeListener mProxySeekBarListener;
    
    public LabeledSeekBar(final Context context, final AttributeSet set) {
        this(context, set, 16842875);
    }
    
    public LabeledSeekBar(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public LabeledSeekBar(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mProxySeekBarListener = (SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener() {
            public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
                if (LabeledSeekBar.this.mOnSeekBarChangeListener != null) {
                    LabeledSeekBar.this.mOnSeekBarChangeListener.onProgressChanged(seekBar, n, b);
                    LabeledSeekBar.this.sendClickEventForAccessibility(n);
                }
            }
            
            public void onStartTrackingTouch(final SeekBar seekBar) {
                if (LabeledSeekBar.this.mOnSeekBarChangeListener != null) {
                    LabeledSeekBar.this.mOnSeekBarChangeListener.onStartTrackingTouch(seekBar);
                }
            }
            
            public void onStopTrackingTouch(final SeekBar seekBar) {
                if (LabeledSeekBar.this.mOnSeekBarChangeListener != null) {
                    LabeledSeekBar.this.mOnSeekBarChangeListener.onStopTrackingTouch(seekBar);
                }
            }
        };
        ViewCompat.setAccessibilityDelegate((View)this, this.mAccessHelper = new LabeledSeekBarExploreByTouchHelper(this));
        super.setOnSeekBarChangeListener(this.mProxySeekBarListener);
    }
    
    private void sendClickEventForAccessibility(final int n) {
        this.mAccessHelper.invalidateRoot();
        this.mAccessHelper.sendEventForVirtualView(n, 1);
    }
    
    protected boolean dispatchHoverEvent(final MotionEvent motionEvent) {
        return this.mAccessHelper.dispatchHoverEvent(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }
    
    public void setLabels(final String[] mLabels) {
        this.mLabels = mLabels;
    }
    
    public void setOnSeekBarChangeListener(final SeekBar$OnSeekBarChangeListener mOnSeekBarChangeListener) {
        this.mOnSeekBarChangeListener = mOnSeekBarChangeListener;
    }
    
    public void setProgress(final int progress) {
        synchronized (this) {
            if (this.mAccessHelper != null) {
                this.mAccessHelper.invalidateRoot();
            }
            super.setProgress(progress);
        }
    }
    
    private class LabeledSeekBarExploreByTouchHelper extends ExploreByTouchHelper
    {
        private boolean mIsLayoutRtl;
        
        public LabeledSeekBarExploreByTouchHelper(final LabeledSeekBar labeledSeekBar) {
            super((View)labeledSeekBar);
            final int layoutDirection = labeledSeekBar.getResources().getConfiguration().getLayoutDirection();
            boolean mIsLayoutRtl = true;
            if (layoutDirection != 1) {
                mIsLayoutRtl = false;
            }
            this.mIsLayoutRtl = mIsLayoutRtl;
        }
        
        private Rect getBoundsInParentFromVirtualViewId(int width) {
            if (this.mIsLayoutRtl) {
                width = LabeledSeekBar.this.getMax() - width;
            }
            final int halfVirtualViewWidth = this.getHalfVirtualViewWidth();
            final int paddingStart = LabeledSeekBar.this.getPaddingStart();
            final int halfVirtualViewWidth2 = this.getHalfVirtualViewWidth();
            final int paddingStart2 = LabeledSeekBar.this.getPaddingStart();
            int n;
            if (width == 0) {
                n = 0;
            }
            else {
                n = (width * 2 - 1) * halfVirtualViewWidth + paddingStart;
            }
            if (width == LabeledSeekBar.this.getMax()) {
                width = LabeledSeekBar.this.getWidth();
            }
            else {
                width = (width * 2 + 1) * halfVirtualViewWidth2 + paddingStart2;
            }
            final Rect rect = new Rect();
            rect.set(n, 0, width, LabeledSeekBar.this.getHeight());
            return rect;
        }
        
        private int getHalfVirtualViewWidth() {
            return Math.max(0, (LabeledSeekBar.this.getWidth() - LabeledSeekBar.this.getPaddingStart() - LabeledSeekBar.this.getPaddingEnd()) / (LabeledSeekBar.this.getMax() * 2));
        }
        
        private int getVirtualViewIdIndexFromX(final float n) {
            int min = Math.min((Math.max(0, ((int)n - LabeledSeekBar.this.getPaddingStart()) / this.getHalfVirtualViewWidth()) + 1) / 2, LabeledSeekBar.this.getMax());
            if (this.mIsLayoutRtl) {
                min = LabeledSeekBar.this.getMax() - min;
            }
            return min;
        }
        
        @Override
        protected int getVirtualViewAt(final float n, final float n2) {
            return this.getVirtualViewIdIndexFromX(n);
        }
        
        @Override
        protected void getVisibleVirtualViews(final List<Integer> list) {
            for (int i = 0; i <= LabeledSeekBar.this.getMax(); ++i) {
                list.add(i);
            }
        }
        
        @Override
        protected boolean onPerformActionForVirtualView(final int progress, final int n, final Bundle bundle) {
            if (progress == -1) {
                return false;
            }
            if (n != 16) {
                return false;
            }
            LabeledSeekBar.this.setProgress(progress);
            this.sendEventForVirtualView(progress, 1);
            return true;
        }
        
        @Override
        protected void onPopulateEventForHost(final AccessibilityEvent accessibilityEvent) {
            accessibilityEvent.setClassName((CharSequence)RadioGroup.class.getName());
        }
        
        @Override
        protected void onPopulateEventForVirtualView(final int n, final AccessibilityEvent accessibilityEvent) {
            accessibilityEvent.setClassName((CharSequence)RadioButton.class.getName());
            accessibilityEvent.setContentDescription((CharSequence)LabeledSeekBar.this.mLabels[n]);
            accessibilityEvent.setChecked(n == LabeledSeekBar.this.getProgress());
        }
        
        @Override
        protected void onPopulateNodeForHost(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            accessibilityNodeInfoCompat.setClassName(RadioGroup.class.getName());
        }
        
        @Override
        protected void onPopulateNodeForVirtualView(final int n, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            accessibilityNodeInfoCompat.setClassName(RadioButton.class.getName());
            accessibilityNodeInfoCompat.setBoundsInParent(this.getBoundsInParentFromVirtualViewId(n));
            accessibilityNodeInfoCompat.addAction(16);
            accessibilityNodeInfoCompat.setContentDescription(LabeledSeekBar.this.mLabels[n]);
            boolean checked = true;
            accessibilityNodeInfoCompat.setClickable(true);
            accessibilityNodeInfoCompat.setCheckable(true);
            if (n != LabeledSeekBar.this.getProgress()) {
                checked = false;
            }
            accessibilityNodeInfoCompat.setChecked(checked);
        }
    }
}
