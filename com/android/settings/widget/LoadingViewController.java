package com.android.settings.widget;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.Animation$AnimationListener;
import android.view.animation.AnimationUtils;
import android.os.Looper;
import android.os.Handler;
import android.view.View;

public class LoadingViewController
{
    public final View mContentView;
    public final Handler mFgHandler;
    public final View mLoadingView;
    private Runnable mShowLoadingContainerRunnable;
    
    public LoadingViewController(final View mLoadingView, final View mContentView) {
        this.mShowLoadingContainerRunnable = new Runnable() {
            @Override
            public void run() {
                LoadingViewController.this.handleLoadingContainer(false, false);
            }
        };
        this.mLoadingView = mLoadingView;
        this.mContentView = mContentView;
        this.mFgHandler = new Handler(Looper.getMainLooper());
    }
    
    public static void handleLoadingContainer(final View view, final View view2, final boolean b, final boolean b2) {
        setViewShown(view, b ^ true, b2);
        setViewShown(view2, b, b2);
    }
    
    private static void setViewShown(final View view, final boolean b, final boolean b2) {
        int visibility = 0;
        if (b2) {
            final Context context = view.getContext();
            int n;
            if (b) {
                n = 17432576;
            }
            else {
                n = 17432577;
            }
            final Animation loadAnimation = AnimationUtils.loadAnimation(context, n);
            if (b) {
                view.setVisibility(0);
            }
            else {
                loadAnimation.setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener() {
                    public void onAnimationEnd(final Animation animation) {
                        view.setVisibility(4);
                    }
                    
                    public void onAnimationRepeat(final Animation animation) {
                    }
                    
                    public void onAnimationStart(final Animation animation) {
                    }
                });
            }
            view.startAnimation(loadAnimation);
        }
        else {
            view.clearAnimation();
            if (!b) {
                visibility = 4;
            }
            view.setVisibility(visibility);
        }
    }
    
    public void handleLoadingContainer(final boolean b, final boolean b2) {
        handleLoadingContainer(this.mLoadingView, this.mContentView, b, b2);
    }
    
    public void showContent(final boolean b) {
        this.mFgHandler.removeCallbacks(this.mShowLoadingContainerRunnable);
        this.handleLoadingContainer(true, b);
    }
    
    public void showLoadingViewDelayed() {
        this.mFgHandler.postDelayed(this.mShowLoadingContainerRunnable, 100L);
    }
}
