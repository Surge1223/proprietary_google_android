package com.android.settings.widget;

import android.content.res.TypedArray;
import com.android.internal.R$styleable;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.PreferenceViewHolder;
import android.view.View;
import android.support.v7.preference.CheckBoxPreference;

public class DisabledCheckBoxPreference extends CheckBoxPreference
{
    private View mCheckBox;
    private boolean mEnabledCheckBox;
    private PreferenceViewHolder mViewHolder;
    
    public DisabledCheckBoxPreference(final Context context) {
        super(context);
        this.setupDisabledCheckBoxPreference(context, null, 0, 0);
    }
    
    public DisabledCheckBoxPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setupDisabledCheckBoxPreference(context, set, 0, 0);
    }
    
    public DisabledCheckBoxPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.setupDisabledCheckBoxPreference(context, set, n, 0);
    }
    
    public DisabledCheckBoxPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setupDisabledCheckBoxPreference(context, set, n, n2);
    }
    
    private void setupDisabledCheckBoxPreference(final Context context, final AttributeSet set, int i, int index) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R$styleable.Preference, i, index);
        for (i = obtainStyledAttributes.getIndexCount() - 1; i >= 0; --i) {
            index = obtainStyledAttributes.getIndex(i);
            if (index == 2) {
                this.mEnabledCheckBox = obtainStyledAttributes.getBoolean(index, true);
            }
        }
        obtainStyledAttributes.recycle();
        super.setEnabled(true);
        this.enableCheckbox(this.mEnabledCheckBox);
    }
    
    public void enableCheckbox(final boolean mEnabledCheckBox) {
        this.mEnabledCheckBox = mEnabledCheckBox;
        if (this.mViewHolder != null && this.mCheckBox != null) {
            this.mCheckBox.setEnabled(this.mEnabledCheckBox);
            this.mViewHolder.itemView.setEnabled(this.mEnabledCheckBox);
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder mViewHolder) {
        super.onBindViewHolder(mViewHolder);
        this.mViewHolder = mViewHolder;
        this.mCheckBox = mViewHolder.findViewById(16908289);
        this.enableCheckbox(this.mEnabledCheckBox);
    }
    
    @Override
    protected void performClick(final View view) {
        if (this.mEnabledCheckBox) {
            super.performClick(view);
        }
    }
}
