package com.android.settings.widget;

import android.content.res.TypedArray;
import android.view.Surface;
import android.graphics.SurfaceTexture;
import android.view.TextureView$SurfaceTextureListener;
import android.view.View.OnClickListener;
import android.view.TextureView;
import android.support.v7.preference.PreferenceViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.util.Log;
import android.media.MediaPlayer$OnPreparedListener;
import android.media.MediaPlayer$OnSeekCompleteListener;
import android.net.Uri.Builder;
import com.android.settings.R;
import android.util.AttributeSet;
import android.net.Uri;
import android.media.MediaPlayer;
import android.content.Context;
import android.support.v7.preference.Preference;

public class VideoPreference extends Preference
{
    boolean mAnimationAvailable;
    private float mAspectRadio;
    private final Context mContext;
    MediaPlayer mMediaPlayer;
    private int mPreviewResource;
    private Uri mVideoPath;
    private boolean mVideoPaused;
    private boolean mVideoReady;
    
    public VideoPreference(final Context mContext, AttributeSet obtainStyledAttributes) {
        super(mContext, obtainStyledAttributes);
        this.mAspectRadio = 1.0f;
        this.mContext = mContext;
        obtainStyledAttributes = (AttributeSet)mContext.getTheme().obtainStyledAttributes(obtainStyledAttributes, R.styleable.VideoPreference, 0, 0);
        try {
            try {
                this.mVideoPath = new Uri.Builder().scheme("android.resource").authority(mContext.getPackageName()).appendPath(String.valueOf(((TypedArray)obtainStyledAttributes).getResourceId(0, 0))).build();
                this.mMediaPlayer = MediaPlayer.create(this.mContext, this.mVideoPath);
                if (this.mMediaPlayer != null && this.mMediaPlayer.getDuration() > 0) {
                    this.setVisible(true);
                    this.setLayoutResource(2131558877);
                    this.mPreviewResource = ((TypedArray)obtainStyledAttributes).getResourceId(1, 0);
                    this.mMediaPlayer.setOnSeekCompleteListener((MediaPlayer$OnSeekCompleteListener)new _$$Lambda$VideoPreference$dH8H9UsxsQzXI7GaCcZWWDvTxoU(this));
                    this.mMediaPlayer.setOnPreparedListener((MediaPlayer$OnPreparedListener)_$$Lambda$VideoPreference$2crRm1Sj4_bqGlDPLY9cVIbC7CU.INSTANCE);
                    this.mAnimationAvailable = true;
                    this.updateAspectRatio();
                }
                this.setVisible(false);
            }
            finally {}
        }
        catch (Exception ex) {
            Log.w("VideoPreference", "Animation resource not found. Will not show animation.");
        }
        ((TypedArray)obtainStyledAttributes).recycle();
        return;
        ((TypedArray)obtainStyledAttributes).recycle();
    }
    
    public boolean isVideoPaused() {
        return this.mVideoPaused;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        if (!this.mAnimationAvailable) {
            return;
        }
        final TextureView textureView = (TextureView)preferenceViewHolder.findViewById(2131362807);
        final ImageView imageView = (ImageView)preferenceViewHolder.findViewById(2131362806);
        final ImageView imageView2 = (ImageView)preferenceViewHolder.findViewById(2131362805);
        final AspectRatioFrameLayout aspectRatioFrameLayout = (AspectRatioFrameLayout)preferenceViewHolder.findViewById(2131362804);
        imageView.setImageResource(this.mPreviewResource);
        aspectRatioFrameLayout.setAspectRatio(this.mAspectRadio);
        textureView.setOnClickListener((View.OnClickListener)new _$$Lambda$VideoPreference$n3lVCTPDzJxvnNXXv__BWcO0YKM(this, imageView2));
        textureView.setSurfaceTextureListener((TextureView$SurfaceTextureListener)new TextureView$SurfaceTextureListener() {
            public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, final int n, final int n2) {
                if (VideoPreference.this.mMediaPlayer != null) {
                    VideoPreference.this.mMediaPlayer.setSurface(new Surface(surfaceTexture));
                    VideoPreference.this.mVideoReady = false;
                    VideoPreference.this.mMediaPlayer.seekTo(0);
                }
            }
            
            public boolean onSurfaceTextureDestroyed(final SurfaceTexture surfaceTexture) {
                imageView.setVisibility(0);
                return false;
            }
            
            public void onSurfaceTextureSizeChanged(final SurfaceTexture surfaceTexture, final int n, final int n2) {
            }
            
            public void onSurfaceTextureUpdated(final SurfaceTexture surfaceTexture) {
                if (VideoPreference.this.mVideoReady) {
                    if (imageView.getVisibility() == 0) {
                        imageView.setVisibility(8);
                    }
                    if (!VideoPreference.this.mVideoPaused && VideoPreference.this.mMediaPlayer != null && !VideoPreference.this.mMediaPlayer.isPlaying()) {
                        VideoPreference.this.mMediaPlayer.start();
                        imageView2.setVisibility(8);
                    }
                }
                if (VideoPreference.this.mMediaPlayer != null && !VideoPreference.this.mMediaPlayer.isPlaying() && imageView2.getVisibility() != 0) {
                    imageView2.setVisibility(0);
                }
            }
        });
    }
    
    @Override
    public void onDetached() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.reset();
            this.mMediaPlayer.release();
        }
        super.onDetached();
    }
    
    public void onViewInvisible() {
        if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
        }
    }
    
    public void onViewVisible(final boolean mVideoPaused) {
        this.mVideoPaused = mVideoPaused;
        if (this.mVideoReady && this.mMediaPlayer != null && !this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.seekTo(0);
        }
    }
    
    void updateAspectRatio() {
        this.mAspectRadio = this.mMediaPlayer.getVideoWidth() / this.mMediaPlayer.getVideoHeight();
    }
}
