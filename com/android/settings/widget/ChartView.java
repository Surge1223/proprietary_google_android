package com.android.settings.widget;

import android.view.View$MeasureSpec;
import android.view.View;
import android.widget.FrameLayout$LayoutParams;
import android.view.Gravity;
import com.android.internal.util.Preconditions;
import android.content.res.TypedArray;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewDebug$ExportedProperty;
import android.graphics.Rect;
import android.widget.FrameLayout;

public class ChartView extends FrameLayout
{
    private Rect mContent;
    ChartAxis mHoriz;
    @ViewDebug$ExportedProperty
    private int mOptimalWidth;
    private float mOptimalWidthWeight;
    ChartAxis mVert;
    
    public ChartView(final Context context) {
        this(context, null, 0);
    }
    
    public ChartView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChartView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mOptimalWidth = -1;
        this.mOptimalWidthWeight = 0.0f;
        this.mContent = new Rect();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ChartView, n, 0);
        this.setOptimalWidth(obtainStyledAttributes.getDimensionPixelSize(0, -1), obtainStyledAttributes.getFloat(1, 0.0f));
        obtainStyledAttributes.recycle();
        this.setClipToPadding(false);
        this.setClipChildren(false);
    }
    
    void init(final ChartAxis chartAxis, final ChartAxis chartAxis2) {
        this.mHoriz = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis, (Object)"missing horiz");
        this.mVert = (ChartAxis)Preconditions.checkNotNull((Object)chartAxis2, (Object)"missing vert");
    }
    
    protected void layoutSweep(final ChartSweepView chartSweepView) {
        final Rect rect = new Rect(this.mContent);
        final Rect rect2 = new Rect();
        this.layoutSweep(chartSweepView, rect, rect2);
        chartSweepView.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
    }
    
    protected void layoutSweep(final ChartSweepView chartSweepView, final Rect rect, final Rect rect2) {
        final Rect margins = chartSweepView.getMargins();
        if (chartSweepView.getFollowAxis() == 1) {
            rect.top += margins.top + (int)chartSweepView.getPoint();
            rect.bottom = rect.top;
            rect.left += margins.left;
            rect.right += margins.right;
            Gravity.apply(8388659, rect.width(), chartSweepView.getMeasuredHeight(), rect, rect2);
        }
        else {
            rect.left += margins.left + (int)chartSweepView.getPoint();
            rect.right = rect.left;
            rect.top += margins.top;
            rect.bottom += margins.bottom;
            Gravity.apply(8388659, chartSweepView.getMeasuredWidth(), rect.height(), rect, rect2);
        }
    }
    
    protected void onLayout(final boolean b, int i, int width, int height, final int n) {
        this.mContent.set(this.getPaddingLeft(), this.getPaddingTop(), height - i - this.getPaddingRight(), n - width - this.getPaddingBottom());
        width = this.mContent.width();
        height = this.mContent.height();
        this.mHoriz.setSize(width);
        this.mVert.setSize(height);
        final Rect rect = new Rect();
        final Rect rect2 = new Rect();
        View child;
        FrameLayout$LayoutParams frameLayout$LayoutParams;
        for (i = 0; i < this.getChildCount(); ++i) {
            child = this.getChildAt(i);
            frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            rect.set(this.mContent);
            if (child instanceof ChartNetworkSeriesView) {
                Gravity.apply(frameLayout$LayoutParams.gravity, width, height, rect, rect2);
                child.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
            }
            else if (child instanceof ChartGridView) {
                Gravity.apply(frameLayout$LayoutParams.gravity, width, height, rect, rect2);
                child.layout(rect2.left, rect2.top, rect2.right, rect2.bottom + child.getPaddingBottom());
            }
            else if (child instanceof ChartSweepView) {
                this.layoutSweep((ChartSweepView)child, rect, rect2);
                child.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
            }
        }
    }
    
    protected void onMeasure(int n, final int n2) {
        super.onMeasure(n, n2);
        n = this.getMeasuredWidth() - this.mOptimalWidth;
        if (this.mOptimalWidth > 0 && n > 0) {
            super.onMeasure(View$MeasureSpec.makeMeasureSpec((int)(this.mOptimalWidth + n * this.mOptimalWidthWeight), 1073741824), n2);
        }
    }
    
    public void setOptimalWidth(final int mOptimalWidth, final float mOptimalWidthWeight) {
        this.mOptimalWidth = mOptimalWidth;
        this.mOptimalWidthWeight = mOptimalWidthWeight;
        this.requestLayout();
    }
}
