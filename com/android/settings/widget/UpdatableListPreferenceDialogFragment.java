package com.android.settings.widget;

import android.content.res.TypedArray;
import android.content.DialogInterface$OnClickListener;
import android.widget.ListAdapter;
import java.util.List;
import android.util.AttributeSet;
import com.android.internal.R$styleable;
import android.app.AlertDialog$Builder;
import android.os.Bundle;
import android.content.DialogInterface;
import android.support.v7.preference.ListPreference;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.support.v14.preference.PreferenceDialogFragment;

public class UpdatableListPreferenceDialogFragment extends PreferenceDialogFragment implements Instrumentable
{
    private ArrayAdapter mAdapter;
    private int mClickedDialogEntryIndex;
    private ArrayList<CharSequence> mEntries;
    private CharSequence[] mEntryValues;
    private int mMetricsCategory;
    
    public UpdatableListPreferenceDialogFragment() {
        this.mMetricsCategory = 0;
    }
    
    private ListPreference getListPreference() {
        return (ListPreference)this.getPreference();
    }
    
    public static UpdatableListPreferenceDialogFragment newInstance(final String s, final int n) {
        final UpdatableListPreferenceDialogFragment updatableListPreferenceDialogFragment = new UpdatableListPreferenceDialogFragment();
        final Bundle arguments = new Bundle(1);
        arguments.putString("key", s);
        arguments.putInt("metrics_category_key", n);
        updatableListPreferenceDialogFragment.setArguments(arguments);
        return updatableListPreferenceDialogFragment;
    }
    
    private void setPreferenceData(final ListPreference listPreference) {
        this.mEntries.clear();
        this.mClickedDialogEntryIndex = listPreference.findIndexOfValue(listPreference.getValue());
        final CharSequence[] entries = listPreference.getEntries();
        for (int length = entries.length, i = 0; i < length; ++i) {
            this.mEntries.add(entries[i]);
        }
        this.mEntryValues = listPreference.getEntryValues();
    }
    
    ArrayAdapter getAdapter() {
        return this.mAdapter;
    }
    
    @Override
    public int getMetricsCategory() {
        return this.mMetricsCategory;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mMetricsCategory = this.getArguments().getInt("metrics_category_key", 0);
        if (bundle == null) {
            this.mEntries = new ArrayList<CharSequence>();
            this.setPreferenceData(this.getListPreference());
        }
        else {
            this.mClickedDialogEntryIndex = bundle.getInt("UpdatableListPreferenceDialogFragment.index", 0);
            this.mEntries = (ArrayList<CharSequence>)bundle.getCharSequenceArrayList("UpdatableListPreferenceDialogFragment.entries");
            this.mEntryValues = bundle.getCharSequenceArray("UpdatableListPreferenceDialogFragment.entryValues");
        }
    }
    
    @Override
    public void onDialogClosed(final boolean b) {
        final ListPreference listPreference = this.getListPreference();
        if (b && this.mClickedDialogEntryIndex >= 0) {
            final String string = this.mEntryValues[this.mClickedDialogEntryIndex].toString();
            if (listPreference.callChangeListener(string)) {
                listPreference.setValue(string);
            }
        }
    }
    
    public void onListPreferenceUpdated(final ListPreference preferenceData) {
        if (this.mAdapter != null) {
            this.setPreferenceData(preferenceData);
            this.mAdapter.notifyDataSetChanged();
        }
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder) {
        super.onPrepareDialogBuilder(alertDialog$Builder);
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes((AttributeSet)null, R$styleable.AlertDialog, 16842845, 0);
        alertDialog$Builder.setSingleChoiceItems((ListAdapter)(this.mAdapter = new ArrayAdapter(this.getContext(), obtainStyledAttributes.getResourceId(21, 17367058), (List)this.mEntries)), this.mClickedDialogEntryIndex, (DialogInterface$OnClickListener)new _$$Lambda$UpdatableListPreferenceDialogFragment$yZRmvmWflT3ytJ4m_nzXQtpejcQ(this));
        alertDialog$Builder.setPositiveButton((CharSequence)null, (DialogInterface$OnClickListener)null);
        obtainStyledAttributes.recycle();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("UpdatableListPreferenceDialogFragment.index", this.mClickedDialogEntryIndex);
        bundle.putCharSequenceArrayList("UpdatableListPreferenceDialogFragment.entries", (ArrayList)this.mEntries);
        bundle.putCharSequenceArray("UpdatableListPreferenceDialogFragment.entryValues", this.mEntryValues);
    }
    
    void setAdapter(final ArrayAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }
    
    void setEntries(final ArrayList<CharSequence> mEntries) {
        this.mEntries = mEntries;
    }
    
    void setMetricsCategory(final Bundle bundle) {
        this.mMetricsCategory = bundle.getInt("metrics_category_key", 0);
    }
}
