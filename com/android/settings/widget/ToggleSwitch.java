package com.android.settings.widget;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.Switch;

public class ToggleSwitch extends Switch
{
    private OnBeforeCheckedChangeListener mOnBeforeListener;
    
    public ToggleSwitch(final Context context) {
        super(context);
    }
    
    public ToggleSwitch(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public ToggleSwitch(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public ToggleSwitch(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    public void setChecked(final boolean checked) {
        if (this.mOnBeforeListener != null && this.mOnBeforeListener.onBeforeCheckedChanged(this, checked)) {
            return;
        }
        super.setChecked(checked);
    }
    
    public void setCheckedInternal(final boolean checked) {
        super.setChecked(checked);
    }
    
    public void setOnBeforeCheckedChangeListener(final OnBeforeCheckedChangeListener mOnBeforeListener) {
        this.mOnBeforeListener = mOnBeforeListener;
    }
    
    public interface OnBeforeCheckedChangeListener
    {
        boolean onBeforeCheckedChanged(final ToggleSwitch p0, final boolean p1);
    }
}
