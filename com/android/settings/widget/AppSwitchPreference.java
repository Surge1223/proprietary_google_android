package com.android.settings.widget;

import android.view.View;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;

public class AppSwitchPreference extends SwitchPreference
{
    public AppSwitchPreference(final Context context) {
        super(context);
        this.setLayoutResource(2131558644);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362671);
        int visibility;
        if (TextUtils.isEmpty(this.getSummary())) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        viewById.setVisibility(visibility);
    }
}
