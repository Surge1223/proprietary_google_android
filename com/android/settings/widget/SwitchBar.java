package com.android.settings.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;
import android.content.res.Resources;
import android.os.Parcelable;
import android.widget.CompoundButton;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.TouchDelegate;
import android.graphics.Rect;
import android.widget.Switch;
import android.content.res.TypedArray;
import com.android.settings.overlay.FeatureFactory;
import android.view.View.OnClickListener;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import java.util.List;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.RestrictedLockUtils;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.LinearLayout;

public class SwitchBar extends LinearLayout implements CompoundButton$OnCheckedChangeListener
{
    private static final int[] XML_ATTRIBUTES;
    private int mBackgroundActivatedColor;
    private int mBackgroundColor;
    private boolean mDisabledByAdmin;
    private RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    private String mLabel;
    private boolean mLoggingIntialized;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private String mMetricsTag;
    private int mOffTextId;
    private int mOnTextId;
    private View mRestrictedIcon;
    private String mSummary;
    private final TextAppearanceSpan mSummarySpan;
    private ToggleSwitch mSwitch;
    private final List<OnSwitchChangeListener> mSwitchChangeListeners;
    private TextView mTextView;
    
    static {
        XML_ATTRIBUTES = new int[] { 2130969039, 2130969038, 2130969037, 2130969036 };
    }
    
    public SwitchBar(final Context context) {
        this(context, null);
    }
    
    public SwitchBar(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public SwitchBar(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public SwitchBar(final Context context, final AttributeSet set, int marginStart, int marginEnd) {
        super(context, set, marginStart, marginEnd);
        this.mSwitchChangeListeners = new ArrayList<OnSwitchChangeListener>();
        this.mEnforcedAdmin = null;
        LayoutInflater.from(context).inflate(2131558842, (ViewGroup)this);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, SwitchBar.XML_ATTRIBUTES);
        marginStart = (int)obtainStyledAttributes.getDimension(0, 0.0f);
        marginEnd = (int)obtainStyledAttributes.getDimension(1, 0.0f);
        this.mBackgroundColor = obtainStyledAttributes.getColor(2, 0);
        this.mBackgroundActivatedColor = obtainStyledAttributes.getColor(3, 0);
        obtainStyledAttributes.recycle();
        this.mTextView = (TextView)this.findViewById(2131362708);
        this.mSummarySpan = new TextAppearanceSpan(this.mContext, 2131952048);
        ((ViewGroup$MarginLayoutParams)this.mTextView.getLayoutParams()).setMarginStart(marginStart);
        (this.mSwitch = (ToggleSwitch)this.findViewById(2131362709)).setSaveEnabled(false);
        ((ViewGroup$MarginLayoutParams)this.mSwitch.getLayoutParams()).setMarginEnd(marginEnd);
        this.setBackgroundColor(this.mBackgroundColor);
        this.setSwitchBarText(2131889422, 2131889421);
        this.addOnSwitchChangeListener((OnSwitchChangeListener)new _$$Lambda$SwitchBar$xcPsCGGwUScwZOtx6bxg2zuPXc8(this));
        (this.mRestrictedIcon = this.findViewById(R.id.restricted_icon)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (SwitchBar.this.mDisabledByAdmin) {
                    final MetricsFeatureProvider access$300 = SwitchBar.this.mMetricsFeatureProvider;
                    final Context access$301 = SwitchBar.this.mContext;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(SwitchBar.this.mMetricsTag);
                    sb.append("/switch_bar|restricted");
                    access$300.count(access$301, sb.toString(), 1);
                    RestrictedLockUtils.sendShowAdminSupportDetailsIntent(context, SwitchBar.this.mEnforcedAdmin);
                }
            }
        });
        this.setVisibility(8);
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
    }
    
    private void updateText() {
        if (TextUtils.isEmpty((CharSequence)this.mSummary)) {
            this.mTextView.setText((CharSequence)this.mLabel);
            return;
        }
        final SpannableStringBuilder append = new SpannableStringBuilder((CharSequence)this.mLabel).append('\n');
        final int length = append.length();
        append.append((CharSequence)this.mSummary);
        append.setSpan((Object)this.mSummarySpan, length, append.length(), 0);
        this.mTextView.setText((CharSequence)append);
    }
    
    public void addOnSwitchChangeListener(final OnSwitchChangeListener onSwitchChangeListener) {
        if (!this.mSwitchChangeListeners.contains(onSwitchChangeListener)) {
            this.mSwitchChangeListeners.add(onSwitchChangeListener);
            return;
        }
        throw new IllegalStateException("Cannot add twice the same OnSwitchChangeListener");
    }
    
    View getDelegatingView() {
        Object o;
        if (this.mDisabledByAdmin) {
            o = this.mRestrictedIcon;
        }
        else {
            o = this.mSwitch;
        }
        return (View)o;
    }
    
    public final ToggleSwitch getSwitch() {
        return this.mSwitch;
    }
    
    public void hide() {
        if (this.isShowing()) {
            this.setVisibility(8);
            this.mSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        }
    }
    
    public boolean isChecked() {
        return this.mSwitch.isChecked();
    }
    
    public boolean isShowing() {
        return this.getVisibility() == 0;
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        if (this.mLoggingIntialized) {
            final MetricsFeatureProvider mMetricsFeatureProvider = this.mMetricsFeatureProvider;
            final Context mContext = this.mContext;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mMetricsTag);
            sb.append("/switch_bar|");
            sb.append(b);
            mMetricsFeatureProvider.count(mContext, sb.toString(), 1);
        }
        this.mLoggingIntialized = true;
        this.propagateChecked(b);
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mSwitch.setCheckedInternal(savedState.checked);
        this.setTextViewLabelAndBackground(savedState.checked);
        int visibility;
        if (savedState.visible) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.setVisibility(visibility);
        final ToggleSwitch mSwitch = this.mSwitch;
        Object onCheckedChangeListener;
        if (savedState.visible) {
            onCheckedChangeListener = this;
        }
        else {
            onCheckedChangeListener = null;
        }
        mSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)onCheckedChangeListener);
        this.requestLayout();
    }
    
    public Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.checked = this.mSwitch.isChecked();
        savedState.visible = this.isShowing();
        return (Parcelable)savedState;
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        if (n > 0 && n2 > 0) {
            this.setTouchDelegate(new TouchDelegate(new Rect(0, 0, n, n2), this.getDelegatingView()));
        }
    }
    
    public void propagateChecked(final boolean b) {
        for (int size = this.mSwitchChangeListeners.size(), i = 0; i < size; ++i) {
            this.mSwitchChangeListeners.get(i).onSwitchChanged(this.mSwitch, b);
        }
    }
    
    public void removeOnSwitchChangeListener(final OnSwitchChangeListener onSwitchChangeListener) {
        if (this.mSwitchChangeListeners.contains(onSwitchChangeListener)) {
            this.mSwitchChangeListeners.remove(onSwitchChangeListener);
            return;
        }
        throw new IllegalStateException("Cannot remove OnSwitchChangeListener");
    }
    
    public void setChecked(final boolean b) {
        this.setTextViewLabelAndBackground(b);
        this.mSwitch.setChecked(b);
    }
    
    public void setCheckedInternal(final boolean b) {
        this.setTextViewLabelAndBackground(b);
        this.mSwitch.setCheckedInternal(b);
    }
    
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin) {
        this.mEnforcedAdmin = mEnforcedAdmin;
        if (mEnforcedAdmin != null) {
            super.setEnabled(true);
            this.mDisabledByAdmin = true;
            this.mTextView.setEnabled(false);
            this.mSwitch.setEnabled(false);
            this.mSwitch.setVisibility(8);
            this.mRestrictedIcon.setVisibility(0);
        }
        else {
            this.mDisabledByAdmin = false;
            this.mSwitch.setVisibility(0);
            this.mRestrictedIcon.setVisibility(8);
            this.setEnabled(true);
        }
        this.setTouchDelegate(new TouchDelegate(new Rect(0, 0, this.getWidth(), this.getHeight()), this.getDelegatingView()));
    }
    
    public void setEnabled(final boolean enabled) {
        if (enabled && this.mDisabledByAdmin) {
            this.setDisabledByAdmin(null);
            return;
        }
        super.setEnabled(enabled);
        this.mTextView.setEnabled(enabled);
        this.mSwitch.setEnabled(enabled);
    }
    
    public void setMetricsTag(final String mMetricsTag) {
        this.mMetricsTag = mMetricsTag;
    }
    
    public void setSummary(final String mSummary) {
        this.mSummary = mSummary;
        this.updateText();
    }
    
    public void setSwitchBarText(final int mOnTextId, final int mOffTextId) {
        this.mOnTextId = mOnTextId;
        this.mOffTextId = mOffTextId;
        this.setTextViewLabelAndBackground(this.isChecked());
    }
    
    public void setTextViewLabelAndBackground(final boolean b) {
        final Resources resources = this.getResources();
        int n;
        if (b) {
            n = this.mOnTextId;
        }
        else {
            n = this.mOffTextId;
        }
        this.mLabel = resources.getString(n);
        int backgroundColor;
        if (b) {
            backgroundColor = this.mBackgroundActivatedColor;
        }
        else {
            backgroundColor = this.mBackgroundColor;
        }
        this.setBackgroundColor(backgroundColor);
        this.updateText();
    }
    
    public void show() {
        if (!this.isShowing()) {
            this.setVisibility(0);
            this.mSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
            this.post((Runnable)new _$$Lambda$SwitchBar$H3bwEmU9c2USPE1paf4Zlyfzp3I(this));
        }
    }
    
    public interface OnSwitchChangeListener
    {
        void onSwitchChanged(final Switch p0, final boolean p1);
    }
    
    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        boolean checked;
        boolean visible;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        private SavedState(final Parcel parcel) {
            super(parcel);
            this.checked = (boolean)parcel.readValue((ClassLoader)null);
            this.visible = (boolean)parcel.readValue((ClassLoader)null);
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SwitchBar.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" checked=");
            sb.append(this.checked);
            sb.append(" visible=");
            sb.append(this.visible);
            sb.append("}");
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeValue((Object)this.checked);
            parcel.writeValue((Object)this.visible);
        }
    }
}
