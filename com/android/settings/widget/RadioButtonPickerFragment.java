package com.android.settings.widget;

import java.util.Iterator;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.os.UserHandle;
import android.content.Context;
import java.util.List;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.util.ArrayMap;
import android.os.UserManager;
import com.android.settingslib.widget.CandidateInfo;
import java.util.Map;
import com.android.settings.core.InstrumentedPreferenceFragment;

public abstract class RadioButtonPickerFragment extends InstrumentedPreferenceFragment implements OnClickListener
{
    static final String EXTRA_FOR_WORK = "for_work";
    private final Map<String, CandidateInfo> mCandidates;
    protected int mUserId;
    protected UserManager mUserManager;
    
    public RadioButtonPickerFragment() {
        this.mCandidates = (Map<String, CandidateInfo>)new ArrayMap();
    }
    
    protected void addStaticPreferences(final PreferenceScreen preferenceScreen) {
    }
    
    public RadioButtonPreference bindPreference(final RadioButtonPreference radioButtonPreference, final String key, final CandidateInfo candidateInfo, final String s) {
        radioButtonPreference.setTitle(candidateInfo.loadLabel());
        Utils.setSafeIcon(radioButtonPreference, candidateInfo.loadIcon());
        radioButtonPreference.setKey(key);
        if (TextUtils.equals((CharSequence)s, (CharSequence)key)) {
            radioButtonPreference.setChecked(true);
        }
        radioButtonPreference.setEnabled(candidateInfo.enabled);
        radioButtonPreference.setOnClickListener((RadioButtonPreference.OnClickListener)this);
        return radioButtonPreference;
    }
    
    public void bindPreferenceExtra(final RadioButtonPreference radioButtonPreference, final String s, final CandidateInfo candidateInfo, final String s2, final String s3) {
    }
    
    protected CandidateInfo getCandidate(final String s) {
        return this.mCandidates.get(s);
    }
    
    protected abstract List<? extends CandidateInfo> getCandidates();
    
    protected abstract String getDefaultKey();
    
    @Override
    protected abstract int getPreferenceScreenResId();
    
    protected int getRadioButtonPreferenceCustomLayoutResId() {
        return 0;
    }
    
    protected String getSystemDefaultKey() {
        return null;
    }
    
    public void mayCheckOnlyRadioButton() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (preferenceScreen != null && preferenceScreen.getPreferenceCount() == 1) {
            final Preference preference = preferenceScreen.getPreference(0);
            if (preference instanceof RadioButtonPreference) {
                ((RadioButtonPreference)preference).setChecked(true);
            }
        }
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        final Bundle arguments = this.getArguments();
        boolean boolean1 = false;
        if (arguments != null) {
            boolean1 = arguments.getBoolean("for_work");
        }
        final UserHandle managedProfile = Utils.getManagedProfile(this.mUserManager);
        int mUserId;
        if (boolean1 && managedProfile != null) {
            mUserId = managedProfile.getIdentifier();
        }
        else {
            mUserId = UserHandle.myUserId();
        }
        this.mUserId = mUserId;
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        super.onCreatePreferences(bundle, s);
        this.updateCandidates();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.setHasOptionsMenu(true);
        return onCreateView;
    }
    
    @Override
    public void onRadioButtonClicked(final RadioButtonPreference radioButtonPreference) {
        this.onRadioButtonConfirmed(radioButtonPreference.getKey());
    }
    
    protected void onRadioButtonConfirmed(final String defaultKey) {
        final boolean setDefaultKey = this.setDefaultKey(defaultKey);
        if (setDefaultKey) {
            this.updateCheckedState(defaultKey);
        }
        this.onSelectionPerformed(setDefaultKey);
    }
    
    protected void onSelectionPerformed(final boolean b) {
    }
    
    protected abstract boolean setDefaultKey(final String p0);
    
    protected boolean shouldShowItemNone() {
        return false;
    }
    
    public void updateCandidates() {
        this.mCandidates.clear();
        final List<? extends CandidateInfo> candidates = this.getCandidates();
        if (candidates != null) {
            for (final CandidateInfo candidateInfo : candidates) {
                this.mCandidates.put(candidateInfo.getKey(), candidateInfo);
            }
        }
        final String defaultKey = this.getDefaultKey();
        final String systemDefaultKey = this.getSystemDefaultKey();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        this.addStaticPreferences(preferenceScreen);
        final int radioButtonPreferenceCustomLayoutResId = this.getRadioButtonPreferenceCustomLayoutResId();
        if (this.shouldShowItemNone()) {
            final RadioButtonPreference radioButtonPreference = new RadioButtonPreference(this.getPrefContext());
            if (radioButtonPreferenceCustomLayoutResId > 0) {
                radioButtonPreference.setLayoutResource(radioButtonPreferenceCustomLayoutResId);
            }
            radioButtonPreference.setIcon(2131231101);
            radioButtonPreference.setTitle(2131886368);
            radioButtonPreference.setChecked(TextUtils.isEmpty((CharSequence)defaultKey));
            radioButtonPreference.setOnClickListener((RadioButtonPreference.OnClickListener)this);
            preferenceScreen.addPreference(radioButtonPreference);
        }
        if (candidates != null) {
            for (final CandidateInfo candidateInfo2 : candidates) {
                final RadioButtonPreference radioButtonPreference2 = new RadioButtonPreference(this.getPrefContext());
                if (radioButtonPreferenceCustomLayoutResId > 0) {
                    radioButtonPreference2.setLayoutResource(radioButtonPreferenceCustomLayoutResId);
                }
                this.bindPreference(radioButtonPreference2, candidateInfo2.getKey(), candidateInfo2, defaultKey);
                this.bindPreferenceExtra(radioButtonPreference2, candidateInfo2.getKey(), candidateInfo2, defaultKey, systemDefaultKey);
                preferenceScreen.addPreference(radioButtonPreference2);
            }
        }
        this.mayCheckOnlyRadioButton();
    }
    
    public void updateCheckedState(final String s) {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (preferenceScreen != null) {
            for (int preferenceCount = preferenceScreen.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
                final Preference preference = preferenceScreen.getPreference(i);
                if (preference instanceof RadioButtonPreference) {
                    final RadioButtonPreference radioButtonPreference = (RadioButtonPreference)preference;
                    if (radioButtonPreference.isChecked() != TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)s)) {
                        radioButtonPreference.setChecked(TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)s));
                    }
                }
            }
        }
    }
}
