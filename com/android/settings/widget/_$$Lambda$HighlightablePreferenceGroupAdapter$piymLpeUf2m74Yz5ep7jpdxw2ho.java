package com.android.settings.widget;

import android.support.v7.preference.PreferenceViewHolder;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v7.widget.RecyclerView;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import com.android.settings.SettingsPreferenceFragment;
import android.animation.TypeEvaluator;
import android.animation.ArgbEvaluator;
import android.util.Log;
import android.content.Context;
import android.util.TypedValue;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceGroupAdapter;
import android.animation.ValueAnimator;
import android.view.View;
import android.animation.ValueAnimator$AnimatorUpdateListener;

public final class _$$Lambda$HighlightablePreferenceGroupAdapter$piymLpeUf2m74Yz5ep7jpdxw2ho implements ValueAnimator$AnimatorUpdateListener
{
    public final void onAnimationUpdate(final ValueAnimator valueAnimator) {
        HighlightablePreferenceGroupAdapter.lambda$addHighlightBackground$2(this.f$0, valueAnimator);
    }
}
