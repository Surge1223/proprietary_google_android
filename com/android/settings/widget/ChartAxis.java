package com.android.settings.widget;

import android.text.SpannableStringBuilder;
import android.content.res.Resources;

public interface ChartAxis
{
    long buildLabel(final Resources p0, final SpannableStringBuilder p1, final long p2);
    
    float convertToPoint(final long p0);
    
    long convertToValue(final float p0);
    
    float[] getTickPoints();
    
    boolean setBounds(final long p0, final long p1);
    
    boolean setSize(final float p0);
    
    int shouldAdjustAxis(final long p0);
}
