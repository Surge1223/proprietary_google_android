package com.android.settings.widget;

import android.text.TextUtils;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class ActionButtonPreference extends Preference
{
    private final ButtonInfo mButton1Info;
    private final ButtonInfo mButton2Info;
    
    public ActionButtonPreference(final Context context) {
        super(context);
        this.mButton1Info = new ButtonInfo();
        this.mButton2Info = new ButtonInfo();
        this.init();
    }
    
    public ActionButtonPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mButton1Info = new ButtonInfo();
        this.mButton2Info = new ButtonInfo();
        this.init();
    }
    
    public ActionButtonPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mButton1Info = new ButtonInfo();
        this.mButton2Info = new ButtonInfo();
        this.init();
    }
    
    public ActionButtonPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mButton1Info = new ButtonInfo();
        this.mButton2Info = new ButtonInfo();
        this.init();
    }
    
    private void init() {
        this.setLayoutResource(2131558857);
        this.setSelectable(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        preferenceViewHolder.setDividerAllowedAbove(false);
        preferenceViewHolder.setDividerAllowedBelow(false);
        this.mButton1Info.mPositiveButton = (Button)preferenceViewHolder.findViewById(2131361944);
        this.mButton1Info.mNegativeButton = (Button)preferenceViewHolder.findViewById(2131361943);
        this.mButton2Info.mPositiveButton = (Button)preferenceViewHolder.findViewById(2131361946);
        this.mButton2Info.mNegativeButton = (Button)preferenceViewHolder.findViewById(2131361945);
        this.mButton1Info.setUpButton();
        this.mButton2Info.setUpButton();
    }
    
    public ActionButtonPreference setButton1Enabled(final boolean b) {
        if (b != this.mButton1Info.mIsEnabled) {
            this.mButton1Info.mIsEnabled = b;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton1OnClickListener(final View.OnClickListener view$OnClickListener) {
        if (view$OnClickListener != this.mButton1Info.mListener) {
            this.mButton1Info.mListener = view$OnClickListener;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton1Positive(final boolean b) {
        if (b != this.mButton1Info.mIsPositive) {
            this.mButton1Info.mIsPositive = b;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton1Text(final int n) {
        final String string = this.getContext().getString(n);
        if (!TextUtils.equals((CharSequence)string, this.mButton1Info.mText)) {
            this.mButton1Info.mText = string;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton1Visible(final boolean b) {
        if (b != this.mButton1Info.mIsVisible) {
            this.mButton1Info.mIsVisible = b;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton2Enabled(final boolean b) {
        if (b != this.mButton2Info.mIsEnabled) {
            this.mButton2Info.mIsEnabled = b;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton2OnClickListener(final View.OnClickListener view$OnClickListener) {
        if (view$OnClickListener != this.mButton2Info.mListener) {
            this.mButton2Info.mListener = view$OnClickListener;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton2Positive(final boolean b) {
        if (b != this.mButton2Info.mIsPositive) {
            this.mButton2Info.mIsPositive = b;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton2Text(final int n) {
        final String string = this.getContext().getString(n);
        if (!TextUtils.equals((CharSequence)string, this.mButton2Info.mText)) {
            this.mButton2Info.mText = string;
            this.notifyChanged();
        }
        return this;
    }
    
    public ActionButtonPreference setButton2Visible(final boolean b) {
        if (b != this.mButton2Info.mIsVisible) {
            this.mButton2Info.mIsVisible = b;
            this.notifyChanged();
        }
        return this;
    }
    
    static class ButtonInfo
    {
        private boolean mIsEnabled;
        private boolean mIsPositive;
        private boolean mIsVisible;
        private View.OnClickListener mListener;
        private Button mNegativeButton;
        private Button mPositiveButton;
        private CharSequence mText;
        
        ButtonInfo() {
            this.mIsPositive = true;
            this.mIsEnabled = true;
            this.mIsVisible = true;
        }
        
        private void setUpButton(final Button button) {
            button.setText(this.mText);
            button.setOnClickListener(this.mListener);
            button.setEnabled(this.mIsEnabled);
        }
        
        void setUpButton() {
            this.setUpButton(this.mPositiveButton);
            this.setUpButton(this.mNegativeButton);
            if (!this.mIsVisible) {
                this.mPositiveButton.setVisibility(4);
                this.mNegativeButton.setVisibility(4);
            }
            else if (this.mIsPositive) {
                this.mPositiveButton.setVisibility(0);
                this.mNegativeButton.setVisibility(4);
            }
            else {
                this.mPositiveButton.setVisibility(4);
                this.mNegativeButton.setVisibility(0);
            }
        }
    }
}
