package com.android.settings.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v7.preference.Preference;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityNodeInfo$RangeInfo;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.View$AccessibilityDelegate;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceViewHolder;
import android.view.View;
import android.content.res.TypedArray;
import com.android.internal.R$styleable;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.SeekBar;
import android.widget.SeekBar$OnSeekBarChangeListener;
import android.view.View$OnKeyListener;
import com.android.settingslib.RestrictedPreference;

public class SeekBarPreference extends RestrictedPreference implements View$OnKeyListener, SeekBar$OnSeekBarChangeListener
{
    private int mAccessibilityRangeInfoType;
    private boolean mContinuousUpdates;
    private int mDefaultProgress;
    private int mMax;
    private int mMin;
    private int mProgress;
    private SeekBar mSeekBar;
    private CharSequence mSeekBarContentDescription;
    private boolean mShouldBlink;
    private boolean mTrackingTouch;
    
    public SeekBarPreference(final Context context) {
        this(context, null);
    }
    
    public SeekBarPreference(final Context context, final AttributeSet set) {
        this(context, set, TypedArrayUtils.getAttr(context, R.attr.seekBarPreferenceStyle, 17891515));
    }
    
    public SeekBarPreference(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public SeekBarPreference(final Context context, final AttributeSet set, int resourceId, final int n) {
        super(context, set, resourceId, n);
        this.mDefaultProgress = -1;
        this.mAccessibilityRangeInfoType = 0;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R$styleable.ProgressBar, resourceId, n);
        this.setMax(obtainStyledAttributes.getInt(2, this.mMax));
        this.setMin(obtainStyledAttributes.getInt(26, this.mMin));
        obtainStyledAttributes.recycle();
        final TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(set, R$styleable.SeekBarPreference, resourceId, n);
        resourceId = obtainStyledAttributes2.getResourceId(0, 17367238);
        obtainStyledAttributes2.recycle();
        this.setLayoutResource(resourceId);
    }
    
    private void setProgress(int mMin, final boolean b) {
        int mMax = mMin;
        if (mMin > this.mMax) {
            mMax = this.mMax;
        }
        if ((mMin = mMax) < this.mMin) {
            mMin = this.mMin;
        }
        if (mMin != this.mProgress) {
            this.persistInt(this.mProgress = mMin);
            if (b) {
                this.notifyChanged();
            }
        }
    }
    
    public int getMax() {
        return this.mMax;
    }
    
    public int getMin() {
        return this.mMin;
    }
    
    public int getProgress() {
        return this.mProgress;
    }
    
    public CharSequence getSummary() {
        return null;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        preferenceViewHolder.itemView.setOnKeyListener((View$OnKeyListener)this);
        (this.mSeekBar = (SeekBar)preferenceViewHolder.findViewById(16909273)).setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)this);
        this.mSeekBar.setMax(this.mMax);
        this.mSeekBar.setMin(this.mMin);
        this.mSeekBar.setProgress(this.mProgress);
        this.mSeekBar.setEnabled(this.isEnabled());
        final CharSequence title = this.getTitle();
        if (!TextUtils.isEmpty(this.mSeekBarContentDescription)) {
            this.mSeekBar.setContentDescription(this.mSeekBarContentDescription);
        }
        else if (!TextUtils.isEmpty(title)) {
            this.mSeekBar.setContentDescription(title);
        }
        if (this.mSeekBar instanceof DefaultIndicatorSeekBar) {
            ((DefaultIndicatorSeekBar)this.mSeekBar).setDefaultProgress(this.mDefaultProgress);
        }
        if (this.mShouldBlink) {
            final View itemView = preferenceViewHolder.itemView;
            itemView.post((Runnable)new _$$Lambda$SeekBarPreference$dLBfCJMqk38mmQ3tQY_pIyDA0S8(this, itemView));
        }
        this.mSeekBar.setAccessibilityDelegate((View$AccessibilityDelegate)new View$AccessibilityDelegate() {
            public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfo accessibilityNodeInfo) {
                super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfo);
                final AccessibilityNodeInfo$RangeInfo rangeInfo = accessibilityNodeInfo.getRangeInfo();
                if (rangeInfo != null) {
                    accessibilityNodeInfo.setRangeInfo(AccessibilityNodeInfo$RangeInfo.obtain(SeekBarPreference.this.mAccessibilityRangeInfoType, rangeInfo.getMin(), rangeInfo.getMax(), rangeInfo.getCurrent()));
                }
            }
        });
    }
    
    protected Object onGetDefaultValue(final TypedArray typedArray, final int n) {
        return typedArray.getInt(n, 0);
    }
    
    public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        final SeekBar seekBar = (SeekBar)view.findViewById(16909273);
        return seekBar != null && seekBar.onKeyDown(n, keyEvent);
    }
    
    public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
        if (b && (this.mContinuousUpdates || !this.mTrackingTouch)) {
            this.syncProgress(seekBar);
        }
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!parcelable.getClass().equals(SavedState.class)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mProgress = savedState.progress;
        this.mMax = savedState.max;
        this.mMin = savedState.min;
        this.notifyChanged();
    }
    
    protected Parcelable onSaveInstanceState() {
        final Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (this.isPersistent()) {
            return onSaveInstanceState;
        }
        final SavedState savedState = new SavedState(onSaveInstanceState);
        savedState.progress = this.mProgress;
        savedState.max = this.mMax;
        savedState.min = this.mMin;
        return (Parcelable)savedState;
    }
    
    protected void onSetInitialValue(final boolean b, final Object o) {
        int progress;
        if (b) {
            progress = this.getPersistedInt(this.mProgress);
        }
        else {
            progress = (int)o;
        }
        this.setProgress(progress);
    }
    
    public void onStartTrackingTouch(final SeekBar seekBar) {
        this.mTrackingTouch = true;
    }
    
    public void onStopTrackingTouch(final SeekBar seekBar) {
        this.mTrackingTouch = false;
        if (seekBar.getProgress() != this.mProgress) {
            this.syncProgress(seekBar);
        }
    }
    
    public void setAccessibilityRangeInfoType(final int mAccessibilityRangeInfoType) {
        this.mAccessibilityRangeInfoType = mAccessibilityRangeInfoType;
    }
    
    public void setContinuousUpdates(final boolean mContinuousUpdates) {
        this.mContinuousUpdates = mContinuousUpdates;
    }
    
    public void setMax(final int mMax) {
        if (mMax != this.mMax) {
            this.mMax = mMax;
            this.notifyChanged();
        }
    }
    
    public void setMin(final int mMin) {
        if (mMin != this.mMin) {
            this.mMin = mMin;
            this.notifyChanged();
        }
    }
    
    public void setProgress(final int n) {
        this.setProgress(n, true);
    }
    
    public void setSeekBarContentDescription(final CharSequence charSequence) {
        this.mSeekBarContentDescription = charSequence;
        if (this.mSeekBar != null) {
            this.mSeekBar.setContentDescription(charSequence);
        }
    }
    
    public void setShouldBlink(final boolean mShouldBlink) {
        this.mShouldBlink = mShouldBlink;
        this.notifyChanged();
    }
    
    void syncProgress(final SeekBar seekBar) {
        final int progress = seekBar.getProgress();
        if (progress != this.mProgress) {
            if (this.callChangeListener(progress)) {
                this.setProgress(progress, false);
            }
            else {
                seekBar.setProgress(this.mProgress);
            }
        }
    }
    
    private static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        int max;
        int min;
        int progress;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel) {
            super(parcel);
            this.progress = parcel.readInt();
            this.max = parcel.readInt();
            this.min = parcel.readInt();
        }
        
        public SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.progress);
            parcel.writeInt(this.max);
            parcel.writeInt(this.min);
        }
    }
}
