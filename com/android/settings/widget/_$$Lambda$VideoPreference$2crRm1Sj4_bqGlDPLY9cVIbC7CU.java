package com.android.settings.widget;

import android.view.Surface;
import android.graphics.SurfaceTexture;
import android.view.TextureView$SurfaceTextureListener;
import android.view.View.OnClickListener;
import android.view.TextureView;
import android.support.v7.preference.PreferenceViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.util.Log;
import android.media.MediaPlayer$OnSeekCompleteListener;
import android.net.Uri.Builder;
import com.android.settings.R;
import android.util.AttributeSet;
import android.net.Uri;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.media.MediaPlayer;
import android.media.MediaPlayer$OnPreparedListener;

public final class _$$Lambda$VideoPreference$2crRm1Sj4_bqGlDPLY9cVIbC7CU implements MediaPlayer$OnPreparedListener
{
    public final void onPrepared(final MediaPlayer mediaPlayer) {
        VideoPreference.lambda$new$1(mediaPlayer);
    }
}
