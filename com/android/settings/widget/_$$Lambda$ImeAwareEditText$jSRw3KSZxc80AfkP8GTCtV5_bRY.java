package com.android.settings.widget;

import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.EditText;

public final class _$$Lambda$ImeAwareEditText$jSRw3KSZxc80AfkP8GTCtV5_bRY implements Runnable
{
    @Override
    public final void run() {
        this.f$0.showSoftInputIfNecessary();
    }
}
