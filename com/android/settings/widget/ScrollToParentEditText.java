package com.android.settings.widget;

import android.view.ViewParent;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;

public class ScrollToParentEditText extends ImeAwareEditText
{
    private Rect mRect;
    
    public ScrollToParentEditText(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRect = new Rect();
    }
    
    public boolean requestRectangleOnScreen(final Rect rect, final boolean b) {
        final ViewParent parent = this.getParent();
        if (parent instanceof View) {
            ((View)parent).getDrawingRect(this.mRect);
            return ((View)parent).requestRectangleOnScreen(this.mRect, b);
        }
        return super.requestRectangleOnScreen(rect, b);
    }
}
