package com.android.settings.widget;

import android.text.TextUtils;
import android.content.Context;

public abstract class SummaryUpdater
{
    protected final Context mContext;
    private final OnSummaryChangeListener mListener;
    private String mSummary;
    
    public SummaryUpdater(final Context mContext, final OnSummaryChangeListener mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
    }
    
    protected abstract String getSummary();
    
    protected void notifyChangeIfNeeded() {
        final String summary = this.getSummary();
        if (!TextUtils.equals((CharSequence)this.mSummary, (CharSequence)summary)) {
            this.mSummary = summary;
            if (this.mListener != null) {
                this.mListener.onSummaryChanged(summary);
            }
        }
    }
    
    public interface OnSummaryChangeListener
    {
        void onSummaryChanged(final String p0);
    }
}
