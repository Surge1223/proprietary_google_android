package com.android.settings.widget;

import android.view.ViewDebug$IntToString;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.RemotableViewMethod;
import android.view.Gravity;
import android.view.ViewHierarchyEncoder;
import android.graphics.Canvas;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.view.View$MeasureSpec;
import android.content.res.TypedArray;
import com.android.internal.R$styleable;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewDebug$FlagToString;
import android.graphics.drawable.Drawable;
import android.view.ViewDebug$ExportedProperty;
import android.view.ViewGroup;

public class MatchParentShrinkingLinearLayout extends ViewGroup
{
    @ViewDebug$ExportedProperty(category = "layout")
    private boolean mBaselineAligned;
    @ViewDebug$ExportedProperty(category = "layout")
    private int mBaselineAlignedChildIndex;
    @ViewDebug$ExportedProperty(category = "measurement")
    private int mBaselineChildTop;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;
    @ViewDebug$ExportedProperty(category = "measurement", flagMapping = { @ViewDebug$FlagToString(equals = -1, mask = -1, name = "NONE"), @ViewDebug$FlagToString(equals = 0, mask = 0, name = "NONE"), @ViewDebug$FlagToString(equals = 48, mask = 48, name = "TOP"), @ViewDebug$FlagToString(equals = 80, mask = 80, name = "BOTTOM"), @ViewDebug$FlagToString(equals = 3, mask = 3, name = "LEFT"), @ViewDebug$FlagToString(equals = 5, mask = 5, name = "RIGHT"), @ViewDebug$FlagToString(equals = 8388611, mask = 8388611, name = "START"), @ViewDebug$FlagToString(equals = 8388613, mask = 8388613, name = "END"), @ViewDebug$FlagToString(equals = 16, mask = 16, name = "CENTER_VERTICAL"), @ViewDebug$FlagToString(equals = 112, mask = 112, name = "FILL_VERTICAL"), @ViewDebug$FlagToString(equals = 1, mask = 1, name = "CENTER_HORIZONTAL"), @ViewDebug$FlagToString(equals = 7, mask = 7, name = "FILL_HORIZONTAL"), @ViewDebug$FlagToString(equals = 17, mask = 17, name = "CENTER"), @ViewDebug$FlagToString(equals = 119, mask = 119, name = "FILL"), @ViewDebug$FlagToString(equals = 8388608, mask = 8388608, name = "RELATIVE") }, formatToHexString = true)
    private int mGravity;
    private int mLayoutDirection;
    private int[] mMaxAscent;
    private int[] mMaxDescent;
    @ViewDebug$ExportedProperty(category = "measurement")
    private int mOrientation;
    private int mShowDividers;
    @ViewDebug$ExportedProperty(category = "measurement")
    private int mTotalLength;
    @ViewDebug$ExportedProperty(category = "layout")
    private boolean mUseLargestChild;
    @ViewDebug$ExportedProperty(category = "layout")
    private float mWeightSum;
    
    public MatchParentShrinkingLinearLayout(final Context context) {
        this(context, null);
    }
    
    public MatchParentShrinkingLinearLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MatchParentShrinkingLinearLayout(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public MatchParentShrinkingLinearLayout(final Context context, final AttributeSet set, int n, final int n2) {
        super(context, set, n, n2);
        this.mBaselineAligned = true;
        this.mBaselineAlignedChildIndex = -1;
        this.mBaselineChildTop = 0;
        this.mGravity = 8388659;
        this.mLayoutDirection = -1;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R$styleable.LinearLayout, n, n2);
        n = obtainStyledAttributes.getInt(1, -1);
        if (n >= 0) {
            this.setOrientation(n);
        }
        n = obtainStyledAttributes.getInt(0, -1);
        if (n >= 0) {
            this.setGravity(n);
        }
        final boolean boolean1 = obtainStyledAttributes.getBoolean(2, true);
        if (!boolean1) {
            this.setBaselineAligned(boolean1);
        }
        this.mWeightSum = obtainStyledAttributes.getFloat(4, -1.0f);
        this.mBaselineAlignedChildIndex = obtainStyledAttributes.getInt(3, -1);
        this.mUseLargestChild = obtainStyledAttributes.getBoolean(6, false);
        this.setDividerDrawable(obtainStyledAttributes.getDrawable(5));
        this.mShowDividers = obtainStyledAttributes.getInt(7, 0);
        this.mDividerPadding = obtainStyledAttributes.getDimensionPixelSize(8, 0);
        obtainStyledAttributes.recycle();
    }
    
    private void forceUniformWidth(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.getMeasuredWidth(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                if (layoutParams.width == -1) {
                    final int height = layoutParams.height;
                    layoutParams.height = virtualChild.getMeasuredHeight();
                    this.measureChildWithMargins(virtualChild, measureSpec, 0, n2, 0);
                    layoutParams.height = height;
                }
            }
        }
    }
    
    private void setChildFrame(final View view, final int n, final int n2, final int n3, final int n4) {
        view.layout(n, n2, n + n3, n2 + n4);
    }
    
    protected boolean checkLayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        return viewGroup.LayoutParams instanceof LayoutParams;
    }
    
    void drawDividersHorizontal(final Canvas canvas) {
        final int virtualChildCount = this.getVirtualChildCount();
        final boolean layoutRtl = this.isLayoutRtl();
        for (int i = 0; i < virtualChildCount; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild != null && virtualChild.getVisibility() != 8 && this.hasDividerBeforeChildAt(i)) {
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                int n;
                if (layoutRtl) {
                    n = virtualChild.getRight() + layoutParams.rightMargin;
                }
                else {
                    n = virtualChild.getLeft() - layoutParams.leftMargin - this.mDividerWidth;
                }
                this.drawVerticalDivider(canvas, n);
            }
        }
        if (this.hasDividerBeforeChildAt(virtualChildCount)) {
            final View virtualChild2 = this.getVirtualChildAt(virtualChildCount - 1);
            int paddingLeft;
            if (virtualChild2 == null) {
                if (layoutRtl) {
                    paddingLeft = this.getPaddingLeft();
                }
                else {
                    paddingLeft = this.getWidth() - this.getPaddingRight() - this.mDividerWidth;
                }
            }
            else {
                final LayoutParams layoutParams2 = (LayoutParams)virtualChild2.getLayoutParams();
                if (layoutRtl) {
                    paddingLeft = virtualChild2.getLeft() - layoutParams2.leftMargin - this.mDividerWidth;
                }
                else {
                    paddingLeft = virtualChild2.getRight() + layoutParams2.rightMargin;
                }
            }
            this.drawVerticalDivider(canvas, paddingLeft);
        }
    }
    
    void drawDividersVertical(final Canvas canvas) {
        final int virtualChildCount = this.getVirtualChildCount();
        for (int i = 0; i < virtualChildCount; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild != null && virtualChild.getVisibility() != 8 && this.hasDividerBeforeChildAt(i)) {
                this.drawHorizontalDivider(canvas, virtualChild.getTop() - ((LayoutParams)virtualChild.getLayoutParams()).topMargin - this.mDividerHeight);
            }
        }
        if (this.hasDividerBeforeChildAt(virtualChildCount)) {
            final View virtualChild2 = this.getVirtualChildAt(virtualChildCount - 1);
            int n;
            if (virtualChild2 == null) {
                n = this.getHeight() - this.getPaddingBottom() - this.mDividerHeight;
            }
            else {
                n = virtualChild2.getBottom() + ((LayoutParams)virtualChild2.getLayoutParams()).bottomMargin;
            }
            this.drawHorizontalDivider(canvas, n);
        }
    }
    
    void drawHorizontalDivider(final Canvas canvas, final int n) {
        this.mDivider.setBounds(this.getPaddingLeft() + this.mDividerPadding, n, this.getWidth() - this.getPaddingRight() - this.mDividerPadding, this.mDividerHeight + n);
        this.mDivider.draw(canvas);
    }
    
    void drawVerticalDivider(final Canvas canvas, final int n) {
        this.mDivider.setBounds(n, this.getPaddingTop() + this.mDividerPadding, this.mDividerWidth + n, this.getHeight() - this.getPaddingBottom() - this.mDividerPadding);
        this.mDivider.draw(canvas);
    }
    
    protected void encodeProperties(final ViewHierarchyEncoder viewHierarchyEncoder) {
        super.encodeProperties(viewHierarchyEncoder);
        viewHierarchyEncoder.addProperty("layout:baselineAligned", this.mBaselineAligned);
        viewHierarchyEncoder.addProperty("layout:baselineAlignedChildIndex", this.mBaselineAlignedChildIndex);
        viewHierarchyEncoder.addProperty("measurement:baselineChildTop", this.mBaselineChildTop);
        viewHierarchyEncoder.addProperty("measurement:orientation", this.mOrientation);
        viewHierarchyEncoder.addProperty("measurement:gravity", this.mGravity);
        viewHierarchyEncoder.addProperty("measurement:totalLength", this.mTotalLength);
        viewHierarchyEncoder.addProperty("layout:totalLength", this.mTotalLength);
        viewHierarchyEncoder.addProperty("layout:useLargestChild", this.mUseLargestChild);
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        if (this.mOrientation == 0) {
            return new LayoutParams(-2, -2);
        }
        if (this.mOrientation == 1) {
            return new LayoutParams(-1, -2);
        }
        return null;
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    protected LayoutParams generateLayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        return new LayoutParams(viewGroup.LayoutParams);
    }
    
    public CharSequence getAccessibilityClassName() {
        return MatchParentShrinkingLinearLayout.class.getName();
    }
    
    public int getBaseline() {
        if (this.mBaselineAlignedChildIndex < 0) {
            return super.getBaseline();
        }
        if (this.getChildCount() <= this.mBaselineAlignedChildIndex) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        final View child = this.getChildAt(this.mBaselineAlignedChildIndex);
        final int baseline = child.getBaseline();
        if (baseline != -1) {
            int mBaselineChildTop;
            final int n = mBaselineChildTop = this.mBaselineChildTop;
            if (this.mOrientation == 1) {
                final int n2 = this.mGravity & 0x70;
                mBaselineChildTop = n;
                if (n2 != 48) {
                    if (n2 != 16) {
                        if (n2 != 80) {
                            mBaselineChildTop = n;
                        }
                        else {
                            mBaselineChildTop = this.mBottom - this.mTop - this.mPaddingBottom - this.mTotalLength;
                        }
                    }
                    else {
                        mBaselineChildTop = n + (this.mBottom - this.mTop - this.mPaddingTop - this.mPaddingBottom - this.mTotalLength) / 2;
                    }
                }
            }
            return ((LayoutParams)child.getLayoutParams()).topMargin + mBaselineChildTop + baseline;
        }
        if (this.mBaselineAlignedChildIndex == 0) {
            return -1;
        }
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
    }
    
    public int getBaselineAlignedChildIndex() {
        return this.mBaselineAlignedChildIndex;
    }
    
    int getChildrenSkipCount(final View view, final int n) {
        return 0;
    }
    
    public Drawable getDividerDrawable() {
        return this.mDivider;
    }
    
    public int getDividerPadding() {
        return this.mDividerPadding;
    }
    
    public int getDividerWidth() {
        return this.mDividerWidth;
    }
    
    int getLocationOffset(final View view) {
        return 0;
    }
    
    int getNextLocationOffset(final View view) {
        return 0;
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public int getShowDividers() {
        return this.mShowDividers;
    }
    
    View getVirtualChildAt(final int n) {
        return this.getChildAt(n);
    }
    
    int getVirtualChildCount() {
        return this.getChildCount();
    }
    
    public float getWeightSum() {
        return this.mWeightSum;
    }
    
    protected boolean hasDividerBeforeChildAt(int n) {
        final boolean b = false;
        boolean b2 = false;
        if (n == 0) {
            if ((this.mShowDividers & 0x1) != 0x0) {
                b2 = true;
            }
            return b2;
        }
        if (n == this.getChildCount()) {
            boolean b3 = b;
            if ((this.mShowDividers & 0x4) != 0x0) {
                b3 = true;
            }
            return b3;
        }
        if ((this.mShowDividers & 0x2) != 0x0) {
            final boolean b4 = false;
            --n;
            boolean b5;
            while (true) {
                b5 = b4;
                if (n < 0) {
                    break;
                }
                if (this.getChildAt(n).getVisibility() != 8) {
                    b5 = true;
                    break;
                }
                --n;
            }
            return b5;
        }
        return false;
    }
    
    void layoutHorizontal(int n, int absoluteGravity, int i, int n2) {
        final boolean layoutRtl = this.isLayoutRtl();
        final int mPaddingTop = this.mPaddingTop;
        final int n3 = n2 - absoluteGravity;
        final int mPaddingBottom = this.mPaddingBottom;
        final int mPaddingBottom2 = this.mPaddingBottom;
        final int virtualChildCount = this.getVirtualChildCount();
        final int n4 = this.mGravity & 0x800007;
        final int mGravity = this.mGravity;
        final boolean mBaselineAligned = this.mBaselineAligned;
        final int[] mMaxAscent = this.mMaxAscent;
        final int[] mMaxDescent = this.mMaxDescent;
        absoluteGravity = Gravity.getAbsoluteGravity(n4, this.getLayoutDirection());
        if (absoluteGravity != 1) {
            if (absoluteGravity != 5) {
                n = this.mPaddingLeft;
            }
            else {
                n = this.mPaddingLeft + i - n - this.mTotalLength;
            }
        }
        else {
            n = this.mPaddingLeft + (i - n - this.mTotalLength) / 2;
        }
        n2 = 0;
        int n5 = 1;
        if (layoutRtl) {
            n2 = virtualChildCount - 1;
            n5 = -1;
        }
        i = 0;
        absoluteGravity = n;
        while (i < virtualChildCount) {
            final int n6 = n2 + n5 * i;
            final View virtualChild = this.getVirtualChildAt(n6);
            if (virtualChild == null) {
                absoluteGravity += this.measureNullChild(n6);
            }
            else if (virtualChild.getVisibility() != 8) {
                final int measuredWidth = virtualChild.getMeasuredWidth();
                final int measuredHeight = virtualChild.getMeasuredHeight();
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                int baseline;
                if (mBaselineAligned && layoutParams.height != -1) {
                    baseline = virtualChild.getBaseline();
                }
                else {
                    baseline = -1;
                }
                if ((n = layoutParams.gravity) < 0) {
                    n = (mGravity & 0x70);
                }
                n &= 0x70;
                if (n != 16) {
                    if (n != 48) {
                        if (n != 80) {
                            n = mPaddingTop;
                        }
                        else {
                            final int n7 = n = n3 - mPaddingBottom - measuredHeight - layoutParams.bottomMargin;
                            if (baseline != -1) {
                                n = virtualChild.getMeasuredHeight();
                                n = n7 - (mMaxDescent[2] - (n - baseline));
                            }
                        }
                    }
                    else {
                        final int n8 = n = layoutParams.topMargin + mPaddingTop;
                        if (baseline != -1) {
                            n = n8 + (mMaxAscent[1] - baseline);
                        }
                    }
                }
                else {
                    n = (n3 - mPaddingTop - mPaddingBottom2 - measuredHeight) / 2 + mPaddingTop + layoutParams.topMargin - layoutParams.bottomMargin;
                }
                int n9 = absoluteGravity;
                if (this.hasDividerBeforeChildAt(n6)) {
                    n9 = absoluteGravity + this.mDividerWidth;
                }
                absoluteGravity = n9 + layoutParams.leftMargin;
                this.setChildFrame(virtualChild, absoluteGravity + this.getLocationOffset(virtualChild), n, measuredWidth, measuredHeight);
                absoluteGravity += measuredWidth + layoutParams.rightMargin + this.getNextLocationOffset(virtualChild);
                i += this.getChildrenSkipCount(virtualChild, n6);
            }
            ++i;
        }
    }
    
    void layoutVertical(int mPaddingTop, int n, int n2, int n3) {
        final int mPaddingLeft = this.mPaddingLeft;
        final int n4 = n2 - mPaddingTop;
        final int mPaddingRight = this.mPaddingRight;
        final int mPaddingRight2 = this.mPaddingRight;
        final int virtualChildCount = this.getVirtualChildCount();
        final int n5 = this.mGravity & 0x70;
        final int mGravity = this.mGravity;
        if (n5 != 16) {
            if (n5 != 80) {
                mPaddingTop = this.mPaddingTop;
            }
            else {
                mPaddingTop = this.mPaddingTop + n3 - n - this.mTotalLength;
            }
        }
        else {
            mPaddingTop = this.mPaddingTop + (n3 - n - this.mTotalLength) / 2;
        }
        n = 0;
        n2 = mPaddingLeft;
        n3 = n5;
        while (true) {
            int n6 = n;
            if (n6 >= virtualChildCount) {
                break;
            }
            final View virtualChild = this.getVirtualChildAt(n6);
            if (virtualChild == null) {
                mPaddingTop += this.measureNullChild(n6);
            }
            else if (virtualChild.getVisibility() != 8) {
                final int measuredWidth = virtualChild.getMeasuredWidth();
                final int measuredHeight = virtualChild.getMeasuredHeight();
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                if ((n = layoutParams.gravity) < 0) {
                    n = (mGravity & 0x800007);
                }
                n = (Gravity.getAbsoluteGravity(n, this.getLayoutDirection()) & 0x7);
                if (n != 1) {
                    if (n != 5) {
                        n = layoutParams.leftMargin + n2;
                    }
                    else {
                        n = n4 - mPaddingRight - measuredWidth - layoutParams.rightMargin;
                    }
                }
                else {
                    n = (n4 - mPaddingLeft - mPaddingRight2 - measuredWidth) / 2 + n2 + layoutParams.leftMargin - layoutParams.rightMargin;
                }
                int n7 = mPaddingTop;
                if (this.hasDividerBeforeChildAt(n6)) {
                    n7 = mPaddingTop + this.mDividerHeight;
                }
                mPaddingTop = n7 + layoutParams.topMargin;
                this.setChildFrame(virtualChild, n, mPaddingTop + this.getLocationOffset(virtualChild), measuredWidth, measuredHeight);
                final int bottomMargin = layoutParams.bottomMargin;
                n = this.getNextLocationOffset(virtualChild);
                n6 += this.getChildrenSkipCount(virtualChild, n6);
                mPaddingTop += measuredHeight + bottomMargin + n;
            }
            n = n6 + 1;
        }
    }
    
    void measureChildBeforeLayout(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        this.measureChildWithMargins(view, n2, n3, n4, n5);
    }
    
    void measureHorizontal(final int n, final int n2) {
        throw new IllegalStateException("horizontal mode not supported.");
    }
    
    int measureNullChild(final int n) {
        return 0;
    }
    
    void measureVertical(final int n, final int n2) {
        this.mTotalLength = 0;
        int n3 = 0;
        float mWeightSum = 0.0f;
        final int virtualChildCount = this.getVirtualChildCount();
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        boolean b = false;
        final int mBaselineAlignedChildIndex = this.mBaselineAlignedChildIndex;
        final boolean mUseLargestChild = this.mUseLargestChild;
        boolean b2 = false;
        int n4 = 0;
        int max = 0;
        int i = 0;
        int n5 = 0;
        int max2 = Integer.MIN_VALUE;
        int n6 = 1;
        while (i < virtualChildCount) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild == null) {
                this.mTotalLength += this.measureNullChild(i);
            }
            else if (virtualChild.getVisibility() == 8) {
                i += this.getChildrenSkipCount(virtualChild, i);
            }
            else {
                if (this.hasDividerBeforeChildAt(i)) {
                    this.mTotalLength += this.mDividerHeight;
                }
                final LayoutParams layoutParams = (LayoutParams)virtualChild.getLayoutParams();
                mWeightSum += layoutParams.weight;
                if (mode2 == 1073741824 && layoutParams.height == 0 && layoutParams.weight > 0.0f) {
                    final int mTotalLength = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength, layoutParams.topMargin + mTotalLength + layoutParams.bottomMargin);
                    b = true;
                }
                else {
                    int height;
                    final int n7 = height = Integer.MIN_VALUE;
                    if (layoutParams.height == 0) {
                        height = n7;
                        if (layoutParams.weight > 0.0f) {
                            height = 0;
                            layoutParams.height = -2;
                        }
                    }
                    int mTotalLength2;
                    if (mWeightSum == 0.0f) {
                        mTotalLength2 = this.mTotalLength;
                    }
                    else {
                        mTotalLength2 = 0;
                    }
                    final View view = virtualChild;
                    this.measureChildBeforeLayout(virtualChild, i, n, 0, n2, mTotalLength2);
                    if (height != Integer.MIN_VALUE) {
                        layoutParams.height = height;
                    }
                    final LayoutParams layoutParams2 = layoutParams;
                    final int measuredHeight = view.getMeasuredHeight();
                    final int mTotalLength3 = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength3, mTotalLength3 + measuredHeight + layoutParams2.topMargin + layoutParams2.bottomMargin + this.getNextLocationOffset(view));
                    if (mUseLargestChild) {
                        max2 = Math.max(measuredHeight, max2);
                    }
                }
                final int n8 = n5;
                if (mBaselineAlignedChildIndex >= 0 && mBaselineAlignedChildIndex == i + 1) {
                    this.mBaselineChildTop = this.mTotalLength;
                }
                final int n9 = i;
                if (n9 < mBaselineAlignedChildIndex && layoutParams.weight > 0.0f) {
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                }
                boolean b3 = false;
                if (mode != 1073741824 && layoutParams.width == -1) {
                    b2 = true;
                    b3 = true;
                }
                final int n10 = layoutParams.leftMargin + layoutParams.rightMargin;
                int n11 = virtualChild.getMeasuredWidth() + n10;
                max = Math.max(max, n11);
                final int combineMeasuredStates = combineMeasuredStates(n3, virtualChild.getMeasuredState());
                final boolean b4 = n6 != 0 && layoutParams.width == -1;
                int max3;
                int max4;
                if (layoutParams.weight > 0.0f) {
                    if (b3) {
                        n11 = n10;
                    }
                    max3 = Math.max(n8, n11);
                    max4 = n4;
                }
                else {
                    max3 = n8;
                    int n12;
                    if (b3) {
                        n12 = n10;
                    }
                    else {
                        n12 = n11;
                    }
                    max4 = Math.max(n4, n12);
                }
                final int n13 = n9 + this.getChildrenSkipCount(virtualChild, n9);
                final int n14 = max3;
                n4 = max4;
                n3 = combineMeasuredStates;
                n6 = (b4 ? 1 : 0);
                n5 = n14;
                i = n13;
            }
            ++i;
        }
        final int n15 = n4;
        final int n16 = n5;
        final int n17 = max2;
        if (this.mTotalLength > 0 && this.hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerHeight;
        }
        final int n18 = virtualChildCount;
        if (mUseLargestChild) {
            final int n19 = mode2;
            if (n19 == Integer.MIN_VALUE || n19 == 0) {
                this.mTotalLength = 0;
                for (int j = 0; j < n18; ++j) {
                    final View virtualChild2 = this.getVirtualChildAt(j);
                    if (virtualChild2 == null) {
                        this.mTotalLength += this.measureNullChild(j);
                    }
                    else if (virtualChild2.getVisibility() == 8) {
                        j += this.getChildrenSkipCount(virtualChild2, j);
                    }
                    else {
                        final LayoutParams layoutParams3 = (LayoutParams)virtualChild2.getLayoutParams();
                        final int mTotalLength4 = this.mTotalLength;
                        this.mTotalLength = Math.max(mTotalLength4, mTotalLength4 + n17 + layoutParams3.topMargin + layoutParams3.bottomMargin + this.getNextLocationOffset(virtualChild2));
                    }
                }
            }
        }
        final int n20 = mode2;
        this.mTotalLength += this.mPaddingTop + this.mPaddingBottom;
        final int resolveSizeAndState = resolveSizeAndState(Math.max(this.mTotalLength, this.getSuggestedMinimumHeight()), n2, 0);
        final int n21 = (resolveSizeAndState & 0xFFFFFF) - this.mTotalLength;
        int n25;
        int n26;
        int n27;
        if (!b && (n21 == 0 || mWeightSum <= 0.0f)) {
            final int max5 = Math.max(n15, n16);
            int n24;
            if (mUseLargestChild) {
                int n22 = max5;
                if (n20 != 1073741824) {
                    int n23 = 0;
                    while (true) {
                        n22 = max5;
                        if (n23 >= n18) {
                            break;
                        }
                        final View virtualChild3 = this.getVirtualChildAt(n23);
                        if (virtualChild3 != null) {
                            if (virtualChild3.getVisibility() != 8) {
                                if (((LayoutParams)virtualChild3.getLayoutParams()).weight > 0.0f) {
                                    virtualChild3.measure(View$MeasureSpec.makeMeasureSpec(virtualChild3.getMeasuredWidth(), 1073741824), View$MeasureSpec.makeMeasureSpec(n17, 1073741824));
                                }
                            }
                        }
                        ++n23;
                    }
                }
                n24 = n22;
            }
            else {
                n24 = max5;
            }
            n25 = max;
            n26 = n3;
            n27 = n24;
        }
        else {
            if (this.mWeightSum > 0.0f) {
                mWeightSum = this.mWeightSum;
            }
            this.mTotalLength = 0;
            final int n28 = 0;
            final int n29 = n21;
            int n30 = n3;
            n25 = max;
            int n31 = n15;
            int n32 = n29;
            final int n33 = n20;
            for (int k = n28; k < n18; ++k) {
                final View virtualChild4 = this.getVirtualChildAt(k);
                if (virtualChild4.getVisibility() != 8) {
                    final LayoutParams layoutParams4 = (LayoutParams)virtualChild4.getLayoutParams();
                    final float weight = layoutParams4.weight;
                    if (weight > 0.0f && n32 > 0) {
                        final int n34 = (int)(n32 * weight / mWeightSum);
                        final int mPaddingLeft = this.mPaddingLeft;
                        final int n35 = n32 - n34;
                        final int childMeasureSpec = getChildMeasureSpec(n, mPaddingLeft + this.mPaddingRight + layoutParams4.leftMargin + layoutParams4.rightMargin, layoutParams4.width);
                        if (layoutParams4.height == 0 && n33 == 1073741824) {
                            int n36;
                            if (n34 > 0) {
                                n36 = n34;
                            }
                            else {
                                n36 = 0;
                            }
                            virtualChild4.measure(childMeasureSpec, View$MeasureSpec.makeMeasureSpec(n36, 1073741824));
                        }
                        else {
                            int n37;
                            if ((n37 = virtualChild4.getMeasuredHeight() + n34) < 0) {
                                n37 = 0;
                            }
                            virtualChild4.measure(childMeasureSpec, View$MeasureSpec.makeMeasureSpec(n37, 1073741824));
                        }
                        n30 = combineMeasuredStates(n30, virtualChild4.getMeasuredState() & 0xFFFFFF00);
                        mWeightSum -= weight;
                        n32 = n35;
                    }
                    else if (n32 < 0 && layoutParams4.height == -1) {
                        final int childMeasureSpec2 = getChildMeasureSpec(n, this.mPaddingLeft + this.mPaddingRight + layoutParams4.leftMargin + layoutParams4.rightMargin, layoutParams4.width);
                        int n38;
                        if ((n38 = virtualChild4.getMeasuredHeight() + n32) < 0) {
                            n38 = 0;
                        }
                        final int measuredHeight2 = virtualChild4.getMeasuredHeight();
                        virtualChild4.measure(childMeasureSpec2, View$MeasureSpec.makeMeasureSpec(n38, 1073741824));
                        n30 = combineMeasuredStates(n30, virtualChild4.getMeasuredState() & 0xFFFFFF00);
                        n32 -= n38 - measuredHeight2;
                    }
                    final int n39 = layoutParams4.leftMargin + layoutParams4.rightMargin;
                    final int n40 = virtualChild4.getMeasuredWidth() + n39;
                    final int max6 = Math.max(n25, n40);
                    int n41;
                    if (mode != 1073741824 && layoutParams4.width == -1) {
                        n41 = n39;
                    }
                    else {
                        n41 = n40;
                    }
                    final int max7 = Math.max(n31, n41);
                    final boolean b5 = n6 != 0 && layoutParams4.width == -1;
                    final int mTotalLength5 = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength5, mTotalLength5 + virtualChild4.getMeasuredHeight() + layoutParams4.topMargin + layoutParams4.bottomMargin + this.getNextLocationOffset(virtualChild4));
                    n25 = max6;
                    n6 = (b5 ? 1 : 0);
                    n31 = max7;
                }
            }
            this.mTotalLength += this.mPaddingTop + this.mPaddingBottom;
            n27 = n31;
            n26 = n30;
        }
        int n42 = n25;
        if (n6 == 0) {
            n42 = n25;
            if (mode != 1073741824) {
                n42 = n27;
            }
        }
        this.setMeasuredDimension(resolveSizeAndState(Math.max(n42 + (this.mPaddingLeft + this.mPaddingRight), this.getSuggestedMinimumWidth()), n, n26), resolveSizeAndState);
        if (b2) {
            this.forceUniformWidth(n18, n2);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        if (this.mDivider == null) {
            return;
        }
        if (this.mOrientation == 1) {
            this.drawDividersVertical(canvas);
        }
        else {
            this.drawDividersHorizontal(canvas);
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        if (this.mOrientation == 1) {
            this.layoutVertical(n, n2, n3, n4);
        }
        else {
            this.layoutHorizontal(n, n2, n3, n4);
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        if (this.mOrientation == 1) {
            this.measureVertical(n, n2);
        }
        else {
            this.measureHorizontal(n, n2);
        }
    }
    
    public void onRtlPropertiesChanged(final int mLayoutDirection) {
        super.onRtlPropertiesChanged(mLayoutDirection);
        if (mLayoutDirection != this.mLayoutDirection) {
            this.mLayoutDirection = mLayoutDirection;
            if (this.mOrientation == 0) {
                this.requestLayout();
            }
        }
    }
    
    @RemotableViewMethod
    public void setBaselineAligned(final boolean mBaselineAligned) {
        this.mBaselineAligned = mBaselineAligned;
    }
    
    @RemotableViewMethod
    public void setBaselineAlignedChildIndex(final int mBaselineAlignedChildIndex) {
        if (mBaselineAlignedChildIndex >= 0 && mBaselineAlignedChildIndex < this.getChildCount()) {
            this.mBaselineAlignedChildIndex = mBaselineAlignedChildIndex;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("base aligned child index out of range (0, ");
        sb.append(this.getChildCount());
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void setDividerDrawable(final Drawable mDivider) {
        if (mDivider == this.mDivider) {
            return;
        }
        this.mDivider = mDivider;
        boolean willNotDraw = false;
        if (mDivider != null) {
            this.mDividerWidth = mDivider.getIntrinsicWidth();
            this.mDividerHeight = mDivider.getIntrinsicHeight();
        }
        else {
            this.mDividerWidth = 0;
            this.mDividerHeight = 0;
        }
        if (mDivider == null) {
            willNotDraw = true;
        }
        this.setWillNotDraw(willNotDraw);
        this.requestLayout();
    }
    
    public void setDividerPadding(final int mDividerPadding) {
        this.mDividerPadding = mDividerPadding;
    }
    
    @RemotableViewMethod
    public void setGravity(int mGravity) {
        if (this.mGravity != mGravity) {
            int n = mGravity;
            if ((0x800007 & mGravity) == 0x0) {
                n = (mGravity | 0x800003);
            }
            mGravity = n;
            if ((n & 0x70) == 0x0) {
                mGravity = (n | 0x30);
            }
            this.mGravity = mGravity;
            this.requestLayout();
        }
    }
    
    @RemotableViewMethod
    public void setHorizontalGravity(int n) {
        n &= 0x800007;
        if ((0x800007 & this.mGravity) != n) {
            this.mGravity = ((this.mGravity & 0xFF7FFFF8) | n);
            this.requestLayout();
        }
    }
    
    @RemotableViewMethod
    public void setMeasureWithLargestChildEnabled(final boolean mUseLargestChild) {
        this.mUseLargestChild = mUseLargestChild;
    }
    
    public void setOrientation(final int mOrientation) {
        if (this.mOrientation != mOrientation) {
            this.mOrientation = mOrientation;
            this.requestLayout();
        }
    }
    
    public void setShowDividers(final int mShowDividers) {
        if (mShowDividers != this.mShowDividers) {
            this.requestLayout();
        }
        this.mShowDividers = mShowDividers;
    }
    
    @RemotableViewMethod
    public void setVerticalGravity(int n) {
        n &= 0x70;
        if ((this.mGravity & 0x70) != n) {
            this.mGravity = ((this.mGravity & 0xFFFFFF8F) | n);
            this.requestLayout();
        }
    }
    
    @RemotableViewMethod
    public void setWeightSum(final float n) {
        this.mWeightSum = Math.max(0.0f, n);
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        @ViewDebug$ExportedProperty(category = "layout", mapping = { @ViewDebug$IntToString(from = -1, to = "NONE"), @ViewDebug$IntToString(from = 0, to = "NONE"), @ViewDebug$IntToString(from = 48, to = "TOP"), @ViewDebug$IntToString(from = 80, to = "BOTTOM"), @ViewDebug$IntToString(from = 3, to = "LEFT"), @ViewDebug$IntToString(from = 5, to = "RIGHT"), @ViewDebug$IntToString(from = 8388611, to = "START"), @ViewDebug$IntToString(from = 8388613, to = "END"), @ViewDebug$IntToString(from = 16, to = "CENTER_VERTICAL"), @ViewDebug$IntToString(from = 112, to = "FILL_VERTICAL"), @ViewDebug$IntToString(from = 1, to = "CENTER_HORIZONTAL"), @ViewDebug$IntToString(from = 7, to = "FILL_HORIZONTAL"), @ViewDebug$IntToString(from = 17, to = "CENTER"), @ViewDebug$IntToString(from = 119, to = "FILL") })
        public int gravity;
        @ViewDebug$ExportedProperty(category = "layout")
        public float weight;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.gravity = -1;
            this.weight = 0.0f;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.gravity = -1;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R$styleable.LinearLayout_Layout);
            this.weight = obtainStyledAttributes.getFloat(3, 0.0f);
            this.gravity = obtainStyledAttributes.getInt(0, -1);
            obtainStyledAttributes.recycle();
        }
        
        public LayoutParams(final ViewGroup.LayoutParams viewGroup.LayoutParams) {
            super(viewGroup.LayoutParams);
            this.gravity = -1;
        }
        
        public String debug(final String s) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append("MatchParentShrinkingLinearLayout.LayoutParams={width=");
            sb.append(sizeToString(this.width));
            sb.append(", height=");
            sb.append(sizeToString(this.height));
            sb.append(" weight=");
            sb.append(this.weight);
            sb.append("}");
            return sb.toString();
        }
        
        protected void encodeProperties(final ViewHierarchyEncoder viewHierarchyEncoder) {
            super.encodeProperties(viewHierarchyEncoder);
            viewHierarchyEncoder.addProperty("layout:weight", this.weight);
            viewHierarchyEncoder.addProperty("layout:gravity", this.gravity);
        }
    }
}
