package com.android.settings.widget;

import com.android.settingslib.RestrictedLockUtils;

public abstract class SwitchWidgetController
{
    protected OnSwitchChangeListener mListener;
    
    public abstract boolean isChecked();
    
    public abstract void setChecked(final boolean p0);
    
    public abstract void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin p0);
    
    public abstract void setEnabled(final boolean p0);
    
    public void setListener(final OnSwitchChangeListener mListener) {
        this.mListener = mListener;
    }
    
    public void setupView() {
    }
    
    public abstract void startListening();
    
    public abstract void stopListening();
    
    public void teardownView() {
    }
    
    public abstract void updateTitle(final boolean p0);
    
    public interface OnSwitchChangeListener
    {
        boolean onSwitchToggled(final boolean p0);
    }
}
