package com.android.settings.widget;

import android.view.View;
import android.widget.TextView;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.CheckBoxPreference;

public class RadioButtonPreference extends CheckBoxPreference
{
    private OnClickListener mListener;
    
    public RadioButtonPreference(final Context context) {
        this(context, null);
    }
    
    public RadioButtonPreference(final Context context, final AttributeSet set) {
        this(context, set, TypedArrayUtils.getAttr(context, R.attr.preferenceStyle, 16842894));
    }
    
    public RadioButtonPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mListener = null;
        this.setWidgetLayoutResource(2131558684);
        this.setLayoutResource(2131558669);
        this.setIconSpaceReserved(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362671);
        if (viewById != null) {
            int visibility;
            if (TextUtils.isEmpty(this.getSummary())) {
                visibility = 8;
            }
            else {
                visibility = 0;
            }
            viewById.setVisibility(visibility);
        }
        final TextView textView = (TextView)preferenceViewHolder.findViewById(16908310);
        if (textView != null) {
            textView.setSingleLine(false);
            textView.setMaxLines(3);
        }
    }
    
    public void onClick() {
        if (this.mListener != null) {
            this.mListener.onRadioButtonClicked(this);
        }
    }
    
    public void setOnClickListener(final OnClickListener mListener) {
        this.mListener = mListener;
    }
    
    public interface OnClickListener
    {
        void onRadioButtonClicked(final RadioButtonPreference p0);
    }
}
