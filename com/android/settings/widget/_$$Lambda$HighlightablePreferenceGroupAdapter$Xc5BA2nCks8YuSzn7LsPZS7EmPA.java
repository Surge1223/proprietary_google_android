package com.android.settings.widget;

import android.support.v7.preference.PreferenceViewHolder;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import com.android.settings.SettingsPreferenceFragment;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.animation.ArgbEvaluator;
import android.util.Log;
import android.view.View;
import android.content.Context;
import android.util.TypedValue;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceGroupAdapter;
import android.support.v7.widget.RecyclerView;

public final class _$$Lambda$HighlightablePreferenceGroupAdapter$Xc5BA2nCks8YuSzn7LsPZS7EmPA implements Runnable
{
    @Override
    public final void run() {
        HighlightablePreferenceGroupAdapter.lambda$requestHighlight$0(this.f$0, this.f$1);
    }
}
