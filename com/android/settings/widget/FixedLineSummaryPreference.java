package com.android.settings.widget;

import android.text.TextUtils$TruncateAt;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.res.TypedArray;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class FixedLineSummaryPreference extends Preference
{
    private int mSummaryLineCount;
    
    public FixedLineSummaryPreference(final Context context, final AttributeSet set) {
        super(context, set);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.FixedLineSummaryPreference, 0, 0);
        if (obtainStyledAttributes.hasValue(0)) {
            this.mSummaryLineCount = obtainStyledAttributes.getInteger(0, 1);
        }
        else {
            this.mSummaryLineCount = 1;
        }
        obtainStyledAttributes.recycle();
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(16908304);
        if (textView != null) {
            textView.setMinLines(this.mSummaryLineCount);
            textView.setMaxLines(this.mSummaryLineCount);
            textView.setEllipsize(TextUtils$TruncateAt.END);
        }
    }
}
