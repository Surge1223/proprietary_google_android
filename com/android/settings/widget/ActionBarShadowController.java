package com.android.settings.widget;

import android.app.ActionBar;
import android.view.View;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class ActionBarShadowController implements LifecycleObserver, OnStart, OnStop
{
    static final float ELEVATION_HIGH = 8.0f;
    static final float ELEVATION_LOW = 0.0f;
    private boolean isScrollWatcherAttached;
    private RecyclerView mRecyclerView;
    ScrollChangeWatcher mScrollChangeWatcher;
    
    private ActionBarShadowController(final Activity activity, final Lifecycle lifecycle, final RecyclerView mRecyclerView) {
        this.mScrollChangeWatcher = new ScrollChangeWatcher(activity);
        this.mRecyclerView = mRecyclerView;
        this.attachScrollWatcher();
        lifecycle.addObserver(this);
    }
    
    private ActionBarShadowController(final View view, final Lifecycle lifecycle, final RecyclerView mRecyclerView) {
        this.mScrollChangeWatcher = new ScrollChangeWatcher(view);
        this.mRecyclerView = mRecyclerView;
        this.attachScrollWatcher();
        lifecycle.addObserver(this);
    }
    
    private void attachScrollWatcher() {
        if (!this.isScrollWatcherAttached) {
            this.isScrollWatcherAttached = true;
            this.mRecyclerView.addOnScrollListener((RecyclerView.OnScrollListener)this.mScrollChangeWatcher);
            this.mScrollChangeWatcher.updateDropShadow((View)this.mRecyclerView);
        }
    }
    
    public static ActionBarShadowController attachToRecyclerView(final Activity activity, final Lifecycle lifecycle, final RecyclerView recyclerView) {
        return new ActionBarShadowController(activity, lifecycle, recyclerView);
    }
    
    public static ActionBarShadowController attachToRecyclerView(final View view, final Lifecycle lifecycle, final RecyclerView recyclerView) {
        return new ActionBarShadowController(view, lifecycle, recyclerView);
    }
    
    private void detachScrollWatcher() {
        this.mRecyclerView.removeOnScrollListener((RecyclerView.OnScrollListener)this.mScrollChangeWatcher);
        this.isScrollWatcherAttached = false;
    }
    
    @Override
    public void onStart() {
        this.attachScrollWatcher();
    }
    
    @Override
    public void onStop() {
        this.detachScrollWatcher();
    }
    
    final class ScrollChangeWatcher extends OnScrollListener
    {
        private final Activity mActivity;
        private final View mAnchorView;
        
        public ScrollChangeWatcher(final Activity mActivity) {
            this.mActivity = mActivity;
            this.mAnchorView = null;
        }
        
        public ScrollChangeWatcher(final View mAnchorView) {
            this.mAnchorView = mAnchorView;
            this.mActivity = null;
        }
        
        @Override
        public void onScrolled(final RecyclerView recyclerView, final int n, final int n2) {
            this.updateDropShadow((View)recyclerView);
        }
        
        public void updateDropShadow(View view) {
            final boolean canScrollVertically = view.canScrollVertically(-1);
            view = this.mAnchorView;
            final float n = 0.0f;
            float elevation = 0.0f;
            if (view != null) {
                view = this.mAnchorView;
                if (canScrollVertically) {
                    elevation = 8.0f;
                }
                view.setElevation(elevation);
            }
            else if (this.mActivity != null) {
                final ActionBar actionBar = this.mActivity.getActionBar();
                if (actionBar != null) {
                    float elevation2 = n;
                    if (canScrollVertically) {
                        elevation2 = 8.0f;
                    }
                    actionBar.setElevation(elevation2);
                }
            }
        }
    }
}
