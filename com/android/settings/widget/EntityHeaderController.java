package com.android.settings.widget;

import android.app.ActionBar;
import android.graphics.drawable.ColorDrawable;
import com.android.settingslib.Utils;
import android.content.pm.PackageInfo;
import android.util.IconDrawableFactory;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.LayoutPreference;
import android.widget.ImageView;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.ImageButton;
import android.util.Log;
import com.android.settings.applications.AppInfoBase;
import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.overlay.FeatureFactory;
import android.support.v7.widget.RecyclerView;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.app.Fragment;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.content.Context;
import android.app.Activity;

public class EntityHeaderController
{
    private int mAction1;
    private int mAction2;
    private final Activity mActivity;
    private final Context mAppContext;
    private Intent mAppNotifPrefIntent;
    private View.OnClickListener mEditRuleNameOnClickListener;
    private final Fragment mFragment;
    private boolean mHasAppInfoLink;
    private final View mHeader;
    private Drawable mIcon;
    private String mIconContentDescription;
    private boolean mIsInstantApp;
    private CharSequence mLabel;
    private Lifecycle mLifecycle;
    private final int mMetricsCategory;
    private String mPackageName;
    private RecyclerView mRecyclerView;
    private CharSequence mSecondSummary;
    private CharSequence mSummary;
    private int mUid;
    
    private EntityHeaderController(final Activity mActivity, final Fragment mFragment, final View mHeader) {
        this.mUid = -10000;
        this.mActivity = mActivity;
        this.mAppContext = mActivity.getApplicationContext();
        this.mFragment = mFragment;
        this.mMetricsCategory = FeatureFactory.getFactory(this.mAppContext).getMetricsFeatureProvider().getMetricsCategory(mFragment);
        if (mHeader != null) {
            this.mHeader = mHeader;
        }
        else {
            this.mHeader = LayoutInflater.from(mFragment.getContext()).inflate(2131558743, (ViewGroup)null);
        }
    }
    
    private void bindAppInfoLink(final View view) {
        if (!this.mHasAppInfoLink) {
            return;
        }
        if (view != null && this.mPackageName != null && !this.mPackageName.equals("os") && this.mUid != -10000) {
            view.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    AppInfoBase.startAppInfoFragment(AppInfoDashboardFragment.class, 2131886405, EntityHeaderController.this.mPackageName, EntityHeaderController.this.mUid, EntityHeaderController.this.mFragment, 0, EntityHeaderController.this.mMetricsCategory);
                }
            });
            return;
        }
        Log.w("AppDetailFeature", "Missing ingredients to build app info link, skip");
    }
    
    private void bindButton(final ImageButton imageButton, final int n) {
        if (imageButton == null) {
            return;
        }
        switch (n) {
            default: {}
            case 2: {
                if (this.mEditRuleNameOnClickListener == null) {
                    imageButton.setVisibility(8);
                }
                else {
                    imageButton.setImageResource(2131231069);
                    imageButton.setVisibility(0);
                    imageButton.setOnClickListener(this.mEditRuleNameOnClickListener);
                }
            }
            case 1: {
                if (this.mAppNotifPrefIntent == null) {
                    imageButton.setVisibility(8);
                }
                else {
                    imageButton.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                        public void onClick(final View view) {
                            FeatureFactory.getFactory(EntityHeaderController.this.mAppContext).getMetricsFeatureProvider().actionWithSource(EntityHeaderController.this.mAppContext, EntityHeaderController.this.mMetricsCategory, 1016);
                            EntityHeaderController.this.mFragment.startActivity(EntityHeaderController.this.mAppNotifPrefIntent);
                        }
                    });
                    imageButton.setVisibility(0);
                }
            }
            case 0: {
                imageButton.setVisibility(8);
            }
        }
    }
    
    public static EntityHeaderController newInstance(final Activity activity, final Fragment fragment, final View view) {
        return new EntityHeaderController(activity, fragment, view);
    }
    
    private void setText(int visibility, final CharSequence text) {
        final TextView textView = (TextView)this.mHeader.findViewById(visibility);
        if (textView != null) {
            textView.setText(text);
            if (TextUtils.isEmpty(text)) {
                visibility = 8;
            }
            else {
                visibility = 0;
            }
            textView.setVisibility(visibility);
        }
    }
    
    public EntityHeaderController bindHeaderButtons() {
        final View viewById = this.mHeader.findViewById(2131362113);
        final ImageButton imageButton = (ImageButton)this.mHeader.findViewById(16908313);
        final ImageButton imageButton2 = (ImageButton)this.mHeader.findViewById(16908314);
        this.bindAppInfoLink(viewById);
        this.bindButton(imageButton, this.mAction1);
        this.bindButton(imageButton2, this.mAction2);
        return this;
    }
    
    View done(final Activity activity) {
        return this.done(activity, true);
    }
    
    public View done(final Activity activity, final boolean b) {
        this.styleActionBar(activity);
        final ImageView imageView = (ImageView)this.mHeader.findViewById(2131362114);
        if (imageView != null) {
            imageView.setImageDrawable(this.mIcon);
            imageView.setContentDescription((CharSequence)this.mIconContentDescription);
        }
        this.setText(2131362118, this.mLabel);
        this.setText(2131362117, this.mSummary);
        this.setText(2131362116, this.mSecondSummary);
        if (this.mIsInstantApp) {
            this.setText(2131362274, this.mHeader.getResources().getString(2131887895));
        }
        if (b) {
            this.bindHeaderButtons();
        }
        return this.mHeader;
    }
    
    public LayoutPreference done(final Activity activity, final Context context) {
        final LayoutPreference layoutPreference = new LayoutPreference(context, this.done(activity));
        layoutPreference.setOrder(-1000);
        layoutPreference.setSelectable(false);
        layoutPreference.setKey("pref_app_header");
        return layoutPreference;
    }
    
    public EntityHeaderController setButtonActions(final int mAction1, final int mAction2) {
        this.mAction1 = mAction1;
        this.mAction2 = mAction2;
        return this;
    }
    
    public EntityHeaderController setEditZenRuleNameListener(final View.OnClickListener mEditRuleNameOnClickListener) {
        this.mEditRuleNameOnClickListener = mEditRuleNameOnClickListener;
        return this;
    }
    
    public EntityHeaderController setHasAppInfoLink(final boolean mHasAppInfoLink) {
        this.mHasAppInfoLink = mHasAppInfoLink;
        return this;
    }
    
    public EntityHeaderController setIcon(final Drawable drawable) {
        if (drawable != null) {
            this.mIcon = drawable.getConstantState().newDrawable(this.mAppContext.getResources());
        }
        return this;
    }
    
    public EntityHeaderController setIcon(final ApplicationsState.AppEntry appEntry) {
        this.mIcon = IconDrawableFactory.newInstance(this.mAppContext).getBadgedIcon(appEntry.info);
        return this;
    }
    
    public EntityHeaderController setIconContentDescription(final String mIconContentDescription) {
        this.mIconContentDescription = mIconContentDescription;
        return this;
    }
    
    public EntityHeaderController setIsInstantApp(final boolean mIsInstantApp) {
        this.mIsInstantApp = mIsInstantApp;
        return this;
    }
    
    public EntityHeaderController setLabel(final ApplicationsState.AppEntry appEntry) {
        this.mLabel = appEntry.label;
        return this;
    }
    
    public EntityHeaderController setLabel(final CharSequence mLabel) {
        this.mLabel = mLabel;
        return this;
    }
    
    public EntityHeaderController setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
        return this;
    }
    
    public EntityHeaderController setRecyclerView(final RecyclerView mRecyclerView, final Lifecycle mLifecycle) {
        this.mRecyclerView = mRecyclerView;
        this.mLifecycle = mLifecycle;
        return this;
    }
    
    public EntityHeaderController setSecondSummary(final CharSequence mSecondSummary) {
        this.mSecondSummary = mSecondSummary;
        return this;
    }
    
    public EntityHeaderController setSummary(final PackageInfo packageInfo) {
        if (packageInfo != null) {
            this.mSummary = packageInfo.versionName;
        }
        return this;
    }
    
    public EntityHeaderController setSummary(final CharSequence mSummary) {
        this.mSummary = mSummary;
        return this;
    }
    
    public EntityHeaderController setUid(final int mUid) {
        this.mUid = mUid;
        return this;
    }
    
    public EntityHeaderController styleActionBar(final Activity activity) {
        if (activity == null) {
            Log.w("AppDetailFeature", "No activity, cannot style actionbar.");
            return this;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (actionBar == null) {
            Log.w("AppDetailFeature", "No actionbar, cannot style actionbar.");
            return this;
        }
        actionBar.setBackgroundDrawable((Drawable)new ColorDrawable(Utils.getColorAttr((Context)activity, 16843827)));
        actionBar.setElevation(0.0f);
        if (this.mRecyclerView != null && this.mLifecycle != null) {
            ActionBarShadowController.attachToRecyclerView(this.mActivity, this.mLifecycle, this.mRecyclerView);
        }
        return this;
    }
}
