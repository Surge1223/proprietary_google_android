package com.android.settings.widget;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.ProgressBar;

public class RingProgressBar extends ProgressBar
{
    public RingProgressBar(final Context context) {
        this(context, null);
    }
    
    public RingProgressBar(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public RingProgressBar(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 2131951884);
    }
    
    public RingProgressBar(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    protected void onMeasure(int min, final int n) {
        synchronized (this) {
            super.onMeasure(min, n);
            min = Math.min(this.getMeasuredHeight(), this.getMeasuredWidth());
            this.setMeasuredDimension(min, min);
        }
    }
}
