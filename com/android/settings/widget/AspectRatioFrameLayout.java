package com.android.settings.widget;

import android.view.View$MeasureSpec;
import android.content.res.TypedArray;
import com.android.settings.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.FrameLayout;

public final class AspectRatioFrameLayout extends FrameLayout
{
    float mAspectRatio;
    
    public AspectRatioFrameLayout(final Context context) {
        this(context, null);
    }
    
    public AspectRatioFrameLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AspectRatioFrameLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mAspectRatio = 1.0f;
        if (set != null) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.AspectRatioFrameLayout);
            this.mAspectRatio = obtainStyledAttributes.getFloat(0, 1.0f);
            obtainStyledAttributes.recycle();
        }
    }
    
    protected void onMeasure(int measuredWidth, int measuredHeight) {
        super.onMeasure(measuredWidth, measuredHeight);
        measuredWidth = this.getMeasuredWidth();
        measuredHeight = this.getMeasuredHeight();
        if (measuredWidth == 0 || measuredHeight == 0) {
            return;
        }
        if (Math.abs(this.mAspectRatio - measuredWidth / measuredHeight) <= 0.01f) {
            return;
        }
        super.onMeasure(View$MeasureSpec.makeMeasureSpec((int)(measuredHeight * this.mAspectRatio), 1073741824), View$MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824));
    }
    
    public void setAspectRatio(final float mAspectRatio) {
        this.mAspectRatio = mAspectRatio;
    }
}
