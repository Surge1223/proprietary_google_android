package com.android.settings.widget;

import com.android.settings.Utils;
import android.os.UserManager;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.SelfAvailablePreference;
import android.support.v7.preference.PreferenceCategory;

public class WorkOnlyCategory extends PreferenceCategory implements SelfAvailablePreference
{
    public WorkOnlyCategory(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public boolean isAvailable(final Context context) {
        return Utils.getManagedProfile(UserManager.get(context)) != null;
    }
}
