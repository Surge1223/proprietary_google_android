package com.android.settings.widget;

import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View.OnClickListener;
import com.android.settingslib.RestrictedPreference;

public class GearPreference extends RestrictedPreference implements View.OnClickListener
{
    private OnGearClickListener mOnGearClickListener;
    
    public GearPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    protected int getSecondTargetResId() {
        return 2131558679;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362594);
        if (this.mOnGearClickListener != null) {
            viewById.setVisibility(0);
            viewById.setOnClickListener((View.OnClickListener)this);
        }
        else {
            viewById.setVisibility(8);
            viewById.setOnClickListener((View.OnClickListener)null);
        }
        viewById.setEnabled(true);
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131362594 && this.mOnGearClickListener != null) {
            this.mOnGearClickListener.onGearClick(this);
        }
    }
    
    public void setOnGearClickListener(final OnGearClickListener mOnGearClickListener) {
        this.mOnGearClickListener = mOnGearClickListener;
        this.notifyChanged();
    }
    
    @Override
    protected boolean shouldHideSecondTarget() {
        return this.mOnGearClickListener == null;
    }
    
    public interface OnGearClickListener
    {
        void onGearClick(final GearPreference p0);
    }
}
