package com.android.settings;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.os.RecoverySystem;
import android.os.AsyncTask;
import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.widget.Toast;
import android.telephony.SubscriptionManager;
import android.net.Uri;
import android.provider.Telephony$Sms;
import android.bluetooth.BluetoothAdapter;
import android.app.Activity;
import android.content.Context;
import com.android.ims.ImsManager;
import android.bluetooth.BluetoothManager;
import android.net.NetworkPolicyManager;
import android.telephony.TelephonyManager;
import android.net.wifi.WifiManager;
import android.net.ConnectivityManager;
import android.view.View.OnClickListener;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;

public final class _$$Lambda$ResetNetworkConfirm$YTG2_gTxf5vyFkKGLAaR8nzFOxo implements DialogInterface$OnDismissListener
{
    public final void onDismiss(final DialogInterface dialogInterface) {
        ResetNetworkConfirm.lambda$onCreateView$0(this.f$0, dialogInterface);
    }
}
