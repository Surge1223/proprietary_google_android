package com.android.settings.graph;

import android.view.View;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

public class BottomLabelLayout extends LinearLayout
{
    public BottomLabelLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    private boolean isStacked() {
        final int orientation = this.getOrientation();
        boolean b = true;
        if (orientation != 1) {
            b = false;
        }
        return b;
    }
    
    protected void onMeasure(final int n, final int n2) {
        final int size = View$MeasureSpec.getSize(n);
        final boolean stacked = this.isStacked();
        boolean b = false;
        int measureSpec;
        if (!stacked && View$MeasureSpec.getMode(n) == 1073741824) {
            measureSpec = View$MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE);
            b = true;
        }
        else {
            measureSpec = n;
        }
        super.onMeasure(measureSpec, n2);
        boolean b2 = b;
        if (!stacked) {
            b2 = b;
            if ((0xFF000000 & this.getMeasuredWidthAndState()) == 0x1000000) {
                this.setStacked(true);
                b2 = true;
            }
        }
        if (b2) {
            super.onMeasure(n, n2);
        }
    }
    
    void setStacked(final boolean orientation) {
        this.setOrientation((int)(orientation ? 1 : 0));
        int gravity;
        if ((orientation ? 1 : 0) != 0) {
            gravity = 8388611;
        }
        else {
            gravity = 80;
        }
        this.setGravity(gravity);
        final View viewById = this.findViewById(R.id.spacer);
        if (viewById != null) {
            int visibility;
            if ((orientation ? 1 : 0) != 0) {
                visibility = 8;
            }
            else {
                visibility = 0;
            }
            viewById.setVisibility(visibility);
        }
    }
}
