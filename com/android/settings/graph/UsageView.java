package com.android.settings.graph;

import android.util.SparseIntArray;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.content.res.TypedArray;
import android.view.View;
import android.widget.LinearLayout;
import com.android.settingslib.R;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.widget.FrameLayout;

public class UsageView extends FrameLayout
{
    private final TextView[] mBottomLabels;
    private final TextView[] mLabels;
    private final UsageGraph mUsageGraph;
    
    public UsageView(final Context context, final AttributeSet set) {
        super(context, set);
        LayoutInflater.from(context).inflate(2131558865, (ViewGroup)this);
        this.mUsageGraph = (UsageGraph)this.findViewById(2131362780);
        this.mLabels = new TextView[] { (TextView)this.findViewById(2131362313), (TextView)this.findViewById(2131362316), (TextView)this.findViewById(2131362318) };
        this.mBottomLabels = new TextView[] { (TextView)this.findViewById(2131362317), (TextView)this.findViewById(2131362314) };
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.UsageView, 0, 0);
        if (obtainStyledAttributes.hasValue(3)) {
            this.setSideLabels(obtainStyledAttributes.getTextArray(3));
        }
        if (obtainStyledAttributes.hasValue(2)) {
            this.setBottomLabels(obtainStyledAttributes.getTextArray(2));
        }
        if (obtainStyledAttributes.hasValue(4)) {
            final int color = obtainStyledAttributes.getColor(4, 0);
            final TextView[] mLabels = this.mLabels;
            for (int length = mLabels.length, i = 0; i < length; ++i) {
                mLabels[i].setTextColor(color);
            }
            final TextView[] mBottomLabels = this.mBottomLabels;
            for (int length2 = mBottomLabels.length, j = 0; j < length2; ++j) {
                mBottomLabels[j].setTextColor(color);
            }
        }
        if (obtainStyledAttributes.hasValue(0)) {
            final int int1 = obtainStyledAttributes.getInt(0, 0);
            if (int1 == 8388613) {
                final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131362179);
                final LinearLayout linearLayout2 = (LinearLayout)this.findViewById(2131362315);
                linearLayout.removeView((View)linearLayout2);
                linearLayout.addView((View)linearLayout2);
                linearLayout2.setGravity(8388613);
                final LinearLayout linearLayout3 = (LinearLayout)this.findViewById(2131361918);
                final View viewById = linearLayout3.findViewById(2131361919);
                linearLayout3.removeView(viewById);
                linearLayout3.addView(viewById);
            }
            else if (int1 != 8388611) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported gravity ");
                sb.append(int1);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        this.mUsageGraph.setAccentColor(obtainStyledAttributes.getColor(1, 0));
        obtainStyledAttributes.recycle();
    }
    
    private void setWeight(final int n, final float weight) {
        final View viewById = this.findViewById(n);
        final LinearLayout$LayoutParams layoutParams = (LinearLayout$LayoutParams)viewById.getLayoutParams();
        layoutParams.weight = weight;
        viewById.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    }
    
    public void addPath(final SparseIntArray sparseIntArray) {
        this.mUsageGraph.addPath(sparseIntArray);
    }
    
    public void addProjectedPath(final SparseIntArray sparseIntArray) {
        this.mUsageGraph.addProjectedPath(sparseIntArray);
    }
    
    public void clearPaths() {
        this.mUsageGraph.clearPaths();
    }
    
    public void configureGraph(final int n, final int n2) {
        this.mUsageGraph.setMax(n, n2);
    }
    
    public void setAccentColor(final int accentColor) {
        this.mUsageGraph.setAccentColor(accentColor);
    }
    
    public void setBottomLabels(final CharSequence[] array) {
        if (array.length == this.mBottomLabels.length) {
            for (int i = 0; i < this.mBottomLabels.length; ++i) {
                this.mBottomLabels[i].setText(array[i]);
            }
            return;
        }
        throw new IllegalArgumentException("Invalid number of labels");
    }
    
    public void setDividerColors(final int n, final int n2) {
        this.mUsageGraph.setDividerColors(n, n2);
    }
    
    public void setDividerLoc(final int dividerLoc) {
        this.mUsageGraph.setDividerLoc(dividerLoc);
    }
    
    public void setSideLabelWeights(final float n, final float n2) {
        this.setWeight(2131362628, n);
        this.setWeight(2131362629, n2);
    }
    
    public void setSideLabels(final CharSequence[] array) {
        if (array.length == this.mLabels.length) {
            for (int i = 0; i < this.mLabels.length; ++i) {
                this.mLabels[i].setText(array[i]);
            }
            return;
        }
        throw new IllegalArgumentException("Invalid number of labels");
    }
}
