package com.android.settings.graph;

import android.graphics.Shader;
import android.graphics.LinearGradient;
import android.graphics.Shader$TileMode;
import android.graphics.Canvas;
import com.android.settings.fuelgauge.BatteryUtils;
import android.content.res.Resources;
import android.util.TypedValue;
import android.graphics.DashPathEffect;
import android.graphics.PathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.Paint$Join;
import android.graphics.Paint$Cap;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Path;
import android.util.SparseIntArray;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;

public class UsageGraph extends View
{
    private int mAccentColor;
    private final int mCornerRadius;
    private final Drawable mDivider;
    private final int mDividerSize;
    private final Paint mDottedPaint;
    private final Paint mFillPaint;
    private final Paint mLinePaint;
    private final SparseIntArray mLocalPaths;
    private final SparseIntArray mLocalProjectedPaths;
    private float mMaxX;
    private float mMaxY;
    private float mMiddleDividerLoc;
    private int mMiddleDividerTint;
    private final Path mPath;
    private final SparseIntArray mPaths;
    private final SparseIntArray mProjectedPaths;
    private final Drawable mTintedDivider;
    private int mTopDividerTint;
    
    public UsageGraph(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPath = new Path();
        this.mPaths = new SparseIntArray();
        this.mLocalPaths = new SparseIntArray();
        this.mProjectedPaths = new SparseIntArray();
        this.mLocalProjectedPaths = new SparseIntArray();
        this.mMaxX = 100.0f;
        this.mMaxY = 100.0f;
        this.mMiddleDividerLoc = 0.5f;
        this.mMiddleDividerTint = -1;
        this.mTopDividerTint = -1;
        final Resources resources = context.getResources();
        (this.mLinePaint = new Paint()).setStyle(Paint.Style.STROKE);
        this.mLinePaint.setStrokeCap(Paint$Cap.ROUND);
        this.mLinePaint.setStrokeJoin(Paint$Join.ROUND);
        this.mLinePaint.setAntiAlias(true);
        this.mCornerRadius = resources.getDimensionPixelSize(2131165687);
        this.mLinePaint.setPathEffect((PathEffect)new CornerPathEffect((float)this.mCornerRadius));
        this.mLinePaint.setStrokeWidth((float)resources.getDimensionPixelSize(2131165688));
        (this.mFillPaint = new Paint(this.mLinePaint)).setStyle(Paint.Style.FILL);
        (this.mDottedPaint = new Paint(this.mLinePaint)).setStyle(Paint.Style.STROKE);
        final float n = resources.getDimensionPixelSize(2131165684);
        final float n2 = resources.getDimensionPixelSize(2131165683);
        this.mDottedPaint.setStrokeWidth(3.0f * n);
        this.mDottedPaint.setPathEffect((PathEffect)new DashPathEffect(new float[] { n, n2 }, 0.0f));
        this.mDottedPaint.setColor(context.getColor(2131099887));
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843284, typedValue, true);
        this.mDivider = context.getDrawable(typedValue.resourceId);
        this.mTintedDivider = context.getDrawable(typedValue.resourceId);
        this.mDividerSize = resources.getDimensionPixelSize(2131165682);
    }
    
    private void addPathAndUpdate(final SparseIntArray sparseIntArray, final SparseIntArray sparseIntArray2, final SparseIntArray sparseIntArray3) {
        final long currentTimeMillis = System.currentTimeMillis();
        for (int i = 0; i < sparseIntArray.size(); ++i) {
            sparseIntArray2.put(sparseIntArray.keyAt(i), sparseIntArray.valueAt(i));
        }
        sparseIntArray2.put(sparseIntArray.keyAt(sparseIntArray.size() - 1) + 1, -1);
        this.calculateLocalPaths(sparseIntArray2, sparseIntArray3);
        this.postInvalidate();
        BatteryUtils.logRuntime("UsageGraph", "addPathAndUpdate", currentTimeMillis);
    }
    
    private void calculateLocalPaths() {
        this.calculateLocalPaths(this.mPaths, this.mLocalPaths);
        this.calculateLocalPaths(this.mProjectedPaths, this.mLocalProjectedPaths);
    }
    
    private void drawDivider(final int n, final Canvas canvas, final int tint) {
        Drawable drawable = this.mDivider;
        if (tint != -1) {
            this.mTintedDivider.setTint(tint);
            drawable = this.mTintedDivider;
        }
        drawable.setBounds(0, n, canvas.getWidth(), this.mDividerSize + n);
        drawable.draw(canvas);
    }
    
    private void drawFilledPath(final Canvas canvas, final SparseIntArray sparseIntArray, final Paint paint) {
        this.mPath.reset();
        float n = sparseIntArray.keyAt(0);
        this.mPath.moveTo((float)sparseIntArray.keyAt(0), (float)sparseIntArray.valueAt(0));
        for (int i = 1; i < sparseIntArray.size(); ++i) {
            final int key = sparseIntArray.keyAt(i);
            final int value = sparseIntArray.valueAt(i);
            if (value == -1) {
                this.mPath.lineTo((float)sparseIntArray.keyAt(i - 1), (float)this.getHeight());
                this.mPath.lineTo(n, (float)this.getHeight());
                this.mPath.close();
                final int n2 = i + 1;
                if ((i = n2) < sparseIntArray.size()) {
                    n = sparseIntArray.keyAt(n2);
                    this.mPath.moveTo((float)sparseIntArray.keyAt(n2), (float)sparseIntArray.valueAt(n2));
                    i = n2;
                }
            }
            else {
                this.mPath.lineTo((float)key, (float)value);
            }
        }
        canvas.drawPath(this.mPath, paint);
    }
    
    private void drawLinePath(final Canvas canvas, final SparseIntArray sparseIntArray, final Paint paint) {
        if (sparseIntArray.size() == 0) {
            return;
        }
        this.mPath.reset();
        this.mPath.moveTo((float)sparseIntArray.keyAt(0), (float)sparseIntArray.valueAt(0));
        for (int i = 1; i < sparseIntArray.size(); ++i) {
            final int key = sparseIntArray.keyAt(i);
            final int value = sparseIntArray.valueAt(i);
            if (value == -1) {
                final int n = i + 1;
                if ((i = n) < sparseIntArray.size()) {
                    this.mPath.moveTo((float)sparseIntArray.keyAt(n), (float)sparseIntArray.valueAt(n));
                    i = n;
                }
            }
            else {
                this.mPath.lineTo((float)key, (float)value);
            }
        }
        canvas.drawPath(this.mPath, paint);
    }
    
    private int getColor(final int n, final float n2) {
        return ((int)(255.0f * n2) << 24 | 0xFFFFFF) & n;
    }
    
    private int getX(final float n) {
        return (int)(n / this.mMaxX * this.getWidth());
    }
    
    private int getY(final float n) {
        return (int)(this.getHeight() * (1.0f - n / this.mMaxY));
    }
    
    private boolean hasDiff(final int n, final int n2) {
        return Math.abs(n2 - n) >= this.mCornerRadius;
    }
    
    private void updateGradient() {
        this.mFillPaint.setShader((Shader)new LinearGradient(0.0f, 0.0f, 0.0f, (float)this.getHeight(), this.getColor(this.mAccentColor, 0.2f), 0, Shader$TileMode.CLAMP));
    }
    
    public void addPath(final SparseIntArray sparseIntArray) {
        this.addPathAndUpdate(sparseIntArray, this.mPaths, this.mLocalPaths);
    }
    
    public void addProjectedPath(final SparseIntArray sparseIntArray) {
        this.addPathAndUpdate(sparseIntArray, this.mProjectedPaths, this.mLocalProjectedPaths);
    }
    
    void calculateLocalPaths(final SparseIntArray sparseIntArray, final SparseIntArray sparseIntArray2) {
        final long currentTimeMillis = System.currentTimeMillis();
        if (this.getWidth() == 0) {
            return;
        }
        sparseIntArray2.clear();
        int x = 0;
        int y = -1;
        int n = 0;
        for (int i = 0; i < sparseIntArray.size(); ++i) {
            final int key = sparseIntArray.keyAt(i);
            final int value = sparseIntArray.valueAt(i);
            if (value == -1) {
                if (i == 1) {
                    sparseIntArray2.put(this.getX(key + 1) - 1, this.getY(0.0f));
                }
                else {
                    if (i == sparseIntArray.size() - 1 && n != 0) {
                        sparseIntArray2.put(x, y);
                    }
                    n = 0;
                    sparseIntArray2.put(x + 1, -1);
                }
            }
            else {
                x = this.getX(key);
                y = this.getY(value);
                if (sparseIntArray2.size() > 0) {
                    final int key2 = sparseIntArray2.keyAt(sparseIntArray2.size() - 1);
                    final int value2 = sparseIntArray2.valueAt(sparseIntArray2.size() - 1);
                    if (value2 != -1 && !this.hasDiff(key2, x) && !this.hasDiff(value2, y)) {
                        n = 1;
                        continue;
                    }
                }
                n = 0;
                sparseIntArray2.put(x, y);
            }
        }
        BatteryUtils.logRuntime("UsageGraph", "calculateLocalPaths", currentTimeMillis);
    }
    
    void clearPaths() {
        this.mPaths.clear();
        this.mLocalPaths.clear();
        this.mProjectedPaths.clear();
        this.mLocalProjectedPaths.clear();
    }
    
    protected void onDraw(final Canvas canvas) {
        final long currentTimeMillis = System.currentTimeMillis();
        if (this.mMiddleDividerLoc != 0.0f) {
            this.drawDivider(0, canvas, this.mTopDividerTint);
        }
        this.drawDivider((int)((canvas.getHeight() - this.mDividerSize) * this.mMiddleDividerLoc), canvas, this.mMiddleDividerTint);
        this.drawDivider(canvas.getHeight() - this.mDividerSize, canvas, -1);
        if (this.mLocalPaths.size() == 0 && this.mLocalProjectedPaths.size() == 0) {
            return;
        }
        this.drawLinePath(canvas, this.mLocalProjectedPaths, this.mDottedPaint);
        this.drawFilledPath(canvas, this.mLocalPaths, this.mFillPaint);
        this.drawLinePath(canvas, this.mLocalPaths, this.mLinePaint);
        BatteryUtils.logRuntime("UsageGraph", "onDraw", currentTimeMillis);
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        final long currentTimeMillis = System.currentTimeMillis();
        super.onSizeChanged(n, n2, n3, n4);
        this.updateGradient();
        this.calculateLocalPaths();
        BatteryUtils.logRuntime("UsageGraph", "onSizeChanged", currentTimeMillis);
    }
    
    void setAccentColor(final int mAccentColor) {
        this.mAccentColor = mAccentColor;
        this.mLinePaint.setColor(this.mAccentColor);
        this.updateGradient();
        this.postInvalidate();
    }
    
    void setDividerColors(final int mMiddleDividerTint, final int mTopDividerTint) {
        this.mMiddleDividerTint = mMiddleDividerTint;
        this.mTopDividerTint = mTopDividerTint;
    }
    
    void setDividerLoc(final int n) {
        this.mMiddleDividerLoc = 1.0f - n / this.mMaxY;
    }
    
    void setMax(final int n, final int n2) {
        final long currentTimeMillis = System.currentTimeMillis();
        this.mMaxX = n;
        this.mMaxY = n2;
        this.calculateLocalPaths();
        this.postInvalidate();
        BatteryUtils.logRuntime("UsageGraph", "setMax", currentTimeMillis);
    }
}
