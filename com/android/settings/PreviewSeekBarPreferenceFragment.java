package com.android.settings;

import android.widget.SeekBar;
import android.widget.SeekBar$OnSeekBarChangeListener;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.res.Configuration;
import com.android.settings.widget.LabeledSeekBar;
import android.support.v4.view.ViewPager;
import com.android.settings.widget.DotsPageIndicator;
import android.view.View;
import android.widget.TextView;

public abstract class PreviewSeekBarPreferenceFragment extends SettingsPreferenceFragment
{
    protected int mActivityLayoutResId;
    protected int mCurrentIndex;
    protected String[] mEntries;
    protected int mInitialIndex;
    private TextView mLabel;
    private View mLarger;
    private DotsPageIndicator mPageIndicator;
    private ViewPager.OnPageChangeListener mPageIndicatorPageChangeListener;
    private ViewPager.OnPageChangeListener mPreviewPageChangeListener;
    private ViewPager mPreviewPager;
    private PreviewPagerAdapter mPreviewPagerAdapter;
    protected int[] mPreviewSampleResIds;
    private LabeledSeekBar mSeekBar;
    private View mSmaller;
    
    public PreviewSeekBarPreferenceFragment() {
        this.mPreviewPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(final int n) {
            }
            
            @Override
            public void onPageScrolled(final int n, final float n2, final int n3) {
            }
            
            @Override
            public void onPageSelected(final int n) {
                PreviewSeekBarPreferenceFragment.this.mPreviewPager.sendAccessibilityEvent(16384);
            }
        };
        this.mPageIndicatorPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(final int n) {
            }
            
            @Override
            public void onPageScrolled(final int n, final float n2, final int n3) {
            }
            
            @Override
            public void onPageSelected(final int n) {
                PreviewSeekBarPreferenceFragment.this.setPagerIndicatorContentDescription(n);
            }
        };
    }
    
    private void setPagerIndicatorContentDescription(final int n) {
        this.mPageIndicator.setContentDescription((CharSequence)this.getPrefContext().getString(2131888639, new Object[] { n + 1, this.mPreviewSampleResIds.length }));
    }
    
    private void setPreviewLayer(final int mCurrentIndex, final boolean b) {
        this.mLabel.setText((CharSequence)this.mEntries[mCurrentIndex]);
        final View mSmaller = this.mSmaller;
        final boolean b2 = false;
        mSmaller.setEnabled(mCurrentIndex > 0);
        final View mLarger = this.mLarger;
        boolean enabled = b2;
        if (mCurrentIndex < this.mEntries.length - 1) {
            enabled = true;
        }
        mLarger.setEnabled(enabled);
        this.setPagerIndicatorContentDescription(this.mPreviewPager.getCurrentItem());
        this.mPreviewPagerAdapter.setPreviewLayer(mCurrentIndex, this.mCurrentIndex, this.mPreviewPager.getCurrentItem(), b);
        this.mCurrentIndex = mCurrentIndex;
    }
    
    protected abstract void commit();
    
    protected abstract Configuration createConfig(final Configuration p0, final int p1);
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        final ViewGroup viewGroup2 = (ViewGroup)onCreateView.findViewById(16908351);
        viewGroup2.removeAllViews();
        final View inflate = layoutInflater.inflate(this.mActivityLayoutResId, viewGroup2, false);
        viewGroup2.addView(inflate);
        this.mLabel = (TextView)inflate.findViewById(2131362027);
        final int max = Math.max(1, this.mEntries.length - 1);
        (this.mSeekBar = (LabeledSeekBar)inflate.findViewById(2131362579)).setLabels(this.mEntries);
        this.mSeekBar.setMax(max);
        (this.mSmaller = inflate.findViewById(2131362620)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final int progress = PreviewSeekBarPreferenceFragment.this.mSeekBar.getProgress();
                if (progress > 0) {
                    PreviewSeekBarPreferenceFragment.this.mSeekBar.setProgress(progress - 1, true);
                }
            }
        });
        (this.mLarger = inflate.findViewById(2131362320)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final int progress = PreviewSeekBarPreferenceFragment.this.mSeekBar.getProgress();
                if (progress < PreviewSeekBarPreferenceFragment.this.mSeekBar.getMax()) {
                    PreviewSeekBarPreferenceFragment.this.mSeekBar.setProgress(progress + 1, true);
                }
            }
        });
        if (this.mEntries.length == 1) {
            this.mSeekBar.setEnabled(false);
        }
        final Context context = this.getContext();
        final Configuration configuration = context.getResources().getConfiguration();
        final boolean b = configuration.getLayoutDirection() == 1;
        final Configuration[] array = new Configuration[this.mEntries.length];
        for (int i = 0; i < this.mEntries.length; ++i) {
            array[i] = this.createConfig(configuration, i);
        }
        this.mPreviewPager = (ViewPager)inflate.findViewById(2131362464);
        this.mPreviewPagerAdapter = new PreviewPagerAdapter(context, b, this.mPreviewSampleResIds, array);
        this.mPreviewPager.setAdapter(this.mPreviewPagerAdapter);
        final ViewPager mPreviewPager = this.mPreviewPager;
        int currentItem;
        if (b) {
            currentItem = this.mPreviewSampleResIds.length - 1;
        }
        else {
            currentItem = 0;
        }
        mPreviewPager.setCurrentItem(currentItem);
        this.mPreviewPager.addOnPageChangeListener(this.mPreviewPageChangeListener);
        this.mPageIndicator = (DotsPageIndicator)inflate.findViewById(2131362431);
        if (this.mPreviewSampleResIds.length > 1) {
            this.mPageIndicator.setViewPager(this.mPreviewPager);
            this.mPageIndicator.setVisibility(0);
            this.mPageIndicator.setOnPageChangeListener(this.mPageIndicatorPageChangeListener);
        }
        else {
            this.mPageIndicator.setVisibility(8);
        }
        this.setPreviewLayer(this.mInitialIndex, false);
        return onCreateView;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.mSeekBar.setProgress(this.mCurrentIndex);
        this.mSeekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new onPreviewSeekBarChangeListener());
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.mSeekBar.setOnSeekBarChangeListener(null);
    }
    
    private class onPreviewSeekBarChangeListener implements SeekBar$OnSeekBarChangeListener
    {
        private boolean mSeekByTouch;
        
        public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
            PreviewSeekBarPreferenceFragment.this.setPreviewLayer(n, false);
            if (!this.mSeekByTouch) {
                PreviewSeekBarPreferenceFragment.this.commit();
            }
        }
        
        public void onStartTrackingTouch(final SeekBar seekBar) {
            this.mSeekByTouch = true;
        }
        
        public void onStopTrackingTouch(final SeekBar seekBar) {
            if (PreviewSeekBarPreferenceFragment.this.mPreviewPagerAdapter.isAnimating()) {
                PreviewSeekBarPreferenceFragment.this.mPreviewPagerAdapter.setAnimationEndAction(new Runnable() {
                    @Override
                    public void run() {
                        PreviewSeekBarPreferenceFragment.this.commit();
                    }
                });
            }
            else {
                PreviewSeekBarPreferenceFragment.this.commit();
            }
            this.mSeekByTouch = false;
        }
    }
}
