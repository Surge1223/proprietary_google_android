package com.android.settings;

import android.content.Intent;
import android.net.ProxyInfo;
import android.app.Activity;
import android.text.TextUtils;
import android.net.ConnectivityManager;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.os.Bundle;
import android.net.Proxy;
import android.util.Log;
import android.text.Selection;
import android.widget.TextView;
import android.text.Spannable;
import android.view.View;
import android.view.View$OnFocusChangeListener;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.android.settings.core.InstrumentedFragment;

public class ProxySelector extends InstrumentedFragment implements DialogCreatable
{
    Button mClearButton;
    View.OnClickListener mClearHandler;
    Button mDefaultButton;
    View.OnClickListener mDefaultHandler;
    private SettingsPreferenceFragment.SettingsDialogFragment mDialogFragment;
    EditText mExclusionListField;
    EditText mHostnameField;
    Button mOKButton;
    View.OnClickListener mOKHandler;
    View$OnFocusChangeListener mOnFocusChangeHandler;
    EditText mPortField;
    private View mView;
    
    public ProxySelector() {
        this.mOKHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (ProxySelector.this.saveToDb()) {
                    ProxySelector.this.getActivity().onBackPressed();
                }
            }
        };
        this.mClearHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ProxySelector.this.mHostnameField.setText((CharSequence)"");
                ProxySelector.this.mPortField.setText((CharSequence)"");
                ProxySelector.this.mExclusionListField.setText((CharSequence)"");
            }
        };
        this.mDefaultHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ProxySelector.this.populateFields();
            }
        };
        this.mOnFocusChangeHandler = (View$OnFocusChangeListener)new View$OnFocusChangeListener() {
            public void onFocusChange(final View view, final boolean b) {
                if (b) {
                    Selection.selectAll((Spannable)((TextView)view).getText());
                }
            }
        };
    }
    
    private void initView(final View view) {
        (this.mHostnameField = (EditText)view.findViewById(2131362231)).setOnFocusChangeListener(this.mOnFocusChangeHandler);
        (this.mPortField = (EditText)view.findViewById(2131362459)).setOnClickListener(this.mOKHandler);
        this.mPortField.setOnFocusChangeListener(this.mOnFocusChangeHandler);
        (this.mExclusionListField = (EditText)view.findViewById(2131362132)).setOnFocusChangeListener(this.mOnFocusChangeHandler);
        (this.mOKButton = (Button)view.findViewById(2131361805)).setOnClickListener(this.mOKHandler);
        (this.mClearButton = (Button)view.findViewById(2131361985)).setOnClickListener(this.mClearHandler);
        (this.mDefaultButton = (Button)view.findViewById(2131362054)).setOnClickListener(this.mDefaultHandler);
    }
    
    private void showDialog(final int n) {
        if (this.mDialogFragment != null) {
            Log.e("ProxySelector", "Old dialog fragment not null!");
        }
        (this.mDialogFragment = new SettingsPreferenceFragment.SettingsDialogFragment(this, n)).show(this.getActivity().getFragmentManager(), Integer.toString(n));
    }
    
    public static int validate(final String s, final String s2, final String s3) {
        switch (Proxy.validate(s, s2, s3)) {
            default: {
                Log.e("ProxySelector", "Unknown proxy settings error");
                return -1;
            }
            case 5: {
                return 2131888708;
            }
            case 4: {
                return 2131888710;
            }
            case 3: {
                return 2131888707;
            }
            case 2: {
                return 2131888709;
            }
            case 1: {
                return 2131888706;
            }
            case 0: {
                return 0;
            }
        }
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        return 574;
    }
    
    @Override
    public int getMetricsCategory() {
        return 82;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final boolean b = ((DevicePolicyManager)this.getActivity().getSystemService("device_policy")).getGlobalProxyAdmin() == null;
        this.mHostnameField.setEnabled(b);
        this.mPortField.setEnabled(b);
        this.mExclusionListField.setEnabled(b);
        this.mOKButton.setEnabled(b);
        this.mClearButton.setEnabled(b);
        this.mDefaultButton.setEnabled(b);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (n == 0) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888704).setPositiveButton(2131888705, (DialogInterface$OnClickListener)null).setMessage((CharSequence)this.getActivity().getString(validate(this.mHostnameField.getText().toString().trim(), this.mPortField.getText().toString().trim(), this.mExclusionListField.getText().toString().trim()))).create();
        }
        return null;
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.initView(this.mView = layoutInflater.inflate(2131558699, viewGroup, false));
        this.populateFields();
        return this.mView;
    }
    
    void populateFields() {
        final Activity activity = this.getActivity();
        String host = "";
        int port = -1;
        String exclusionListAsString = "";
        final ProxyInfo globalProxy = ((ConnectivityManager)this.getActivity().getSystemService("connectivity")).getGlobalProxy();
        if (globalProxy != null) {
            host = globalProxy.getHost();
            port = globalProxy.getPort();
            exclusionListAsString = globalProxy.getExclusionListAsString();
        }
        String text;
        if ((text = host) == null) {
            text = "";
        }
        this.mHostnameField.setText((CharSequence)text);
        String string;
        if (port == -1) {
            string = "";
        }
        else {
            string = Integer.toString(port);
        }
        this.mPortField.setText((CharSequence)string);
        this.mExclusionListField.setText((CharSequence)exclusionListAsString);
        final Intent intent = activity.getIntent();
        final String stringExtra = intent.getStringExtra("button-label");
        if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
            this.mOKButton.setText((CharSequence)stringExtra);
        }
        final String stringExtra2 = intent.getStringExtra("title");
        if (!TextUtils.isEmpty((CharSequence)stringExtra2)) {
            activity.setTitle((CharSequence)stringExtra2);
        }
        else {
            activity.setTitle(2131888718);
        }
    }
    
    boolean saveToDb() {
        final String trim = this.mHostnameField.getText().toString().trim();
        final String trim2 = this.mPortField.getText().toString().trim();
        final String trim3 = this.mExclusionListField.getText().toString().trim();
        int int1 = 0;
        if (validate(trim, trim2, trim3) != 0) {
            this.showDialog(0);
            return false;
        }
        if (trim2.length() > 0) {
            try {
                int1 = Integer.parseInt(trim2);
            }
            catch (NumberFormatException ex) {
                return false;
            }
        }
        ((ConnectivityManager)this.getActivity().getSystemService("connectivity")).setGlobalProxy(new ProxyInfo(trim, int1, trim3));
        return true;
    }
}
