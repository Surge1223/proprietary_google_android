package com.android.settings.utils;

import android.text.BidiFormatter;
import android.content.Context;
import android.text.format.Formatter$BytesResult;
import android.content.res.Resources;

public final class FileSizeFormatter
{
    private static Formatter$BytesResult formatBytes(final Resources resources, long n, final int n2, final long n3) {
        final boolean b = n < 0L;
        float n4;
        if (b) {
            n4 = -n;
        }
        else {
            n4 = n;
        }
        final float n5 = n4 / n3;
        int n6;
        String s;
        if (n3 == 1L) {
            n6 = 1;
            s = "%.0f";
        }
        else if (n5 < 1.0f) {
            n6 = 100;
            s = "%.2f";
        }
        else if (n5 < 10.0f) {
            n6 = 10;
            s = "%.1f";
        }
        else {
            n6 = 1;
            s = "%.0f";
        }
        float n7 = n5;
        if (b) {
            n7 = -n5;
        }
        final String format = String.format(s, n7);
        n = Math.round(n6 * n7) * n3 / n6;
        return new Formatter$BytesResult(format, resources.getString(n2), n);
    }
    
    public static String formatFileSize(final Context context, final long n, final int n2, final long n3) {
        if (context == null) {
            return "";
        }
        final Formatter$BytesResult formatBytes = formatBytes(context.getResources(), n, n2, n3);
        return BidiFormatter.getInstance().unicodeWrap(context.getString(getFileSizeSuffix(context), new Object[] { formatBytes.value, formatBytes.units }));
    }
    
    private static int getFileSizeSuffix(final Context context) {
        return context.getResources().getIdentifier("fileSizeSuffix", "string", "android");
    }
}
