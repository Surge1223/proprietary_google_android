package com.android.settings.utils;

import android.app.Fragment;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.ComponentName;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$ManagedServiceSettings$ScaryWarningDialogFragment$GfuRaJIB12V_MS8RLGOsdgpO8G0 implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        ManagedServiceSettings.ScaryWarningDialogFragment.lambda$onCreateDialog$0(this.f$0, this.f$1, dialogInterface, n);
    }
}
