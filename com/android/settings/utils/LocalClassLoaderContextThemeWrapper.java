package com.android.settings.utils;

import android.content.Context;
import android.view.ContextThemeWrapper;

public class LocalClassLoaderContextThemeWrapper extends ContextThemeWrapper
{
    private Class mLocalClass;
    
    public LocalClassLoaderContextThemeWrapper(final Class mLocalClass, final Context context, final int n) {
        super(context, n);
        this.mLocalClass = mLocalClass;
    }
    
    public ClassLoader getClassLoader() {
        return this.mLocalClass.getClassLoader();
    }
}
