package com.android.settings.utils;

import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.widget.RecyclerView;
import android.content.Context;
import com.android.setupwizardlib.DividerItemDecoration;

public class SettingsDividerItemDecoration extends DividerItemDecoration
{
    public SettingsDividerItemDecoration(final Context context) {
        super(context);
    }
    
    @Override
    protected boolean isDividerAllowedAbove(final ViewHolder viewHolder) {
        if (viewHolder instanceof PreferenceViewHolder) {
            return ((PreferenceViewHolder)viewHolder).isDividerAllowedAbove();
        }
        return super.isDividerAllowedAbove(viewHolder);
    }
    
    @Override
    protected boolean isDividerAllowedBelow(final ViewHolder viewHolder) {
        if (viewHolder instanceof PreferenceViewHolder) {
            return ((PreferenceViewHolder)viewHolder).isDividerAllowedBelow();
        }
        return super.isDividerAllowedBelow(viewHolder);
    }
}
