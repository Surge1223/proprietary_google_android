package com.android.settings.utils;

import android.app.Fragment;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.ComponentName;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$ManagedServiceSettings$ScaryWarningDialogFragment$zGrX_jMl8gPwJu7rfyhg512VL6Y implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        ManagedServiceSettings.ScaryWarningDialogFragment.lambda$onCreateDialog$1(dialogInterface, n);
    }
}
