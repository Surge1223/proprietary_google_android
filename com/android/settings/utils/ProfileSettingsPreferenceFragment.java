package com.android.settings.utils;

import android.content.Intent;
import android.os.UserHandle;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import android.content.Context;
import com.android.settingslib.drawer.UserAdapter;
import android.os.UserManager;
import android.os.Bundle;
import android.view.View;
import com.android.settings.SettingsPreferenceFragment;

public abstract class ProfileSettingsPreferenceFragment extends SettingsPreferenceFragment
{
    protected abstract String getIntentActionString();
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        final UserAdapter userSpinnerAdapter = UserAdapter.createUserSpinnerAdapter((UserManager)this.getSystemService("user"), (Context)this.getActivity());
        if (userSpinnerAdapter != null) {
            final Spinner spinner = (Spinner)this.setPinnedHeaderView(2131558759);
            spinner.setAdapter((SpinnerAdapter)userSpinnerAdapter);
            spinner.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
                public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                    final UserHandle userHandle = userSpinnerAdapter.getUserHandle(n);
                    if (userHandle.getIdentifier() != UserHandle.myUserId()) {
                        final Intent intent = new Intent(ProfileSettingsPreferenceFragment.this.getIntentActionString());
                        intent.addFlags(268435456);
                        intent.addFlags(32768);
                        ProfileSettingsPreferenceFragment.this.getActivity().startActivityAsUser(intent, userHandle);
                        spinner.setSelection(0);
                    }
                }
                
                public void onNothingSelected(final AdapterView<?> adapterView) {
                }
            });
        }
    }
}
