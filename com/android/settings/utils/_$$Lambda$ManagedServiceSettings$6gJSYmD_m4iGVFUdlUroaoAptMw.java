package com.android.settings.utils;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.view.View;
import android.app.ActivityManager;
import android.os.Bundle;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import android.content.pm.PackageItemInfo;
import com.android.settings.widget.AppSwitchPreference;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.Comparator;
import android.content.pm.PackageItemInfo$DisplayNameComparator;
import com.android.settings.Utils;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import android.content.ComponentName;
import android.content.pm.ServiceInfo;
import android.os.UserHandle;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import com.android.settings.notification.EmptyTextSettings;
import java.util.List;
import com.android.settingslib.applications.ServiceListing;

public final class _$$Lambda$ManagedServiceSettings$6gJSYmD_m4iGVFUdlUroaoAptMw implements Callback
{
    @Override
    public final void onServicesReloaded(final List list) {
        this.f$0.updateList(list);
    }
}
