package com.android.settings.utils;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.view.View;
import android.app.ActivityManager;
import android.os.Bundle;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import android.content.pm.PackageItemInfo;
import com.android.settings.widget.AppSwitchPreference;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.Comparator;
import android.content.pm.PackageItemInfo$DisplayNameComparator;
import com.android.settings.Utils;
import android.os.UserManager;
import android.content.pm.ServiceInfo;
import java.util.List;
import android.os.UserHandle;
import com.android.settingslib.applications.ServiceListing;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import com.android.settings.notification.EmptyTextSettings;
import android.content.ComponentName;
import android.support.v7.preference.Preference;

public final class _$$Lambda$ManagedServiceSettings$qzumG4qfCDX22E2_mvpKDzSZyck implements OnPreferenceChangeListener
{
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return ManagedServiceSettings.lambda$updateList$0(this.f$0, this.f$1, this.f$2, preference, o);
    }
}
