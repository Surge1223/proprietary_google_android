package com.android.settings.utils;

import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.app.Fragment;
import android.view.View;
import android.app.ActivityManager;
import android.os.Bundle;
import java.util.Iterator;
import android.support.v7.preference.PreferenceScreen;
import android.content.pm.PackageItemInfo;
import com.android.settings.widget.AppSwitchPreference;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.Comparator;
import android.content.pm.PackageItemInfo$DisplayNameComparator;
import com.android.settings.Utils;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import android.content.ComponentName;
import android.content.pm.ServiceInfo;
import java.util.List;
import android.os.UserHandle;
import com.android.settingslib.applications.ServiceListing;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import com.android.settings.notification.EmptyTextSettings;

public abstract class ManagedServiceSettings extends EmptyTextSettings
{
    private final Config mConfig;
    protected Context mContext;
    private DevicePolicyManager mDpm;
    private IconDrawableFactory mIconDrawableFactory;
    private PackageManager mPm;
    private ServiceListing mServiceListing;
    
    public ManagedServiceSettings() {
        this.mConfig = this.getConfig();
    }
    
    private int getCurrentUser(final int n) {
        if (n != -10000) {
            return n;
        }
        return UserHandle.myUserId();
    }
    
    private void updateList(List<ServiceInfo> loadLabel) {
        final int managedProfileId = Utils.getManagedProfileId((UserManager)this.mContext.getSystemService("user"), UserHandle.myUserId());
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        ((List<ServiceInfo>)loadLabel).sort((Comparator<? super ServiceInfo>)new PackageItemInfo$DisplayNameComparator(this.mPm));
        for (final ServiceInfo serviceInfo : loadLabel) {
            final ComponentName componentName = new ComponentName(serviceInfo.packageName, serviceInfo.name);
            loadLabel = null;
            try {
                loadLabel = this.mPm.getApplicationInfoAsUser(serviceInfo.packageName, 0, this.getCurrentUser(managedProfileId)).loadLabel(this.mPm);
            }
            catch (PackageManager$NameNotFoundException ex) {
                Log.e("ManagedServiceSettings", "can't find package name", (Throwable)ex);
            }
            final String string = serviceInfo.loadLabel(this.mPm).toString();
            final AppSwitchPreference appSwitchPreference = new AppSwitchPreference(this.getPrefContext());
            appSwitchPreference.setPersistent(false);
            appSwitchPreference.setIcon(this.mIconDrawableFactory.getBadgedIcon((PackageItemInfo)serviceInfo, serviceInfo.applicationInfo, UserHandle.getUserId(serviceInfo.applicationInfo.uid)));
            if (loadLabel != null && !loadLabel.equals(string)) {
                appSwitchPreference.setTitle(loadLabel);
                appSwitchPreference.setSummary(string);
            }
            else {
                appSwitchPreference.setTitle(string);
            }
            appSwitchPreference.setKey(componentName.flattenToString());
            appSwitchPreference.setChecked(this.isServiceEnabled(componentName));
            if (managedProfileId != -10000 && !this.mDpm.isNotificationListenerServicePermitted(serviceInfo.packageName, managedProfileId)) {
                appSwitchPreference.setSummary(2131890213);
            }
            appSwitchPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new _$$Lambda$ManagedServiceSettings$qzumG4qfCDX22E2_mvpKDzSZyck(this, componentName, string));
            appSwitchPreference.setKey(componentName.flattenToString());
            preferenceScreen.addPreference(appSwitchPreference);
        }
        this.highlightPreferenceIfNeeded();
    }
    
    protected void enable(final ComponentName componentName) {
        this.mServiceListing.setEnabled(componentName, true);
    }
    
    protected abstract Config getConfig();
    
    protected boolean isServiceEnabled(final ComponentName componentName) {
        return this.mServiceListing.isEnabled(componentName);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Context)this.getActivity();
        this.mPm = this.mContext.getPackageManager();
        this.mDpm = (DevicePolicyManager)this.mContext.getSystemService("device_policy");
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(this.mContext);
        (this.mServiceListing = new ServiceListing.Builder(this.mContext).setPermission(this.mConfig.permission).setIntentAction(this.mConfig.intentAction).setNoun(this.mConfig.noun).setSetting(this.mConfig.setting).setTag(this.mConfig.tag).build()).addCallback((ServiceListing.Callback)new _$$Lambda$ManagedServiceSettings$6gJSYmD_m4iGVFUdlUroaoAptMw(this));
        this.setPreferenceScreen(this.getPreferenceManager().createPreferenceScreen(this.mContext));
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mServiceListing.setListening(false);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (!ActivityManager.isLowRamDeviceStatic()) {
            this.mServiceListing.reload();
            this.mServiceListing.setListening(true);
        }
        else {
            this.setEmptyText(2131887437);
        }
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.setEmptyText(this.mConfig.emptyText);
    }
    
    protected boolean setEnabled(final ComponentName componentName, final String s, final boolean b) {
        if (!b) {
            this.mServiceListing.setEnabled(componentName, false);
            return true;
        }
        if (this.mServiceListing.isEnabled(componentName)) {
            return true;
        }
        new ScaryWarningDialogFragment().setServiceInfo(componentName, s, this).show(this.getFragmentManager(), "dialog");
        return false;
    }
    
    public static class Config
    {
        public final int emptyText;
        public final String intentAction;
        public final String noun;
        public final String permission;
        public final String setting;
        public final String tag;
        public final int warningDialogSummary;
        public final int warningDialogTitle;
        
        private Config(final String tag, final String setting, final String intentAction, final String permission, final String noun, final int warningDialogTitle, final int warningDialogSummary, final int emptyText) {
            this.tag = tag;
            this.setting = setting;
            this.intentAction = intentAction;
            this.permission = permission;
            this.noun = noun;
            this.warningDialogTitle = warningDialogTitle;
            this.warningDialogSummary = warningDialogSummary;
            this.emptyText = emptyText;
        }
        
        public static class Builder
        {
            private int mEmptyText;
            private String mIntentAction;
            private String mNoun;
            private String mPermission;
            private String mSetting;
            private String mTag;
            private int mWarningDialogSummary;
            private int mWarningDialogTitle;
            
            public Config build() {
                return new Config(this.mTag, this.mSetting, this.mIntentAction, this.mPermission, this.mNoun, this.mWarningDialogTitle, this.mWarningDialogSummary, this.mEmptyText);
            }
            
            public Builder setEmptyText(final int mEmptyText) {
                this.mEmptyText = mEmptyText;
                return this;
            }
            
            public Builder setIntentAction(final String mIntentAction) {
                this.mIntentAction = mIntentAction;
                return this;
            }
            
            public Builder setNoun(final String mNoun) {
                this.mNoun = mNoun;
                return this;
            }
            
            public Builder setPermission(final String mPermission) {
                this.mPermission = mPermission;
                return this;
            }
            
            public Builder setSetting(final String mSetting) {
                this.mSetting = mSetting;
                return this;
            }
            
            public Builder setTag(final String mTag) {
                this.mTag = mTag;
                return this;
            }
            
            public Builder setWarningDialogSummary(final int mWarningDialogSummary) {
                this.mWarningDialogSummary = mWarningDialogSummary;
                return this;
            }
            
            public Builder setWarningDialogTitle(final int mWarningDialogTitle) {
                this.mWarningDialogTitle = mWarningDialogTitle;
                return this;
            }
        }
    }
    
    public static class ScaryWarningDialogFragment extends InstrumentedDialogFragment
    {
        @Override
        public int getMetricsCategory() {
            return 557;
        }
        
        public Dialog onCreateDialog(Bundle arguments) {
            arguments = this.getArguments();
            final String string = arguments.getString("l");
            final ComponentName unflattenFromString = ComponentName.unflattenFromString(arguments.getString("c"));
            final ManagedServiceSettings managedServiceSettings = (ManagedServiceSettings)this.getTargetFragment();
            return (Dialog)new AlertDialog$Builder(this.getContext()).setMessage((CharSequence)this.getResources().getString(managedServiceSettings.mConfig.warningDialogSummary, new Object[] { string })).setTitle((CharSequence)this.getResources().getString(managedServiceSettings.mConfig.warningDialogTitle, new Object[] { string })).setCancelable(true).setPositiveButton(2131886285, (DialogInterface$OnClickListener)new _$$Lambda$ManagedServiceSettings$ScaryWarningDialogFragment$GfuRaJIB12V_MS8RLGOsdgpO8G0(managedServiceSettings, unflattenFromString)).setNegativeButton(2131887370, (DialogInterface$OnClickListener)_$$Lambda$ManagedServiceSettings$ScaryWarningDialogFragment$zGrX_jMl8gPwJu7rfyhg512VL6Y.INSTANCE).create();
        }
        
        public ScaryWarningDialogFragment setServiceInfo(final ComponentName componentName, final String s, final Fragment fragment) {
            final Bundle arguments = new Bundle();
            arguments.putString("c", componentName.flattenToString());
            arguments.putString("l", s);
            this.setArguments(arguments);
            this.setTargetFragment(fragment, 0);
            return this;
        }
    }
}
