package com.android.settings.utils;

import android.content.ActivityNotFoundException;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.text.TextPaint;
import android.view.View;
import android.text.SpannableStringBuilder;
import android.text.Annotation;
import android.text.SpannableString;
import android.view.View.OnClickListener;
import android.text.style.URLSpan;

public class AnnotationSpan extends URLSpan
{
    private final View.OnClickListener mClickListener;
    
    private AnnotationSpan(final View.OnClickListener mClickListener) {
        super((String)null);
        this.mClickListener = mClickListener;
    }
    
    public static CharSequence linkify(final CharSequence charSequence, final LinkInfo... array) {
        final SpannableString spannableString = new SpannableString(charSequence);
        final Annotation[] array2 = (Annotation[])spannableString.getSpans(0, spannableString.length(), (Class)Annotation.class);
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder((CharSequence)spannableString);
        for (final Annotation annotation : array2) {
            final String value = annotation.getValue();
            final int spanStart = spannableString.getSpanStart((Object)annotation);
            final int spanEnd = spannableString.getSpanEnd((Object)annotation);
            final Object o = null;
            final int length2 = array.length;
            int n = 0;
            Object o2;
            while (true) {
                o2 = o;
                if (n >= length2) {
                    break;
                }
                final LinkInfo linkInfo = array[n];
                if (linkInfo.mAnnotation.equals(value)) {
                    o2 = new AnnotationSpan(linkInfo.mListener);
                    break;
                }
                ++n;
            }
            if (o2 != null) {
                spannableStringBuilder.setSpan(o2, spanStart, spanEnd, spannableString.getSpanFlags(o2));
            }
        }
        return (CharSequence)spannableStringBuilder;
    }
    
    public void onClick(final View view) {
        if (this.mClickListener != null) {
            this.mClickListener.onClick(view);
        }
    }
    
    public void updateDrawState(final TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setUnderlineText(false);
    }
    
    public static class LinkInfo
    {
        private final Boolean mActionable;
        private final String mAnnotation;
        private final View.OnClickListener mListener;
        
        public LinkInfo(final Context context, final String mAnnotation, final Intent intent) {
            this.mAnnotation = mAnnotation;
            boolean b = false;
            if (intent != null) {
                if (context.getPackageManager().resolveActivity(intent, 0) != null) {
                    b = true;
                }
                this.mActionable = b;
            }
            else {
                this.mActionable = false;
            }
            if (!this.mActionable) {
                this.mListener = null;
            }
            else {
                this.mListener = (View.OnClickListener)new _$$Lambda$AnnotationSpan$LinkInfo$z7jQ60cPKy5FsRC4nTEr8I88qP0(intent);
            }
        }
        
        public LinkInfo(final String mAnnotation, final View.OnClickListener mListener) {
            this.mAnnotation = mAnnotation;
            this.mListener = mListener;
            this.mActionable = true;
        }
        
        public boolean isActionable() {
            return this.mActionable;
        }
    }
}
