package com.android.settings.utils;

import java.util.Iterator;
import android.content.ComponentName;
import android.util.Slog;
import android.content.pm.ResolveInfo;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import java.util.ArrayList;
import android.util.ArraySet;
import java.util.List;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ServiceInfo;
import java.util.Set;

public class ZenServiceListing
{
    private final Set<ServiceInfo> mApprovedServices;
    private final ManagedServiceSettings.Config mConfig;
    private final Context mContext;
    private final NotificationManager mNm;
    private final List<Callback> mZenCallbacks;
    
    public ZenServiceListing(final Context mContext, final ManagedServiceSettings.Config mConfig) {
        this.mApprovedServices = (Set<ServiceInfo>)new ArraySet();
        this.mZenCallbacks = new ArrayList<Callback>();
        this.mContext = mContext;
        this.mConfig = mConfig;
        this.mNm = (NotificationManager)mContext.getSystemService("notification");
    }
    
    private static int getServices(final ManagedServiceSettings.Config config, final List<ServiceInfo> list, final PackageManager packageManager) {
        int n = 0;
        if (list != null) {
            list.clear();
        }
        final List queryIntentServicesAsUser = packageManager.queryIntentServicesAsUser(new Intent(config.intentAction), 132, ActivityManager.getCurrentUser());
        for (int i = 0; i < queryIntentServicesAsUser.size(); ++i) {
            final ServiceInfo serviceInfo = queryIntentServicesAsUser.get(i).serviceInfo;
            if (!config.permission.equals(serviceInfo.permission)) {
                final String tag = config.tag;
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping ");
                sb.append(config.noun);
                sb.append(" service ");
                sb.append(serviceInfo.packageName);
                sb.append("/");
                sb.append(serviceInfo.name);
                sb.append(": it does not require the permission ");
                sb.append(config.permission);
                Slog.w(tag, sb.toString());
            }
            else {
                if (list != null) {
                    list.add(serviceInfo);
                }
                ++n;
            }
        }
        return n;
    }
    
    public void addZenCallback(final Callback callback) {
        this.mZenCallbacks.add(callback);
    }
    
    public ServiceInfo findService(final ComponentName componentName) {
        for (final ServiceInfo serviceInfo : this.mApprovedServices) {
            if (new ComponentName(serviceInfo.packageName, serviceInfo.name).equals((Object)componentName)) {
                return serviceInfo;
            }
        }
        return null;
    }
    
    public void reloadApprovedServices() {
        this.mApprovedServices.clear();
        final List enabledNotificationListenerPackages = this.mNm.getEnabledNotificationListenerPackages();
        final ArrayList<ServiceInfo> list = new ArrayList<ServiceInfo>();
        getServices(this.mConfig, list, this.mContext.getPackageManager());
        for (final ServiceInfo serviceInfo : list) {
            final String packageName = serviceInfo.getComponentName().getPackageName();
            if (this.mNm.isNotificationPolicyAccessGrantedForPackage(packageName) || enabledNotificationListenerPackages.contains(packageName)) {
                this.mApprovedServices.add(serviceInfo);
            }
        }
        if (!this.mApprovedServices.isEmpty()) {
            final Iterator<Callback> iterator2 = this.mZenCallbacks.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onServicesReloaded(this.mApprovedServices);
            }
        }
    }
    
    public void removeZenCallback(final Callback callback) {
        this.mZenCallbacks.remove(callback);
    }
    
    public interface Callback
    {
        void onServicesReloaded(final Set<ServiceInfo> p0);
    }
}
