package com.android.settings.utils;

import android.content.Intent;
import android.util.Log;
import android.app.VoiceInteractor$CompleteVoiceRequest;
import android.app.VoiceInteractor$Request;
import android.os.Bundle;
import android.app.VoiceInteractor$AbortVoiceRequest;
import android.app.Activity;

public abstract class VoiceSettingsActivity extends Activity
{
    protected void notifyFailure(final CharSequence charSequence) {
        if (this.getVoiceInteractor() != null) {
            this.getVoiceInteractor().submitRequest((VoiceInteractor$Request)new VoiceInteractor$AbortVoiceRequest(charSequence, (Bundle)null));
        }
    }
    
    protected void notifySuccess(final CharSequence charSequence) {
        if (this.getVoiceInteractor() != null) {
            this.getVoiceInteractor().submitRequest((VoiceInteractor$Request)new VoiceInteractor$CompleteVoiceRequest(charSequence, null) {
                public void onCompleteResult(final Bundle bundle) {
                    VoiceSettingsActivity.this.finish();
                }
            });
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (this.isVoiceInteractionRoot()) {
            if (this.onVoiceSettingInteraction(this.getIntent())) {
                this.finish();
            }
        }
        else {
            Log.v("VoiceSettingsActivity", "Cannot modify settings without voice interaction");
            this.finish();
        }
    }
    
    protected abstract boolean onVoiceSettingInteraction(final Intent p0);
}
