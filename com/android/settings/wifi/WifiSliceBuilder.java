package com.android.settings.wifi;

import android.text.TextUtils;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.util.Consumer;
import androidx.slice.builders.SliceAction;
import com.android.settingslib.Utils;
import androidx.slice.builders.ListBuilder;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.Slice;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;
import android.content.IntentFilter;

public class WifiSliceBuilder
{
    public static final IntentFilter INTENT_FILTER;
    public static final Uri WIFI_URI;
    
    static {
        WIFI_URI = new Uri.Builder().scheme("content").authority("android.settings.slices").appendPath("action").appendPath("wifi").build();
        (INTENT_FILTER = new IntentFilter()).addAction("android.net.wifi.WIFI_STATE_CHANGED");
        WifiSliceBuilder.INTENT_FILTER.addAction("android.net.wifi.STATE_CHANGE");
    }
    
    private static PendingIntent getBroadcastIntent(final Context context) {
        final Intent intent = new Intent("com.android.settings.wifi.action.WIFI_CHANGED");
        intent.setClass(context, (Class)SliceBroadcastReceiver.class);
        return PendingIntent.getBroadcast(context, 0, intent, 268435456);
    }
    
    public static Intent getIntent(final Context context) {
        return DatabaseIndexingUtils.buildSearchResultPageIntent(context, WifiSettings.class.getName(), "wifi", context.getText(2131890112).toString(), 603).setClassName(context.getPackageName(), SubSettings.class.getName()).setData(new Uri.Builder().appendPath("wifi").build());
    }
    
    private static PendingIntent getPrimaryAction(final Context context) {
        return PendingIntent.getActivity(context, 0, getIntent(context), 0);
    }
    
    public static Slice getSlice(final Context context) {
        final boolean wifiEnabled = isWifiEnabled(context);
        final IconCompat withResource = IconCompat.createWithResource(context, 2131231146);
        final String string = context.getString(2131890112);
        return new ListBuilder(context, WifiSliceBuilder.WIFI_URI, -1L).setAccentColor(Utils.getColorAccent(context)).addRow(new _$$Lambda$WifiSliceBuilder$zGyWboi_khe6O7kGcUmHExYnEzU(string, getSummary(context), new SliceAction(getBroadcastIntent(context), null, wifiEnabled), new SliceAction(getPrimaryAction(context), withResource, string))).build();
    }
    
    private static CharSequence getSummary(final Context context) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService((Class)WifiManager.class);
        switch (wifiManager.getWifiState()) {
            default: {
                return "";
            }
            case 3: {
                final String removeDoubleQuotes = WifiInfo.removeDoubleQuotes(wifiManager.getConnectionInfo().getSSID());
                if (TextUtils.equals((CharSequence)removeDoubleQuotes, (CharSequence)"<unknown ssid>")) {
                    return context.getText(2131887439);
                }
                return removeDoubleQuotes;
            }
            case 2: {
                return context.getText(2131887439);
            }
            case 0:
            case 1: {
                return context.getText(2131889421);
            }
        }
    }
    
    public static void handleUriChange(final Context context, final Intent intent) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService((Class)WifiManager.class);
        wifiManager.setWifiEnabled(intent.getBooleanExtra("android.app.slice.extra.TOGGLE_STATE", wifiManager.isWifiEnabled()));
    }
    
    private static boolean isWifiEnabled(final Context context) {
        switch (((WifiManager)context.getSystemService((Class)WifiManager.class)).getWifiState()) {
            default: {
                return false;
            }
            case 2:
            case 3: {
                return true;
            }
        }
    }
}
