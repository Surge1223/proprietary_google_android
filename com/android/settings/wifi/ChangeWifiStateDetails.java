package com.android.settings.wifi;

import android.app.Activity;
import android.os.Bundle;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.app.AlertDialog;
import com.android.settings.applications.AppStateBaseBridge;
import com.android.settings.applications.AppStateAppOpsBridge;
import com.android.settingslib.applications.ApplicationsState;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import android.app.AppOpsManager;
import android.support.v7.preference.Preference;
import com.android.settings.applications.AppInfoWithHeader;

public class ChangeWifiStateDetails extends AppInfoWithHeader implements OnPreferenceChangeListener
{
    private AppStateChangeWifiStateBridge mAppBridge;
    private AppOpsManager mAppOpsManager;
    private SwitchPreference mSwitchPref;
    private AppStateChangeWifiStateBridge.WifiSettingsState mWifiSettingsState;
    
    public static CharSequence getSummary(final Context context, final AppStateChangeWifiStateBridge.WifiSettingsState wifiSettingsState) {
        int n;
        if (((AppStateAppOpsBridge.PermissionState)wifiSettingsState).isPermissible()) {
            n = 2131886392;
        }
        else {
            n = 2131886393;
        }
        return context.getString(n);
    }
    
    public static CharSequence getSummary(final Context context, final AppEntry appEntry) {
        AppStateAppOpsBridge.PermissionState wifiSettingsInfo;
        if (appEntry.extraInfo instanceof AppStateChangeWifiStateBridge.WifiSettingsState) {
            wifiSettingsInfo = (AppStateChangeWifiStateBridge.WifiSettingsState)appEntry.extraInfo;
        }
        else if (appEntry.extraInfo instanceof AppStateAppOpsBridge.PermissionState) {
            wifiSettingsInfo = new AppStateChangeWifiStateBridge.WifiSettingsState((AppStateAppOpsBridge.PermissionState)appEntry.extraInfo);
        }
        else {
            wifiSettingsInfo = new AppStateChangeWifiStateBridge(context, null, null).getWifiSettingsInfo(appEntry.info.packageName, appEntry.info.uid);
        }
        return getSummary(context, (AppStateChangeWifiStateBridge.WifiSettingsState)wifiSettingsInfo);
    }
    
    private void setCanChangeWifiState(final boolean b) {
        this.logSpecialPermissionChange(b, this.mPackageName);
        final AppOpsManager mAppOpsManager = this.mAppOpsManager;
        final int uid = this.mPackageInfo.applicationInfo.uid;
        final String mPackageName = this.mPackageName;
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 1;
        }
        mAppOpsManager.setMode(71, uid, mPackageName, n);
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 338;
    }
    
    protected void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 774;
        }
        else {
            n = 775;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mAppBridge = new AppStateChangeWifiStateBridge((Context)activity, this.mState, null);
        this.mAppOpsManager = (AppOpsManager)((Context)activity).getSystemService("appops");
        this.addPreferencesFromResource(2132082731);
        (this.mSwitchPref = (SwitchPreference)this.findPreference("app_ops_settings_switch")).setTitle(2131887001);
        this.mSwitchPref.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mSwitchPref) {
            if (this.mWifiSettingsState != null && (boolean)o != ((AppStateAppOpsBridge.PermissionState)this.mWifiSettingsState).isPermissible()) {
                this.setCanChangeWifiState(((AppStateAppOpsBridge.PermissionState)this.mWifiSettingsState).isPermissible() ^ true);
                this.refreshUi();
            }
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean refreshUi() {
        if (this.mPackageInfo != null && this.mPackageInfo.applicationInfo != null) {
            this.mWifiSettingsState = this.mAppBridge.getWifiSettingsInfo(this.mPackageName, this.mPackageInfo.applicationInfo.uid);
            this.mSwitchPref.setChecked(((AppStateAppOpsBridge.PermissionState)this.mWifiSettingsState).isPermissible());
            this.mSwitchPref.setEnabled(this.mWifiSettingsState.permissionDeclared);
            return true;
        }
        return false;
    }
}
