package com.android.settings.wifi;

import android.os.Bundle;
import com.android.settings.SettingsPreferenceFragment;

public class WifiInfo extends SettingsPreferenceFragment
{
    @Override
    public int getMetricsCategory() {
        return 89;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082843);
    }
}
