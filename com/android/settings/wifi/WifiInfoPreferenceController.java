package com.android.settings.wifi;

import android.net.wifi.WifiInfo;
import android.support.v4.text.BidiFormatter;
import com.android.settings.Utils;
import android.text.TextUtils;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v7.preference.Preference;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiInfoPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final IntentFilter mFilter;
    private final BroadcastReceiver mReceiver;
    private Preference mWifiIpAddressPref;
    private Preference mWifiMacAddressPref;
    private final WifiManager mWifiManager;
    
    public WifiInfoPreferenceController(final Context context, final Lifecycle lifecycle, final WifiManager mWifiManager) {
        super(context);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (action.equals("android.net.wifi.LINK_CONFIGURATION_CHANGED") || action.equals("android.net.wifi.STATE_CHANGE")) {
                    WifiInfoPreferenceController.this.updateWifiInfo();
                }
            }
        };
        this.mWifiManager = mWifiManager;
        (this.mFilter = new IntentFilter()).addAction("android.net.wifi.LINK_CONFIGURATION_CHANGED");
        this.mFilter.addAction("android.net.wifi.STATE_CHANGE");
        lifecycle.addObserver(this);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mWifiMacAddressPref = preferenceScreen.findPreference("mac_address")).setSelectable(false);
        (this.mWifiIpAddressPref = preferenceScreen.findPreference("current_ip_address")).setSelectable(false);
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onPause() {
        this.mContext.unregisterReceiver(this.mReceiver);
    }
    
    @Override
    public void onResume() {
        this.mContext.registerReceiver(this.mReceiver, this.mFilter);
        this.updateWifiInfo();
    }
    
    public void updateWifiInfo() {
        if (this.mWifiMacAddressPref != null) {
            final WifiInfo connectionInfo = this.mWifiManager.getConnectionInfo();
            final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "wifi_connected_mac_randomization_enabled", 0);
            CharSequence macAddress;
            if (connectionInfo == null) {
                macAddress = null;
            }
            else {
                macAddress = connectionInfo.getMacAddress();
            }
            if (TextUtils.isEmpty(macAddress)) {
                this.mWifiMacAddressPref.setSummary(R.string.status_unavailable);
            }
            else if (int1 == 1 && "02:00:00:00:00:00".equals(macAddress)) {
                this.mWifiMacAddressPref.setSummary(R.string.wifi_status_mac_randomized);
            }
            else {
                this.mWifiMacAddressPref.setSummary(macAddress);
            }
        }
        if (this.mWifiIpAddressPref != null) {
            final String wifiIpAddresses = Utils.getWifiIpAddresses(this.mContext);
            final Preference mWifiIpAddressPref = this.mWifiIpAddressPref;
            String summary;
            if (wifiIpAddresses == null) {
                summary = this.mContext.getString(R.string.status_unavailable);
            }
            else {
                summary = BidiFormatter.getInstance().unicodeWrap(wifiIpAddresses);
            }
            mWifiIpAddressPref.setSummary(summary);
        }
    }
}
