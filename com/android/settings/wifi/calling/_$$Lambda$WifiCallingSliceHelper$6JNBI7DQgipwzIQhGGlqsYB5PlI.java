package com.android.settings.wifi.calling;

import android.database.ContentObserver;
import com.android.settings.slices.SliceBuilderUtils;
import android.os.PersistableBundle;
import android.content.ComponentName;
import android.text.TextUtils;
import android.telephony.CarrierConfigManager;
import android.util.Log;
import androidx.slice.builders.SliceAction;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Callable;
import com.android.ims.ImsManager;
import android.telephony.TelephonyManager;
import androidx.slice.Slice;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.net.Uri.Builder;
import android.telephony.SubscriptionManager;
import android.content.Context;
import android.net.Uri;
import androidx.slice.builders.ListBuilder;
import android.support.v4.graphics.drawable.IconCompat;
import android.app.PendingIntent;
import android.support.v4.util.Consumer;

public final class _$$Lambda$WifiCallingSliceHelper$6JNBI7DQgipwzIQhGGlqsYB5PlI implements Consumer
{
    @Override
    public final void accept(final Object o) {
        WifiCallingSliceHelper.lambda$getNonActionableWifiCallingSlice$1(this.f$0, this.f$1, this.f$2, this.f$3, (ListBuilder.RowBuilder)o);
    }
}
