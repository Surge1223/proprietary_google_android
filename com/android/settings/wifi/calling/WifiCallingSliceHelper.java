package com.android.settings.wifi.calling;

import android.database.ContentObserver;
import com.android.settings.slices.SliceBuilderUtils;
import android.os.PersistableBundle;
import android.content.ComponentName;
import android.text.TextUtils;
import android.telephony.CarrierConfigManager;
import android.util.Log;
import androidx.slice.builders.SliceAction;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Callable;
import com.android.ims.ImsManager;
import android.telephony.TelephonyManager;
import android.support.v4.util.Consumer;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.builders.ListBuilder;
import androidx.slice.Slice;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.net.Uri.Builder;
import android.telephony.SubscriptionManager;
import android.content.Context;
import android.net.Uri;

public class WifiCallingSliceHelper
{
    public static final Uri WIFI_CALLING_URI;
    private final Context mContext;
    protected SubscriptionManager mSubscriptionManager;
    
    static {
        WIFI_CALLING_URI = new Uri.Builder().scheme("content").authority("com.android.settings.slices").appendPath("wifi_calling").build();
    }
    
    public WifiCallingSliceHelper(final Context mContext) {
        this.mContext = mContext;
    }
    
    private PendingIntent getActivityIntent(final String s) {
        final Intent intent = new Intent(s);
        intent.addFlags(268435456);
        return PendingIntent.getActivity(this.mContext, 0, intent, 0);
    }
    
    private PendingIntent getBroadcastIntent(final String s) {
        final Intent intent = new Intent(s);
        intent.setClass(this.mContext, (Class)SliceBroadcastReceiver.class);
        return PendingIntent.getBroadcast(this.mContext, 0, intent, 268435456);
    }
    
    private Slice getNonActionableWifiCallingSlice(final String s, final String s2, final Uri uri, final PendingIntent pendingIntent) {
        return new ListBuilder(this.mContext, uri, -1L).setColor(2131099783).addRow(new _$$Lambda$WifiCallingSliceHelper$6JNBI7DQgipwzIQhGGlqsYB5PlI(s, s2, pendingIntent, IconCompat.createWithResource(this.mContext, 2131231303))).build();
    }
    
    public static PendingIntent getSettingsIntent(final Context context) {
        return PendingIntent.getActivity(context, 0, new Intent("android.settings.SETTINGS"), 0);
    }
    
    private String getSimCarrierName() {
        final CharSequence simCarrierIdName = ((TelephonyManager)this.mContext.getSystemService((Class)TelephonyManager.class)).getSimCarrierIdName();
        if (simCarrierIdName == null) {
            return this.mContext.getString(2131886972);
        }
        return simCarrierIdName.toString();
    }
    
    private Slice getWifiCallingSlice(final Uri uri, final Context context, final boolean b) {
        return new ListBuilder(context, uri, -1L).setColor(2131099783).addRow(new _$$Lambda$WifiCallingSliceHelper$zbtZymRgbM5OtQTuVraAeUKJDfQ(this, context.getString(2131889923), b, IconCompat.createWithResource(context, 2131231303))).build();
    }
    
    private boolean isWifiCallingEnabled(final ImsManager imsManager) throws InterruptedException, ExecutionException, TimeoutException {
        final FutureTask<Boolean> futureTask = new FutureTask<Boolean>(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return imsManager.isWfcEnabledByUser();
            }
        });
        Executors.newSingleThreadExecutor().execute(futureTask);
        boolean b = false;
        if (futureTask.get(2000L, TimeUnit.MILLISECONDS)) {
            b = b;
            if (imsManager.isNonTtyOrTtyOnVolteEnabled()) {
                b = true;
            }
        }
        return b;
    }
    
    public Slice createWifiCallingSlice(final Uri uri) {
        final int defaultVoiceSubId = this.getDefaultVoiceSubId();
        final String simCarrierName = this.getSimCarrierName();
        if (defaultVoiceSubId <= -1) {
            Log.d("WifiCallingSliceHelper", "Invalid subscription Id");
            return this.getNonActionableWifiCallingSlice(this.mContext.getString(2131889923), this.mContext.getString(2131889915, new Object[] { simCarrierName }), uri, getSettingsIntent(this.mContext));
        }
        final ImsManager imsManager = this.getImsManager(defaultVoiceSubId);
        if (imsManager.isWfcEnabledByPlatform()) {
            if (imsManager.isWfcProvisionedOnDevice()) {
                try {
                    final boolean wifiCallingEnabled = this.isWifiCallingEnabled(imsManager);
                    if (this.getWifiCallingCarrierActivityIntent(defaultVoiceSubId) != null && !wifiCallingEnabled) {
                        Log.d("WifiCallingSliceHelper", "Needs Activation");
                        return this.getNonActionableWifiCallingSlice(this.mContext.getString(2131889923), this.mContext.getString(2131889922), uri, this.getActivityIntent("android.settings.WIFI_CALLING_SETTINGS"));
                    }
                    return this.getWifiCallingSlice(uri, this.mContext, wifiCallingEnabled);
                }
                catch (InterruptedException | TimeoutException | ExecutionException ex) {
                    final Throwable t;
                    Log.e("WifiCallingSliceHelper", "Unable to read the current WiFi calling status", t);
                    return this.getNonActionableWifiCallingSlice(this.mContext.getString(2131889923), this.mContext.getString(2131889926), uri, this.getActivityIntent("android.settings.WIFI_CALLING_SETTINGS"));
                }
            }
        }
        Log.d("WifiCallingSliceHelper", "Wifi calling is either not provisioned or not enabled by Platform");
        return this.getNonActionableWifiCallingSlice(this.mContext.getString(2131889923), this.mContext.getString(2131889915, new Object[] { simCarrierName }), uri, getSettingsIntent(this.mContext));
    }
    
    protected CarrierConfigManager getCarrierConfigManager(final Context context) {
        return (CarrierConfigManager)context.getSystemService((Class)CarrierConfigManager.class);
    }
    
    protected int getDefaultVoiceSubId() {
        if (this.mSubscriptionManager == null) {
            this.mSubscriptionManager = (SubscriptionManager)this.mContext.getSystemService((Class)SubscriptionManager.class);
        }
        return SubscriptionManager.getDefaultVoiceSubscriptionId();
    }
    
    protected ImsManager getImsManager(final int n) {
        return ImsManager.getInstance(this.mContext, SubscriptionManager.getPhoneId(n));
    }
    
    protected Intent getWifiCallingCarrierActivityIntent(final int n) {
        final CarrierConfigManager carrierConfigManager = this.getCarrierConfigManager(this.mContext);
        if (carrierConfigManager == null) {
            return null;
        }
        final PersistableBundle configForSubId = carrierConfigManager.getConfigForSubId(n);
        if (configForSubId == null) {
            return null;
        }
        final String string = configForSubId.getString("wfc_emergency_address_carrier_app_string");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        final ComponentName unflattenFromString = ComponentName.unflattenFromString(string);
        if (unflattenFromString == null) {
            return null;
        }
        final Intent intent = new Intent();
        intent.setComponent(unflattenFromString);
        return intent;
    }
    
    public void handleWifiCallingChanged(Intent wifiCallingCarrierActivityIntent) {
        final int defaultVoiceSubId = this.getDefaultVoiceSubId();
        if (defaultVoiceSubId > -1) {
            final ImsManager imsManager = this.getImsManager(defaultVoiceSubId);
            if (imsManager.isWfcEnabledByPlatform() || imsManager.isWfcProvisionedOnDevice()) {
                final boolean b = imsManager.isWfcEnabledByUser() && imsManager.isNonTtyOrTtyOnVolteEnabled();
                final boolean booleanExtra = wifiCallingCarrierActivityIntent.getBooleanExtra("android.app.slice.extra.TOGGLE_STATE", b);
                wifiCallingCarrierActivityIntent = this.getWifiCallingCarrierActivityIntent(defaultVoiceSubId);
                if ((!booleanExtra || wifiCallingCarrierActivityIntent == null) && booleanExtra != b) {
                    imsManager.setWfcSetting(booleanExtra);
                }
            }
        }
        this.mContext.getContentResolver().notifyChange(SliceBuilderUtils.getUri("wifi_calling", false), (ContentObserver)null);
    }
}
