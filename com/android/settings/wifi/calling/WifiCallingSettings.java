package com.android.settings.wifi.calling;

import android.app.Fragment;
import android.util.Log;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.support.actionbar.HelpMenuController;
import com.android.settingslib.core.lifecycle.ObservableFragment;
import com.android.settings.search.actionbar.SearchMenuController;
import android.os.Bundle;
import com.android.ims.ImsManager;
import android.content.Context;
import android.telephony.SubscriptionManager;
import com.android.settings.widget.RtlCompatibleViewPager;
import com.android.settings.widget.SlidingTabLayout;
import android.telephony.SubscriptionInfo;
import java.util.List;
import com.android.settings.support.actionbar.HelpResourceProvider;
import com.android.settings.core.InstrumentedFragment;

public class WifiCallingSettings extends InstrumentedFragment implements HelpResourceProvider
{
    private WifiCallingViewPagerAdapter mPagerAdapter;
    private List<SubscriptionInfo> mSil;
    private SlidingTabLayout mTabLayout;
    private RtlCompatibleViewPager mViewPager;
    
    private void updateSubList() {
        this.mSil = (List<SubscriptionInfo>)SubscriptionManager.from((Context)this.getActivity()).getActiveSubscriptionInfoList();
        if (this.mSil == null) {
            return;
        }
        int i = 0;
        while (i < this.mSil.size()) {
            if (!ImsManager.getInstance((Context)this.getActivity(), this.mSil.get(i).getSimSlotIndex()).isWfcEnabledByPlatform()) {
                this.mSil.remove(i);
            }
            else {
                ++i;
            }
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887789;
    }
    
    @Override
    public int getMetricsCategory() {
        return 105;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setHasOptionsMenu(true);
        SearchMenuController.init(this);
        HelpMenuController.init(this);
        this.updateSubList();
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558884, viewGroup, false);
        this.mTabLayout = (SlidingTabLayout)inflate.findViewById(2131362619);
        this.mViewPager = (RtlCompatibleViewPager)inflate.findViewById(2131362808);
        this.mPagerAdapter = new WifiCallingViewPagerAdapter(this.getChildFragmentManager(), this.mViewPager);
        this.mViewPager.setAdapter(this.mPagerAdapter);
        return inflate;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (this.mSil != null && this.mSil.size() > 1) {
            this.mTabLayout.setViewPager(this.mViewPager);
        }
        else {
            this.mTabLayout.setVisibility(8);
        }
    }
    
    private final class WifiCallingViewPagerAdapter extends FragmentPagerAdapter
    {
        private final RtlCompatibleViewPager mViewPager;
        
        public WifiCallingViewPagerAdapter(final FragmentManager fragmentManager, final RtlCompatibleViewPager mViewPager) {
            super(fragmentManager);
            this.mViewPager = mViewPager;
        }
        
        @Override
        public int getCount() {
            if (WifiCallingSettings.this.mSil == null) {
                Log.d("WifiCallingSettings", "Adapter getCount null mSil ");
                return 0;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Adapter getCount ");
            sb.append(WifiCallingSettings.this.mSil.size());
            Log.d("WifiCallingSettings", sb.toString());
            return WifiCallingSettings.this.mSil.size();
        }
        
        @Override
        public Fragment getItem(final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Adapter getItem ");
            sb.append(n);
            Log.d("WifiCallingSettings", sb.toString());
            final Bundle arguments = new Bundle();
            arguments.putBoolean("need_search_icon_in_action_bar", false);
            arguments.putInt("subId", WifiCallingSettings.this.mSil.get(n).getSubscriptionId());
            final WifiCallingSettingsForSub wifiCallingSettingsForSub = new WifiCallingSettingsForSub();
            wifiCallingSettingsForSub.setArguments(arguments);
            return wifiCallingSettingsForSub;
        }
        
        @Override
        public CharSequence getPageTitle(final int n) {
            return String.valueOf(WifiCallingSettings.this.mSil.get(n).getDisplayName());
        }
        
        @Override
        public Object instantiateItem(final ViewGroup viewGroup, final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Adapter instantiateItem ");
            sb.append(n);
            Log.d("WifiCallingSettings", sb.toString());
            return super.instantiateItem(viewGroup, this.mViewPager.getRtlAwareIndex(n));
        }
    }
}
