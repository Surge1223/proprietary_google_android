package com.android.settings.wifi.calling;

import com.android.ims.ImsManager;
import android.content.Context;
import com.android.settings.SettingsActivity;

public class WifiCallingSuggestionActivity extends SettingsActivity
{
    public static boolean isSuggestionComplete(final Context context) {
        final boolean wfcEnabledByPlatform = ImsManager.isWfcEnabledByPlatform(context);
        boolean b = true;
        if (wfcEnabledByPlatform && ImsManager.isWfcProvisionedOnDevice(context)) {
            if (!ImsManager.isWfcEnabledByUser(context) || !ImsManager.isNonTtyOrTtyOnVolteEnabled(context)) {
                b = false;
            }
            return b;
        }
        return true;
    }
}
