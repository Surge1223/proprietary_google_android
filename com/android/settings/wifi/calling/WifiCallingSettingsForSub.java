package com.android.settings.wifi.calling;

import android.telephony.TelephonyManager;
import com.android.settings.Utils;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.telephony.SubscriptionManager;
import android.view.View;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.app.Activity;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.util.Log;
import android.content.ComponentName;
import android.text.TextUtils;
import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;
import android.telephony.CarrierConfigManager;
import com.android.settings.SettingsActivity;
import android.widget.Switch;
import android.telephony.PhoneStateListener;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.ims.ImsManager;
import android.widget.TextView;
import android.support.v7.preference.ListPreference;
import com.android.settings.widget.SwitchBar;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class WifiCallingSettingsForSub extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnSwitchChangeListener
{
    private ListPreference mButtonWfcMode;
    private ListPreference mButtonWfcRoamingMode;
    private boolean mEditableWfcMode;
    private boolean mEditableWfcRoamingMode;
    private TextView mEmptyView;
    private ImsManager mImsManager;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mIntentReceiver;
    private final PhoneStateListener mPhoneStateListener;
    private int mSubId;
    private Switch mSwitch;
    private SwitchBar mSwitchBar;
    private Preference mUpdateAddress;
    private final OnPreferenceClickListener mUpdateAddressListener;
    private boolean mValidListener;
    
    public WifiCallingSettingsForSub() {
        this.mValidListener = false;
        this.mEditableWfcMode = true;
        this.mEditableWfcRoamingMode = true;
        this.mSubId = -1;
        this.mPhoneStateListener = new PhoneStateListener() {
            public void onCallStateChanged(final int n, final String s) {
                final SettingsActivity settingsActivity = (SettingsActivity)WifiCallingSettingsForSub.this.getActivity();
                final boolean nonTtyOrTtyOnVolteEnabled = WifiCallingSettingsForSub.this.mImsManager.isNonTtyOrTtyOnVolteEnabled();
                final boolean checked = WifiCallingSettingsForSub.this.mSwitchBar.isChecked();
                final boolean b = false;
                final boolean b2 = checked && nonTtyOrTtyOnVolteEnabled;
                WifiCallingSettingsForSub.this.mSwitchBar.setEnabled(n == 0 && nonTtyOrTtyOnVolteEnabled);
                final boolean b3 = true;
                final boolean b4 = false;
                final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)settingsActivity.getSystemService("carrier_config");
                boolean boolean1 = b3;
                boolean boolean2 = b4;
                if (carrierConfigManager != null) {
                    final PersistableBundle configForSubId = carrierConfigManager.getConfigForSubId((int)this.mSubId);
                    boolean1 = b3;
                    boolean2 = b4;
                    if (configForSubId != null) {
                        boolean1 = configForSubId.getBoolean("editable_wfc_mode_bool");
                        boolean2 = configForSubId.getBoolean("editable_wfc_roaming_mode_bool");
                    }
                }
                final Preference preference = WifiCallingSettingsForSub.this.getPreferenceScreen().findPreference("wifi_calling_mode");
                if (preference != null) {
                    preference.setEnabled(b2 && boolean1 && n == 0);
                }
                final Preference preference2 = WifiCallingSettingsForSub.this.getPreferenceScreen().findPreference("wifi_calling_roaming_mode");
                if (preference2 != null) {
                    boolean enabled = b;
                    if (b2) {
                        enabled = b;
                        if (boolean2) {
                            enabled = b;
                            if (n == 0) {
                                enabled = true;
                            }
                        }
                    }
                    preference2.setEnabled(enabled);
                }
            }
        };
        this.mUpdateAddressListener = new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                final Intent access$200 = WifiCallingSettingsForSub.this.getCarrierActivityIntent();
                if (access$200 != null) {
                    access$200.putExtra("EXTRA_LAUNCH_CARRIER_APP", 1);
                    WifiCallingSettingsForSub.this.startActivity(access$200);
                }
                return true;
            }
        };
        this.mIntentReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("com.android.ims.REGISTRATION_ERROR")) {
                    this.setResultCode(0);
                    WifiCallingSettingsForSub.this.showAlert(intent);
                }
            }
        };
    }
    
    private Intent getCarrierActivityIntent() {
        final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)this.getActivity().getSystemService((Class)CarrierConfigManager.class);
        if (carrierConfigManager == null) {
            return null;
        }
        final PersistableBundle configForSubId = carrierConfigManager.getConfigForSubId(this.mSubId);
        if (configForSubId == null) {
            return null;
        }
        final String string = configForSubId.getString("wfc_emergency_address_carrier_app_string");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        final ComponentName unflattenFromString = ComponentName.unflattenFromString(string);
        if (unflattenFromString == null) {
            return null;
        }
        final Intent intent = new Intent();
        intent.setComponent(unflattenFromString);
        return intent;
    }
    
    private int getWfcModeSummary(final int n) {
        int n2 = 17041102;
        if (this.mImsManager.isWfcEnabledByUser()) {
            switch (n) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected WFC mode value: ");
                    sb.append(n);
                    Log.e("WifiCallingForSub", sb.toString());
                    n2 = n2;
                    break;
                }
                case 2: {
                    n2 = 17041069;
                    break;
                }
                case 1: {
                    n2 = 17041067;
                    break;
                }
                case 0: {
                    n2 = 17041068;
                    break;
                }
            }
        }
        return n2;
    }
    
    private void showAlert(final Intent intent) {
        final Activity activity = this.getActivity();
        final CharSequence charSequenceExtra = intent.getCharSequenceExtra("alertTitle");
        final CharSequence charSequenceExtra2 = intent.getCharSequenceExtra("alertMessage");
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
        alertDialog$Builder.setMessage(charSequenceExtra2).setTitle(charSequenceExtra).setIcon(17301543).setPositiveButton(17039370, (DialogInterface$OnClickListener)null);
        alertDialog$Builder.create().show();
    }
    
    private void updateBody() {
        final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)this.getSystemService("carrier_config");
        boolean boolean1 = true;
        if (carrierConfigManager != null) {
            final PersistableBundle configForSubId = carrierConfigManager.getConfigForSubId(this.mSubId);
            boolean1 = boolean1;
            if (configForSubId != null) {
                this.mEditableWfcMode = configForSubId.getBoolean("editable_wfc_mode_bool");
                this.mEditableWfcRoamingMode = configForSubId.getBoolean("editable_wfc_roaming_mode_bool");
                boolean1 = configForSubId.getBoolean("carrier_wfc_supports_wifi_only_bool", true);
            }
        }
        if (!boolean1) {
            this.mButtonWfcMode.setEntries(2130903181);
            this.mButtonWfcMode.setEntryValues(2130903183);
            this.mButtonWfcRoamingMode.setEntries(2130903180);
            this.mButtonWfcRoamingMode.setEntryValues(2130903183);
        }
        final boolean checked = this.mImsManager.isWfcEnabledByUser() && this.mImsManager.isNonTtyOrTtyOnVolteEnabled();
        this.mSwitch.setChecked(checked);
        final int wfcMode = this.mImsManager.getWfcMode(false);
        final int wfcMode2 = this.mImsManager.getWfcMode(true);
        this.mButtonWfcMode.setValue(Integer.toString(wfcMode));
        this.mButtonWfcRoamingMode.setValue(Integer.toString(wfcMode2));
        this.updateButtonWfcMode(checked, wfcMode, wfcMode2);
    }
    
    private void updateButtonWfcMode(final boolean b, int n, final int n2) {
        this.mButtonWfcMode.setSummary(this.getWfcModeSummary(n));
        final ListPreference mButtonWfcMode = this.mButtonWfcMode;
        n = 0;
        mButtonWfcMode.setEnabled(b && this.mEditableWfcMode);
        this.mButtonWfcRoamingMode.setEnabled(b && this.mEditableWfcRoamingMode);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (this.getCarrierActivityIntent() != null) {
            n = 1;
        }
        if (b) {
            if (this.mEditableWfcMode) {
                preferenceScreen.addPreference(this.mButtonWfcMode);
            }
            else {
                preferenceScreen.removePreference(this.mButtonWfcMode);
            }
            if (this.mEditableWfcRoamingMode) {
                preferenceScreen.addPreference(this.mButtonWfcRoamingMode);
            }
            else {
                preferenceScreen.removePreference(this.mButtonWfcRoamingMode);
            }
            if (n != 0) {
                preferenceScreen.addPreference(this.mUpdateAddress);
            }
            else {
                preferenceScreen.removePreference(this.mUpdateAddress);
            }
        }
        else {
            preferenceScreen.removePreference(this.mButtonWfcMode);
            preferenceScreen.removePreference(this.mButtonWfcRoamingMode);
            preferenceScreen.removePreference(this.mUpdateAddress);
        }
    }
    
    private void updateWfcMode(final boolean wfcSetting) {
        final StringBuilder sb = new StringBuilder();
        sb.append("updateWfcMode(");
        sb.append(wfcSetting);
        sb.append(")");
        Log.i("WifiCallingForSub", sb.toString());
        this.mImsManager.setWfcSetting(wfcSetting);
        final int wfcMode = this.mImsManager.getWfcMode(false);
        this.updateButtonWfcMode(wfcSetting, wfcMode, this.mImsManager.getWfcMode(true));
        if (wfcSetting) {
            this.mMetricsFeatureProvider.action((Context)this.getActivity(), this.getMetricsCategory(), wfcMode);
        }
        else {
            this.mMetricsFeatureProvider.action((Context)this.getActivity(), this.getMetricsCategory(), -1);
        }
    }
    
    @Override
    public int getHelpResource() {
        return 0;
    }
    
    @Override
    public int getMetricsCategory() {
        return 1230;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        this.setEmptyView((View)(this.mEmptyView = (TextView)this.getView().findViewById(16908292)));
        final StringBuilder sb = new StringBuilder();
        sb.append(settingsActivity.getString(2131889916));
        sb.append(settingsActivity.getString(2131889917));
        this.mEmptyView.setText((CharSequence)sb.toString());
        (this.mSwitchBar = (SwitchBar)this.getView().findViewById(2131362706)).show();
        this.mSwitch = this.mSwitchBar.getSwitch();
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        this.getActivity();
        if (n == 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("WFC emergency address activity result = ");
            sb.append(n2);
            Log.d("WifiCallingForSub", sb.toString());
            if (n2 == -1) {
                this.updateWfcMode(true);
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082867);
        if (this.getArguments() != null && this.getArguments().containsKey("subId")) {
            this.mSubId = this.getArguments().getInt("subId");
        }
        else if (bundle != null) {
            this.mSubId = bundle.getInt("subId", -1);
        }
        this.mImsManager = ImsManager.getInstance((Context)this.getActivity(), SubscriptionManager.getPhoneId(this.mSubId));
        (this.mButtonWfcMode = (ListPreference)this.findPreference("wifi_calling_mode")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        (this.mButtonWfcRoamingMode = (ListPreference)this.findPreference("wifi_calling_roaming_mode")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        (this.mUpdateAddress = this.findPreference("emergency_address_key")).setOnPreferenceClickListener(this.mUpdateAddressListener);
        (this.mIntentFilter = new IntentFilter()).addAction("com.android.ims.REGISTRATION_ERROR");
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558883, viewGroup, false);
        final ViewGroup viewGroup2 = (ViewGroup)inflate.findViewById(2131362462);
        Utils.prepareCustomPreferencesList(viewGroup, inflate, (View)viewGroup2, false);
        viewGroup2.addView(super.onCreateView(layoutInflater, viewGroup2, bundle));
        return inflate;
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.mSwitchBar.hide();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        final Activity activity = this.getActivity();
        if (this.mValidListener) {
            this.mValidListener = false;
            ((TelephonyManager)this.getSystemService("phone")).listen(this.mPhoneStateListener, 0);
            this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        }
        ((Context)activity).unregisterReceiver(this.mIntentReceiver);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mButtonWfcMode) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onPreferenceChange mButtonWfcMode ");
            sb.append(o);
            Log.d("WifiCallingForSub", sb.toString());
            this.mButtonWfcMode.setValue((String)o);
            final int intValue = Integer.valueOf((String)o);
            if (intValue != this.mImsManager.getWfcMode(false)) {
                this.mImsManager.setWfcMode(intValue, false);
                this.mButtonWfcMode.setSummary(this.getWfcModeSummary(intValue));
                this.mMetricsFeatureProvider.action((Context)this.getActivity(), this.getMetricsCategory(), intValue);
            }
            if (!this.mEditableWfcRoamingMode && intValue != this.mImsManager.getWfcMode(true)) {
                this.mImsManager.setWfcMode(intValue, true);
            }
        }
        else if (preference == this.mButtonWfcRoamingMode) {
            this.mButtonWfcRoamingMode.setValue((String)o);
            final int intValue2 = Integer.valueOf((String)o);
            if (intValue2 != this.mImsManager.getWfcMode(true)) {
                this.mImsManager.setWfcMode(intValue2, true);
                this.mMetricsFeatureProvider.action((Context)this.getActivity(), this.getMetricsCategory(), intValue2);
            }
        }
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final Activity activity = this.getActivity();
        this.updateBody();
        if (this.mImsManager.isWfcEnabledByPlatform()) {
            ((TelephonyManager)this.getSystemService("phone")).listen(this.mPhoneStateListener, 32);
            this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
            this.mValidListener = true;
        }
        ((Context)activity).registerReceiver(this.mIntentReceiver, this.mIntentFilter);
        final Intent intent = this.getActivity().getIntent();
        if (intent.getBooleanExtra("alertShow", false)) {
            this.showAlert(intent);
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putInt("subId", this.mSubId);
        super.onSaveInstanceState(bundle);
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onSwitchChanged(");
        sb.append(b);
        sb.append(")");
        Log.d("WifiCallingForSub", sb.toString());
        if (!b) {
            this.updateWfcMode(false);
            return;
        }
        final Intent carrierActivityIntent = this.getCarrierActivityIntent();
        if (carrierActivityIntent != null) {
            carrierActivityIntent.putExtra("EXTRA_LAUNCH_CARRIER_APP", 0);
            this.startActivityForResult(carrierActivityIntent, 1);
        }
        else {
            this.updateWfcMode(true);
        }
    }
}
