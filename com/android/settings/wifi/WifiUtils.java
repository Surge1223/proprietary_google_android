package com.android.settings.wifi;

import android.content.ComponentName;
import android.provider.Settings;
import android.content.pm.PackageManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.app.admin.DevicePolicyManager;
import android.net.wifi.WifiConfiguration;
import android.content.Context;
import android.text.TextUtils;
import android.net.NetworkCapabilities;

public class WifiUtils
{
    public static boolean canSignIntoNetwork(final NetworkCapabilities networkCapabilities) {
        return networkCapabilities != null && networkCapabilities.hasCapability(17);
    }
    
    public static boolean isHotspotPasswordValid(final String s) {
        final boolean empty = TextUtils.isEmpty((CharSequence)s);
        final boolean b = false;
        if (empty) {
            return false;
        }
        final int length = s.length();
        boolean b2 = b;
        if (length >= 8) {
            b2 = b;
            if (length <= 63) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean isNetworkLockedDown(final Context context, final WifiConfiguration wifiConfiguration) {
        boolean b = false;
        if (wifiConfiguration == null) {
            return false;
        }
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        final PackageManagerWrapper packageManagerWrapper = new PackageManagerWrapper(context.getPackageManager());
        if (packageManagerWrapper.hasSystemFeature("android.software.device_admin") && devicePolicyManager == null) {
            return true;
        }
        boolean b3;
        final boolean b2 = b3 = false;
        if (devicePolicyManager != null) {
            final ComponentName deviceOwnerComponentOnAnyUser = devicePolicyManager.getDeviceOwnerComponentOnAnyUser();
            b3 = b2;
            if (deviceOwnerComponentOnAnyUser != null) {
                final int deviceOwnerUserId = devicePolicyManager.getDeviceOwnerUserId();
                try {
                    b3 = (packageManagerWrapper.getPackageUidAsUser(deviceOwnerComponentOnAnyUser.getPackageName(), deviceOwnerUserId) == wifiConfiguration.creatorUid);
                }
                catch (PackageManager$NameNotFoundException ex) {
                    b3 = b2;
                }
            }
        }
        if (!b3) {
            return false;
        }
        if (Settings.Global.getInt(context.getContentResolver(), "wifi_device_owner_configs_lockdown", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    public static boolean isSSIDTooLong(final String s) {
        final boolean empty = TextUtils.isEmpty((CharSequence)s);
        boolean b = false;
        if (empty) {
            return false;
        }
        if (s.length() > 32) {
            b = true;
        }
        return b;
    }
    
    public static boolean isSSIDTooShort(final String s) {
        final boolean empty = TextUtils.isEmpty((CharSequence)s);
        boolean b = true;
        if (empty) {
            return true;
        }
        if (s.length() >= 1) {
            b = false;
        }
        return b;
    }
}
