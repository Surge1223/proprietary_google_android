package com.android.settings.wifi;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class CellularFallbackPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public CellularFallbackPreferenceController(final Context context) {
        super(context);
    }
    
    private boolean avoidBadWifiConfig() {
        final int integer = this.mContext.getResources().getInteger(17694825);
        boolean b = true;
        if (integer != 1) {
            b = false;
        }
        return b;
    }
    
    private boolean avoidBadWifiCurrentSettings() {
        return "1".equals(Settings.Global.getString(this.mContext.getContentResolver(), "network_avoid_bad_wifi"));
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_cellular_data_fallback";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"wifi_cellular_data_fallback")) {
            return false;
        }
        if (!(preference instanceof SwitchPreference)) {
            return false;
        }
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        String s;
        if (((SwitchPreference)preference).isChecked()) {
            s = "1";
        }
        else {
            s = null;
        }
        Settings.Global.putString(contentResolver, "network_avoid_bad_wifi", s);
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return this.avoidBadWifiConfig() ^ true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final boolean avoidBadWifiCurrentSettings = this.avoidBadWifiCurrentSettings();
        if (preference != null) {
            ((SwitchPreference)preference).setChecked(avoidBadWifiCurrentSettings);
        }
    }
}
