package com.android.settings.wifi;

import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.widget.MasterSwitchController;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.widget.MasterSwitchPreference;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.widget.SummaryUpdater;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiMasterSwitchPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, OnSummaryChangeListener, LifecycleObserver, OnPause, OnResume, OnStart, OnStop
{
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final WifiSummaryUpdater mSummaryHelper;
    private WifiEnabler mWifiEnabler;
    private MasterSwitchPreference mWifiPreference;
    
    public WifiMasterSwitchPreferenceController(final Context context, final MetricsFeatureProvider mMetricsFeatureProvider) {
        super(context);
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        this.mSummaryHelper = new WifiSummaryUpdater(this.mContext, this);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mWifiPreference = (MasterSwitchPreference)preferenceScreen.findPreference("toggle_wifi");
    }
    
    @Override
    public String getPreferenceKey() {
        return "toggle_wifi";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034176);
    }
    
    @Override
    public void onPause() {
        if (this.mWifiEnabler != null) {
            this.mWifiEnabler.pause();
        }
        this.mSummaryHelper.register(false);
    }
    
    @Override
    public void onResume() {
        this.mSummaryHelper.register(true);
        if (this.mWifiEnabler != null) {
            this.mWifiEnabler.resume(this.mContext);
        }
    }
    
    @Override
    public void onStart() {
        this.mWifiEnabler = new WifiEnabler(this.mContext, new MasterSwitchController(this.mWifiPreference), this.mMetricsFeatureProvider);
    }
    
    @Override
    public void onStop() {
        if (this.mWifiEnabler != null) {
            this.mWifiEnabler.teardownSwitchController();
        }
    }
    
    @Override
    public void onSummaryChanged(final String summary) {
        if (this.mWifiPreference != null) {
            this.mWifiPreference.setSummary(summary);
        }
    }
}
