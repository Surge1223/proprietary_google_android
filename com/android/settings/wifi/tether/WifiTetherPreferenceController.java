package com.android.settings.wifi.tether;

import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.text.BidiFormatter;
import android.net.wifi.WifiConfiguration;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Intent;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.content.BroadcastReceiver;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.net.ConnectivityManager;
import android.content.IntentFilter;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiTetherPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart, OnStop
{
    private static final IntentFilter AIRPLANE_INTENT_FILTER;
    private final ConnectivityManager mConnectivityManager;
    private final Lifecycle mLifecycle;
    Preference mPreference;
    private final BroadcastReceiver mReceiver;
    private int mSoftApState;
    private final WifiManager mWifiManager;
    private final String[] mWifiRegexs;
    WifiTetherSoftApManager mWifiTetherSoftApManager;
    
    static {
        AIRPLANE_INTENT_FILTER = new IntentFilter("android.intent.action.AIRPLANE_MODE");
    }
    
    public WifiTetherPreferenceController(final Context context, final Lifecycle lifecycle) {
        this(context, lifecycle, true);
    }
    
    WifiTetherPreferenceController(final Context context, final Lifecycle mLifecycle, final boolean b) {
        super(context);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if ("android.intent.action.AIRPLANE_MODE".equals(intent.getAction())) {
                    WifiTetherPreferenceController.this.clearSummaryForAirplaneMode(2131890020);
                }
            }
        };
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
        this.mWifiManager = (WifiManager)context.getSystemService("wifi");
        this.mWifiRegexs = this.mConnectivityManager.getTetherableWifiRegexs();
        this.mLifecycle = mLifecycle;
        if (mLifecycle != null) {
            mLifecycle.addObserver(this);
        }
        if (b) {
            this.initWifiTetherSoftApManager();
        }
    }
    
    private void clearSummaryForAirplaneMode() {
        this.clearSummaryForAirplaneMode(-1);
    }
    
    private void clearSummaryForAirplaneMode(final int summary) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 0) {
            b = true;
        }
        if (b) {
            this.mPreference.setSummary(2131890180);
        }
        else if (summary != -1) {
            this.mPreference.setSummary(summary);
        }
    }
    
    private void updateConfigSummary(final WifiConfiguration wifiConfiguration) {
        final String string = this.mContext.getString(17041124);
        final Preference mPreference = this.mPreference;
        final Context mContext = this.mContext;
        final BidiFormatter instance = BidiFormatter.getInstance();
        String ssid;
        if (wifiConfiguration == null) {
            ssid = string;
        }
        else {
            ssid = wifiConfiguration.SSID;
        }
        mPreference.setSummary(mContext.getString(2131890181, new Object[] { instance.unicodeWrap(ssid) }));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference("wifi_tether");
        if (this.mPreference == null) {
            return;
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_tether";
    }
    
    void handleWifiApStateChanged(final int n, final int n2) {
        switch (n) {
            default: {
                if (n2 == 1) {
                    this.mPreference.setSummary(2131890082);
                }
                else {
                    this.mPreference.setSummary(2131889993);
                }
                this.clearSummaryForAirplaneMode();
                break;
            }
            case 13: {
                this.updateConfigSummary(this.mWifiManager.getWifiApConfiguration());
                break;
            }
            case 12: {
                this.mPreference.setSummary(2131890183);
                break;
            }
            case 11: {
                this.mPreference.setSummary(2131890020);
                this.clearSummaryForAirplaneMode();
                break;
            }
            case 10: {
                this.mPreference.setSummary(2131890184);
                break;
            }
        }
    }
    
    void initWifiTetherSoftApManager() {
        this.mWifiTetherSoftApManager = new WifiTetherSoftApManager(this.mWifiManager, (WifiTetherSoftApManager.WifiTetherSoftApCallback)new WifiTetherSoftApManager.WifiTetherSoftApCallback() {
            @Override
            public void onNumClientsChanged(final int n) {
                if (WifiTetherPreferenceController.this.mPreference != null && WifiTetherPreferenceController.this.mSoftApState == 13) {
                    WifiTetherPreferenceController.this.mPreference.setSummary(WifiTetherPreferenceController.this.mContext.getResources().getQuantityString(2131755073, n, new Object[] { n }));
                }
            }
            
            @Override
            public void onStateChanged(final int n, final int n2) {
                WifiTetherPreferenceController.this.mSoftApState = n;
                WifiTetherPreferenceController.this.handleWifiApStateChanged(n, n2);
            }
        });
    }
    
    @Override
    public boolean isAvailable() {
        return this.mWifiRegexs != null && this.mWifiRegexs.length != 0 && !Utils.isMonkeyRunning();
    }
    
    @Override
    public void onStart() {
        if (this.mPreference != null) {
            this.mContext.registerReceiver(this.mReceiver, WifiTetherPreferenceController.AIRPLANE_INTENT_FILTER);
            this.clearSummaryForAirplaneMode();
            if (this.mWifiTetherSoftApManager != null) {
                this.mWifiTetherSoftApManager.registerSoftApCallback();
            }
        }
    }
    
    @Override
    public void onStop() {
        if (this.mPreference != null) {
            this.mContext.unregisterReceiver(this.mReceiver);
            if (this.mWifiTetherSoftApManager != null) {
                this.mWifiTetherSoftApManager.unRegisterSoftApCallback();
            }
        }
    }
}
