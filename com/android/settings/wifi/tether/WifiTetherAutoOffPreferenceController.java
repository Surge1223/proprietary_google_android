package com.android.settings.wifi.tether;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class WifiTetherAutoOffPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    public WifiTetherAutoOffPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "soft_ap_timeout_enabled", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = true;
        if (Settings.Global.getInt(contentResolver, "soft_ap_timeout_enabled", 1) == 0) {
            checked = false;
        }
        ((SwitchPreference)preference).setChecked(checked);
    }
}
