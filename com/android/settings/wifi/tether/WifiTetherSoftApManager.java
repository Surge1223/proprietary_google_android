package com.android.settings.wifi.tether;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager$SoftApCallback;
import android.os.Handler;

public class WifiTetherSoftApManager
{
    private Handler mHandler;
    private WifiManager$SoftApCallback mSoftApCallback;
    private WifiManager mWifiManager;
    private WifiTetherSoftApCallback mWifiTetherSoftApCallback;
    
    WifiTetherSoftApManager(final WifiManager mWifiManager, final WifiTetherSoftApCallback mWifiTetherSoftApCallback) {
        this.mSoftApCallback = (WifiManager$SoftApCallback)new WifiManager$SoftApCallback() {
            public void onNumClientsChanged(final int n) {
                WifiTetherSoftApManager.this.mWifiTetherSoftApCallback.onNumClientsChanged(n);
            }
            
            public void onStateChanged(final int n, final int n2) {
                WifiTetherSoftApManager.this.mWifiTetherSoftApCallback.onStateChanged(n, n2);
            }
        };
        this.mWifiManager = mWifiManager;
        this.mWifiTetherSoftApCallback = mWifiTetherSoftApCallback;
        this.mHandler = new Handler();
    }
    
    public void registerSoftApCallback() {
        this.mWifiManager.registerSoftApCallback(this.mSoftApCallback, this.mHandler);
    }
    
    public void unRegisterSoftApCallback() {
        this.mWifiManager.unregisterSoftApCallback(this.mSoftApCallback);
    }
    
    public interface WifiTetherSoftApCallback
    {
        void onNumClientsChanged(final int p0);
        
        void onStateChanged(final int p0, final int p1);
    }
}
