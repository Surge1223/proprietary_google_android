package com.android.settings.wifi.tether;

import android.content.Intent;
import android.util.Log;
import android.content.BroadcastReceiver;
import com.android.settings.widget.SwitchBar;
import android.arch.lifecycle.LifecycleObserver;
import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.widget.SwitchBarController;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.content.IntentFilter;
import com.android.settings.dashboard.RestrictedDashboardFragment;

public class WifiTetherSettings extends RestrictedDashboardFragment implements OnTetherConfigUpdateListener
{
    private static final IntentFilter TETHER_STATE_CHANGE_FILTER;
    private WifiTetherApBandPreferenceController mApBandPreferenceController;
    private WifiTetherPasswordPreferenceController mPasswordPreferenceController;
    private boolean mRestartWifiApAfterConfigChange;
    private WifiTetherSSIDPreferenceController mSSIDPreferenceController;
    private WifiTetherSecurityPreferenceController mSecurityPreferenceController;
    private WifiTetherSwitchBarController mSwitchBarController;
    TetherChangeReceiver mTetherChangeReceiver;
    private WifiManager mWifiManager;
    
    static {
        (TETHER_STATE_CHANGE_FILTER = new IntentFilter("android.net.conn.TETHER_STATE_CHANGED")).addAction("android.net.wifi.WIFI_AP_STATE_CHANGED");
    }
    
    public WifiTetherSettings() {
        super("no_config_tethering");
    }
    
    private WifiConfiguration buildNewConfig() {
        final WifiConfiguration wifiConfiguration = new WifiConfiguration();
        final int securityType = this.mSecurityPreferenceController.getSecurityType();
        wifiConfiguration.SSID = this.mSSIDPreferenceController.getSSID();
        wifiConfiguration.allowedKeyManagement.set(securityType);
        wifiConfiguration.preSharedKey = this.mPasswordPreferenceController.getPasswordValidated(securityType);
        wifiConfiguration.allowedAuthAlgorithms.set(0);
        wifiConfiguration.apBand = this.mApBandPreferenceController.getBandIndex();
        return wifiConfiguration;
    }
    
    private void startTether() {
        this.mRestartWifiApAfterConfigChange = false;
        this.mSwitchBarController.startTether();
    }
    
    private void updateDisplayWithNewConfig() {
        this.use(WifiTetherSSIDPreferenceController.class).updateDisplay();
        this.use(WifiTetherSecurityPreferenceController.class).updateDisplay();
        this.use(WifiTetherPasswordPreferenceController.class).updateDisplay();
        this.use(WifiTetherApBandPreferenceController.class).updateDisplay();
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<WifiTetherAutoOffPreferenceController> list = (ArrayList<WifiTetherAutoOffPreferenceController>)new ArrayList<WifiTetherApBandPreferenceController>();
        this.mSSIDPreferenceController = new WifiTetherSSIDPreferenceController(context, this);
        this.mSecurityPreferenceController = new WifiTetherSecurityPreferenceController(context, this);
        this.mPasswordPreferenceController = new WifiTetherPasswordPreferenceController(context, this);
        this.mApBandPreferenceController = new WifiTetherApBandPreferenceController(context, this);
        list.add((WifiTetherApBandPreferenceController)this.mSSIDPreferenceController);
        list.add((WifiTetherApBandPreferenceController)this.mSecurityPreferenceController);
        list.add((WifiTetherApBandPreferenceController)this.mPasswordPreferenceController);
        list.add(this.mApBandPreferenceController);
        list.add(new WifiTetherAutoOffPreferenceController(context, "wifi_tether_auto_turn_off"));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "WifiTetherSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1014;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082874;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        final SwitchBar switchBar = settingsActivity.getSwitchBar();
        this.mSwitchBarController = new WifiTetherSwitchBarController((Context)settingsActivity, new SwitchBarController(switchBar));
        this.getLifecycle().addObserver(this.mSwitchBarController);
        switchBar.show();
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mWifiManager = (WifiManager)context.getSystemService("wifi");
        this.mTetherChangeReceiver = new TetherChangeReceiver();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        final Context context = this.getContext();
        if (context != null) {
            context.registerReceiver((BroadcastReceiver)this.mTetherChangeReceiver, WifiTetherSettings.TETHER_STATE_CHANGE_FILTER);
        }
    }
    
    @Override
    public void onStop() {
        super.onStop();
        final Context context = this.getContext();
        if (context != null) {
            context.unregisterReceiver((BroadcastReceiver)this.mTetherChangeReceiver);
        }
    }
    
    @Override
    public void onTetherConfigUpdated() {
        final WifiConfiguration buildNewConfig = this.buildNewConfig();
        this.mPasswordPreferenceController.updateVisibility(buildNewConfig.getAuthType());
        if (this.mWifiManager.getWifiApState() == 13) {
            Log.d("TetheringSettings", "Wifi AP config changed while enabled, stop and restart");
            this.mRestartWifiApAfterConfigChange = true;
            this.mSwitchBarController.stopTether();
        }
        this.mWifiManager.setWifiApConfiguration(buildNewConfig);
    }
    
    class TetherChangeReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            final StringBuilder sb = new StringBuilder();
            sb.append("updating display config due to receiving broadcast action ");
            sb.append(action);
            Log.d("WifiTetherSettings", sb.toString());
            WifiTetherSettings.this.updateDisplayWithNewConfig();
            if (action.equals("android.net.conn.TETHER_STATE_CHANGED")) {
                if (WifiTetherSettings.this.mWifiManager.getWifiApState() == 11 && WifiTetherSettings.this.mRestartWifiApAfterConfigChange) {
                    WifiTetherSettings.this.startTether();
                }
            }
            else if (action.equals("android.net.wifi.WIFI_AP_STATE_CHANGED") && intent.getIntExtra("wifi_state", 0) == 11 && WifiTetherSettings.this.mRestartWifiApAfterConfigChange) {
                WifiTetherSettings.this.startTether();
            }
        }
    }
}
