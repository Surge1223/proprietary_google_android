package com.android.settings.wifi.tether;

import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.ConnectivityManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class WifiTetherBasePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    protected final ConnectivityManager mCm;
    protected final OnTetherConfigUpdateListener mListener;
    protected Preference mPreference;
    protected final WifiManager mWifiManager;
    protected final String[] mWifiRegexs;
    
    public WifiTetherBasePreferenceController(final Context context, final OnTetherConfigUpdateListener mListener) {
        super(context);
        this.mListener = mListener;
        this.mWifiManager = (WifiManager)context.getSystemService("wifi");
        this.mCm = (ConnectivityManager)context.getSystemService("connectivity");
        this.mWifiRegexs = this.mCm.getTetherableWifiRegexs();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
        this.updateDisplay();
    }
    
    @Override
    public boolean isAvailable() {
        return this.mWifiManager != null && this.mWifiRegexs != null && this.mWifiRegexs.length > 0;
    }
    
    public abstract void updateDisplay();
    
    public interface OnTetherConfigUpdateListener
    {
        void onTetherConfigUpdated();
    }
}
