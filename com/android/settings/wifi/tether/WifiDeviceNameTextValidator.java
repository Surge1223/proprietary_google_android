package com.android.settings.wifi.tether;

import com.android.settings.wifi.WifiUtils;
import com.android.settings.widget.ValidatedEditTextPreference;

public class WifiDeviceNameTextValidator implements Validator
{
    @Override
    public boolean isTextValid(final String s) {
        return !WifiUtils.isSSIDTooLong(s) && !WifiUtils.isSSIDTooShort(s);
    }
}
