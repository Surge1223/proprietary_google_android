package com.android.settings.wifi.tether;

import android.net.wifi.WifiConfiguration;
import android.util.Log;
import android.support.v7.preference.Preference;
import android.support.v7.preference.EditTextPreference;
import android.content.Context;
import com.android.settings.widget.ValidatedEditTextPreference;

public class WifiTetherSSIDPreferenceController extends WifiTetherBasePreferenceController implements Validator
{
    static final String DEFAULT_SSID = "AndroidAP";
    private String mSSID;
    private WifiDeviceNameTextValidator mWifiDeviceNameTextValidator;
    
    public WifiTetherSSIDPreferenceController(final Context context, final OnTetherConfigUpdateListener onTetherConfigUpdateListener) {
        super(context, onTetherConfigUpdateListener);
        this.mWifiDeviceNameTextValidator = new WifiDeviceNameTextValidator();
    }
    
    private void updateSsidDisplay(final EditTextPreference editTextPreference) {
        editTextPreference.setText(this.mSSID);
        editTextPreference.setSummary(this.mSSID);
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_tether_network_name";
    }
    
    public String getSSID() {
        return this.mSSID;
    }
    
    @Override
    public boolean isTextValid(final String s) {
        return this.mWifiDeviceNameTextValidator.isTextValid(s);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mSSID = (String)o;
        this.updateSsidDisplay((EditTextPreference)preference);
        this.mListener.onTetherConfigUpdated();
        return true;
    }
    
    @Override
    public void updateDisplay() {
        final WifiConfiguration wifiApConfiguration = this.mWifiManager.getWifiApConfiguration();
        if (wifiApConfiguration != null) {
            this.mSSID = wifiApConfiguration.SSID;
            final StringBuilder sb = new StringBuilder();
            sb.append("Updating SSID in Preference, ");
            sb.append(this.mSSID);
            Log.d("WifiTetherSsidPref", sb.toString());
        }
        else {
            this.mSSID = "AndroidAP";
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Updating to default SSID in Preference, ");
            sb2.append(this.mSSID);
            Log.d("WifiTetherSsidPref", sb2.toString());
        }
        ((ValidatedEditTextPreference)this.mPreference).setValidator((ValidatedEditTextPreference.Validator)this);
        this.updateSsidDisplay((EditTextPreference)this.mPreference);
    }
}
