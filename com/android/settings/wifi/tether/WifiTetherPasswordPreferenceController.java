package com.android.settings.wifi.tether;

import android.net.wifi.WifiConfiguration;
import android.support.v7.preference.Preference;
import com.android.settings.wifi.WifiUtils;
import android.text.TextUtils;
import android.support.v7.preference.EditTextPreference;
import java.util.UUID;
import android.content.Context;
import com.android.settings.widget.ValidatedEditTextPreference;

public class WifiTetherPasswordPreferenceController extends WifiTetherBasePreferenceController implements Validator
{
    private String mPassword;
    
    public WifiTetherPasswordPreferenceController(final Context context, final OnTetherConfigUpdateListener onTetherConfigUpdateListener) {
        super(context, onTetherConfigUpdateListener);
    }
    
    private static String generateRandomPassword() {
        final String string = UUID.randomUUID().toString();
        final StringBuilder sb = new StringBuilder();
        sb.append(string.substring(0, 8));
        sb.append(string.substring(9, 13));
        return sb.toString();
    }
    
    private void updatePasswordDisplay(final EditTextPreference editTextPreference) {
        final ValidatedEditTextPreference validatedEditTextPreference = (ValidatedEditTextPreference)editTextPreference;
        validatedEditTextPreference.setText(this.mPassword);
        if (!TextUtils.isEmpty((CharSequence)this.mPassword)) {
            validatedEditTextPreference.setIsSummaryPassword(true);
            validatedEditTextPreference.setSummary(this.mPassword);
            validatedEditTextPreference.setVisible(true);
        }
        else {
            validatedEditTextPreference.setIsSummaryPassword(false);
            validatedEditTextPreference.setSummary(2131890019);
            validatedEditTextPreference.setVisible(false);
        }
    }
    
    public String getPasswordValidated(final int n) {
        if (n == 0) {
            return "";
        }
        if (!this.isTextValid(this.mPassword)) {
            this.mPassword = generateRandomPassword();
            this.updatePasswordDisplay((EditTextPreference)this.mPreference);
        }
        return this.mPassword;
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_tether_network_password";
    }
    
    @Override
    public boolean isTextValid(final String s) {
        return WifiUtils.isHotspotPasswordValid(s);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mPassword = (String)o;
        this.updatePasswordDisplay((EditTextPreference)this.mPreference);
        this.mListener.onTetherConfigUpdated();
        return true;
    }
    
    @Override
    public void updateDisplay() {
        final WifiConfiguration wifiApConfiguration = this.mWifiManager.getWifiApConfiguration();
        if (wifiApConfiguration != null && (wifiApConfiguration.getAuthType() != 4 || !TextUtils.isEmpty((CharSequence)wifiApConfiguration.preSharedKey))) {
            this.mPassword = wifiApConfiguration.preSharedKey;
        }
        else {
            this.mPassword = generateRandomPassword();
        }
        ((ValidatedEditTextPreference)this.mPreference).setValidator((ValidatedEditTextPreference.Validator)this);
        ((ValidatedEditTextPreference)this.mPreference).setIsPassword(true);
        ((ValidatedEditTextPreference)this.mPreference).setIsSummaryPassword(true);
        this.updatePasswordDisplay((EditTextPreference)this.mPreference);
    }
    
    public void updateVisibility(final int n) {
        this.mPreference.setVisible(n != 0);
    }
}
