package com.android.settings.wifi.tether;

import android.content.res.Resources;
import android.net.wifi.WifiConfiguration;
import android.support.v7.preference.ListPreference;
import android.util.Log;
import android.support.v7.preference.Preference;
import android.content.Context;

public class WifiTetherApBandPreferenceController extends WifiTetherBasePreferenceController
{
    private boolean isDualMode;
    private String[] mBandEntries;
    private int mBandIndex;
    private String[] mBandSummaries;
    
    public WifiTetherApBandPreferenceController(final Context context, final OnTetherConfigUpdateListener onTetherConfigUpdateListener) {
        super(context, onTetherConfigUpdateListener);
        this.isDualMode = this.mWifiManager.isDualModeSupported();
        this.updatePreferenceEntries();
    }
    
    private boolean is5GhzBandSupported() {
        final String countryCode = this.mWifiManager.getCountryCode();
        return this.mWifiManager.isDualBandSupported() && countryCode != null;
    }
    
    private int validateSelection(final int n) {
        final boolean dualModeSupported = this.mWifiManager.isDualModeSupported();
        if (!dualModeSupported && -1 == n) {
            return 1;
        }
        if (!this.is5GhzBandSupported() && 1 == n) {
            return 0;
        }
        if (dualModeSupported && 1 == n) {
            return -1;
        }
        return n;
    }
    
    public int getBandIndex() {
        return this.mBandIndex;
    }
    
    String getConfigSummary() {
        if (this.mBandIndex == -1) {
            return this.mContext.getString(2131889904);
        }
        return this.mBandSummaries[this.mBandIndex];
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_tether_network_ap_band";
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mBandIndex = this.validateSelection(Integer.parseInt((String)o));
        final StringBuilder sb = new StringBuilder();
        sb.append("Band preference changed, updating band index to ");
        sb.append(this.mBandIndex);
        Log.d("WifiTetherApBandPref", sb.toString());
        preference.setSummary(this.getConfigSummary());
        this.mListener.onTetherConfigUpdated();
        return true;
    }
    
    @Override
    public void updateDisplay() {
        final WifiConfiguration wifiApConfiguration = this.mWifiManager.getWifiApConfiguration();
        if (wifiApConfiguration == null) {
            this.mBandIndex = 0;
            Log.d("WifiTetherApBandPref", "Updating band index to 0 because no config");
        }
        else if (this.is5GhzBandSupported()) {
            this.mBandIndex = this.validateSelection(wifiApConfiguration.apBand);
            final StringBuilder sb = new StringBuilder();
            sb.append("Updating band index to ");
            sb.append(this.mBandIndex);
            Log.d("WifiTetherApBandPref", sb.toString());
        }
        else {
            wifiApConfiguration.apBand = 0;
            this.mWifiManager.setWifiApConfiguration(wifiApConfiguration);
            this.mBandIndex = wifiApConfiguration.apBand;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("5Ghz not supported, updating band index to ");
            sb2.append(this.mBandIndex);
            Log.d("WifiTetherApBandPref", sb2.toString());
        }
        final ListPreference listPreference = (ListPreference)this.mPreference;
        listPreference.setEntries(this.mBandSummaries);
        listPreference.setEntryValues(this.mBandEntries);
        if (!this.is5GhzBandSupported()) {
            listPreference.setEnabled(false);
            listPreference.setSummary(2131889901);
        }
        else {
            listPreference.setValue(Integer.toString(wifiApConfiguration.apBand));
            listPreference.setSummary(this.getConfigSummary());
        }
    }
    
    void updatePreferenceEntries() {
        final Resources resources = this.mContext.getResources();
        int n = 2130903174;
        int n2 = 2130903177;
        if (this.isDualMode) {
            n = 2130903175;
            n2 = 2130903176;
        }
        this.mBandEntries = resources.getStringArray(n);
        this.mBandSummaries = resources.getStringArray(n2);
    }
}
