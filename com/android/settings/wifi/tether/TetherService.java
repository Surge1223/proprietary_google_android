package com.android.settings.wifi.tether;

import android.app.usage.UsageStatsManager;
import android.os.SystemClock;
import android.os.Handler;
import android.content.IntentFilter;
import android.os.IBinder;
import android.app.PendingIntent;
import android.app.AlarmManager;
import android.text.TextUtils;
import android.content.pm.ResolveInfo;
import java.util.Iterator;
import android.os.Bundle;
import android.net.ConnectivityManager;
import android.bluetooth.BluetoothPan;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.os.ResultReceiver;
import java.util.List;
import android.util.ArrayMap;
import java.util.ArrayList;
import com.android.internal.annotations.VisibleForTesting;
import android.app.Service;

public class TetherService extends Service
{
    private static final boolean DEBUG;
    @VisibleForTesting
    public static final String EXTRA_RESULT = "EntitlementResult";
    private ArrayList<Integer> mCurrentTethers;
    private int mCurrentTypeIndex;
    private HotspotOffReceiver mHotspotReceiver;
    private boolean mInProvisionCheck;
    private ArrayMap<Integer, List<ResultReceiver>> mPendingCallbacks;
    private final BroadcastReceiver mReceiver;
    private UsageStatsManagerWrapper mUsageManagerWrapper;
    
    static {
        DEBUG = Log.isLoggable("TetherService", 3);
    }
    
    public TetherService() {
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (TetherService.DEBUG) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Got provision result ");
                    sb.append(intent);
                    Log.d("TetherService", sb.toString());
                }
                if (TetherService.this.getResources().getString(17039703).equals(intent.getAction())) {
                    if (!TetherService.this.mInProvisionCheck) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unexpected provision response ");
                        sb2.append(intent);
                        Log.e("TetherService", sb2.toString());
                        return;
                    }
                    final int intValue = TetherService.this.mCurrentTethers.get(TetherService.this.mCurrentTypeIndex);
                    TetherService.this.mInProvisionCheck = false;
                    final int intExtra = intent.getIntExtra("EntitlementResult", 0);
                    if (intExtra != -1) {
                        switch (intValue) {
                            case 2: {
                                TetherService.this.disableBtTethering();
                                break;
                            }
                            case 1: {
                                TetherService.this.disableUsbTethering();
                                break;
                            }
                            case 0: {
                                TetherService.this.disableWifiTethering();
                                break;
                            }
                        }
                    }
                    TetherService.this.fireCallbacksForType(intValue, intExtra);
                    if (++TetherService.this.mCurrentTypeIndex >= TetherService.this.mCurrentTethers.size()) {
                        TetherService.this.stopSelf();
                    }
                    else {
                        TetherService.this.startProvisioning(TetherService.this.mCurrentTypeIndex);
                    }
                }
            }
        };
    }
    
    public static void cancelRecheckAlarmIfNecessary(final Context context, final int n) {
        final Intent intent = new Intent(context, (Class)TetherService.class);
        intent.putExtra("extraRemTetherType", n);
        context.startService(intent);
    }
    
    private void disableBtTethering() {
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            defaultAdapter.getProfileProxy((Context)this, (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
                public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                    ((BluetoothPan)bluetoothProfile).setBluetoothTethering(false);
                    defaultAdapter.closeProfileProxy(5, bluetoothProfile);
                }
                
                public void onServiceDisconnected(final int n) {
                }
            }, 5);
        }
    }
    
    private void disableUsbTethering() {
        ((ConnectivityManager)this.getSystemService("connectivity")).setUsbTethering(false);
    }
    
    private void disableWifiTethering() {
        ((ConnectivityManager)this.getSystemService("connectivity")).stopTethering(0);
    }
    
    private void fireCallbacksForType(int n, final int n2) {
        final List list = (List)this.mPendingCallbacks.get((Object)n);
        if (list == null) {
            return;
        }
        if (n2 == -1) {
            n = 0;
        }
        else {
            n = 11;
        }
        for (final ResultReceiver resultReceiver : list) {
            if (TetherService.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Firing result: ");
                sb.append(n);
                sb.append(" to callback");
                Log.d("TetherService", sb.toString());
            }
            resultReceiver.send(n, (Bundle)null);
        }
        list.clear();
    }
    
    private Intent getProvisionBroadcastIntent(final int n) {
        final Intent intent = new Intent(this.getResources().getString(17039702));
        intent.putExtra("TETHER_TYPE", (int)this.mCurrentTethers.get(n));
        intent.setFlags(285212672);
        return intent;
    }
    
    private void removeTypeAtIndex(final int n) {
        this.mCurrentTethers.remove(n);
        if (TetherService.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("mCurrentTypeIndex: ");
            sb.append(this.mCurrentTypeIndex);
            Log.d("TetherService", sb.toString());
        }
        if (n <= this.mCurrentTypeIndex && this.mCurrentTypeIndex > 0) {
            --this.mCurrentTypeIndex;
        }
    }
    
    private void setEntitlementAppActive(final int n) {
        final List queryBroadcastReceivers = this.getPackageManager().queryBroadcastReceivers(this.getProvisionBroadcastIntent(n), 131072);
        if (queryBroadcastReceivers.isEmpty()) {
            Log.e("TetherService", "No found BroadcastReceivers for provision intent.");
            return;
        }
        for (final ResolveInfo resolveInfo : queryBroadcastReceivers) {
            if (resolveInfo.activityInfo.applicationInfo.isSystemApp()) {
                this.mUsageManagerWrapper.setAppInactive(resolveInfo.activityInfo.packageName, false);
            }
        }
    }
    
    private void startProvisioning(final int entitlementAppActive) {
        if (entitlementAppActive < this.mCurrentTethers.size()) {
            final Intent provisionBroadcastIntent = this.getProvisionBroadcastIntent(entitlementAppActive);
            this.setEntitlementAppActive(entitlementAppActive);
            if (TetherService.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Sending provisioning broadcast: ");
                sb.append(provisionBroadcastIntent.getAction());
                sb.append(" type: ");
                sb.append(this.mCurrentTethers.get(entitlementAppActive));
                Log.d("TetherService", sb.toString());
            }
            this.sendBroadcast(provisionBroadcastIntent);
            this.mInProvisionCheck = true;
        }
    }
    
    private ArrayList<Integer> stringToTethers(final String s) {
        final ArrayList<Integer> list = new ArrayList<Integer>();
        if (TextUtils.isEmpty((CharSequence)s)) {
            return list;
        }
        final String[] split = s.split(",");
        for (int i = 0; i < split.length; ++i) {
            list.add(Integer.parseInt(split[i]));
        }
        return list;
    }
    
    private String tethersToString(final ArrayList<Integer> list) {
        final StringBuffer sb = new StringBuffer();
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (i != 0) {
                sb.append(',');
            }
            sb.append(list.get(i));
        }
        return sb.toString();
    }
    
    private void unregisterReceivers() {
        this.unregisterReceiver(this.mReceiver);
        this.mHotspotReceiver.unregister();
    }
    
    @VisibleForTesting
    void cancelAlarmIfNecessary() {
        if (this.mCurrentTethers.size() != 0) {
            if (TetherService.DEBUG) {
                Log.d("TetherService", "Tethering still active, not cancelling alarm");
            }
            return;
        }
        ((AlarmManager)this.getSystemService("alarm")).cancel(PendingIntent.getService((Context)this, 0, new Intent((Context)this, (Class)TetherService.class), 0));
        if (TetherService.DEBUG) {
            Log.d("TetherService", "Tethering no longer active, canceling recheck");
        }
        this.mHotspotReceiver.unregister();
    }
    
    public IBinder onBind(final Intent intent) {
        return null;
    }
    
    public void onCreate() {
        super.onCreate();
        if (TetherService.DEBUG) {
            Log.d("TetherService", "Creating TetherService");
        }
        this.registerReceiver(this.mReceiver, new IntentFilter(this.getResources().getString(17039703)), "android.permission.CONNECTIVITY_INTERNAL", (Handler)null);
        this.mCurrentTethers = this.stringToTethers(this.getSharedPreferences("tetherPrefs", 0).getString("currentTethers", ""));
        this.mCurrentTypeIndex = 0;
        (this.mPendingCallbacks = (ArrayMap<Integer, List<ResultReceiver>>)new ArrayMap(3)).put((Object)0, (Object)new ArrayList());
        this.mPendingCallbacks.put((Object)1, (Object)new ArrayList());
        this.mPendingCallbacks.put((Object)2, (Object)new ArrayList());
        if (this.mUsageManagerWrapper == null) {
            this.mUsageManagerWrapper = new UsageStatsManagerWrapper((Context)this);
        }
        this.mHotspotReceiver = new HotspotOffReceiver((Context)this);
    }
    
    public void onDestroy() {
        if (this.mInProvisionCheck) {
            final StringBuilder sb = new StringBuilder();
            sb.append("TetherService getting destroyed while mid-provisioning");
            sb.append(this.mCurrentTethers.get(this.mCurrentTypeIndex));
            Log.e("TetherService", sb.toString());
        }
        this.getSharedPreferences("tetherPrefs", 0).edit().putString("currentTethers", this.tethersToString(this.mCurrentTethers)).commit();
        this.unregisterReceivers();
        if (TetherService.DEBUG) {
            Log.d("TetherService", "Destroying TetherService");
        }
        super.onDestroy();
    }
    
    public int onStartCommand(final Intent intent, int n, final int n2) {
        if (intent.hasExtra("extraAddTetherType")) {
            n = intent.getIntExtra("extraAddTetherType", -1);
            final ResultReceiver resultReceiver = (ResultReceiver)intent.getParcelableExtra("extraProvisionCallback");
            if (resultReceiver != null) {
                final List list = (List)this.mPendingCallbacks.get((Object)n);
                if (list == null) {
                    resultReceiver.send(1, (Bundle)null);
                    this.stopSelf();
                    return 2;
                }
                list.add(resultReceiver);
            }
            if (!this.mCurrentTethers.contains(n)) {
                if (TetherService.DEBUG) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Adding tether ");
                    sb.append(n);
                    Log.d("TetherService", sb.toString());
                }
                this.mCurrentTethers.add(n);
            }
        }
        if (intent.hasExtra("extraRemTetherType")) {
            if (!this.mInProvisionCheck) {
                n = intent.getIntExtra("extraRemTetherType", -1);
                final int index = this.mCurrentTethers.indexOf(n);
                if (TetherService.DEBUG) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Removing tether ");
                    sb2.append(n);
                    sb2.append(", index ");
                    sb2.append(index);
                    Log.d("TetherService", sb2.toString());
                }
                if (index >= 0) {
                    this.removeTypeAtIndex(index);
                }
                this.cancelAlarmIfNecessary();
            }
            else if (TetherService.DEBUG) {
                Log.d("TetherService", "Don't cancel alarm during provisioning");
            }
        }
        if (intent.getBooleanExtra("extraSetAlarm", false) && this.mCurrentTethers.size() == 1) {
            this.scheduleAlarm();
        }
        if (intent.getBooleanExtra("extraRunProvision", false)) {
            this.startProvisioning(this.mCurrentTypeIndex);
        }
        else if (!this.mInProvisionCheck) {
            if (TetherService.DEBUG) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Stopping self.  startid: ");
                sb3.append(n2);
                Log.d("TetherService", sb3.toString());
            }
            this.stopSelf();
            return 2;
        }
        return 3;
    }
    
    @VisibleForTesting
    void scheduleAlarm() {
        final Intent intent = new Intent((Context)this, (Class)TetherService.class);
        intent.putExtra("extraRunProvision", true);
        final PendingIntent service = PendingIntent.getService((Context)this, 0, intent, 0);
        final AlarmManager alarmManager = (AlarmManager)this.getSystemService("alarm");
        final long n = 3600000 * this.getResources().getInteger(17694820);
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (TetherService.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Scheduling alarm at interval ");
            sb.append(n);
            Log.d("TetherService", sb.toString());
        }
        alarmManager.setRepeating(3, elapsedRealtime + n, n, service);
        this.mHotspotReceiver.register();
    }
    
    @VisibleForTesting
    void setHotspotOffReceiver(final HotspotOffReceiver mHotspotReceiver) {
        this.mHotspotReceiver = mHotspotReceiver;
    }
    
    @VisibleForTesting
    void setUsageStatsManagerWrapper(final UsageStatsManagerWrapper mUsageManagerWrapper) {
        this.mUsageManagerWrapper = mUsageManagerWrapper;
    }
    
    @VisibleForTesting
    public static class UsageStatsManagerWrapper
    {
        private final UsageStatsManager mUsageStatsManager;
        
        UsageStatsManagerWrapper(final Context context) {
            this.mUsageStatsManager = (UsageStatsManager)context.getSystemService("usagestats");
        }
        
        void setAppInactive(final String s, final boolean b) {
            this.mUsageStatsManager.setAppInactive(s, b);
        }
    }
}
