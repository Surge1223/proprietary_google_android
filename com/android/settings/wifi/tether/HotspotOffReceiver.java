package com.android.settings.wifi.tether;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.content.Intent;
import android.util.Log;
import android.content.Context;
import android.content.BroadcastReceiver;

public class HotspotOffReceiver extends BroadcastReceiver
{
    private static final boolean DEBUG;
    private Context mContext;
    private boolean mRegistered;
    
    static {
        DEBUG = Log.isLoggable("HotspotOffReceiver", 3);
    }
    
    public HotspotOffReceiver(final Context mContext) {
        this.mContext = mContext;
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if ("android.net.wifi.WIFI_AP_STATE_CHANGED".equals(intent.getAction()) && ((WifiManager)context.getSystemService("wifi")).getWifiApState() == 11) {
            if (HotspotOffReceiver.DEBUG) {
                Log.d("HotspotOffReceiver", "TetherService.cancelRecheckAlarmIfNecessary called");
            }
            TetherService.cancelRecheckAlarmIfNecessary(context, 0);
        }
    }
    
    public void register() {
        if (!this.mRegistered) {
            this.mContext.registerReceiver((BroadcastReceiver)this, new IntentFilter("android.net.wifi.WIFI_AP_STATE_CHANGED"));
            this.mRegistered = true;
        }
    }
    
    public void unregister() {
        if (this.mRegistered) {
            this.mContext.unregisterReceiver((BroadcastReceiver)this);
            this.mRegistered = false;
        }
    }
}
