package com.android.settings.wifi.tether;

import android.net.wifi.WifiConfiguration;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.content.Context;

public class WifiTetherSecurityPreferenceController extends WifiTetherBasePreferenceController
{
    private final String[] mSecurityEntries;
    private int mSecurityValue;
    
    public WifiTetherSecurityPreferenceController(final Context context, final OnTetherConfigUpdateListener onTetherConfigUpdateListener) {
        super(context, onTetherConfigUpdateListener);
        this.mSecurityEntries = this.mContext.getResources().getStringArray(2130903201);
    }
    
    private String getSummaryForSecurityType(final int n) {
        if (n == 0) {
            return this.mSecurityEntries[1];
        }
        return this.mSecurityEntries[0];
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_tether_security";
    }
    
    public int getSecurityType() {
        return this.mSecurityValue;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mSecurityValue = Integer.parseInt((String)o);
        preference.setSummary(this.getSummaryForSecurityType(this.mSecurityValue));
        this.mListener.onTetherConfigUpdated();
        return true;
    }
    
    @Override
    public void updateDisplay() {
        final WifiConfiguration wifiApConfiguration = this.mWifiManager.getWifiApConfiguration();
        if (wifiApConfiguration != null && wifiApConfiguration.getAuthType() == 0) {
            this.mSecurityValue = 0;
        }
        else {
            this.mSecurityValue = 4;
        }
        final ListPreference listPreference = (ListPreference)this.mPreference;
        listPreference.setSummary(this.getSummaryForSecurityType(this.mSecurityValue));
        listPreference.setValue(String.valueOf(this.mSecurityValue));
    }
}
