package com.android.settings.wifi.tether;

import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.content.BroadcastReceiver;
import android.net.ConnectivityManager$OnStartTetheringCallback;
import android.content.Context;
import android.net.ConnectivityManager;
import android.content.IntentFilter;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.datausage.DataSaverBackend;

public class WifiTetherSwitchBarController implements Listener, OnSwitchChangeListener, LifecycleObserver, OnStart, OnStop
{
    private static final IntentFilter WIFI_INTENT_FILTER;
    private final ConnectivityManager mConnectivityManager;
    private final Context mContext;
    final DataSaverBackend mDataSaverBackend;
    final ConnectivityManager$OnStartTetheringCallback mOnStartTetheringCallback;
    private final BroadcastReceiver mReceiver;
    private final SwitchWidgetController mSwitchBar;
    private final WifiManager mWifiManager;
    
    static {
        (WIFI_INTENT_FILTER = new IntentFilter("android.net.wifi.WIFI_AP_STATE_CHANGED")).addAction("android.intent.action.AIRPLANE_MODE");
    }
    
    WifiTetherSwitchBarController(final Context mContext, final SwitchWidgetController mSwitchBar) {
        this.mOnStartTetheringCallback = new ConnectivityManager$OnStartTetheringCallback() {
            public void onTetheringFailed() {
                super.onTetheringFailed();
                WifiTetherSwitchBarController.this.mSwitchBar.setChecked(false);
                WifiTetherSwitchBarController.this.updateWifiSwitch();
            }
        };
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.net.wifi.WIFI_AP_STATE_CHANGED".equals(action)) {
                    WifiTetherSwitchBarController.this.handleWifiApStateChanged(intent.getIntExtra("wifi_state", 14));
                }
                else if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    WifiTetherSwitchBarController.this.updateWifiSwitch();
                }
            }
        };
        this.mContext = mContext;
        this.mSwitchBar = mSwitchBar;
        this.mDataSaverBackend = new DataSaverBackend(mContext);
        this.mConnectivityManager = (ConnectivityManager)mContext.getSystemService("connectivity");
        this.mWifiManager = (WifiManager)mContext.getSystemService("wifi");
        this.mSwitchBar.setChecked(this.mWifiManager.getWifiApState() == 13);
        this.mSwitchBar.setListener((SwitchWidgetController.OnSwitchChangeListener)this);
        this.updateWifiSwitch();
    }
    
    private void handleWifiApStateChanged(final int n) {
        switch (n) {
            default: {
                this.mSwitchBar.setChecked(false);
                this.updateWifiSwitch();
                break;
            }
            case 13: {
                if (!this.mSwitchBar.isChecked()) {
                    this.mSwitchBar.setChecked(true);
                }
                this.updateWifiSwitch();
                break;
            }
            case 12: {
                this.mSwitchBar.setEnabled(false);
                break;
            }
            case 11: {
                this.mSwitchBar.setChecked(false);
                this.updateWifiSwitch();
                break;
            }
            case 10: {
                if (this.mSwitchBar.isChecked()) {
                    this.mSwitchBar.setChecked(false);
                }
                this.mSwitchBar.setEnabled(false);
                break;
            }
        }
    }
    
    private void updateWifiSwitch() {
        if (Settings.Global.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0) == 0) {
            this.mSwitchBar.setEnabled(true ^ this.mDataSaverBackend.isDataSaverEnabled());
        }
        else {
            this.mSwitchBar.setEnabled(false);
        }
    }
    
    @Override
    public void onBlacklistStatusChanged(final int n, final boolean b) {
    }
    
    @Override
    public void onDataSaverChanged(final boolean b) {
        this.updateWifiSwitch();
    }
    
    @Override
    public void onStart() {
        this.mDataSaverBackend.addListener((DataSaverBackend.Listener)this);
        this.mSwitchBar.startListening();
        this.mContext.registerReceiver(this.mReceiver, WifiTetherSwitchBarController.WIFI_INTENT_FILTER);
    }
    
    @Override
    public void onStop() {
        this.mDataSaverBackend.remListener((DataSaverBackend.Listener)this);
        this.mSwitchBar.stopListening();
        this.mContext.unregisterReceiver(this.mReceiver);
    }
    
    @Override
    public boolean onSwitchToggled(final boolean b) {
        if (!b) {
            this.stopTether();
        }
        else if (!this.mWifiManager.isWifiApEnabled()) {
            this.startTether();
        }
        return true;
    }
    
    @Override
    public void onWhitelistStatusChanged(final int n, final boolean b) {
    }
    
    void startTether() {
        this.mSwitchBar.setEnabled(false);
        this.mConnectivityManager.startTethering(0, true, this.mOnStartTetheringCallback, new Handler(Looper.getMainLooper()));
    }
    
    void stopTether() {
        this.mSwitchBar.setEnabled(false);
        this.mConnectivityManager.stopTethering(0);
    }
}
