package com.android.settings.wifi;

import com.android.settings.widget.SummaryUpdater;
import android.app.Dialog;
import android.nfc.NfcAdapter;
import android.view.ContextMenu$ContextMenuInfo;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.Toast;
import com.android.settingslib.wifi.WifiTrackerFactory;
import android.net.NetworkInfo.State;
import android.util.Pair;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.LinkifyUtils;
import android.net.ConnectivityManager$NetworkCallback;
import android.os.Handler;
import android.os.Looper;
import android.net.NetworkRequest$Builder;
import com.android.settings.wifi.details.WifiNetworkDetailsFragment;
import com.android.settings.location.ScanningSettings;
import com.android.settings.core.SubSettingLauncher;
import android.content.ContentResolver;
import android.provider.Settings;
import android.os.PowerManager;
import android.util.Log;
import android.net.wifi.WifiConfiguration$NetworkSelectionStatus;
import android.net.wifi.WifiConfiguration;
import android.net.Network;
import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.widget.SwitchBarController;
import com.android.settings.SettingsActivity;
import android.app.Fragment;
import android.app.Activity;
import android.content.res.Resources;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.net.wifi.WifiManager;
import com.android.settingslib.wifi.AccessPointPreference;
import android.view.View;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager$ActionListener;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.os.Bundle;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settingslib.wifi.WifiTracker;
import com.android.settingslib.wifi.AccessPoint;
import com.android.settings.search.Indexable;
import com.android.settings.RestrictedSettingsFragment;

public final class _$$Lambda$WifiSettings$Dc8tARLt9797q5fiCWMG56ysJZ4 implements Runnable
{
    @Override
    public final void run() {
        this.f$0.updateAccessPointPreferences();
    }
}
