package com.android.settings.wifi;

import android.text.method.MovementMethod;
import android.text.method.LinkMovementMethod;
import android.text.style.TextAppearanceSpan;
import android.text.Spannable;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.LinkifyUtils;
import android.support.v7.preference.Preference;

public class LinkablePreference extends Preference
{
    private LinkifyUtils.OnClickListener mClickListener;
    private CharSequence mContentDescription;
    private CharSequence mContentTitle;
    
    public LinkablePreference(final Context context) {
        super(context);
        this.setSelectable(false);
    }
    
    public LinkablePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setSelectable(false);
    }
    
    public LinkablePreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.setSelectable(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(16908310);
        if (textView == null) {
            return;
        }
        textView.setSingleLine(false);
        if (this.mContentTitle != null && this.mClickListener != null) {
            final StringBuilder append = new StringBuilder().append(this.mContentTitle);
            if (this.mContentDescription != null) {
                append.append("\n\n");
                append.append(this.mContentDescription);
            }
            if (LinkifyUtils.linkify(textView, append, this.mClickListener) && this.mContentTitle != null) {
                final Spannable text = (Spannable)textView.getText();
                text.setSpan((Object)new TextAppearanceSpan(this.getContext(), 16973892), 0, this.mContentTitle.length(), 17);
                textView.setText((CharSequence)text);
                textView.setMovementMethod((MovementMethod)new LinkMovementMethod());
            }
        }
    }
    
    public void setText(final CharSequence charSequence, final CharSequence mContentDescription, final LinkifyUtils.OnClickListener mClickListener) {
        this.mContentTitle = charSequence;
        this.mContentDescription = mContentDescription;
        this.mClickListener = mClickListener;
        super.setTitle(charSequence);
    }
    
    @Override
    public void setTitle(final int title) {
        this.mContentTitle = null;
        this.mContentDescription = null;
        super.setTitle(title);
    }
    
    @Override
    public void setTitle(final CharSequence title) {
        this.mContentTitle = null;
        this.mContentDescription = null;
        super.setTitle(title);
    }
}
