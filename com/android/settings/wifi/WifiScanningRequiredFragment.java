package com.android.settings.wifi;

import android.app.Dialog;
import android.os.Bundle;
import android.content.ContentResolver;
import android.widget.Toast;
import android.provider.Settings;
import android.content.DialogInterface;
import com.android.settingslib.HelpUtils;
import android.content.Context;
import android.text.TextUtils;
import android.app.AlertDialog$Builder;
import android.content.Intent;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class WifiScanningRequiredFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    public static WifiScanningRequiredFragment newInstance() {
        return new WifiScanningRequiredFragment();
    }
    
    private void openHelpPage() {
        final Intent helpIntent = this.getHelpIntent(this.getContext());
        if (helpIntent != null) {
            try {
                this.startActivity(helpIntent);
            }
            catch (ActivityNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Activity was not found for intent, ");
                sb.append(helpIntent.toString());
                Log.e("WifiScanReqFrag", sb.toString());
            }
        }
    }
    
    void addButtonIfNeeded(final AlertDialog$Builder alertDialog$Builder) {
        if (!TextUtils.isEmpty((CharSequence)this.getContext().getString(2131887790))) {
            alertDialog$Builder.setNeutralButton(2131888022, (DialogInterface$OnClickListener)this);
        }
    }
    
    Intent getHelpIntent(final Context context) {
        return HelpUtils.getHelpIntent(context, context.getString(2131887790), context.getClass().getName());
    }
    
    public int getMetricsCategory() {
        return 1373;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final Context context = this.getContext();
        final ContentResolver contentResolver = context.getContentResolver();
        if (n != -3) {
            if (n == -1) {
                Settings.Global.putInt(contentResolver, "wifi_scan_always_enabled", 1);
                Toast.makeText(context, (CharSequence)context.getString(2131890115), 0).show();
                this.getTargetFragment().onActivityResult(this.getTargetRequestCode(), -1, (Intent)null);
            }
        }
        else {
            this.openHelpPage();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final AlertDialog$Builder setNegativeButton = new AlertDialog$Builder(this.getContext()).setTitle(2131890118).setView(2131558894).setPositiveButton(2131890119, (DialogInterface$OnClickListener)this).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
        this.addButtonIfNeeded(setNegativeButton);
        return (Dialog)setNegativeButton.create();
    }
}
