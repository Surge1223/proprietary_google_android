package com.android.settings.wifi;

import android.content.Intent;
import com.android.settings.wifi.p2p.WifiP2pPreferenceController;
import java.util.ArrayList;
import android.net.wifi.WifiManager;
import android.app.Fragment;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class ConfigureWifiSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private UseOpenWifiPreferenceController mUseOpenWifiPreferenceController;
    private WifiWakeupPreferenceController mWifiWakeupPreferenceController;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                final NetworkInfo activeNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo == null || activeNetworkInfo.getType() == 1) {
                    nonIndexableKeys.add("current_ip_address");
                }
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082868;
                return Arrays.asList(searchIndexableResource);
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return context.getResources().getBoolean(2131034176);
            }
        };
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        this.mWifiWakeupPreferenceController = new WifiWakeupPreferenceController(context, this);
        this.mUseOpenWifiPreferenceController = new UseOpenWifiPreferenceController(context, this, this.getLifecycle());
        final WifiManager wifiManager = (WifiManager)this.getSystemService("wifi");
        final ArrayList<NotifyOpenNetworksPreferenceController> list = (ArrayList<NotifyOpenNetworksPreferenceController>)new ArrayList<WifiP2pPreferenceController>();
        list.add((WifiP2pPreferenceController)this.mWifiWakeupPreferenceController);
        list.add((WifiP2pPreferenceController)new NotifyOpenNetworksPreferenceController(context, this.getLifecycle()));
        list.add((WifiP2pPreferenceController)this.mUseOpenWifiPreferenceController);
        list.add((WifiP2pPreferenceController)new WifiInfoPreferenceController(context, this.getLifecycle(), wifiManager));
        list.add((WifiP2pPreferenceController)new CellularFallbackPreferenceController(context));
        list.add(new WifiP2pPreferenceController(context, this.getLifecycle(), wifiManager));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getInitialExpandedChildCount() {
        int n = 2;
        if (this.mUseOpenWifiPreferenceController.isAvailable()) {
            n = 2 + 1;
        }
        return n;
    }
    
    @Override
    protected String getLogTag() {
        return "ConfigureWifiSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 338;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082868;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 600 && this.mWifiWakeupPreferenceController != null) {
            this.mWifiWakeupPreferenceController.onActivityResult(n, n2);
            return;
        }
        if (n == 400 && this.mUseOpenWifiPreferenceController != null) {
            this.mUseOpenWifiPreferenceController.onActivityResult(n, n2);
            return;
        }
        super.onActivityResult(n, n2, intent);
    }
}
