package com.android.settings.wifi;

import android.view.ViewGroup;
import android.os.Bundle;
import com.android.settingslib.RestrictedLockUtils;
import android.content.DialogInterface;
import android.widget.Button;
import android.content.Context;
import android.view.View;
import com.android.settingslib.wifi.AccessPoint;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog;

public class WifiDialog extends AlertDialog implements DialogInterface$OnClickListener, WifiConfigUiBase
{
    private final AccessPoint mAccessPoint;
    private WifiConfigController mController;
    private boolean mHideSubmitButton;
    private final WifiDialogListener mListener;
    private final int mMode;
    private View mView;
    
    WifiDialog(final Context context, final WifiDialogListener mListener, final AccessPoint mAccessPoint, final int mMode, final int n, final boolean mHideSubmitButton) {
        super(context, n);
        this.mMode = mMode;
        this.mListener = mListener;
        this.mAccessPoint = mAccessPoint;
        this.mHideSubmitButton = mHideSubmitButton;
    }
    
    public static WifiDialog createFullscreen(final Context context, final WifiDialogListener wifiDialogListener, final AccessPoint accessPoint, final int n) {
        return new WifiDialog(context, wifiDialogListener, accessPoint, n, 2131952103, false);
    }
    
    public static WifiDialog createModal(final Context context, final WifiDialogListener wifiDialogListener, final AccessPoint accessPoint, final int n) {
        return new WifiDialog(context, wifiDialogListener, accessPoint, n, 0, n == 0);
    }
    
    public void dispatchSubmit() {
        if (this.mListener != null) {
            this.mListener.onSubmit(this);
        }
        this.dismiss();
    }
    
    public WifiConfigController getController() {
        return this.mController;
    }
    
    public Button getForgetButton() {
        return this.getButton(-3);
    }
    
    public Button getSubmitButton() {
        return this.getButton(-1);
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (this.mListener != null) {
            if (n != -3) {
                if (n == -1) {
                    this.mListener.onSubmit(this);
                }
            }
            else {
                if (WifiUtils.isNetworkLockedDown(this.getContext(), this.mAccessPoint.getConfig())) {
                    RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.getContext(), RestrictedLockUtils.getDeviceOwner(this.getContext()));
                    return;
                }
                this.mListener.onForget(this);
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setView(this.mView = this.getLayoutInflater().inflate(2131558886, (ViewGroup)null));
        this.setInverseBackgroundForced(true);
        this.mController = new WifiConfigController(this, this.mView, this.mAccessPoint, this.mMode);
        super.onCreate(bundle);
        if (this.mHideSubmitButton) {
            this.mController.hideSubmitButton();
        }
        else {
            this.mController.enableSubmitIfAppropriate();
        }
        if (this.mAccessPoint == null) {
            this.mController.hideForgetButton();
        }
    }
    
    public void onRestoreInstanceState(final Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.mController.updatePassword();
    }
    
    public void setCancelButton(final CharSequence charSequence) {
        this.setButton(-2, charSequence, (DialogInterface$OnClickListener)this);
    }
    
    public void setForgetButton(final CharSequence charSequence) {
        this.setButton(-3, charSequence, (DialogInterface$OnClickListener)this);
    }
    
    public void setSubmitButton(final CharSequence charSequence) {
        this.setButton(-1, charSequence, (DialogInterface$OnClickListener)this);
    }
    
    public interface WifiDialogListener
    {
        void onForget(final WifiDialog p0);
        
        void onSubmit(final WifiDialog p0);
    }
}
