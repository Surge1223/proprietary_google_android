package com.android.settings.wifi;

import android.text.TextUtils;
import android.net.wifi.WifiInfo;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkScoreManager;
import android.net.wifi.WifiManager;
import android.content.Context;
import com.android.settingslib.wifi.WifiStatusTracker;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settings.widget.SummaryUpdater;

public final class _$$Lambda$WifiSummaryUpdater$5w1MXX8MJfsbMZcSIHVb0vJmaww implements Runnable
{
    @Override
    public final void run() {
        this.f$0.notifyChangeIfNeeded();
    }
}
