package com.android.settings.wifi;

import android.widget.AdapterView;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.CompoundButton;
import android.os.UserManager;
import android.security.KeyStore;
import android.net.IpConfiguration;
import android.widget.Button;
import com.android.settingslib.utils.ThreadUtils;
import android.text.Editable;
import java.net.UnknownHostException;
import android.net.LinkAddress;
import android.net.wifi.WifiEnterpriseConfig;
import java.util.Iterator;
import java.net.InetAddress;
import android.widget.SpinnerAdapter;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import android.net.Uri;
import android.text.TextUtils;
import com.android.settings.ProxySelector;
import android.net.NetworkUtils;
import java.net.Inet4Address;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiConfiguration;
import android.content.res.Resources;
import android.util.Log;
import android.net.NetworkInfo$DetailedState;
import android.view.ViewGroup;
import com.android.settingslib.Utils;
import android.view.View;
import android.net.StaticIpConfiguration;
import android.widget.CheckBox;
import android.net.IpConfiguration$ProxySettings;
import android.widget.ArrayAdapter;
import android.net.IpConfiguration$IpAssignment;
import android.net.ProxyInfo;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ScrollView;
import android.content.Context;
import com.android.settingslib.wifi.AccessPoint;
import android.widget.TextView$OnEditorActionListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.AdapterView$OnItemSelectedListener;
import android.view.View$OnKeyListener;
import android.text.TextWatcher;

public class WifiConfigController implements TextWatcher, View$OnKeyListener, AdapterView$OnItemSelectedListener, CompoundButton$OnCheckedChangeListener, TextView$OnEditorActionListener
{
    private final AccessPoint mAccessPoint;
    private int mAccessPointSecurity;
    private final WifiConfigUiBase mConfigUi;
    private Context mContext;
    private ScrollView mDialogContainer;
    private TextView mDns1View;
    private TextView mDns2View;
    private String mDoNotProvideEapUserCertString;
    private String mDoNotValidateEapServerString;
    private TextView mEapAnonymousView;
    private Spinner mEapCaCertSpinner;
    private TextView mEapDomainView;
    private TextView mEapIdentityView;
    private Spinner mEapMethodSpinner;
    private Spinner mEapUserCertSpinner;
    private TextView mGatewayView;
    private Spinner mHiddenSettingsSpinner;
    private TextView mHiddenWarningView;
    private ProxyInfo mHttpProxy;
    private TextView mIpAddressView;
    private IpConfiguration$IpAssignment mIpAssignment;
    private Spinner mIpSettingsSpinner;
    private String[] mLevels;
    private Spinner mMeteredSettingsSpinner;
    private int mMode;
    private String mMultipleCertSetString;
    private TextView mNetworkPrefixLengthView;
    private TextView mPasswordView;
    private ArrayAdapter<String> mPhase2Adapter;
    private final ArrayAdapter<String> mPhase2FullAdapter;
    private final ArrayAdapter<String> mPhase2PeapAdapter;
    private Spinner mPhase2Spinner;
    private TextView mProxyExclusionListView;
    private TextView mProxyHostView;
    private TextView mProxyPacView;
    private TextView mProxyPortView;
    private IpConfiguration$ProxySettings mProxySettings;
    private Spinner mProxySettingsSpinner;
    private Spinner mSecuritySpinner;
    private CheckBox mSharedCheckBox;
    private TextView mSsidView;
    private StaticIpConfiguration mStaticIpConfiguration;
    private String mUnspecifiedCertString;
    private String mUseSystemCertsString;
    private final View mView;
    
    public WifiConfigController(final WifiConfigUiBase mConfigUi, final View mView, final AccessPoint mAccessPoint, int frequency) {
        this.mIpAssignment = IpConfiguration$IpAssignment.UNASSIGNED;
        this.mProxySettings = IpConfiguration$ProxySettings.UNASSIGNED;
        this.mHttpProxy = null;
        this.mStaticIpConfiguration = null;
        this.mConfigUi = mConfigUi;
        this.mView = mView;
        this.mAccessPoint = mAccessPoint;
        int security;
        if (mAccessPoint == null) {
            security = 0;
        }
        else {
            security = mAccessPoint.getSecurity();
        }
        this.mAccessPointSecurity = security;
        this.mMode = frequency;
        this.mContext = this.mConfigUi.getContext();
        final Resources resources = this.mContext.getResources();
        this.mLevels = resources.getStringArray(2130903198);
        if (!Utils.isWifiOnly(this.mContext) && this.mContext.getResources().getBoolean(17956948)) {
            this.mPhase2PeapAdapter = (ArrayAdapter<String>)new ArrayAdapter(this.mContext, 17367048, (Object[])resources.getStringArray(2130903193));
        }
        else {
            this.mPhase2PeapAdapter = (ArrayAdapter<String>)new ArrayAdapter(this.mContext, 17367048, (Object[])resources.getStringArray(2130903192));
        }
        this.mPhase2PeapAdapter.setDropDownViewResource(17367049);
        (this.mPhase2FullAdapter = (ArrayAdapter<String>)new ArrayAdapter(this.mContext, 17367048, (Object[])resources.getStringArray(2130903194))).setDropDownViewResource(17367049);
        this.mUnspecifiedCertString = this.mContext.getString(2131890187);
        this.mMultipleCertSetString = this.mContext.getString(2131890050);
        this.mUseSystemCertsString = this.mContext.getString(2131890189);
        this.mDoNotProvideEapUserCertString = this.mContext.getString(2131889981);
        this.mDoNotValidateEapServerString = this.mContext.getString(2131889982);
        this.mDialogContainer = (ScrollView)this.mView.findViewById(2131362074);
        (this.mIpSettingsSpinner = (Spinner)this.mView.findViewById(2131362285)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        (this.mProxySettingsSpinner = (Spinner)this.mView.findViewById(2131362501)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        this.mSharedCheckBox = (CheckBox)this.mView.findViewById(2131362599);
        this.mMeteredSettingsSpinner = (Spinner)this.mView.findViewById(2131362368);
        (this.mHiddenSettingsSpinner = (Spinner)this.mView.findViewById(2131362220)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        this.mHiddenWarningView = (TextView)this.mView.findViewById(2131362223);
        final TextView mHiddenWarningView = this.mHiddenWarningView;
        if (this.mHiddenSettingsSpinner.getSelectedItemPosition() == 0) {
            frequency = 8;
        }
        else {
            frequency = 0;
        }
        mHiddenWarningView.setVisibility(frequency);
        if (this.mAccessPoint == null) {
            this.mConfigUi.setTitle(2131889889);
            (this.mSsidView = (TextView)this.mView.findViewById(2131362636)).addTextChangedListener((TextWatcher)this);
            (this.mSecuritySpinner = (Spinner)this.mView.findViewById(2131362570)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
            this.mView.findViewById(2131362759).setVisibility(0);
            this.showIpConfigFields();
            this.showProxyFields();
            this.mView.findViewById(2131362824).setVisibility(0);
            this.mView.findViewById(2131362221).setVisibility(0);
            ((CheckBox)this.mView.findViewById(2131362825)).setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
            this.mConfigUi.setSubmitButton(resources.getString(2131890083));
        }
        else {
            if (!this.mAccessPoint.isPasspointConfig()) {
                this.mConfigUi.setTitle(this.mAccessPoint.getSsid());
            }
            else {
                this.mConfigUi.setTitle(this.mAccessPoint.getConfigName());
            }
            final ViewGroup viewGroup = (ViewGroup)this.mView.findViewById(2131362269);
            boolean checked = false;
            boolean b = false;
            if (this.mAccessPoint.isSaved()) {
                final WifiConfiguration config = this.mAccessPoint.getConfig();
                this.mMeteredSettingsSpinner.setSelection(config.meteredOverride);
                final Spinner mHiddenSettingsSpinner = this.mHiddenSettingsSpinner;
                if (config.hiddenSSID) {
                    frequency = 1;
                }
                else {
                    frequency = 0;
                }
                mHiddenSettingsSpinner.setSelection(frequency);
                if (config.getIpAssignment() == IpConfiguration$IpAssignment.STATIC) {
                    this.mIpSettingsSpinner.setSelection(1);
                    b = true;
                    final StaticIpConfiguration staticIpConfiguration = config.getStaticIpConfiguration();
                    if (staticIpConfiguration != null && staticIpConfiguration.ipAddress != null) {
                        this.addRow(viewGroup, 2131890027, staticIpConfiguration.ipAddress.getAddress().getHostAddress());
                    }
                }
                else {
                    this.mIpSettingsSpinner.setSelection(0);
                }
                this.mSharedCheckBox.setEnabled(config.shared);
                if (!config.shared) {
                    b = true;
                }
                Label_0863: {
                    if (config.getProxySettings() == IpConfiguration$ProxySettings.STATIC) {
                        this.mProxySettingsSpinner.setSelection(1);
                    }
                    else {
                        if (config.getProxySettings() != IpConfiguration$ProxySettings.PAC) {
                            this.mProxySettingsSpinner.setSelection(0);
                            break Label_0863;
                        }
                        this.mProxySettingsSpinner.setSelection(2);
                    }
                    b = true;
                }
                checked = b;
                if (config != null) {
                    checked = b;
                    if (config.isPasspoint()) {
                        this.addRow(viewGroup, 2131888554, String.format(this.mContext.getString(2131888553), config.providerFriendlyName));
                        checked = b;
                    }
                }
            }
            if ((!this.mAccessPoint.isSaved() && !this.mAccessPoint.isActive() && !this.mAccessPoint.isPasspointConfig()) || this.mMode != 0) {
                this.showSecurityFields();
                this.showIpConfigFields();
                this.showProxyFields();
                final CheckBox checkBox = (CheckBox)this.mView.findViewById(2131362825);
                final View viewById = this.mView.findViewById(2131362824);
                if (this.mAccessPoint.isCarrierAp()) {
                    frequency = 8;
                }
                else {
                    frequency = 0;
                }
                viewById.setVisibility(frequency);
                checkBox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
                checkBox.setChecked(checked);
                final View viewById2 = this.mView.findViewById(2131362823);
                if (checked) {
                    frequency = 0;
                }
                else {
                    frequency = 8;
                }
                viewById2.setVisibility(frequency);
                if (this.mAccessPoint.isCarrierAp()) {
                    this.addRow(viewGroup, 2131889930, String.format(this.mContext.getString(2131889931), this.mAccessPoint.getCarrierName()));
                }
            }
            if (this.mMode == 2) {
                this.mConfigUi.setSubmitButton(resources.getString(2131890083));
            }
            else if (this.mMode == 1) {
                this.mConfigUi.setSubmitButton(resources.getString(2131889939));
            }
            else {
                final NetworkInfo$DetailedState detailedState = this.mAccessPoint.getDetailedState();
                final String signalString = this.getSignalString();
                if ((detailedState == null || detailedState == NetworkInfo$DetailedState.DISCONNECTED) && signalString != null) {
                    this.mConfigUi.setSubmitButton(resources.getString(2131889939));
                }
                else {
                    if (detailedState != null) {
                        final boolean ephemeral = this.mAccessPoint.isEphemeral();
                        final WifiConfiguration config2 = this.mAccessPoint.getConfig();
                        String providerFriendlyName = null;
                        if (config2 != null) {
                            providerFriendlyName = providerFriendlyName;
                            if (config2.isPasspoint()) {
                                providerFriendlyName = config2.providerFriendlyName;
                            }
                        }
                        this.addRow(viewGroup, 2131890167, AccessPoint.getSummary(this.mConfigUi.getContext(), detailedState, ephemeral, providerFriendlyName));
                    }
                    if (signalString != null) {
                        this.addRow(viewGroup, 2131890155, signalString);
                    }
                    final WifiInfo info = this.mAccessPoint.getInfo();
                    if (info != null && info.getLinkSpeed() != -1) {
                        this.addRow(viewGroup, 2131890156, String.format(resources.getString(2131888026), info.getLinkSpeed()));
                    }
                    if (info != null && info.getFrequency() != -1) {
                        frequency = info.getFrequency();
                        String s = null;
                        if (frequency >= 2400 && frequency < 2500) {
                            s = resources.getString(2131889911);
                        }
                        else if (frequency >= 4900 && frequency < 5900) {
                            s = resources.getString(2131889912);
                        }
                        else {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unexpected frequency ");
                            sb.append(frequency);
                            Log.e("WifiConfigController", sb.toString());
                        }
                        if (s != null) {
                            this.addRow(viewGroup, 2131890001, s);
                        }
                    }
                    this.addRow(viewGroup, 2131890093, this.mAccessPoint.getSecurityString(false));
                    this.mView.findViewById(2131362284).setVisibility(8);
                }
                if (this.mAccessPoint.isSaved() || this.mAccessPoint.isActive() || this.mAccessPoint.isPasspointConfig()) {
                    this.mConfigUi.setForgetButton(resources.getString(2131889998));
                }
            }
        }
        if (!this.isSplitSystemUser()) {
            this.mSharedCheckBox.setVisibility(8);
        }
        this.mConfigUi.setCancelButton(resources.getString(2131889927));
        if (this.mConfigUi.getSubmitButton() != null) {
            this.enableSubmitIfAppropriate();
        }
        this.mView.findViewById(2131362310).requestFocus();
    }
    
    private void addRow(final ViewGroup viewGroup, final int text, final String text2) {
        final View inflate = this.mConfigUi.getLayoutInflater().inflate(2131558887, viewGroup, false);
        ((TextView)inflate.findViewById(2131362382)).setText(text);
        ((TextView)inflate.findViewById(2131362800)).setText((CharSequence)text2);
        viewGroup.addView(inflate);
    }
    
    private Inet4Address getIPv4Address(final String s) {
        try {
            return (Inet4Address)NetworkUtils.numericToInetAddress(s);
        }
        catch (IllegalArgumentException | ClassCastException ex) {
            return null;
        }
    }
    
    private boolean ipAndProxyFieldsAreValid() {
        IpConfiguration$IpAssignment mIpAssignment;
        if (this.mIpSettingsSpinner != null && this.mIpSettingsSpinner.getSelectedItemPosition() == 1) {
            mIpAssignment = IpConfiguration$IpAssignment.STATIC;
        }
        else {
            mIpAssignment = IpConfiguration$IpAssignment.DHCP;
        }
        this.mIpAssignment = mIpAssignment;
        if (this.mIpAssignment == IpConfiguration$IpAssignment.STATIC) {
            this.mStaticIpConfiguration = new StaticIpConfiguration();
            if (this.validateIpConfigFields(this.mStaticIpConfiguration) != 0) {
                return false;
            }
        }
        final int selectedItemPosition = this.mProxySettingsSpinner.getSelectedItemPosition();
        this.mProxySettings = IpConfiguration$ProxySettings.NONE;
        this.mHttpProxy = null;
        if (selectedItemPosition == 1 && this.mProxyHostView != null) {
            this.mProxySettings = IpConfiguration$ProxySettings.STATIC;
            final String string = this.mProxyHostView.getText().toString();
            final String string2 = this.mProxyPortView.getText().toString();
            final String string3 = this.mProxyExclusionListView.getText().toString();
            int n = 0;
            int n3;
            int n4;
            try {
                final int n2 = n = Integer.parseInt(string2);
                n = ProxySelector.validate(string, string2, string3);
                n3 = n2;
                n4 = n;
            }
            catch (NumberFormatException ex) {
                n4 = 2131888710;
                n3 = n;
            }
            if (n4 != 0) {
                return false;
            }
            this.mHttpProxy = new ProxyInfo(string, n3, string3);
        }
        else if (selectedItemPosition == 2 && this.mProxyPacView != null) {
            this.mProxySettings = IpConfiguration$ProxySettings.PAC;
            final CharSequence text = this.mProxyPacView.getText();
            if (TextUtils.isEmpty(text)) {
                return false;
            }
            final Uri parse = Uri.parse(text.toString());
            if (parse == null) {
                return false;
            }
            this.mHttpProxy = new ProxyInfo(parse);
        }
        return true;
    }
    
    private void loadCertificates(final Spinner spinner, final String s, final String s2, final boolean b, final boolean b2) {
        final Context context = this.mConfigUi.getContext();
        final ArrayList<String> list = new ArrayList<String>();
        list.add(this.mUnspecifiedCertString);
        if (b) {
            list.add(this.mMultipleCertSetString);
        }
        if (b2) {
            list.add(this.mUseSystemCertsString);
        }
        try {
            list.addAll(Arrays.asList(this.getKeyStore().list(s, 1010)));
        }
        catch (Exception ex) {
            Log.e("WifiConfigController", "can't get the certificate list from KeyStore");
        }
        list.add(s2);
        final ArrayAdapter adapter = new ArrayAdapter(context, 17367048, (Object[])list.toArray(new String[list.size()]));
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter)adapter);
    }
    
    private void setAnonymousIdentInvisible() {
        this.mView.findViewById(2131362303).setVisibility(8);
        this.mEapAnonymousView.setText((CharSequence)"");
    }
    
    private void setCaCertInvisible() {
        this.mView.findViewById(2131362304).setVisibility(8);
        this.setSelection(this.mEapCaCertSpinner, this.mUnspecifiedCertString);
    }
    
    private void setDomainInvisible() {
        this.mView.findViewById(2131362305).setVisibility(8);
        this.mEapDomainView.setText((CharSequence)"");
    }
    
    private void setEapMethodInvisible() {
        this.mView.findViewById(2131362095).setVisibility(8);
    }
    
    private void setIdentityInvisible() {
        this.mView.findViewById(2131362306).setVisibility(8);
        this.mPhase2Spinner.setSelection(0);
    }
    
    private void setPasswordInvisible() {
        this.mPasswordView.setText((CharSequence)"");
        this.mView.findViewById(2131362441).setVisibility(8);
        this.mView.findViewById(2131362607).setVisibility(8);
    }
    
    private void setPhase2Invisible() {
        this.mView.findViewById(2131362308).setVisibility(8);
        this.mPhase2Spinner.setSelection(0);
    }
    
    private void setSelection(final Spinner spinner, final String s) {
        if (s != null) {
            final ArrayAdapter arrayAdapter = (ArrayAdapter)spinner.getAdapter();
            for (int i = arrayAdapter.getCount() - 1; i >= 0; --i) {
                if (s.equals(arrayAdapter.getItem(i))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }
    
    private void setUserCertInvisible() {
        this.mView.findViewById(2131362309).setVisibility(8);
        this.setSelection(this.mEapUserCertSpinner, this.mUnspecifiedCertString);
    }
    
    private void setVisibility(final int n, final int visibility) {
        final View viewById = this.mView.findViewById(n);
        if (viewById != null) {
            viewById.setVisibility(visibility);
        }
    }
    
    private void showEapFieldsByMethod(final int n) {
        this.mView.findViewById(2131362307).setVisibility(0);
        this.mView.findViewById(2131362306).setVisibility(0);
        this.mView.findViewById(2131362305).setVisibility(0);
        this.mView.findViewById(2131362304).setVisibility(0);
        this.mView.findViewById(2131362441).setVisibility(0);
        this.mView.findViewById(2131362607).setVisibility(0);
        this.mConfigUi.getContext();
        switch (n) {
            case 4:
            case 5:
            case 6: {
                this.setPhase2Invisible();
                this.setAnonymousIdentInvisible();
                this.setCaCertInvisible();
                this.setDomainInvisible();
                this.setUserCertInvisible();
                this.setPasswordInvisible();
                this.setIdentityInvisible();
                if (this.mAccessPoint != null && this.mAccessPoint.isCarrierAp()) {
                    this.setEapMethodInvisible();
                    break;
                }
                break;
            }
            case 3: {
                this.setPhase2Invisible();
                this.setCaCertInvisible();
                this.setDomainInvisible();
                this.setAnonymousIdentInvisible();
                this.setUserCertInvisible();
                break;
            }
            case 2: {
                if (this.mPhase2Adapter != this.mPhase2FullAdapter) {
                    this.mPhase2Adapter = this.mPhase2FullAdapter;
                    this.mPhase2Spinner.setAdapter((SpinnerAdapter)this.mPhase2Adapter);
                }
                this.mView.findViewById(2131362308).setVisibility(0);
                this.mView.findViewById(2131362303).setVisibility(0);
                this.setUserCertInvisible();
                break;
            }
            case 1: {
                this.mView.findViewById(2131362309).setVisibility(0);
                this.setPhase2Invisible();
                this.setAnonymousIdentInvisible();
                this.setPasswordInvisible();
                break;
            }
            case 0: {
                if (this.mPhase2Adapter != this.mPhase2PeapAdapter) {
                    this.mPhase2Adapter = this.mPhase2PeapAdapter;
                    this.mPhase2Spinner.setAdapter((SpinnerAdapter)this.mPhase2Adapter);
                }
                this.mView.findViewById(2131362308).setVisibility(0);
                this.mView.findViewById(2131362303).setVisibility(0);
                this.showPeapFields();
                this.setUserCertInvisible();
                break;
            }
        }
        if (this.mView.findViewById(2131362304).getVisibility() != 8) {
            final String s = (String)this.mEapCaCertSpinner.getSelectedItem();
            if (s.equals(this.mDoNotValidateEapServerString) || s.equals(this.mUnspecifiedCertString)) {
                this.setDomainInvisible();
            }
        }
    }
    
    private void showIpConfigFields() {
        final WifiConfiguration wifiConfiguration = null;
        this.mView.findViewById(2131362284).setVisibility(0);
        WifiConfiguration config = wifiConfiguration;
        if (this.mAccessPoint != null) {
            config = wifiConfiguration;
            if (this.mAccessPoint.isSaved()) {
                config = this.mAccessPoint.getConfig();
            }
        }
        if (this.mIpSettingsSpinner.getSelectedItemPosition() == 1) {
            this.mView.findViewById(2131362644).setVisibility(0);
            if (this.mIpAddressView == null) {
                (this.mIpAddressView = (TextView)this.mView.findViewById(2131362287)).addTextChangedListener((TextWatcher)this);
                (this.mGatewayView = (TextView)this.mView.findViewById(2131362175)).addTextChangedListener((TextWatcher)this);
                (this.mNetworkPrefixLengthView = (TextView)this.mView.findViewById(2131362384)).addTextChangedListener((TextWatcher)this);
                (this.mDns1View = (TextView)this.mView.findViewById(2131362080)).addTextChangedListener((TextWatcher)this);
                (this.mDns2View = (TextView)this.mView.findViewById(2131362081)).addTextChangedListener((TextWatcher)this);
            }
            if (config != null) {
                final StaticIpConfiguration staticIpConfiguration = config.getStaticIpConfiguration();
                if (staticIpConfiguration != null) {
                    if (staticIpConfiguration.ipAddress != null) {
                        this.mIpAddressView.setText((CharSequence)staticIpConfiguration.ipAddress.getAddress().getHostAddress());
                        this.mNetworkPrefixLengthView.setText((CharSequence)Integer.toString(staticIpConfiguration.ipAddress.getNetworkPrefixLength()));
                    }
                    if (staticIpConfiguration.gateway != null) {
                        this.mGatewayView.setText((CharSequence)staticIpConfiguration.gateway.getHostAddress());
                    }
                    final Iterator iterator = staticIpConfiguration.dnsServers.iterator();
                    if (iterator.hasNext()) {
                        this.mDns1View.setText((CharSequence)iterator.next().getHostAddress());
                    }
                    if (iterator.hasNext()) {
                        this.mDns2View.setText((CharSequence)iterator.next().getHostAddress());
                    }
                }
            }
        }
        else {
            this.mView.findViewById(2131362644).setVisibility(8);
        }
    }
    
    private void showPeapFields() {
        final int selectedItemPosition = this.mPhase2Spinner.getSelectedItemPosition();
        if (selectedItemPosition != 3 && selectedItemPosition != 4 && selectedItemPosition != 5) {
            this.mView.findViewById(2131362306).setVisibility(0);
            this.mView.findViewById(2131362303).setVisibility(0);
            this.mView.findViewById(2131362441).setVisibility(0);
            this.mView.findViewById(2131362607).setVisibility(0);
        }
        else {
            this.mEapIdentityView.setText((CharSequence)"");
            this.mView.findViewById(2131362306).setVisibility(8);
            this.setPasswordInvisible();
        }
    }
    
    private void showProxyFields() {
        final WifiConfiguration wifiConfiguration = null;
        this.mView.findViewById(2131362502).setVisibility(0);
        WifiConfiguration config = wifiConfiguration;
        if (this.mAccessPoint != null) {
            config = wifiConfiguration;
            if (this.mAccessPoint.isSaved()) {
                config = this.mAccessPoint.getConfig();
            }
        }
        if (this.mProxySettingsSpinner.getSelectedItemPosition() == 1) {
            this.setVisibility(2131362504, 0);
            this.setVisibility(2131362496, 0);
            this.setVisibility(2131362499, 8);
            if (this.mProxyHostView == null) {
                (this.mProxyHostView = (TextView)this.mView.findViewById(2131362497)).addTextChangedListener((TextWatcher)this);
                (this.mProxyPortView = (TextView)this.mView.findViewById(2131362500)).addTextChangedListener((TextWatcher)this);
                (this.mProxyExclusionListView = (TextView)this.mView.findViewById(2131362495)).addTextChangedListener((TextWatcher)this);
            }
            if (config != null) {
                final ProxyInfo httpProxy = config.getHttpProxy();
                if (httpProxy != null) {
                    this.mProxyHostView.setText((CharSequence)httpProxy.getHost());
                    this.mProxyPortView.setText((CharSequence)Integer.toString(httpProxy.getPort()));
                    this.mProxyExclusionListView.setText((CharSequence)httpProxy.getExclusionListAsString());
                }
            }
        }
        else if (this.mProxySettingsSpinner.getSelectedItemPosition() == 2) {
            this.setVisibility(2131362504, 8);
            this.setVisibility(2131362496, 8);
            this.setVisibility(2131362499, 0);
            if (this.mProxyPacView == null) {
                (this.mProxyPacView = (TextView)this.mView.findViewById(2131362498)).addTextChangedListener((TextWatcher)this);
            }
            if (config != null) {
                final ProxyInfo httpProxy2 = config.getHttpProxy();
                if (httpProxy2 != null) {
                    this.mProxyPacView.setText((CharSequence)httpProxy2.getPacFileUrl().toString());
                }
            }
        }
        else {
            this.setVisibility(2131362504, 8);
            this.setVisibility(2131362496, 8);
            this.setVisibility(2131362499, 8);
        }
    }
    
    private void showSecurityFields() {
        if (this.mAccessPointSecurity == 0) {
            this.mView.findViewById(2131362571).setVisibility(8);
            return;
        }
        this.mView.findViewById(2131362571).setVisibility(0);
        if (this.mPasswordView == null) {
            (this.mPasswordView = (TextView)this.mView.findViewById(2131362436)).addTextChangedListener((TextWatcher)this);
            this.mPasswordView.setOnEditorActionListener((TextView$OnEditorActionListener)this);
            this.mPasswordView.setOnKeyListener((View$OnKeyListener)this);
            ((CheckBox)this.mView.findViewById(2131362606)).setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
            if (this.mAccessPoint != null && this.mAccessPoint.isSaved()) {
                this.mPasswordView.setHint(2131890185);
            }
        }
        if (this.mAccessPointSecurity != 3) {
            this.mView.findViewById(2131362095).setVisibility(8);
            return;
        }
        this.mView.findViewById(2131362095).setVisibility(0);
        if (this.mEapMethodSpinner == null) {
            (this.mEapMethodSpinner = (Spinner)this.mView.findViewById(2131362371)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
            if (Utils.isWifiOnly(this.mContext) || !this.mContext.getResources().getBoolean(17956948)) {
                final ArrayAdapter adapter = new ArrayAdapter(this.mContext, 17367048, (Object[])this.mContext.getResources().getStringArray(2130903115));
                adapter.setDropDownViewResource(17367049);
                this.mEapMethodSpinner.setAdapter((SpinnerAdapter)adapter);
            }
            (this.mPhase2Spinner = (Spinner)this.mView.findViewById(2131362444)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
            (this.mEapCaCertSpinner = (Spinner)this.mView.findViewById(2131361956)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
            (this.mEapDomainView = (TextView)this.mView.findViewById(2131362086)).addTextChangedListener((TextWatcher)this);
            (this.mEapUserCertSpinner = (Spinner)this.mView.findViewById(2131362785)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
            this.mEapIdentityView = (TextView)this.mView.findViewById(2131362243);
            this.mEapAnonymousView = (TextView)this.mView.findViewById(2131361869);
            if (this.mAccessPoint != null && this.mAccessPoint.isCarrierAp()) {
                this.mEapMethodSpinner.setSelection(this.mAccessPoint.getCarrierApEapType());
            }
            this.loadCertificates(this.mEapCaCertSpinner, "CACERT_", this.mDoNotValidateEapServerString, false, true);
            this.loadCertificates(this.mEapUserCertSpinner, "USRPKEY_", this.mDoNotProvideEapUserCertString, false, false);
            if (this.mAccessPoint != null && this.mAccessPoint.isSaved()) {
                final WifiEnterpriseConfig enterpriseConfig = this.mAccessPoint.getConfig().enterpriseConfig;
                final int eapMethod = enterpriseConfig.getEapMethod();
                final int phase2Method = enterpriseConfig.getPhase2Method();
                this.mEapMethodSpinner.setSelection(eapMethod);
                this.showEapFieldsByMethod(eapMethod);
                if (eapMethod != 0) {
                    this.mPhase2Spinner.setSelection(phase2Method);
                }
                else if (phase2Method != 0) {
                    switch (phase2Method) {
                        default: {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Invalid phase 2 method ");
                            sb.append(phase2Method);
                            Log.e("WifiConfigController", sb.toString());
                            break;
                        }
                        case 7: {
                            this.mPhase2Spinner.setSelection(5);
                            break;
                        }
                        case 6: {
                            this.mPhase2Spinner.setSelection(4);
                            break;
                        }
                        case 5: {
                            this.mPhase2Spinner.setSelection(3);
                            break;
                        }
                        case 4: {
                            this.mPhase2Spinner.setSelection(2);
                            break;
                        }
                        case 3: {
                            this.mPhase2Spinner.setSelection(1);
                            break;
                        }
                    }
                }
                else {
                    this.mPhase2Spinner.setSelection(0);
                }
                if (!TextUtils.isEmpty((CharSequence)enterpriseConfig.getCaPath())) {
                    this.setSelection(this.mEapCaCertSpinner, this.mUseSystemCertsString);
                }
                else {
                    final String[] caCertificateAliases = enterpriseConfig.getCaCertificateAliases();
                    if (caCertificateAliases == null) {
                        this.setSelection(this.mEapCaCertSpinner, this.mDoNotValidateEapServerString);
                    }
                    else if (caCertificateAliases.length == 1) {
                        this.setSelection(this.mEapCaCertSpinner, caCertificateAliases[0]);
                    }
                    else {
                        this.loadCertificates(this.mEapCaCertSpinner, "CACERT_", this.mDoNotValidateEapServerString, true, true);
                        this.setSelection(this.mEapCaCertSpinner, this.mMultipleCertSetString);
                    }
                }
                this.mEapDomainView.setText((CharSequence)enterpriseConfig.getDomainSuffixMatch());
                final String clientCertificateAlias = enterpriseConfig.getClientCertificateAlias();
                if (TextUtils.isEmpty((CharSequence)clientCertificateAlias)) {
                    this.setSelection(this.mEapUserCertSpinner, this.mDoNotProvideEapUserCertString);
                }
                else {
                    this.setSelection(this.mEapUserCertSpinner, clientCertificateAlias);
                }
                this.mEapIdentityView.setText((CharSequence)enterpriseConfig.getIdentity());
                this.mEapAnonymousView.setText((CharSequence)enterpriseConfig.getAnonymousIdentity());
            }
            else {
                this.mPhase2Spinner = (Spinner)this.mView.findViewById(2131362444);
                this.showEapFieldsByMethod(this.mEapMethodSpinner.getSelectedItemPosition());
            }
        }
        else {
            this.showEapFieldsByMethod(this.mEapMethodSpinner.getSelectedItemPosition());
        }
    }
    
    private int validateIpConfigFields(final StaticIpConfiguration staticIpConfiguration) {
        if (this.mIpAddressView == null) {
            return 0;
        }
        final String string = this.mIpAddressView.getText().toString();
        if (TextUtils.isEmpty((CharSequence)string)) {
            return 2131890032;
        }
        final Inet4Address iPv4Address = this.getIPv4Address(string);
        if (iPv4Address != null && !iPv4Address.equals(Inet4Address.ANY)) {
            int n = -1;
            try {
                final int int1 = Integer.parseInt(this.mNetworkPrefixLengthView.getText().toString());
                if (int1 < 0 || int1 > 32) {
                    return 2131890033;
                }
                n = int1;
                n = int1;
                final LinkAddress ipAddress = new LinkAddress((InetAddress)iPv4Address, int1);
                n = int1;
                staticIpConfiguration.ipAddress = ipAddress;
                n = int1;
            }
            catch (IllegalArgumentException ex) {
                return 2131890032;
            }
            catch (NumberFormatException ex2) {
                this.mNetworkPrefixLengthView.setText((CharSequence)this.mConfigUi.getContext().getString(2131890052));
            }
            final String string2 = this.mGatewayView.getText().toString();
            if (TextUtils.isEmpty((CharSequence)string2)) {
                try {
                    final byte[] address = NetworkUtils.getNetworkPart((InetAddress)iPv4Address, n).getAddress();
                    address[address.length - 1] = 1;
                    this.mGatewayView.setText((CharSequence)InetAddress.getByAddress(address).getHostAddress());
                }
                catch (UnknownHostException ex3) {}
                catch (RuntimeException ex4) {}
            }
            else {
                final Inet4Address iPv4Address2 = this.getIPv4Address(string2);
                if (iPv4Address2 == null) {
                    return 2131890031;
                }
                if (iPv4Address2.isMulticastAddress()) {
                    return 2131890031;
                }
                staticIpConfiguration.gateway = iPv4Address2;
            }
            final String string3 = this.mDns1View.getText().toString();
            if (TextUtils.isEmpty((CharSequence)string3)) {
                this.mDns1View.setText((CharSequence)this.mConfigUi.getContext().getString(2131889978));
            }
            else {
                final Inet4Address iPv4Address3 = this.getIPv4Address(string3);
                if (iPv4Address3 == null) {
                    return 2131890030;
                }
                staticIpConfiguration.dnsServers.add(iPv4Address3);
            }
            if (this.mDns2View.length() > 0) {
                final Inet4Address iPv4Address4 = this.getIPv4Address(this.mDns2View.getText().toString());
                if (iPv4Address4 == null) {
                    return 2131890030;
                }
                staticIpConfiguration.dnsServers.add(iPv4Address4);
            }
            return 0;
        }
        return 2131890032;
    }
    
    public void afterTextChanged(final Editable editable) {
        ThreadUtils.postOnMainThread(new _$$Lambda$WifiConfigController$zJJNImzldn2IDwntJeWg8KPYIDY(this));
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    void enableSubmitIfAppropriate() {
        final Button submitButton = this.mConfigUi.getSubmitButton();
        if (submitButton == null) {
            return;
        }
        submitButton.setEnabled(this.isSubmittable());
    }
    
    public AccessPoint getAccessPoint() {
        return this.mAccessPoint;
    }
    
    public WifiConfiguration getConfig() {
        if (this.mMode == 0) {
            return null;
        }
        final WifiConfiguration wifiConfiguration = new WifiConfiguration();
        if (this.mAccessPoint == null) {
            wifiConfiguration.SSID = AccessPoint.convertToQuotedString(this.mSsidView.getText().toString());
            wifiConfiguration.hiddenSSID = (this.mHiddenSettingsSpinner.getSelectedItemPosition() == 1);
        }
        else if (!this.mAccessPoint.isSaved()) {
            wifiConfiguration.SSID = AccessPoint.convertToQuotedString(this.mAccessPoint.getSsidStr());
        }
        else {
            wifiConfiguration.networkId = this.mAccessPoint.getConfig().networkId;
            wifiConfiguration.hiddenSSID = this.mAccessPoint.getConfig().hiddenSSID;
        }
        wifiConfiguration.shared = this.mSharedCheckBox.isChecked();
        switch (this.mAccessPointSecurity) {
            default: {
                return null;
            }
            case 3: {
                wifiConfiguration.allowedKeyManagement.set(2);
                wifiConfiguration.allowedKeyManagement.set(3);
                wifiConfiguration.enterpriseConfig = new WifiEnterpriseConfig();
                final int selectedItemPosition = this.mEapMethodSpinner.getSelectedItemPosition();
                final int selectedItemPosition2 = this.mPhase2Spinner.getSelectedItemPosition();
                wifiConfiguration.enterpriseConfig.setEapMethod(selectedItemPosition);
                if (selectedItemPosition != 0) {
                    wifiConfiguration.enterpriseConfig.setPhase2Method(selectedItemPosition2);
                }
                else {
                    switch (selectedItemPosition2) {
                        default: {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unknown phase2 method");
                            sb.append(selectedItemPosition2);
                            Log.e("WifiConfigController", sb.toString());
                            break;
                        }
                        case 5: {
                            wifiConfiguration.enterpriseConfig.setPhase2Method(7);
                            break;
                        }
                        case 4: {
                            wifiConfiguration.enterpriseConfig.setPhase2Method(6);
                            break;
                        }
                        case 3: {
                            wifiConfiguration.enterpriseConfig.setPhase2Method(5);
                            break;
                        }
                        case 2: {
                            wifiConfiguration.enterpriseConfig.setPhase2Method(4);
                            break;
                        }
                        case 1: {
                            wifiConfiguration.enterpriseConfig.setPhase2Method(3);
                            break;
                        }
                        case 0: {
                            wifiConfiguration.enterpriseConfig.setPhase2Method(0);
                            break;
                        }
                    }
                }
                final String s = (String)this.mEapCaCertSpinner.getSelectedItem();
                wifiConfiguration.enterpriseConfig.setCaCertificateAliases((String[])null);
                wifiConfiguration.enterpriseConfig.setCaPath((String)null);
                wifiConfiguration.enterpriseConfig.setDomainSuffixMatch(this.mEapDomainView.getText().toString());
                if (!s.equals(this.mUnspecifiedCertString)) {
                    if (!s.equals(this.mDoNotValidateEapServerString)) {
                        if (s.equals(this.mUseSystemCertsString)) {
                            wifiConfiguration.enterpriseConfig.setCaPath("/system/etc/security/cacerts");
                        }
                        else if (s.equals(this.mMultipleCertSetString)) {
                            if (this.mAccessPoint != null) {
                                if (!this.mAccessPoint.isSaved()) {
                                    Log.e("WifiConfigController", "Multiple certs can only be set when editing saved network");
                                }
                                wifiConfiguration.enterpriseConfig.setCaCertificateAliases(this.mAccessPoint.getConfig().enterpriseConfig.getCaCertificateAliases());
                            }
                        }
                        else {
                            wifiConfiguration.enterpriseConfig.setCaCertificateAliases(new String[] { s });
                        }
                    }
                }
                if (wifiConfiguration.enterpriseConfig.getCaCertificateAliases() != null && wifiConfiguration.enterpriseConfig.getCaPath() != null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("ca_cert (");
                    sb2.append(wifiConfiguration.enterpriseConfig.getCaCertificateAliases());
                    sb2.append(") and ca_path (");
                    sb2.append(wifiConfiguration.enterpriseConfig.getCaPath());
                    sb2.append(") should not both be non-null");
                    Log.e("WifiConfigController", sb2.toString());
                }
                final String s2 = (String)this.mEapUserCertSpinner.getSelectedItem();
                String clientCertificateAlias = null;
                Label_0702: {
                    if (!s2.equals(this.mUnspecifiedCertString)) {
                        clientCertificateAlias = s2;
                        if (!s2.equals(this.mDoNotProvideEapUserCertString)) {
                            break Label_0702;
                        }
                    }
                    clientCertificateAlias = "";
                }
                wifiConfiguration.enterpriseConfig.setClientCertificateAlias(clientCertificateAlias);
                if (selectedItemPosition != 4 && selectedItemPosition != 5 && selectedItemPosition != 6) {
                    if (selectedItemPosition == 3) {
                        wifiConfiguration.enterpriseConfig.setIdentity(this.mEapIdentityView.getText().toString());
                        wifiConfiguration.enterpriseConfig.setAnonymousIdentity("");
                    }
                    else {
                        wifiConfiguration.enterpriseConfig.setIdentity(this.mEapIdentityView.getText().toString());
                        wifiConfiguration.enterpriseConfig.setAnonymousIdentity(this.mEapAnonymousView.getText().toString());
                    }
                }
                else {
                    wifiConfiguration.enterpriseConfig.setIdentity("");
                    wifiConfiguration.enterpriseConfig.setAnonymousIdentity("");
                }
                if (!this.mPasswordView.isShown()) {
                    wifiConfiguration.enterpriseConfig.setPassword(this.mPasswordView.getText().toString());
                    break;
                }
                if (this.mPasswordView.length() > 0) {
                    wifiConfiguration.enterpriseConfig.setPassword(this.mPasswordView.getText().toString());
                    break;
                }
                break;
            }
            case 2: {
                wifiConfiguration.allowedKeyManagement.set(1);
                if (this.mPasswordView.length() != 0) {
                    final String string = this.mPasswordView.getText().toString();
                    if (string.matches("[0-9A-Fa-f]{64}")) {
                        wifiConfiguration.preSharedKey = string;
                    }
                    else {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append('\"');
                        sb3.append(string);
                        sb3.append('\"');
                        wifiConfiguration.preSharedKey = sb3.toString();
                    }
                    break;
                }
                break;
            }
            case 1: {
                wifiConfiguration.allowedKeyManagement.set(0);
                wifiConfiguration.allowedAuthAlgorithms.set(0);
                wifiConfiguration.allowedAuthAlgorithms.set(1);
                if (this.mPasswordView.length() != 0) {
                    final int length = this.mPasswordView.length();
                    final String string2 = this.mPasswordView.getText().toString();
                    if ((length == 10 || length == 26 || length == 58) && string2.matches("[0-9A-Fa-f]*")) {
                        wifiConfiguration.wepKeys[0] = string2;
                    }
                    else {
                        final String[] wepKeys = wifiConfiguration.wepKeys;
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append('\"');
                        sb4.append(string2);
                        sb4.append('\"');
                        wepKeys[0] = sb4.toString();
                    }
                    break;
                }
                break;
            }
            case 0: {
                wifiConfiguration.allowedKeyManagement.set(0);
                break;
            }
        }
        wifiConfiguration.setIpConfiguration(new IpConfiguration(this.mIpAssignment, this.mProxySettings, this.mStaticIpConfiguration, this.mHttpProxy));
        if (this.mMeteredSettingsSpinner != null) {
            wifiConfiguration.meteredOverride = this.mMeteredSettingsSpinner.getSelectedItemPosition();
        }
        return wifiConfiguration;
    }
    
    KeyStore getKeyStore() {
        return KeyStore.getInstance();
    }
    
    public int getMode() {
        return this.mMode;
    }
    
    String getSignalString() {
        final boolean reachable = this.mAccessPoint.isReachable();
        final String s = null;
        if (!reachable) {
            return null;
        }
        final int level = this.mAccessPoint.getLevel();
        String s2 = s;
        if (level > -1) {
            s2 = s;
            if (level < this.mLevels.length) {
                s2 = this.mLevels[level];
            }
        }
        return s2;
    }
    
    void hideForgetButton() {
        final Button forgetButton = this.mConfigUi.getForgetButton();
        if (forgetButton == null) {
            return;
        }
        forgetButton.setVisibility(8);
    }
    
    void hideSubmitButton() {
        final Button submitButton = this.mConfigUi.getSubmitButton();
        if (submitButton == null) {
            return;
        }
        submitButton.setVisibility(8);
    }
    
    boolean isSplitSystemUser() {
        this.mContext.getSystemService("user");
        return UserManager.isSplitSystemUser();
    }
    
    boolean isSubmittable() {
        boolean b2;
        final boolean b = b2 = false;
        Label_0062: {
            if (this.mPasswordView != null) {
                if (this.mAccessPointSecurity != 1 || this.mPasswordView.length() != 0) {
                    b2 = b;
                    if (this.mAccessPointSecurity != 2) {
                        break Label_0062;
                    }
                    b2 = b;
                    if (this.isValidPsk(this.mPasswordView.getText().toString())) {
                        break Label_0062;
                    }
                }
                b2 = true;
            }
        }
        boolean b4;
        boolean b3 = b4 = ((this.mSsidView == null || this.mSsidView.length() != 0) && ((this.mAccessPoint != null && this.mAccessPoint.isSaved()) || !b2) && (this.mAccessPoint == null || !this.mAccessPoint.isSaved() || !b2 || this.mPasswordView.length() <= 0) && this.ipAndProxyFieldsAreValid());
        if (this.mEapCaCertSpinner != null) {
            b4 = b3;
            if (this.mView.findViewById(2131362304).getVisibility() != 8) {
                final String s = (String)this.mEapCaCertSpinner.getSelectedItem();
                if (s.equals(this.mUnspecifiedCertString)) {
                    b3 = false;
                }
                b4 = b3;
                if (s.equals(this.mUseSystemCertsString)) {
                    b4 = b3;
                    if (this.mEapDomainView != null) {
                        b4 = b3;
                        if (this.mView.findViewById(2131362305).getVisibility() != 8) {
                            b4 = b3;
                            if (TextUtils.isEmpty((CharSequence)this.mEapDomainView.getText().toString())) {
                                b4 = false;
                            }
                        }
                    }
                }
            }
        }
        boolean b5 = b4;
        if (this.mEapUserCertSpinner != null) {
            b5 = b4;
            if (this.mView.findViewById(2131362309).getVisibility() != 8) {
                b5 = b4;
                if (((String)this.mEapUserCertSpinner.getSelectedItem()).equals(this.mUnspecifiedCertString)) {
                    b5 = false;
                }
            }
        }
        return b5;
    }
    
    boolean isValidPsk(final String s) {
        return (s.length() == 64 && s.matches("[0-9A-Fa-f]{64}")) || (s.length() >= 8 && s.length() <= 63);
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        if (compoundButton.getId() == 2131362606) {
            final int selectionEnd = this.mPasswordView.getSelectionEnd();
            final TextView mPasswordView = this.mPasswordView;
            int n;
            if (b) {
                n = 144;
            }
            else {
                n = 128;
            }
            mPasswordView.setInputType(0x1 | n);
            if (selectionEnd >= 0) {
                ((EditText)this.mPasswordView).setSelection(selectionEnd);
            }
        }
        else if (compoundButton.getId() == 2131362825) {
            final View viewById = this.mView.findViewById(2131362824);
            int visibility;
            int n2;
            if (b) {
                visibility = 0;
                n2 = 2131889896;
            }
            else {
                visibility = 8;
                n2 = 2131889895;
            }
            this.mView.findViewById(2131362823).setVisibility(visibility);
            viewById.setContentDescription((CharSequence)this.mContext.getString(n2));
        }
    }
    
    public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
        if (textView == this.mPasswordView && n == 6 && this.isSubmittable()) {
            this.mConfigUi.dispatchSubmit();
            return true;
        }
        return false;
    }
    
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int mAccessPointSecurity, final long n) {
        if (adapterView == this.mSecuritySpinner) {
            this.mAccessPointSecurity = mAccessPointSecurity;
            this.showSecurityFields();
        }
        else if (adapterView != this.mEapMethodSpinner && adapterView != this.mEapCaCertSpinner) {
            if (adapterView == this.mPhase2Spinner && this.mEapMethodSpinner.getSelectedItemPosition() == 0) {
                this.showPeapFields();
            }
            else if (adapterView == this.mProxySettingsSpinner) {
                this.showProxyFields();
            }
            else if (adapterView == this.mHiddenSettingsSpinner) {
                final TextView mHiddenWarningView = this.mHiddenWarningView;
                int visibility;
                if (mAccessPointSecurity == 0) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                mHiddenWarningView.setVisibility(visibility);
                if (mAccessPointSecurity == 1) {
                    this.mDialogContainer.post((Runnable)new _$$Lambda$WifiConfigController$7dViR1XLVJsqzanUSq_nLAYeTK0(this));
                }
            }
            else {
                this.showIpConfigFields();
            }
        }
        else {
            this.showSecurityFields();
        }
        this.showWarningMessagesIfAppropriate();
        this.enableSubmitIfAppropriate();
    }
    
    public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
        if (view == this.mPasswordView && n == 66 && this.isSubmittable()) {
            this.mConfigUi.dispatchSubmit();
            return true;
        }
        return false;
    }
    
    public void onNothingSelected(final AdapterView<?> adapterView) {
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    void showWarningMessagesIfAppropriate() {
        this.mView.findViewById(2131362401).setVisibility(8);
        this.mView.findViewById(2131362403).setVisibility(8);
        this.mView.findViewById(2131362637).setVisibility(8);
        if (this.mSsidView != null && WifiUtils.isSSIDTooLong(this.mSsidView.getText().toString())) {
            this.mView.findViewById(2131362637).setVisibility(0);
        }
        if (this.mEapCaCertSpinner != null && this.mView.findViewById(2131362304).getVisibility() != 8) {
            final String s = (String)this.mEapCaCertSpinner.getSelectedItem();
            if (s.equals(this.mDoNotValidateEapServerString)) {
                this.mView.findViewById(2131362401).setVisibility(0);
            }
            if (s.equals(this.mUseSystemCertsString) && this.mEapDomainView != null && this.mView.findViewById(2131362305).getVisibility() != 8 && TextUtils.isEmpty((CharSequence)this.mEapDomainView.getText().toString())) {
                this.mView.findViewById(2131362403).setVisibility(0);
            }
        }
    }
    
    public void updatePassword() {
        final TextView textView = (TextView)this.mView.findViewById(2131362436);
        int n;
        if (((CheckBox)this.mView.findViewById(2131362606)).isChecked()) {
            n = 144;
        }
        else {
            n = 128;
        }
        textView.setInputType(n | 0x1);
    }
}
