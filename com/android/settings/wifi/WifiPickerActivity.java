package com.android.settings.wifi;

import com.android.settings.wifi.p2p.WifiP2pSettings;
import android.support.v14.preference.PreferenceFragment;
import android.content.Intent;
import com.android.settings.ButtonBarHandler;
import com.android.settings.SettingsActivity;

public class WifiPickerActivity extends SettingsActivity implements ButtonBarHandler
{
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        if (!intent.hasExtra(":settings:show_fragment")) {
            intent.putExtra(":settings:show_fragment", this.getWifiSettingsClass().getName());
            intent.putExtra(":settings:show_fragment_title_resid", 2131890108);
        }
        return intent;
    }
    
    Class<? extends PreferenceFragment> getWifiSettingsClass() {
        return WifiSettings.class;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return WifiSettings.class.getName().equals(s) || WifiP2pSettings.class.getName().equals(s) || SavedAccessPointsWifiSettings.class.getName().equals(s);
    }
}
