package com.android.settings.wifi;

import android.os.Handler;
import android.os.Looper;
import android.net.Uri;
import android.database.ContentObserver;
import android.support.v14.preference.SwitchPreference;
import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import android.provider.Settings;
import java.util.Iterator;
import android.net.NetworkScorerAppData;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.net.NetworkScoreManager;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.ContentResolver;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class UseOpenWifiPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final ContentResolver mContentResolver;
    private boolean mDoFeatureSupportedScorersExist;
    private ComponentName mEnableUseWifiComponentName;
    private final Fragment mFragment;
    private final NetworkScoreManager mNetworkScoreManager;
    private Preference mPreference;
    private final SettingObserver mSettingObserver;
    
    public UseOpenWifiPreferenceController(final Context context, final Fragment mFragment, final Lifecycle lifecycle) {
        super(context);
        this.mContentResolver = context.getContentResolver();
        this.mFragment = mFragment;
        this.mNetworkScoreManager = (NetworkScoreManager)context.getSystemService("network_score");
        this.mSettingObserver = new SettingObserver();
        this.updateEnableUseWifiComponentName();
        this.checkForFeatureSupportedScorers();
        lifecycle.addObserver(this);
    }
    
    private void checkForFeatureSupportedScorers() {
        if (this.mEnableUseWifiComponentName != null) {
            this.mDoFeatureSupportedScorersExist = true;
            return;
        }
        final Iterator<NetworkScorerAppData> iterator = this.mNetworkScoreManager.getAllValidScorers().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getEnableUseOpenWifiActivity() != null) {
                this.mDoFeatureSupportedScorersExist = true;
                return;
            }
        }
        this.mDoFeatureSupportedScorersExist = false;
    }
    
    private boolean isSettingEnabled() {
        final String string = Settings.Global.getString(this.mContentResolver, "use_open_wifi_package");
        CharSequence packageName;
        if (this.mEnableUseWifiComponentName == null) {
            packageName = null;
        }
        else {
            packageName = this.mEnableUseWifiComponentName.getPackageName();
        }
        return TextUtils.equals((CharSequence)string, packageName);
    }
    
    private void updateEnableUseWifiComponentName() {
        final NetworkScorerAppData activeScorer = this.mNetworkScoreManager.getActiveScorer();
        ComponentName enableUseOpenWifiActivity;
        if (activeScorer == null) {
            enableUseOpenWifiActivity = null;
        }
        else {
            enableUseOpenWifiActivity = activeScorer.getEnableUseOpenWifiActivity();
        }
        this.mEnableUseWifiComponentName = enableUseOpenWifiActivity;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference("use_open_wifi_automatically");
    }
    
    @Override
    public String getPreferenceKey() {
        return "use_open_wifi_automatically";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mDoFeatureSupportedScorersExist;
    }
    
    public boolean onActivityResult(final int n, final int n2) {
        if (n != 400) {
            return false;
        }
        if (n2 == -1) {
            Settings.Global.putString(this.mContentResolver, "use_open_wifi_package", this.mEnableUseWifiComponentName.getPackageName());
        }
        return true;
    }
    
    @Override
    public void onPause() {
        this.mSettingObserver.unregister(this.mContentResolver);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"use_open_wifi_automatically") || !this.isAvailable()) {
            return false;
        }
        if (this.isSettingEnabled()) {
            Settings.Global.putString(this.mContentResolver, "use_open_wifi_package", "");
            return true;
        }
        final Intent intent = new Intent("android.net.scoring.CUSTOM_ENABLE");
        intent.setComponent(this.mEnableUseWifiComponentName);
        this.mFragment.startActivityForResult(intent, 400);
        return false;
    }
    
    @Override
    public void onResume() {
        this.mSettingObserver.register(this.mContentResolver);
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof SwitchPreference)) {
            return;
        }
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        final String activeScorerPackage = this.mNetworkScoreManager.getActiveScorerPackage();
        final boolean b = false;
        final boolean b2 = activeScorerPackage != null;
        final boolean b3 = this.mEnableUseWifiComponentName != null;
        switchPreference.setChecked(this.isSettingEnabled());
        switchPreference.setVisible(this.isAvailable());
        boolean enabled = b;
        if (b2) {
            enabled = b;
            if (b3) {
                enabled = true;
            }
        }
        switchPreference.setEnabled(enabled);
        if (!b2) {
            switchPreference.setSummary(2131889673);
        }
        else if (!b3) {
            switchPreference.setSummary(2131889672);
        }
        else {
            switchPreference.setSummary(2131889671);
        }
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri NETWORK_RECOMMENDATIONS_ENABLED_URI;
        
        public SettingObserver() {
            super(new Handler(Looper.getMainLooper()));
            this.NETWORK_RECOMMENDATIONS_ENABLED_URI = Settings.Global.getUriFor("network_recommendations_enabled");
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.NETWORK_RECOMMENDATIONS_ENABLED_URI.equals((Object)uri)) {
                UseOpenWifiPreferenceController.this.updateEnableUseWifiComponentName();
                UseOpenWifiPreferenceController.this.updateState(UseOpenWifiPreferenceController.this.mPreference);
            }
        }
        
        public void register(final ContentResolver contentResolver) {
            contentResolver.registerContentObserver(this.NETWORK_RECOMMENDATIONS_ENABLED_URI, false, (ContentObserver)this);
            this.onChange(true, this.NETWORK_RECOMMENDATIONS_ENABLED_URI);
        }
        
        public void unregister(final ContentResolver contentResolver) {
            contentResolver.unregisterContentObserver((ContentObserver)this);
        }
    }
}
