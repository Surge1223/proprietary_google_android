package com.android.settings.wifi;

import android.text.TextUtils;
import android.net.wifi.WifiInfo;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkScoreManager;
import android.net.wifi.WifiManager;
import android.content.Context;
import com.android.settingslib.wifi.WifiStatusTracker;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settings.widget.SummaryUpdater;

public final class WifiSummaryUpdater extends SummaryUpdater
{
    private static final IntentFilter INTENT_FILTER;
    private final BroadcastReceiver mReceiver;
    private final WifiStatusTracker mWifiTracker;
    
    static {
        (INTENT_FILTER = new IntentFilter()).addAction("android.net.wifi.WIFI_STATE_CHANGED");
        WifiSummaryUpdater.INTENT_FILTER.addAction("android.net.wifi.STATE_CHANGE");
        WifiSummaryUpdater.INTENT_FILTER.addAction("android.net.wifi.RSSI_CHANGED");
    }
    
    public WifiSummaryUpdater(final Context context, final OnSummaryChangeListener onSummaryChangeListener) {
        this(context, onSummaryChangeListener, null);
    }
    
    public WifiSummaryUpdater(final Context context, final OnSummaryChangeListener onSummaryChangeListener, WifiStatusTracker mWifiTracker) {
        super(context, onSummaryChangeListener);
        if (mWifiTracker == null) {
            mWifiTracker = new WifiStatusTracker(context, (WifiManager)context.getSystemService((Class)WifiManager.class), (NetworkScoreManager)context.getSystemService((Class)NetworkScoreManager.class), (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class), new _$$Lambda$WifiSummaryUpdater$5w1MXX8MJfsbMZcSIHVb0vJmaww(this));
        }
        this.mWifiTracker = mWifiTracker;
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                WifiSummaryUpdater.this.mWifiTracker.handleBroadcast(intent);
                SummaryUpdater.this.notifyChangeIfNeeded();
            }
        };
    }
    
    public String getSummary() {
        if (!this.mWifiTracker.enabled) {
            return this.mContext.getString(2131889421);
        }
        if (!this.mWifiTracker.connected) {
            return this.mContext.getString(2131887439);
        }
        final String removeDoubleQuotes = WifiInfo.removeDoubleQuotes(this.mWifiTracker.ssid);
        if (TextUtils.isEmpty((CharSequence)this.mWifiTracker.statusLabel)) {
            return removeDoubleQuotes;
        }
        return this.mContext.getResources().getString(R.string.preference_summary_default_combination, new Object[] { removeDoubleQuotes, this.mWifiTracker.statusLabel });
    }
    
    public void register(final boolean listening) {
        if (listening) {
            this.mContext.registerReceiver(this.mReceiver, WifiSummaryUpdater.INTENT_FILTER);
        }
        else {
            this.mContext.unregisterReceiver(this.mReceiver);
        }
        this.mWifiTracker.setListening(listening);
    }
}
