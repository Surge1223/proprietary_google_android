package com.android.settings.wifi;

import android.text.TextUtils;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class WifiDetailPreference extends Preference
{
    private String mDetailText;
    
    public WifiDetailPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setWidgetLayoutResource(2131558688);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(2131362820);
        textView.setText((CharSequence)this.mDetailText);
        textView.setPadding(0, 0, 10, 0);
    }
    
    public void setDetailText(final String mDetailText) {
        if (TextUtils.equals((CharSequence)this.mDetailText, (CharSequence)mDetailText)) {
            return;
        }
        this.mDetailText = mDetailText;
        this.notifyChanged();
    }
}
