package com.android.settings.wifi;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View.OnClickListener;
import com.android.settings.utils.AnnotationSpan;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import com.android.settings.dashboard.DashboardFragment;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import android.location.LocationManager;
import android.app.Fragment;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiWakeupPreferenceController extends AbstractPreferenceController
{
    private final Fragment mFragment;
    LocationManager mLocationManager;
    SwitchPreference mPreference;
    
    public WifiWakeupPreferenceController(final Context context, final DashboardFragment mFragment) {
        super(context);
        this.mFragment = mFragment;
        this.mLocationManager = (LocationManager)context.getSystemService("location");
    }
    
    private boolean getWifiScanningEnabled() {
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "wifi_scan_always_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b;
    }
    
    private boolean getWifiWakeupEnabled() {
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "wifi_wakeup_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        return b;
    }
    
    private void setWifiWakeupEnabled(final boolean b) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "wifi_wakeup_enabled", (int)(b ? 1 : 0));
    }
    
    private void showScanningDialog() {
        final WifiScanningRequiredFragment instance = WifiScanningRequiredFragment.newInstance();
        instance.setTargetFragment(this.mFragment, 600);
        instance.show(this.mFragment.getFragmentManager(), "WifiWakeupPrefController");
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.updateState(this.mPreference = (SwitchPreference)preferenceScreen.findPreference("enable_wifi_wakeup"));
    }
    
    CharSequence getNoLocationSummary() {
        return AnnotationSpan.linkify(this.mContext.getText(2131890194), new AnnotationSpan.LinkInfo("link", null));
    }
    
    @Override
    public String getPreferenceKey() {
        return "enable_wifi_wakeup";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"enable_wifi_wakeup")) {
            return false;
        }
        if (!(preference instanceof SwitchPreference)) {
            return false;
        }
        if (!this.mLocationManager.isLocationEnabled()) {
            this.mFragment.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        }
        else if (this.getWifiWakeupEnabled()) {
            this.setWifiWakeupEnabled(false);
        }
        else if (!this.getWifiScanningEnabled()) {
            this.showScanningDialog();
        }
        else {
            this.setWifiWakeupEnabled(true);
        }
        this.updateState(this.mPreference);
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void onActivityResult(final int n, final int n2) {
        if (n != 600) {
            return;
        }
        if (this.mLocationManager.isLocationEnabled()) {
            this.setWifiWakeupEnabled(true);
        }
        this.updateState(this.mPreference);
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof SwitchPreference)) {
            return;
        }
        ((SwitchPreference)preference).setChecked(this.getWifiWakeupEnabled() && this.getWifiScanningEnabled() && this.mLocationManager.isLocationEnabled());
        if (!this.mLocationManager.isLocationEnabled()) {
            preference.setSummary(this.getNoLocationSummary());
        }
        else {
            preference.setSummary(2131890193);
        }
    }
}
