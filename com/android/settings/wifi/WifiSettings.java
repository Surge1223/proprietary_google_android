package com.android.settings.wifi;

import com.android.settings.widget.SummaryUpdater;
import android.app.Dialog;
import android.nfc.NfcAdapter;
import android.view.ContextMenu$ContextMenuInfo;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.Toast;
import com.android.settingslib.wifi.WifiTrackerFactory;
import android.net.NetworkInfo.State;
import android.util.Pair;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.LinkifyUtils;
import android.net.ConnectivityManager$NetworkCallback;
import android.os.Handler;
import android.os.Looper;
import android.net.NetworkRequest$Builder;
import com.android.settings.wifi.details.WifiNetworkDetailsFragment;
import com.android.settings.location.ScanningSettings;
import com.android.settings.core.SubSettingLauncher;
import android.content.ContentResolver;
import android.provider.Settings;
import android.os.PowerManager;
import android.util.Log;
import android.net.wifi.WifiConfiguration$NetworkSelectionStatus;
import android.net.wifi.WifiConfiguration;
import android.net.Network;
import com.android.settings.widget.SwitchWidgetController;
import com.android.settings.widget.SwitchBarController;
import com.android.settings.SettingsActivity;
import android.app.Fragment;
import android.app.Activity;
import android.content.res.Resources;
import java.util.ArrayList;
import com.android.settings.search.SearchIndexableRaw;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.net.wifi.WifiManager;
import com.android.settingslib.wifi.AccessPointPreference;
import android.view.View;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager$ActionListener;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.os.Bundle;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settingslib.wifi.WifiTracker;
import com.android.settingslib.wifi.AccessPoint;
import com.android.settings.search.Indexable;
import com.android.settings.RestrictedSettingsFragment;

public class WifiSettings extends RestrictedSettingsFragment implements Indexable, WifiDialogListener, AccessPointListener, WifiListener
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private Bundle mAccessPointSavedState;
    private PreferenceCategory mAccessPointsPreferenceCategory;
    private Preference mAddPreference;
    private PreferenceCategory mAdditionalSettingsPreferenceCategory;
    private CaptivePortalNetworkCallback mCaptivePortalNetworkCallback;
    private boolean mClickedConnect;
    private Preference mConfigureWifiSettingsPreference;
    private WifiManager$ActionListener mConnectListener;
    private PreferenceCategory mConnectedAccessPointPreferenceCategory;
    private ConnectivityManager mConnectivityManager;
    private WifiDialog mDialog;
    private int mDialogMode;
    private AccessPoint mDlgAccessPoint;
    private boolean mEnableNextOnConnection;
    private WifiManager$ActionListener mForgetListener;
    private final Runnable mHideProgressBarRunnable;
    private boolean mIsRestricted;
    private String mOpenSsid;
    private View mProgressHeader;
    private WifiManager$ActionListener mSaveListener;
    private Preference mSavedNetworksPreference;
    private AccessPoint mSelectedAccessPoint;
    private LinkablePreference mStatusMessagePreference;
    private final Runnable mUpdateAccessPointsRunnable;
    private AccessPointPreference.UserBadgeCache mUserBadgeCache;
    private WifiEnabler mWifiEnabler;
    protected WifiManager mWifiManager;
    private Bundle mWifiNfcDialogSavedState;
    private WriteWifiConfigToNfcDialog mWifiToNfcDialog;
    private WifiTracker mWifiTracker;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
                final Resources resources = context.getResources();
                if (resources.getBoolean(2131034176)) {
                    final SearchIndexableRaw searchIndexableRaw = new SearchIndexableRaw(context);
                    searchIndexableRaw.title = resources.getString(2131890112);
                    searchIndexableRaw.screenTitle = resources.getString(2131890112);
                    searchIndexableRaw.keywords = resources.getString(2131888002);
                    searchIndexableRaw.key = "main_toggle_wifi";
                    list.add(searchIndexableRaw);
                }
                return list;
            }
        };
        SUMMARY_PROVIDER_FACTORY = new SummaryLoader.SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new WifiSettings.SummaryProvider((Context)activity, summaryLoader);
            }
        };
    }
    
    public WifiSettings() {
        super("no_config_wifi");
        this.mUpdateAccessPointsRunnable = new _$$Lambda$WifiSettings$Dc8tARLt9797q5fiCWMG56ysJZ4(this);
        this.mHideProgressBarRunnable = new _$$Lambda$WifiSettings$ojra5gZ2Zt1OL2cVDalsbhFOQY0(this);
    }
    
    private void addConnectedAccessPointPreference(final AccessPoint accessPoint) {
        final ConnectedAccessPointPreference connectedAccessPointPreference = this.createConnectedAccessPointPreference(accessPoint);
        this.registerCaptivePortalNetworkCallback(this.getCurrentWifiNetwork(), connectedAccessPointPreference);
        connectedAccessPointPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$WifiSettings$xBxQqI4PVRSANGJ1NAFPK4yzyyw(this, connectedAccessPointPreference));
        connectedAccessPointPreference.setOnGearClickListener((ConnectedAccessPointPreference.OnGearClickListener)new _$$Lambda$WifiSettings$gxNoP_iqTz6xulv3o7cQv7agDKI(this, connectedAccessPointPreference));
        connectedAccessPointPreference.refresh();
        this.mConnectedAccessPointPreferenceCategory.addPreference(connectedAccessPointPreference);
        this.mConnectedAccessPointPreferenceCategory.setVisible(true);
        if (this.mClickedConnect) {
            this.mClickedConnect = false;
            this.scrollToPreference(this.mConnectedAccessPointPreferenceCategory);
        }
    }
    
    private void addMessagePreference(final int title) {
        this.mStatusMessagePreference.setTitle(title);
        this.removeConnectedAccessPointPreference();
        this.mAccessPointsPreferenceCategory.removeAll();
        this.mAccessPointsPreferenceCategory.addPreference(this.mStatusMessagePreference);
    }
    
    private void addPreferences() {
        this.addPreferencesFromResource(2132082873);
        this.mConnectedAccessPointPreferenceCategory = (PreferenceCategory)this.findPreference("connected_access_point");
        this.mAccessPointsPreferenceCategory = (PreferenceCategory)this.findPreference("access_points");
        this.mAdditionalSettingsPreferenceCategory = (PreferenceCategory)this.findPreference("additional_settings");
        this.mConfigureWifiSettingsPreference = this.findPreference("configure_settings");
        this.mSavedNetworksPreference = this.findPreference("saved_networks");
        final Context prefContext = this.getPrefContext();
        (this.mAddPreference = new Preference(prefContext)).setIcon(2131231065);
        this.mAddPreference.setTitle(2131889889);
        this.mStatusMessagePreference = new LinkablePreference(prefContext);
        this.mUserBadgeCache = new AccessPointPreference.UserBadgeCache(this.getPackageManager());
    }
    
    private void changeNextButtonState(final boolean enabled) {
        if (this.mEnableNextOnConnection && this.hasNextButton()) {
            this.getNextButton().setEnabled(enabled);
        }
    }
    
    private boolean configureConnectedAccessPointPreferenceCategory(final List<AccessPoint> list) {
        if (list.size() == 0) {
            this.removeConnectedAccessPointPreference();
            return false;
        }
        final AccessPoint accessPoint = list.get(0);
        if (!accessPoint.isActive()) {
            this.removeConnectedAccessPointPreference();
            return false;
        }
        if (this.mConnectedAccessPointPreferenceCategory.getPreferenceCount() == 0) {
            this.addConnectedAccessPointPreference(accessPoint);
            return true;
        }
        final ConnectedAccessPointPreference connectedAccessPointPreference = (ConnectedAccessPointPreference)this.mConnectedAccessPointPreferenceCategory.getPreference(0);
        if (connectedAccessPointPreference.getAccessPoint() != accessPoint) {
            this.removeConnectedAccessPointPreference();
            this.addConnectedAccessPointPreference(accessPoint);
            return true;
        }
        connectedAccessPointPreference.refresh();
        this.registerCaptivePortalNetworkCallback(this.getCurrentWifiNetwork(), connectedAccessPointPreference);
        return true;
    }
    
    private ConnectedAccessPointPreference createConnectedAccessPointPreference(final AccessPoint accessPoint) {
        return new ConnectedAccessPointPreference(accessPoint, this.getPrefContext(), this.mUserBadgeCache, 2131231198, false);
    }
    
    private LongPressAccessPointPreference createLongPressAccessPointPreference(final AccessPoint accessPoint) {
        return new LongPressAccessPointPreference(accessPoint, this.getPrefContext(), this.mUserBadgeCache, false, 2131231198, this);
    }
    
    private WifiEnabler createWifiEnabler() {
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        return new WifiEnabler((Context)settingsActivity, new SwitchBarController(settingsActivity.getSwitchBar()), this.mMetricsFeatureProvider);
    }
    
    private Network getCurrentWifiNetwork() {
        Network currentNetwork;
        if (this.mWifiManager != null) {
            currentNetwork = this.mWifiManager.getCurrentNetwork();
        }
        else {
            currentNetwork = null;
        }
        return currentNetwork;
    }
    
    private static boolean isDisabledByWrongPassword(final AccessPoint accessPoint) {
        final WifiConfiguration config = accessPoint.getConfig();
        boolean b = false;
        if (config == null) {
            return false;
        }
        final WifiConfiguration$NetworkSelectionStatus networkSelectionStatus = config.getNetworkSelectionStatus();
        if (networkSelectionStatus != null && !networkSelectionStatus.isNetworkEnabled()) {
            if (13 == networkSelectionStatus.getNetworkSelectionDisableReason()) {
                b = true;
            }
            return b;
        }
        return false;
    }
    
    private static boolean isVerboseLoggingEnabled() {
        return WifiTracker.sVerboseLogging || Log.isLoggable("WifiSettings", 2);
    }
    
    private boolean isWifiWakeupEnabled() {
        final PowerManager powerManager = (PowerManager)this.getSystemService("power");
        final ContentResolver contentResolver = this.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "wifi_wakeup_enabled", 0) == 1 && Settings.Global.getInt(contentResolver, "wifi_scan_always_enabled", 0) == 1 && Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) == 0 && !powerManager.isPowerSaveMode()) {
            b = true;
        }
        return b;
    }
    
    private void launchNetworkDetailsFragment(final ConnectedAccessPointPreference connectedAccessPointPreference) {
        new SubSettingLauncher(this.getContext()).setTitle(2131888621).setDestination(WifiNetworkDetailsFragment.class.getName()).setArguments(connectedAccessPointPreference.getExtras()).setSourceMetricsCategory(this.getMetricsCategory()).launch();
    }
    
    private void registerCaptivePortalNetworkCallback(final Network network, final ConnectedAccessPointPreference connectedAccessPointPreference) {
        if (network == null || connectedAccessPointPreference == null) {
            Log.w("WifiSettings", "Network or Preference were null when registering callback.");
            return;
        }
        if (this.mCaptivePortalNetworkCallback != null && this.mCaptivePortalNetworkCallback.isSameNetworkAndPreference(network, connectedAccessPointPreference)) {
            return;
        }
        this.unregisterCaptivePortalNetworkCallback();
        this.mCaptivePortalNetworkCallback = new CaptivePortalNetworkCallback(network, connectedAccessPointPreference);
        this.mConnectivityManager.registerNetworkCallback(new NetworkRequest$Builder().clearCapabilities().addTransportType(1).build(), (ConnectivityManager$NetworkCallback)this.mCaptivePortalNetworkCallback, new Handler(Looper.getMainLooper()));
    }
    
    private void removeConnectedAccessPointPreference() {
        this.mConnectedAccessPointPreferenceCategory.removeAll();
        this.mConnectedAccessPointPreferenceCategory.setVisible(false);
        this.unregisterCaptivePortalNetworkCallback();
    }
    
    private void restrictUi() {
        if (!this.isUiRestrictedByOnlyAdmin()) {
            this.getEmptyTextView().setText(2131889990);
        }
        this.getPreferenceScreen().removeAll();
    }
    
    private void setAdditionalSettingsSummaries() {
        this.mAdditionalSettingsPreferenceCategory.addPreference(this.mConfigureWifiSettingsPreference);
        final Preference mConfigureWifiSettingsPreference = this.mConfigureWifiSettingsPreference;
        int n;
        if (this.isWifiWakeupEnabled()) {
            n = 2131889937;
        }
        else {
            n = 2131889936;
        }
        mConfigureWifiSettingsPreference.setSummary(this.getString(n));
        final int numSavedNetworks = this.mWifiTracker.getNumSavedNetworks();
        if (numSavedNetworks > 0) {
            this.mAdditionalSettingsPreferenceCategory.addPreference(this.mSavedNetworksPreference);
            this.mSavedNetworksPreference.setSummary(this.getResources().getQuantityString(2131755072, numSavedNetworks, new Object[] { numSavedNetworks }));
        }
        else {
            this.mAdditionalSettingsPreferenceCategory.removePreference(this.mSavedNetworksPreference);
        }
    }
    
    private void setOffMessage() {
        final CharSequence text = this.getText(2131889991);
        final int int1 = Settings.Global.getInt(this.getActivity().getContentResolver(), "wifi_scan_always_enabled", 0);
        boolean b = true;
        if (int1 != 1) {
            b = false;
        }
        CharSequence charSequence;
        if (b) {
            charSequence = this.getText(2131890091);
        }
        else {
            charSequence = this.getText(2131890092);
        }
        this.mStatusMessagePreference.setText(text, charSequence, new _$$Lambda$WifiSettings$G0_vWzmi3g45SjhkhuPVMzYpO5w(this));
        this.removeConnectedAccessPointPreference();
        this.mAccessPointsPreferenceCategory.removeAll();
        this.mAccessPointsPreferenceCategory.addPreference(this.mStatusMessagePreference);
    }
    
    private void showDialog(final AccessPoint mDlgAccessPoint, final int mDialogMode) {
        if (mDlgAccessPoint != null && WifiUtils.isNetworkLockedDown((Context)this.getActivity(), mDlgAccessPoint.getConfig()) && mDlgAccessPoint.isActive()) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this.getActivity(), RestrictedLockUtils.getDeviceOwner((Context)this.getActivity()));
            return;
        }
        if (this.mDialog != null) {
            this.removeDialog(1);
            this.mDialog = null;
        }
        this.mDlgAccessPoint = mDlgAccessPoint;
        this.mDialogMode = mDialogMode;
        this.showDialog(1);
    }
    
    private void unregisterCaptivePortalNetworkCallback() {
        if (this.mCaptivePortalNetworkCallback != null) {
            try {
                this.mConnectivityManager.unregisterNetworkCallback((ConnectivityManager$NetworkCallback)this.mCaptivePortalNetworkCallback);
            }
            catch (RuntimeException ex) {
                Log.e("WifiSettings", "Unregistering CaptivePortalNetworkCallback failed.", (Throwable)ex);
            }
            this.mCaptivePortalNetworkCallback = null;
        }
    }
    
    private void updateAccessPointPreferences() {
        if (!this.mWifiManager.isWifiEnabled()) {
            return;
        }
        final List<AccessPoint> accessPoints = this.mWifiTracker.getAccessPoints();
        if (isVerboseLoggingEnabled()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("updateAccessPoints called for: ");
            sb.append(accessPoints);
            Log.i("WifiSettings", sb.toString());
        }
        boolean b = false;
        this.mAccessPointsPreferenceCategory.removePreference(this.mStatusMessagePreference);
        this.cacheRemoveAllPrefs(this.mAccessPointsPreferenceCategory);
        int i;
        for (i = (this.configureConnectedAccessPointPreferenceCategory(accessPoints) ? 1 : 0); i < accessPoints.size(); ++i) {
            final AccessPoint accessPoint = accessPoints.get(i);
            if (accessPoint.isReachable()) {
                final String key = accessPoint.getKey();
                b = true;
                final LongPressAccessPointPreference longPressAccessPointPreference = (LongPressAccessPointPreference)this.getCachedPreference(key);
                if (longPressAccessPointPreference != null) {
                    longPressAccessPointPreference.setOrder(i);
                }
                else {
                    final LongPressAccessPointPreference longPressAccessPointPreference2 = this.createLongPressAccessPointPreference(accessPoint);
                    longPressAccessPointPreference2.setKey(key);
                    longPressAccessPointPreference2.setOrder(i);
                    if (this.mOpenSsid != null && this.mOpenSsid.equals(accessPoint.getSsidStr()) && accessPoint.getSecurity() != 0 && (!accessPoint.isSaved() || isDisabledByWrongPassword(accessPoint))) {
                        this.onPreferenceTreeClick(longPressAccessPointPreference2);
                        this.mOpenSsid = null;
                    }
                    this.mAccessPointsPreferenceCategory.addPreference(longPressAccessPointPreference2);
                    accessPoint.setListener((AccessPoint.AccessPointListener)this);
                    longPressAccessPointPreference2.refresh();
                }
            }
        }
        this.removeCachedPrefs(this.mAccessPointsPreferenceCategory);
        this.mAddPreference.setOrder(i);
        this.mAccessPointsPreferenceCategory.addPreference(this.mAddPreference);
        this.setAdditionalSettingsSummaries();
        if (!b) {
            this.setProgressBarVisible(true);
            final Preference preference = new Preference(this.getPrefContext());
            preference.setSelectable(false);
            preference.setSummary(2131889992);
            preference.setOrder(i);
            preference.setKey("wifi_empty_list");
            this.mAccessPointsPreferenceCategory.addPreference(preference);
        }
        else {
            this.getView().postDelayed(this.mHideProgressBarRunnable, 1700L);
        }
    }
    
    private void updateAccessPointsDelayed() {
        if (this.getActivity() != null && !this.mIsRestricted && this.mWifiManager.isWifiEnabled()) {
            final View view = this.getView();
            final Handler handler = view.getHandler();
            if (handler != null && handler.hasCallbacks(this.mUpdateAccessPointsRunnable)) {
                return;
            }
            this.setProgressBarVisible(true);
            view.postDelayed(this.mUpdateAccessPointsRunnable, 300L);
        }
    }
    
    protected void connect(final WifiConfiguration wifiConfiguration, final boolean b) {
        this.mMetricsFeatureProvider.action(this.getVisibilityLogger(), 135, b);
        this.mWifiManager.connect(wifiConfiguration, this.mConnectListener);
        this.mClickedConnect = true;
    }
    
    void forget() {
        this.mMetricsFeatureProvider.action((Context)this.getActivity(), 137, (Pair<Integer, Object>[])new Pair[0]);
        if (!this.mSelectedAccessPoint.isSaved()) {
            if (this.mSelectedAccessPoint.getNetworkInfo() == null || this.mSelectedAccessPoint.getNetworkInfo().getState() == NetworkInfo.State.DISCONNECTED) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to forget invalid network ");
                sb.append(this.mSelectedAccessPoint.getConfig());
                Log.e("WifiSettings", sb.toString());
                return;
            }
            this.mWifiManager.disableEphemeralNetwork(AccessPoint.convertToQuotedString(this.mSelectedAccessPoint.getSsidStr()));
        }
        else if (this.mSelectedAccessPoint.getConfig().isPasspoint()) {
            this.mWifiManager.removePasspointConfiguration(this.mSelectedAccessPoint.getConfig().FQDN);
        }
        else {
            this.mWifiManager.forget(this.mSelectedAccessPoint.getConfig().networkId, this.mForgetListener);
        }
        this.mWifiTracker.resumeScanning();
        this.changeNextButtonState(false);
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        if (n == 1) {
            return 603;
        }
        if (n != 6) {
            return 0;
        }
        return 606;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887840;
    }
    
    @Override
    public int getMetricsCategory() {
        return 103;
    }
    
    @Override
    public void onAccessPointChanged(final AccessPoint accessPoint) {
        Log.d("WifiSettings", "onAccessPointChanged (singular) callback initiated");
        final View view = this.getView();
        if (view != null) {
            view.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    final Object tag = accessPoint.getTag();
                    if (tag != null) {
                        ((AccessPointPreference)tag).refresh();
                    }
                }
            });
        }
    }
    
    @Override
    public void onAccessPointsChanged() {
        Log.d("WifiSettings", "onAccessPointsChanged (WifiTracker) callback initiated");
        this.updateAccessPointsDelayed();
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.mWifiTracker = WifiTrackerFactory.create((Context)this.getActivity(), this, this.getLifecycle(), true, true);
        this.mWifiManager = this.mWifiTracker.getManager();
        if (this.getActivity() != null) {
            this.mConnectivityManager = (ConnectivityManager)this.getActivity().getSystemService((Class)ConnectivityManager.class);
        }
        this.mConnectListener = (WifiManager$ActionListener)new WifiManager$ActionListener() {
            public void onFailure(final int n) {
                final Activity activity = WifiSettings.this.getActivity();
                if (activity != null) {
                    Toast.makeText((Context)activity, 2131889995, 0).show();
                }
            }
            
            public void onSuccess() {
            }
        };
        this.mSaveListener = (WifiManager$ActionListener)new WifiManager$ActionListener() {
            public void onFailure(final int n) {
                final Activity activity = WifiSettings.this.getActivity();
                if (activity != null) {
                    Toast.makeText((Context)activity, 2131889997, 0).show();
                }
            }
            
            public void onSuccess() {
            }
        };
        this.mForgetListener = (WifiManager$ActionListener)new WifiManager$ActionListener() {
            public void onFailure(final int n) {
                final Activity activity = WifiSettings.this.getActivity();
                if (activity != null) {
                    Toast.makeText((Context)activity, 2131889996, 0).show();
                }
            }
            
            public void onSuccess() {
            }
        };
        if (bundle != null) {
            this.mDialogMode = bundle.getInt("dialog_mode");
            if (bundle.containsKey("wifi_ap_state")) {
                this.mAccessPointSavedState = bundle.getBundle("wifi_ap_state");
            }
            if (bundle.containsKey("wifi_nfc_dlg_state")) {
                this.mWifiNfcDialogSavedState = bundle.getBundle("wifi_nfc_dlg_state");
            }
        }
        final Intent intent = this.getActivity().getIntent();
        this.mEnableNextOnConnection = intent.getBooleanExtra("wifi_enable_next_on_connect", false);
        if (this.mEnableNextOnConnection && this.hasNextButton()) {
            final ConnectivityManager connectivityManager = (ConnectivityManager)this.getActivity().getSystemService("connectivity");
            if (connectivityManager != null) {
                this.changeNextButtonState(connectivityManager.getNetworkInfo(1).isConnected());
            }
        }
        this.registerForContextMenu((View)this.getListView());
        this.setHasOptionsMenu(true);
        if (intent.hasExtra("wifi_start_connect_ssid")) {
            this.mOpenSsid = intent.getStringExtra("wifi_start_connect_ssid");
        }
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        final boolean mIsRestricted = this.mIsRestricted;
        this.mIsRestricted = this.isUiRestricted();
        if (mIsRestricted && !this.mIsRestricted && this.getPreferenceScreen().getPreferenceCount() == 0) {
            this.addPreferences();
        }
    }
    
    void onAddNetworkPressed() {
        this.mMetricsFeatureProvider.action((Context)this.getActivity(), 134, (Pair<Integer, Object>[])new Pair[0]);
        this.showDialog(this.mSelectedAccessPoint = null, 1);
    }
    
    @Override
    public void onConnectedChanged() {
        this.changeNextButtonState(this.mWifiTracker.isConnected());
    }
    
    public boolean onContextItemSelected(final MenuItem menuItem) {
        if (this.mSelectedAccessPoint == null) {
            return super.onContextItemSelected(menuItem);
        }
        switch (menuItem.getItemId()) {
            default: {
                return super.onContextItemSelected(menuItem);
            }
            case 10: {
                this.showDialog(6);
                return true;
            }
            case 9: {
                this.showDialog(this.mSelectedAccessPoint, 2);
                return true;
            }
            case 8: {
                this.forget();
                return true;
            }
            case 7: {
                final boolean saved = this.mSelectedAccessPoint.isSaved();
                if (saved) {
                    this.connect(this.mSelectedAccessPoint.getConfig(), saved);
                }
                else if (this.mSelectedAccessPoint.getSecurity() == 0) {
                    this.mSelectedAccessPoint.generateOpenNetworkConfig();
                    this.connect(this.mSelectedAccessPoint.getConfig(), saved);
                }
                else {
                    this.showDialog(this.mSelectedAccessPoint, 1);
                }
                return true;
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setAnimationAllowed(false);
        this.addPreferences();
        this.mIsRestricted = this.isUiRestricted();
    }
    
    public void onCreateContextMenu(final ContextMenu contextMenu, final View view, final ContextMenu$ContextMenuInfo contextMenu$ContextMenuInfo) {
        final Preference preference = (Preference)view.getTag();
        if (preference instanceof LongPressAccessPointPreference) {
            this.mSelectedAccessPoint = ((LongPressAccessPointPreference)preference).getAccessPoint();
            contextMenu.setHeaderTitle(this.mSelectedAccessPoint.getSsid());
            if (this.mSelectedAccessPoint.isConnectable()) {
                contextMenu.add(0, 7, 0, 2131890039);
            }
            if (WifiUtils.isNetworkLockedDown((Context)this.getActivity(), this.mSelectedAccessPoint.getConfig())) {
                return;
            }
            if (this.mSelectedAccessPoint.isSaved() || this.mSelectedAccessPoint.isEphemeral()) {
                contextMenu.add(0, 8, 0, 2131890040);
            }
            if (this.mSelectedAccessPoint.isSaved()) {
                contextMenu.add(0, 9, 0, 2131890041);
                final NfcAdapter defaultAdapter = NfcAdapter.getDefaultAdapter((Context)this.getActivity());
                if (defaultAdapter != null && defaultAdapter.isEnabled() && this.mSelectedAccessPoint.getSecurity() != 0) {
                    contextMenu.add(0, 10, 0, 2131890046);
                }
            }
        }
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (n == 1) {
            if (this.mDlgAccessPoint == null && this.mAccessPointSavedState == null) {
                this.mDialog = WifiDialog.createFullscreen((Context)this.getActivity(), (WifiDialog.WifiDialogListener)this, this.mDlgAccessPoint, this.mDialogMode);
            }
            else {
                if (this.mDlgAccessPoint == null) {
                    this.mDlgAccessPoint = new AccessPoint((Context)this.getActivity(), this.mAccessPointSavedState);
                    this.mAccessPointSavedState = null;
                }
                this.mDialog = WifiDialog.createModal((Context)this.getActivity(), (WifiDialog.WifiDialogListener)this, this.mDlgAccessPoint, this.mDialogMode);
            }
            this.mSelectedAccessPoint = this.mDlgAccessPoint;
            return (Dialog)this.mDialog;
        }
        if (n != 6) {
            return super.onCreateDialog(n);
        }
        if (this.mSelectedAccessPoint != null) {
            this.mWifiToNfcDialog = new WriteWifiConfigToNfcDialog((Context)this.getActivity(), this.mSelectedAccessPoint.getSecurity());
        }
        else if (this.mWifiNfcDialogSavedState != null) {
            this.mWifiToNfcDialog = new WriteWifiConfigToNfcDialog((Context)this.getActivity(), this.mWifiNfcDialogSavedState);
        }
        return (Dialog)this.mWifiToNfcDialog;
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.mWifiEnabler != null) {
            this.mWifiEnabler.teardownSwitchController();
        }
    }
    
    @Override
    public void onForget(final WifiDialog wifiDialog) {
        this.forget();
    }
    
    @Override
    public void onLevelChanged(final AccessPoint accessPoint) {
        ((AccessPointPreference)accessPoint.getTag()).onLevelChanged();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (this.mWifiEnabler != null) {
            this.mWifiEnabler.pause();
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference.getFragment() != null) {
            preference.setOnPreferenceClickListener(null);
            return super.onPreferenceTreeClick(preference);
        }
        if (preference instanceof LongPressAccessPointPreference) {
            this.mSelectedAccessPoint = ((LongPressAccessPointPreference)preference).getAccessPoint();
            if (this.mSelectedAccessPoint == null) {
                return false;
            }
            if (this.mSelectedAccessPoint.isActive()) {
                return super.onPreferenceTreeClick(preference);
            }
            final WifiConfiguration config = this.mSelectedAccessPoint.getConfig();
            if (this.mSelectedAccessPoint.getSecurity() == 0) {
                this.mSelectedAccessPoint.generateOpenNetworkConfig();
                this.connect(this.mSelectedAccessPoint.getConfig(), this.mSelectedAccessPoint.isSaved());
            }
            else if (this.mSelectedAccessPoint.isSaved() && config != null && config.getNetworkSelectionStatus() != null && config.getNetworkSelectionStatus().getHasEverConnected()) {
                this.connect(config, true);
            }
            else if (this.mSelectedAccessPoint.isPasspoint()) {
                this.connect(config, true);
            }
            else {
                this.showDialog(this.mSelectedAccessPoint, 1);
            }
        }
        else {
            if (preference != this.mAddPreference) {
                return super.onPreferenceTreeClick(preference);
            }
            this.onAddNetworkPressed();
        }
        return true;
    }
    
    @Override
    public void onResume() {
        final Activity activity = this.getActivity();
        super.onResume();
        final boolean mIsRestricted = this.mIsRestricted;
        this.mIsRestricted = this.isUiRestricted();
        if (!mIsRestricted && this.mIsRestricted) {
            this.restrictUi();
        }
        if (this.mWifiEnabler != null) {
            this.mWifiEnabler.resume((Context)activity);
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mDialog != null && this.mDialog.isShowing()) {
            bundle.putInt("dialog_mode", this.mDialogMode);
            if (this.mDlgAccessPoint != null) {
                this.mAccessPointSavedState = new Bundle();
                this.mDlgAccessPoint.saveWifiState(this.mAccessPointSavedState);
                bundle.putBundle("wifi_ap_state", this.mAccessPointSavedState);
            }
        }
        if (this.mWifiToNfcDialog != null && this.mWifiToNfcDialog.isShowing()) {
            final Bundle bundle2 = new Bundle();
            this.mWifiToNfcDialog.saveState(bundle2);
            bundle.putBundle("wifi_nfc_dlg_state", bundle2);
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.mWifiEnabler = this.createWifiEnabler();
        if (this.mIsRestricted) {
            this.restrictUi();
            return;
        }
        this.onWifiStateChanged(this.mWifiManager.getWifiState());
    }
    
    @Override
    public void onStop() {
        this.getView().removeCallbacks(this.mUpdateAccessPointsRunnable);
        this.getView().removeCallbacks(this.mHideProgressBarRunnable);
        this.unregisterCaptivePortalNetworkCallback();
        super.onStop();
    }
    
    @Override
    public void onSubmit(final WifiDialog wifiDialog) {
        if (this.mDialog != null) {
            this.submit(this.mDialog.getController());
        }
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        final Activity activity = this.getActivity();
        if (activity != null) {
            this.mProgressHeader = this.setPinnedHeaderView(2131558892).findViewById(2131362485);
            this.setProgressBarVisible(false);
        }
        ((SettingsActivity)activity).getSwitchBar().setSwitchBarText(2131890114, 2131890114);
    }
    
    @Override
    public void onWifiStateChanged(final int n) {
        if (this.mIsRestricted) {
            return;
        }
        switch (this.mWifiManager.getWifiState()) {
            case 3: {
                this.updateAccessPointPreferences();
                break;
            }
            case 2: {
                this.removeConnectedAccessPointPreference();
                this.mAccessPointsPreferenceCategory.removeAll();
                this.addMessagePreference(2131890160);
                this.setProgressBarVisible(true);
                break;
            }
            case 1: {
                this.setOffMessage();
                this.setAdditionalSettingsSummaries();
                this.setProgressBarVisible(false);
                break;
            }
            case 0: {
                this.removeConnectedAccessPointPreference();
                this.mAccessPointsPreferenceCategory.removeAll();
                this.addMessagePreference(2131890172);
                break;
            }
        }
    }
    
    protected void setProgressBarVisible(final boolean b) {
        if (this.mProgressHeader != null) {
            final View mProgressHeader = this.mProgressHeader;
            int visibility;
            if (b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            mProgressHeader.setVisibility(visibility);
        }
    }
    
    void submit(final WifiConfigController wifiConfigController) {
        final WifiConfiguration config = wifiConfigController.getConfig();
        if (config == null) {
            if (this.mSelectedAccessPoint != null && this.mSelectedAccessPoint.isSaved()) {
                this.connect(this.mSelectedAccessPoint.getConfig(), true);
            }
        }
        else if (wifiConfigController.getMode() == 2) {
            this.mWifiManager.save(config, this.mSaveListener);
        }
        else {
            this.mWifiManager.save(config, this.mSaveListener);
            if (this.mSelectedAccessPoint != null) {
                this.connect(config, false);
            }
        }
        this.mWifiTracker.resumeScanning();
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider, OnSummaryChangeListener
    {
        private final Context mContext;
        WifiSummaryUpdater mSummaryHelper;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
            this.mSummaryHelper = new WifiSummaryUpdater(this.mContext, this);
        }
        
        @Override
        public void onSummaryChanged(final String s) {
            this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, s);
        }
        
        @Override
        public void setListening(final boolean b) {
            this.mSummaryHelper.register(b);
        }
    }
}
