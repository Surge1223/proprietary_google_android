package com.android.settings.wifi;

import android.widget.AdapterView;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.CompoundButton;
import android.os.UserManager;
import android.security.KeyStore;
import android.net.IpConfiguration;
import android.widget.Button;
import com.android.settingslib.utils.ThreadUtils;
import android.text.Editable;
import java.net.UnknownHostException;
import android.net.LinkAddress;
import android.net.wifi.WifiEnterpriseConfig;
import java.util.Iterator;
import java.net.InetAddress;
import android.widget.SpinnerAdapter;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import android.net.Uri;
import android.text.TextUtils;
import com.android.settings.ProxySelector;
import android.net.NetworkUtils;
import java.net.Inet4Address;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiConfiguration;
import android.content.res.Resources;
import android.util.Log;
import android.net.NetworkInfo$DetailedState;
import android.view.ViewGroup;
import com.android.settingslib.Utils;
import android.view.View;
import android.net.StaticIpConfiguration;
import android.widget.CheckBox;
import android.net.IpConfiguration$ProxySettings;
import android.widget.ArrayAdapter;
import android.net.IpConfiguration$IpAssignment;
import android.net.ProxyInfo;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ScrollView;
import android.content.Context;
import com.android.settingslib.wifi.AccessPoint;
import android.widget.TextView$OnEditorActionListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.AdapterView$OnItemSelectedListener;
import android.view.View$OnKeyListener;
import android.text.TextWatcher;

public final class _$$Lambda$WifiConfigController$7dViR1XLVJsqzanUSq_nLAYeTK0 implements Runnable
{
    @Override
    public final void run() {
        WifiConfigController.lambda$onItemSelected$1(this.f$0);
    }
}
