package com.android.settings.wifi;

import android.text.TextUtils;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.android.settingslib.Utils;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.Slice;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.slices.SliceBroadcastReceiver;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;
import android.content.IntentFilter;
import androidx.slice.builders.ListBuilder;
import androidx.slice.builders.SliceAction;
import android.support.v4.util.Consumer;

public final class _$$Lambda$WifiSliceBuilder$zGyWboi_khe6O7kGcUmHExYnEzU implements Consumer
{
    @Override
    public final void accept(final Object o) {
        WifiSliceBuilder.lambda$getSlice$0(this.f$0, this.f$1, this.f$2, this.f$3, (ListBuilder.RowBuilder)o);
    }
}
