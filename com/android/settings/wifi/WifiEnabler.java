package com.android.settings.wifi;

import android.util.Pair;
import android.widget.Toast;
import com.android.settingslib.WirelessUtils;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.net.NetworkInfo$DetailedState;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.SupplicantState;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.IntentFilter;
import android.content.Context;
import android.net.ConnectivityManager;
import java.util.concurrent.atomic.AtomicBoolean;
import com.android.settings.widget.SwitchWidgetController;

public class WifiEnabler implements OnSwitchChangeListener
{
    private AtomicBoolean mConnected;
    private final ConnectivityManager mConnectivityManager;
    private Context mContext;
    private final IntentFilter mIntentFilter;
    private boolean mListeningToOnSwitchChange;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private final BroadcastReceiver mReceiver;
    private boolean mStateMachineEvent;
    private final SwitchWidgetController mSwitchWidget;
    private final WifiManager mWifiManager;
    
    public WifiEnabler(final Context context, final SwitchWidgetController switchWidgetController, final MetricsFeatureProvider metricsFeatureProvider) {
        this(context, switchWidgetController, metricsFeatureProvider, (ConnectivityManager)context.getSystemService("connectivity"));
    }
    
    WifiEnabler(final Context mContext, final SwitchWidgetController mSwitchWidget, final MetricsFeatureProvider mMetricsFeatureProvider, final ConnectivityManager mConnectivityManager) {
        this.mListeningToOnSwitchChange = false;
        this.mConnected = new AtomicBoolean(false);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.net.wifi.WIFI_STATE_CHANGED".equals(action)) {
                    WifiEnabler.this.handleWifiStateChanged(WifiEnabler.this.mWifiManager.getWifiState());
                }
                else if ("android.net.wifi.supplicant.STATE_CHANGE".equals(action)) {
                    if (!WifiEnabler.this.mConnected.get()) {
                        WifiEnabler.this.handleStateChanged(WifiInfo.getDetailedStateOf((SupplicantState)intent.getParcelableExtra("newState")));
                    }
                }
                else if ("android.net.wifi.STATE_CHANGE".equals(action)) {
                    final NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra("networkInfo");
                    WifiEnabler.this.mConnected.set(networkInfo.isConnected());
                    WifiEnabler.this.handleStateChanged(networkInfo.getDetailedState());
                }
            }
        };
        this.mContext = mContext;
        (this.mSwitchWidget = mSwitchWidget).setListener((SwitchWidgetController.OnSwitchChangeListener)this);
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        this.mWifiManager = (WifiManager)mContext.getSystemService("wifi");
        this.mConnectivityManager = mConnectivityManager;
        (this.mIntentFilter = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED")).addAction("android.net.wifi.supplicant.STATE_CHANGE");
        this.mIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
        this.setupSwitchController();
    }
    
    private void handleStateChanged(final NetworkInfo$DetailedState networkInfo$DetailedState) {
    }
    
    private void handleWifiStateChanged(final int n) {
        this.mSwitchWidget.setDisabledByAdmin(null);
        while (true) {
            switch (n) {
                default: {
                    this.setSwitchBarChecked(false);
                    this.mSwitchWidget.setEnabled(true);
                }
                case 0: {
                    if (RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_config_tethering", UserHandle.myUserId())) {
                        this.mSwitchWidget.setEnabled(false);
                    }
                    else {
                        this.mSwitchWidget.setDisabledByAdmin(RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_config_tethering", UserHandle.myUserId()));
                    }
                }
                case 3: {
                    this.setSwitchBarChecked(true);
                    this.mSwitchWidget.setEnabled(true);
                }
                case 2: {
                    continue;
                }
                case 1: {
                    this.setSwitchBarChecked(false);
                    this.mSwitchWidget.setEnabled(true);
                    continue;
                }
            }
            break;
        }
    }
    
    private void setSwitchBarChecked(final boolean checked) {
        this.mStateMachineEvent = true;
        this.mSwitchWidget.setChecked(checked);
        this.mStateMachineEvent = false;
    }
    
    @Override
    public boolean onSwitchToggled(final boolean wifiEnabled) {
        if (this.mStateMachineEvent) {
            return true;
        }
        if (wifiEnabled && !WirelessUtils.isRadioAllowed(this.mContext, "wifi")) {
            Toast.makeText(this.mContext, 2131890025, 0).show();
            this.mSwitchWidget.setChecked(false);
            return false;
        }
        if (wifiEnabled) {
            this.mMetricsFeatureProvider.action(this.mContext, 139, (Pair<Integer, Object>[])new Pair[0]);
        }
        else {
            this.mMetricsFeatureProvider.action(this.mContext, 138, this.mConnected.get());
        }
        if (!this.mWifiManager.setWifiEnabled(wifiEnabled)) {
            this.mSwitchWidget.setEnabled(true);
            Toast.makeText(this.mContext, 2131889993, 0).show();
        }
        return true;
    }
    
    public void pause() {
        this.mContext.unregisterReceiver(this.mReceiver);
        if (this.mListeningToOnSwitchChange) {
            this.mSwitchWidget.stopListening();
            this.mListeningToOnSwitchChange = false;
        }
    }
    
    public void resume(final Context mContext) {
        (this.mContext = mContext).registerReceiver(this.mReceiver, this.mIntentFilter);
        if (!this.mListeningToOnSwitchChange) {
            this.mSwitchWidget.startListening();
            this.mListeningToOnSwitchChange = true;
        }
    }
    
    public void setupSwitchController() {
        this.handleWifiStateChanged(this.mWifiManager.getWifiState());
        if (!this.mListeningToOnSwitchChange) {
            this.mSwitchWidget.startListening();
            this.mListeningToOnSwitchChange = true;
        }
        this.mSwitchWidget.setupView();
    }
    
    public void teardownSwitchController() {
        if (this.mListeningToOnSwitchChange) {
            this.mSwitchWidget.stopListening();
            this.mListeningToOnSwitchChange = false;
        }
        this.mSwitchWidget.teardownView();
    }
}
