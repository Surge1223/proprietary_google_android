package com.android.settings.wifi.details;

import com.android.settingslib.RestrictedLockUtils;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settings.wifi.WifiDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.net.ConnectivityManager;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settingslib.wifi.AccessPoint;
import com.android.settings.dashboard.DashboardFragment;

public class WifiNetworkDetailsFragment extends DashboardFragment
{
    private AccessPoint mAccessPoint;
    private WifiDetailPreferenceController mWifiDetailPreferenceController;
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<WifiMeteredPreferenceController> list = (ArrayList<WifiMeteredPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(this.mWifiDetailPreferenceController = WifiDetailPreferenceController.newInstance(this.mAccessPoint, (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class), context, this, new Handler(Looper.getMainLooper()), this.getLifecycle(), (WifiManager)context.getSystemService((Class)WifiManager.class), this.mMetricsFeatureProvider));
        list.add(new WifiMeteredPreferenceController(context, this.mAccessPoint.getConfig()));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        if (n == 1) {
            return 603;
        }
        return 0;
    }
    
    @Override
    protected String getLogTag() {
        return "WifiNetworkDetailsFrg";
    }
    
    @Override
    public int getMetricsCategory() {
        return 849;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082871;
    }
    
    @Override
    public void onAttach(final Context context) {
        this.mAccessPoint = new AccessPoint(context, this.getArguments());
        super.onAttach(context);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (this.getActivity() != null && this.mWifiDetailPreferenceController != null && this.mAccessPoint != null) {
            return (Dialog)WifiDialog.createModal((Context)this.getActivity(), (WifiDialog.WifiDialogListener)this.mWifiDetailPreferenceController, this.mAccessPoint, 2);
        }
        return null;
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final MenuItem add = menu.add(0, 1, 0, 2131890048);
        add.setIcon(2131231069);
        add.setShowAsAction(2);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 1) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (!this.mWifiDetailPreferenceController.canModifyNetwork()) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.getContext(), RestrictedLockUtils.getDeviceOwner(this.getContext()));
        }
        else {
            this.showDialog(1);
        }
        return true;
    }
}
