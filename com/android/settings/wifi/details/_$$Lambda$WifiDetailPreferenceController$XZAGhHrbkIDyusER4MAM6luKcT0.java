package com.android.settings.wifi.details;

import android.app.Activity;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.text.TextUtils;
import java.util.Iterator;
import android.support.v4.text.BidiFormatter;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import android.net.RouteInfo;
import java.net.Inet6Address;
import java.net.Inet4Address;
import android.net.LinkAddress;
import java.util.StringJoiner;
import android.widget.ImageView$ScaleType;
import android.widget.ImageView;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import android.graphics.drawable.Drawable;
import com.android.settingslib.Utils;
import android.view.View;
import java.net.UnknownHostException;
import android.net.NetworkUtils;
import android.util.Pair;
import android.net.wifi.WifiManager$ActionListener;
import com.android.settings.wifi.WifiUtils;
import android.net.NetworkRequest$Builder;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.util.Log;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiConfiguration;
import android.content.BroadcastReceiver;
import android.net.NetworkRequest;
import android.net.NetworkInfo;
import android.net.NetworkCapabilities;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.Network;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.net.LinkProperties;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.os.Handler;
import android.app.Fragment;
import android.content.IntentFilter;
import com.android.settings.widget.EntityHeaderController;
import com.android.settings.wifi.WifiDetailPreference;
import android.net.ConnectivityManager;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settingslib.wifi.AccessPoint;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.wifi.WifiDialog;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import java.net.InetAddress;
import java.util.function.Function;

public final class _$$Lambda$WifiDetailPreferenceController$XZAGhHrbkIDyusER4MAM6luKcT0 implements Function
{
    @Override
    public final Object apply(final Object o) {
        return ((InetAddress)o).getHostAddress();
    }
}
