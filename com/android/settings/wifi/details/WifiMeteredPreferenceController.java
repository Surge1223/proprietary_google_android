package com.android.settings.wifi.details;

import android.app.backup.BackupManager;
import android.support.v7.preference.DropDownPreference;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class WifiMeteredPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    private static final String KEY_WIFI_METERED = "metered";
    private WifiConfiguration mWifiConfiguration;
    private WifiManager mWifiManager;
    
    public WifiMeteredPreferenceController(final Context context, final WifiConfiguration mWifiConfiguration) {
        super(context, "metered");
        this.mWifiConfiguration = mWifiConfiguration;
        this.mWifiManager = (WifiManager)context.getSystemService("wifi");
    }
    
    private void updateSummary(final DropDownPreference dropDownPreference, final int n) {
        dropDownPreference.setSummary(dropDownPreference.getEntries()[n]);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    int getMeteredOverride() {
        if (this.mWifiConfiguration != null) {
            return this.mWifiConfiguration.meteredOverride;
        }
        return 0;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mWifiConfiguration != null) {
            this.mWifiConfiguration.meteredOverride = Integer.parseInt((String)o);
        }
        this.mWifiManager.updateNetwork(this.mWifiConfiguration);
        BackupManager.dataChanged("com.android.providers.settings");
        this.updateSummary((DropDownPreference)preference, this.getMeteredOverride());
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final DropDownPreference dropDownPreference = (DropDownPreference)preference;
        final int meteredOverride = this.getMeteredOverride();
        dropDownPreference.setValue(Integer.toString(meteredOverride));
        this.updateSummary(dropDownPreference, meteredOverride);
    }
}
