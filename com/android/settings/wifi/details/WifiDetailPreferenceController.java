package com.android.settings.wifi.details;

import android.app.Activity;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.text.TextUtils;
import java.util.Iterator;
import android.support.v4.text.BidiFormatter;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import android.net.RouteInfo;
import java.net.Inet6Address;
import java.net.Inet4Address;
import android.net.LinkAddress;
import java.util.StringJoiner;
import android.widget.ImageView$ScaleType;
import android.widget.ImageView;
import com.android.settings.applications.LayoutPreference;
import android.support.v7.preference.PreferenceScreen;
import android.graphics.drawable.Drawable;
import com.android.settingslib.Utils;
import android.view.View;
import java.net.UnknownHostException;
import android.net.NetworkUtils;
import java.net.InetAddress;
import android.util.Pair;
import android.net.wifi.WifiManager$ActionListener;
import com.android.settings.wifi.WifiUtils;
import android.net.NetworkRequest$Builder;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.util.Log;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiConfiguration;
import android.content.BroadcastReceiver;
import android.net.NetworkRequest;
import android.net.NetworkInfo;
import android.net.NetworkCapabilities;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.Network;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.net.LinkProperties;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.os.Handler;
import android.app.Fragment;
import android.content.IntentFilter;
import com.android.settings.widget.EntityHeaderController;
import com.android.settings.wifi.WifiDetailPreference;
import android.net.ConnectivityManager;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settingslib.wifi.AccessPoint;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.wifi.WifiDialog;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiDetailPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, WifiDialogListener, LifecycleObserver, OnPause, OnResume
{
    private static final boolean DEBUG;
    @VisibleForTesting
    static final String KEY_BUTTONS_PREF = "buttons";
    @VisibleForTesting
    static final String KEY_DNS_PREF = "dns";
    @VisibleForTesting
    static final String KEY_FREQUENCY_PREF = "frequency";
    @VisibleForTesting
    static final String KEY_GATEWAY_PREF = "gateway";
    @VisibleForTesting
    static final String KEY_HEADER = "connection_header";
    @VisibleForTesting
    static final String KEY_IPV6_ADDRESSES_PREF = "ipv6_addresses";
    @VisibleForTesting
    static final String KEY_IPV6_CATEGORY = "ipv6_category";
    @VisibleForTesting
    static final String KEY_IP_ADDRESS_PREF = "ip_address";
    @VisibleForTesting
    static final String KEY_LINK_SPEED = "link_speed";
    @VisibleForTesting
    static final String KEY_MAC_ADDRESS_PREF = "mac_address";
    @VisibleForTesting
    static final String KEY_SECURITY_PREF = "security";
    @VisibleForTesting
    static final String KEY_SIGNAL_STRENGTH_PREF = "signal_strength";
    @VisibleForTesting
    static final String KEY_SUBNET_MASK_PREF = "subnet_mask";
    private AccessPoint mAccessPoint;
    private ActionButtonPreference mButtonsPref;
    private final ConnectivityManager mConnectivityManager;
    private WifiDetailPreference mDnsPref;
    private EntityHeaderController mEntityHeaderController;
    private final IntentFilter mFilter;
    private final Fragment mFragment;
    private WifiDetailPreference mFrequencyPref;
    private WifiDetailPreference mGatewayPref;
    private final Handler mHandler;
    private final IconInjector mIconInjector;
    private WifiDetailPreference mIpAddressPref;
    private Preference mIpv6AddressPref;
    private PreferenceCategory mIpv6Category;
    private LinkProperties mLinkProperties;
    private WifiDetailPreference mLinkSpeedPref;
    private WifiDetailPreference mMacAddressPref;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    private Network mNetwork;
    private final ConnectivityManager$NetworkCallback mNetworkCallback;
    private NetworkCapabilities mNetworkCapabilities;
    private NetworkInfo mNetworkInfo;
    private final NetworkRequest mNetworkRequest;
    private final BroadcastReceiver mReceiver;
    private int mRssiSignalLevel;
    private WifiDetailPreference mSecurityPref;
    private String[] mSignalStr;
    private WifiDetailPreference mSignalStrengthPref;
    private WifiDetailPreference mSubnetPref;
    private WifiConfiguration mWifiConfig;
    private WifiInfo mWifiInfo;
    private final WifiManager mWifiManager;
    
    static {
        DEBUG = Log.isLoggable("WifiDetailsPrefCtrl", 3);
    }
    
    WifiDetailPreferenceController(final AccessPoint mAccessPoint, final ConnectivityManager mConnectivityManager, final Context context, final Fragment mFragment, final Handler mHandler, final Lifecycle lifecycle, final WifiManager mWifiManager, final MetricsFeatureProvider mMetricsFeatureProvider, final IconInjector mIconInjector) {
        super(context);
        this.mRssiSignalLevel = -1;
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                final int hashCode = action.hashCode();
                int n = 0;
                Label_0075: {
                    if (hashCode != -385684331) {
                        if (hashCode != -343630553) {
                            if (hashCode == 1625920338) {
                                if (action.equals("android.net.wifi.CONFIGURED_NETWORKS_CHANGE")) {
                                    n = 0;
                                    break Label_0075;
                                }
                            }
                        }
                        else if (action.equals("android.net.wifi.STATE_CHANGE")) {
                            n = 1;
                            break Label_0075;
                        }
                    }
                    else if (action.equals("android.net.wifi.RSSI_CHANGED")) {
                        n = 2;
                        break Label_0075;
                    }
                    n = -1;
                }
                Label_0150: {
                    switch (n) {
                        case 0: {
                            if (intent.getBooleanExtra("multipleChanges", false)) {
                                break Label_0150;
                            }
                            final WifiConfiguration wifiConfiguration = (WifiConfiguration)intent.getParcelableExtra("wifiConfiguration");
                            if (WifiDetailPreferenceController.this.mAccessPoint.matches(wifiConfiguration)) {
                                WifiDetailPreferenceController.this.mWifiConfig = wifiConfiguration;
                            }
                            break Label_0150;
                        }
                        case 1:
                        case 2: {
                            WifiDetailPreferenceController.this.updateInfo();
                            break;
                        }
                    }
                }
            }
        };
        this.mNetworkRequest = new NetworkRequest$Builder().clearCapabilities().addTransportType(1).build();
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            private boolean hasCapabilityChanged(final NetworkCapabilities networkCapabilities, final int n) {
                final NetworkCapabilities access$600 = WifiDetailPreferenceController.this.mNetworkCapabilities;
                boolean b = true;
                if (access$600 == null) {
                    return true;
                }
                if (WifiDetailPreferenceController.this.mNetworkCapabilities.hasCapability(n) == networkCapabilities.hasCapability(n)) {
                    b = false;
                }
                return b;
            }
            
            public void onCapabilitiesChanged(final Network network, final NetworkCapabilities networkCapabilities) {
                if (network.equals((Object)WifiDetailPreferenceController.this.mNetwork) && !networkCapabilities.equals((Object)WifiDetailPreferenceController.this.mNetworkCapabilities)) {
                    if (this.hasCapabilityChanged(networkCapabilities, 16) || this.hasCapabilityChanged(networkCapabilities, 17)) {
                        WifiDetailPreferenceController.this.refreshNetworkState();
                    }
                    WifiDetailPreferenceController.this.mNetworkCapabilities = networkCapabilities;
                    WifiDetailPreferenceController.this.updateIpLayerInfo();
                }
            }
            
            public void onLinkPropertiesChanged(final Network network, final LinkProperties linkProperties) {
                if (network.equals((Object)WifiDetailPreferenceController.this.mNetwork) && !linkProperties.equals((Object)WifiDetailPreferenceController.this.mLinkProperties)) {
                    WifiDetailPreferenceController.this.mLinkProperties = linkProperties;
                    WifiDetailPreferenceController.this.updateIpLayerInfo();
                }
            }
            
            public void onLost(final Network network) {
                if (network.equals((Object)WifiDetailPreferenceController.this.mNetwork)) {
                    WifiDetailPreferenceController.this.exitActivity();
                }
            }
        };
        this.mAccessPoint = mAccessPoint;
        this.mConnectivityManager = mConnectivityManager;
        this.mFragment = mFragment;
        this.mHandler = mHandler;
        this.mSignalStr = context.getResources().getStringArray(2130903198);
        this.mWifiConfig = mAccessPoint.getConfig();
        this.mWifiManager = mWifiManager;
        this.mMetricsFeatureProvider = mMetricsFeatureProvider;
        this.mIconInjector = mIconInjector;
        (this.mFilter = new IntentFilter()).addAction("android.net.wifi.STATE_CHANGE");
        this.mFilter.addAction("android.net.wifi.RSSI_CHANGED");
        this.mFilter.addAction("android.net.wifi.CONFIGURED_NETWORKS_CHANGE");
        lifecycle.addObserver(this);
    }
    
    private boolean canForgetNetwork() {
        return (this.mWifiInfo != null && this.mWifiInfo.isEphemeral()) || this.canModifyNetwork();
    }
    
    private boolean canSignIntoNetwork() {
        return WifiUtils.canSignIntoNetwork(this.mNetworkCapabilities);
    }
    
    private void exitActivity() {
        if (WifiDetailPreferenceController.DEBUG) {
            Log.d("WifiDetailsPrefCtrl", "Exiting the WifiNetworkDetailsPage");
        }
        this.mFragment.getActivity().finish();
    }
    
    private void forgetNetwork() {
        if (this.mWifiInfo != null && this.mWifiInfo.isEphemeral()) {
            this.mWifiManager.disableEphemeralNetwork(this.mWifiInfo.getSSID());
        }
        else if (this.mWifiConfig != null) {
            if (this.mWifiConfig.isPasspoint()) {
                this.mWifiManager.removePasspointConfiguration(this.mWifiConfig.FQDN);
            }
            else {
                this.mWifiManager.forget(this.mWifiConfig.networkId, (WifiManager$ActionListener)null);
            }
        }
        this.mMetricsFeatureProvider.action((Context)this.mFragment.getActivity(), 137, (Pair<Integer, Object>[])new Pair[0]);
        this.mFragment.getActivity().finish();
    }
    
    private static String ipv4PrefixLengthToSubnetMask(final int n) {
        try {
            return NetworkUtils.getNetworkPart(InetAddress.getByAddress(new byte[] { -1, -1, -1, -1 }), n).getHostAddress();
        }
        catch (UnknownHostException ex) {
            return null;
        }
    }
    
    public static WifiDetailPreferenceController newInstance(final AccessPoint accessPoint, final ConnectivityManager connectivityManager, final Context context, final Fragment fragment, final Handler handler, final Lifecycle lifecycle, final WifiManager wifiManager, final MetricsFeatureProvider metricsFeatureProvider) {
        return new WifiDetailPreferenceController(accessPoint, connectivityManager, context, fragment, handler, lifecycle, wifiManager, metricsFeatureProvider, new IconInjector(context));
    }
    
    private void refreshNetworkState() {
        this.mAccessPoint.update(this.mWifiConfig, this.mWifiInfo, this.mNetworkInfo);
        this.mEntityHeaderController.setSummary(this.mAccessPoint.getSettingsSummary()).done(this.mFragment.getActivity(), true);
    }
    
    private void refreshRssiViews() {
        final int level = this.mAccessPoint.getLevel();
        if (this.mRssiSignalLevel == level) {
            return;
        }
        this.mRssiSignalLevel = level;
        final Drawable icon = this.mIconInjector.getIcon(this.mRssiSignalLevel);
        icon.setTint(Utils.getColorAccent(this.mContext));
        this.mEntityHeaderController.setIcon(icon).done(this.mFragment.getActivity(), true);
        final Drawable mutate = icon.getConstantState().newDrawable().mutate();
        mutate.setTint(this.mContext.getResources().getColor(2131099889, this.mContext.getTheme()));
        this.mSignalStrengthPref.setIcon(mutate);
        this.mSignalStrengthPref.setDetailText(this.mSignalStr[this.mRssiSignalLevel]);
    }
    
    private void setupEntityHeader(final PreferenceScreen preferenceScreen) {
        final LayoutPreference layoutPreference = (LayoutPreference)preferenceScreen.findPreference("connection_header");
        this.mEntityHeaderController = EntityHeaderController.newInstance(this.mFragment.getActivity(), this.mFragment, layoutPreference.findViewById(2131362112));
        final ImageView imageView = layoutPreference.findViewById(2131362114);
        imageView.setBackground(this.mContext.getDrawable(2131231145));
        imageView.setScaleType(ImageView$ScaleType.CENTER_INSIDE);
        this.mEntityHeaderController.setLabel(this.mAccessPoint.getSsidStr());
    }
    
    private void signIntoNetwork() {
        this.mMetricsFeatureProvider.action((Context)this.mFragment.getActivity(), 1008, (Pair<Integer, Object>[])new Pair[0]);
        this.mConnectivityManager.startCaptivePortalApp(this.mNetwork);
    }
    
    private void updateInfo() {
        this.mNetworkInfo = this.mConnectivityManager.getNetworkInfo(this.mNetwork);
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
        if (this.mNetwork != null && this.mNetworkInfo != null && this.mWifiInfo != null) {
            this.mButtonsPref.setButton1Visible(this.canForgetNetwork());
            this.refreshNetworkState();
            this.refreshRssiViews();
            this.mMacAddressPref.setDetailText(this.mWifiInfo.getMacAddress());
            this.mLinkSpeedPref.setVisible(this.mWifiInfo.getLinkSpeed() >= 0);
            this.mLinkSpeedPref.setDetailText(this.mContext.getString(2131888026, new Object[] { this.mWifiInfo.getLinkSpeed() }));
            final int frequency = this.mWifiInfo.getFrequency();
            String detailText = null;
            if (frequency >= 2400 && frequency < 2500) {
                detailText = this.mContext.getResources().getString(2131889911);
            }
            else if (frequency >= 4900 && frequency < 5900) {
                detailText = this.mContext.getResources().getString(2131889912);
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected frequency ");
                sb.append(frequency);
                Log.e("WifiDetailsPrefCtrl", sb.toString());
            }
            this.mFrequencyPref.setDetailText(detailText);
            this.updateIpLayerInfo();
            return;
        }
        this.exitActivity();
    }
    
    private void updateIpLayerInfo() {
        this.mButtonsPref.setButton2Visible(this.canSignIntoNetwork());
        this.mButtonsPref.setVisible(this.canSignIntoNetwork() || this.canForgetNetwork());
        if (this.mNetwork != null && this.mLinkProperties != null) {
            String s = null;
            String s2 = null;
            final StringJoiner stringJoiner = new StringJoiner("\n");
            for (final LinkAddress linkAddress : this.mLinkProperties.getLinkAddresses()) {
                String hostAddress;
                String ipv4PrefixLengthToSubnetMask;
                if (linkAddress.getAddress() instanceof Inet4Address) {
                    hostAddress = linkAddress.getAddress().getHostAddress();
                    ipv4PrefixLengthToSubnetMask = ipv4PrefixLengthToSubnetMask(linkAddress.getPrefixLength());
                }
                else {
                    hostAddress = s;
                    ipv4PrefixLengthToSubnetMask = s2;
                    if (linkAddress.getAddress() instanceof Inet6Address) {
                        stringJoiner.add(linkAddress.getAddress().getHostAddress());
                        ipv4PrefixLengthToSubnetMask = s2;
                        hostAddress = s;
                    }
                }
                s = hostAddress;
                s2 = ipv4PrefixLengthToSubnetMask;
            }
            final String s3 = null;
            final Iterator iterator2 = this.mLinkProperties.getRoutes().iterator();
            String hostAddress2;
            while (true) {
                hostAddress2 = s3;
                if (!iterator2.hasNext()) {
                    break;
                }
                final RouteInfo routeInfo = iterator2.next();
                if (routeInfo.isIPv4Default() && routeInfo.hasGateway()) {
                    hostAddress2 = routeInfo.getGateway().getHostAddress();
                    break;
                }
            }
            final String s4 = (String)this.mLinkProperties.getDnsServers().stream().map((Function)_$$Lambda$WifiDetailPreferenceController$XZAGhHrbkIDyusER4MAM6luKcT0.INSTANCE).collect(Collectors.joining("\n"));
            this.updatePreference(this.mIpAddressPref, s);
            this.updatePreference(this.mSubnetPref, s2);
            this.updatePreference(this.mGatewayPref, hostAddress2);
            this.updatePreference(this.mDnsPref, s4);
            if (stringJoiner.length() > 0) {
                this.mIpv6AddressPref.setSummary(BidiFormatter.getInstance().unicodeWrap(stringJoiner.toString()));
                this.mIpv6Category.setVisible(true);
            }
            else {
                this.mIpv6Category.setVisible(false);
            }
            return;
        }
        this.mIpAddressPref.setVisible(false);
        this.mSubnetPref.setVisible(false);
        this.mGatewayPref.setVisible(false);
        this.mDnsPref.setVisible(false);
        this.mIpv6Category.setVisible(false);
    }
    
    private void updatePreference(final WifiDetailPreference wifiDetailPreference, final String detailText) {
        if (!TextUtils.isEmpty((CharSequence)detailText)) {
            wifiDetailPreference.setDetailText(detailText);
            wifiDetailPreference.setVisible(true);
        }
        else {
            wifiDetailPreference.setVisible(false);
        }
    }
    
    public boolean canModifyNetwork() {
        return this.mWifiConfig != null && !WifiUtils.isNetworkLockedDown(this.mContext, this.mWifiConfig);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.setupEntityHeader(preferenceScreen);
        this.mButtonsPref = ((ActionButtonPreference)preferenceScreen.findPreference("buttons")).setButton1Text(2131887697).setButton1Positive(false).setButton1OnClickListener((View.OnClickListener)new _$$Lambda$WifiDetailPreferenceController$HDOTYXVF80U7sCZa22KqorlzriY(this)).setButton2Text(2131890154).setButton2Positive(true).setButton2OnClickListener((View.OnClickListener)new _$$Lambda$WifiDetailPreferenceController$PxMNywf_HXiVAESmLubuiIo869s(this));
        this.mSignalStrengthPref = (WifiDetailPreference)preferenceScreen.findPreference("signal_strength");
        this.mLinkSpeedPref = (WifiDetailPreference)preferenceScreen.findPreference("link_speed");
        this.mFrequencyPref = (WifiDetailPreference)preferenceScreen.findPreference("frequency");
        this.mSecurityPref = (WifiDetailPreference)preferenceScreen.findPreference("security");
        this.mMacAddressPref = (WifiDetailPreference)preferenceScreen.findPreference("mac_address");
        this.mIpAddressPref = (WifiDetailPreference)preferenceScreen.findPreference("ip_address");
        this.mGatewayPref = (WifiDetailPreference)preferenceScreen.findPreference("gateway");
        this.mSubnetPref = (WifiDetailPreference)preferenceScreen.findPreference("subnet_mask");
        this.mDnsPref = (WifiDetailPreference)preferenceScreen.findPreference("dns");
        this.mIpv6Category = (PreferenceCategory)preferenceScreen.findPreference("ipv6_category");
        this.mIpv6AddressPref = preferenceScreen.findPreference("ipv6_addresses");
        this.mSecurityPref.setDetailText(this.mAccessPoint.getSecurityString(false));
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onForget(final WifiDialog wifiDialog) {
    }
    
    @Override
    public void onPause() {
        this.mNetwork = null;
        this.mLinkProperties = null;
        this.mNetworkCapabilities = null;
        this.mNetworkInfo = null;
        this.mWifiInfo = null;
        this.mContext.unregisterReceiver(this.mReceiver);
        this.mConnectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
    }
    
    @Override
    public void onResume() {
        this.mNetwork = this.mWifiManager.getCurrentNetwork();
        this.mLinkProperties = this.mConnectivityManager.getLinkProperties(this.mNetwork);
        this.mNetworkCapabilities = this.mConnectivityManager.getNetworkCapabilities(this.mNetwork);
        this.updateInfo();
        this.mContext.registerReceiver(this.mReceiver, this.mFilter);
        this.mConnectivityManager.registerNetworkCallback(this.mNetworkRequest, this.mNetworkCallback, this.mHandler);
    }
    
    @Override
    public void onSubmit(final WifiDialog wifiDialog) {
        if (wifiDialog.getController() != null) {
            this.mWifiManager.save(wifiDialog.getController().getConfig(), (WifiManager$ActionListener)new WifiManager$ActionListener() {
                public void onFailure(final int n) {
                    final Activity activity = WifiDetailPreferenceController.this.mFragment.getActivity();
                    if (activity != null) {
                        Toast.makeText((Context)activity, 2131889997, 0).show();
                    }
                }
                
                public void onSuccess() {
                }
            });
        }
    }
    
    @VisibleForTesting
    static class IconInjector
    {
        private final Context mContext;
        
        public IconInjector(final Context mContext) {
            this.mContext = mContext;
        }
        
        public Drawable getIcon(final int n) {
            return this.mContext.getDrawable(Utils.getWifiIconResource(n)).mutate();
        }
    }
}
