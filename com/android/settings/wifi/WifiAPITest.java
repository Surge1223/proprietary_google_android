package com.android.settings.wifi;

import android.text.Editable;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.View;
import android.widget.EditText;
import android.app.AlertDialog$Builder;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.net.wifi.WifiManager;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class WifiAPITest extends SettingsPreferenceFragment implements OnPreferenceClickListener
{
    private Preference mWifiDisableNetwork;
    private Preference mWifiDisconnect;
    private Preference mWifiEnableNetwork;
    private WifiManager mWifiManager;
    private int netid;
    
    @Override
    public int getMetricsCategory() {
        return 89;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mWifiManager = (WifiManager)this.getSystemService("wifi");
    }
    
    @Override
    public void onCreatePreferences(final Bundle bundle, final String s) {
        this.addPreferencesFromResource(2131558882);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        (this.mWifiDisconnect = preferenceScreen.findPreference("disconnect")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        (this.mWifiDisableNetwork = preferenceScreen.findPreference("disable_network")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        (this.mWifiEnableNetwork = preferenceScreen.findPreference("enable_network")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (preference == this.mWifiDisconnect) {
            this.mWifiManager.disconnect();
        }
        else if (preference == this.mWifiDisableNetwork) {
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(this.getContext());
            alertDialog$Builder.setTitle((CharSequence)"Input");
            alertDialog$Builder.setMessage((CharSequence)"Enter Network ID");
            final EditText view = new EditText(this.getPrefContext());
            alertDialog$Builder.setView((View)view);
            alertDialog$Builder.setPositiveButton((CharSequence)"Ok", (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    final Editable text = view.getText();
                    try {
                        WifiAPITest.this.netid = Integer.parseInt(text.toString());
                        WifiAPITest.this.mWifiManager.disableNetwork(WifiAPITest.this.netid);
                    }
                    catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            });
            alertDialog$Builder.setNegativeButton((CharSequence)"Cancel", (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                }
            });
            alertDialog$Builder.show();
        }
        else if (preference == this.mWifiEnableNetwork) {
            final AlertDialog$Builder alertDialog$Builder2 = new AlertDialog$Builder(this.getContext());
            alertDialog$Builder2.setTitle((CharSequence)"Input");
            alertDialog$Builder2.setMessage((CharSequence)"Enter Network ID");
            final EditText view2 = new EditText(this.getPrefContext());
            alertDialog$Builder2.setView((View)view2);
            alertDialog$Builder2.setPositiveButton((CharSequence)"Ok", (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    WifiAPITest.this.netid = Integer.parseInt(view2.getText().toString());
                    WifiAPITest.this.mWifiManager.enableNetwork(WifiAPITest.this.netid, false);
                }
            });
            alertDialog$Builder2.setNegativeButton((CharSequence)"Cancel", (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                }
            });
            alertDialog$Builder2.show();
        }
        return true;
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        super.onPreferenceTreeClick(preference);
        return false;
    }
}
