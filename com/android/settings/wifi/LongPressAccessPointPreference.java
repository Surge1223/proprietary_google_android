package com.android.settings.wifi;

import android.view.View$OnCreateContextMenuListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import com.android.settingslib.wifi.AccessPoint;
import android.app.Fragment;
import com.android.settingslib.wifi.AccessPointPreference;

public class LongPressAccessPointPreference extends AccessPointPreference
{
    private final Fragment mFragment;
    
    public LongPressAccessPointPreference(final AccessPoint accessPoint, final Context context, final UserBadgeCache userBadgeCache, final boolean b, final int n, final Fragment mFragment) {
        super(accessPoint, context, userBadgeCache, n, b);
        this.mFragment = mFragment;
    }
    
    public LongPressAccessPointPreference(final AccessPoint accessPoint, final Context context, final UserBadgeCache userBadgeCache, final boolean b, final Fragment mFragment) {
        super(accessPoint, context, userBadgeCache, b);
        this.mFragment = mFragment;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        if (this.mFragment != null) {
            preferenceViewHolder.itemView.setOnCreateContextMenuListener((View$OnCreateContextMenuListener)this.mFragment);
            preferenceViewHolder.itemView.setTag((Object)this);
            preferenceViewHolder.itemView.setLongClickable(true);
        }
    }
}
