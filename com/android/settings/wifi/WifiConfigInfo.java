package com.android.settings.wifi;

import java.util.List;
import android.os.Bundle;
import android.net.wifi.WifiManager;
import android.widget.TextView;
import android.app.Activity;

public class WifiConfigInfo extends Activity
{
    private TextView mConfigList;
    private WifiManager mWifiManager;
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mWifiManager = (WifiManager)this.getSystemService("wifi");
        this.setContentView(2131558885);
        this.mConfigList = (TextView)this.findViewById(2131362001);
    }
    
    protected void onResume() {
        super.onResume();
        if (this.mWifiManager.isWifiEnabled()) {
            final List configuredNetworks = this.mWifiManager.getConfiguredNetworks();
            final StringBuffer text = new StringBuffer();
            for (int i = configuredNetworks.size() - 1; i >= 0; --i) {
                text.append(configuredNetworks.get(i));
            }
            this.mConfigList.setText((CharSequence)text);
        }
        else {
            this.mConfigList.setText(2131890161);
        }
    }
}
