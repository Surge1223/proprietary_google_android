package com.android.settings.wifi;

import android.os.Bundle;
import android.os.Handler;
import java.net.UnknownHostException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import android.text.TextUtils;
import android.net.wifi.ScanResult;
import com.android.settingslib.wifi.AccessPoint;
import android.net.wifi.WifiInfo;
import android.view.View;
import android.util.Log;
import android.net.wifi.SupplicantState;
import android.net.NetworkInfo;
import android.content.Intent;
import android.content.Context;
import android.widget.Button;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.app.Activity;

public class WifiStatusTest extends Activity
{
    private TextView mBSSID;
    private TextView mHiddenSSID;
    private TextView mHttpClientTest;
    private String mHttpClientTestResult;
    private TextView mIPAddr;
    private TextView mLinkSpeed;
    private TextView mMACAddr;
    private TextView mNetworkId;
    private TextView mNetworkState;
    View.OnClickListener mPingButtonHandler;
    private TextView mPingHostname;
    private String mPingHostnameResult;
    private TextView mRSSI;
    private TextView mSSID;
    private TextView mScanList;
    private TextView mSupplicantState;
    private WifiManager mWifiManager;
    private TextView mWifiState;
    private IntentFilter mWifiStateFilter;
    private final BroadcastReceiver mWifiStateReceiver;
    private Button pingTestButton;
    private Button updateButton;
    View.OnClickListener updateButtonHandler;
    
    public WifiStatusTest() {
        this.mWifiStateReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                    WifiStatusTest.this.handleWifiStateChanged(intent.getIntExtra("wifi_state", 4));
                }
                else if (intent.getAction().equals("android.net.wifi.STATE_CHANGE")) {
                    WifiStatusTest.this.handleNetworkStateChanged((NetworkInfo)intent.getParcelableExtra("networkInfo"));
                }
                else if (intent.getAction().equals("android.net.wifi.SCAN_RESULTS")) {
                    WifiStatusTest.this.handleScanResultsAvailable();
                }
                else if (!intent.getAction().equals("android.net.wifi.supplicant.CONNECTION_CHANGE")) {
                    if (intent.getAction().equals("android.net.wifi.supplicant.STATE_CHANGE")) {
                        WifiStatusTest.this.handleSupplicantStateChanged((SupplicantState)intent.getParcelableExtra("newState"), intent.hasExtra("supplicantError"), intent.getIntExtra("supplicantError", 0));
                    }
                    else if (intent.getAction().equals("android.net.wifi.RSSI_CHANGED")) {
                        WifiStatusTest.this.handleSignalChanged(intent.getIntExtra("newRssi", 0));
                    }
                    else if (!intent.getAction().equals("android.net.wifi.NETWORK_IDS_CHANGED")) {
                        Log.e("WifiStatusTest", "Received an unknown Wifi Intent");
                    }
                }
            }
        };
        this.mPingButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                WifiStatusTest.this.updatePingState();
            }
        };
        this.updateButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final WifiInfo connectionInfo = WifiStatusTest.this.mWifiManager.getConnectionInfo();
                WifiStatusTest.this.setWifiStateText(WifiStatusTest.this.mWifiManager.getWifiState());
                WifiStatusTest.this.mBSSID.setText((CharSequence)connectionInfo.getBSSID());
                WifiStatusTest.this.mHiddenSSID.setText((CharSequence)String.valueOf(connectionInfo.getHiddenSSID()));
                final int ipAddress = connectionInfo.getIpAddress();
                final StringBuffer text = new StringBuffer();
                text.append(ipAddress & 0xFF);
                text.append('.');
                final int n = ipAddress >>> 8;
                text.append(n & 0xFF);
                text.append('.');
                final int n2 = n >>> 8;
                text.append(n2 & 0xFF);
                text.append('.');
                text.append(n2 >>> 8 & 0xFF);
                WifiStatusTest.this.mIPAddr.setText((CharSequence)text);
                final TextView access$1100 = WifiStatusTest.this.mLinkSpeed;
                final StringBuilder sb = new StringBuilder();
                sb.append(String.valueOf(connectionInfo.getLinkSpeed()));
                sb.append(" Mbps");
                access$1100.setText((CharSequence)sb.toString());
                WifiStatusTest.this.mMACAddr.setText((CharSequence)connectionInfo.getMacAddress());
                WifiStatusTest.this.mNetworkId.setText((CharSequence)String.valueOf(connectionInfo.getNetworkId()));
                WifiStatusTest.this.mRSSI.setText((CharSequence)String.valueOf(connectionInfo.getRssi()));
                WifiStatusTest.this.mSSID.setText((CharSequence)connectionInfo.getSSID());
                WifiStatusTest.this.setSupplicantStateText(connectionInfo.getSupplicantState());
            }
        };
    }
    
    private void handleNetworkStateChanged(final NetworkInfo networkInfo) {
        if (this.mWifiManager.isWifiEnabled()) {
            final WifiInfo connectionInfo = this.mWifiManager.getConnectionInfo();
            this.mNetworkState.setText((CharSequence)AccessPoint.getSummary((Context)this, connectionInfo.getSSID(), networkInfo.getDetailedState(), connectionInfo.getNetworkId() == -1, null));
        }
    }
    
    private void handleScanResultsAvailable() {
        final List scanResults = this.mWifiManager.getScanResults();
        final StringBuffer text = new StringBuffer();
        if (scanResults != null) {
            for (int i = scanResults.size() - 1; i >= 0; --i) {
                final ScanResult scanResult = scanResults.get(i);
                if (scanResult != null) {
                    if (!TextUtils.isEmpty((CharSequence)scanResult.SSID)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(scanResult.SSID);
                        sb.append(" ");
                        text.append(sb.toString());
                    }
                }
            }
        }
        this.mScanList.setText((CharSequence)text);
    }
    
    private void handleSignalChanged(final int n) {
        this.mRSSI.setText((CharSequence)String.valueOf(n));
    }
    
    private void handleSupplicantStateChanged(final SupplicantState supplicantStateText, final boolean b, final int n) {
        if (b) {
            this.mSupplicantState.setText((CharSequence)"ERROR AUTHENTICATING");
        }
        else {
            this.setSupplicantStateText(supplicantStateText);
        }
    }
    
    private void handleWifiStateChanged(final int wifiStateText) {
        this.setWifiStateText(wifiStateText);
    }
    
    private void httpClientTest() {
        HttpURLConnection httpURLConnection2;
        final HttpURLConnection httpURLConnection = httpURLConnection2 = null;
        try {
            try {
                httpURLConnection2 = httpURLConnection;
                final URL url = new URL("https://www.google.com");
                httpURLConnection2 = httpURLConnection;
                final HttpURLConnection httpURLConnection3 = httpURLConnection2 = (HttpURLConnection)url.openConnection();
                if (httpURLConnection3.getResponseCode() == 200) {
                    httpURLConnection2 = httpURLConnection3;
                    this.mHttpClientTestResult = "Pass";
                }
                else {
                    httpURLConnection2 = httpURLConnection3;
                    httpURLConnection2 = httpURLConnection3;
                    final StringBuilder sb = new StringBuilder();
                    httpURLConnection2 = httpURLConnection3;
                    sb.append("Fail: Code: ");
                    httpURLConnection2 = httpURLConnection3;
                    sb.append(httpURLConnection3.getResponseMessage());
                    httpURLConnection2 = httpURLConnection3;
                    this.mHttpClientTestResult = sb.toString();
                }
                if (httpURLConnection3 != null) {
                    httpURLConnection3.disconnect();
                }
            }
            finally {
                if (httpURLConnection2 != null) {
                    httpURLConnection2.disconnect();
                }
            }
        }
        catch (IOException ex) {}
    }
    
    private final void pingHostname() {
        try {
            if (Runtime.getRuntime().exec("ping -c 1 -w 100 www.google.com").waitFor() == 0) {
                this.mPingHostnameResult = "Pass";
            }
            else {
                this.mPingHostnameResult = "Fail: Host unreachable";
            }
        }
        catch (InterruptedException ex) {
            this.mPingHostnameResult = "Fail: InterruptedException";
        }
        catch (IOException ex2) {
            this.mPingHostnameResult = "Fail: IOException";
        }
        catch (UnknownHostException ex3) {
            this.mPingHostnameResult = "Fail: Unknown Host";
        }
    }
    
    private void setSupplicantStateText(final SupplicantState supplicantState) {
        if (SupplicantState.FOUR_WAY_HANDSHAKE.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"FOUR WAY HANDSHAKE");
        }
        else if (SupplicantState.ASSOCIATED.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"ASSOCIATED");
        }
        else if (SupplicantState.ASSOCIATING.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"ASSOCIATING");
        }
        else if (SupplicantState.COMPLETED.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"COMPLETED");
        }
        else if (SupplicantState.DISCONNECTED.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"DISCONNECTED");
        }
        else if (SupplicantState.DORMANT.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"DORMANT");
        }
        else if (SupplicantState.GROUP_HANDSHAKE.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"GROUP HANDSHAKE");
        }
        else if (SupplicantState.INACTIVE.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"INACTIVE");
        }
        else if (SupplicantState.INVALID.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"INVALID");
        }
        else if (SupplicantState.SCANNING.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"SCANNING");
        }
        else if (SupplicantState.UNINITIALIZED.equals((Object)supplicantState)) {
            this.mSupplicantState.setText((CharSequence)"UNINITIALIZED");
        }
        else {
            this.mSupplicantState.setText((CharSequence)"BAD");
            Log.e("WifiStatusTest", "supplicant state is bad");
        }
    }
    
    private void setWifiStateText(final int n) {
        String text = null;
        switch (n) {
            default: {
                text = "BAD";
                Log.e("WifiStatusTest", "wifi state is bad");
                break;
            }
            case 4: {
                text = this.getString(2131890166);
                break;
            }
            case 3: {
                text = this.getString(2131890163);
                break;
            }
            case 2: {
                text = this.getString(2131890164);
                break;
            }
            case 1: {
                text = this.getString(2131890161);
                break;
            }
            case 0: {
                text = this.getString(2131890162);
                break;
            }
        }
        this.mWifiState.setText((CharSequence)text);
    }
    
    private final void updatePingState() {
        final Handler handler = new Handler();
        this.mPingHostnameResult = this.getResources().getString(2131888746);
        this.mHttpClientTestResult = this.getResources().getString(2131888746);
        this.mPingHostname.setText((CharSequence)this.mPingHostnameResult);
        this.mHttpClientTest.setText((CharSequence)this.mHttpClientTestResult);
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                WifiStatusTest.this.mPingHostname.setText((CharSequence)WifiStatusTest.this.mPingHostnameResult);
                WifiStatusTest.this.mHttpClientTest.setText((CharSequence)WifiStatusTest.this.mHttpClientTestResult);
            }
        };
        new Thread() {
            @Override
            public void run() {
                WifiStatusTest.this.pingHostname();
                handler.post(runnable);
            }
        }.start();
        new Thread() {
            @Override
            public void run() {
                WifiStatusTest.this.httpClientTest();
                handler.post(runnable);
            }
        }.start();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mWifiManager = (WifiManager)this.getSystemService("wifi");
        (this.mWifiStateFilter = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED")).addAction("android.net.wifi.STATE_CHANGE");
        this.mWifiStateFilter.addAction("android.net.wifi.SCAN_RESULTS");
        this.mWifiStateFilter.addAction("android.net.wifi.supplicant.STATE_CHANGE");
        this.mWifiStateFilter.addAction("android.net.wifi.RSSI_CHANGED");
        this.mWifiStateFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        this.registerReceiver(this.mWifiStateReceiver, this.mWifiStateFilter);
        this.setContentView(2131558896);
        (this.updateButton = (Button)this.findViewById(2131362776)).setOnClickListener(this.updateButtonHandler);
        this.mWifiState = (TextView)this.findViewById(2131362836);
        this.mNetworkState = (TextView)this.findViewById(2131362385);
        this.mSupplicantState = (TextView)this.findViewById(2131362674);
        this.mRSSI = (TextView)this.findViewById(2131362540);
        this.mBSSID = (TextView)this.findViewById(2131361930);
        this.mSSID = (TextView)this.findViewById(2131362636);
        this.mHiddenSSID = (TextView)this.findViewById(2131362224);
        this.mIPAddr = (TextView)this.findViewById(2131362286);
        this.mMACAddr = (TextView)this.findViewById(2131362351);
        this.mNetworkId = (TextView)this.findViewById(2131362386);
        this.mLinkSpeed = (TextView)this.findViewById(2131362335);
        this.mScanList = (TextView)this.findViewById(2131362544);
        this.mPingHostname = (TextView)this.findViewById(2131362449);
        this.mHttpClientTest = (TextView)this.findViewById(2131362233);
        (this.pingTestButton = (Button)this.findViewById(2131362452)).setOnClickListener(this.mPingButtonHandler);
    }
    
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mWifiStateReceiver);
    }
    
    protected void onResume() {
        super.onResume();
        this.registerReceiver(this.mWifiStateReceiver, this.mWifiStateFilter);
    }
}
