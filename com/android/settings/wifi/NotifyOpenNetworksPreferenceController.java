package com.android.settings.wifi;

import android.content.ContentResolver;
import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.provider.Settings;
import android.support.v14.preference.SwitchPreference;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class NotifyOpenNetworksPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private SettingObserver mSettingObserver;
    
    public NotifyOpenNetworksPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        lifecycle.addObserver(this);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mSettingObserver = new SettingObserver(preferenceScreen.findPreference("notify_open_networks"));
    }
    
    @Override
    public String getPreferenceKey() {
        return "notify_open_networks";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"notify_open_networks")) {
            return false;
        }
        if (!(preference instanceof SwitchPreference)) {
            return false;
        }
        Settings.Global.putInt(this.mContext.getContentResolver(), "wifi_networks_available_notification_on", (int)(((SwitchPreference)preference).isChecked() ? 1 : 0));
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onPause() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), false);
        }
    }
    
    @Override
    public void onResume() {
        if (this.mSettingObserver != null) {
            this.mSettingObserver.register(this.mContext.getContentResolver(), true);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof SwitchPreference)) {
            return;
        }
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "wifi_networks_available_notification_on", 0);
        boolean checked = true;
        if (int1 != 1) {
            checked = false;
        }
        switchPreference.setChecked(checked);
    }
    
    class SettingObserver extends ContentObserver
    {
        private final Uri NETWORKS_AVAILABLE_URI;
        private final Preference mPreference;
        
        public SettingObserver(final Preference mPreference) {
            super(new Handler());
            this.NETWORKS_AVAILABLE_URI = Settings.Global.getUriFor("wifi_networks_available_notification_on");
            this.mPreference = mPreference;
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.NETWORKS_AVAILABLE_URI.equals((Object)uri)) {
                NotifyOpenNetworksPreferenceController.this.updateState(this.mPreference);
            }
        }
        
        public void register(final ContentResolver contentResolver, final boolean b) {
            if (b) {
                contentResolver.registerContentObserver(this.NETWORKS_AVAILABLE_URI, false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
