package com.android.settings.wifi;

import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.app.DialogFragment;
import android.app.Activity;

public class WifiScanModeActivity extends Activity
{
    private String mApp;
    private DialogFragment mDialog;
    
    private void createDialog() {
        if (this.mDialog == null) {
            (this.mDialog = AlertDialogFragment.newInstance(this.mApp)).show(this.getFragmentManager(), "dialog");
        }
    }
    
    private void dismissDialog() {
        if (this.mDialog != null) {
            this.mDialog.dismiss();
            this.mDialog = null;
        }
    }
    
    private void doNegativeClick() {
        this.setResult(0);
        this.finish();
    }
    
    private void doPositiveClick() {
        Settings.Global.putInt(this.getContentResolver(), "wifi_scan_always_enabled", 1);
        this.setResult(-1);
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent intent = this.getIntent();
        if (bundle == null) {
            if (intent == null || !intent.getAction().equals("android.net.wifi.action.REQUEST_SCAN_ALWAYS_AVAILABLE")) {
                this.finish();
                return;
            }
            this.mApp = this.getCallingPackage();
            try {
                final PackageManager packageManager = this.getPackageManager();
                this.mApp = (String)packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.mApp, 0));
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        else {
            this.mApp = bundle.getString("app");
        }
        this.createDialog();
    }
    
    public void onPause() {
        super.onPause();
        this.dismissDialog();
    }
    
    public void onResume() {
        super.onResume();
        this.createDialog();
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("app", this.mApp);
    }
    
    public static class AlertDialogFragment extends InstrumentedDialogFragment
    {
        private final String mApp;
        
        public AlertDialogFragment() {
            this.mApp = null;
        }
        
        public AlertDialogFragment(final String mApp) {
            this.mApp = mApp;
        }
        
        static AlertDialogFragment newInstance(final String s) {
            return new AlertDialogFragment(s);
        }
        
        @Override
        public int getMetricsCategory() {
            return 543;
        }
        
        public void onCancel(final DialogInterface dialogInterface) {
            ((WifiScanModeActivity)this.getActivity()).doNegativeClick();
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage((CharSequence)this.getString(2131890089, new Object[] { this.mApp })).setPositiveButton(2131890086, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    ((WifiScanModeActivity)AlertDialogFragment.this.getActivity()).doPositiveClick();
                }
            }).setNegativeButton(2131890087, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    ((WifiScanModeActivity)AlertDialogFragment.this.getActivity()).doNegativeClick();
                }
            }).create();
        }
    }
}
