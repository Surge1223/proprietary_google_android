package com.android.settings.wifi;

import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.os.Parcelable;
import android.net.wifi.WifiManager$ActionListener;
import android.util.Log;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import com.android.settingslib.wifi.AccessPoint;
import com.android.settings.SetupWizardUtils;
import com.android.setupwizardlib.util.WizardManagerHelper;
import android.os.Bundle;
import android.content.DialogInterface$OnDismissListener;
import android.app.Activity;

public class WifiDialogActivity extends Activity implements DialogInterface$OnDismissListener, WifiDialogListener
{
    static final String KEY_CONNECT_FOR_CALLER = "connect_for_caller";
    
    public void finish() {
        super.finish();
        this.overridePendingTransition(0, 0);
    }
    
    protected void onCreate(final Bundle bundle) {
        final Intent intent = this.getIntent();
        if (WizardManagerHelper.isSetupWizardIntent(intent)) {
            this.setTheme(SetupWizardUtils.getTransparentTheme(intent));
        }
        super.onCreate(bundle);
        final Bundle bundleExtra = intent.getBundleExtra("access_point_state");
        AccessPoint accessPoint = null;
        if (bundleExtra != null) {
            accessPoint = new AccessPoint((Context)this, bundleExtra);
        }
        final WifiDialog modal = WifiDialog.createModal((Context)this, (WifiDialog.WifiDialogListener)this, accessPoint, 1);
        modal.show();
        modal.setOnDismissListener((DialogInterface$OnDismissListener)this);
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        this.finish();
    }
    
    public void onForget(final WifiDialog wifiDialog) {
        final WifiManager wifiManager = (WifiManager)this.getSystemService((Class)WifiManager.class);
        final AccessPoint accessPoint = wifiDialog.getController().getAccessPoint();
        if (accessPoint != null) {
            if (!accessPoint.isSaved()) {
                if (accessPoint.getNetworkInfo() != null && accessPoint.getNetworkInfo().getState() != NetworkInfo.State.DISCONNECTED) {
                    wifiManager.disableEphemeralNetwork(AccessPoint.convertToQuotedString(accessPoint.getSsidStr()));
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to forget invalid network ");
                    sb.append(accessPoint.getConfig());
                    Log.e("WifiDialogActivity", sb.toString());
                }
            }
            else {
                wifiManager.forget(accessPoint.getConfig().networkId, (WifiManager$ActionListener)null);
            }
        }
        final Intent intent = new Intent();
        if (accessPoint != null) {
            final Bundle bundle = new Bundle();
            accessPoint.saveWifiState(bundle);
            intent.putExtra("access_point_state", bundle);
        }
        this.setResult(2);
        this.finish();
    }
    
    public void onSubmit(final WifiDialog wifiDialog) {
        final WifiConfiguration config = wifiDialog.getController().getConfig();
        final AccessPoint accessPoint = wifiDialog.getController().getAccessPoint();
        final WifiManager wifiManager = (WifiManager)this.getSystemService((Class)WifiManager.class);
        if (this.getIntent().getBooleanExtra("connect_for_caller", true)) {
            if (config == null) {
                if (accessPoint != null && accessPoint.isSaved()) {
                    wifiManager.connect(accessPoint.getConfig(), (WifiManager$ActionListener)null);
                }
            }
            else {
                wifiManager.save(config, (WifiManager$ActionListener)null);
                if (accessPoint != null) {
                    final NetworkInfo networkInfo = accessPoint.getNetworkInfo();
                    if (networkInfo == null || !networkInfo.isConnected()) {
                        wifiManager.connect(config, (WifiManager$ActionListener)null);
                    }
                }
            }
        }
        final Intent intent = new Intent();
        if (accessPoint != null) {
            final Bundle bundle = new Bundle();
            accessPoint.saveWifiState(bundle);
            intent.putExtra("access_point_state", bundle);
        }
        if (config != null) {
            intent.putExtra("wifi_configuration", (Parcelable)config);
        }
        this.setResult(1, intent);
        this.finish();
    }
}
