package com.android.settings.wifi;

import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import com.android.settingslib.wifi.AccessPoint;
import android.view.View.OnClickListener;
import com.android.settingslib.wifi.AccessPointPreference;

public class ConnectedAccessPointPreference extends AccessPointPreference implements View.OnClickListener
{
    private boolean mIsCaptivePortal;
    private OnGearClickListener mOnGearClickListener;
    
    public ConnectedAccessPointPreference(final AccessPoint accessPoint, final Context context, final UserBadgeCache userBadgeCache, final int n, final boolean b) {
        super(accessPoint, context, userBadgeCache, n, b);
    }
    
    @Override
    protected int getWidgetLayoutResourceId() {
        return 2131558681;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(2131362594);
        viewById.setOnClickListener((View.OnClickListener)this);
        final View viewById2 = preferenceViewHolder.findViewById(2131362595);
        final boolean mIsCaptivePortal = this.mIsCaptivePortal;
        final boolean b = false;
        int visibility;
        if (mIsCaptivePortal) {
            visibility = 4;
        }
        else {
            visibility = 0;
        }
        viewById2.setVisibility(visibility);
        int visibility2;
        if (this.mIsCaptivePortal) {
            visibility2 = (b ? 1 : 0);
        }
        else {
            visibility2 = 4;
        }
        viewById.setVisibility(visibility2);
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131362594 && this.mOnGearClickListener != null) {
            this.mOnGearClickListener.onGearClick(this);
        }
    }
    
    @Override
    public void refresh() {
        super.refresh();
        this.setShowDivider(this.mIsCaptivePortal);
        if (this.mIsCaptivePortal) {
            this.setSummary(2131890177);
        }
    }
    
    public void setCaptivePortal(final boolean mIsCaptivePortal) {
        if (this.mIsCaptivePortal != mIsCaptivePortal) {
            this.mIsCaptivePortal = mIsCaptivePortal;
            this.refresh();
        }
    }
    
    public void setOnGearClickListener(final OnGearClickListener mOnGearClickListener) {
        this.mOnGearClickListener = mOnGearClickListener;
        this.notifyChanged();
    }
    
    public interface OnGearClickListener
    {
        void onGearClick(final ConnectedAccessPointPreference p0);
    }
}
