package com.android.settings.wifi.p2p;

import android.os.Parcelable;
import android.os.SystemProperties;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pGroupList;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.View;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.app.AlertDialog$Builder;
import android.text.TextUtils;
import android.app.Dialog;
import android.widget.Toast;
import android.content.DialogInterface;
import android.util.Log;
import android.net.wifi.p2p.WifiP2pManager$ChannelListener;
import android.os.Bundle;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.app.Activity;
import android.net.wifi.p2p.WifiP2pManager$ActionListener;
import java.util.Iterator;
import android.support.v7.preference.Preference;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.NetworkInfo;
import android.content.Intent;
import android.content.Context;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.content.BroadcastReceiver;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.content.IntentFilter;
import android.widget.EditText;
import android.net.wifi.p2p.WifiP2pManager$Channel;
import android.content.DialogInterface$OnClickListener;
import android.net.wifi.p2p.WifiP2pManager$PersistentGroupInfoListener;
import android.net.wifi.p2p.WifiP2pManager$PeerListListener;
import com.android.settings.dashboard.DashboardFragment;

public class WifiP2pSettings extends DashboardFragment implements WifiP2pManager$PeerListListener, WifiP2pManager$PersistentGroupInfoListener
{
    private DialogInterface$OnClickListener mCancelConnectListener;
    private WifiP2pManager$Channel mChannel;
    private int mConnectedDevices;
    private DialogInterface$OnClickListener mDeleteGroupListener;
    private EditText mDeviceNameText;
    private DialogInterface$OnClickListener mDisconnectListener;
    private final IntentFilter mIntentFilter;
    private boolean mLastGroupFormed;
    private P2pPeerCategoryPreferenceController mPeerCategoryController;
    private WifiP2pDeviceList mPeers;
    private P2pPersistentCategoryPreferenceController mPersistentCategoryController;
    private final BroadcastReceiver mReceiver;
    private DialogInterface$OnClickListener mRenameListener;
    private String mSavedDeviceName;
    private WifiP2pPersistentGroup mSelectedGroup;
    private String mSelectedGroupName;
    private WifiP2pPeer mSelectedWifiPeer;
    private WifiP2pDevice mThisDevice;
    private P2pThisDevicePreferenceController mThisDevicePreferenceController;
    private boolean mWifiP2pEnabled;
    private WifiP2pManager mWifiP2pManager;
    private boolean mWifiP2pSearching;
    
    public WifiP2pSettings() {
        this.mIntentFilter = new IntentFilter();
        this.mLastGroupFormed = false;
        this.mPeers = new WifiP2pDeviceList();
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                final boolean equals = "android.net.wifi.p2p.STATE_CHANGED".equals(action);
                boolean b = false;
                if (equals) {
                    final WifiP2pSettings this$0 = WifiP2pSettings.this;
                    if (intent.getIntExtra("wifi_p2p_state", 1) == 2) {
                        b = true;
                    }
                    this$0.mWifiP2pEnabled = b;
                    WifiP2pSettings.this.handleP2pStateChanged();
                }
                else if ("android.net.wifi.p2p.PEERS_CHANGED".equals(action)) {
                    WifiP2pSettings.this.mPeers = (WifiP2pDeviceList)intent.getParcelableExtra("wifiP2pDeviceList");
                    WifiP2pSettings.this.handlePeersChanged();
                }
                else if ("android.net.wifi.p2p.CONNECTION_STATE_CHANGE".equals(action)) {
                    if (WifiP2pSettings.this.mWifiP2pManager == null) {
                        return;
                    }
                    final NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra("networkInfo");
                    final WifiP2pInfo wifiP2pInfo = (WifiP2pInfo)intent.getParcelableExtra("wifiP2pInfo");
                    if (!networkInfo.isConnected()) {
                        if (!WifiP2pSettings.this.mLastGroupFormed) {
                            WifiP2pSettings.this.startSearch();
                        }
                    }
                    WifiP2pSettings.this.mLastGroupFormed = wifiP2pInfo.groupFormed;
                }
                else if ("android.net.wifi.p2p.THIS_DEVICE_CHANGED".equals(action)) {
                    WifiP2pSettings.this.mThisDevice = (WifiP2pDevice)intent.getParcelableExtra("wifiP2pDevice");
                    WifiP2pSettings.this.mThisDevicePreferenceController.updateDeviceName(WifiP2pSettings.this.mThisDevice);
                }
                else if ("android.net.wifi.p2p.DISCOVERY_STATE_CHANGE".equals(action)) {
                    if (intent.getIntExtra("discoveryState", 1) == 2) {
                        WifiP2pSettings.this.updateSearchMenu(true);
                    }
                    else {
                        WifiP2pSettings.this.updateSearchMenu(false);
                    }
                }
                else if ("android.net.wifi.p2p.PERSISTENT_GROUPS_CHANGED".equals(action) && WifiP2pSettings.this.mWifiP2pManager != null) {
                    WifiP2pSettings.this.mWifiP2pManager.requestPersistentGroupInfo(WifiP2pSettings.this.mChannel, (WifiP2pManager$PersistentGroupInfoListener)WifiP2pSettings.this);
                }
            }
        };
    }
    
    private void handleP2pStateChanged() {
        this.updateSearchMenu(false);
        this.mThisDevicePreferenceController.setEnabled(this.mWifiP2pEnabled);
        this.mPersistentCategoryController.setEnabled(this.mWifiP2pEnabled);
        this.mPeerCategoryController.setEnabled(this.mWifiP2pEnabled);
    }
    
    private void handlePeersChanged() {
        this.mPeerCategoryController.removeAllChildren();
        this.mConnectedDevices = 0;
        for (final WifiP2pDevice wifiP2pDevice : this.mPeers.getDeviceList()) {
            this.mPeerCategoryController.addChild(new WifiP2pPeer(this.getPrefContext(), wifiP2pDevice));
            if (wifiP2pDevice.status == 0) {
                ++this.mConnectedDevices;
            }
        }
    }
    
    private void startSearch() {
        if (this.mWifiP2pManager != null && !this.mWifiP2pSearching) {
            this.mWifiP2pManager.discoverPeers(this.mChannel, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
                public void onFailure(final int n) {
                }
                
                public void onSuccess() {
                }
            });
        }
    }
    
    private void updateSearchMenu(final boolean mWifiP2pSearching) {
        this.mWifiP2pSearching = mWifiP2pSearching;
        final Activity activity = this.getActivity();
        if (activity != null) {
            activity.invalidateOptionsMenu();
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<P2pPersistentCategoryPreferenceController> list = (ArrayList<P2pPersistentCategoryPreferenceController>)new ArrayList<P2pThisDevicePreferenceController>();
        this.mPersistentCategoryController = new P2pPersistentCategoryPreferenceController(context);
        this.mPeerCategoryController = new P2pPeerCategoryPreferenceController(context);
        this.mThisDevicePreferenceController = new P2pThisDevicePreferenceController(context);
        list.add((P2pThisDevicePreferenceController)this.mPersistentCategoryController);
        list.add((P2pThisDevicePreferenceController)this.mPeerCategoryController);
        list.add(this.mThisDevicePreferenceController);
        return (List<AbstractPreferenceController>)list;
    }
    
    public int getDialogMetricsCategory(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 4: {
                return 578;
            }
            case 3: {
                return 577;
            }
            case 2: {
                return 576;
            }
            case 1: {
                return 575;
            }
        }
    }
    
    public int getHelpResource() {
        return 2131887841;
    }
    
    @Override
    protected String getLogTag() {
        return "WifiP2pSettings";
    }
    
    public int getMetricsCategory() {
        return 109;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082872;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        final Activity activity = this.getActivity();
        this.mWifiP2pManager = (WifiP2pManager)this.getSystemService("wifip2p");
        if (this.mWifiP2pManager != null) {
            this.mChannel = this.mWifiP2pManager.initialize(activity.getApplicationContext(), this.getActivity().getMainLooper(), (WifiP2pManager$ChannelListener)null);
            if (this.mChannel == null) {
                Log.e("WifiP2pSettings", "Failed to set up connection with wifi p2p service");
                this.mWifiP2pManager = null;
            }
        }
        else {
            Log.e("WifiP2pSettings", "mWifiP2pManager is null !");
        }
        if (bundle != null && bundle.containsKey("PEER_STATE")) {
            this.mSelectedWifiPeer = new WifiP2pPeer(this.getPrefContext(), (WifiP2pDevice)bundle.getParcelable("PEER_STATE"));
        }
        if (bundle != null && bundle.containsKey("DEV_NAME")) {
            this.mSavedDeviceName = bundle.getString("DEV_NAME");
        }
        if (bundle != null && bundle.containsKey("GROUP_NAME")) {
            this.mSelectedGroupName = bundle.getString("GROUP_NAME");
        }
        this.mRenameListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, int i) {
                if (i == -1 && WifiP2pSettings.this.mWifiP2pManager != null) {
                    final String string = WifiP2pSettings.this.mDeviceNameText.getText().toString();
                    if (string != null) {
                        char char1;
                        for (i = 0; i < string.length(); ++i) {
                            char1 = string.charAt(i);
                            if (!Character.isDigit(char1) && !Character.isLetter(char1) && char1 != '-' && char1 != '_' && char1 != ' ') {
                                Toast.makeText((Context)WifiP2pSettings.this.getActivity(), 2131890068, 1).show();
                                return;
                            }
                        }
                    }
                    WifiP2pSettings.this.mWifiP2pManager.setDeviceName(WifiP2pSettings.this.mChannel, WifiP2pSettings.this.mDeviceNameText.getText().toString(), (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
                        public void onFailure(final int n) {
                            Toast.makeText((Context)WifiP2pSettings.this.getActivity(), 2131890068, 1).show();
                        }
                        
                        public void onSuccess() {
                        }
                    });
                }
            }
        };
        this.mDisconnectListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (n == -1 && WifiP2pSettings.this.mWifiP2pManager != null) {
                    WifiP2pSettings.this.mWifiP2pManager.removeGroup(WifiP2pSettings.this.mChannel, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
                        public void onFailure(final int n) {
                        }
                        
                        public void onSuccess() {
                        }
                    });
                }
            }
        };
        this.mCancelConnectListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (n == -1 && WifiP2pSettings.this.mWifiP2pManager != null) {
                    WifiP2pSettings.this.mWifiP2pManager.cancelConnect(WifiP2pSettings.this.mChannel, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
                        public void onFailure(final int n) {
                        }
                        
                        public void onSuccess() {
                        }
                    });
                }
            }
        };
        this.mDeleteGroupListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (n == -1) {
                    if (WifiP2pSettings.this.mWifiP2pManager != null && WifiP2pSettings.this.mSelectedGroup != null) {
                        WifiP2pSettings.this.mWifiP2pManager.deletePersistentGroup(WifiP2pSettings.this.mChannel, WifiP2pSettings.this.mSelectedGroup.getNetworkId(), (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
                            public void onFailure(final int n) {
                            }
                            
                            public void onSuccess() {
                            }
                        });
                        WifiP2pSettings.this.mSelectedGroup = null;
                    }
                }
                else if (n == -2) {
                    WifiP2pSettings.this.mSelectedGroup = null;
                }
            }
        };
        super.onActivityCreated(bundle);
    }
    
    public Dialog onCreateDialog(final int n) {
        if (n == 1) {
            String s;
            if (TextUtils.isEmpty((CharSequence)this.mSelectedWifiPeer.device.deviceName)) {
                s = this.mSelectedWifiPeer.device.deviceAddress;
            }
            else {
                s = this.mSelectedWifiPeer.device.deviceName;
            }
            String message;
            if (this.mConnectedDevices > 1) {
                message = this.getActivity().getString(2131890065, new Object[] { s, this.mConnectedDevices - 1 });
            }
            else {
                message = this.getActivity().getString(2131890064, new Object[] { s });
            }
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131890066).setMessage((CharSequence)message).setPositiveButton((CharSequence)this.getActivity().getString(2131887460), this.mDisconnectListener).setNegativeButton((CharSequence)this.getActivity().getString(2131887454), (DialogInterface$OnClickListener)null).create();
        }
        if (n == 2) {
            String s2;
            if (TextUtils.isEmpty((CharSequence)this.mSelectedWifiPeer.device.deviceName)) {
                s2 = this.mSelectedWifiPeer.device.deviceAddress;
            }
            else {
                s2 = this.mSelectedWifiPeer.device.deviceName;
            }
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131890061).setMessage((CharSequence)this.getActivity().getString(2131890060, new Object[] { s2 })).setPositiveButton((CharSequence)this.getActivity().getString(2131887460), this.mCancelConnectListener).setNegativeButton((CharSequence)this.getActivity().getString(2131887454), (DialogInterface$OnClickListener)null).create();
        }
        if (n == 3) {
            (this.mDeviceNameText = new EditText((Context)this.getActivity())).setFilters(new InputFilter[] { new InputFilter$LengthFilter(30) });
            if (this.mSavedDeviceName != null) {
                this.mDeviceNameText.setText((CharSequence)this.mSavedDeviceName);
                this.mDeviceNameText.setSelection(this.mSavedDeviceName.length());
            }
            else if (this.mThisDevice != null && !TextUtils.isEmpty((CharSequence)this.mThisDevice.deviceName)) {
                this.mDeviceNameText.setText((CharSequence)this.mThisDevice.deviceName);
                this.mDeviceNameText.setSelection(0, this.mThisDevice.deviceName.length());
            }
            this.mSavedDeviceName = null;
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131890069).setView((View)this.mDeviceNameText).setPositiveButton((CharSequence)this.getActivity().getString(2131887460), this.mRenameListener).setNegativeButton((CharSequence)this.getActivity().getString(2131887454), (DialogInterface$OnClickListener)null).create();
        }
        if (n == 4) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage((CharSequence)this.getActivity().getString(2131890062)).setPositiveButton((CharSequence)this.getActivity().getString(2131887460), this.mDeleteGroupListener).setNegativeButton((CharSequence)this.getActivity().getString(2131887454), this.mDeleteGroupListener).create();
        }
        return null;
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        int n;
        if (this.mWifiP2pSearching) {
            n = 2131890071;
        }
        else {
            n = 2131890070;
        }
        menu.add(0, 1, 0, n).setEnabled(this.mWifiP2pEnabled).setShowAsAction(1);
        menu.add(0, 2, 0, 2131890069).setEnabled(this.mWifiP2pEnabled).setShowAsAction(1);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2: {
                this.showDialog(3);
                return true;
            }
            case 1: {
                this.startSearch();
                return true;
            }
        }
    }
    
    public void onPause() {
        super.onPause();
        if (this.mWifiP2pManager != null) {
            this.mWifiP2pManager.stopPeerDiscovery(this.mChannel, (WifiP2pManager$ActionListener)null);
        }
        this.getActivity().unregisterReceiver(this.mReceiver);
    }
    
    public void onPeersAvailable(final WifiP2pDeviceList mPeers) {
        this.mPeers = mPeers;
        this.handlePeersChanged();
    }
    
    public void onPersistentGroupInfoAvailable(final WifiP2pGroupList list) {
        this.mPersistentCategoryController.removeAllChildren();
        final Iterator<WifiP2pGroup> iterator = list.getGroupList().iterator();
        while (iterator.hasNext()) {
            final WifiP2pPersistentGroup mSelectedGroup = new WifiP2pPersistentGroup(this.getPrefContext(), iterator.next());
            this.mPersistentCategoryController.addChild(mSelectedGroup);
            if (mSelectedGroup.getGroupName().equals(this.mSelectedGroupName)) {
                this.mSelectedGroup = mSelectedGroup;
                this.mSelectedGroupName = null;
            }
        }
        if (this.mSelectedGroupName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" Selected group ");
            sb.append(this.mSelectedGroupName);
            sb.append(" disappered on next query ");
            Log.w("WifiP2pSettings", sb.toString());
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof WifiP2pPeer) {
            this.mSelectedWifiPeer = (WifiP2pPeer)preference;
            if (this.mSelectedWifiPeer.device.status == 0) {
                this.showDialog(1);
            }
            else if (this.mSelectedWifiPeer.device.status == 1) {
                this.showDialog(2);
            }
            else {
                final WifiP2pConfig wifiP2pConfig = new WifiP2pConfig();
                wifiP2pConfig.deviceAddress = this.mSelectedWifiPeer.device.deviceAddress;
                final int int1 = SystemProperties.getInt("wifidirect.wps", -1);
                if (int1 != -1) {
                    wifiP2pConfig.wps.setup = int1;
                }
                else if (this.mSelectedWifiPeer.device.wpsPbcSupported()) {
                    wifiP2pConfig.wps.setup = 0;
                }
                else if (this.mSelectedWifiPeer.device.wpsKeypadSupported()) {
                    wifiP2pConfig.wps.setup = 2;
                }
                else {
                    wifiP2pConfig.wps.setup = 1;
                }
                this.mWifiP2pManager.connect(this.mChannel, wifiP2pConfig, (WifiP2pManager$ActionListener)new WifiP2pManager$ActionListener() {
                    public void onFailure(final int n) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(" connect fail ");
                        sb.append(n);
                        Log.e("WifiP2pSettings", sb.toString());
                        Toast.makeText((Context)WifiP2pSettings.this.getActivity(), 2131890067, 0).show();
                    }
                    
                    public void onSuccess() {
                    }
                });
            }
        }
        else if (preference instanceof WifiP2pPersistentGroup) {
            this.mSelectedGroup = (WifiP2pPersistentGroup)preference;
            this.showDialog(4);
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        final MenuItem item = menu.findItem(1);
        final MenuItem item2 = menu.findItem(2);
        if (this.mWifiP2pEnabled) {
            item.setEnabled(true);
            item2.setEnabled(true);
        }
        else {
            item.setEnabled(false);
            item2.setEnabled(false);
        }
        if (this.mWifiP2pSearching) {
            item.setTitle(2131890071);
        }
        else {
            item.setTitle(2131890070);
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mIntentFilter.addAction("android.net.wifi.p2p.STATE_CHANGED");
        this.mIntentFilter.addAction("android.net.wifi.p2p.PEERS_CHANGED");
        this.mIntentFilter.addAction("android.net.wifi.p2p.CONNECTION_STATE_CHANGE");
        this.mIntentFilter.addAction("android.net.wifi.p2p.THIS_DEVICE_CHANGED");
        this.mIntentFilter.addAction("android.net.wifi.p2p.DISCOVERY_STATE_CHANGE");
        this.mIntentFilter.addAction("android.net.wifi.p2p.PERSISTENT_GROUPS_CHANGED");
        this.getPreferenceScreen();
        this.getActivity().registerReceiver(this.mReceiver, this.mIntentFilter);
        if (this.mWifiP2pManager != null) {
            this.mWifiP2pManager.requestPeers(this.mChannel, (WifiP2pManager$PeerListListener)this);
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        if (this.mSelectedWifiPeer != null) {
            bundle.putParcelable("PEER_STATE", (Parcelable)this.mSelectedWifiPeer.device);
        }
        if (this.mDeviceNameText != null) {
            bundle.putString("DEV_NAME", this.mDeviceNameText.getText().toString());
        }
        if (this.mSelectedGroup != null) {
            bundle.putString("GROUP_NAME", this.mSelectedGroup.getGroupName());
        }
    }
}
