package com.android.settings.wifi.p2p;

import android.support.v7.preference.PreferenceScreen;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v7.preference.Preference;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class WifiP2pPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final IntentFilter mFilter;
    final BroadcastReceiver mReceiver;
    private Preference mWifiDirectPref;
    private final WifiManager mWifiManager;
    
    public WifiP2pPreferenceController(final Context context, final Lifecycle lifecycle, final WifiManager mWifiManager) {
        super(context);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                WifiP2pPreferenceController.this.togglePreferences();
            }
        };
        this.mFilter = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED");
        this.mWifiManager = mWifiManager;
        lifecycle.addObserver(this);
    }
    
    private void togglePreferences() {
        if (this.mWifiDirectPref != null) {
            this.mWifiDirectPref.setEnabled(this.mWifiManager.isWifiEnabled());
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mWifiDirectPref = preferenceScreen.findPreference("wifi_direct");
        this.togglePreferences();
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_direct";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onPause() {
        this.mContext.unregisterReceiver(this.mReceiver);
    }
    
    @Override
    public void onResume() {
        this.mContext.registerReceiver(this.mReceiver, this.mFilter);
    }
}
