package com.android.settings.wifi.p2p;

import android.graphics.drawable.Drawable;
import android.support.v7.preference.PreferenceViewHolder;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.content.Context;
import android.widget.ImageView;
import android.net.wifi.p2p.WifiP2pDevice;
import android.support.v7.preference.Preference;

public class WifiP2pPeer extends Preference
{
    private static final int[] STATE_SECURED;
    public WifiP2pDevice device;
    private final int mRssi;
    private ImageView mSignal;
    
    static {
        STATE_SECURED = new int[] { R.attr.state_encrypted };
    }
    
    public WifiP2pPeer(final Context context, final WifiP2pDevice device) {
        super(context);
        this.device = device;
        this.setWidgetLayoutResource(2131558692);
        this.mRssi = 60;
        if (TextUtils.isEmpty((CharSequence)this.device.deviceName)) {
            this.setTitle(this.device.deviceAddress);
        }
        else {
            this.setTitle(this.device.deviceName);
        }
        this.setSummary(context.getResources().getStringArray(2130903190)[this.device.status]);
    }
    
    @Override
    public int compareTo(final Preference preference) {
        final boolean b = preference instanceof WifiP2pPeer;
        int n = 1;
        if (!b) {
            return 1;
        }
        final WifiP2pPeer wifiP2pPeer = (WifiP2pPeer)preference;
        if (this.device.status != wifiP2pPeer.device.status) {
            if (this.device.status < wifiP2pPeer.device.status) {
                n = -1;
            }
            return n;
        }
        if (this.device.deviceName != null) {
            return this.device.deviceName.compareToIgnoreCase(wifiP2pPeer.device.deviceName);
        }
        return this.device.deviceAddress.compareToIgnoreCase(wifiP2pPeer.device.deviceAddress);
    }
    
    int getLevel() {
        if (this.mRssi == Integer.MAX_VALUE) {
            return -1;
        }
        return WifiManager.calculateSignalLevel(this.mRssi, 4);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.mSignal = (ImageView)preferenceViewHolder.findViewById(2131362609);
        if (this.mRssi == Integer.MAX_VALUE) {
            this.mSignal.setImageDrawable((Drawable)null);
        }
        else {
            this.mSignal.setImageResource(2131231304);
            this.mSignal.setImageState(WifiP2pPeer.STATE_SECURED, true);
        }
        this.mSignal.setImageLevel(this.getLevel());
    }
}
