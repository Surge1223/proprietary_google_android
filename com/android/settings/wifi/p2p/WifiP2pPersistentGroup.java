package com.android.settings.wifi.p2p;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pGroup;
import android.support.v7.preference.Preference;

public class WifiP2pPersistentGroup extends Preference
{
    public WifiP2pGroup mGroup;
    
    public WifiP2pPersistentGroup(final Context context, final WifiP2pGroup mGroup) {
        super(context);
        this.mGroup = mGroup;
        this.setTitle(this.mGroup.getNetworkName());
    }
    
    String getGroupName() {
        return this.mGroup.getNetworkName();
    }
    
    int getNetworkId() {
        return this.mGroup.getNetworkId();
    }
}
