package com.android.settings.wifi.p2p;

import android.text.TextUtils;
import android.net.wifi.p2p.WifiP2pDevice;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class P2pThisDevicePreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private Preference mPreference;
    
    public P2pThisDevicePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "p2p_this_device";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void setEnabled(final boolean enabled) {
        if (this.mPreference != null) {
            this.mPreference.setEnabled(enabled);
        }
    }
    
    public void updateDeviceName(final WifiP2pDevice wifiP2pDevice) {
        if (this.mPreference != null && wifiP2pDevice != null) {
            if (TextUtils.isEmpty((CharSequence)wifiP2pDevice.deviceName)) {
                this.mPreference.setTitle(wifiP2pDevice.deviceAddress);
            }
            else {
                this.mPreference.setTitle(wifiP2pDevice.deviceName);
            }
        }
    }
}
