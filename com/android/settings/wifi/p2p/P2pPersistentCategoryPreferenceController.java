package com.android.settings.wifi.p2p;

import android.content.Context;

public class P2pPersistentCategoryPreferenceController extends P2pCategoryPreferenceController
{
    public P2pPersistentCategoryPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "p2p_persistent_group";
    }
}
