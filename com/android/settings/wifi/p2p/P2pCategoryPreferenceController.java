package com.android.settings.wifi.p2p;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class P2pCategoryPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    protected PreferenceGroup mCategory;
    
    public P2pCategoryPreferenceController(final Context context) {
        super(context);
    }
    
    public void addChild(final Preference preference) {
        if (this.mCategory != null) {
            this.mCategory.addPreference(preference);
            this.mCategory.setVisible(true);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mCategory = (PreferenceGroup)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void removeAllChildren() {
        if (this.mCategory != null) {
            this.mCategory.removeAll();
            this.mCategory.setVisible(false);
        }
    }
    
    public void setEnabled(final boolean enabled) {
        if (this.mCategory != null) {
            this.mCategory.setEnabled(enabled);
        }
    }
}
