package com.android.settings.wifi;

import android.view.LayoutInflater;
import android.widget.Button;
import android.content.Context;

public interface WifiConfigUiBase
{
    void dispatchSubmit();
    
    Context getContext();
    
    Button getForgetButton();
    
    LayoutInflater getLayoutInflater();
    
    Button getSubmitButton();
    
    void setCancelButton(final CharSequence p0);
    
    void setForgetButton(final CharSequence p0);
    
    void setSubmitButton(final CharSequence p0);
    
    void setTitle(final int p0);
    
    void setTitle(final CharSequence p0);
}
