package com.android.settings.wifi;

import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.WifiInfo;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest$Builder;
import android.os.Bundle;
import android.content.ContentResolver;
import android.util.Log;
import android.provider.Settings;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import com.android.internal.app.AlertController$AlertParams;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.Network;
import android.net.ConnectivityManager;
import android.widget.CheckBox;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public final class WifiNoInternetDialog extends AlertActivity implements DialogInterface$OnClickListener
{
    private String mAction;
    private CheckBox mAlwaysAllow;
    private ConnectivityManager mCM;
    private Network mNetwork;
    private ConnectivityManager$NetworkCallback mNetworkCallback;
    private String mNetworkName;
    
    private void createDialog() {
        this.mAlert.setIcon(2131231146);
        final AlertController$AlertParams mAlertParams = this.mAlertParams;
        if ("android.net.conn.PROMPT_UNVALIDATED".equals(this.mAction)) {
            mAlertParams.mTitle = this.mNetworkName;
            mAlertParams.mMessage = this.getString(2131888418);
            mAlertParams.mPositiveButtonText = this.getString(2131890232);
            mAlertParams.mNegativeButtonText = this.getString(2131888406);
        }
        else {
            mAlertParams.mTitle = this.getString(2131888208);
            mAlertParams.mMessage = this.getString(2131888207);
            mAlertParams.mPositiveButtonText = this.getString(2131888206);
            mAlertParams.mNegativeButtonText = this.getString(2131888204);
        }
        mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
        mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
        final View inflate = LayoutInflater.from(mAlertParams.mContext).inflate(17367090, (ViewGroup)null);
        mAlertParams.mView = inflate;
        this.mAlwaysAllow = (CheckBox)inflate.findViewById(16908711);
        if ("android.net.conn.PROMPT_UNVALIDATED".equals(this.mAction)) {
            this.mAlwaysAllow.setText((CharSequence)this.getString(2131888417));
        }
        else {
            this.mAlwaysAllow.setText((CharSequence)this.getString(2131888205));
        }
        this.setupAlert();
    }
    
    private boolean isKnownAction(final Intent intent) {
        return intent.getAction().equals("android.net.conn.PROMPT_UNVALIDATED") || intent.getAction().equals("android.net.conn.PROMPT_LOST_VALIDATION");
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n != -2 && n != -1) {
            return;
        }
        final boolean checked = this.mAlwaysAllow.isChecked();
        final boolean equals = "android.net.conn.PROMPT_UNVALIDATED".equals(this.mAction);
        boolean b = false;
        boolean b2 = false;
        String s;
        String s3;
        if (equals) {
            s = "NO_INTERNET";
            if (n == -1) {
                b2 = true;
            }
            String s2;
            if (b2) {
                s2 = "Connect";
            }
            else {
                s2 = "Ignore";
            }
            this.mCM.setAcceptUnvalidated(this.mNetwork, b2, checked);
            s3 = s2;
        }
        else {
            final String s4 = "LOST_INTERNET";
            if (n == -1) {
                b = true;
            }
            String s5;
            if (b) {
                s5 = "Switch away";
            }
            else {
                s5 = "Get stuck";
            }
            if (checked) {
                final ContentResolver contentResolver = this.mAlertParams.mContext.getContentResolver();
                String s6;
                if (b) {
                    s6 = "1";
                }
                else {
                    s6 = "0";
                }
                Settings.Global.putString(contentResolver, "network_avoid_bad_wifi", s6);
                s = s4;
                s3 = s5;
            }
            else {
                s = s4;
                s3 = s5;
                if (b) {
                    this.mCM.setAvoidUnvalidated(this.mNetwork);
                    s3 = s5;
                    s = s4;
                }
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(": ");
        sb.append(s3);
        sb.append(" network=");
        sb.append(this.mNetwork);
        String s7;
        if (checked) {
            s7 = " and remember";
        }
        else {
            s7 = "";
        }
        sb.append(s7);
        Log.d("WifiNoInternetDialog", sb.toString());
    }
    
    public void onCreate(Bundle intent) {
        super.onCreate(intent);
        intent = (Bundle)this.getIntent();
        if (intent == null || !this.isKnownAction((Intent)intent) || !"netId".equals(((Intent)intent).getScheme())) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected intent ");
            sb.append(intent);
            sb.append(", exiting");
            Log.e("WifiNoInternetDialog", sb.toString());
            this.finish();
            return;
        }
        this.mAction = ((Intent)intent).getAction();
        try {
            this.mNetwork = new Network(Integer.parseInt(((Intent)intent).getData().getSchemeSpecificPart()));
        }
        catch (NullPointerException | NumberFormatException ex) {
            this.mNetwork = null;
        }
        if (this.mNetwork == null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Can't determine network from '");
            sb2.append(((Intent)intent).getData());
            sb2.append("' , exiting");
            Log.e("WifiNoInternetDialog", sb2.toString());
            this.finish();
            return;
        }
        final NetworkRequest build = new NetworkRequest$Builder().clearCapabilities().build();
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            public void onCapabilitiesChanged(final Network network, final NetworkCapabilities networkCapabilities) {
                if (WifiNoInternetDialog.this.mNetwork.equals((Object)network) && networkCapabilities.hasCapability(16)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Network ");
                    sb.append(WifiNoInternetDialog.this.mNetwork);
                    sb.append(" validated");
                    Log.d("WifiNoInternetDialog", sb.toString());
                    WifiNoInternetDialog.this.finish();
                }
            }
            
            public void onLost(final Network network) {
                if (WifiNoInternetDialog.this.mNetwork.equals((Object)network)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Network ");
                    sb.append(WifiNoInternetDialog.this.mNetwork);
                    sb.append(" disconnected");
                    Log.d("WifiNoInternetDialog", sb.toString());
                    WifiNoInternetDialog.this.finish();
                }
            }
        };
        (this.mCM = (ConnectivityManager)this.getSystemService("connectivity")).registerNetworkCallback(build, this.mNetworkCallback);
        final NetworkInfo networkInfo = this.mCM.getNetworkInfo(this.mNetwork);
        final NetworkCapabilities networkCapabilities = this.mCM.getNetworkCapabilities(this.mNetwork);
        if (networkInfo != null && networkInfo.isConnectedOrConnecting() && networkCapabilities != null) {
            this.mNetworkName = networkCapabilities.getSSID();
            if (this.mNetworkName != null) {
                this.mNetworkName = WifiInfo.removeDoubleQuotes(this.mNetworkName);
            }
            this.createDialog();
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Network ");
        sb3.append(this.mNetwork);
        sb3.append(" is not connected: ");
        sb3.append(networkInfo);
        Log.d("WifiNoInternetDialog", sb3.toString());
        this.finish();
    }
    
    protected void onDestroy() {
        if (this.mNetworkCallback != null) {
            this.mCM.unregisterNetworkCallback(this.mNetworkCallback);
            this.mNetworkCallback = null;
        }
        super.onDestroy();
    }
}
