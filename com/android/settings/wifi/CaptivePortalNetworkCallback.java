package com.android.settings.wifi;

import android.net.NetworkCapabilities;
import com.android.internal.util.Preconditions;
import android.net.Network;
import android.net.ConnectivityManager$NetworkCallback;

final class CaptivePortalNetworkCallback extends ConnectivityManager$NetworkCallback
{
    private final ConnectedAccessPointPreference mConnectedApPreference;
    private boolean mIsCaptivePortal;
    private final Network mNetwork;
    
    CaptivePortalNetworkCallback(final Network network, final ConnectedAccessPointPreference connectedAccessPointPreference) {
        this.mNetwork = (Network)Preconditions.checkNotNull((Object)network);
        this.mConnectedApPreference = (ConnectedAccessPointPreference)Preconditions.checkNotNull((Object)connectedAccessPointPreference);
    }
    
    public Network getNetwork() {
        return this.mNetwork;
    }
    
    public boolean isCaptivePortal() {
        return this.mIsCaptivePortal;
    }
    
    public boolean isSameNetworkAndPreference(final Network network, final ConnectedAccessPointPreference connectedAccessPointPreference) {
        return this.mNetwork.equals((Object)network) && this.mConnectedApPreference == connectedAccessPointPreference;
    }
    
    public void onCapabilitiesChanged(final Network network, final NetworkCapabilities networkCapabilities) {
        if (this.mNetwork.equals((Object)network)) {
            this.mIsCaptivePortal = WifiUtils.canSignIntoNetwork(networkCapabilities);
            this.mConnectedApPreference.setCaptivePortal(this.mIsCaptivePortal);
        }
    }
    
    public void onLost(final Network network) {
        if (this.mNetwork.equals((Object)network)) {
            this.mIsCaptivePortal = false;
        }
    }
}
