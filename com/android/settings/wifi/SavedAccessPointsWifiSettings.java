package com.android.settings.wifi;

import android.app.Dialog;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;
import android.graphics.drawable.Drawable;
import android.app.Fragment;
import android.support.v7.preference.PreferenceGroup;
import java.util.List;
import java.util.Collections;
import com.android.settingslib.wifi.WifiSavedConfigUtils;
import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import android.os.Message;
import android.icu.text.Collator;
import android.net.wifi.WifiManager;
import com.android.settingslib.wifi.AccessPointPreference;
import android.os.Handler;
import android.net.wifi.WifiManager$ActionListener;
import android.support.v7.preference.Preference;
import android.os.Bundle;
import com.android.settingslib.wifi.AccessPoint;
import java.util.Comparator;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public class SavedAccessPointsWifiSettings extends SettingsPreferenceFragment implements Indexable, WifiDialogListener
{
    static final int MSG_UPDATE_PREFERENCES = 1;
    private static final Comparator<AccessPoint> SAVED_NETWORK_COMPARATOR;
    private Bundle mAccessPointSavedState;
    private Preference mAddNetworkPreference;
    private WifiDialog mDialog;
    private AccessPoint mDlgAccessPoint;
    final WifiManager$ActionListener mForgetListener;
    final Handler mHandler;
    private final WifiManager$ActionListener mSaveListener;
    private AccessPoint mSelectedAccessPoint;
    private AccessPointPreference.UserBadgeCache mUserBadgeCache;
    private WifiManager mWifiManager;
    
    static {
        SAVED_NETWORK_COMPARATOR = new Comparator<AccessPoint>() {
            final Collator mCollator = Collator.getInstance();
            
            private String nullToEmpty(String s) {
                if (s == null) {
                    s = "";
                }
                return s;
            }
            
            @Override
            public int compare(final AccessPoint accessPoint, final AccessPoint accessPoint2) {
                return this.mCollator.compare(this.nullToEmpty(accessPoint.getConfigName()), this.nullToEmpty(accessPoint2.getConfigName()));
            }
        };
    }
    
    public SavedAccessPointsWifiSettings() {
        this.mForgetListener = (WifiManager$ActionListener)new WifiManager$ActionListener() {
            public void onFailure(final int n) {
                SavedAccessPointsWifiSettings.this.postUpdatePreference();
            }
            
            public void onSuccess() {
                SavedAccessPointsWifiSettings.this.postUpdatePreference();
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                if (message.what == 1) {
                    SavedAccessPointsWifiSettings.this.initPreferences();
                }
            }
        };
        this.mSaveListener = (WifiManager$ActionListener)new WifiManager$ActionListener() {
            public void onFailure(final int n) {
                final Activity activity = SavedAccessPointsWifiSettings.this.getActivity();
                if (activity != null) {
                    Toast.makeText((Context)activity, 2131889997, 0).show();
                }
            }
            
            public void onSuccess() {
                SavedAccessPointsWifiSettings.this.postUpdatePreference();
            }
        };
    }
    
    private void initPreferences() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        final Context prefContext = this.getPrefContext();
        final List<AccessPoint> allConfigs = WifiSavedConfigUtils.getAllConfigs(prefContext, this.mWifiManager);
        Collections.sort((List<Object>)allConfigs, (Comparator<? super Object>)SavedAccessPointsWifiSettings.SAVED_NETWORK_COMPARATOR);
        this.cacheRemoveAllPrefs(preferenceScreen);
        final int size = allConfigs.size();
        for (int i = 0; i < size; ++i) {
            final AccessPoint accessPoint = allConfigs.get(i);
            final String key = accessPoint.getKey();
            LongPressAccessPointPreference longPressAccessPointPreference;
            if ((longPressAccessPointPreference = (LongPressAccessPointPreference)this.getCachedPreference(key)) == null) {
                longPressAccessPointPreference = new LongPressAccessPointPreference(accessPoint, prefContext, this.mUserBadgeCache, true, this);
                longPressAccessPointPreference.setKey(key);
                longPressAccessPointPreference.setIcon(null);
                preferenceScreen.addPreference(longPressAccessPointPreference);
            }
            longPressAccessPointPreference.setOrder(i);
        }
        this.removeCachedPrefs(preferenceScreen);
        if (this.mAddNetworkPreference == null) {
            (this.mAddNetworkPreference = new Preference(this.getPrefContext())).setIcon(2131231065);
            this.mAddNetworkPreference.setTitle(2131889889);
        }
        this.mAddNetworkPreference.setOrder(size);
        preferenceScreen.addPreference(this.mAddNetworkPreference);
        if (this.getPreferenceScreen().getPreferenceCount() < 1) {
            Log.w("SavedAccessPoints", "Saved networks activity loaded, but there are no saved networks!");
        }
    }
    
    private void postUpdatePreference() {
        if (!this.mHandler.hasMessages(1)) {
            this.mHandler.sendEmptyMessage(1);
        }
    }
    
    private void showWifiDialog(final LongPressAccessPointPreference longPressAccessPointPreference) {
        if (this.mDialog != null) {
            this.removeDialog(1);
            this.mDialog = null;
        }
        if (longPressAccessPointPreference != null) {
            this.mDlgAccessPoint = longPressAccessPointPreference.getAccessPoint();
        }
        else {
            this.mDlgAccessPoint = null;
            this.mAccessPointSavedState = null;
        }
        this.showDialog(1);
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        if (n != 1) {
            return 0;
        }
        return 602;
    }
    
    @Override
    public int getMetricsCategory() {
        return 106;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.mWifiManager = (WifiManager)this.getSystemService("wifi");
        if (bundle != null && bundle.containsKey("wifi_ap_state")) {
            this.mAccessPointSavedState = bundle.getBundle("wifi_ap_state");
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082869);
        this.mUserBadgeCache = new AccessPointPreference.UserBadgeCache(this.getPackageManager());
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        if (n != 1) {
            return super.onCreateDialog(n);
        }
        if (this.mDlgAccessPoint == null && this.mAccessPointSavedState == null) {
            this.mDialog = WifiDialog.createFullscreen((Context)this.getActivity(), (WifiDialog.WifiDialogListener)this, null, 1);
        }
        else {
            if (this.mDlgAccessPoint == null) {
                this.mDlgAccessPoint = new AccessPoint((Context)this.getActivity(), this.mAccessPointSavedState);
                this.mAccessPointSavedState = null;
            }
            this.mDialog = WifiDialog.createModal((Context)this.getActivity(), (WifiDialog.WifiDialogListener)this, this.mDlgAccessPoint, 0);
        }
        this.mSelectedAccessPoint = this.mDlgAccessPoint;
        return (Dialog)this.mDialog;
    }
    
    @Override
    public void onForget(final WifiDialog wifiDialog) {
        if (this.mSelectedAccessPoint != null) {
            if (this.mSelectedAccessPoint.isPasspointConfig()) {
                try {
                    this.mWifiManager.removePasspointConfiguration(this.mSelectedAccessPoint.getPasspointFqdn());
                }
                catch (RuntimeException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to remove Passpoint configuration for ");
                    sb.append(this.mSelectedAccessPoint.getConfigName());
                    Log.e("SavedAccessPoints", sb.toString());
                }
                this.postUpdatePreference();
            }
            else {
                this.mWifiManager.forget(this.mSelectedAccessPoint.getConfig().networkId, this.mForgetListener);
            }
            this.mSelectedAccessPoint = null;
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof LongPressAccessPointPreference) {
            this.showWifiDialog((LongPressAccessPointPreference)preference);
            return true;
        }
        if (preference == this.mAddNetworkPreference) {
            this.showWifiDialog(null);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.initPreferences();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mDialog != null && this.mDialog.isShowing() && this.mDlgAccessPoint != null) {
            this.mAccessPointSavedState = new Bundle();
            this.mDlgAccessPoint.saveWifiState(this.mAccessPointSavedState);
            bundle.putBundle("wifi_ap_state", this.mAccessPointSavedState);
        }
    }
    
    @Override
    public void onSubmit(final WifiDialog wifiDialog) {
        this.mWifiManager.save(wifiDialog.getController().getConfig(), this.mSaveListener);
    }
}
