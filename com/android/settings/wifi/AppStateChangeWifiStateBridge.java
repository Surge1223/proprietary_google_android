package com.android.settings.wifi;

import java.util.Iterator;
import com.android.settings.applications.AppStateBaseBridge;
import android.content.Context;
import com.android.internal.util.ArrayUtils;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.AppStateAppOpsBridge;

public class AppStateChangeWifiStateBridge extends AppStateAppOpsBridge
{
    public static final AppFilter FILTER_CHANGE_WIFI_STATE;
    private static final String[] PM_PERMISSIONS;
    
    static {
        PM_PERMISSIONS = new String[] { "android.permission.CHANGE_WIFI_STATE" };
        FILTER_CHANGE_WIFI_STATE = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                if (appEntry != null && appEntry.extraInfo != null) {
                    final WifiSettingsState wifiSettingsState = (WifiSettingsState)appEntry.extraInfo;
                    return (wifiSettingsState.packageInfo == null || !ArrayUtils.contains((Object[])wifiSettingsState.packageInfo.requestedPermissions, (Object)"android.permission.NETWORK_SETTINGS")) && wifiSettingsState.permissionDeclared;
                }
                return false;
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    public AppStateChangeWifiStateBridge(final Context context, final ApplicationsState applicationsState, final Callback callback) {
        super(context, applicationsState, callback, 71, AppStateChangeWifiStateBridge.PM_PERMISSIONS);
    }
    
    public WifiSettingsState getWifiSettingsInfo(final String s, final int n) {
        return new WifiSettingsState(super.getPermissionInfo(s, n));
    }
    
    @Override
    protected void loadAllExtraInfo() {
        for (final AppEntry appEntry : this.mAppSession.getAllApps()) {
            this.updateExtraInfo(appEntry, appEntry.info.packageName, appEntry.info.uid);
        }
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = this.getWifiSettingsInfo(s, n);
    }
    
    public static class WifiSettingsState extends PermissionState
    {
        public WifiSettingsState(final PermissionState permissionState) {
            super(permissionState.packageName, permissionState.userHandle);
            this.packageInfo = permissionState.packageInfo;
            this.appOpMode = permissionState.appOpMode;
            this.permissionDeclared = permissionState.permissionDeclared;
            this.staticPermissionGranted = permissionState.staticPermissionGranted;
        }
    }
}
