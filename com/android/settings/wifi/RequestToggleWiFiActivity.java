package com.android.settings.wifi;

import android.app.Activity;
import android.widget.Toast;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.pm.PackageManager;
import android.util.Log;
import android.text.TextUtils;
import android.os.Bundle;
import android.content.DialogInterface;
import android.os.Message;
import android.net.wifi.WifiManager;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public class RequestToggleWiFiActivity extends AlertActivity implements DialogInterface$OnClickListener
{
    private CharSequence mAppLabel;
    private int mLastUpdateState;
    private final StateChangeReceiver mReceiver;
    private int mState;
    private final Runnable mTimeoutCommand;
    private WifiManager mWiFiManager;
    
    public RequestToggleWiFiActivity() {
        this.mReceiver = new StateChangeReceiver();
        this.mTimeoutCommand = new _$$Lambda$RequestToggleWiFiActivity$PwZgoHTFFBr3iYEQbWj0vZPfHpw(this);
        this.mState = -1;
        this.mLastUpdateState = -1;
    }
    
    private void scheduleToggleTimeout() {
        this.getWindow().getDecorView().postDelayed(this.mTimeoutCommand, 10000L);
    }
    
    private void unscheduleToggleTimeout() {
        this.getWindow().getDecorView().removeCallbacks(this.mTimeoutCommand);
    }
    
    private void updateUi() {
        if (this.mLastUpdateState == this.mState) {
            return;
        }
        this.mLastUpdateState = this.mState;
        switch (this.mState) {
            case 4: {
                this.mAlert.setButton(-1, (CharSequence)null, (DialogInterface$OnClickListener)null, (Message)null);
                this.mAlert.setButton(-2, (CharSequence)null, (DialogInterface$OnClickListener)null, (Message)null);
                this.mAlertParams.mPositiveButtonText = null;
                this.mAlertParams.mPositiveButtonListener = null;
                this.mAlertParams.mNegativeButtonText = null;
                this.mAlertParams.mNegativeButtonListener = null;
                this.mAlertParams.mMessage = this.getString(2131890172);
                break;
            }
            case 3: {
                this.mAlertParams.mPositiveButtonText = this.getString(2131886285);
                this.mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
                this.mAlertParams.mNegativeButtonText = this.getString(2131887370);
                this.mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
                this.mAlertParams.mMessage = this.getString(2131889907, new Object[] { this.mAppLabel });
                break;
            }
            case 2: {
                this.mAlert.setButton(-1, (CharSequence)null, (DialogInterface$OnClickListener)null, (Message)null);
                this.mAlert.setButton(-2, (CharSequence)null, (DialogInterface$OnClickListener)null, (Message)null);
                this.mAlertParams.mPositiveButtonText = null;
                this.mAlertParams.mPositiveButtonListener = null;
                this.mAlertParams.mNegativeButtonText = null;
                this.mAlertParams.mNegativeButtonListener = null;
                this.mAlertParams.mMessage = this.getString(2131890160);
                break;
            }
            case 1: {
                this.mAlertParams.mPositiveButtonText = this.getString(2131886285);
                this.mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
                this.mAlertParams.mNegativeButtonText = this.getString(2131887370);
                this.mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this;
                this.mAlertParams.mMessage = this.getString(2131889908, new Object[] { this.mAppLabel });
                break;
            }
        }
        this.setupAlert();
    }
    
    public void dismiss() {
    }
    
    public void onClick(final DialogInterface dialogInterface, int mState) {
        switch (mState) {
            case -1: {
                mState = this.mState;
                if (mState != 1) {
                    if (mState == 3) {
                        this.mWiFiManager.setWifiEnabled(false);
                        this.mState = 4;
                        this.scheduleToggleTimeout();
                        this.updateUi();
                    }
                }
                else {
                    this.mWiFiManager.setWifiEnabled(true);
                    this.mState = 2;
                    this.scheduleToggleTimeout();
                    this.updateUi();
                }
                break;
            }
            case -2: {
                this.finish();
                break;
            }
        }
    }
    
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mWiFiManager = (WifiManager)this.getSystemService((Class)WifiManager.class);
        int n = 0;
        this.setResult(0);
        bundle = (Bundle)this.getIntent().getStringExtra("android.intent.extra.PACKAGE_NAME");
        if (TextUtils.isEmpty((CharSequence)bundle)) {
            this.finish();
            return;
        }
        try {
            this.mAppLabel = this.getPackageManager().getApplicationInfo((String)bundle, 0).loadSafeLabel(this.getPackageManager());
            bundle = (Bundle)this.getIntent().getAction();
            final int hashCode = ((String)bundle).hashCode();
            Label_0123: {
                if (hashCode != -2035256254) {
                    if (hashCode == 317500393) {
                        if (((String)bundle).equals("android.net.wifi.action.REQUEST_DISABLE")) {
                            n = 1;
                            break Label_0123;
                        }
                    }
                }
                else if (((String)bundle).equals("android.net.wifi.action.REQUEST_ENABLE")) {
                    break Label_0123;
                }
                n = -1;
            }
            switch (n) {
                default: {
                    this.finish();
                    break;
                }
                case 1: {
                    this.mState = 3;
                    break;
                }
                case 0: {
                    this.mState = 1;
                    break;
                }
            }
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Couldn't find app with package name ");
            sb.append((String)bundle);
            Log.e("RequestToggleWiFiActivity", sb.toString());
            this.finish();
        }
    }
    
    protected void onStart() {
        super.onStart();
        this.mReceiver.register();
        final int wifiState = this.mWiFiManager.getWifiState();
        Label_0274: {
            switch (this.mState) {
                case 4: {
                    switch (wifiState) {
                        default: {
                            break Label_0274;
                        }
                        case 2:
                        case 3: {
                            this.mState = 3;
                            break Label_0274;
                        }
                        case 1: {
                            this.setResult(-1);
                            this.finish();
                            return;
                        }
                        case 0: {
                            this.scheduleToggleTimeout();
                            break Label_0274;
                        }
                    }
                    break;
                }
                case 3: {
                    switch (wifiState) {
                        case 2: {
                            this.mState = 4;
                            this.scheduleToggleTimeout();
                            break;
                        }
                        case 1: {
                            this.setResult(-1);
                            this.finish();
                            return;
                        }
                    }
                    break;
                }
                case 2: {
                    switch (wifiState) {
                        case 3: {
                            this.setResult(-1);
                            this.finish();
                            return;
                        }
                        case 2: {
                            this.scheduleToggleTimeout();
                            break;
                        }
                        case 0:
                        case 1: {
                            this.mState = 1;
                            break;
                        }
                    }
                    break;
                }
                case 1: {
                    switch (wifiState) {
                        default: {
                            break Label_0274;
                        }
                        case 3: {
                            this.setResult(-1);
                            this.finish();
                            return;
                        }
                        case 2: {
                            this.mState = 2;
                            this.scheduleToggleTimeout();
                            break Label_0274;
                        }
                    }
                    break;
                }
            }
        }
        this.updateUi();
    }
    
    protected void onStop() {
        this.mReceiver.unregister();
        this.unscheduleToggleTimeout();
        super.onStop();
    }
    
    private final class StateChangeReceiver extends BroadcastReceiver
    {
        private final IntentFilter mFilter;
        
        private StateChangeReceiver() {
            this.mFilter = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED");
        }
        
        public void onReceive(final Context context, final Intent intent) {
            final RequestToggleWiFiActivity this$0 = RequestToggleWiFiActivity.this;
            if (!((Activity)this$0).isFinishing() && !((Activity)this$0).isDestroyed()) {
                final int wifiState = RequestToggleWiFiActivity.this.mWiFiManager.getWifiState();
                if (wifiState != 3) {
                    switch (wifiState) {
                        default: {
                            return;
                        }
                        case 0: {
                            Toast.makeText((Context)this$0, 2131889993, 0).show();
                            RequestToggleWiFiActivity.this.finish();
                            return;
                        }
                        case 1: {
                            break;
                        }
                    }
                }
                if (RequestToggleWiFiActivity.this.mState == 2 || RequestToggleWiFiActivity.this.mState == 4) {
                    RequestToggleWiFiActivity.this.setResult(-1);
                    RequestToggleWiFiActivity.this.finish();
                }
            }
        }
        
        public void register() {
            RequestToggleWiFiActivity.this.registerReceiver((BroadcastReceiver)this, this.mFilter);
        }
        
        public void unregister() {
            RequestToggleWiFiActivity.this.unregisterReceiver((BroadcastReceiver)this);
        }
    }
}
