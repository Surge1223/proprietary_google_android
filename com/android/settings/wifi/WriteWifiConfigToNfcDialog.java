package com.android.settings.wifi;

import android.content.DialogInterface$OnClickListener;
import android.view.ViewGroup;
import android.app.Activity;
import android.view.inputmethod.InputMethodManager;
import android.nfc.NfcAdapter$ReaderCallback;
import android.nfc.NfcAdapter;
import android.widget.CompoundButton;
import android.text.Editable;
import java.io.IOException;
import android.nfc.FormatException;
import android.util.Log;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.tech.Ndef;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.PowerManager;
import android.net.wifi.WifiManager;
import android.os.PowerManager$WakeLock;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.CheckBox;
import android.widget.TextView;
import android.content.Context;
import android.widget.Button;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.View.OnClickListener;
import android.text.TextWatcher;
import android.app.AlertDialog;

class WriteWifiConfigToNfcDialog extends AlertDialog implements TextWatcher, View.OnClickListener, CompoundButton$OnCheckedChangeListener
{
    private static final String TAG;
    private static final char[] hexArray;
    private Button mCancelButton;
    private Context mContext;
    private TextView mLabelView;
    private CheckBox mPasswordCheckBox;
    private TextView mPasswordView;
    private ProgressBar mProgressBar;
    private int mSecurity;
    private Button mSubmitButton;
    private View mView;
    private final PowerManager$WakeLock mWakeLock;
    private WifiManager mWifiManager;
    private String mWpsNfcConfigurationToken;
    
    static {
        TAG = WriteWifiConfigToNfcDialog.class.getName().toString();
        hexArray = "0123456789ABCDEF".toCharArray();
    }
    
    WriteWifiConfigToNfcDialog(final Context mContext, final int mSecurity) {
        super(mContext);
        this.mContext = mContext;
        this.mWakeLock = ((PowerManager)mContext.getSystemService("power")).newWakeLock(1, "WriteWifiConfigToNfcDialog:wakeLock");
        this.mSecurity = mSecurity;
        this.mWifiManager = (WifiManager)mContext.getSystemService("wifi");
    }
    
    WriteWifiConfigToNfcDialog(final Context mContext, final Bundle bundle) {
        super(mContext);
        this.mContext = mContext;
        this.mWakeLock = ((PowerManager)mContext.getSystemService("power")).newWakeLock(1, "WriteWifiConfigToNfcDialog:wakeLock");
        this.mSecurity = bundle.getInt("security");
        this.mWifiManager = (WifiManager)mContext.getSystemService("wifi");
    }
    
    private static String byteArrayToHexString(final byte[] array) {
        final char[] array2 = new char[array.length * 2];
        for (int i = 0; i < array.length; ++i) {
            final int n = array[i] & 0xFF;
            array2[i * 2] = WriteWifiConfigToNfcDialog.hexArray[n >>> 4];
            array2[i * 2 + 1] = WriteWifiConfigToNfcDialog.hexArray[n & 0xF];
        }
        return new String(array2);
    }
    
    private void enableSubmitIfAppropriate() {
        final TextView mPasswordView = this.mPasswordView;
        final boolean b = false;
        boolean enabled = false;
        if (mPasswordView != null) {
            if (this.mSecurity == 1) {
                final Button mSubmitButton = this.mSubmitButton;
                if (this.mPasswordView.length() > 0) {
                    enabled = true;
                }
                mSubmitButton.setEnabled(enabled);
            }
            else if (this.mSecurity == 2) {
                final Button mSubmitButton2 = this.mSubmitButton;
                boolean enabled2 = b;
                if (this.mPasswordView.length() >= 8) {
                    enabled2 = true;
                }
                mSubmitButton2.setEnabled(enabled2);
            }
        }
        else {
            this.mSubmitButton.setEnabled(false);
        }
    }
    
    private void handleWriteNfcEvent(final Tag tag) {
        final Ndef value = Ndef.get(tag);
        if (value != null) {
            if (value.isWritable()) {
                final NdefRecord mime = NdefRecord.createMime("application/vnd.wfa.wsc", hexStringToByteArray(this.mWpsNfcConfigurationToken));
                try {
                    value.connect();
                    value.writeNdefMessage(new NdefMessage(mime, new NdefRecord[0]));
                    this.getOwnerActivity().runOnUiThread((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            WriteWifiConfigToNfcDialog.this.mProgressBar.setVisibility(8);
                        }
                    });
                    this.setViewText(this.mLabelView, 2131889253);
                    this.setViewText((TextView)this.mCancelButton, 17039811);
                    return;
                }
                catch (FormatException ex) {
                    this.setViewText(this.mLabelView, 2131889229);
                    Log.e(WriteWifiConfigToNfcDialog.TAG, "Unable to write Wi-Fi config to NFC tag.", (Throwable)ex);
                    return;
                }
                catch (IOException ex2) {
                    this.setViewText(this.mLabelView, 2131889229);
                    Log.e(WriteWifiConfigToNfcDialog.TAG, "Unable to write Wi-Fi config to NFC tag.", (Throwable)ex2);
                    return;
                }
            }
            this.setViewText(this.mLabelView, 2131889248);
            Log.e(WriteWifiConfigToNfcDialog.TAG, "Tag is not writable");
        }
        else {
            this.setViewText(this.mLabelView, 2131889248);
            Log.e(WriteWifiConfigToNfcDialog.TAG, "Tag does not support NDEF");
        }
    }
    
    private static byte[] hexStringToByteArray(final String s) {
        final int length = s.length();
        final byte[] array = new byte[length / 2];
        for (int i = 0; i < length; i += 2) {
            array[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return array;
    }
    
    private void setViewText(final TextView textView, final int n) {
        this.getOwnerActivity().runOnUiThread((Runnable)new Runnable() {
            @Override
            public void run() {
                textView.setText(n);
            }
        });
    }
    
    public void afterTextChanged(final Editable editable) {
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    public void dismiss() {
        if (this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
        super.dismiss();
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        final TextView mPasswordView = this.mPasswordView;
        int n;
        if (b) {
            n = 144;
        }
        else {
            n = 128;
        }
        mPasswordView.setInputType(n | 0x1);
    }
    
    public void onClick(final View view) {
        this.mWakeLock.acquire();
        final String string = this.mPasswordView.getText().toString();
        final String currentNetworkWpsNfcConfigurationToken = this.mWifiManager.getCurrentNetworkWpsNfcConfigurationToken();
        final String byteArrayToHexString = byteArrayToHexString(string.getBytes());
        String s;
        if (string.length() >= 16) {
            s = Integer.toString(string.length(), 16);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("0");
            sb.append(Character.forDigit(string.length(), 16));
            s = sb.toString();
        }
        final String lowerCase = String.format("102700%s%s", s, byteArrayToHexString).toLowerCase();
        if (currentNetworkWpsNfcConfigurationToken != null && currentNetworkWpsNfcConfigurationToken.contains(lowerCase)) {
            this.mWpsNfcConfigurationToken = currentNetworkWpsNfcConfigurationToken;
            final Activity ownerActivity = this.getOwnerActivity();
            NfcAdapter.getDefaultAdapter((Context)ownerActivity).enableReaderMode(ownerActivity, (NfcAdapter$ReaderCallback)new NfcAdapter$ReaderCallback() {
                public void onTagDiscovered(final Tag tag) {
                    WriteWifiConfigToNfcDialog.this.handleWriteNfcEvent(tag);
                }
            }, 31, (Bundle)null);
            this.mPasswordView.setVisibility(8);
            this.mPasswordCheckBox.setVisibility(8);
            this.mSubmitButton.setVisibility(8);
            ((InputMethodManager)this.getOwnerActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.mPasswordView.getWindowToken(), 0);
            this.mLabelView.setText(2131889222);
            this.mView.findViewById(2131362441).setTextAlignment(4);
            this.mProgressBar.setVisibility(0);
        }
        else {
            this.mLabelView.setText(2131889234);
        }
    }
    
    public void onCreate(final Bundle bundle) {
        this.setView(this.mView = this.getLayoutInflater().inflate(2131558897, (ViewGroup)null));
        this.setInverseBackgroundForced(true);
        this.setTitle(2131889076);
        this.setCancelable(true);
        final String string = this.mContext.getResources().getString(2131890230);
        final DialogInterface$OnClickListener dialogInterface$OnClickListener = null;
        this.setButton(-3, (CharSequence)string, dialogInterface$OnClickListener);
        this.setButton(-2, (CharSequence)this.mContext.getResources().getString(17039360), dialogInterface$OnClickListener);
        this.mPasswordView = (TextView)this.mView.findViewById(2131362436);
        this.mLabelView = (TextView)this.mView.findViewById(2131362440);
        this.mPasswordView.addTextChangedListener((TextWatcher)this);
        (this.mPasswordCheckBox = (CheckBox)this.mView.findViewById(2131362606)).setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
        this.mProgressBar = (ProgressBar)this.mView.findViewById(2131362484);
        super.onCreate(bundle);
        (this.mSubmitButton = this.getButton(-3)).setOnClickListener((View.OnClickListener)this);
        this.mSubmitButton.setEnabled(false);
        this.mCancelButton = this.getButton(-2);
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        this.enableSubmitIfAppropriate();
    }
    
    public void saveState(final Bundle bundle) {
        bundle.putInt("security", this.mSecurity);
    }
}
