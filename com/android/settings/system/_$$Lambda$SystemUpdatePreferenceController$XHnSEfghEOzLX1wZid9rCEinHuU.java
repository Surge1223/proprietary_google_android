package com.android.settings.system;

import android.telephony.CarrierConfigManager;
import android.support.v7.preference.Preference;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import android.os.Build.VERSION;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import android.text.TextUtils;
import android.os.PersistableBundle;
import android.content.Context;
import android.os.SystemUpdateManager;
import android.os.UserManager;
import com.android.settings.core.BasePreferenceController;
import java.util.concurrent.Callable;

public final class _$$Lambda$SystemUpdatePreferenceController$XHnSEfghEOzLX1wZid9rCEinHuU implements Callable
{
    @Override
    public final Object call() {
        return SystemUpdatePreferenceController.lambda$getSummary$0(this.f$0);
    }
}
