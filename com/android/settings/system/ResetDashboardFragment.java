package com.android.settings.system;

import com.android.settings.network.NetworkResetPreferenceController;
import com.android.settings.applications.manageapplications.ResetAppPrefPreferenceController;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class ResetDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082820;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<FactoryResetPreferenceController> list = (ArrayList<FactoryResetPreferenceController>)new ArrayList<ResetAppPrefPreferenceController>();
        list.add((ResetAppPrefPreferenceController)new NetworkResetPreferenceController(context));
        list.add((ResetAppPrefPreferenceController)new FactoryResetPreferenceController(context));
        list.add(new ResetAppPrefPreferenceController(context, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    protected String getLogTag() {
        return "ResetDashboardFragment";
    }
    
    @Override
    public int getMetricsCategory() {
        return 924;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082820;
    }
}
