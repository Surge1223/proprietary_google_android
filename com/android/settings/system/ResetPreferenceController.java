package com.android.settings.system;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class ResetPreferenceController extends BasePreferenceController
{
    public ResetPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034160)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
