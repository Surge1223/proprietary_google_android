package com.android.settings.system;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class AdditionalSystemUpdatePreferenceController extends BasePreferenceController
{
    private static final String KEY_UPDATE_SETTING = "additional_system_update_settings";
    
    public AdditionalSystemUpdatePreferenceController(final Context context) {
        super(context, "additional_system_update_settings");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034116)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
