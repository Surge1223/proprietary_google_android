package com.android.settings.system;

import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settings.backup.BackupSettingsActivityPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class SystemDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add(new BackupSettingsActivityPreferenceController(context).getPreferenceKey());
                nonIndexableKeys.add("reset_dashboard");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082841;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private int getVisiblePreferenceCount(final PreferenceGroup preferenceGroup) {
        int n = 0;
        int n2;
        for (int i = 0; i < preferenceGroup.getPreferenceCount(); ++i, n = n2) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (preference instanceof PreferenceGroup) {
                n2 = n + this.getVisiblePreferenceCount((PreferenceGroup)preference);
            }
            else {
                n2 = n;
                if (preference.isVisible()) {
                    n2 = n + 1;
                }
            }
        }
        return n;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887831;
    }
    
    @Override
    protected String getLogTag() {
        return "SystemDashboardFrag";
    }
    
    @Override
    public int getMetricsCategory() {
        return 744;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082841;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (this.getVisiblePreferenceCount(preferenceScreen) == preferenceScreen.getInitialExpandedChildrenCount() + 1) {
            preferenceScreen.setInitialExpandedChildrenCount(Integer.MAX_VALUE);
        }
    }
}
