package com.android.settings.system;

import android.telephony.CarrierConfigManager;
import android.support.v7.preference.Preference;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import android.os.Build.VERSION;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import android.text.TextUtils;
import android.os.PersistableBundle;
import android.content.Context;
import android.os.SystemUpdateManager;
import android.os.UserManager;
import com.android.settings.core.BasePreferenceController;

public class SystemUpdatePreferenceController extends BasePreferenceController
{
    private static final String KEY_SYSTEM_UPDATE_SETTINGS = "system_update_settings";
    private static final String TAG = "SysUpdatePrefContr";
    private final UserManager mUm;
    private final SystemUpdateManager mUpdateManager;
    
    public SystemUpdatePreferenceController(final Context context) {
        super(context, "system_update_settings");
        this.mUm = UserManager.get(context);
        this.mUpdateManager = (SystemUpdateManager)context.getSystemService("system_update");
    }
    
    private void ciActionOnSysUpdate(final PersistableBundle persistableBundle) {
        final String string = persistableBundle.getString("ci_action_on_sys_update_intent_string");
        if (!TextUtils.isEmpty((CharSequence)string)) {
            final String string2 = persistableBundle.getString("ci_action_on_sys_update_extra_string");
            final String string3 = persistableBundle.getString("ci_action_on_sys_update_extra_val_string");
            final Intent intent = new Intent(string);
            if (!TextUtils.isEmpty((CharSequence)string2)) {
                intent.putExtra(string2, string3);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("ciActionOnSysUpdate: broadcasting intent ");
            sb.append(string);
            sb.append(" with extra ");
            sb.append(string2);
            sb.append(", ");
            sb.append(string3);
            Log.d("SysUpdatePrefContr", sb.toString());
            this.mContext.getApplicationContext().sendBroadcast(intent);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            Utils.updatePreferenceToSpecificActivityOrRemove(this.mContext, preferenceScreen, this.getPreferenceKey(), 1);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034165) && this.mUm.isAdminUser()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        CharSequence charSequence = this.mContext.getString(2131886314, new Object[] { Build.VERSION.RELEASE });
        final FutureTask<Bundle> futureTask = new FutureTask<Bundle>(new _$$Lambda$SystemUpdatePreferenceController$XHnSEfghEOzLX1wZid9rCEinHuU(this));
        try {
            futureTask.run();
            final Bundle bundle = futureTask.get();
            switch (bundle.getInt("status")) {
                case 2:
                case 3:
                case 4:
                case 5: {
                    charSequence = this.mContext.getText(2131886313);
                    break;
                }
                case 0: {
                    Log.d("SysUpdatePrefContr", "Update statue unknown");
                }
                case 1: {
                    final String string = bundle.getString("title");
                    if (!TextUtils.isEmpty((CharSequence)string)) {
                        charSequence = this.mContext.getString(2131886314, new Object[] { string });
                        break;
                    }
                    break;
                }
            }
            return charSequence;
        }
        catch (InterruptedException | ExecutionException ex) {
            Log.w("SysUpdatePrefContr", "Error getting system update info.");
            return charSequence;
        }
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)preference.getKey())) {
            final PersistableBundle config = ((CarrierConfigManager)this.mContext.getSystemService("carrier_config")).getConfig();
            if (config != null && config.getBoolean("ci_action_on_sys_update_bool")) {
                this.ciActionOnSysUpdate(config);
            }
        }
        return false;
    }
}
