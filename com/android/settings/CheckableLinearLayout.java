package com.android.settings;

import android.view.View;
import android.util.TypedValue;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout implements Checkable
{
    private boolean mChecked;
    private float mDisabledAlpha;
    
    public CheckableLinearLayout(final Context context, final AttributeSet set) {
        super(context, set);
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16842803, typedValue, true);
        this.mDisabledAlpha = typedValue.getFloat();
    }
    
    private void updateChecked() {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child instanceof Checkable) {
                ((Checkable)child).setChecked(this.mChecked);
            }
        }
    }
    
    public boolean isChecked() {
        return this.mChecked;
    }
    
    public void setChecked(final boolean mChecked) {
        this.mChecked = mChecked;
        this.updateChecked();
    }
    
    public void setEnabled(final boolean enabled) {
        super.setEnabled(enabled);
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            float mDisabledAlpha;
            if (enabled) {
                mDisabledAlpha = 1.0f;
            }
            else {
                mDisabledAlpha = this.mDisabledAlpha;
            }
            child.setAlpha(mDisabledAlpha);
        }
    }
    
    public void toggle() {
        this.setChecked(this.mChecked ^ true);
    }
}
