package com.android.settings.enterprise;

import android.content.Context;

public class AdminGrantedMicrophonePermissionPreferenceController extends AdminGrantedPermissionsPreferenceControllerBase
{
    public AdminGrantedMicrophonePermissionPreferenceController(final Context context, final boolean b) {
        super(context, b, new String[] { "android.permission.RECORD_AUDIO" });
    }
    
    @Override
    public String getPreferenceKey() {
        return "enterprise_privacy_number_microphone_access_packages";
    }
}
