package com.android.settings.enterprise;

import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import android.support.v7.preference.Preference;
import com.android.settings.applications.ApplicationFeatureProvider;

public final class _$$Lambda$EnterpriseInstalledPackagesPreferenceController$ywnQ5T98AEytxQMBHl3WTR7fuAo implements NumberOfAppsCallback
{
    @Override
    public final void onNumberOfAppsResult(final int n) {
        EnterpriseInstalledPackagesPreferenceController.lambda$updateState$0(this.f$0, this.f$1, n);
    }
}
