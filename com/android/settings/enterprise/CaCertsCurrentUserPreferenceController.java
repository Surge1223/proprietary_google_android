package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import android.content.Context;

public class CaCertsCurrentUserPreferenceController extends CaCertsPreferenceControllerBase
{
    static final String CA_CERTS_CURRENT_USER = "ca_certs_current_user";
    
    public CaCertsCurrentUserPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    protected int getNumberOfCaCerts() {
        return this.mFeatureProvider.getNumberOfOwnerInstalledCaCertsForCurrentUser();
    }
    
    @Override
    public String getPreferenceKey() {
        return "ca_certs_current_user";
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        int title;
        if (this.mFeatureProvider.isInCompMode()) {
            title = 2131887570;
        }
        else {
            title = 2131887569;
        }
        preference.setTitle(title);
    }
}
