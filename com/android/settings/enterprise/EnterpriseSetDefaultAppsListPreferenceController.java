package com.android.settings.enterprise;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import java.util.Iterator;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.applications.UserAppInfo;
import android.os.UserHandle;
import java.util.ArrayList;
import com.android.settings.overlay.FeatureFactory;
import java.util.Collections;
import android.content.Context;
import android.content.pm.UserInfo;
import com.android.settings.users.UserFeatureProvider;
import android.content.pm.PackageManager;
import com.android.settings.SettingsPreferenceFragment;
import android.content.pm.ApplicationInfo;
import com.android.settings.applications.EnterpriseDefaultApps;
import java.util.EnumMap;
import java.util.List;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class EnterpriseSetDefaultAppsListPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final ApplicationFeatureProvider mApplicationFeatureProvider;
    private List<EnumMap<EnterpriseDefaultApps, List<ApplicationInfo>>> mApps;
    private final EnterprisePrivacyFeatureProvider mEnterprisePrivacyFeatureProvider;
    private final SettingsPreferenceFragment mParent;
    private final PackageManager mPm;
    private final UserFeatureProvider mUserFeatureProvider;
    private List<UserInfo> mUsers;
    
    public EnterpriseSetDefaultAppsListPreferenceController(final Context context, final SettingsPreferenceFragment mParent, final PackageManager mPm) {
        super(context);
        this.mUsers = Collections.emptyList();
        this.mApps = Collections.emptyList();
        this.mPm = mPm;
        this.mParent = mParent;
        final FeatureFactory factory = FeatureFactory.getFactory(context);
        this.mApplicationFeatureProvider = factory.getApplicationFeatureProvider(context);
        this.mEnterprisePrivacyFeatureProvider = factory.getEnterprisePrivacyFeatureProvider(context);
        this.mUserFeatureProvider = factory.getUserFeatureProvider(context);
        this.buildAppList();
    }
    
    private void buildAppList() {
        this.mUsers = new ArrayList<UserInfo>();
        this.mApps = new ArrayList<EnumMap<EnterpriseDefaultApps, List<ApplicationInfo>>>();
        for (final UserHandle userHandle : this.mUserFeatureProvider.getUserProfiles()) {
            EnumMap<EnterpriseDefaultApps, ArrayList<ApplicationInfo>> enumMap = null;
            final EnterpriseDefaultApps[] values = EnterpriseDefaultApps.values();
            final int length = values.length;
            int n = 0;
            for (final EnterpriseDefaultApps enterpriseDefaultApps : values) {
                final List<UserAppInfo> persistentPreferredActivities = this.mApplicationFeatureProvider.findPersistentPreferredActivities(userHandle.getIdentifier(), enterpriseDefaultApps.getIntents());
                if (!persistentPreferredActivities.isEmpty()) {
                    int n2;
                    if ((n2 = n) == 0) {
                        n2 = 1;
                        this.mUsers.add(persistentPreferredActivities.get(0).userInfo);
                        enumMap = new EnumMap<EnterpriseDefaultApps, ArrayList<ApplicationInfo>>(EnterpriseDefaultApps.class);
                        this.mApps.add((EnumMap<EnterpriseDefaultApps, List<ApplicationInfo>>)enumMap);
                    }
                    final ArrayList<ApplicationInfo> list = new ArrayList<ApplicationInfo>();
                    final Iterator<UserAppInfo> iterator2 = persistentPreferredActivities.iterator();
                    while (iterator2.hasNext()) {
                        list.add(iterator2.next().appInfo);
                    }
                    enumMap.put(enterpriseDefaultApps, list);
                    n = n2;
                }
            }
        }
        ThreadUtils.postOnMainThread(new _$$Lambda$EnterpriseSetDefaultAppsListPreferenceController$iIsgYxioer_lSG0lJzt4UtTCm2Y(this));
    }
    
    private CharSequence buildSummaryString(final Context context, final List<ApplicationInfo> list) {
        final String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            array[i] = (String)list.get(i).loadLabel(this.mPm);
        }
        if (list.size() == 1) {
            return array[0];
        }
        if (list.size() == 2) {
            return context.getString(2131886371, new Object[] { array[0], array[1] });
        }
        return context.getString(2131886372, new Object[] { array[0], array[1], array[2] });
    }
    
    private void createPreferences(final Context context, final PreferenceGroup preferenceGroup, final EnumMap<EnterpriseDefaultApps, List<ApplicationInfo>> enumMap) {
        if (preferenceGroup == null) {
            return;
        }
        for (final EnterpriseDefaultApps enterpriseDefaultApps : EnterpriseDefaultApps.values()) {
            final List<ApplicationInfo> list = enumMap.get(enterpriseDefaultApps);
            if (list != null) {
                if (!list.isEmpty()) {
                    final Preference preference = new Preference(context);
                    preference.setTitle(this.getTitle(context, enterpriseDefaultApps, list.size()));
                    preference.setSummary(this.buildSummaryString(context, list));
                    preference.setOrder(enterpriseDefaultApps.ordinal());
                    preference.setSelectable(false);
                    preferenceGroup.addPreference(preference);
                }
            }
        }
    }
    
    private String getTitle(final Context context, final EnterpriseDefaultApps enterpriseDefaultApps, final int n) {
        switch (enterpriseDefaultApps) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown type of default ");
                sb.append(enterpriseDefaultApps);
                throw new IllegalStateException(sb.toString());
            }
            case CAMERA: {
                return context.getResources().getQuantityString(2131755025, n);
            }
            case EMAIL: {
                return context.getResources().getQuantityString(2131755026, n);
            }
            case MAP: {
                return context.getString(2131887358);
            }
            case PHONE: {
                return context.getResources().getQuantityString(2131755027, n);
            }
            case CONTACTS: {
                return context.getString(2131887354);
            }
            case CALENDAR: {
                return context.getString(2131887353);
            }
            case BROWSER: {
                return context.getString(2131887351);
            }
        }
    }
    
    private void updateUi() {
        final Context context = this.mParent.getPreferenceManager().getContext();
        final PreferenceScreen preferenceScreen = this.mParent.getPreferenceScreen();
        if (preferenceScreen == null) {
            return;
        }
        final boolean inCompMode = this.mEnterprisePrivacyFeatureProvider.isInCompMode();
        int i = 0;
        if (!inCompMode && this.mUsers.size() == 1) {
            this.createPreferences(context, preferenceScreen, this.mApps.get(0));
        }
        else {
            while (i < this.mUsers.size()) {
                final UserInfo userInfo = this.mUsers.get(i);
                final PreferenceCategory preferenceCategory = new PreferenceCategory(context);
                preferenceScreen.addPreference(preferenceCategory);
                if (userInfo.isManagedProfile()) {
                    preferenceCategory.setTitle(2131888222);
                }
                else {
                    preferenceCategory.setTitle(2131888561);
                }
                preferenceCategory.setOrder(i);
                this.createPreferences(context, preferenceCategory, this.mApps.get(i));
                ++i;
            }
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
}
