package com.android.settings.enterprise;

import android.content.Context;

public class AdminGrantedLocationPermissionsPreferenceController extends AdminGrantedPermissionsPreferenceControllerBase
{
    public AdminGrantedLocationPermissionsPreferenceController(final Context context, final boolean b) {
        super(context, b, new String[] { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" });
    }
    
    @Override
    public String getPreferenceKey() {
        return "enterprise_privacy_number_location_access_packages";
    }
}
