package com.android.settings.enterprise;

import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class GlobalHttpProxyPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public GlobalHttpProxyPreferenceController(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "global_http_proxy";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mFeatureProvider.isGlobalHttpProxySet();
    }
}
