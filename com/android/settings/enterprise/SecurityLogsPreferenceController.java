package com.android.settings.enterprise;

import java.util.Date;
import android.content.Context;

public class SecurityLogsPreferenceController extends AdminActionPreferenceControllerBase
{
    public SecurityLogsPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    protected Date getAdminActionTimestamp() {
        return this.mFeatureProvider.getLastSecurityLogRetrievalTime();
    }
    
    @Override
    public String getPreferenceKey() {
        return "security_logs";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mFeatureProvider.isSecurityLoggingEnabled() || this.mFeatureProvider.getLastSecurityLogRetrievalTime() != null;
    }
}
