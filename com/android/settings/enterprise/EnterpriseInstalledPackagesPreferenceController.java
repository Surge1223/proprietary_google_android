package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class EnterpriseInstalledPackagesPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final boolean mAsync;
    private final ApplicationFeatureProvider mFeatureProvider;
    
    public EnterpriseInstalledPackagesPreferenceController(final Context context, final boolean mAsync) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getApplicationFeatureProvider(context);
        this.mAsync = mAsync;
    }
    
    @Override
    public String getPreferenceKey() {
        return "number_enterprise_installed_packages";
    }
    
    @Override
    public boolean isAvailable() {
        if (this.mAsync) {
            return true;
        }
        final Boolean[] array = { null };
        this.mFeatureProvider.calculateNumberOfPolicyInstalledApps(false, (ApplicationFeatureProvider.NumberOfAppsCallback)new _$$Lambda$EnterpriseInstalledPackagesPreferenceController$cz4T_BR7YJ9IEY1tdj7V5o__Yuo(array));
        return array[0];
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.mFeatureProvider.calculateNumberOfPolicyInstalledApps(true, (ApplicationFeatureProvider.NumberOfAppsCallback)new _$$Lambda$EnterpriseInstalledPackagesPreferenceController$ywnQ5T98AEytxQMBHl3WTR7fuAo(this, preference));
    }
}
