package com.android.settings.enterprise;

import android.text.format.DateUtils;
import android.support.v7.preference.Preference;
import java.util.Date;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class AdminActionPreferenceControllerBase extends AbstractPreferenceController implements PreferenceControllerMixin
{
    protected final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public AdminActionPreferenceControllerBase(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    protected abstract Date getAdminActionTimestamp();
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final Date adminActionTimestamp = this.getAdminActionTimestamp();
        String summary;
        if (adminActionTimestamp == null) {
            summary = this.mContext.getString(2131887590);
        }
        else {
            summary = DateUtils.formatDateTime(this.mContext, adminActionTimestamp.getTime(), 17);
        }
        preference.setSummary(summary);
    }
}
