package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import java.util.Iterator;
import com.android.settings.applications.EnterpriseDefaultApps;
import android.os.UserHandle;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.users.UserFeatureProvider;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class EnterpriseSetDefaultAppsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final ApplicationFeatureProvider mApplicationFeatureProvider;
    private final UserFeatureProvider mUserFeatureProvider;
    
    public EnterpriseSetDefaultAppsPreferenceController(final Context context) {
        super(context);
        final FeatureFactory factory = FeatureFactory.getFactory(context);
        this.mApplicationFeatureProvider = factory.getApplicationFeatureProvider(context);
        this.mUserFeatureProvider = factory.getUserFeatureProvider(context);
    }
    
    private int getNumberOfEnterpriseSetDefaultApps() {
        int n = 0;
        for (final UserHandle userHandle : this.mUserFeatureProvider.getUserProfiles()) {
            final EnterpriseDefaultApps[] values = EnterpriseDefaultApps.values();
            for (int length = values.length, i = 0; i < length; ++i) {
                n += this.mApplicationFeatureProvider.findPersistentPreferredActivities(userHandle.getIdentifier(), values[i].getIntents()).size();
            }
        }
        return n;
    }
    
    @Override
    public String getPreferenceKey() {
        return "number_enterprise_set_default_apps";
    }
    
    @Override
    public boolean isAvailable() {
        return this.getNumberOfEnterpriseSetDefaultApps() > 0;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int numberOfEnterpriseSetDefaultApps = this.getNumberOfEnterpriseSetDefaultApps();
        preference.setSummary(this.mContext.getResources().getQuantityString(2131755033, numberOfEnterpriseSetDefaultApps, new Object[] { numberOfEnterpriseSetDefaultApps }));
    }
}
