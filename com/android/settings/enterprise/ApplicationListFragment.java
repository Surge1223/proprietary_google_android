package com.android.settings.enterprise;

import com.android.settings.overlay.FeatureFactory;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.SettingsPreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.dashboard.DashboardFragment;

public abstract class ApplicationListFragment extends DashboardFragment implements ApplicationListBuilder
{
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<ApplicationListPreferenceController> list = (ArrayList<ApplicationListPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new ApplicationListPreferenceController(context, (ApplicationListPreferenceController.ApplicationListBuilder)this, context.getPackageManager(), this));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "EnterprisePrivacySettings";
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082710;
    }
    
    private abstract static class AdminGrantedPermission extends ApplicationListFragment
    {
        private final String[] mPermissions;
        
        public AdminGrantedPermission(final String[] mPermissions) {
            this.mPermissions = mPermissions;
        }
        
        @Override
        public void buildApplicationList(final Context context, final ListOfAppsCallback listOfAppsCallback) {
            FeatureFactory.getFactory(context).getApplicationFeatureProvider(context).listAppsWithAdminGrantedPermissions(this.mPermissions, listOfAppsCallback);
        }
        
        @Override
        public int getMetricsCategory() {
            return 939;
        }
    }
    
    public static class AdminGrantedPermissionCamera extends AdminGrantedPermission
    {
        public AdminGrantedPermissionCamera() {
            super(new String[] { "android.permission.CAMERA" });
        }
    }
    
    public static class AdminGrantedPermissionLocation extends AdminGrantedPermission
    {
        public AdminGrantedPermissionLocation() {
            super(new String[] { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" });
        }
    }
    
    public static class AdminGrantedPermissionMicrophone extends AdminGrantedPermission
    {
        public AdminGrantedPermissionMicrophone() {
            super(new String[] { "android.permission.RECORD_AUDIO" });
        }
    }
    
    public static class EnterpriseInstalledPackages extends ApplicationListFragment
    {
        @Override
        public void buildApplicationList(final Context context, final ListOfAppsCallback listOfAppsCallback) {
            FeatureFactory.getFactory(context).getApplicationFeatureProvider(context).listPolicyInstalledApps(listOfAppsCallback);
        }
        
        @Override
        public int getMetricsCategory() {
            return 938;
        }
    }
}
