package com.android.settings.enterprise;

import java.util.Date;
import android.content.Context;

public class NetworkLogsPreferenceController extends AdminActionPreferenceControllerBase
{
    public NetworkLogsPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    protected Date getAdminActionTimestamp() {
        return this.mFeatureProvider.getLastNetworkLogRetrievalTime();
    }
    
    @Override
    public String getPreferenceKey() {
        return "network_logs";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mFeatureProvider.isNetworkLoggingEnabled() || this.mFeatureProvider.getLastNetworkLogRetrievalTime() != null;
    }
}
