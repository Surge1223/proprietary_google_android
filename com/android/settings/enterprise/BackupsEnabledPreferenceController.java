package com.android.settings.enterprise;

import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class BackupsEnabledPreferenceController extends BasePreferenceController
{
    private static final String KEY_BACKUPS_ENABLED = "backups_enabled";
    private final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public BackupsEnabledPreferenceController(final Context context) {
        super(context, "backups_enabled");
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mFeatureProvider.areBackupsMandatory()) {
            n = 0;
        }
        else {
            n = 3;
        }
        return n;
    }
}
