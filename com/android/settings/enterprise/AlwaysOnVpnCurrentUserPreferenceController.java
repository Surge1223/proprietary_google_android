package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AlwaysOnVpnCurrentUserPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public AlwaysOnVpnCurrentUserPreferenceController(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "always_on_vpn_primary_user";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mFeatureProvider.isAlwaysOnVpnSetInCurrentUser();
    }
    
    @Override
    public void updateState(final Preference preference) {
        int title;
        if (this.mFeatureProvider.isInCompMode()) {
            title = 2131887564;
        }
        else {
            title = 2131887563;
        }
        preference.setTitle(title);
    }
}
