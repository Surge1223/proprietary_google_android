package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class CaCertsPreferenceControllerBase extends AbstractPreferenceController implements PreferenceControllerMixin
{
    protected final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public CaCertsPreferenceControllerBase(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    protected abstract int getNumberOfCaCerts();
    
    @Override
    public boolean isAvailable() {
        return this.getNumberOfCaCerts() > 0;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int numberOfCaCerts = this.getNumberOfCaCerts();
        preference.setSummary(this.mContext.getResources().getQuantityString(2131755031, numberOfCaCerts, new Object[] { numberOfCaCerts }));
    }
}
