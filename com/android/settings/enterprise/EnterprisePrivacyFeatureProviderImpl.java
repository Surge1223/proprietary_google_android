package com.android.settings.enterprise;

import android.content.Intent;
import android.view.View;
import android.text.style.ClickableSpan;
import com.android.settings.vpn2.VpnUtils;
import java.util.List;
import android.content.ComponentName;
import java.util.Date;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import java.util.Iterator;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.res.Resources;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.net.ConnectivityManager;

public class EnterprisePrivacyFeatureProviderImpl implements EnterprisePrivacyFeatureProvider
{
    private static final int MY_USER_ID;
    private final ConnectivityManager mCm;
    private final Context mContext;
    private final DevicePolicyManager mDpm;
    private final PackageManagerWrapper mPm;
    private final Resources mResources;
    private final UserManager mUm;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public EnterprisePrivacyFeatureProviderImpl(final Context context, final DevicePolicyManager mDpm, final PackageManagerWrapper mPm, final UserManager mUm, final ConnectivityManager mCm, final Resources mResources) {
        this.mContext = context.getApplicationContext();
        this.mDpm = mDpm;
        this.mPm = mPm;
        this.mUm = mUm;
        this.mCm = mCm;
        this.mResources = mResources;
    }
    
    private int getManagedProfileUserId() {
        for (final UserInfo userInfo : this.mUm.getProfiles(EnterprisePrivacyFeatureProviderImpl.MY_USER_ID)) {
            if (userInfo.isManagedProfile()) {
                return userInfo.id;
            }
        }
        return -10000;
    }
    
    @Override
    public boolean areBackupsMandatory() {
        return this.mDpm.getMandatoryBackupTransport() != null;
    }
    
    @Override
    public CharSequence getDeviceOwnerDisclosure() {
        if (!this.hasDeviceOwner()) {
            return null;
        }
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        final CharSequence deviceOwnerOrganizationName = this.mDpm.getDeviceOwnerOrganizationName();
        if (deviceOwnerOrganizationName != null) {
            spannableStringBuilder.append((CharSequence)this.mResources.getString(2131887467, new Object[] { deviceOwnerOrganizationName }));
        }
        else {
            spannableStringBuilder.append((CharSequence)this.mResources.getString(2131887465));
        }
        spannableStringBuilder.append((CharSequence)this.mResources.getString(2131887466));
        spannableStringBuilder.append((CharSequence)this.mResources.getString(2131888022), (Object)new EnterprisePrivacySpan(this.mContext), 0);
        return (CharSequence)spannableStringBuilder;
    }
    
    @Override
    public String getDeviceOwnerOrganizationName() {
        final CharSequence deviceOwnerOrganizationName = this.mDpm.getDeviceOwnerOrganizationName();
        if (deviceOwnerOrganizationName == null) {
            return null;
        }
        return deviceOwnerOrganizationName.toString();
    }
    
    @Override
    public String getImeLabelIfOwnerSet() {
        if (!this.mDpm.isCurrentInputMethodSetByOwner()) {
            return null;
        }
        final String stringForUser = Settings.Secure.getStringForUser(this.mContext.getContentResolver(), "default_input_method", EnterprisePrivacyFeatureProviderImpl.MY_USER_ID);
        if (stringForUser == null) {
            return null;
        }
        try {
            return this.mPm.getApplicationInfoAsUser(stringForUser, 0, EnterprisePrivacyFeatureProviderImpl.MY_USER_ID).loadLabel(this.mPm.getPackageManager()).toString();
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @Override
    public Date getLastBugReportRequestTime() {
        final long lastBugReportRequestTime = this.mDpm.getLastBugReportRequestTime();
        Date date;
        if (lastBugReportRequestTime < 0L) {
            date = null;
        }
        else {
            date = new Date(lastBugReportRequestTime);
        }
        return date;
    }
    
    @Override
    public Date getLastNetworkLogRetrievalTime() {
        final long lastNetworkLogRetrievalTime = this.mDpm.getLastNetworkLogRetrievalTime();
        Date date;
        if (lastNetworkLogRetrievalTime < 0L) {
            date = null;
        }
        else {
            date = new Date(lastNetworkLogRetrievalTime);
        }
        return date;
    }
    
    @Override
    public Date getLastSecurityLogRetrievalTime() {
        final long lastSecurityLogRetrievalTime = this.mDpm.getLastSecurityLogRetrievalTime();
        Date date;
        if (lastSecurityLogRetrievalTime < 0L) {
            date = null;
        }
        else {
            date = new Date(lastSecurityLogRetrievalTime);
        }
        return date;
    }
    
    @Override
    public int getMaximumFailedPasswordsBeforeWipeInCurrentUser() {
        ComponentName componentName;
        if ((componentName = this.mDpm.getDeviceOwnerComponentOnCallingUser()) == null) {
            componentName = this.mDpm.getProfileOwnerAsUser(EnterprisePrivacyFeatureProviderImpl.MY_USER_ID);
        }
        if (componentName == null) {
            return 0;
        }
        return this.mDpm.getMaximumFailedPasswordsForWipe(componentName, EnterprisePrivacyFeatureProviderImpl.MY_USER_ID);
    }
    
    @Override
    public int getMaximumFailedPasswordsBeforeWipeInManagedProfile() {
        final int managedProfileUserId = this.getManagedProfileUserId();
        if (managedProfileUserId == -10000) {
            return 0;
        }
        final ComponentName profileOwnerAsUser = this.mDpm.getProfileOwnerAsUser(managedProfileUserId);
        if (profileOwnerAsUser == null) {
            return 0;
        }
        return this.mDpm.getMaximumFailedPasswordsForWipe(profileOwnerAsUser, managedProfileUserId);
    }
    
    @Override
    public int getNumberOfActiveDeviceAdminsForCurrentUserAndManagedProfile() {
        int n = 0;
        final Iterator<UserInfo> iterator = (Iterator<UserInfo>)this.mUm.getProfiles(EnterprisePrivacyFeatureProviderImpl.MY_USER_ID).iterator();
        while (iterator.hasNext()) {
            final List activeAdminsAsUser = this.mDpm.getActiveAdminsAsUser(iterator.next().id);
            int n2 = n;
            if (activeAdminsAsUser != null) {
                n2 = n + activeAdminsAsUser.size();
            }
            n = n2;
        }
        return n;
    }
    
    @Override
    public int getNumberOfOwnerInstalledCaCertsForCurrentUser() {
        final List ownerInstalledCaCerts = this.mDpm.getOwnerInstalledCaCerts(new UserHandle(EnterprisePrivacyFeatureProviderImpl.MY_USER_ID));
        if (ownerInstalledCaCerts == null) {
            return 0;
        }
        return ownerInstalledCaCerts.size();
    }
    
    @Override
    public int getNumberOfOwnerInstalledCaCertsForManagedProfile() {
        final int managedProfileUserId = this.getManagedProfileUserId();
        if (managedProfileUserId == -10000) {
            return 0;
        }
        final List ownerInstalledCaCerts = this.mDpm.getOwnerInstalledCaCerts(new UserHandle(managedProfileUserId));
        if (ownerInstalledCaCerts == null) {
            return 0;
        }
        return ownerInstalledCaCerts.size();
    }
    
    @Override
    public boolean hasDeviceOwner() {
        final boolean hasSystemFeature = this.mPm.hasSystemFeature("android.software.device_admin");
        boolean b = false;
        if (!hasSystemFeature) {
            return false;
        }
        if (this.mDpm.getDeviceOwnerComponentOnAnyUser() != null) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isAlwaysOnVpnSetInCurrentUser() {
        return VpnUtils.isAlwaysOnVpnSet(this.mCm, EnterprisePrivacyFeatureProviderImpl.MY_USER_ID);
    }
    
    @Override
    public boolean isAlwaysOnVpnSetInManagedProfile() {
        final int managedProfileUserId = this.getManagedProfileUserId();
        return managedProfileUserId != -10000 && VpnUtils.isAlwaysOnVpnSet(this.mCm, managedProfileUserId);
    }
    
    @Override
    public boolean isGlobalHttpProxySet() {
        return this.mCm.getGlobalProxy() != null;
    }
    
    @Override
    public boolean isInCompMode() {
        return this.hasDeviceOwner() && this.getManagedProfileUserId() != -10000;
    }
    
    @Override
    public boolean isNetworkLoggingEnabled() {
        return this.mDpm.isNetworkLoggingEnabled((ComponentName)null);
    }
    
    @Override
    public boolean isSecurityLoggingEnabled() {
        return this.mDpm.isSecurityLoggingEnabled((ComponentName)null);
    }
    
    protected static class EnterprisePrivacySpan extends ClickableSpan
    {
        private final Context mContext;
        
        public EnterprisePrivacySpan(final Context mContext) {
            this.mContext = mContext;
        }
        
        public boolean equals(final Object o) {
            return o instanceof EnterprisePrivacySpan && ((EnterprisePrivacySpan)o).mContext == this.mContext;
        }
        
        public void onClick(final View view) {
            this.mContext.startActivity(new Intent("android.settings.ENTERPRISE_PRIVACY_SETTINGS").addFlags(268435456));
        }
    }
}
