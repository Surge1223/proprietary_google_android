package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class EnterprisePrivacyPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public EnterprisePrivacyPreferenceController(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "enterprise_privacy";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mFeatureProvider.hasDeviceOwner();
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (preference == null) {
            return;
        }
        final String deviceOwnerOrganizationName = this.mFeatureProvider.getDeviceOwnerOrganizationName();
        if (deviceOwnerOrganizationName == null) {
            preference.setSummary(2131887593);
        }
        else {
            preference.setSummary(this.mContext.getResources().getString(2131887594, new Object[] { deviceOwnerOrganizationName }));
        }
    }
}
