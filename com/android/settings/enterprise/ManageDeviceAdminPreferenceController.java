package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ManageDeviceAdminPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public ManageDeviceAdminPreferenceController(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "manage_device_admin";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034148);
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int numberOfActiveDeviceAdminsForCurrentUserAndManagedProfile = this.mFeatureProvider.getNumberOfActiveDeviceAdminsForCurrentUserAndManagedProfile();
        String summary;
        if (numberOfActiveDeviceAdminsForCurrentUserAndManagedProfile == 0) {
            summary = this.mContext.getResources().getString(2131888523);
        }
        else {
            summary = this.mContext.getResources().getQuantityString(2131755050, numberOfActiveDeviceAdminsForCurrentUserAndManagedProfile, new Object[] { numberOfActiveDeviceAdminsForCurrentUserAndManagedProfile });
        }
        preference.setSummary(summary);
    }
}
