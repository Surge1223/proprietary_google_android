package com.android.settings.enterprise;

import com.android.settings.overlay.FeatureFactory;
import com.android.settings.widget.PreferenceCategoryController;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class EnterprisePrivacySettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, false);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082771;
                return Arrays.asList(searchIndexableResource);
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return EnterprisePrivacySettings.isPageEnabled(context);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final boolean b) {
        final ArrayList<FailedPasswordWipeManagedProfilePreferenceController> list = (ArrayList<FailedPasswordWipeManagedProfilePreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new NetworkLogsPreferenceController(context));
        list.add(new BugReportsPreferenceController(context));
        list.add(new SecurityLogsPreferenceController(context));
        final ArrayList<FailedPasswordWipeManagedProfilePreferenceController> children = new ArrayList<FailedPasswordWipeManagedProfilePreferenceController>();
        children.add(new EnterpriseInstalledPackagesPreferenceController(context, b));
        children.add((BackupsEnabledPreferenceController)new AdminGrantedLocationPermissionsPreferenceController(context, b));
        children.add((BackupsEnabledPreferenceController)new AdminGrantedMicrophonePermissionPreferenceController(context, b));
        children.add((BackupsEnabledPreferenceController)new AdminGrantedCameraPermissionPreferenceController(context, b));
        children.add((BackupsEnabledPreferenceController)new EnterpriseSetDefaultAppsPreferenceController(context));
        children.add((BackupsEnabledPreferenceController)new AlwaysOnVpnCurrentUserPreferenceController(context));
        children.add((BackupsEnabledPreferenceController)new AlwaysOnVpnManagedProfilePreferenceController(context));
        children.add((BackupsEnabledPreferenceController)new ImePreferenceController(context));
        children.add((BackupsEnabledPreferenceController)new GlobalHttpProxyPreferenceController(context));
        children.add((BackupsEnabledPreferenceController)new CaCertsCurrentUserPreferenceController(context));
        children.add((BackupsEnabledPreferenceController)new CaCertsManagedProfilePreferenceController(context));
        children.add(new BackupsEnabledPreferenceController(context));
        list.addAll((Collection<? extends AbstractPreferenceController>)children);
        list.add(new PreferenceCategoryController(context, "exposure_changes_category").setChildren((List<AbstractPreferenceController>)children));
        list.add(new FailedPasswordWipeCurrentUserPreferenceController(context));
        list.add(new FailedPasswordWipeManagedProfilePreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    public static boolean isPageEnabled(final Context context) {
        return FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context).hasDeviceOwner();
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, true);
    }
    
    @Override
    protected String getLogTag() {
        return "EnterprisePrivacySettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 628;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082771;
    }
}
