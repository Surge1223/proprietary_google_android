package com.android.settings.enterprise;

import java.util.Objects;
import com.android.settings.Settings;
import android.os.Parcelable;
import com.android.settings.DeviceAdminAdd;
import android.content.Intent;
import android.widget.TextView;
import android.os.Process;
import android.os.UserHandle;
import android.app.admin.DevicePolicyManager;
import android.view.LayoutInflater;
import android.app.AlertDialog$Builder;
import com.android.settings.Utils;
import android.util.IconDrawableFactory;
import android.widget.ImageView;
import android.content.Context;
import android.content.ComponentName;
import android.view.View;
import com.android.settingslib.RestrictedLockUtils;
import android.view.ViewGroup;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$ActionDisabledByAdminDialogHelper$1vfAOqcacTgM_c2XJLB5Z1_4lQ4 implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        ActionDisabledByAdminDialogHelper.lambda$prepareDialogBuilder$0(this.f$0, dialogInterface, n);
    }
}
