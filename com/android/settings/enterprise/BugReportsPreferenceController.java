package com.android.settings.enterprise;

import java.util.Date;
import android.content.Context;

public class BugReportsPreferenceController extends AdminActionPreferenceControllerBase
{
    public BugReportsPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    protected Date getAdminActionTimestamp() {
        return this.mFeatureProvider.getLastBugReportRequestTime();
    }
    
    @Override
    public String getPreferenceKey() {
        return "bug_reports";
    }
}
