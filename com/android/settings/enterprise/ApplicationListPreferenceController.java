package com.android.settings.enterprise;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import com.android.settings.widget.AppPreference;
import android.util.IconDrawableFactory;
import com.android.settings.applications.UserAppInfo;
import java.util.List;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settingslib.core.AbstractPreferenceController;

public class ApplicationListPreferenceController extends AbstractPreferenceController implements ListOfAppsCallback, PreferenceControllerMixin
{
    private SettingsPreferenceFragment mParent;
    private final PackageManager mPm;
    
    public ApplicationListPreferenceController(final Context context, final ApplicationListBuilder applicationListBuilder, final PackageManager mPm, final SettingsPreferenceFragment mParent) {
        super(context);
        this.mPm = mPm;
        this.mParent = mParent;
        applicationListBuilder.buildApplicationList(context, this);
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onListOfAppsResult(final List<UserAppInfo> list) {
        final PreferenceScreen preferenceScreen = this.mParent.getPreferenceScreen();
        if (preferenceScreen == null) {
            return;
        }
        final IconDrawableFactory instance = IconDrawableFactory.newInstance(this.mContext);
        final Context context = this.mParent.getPreferenceManager().getContext();
        for (int i = 0; i < list.size(); ++i) {
            final UserAppInfo userAppInfo = list.get(i);
            final AppPreference appPreference = new AppPreference(context);
            appPreference.setTitle(userAppInfo.appInfo.loadLabel(this.mPm));
            appPreference.setIcon(instance.getBadgedIcon(userAppInfo.appInfo));
            appPreference.setOrder(i);
            appPreference.setSelectable(false);
            preferenceScreen.addPreference(appPreference);
        }
    }
    
    public interface ApplicationListBuilder
    {
        void buildApplicationList(final Context p0, final ListOfAppsCallback p1);
    }
}
