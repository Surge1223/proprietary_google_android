package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settings.applications.ApplicationFeatureProvider;

public final class _$$Lambda$EnterpriseInstalledPackagesPreferenceController$cz4T_BR7YJ9IEY1tdj7V5o__Yuo implements NumberOfAppsCallback
{
    @Override
    public final void onNumberOfAppsResult(final int n) {
        EnterpriseInstalledPackagesPreferenceController.lambda$isAvailable$1(this.f$0, n);
    }
}
