package com.android.settings.enterprise;

import java.util.Objects;
import com.android.settings.Settings;
import android.os.Parcelable;
import com.android.settings.DeviceAdminAdd;
import android.content.Intent;
import android.widget.TextView;
import android.os.Process;
import android.os.UserHandle;
import android.app.admin.DevicePolicyManager;
import android.content.DialogInterface$OnClickListener;
import android.view.LayoutInflater;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface;
import com.android.settings.Utils;
import android.util.IconDrawableFactory;
import android.widget.ImageView;
import android.content.Context;
import android.content.ComponentName;
import android.view.View;
import com.android.settingslib.RestrictedLockUtils;
import android.view.ViewGroup;
import android.app.Activity;

public class ActionDisabledByAdminDialogHelper
{
    private static final String TAG;
    private Activity mActivity;
    private ViewGroup mDialogView;
    private RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    private String mRestriction;
    
    static {
        TAG = ActionDisabledByAdminDialogHelper.class.getName();
    }
    
    public ActionDisabledByAdminDialogHelper(final Activity mActivity) {
        this.mRestriction = null;
        this.mActivity = mActivity;
    }
    
    private void initializeDialogViews(final View view, ComponentName componentName, final int n, final String s) {
        if (componentName == null) {
            return;
        }
        if (RestrictedLockUtils.isAdminInCurrentUserOrProfile((Context)this.mActivity, componentName) && RestrictedLockUtils.isCurrentUserOrProfile((Context)this.mActivity, n)) {
            ((ImageView)view.findViewById(2131361852)).setImageDrawable(Utils.getBadgedIcon(IconDrawableFactory.newInstance((Context)this.mActivity), this.mActivity.getPackageManager(), componentName.getPackageName(), n));
        }
        else {
            componentName = null;
        }
        this.setAdminSupportTitle(view, s);
        this.setAdminSupportDetails(this.mActivity, view, new RestrictedLockUtils.EnforcedAdmin(componentName, n));
    }
    
    public AlertDialog$Builder prepareDialogBuilder(final String mRestriction, final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin) {
        this.mEnforcedAdmin = mEnforcedAdmin;
        this.mRestriction = mRestriction;
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this.mActivity);
        this.initializeDialogViews((View)(this.mDialogView = (ViewGroup)LayoutInflater.from(alertDialog$Builder.getContext()).inflate(2131558443, (ViewGroup)null)), this.mEnforcedAdmin.component, this.mEnforcedAdmin.userId, this.mRestriction);
        return alertDialog$Builder.setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)null).setNeutralButton(2131888022, (DialogInterface$OnClickListener)new _$$Lambda$ActionDisabledByAdminDialogHelper$1vfAOqcacTgM_c2XJLB5Z1_4lQ4(this)).setView((View)this.mDialogView);
    }
    
    void setAdminSupportDetails(final Activity activity, final View view, final RestrictedLockUtils.EnforcedAdmin enforcedAdmin) {
        if (enforcedAdmin != null && enforcedAdmin.component != null) {
            final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)activity.getSystemService("device_policy");
            if (RestrictedLockUtils.isAdminInCurrentUserOrProfile((Context)activity, enforcedAdmin.component) && RestrictedLockUtils.isCurrentUserOrProfile((Context)activity, enforcedAdmin.userId)) {
                if (enforcedAdmin.userId == -10000) {
                    enforcedAdmin.userId = UserHandle.myUserId();
                }
                CharSequence shortSupportMessageForUser = null;
                if (UserHandle.isSameApp(Process.myUid(), 1000)) {
                    shortSupportMessageForUser = devicePolicyManager.getShortSupportMessageForUser(enforcedAdmin.component, enforcedAdmin.userId);
                }
                if (shortSupportMessageForUser != null) {
                    ((TextView)view.findViewById(2131361854)).setText(shortSupportMessageForUser);
                }
            }
            else {
                enforcedAdmin.component = null;
            }
        }
    }
    
    void setAdminSupportTitle(final View view, final String s) {
        final TextView textView = (TextView)view.findViewById(2131361851);
        if (textView == null) {
            return;
        }
        if (s == null) {
            textView.setText(2131887427);
            return;
        }
        switch (s) {
            default: {
                textView.setText(2131887427);
                break;
            }
            case "policy_suspend_packages": {
                textView.setText(2131887433);
                break;
            }
            case "policy_mandatory_backups": {
                textView.setText(2131887434);
                break;
            }
            case "policy_disable_screen_capture": {
                textView.setText(2131887431);
                break;
            }
            case "policy_disable_camera": {
                textView.setText(2131887429);
                break;
            }
            case "no_sms": {
                textView.setText(2131887432);
                break;
            }
            case "no_outgoing_calls": {
                textView.setText(2131887430);
                break;
            }
            case "no_adjust_volume": {
                textView.setText(2131887428);
                break;
            }
        }
    }
    
    void showAdminPolicies(final RestrictedLockUtils.EnforcedAdmin enforcedAdmin, final Activity activity) {
        final Intent intent = new Intent();
        if (enforcedAdmin.component != null) {
            intent.setClass((Context)activity, (Class)DeviceAdminAdd.class);
            intent.putExtra("android.app.extra.DEVICE_ADMIN", (Parcelable)enforcedAdmin.component);
            intent.putExtra("android.app.extra.CALLED_FROM_SUPPORT_DIALOG", true);
            activity.startActivityAsUser(intent, new UserHandle(enforcedAdmin.userId));
        }
        else {
            intent.setClass((Context)activity, (Class)Settings.DeviceAdminSettingsActivity.class);
            intent.addFlags(268435456);
            activity.startActivity(intent);
        }
    }
    
    public void updateDialog(final String mRestriction, final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin) {
        if (this.mEnforcedAdmin.equals(mEnforcedAdmin) && Objects.equals(this.mRestriction, mRestriction)) {
            return;
        }
        this.mEnforcedAdmin = mEnforcedAdmin;
        this.mRestriction = mRestriction;
        this.initializeDialogViews((View)this.mDialogView, this.mEnforcedAdmin.component, this.mEnforcedAdmin.userId, this.mRestriction);
    }
}
