package com.android.settings.enterprise;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import java.util.Iterator;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.applications.UserAppInfo;
import android.os.UserHandle;
import java.util.ArrayList;
import com.android.settings.overlay.FeatureFactory;
import java.util.Collections;
import android.content.Context;
import android.content.pm.UserInfo;
import com.android.settings.users.UserFeatureProvider;
import android.content.pm.PackageManager;
import com.android.settings.SettingsPreferenceFragment;
import android.content.pm.ApplicationInfo;
import com.android.settings.applications.EnterpriseDefaultApps;
import java.util.EnumMap;
import java.util.List;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public final class _$$Lambda$EnterpriseSetDefaultAppsListPreferenceController$iIsgYxioer_lSG0lJzt4UtTCm2Y implements Runnable
{
    @Override
    public final void run() {
        this.f$0.updateUi();
    }
}
