package com.android.settings.enterprise;

import android.content.Context;

public class AdminGrantedCameraPermissionPreferenceController extends AdminGrantedPermissionsPreferenceControllerBase
{
    public AdminGrantedCameraPermissionPreferenceController(final Context context, final boolean b) {
        super(context, b, new String[] { "android.permission.CAMERA" });
    }
    
    @Override
    public String getPreferenceKey() {
        return "enterprise_privacy_number_camera_access_packages";
    }
}
