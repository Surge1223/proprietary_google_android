package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class AdminGrantedPermissionsPreferenceControllerBase extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final boolean mAsync;
    private final ApplicationFeatureProvider mFeatureProvider;
    private boolean mHasApps;
    private final String[] mPermissions;
    
    public AdminGrantedPermissionsPreferenceControllerBase(final Context context, final boolean mAsync, final String[] mPermissions) {
        super(context);
        this.mPermissions = mPermissions;
        this.mFeatureProvider = FeatureFactory.getFactory(context).getApplicationFeatureProvider(context);
        this.mAsync = mAsync;
        this.mHasApps = false;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        return this.getPreferenceKey().equals(preference.getKey()) && this.mHasApps && super.handlePreferenceTreeClick(preference);
    }
    
    @Override
    public boolean isAvailable() {
        if (this.mAsync) {
            return true;
        }
        final Boolean[] array = { null };
        this.mFeatureProvider.calculateNumberOfAppsWithAdminGrantedPermissions(this.mPermissions, false, (ApplicationFeatureProvider.NumberOfAppsCallback)new _$$Lambda$AdminGrantedPermissionsPreferenceControllerBase$4ZAcP8cSJJvD_RXkeJP9Rdjuu0k(array));
        return this.mHasApps = array[0];
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.mFeatureProvider.calculateNumberOfAppsWithAdminGrantedPermissions(this.mPermissions, true, (ApplicationFeatureProvider.NumberOfAppsCallback)new _$$Lambda$AdminGrantedPermissionsPreferenceControllerBase$8oa0oEjJK2SZdXqZGB2HrMlBk_0(this, preference));
    }
}
