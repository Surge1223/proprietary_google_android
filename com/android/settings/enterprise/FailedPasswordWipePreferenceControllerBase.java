package com.android.settings.enterprise;

import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class FailedPasswordWipePreferenceControllerBase extends AbstractPreferenceController implements PreferenceControllerMixin
{
    protected final EnterprisePrivacyFeatureProvider mFeatureProvider;
    
    public FailedPasswordWipePreferenceControllerBase(final Context context) {
        super(context);
        this.mFeatureProvider = FeatureFactory.getFactory(context).getEnterprisePrivacyFeatureProvider(context);
    }
    
    protected abstract int getMaximumFailedPasswordsBeforeWipe();
    
    @Override
    public boolean isAvailable() {
        return this.getMaximumFailedPasswordsBeforeWipe() > 0;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int maximumFailedPasswordsBeforeWipe = this.getMaximumFailedPasswordsBeforeWipe();
        preference.setSummary(this.mContext.getResources().getQuantityString(2131755032, maximumFailedPasswordsBeforeWipe, new Object[] { maximumFailedPasswordsBeforeWipe }));
    }
}
