package com.android.settings.enterprise;

import com.android.settings.SettingsPreferenceFragment;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.dashboard.DashboardFragment;

public class EnterpriseSetDefaultAppsListFragment extends DashboardFragment
{
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<EnterpriseSetDefaultAppsListPreferenceController> list = (ArrayList<EnterpriseSetDefaultAppsListPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new EnterpriseSetDefaultAppsListPreferenceController(context, this, context.getPackageManager()));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "EnterprisePrivacySettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 940;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082772;
    }
}
