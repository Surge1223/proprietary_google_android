package com.android.settings.enterprise;

import android.content.DialogInterface;
import android.os.Bundle;
import android.content.ComponentName;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Intent;
import android.content.DialogInterface$OnDismissListener;
import android.app.Activity;

public class ActionDisabledByAdminDialog extends Activity implements DialogInterface$OnDismissListener
{
    private ActionDisabledByAdminDialogHelper mDialogHelper;
    
    RestrictedLockUtils.EnforcedAdmin getAdminDetailsFromIntent(final Intent intent) {
        final RestrictedLockUtils.EnforcedAdmin enforcedAdmin = new RestrictedLockUtils.EnforcedAdmin(null, UserHandle.myUserId());
        if (intent == null) {
            return enforcedAdmin;
        }
        enforcedAdmin.component = (ComponentName)intent.getParcelableExtra("android.app.extra.DEVICE_ADMIN");
        enforcedAdmin.userId = intent.getIntExtra("android.intent.extra.USER_ID", UserHandle.myUserId());
        return enforcedAdmin;
    }
    
    String getRestrictionFromIntent(final Intent intent) {
        if (intent == null) {
            return null;
        }
        return intent.getStringExtra("android.app.extra.RESTRICTION");
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final RestrictedLockUtils.EnforcedAdmin adminDetailsFromIntent = this.getAdminDetailsFromIntent(this.getIntent());
        final String restrictionFromIntent = this.getRestrictionFromIntent(this.getIntent());
        this.mDialogHelper = new ActionDisabledByAdminDialogHelper(this);
        this.mDialogHelper.prepareDialogBuilder(restrictionFromIntent, adminDetailsFromIntent).setOnDismissListener((DialogInterface$OnDismissListener)this).show();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        this.finish();
    }
    
    public void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        this.mDialogHelper.updateDialog(this.getRestrictionFromIntent(intent), this.getAdminDetailsFromIntent(intent));
    }
}
