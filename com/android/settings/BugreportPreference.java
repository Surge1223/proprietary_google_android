package com.android.settings;

import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.content.DialogInterface;
import android.os.RemoteException;
import android.util.Log;
import android.app.ActivityManager;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CheckedTextView;
import android.widget.TextView;
import com.android.settingslib.CustomDialogPreference;

public class BugreportPreference extends CustomDialogPreference
{
    private TextView mFullSummary;
    private CheckedTextView mFullTitle;
    private TextView mInteractiveSummary;
    private CheckedTextView mInteractiveTitle;
    
    public BugreportPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    private void takeBugreport(final int n) {
        try {
            ActivityManager.getService().requestBugReport(n);
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("error taking bugreport (bugreportType=");
            sb.append(n);
            sb.append(")");
            Log.e("BugreportPreference", sb.toString(), (Throwable)ex);
        }
    }
    
    @Override
    protected void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            final Context context = this.getContext();
            if (this.mFullTitle.isChecked()) {
                Log.v("BugreportPreference", "Taking full bugreport right away");
                FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 295, (Pair<Integer, Object>[])new Pair[0]);
                this.takeBugreport(0);
            }
            else {
                Log.v("BugreportPreference", "Taking interactive bugreport right away");
                FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 294, (Pair<Integer, Object>[])new Pair[0]);
                this.takeBugreport(1);
            }
        }
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        super.onPrepareDialogBuilder(alertDialog$Builder, dialogInterface$OnClickListener);
        final View inflate = View.inflate(this.getContext(), 2131558478, (ViewGroup)null);
        this.mInteractiveTitle = (CheckedTextView)inflate.findViewById(2131361939);
        this.mInteractiveSummary = (TextView)inflate.findViewById(2131361938);
        this.mFullTitle = (CheckedTextView)inflate.findViewById(2131361937);
        this.mFullSummary = (TextView)inflate.findViewById(2131361936);
        final View.OnClickListener view$OnClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (view == BugreportPreference.this.mFullTitle || view == BugreportPreference.this.mFullSummary) {
                    BugreportPreference.this.mInteractiveTitle.setChecked(false);
                    BugreportPreference.this.mFullTitle.setChecked(true);
                }
                if (view == BugreportPreference.this.mInteractiveTitle || view == BugreportPreference.this.mInteractiveSummary) {
                    BugreportPreference.this.mInteractiveTitle.setChecked(true);
                    BugreportPreference.this.mFullTitle.setChecked(false);
                }
            }
        };
        this.mInteractiveTitle.setOnClickListener((View.OnClickListener)view$OnClickListener);
        this.mFullTitle.setOnClickListener((View.OnClickListener)view$OnClickListener);
        this.mInteractiveSummary.setOnClickListener((View.OnClickListener)view$OnClickListener);
        this.mFullSummary.setOnClickListener((View.OnClickListener)view$OnClickListener);
        alertDialog$Builder.setPositiveButton(17040771, dialogInterface$OnClickListener);
        alertDialog$Builder.setView(inflate);
    }
}
