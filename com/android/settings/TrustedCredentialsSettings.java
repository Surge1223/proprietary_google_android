package com.android.settings;

import android.widget.ExpandableListView;
import java.util.Objects;
import android.content.pm.UserInfo;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.app.UnlaunchableAppActivity;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ExpandableListView$OnGroupClickListener;
import android.widget.ExpandableListView$OnChildClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListAdapter;
import android.widget.AdapterView;
import android.view.ViewGroup.LayoutParams;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R$styleable;
import android.graphics.drawable.Drawable;
import android.database.DataSetObserver;
import android.widget.ListView;
import android.widget.ImageView;
import android.widget.LinearLayout$LayoutParams;
import android.widget.AdapterView$OnItemClickListener;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.net.http.SslCertificate;
import java.security.cert.CertificateEncodingException;
import android.widget.FrameLayout;
import java.util.Collections;
import android.app.admin.DevicePolicyManager;
import android.os.UserHandle;
import android.widget.ProgressBar;
import android.os.AsyncTask;
import android.app.Activity;
import android.content.IntentFilter;
import java.util.Collection;
import android.os.Bundle;
import android.security.IKeyChainService;
import android.os.RemoteException;
import android.util.Log;
import android.security.KeyChain;
import java.security.cert.X509Certificate;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import android.view.View;
import android.widget.LinearLayout;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.List;
import java.util.Iterator;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.os.UserManager;
import android.widget.TabHost;
import android.app.KeyguardManager;
import com.android.internal.annotations.GuardedBy;
import android.security.KeyChain$KeyChainConnection;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.function.IntConsumer;
import android.util.ArraySet;
import java.util.Set;
import com.android.settings.core.InstrumentedFragment;

public class TrustedCredentialsSettings extends InstrumentedFragment implements DelegateInterface
{
    private Set<AliasLoader> mAliasLoaders;
    private AliasOperation mAliasOperation;
    private ArraySet<Integer> mConfirmedCredentialUsers;
    private IntConsumer mConfirmingCredentialListener;
    private int mConfirmingCredentialUser;
    private ArrayList<GroupAdapter> mGroupAdapters;
    @GuardedBy("mKeyChainConnectionByProfileId")
    private final SparseArray<KeyChain$KeyChainConnection> mKeyChainConnectionByProfileId;
    private KeyguardManager mKeyguardManager;
    private TabHost mTabHost;
    private int mTrustAllCaUserId;
    private UserManager mUserManager;
    private BroadcastReceiver mWorkProfileChangedReceiver;
    
    public TrustedCredentialsSettings() {
        this.mGroupAdapters = new ArrayList<GroupAdapter>(2);
        this.mAliasLoaders = (Set<AliasLoader>)new ArraySet(2);
        this.mKeyChainConnectionByProfileId = (SparseArray<KeyChain$KeyChainConnection>)new SparseArray();
        this.mWorkProfileChangedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.intent.action.MANAGED_PROFILE_AVAILABLE".equals(action) || "android.intent.action.MANAGED_PROFILE_UNAVAILABLE".equals(action) || "android.intent.action.MANAGED_PROFILE_UNLOCKED".equals(action)) {
                    final Iterator<GroupAdapter> iterator = TrustedCredentialsSettings.this.mGroupAdapters.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().load();
                    }
                }
            }
        };
    }
    
    private void addTab(final Tab tab) {
        this.mTabHost.addTab(this.mTabHost.newTabSpec(tab.mTag).setIndicator((CharSequence)this.getActivity().getString(tab.mLabel)).setContent(tab.mView));
        final GroupAdapter groupAdapter = new GroupAdapter(tab);
        this.mGroupAdapters.add(groupAdapter);
        final int groupCount = groupAdapter.getGroupCount();
        final ViewGroup viewGroup = (ViewGroup)this.mTabHost.findViewById(tab.mContentView);
        viewGroup.getLayoutTransition().enableTransitionType(4);
        final LayoutInflater from = LayoutInflater.from((Context)this.getActivity());
        for (int i = 0; i < groupAdapter.getGroupCount(); ++i) {
            final boolean managedProfile = groupAdapter.getUserInfoByGroup(i).isManagedProfile();
            final ChildAdapter childAdapter = groupAdapter.getChildAdapter(i);
            final LinearLayout containerView = (LinearLayout)from.inflate(2131558854, viewGroup, false);
            childAdapter.setContainerView(containerView);
            final boolean b = true;
            childAdapter.showHeader(groupCount > 1);
            childAdapter.showDivider(managedProfile);
            childAdapter.setExpandIfAvailable((groupCount <= 2 || !managedProfile) && b);
            if (managedProfile) {
                viewGroup.addView((View)containerView);
            }
            else {
                viewGroup.addView((View)containerView, 0);
            }
        }
    }
    
    private void closeKeyChainConnections() {
        synchronized (this.mKeyChainConnectionByProfileId) {
            for (int size = this.mKeyChainConnectionByProfileId.size(), i = 0; i < size; ++i) {
                ((KeyChain$KeyChainConnection)this.mKeyChainConnectionByProfileId.valueAt(i)).close();
            }
            this.mKeyChainConnectionByProfileId.clear();
        }
    }
    
    private boolean isTrustAllCaCertModeInProgress() {
        return this.mTrustAllCaUserId != -10000;
    }
    
    private void showCertDialog(final CertHolder certHolder) {
        new TrustedCredentialsDialogBuilder(this.getActivity(), (TrustedCredentialsDialogBuilder.DelegateInterface)this).setCertHolder(certHolder).show();
    }
    
    private void showTrustAllCaDialog(final List<CertHolder> list) {
        new TrustedCredentialsDialogBuilder(this.getActivity(), (TrustedCredentialsDialogBuilder.DelegateInterface)this).setCertHolders((CertHolder[])list.toArray(new CertHolder[list.size()])).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                TrustedCredentialsSettings.this.getActivity().getIntent().removeExtra("ARG_SHOW_NEW_FOR_USER");
                TrustedCredentialsSettings.this.mTrustAllCaUserId = -10000;
            }
        }).show();
    }
    
    private boolean startConfirmCredential(final int mConfirmingCredentialUser) {
        final Intent confirmDeviceCredentialIntent = this.mKeyguardManager.createConfirmDeviceCredentialIntent((CharSequence)null, (CharSequence)null, mConfirmingCredentialUser);
        if (confirmDeviceCredentialIntent == null) {
            return false;
        }
        this.mConfirmingCredentialUser = mConfirmingCredentialUser;
        this.startActivityForResult(confirmDeviceCredentialIntent, 1);
        return true;
    }
    
    @Override
    public int getMetricsCategory() {
        return 92;
    }
    
    @Override
    public List<X509Certificate> getX509CertsFromCertHolder(final CertHolder certHolder) {
        final List<X509Certificate> list = null;
        final List<X509Certificate> list2 = null;
        List<X509Certificate> list3 = list;
        try {
            final SparseArray<KeyChain$KeyChainConnection> mKeyChainConnectionByProfileId = this.mKeyChainConnectionByProfileId;
            list3 = list;
            // monitorenter(mKeyChainConnectionByProfileId)
            list3 = list2;
            try {
                final IKeyChainService service = ((KeyChain$KeyChainConnection)this.mKeyChainConnectionByProfileId.get(certHolder.mProfileId)).getService();
                list3 = list2;
                final List caCertificateChainAliases = service.getCaCertificateChainAliases(certHolder.mAlias, true);
                list3 = list2;
                final int size = caCertificateChainAliases.size();
                list3 = list2;
                list3 = list2;
                final ArrayList list4 = new ArrayList<X509Certificate>(size);
                for (int i = 0; i < size; ++i) {
                    list3 = (List<X509Certificate>)list4;
                    list4.add(KeyChain.toCertificate(service.getEncodedCaCertificate((String)caCertificateChainAliases.get(i), true)));
                }
                list3 = (List<X509Certificate>)list4;
                // monitorexit(mKeyChainConnectionByProfileId)
                list3 = (List<X509Certificate>)list4;
            }
            finally {
            }
            // monitorexit(mKeyChainConnectionByProfileId)
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("RemoteException while retrieving certificate chain for root ");
            sb.append(certHolder.mAlias);
            Log.e("TrustedCredentialsSettings", sb.toString(), (Throwable)ex);
        }
        return list3;
    }
    
    public void onActivityResult(int mConfirmingCredentialUser, final int n, final Intent intent) {
        if (mConfirmingCredentialUser == 1) {
            mConfirmingCredentialUser = this.mConfirmingCredentialUser;
            final IntConsumer mConfirmingCredentialListener = this.mConfirmingCredentialListener;
            this.mConfirmingCredentialUser = -10000;
            this.mConfirmingCredentialListener = null;
            if (n == -1) {
                this.mConfirmedCredentialUsers.add((Object)mConfirmingCredentialUser);
                if (mConfirmingCredentialListener != null) {
                    mConfirmingCredentialListener.accept(mConfirmingCredentialUser);
                }
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mUserManager = (UserManager)activity.getSystemService("user");
        this.mKeyguardManager = (KeyguardManager)activity.getSystemService("keyguard");
        this.mTrustAllCaUserId = activity.getIntent().getIntExtra("ARG_SHOW_NEW_FOR_USER", -10000);
        this.mConfirmedCredentialUsers = (ArraySet<Integer>)new ArraySet(2);
        this.mConfirmingCredentialUser = -10000;
        if (bundle != null) {
            this.mConfirmingCredentialUser = bundle.getInt("ConfirmingCredentialUser", -10000);
            final ArrayList integerArrayList = bundle.getIntegerArrayList("ConfirmedCredentialUsers");
            if (integerArrayList != null) {
                this.mConfirmedCredentialUsers.addAll((Collection)integerArrayList);
            }
        }
        this.mConfirmingCredentialListener = null;
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_AVAILABLE");
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_UNAVAILABLE");
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_UNLOCKED");
        activity.registerReceiver(this.mWorkProfileChangedReceiver, intentFilter);
        activity.setTitle(2131889492);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        (this.mTabHost = (TabHost)layoutInflater.inflate(2131558855, viewGroup, false)).setup();
        this.addTab(Tab.SYSTEM);
        this.addTab(Tab.USER);
        if (this.getActivity().getIntent() != null && "com.android.settings.TRUSTED_CREDENTIALS_USER".equals(this.getActivity().getIntent().getAction())) {
            this.mTabHost.setCurrentTabByTag(Tab.USER.mTag);
        }
        return (View)this.mTabHost;
    }
    
    @Override
    public void onDestroy() {
        this.getActivity().unregisterReceiver(this.mWorkProfileChangedReceiver);
        final Iterator<AliasLoader> iterator = this.mAliasLoaders.iterator();
        while (iterator.hasNext()) {
            iterator.next().cancel(true);
        }
        this.mAliasLoaders.clear();
        this.mGroupAdapters.clear();
        if (this.mAliasOperation != null) {
            this.mAliasOperation.cancel(true);
            this.mAliasOperation = null;
        }
        this.closeKeyChainConnections();
        super.onDestroy();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putIntegerArrayList("ConfirmedCredentialUsers", new ArrayList((Collection<? extends E>)this.mConfirmedCredentialUsers));
        bundle.putInt("ConfirmingCredentialUser", this.mConfirmingCredentialUser);
    }
    
    @Override
    public void removeOrInstallCert(final CertHolder certHolder) {
        new AliasOperation(certHolder).execute((Object[])new Void[0]);
    }
    
    @Override
    public boolean startConfirmCredentialIfNotConfirmed(final int n, final IntConsumer mConfirmingCredentialListener) {
        if (this.mConfirmedCredentialUsers.contains((Object)n)) {
            return false;
        }
        final boolean startConfirmCredential = this.startConfirmCredential(n);
        if (startConfirmCredential) {
            this.mConfirmingCredentialListener = mConfirmingCredentialListener;
        }
        return startConfirmCredential;
    }
    
    private class AdapterData
    {
        private final GroupAdapter mAdapter;
        private final SparseArray<List<CertHolder>> mCertHoldersByUserId;
        private final Tab mTab;
        final /* synthetic */ TrustedCredentialsSettings this$0;
        
        private AdapterData(final Tab mTab, final GroupAdapter mAdapter) {
            this.mCertHoldersByUserId = (SparseArray<List<CertHolder>>)new SparseArray();
            this.mAdapter = mAdapter;
            this.mTab = mTab;
        }
        
        public void remove(final CertHolder certHolder) {
            if (this.mCertHoldersByUserId != null) {
                final List list = (List)this.mCertHoldersByUserId.get(certHolder.mProfileId);
                if (list != null) {
                    list.remove(certHolder);
                }
            }
        }
        
        private class AliasLoader extends AsyncTask<Void, Integer, SparseArray<List<CertHolder>>>
        {
            private View mContentView;
            private Context mContext;
            private ProgressBar mProgressBar;
            
            public AliasLoader() {
                this.mContext = (Context)AdapterData.this.this$0.getActivity();
                AdapterData.this.this$0.mAliasLoaders.add(this);
                final Iterator<UserHandle> iterator = AdapterData.this.this$0.mUserManager.getUserProfiles().iterator();
                while (iterator.hasNext()) {
                    AdapterData.this.mCertHoldersByUserId.put(iterator.next().getIdentifier(), (Object)new ArrayList());
                }
            }
            
            private boolean isUserTabAndTrustAllCertMode() {
                return TrustedCredentialsSettings.this.isTrustAllCaCertModeInProgress() && AdapterData.this.mTab == Tab.USER;
            }
            
            private boolean shouldSkipProfile(final UserHandle userHandle) {
                return TrustedCredentialsSettings.this.mUserManager.isQuietModeEnabled(userHandle) || !TrustedCredentialsSettings.this.mUserManager.isUserUnlocked(userHandle.getIdentifier());
            }
            
            private void showTrustAllCaDialogIfNeeded() {
                if (!this.isUserTabAndTrustAllCertMode()) {
                    return;
                }
                final List list = (List)AdapterData.this.mCertHoldersByUserId.get(TrustedCredentialsSettings.this.mTrustAllCaUserId);
                if (list == null) {
                    return;
                }
                final ArrayList<CertHolder> list2 = new ArrayList<CertHolder>();
                final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)this.mContext.getSystemService((Class)DevicePolicyManager.class);
                for (final CertHolder certHolder : list) {
                    if (certHolder != null && !devicePolicyManager.isCaCertApproved(certHolder.mAlias, TrustedCredentialsSettings.this.mTrustAllCaUserId)) {
                        list2.add(certHolder);
                    }
                }
                if (list2.size() == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no cert is pending approval for user ");
                    sb.append(TrustedCredentialsSettings.this.mTrustAllCaUserId);
                    Log.w("TrustedCredentialsSettings", sb.toString());
                    return;
                }
                TrustedCredentialsSettings.this.showTrustAllCaDialog(list2);
            }
            
            protected SparseArray<List<CertHolder>> doInBackground(final Void... array) {
                final SparseArray sparseArray = new SparseArray();
                try {
                    synchronized (TrustedCredentialsSettings.this.mKeyChainConnectionByProfileId) {
                        List<UserHandle> userProfiles = (List<UserHandle>)TrustedCredentialsSettings.this.mUserManager.getUserProfiles();
                        final int size = userProfiles.size();
                        SparseArray sparseArray2 = new SparseArray(size);
                        int n = 0;
                        int n2 = 0;
                        for (int i = 0; i < size; ++i) {
                            final UserHandle userHandle = userProfiles.get(i);
                            final int identifier = userHandle.getIdentifier();
                            if (!this.shouldSkipProfile(userHandle)) {
                                final KeyChain$KeyChainConnection bindAsUser = KeyChain.bindAsUser(this.mContext, userHandle);
                                TrustedCredentialsSettings.this.mKeyChainConnectionByProfileId.put(identifier, (Object)bindAsUser);
                                final List access$2700 = AdapterData.this.mTab.getAliases(bindAsUser.getService());
                                if (this.isCancelled()) {
                                    return (SparseArray<List<CertHolder>>)new SparseArray();
                                }
                                n2 += access$2700.size();
                                sparseArray2.put(identifier, (Object)access$2700);
                            }
                        }
                        SparseArray sparseArray3;
                        List<UserHandle> list4;
                        List<UserHandle> list5;
                        for (int j = 0; j < size; ++j, list5 = list4, sparseArray2 = sparseArray3, userProfiles = list5) {
                            final UserHandle userHandle2 = userProfiles.get(j);
                            final int identifier2 = userHandle2.getIdentifier();
                            final List list = (List)sparseArray2.get(identifier2);
                            if (this.isCancelled()) {
                                return (SparseArray<List<CertHolder>>)new SparseArray();
                            }
                            final KeyChain$KeyChainConnection keyChain$KeyChainConnection = (KeyChain$KeyChainConnection)TrustedCredentialsSettings.this.mKeyChainConnectionByProfileId.get(identifier2);
                            if (!this.shouldSkipProfile(userHandle2) && list != null && keyChain$KeyChainConnection != null) {
                                final IKeyChainService service = keyChain$KeyChainConnection.getService();
                                final ArrayList list2 = new ArrayList<CertHolder>(n2);
                                for (int size2 = list.size(), k = 0; k < size2; ++k) {
                                    final String s = list.get(k);
                                    list2.add(new CertHolder(service, AdapterData.this.mAdapter, AdapterData.this.mTab, s, KeyChain.toCertificate(service.getEncodedCaCertificate(s, true)), identifier2));
                                    ++n;
                                    this.publishProgress((Object[])new Integer[] { n, n2 });
                                }
                                final List<UserHandle> list3 = userProfiles;
                                sparseArray3 = sparseArray2;
                                Collections.sort((List)list2);
                                sparseArray.put(identifier2, (Object)list2);
                                list4 = list3;
                            }
                            else {
                                final SparseArray sparseArray4 = sparseArray2;
                                list4 = userProfiles;
                                sparseArray.put(identifier2, (Object)new ArrayList(0));
                                sparseArray3 = sparseArray4;
                            }
                        }
                        return (SparseArray<List<CertHolder>>)sparseArray;
                    }
                }
                catch (InterruptedException ex) {
                    Log.e("TrustedCredentialsSettings", "InterruptedException while loading aliases.", (Throwable)ex);
                    return (SparseArray<List<CertHolder>>)new SparseArray();
                }
                catch (RemoteException ex2) {
                    Log.e("TrustedCredentialsSettings", "Remote exception while loading aliases.", (Throwable)ex2);
                    return (SparseArray<List<CertHolder>>)new SparseArray();
                }
            }
            
            protected void onPostExecute(final SparseArray<List<CertHolder>> sparseArray) {
                AdapterData.this.mCertHoldersByUserId.clear();
                for (int size = sparseArray.size(), i = 0; i < size; ++i) {
                    AdapterData.this.mCertHoldersByUserId.put(sparseArray.keyAt(i), (Object)sparseArray.valueAt(i));
                }
                AdapterData.this.mAdapter.notifyDataSetChanged();
                this.mProgressBar.setVisibility(8);
                this.mContentView.setVisibility(0);
                this.mProgressBar.setProgress(0);
                TrustedCredentialsSettings.this.mAliasLoaders.remove(this);
                this.showTrustAllCaDialogIfNeeded();
            }
            
            protected void onPreExecute() {
                final FrameLayout tabContentView = TrustedCredentialsSettings.this.mTabHost.getTabContentView();
                this.mProgressBar = (ProgressBar)((View)tabContentView).findViewById(AdapterData.this.mTab.mProgress);
                this.mContentView = ((View)tabContentView).findViewById(AdapterData.this.mTab.mContentView);
                this.mProgressBar.setVisibility(0);
                this.mContentView.setVisibility(8);
            }
            
            protected void onProgressUpdate(final Integer... array) {
                final int intValue = array[0];
                final int intValue2 = array[1];
                if (intValue2 != this.mProgressBar.getMax()) {
                    this.mProgressBar.setMax(intValue2);
                }
                this.mProgressBar.setProgress(intValue);
            }
        }
    }
    
    private class AliasOperation extends AsyncTask<Void, Void, Boolean>
    {
        private final CertHolder mCertHolder;
        
        private AliasOperation(final CertHolder mCertHolder) {
            this.mCertHolder = mCertHolder;
            TrustedCredentialsSettings.this.mAliasOperation = this;
        }
        
        protected Boolean doInBackground(final Void... array) {
            try {
                synchronized (TrustedCredentialsSettings.this.mKeyChainConnectionByProfileId) {
                    final IKeyChainService service = ((KeyChain$KeyChainConnection)TrustedCredentialsSettings.this.mKeyChainConnectionByProfileId.get(this.mCertHolder.mProfileId)).getService();
                    if (this.mCertHolder.mDeleted) {
                        service.installCaCertificate(this.mCertHolder.mX509Cert.getEncoded());
                        // monitorexit(TrustedCredentialsSettings.access$2600(this.this$0))
                        return true;
                    }
                    // monitorexit(TrustedCredentialsSettings.access$2600(this.this$0))
                    return service.deleteCaCertificate(this.mCertHolder.mAlias);
                }
            }
            catch (CertificateEncodingException | SecurityException | IllegalStateException | RemoteException ex) {
                final Object o;
                final Throwable t = (Throwable)o;
                final StringBuilder sb = new StringBuilder();
                sb.append("Error while toggling alias ");
                sb.append(this.mCertHolder.mAlias);
                Log.w("TrustedCredentialsSettings", sb.toString(), t);
                return false;
            }
        }
        
        protected void onPostExecute(final Boolean b) {
            if (b) {
                if (this.mCertHolder.mTab.mSwitch) {
                    this.mCertHolder.mDeleted ^= true;
                }
                else {
                    this.mCertHolder.mAdapter.remove(this.mCertHolder);
                }
                this.mCertHolder.mAdapter.notifyDataSetChanged();
            }
            else {
                this.mCertHolder.mAdapter.load();
            }
            TrustedCredentialsSettings.this.mAliasOperation = null;
        }
    }
    
    static class CertHolder implements Comparable<CertHolder>
    {
        private final GroupAdapter mAdapter;
        private final String mAlias;
        private boolean mDeleted;
        public int mProfileId;
        private final IKeyChainService mService;
        private final SslCertificate mSslCert;
        private final String mSubjectPrimary;
        private final String mSubjectSecondary;
        private final Tab mTab;
        private final X509Certificate mX509Cert;
        
        private CertHolder(final IKeyChainService mService, final GroupAdapter mAdapter, final Tab mTab, final String mAlias, final X509Certificate mx509Cert, final int mProfileId) {
            this.mProfileId = mProfileId;
            this.mService = mService;
            this.mAdapter = mAdapter;
            this.mTab = mTab;
            this.mAlias = mAlias;
            this.mX509Cert = mx509Cert;
            this.mSslCert = new SslCertificate(mx509Cert);
            final String cName = this.mSslCert.getIssuedTo().getCName();
            final String oName = this.mSslCert.getIssuedTo().getOName();
            final String uName = this.mSslCert.getIssuedTo().getUName();
            if (!oName.isEmpty()) {
                if (!cName.isEmpty()) {
                    this.mSubjectPrimary = oName;
                    this.mSubjectSecondary = cName;
                }
                else {
                    this.mSubjectPrimary = oName;
                    this.mSubjectSecondary = uName;
                }
            }
            else if (!cName.isEmpty()) {
                this.mSubjectPrimary = cName;
                this.mSubjectSecondary = "";
            }
            else {
                this.mSubjectPrimary = this.mSslCert.getIssuedTo().getDName();
                this.mSubjectSecondary = "";
            }
            try {
                this.mDeleted = this.mTab.deleted(this.mService, this.mAlias);
            }
            catch (RemoteException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Remote exception while checking if alias ");
                sb.append(this.mAlias);
                sb.append(" is deleted.");
                Log.e("TrustedCredentialsSettings", sb.toString(), (Throwable)ex);
                this.mDeleted = false;
            }
        }
        
        @Override
        public int compareTo(final CertHolder certHolder) {
            final int compareToIgnoreCase = this.mSubjectPrimary.compareToIgnoreCase(certHolder.mSubjectPrimary);
            if (compareToIgnoreCase != 0) {
                return compareToIgnoreCase;
            }
            return this.mSubjectSecondary.compareToIgnoreCase(certHolder.mSubjectSecondary);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof CertHolder && this.mAlias.equals(((CertHolder)o).mAlias);
        }
        
        public String getAlias() {
            return this.mAlias;
        }
        
        public int getUserId() {
            return this.mProfileId;
        }
        
        @Override
        public int hashCode() {
            return this.mAlias.hashCode();
        }
        
        public boolean isDeleted() {
            return this.mDeleted;
        }
        
        public boolean isSystemCert() {
            return this.mTab == Tab.SYSTEM;
        }
    }
    
    private class ChildAdapter extends BaseAdapter implements View.OnClickListener, AdapterView$OnItemClickListener
    {
        private final int[] EMPTY_STATE_SET;
        private final int[] GROUP_EXPANDED_STATE_SET;
        private final LinearLayout$LayoutParams HIDE_CONTAINER_LAYOUT_PARAMS;
        private final LinearLayout$LayoutParams HIDE_LIST_LAYOUT_PARAMS;
        private final LinearLayout$LayoutParams SHOW_LAYOUT_PARAMS;
        private LinearLayout mContainerView;
        private final int mGroupPosition;
        private ViewGroup mHeaderView;
        private ImageView mIndicatorView;
        private boolean mIsListExpanded;
        private ListView mListView;
        private final DataSetObserver mObserver;
        private final GroupAdapter mParent;
        
        private ChildAdapter(final GroupAdapter mParent, final int mGroupPosition) {
            this.GROUP_EXPANDED_STATE_SET = new int[] { 16842920 };
            this.EMPTY_STATE_SET = new int[0];
            this.HIDE_CONTAINER_LAYOUT_PARAMS = new LinearLayout$LayoutParams(-1, -2, 0.0f);
            this.HIDE_LIST_LAYOUT_PARAMS = new LinearLayout$LayoutParams(-1, 0);
            this.SHOW_LAYOUT_PARAMS = new LinearLayout$LayoutParams(-1, -1, 1.0f);
            this.mObserver = new DataSetObserver() {
                public void onChanged() {
                    super.onChanged();
                    ChildAdapter.access$2101(ChildAdapter.this);
                }
                
                public void onInvalidated() {
                    super.onInvalidated();
                    ChildAdapter.access$2201(ChildAdapter.this);
                }
            };
            this.mIsListExpanded = true;
            this.mParent = mParent;
            this.mGroupPosition = mGroupPosition;
            this.mParent.registerDataSetObserver(this.mObserver);
        }
        
        static /* synthetic */ void access$2101(final ChildAdapter childAdapter) {
            childAdapter.notifyDataSetChanged();
        }
        
        static /* synthetic */ void access$2201(final ChildAdapter childAdapter) {
            childAdapter.notifyDataSetInvalidated();
        }
        
        private boolean checkGroupExpandableAndStartWarningActivity() {
            return this.mParent.checkGroupExpandableAndStartWarningActivity(this.mGroupPosition);
        }
        
        private Drawable getGroupIndicator() {
            final TypedArray obtainStyledAttributes = TrustedCredentialsSettings.this.getActivity().obtainStyledAttributes((AttributeSet)null, R$styleable.ExpandableListView, 16842863, 0);
            final Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        }
        
        private void refreshViews() {
            final ImageView mIndicatorView = this.mIndicatorView;
            int[] array;
            if (this.mIsListExpanded) {
                array = this.GROUP_EXPANDED_STATE_SET;
            }
            else {
                array = this.EMPTY_STATE_SET;
            }
            mIndicatorView.setImageState(array, false);
            final ListView mListView = this.mListView;
            LinearLayout$LayoutParams layoutParams;
            if (this.mIsListExpanded) {
                layoutParams = this.SHOW_LAYOUT_PARAMS;
            }
            else {
                layoutParams = this.HIDE_LIST_LAYOUT_PARAMS;
            }
            mListView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            final LinearLayout mContainerView = this.mContainerView;
            LinearLayout$LayoutParams layoutParams2;
            if (this.mIsListExpanded) {
                layoutParams2 = this.SHOW_LAYOUT_PARAMS;
            }
            else {
                layoutParams2 = this.HIDE_CONTAINER_LAYOUT_PARAMS;
            }
            mContainerView.setLayoutParams((ViewGroup.LayoutParams)layoutParams2);
        }
        
        public int getCount() {
            return this.mParent.getChildrenCount(this.mGroupPosition);
        }
        
        public CertHolder getItem(final int n) {
            return this.mParent.getChild(this.mGroupPosition, n);
        }
        
        public long getItemId(final int n) {
            return this.mParent.getChildId(this.mGroupPosition, n);
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            return this.mParent.getChildView(this.mGroupPosition, n, false, view, viewGroup);
        }
        
        public void notifyDataSetChanged() {
            this.mParent.notifyDataSetChanged();
        }
        
        public void notifyDataSetInvalidated() {
            this.mParent.notifyDataSetInvalidated();
        }
        
        public void onClick(final View view) {
            this.mIsListExpanded = (this.checkGroupExpandableAndStartWarningActivity() && !this.mIsListExpanded);
            this.refreshViews();
        }
        
        public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
            TrustedCredentialsSettings.this.showCertDialog(this.getItem(n));
        }
        
        public void setContainerView(final LinearLayout mContainerView) {
            this.mContainerView = mContainerView;
            (this.mListView = (ListView)this.mContainerView.findViewById(2131361975)).setAdapter((ListAdapter)this);
            this.mListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
            this.mListView.setItemsCanFocus(true);
            (this.mHeaderView = (ViewGroup)this.mContainerView.findViewById(2131362219)).setOnClickListener((View.OnClickListener)this);
            (this.mIndicatorView = (ImageView)this.mHeaderView.findViewById(2131362182)).setImageDrawable(this.getGroupIndicator());
            final FrameLayout frameLayout = (FrameLayout)this.mHeaderView.findViewById(2131362217);
            frameLayout.addView(this.mParent.getGroupView(this.mGroupPosition, true, null, (ViewGroup)frameLayout));
        }
        
        public void setExpandIfAvailable(final boolean b) {
            boolean mIsListExpanded = false;
            if (b) {
                mIsListExpanded = mIsListExpanded;
                if (this.mParent.checkGroupExpandableAndStartWarningActivity(this.mGroupPosition, false)) {
                    mIsListExpanded = true;
                }
            }
            this.mIsListExpanded = mIsListExpanded;
            this.refreshViews();
        }
        
        public void showDivider(final boolean b) {
            final View viewById = this.mHeaderView.findViewById(2131362218);
            int visibility;
            if (b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
        }
        
        public void showHeader(final boolean b) {
            final ViewGroup mHeaderView = this.mHeaderView;
            int visibility;
            if (b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            mHeaderView.setVisibility(visibility);
        }
    }
    
    private class GroupAdapter extends BaseExpandableListAdapter implements View.OnClickListener, ExpandableListView$OnChildClickListener, ExpandableListView$OnGroupClickListener
    {
        private final AdapterData mData;
        
        private GroupAdapter(final Tab tab) {
            this.mData = new AdapterData(tab, this);
            this.load();
        }
        
        private int getUserIdByGroup(final int n) {
            return this.mData.mCertHoldersByUserId.keyAt(n);
        }
        
        private View getViewForCertificate(final CertHolder tag, final Tab tab, View inflate, final ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (inflate == null) {
                final ViewHolder tag2 = new ViewHolder();
                inflate = LayoutInflater.from((Context)TrustedCredentialsSettings.this.getActivity()).inflate(2131558852, viewGroup, false);
                inflate.setTag((Object)tag2);
                tag2.mSubjectPrimaryView = (TextView)inflate.findViewById(2131362753);
                tag2.mSubjectSecondaryView = (TextView)inflate.findViewById(2131362754);
                tag2.mSwitch = (Switch)inflate.findViewById(2131362752);
                tag2.mSwitch.setOnClickListener((View.OnClickListener)this);
                viewHolder = tag2;
            }
            else {
                viewHolder = (ViewHolder)inflate.getTag();
            }
            viewHolder.mSubjectPrimaryView.setText((CharSequence)tag.mSubjectPrimary);
            viewHolder.mSubjectSecondaryView.setText((CharSequence)tag.mSubjectSecondary);
            if (tab.mSwitch) {
                viewHolder.mSwitch.setChecked(tag.mDeleted ^ true);
                viewHolder.mSwitch.setEnabled(TrustedCredentialsSettings.this.mUserManager.hasUserRestriction("no_config_credentials", new UserHandle(tag.mProfileId)) ^ true);
                viewHolder.mSwitch.setVisibility(0);
                viewHolder.mSwitch.setTag((Object)tag);
            }
            return inflate;
        }
        
        public boolean checkGroupExpandableAndStartWarningActivity(final int n) {
            return this.checkGroupExpandableAndStartWarningActivity(n, true);
        }
        
        public boolean checkGroupExpandableAndStartWarningActivity(int identifier, final boolean b) {
            final UserHandle group = this.getGroup(identifier);
            identifier = group.getIdentifier();
            if (TrustedCredentialsSettings.this.mUserManager.isQuietModeEnabled(group)) {
                final Intent inQuietModeDialogIntent = UnlaunchableAppActivity.createInQuietModeDialogIntent(identifier);
                if (b) {
                    TrustedCredentialsSettings.this.getActivity().startActivity(inQuietModeDialogIntent);
                }
                return false;
            }
            if (!TrustedCredentialsSettings.this.mUserManager.isUserUnlocked(group) && new LockPatternUtils((Context)TrustedCredentialsSettings.this.getActivity()).isSeparateProfileChallengeEnabled(identifier)) {
                if (b) {
                    TrustedCredentialsSettings.this.startConfirmCredential(identifier);
                }
                return false;
            }
            return true;
        }
        
        public CertHolder getChild(final int n, final int n2) {
            return ((List)this.mData.mCertHoldersByUserId.get(this.getUserIdByGroup(n))).get(n2);
        }
        
        public ChildAdapter getChildAdapter(final int n) {
            return new ChildAdapter(this, n);
        }
        
        public long getChildId(final int n, final int n2) {
            return n2;
        }
        
        public View getChildView(final int n, final int n2, final boolean b, final View view, final ViewGroup viewGroup) {
            return this.getViewForCertificate(this.getChild(n, n2), this.mData.mTab, view, viewGroup);
        }
        
        public int getChildrenCount(final int n) {
            final List list = (List)this.mData.mCertHoldersByUserId.valueAt(n);
            if (list != null) {
                return list.size();
            }
            return 0;
        }
        
        public UserHandle getGroup(final int n) {
            return new UserHandle(this.mData.mCertHoldersByUserId.keyAt(n));
        }
        
        public int getGroupCount() {
            return this.mData.mCertHoldersByUserId.size();
        }
        
        public long getGroupId(final int n) {
            return this.getUserIdByGroup(n);
        }
        
        public View getGroupView(final int n, final boolean b, final View view, final ViewGroup viewGroup) {
            View inflateCategoryHeader = view;
            if (view == null) {
                inflateCategoryHeader = Utils.inflateCategoryHeader((LayoutInflater)TrustedCredentialsSettings.this.getActivity().getSystemService("layout_inflater"), viewGroup);
            }
            final TextView textView = (TextView)inflateCategoryHeader.findViewById(16908310);
            if (this.getUserInfoByGroup(n).isManagedProfile()) {
                textView.setText(R.string.category_work);
            }
            else {
                textView.setText(R.string.category_personal);
            }
            textView.setTextAlignment(6);
            return inflateCategoryHeader;
        }
        
        public UserInfo getUserInfoByGroup(final int n) {
            return TrustedCredentialsSettings.this.mUserManager.getUserInfo(this.getUserIdByGroup(n));
        }
        
        public boolean hasStableIds() {
            return false;
        }
        
        public boolean isChildSelectable(final int n, final int n2) {
            return true;
        }
        
        public void load() {
            final AdapterData mData = this.mData;
            Objects.requireNonNull(mData);
            mData.new AliasLoader().execute((Object[])new Void[0]);
        }
        
        public boolean onChildClick(final ExpandableListView expandableListView, final View view, final int n, final int n2, final long n3) {
            TrustedCredentialsSettings.this.showCertDialog(this.getChild(n, n2));
            return true;
        }
        
        public void onClick(final View view) {
            TrustedCredentialsSettings.this.removeOrInstallCert((CertHolder)view.getTag());
        }
        
        public boolean onGroupClick(final ExpandableListView expandableListView, final View view, final int n, final long n2) {
            return this.checkGroupExpandableAndStartWarningActivity(n) ^ true;
        }
        
        public void remove(final CertHolder certHolder) {
            this.mData.remove(certHolder);
        }
        
        private class ViewHolder
        {
            private TextView mSubjectPrimaryView;
            private TextView mSubjectSecondaryView;
            private Switch mSwitch;
        }
    }
    
    private enum Tab
    {
        SYSTEM("system", 2131889500, 2131362719, 2131362718, 2131362715, true), 
        USER("user", 2131889502, 2131362798, 2131362797, 2131362786, false);
        
        private final int mContentView;
        private final int mLabel;
        private final int mProgress;
        private final boolean mSwitch;
        private final String mTag;
        private final int mView;
        
        private Tab(final String mTag, final int mLabel, final int mView, final int mProgress, final int mContentView, final boolean mSwitch) {
            this.mTag = mTag;
            this.mLabel = mLabel;
            this.mView = mView;
            this.mProgress = mProgress;
            this.mContentView = mContentView;
            this.mSwitch = mSwitch;
        }
        
        private boolean deleted(final IKeyChainService keyChainService, final String s) throws RemoteException {
            switch (this) {
                default: {
                    throw new AssertionError();
                }
                case USER: {
                    return false;
                }
                case SYSTEM: {
                    return keyChainService.containsCaAlias(s) ^ true;
                }
            }
        }
        
        private List<String> getAliases(final IKeyChainService keyChainService) throws RemoteException {
            switch (this) {
                default: {
                    throw new AssertionError();
                }
                case USER: {
                    return (List<String>)keyChainService.getUserCaAliases().getList();
                }
                case SYSTEM: {
                    return (List<String>)keyChainService.getSystemCaAliases().getList();
                }
            }
        }
    }
}
