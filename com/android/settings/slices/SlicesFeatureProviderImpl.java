package com.android.settings.slices;

import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.wifi.calling.WifiCallingSliceHelper;
import android.content.Context;

public class SlicesFeatureProviderImpl implements SlicesFeatureProvider
{
    private SliceDataConverter mSliceDataConverter;
    private SlicesIndexer mSlicesIndexer;
    
    @Override
    public WifiCallingSliceHelper getNewWifiCallingSliceHelper(final Context context) {
        return new WifiCallingSliceHelper(context);
    }
    
    @Override
    public SliceDataConverter getSliceDataConverter(final Context context) {
        if (this.mSliceDataConverter == null) {
            this.mSliceDataConverter = new SliceDataConverter(context.getApplicationContext());
        }
        return this.mSliceDataConverter;
    }
    
    public SlicesIndexer getSliceIndexer(final Context context) {
        if (this.mSlicesIndexer == null) {
            this.mSlicesIndexer = new SlicesIndexer(context);
        }
        return this.mSlicesIndexer;
    }
    
    @Override
    public void indexSliceData(final Context context) {
        this.getSliceIndexer(context).indexSliceData();
    }
    
    @Override
    public void indexSliceDataAsync(final Context context) {
        ThreadUtils.postOnBackgroundThread(this.getSliceIndexer(context));
    }
}
