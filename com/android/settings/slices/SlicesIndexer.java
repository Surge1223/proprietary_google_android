package com.android.settings.slices;

import java.util.Iterator;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.android.settings.overlay.FeatureFactory;
import java.util.List;
import android.content.Context;

class SlicesIndexer implements Runnable
{
    private Context mContext;
    private SlicesDatabaseHelper mHelper;
    
    public SlicesIndexer(final Context mContext) {
        this.mContext = mContext;
        this.mHelper = SlicesDatabaseHelper.getInstance(this.mContext);
    }
    
    List<SliceData> getSliceData() {
        return FeatureFactory.getFactory(this.mContext).getSlicesFeatureProvider().getSliceDataConverter(this.mContext).getSliceData();
    }
    
    protected void indexSliceData() {
        if (this.mHelper.isSliceDataIndexed()) {
            Log.d("SlicesIndexer", "Slices already indexed - returning.");
            return;
        }
        final SQLiteDatabase writableDatabase = this.mHelper.getWritableDatabase();
        try {
            final long currentTimeMillis = System.currentTimeMillis();
            writableDatabase.beginTransaction();
            this.mHelper.reconstruct(this.mHelper.getWritableDatabase());
            this.insertSliceData(writableDatabase, this.getSliceData());
            this.mHelper.setIndexedState();
            final StringBuilder sb = new StringBuilder();
            sb.append("Indexing slices database took: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.d("SlicesIndexer", sb.toString());
            writableDatabase.setTransactionSuccessful();
        }
        finally {
            writableDatabase.endTransaction();
        }
    }
    
    void insertSliceData(final SQLiteDatabase sqLiteDatabase, final List<SliceData> list) {
        for (final SliceData sliceData : list) {
            final ContentValues contentValues = new ContentValues();
            contentValues.put("key", sliceData.getKey());
            contentValues.put("title", sliceData.getTitle());
            contentValues.put("summary", sliceData.getSummary());
            contentValues.put("screentitle", sliceData.getScreenTitle().toString());
            contentValues.put("keywords", sliceData.getKeywords());
            contentValues.put("icon", sliceData.getIconResource());
            contentValues.put("fragment", sliceData.getFragmentClassName());
            contentValues.put("controller", sliceData.getPreferenceController());
            contentValues.put("platform_slice", sliceData.isPlatformDefined());
            contentValues.put("slice_type", sliceData.getSliceType());
            sqLiteDatabase.replaceOrThrow("slices_index", (String)null, contentValues);
        }
    }
    
    @Override
    public void run() {
        this.indexSliceData();
    }
}
