package com.android.settings.slices;

import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.os.Binder;
import android.net.Uri;
import android.database.Cursor;
import android.content.Context;

public class SlicesDatabaseAccessor
{
    public static final String[] SELECT_COLUMNS_ALL;
    private final int TRUE;
    private final Context mContext;
    private final SlicesDatabaseHelper mHelper;
    
    static {
        SELECT_COLUMNS_ALL = new String[] { "key", "title", "summary", "screentitle", "keywords", "icon", "fragment", "controller", "platform_slice", "slice_type" };
    }
    
    public SlicesDatabaseAccessor(final Context mContext) {
        this.TRUE = 1;
        this.mContext = mContext;
        this.mHelper = SlicesDatabaseHelper.getInstance(this.mContext);
    }
    
    private String buildKeyMatchWhereClause() {
        final StringBuilder sb = new StringBuilder("key");
        sb.append(" = ?");
        return sb.toString();
    }
    
    private SliceData buildSliceData(final Cursor cursor, final Uri uri, final boolean b) {
        final String string = cursor.getString(cursor.getColumnIndex("key"));
        final String string2 = cursor.getString(cursor.getColumnIndex("title"));
        final String string3 = cursor.getString(cursor.getColumnIndex("summary"));
        final String string4 = cursor.getString(cursor.getColumnIndex("screentitle"));
        final String string5 = cursor.getString(cursor.getColumnIndex("keywords"));
        final int int1 = cursor.getInt(cursor.getColumnIndex("icon"));
        final String string6 = cursor.getString(cursor.getColumnIndex("fragment"));
        final String string7 = cursor.getString(cursor.getColumnIndex("controller"));
        final int int2 = cursor.getInt(cursor.getColumnIndex("platform_slice"));
        boolean platformDefined = true;
        if (int2 != 1) {
            platformDefined = false;
        }
        int int3 = cursor.getInt(cursor.getColumnIndex("slice_type"));
        if (b) {
            int3 = 0;
        }
        return new SliceData.Builder().setKey(string).setTitle(string2).setSummary(string3).setScreenTitle(string4).setKeywords(string5).setIcon(int1).setFragmentName(string6).setPreferenceControllerClassName(string7).setUri(uri).setPlatformDefined(platformDefined).setSliceType(int3).build();
    }
    
    private Cursor getIndexedSliceData(final String s) {
        this.verifyIndexing();
        final Cursor query = this.mHelper.getReadableDatabase().query("slices_index", SlicesDatabaseAccessor.SELECT_COLUMNS_ALL, this.buildKeyMatchWhereClause(), new String[] { s }, (String)null, (String)null, (String)null);
        final int count = query.getCount();
        if (count == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid Slices key from path: ");
            sb.append(s);
            throw new IllegalStateException(sb.toString());
        }
        if (count <= 1) {
            query.moveToFirst();
            return query;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Should not match more than 1 slice with path: ");
        sb2.append(s);
        throw new IllegalStateException(sb2.toString());
    }
    
    private void verifyIndexing() {
        final long clearCallingIdentity = Binder.clearCallingIdentity();
        try {
            FeatureFactory.getFactory(this.mContext).getSlicesFeatureProvider().indexSliceData(this.mContext);
        }
        finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }
    
    public SliceData getSliceDataFromKey(final String s) {
        return this.buildSliceData(this.getIndexedSliceData(s), null, false);
    }
    
    public SliceData getSliceDataFromUri(final Uri uri) {
        final Pair<Boolean, String> pathData = SliceBuilderUtils.getPathData(uri);
        return this.buildSliceData(this.getIndexedSliceData((String)pathData.second), uri, (boolean)pathData.first);
    }
    
    public List<String> getSliceKeys(final boolean b) {
        this.verifyIndexing();
        String s;
        if (b) {
            s = "platform_slice = 1";
        }
        else {
            s = "platform_slice = 0";
        }
        final SQLiteDatabase readableDatabase = this.mHelper.getReadableDatabase();
        final ArrayList<String> list = new ArrayList<String>();
        final Cursor query = readableDatabase.query("slices_index", new String[] { "key" }, s, (String[])null, (String)null, (String)null, (String)null);
        Object o2;
        final Object o = o2 = null;
        while (true) {
            try {
                try {
                    if (!query.moveToFirst()) {
                        if (query != null) {
                            query.close();
                        }
                        return list;
                    }
                    do {
                        o2 = o;
                        list.add(query.getString(0));
                        o2 = o;
                    } while (query.moveToNext());
                    if (query != null) {
                        query.close();
                    }
                    return list;
                }
                finally {
                    if (query != null) {
                        if (o2 != null) {
                            final Cursor cursor = query;
                            cursor.close();
                        }
                        else {
                            query.close();
                        }
                    }
                }
            }
            catch (Throwable t) {}
            try {
                final Cursor cursor = query;
                cursor.close();
                continue;
            }
            catch (Throwable t2) {}
            break;
        }
    }
}
