package com.android.settings.slices;

import android.os.Build;
import java.util.Locale;
import android.util.Log;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public class SlicesDatabaseHelper extends SQLiteOpenHelper
{
    private static SlicesDatabaseHelper sSingleton;
    private final Context mContext;
    
    private SlicesDatabaseHelper(final Context mContext) {
        super(mContext, "slices_index.db", (SQLiteDatabase$CursorFactory)null, 2);
        this.mContext = mContext;
    }
    
    private void createDatabases(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE VIRTUAL TABLE slices_index USING fts4(key, title, summary, screentitle, keywords, icon, fragment, controller, platform_slice, slice_type);");
        Log.d("SlicesDatabaseHelper", "Created databases");
    }
    
    private void dropTables(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS slices_index");
    }
    
    public static SlicesDatabaseHelper getInstance(final Context context) {
        synchronized (SlicesDatabaseHelper.class) {
            if (SlicesDatabaseHelper.sSingleton == null) {
                SlicesDatabaseHelper.sSingleton = new SlicesDatabaseHelper(context.getApplicationContext());
            }
            return SlicesDatabaseHelper.sSingleton;
        }
    }
    
    private boolean isBuildIndexed() {
        return this.mContext.getSharedPreferences("slices_shared_prefs", 0).getBoolean(this.getBuildTag(), false);
    }
    
    private boolean isLocaleIndexed() {
        return this.mContext.getSharedPreferences("slices_shared_prefs", 0).getBoolean(Locale.getDefault().toString(), false);
    }
    
    private void setBuildIndexed() {
        this.mContext.getSharedPreferences("slices_shared_prefs", 0).edit().putBoolean(this.getBuildTag(), true).apply();
    }
    
    private void setLocaleIndexed() {
        this.mContext.getSharedPreferences("slices_shared_prefs", 0).edit().putBoolean(Locale.getDefault().toString(), true).apply();
    }
    
    String getBuildTag() {
        return Build.FINGERPRINT;
    }
    
    public boolean isSliceDataIndexed() {
        return this.isBuildIndexed() && this.isLocaleIndexed();
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        this.createDatabases(sqLiteDatabase);
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        if (n < 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Reconstructing DB from ");
            sb.append(n);
            sb.append("to ");
            sb.append(n2);
            Log.d("SlicesDatabaseHelper", sb.toString());
            this.reconstruct(sqLiteDatabase);
        }
    }
    
    void reconstruct(final SQLiteDatabase sqLiteDatabase) {
        this.mContext.getSharedPreferences("slices_shared_prefs", 0).edit().clear().apply();
        this.dropTables(sqLiteDatabase);
        this.createDatabases(sqLiteDatabase);
    }
    
    public void setIndexedState() {
        this.setBuildIndexed();
        this.setLocaleIndexed();
    }
}
