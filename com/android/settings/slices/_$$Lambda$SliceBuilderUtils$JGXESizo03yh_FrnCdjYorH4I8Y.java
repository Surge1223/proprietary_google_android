package com.android.settings.slices;

import com.android.internal.annotations.VisibleForTesting;
import android.net.Uri;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import android.net.Uri.Builder;
import android.content.Intent;
import android.app.PendingIntent;
import com.android.settings.core.TogglePreferenceController;
import com.android.settings.core.SliderPreferenceController;
import java.util.Collection;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.Arrays;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.util.Log;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settingslib.Utils;
import androidx.slice.Slice;
import com.android.settings.core.BasePreferenceController;
import android.content.Context;
import androidx.slice.builders.ListBuilder;
import androidx.slice.builders.SliceAction;
import android.support.v4.graphics.drawable.IconCompat;
import android.support.v4.util.Consumer;

public final class _$$Lambda$SliceBuilderUtils$JGXESizo03yh_FrnCdjYorH4I8Y implements Consumer
{
    @Override
    public final void accept(final Object o) {
        SliceBuilderUtils.lambda$buildUnavailableSlice$4(this.f$0, this.f$1, this.f$2, this.f$3, (ListBuilder.RowBuilder)o);
    }
}
