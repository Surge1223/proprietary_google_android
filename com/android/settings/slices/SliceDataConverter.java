package com.android.settings.slices;

import android.content.res.XmlResourceParser;
import com.android.settings.search.DatabaseIndexingUtils;
import com.android.settings.overlay.FeatureFactory;
import com.android.internal.annotations.VisibleForTesting;
import android.view.accessibility.AccessibilityManager;
import com.android.settings.core.BasePreferenceController;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.res.Resources$NotFoundException;
import android.text.TextUtils;
import android.os.Bundle;
import com.android.settings.core.PreferenceXmlParserUtils;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.provider.SearchIndexableResource;
import com.android.settings.search.Indexable;
import android.content.pm.ServiceInfo;
import android.content.pm.ResolveInfo;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.ComponentName;
import android.accessibilityservice.AccessibilityServiceInfo;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import com.android.settings.accessibility.AccessibilitySlicePreferenceController;
import com.android.settings.accessibility.AccessibilitySettings;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;

class SliceDataConverter
{
    private Context mContext;
    private List<SliceData> mSliceData;
    
    public SliceDataConverter(final Context mContext) {
        this.mContext = mContext;
        this.mSliceData = new ArrayList<SliceData>();
    }
    
    private List<SliceData> getAccessibilitySliceData() {
        final ArrayList<SliceData> list = new ArrayList<SliceData>();
        final SliceData.Builder setPreferenceControllerClassName = new SliceData.Builder().setFragmentName(AccessibilitySettings.class.getName()).setScreenTitle(this.mContext.getText(2131886186)).setPreferenceControllerClassName(AccessibilitySlicePreferenceController.class.getName());
        final HashSet<Object> set = new HashSet<Object>();
        Collections.addAll(set, this.mContext.getResources().getStringArray(2130903105));
        final List<AccessibilityServiceInfo> accessibilityServiceInfoList = this.getAccessibilityServiceInfoList();
        final PackageManager packageManager = this.mContext.getPackageManager();
        final Iterator<AccessibilityServiceInfo> iterator = accessibilityServiceInfoList.iterator();
        while (iterator.hasNext()) {
            final ResolveInfo resolveInfo = iterator.next().getResolveInfo();
            final ServiceInfo serviceInfo = resolveInfo.serviceInfo;
            final String flattenToString = new ComponentName(serviceInfo.packageName, serviceInfo.name).flattenToString();
            if (!set.contains(flattenToString)) {
                continue;
            }
            final String string = resolveInfo.loadLabel(packageManager).toString();
            int iconResource;
            if ((iconResource = resolveInfo.getIconResource()) == 0) {
                iconResource = 2131689472;
            }
            setPreferenceControllerClassName.setKey(flattenToString).setTitle(string).setIcon(iconResource).setSliceType(1);
            try {
                list.add(setPreferenceControllerClassName.build());
            }
            catch (SliceData.InvalidSliceDataException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid data when building a11y SliceData for ");
                sb.append(flattenToString);
                Log.w("SliceDataConverter", sb.toString(), (Throwable)ex);
            }
        }
        return list;
    }
    
    private List<SliceData> getSliceDataFromProvider(final Indexable.SearchIndexProvider searchIndexProvider, final String s) {
        final ArrayList<Object> list = new ArrayList<Object>();
        final List<SearchIndexableResource> xmlResourcesToIndex = searchIndexProvider.getXmlResourcesToIndex(this.mContext, true);
        if (xmlResourcesToIndex == null) {
            return (List<SliceData>)list;
        }
        final Iterator<SearchIndexableResource> iterator = xmlResourcesToIndex.iterator();
        while (iterator.hasNext()) {
            final int xmlResId = iterator.next().xmlResId;
            if (xmlResId == 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append(" provides invalid XML (0) in search provider.");
                Log.e("SliceDataConverter", sb.toString());
            }
            else {
                list.addAll(this.getSliceDataFromXML(xmlResId, s));
            }
        }
        return (List<SliceData>)list;
    }
    
    private List<SliceData> getSliceDataFromXML(int n, final String fragmentName) {
        Object o = null;
        Object o2 = null;
        Object o3 = null;
        Object o4 = null;
        Object o5 = null;
        final ArrayList<SliceData> list = new ArrayList<SliceData>();
        while (true) {
            try {
                final Object xml = this.mContext.getResources().getXml(n);
                int next;
                do {
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                } while ((next = ((XmlResourceParser)xml).next()) != 1 && next != 2);
                o5 = xml;
                o = xml;
                o2 = xml;
                o3 = xml;
                o4 = xml;
                final String name = ((XmlResourceParser)xml).getName();
                o5 = xml;
                o = xml;
                o2 = xml;
                o3 = xml;
                o4 = xml;
                if (!"PreferenceScreen".equals(name)) {
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final StringBuilder sb = new StringBuilder();
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    sb.append("XML document must start with <PreferenceScreen> tag; found");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    sb.append(name);
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    sb.append(" at ");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    sb.append(((XmlResourceParser)xml).getPositionDescription());
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final RuntimeException ex = new RuntimeException(sb.toString());
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    throw ex;
                }
                o5 = xml;
                o = xml;
                o2 = xml;
                o3 = xml;
                o4 = xml;
                final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
                o5 = xml;
                o = xml;
                o2 = xml;
                o3 = xml;
                o4 = xml;
                final String dataTitle = PreferenceXmlParserUtils.getDataTitle(this.mContext, attributeSet);
                o5 = xml;
                o = xml;
                o2 = xml;
                o3 = xml;
                o4 = xml;
                final List<Bundle> metadata = PreferenceXmlParserUtils.extractMetadata(this.mContext, n, 254);
                o5 = xml;
                o = xml;
                o2 = xml;
                o3 = xml;
                o4 = xml;
                final Iterator<Bundle> iterator = metadata.iterator();
                n = next;
                while (true) {
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final Bundle bundle = iterator.next();
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final String string = bundle.getString("controller");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    if (TextUtils.isEmpty((CharSequence)string)) {
                        continue;
                    }
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final String string2 = bundle.getString("key");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final String string3 = bundle.getString("title");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final String string4 = bundle.getString("summary");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final int int1 = bundle.getInt("icon");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final int sliceType = SliceBuilderUtils.getSliceType(this.mContext, string, string2);
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final boolean boolean1 = bundle.getBoolean("platform_slice");
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final SliceData.Builder builder = new SliceData.Builder();
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final SliceData build = builder.setKey(string2).setTitle(string3).setSummary(string4).setIcon(int1).setScreenTitle(dataTitle).setPreferenceControllerClassName(string).setFragmentName(fragmentName).setSliceType(sliceType).setPlatformDefined(boolean1).build();
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    final BasePreferenceController preferenceController = SliceBuilderUtils.getPreferenceController(this.mContext, build);
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    if (!preferenceController.isAvailable()) {
                        continue;
                    }
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    if (!preferenceController.isSliceable()) {
                        continue;
                    }
                    o5 = xml;
                    o = xml;
                    o2 = xml;
                    o3 = xml;
                    o4 = xml;
                    list.add(build);
                }
                if (xml != null) {
                    ((XmlResourceParser)xml).close();
                    return list;
                }
                return list;
            }
            catch (Resources$NotFoundException ex2) {}
            catch (IOException ex3) {}
            catch (XmlPullParserException ex4) {}
            catch (SliceData.InvalidSliceDataException ex5) {}
            finally {
                if (o5 != null) {
                    ((XmlResourceParser)o5).close();
                }
                Object xml = o;
                continue;
                xml = o4;
                continue;
                xml = o2;
                continue;
                xml = o3;
                continue;
            }
            break;
        }
    }
    
    @VisibleForTesting
    List<AccessibilityServiceInfo> getAccessibilityServiceInfoList() {
        return (List<AccessibilityServiceInfo>)AccessibilityManager.getInstance(this.mContext).getInstalledAccessibilityServiceList();
    }
    
    public List<SliceData> getSliceData() {
        if (!this.mSliceData.isEmpty()) {
            return this.mSliceData;
        }
        for (final Class<?> clazz : FeatureFactory.getFactory(this.mContext).getSearchFeatureProvider().getSearchIndexableResources().getProviderValues()) {
            final String name = clazz.getName();
            final Indexable.SearchIndexProvider searchIndexProvider = DatabaseIndexingUtils.getSearchIndexProvider(clazz);
            if (searchIndexProvider == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(name);
                sb.append(" dose not implement Search Index Provider");
                Log.e("SliceDataConverter", sb.toString());
            }
            else {
                this.mSliceData.addAll(this.getSliceDataFromProvider(searchIndexProvider, name));
            }
        }
        this.mSliceData.addAll(this.getAccessibilitySliceData());
        return this.mSliceData;
    }
}
