package com.android.settings.slices;

import android.database.ContentObserver;
import android.net.Uri;
import android.text.TextUtils;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class SliceRelayReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final String stringExtra = intent.getStringExtra("uri");
        if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
            context.getContentResolver().notifyChange(Uri.parse(stringExtra), (ContentObserver)null);
        }
    }
}
