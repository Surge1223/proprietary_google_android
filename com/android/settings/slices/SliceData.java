package com.android.settings.slices;

import android.text.TextUtils;
import android.net.Uri;

public class SliceData
{
    private final String mFragmentClassName;
    private final int mIconResource;
    private final boolean mIsPlatformDefined;
    private final String mKey;
    private final String mKeywords;
    private final String mPreferenceController;
    private final CharSequence mScreenTitle;
    private final int mSliceType;
    private final String mSummary;
    private final String mTitle;
    private final Uri mUri;
    
    private SliceData(final Builder builder) {
        this.mKey = builder.mKey;
        this.mTitle = builder.mTitle;
        this.mSummary = builder.mSummary;
        this.mScreenTitle = builder.mScreenTitle;
        this.mKeywords = builder.mKeywords;
        this.mIconResource = builder.mIconResource;
        this.mFragmentClassName = builder.mFragmentClassName;
        this.mUri = builder.mUri;
        this.mPreferenceController = builder.mPrefControllerClassName;
        this.mSliceType = builder.mSliceType;
        this.mIsPlatformDefined = builder.mIsPlatformDefined;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SliceData && TextUtils.equals((CharSequence)this.mKey, (CharSequence)((SliceData)o).mKey);
    }
    
    public String getFragmentClassName() {
        return this.mFragmentClassName;
    }
    
    public int getIconResource() {
        return this.mIconResource;
    }
    
    public String getKey() {
        return this.mKey;
    }
    
    public String getKeywords() {
        return this.mKeywords;
    }
    
    public String getPreferenceController() {
        return this.mPreferenceController;
    }
    
    public CharSequence getScreenTitle() {
        return this.mScreenTitle;
    }
    
    public int getSliceType() {
        return this.mSliceType;
    }
    
    public String getSummary() {
        return this.mSummary;
    }
    
    public String getTitle() {
        return this.mTitle;
    }
    
    public Uri getUri() {
        return this.mUri;
    }
    
    @Override
    public int hashCode() {
        return this.mKey.hashCode();
    }
    
    public boolean isPlatformDefined() {
        return this.mIsPlatformDefined;
    }
    
    static class Builder
    {
        private String mFragmentClassName;
        private int mIconResource;
        private boolean mIsPlatformDefined;
        private String mKey;
        private String mKeywords;
        private String mPrefControllerClassName;
        private CharSequence mScreenTitle;
        private int mSliceType;
        private String mSummary;
        private String mTitle;
        private Uri mUri;
        
        public SliceData build() {
            if (TextUtils.isEmpty((CharSequence)this.mKey)) {
                throw new InvalidSliceDataException("Key cannot be empty");
            }
            if (TextUtils.isEmpty((CharSequence)this.mTitle)) {
                throw new InvalidSliceDataException("Title cannot be empty");
            }
            if (TextUtils.isEmpty((CharSequence)this.mFragmentClassName)) {
                throw new InvalidSliceDataException("Fragment Name cannot be empty");
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPrefControllerClassName)) {
                return new SliceData(this, null);
            }
            throw new InvalidSliceDataException("Preference Controller cannot be empty");
        }
        
        public Builder setFragmentName(final String mFragmentClassName) {
            this.mFragmentClassName = mFragmentClassName;
            return this;
        }
        
        public Builder setIcon(final int mIconResource) {
            this.mIconResource = mIconResource;
            return this;
        }
        
        public Builder setKey(final String mKey) {
            this.mKey = mKey;
            return this;
        }
        
        public Builder setKeywords(final String mKeywords) {
            this.mKeywords = mKeywords;
            return this;
        }
        
        public Builder setPlatformDefined(final boolean mIsPlatformDefined) {
            this.mIsPlatformDefined = mIsPlatformDefined;
            return this;
        }
        
        public Builder setPreferenceControllerClassName(final String mPrefControllerClassName) {
            this.mPrefControllerClassName = mPrefControllerClassName;
            return this;
        }
        
        public Builder setScreenTitle(final CharSequence mScreenTitle) {
            this.mScreenTitle = mScreenTitle;
            return this;
        }
        
        public Builder setSliceType(final int mSliceType) {
            this.mSliceType = mSliceType;
            return this;
        }
        
        public Builder setSummary(final String mSummary) {
            this.mSummary = mSummary;
            return this;
        }
        
        public Builder setTitle(final String mTitle) {
            this.mTitle = mTitle;
            return this;
        }
        
        public Builder setUri(final Uri mUri) {
            this.mUri = mUri;
            return this;
        }
    }
    
    public static class InvalidSliceDataException extends RuntimeException
    {
        public InvalidSliceDataException(final String s) {
            super(s);
        }
    }
}
