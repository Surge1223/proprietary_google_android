package com.android.settings.slices;

import android.content.BroadcastReceiver;
import com.android.settingslib.SliceBroadcastRelay;
import java.net.URISyntaxException;
import android.content.Intent;
import android.text.TextUtils;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import android.os.StrictMode$ThreadPolicy;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.wifi.calling.WifiCallingSliceHelper;
import android.os.StrictMode$ThreadPolicy$Builder;
import android.os.StrictMode;
import com.android.settingslib.utils.ThreadUtils;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.app.slice.SliceManager;
import android.util.Log;
import java.util.Collection;
import java.util.Collections;
import android.provider.Settings;
import com.android.settings.location.LocationSliceBuilder;
import com.android.settings.bluetooth.BluetoothSliceBuilder;
import com.android.settings.wifi.WifiSliceBuilder;
import java.util.Arrays;
import com.android.settings.notification.ZenModeSliceBuilder;
import androidx.slice.Slice;
import java.util.Iterator;
import android.net.Uri.Builder;
import java.util.ArrayList;
import java.util.List;
import android.util.ArraySet;
import java.util.Map;
import android.net.Uri;
import java.util.Set;
import android.util.KeyValueListParser;
import androidx.slice.SliceProvider;

public class SettingsSliceProvider extends SliceProvider
{
    private final KeyValueListParser mParser;
    final Set<Uri> mRegisteredUris;
    Map<Uri, SliceData> mSliceDataCache;
    Map<Uri, SliceData> mSliceWeakDataCache;
    SlicesDatabaseAccessor mSlicesDatabaseAccessor;
    
    public SettingsSliceProvider() {
        super(new String[] { "android.permission.READ_SEARCH_INDEXABLES" });
        this.mRegisteredUris = (Set<Uri>)new ArraySet();
        this.mParser = new KeyValueListParser(',');
    }
    
    private List<Uri> buildUrisFromKeys(final List<String> list, final String s) {
        final ArrayList<Uri> list2 = new ArrayList<Uri>();
        final Uri.Builder appendPath = new Uri.Builder().scheme("content").authority(s).appendPath("action");
        for (final String s2 : list) {
            final StringBuilder sb = new StringBuilder();
            sb.append("action/");
            sb.append(s2);
            appendPath.path(sb.toString());
            list2.add(appendPath.build());
        }
        return list2;
    }
    
    private Slice getSliceStub(final Uri uri) {
        return new Slice.Builder(uri).build();
    }
    
    private List<Uri> getSpecialCaseOemUris() {
        return Arrays.asList(ZenModeSliceBuilder.ZEN_MODE_URI);
    }
    
    private List<Uri> getSpecialCasePlatformUris() {
        return Arrays.asList(WifiSliceBuilder.WIFI_URI, BluetoothSliceBuilder.BLUETOOTH_URI, LocationSliceBuilder.LOCATION_URI);
    }
    
    private List<Uri> getSpecialCaseUris(final boolean b) {
        if (b) {
            return this.getSpecialCasePlatformUris();
        }
        return this.getSpecialCaseOemUris();
    }
    
    private String[] parseStringArray(final String s) {
        if (s != null) {
            final String[] split = s.split(":");
            if (split.length > 0) {
                return split;
            }
        }
        return new String[0];
    }
    
    Set<String> getBlockedKeys() {
        final String string = Settings.Global.getString(this.getContext().getContentResolver(), "blocked_slices");
        final ArraySet set = new ArraySet();
        try {
            this.mParser.setString(string);
            Collections.addAll((Collection<? super String>)set, this.parseStringArray(string));
            return (Set<String>)set;
        }
        catch (IllegalArgumentException ex) {
            Log.e("SettingsSliceProvider", "Bad Settings Slices Whitelist flags", (Throwable)ex);
            return (Set<String>)set;
        }
    }
    
    void loadSlice(final Uri uri) {
        final long currentTimeMillis = System.currentTimeMillis();
        try {
            final SliceData sliceDataFromUri = this.mSlicesDatabaseAccessor.getSliceDataFromUri(uri);
            final IntentFilter intentFilter = SliceBuilderUtils.getPreferenceController(this.getContext(), sliceDataFromUri).getIntentFilter();
            if (intentFilter != null) {
                this.registerIntentToUri(intentFilter, uri);
            }
            if (((SliceManager)this.getContext().getSystemService((Class)SliceManager.class)).getPinnedSlices().contains(uri)) {
                this.mSliceDataCache.put(uri, sliceDataFromUri);
            }
            this.mSliceWeakDataCache.put(uri, sliceDataFromUri);
            this.getContext().getContentResolver().notifyChange(uri, (ContentObserver)null);
            final StringBuilder sb = new StringBuilder();
            sb.append("Built slice (");
            sb.append(uri);
            sb.append(") in: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.d("SettingsSliceProvider", sb.toString());
        }
        catch (IllegalStateException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not get slice data for uri: ");
            sb2.append(uri);
            Log.e("SettingsSliceProvider", sb2.toString(), (Throwable)ex);
        }
    }
    
    void loadSliceInBackground(final Uri uri) {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$SettingsSliceProvider$3mq4GNawZ0Wc_zLrSLnj1f92or0(this, uri));
    }
    
    @Override
    public Slice onBindSlice(final Uri uri) {
        final StrictMode$ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        try {
            if (!ThreadUtils.isMainThread()) {
                StrictMode.setThreadPolicy(new StrictMode$ThreadPolicy$Builder().permitAll().build());
            }
            if (this.getBlockedKeys().contains(uri.getLastPathSegment())) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Requested blocked slice with Uri: ");
                sb.append(uri);
                Log.e("SettingsSliceProvider", sb.toString());
                return null;
            }
            if (WifiCallingSliceHelper.WIFI_CALLING_URI.equals((Object)uri)) {
                return FeatureFactory.getFactory(this.getContext()).getSlicesFeatureProvider().getNewWifiCallingSliceHelper(this.getContext()).createWifiCallingSlice(uri);
            }
            if (WifiSliceBuilder.WIFI_URI.equals((Object)uri)) {
                return WifiSliceBuilder.getSlice(this.getContext());
            }
            if (ZenModeSliceBuilder.ZEN_MODE_URI.equals((Object)uri)) {
                return ZenModeSliceBuilder.getSlice(this.getContext());
            }
            if (BluetoothSliceBuilder.BLUETOOTH_URI.equals((Object)uri)) {
                return BluetoothSliceBuilder.getSlice(this.getContext());
            }
            if (LocationSliceBuilder.LOCATION_URI.equals((Object)uri)) {
                return LocationSliceBuilder.getSlice(this.getContext());
            }
            final SliceData sliceData = this.mSliceWeakDataCache.get(uri);
            if (sliceData == null) {
                this.loadSliceInBackground(uri);
                return this.getSliceStub(uri);
            }
            if (!this.mSliceDataCache.containsKey(uri)) {
                this.mSliceWeakDataCache.remove(uri);
            }
            return SliceBuilderUtils.buildSlice(this.getContext(), sliceData);
        }
        finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }
    
    @Override
    public boolean onCreateSliceProvider() {
        this.mSlicesDatabaseAccessor = new SlicesDatabaseAccessor(this.getContext());
        this.mSliceDataCache = new ConcurrentHashMap<Uri, SliceData>();
        this.mSliceWeakDataCache = new WeakHashMap<Uri, SliceData>();
        return true;
    }
    
    @Override
    public Collection<Uri> onGetSliceDescendants(final Uri uri) {
        final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<Uri>();
        if (SliceBuilderUtils.getPathData(uri) != null) {
            list.add(uri);
            return (Collection<Uri>)list;
        }
        final String authority = uri.getAuthority();
        final String path = uri.getPath();
        final boolean empty = path.isEmpty();
        if (empty && TextUtils.isEmpty((CharSequence)authority)) {
            final List<String> sliceKeys = this.mSlicesDatabaseAccessor.getSliceKeys(true);
            final List<String> sliceKeys2 = this.mSlicesDatabaseAccessor.getSliceKeys(false);
            list.addAll(this.buildUrisFromKeys(sliceKeys, "android.settings.slices"));
            list.addAll(this.buildUrisFromKeys(sliceKeys2, "com.android.settings.slices"));
            list.addAll(this.getSpecialCaseUris(true));
            list.addAll(this.getSpecialCaseUris(false));
            return (Collection<Uri>)list;
        }
        if (!empty && !TextUtils.equals((CharSequence)path, (CharSequence)"/action") && !TextUtils.equals((CharSequence)path, (CharSequence)"/intent")) {
            return (Collection<Uri>)list;
        }
        final boolean equals = TextUtils.equals((CharSequence)authority, (CharSequence)"android.settings.slices");
        list.addAll(this.buildUrisFromKeys(this.mSlicesDatabaseAccessor.getSliceKeys(equals), authority));
        list.addAll(this.getSpecialCaseUris(equals));
        return (Collection<Uri>)list;
    }
    
    @Override
    public Uri onMapIntentToUri(final Intent intent) {
        try {
            return ((SliceManager)this.getContext().getSystemService((Class)SliceManager.class)).mapIntentToUri(SliceDeepLinkSpringBoard.parse(intent.getData(), this.getContext().getPackageName()));
        }
        catch (URISyntaxException ex) {
            return null;
        }
    }
    
    @Override
    public void onSlicePinned(final Uri uri) {
        if (WifiSliceBuilder.WIFI_URI.equals((Object)uri)) {
            this.registerIntentToUri(WifiSliceBuilder.INTENT_FILTER, uri);
            return;
        }
        if (ZenModeSliceBuilder.ZEN_MODE_URI.equals((Object)uri)) {
            this.registerIntentToUri(ZenModeSliceBuilder.INTENT_FILTER, uri);
            return;
        }
        if (BluetoothSliceBuilder.BLUETOOTH_URI.equals((Object)uri)) {
            this.registerIntentToUri(BluetoothSliceBuilder.INTENT_FILTER, uri);
            return;
        }
        this.loadSliceInBackground(uri);
    }
    
    @Override
    public void onSliceUnpinned(final Uri uri) {
        if (this.mRegisteredUris.contains(uri)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unregistering uri broadcast relay: ");
            sb.append(uri);
            Log.d("SettingsSliceProvider", sb.toString());
            SliceBroadcastRelay.unregisterReceivers(this.getContext(), uri);
            this.mRegisteredUris.remove(uri);
        }
        this.mSliceDataCache.remove(uri);
    }
    
    void registerIntentToUri(final IntentFilter intentFilter, final Uri uri) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Registering Uri for broadcast relay: ");
        sb.append(uri);
        Log.d("SettingsSliceProvider", sb.toString());
        this.mRegisteredUris.add(uri);
        SliceBroadcastRelay.registerReceiver(this.getContext(), uri, SliceRelayReceiver.class, intentFilter);
    }
}
