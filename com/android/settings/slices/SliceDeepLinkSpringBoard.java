package com.android.settings.slices;

import com.android.settings.location.LocationSliceBuilder;
import com.android.settings.bluetooth.BluetoothSliceBuilder;
import com.android.settings.notification.ZenModeSliceBuilder;
import android.content.Context;
import com.android.settings.wifi.WifiSliceBuilder;
import android.util.Log;
import android.os.Bundle;
import java.net.URISyntaxException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.app.Activity;

public class SliceDeepLinkSpringBoard extends Activity
{
    public static Intent parse(final Uri uri, final String package1) throws URISyntaxException {
        final Intent uri2 = Intent.parseUri(uri.getQueryParameter("intent"), 2);
        uri2.setComponent((ComponentName)null);
        if (uri2.getExtras() != null) {
            uri2.getExtras().clear();
        }
        uri2.setPackage(package1);
        return uri2;
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Uri data = this.getIntent().getData();
        if (data == null) {
            Log.e("DeeplinkSpringboard", "No data found");
            this.finish();
            return;
        }
        try {
            final Intent parse = parse(data, this.getPackageName());
            if ("com.android.settings.action.VIEW_SLICE".equals(parse.getAction())) {
                final Uri parse2 = Uri.parse(parse.getStringExtra("slice"));
                Intent intent;
                if (WifiSliceBuilder.WIFI_URI.equals((Object)parse2)) {
                    intent = WifiSliceBuilder.getIntent((Context)this);
                }
                else if (ZenModeSliceBuilder.ZEN_MODE_URI.equals((Object)parse2)) {
                    intent = ZenModeSliceBuilder.getIntent((Context)this);
                }
                else if (BluetoothSliceBuilder.BLUETOOTH_URI.equals((Object)parse2)) {
                    intent = BluetoothSliceBuilder.getIntent((Context)this);
                }
                else if (LocationSliceBuilder.LOCATION_URI.equals((Object)parse2)) {
                    intent = LocationSliceBuilder.getIntent((Context)this);
                }
                else {
                    intent = SliceBuilderUtils.getContentIntent((Context)this, new SlicesDatabaseAccessor((Context)this).getSliceDataFromUri(parse2));
                }
                this.startActivity(intent);
            }
            else {
                this.startActivity(parse);
            }
            this.finish();
        }
        catch (IllegalStateException ex) {
            Log.w("DeeplinkSpringboard", "Couldn't launch Slice intent", (Throwable)ex);
            this.startActivity(new Intent("android.settings.SETTINGS"));
            this.finish();
        }
        catch (URISyntaxException ex2) {
            Log.e("DeeplinkSpringboard", "Error decoding uri", (Throwable)ex2);
            this.finish();
        }
    }
}
