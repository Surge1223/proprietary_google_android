package com.android.settings.slices;

import android.content.BroadcastReceiver;
import com.android.settingslib.SliceBroadcastRelay;
import java.net.URISyntaxException;
import android.content.Intent;
import android.text.TextUtils;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import android.os.StrictMode$ThreadPolicy;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.wifi.calling.WifiCallingSliceHelper;
import android.os.StrictMode$ThreadPolicy$Builder;
import android.os.StrictMode;
import com.android.settingslib.utils.ThreadUtils;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.app.slice.SliceManager;
import android.util.Log;
import java.util.Collection;
import java.util.Collections;
import android.provider.Settings;
import com.android.settings.location.LocationSliceBuilder;
import com.android.settings.bluetooth.BluetoothSliceBuilder;
import com.android.settings.wifi.WifiSliceBuilder;
import java.util.Arrays;
import com.android.settings.notification.ZenModeSliceBuilder;
import androidx.slice.Slice;
import java.util.Iterator;
import android.net.Uri.Builder;
import java.util.ArrayList;
import java.util.List;
import android.util.ArraySet;
import java.util.Map;
import java.util.Set;
import android.util.KeyValueListParser;
import androidx.slice.SliceProvider;
import android.net.Uri;

public final class _$$Lambda$SettingsSliceProvider$3mq4GNawZ0Wc_zLrSLnj1f92or0 implements Runnable
{
    @Override
    public final void run() {
        this.f$0.loadSlice(this.f$1);
    }
}
