package com.android.settings.slices;

import com.android.settings.bluetooth.BluetoothSliceBuilder;
import com.android.settings.wifi.WifiSliceBuilder;
import com.android.settings.notification.ZenModeSliceBuilder;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri.Builder;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.core.TogglePreferenceController;
import android.util.Log;
import com.android.settings.core.SliderPreferenceController;
import android.text.TextUtils;
import com.android.settings.core.BasePreferenceController;
import android.content.Context;
import android.content.BroadcastReceiver;

public class SliceBroadcastReceiver extends BroadcastReceiver
{
    private static String TAG;
    
    static {
        SliceBroadcastReceiver.TAG = "SettSliceBroadcastRec";
    }
    
    private BasePreferenceController getPreferenceController(final Context context, final String s) {
        return SliceBuilderUtils.getPreferenceController(context, new SlicesDatabaseAccessor(context).getSliceDataFromKey(s));
    }
    
    private void handleSliderAction(final Context context, final String s, final int sliderPosition, final boolean b) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            throw new IllegalArgumentException("No key passed to Intent for slider controller. Use extra: com.android.settings.slice.extra.key");
        }
        if (sliderPosition == -1) {
            throw new IllegalArgumentException("Invalid position passed to Slider controller");
        }
        final BasePreferenceController preferenceController = this.getPreferenceController(context, s);
        if (!(preferenceController instanceof SliderPreferenceController)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Slider action passed for a non-slider key: ");
            sb.append(s);
            throw new IllegalArgumentException(sb.toString());
        }
        if (!preferenceController.isAvailable()) {
            final String tag = SliceBroadcastReceiver.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Can't update ");
            sb2.append(s);
            sb2.append(" since the setting is unavailable");
            Log.w(tag, sb2.toString());
            this.updateUri(context, s, b);
            return;
        }
        final SliderPreferenceController sliderPreferenceController = (SliderPreferenceController)preferenceController;
        final int maxSteps = sliderPreferenceController.getMaxSteps();
        if (sliderPosition >= 0 && sliderPosition <= maxSteps) {
            sliderPreferenceController.setSliderPosition(sliderPosition);
            this.logSliceValueChange(context, s, sliderPosition);
            this.updateUri(context, s, b);
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Invalid position passed to Slider controller. Expected between 0 and ");
        sb3.append(maxSteps);
        sb3.append(" but found ");
        sb3.append(sliderPosition);
        throw new IllegalArgumentException(sb3.toString());
    }
    
    private void handleToggleAction(final Context context, final String s, final boolean checked, final boolean b) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            throw new IllegalStateException("No key passed to Intent for toggle controller");
        }
        final BasePreferenceController preferenceController = this.getPreferenceController(context, s);
        if (!(preferenceController instanceof TogglePreferenceController)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Toggle action passed for a non-toggle key: ");
            sb.append(s);
            throw new IllegalStateException(sb.toString());
        }
        if (!preferenceController.isAvailable()) {
            final String tag = SliceBroadcastReceiver.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Can't update ");
            sb2.append(s);
            sb2.append(" since the setting is unavailable");
            Log.w(tag, sb2.toString());
            if (!preferenceController.hasAsyncUpdate()) {
                this.updateUri(context, s, b);
            }
            return;
        }
        ((TogglePreferenceController)preferenceController).setChecked(checked);
        this.logSliceValueChange(context, s, checked ? 1 : 0);
        if (!preferenceController.hasAsyncUpdate()) {
            this.updateUri(context, s, b);
        }
    }
    
    private void logSliceValueChange(final Context context, final String s, final int n) {
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 1372, Pair.create((Object)854, (Object)s), Pair.create((Object)1089, (Object)n));
    }
    
    private void updateUri(final Context context, final String s, final boolean b) {
        String s2;
        if (b) {
            s2 = "android.settings.slices";
        }
        else {
            s2 = "com.android.settings.slices";
        }
        context.getContentResolver().notifyChange(new Uri.Builder().scheme("content").authority(s2).appendPath("action").appendPath(s).build(), (ContentObserver)null);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final String action = intent.getAction();
        final String stringExtra = intent.getStringExtra("com.android.settings.slice.extra.key");
        final boolean booleanExtra = intent.getBooleanExtra("com.android.settings.slice.extra.platform", false);
        int n = 0;
        Label_0180: {
            switch (action.hashCode()) {
                case 1913359032: {
                    if (action.equals("com.android.settings.notification.ZEN_MODE_CHANGED")) {
                        n = 5;
                        break Label_0180;
                    }
                    break;
                }
                case 775016264: {
                    if (action.equals("com.android.settings.wifi.action.WIFI_CHANGED")) {
                        n = 3;
                        break Label_0180;
                    }
                    break;
                }
                case 17552563: {
                    if (action.equals("com.android.settings.slice.action.SLIDER_CHANGED")) {
                        n = 1;
                        break Label_0180;
                    }
                    break;
                }
                case -362341757: {
                    if (action.equals("com.android.settings.wifi.calling.action.WIFI_CALLING_CHANGED")) {
                        n = 4;
                        break Label_0180;
                    }
                    break;
                }
                case -932197342: {
                    if (action.equals("com.android.settings.bluetooth.action.BLUETOOTH_MODE_CHANGED")) {
                        n = 2;
                        break Label_0180;
                    }
                    break;
                }
                case -2075790298: {
                    if (action.equals("com.android.settings.slice.action.TOGGLE_CHANGED")) {
                        n = 0;
                        break Label_0180;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            case 5: {
                ZenModeSliceBuilder.handleUriChange(context, intent);
                break;
            }
            case 4: {
                FeatureFactory.getFactory(context).getSlicesFeatureProvider().getNewWifiCallingSliceHelper(context).handleWifiCallingChanged(intent);
                break;
            }
            case 3: {
                WifiSliceBuilder.handleUriChange(context, intent);
                break;
            }
            case 2: {
                BluetoothSliceBuilder.handleUriChange(context, intent);
                break;
            }
            case 1: {
                this.handleSliderAction(context, stringExtra, intent.getIntExtra("android.app.slice.extra.RANGE_VALUE", -1), booleanExtra);
                break;
            }
            case 0: {
                this.handleToggleAction(context, stringExtra, intent.getBooleanExtra("android.app.slice.extra.TOGGLE_STATE", false), booleanExtra);
                break;
            }
        }
    }
}
