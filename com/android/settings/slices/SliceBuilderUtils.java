package com.android.settings.slices;

import com.android.internal.annotations.VisibleForTesting;
import android.net.Uri;
import com.android.settings.SubSettings;
import com.android.settings.search.DatabaseIndexingUtils;
import android.net.Uri.Builder;
import android.content.Intent;
import android.app.PendingIntent;
import com.android.settings.core.TogglePreferenceController;
import com.android.settings.core.SliderPreferenceController;
import androidx.slice.builders.SliceAction;
import java.util.Collection;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.Arrays;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.util.Log;
import android.support.v4.util.Consumer;
import android.support.v4.graphics.drawable.IconCompat;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settingslib.Utils;
import androidx.slice.builders.ListBuilder;
import androidx.slice.Slice;
import com.android.settings.core.BasePreferenceController;
import android.content.Context;

public class SliceBuilderUtils
{
    private static Slice buildIntentSlice(final Context context, final SliceData sliceData, final BasePreferenceController basePreferenceController) {
        return new ListBuilder(context, sliceData.getUri(), -1L).setAccentColor(Utils.getColorAccent(context)).addRow(new _$$Lambda$SliceBuilderUtils$NVqOOBEdIdirrSxUZFgCZXRQ1vA(sliceData, getSubtitleText(context, basePreferenceController, sliceData), getContentPendingIntent(context, sliceData), IconCompat.createWithResource(context, sliceData.getIconResource()))).setKeywords(buildSliceKeywords(sliceData)).build();
    }
    
    public static Slice buildSlice(final Context context, final SliceData sliceData) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Creating slice for: ");
        sb.append(sliceData.getPreferenceController());
        Log.d("SliceBuilder", sb.toString());
        final BasePreferenceController preferenceController = getPreferenceController(context, sliceData);
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, 1371, Pair.create((Object)854, (Object)sliceData.getKey()));
        if (!preferenceController.isAvailable()) {
            return null;
        }
        if (preferenceController.getAvailabilityStatus() == 4) {
            return buildUnavailableSlice(context, sliceData);
        }
        switch (sliceData.getSliceType()) {
            default: {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Slice type passed was invalid: ");
                sb2.append(sliceData.getSliceType());
                throw new IllegalArgumentException(sb2.toString());
            }
            case 2: {
                return buildSliderSlice(context, sliceData, preferenceController);
            }
            case 1: {
                return buildToggleSlice(context, sliceData, preferenceController);
            }
            case 0: {
                return buildIntentSlice(context, sliceData, preferenceController);
            }
        }
    }
    
    private static List<String> buildSliceKeywords(final SliceData sliceData) {
        final ArrayList<String> list = new ArrayList<String>();
        list.add(sliceData.getTitle());
        if (!TextUtils.equals((CharSequence)sliceData.getTitle(), sliceData.getScreenTitle())) {
            list.add(sliceData.getScreenTitle().toString());
        }
        final String keywords = sliceData.getKeywords();
        if (keywords != null) {
            list.addAll((Collection<?>)Arrays.stream(keywords.split(",")).map((Function<? super String, ?>)_$$Lambda$SliceBuilderUtils$H4nQFDLpU9w8T_x_9Cq8nlH2grw.INSTANCE).collect((Collector<? super Object, ?, List<? extends String>>)Collectors.toList()));
        }
        return list;
    }
    
    private static Slice buildSliderSlice(final Context context, final SliceData sliceData, final BasePreferenceController basePreferenceController) {
        return new ListBuilder(context, sliceData.getUri(), -1L).setAccentColor(Utils.getColorAccent(context)).addInputRange(new _$$Lambda$SliceBuilderUtils$qRPBF1K1kbSIREThP22FAM_L1N0(sliceData, getSubtitleText(context, basePreferenceController, sliceData), new SliceAction(getContentPendingIntent(context, sliceData), IconCompat.createWithResource(context, sliceData.getIconResource()), sliceData.getTitle()), (SliderPreferenceController)basePreferenceController, getSliderAction(context, sliceData))).setKeywords(buildSliceKeywords(sliceData)).build();
    }
    
    private static Slice buildToggleSlice(final Context context, final SliceData sliceData, final BasePreferenceController basePreferenceController) {
        return new ListBuilder(context, sliceData.getUri(), -1L).setAccentColor(Utils.getColorAccent(context)).addRow(new _$$Lambda$SliceBuilderUtils$_H4Orhnw7bHLhjHmJgSvCr6cWP8(sliceData, getSubtitleText(context, basePreferenceController, sliceData), getContentPendingIntent(context, sliceData), IconCompat.createWithResource(context, sliceData.getIconResource()), getToggleAction(context, sliceData, ((TogglePreferenceController)basePreferenceController).isChecked()))).setKeywords(buildSliceKeywords(sliceData)).build();
    }
    
    private static Slice buildUnavailableSlice(final Context context, final SliceData sliceData) {
        final String title = sliceData.getTitle();
        final List<String> buildSliceKeywords = buildSliceKeywords(sliceData);
        final int colorAccent = Utils.getColorAccent(context);
        final CharSequence text = context.getText(2131887435);
        final IconCompat withResource = IconCompat.createWithResource(context, sliceData.getIconResource());
        return new ListBuilder(context, sliceData.getUri(), -1L).setAccentColor(colorAccent).addRow(new _$$Lambda$SliceBuilderUtils$JGXESizo03yh_FrnCdjYorH4I8Y(title, withResource, text, new SliceAction(getContentPendingIntent(context, sliceData), withResource, title))).setKeywords(buildSliceKeywords).build();
    }
    
    public static PendingIntent getActionIntent(final Context context, final String s, final SliceData sliceData) {
        final Intent intent = new Intent(s);
        intent.setClass(context, (Class)SliceBroadcastReceiver.class);
        intent.putExtra("com.android.settings.slice.extra.key", sliceData.getKey());
        intent.putExtra("com.android.settings.slice.extra.platform", sliceData.isPlatformDefined());
        return PendingIntent.getBroadcast(context, 0, intent, 268435456);
    }
    
    @VisibleForTesting
    static Intent getContentIntent(final Context context, final SliceData sliceData) {
        final Uri build = new Uri.Builder().appendPath(sliceData.getKey()).build();
        final Intent buildSearchResultPageIntent = DatabaseIndexingUtils.buildSearchResultPageIntent(context, sliceData.getFragmentClassName(), sliceData.getKey(), sliceData.getScreenTitle().toString(), 0);
        buildSearchResultPageIntent.setClassName(context.getPackageName(), SubSettings.class.getName());
        buildSearchResultPageIntent.setData(build);
        return buildSearchResultPageIntent;
    }
    
    public static PendingIntent getContentPendingIntent(final Context context, final SliceData sliceData) {
        return PendingIntent.getActivity(context, 0, getContentIntent(context, sliceData), 0);
    }
    
    public static Pair<Boolean, String> getPathData(final Uri uri) {
        final String[] split = uri.getPath().split("/", 3);
        if (split.length != 3) {
            return null;
        }
        return (Pair<Boolean, String>)new Pair((Object)TextUtils.equals((CharSequence)"intent", (CharSequence)split[1]), (Object)split[2]);
    }
    
    public static BasePreferenceController getPreferenceController(final Context context, final SliceData sliceData) {
        return getPreferenceController(context, sliceData.getPreferenceController(), sliceData.getKey());
    }
    
    private static BasePreferenceController getPreferenceController(final Context context, final String s, final String s2) {
        try {
            return BasePreferenceController.createInstance(context, s);
        }
        catch (IllegalStateException ex) {
            return BasePreferenceController.createInstance(context, s, s2);
        }
    }
    
    public static int getSliceType(final Context context, final String s, final String s2) {
        return getPreferenceController(context, s, s2).getSliceType();
    }
    
    private static PendingIntent getSliderAction(final Context context, final SliceData sliceData) {
        return getActionIntent(context, "com.android.settings.slice.action.SLIDER_CHANGED", sliceData);
    }
    
    public static CharSequence getSubtitleText(final Context context, final AbstractPreferenceController abstractPreferenceController, final SliceData sliceData) {
        final CharSequence screenTitle = sliceData.getScreenTitle();
        if (isValidSummary(context, screenTitle) && !TextUtils.equals(screenTitle, (CharSequence)sliceData.getTitle())) {
            return screenTitle;
        }
        if (abstractPreferenceController != null) {
            final CharSequence summary = abstractPreferenceController.getSummary();
            if (isValidSummary(context, summary)) {
                return summary;
            }
        }
        final String summary2 = sliceData.getSummary();
        if (isValidSummary(context, summary2)) {
            return summary2;
        }
        return "";
    }
    
    private static SliceAction getToggleAction(final Context context, final SliceData sliceData, final boolean b) {
        return new SliceAction(getActionIntent(context, "com.android.settings.slice.action.TOGGLE_CHANGED", sliceData), null, b);
    }
    
    public static Uri getUri(final String s, final boolean b) {
        String s2;
        if (b) {
            s2 = "android.settings.slices";
        }
        else {
            s2 = "com.android.settings.slices";
        }
        return new Uri.Builder().scheme("content").authority(s2).appendPath(s).build();
    }
    
    private static boolean isValidSummary(final Context context, final CharSequence charSequence) {
        final boolean b = false;
        if (charSequence != null && !TextUtils.isEmpty((CharSequence)charSequence.toString().trim())) {
            final CharSequence text = context.getText(2131889405);
            final CharSequence text2 = context.getText(2131889408);
            boolean b2 = b;
            if (!TextUtils.equals(charSequence, text)) {
                b2 = b;
                if (!TextUtils.equals(charSequence, text2)) {
                    b2 = true;
                }
            }
            return b2;
        }
        return false;
    }
}
