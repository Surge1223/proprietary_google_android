package com.android.settings.slices;

import com.android.settings.wifi.calling.WifiCallingSliceHelper;
import android.content.Context;

public interface SlicesFeatureProvider
{
    WifiCallingSliceHelper getNewWifiCallingSliceHelper(final Context p0);
    
    SliceDataConverter getSliceDataConverter(final Context p0);
    
    void indexSliceData(final Context p0);
    
    void indexSliceDataAsync(final Context p0);
}
