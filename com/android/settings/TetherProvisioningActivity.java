package com.android.settings;

import android.os.UserHandle;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.os.ResultReceiver;
import android.app.Activity;

public class TetherProvisioningActivity extends Activity
{
    private static final boolean DEBUG;
    private ResultReceiver mResultReceiver;
    
    static {
        DEBUG = Log.isLoggable("TetherProvisioningAct", 3);
    }
    
    public void onActivityResult(int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 0) {
            if (TetherProvisioningActivity.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Got result from app: ");
                sb.append(n2);
                Log.d("TetherProvisioningAct", sb.toString());
            }
            if (n2 == -1) {
                n = 0;
            }
            else {
                n = 11;
            }
            this.mResultReceiver.send(n, (Bundle)null);
            this.finish();
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mResultReceiver = (ResultReceiver)this.getIntent().getParcelableExtra("extraProvisionCallback");
        final int intExtra = this.getIntent().getIntExtra("extraAddTetherType", -1);
        final String[] stringArray = this.getResources().getStringArray(17236021);
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.setClassName(stringArray[0], stringArray[1]);
        intent.putExtra("TETHER_TYPE", intExtra);
        if (TetherProvisioningActivity.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Starting provisioning app: ");
            sb.append(stringArray[0]);
            sb.append(".");
            sb.append(stringArray[1]);
            Log.d("TetherProvisioningAct", sb.toString());
        }
        if (this.getPackageManager().queryIntentActivities(intent, 65536).isEmpty()) {
            Log.e("TetherProvisioningAct", "Provisioning app is configured, but not available.");
            this.mResultReceiver.send(11, (Bundle)null);
            this.finish();
            return;
        }
        this.startActivityForResultAsUser(intent, 0, UserHandle.CURRENT);
    }
}
