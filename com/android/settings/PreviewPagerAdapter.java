package com.android.settings;

import android.animation.Animator;
import android.view.ViewGroup;
import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.view.View;
import android.view.ViewStub$OnInflateListener;
import android.view.ViewStub;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.content.res.Configuration;
import android.content.Context;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.view.animation.Interpolator;
import android.support.v4.view.PagerAdapter;

public class PreviewPagerAdapter extends PagerAdapter
{
    private static final Interpolator FADE_IN_INTERPOLATOR;
    private static final Interpolator FADE_OUT_INTERPOLATOR;
    private int mAnimationCounter;
    private Runnable mAnimationEndAction;
    private boolean mIsLayoutRtl;
    private FrameLayout[] mPreviewFrames;
    private boolean[][] mViewStubInflated;
    
    static {
        FADE_IN_INTERPOLATOR = (Interpolator)new DecelerateInterpolator();
        FADE_OUT_INTERPOLATOR = (Interpolator)new AccelerateInterpolator();
    }
    
    public PreviewPagerAdapter(final Context context, final boolean mIsLayoutRtl, final int[] array, final Configuration[] array2) {
        this.mIsLayoutRtl = mIsLayoutRtl;
        this.mPreviewFrames = new FrameLayout[array.length];
        this.mViewStubInflated = new boolean[array.length][array2.length];
        for (int i = 0; i < array.length; ++i) {
            int n;
            if (this.mIsLayoutRtl) {
                n = array.length - 1 - i;
            }
            else {
                n = i;
            }
            (this.mPreviewFrames[n] = new FrameLayout(context)).setLayoutParams((ViewGroup.LayoutParams)new LinearLayout$LayoutParams(-1, -1));
            for (int j = 0; j < array2.length; ++j) {
                final Context configurationContext = context.createConfigurationContext(array2[j]);
                configurationContext.getTheme().setTo(context.getTheme());
                LayoutInflater.from(configurationContext);
                final ViewStub viewStub = new ViewStub(configurationContext);
                viewStub.setLayoutResource(array[i]);
                viewStub.setOnInflateListener((ViewStub$OnInflateListener)new ViewStub$OnInflateListener() {
                    public void onInflate(final ViewStub viewStub, final View view) {
                        view.setVisibility(viewStub.getVisibility());
                        PreviewPagerAdapter.this.mViewStubInflated[i][j] = true;
                    }
                });
                this.mPreviewFrames[n].addView((View)viewStub);
            }
        }
    }
    
    private void runAnimationEndAction() {
        if (this.mAnimationEndAction != null && !this.isAnimating()) {
            this.mAnimationEndAction.run();
            this.mAnimationEndAction = null;
        }
    }
    
    private void setVisibility(final View view, final int visibility, final boolean b) {
        float alpha;
        if (visibility == 0) {
            alpha = 1.0f;
        }
        else {
            alpha = 0.0f;
        }
        if (!b) {
            view.setAlpha(alpha);
            view.setVisibility(visibility);
        }
        else {
            if (visibility == 0) {
                final Interpolator fade_IN_INTERPOLATOR = PreviewPagerAdapter.FADE_IN_INTERPOLATOR;
            }
            else {
                final Interpolator fade_OUT_INTERPOLATOR = PreviewPagerAdapter.FADE_OUT_INTERPOLATOR;
            }
            if (visibility == 0) {
                view.animate().alpha(alpha).setInterpolator((TimeInterpolator)PreviewPagerAdapter.FADE_IN_INTERPOLATOR).setDuration(400L).setListener((Animator.AnimatorListener)new PreviewFrameAnimatorListener()).withStartAction((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(visibility);
                    }
                });
            }
            else {
                view.animate().alpha(alpha).setInterpolator((TimeInterpolator)PreviewPagerAdapter.FADE_OUT_INTERPOLATOR).setDuration(400L).setListener((Animator.AnimatorListener)new PreviewFrameAnimatorListener()).withEndAction((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(visibility);
                    }
                });
            }
        }
    }
    
    @Override
    public void destroyItem(final ViewGroup viewGroup, final int n, final Object o) {
        viewGroup.removeView((View)o);
    }
    
    @Override
    public int getCount() {
        return this.mPreviewFrames.length;
    }
    
    @Override
    public Object instantiateItem(final ViewGroup viewGroup, final int n) {
        viewGroup.addView((View)this.mPreviewFrames[n]);
        return this.mPreviewFrames[n];
    }
    
    boolean isAnimating() {
        return this.mAnimationCounter > 0;
    }
    
    @Override
    public boolean isViewFromObject(final View view, final Object o) {
        return view == o;
    }
    
    void setAnimationEndAction(final Runnable mAnimationEndAction) {
        this.mAnimationEndAction = mAnimationEndAction;
    }
    
    void setPreviewLayer(final int n, final int n2, final int n3, final boolean b) {
        for (final FrameLayout frameLayout : this.mPreviewFrames) {
            if (n2 >= 0) {
                final View child = frameLayout.getChildAt(n2);
                if (this.mViewStubInflated[n3][n2]) {
                    if (frameLayout == this.mPreviewFrames[n3]) {
                        this.setVisibility(child, 4, b);
                    }
                    else {
                        this.setVisibility(child, 4, false);
                    }
                }
            }
            final View child2 = frameLayout.getChildAt(n);
            if (frameLayout == this.mPreviewFrames[n3]) {
                View inflate = child2;
                if (!this.mViewStubInflated[n3][n]) {
                    inflate = ((ViewStub)child2).inflate();
                    inflate.setAlpha(0.0f);
                }
                this.setVisibility(inflate, 0, b);
            }
            else {
                this.setVisibility(child2, 0, false);
            }
        }
    }
    
    private class PreviewFrameAnimatorListener implements Animator.AnimatorListener
    {
        public void onAnimationCancel(final Animator animator) {
        }
        
        public void onAnimationEnd(final Animator animator) {
            PreviewPagerAdapter.this.mAnimationCounter--;
            PreviewPagerAdapter.this.runAnimationEndAction();
        }
        
        public void onAnimationRepeat(final Animator animator) {
        }
        
        public void onAnimationStart(final Animator animator) {
            PreviewPagerAdapter.this.mAnimationCounter++;
        }
    }
}
