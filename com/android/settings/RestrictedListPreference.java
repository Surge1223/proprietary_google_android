package com.android.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v14.preference.ListPreferenceDialogFragment;
import android.widget.ImageView;
import android.widget.CheckedTextView;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.android.settingslib.RestrictedLockUtils;
import android.app.KeyguardManager;
import android.os.UserManager;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.widget.ListAdapter;
import java.util.Iterator;
import android.support.v7.preference.Preference;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import java.util.List;
import com.android.settingslib.RestrictedPreferenceHelper;

public class RestrictedListPreference extends CustomListPreference
{
    private final RestrictedPreferenceHelper mHelper;
    private int mProfileUserId;
    private boolean mRequiresActiveUnlockedProfile;
    private final List<RestrictedItem> mRestrictedItems;
    
    public RestrictedListPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRestrictedItems = new ArrayList<RestrictedItem>();
        this.mRequiresActiveUnlockedProfile = false;
        this.setWidgetLayoutResource(R.layout.restricted_icon);
        this.mHelper = new RestrictedPreferenceHelper(context, this, set);
    }
    
    public RestrictedListPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mRestrictedItems = new ArrayList<RestrictedItem>();
        this.mRequiresActiveUnlockedProfile = false;
        this.mHelper = new RestrictedPreferenceHelper(context, this, set);
    }
    
    private RestrictedItem getRestrictedItemForEntryValue(final CharSequence charSequence) {
        if (charSequence == null) {
            return null;
        }
        for (final RestrictedItem restrictedItem : this.mRestrictedItems) {
            if (charSequence.equals(restrictedItem.entryValue)) {
                return restrictedItem;
            }
        }
        return null;
    }
    
    public void addRestrictedItem(final RestrictedItem restrictedItem) {
        this.mRestrictedItems.add(restrictedItem);
    }
    
    public void clearRestrictedItems() {
        this.mRestrictedItems.clear();
    }
    
    protected ListAdapter createListAdapter() {
        return (ListAdapter)new RestrictedArrayAdapter(this.getContext(), this.getEntries(), this.getSelectedValuePos());
    }
    
    public int getSelectedValuePos() {
        final String value = this.getValue();
        int indexOfValue;
        if (value == null) {
            indexOfValue = -1;
        }
        else {
            indexOfValue = this.findIndexOfValue(value);
        }
        return indexOfValue;
    }
    
    public boolean isDisabledByAdmin() {
        return this.mHelper.isDisabledByAdmin();
    }
    
    public boolean isRestrictedForEntry(final CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        final Iterator<RestrictedItem> iterator = this.mRestrictedItems.iterator();
        while (iterator.hasNext()) {
            if (charSequence.equals(iterator.next().entry)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.mHelper.onBindViewHolder(preferenceViewHolder);
        final View viewById = preferenceViewHolder.findViewById(R.id.restricted_icon);
        if (viewById != null) {
            int visibility;
            if (this.isDisabledByAdmin()) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
        }
    }
    
    @Override
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
        alertDialog$Builder.setAdapter(this.createListAdapter(), dialogInterface$OnClickListener);
    }
    
    @Override
    public void performClick() {
        if (this.mRequiresActiveUnlockedProfile) {
            if (Utils.startQuietModeDialogIfNecessary(this.getContext(), UserManager.get(this.getContext()), this.mProfileUserId)) {
                return;
            }
            final KeyguardManager keyguardManager = (KeyguardManager)this.getContext().getSystemService("keyguard");
            if (keyguardManager.isDeviceLocked(this.mProfileUserId)) {
                this.getContext().startActivity(keyguardManager.createConfirmDeviceCredentialIntent((CharSequence)null, (CharSequence)null, this.mProfileUserId));
                return;
            }
        }
        if (!this.mHelper.performClick()) {
            super.performClick();
        }
    }
    
    public void setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin disabledByAdmin) {
        if (this.mHelper.setDisabledByAdmin(disabledByAdmin)) {
            this.notifyChanged();
        }
    }
    
    @Override
    public void setEnabled(final boolean enabled) {
        if (enabled && this.isDisabledByAdmin()) {
            this.mHelper.setDisabledByAdmin(null);
            return;
        }
        super.setEnabled(enabled);
    }
    
    public void setProfileUserId(final int mProfileUserId) {
        this.mProfileUserId = mProfileUserId;
    }
    
    public void setRequiresActiveUnlockedProfile(final boolean mRequiresActiveUnlockedProfile) {
        this.mRequiresActiveUnlockedProfile = mRequiresActiveUnlockedProfile;
    }
    
    public class RestrictedArrayAdapter extends ArrayAdapter<CharSequence>
    {
        private final int mSelectedIndex;
        
        public RestrictedArrayAdapter(final Context context, final CharSequence[] array, final int mSelectedIndex) {
            super(context, 2131558712, 2131362726, (Object[])array);
            this.mSelectedIndex = mSelectedIndex;
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, View view, final ViewGroup viewGroup) {
            view = super.getView(n, view, viewGroup);
            final CharSequence charSequence = (CharSequence)this.getItem(n);
            final CheckedTextView checkedTextView = (CheckedTextView)view.findViewById(2131362726);
            final ImageView imageView = (ImageView)view.findViewById(2131362526);
            final boolean restrictedForEntry = RestrictedListPreference.this.isRestrictedForEntry(charSequence);
            boolean checked = false;
            if (restrictedForEntry) {
                checkedTextView.setEnabled(false);
                checkedTextView.setChecked(false);
                imageView.setVisibility(0);
            }
            else {
                if (this.mSelectedIndex != -1) {
                    if (n == this.mSelectedIndex) {
                        checked = true;
                    }
                    checkedTextView.setChecked(checked);
                }
                if (!checkedTextView.isEnabled()) {
                    checkedTextView.setEnabled(true);
                }
                imageView.setVisibility(8);
            }
            return view;
        }
        
        public boolean hasStableIds() {
            return true;
        }
    }
    
    public static class RestrictedItem
    {
        public final RestrictedLockUtils.EnforcedAdmin enforcedAdmin;
        public final CharSequence entry;
        public final CharSequence entryValue;
        
        public RestrictedItem(final CharSequence entry, final CharSequence entryValue, final RestrictedLockUtils.EnforcedAdmin enforcedAdmin) {
            this.entry = entry;
            this.entryValue = entryValue;
            this.enforcedAdmin = enforcedAdmin;
        }
    }
    
    public static class RestrictedListPreferenceDialogFragment extends CustomListPreferenceDialogFragment
    {
        private int mLastCheckedPosition;
        
        public RestrictedListPreferenceDialogFragment() {
            this.mLastCheckedPosition = -1;
        }
        
        private RestrictedListPreference getCustomizablePreference() {
            return (RestrictedListPreference)this.getPreference();
        }
        
        private int getLastCheckedPosition() {
            if (this.mLastCheckedPosition == -1) {
                this.mLastCheckedPosition = this.getCustomizablePreference().getSelectedValuePos();
            }
            return this.mLastCheckedPosition;
        }
        
        public static ListPreferenceDialogFragment newInstance(final String s) {
            final RestrictedListPreferenceDialogFragment restrictedListPreferenceDialogFragment = new RestrictedListPreferenceDialogFragment();
            final Bundle arguments = new Bundle(1);
            arguments.putString("key", s);
            restrictedListPreferenceDialogFragment.setArguments(arguments);
            return restrictedListPreferenceDialogFragment;
        }
        
        @Override
        protected DialogInterface$OnClickListener getOnItemClickListener() {
            return (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int clickedDialogEntryIndex) {
                    final RestrictedListPreference access$000 = RestrictedListPreferenceDialogFragment.this.getCustomizablePreference();
                    if (clickedDialogEntryIndex >= 0 && clickedDialogEntryIndex < access$000.getEntryValues().length) {
                        final RestrictedItem access$2 = access$000.getRestrictedItemForEntryValue(access$000.getEntryValues()[clickedDialogEntryIndex].toString());
                        if (access$2 != null) {
                            ((AlertDialog)dialogInterface).getListView().setItemChecked(RestrictedListPreferenceDialogFragment.this.getLastCheckedPosition(), true);
                            RestrictedLockUtils.sendShowAdminSupportDetailsIntent(RestrictedListPreferenceDialogFragment.this.getContext(), access$2.enforcedAdmin);
                        }
                        else {
                            RestrictedListPreferenceDialogFragment.this.setClickedDialogEntryIndex(clickedDialogEntryIndex);
                        }
                        if (RestrictedListPreferenceDialogFragment.this.getCustomizablePreference().isAutoClosePreference()) {
                            RestrictedListPreferenceDialogFragment.this.onClick(dialogInterface, -1);
                            dialogInterface.dismiss();
                        }
                    }
                }
            };
        }
        
        @Override
        protected void setClickedDialogEntryIndex(final int n) {
            super.setClickedDialogEntryIndex(n);
            this.mLastCheckedPosition = n;
        }
    }
}
