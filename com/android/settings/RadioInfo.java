package com.android.settings;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.app.QueuedWork;
import android.view.Menu;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.graphics.Typeface;
import com.android.internal.telephony.PhoneFactory;
import android.os.Process;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.net.TrafficStats;
import android.content.res.Resources;
import android.os.Bundle;
import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import android.util.Log;
import android.telephony.CarrierConfigManager;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import android.text.TextUtils;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityGsm;
import java.util.Iterator;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoWcdma;
import android.telephony.CellInfoLte;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellIdentityCdma;
import android.telephony.CellInfoCdma;
import android.content.ContentResolver;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.widget.AdapterView;
import android.content.ComponentName;
import android.content.ActivityNotFoundException;
import android.view.View;
import android.widget.CompoundButton;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.net.Uri;
import android.content.Intent;
import android.view.MenuItem;
import android.os.AsyncResult;
import android.os.Message;
import android.telephony.SignalStrength;
import android.telephony.ServiceState;
import android.telephony.PreciseCallState;
import android.telephony.PhysicalChannelConfig;
import android.telephony.DataConnectionRealTimeInfo;
import android.net.NetworkCapabilities;
import android.net.Network;
import android.net.NetworkRequest$Builder;
import android.widget.EditText;
import com.android.internal.telephony.Phone;
import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;
import android.net.ConnectivityManager$NetworkCallback;
import android.telephony.NeighboringCellInfo;
import com.android.ims.ImsManager;
import android.os.Handler;
import android.view.MenuItem$OnMenuItemClickListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.net.NetworkRequest;
import android.net.ConnectivityManager;
import android.telephony.CellLocation;
import android.telephony.CellInfo;
import java.util.List;
import android.widget.AdapterView$OnItemSelectedListener;
import android.view.View.OnClickListener;
import android.widget.Switch;
import android.widget.Spinner;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;

public class RadioInfo extends Activity
{
    private static final String[] mCellInfoRefreshRateLabels;
    private static final int[] mCellInfoRefreshRates;
    private static final String[] mPreferredNetworkLabels;
    private TextView callState;
    private Button carrierProvisioningButton;
    private Spinner cellInfoRefreshRateSpinner;
    private TextView dBm;
    private TextView dataNetwork;
    private TextView dnsCheckState;
    private Button dnsCheckToggleButton;
    private Switch eabProvisionedSwitch;
    private TextView gprsState;
    private TextView gsmState;
    private Switch imsVolteProvisionedSwitch;
    private Switch imsVtProvisionedSwitch;
    private Switch imsWfcProvisionedSwitch;
    View.OnClickListener mCarrierProvisioningButtonHandler;
    private TextView mCellInfo;
    AdapterView$OnItemSelectedListener mCellInfoRefreshRateHandler;
    private int mCellInfoRefreshRateIndex;
    private List<CellInfo> mCellInfoResult;
    private CellLocation mCellLocationResult;
    private TextView mCfi;
    private boolean mCfiValue;
    private ConnectivityManager mConnectivityManager;
    private TextView mDcRtInfoTv;
    private final NetworkRequest mDefaultNetworkRequest;
    private TextView mDeviceId;
    View.OnClickListener mDnsCheckButtonHandler;
    private TextView mDownlinkKbps;
    CompoundButton$OnCheckedChangeListener mEabCheckedChangeListener;
    private MenuItem$OnMenuItemClickListener mGetImsStatus;
    private Handler mHandler;
    private TextView mHttpClientTest;
    private String mHttpClientTestResult;
    private ImsManager mImsManager;
    CompoundButton$OnCheckedChangeListener mImsVolteCheckedChangeListener;
    CompoundButton$OnCheckedChangeListener mImsVtCheckedChangeListener;
    CompoundButton$OnCheckedChangeListener mImsWfcCheckedChangeListener;
    private TextView mLocation;
    private TextView mMwi;
    private boolean mMwiValue;
    private List<NeighboringCellInfo> mNeighboringCellResult;
    private TextView mNeighboringCids;
    private final ConnectivityManager$NetworkCallback mNetworkCallback;
    View.OnClickListener mOemInfoButtonHandler;
    private final PhoneStateListener mPhoneStateListener;
    private TextView mPhyChanConfig;
    View.OnClickListener mPingButtonHandler;
    private String mPingHostnameResultV4;
    private String mPingHostnameResultV6;
    private TextView mPingHostnameV4;
    private TextView mPingHostnameV6;
    AdapterView$OnItemSelectedListener mPreferredNetworkHandler;
    private int mPreferredNetworkTypeResult;
    CompoundButton$OnCheckedChangeListener mRadioPowerOnChangeListener;
    View.OnClickListener mRefreshSmscButtonHandler;
    private MenuItem$OnMenuItemClickListener mSelectBandCallback;
    private TextView mSubscriberId;
    private TelephonyManager mTelephonyManager;
    private MenuItem$OnMenuItemClickListener mToggleData;
    View.OnClickListener mTriggerCarrierProvisioningButtonHandler;
    View.OnClickListener mUpdateSmscButtonHandler;
    private TextView mUplinkKbps;
    private MenuItem$OnMenuItemClickListener mViewADNCallback;
    private MenuItem$OnMenuItemClickListener mViewFDNCallback;
    private MenuItem$OnMenuItemClickListener mViewSDNCallback;
    private TextView number;
    private Button oemInfoButton;
    private TextView operatorName;
    private Phone phone;
    private Button pingTestButton;
    private Spinner preferredNetworkType;
    private Switch radioPowerOnSwitch;
    private TextView received;
    private Button refreshSmscButton;
    private TextView roamingState;
    private TextView sent;
    private EditText smsc;
    private Button triggercarrierProvisioningButton;
    private Button updateSmscButton;
    private TextView voiceNetwork;
    
    static {
        mPreferredNetworkLabels = new String[] { "WCDMA preferred", "GSM only", "WCDMA only", "GSM auto (PRL)", "CDMA auto (PRL)", "CDMA only", "EvDo only", "Global auto (PRL)", "LTE/CDMA auto (PRL)", "LTE/UMTS auto (PRL)", "LTE/CDMA/UMTS auto (PRL)", "LTE only", "LTE/WCDMA", "TD-SCDMA only", "TD-SCDMA/WCDMA", "LTE/TD-SCDMA", "TD-SCDMA/GSM", "TD-SCDMA/UMTS", "LTE/TD-SCDMA/WCDMA", "LTE/TD-SCDMA/UMTS", "TD-SCDMA/CDMA/UMTS", "Global/TD-SCDMA", "Unknown" };
        mCellInfoRefreshRateLabels = new String[] { "Disabled", "Immediate", "Min 5s", "Min 10s", "Min 60s" };
        mCellInfoRefreshRates = new int[] { Integer.MAX_VALUE, 0, 5000, 10000, 60000 };
    }
    
    public RadioInfo() {
        this.mImsManager = null;
        this.phone = null;
        this.mMwiValue = false;
        this.mCfiValue = false;
        this.mCellInfoResult = null;
        this.mCellLocationResult = null;
        this.mNeighboringCellResult = null;
        this.mDefaultNetworkRequest = new NetworkRequest$Builder().addTransportType(0).addCapability(12).build();
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            public void onCapabilitiesChanged(final Network network, final NetworkCapabilities networkCapabilities) {
                RadioInfo.this.updateBandwidths(networkCapabilities.getLinkDownstreamBandwidthKbps(), networkCapabilities.getLinkUpstreamBandwidthKbps());
            }
        };
        this.mPhoneStateListener = new PhoneStateListener() {
            public void onCallForwardingIndicatorChanged(final boolean b) {
                RadioInfo.this.mCfiValue = b;
                RadioInfo.this.updateCallRedirect();
            }
            
            public void onCallStateChanged(final int n, final String s) {
                RadioInfo.this.updateNetworkType();
                RadioInfo.this.updatePhoneState(n);
            }
            
            public void onCellInfoChanged(final List<CellInfo> list) {
                final RadioInfo this$0 = RadioInfo.this;
                final StringBuilder sb = new StringBuilder();
                sb.append("onCellInfoChanged: arrayCi=");
                sb.append(list);
                this$0.log(sb.toString());
                RadioInfo.this.mCellInfoResult = list;
                RadioInfo.this.updateCellInfo(RadioInfo.this.mCellInfoResult);
            }
            
            public void onCellLocationChanged(final CellLocation cellLocation) {
                RadioInfo.this.updateLocation(cellLocation);
            }
            
            public void onDataActivity(final int n) {
                RadioInfo.this.updateDataStats2();
            }
            
            public void onDataConnectionRealTimeInfoChanged(final DataConnectionRealTimeInfo dataConnectionRealTimeInfo) {
                final RadioInfo this$0 = RadioInfo.this;
                final StringBuilder sb = new StringBuilder();
                sb.append("onDataConnectionRealTimeInfoChanged: dcRtInfo=");
                sb.append(dataConnectionRealTimeInfo);
                this$0.log(sb.toString());
                RadioInfo.this.updateDcRtInfoTv(dataConnectionRealTimeInfo);
            }
            
            public void onDataConnectionStateChanged(final int n) {
                RadioInfo.this.updateDataState();
                RadioInfo.this.updateNetworkType();
            }
            
            public void onMessageWaitingIndicatorChanged(final boolean b) {
                RadioInfo.this.mMwiValue = b;
                RadioInfo.this.updateMessageWaiting();
            }
            
            public void onPhysicalChannelConfigurationChanged(final List<PhysicalChannelConfig> list) {
                RadioInfo.this.updatePhysicalChannelConfiguration(list);
            }
            
            public void onPreciseCallStateChanged(final PreciseCallState preciseCallState) {
                RadioInfo.this.updateNetworkType();
            }
            
            public void onServiceStateChanged(final ServiceState serviceState) {
                final RadioInfo this$0 = RadioInfo.this;
                final StringBuilder sb = new StringBuilder();
                sb.append("onServiceStateChanged: ServiceState=");
                sb.append(serviceState);
                this$0.log(sb.toString());
                RadioInfo.this.updateServiceState(serviceState);
                RadioInfo.this.updateRadioPowerState();
                RadioInfo.this.updateNetworkType();
                RadioInfo.this.updateImsProvisionedState();
            }
            
            public void onSignalStrengthsChanged(final SignalStrength signalStrength) {
                final RadioInfo this$0 = RadioInfo.this;
                final StringBuilder sb = new StringBuilder();
                sb.append("onSignalStrengthChanged: SignalStrength=");
                sb.append(signalStrength);
                this$0.log(sb.toString());
                RadioInfo.this.updateSignalStrength(signalStrength);
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                switch (message.what) {
                    default: {
                        super.handleMessage(message);
                        break;
                    }
                    case 1006: {
                        RadioInfo.this.updateSmscButton.setEnabled(true);
                        if (((AsyncResult)message.obj).exception != null) {
                            RadioInfo.this.smsc.setText((CharSequence)"update error");
                            break;
                        }
                        break;
                    }
                    case 1005: {
                        final AsyncResult asyncResult = (AsyncResult)message.obj;
                        if (asyncResult.exception != null) {
                            RadioInfo.this.smsc.setText((CharSequence)"refresh error");
                            break;
                        }
                        RadioInfo.this.smsc.setText((CharSequence)asyncResult.result);
                        break;
                    }
                    case 1001: {
                        if (((AsyncResult)message.obj).exception != null) {
                            RadioInfo.this.log("Set preferred network type failed.");
                            break;
                        }
                        break;
                    }
                    case 1000: {
                        final AsyncResult asyncResult2 = (AsyncResult)message.obj;
                        if (asyncResult2.exception == null && asyncResult2.result != null) {
                            RadioInfo.this.updatePreferredNetworkType(((int[])asyncResult2.result)[0]);
                            break;
                        }
                        RadioInfo.this.updatePreferredNetworkType(RadioInfo.mPreferredNetworkLabels.length - 1);
                        break;
                    }
                }
            }
        };
        this.mViewADNCallback = (MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
            public boolean onMenuItemClick(final MenuItem menuItem) {
                final Intent intent = new Intent("android.intent.action.VIEW");
                intent.setClassName("com.android.phone", "com.android.phone.SimContacts");
                RadioInfo.this.startActivity(intent);
                return true;
            }
        };
        this.mViewFDNCallback = (MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
            public boolean onMenuItemClick(final MenuItem menuItem) {
                final Intent intent = new Intent("android.intent.action.VIEW");
                intent.setClassName("com.android.phone", "com.android.phone.settings.fdn.FdnList");
                RadioInfo.this.startActivity(intent);
                return true;
            }
        };
        this.mViewSDNCallback = (MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
            public boolean onMenuItemClick(final MenuItem menuItem) {
                final Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("content://icc/sdn"));
                intent.setClassName("com.android.phone", "com.android.phone.ADNList");
                RadioInfo.this.startActivity(intent);
                return true;
            }
        };
        this.mGetImsStatus = (MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
            public boolean onMenuItemClick(final MenuItem menuItem) {
                final boolean imsRegistered = RadioInfo.this.phone.isImsRegistered();
                final boolean volteEnabled = RadioInfo.this.phone.isVolteEnabled();
                final boolean wifiCallingEnabled = RadioInfo.this.phone.isWifiCallingEnabled();
                final boolean videoEnabled = RadioInfo.this.phone.isVideoEnabled();
                final boolean utEnabled = RadioInfo.this.phone.isUtEnabled();
                String s;
                if (imsRegistered) {
                    s = RadioInfo.this.getString(2131888768);
                }
                else {
                    s = RadioInfo.this.getString(2131888767);
                }
                final String string = RadioInfo.this.getString(2131888764);
                String string2 = RadioInfo.this.getString(2131888765);
                final RadioInfo this$0 = RadioInfo.this;
                String s2;
                if (volteEnabled) {
                    s2 = string;
                }
                else {
                    s2 = string2;
                }
                String s3;
                if (wifiCallingEnabled) {
                    s3 = string;
                }
                else {
                    s3 = string2;
                }
                String s4;
                if (videoEnabled) {
                    s4 = string;
                }
                else {
                    s4 = string2;
                }
                if (utEnabled) {
                    string2 = string;
                }
                new AlertDialog$Builder((Context)RadioInfo.this).setMessage((CharSequence)this$0.getString(2131888766, new Object[] { s, s2, s3, s4, string2 })).setTitle((CharSequence)RadioInfo.this.getString(2131888769)).create().show();
                return true;
            }
        };
        this.mSelectBandCallback = (MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
            public boolean onMenuItemClick(final MenuItem menuItem) {
                final Intent intent = new Intent();
                intent.setClass((Context)RadioInfo.this, (Class)BandMode.class);
                RadioInfo.this.startActivity(intent);
                return true;
            }
        };
        this.mToggleData = (MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
            public boolean onMenuItemClick(final MenuItem menuItem) {
                final int dataState = RadioInfo.this.mTelephonyManager.getDataState();
                if (dataState != 0) {
                    if (dataState == 2) {
                        RadioInfo.this.phone.setUserDataEnabled(false);
                    }
                }
                else {
                    RadioInfo.this.phone.setUserDataEnabled(true);
                }
                return true;
            }
        };
        this.mRadioPowerOnChangeListener = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean radioPower) {
                final RadioInfo this$0 = RadioInfo.this;
                final StringBuilder sb = new StringBuilder();
                sb.append("toggle radio power: currently ");
                String s;
                if (RadioInfo.this.isRadioOn()) {
                    s = "on";
                }
                else {
                    s = "off";
                }
                sb.append(s);
                this$0.log(sb.toString());
                RadioInfo.this.phone.setRadioPower(radioPower);
            }
        };
        this.mImsVolteCheckedChangeListener = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean imsVolteProvisionedState) {
                RadioInfo.this.setImsVolteProvisionedState(imsVolteProvisionedState);
            }
        };
        this.mImsVtCheckedChangeListener = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean imsVtProvisionedState) {
                RadioInfo.this.setImsVtProvisionedState(imsVtProvisionedState);
            }
        };
        this.mImsWfcCheckedChangeListener = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean imsWfcProvisionedState) {
                RadioInfo.this.setImsWfcProvisionedState(imsWfcProvisionedState);
            }
        };
        this.mEabCheckedChangeListener = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean eabProvisionedState) {
                RadioInfo.this.setEabProvisionedState(eabProvisionedState);
            }
        };
        this.mDnsCheckButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                RadioInfo.this.phone.disableDnsCheck(RadioInfo.this.phone.isDnsCheckDisabled() ^ true);
                RadioInfo.this.updateDnsCheckState();
            }
        };
        this.mOemInfoButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Intent intent = new Intent("com.android.settings.OEM_RADIO_INFO");
                try {
                    RadioInfo.this.startActivity(intent);
                }
                catch (ActivityNotFoundException ex) {
                    final RadioInfo this$0 = RadioInfo.this;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("OEM-specific Info/Settings Activity Not Found : ");
                    sb.append(ex);
                    this$0.log(sb.toString());
                }
            }
        };
        this.mPingButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                RadioInfo.this.updatePingState();
            }
        };
        this.mUpdateSmscButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                RadioInfo.this.updateSmscButton.setEnabled(false);
                RadioInfo.this.phone.setSmscAddress(RadioInfo.this.smsc.getText().toString(), RadioInfo.this.mHandler.obtainMessage(1006));
            }
        };
        this.mRefreshSmscButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                RadioInfo.this.refreshSmsc();
            }
        };
        this.mCarrierProvisioningButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Intent intent = new Intent("com.android.settings.CARRIER_PROVISIONING");
                intent.setComponent(ComponentName.unflattenFromString("com.android.omadm.service/.DMIntentReceiver"));
                RadioInfo.this.sendBroadcast(intent);
            }
        };
        this.mTriggerCarrierProvisioningButtonHandler = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Intent intent = new Intent("com.android.settings.TRIGGER_CARRIER_PROVISIONING");
                intent.setComponent(ComponentName.unflattenFromString("com.android.omadm.service/.DMIntentReceiver"));
                RadioInfo.this.sendBroadcast(intent);
            }
        };
        this.mPreferredNetworkHandler = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView adapterView, final View view, int subId, final long n) {
                if (RadioInfo.this.mPreferredNetworkTypeResult != subId && subId >= 0 && subId <= RadioInfo.mPreferredNetworkLabels.length - 2) {
                    RadioInfo.this.mPreferredNetworkTypeResult = subId;
                    subId = RadioInfo.this.phone.getSubId();
                    if (SubscriptionManager.isUsableSubIdValue(subId)) {
                        final ContentResolver contentResolver = RadioInfo.this.phone.getContext().getContentResolver();
                        final StringBuilder sb = new StringBuilder();
                        sb.append("preferred_network_mode");
                        sb.append(subId);
                        Settings.Global.putInt(contentResolver, sb.toString(), RadioInfo.this.mPreferredNetworkTypeResult);
                    }
                    final RadioInfo this$0 = RadioInfo.this;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Calling setPreferredNetworkType(");
                    sb2.append(RadioInfo.this.mPreferredNetworkTypeResult);
                    sb2.append(")");
                    this$0.log(sb2.toString());
                    RadioInfo.this.phone.setPreferredNetworkType(RadioInfo.this.mPreferredNetworkTypeResult, RadioInfo.this.mHandler.obtainMessage(1001));
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        };
        this.mCellInfoRefreshRateHandler = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                RadioInfo.this.mCellInfoRefreshRateIndex = n;
                RadioInfo.this.mTelephonyManager.setCellInfoListRate(RadioInfo.mCellInfoRefreshRates[n]);
                RadioInfo.this.updateAllCellInfo();
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        };
    }
    
    private final String buildCdmaInfoString(final CellInfoCdma cellInfoCdma) {
        final CellIdentityCdma cellIdentity = cellInfoCdma.getCellIdentity();
        final CellSignalStrengthCdma cellSignalStrength = cellInfoCdma.getCellSignalStrength();
        return String.format("%-3.3s %-5.5s %-5.5s %-5.5s %-6.6s %-6.6s %-6.6s %-6.6s %-5.5s", this.getConnectionStatusString((CellInfo)cellInfoCdma), this.getCellInfoDisplayString(cellIdentity.getSystemId()), this.getCellInfoDisplayString(cellIdentity.getNetworkId()), this.getCellInfoDisplayString(cellIdentity.getBasestationId()), this.getCellInfoDisplayString(cellSignalStrength.getCdmaDbm()), this.getCellInfoDisplayString(cellSignalStrength.getCdmaEcio()), this.getCellInfoDisplayString(cellSignalStrength.getEvdoDbm()), this.getCellInfoDisplayString(cellSignalStrength.getEvdoEcio()), this.getCellInfoDisplayString(cellSignalStrength.getEvdoSnr()));
    }
    
    private final String buildCellInfoString(final List<CellInfo> list) {
        final String s = new String();
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        final StringBuilder sb3 = new StringBuilder();
        final StringBuilder sb4 = new StringBuilder();
        String string7;
        if (list != null) {
            for (final CellInfo cellInfo : list) {
                if (cellInfo instanceof CellInfoLte) {
                    sb3.append(this.buildLteInfoString((CellInfoLte)cellInfo));
                }
                else if (cellInfo instanceof CellInfoWcdma) {
                    sb4.append(this.buildWcdmaInfoString((CellInfoWcdma)cellInfo));
                }
                else if (cellInfo instanceof CellInfoGsm) {
                    sb2.append(this.buildGsmInfoString((CellInfoGsm)cellInfo));
                }
                else {
                    if (!(cellInfo instanceof CellInfoCdma)) {
                        continue;
                    }
                    sb.append(this.buildCdmaInfoString((CellInfoCdma)cellInfo));
                }
            }
            String string = s;
            if (sb3.length() != 0) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(s);
                sb5.append(String.format("LTE\n%-3.3s %-3.3s %-3.3s %-5.5s %-5.5s %-3.3s %-6.6s %-2.2s %-4.4s %-4.4s %-2.2s\n", "SRV", "MCC", "MNC", "TAC", "CID", "PCI", "EARFCN", "BW", "RSRP", "RSRQ", "TA"));
                final String string2 = sb5.toString();
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string2);
                sb6.append(sb3.toString());
                string = sb6.toString();
            }
            String string3 = string;
            if (sb4.length() != 0) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(string);
                sb7.append(String.format("WCDMA\n%-3.3s %-3.3s %-3.3s %-5.5s %-5.5s %-6.6s %-3.3s %-4.4s\n", "SRV", "MCC", "MNC", "LAC", "CID", "UARFCN", "PSC", "RSCP"));
                final String string4 = sb7.toString();
                final StringBuilder sb8 = new StringBuilder();
                sb8.append(string4);
                sb8.append(sb4.toString());
                string3 = sb8.toString();
            }
            String string5 = string3;
            if (sb2.length() != 0) {
                final StringBuilder sb9 = new StringBuilder();
                sb9.append(string3);
                sb9.append(String.format("GSM\n%-3.3s %-3.3s %-3.3s %-5.5s %-5.5s %-6.6s %-4.4s %-4.4s\n", "SRV", "MCC", "MNC", "LAC", "CID", "ARFCN", "BSIC", "RSSI"));
                final String string6 = sb9.toString();
                final StringBuilder sb10 = new StringBuilder();
                sb10.append(string6);
                sb10.append(sb2.toString());
                string5 = sb10.toString();
            }
            string7 = string5;
            if (sb.length() != 0) {
                final StringBuilder sb11 = new StringBuilder();
                sb11.append(string5);
                sb11.append(String.format("CDMA/EVDO\n%-3.3s %-5.5s %-5.5s %-5.5s %-6.6s %-6.6s %-6.6s %-6.6s %-5.5s\n", "SRV", "SID", "NID", "BSID", "C-RSSI", "C-ECIO", "E-RSSI", "E-ECIO", "E-SNR"));
                final String string8 = sb11.toString();
                final StringBuilder sb12 = new StringBuilder();
                sb12.append(string8);
                sb12.append(sb.toString());
                string7 = sb12.toString();
            }
        }
        else {
            string7 = "unknown";
        }
        return string7.toString();
    }
    
    private final String buildGsmInfoString(final CellInfoGsm cellInfoGsm) {
        final CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
        return String.format("%-3.3s %-3.3s %-3.3s %-5.5s %-5.5s %-6.6s %-4.4s %-4.4s\n", this.getConnectionStatusString((CellInfo)cellInfoGsm), this.getCellInfoDisplayString(cellIdentity.getMcc()), this.getCellInfoDisplayString(cellIdentity.getMnc()), this.getCellInfoDisplayString(cellIdentity.getLac()), this.getCellInfoDisplayString(cellIdentity.getCid()), this.getCellInfoDisplayString(cellIdentity.getArfcn()), this.getCellInfoDisplayString(cellIdentity.getBsic()), this.getCellInfoDisplayString(cellInfoGsm.getCellSignalStrength().getDbm()));
    }
    
    private final String buildLteInfoString(final CellInfoLte cellInfoLte) {
        final CellIdentityLte cellIdentity = cellInfoLte.getCellIdentity();
        final CellSignalStrengthLte cellSignalStrength = cellInfoLte.getCellSignalStrength();
        return String.format("%-3.3s %-3.3s %-3.3s %-5.5s %-5.5s %-3.3s %-6.6s %-2.2s %-4.4s %-4.4s %-2.2s\n", this.getConnectionStatusString((CellInfo)cellInfoLte), this.getCellInfoDisplayString(cellIdentity.getMcc()), this.getCellInfoDisplayString(cellIdentity.getMnc()), this.getCellInfoDisplayString(cellIdentity.getTac()), this.getCellInfoDisplayString(cellIdentity.getCi()), this.getCellInfoDisplayString(cellIdentity.getPci()), this.getCellInfoDisplayString(cellIdentity.getEarfcn()), this.getCellInfoDisplayString(cellIdentity.getBandwidth()), this.getCellInfoDisplayString(cellSignalStrength.getDbm()), this.getCellInfoDisplayString(cellSignalStrength.getRsrq()), this.getCellInfoDisplayString(cellSignalStrength.getTimingAdvance()));
    }
    
    private final String buildWcdmaInfoString(final CellInfoWcdma cellInfoWcdma) {
        final CellIdentityWcdma cellIdentity = cellInfoWcdma.getCellIdentity();
        return String.format("%-3.3s %-3.3s %-3.3s %-5.5s %-5.5s %-6.6s %-3.3s %-4.4s\n", this.getConnectionStatusString((CellInfo)cellInfoWcdma), this.getCellInfoDisplayString(cellIdentity.getMcc()), this.getCellInfoDisplayString(cellIdentity.getMnc()), this.getCellInfoDisplayString(cellIdentity.getLac()), this.getCellInfoDisplayString(cellIdentity.getCid()), this.getCellInfoDisplayString(cellIdentity.getUarfcn()), this.getCellInfoDisplayString(cellIdentity.getPsc()), this.getCellInfoDisplayString(cellInfoWcdma.getCellSignalStrength().getDbm()));
    }
    
    private final String getCellInfoDisplayString(final int n) {
        String string;
        if (n != Integer.MAX_VALUE) {
            string = Integer.toString(n);
        }
        else {
            string = "";
        }
        return string;
    }
    
    private final String getConnectionStatusString(final CellInfo cellInfo) {
        String s = "";
        final String s2 = "";
        final String s3 = "";
        if (cellInfo.isRegistered()) {
            s = "R";
        }
        final int cellConnectionStatus = cellInfo.getCellConnectionStatus();
        String s4 = null;
        if (cellConnectionStatus != Integer.MAX_VALUE) {
            switch (cellConnectionStatus) {
                default: {
                    s4 = s2;
                    break;
                }
                case 2: {
                    s4 = "S";
                    break;
                }
                case 1: {
                    s4 = "P";
                    break;
                }
                case 0: {
                    s4 = "N";
                    break;
                }
            }
        }
        else {
            s4 = s2;
        }
        String s5 = s3;
        if (!TextUtils.isEmpty((CharSequence)s)) {
            s5 = s3;
            if (!TextUtils.isEmpty((CharSequence)s4)) {
                s5 = "+";
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(s5);
        sb.append(s4);
        return sb.toString();
    }
    
    private void httpClientTest() {
        HttpURLConnection httpURLConnection2;
        final HttpURLConnection httpURLConnection = httpURLConnection2 = null;
        try {
            while (true) {
                try {
                    httpURLConnection2 = httpURLConnection;
                    final URL url = new URL("https://www.google.com");
                    httpURLConnection2 = httpURLConnection;
                    final HttpURLConnection httpURLConnection3 = httpURLConnection2 = (HttpURLConnection)url.openConnection();
                    if (httpURLConnection3.getResponseCode() == 200) {
                        httpURLConnection2 = httpURLConnection3;
                        this.mHttpClientTestResult = "Pass";
                    }
                    else {
                        httpURLConnection2 = httpURLConnection3;
                        httpURLConnection2 = httpURLConnection3;
                        final StringBuilder sb = new StringBuilder();
                        httpURLConnection2 = httpURLConnection3;
                        sb.append("Fail: Code: ");
                        httpURLConnection2 = httpURLConnection3;
                        sb.append(httpURLConnection3.getResponseMessage());
                        httpURLConnection2 = httpURLConnection3;
                        this.mHttpClientTestResult = sb.toString();
                    }
                    if (httpURLConnection3 != null) {
                        final HttpURLConnection httpURLConnection4 = httpURLConnection3;
                        httpURLConnection4.disconnect();
                    }
                }
                finally {
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.disconnect();
                    }
                    continue;
                }
                break;
            }
        }
        catch (IOException ex) {}
    }
    
    private static boolean isEabEnabledByPlatform(final Context context) {
        if (context != null) {
            final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)context.getSystemService("carrier_config");
            if (carrierConfigManager != null && carrierConfigManager.getConfig().getBoolean("use_rcs_presence_bool")) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isEabProvisioned() {
        return this.isFeatureProvisioned(25, false);
    }
    
    private boolean isFeatureProvisioned(final int n, boolean n2) {
        boolean b = n2 != 0;
        if (this.mImsManager != null) {
            try {
                final ImsConfig configInterface = this.mImsManager.getConfigInterface();
                b = (n2 != 0);
                if (configInterface != null) {
                    final int provisionedValue = configInterface.getProvisionedValue(n);
                    n2 = 1;
                    if (provisionedValue != 1) {
                        n2 = 0;
                    }
                    b = (n2 != 0);
                }
            }
            catch (ImsException ex) {
                Log.e("RadioInfo", "isFeatureProvisioned() exception:", (Throwable)ex);
                b = (n2 != 0);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("isFeatureProvisioned() featureId=");
        sb.append(n);
        sb.append(" provisioned=");
        sb.append(b);
        this.log(sb.toString());
        return b;
    }
    
    private boolean isImsVolteProvisioned() {
        final Phone phone = this.phone;
        final boolean b = false;
        if (phone != null && this.mImsManager != null) {
            final ImsManager mImsManager = this.mImsManager;
            boolean b2 = b;
            if (ImsManager.isVolteEnabledByPlatform(this.phone.getContext())) {
                final ImsManager mImsManager2 = this.mImsManager;
                b2 = b;
                if (ImsManager.isVolteProvisionedOnDevice(this.phone.getContext())) {
                    b2 = true;
                }
            }
            return b2;
        }
        return false;
    }
    
    private boolean isImsVtProvisioned() {
        final Phone phone = this.phone;
        final boolean b = false;
        if (phone != null && this.mImsManager != null) {
            final ImsManager mImsManager = this.mImsManager;
            boolean b2 = b;
            if (ImsManager.isVtEnabledByPlatform(this.phone.getContext())) {
                final ImsManager mImsManager2 = this.mImsManager;
                b2 = b;
                if (ImsManager.isVtProvisionedOnDevice(this.phone.getContext())) {
                    b2 = true;
                }
            }
            return b2;
        }
        return false;
    }
    
    private boolean isImsWfcProvisioned() {
        final Phone phone = this.phone;
        final boolean b = false;
        if (phone != null && this.mImsManager != null) {
            final ImsManager mImsManager = this.mImsManager;
            boolean b2 = b;
            if (ImsManager.isWfcEnabledByPlatform(this.phone.getContext())) {
                final ImsManager mImsManager2 = this.mImsManager;
                b2 = b;
                if (ImsManager.isWfcProvisionedOnDevice(this.phone.getContext())) {
                    b2 = true;
                }
            }
            return b2;
        }
        return false;
    }
    
    private boolean isRadioOn() {
        return this.phone.getServiceState().getState() != 3;
    }
    
    private void log(final String s) {
        Log.d("RadioInfo", s);
    }
    
    private final void pingHostname() {
        try {
            try {
                final int wait = Runtime.getRuntime().exec("ping -c 1 www.google.com").waitFor();
                if (wait == 0) {
                    this.mPingHostnameResultV4 = "Pass";
                }
                else {
                    this.mPingHostnameResultV4 = String.format("Fail(%d)", wait);
                }
            }
            catch (InterruptedException ex) {}
        }
        catch (IOException ex2) {
            this.mPingHostnameResultV4 = "Fail: IOException";
        }
        try {
            final int wait2 = Runtime.getRuntime().exec("ping6 -c 1 www.google.com").waitFor();
            if (wait2 == 0) {
                this.mPingHostnameResultV6 = "Pass";
            }
            else {
                this.mPingHostnameResultV6 = String.format("Fail(%d)", wait2);
            }
        }
        catch (IOException ex3) {
            this.mPingHostnameResultV6 = "Fail: IOException";
        }
        return;
        this.mPingHostnameResultV6 = "Fail: InterruptedException";
        this.mPingHostnameResultV4 = "Fail: InterruptedException";
    }
    
    private void refreshSmsc() {
        this.phone.getSmscAddress(this.mHandler.obtainMessage(1005));
    }
    
    private void restoreFromBundle(final Bundle bundle) {
        if (bundle == null) {
            return;
        }
        this.mPingHostnameResultV4 = bundle.getString("mPingHostnameResultV4", "");
        this.mPingHostnameResultV6 = bundle.getString("mPingHostnameResultV6", "");
        this.mHttpClientTestResult = bundle.getString("mHttpClientTestResult", "");
        this.mPingHostnameV4.setText((CharSequence)this.mPingHostnameResultV4);
        this.mPingHostnameV6.setText((CharSequence)this.mPingHostnameResultV6);
        this.mHttpClientTest.setText((CharSequence)this.mHttpClientTestResult);
        this.mPreferredNetworkTypeResult = bundle.getInt("mPreferredNetworkTypeResult", RadioInfo.mPreferredNetworkLabels.length - 1);
        this.mCellInfoRefreshRateIndex = bundle.getInt("mCellInfoRefreshRateIndex", 0);
    }
    
    private final void updateAllCellInfo() {
        this.mCellInfo.setText((CharSequence)"");
        this.mNeighboringCids.setText((CharSequence)"");
        this.mLocation.setText((CharSequence)"");
        new Thread() {
            final /* synthetic */ Runnable val$updateAllCellInfoResults = new Runnable(this) {
                @Override
                public void run() {
                    RadioInfo.this.updateNeighboringCids(RadioInfo.this.mNeighboringCellResult);
                    RadioInfo.this.updateLocation(RadioInfo.this.mCellLocationResult);
                    RadioInfo.this.updateCellInfo(RadioInfo.this.mCellInfoResult);
                }
            };
            
            @Override
            public void run() {
                RadioInfo.this.mCellInfoResult = (List<CellInfo>)RadioInfo.this.mTelephonyManager.getAllCellInfo();
                RadioInfo.this.mCellLocationResult = RadioInfo.this.mTelephonyManager.getCellLocation();
                RadioInfo.this.mNeighboringCellResult = (List<NeighboringCellInfo>)RadioInfo.this.mTelephonyManager.getNeighboringCellInfo();
                RadioInfo.this.mHandler.post(this.val$updateAllCellInfoResults);
            }
        }.start();
    }
    
    private void updateBandwidths(int n, final int n2) {
        final int n3 = -1;
        if (n < 0 || n == Integer.MAX_VALUE) {
            n = -1;
        }
        int n4 = n3;
        if (n2 >= 0) {
            if (n2 == Integer.MAX_VALUE) {
                n4 = n3;
            }
            else {
                n4 = n2;
            }
        }
        this.mDownlinkKbps.setText((CharSequence)String.format("%-5d", n));
        this.mUplinkKbps.setText((CharSequence)String.format("%-5d", n4));
    }
    
    private final void updateCallRedirect() {
        this.mCfi.setText((CharSequence)String.valueOf(this.mCfiValue));
    }
    
    private final void updateCellInfo(final List<CellInfo> list) {
        this.mCellInfo.setText((CharSequence)this.buildCellInfoString(list));
    }
    
    private final void updateDataState() {
        final int dataState = this.mTelephonyManager.getDataState();
        final Resources resources = this.getResources();
        String text = resources.getString(2131888746);
        switch (dataState) {
            case 3: {
                text = resources.getString(2131888727);
                break;
            }
            case 2: {
                text = resources.getString(2131888724);
                break;
            }
            case 1: {
                text = resources.getString(2131888725);
                break;
            }
            case 0: {
                text = resources.getString(2131888726);
                break;
            }
        }
        this.gprsState.setText((CharSequence)text);
    }
    
    private final void updateDataStats2() {
        final Resources resources = this.getResources();
        final long mobileTxPackets = TrafficStats.getMobileTxPackets();
        final long mobileRxPackets = TrafficStats.getMobileRxPackets();
        final long mobileTxBytes = TrafficStats.getMobileTxBytes();
        final long mobileRxBytes = TrafficStats.getMobileRxBytes();
        final String string = resources.getString(2131888731);
        final String string2 = resources.getString(2131888729);
        final TextView sent = this.sent;
        final StringBuilder sb = new StringBuilder();
        sb.append(mobileTxPackets);
        sb.append(" ");
        sb.append(string);
        sb.append(", ");
        sb.append(mobileTxBytes);
        sb.append(" ");
        sb.append(string2);
        sent.setText((CharSequence)sb.toString());
        final TextView received = this.received;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(mobileRxPackets);
        sb2.append(" ");
        sb2.append(string);
        sb2.append(", ");
        sb2.append(mobileRxBytes);
        sb2.append(" ");
        sb2.append(string2);
        received.setText((CharSequence)sb2.toString());
    }
    
    private final void updateDcRtInfoTv(final DataConnectionRealTimeInfo dataConnectionRealTimeInfo) {
        this.mDcRtInfoTv.setText((CharSequence)dataConnectionRealTimeInfo.toString());
    }
    
    private void updateDnsCheckState() {
        final TextView dnsCheckState = this.dnsCheckState;
        String text;
        if (this.phone.isDnsCheckDisabled()) {
            text = "0.0.0.0 allowed";
        }
        else {
            text = "0.0.0.0 not allowed";
        }
        dnsCheckState.setText((CharSequence)text);
    }
    
    private void updateImsProvisionedState() {
        final StringBuilder sb = new StringBuilder();
        sb.append("updateImsProvisionedState isImsVolteProvisioned()=");
        sb.append(this.isImsVolteProvisioned());
        this.log(sb.toString());
        this.imsVolteProvisionedSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        this.imsVolteProvisionedSwitch.setChecked(this.isImsVolteProvisioned());
        this.imsVolteProvisionedSwitch.setOnCheckedChangeListener(this.mImsVolteCheckedChangeListener);
        final Switch imsVolteProvisionedSwitch = this.imsVolteProvisionedSwitch;
        final ImsManager mImsManager = this.mImsManager;
        imsVolteProvisionedSwitch.setEnabled(ImsManager.isVolteEnabledByPlatform(this.phone.getContext()));
        this.imsVtProvisionedSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        this.imsVtProvisionedSwitch.setChecked(this.isImsVtProvisioned());
        this.imsVtProvisionedSwitch.setOnCheckedChangeListener(this.mImsVtCheckedChangeListener);
        final Switch imsVtProvisionedSwitch = this.imsVtProvisionedSwitch;
        final ImsManager mImsManager2 = this.mImsManager;
        imsVtProvisionedSwitch.setEnabled(ImsManager.isVtEnabledByPlatform(this.phone.getContext()));
        this.imsWfcProvisionedSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        this.imsWfcProvisionedSwitch.setChecked(this.isImsWfcProvisioned());
        this.imsWfcProvisionedSwitch.setOnCheckedChangeListener(this.mImsWfcCheckedChangeListener);
        final Switch imsWfcProvisionedSwitch = this.imsWfcProvisionedSwitch;
        final ImsManager mImsManager3 = this.mImsManager;
        imsWfcProvisionedSwitch.setEnabled(ImsManager.isWfcEnabledByPlatform(this.phone.getContext()));
        this.eabProvisionedSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        this.eabProvisionedSwitch.setChecked(this.isEabProvisioned());
        this.eabProvisionedSwitch.setOnCheckedChangeListener(this.mEabCheckedChangeListener);
        this.eabProvisionedSwitch.setEnabled(isEabEnabledByPlatform(this.phone.getContext()));
    }
    
    private final void updateLocation(final CellLocation cellLocation) {
        final Resources resources = this.getResources();
        if (cellLocation instanceof GsmCellLocation) {
            final GsmCellLocation gsmCellLocation = (GsmCellLocation)cellLocation;
            final int lac = gsmCellLocation.getLac();
            final int cid = gsmCellLocation.getCid();
            final TextView mLocation = this.mLocation;
            final StringBuilder sb = new StringBuilder();
            sb.append(resources.getString(2131888732));
            sb.append(" = ");
            String hexString;
            if (lac == -1) {
                hexString = "unknown";
            }
            else {
                hexString = Integer.toHexString(lac);
            }
            sb.append(hexString);
            sb.append("   ");
            sb.append(resources.getString(2131888723));
            sb.append(" = ");
            String hexString2;
            if (cid == -1) {
                hexString2 = "unknown";
            }
            else {
                hexString2 = Integer.toHexString(cid);
            }
            sb.append(hexString2);
            mLocation.setText((CharSequence)sb.toString());
        }
        else if (cellLocation instanceof CdmaCellLocation) {
            final CdmaCellLocation cdmaCellLocation = (CdmaCellLocation)cellLocation;
            final int baseStationId = cdmaCellLocation.getBaseStationId();
            final int systemId = cdmaCellLocation.getSystemId();
            final int networkId = cdmaCellLocation.getNetworkId();
            final int baseStationLatitude = cdmaCellLocation.getBaseStationLatitude();
            final int baseStationLongitude = cdmaCellLocation.getBaseStationLongitude();
            final TextView mLocation2 = this.mLocation;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("BID = ");
            String hexString3;
            if (baseStationId == -1) {
                hexString3 = "unknown";
            }
            else {
                hexString3 = Integer.toHexString(baseStationId);
            }
            sb2.append(hexString3);
            sb2.append("   SID = ");
            String hexString4;
            if (systemId == -1) {
                hexString4 = "unknown";
            }
            else {
                hexString4 = Integer.toHexString(systemId);
            }
            sb2.append(hexString4);
            sb2.append("   NID = ");
            String hexString5;
            if (networkId == -1) {
                hexString5 = "unknown";
            }
            else {
                hexString5 = Integer.toHexString(networkId);
            }
            sb2.append(hexString5);
            sb2.append("\nLAT = ");
            String hexString6;
            if (baseStationLatitude == -1) {
                hexString6 = "unknown";
            }
            else {
                hexString6 = Integer.toHexString(baseStationLatitude);
            }
            sb2.append(hexString6);
            sb2.append("   LONG = ");
            String hexString7;
            if (baseStationLongitude == -1) {
                hexString7 = "unknown";
            }
            else {
                hexString7 = Integer.toHexString(baseStationLongitude);
            }
            sb2.append(hexString7);
            mLocation2.setText((CharSequence)sb2.toString());
        }
        else {
            this.mLocation.setText((CharSequence)"unknown");
        }
    }
    
    private final void updateMessageWaiting() {
        this.mMwi.setText((CharSequence)String.valueOf(this.mMwiValue));
    }
    
    private final void updateNeighboringCids(final List<NeighboringCellInfo> list) {
        final StringBuilder sb = new StringBuilder();
        if (list != null) {
            if (list.isEmpty()) {
                sb.append("no neighboring cells");
            }
            else {
                final Iterator<NeighboringCellInfo> iterator = list.iterator();
                while (iterator.hasNext()) {
                    sb.append(iterator.next().toString());
                    sb.append(" ");
                }
            }
        }
        else {
            sb.append("unknown");
        }
        this.mNeighboringCids.setText((CharSequence)sb.toString());
    }
    
    private final void updateNetworkType() {
        if (this.phone != null) {
            this.phone.getServiceState();
            this.dataNetwork.setText((CharSequence)ServiceState.rilRadioTechnologyToString(this.phone.getServiceState().getRilDataRadioTechnology()));
            this.voiceNetwork.setText((CharSequence)ServiceState.rilRadioTechnologyToString(this.phone.getServiceState().getRilVoiceRadioTechnology()));
        }
    }
    
    private final void updatePhoneState(final int n) {
        final Resources resources = this.getResources();
        String text = resources.getString(2131888746);
        switch (n) {
            case 2: {
                text = resources.getString(2131888738);
                break;
            }
            case 1: {
                text = resources.getString(2131888739);
                break;
            }
            case 0: {
                text = resources.getString(2131888737);
                break;
            }
        }
        this.callState.setText((CharSequence)text);
    }
    
    private void updatePhysicalChannelConfiguration(final List<PhysicalChannelConfig> list) {
        final StringBuilder sb = new StringBuilder();
        final String s = "";
        sb.append("{");
        if (list != null) {
            final Iterator<PhysicalChannelConfig> iterator = list.iterator();
            String s2 = s;
            while (iterator.hasNext()) {
                final PhysicalChannelConfig physicalChannelConfig = iterator.next();
                sb.append(s2);
                sb.append(physicalChannelConfig);
                s2 = ",";
            }
        }
        sb.append("}");
        this.mPhyChanConfig.setText((CharSequence)sb.toString());
    }
    
    private final void updatePingState() {
        this.mPingHostnameResultV4 = this.getResources().getString(2131888746);
        this.mPingHostnameResultV6 = this.getResources().getString(2131888746);
        this.mHttpClientTestResult = this.getResources().getString(2131888746);
        this.mPingHostnameV4.setText((CharSequence)this.mPingHostnameResultV4);
        this.mPingHostnameV6.setText((CharSequence)this.mPingHostnameResultV6);
        this.mHttpClientTest.setText((CharSequence)this.mHttpClientTestResult);
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                RadioInfo.this.mPingHostnameV4.setText((CharSequence)RadioInfo.this.mPingHostnameResultV4);
                RadioInfo.this.mPingHostnameV6.setText((CharSequence)RadioInfo.this.mPingHostnameResultV6);
                RadioInfo.this.mHttpClientTest.setText((CharSequence)RadioInfo.this.mHttpClientTestResult);
            }
        };
        new Thread() {
            @Override
            public void run() {
                RadioInfo.this.pingHostname();
                RadioInfo.this.mHandler.post(runnable);
            }
        }.start();
        new Thread() {
            @Override
            public void run() {
                RadioInfo.this.httpClientTest();
                RadioInfo.this.mHandler.post(runnable);
            }
        }.start();
    }
    
    private void updatePreferredNetworkType(final int n) {
        int mPreferredNetworkTypeResult;
        if (n >= RadioInfo.mPreferredNetworkLabels.length || (mPreferredNetworkTypeResult = n) < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("EVENT_QUERY_PREFERRED_TYPE_DONE: unknown type=");
            sb.append(n);
            this.log(sb.toString());
            mPreferredNetworkTypeResult = RadioInfo.mPreferredNetworkLabels.length - 1;
        }
        this.mPreferredNetworkTypeResult = mPreferredNetworkTypeResult;
        this.preferredNetworkType.setSelection(this.mPreferredNetworkTypeResult, true);
    }
    
    private final void updateProperties() {
        final Resources resources = this.getResources();
        String text;
        if ((text = this.phone.getDeviceId()) == null) {
            text = resources.getString(2131888746);
        }
        this.mDeviceId.setText((CharSequence)text);
        String text2;
        if ((text2 = this.phone.getSubscriberId()) == null) {
            text2 = resources.getString(2131888746);
        }
        this.mSubscriberId.setText((CharSequence)text2);
        String text3;
        if ((text3 = this.phone.getLine1Number()) == null) {
            text3 = resources.getString(2131888746);
        }
        this.number.setText((CharSequence)text3);
    }
    
    private void updateRadioPowerState() {
        this.radioPowerOnSwitch.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        this.radioPowerOnSwitch.setChecked(this.isRadioOn());
        this.radioPowerOnSwitch.setOnCheckedChangeListener(this.mRadioPowerOnChangeListener);
    }
    
    private final void updateServiceState(final ServiceState serviceState) {
        final int state = serviceState.getState();
        final Resources resources = this.getResources();
        String text = resources.getString(2131888746);
        switch (state) {
            case 3: {
                text = resources.getString(2131888744);
                break;
            }
            case 1:
            case 2: {
                text = resources.getString(2131888742);
                break;
            }
            case 0: {
                text = resources.getString(2131888743);
                break;
            }
        }
        this.gsmState.setText((CharSequence)text);
        if (serviceState.getRoaming()) {
            this.roamingState.setText(2131888740);
        }
        else {
            this.roamingState.setText(2131888741);
        }
        this.operatorName.setText((CharSequence)serviceState.getOperatorAlphaLong());
    }
    
    private final void updateSignalStrength(final SignalStrength signalStrength) {
        final Resources resources = this.getResources();
        final int dbm = signalStrength.getDbm();
        int asuLevel;
        if (-1 == (asuLevel = signalStrength.getAsuLevel())) {
            asuLevel = 0;
        }
        final TextView dBm = this.dBm;
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(dbm));
        sb.append(" ");
        sb.append(resources.getString(2131888730));
        sb.append("   ");
        sb.append(String.valueOf(asuLevel));
        sb.append(" ");
        sb.append(resources.getString(2131888728));
        dBm.setText((CharSequence)sb.toString());
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (!Process.myUserHandle().isSystem()) {
            Log.e("RadioInfo", "Not run from system user, don't do anything.");
            this.finish();
            return;
        }
        this.setContentView(2131558700);
        this.log("Started onCreate");
        this.mTelephonyManager = (TelephonyManager)this.getSystemService("phone");
        this.mConnectivityManager = (ConnectivityManager)this.getSystemService("connectivity");
        this.phone = PhoneFactory.getDefaultPhone();
        this.mImsManager = ImsManager.getInstance(this.getApplicationContext(), SubscriptionManager.getDefaultVoicePhoneId());
        this.mDeviceId = (TextView)this.findViewById(2131362248);
        this.number = (TextView)this.findViewById(2131362409);
        this.mSubscriberId = (TextView)this.findViewById(2131362260);
        this.callState = (TextView)this.findViewById(2131361957);
        this.operatorName = (TextView)this.findViewById(2131362423);
        this.roamingState = (TextView)this.findViewById(2131362534);
        this.gsmState = (TextView)this.findViewById(2131362183);
        this.gprsState = (TextView)this.findViewById(2131362177);
        this.voiceNetwork = (TextView)this.findViewById(2131362809);
        this.dataNetwork = (TextView)this.findViewById(2131362040);
        this.dBm = (TextView)this.findViewById(2131362051);
        this.mMwi = (TextView)this.findViewById(2131362381);
        this.mCfi = (TextView)this.findViewById(2131361977);
        this.mLocation = (TextView)this.findViewById(2131362344);
        this.mNeighboringCids = (TextView)this.findViewById(2131362383);
        (this.mCellInfo = (TextView)this.findViewById(2131361971)).setTypeface(Typeface.MONOSPACE);
        this.mDcRtInfoTv = (TextView)this.findViewById(2131362052);
        this.sent = (TextView)this.findViewById(2131362584);
        this.received = (TextView)this.findViewById(2131362509);
        this.smsc = (EditText)this.findViewById(2131362621);
        this.dnsCheckState = (TextView)this.findViewById(2131362082);
        this.mPingHostnameV4 = (TextView)this.findViewById(2131362450);
        this.mPingHostnameV6 = (TextView)this.findViewById(2131362451);
        this.mHttpClientTest = (TextView)this.findViewById(2131362233);
        this.mPhyChanConfig = (TextView)this.findViewById(2131362447);
        this.preferredNetworkType = (Spinner)this.findViewById(2131362461);
        final ArrayAdapter adapter = new ArrayAdapter((Context)this, 17367048, (Object[])RadioInfo.mPreferredNetworkLabels);
        adapter.setDropDownViewResource(17367049);
        this.preferredNetworkType.setAdapter((SpinnerAdapter)adapter);
        this.cellInfoRefreshRateSpinner = (Spinner)this.findViewById(2131361969);
        final ArrayAdapter adapter2 = new ArrayAdapter((Context)this, 17367048, (Object[])RadioInfo.mCellInfoRefreshRateLabels);
        adapter2.setDropDownViewResource(17367049);
        this.cellInfoRefreshRateSpinner.setAdapter((SpinnerAdapter)adapter2);
        this.imsVolteProvisionedSwitch = (Switch)this.findViewById(2131362812);
        this.imsVtProvisionedSwitch = (Switch)this.findViewById(2131362814);
        this.imsWfcProvisionedSwitch = (Switch)this.findViewById(2131362817);
        this.eabProvisionedSwitch = (Switch)this.findViewById(2131362094);
        this.radioPowerOnSwitch = (Switch)this.findViewById(2131362508);
        this.mDownlinkKbps = (TextView)this.findViewById(2131362079);
        this.mUplinkKbps = (TextView)this.findViewById(2131362765);
        this.updateBandwidths(0, 0);
        (this.pingTestButton = (Button)this.findViewById(2131362452)).setOnClickListener(this.mPingButtonHandler);
        (this.updateSmscButton = (Button)this.findViewById(2131362777)).setOnClickListener(this.mUpdateSmscButtonHandler);
        (this.refreshSmscButton = (Button)this.findViewById(2131362513)).setOnClickListener(this.mRefreshSmscButtonHandler);
        (this.dnsCheckToggleButton = (Button)this.findViewById(2131362083)).setOnClickListener(this.mDnsCheckButtonHandler);
        (this.carrierProvisioningButton = (Button)this.findViewById(2131361967)).setOnClickListener(this.mCarrierProvisioningButtonHandler);
        (this.triggercarrierProvisioningButton = (Button)this.findViewById(2131362751)).setOnClickListener(this.mTriggerCarrierProvisioningButtonHandler);
        (this.oemInfoButton = (Button)this.findViewById(2131362412)).setOnClickListener(this.mOemInfoButtonHandler);
        if (this.getPackageManager().queryIntentActivities(new Intent("com.android.settings.OEM_RADIO_INFO"), 0).size() == 0) {
            this.oemInfoButton.setEnabled(false);
        }
        this.mCellInfoRefreshRateIndex = 0;
        this.mPreferredNetworkTypeResult = RadioInfo.mPreferredNetworkLabels.length - 1;
        this.phone.getPreferredNetworkType(this.mHandler.obtainMessage(1000));
        this.restoreFromBundle(bundle);
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        menu.add(0, 0, 0, 2131888749).setOnMenuItemClickListener(this.mSelectBandCallback).setAlphabeticShortcut('b');
        menu.add(1, 1, 0, 2131888734).setOnMenuItemClickListener(this.mViewADNCallback);
        menu.add(1, 2, 0, 2131888735).setOnMenuItemClickListener(this.mViewFDNCallback);
        menu.add(1, 3, 0, 2131888736).setOnMenuItemClickListener(this.mViewSDNCallback);
        menu.add(1, 4, 0, 2131888733).setOnMenuItemClickListener(this.mGetImsStatus);
        menu.add(1, 5, 0, 2131888755).setOnMenuItemClickListener(this.mToggleData);
        return true;
    }
    
    protected void onPause() {
        super.onPause();
        this.log("onPause: unregister phone & data intents");
        this.mTelephonyManager.listen(this.mPhoneStateListener, 0);
        this.mTelephonyManager.setCellInfoListRate(Integer.MAX_VALUE);
        this.mConnectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
    }
    
    public boolean onPrepareOptionsMenu(final Menu menu) {
        final MenuItem item = menu.findItem(5);
        final int dataState = this.mTelephonyManager.getDataState();
        boolean visible = true;
        if (dataState != 0) {
            switch (dataState) {
                default: {
                    visible = false;
                    break;
                }
                case 2:
                case 3: {
                    item.setTitle(2131888755);
                    break;
                }
            }
        }
        else {
            item.setTitle(2131888756);
        }
        item.setVisible(visible);
        return true;
    }
    
    protected void onResume() {
        super.onResume();
        this.log("Started onResume");
        this.updateMessageWaiting();
        this.updateCallRedirect();
        this.updateDataState();
        this.updateDataStats2();
        this.updateRadioPowerState();
        this.updateImsProvisionedState();
        this.updateProperties();
        this.updateDnsCheckState();
        this.updateNetworkType();
        this.updateNeighboringCids(this.mNeighboringCellResult);
        this.updateLocation(this.mCellLocationResult);
        this.updateCellInfo(this.mCellInfoResult);
        this.mPingHostnameV4.setText((CharSequence)this.mPingHostnameResultV4);
        this.mPingHostnameV6.setText((CharSequence)this.mPingHostnameResultV6);
        this.mHttpClientTest.setText((CharSequence)this.mHttpClientTestResult);
        this.cellInfoRefreshRateSpinner.setOnItemSelectedListener(this.mCellInfoRefreshRateHandler);
        this.cellInfoRefreshRateSpinner.setSelection(this.mCellInfoRefreshRateIndex);
        this.preferredNetworkType.setSelection(this.mPreferredNetworkTypeResult, true);
        this.preferredNetworkType.setOnItemSelectedListener(this.mPreferredNetworkHandler);
        this.radioPowerOnSwitch.setOnCheckedChangeListener(this.mRadioPowerOnChangeListener);
        this.imsVolteProvisionedSwitch.setOnCheckedChangeListener(this.mImsVolteCheckedChangeListener);
        this.imsVtProvisionedSwitch.setOnCheckedChangeListener(this.mImsVtCheckedChangeListener);
        this.imsWfcProvisionedSwitch.setOnCheckedChangeListener(this.mImsWfcCheckedChangeListener);
        this.eabProvisionedSwitch.setOnCheckedChangeListener(this.mEabCheckedChangeListener);
        this.mTelephonyManager.listen(this.mPhoneStateListener, 1050109);
        this.mConnectivityManager.registerNetworkCallback(this.mDefaultNetworkRequest, this.mNetworkCallback, this.mHandler);
        this.smsc.clearFocus();
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        bundle.putString("mPingHostnameResultV4", this.mPingHostnameResultV4);
        bundle.putString("mPingHostnameResultV6", this.mPingHostnameResultV6);
        bundle.putString("mHttpClientTestResult", this.mHttpClientTestResult);
        bundle.putInt("mPreferredNetworkTypeResult", this.mPreferredNetworkTypeResult);
        bundle.putInt("mCellInfoRefreshRateIndex", this.mCellInfoRefreshRateIndex);
    }
    
    void setEabProvisionedState(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setEabProvisioned() state: ");
        String s;
        if (b) {
            s = "on";
        }
        else {
            s = "off";
        }
        sb.append(s);
        Log.d("RadioInfo", sb.toString());
        this.setImsConfigProvisionedState(25, b);
    }
    
    void setImsConfigProvisionedState(final int n, final boolean b) {
        if (this.phone != null && this.mImsManager != null) {
            QueuedWork.queue((Runnable)new Runnable() {
                @Override
                public void run() {
                    try {
                        RadioInfo.this.mImsManager.getConfigInterface().setProvisionedValue(n, (int)(b ? 1 : 0));
                    }
                    catch (ImsException ex) {
                        Log.e("RadioInfo", "setImsConfigProvisioned() exception:", (Throwable)ex);
                    }
                }
            }, false);
        }
    }
    
    void setImsVolteProvisionedState(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setImsVolteProvisioned state: ");
        String s;
        if (b) {
            s = "on";
        }
        else {
            s = "off";
        }
        sb.append(s);
        Log.d("RadioInfo", sb.toString());
        this.setImsConfigProvisionedState(10, b);
    }
    
    void setImsVtProvisionedState(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setImsVtProvisioned() state: ");
        String s;
        if (b) {
            s = "on";
        }
        else {
            s = "off";
        }
        sb.append(s);
        Log.d("RadioInfo", sb.toString());
        this.setImsConfigProvisionedState(11, b);
    }
    
    void setImsWfcProvisionedState(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setImsWfcProvisioned() state: ");
        String s;
        if (b) {
            s = "on";
        }
        else {
            s = "off";
        }
        sb.append(s);
        Log.d("RadioInfo", sb.toString());
        this.setImsConfigProvisionedState(28, b);
    }
}
