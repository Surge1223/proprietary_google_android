package com.android.settings;

import android.util.Log;

public class SubSettings extends SettingsActivity
{
    @Override
    protected boolean isValidFragment(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Launching fragment ");
        sb.append(s);
        Log.d("SubSettings", sb.toString());
        return true;
    }
    
    @Override
    public boolean onNavigateUp() {
        this.finish();
        return true;
    }
}
