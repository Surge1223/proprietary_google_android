package com.android.settings;

import android.widget.TextView;
import android.text.TextUtils;
import android.widget.ProgressBar;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class SummaryPreference extends Preference
{
    private String mAmount;
    private boolean mChartEnabled;
    private String mEndLabel;
    private float mLeftRatio;
    private float mMiddleRatio;
    private float mRightRatio;
    private String mStartLabel;
    private String mUnits;
    
    public SummaryPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChartEnabled = true;
        this.setLayoutResource(2131558746);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final ProgressBar progressBar = (ProgressBar)preferenceViewHolder.itemView.findViewById(2131361995);
        if (this.mChartEnabled) {
            progressBar.setVisibility(0);
            final int progress = (int)(this.mLeftRatio * 100.0f);
            progressBar.setProgress(progress);
            progressBar.setSecondaryProgress((int)(this.mMiddleRatio * 100.0f) + progress);
        }
        else {
            progressBar.setVisibility(8);
        }
        if (this.mChartEnabled && (!TextUtils.isEmpty((CharSequence)this.mStartLabel) || !TextUtils.isEmpty((CharSequence)this.mEndLabel))) {
            preferenceViewHolder.findViewById(2131362312).setVisibility(0);
            ((TextView)preferenceViewHolder.findViewById(16908308)).setText((CharSequence)this.mStartLabel);
            ((TextView)preferenceViewHolder.findViewById(16908309)).setText((CharSequence)this.mEndLabel);
        }
        else {
            preferenceViewHolder.findViewById(2131362312).setVisibility(8);
        }
    }
    
    public void setAmount(final String mAmount) {
        this.mAmount = mAmount;
        if (this.mAmount != null && this.mUnits != null) {
            this.setTitle(TextUtils.expandTemplate(this.getContext().getText(2131889309), new CharSequence[] { this.mAmount, this.mUnits }));
        }
    }
    
    public void setChartEnabled(final boolean mChartEnabled) {
        if (this.mChartEnabled != mChartEnabled) {
            this.mChartEnabled = mChartEnabled;
            this.notifyChanged();
        }
    }
    
    public void setLabels(final String mStartLabel, final String mEndLabel) {
        this.mStartLabel = mStartLabel;
        this.mEndLabel = mEndLabel;
        this.notifyChanged();
    }
    
    public void setRatios(final float mLeftRatio, final float mMiddleRatio, final float mRightRatio) {
        this.mLeftRatio = mLeftRatio;
        this.mMiddleRatio = mMiddleRatio;
        this.mRightRatio = mRightRatio;
        this.notifyChanged();
    }
    
    public void setUnits(final String mUnits) {
        this.mUnits = mUnits;
        if (this.mAmount != null && this.mUnits != null) {
            this.setTitle(TextUtils.expandTemplate(this.getContext().getText(2131889309), new CharSequence[] { this.mAmount, this.mUnits }));
        }
    }
}
