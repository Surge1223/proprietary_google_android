package com.android.settings;

import android.content.ContentResolver;
import android.content.IntentFilter;
import android.provider.Settings;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.PowerManager;
import android.util.Log;
import android.os.UserHandle;
import java.util.Objects;
import android.os.UserManager;
import android.view.View;
import android.animation.TimeInterpolator;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.os.Message;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.app.Activity;

public class FallbackHome extends Activity
{
    private Handler mHandler;
    private final Runnable mProgressTimeoutRunnable;
    private boolean mProvisioned;
    private BroadcastReceiver mReceiver;
    
    public FallbackHome() {
        this.mProgressTimeoutRunnable = new _$$Lambda$FallbackHome$t1fq3k7x_PY_DiX5Fz_YbaIlCdg(this);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                FallbackHome.this.maybeFinish();
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                FallbackHome.this.maybeFinish();
            }
        };
    }
    
    private void maybeFinish() {
        if (((UserManager)this.getSystemService((Class)UserManager.class)).isUserUnlocked()) {
            if (Objects.equals(this.getPackageName(), this.getPackageManager().resolveActivity(new Intent("android.intent.action.MAIN").addCategory("android.intent.category.HOME"), 0).activityInfo.packageName)) {
                if (UserManager.isSplitSystemUser() && UserHandle.myUserId() == 0) {
                    return;
                }
                Log.d("FallbackHome", "User unlocked but no home; let's hope someone enables one soon?");
                this.mHandler.sendEmptyMessageDelayed(0, 500L);
            }
            else {
                Log.d("FallbackHome", "User unlocked and real home found; let's go!");
                ((PowerManager)this.getSystemService((Class)PowerManager.class)).userActivity(SystemClock.uptimeMillis(), false);
                this.finish();
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final ContentResolver contentResolver = this.getContentResolver();
        boolean mProvisioned = false;
        if (Settings.Global.getInt(contentResolver, "device_provisioned", 0) != 0) {
            mProvisioned = true;
        }
        if (!(this.mProvisioned = mProvisioned)) {
            this.setTheme(2131951793);
            this.getWindow().getDecorView().setSystemUiVisibility(4102);
        }
        else {
            this.getWindow().getDecorView().setSystemUiVisibility(1536);
        }
        this.registerReceiver(this.mReceiver, new IntentFilter("android.intent.action.USER_UNLOCKED"));
        this.maybeFinish();
    }
    
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(this.mReceiver);
    }
    
    protected void onPause() {
        super.onPause();
        this.mHandler.removeCallbacks(this.mProgressTimeoutRunnable);
    }
    
    protected void onResume() {
        super.onResume();
        if (this.mProvisioned) {
            this.mHandler.postDelayed(this.mProgressTimeoutRunnable, 2000L);
        }
    }
}
