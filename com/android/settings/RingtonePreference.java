package com.android.settings;

import android.text.TextUtils;
import android.media.RingtoneManager;
import android.os.Parcelable;
import android.support.v7.preference.PreferenceManager;
import android.net.Uri;
import android.content.res.TypedArray;
import android.os.UserHandle;
import android.content.Intent;
import com.android.internal.R$styleable;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class RingtonePreference extends Preference
{
    private int mRingtoneType;
    private boolean mShowDefault;
    private boolean mShowSilent;
    protected Context mUserContext;
    protected int mUserId;
    
    public RingtonePreference(final Context context, final AttributeSet set) {
        super(context, set);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R$styleable.RingtonePreference, 0, 0);
        this.mRingtoneType = obtainStyledAttributes.getInt(0, 1);
        this.mShowDefault = obtainStyledAttributes.getBoolean(1, true);
        this.mShowSilent = obtainStyledAttributes.getBoolean(2, true);
        this.setIntent(new Intent("android.intent.action.RINGTONE_PICKER"));
        this.setUserId(UserHandle.myUserId());
        obtainStyledAttributes.recycle();
    }
    
    public int getRingtoneType() {
        return this.mRingtoneType;
    }
    
    public int getUserId() {
        return this.mUserId;
    }
    
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (intent != null) {
            final Uri uri = (Uri)intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
            String string;
            if (uri != null) {
                string = uri.toString();
            }
            else {
                string = "";
            }
            if (this.callChangeListener(string)) {
                this.onSaveRingtone(uri);
            }
        }
        return true;
    }
    
    @Override
    protected void onAttachedToHierarchy(final PreferenceManager preferenceManager) {
        super.onAttachedToHierarchy(preferenceManager);
    }
    
    @Override
    protected Object onGetDefaultValue(final TypedArray typedArray, final int n) {
        return typedArray.getString(n);
    }
    
    public void onPrepareRingtonePickerIntent(final Intent intent) {
        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", (Parcelable)this.onRestoreRingtone());
        intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", this.mShowDefault);
        if (this.mShowDefault) {
            intent.putExtra("android.intent.extra.ringtone.DEFAULT_URI", (Parcelable)RingtoneManager.getDefaultUri(this.getRingtoneType()));
        }
        intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", this.mShowSilent);
        intent.putExtra("android.intent.extra.ringtone.TYPE", this.mRingtoneType);
        intent.putExtra("android.intent.extra.ringtone.TITLE", this.getTitle());
        intent.putExtra("android.intent.extra.ringtone.AUDIO_ATTRIBUTES_FLAGS", 64);
    }
    
    protected Uri onRestoreRingtone() {
        Uri parse = null;
        final String persistedString = this.getPersistedString(null);
        if (!TextUtils.isEmpty((CharSequence)persistedString)) {
            parse = Uri.parse(persistedString);
        }
        return parse;
    }
    
    protected void onSaveRingtone(final Uri uri) {
        String string;
        if (uri != null) {
            string = uri.toString();
        }
        else {
            string = "";
        }
        this.persistString(string);
    }
    
    @Override
    protected void onSetInitialValue(final boolean b, final Object o) {
        final String s = (String)o;
        if (b) {
            return;
        }
        if (!TextUtils.isEmpty((CharSequence)s)) {
            this.onSaveRingtone(Uri.parse(s));
        }
    }
    
    public void setUserId(final int mUserId) {
        this.mUserId = mUserId;
        this.mUserContext = Utils.createPackageContextAsUser(this.getContext(), this.mUserId);
    }
}
