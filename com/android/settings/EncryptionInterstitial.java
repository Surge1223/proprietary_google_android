package com.android.settings;

import android.util.Log;
import com.android.setupwizardlib.GlifLayout;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import com.android.settings.core.InstrumentedFragment;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog$Builder;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.view.accessibility.AccessibilityManager;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.widget.LinearLayout;
import android.os.Bundle;
import android.content.res.Resources.Theme;
import android.os.Parcelable;
import android.content.Intent;
import android.content.Context;

public class EncryptionInterstitial extends SettingsActivity
{
    private static final String TAG;
    
    static {
        TAG = EncryptionInterstitial.class.getSimpleName();
    }
    
    public static Intent createStartIntent(final Context context, final int n, final boolean b, final Intent intent) {
        return new Intent(context, (Class)EncryptionInterstitial.class).putExtra("extra_password_quality", n).putExtra(":settings:show_fragment_title_resid", 2131887549).putExtra("extra_require_password", b).putExtra("extra_unlock_method_intent", (Parcelable)intent);
    }
    
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", EncryptionInterstitialFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return EncryptionInterstitialFragment.class.getName().equals(s);
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        super.onApplyThemeResource(resources$Theme, SetupWizardUtils.getTheme(this.getIntent()), b);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ((LinearLayout)this.findViewById(2131362015)).setFitsSystemWindows(false);
    }
    
    public static class AccessibilityWarningDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        public static AccessibilityWarningDialogFragment newInstance(final int n) {
            final AccessibilityWarningDialogFragment accessibilityWarningDialogFragment = new AccessibilityWarningDialogFragment();
            final Bundle arguments = new Bundle(1);
            arguments.putInt("extra_password_quality", n);
            accessibilityWarningDialogFragment.setArguments(arguments);
            return accessibilityWarningDialogFragment;
        }
        
        public int getMetricsCategory() {
            return 581;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final EncryptionInterstitialFragment encryptionInterstitialFragment = (EncryptionInterstitialFragment)this.getParentFragment();
            if (encryptionInterstitialFragment != null) {
                if (n == -1) {
                    encryptionInterstitialFragment.setRequirePasswordState(true);
                    encryptionInterstitialFragment.startLockIntent();
                }
                else if (n == -2) {
                    encryptionInterstitialFragment.setRequirePasswordState(false);
                }
            }
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final int int1 = this.getArguments().getInt("extra_password_quality");
            int title;
            int n;
            if (int1 != 65536) {
                if (int1 != 131072 && int1 != 196608) {
                    title = 2131887543;
                    n = 2131887540;
                }
                else {
                    title = 2131887545;
                    n = 2131887542;
                }
            }
            else {
                title = 2131887544;
                n = 2131887541;
            }
            final Activity activity = this.getActivity();
            final List enabledAccessibilityServiceList = AccessibilityManager.getInstance((Context)activity).getEnabledAccessibilityServiceList(-1);
            CharSequence loadLabel;
            if (enabledAccessibilityServiceList.isEmpty()) {
                loadLabel = "";
            }
            else {
                loadLabel = enabledAccessibilityServiceList.get(0).getResolveInfo().loadLabel(activity.getPackageManager());
            }
            return (Dialog)new AlertDialog$Builder((Context)activity).setTitle(title).setMessage((CharSequence)this.getString(n, new Object[] { loadLabel })).setCancelable(true).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
        }
    }
    
    public static class EncryptionInterstitialFragment extends InstrumentedFragment implements View.OnClickListener
    {
        private View mDontRequirePasswordToDecrypt;
        private boolean mPasswordRequired;
        private int mRequestedPasswordQuality;
        private View mRequirePasswordToDecrypt;
        private Intent mUnlockMethodIntent;
        
        private void setRequirePasswordState(final boolean mPasswordRequired) {
            this.mPasswordRequired = mPasswordRequired;
        }
        
        public void finish() {
            final Activity activity = this.getActivity();
            if (activity == null) {
                return;
            }
            if (this.getFragmentManager().getBackStackEntryCount() > 0) {
                this.getFragmentManager().popBackStack();
            }
            else {
                activity.finish();
            }
        }
        
        public int getMetricsCategory() {
            return 48;
        }
        
        public void onActivityResult(final int n, final int n2, final Intent intent) {
            super.onActivityResult(n, n2, intent);
            if (n == 100 && n2 != 0) {
                this.getActivity().setResult(n2, intent);
                this.finish();
            }
        }
        
        public void onClick(final View view) {
            if (view == this.mRequirePasswordToDecrypt) {
                if (AccessibilityManager.getInstance((Context)this.getActivity()).isEnabled() && !this.mPasswordRequired) {
                    this.setRequirePasswordState(false);
                    AccessibilityWarningDialogFragment.newInstance(this.mRequestedPasswordQuality).show(this.getChildFragmentManager(), "AccessibilityWarningDialog");
                }
                else {
                    this.setRequirePasswordState(true);
                    this.startLockIntent();
                }
            }
            else {
                this.setRequirePasswordState(false);
                this.startLockIntent();
            }
        }
        
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            return layoutInflater.inflate(2131558551, viewGroup, false);
        }
        
        public void onViewCreated(final View view, final Bundle bundle) {
            super.onViewCreated(view, bundle);
            this.mRequirePasswordToDecrypt = view.findViewById(2131362107);
            this.mDontRequirePasswordToDecrypt = view.findViewById(2131362106);
            final boolean booleanExtra = this.getActivity().getIntent().getBooleanExtra("for_fingerprint", false);
            final Intent intent = this.getActivity().getIntent();
            this.mRequestedPasswordQuality = intent.getIntExtra("extra_password_quality", 0);
            this.mUnlockMethodIntent = (Intent)intent.getParcelableExtra("extra_unlock_method_intent");
            final int mRequestedPasswordQuality = this.mRequestedPasswordQuality;
            int text;
            if (mRequestedPasswordQuality != 65536) {
                if (mRequestedPasswordQuality != 131072 && mRequestedPasswordQuality != 196608) {
                    if (booleanExtra) {
                        text = 2131887551;
                    }
                    else {
                        text = 2131887550;
                    }
                }
                else if (booleanExtra) {
                    text = 2131887555;
                }
                else {
                    text = 2131887554;
                }
            }
            else if (booleanExtra) {
                text = 2131887553;
            }
            else {
                text = 2131887552;
            }
            ((TextView)this.getActivity().findViewById(2131362108)).setText(text);
            this.mRequirePasswordToDecrypt.setOnClickListener((View.OnClickListener)this);
            this.mDontRequirePasswordToDecrypt.setOnClickListener((View.OnClickListener)this);
            this.setRequirePasswordState(this.getActivity().getIntent().getBooleanExtra("extra_require_password", true));
            ((GlifLayout)view).setHeaderText(this.getActivity().getTitle());
        }
        
        protected void startLockIntent() {
            if (this.mUnlockMethodIntent != null) {
                this.mUnlockMethodIntent.putExtra("extra_require_password", this.mPasswordRequired);
                this.startActivityForResult(this.mUnlockMethodIntent, 100);
            }
            else {
                Log.wtf(EncryptionInterstitial.TAG, "no unlock intent to start");
                this.finish();
            }
        }
    }
}
