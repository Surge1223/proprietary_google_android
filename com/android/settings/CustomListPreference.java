package com.android.settings;

import android.app.FragmentTransaction;
import android.support.v14.preference.ListPreferenceDialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.os.Bundle;
import android.app.Dialog;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.ListPreference;

public class CustomListPreference extends ListPreference
{
    public CustomListPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public CustomListPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    protected CharSequence getConfirmationMessage(final String s) {
        return null;
    }
    
    protected boolean isAutoClosePreference() {
        return true;
    }
    
    protected void onDialogClosed(final boolean b) {
    }
    
    protected void onDialogCreated(final Dialog dialog) {
    }
    
    protected void onDialogStateRestored(final Dialog dialog, final Bundle bundle) {
    }
    
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
    }
    
    public static class ConfirmDialogFragment extends InstrumentedDialogFragment
    {
        @Override
        public int getMetricsCategory() {
            return 529;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage(this.getArguments().getCharSequence("android.intent.extra.TEXT")).setPositiveButton(17039370, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    final Fragment targetFragment = ConfirmDialogFragment.this.getTargetFragment();
                    if (targetFragment != null) {
                        ((CustomListPreferenceDialogFragment)targetFragment).onItemConfirmed();
                    }
                }
            }).setNegativeButton(17039360, (DialogInterface$OnClickListener)null).create();
        }
    }
    
    public static class CustomListPreferenceDialogFragment extends ListPreferenceDialogFragment
    {
        private int mClickedDialogEntryIndex;
        
        private CustomListPreference getCustomizablePreference() {
            return (CustomListPreference)this.getPreference();
        }
        
        private String getValue() {
            final CustomListPreference customizablePreference = this.getCustomizablePreference();
            if (this.mClickedDialogEntryIndex >= 0 && customizablePreference.getEntryValues() != null) {
                return customizablePreference.getEntryValues()[this.mClickedDialogEntryIndex].toString();
            }
            return null;
        }
        
        public static ListPreferenceDialogFragment newInstance(final String s) {
            final CustomListPreferenceDialogFragment customListPreferenceDialogFragment = new CustomListPreferenceDialogFragment();
            final Bundle arguments = new Bundle(1);
            arguments.putString("key", s);
            customListPreferenceDialogFragment.setArguments(arguments);
            return customListPreferenceDialogFragment;
        }
        
        protected DialogInterface$OnClickListener getOnItemClickListener() {
            return (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int clickedDialogEntryIndex) {
                    CustomListPreferenceDialogFragment.this.setClickedDialogEntryIndex(clickedDialogEntryIndex);
                    if (CustomListPreferenceDialogFragment.this.getCustomizablePreference().isAutoClosePreference()) {
                        CustomListPreferenceDialogFragment.this.onItemChosen();
                    }
                }
            };
        }
        
        public void onActivityCreated(final Bundle bundle) {
            super.onActivityCreated(bundle);
            this.getCustomizablePreference().onDialogStateRestored(this.getDialog(), bundle);
        }
        
        @Override
        public Dialog onCreateDialog(final Bundle bundle) {
            final Dialog onCreateDialog = super.onCreateDialog(bundle);
            if (bundle != null) {
                this.mClickedDialogEntryIndex = bundle.getInt("settings.CustomListPrefDialog.KEY_CLICKED_ENTRY_INDEX", this.mClickedDialogEntryIndex);
            }
            this.getCustomizablePreference().onDialogCreated(onCreateDialog);
            return onCreateDialog;
        }
        
        @Override
        public void onDialogClosed(final boolean b) {
            this.getCustomizablePreference().onDialogClosed(b);
            final CustomListPreference customizablePreference = this.getCustomizablePreference();
            final String value = this.getValue();
            if (b && value != null && customizablePreference.callChangeListener(value)) {
                customizablePreference.setValue(value);
            }
        }
        
        protected void onItemChosen() {
            final CharSequence confirmationMessage = this.getCustomizablePreference().getConfirmationMessage(this.getValue());
            if (confirmationMessage != null) {
                final ConfirmDialogFragment confirmDialogFragment = new ConfirmDialogFragment();
                final Bundle arguments = new Bundle();
                arguments.putCharSequence("android.intent.extra.TEXT", confirmationMessage);
                ((Fragment)confirmDialogFragment).setArguments(arguments);
                ((Fragment)confirmDialogFragment).setTargetFragment((Fragment)this, 0);
                final FragmentTransaction beginTransaction = this.getFragmentManager().beginTransaction();
                final StringBuilder sb = new StringBuilder();
                sb.append(this.getTag());
                sb.append("-Confirm");
                beginTransaction.add((Fragment)confirmDialogFragment, sb.toString());
                beginTransaction.commitAllowingStateLoss();
            }
            else {
                this.onItemConfirmed();
            }
        }
        
        protected void onItemConfirmed() {
            this.onClick((DialogInterface)this.getDialog(), -1);
            this.getDialog().dismiss();
        }
        
        @Override
        protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder) {
            super.onPrepareDialogBuilder(alertDialog$Builder);
            this.mClickedDialogEntryIndex = this.getCustomizablePreference().findIndexOfValue(this.getCustomizablePreference().getValue());
            this.getCustomizablePreference().onPrepareDialogBuilder(alertDialog$Builder, this.getOnItemClickListener());
            if (!this.getCustomizablePreference().isAutoClosePreference()) {
                alertDialog$Builder.setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        CustomListPreferenceDialogFragment.this.onItemChosen();
                    }
                });
            }
        }
        
        @Override
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putInt("settings.CustomListPrefDialog.KEY_CLICKED_ENTRY_INDEX", this.mClickedDialogEntryIndex);
        }
        
        protected void setClickedDialogEntryIndex(final int mClickedDialogEntryIndex) {
            this.mClickedDialogEntryIndex = mClickedDialogEntryIndex;
        }
    }
}
