package com.android.settings;

import android.util.Log;
import android.text.format.DateUtils;
import android.view.ViewGroup;
import java.util.Collections;
import java.util.List;
import java.util.Collection;
import android.content.pm.PackageManager;
import java.util.Calendar;
import java.util.ArrayList;
import android.util.ArrayMap;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Map;
import android.app.usage.UsageStats;
import java.util.Comparator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.os.Bundle;
import android.app.usage.UsageStatsManager;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.widget.AdapterView$OnItemSelectedListener;
import android.app.Activity;

public class UsageStatsActivity extends Activity implements AdapterView$OnItemSelectedListener
{
    private UsageStatsAdapter mAdapter;
    private LayoutInflater mInflater;
    private PackageManager mPm;
    private UsageStatsManager mUsageStatsManager;
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131558863);
        this.mUsageStatsManager = (UsageStatsManager)this.getSystemService("usagestats");
        this.mInflater = (LayoutInflater)this.getSystemService("layout_inflater");
        this.mPm = this.getPackageManager();
        ((Spinner)this.findViewById(2131362760)).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        ((ListView)this.findViewById(2131362454)).setAdapter((ListAdapter)(this.mAdapter = new UsageStatsAdapter()));
    }
    
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        this.mAdapter.sortList(n);
    }
    
    public void onNothingSelected(final AdapterView<?> adapterView) {
    }
    
    public static class AppNameComparator implements Comparator<UsageStats>
    {
        private Map<String, String> mAppLabelList;
        
        AppNameComparator(final Map<String, String> mAppLabelList) {
            this.mAppLabelList = mAppLabelList;
        }
        
        @Override
        public final int compare(final UsageStats usageStats, final UsageStats usageStats2) {
            return this.mAppLabelList.get(usageStats.getPackageName()).compareTo((String)this.mAppLabelList.get(usageStats2.getPackageName()));
        }
    }
    
    static class AppViewHolder
    {
        TextView lastTimeUsed;
        TextView pkgName;
        TextView usageTime;
    }
    
    public static class LastTimeUsedComparator implements Comparator<UsageStats>
    {
        @Override
        public final int compare(final UsageStats usageStats, final UsageStats usageStats2) {
            return Long.compare(usageStats2.getLastTimeUsed(), usageStats.getLastTimeUsed());
        }
    }
    
    class UsageStatsAdapter extends BaseAdapter
    {
        private AppNameComparator mAppLabelComparator;
        private final ArrayMap<String, String> mAppLabelMap;
        private int mDisplayOrder;
        private LastTimeUsedComparator mLastTimeUsedComparator;
        private final ArrayList<UsageStats> mPackageStats;
        private UsageTimeComparator mUsageTimeComparator;
        
        UsageStatsAdapter() {
            this.mDisplayOrder = 0;
            this.mLastTimeUsedComparator = new LastTimeUsedComparator();
            this.mUsageTimeComparator = new UsageTimeComparator();
            this.mAppLabelMap = (ArrayMap<String, String>)new ArrayMap();
            this.mPackageStats = new ArrayList<UsageStats>();
            final Calendar instance = Calendar.getInstance();
            instance.add(6, -5);
            final List queryUsageStats = UsageStatsActivity.this.mUsageStatsManager.queryUsageStats(4, instance.getTimeInMillis(), System.currentTimeMillis());
            if (queryUsageStats == null) {
                return;
            }
            final ArrayMap arrayMap = new ArrayMap();
            for (int size = queryUsageStats.size(), i = 0; i < size; ++i) {
                final UsageStats usageStats = queryUsageStats.get(i);
                try {
                    this.mAppLabelMap.put((Object)usageStats.getPackageName(), (Object)UsageStatsActivity.this.mPm.getApplicationInfo(usageStats.getPackageName(), 0).loadLabel(UsageStatsActivity.this.mPm).toString());
                    final UsageStats usageStats2 = (UsageStats)arrayMap.get((Object)usageStats.getPackageName());
                    if (usageStats2 == null) {
                        arrayMap.put((Object)usageStats.getPackageName(), (Object)usageStats);
                    }
                    else {
                        usageStats2.add(usageStats);
                    }
                }
                catch (PackageManager$NameNotFoundException ex) {}
            }
            this.mPackageStats.addAll(arrayMap.values());
            this.mAppLabelComparator = new AppNameComparator((Map<String, String>)this.mAppLabelMap);
            this.sortList();
        }
        
        private void sortList() {
            if (this.mDisplayOrder == 0) {
                Collections.sort(this.mPackageStats, this.mUsageTimeComparator);
            }
            else if (this.mDisplayOrder == 1) {
                Collections.sort(this.mPackageStats, this.mLastTimeUsedComparator);
            }
            else if (this.mDisplayOrder == 2) {
                Collections.sort(this.mPackageStats, this.mAppLabelComparator);
            }
            this.notifyDataSetChanged();
        }
        
        public int getCount() {
            return this.mPackageStats.size();
        }
        
        public Object getItem(final int n) {
            return this.mPackageStats.get(n);
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, View inflate, final ViewGroup viewGroup) {
            AppViewHolder tag;
            if (inflate == null) {
                inflate = UsageStatsActivity.this.mInflater.inflate(2131558864, (ViewGroup)null);
                tag = new AppViewHolder();
                tag.pkgName = (TextView)inflate.findViewById(2131362430);
                tag.lastTimeUsed = (TextView)inflate.findViewById(2131362321);
                tag.usageTime = (TextView)inflate.findViewById(2131362782);
                inflate.setTag((Object)tag);
            }
            else {
                tag = (AppViewHolder)inflate.getTag();
            }
            final UsageStats usageStats = this.mPackageStats.get(n);
            if (usageStats != null) {
                tag.pkgName.setText((CharSequence)this.mAppLabelMap.get((Object)usageStats.getPackageName()));
                tag.lastTimeUsed.setText(DateUtils.formatSameDayTime(usageStats.getLastTimeUsed(), System.currentTimeMillis(), 2, 2));
                tag.usageTime.setText((CharSequence)DateUtils.formatElapsedTime(usageStats.getTotalTimeInForeground() / 1000L));
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("No usage stats info for package:");
                sb.append(n);
                Log.w("UsageStatsActivity", sb.toString());
            }
            return inflate;
        }
        
        void sortList(final int mDisplayOrder) {
            if (this.mDisplayOrder == mDisplayOrder) {
                return;
            }
            this.mDisplayOrder = mDisplayOrder;
            this.sortList();
        }
    }
    
    public static class UsageTimeComparator implements Comparator<UsageStats>
    {
        @Override
        public final int compare(final UsageStats usageStats, final UsageStats usageStats2) {
            return Long.compare(usageStats2.getTotalTimeInForeground(), usageStats.getTotalTimeInForeground());
        }
    }
}
