package com.android.settings.development;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;

public class AppViewHolder
{
    public ImageView appIcon;
    public TextView appName;
    public TextView disabled;
    public View rootView;
    public TextView summary;
    
    public static AppViewHolder createOrRecycle(final LayoutInflater layoutInflater, final View view) {
        if (view == null) {
            final View inflate = layoutInflater.inflate(2131558644, (ViewGroup)null);
            final AppViewHolder tag = new AppViewHolder();
            tag.rootView = inflate;
            tag.appName = (TextView)inflate.findViewById(16908310);
            tag.appIcon = (ImageView)inflate.findViewById(16908294);
            tag.summary = (TextView)inflate.findViewById(16908304);
            tag.disabled = (TextView)inflate.findViewById(2131361883);
            inflate.setTag((Object)tag);
            return tag;
        }
        return (AppViewHolder)view.getTag();
    }
}
