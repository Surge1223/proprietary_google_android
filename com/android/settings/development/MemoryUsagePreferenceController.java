package com.android.settings.development;

import android.support.v7.preference.Preference;
import com.android.settings.applications.ProcessStatsBase;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.utils.ThreadUtils;
import android.text.format.Formatter;
import android.content.Context;
import com.android.settings.applications.ProcStatsData;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class MemoryUsagePreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    private ProcStatsData mProcStatsData;
    
    public MemoryUsagePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mProcStatsData = this.getProcStatsData();
        this.setDuration();
    }
    
    @Override
    public String getPreferenceKey() {
        return "memory";
    }
    
    ProcStatsData getProcStatsData() {
        return new ProcStatsData(this.mContext, false);
    }
    
    void setDuration() {
        this.mProcStatsData.setDuration(ProcessStatsBase.sDurations[0]);
    }
    
    @Override
    public void updateState(final Preference preference) {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$MemoryUsagePreferenceController$2UovDioLDVLRpJrL4IsFsRdoZts(this));
    }
}
