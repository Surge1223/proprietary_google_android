package com.android.settings.development;

import android.bluetooth.BluetoothCodecConfig;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class BluetoothAudioQualityPreferenceController extends AbstractBluetoothA2dpPreferenceController
{
    public BluetoothAudioQualityPreferenceController(final Context context, final Lifecycle lifecycle, final BluetoothA2dpConfigStore bluetoothA2dpConfigStore) {
        super(context, lifecycle, bluetoothA2dpConfigStore);
    }
    
    @Override
    protected int getCurrentA2dpSettingIndex(final BluetoothCodecConfig bluetoothCodecConfig) {
        final int n = (int)bluetoothCodecConfig.getCodecSpecific1();
        int n2;
        if (n > 0) {
            n2 = n % 10;
        }
        else {
            n2 = 3;
        }
        switch (n2) {
            default: {
                n2 = 3;
                return n2;
            }
            case 0:
            case 1:
            case 2:
            case 3: {
                return n2;
            }
        }
    }
    
    @Override
    protected int getDefaultIndex() {
        return 3;
    }
    
    @Override
    protected String[] getListSummaries() {
        return this.mContext.getResources().getStringArray(2130903069);
    }
    
    @Override
    protected String[] getListValues() {
        return this.mContext.getResources().getStringArray(2130903071);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_select_a2dp_ldac_playback_quality";
    }
    
    @Override
    protected void writeConfigurationValues(final Object o) {
        final int indexOfValue = this.mPreference.findIndexOfValue(o.toString());
        int codecSpecific1Value = 0;
        switch (indexOfValue) {
            case 0:
            case 1:
            case 2:
            case 3: {
                codecSpecific1Value = 1000 + indexOfValue;
                break;
            }
        }
        this.mBluetoothA2dpConfigStore.setCodecSpecific1Value(codecSpecific1Value);
    }
}
