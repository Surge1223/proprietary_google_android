package com.android.settings.development;

import android.os.RemoteException;
import android.support.v7.preference.ListPreference;
import android.view.IWindowManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.view.IWindowManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class TransitionAnimationScalePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final float DEFAULT_VALUE = 1.0f;
    static final int TRANSITION_ANIMATION_SCALE_SELECTOR = 1;
    private final String[] mListSummaries;
    private final String[] mListValues;
    private final IWindowManager mWindowManager;
    
    public TransitionAnimationScalePreferenceController(final Context context) {
        super(context);
        this.mWindowManager = IWindowManager$Stub.asInterface(ServiceManager.getService("window"));
        this.mListValues = context.getResources().getStringArray(2130903158);
        this.mListSummaries = context.getResources().getStringArray(2130903157);
    }
    
    private void updateAnimationScaleValue() {
        try {
            final float animationScale = this.mWindowManager.getAnimationScale(1);
            final int n = 0;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= this.mListValues.length) {
                    break;
                }
                if (animationScale <= Float.parseFloat(this.mListValues[n2])) {
                    n3 = n2;
                    break;
                }
                ++n2;
            }
            final ListPreference listPreference = (ListPreference)this.mPreference;
            listPreference.setValue(this.mListValues[n3]);
            listPreference.setSummary(this.mListSummaries[n3]);
        }
        catch (RemoteException ex) {}
    }
    
    private void writeAnimationScaleOption(final Object o) {
        float float1 = 0.0f;
        Label_0021: {
            if (o != null) {
                try {
                    float1 = Float.parseFloat(o.toString());
                    break Label_0021;
                }
                catch (RemoteException ex) {
                    return;
                }
            }
            float1 = 1.0f;
        }
        this.mWindowManager.setAnimationScale(1, float1);
        this.updateAnimationScaleValue();
    }
    
    @Override
    public String getPreferenceKey() {
        return "transition_animation_scale";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeAnimationScaleOption(null);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeAnimationScaleOption(o);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateAnimationScaleValue();
    }
}
