package com.android.settings.development;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothCodecConfig;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class BluetoothAudioCodecPreferenceController extends AbstractBluetoothA2dpPreferenceController
{
    public BluetoothAudioCodecPreferenceController(final Context context, final Lifecycle lifecycle, final BluetoothA2dpConfigStore bluetoothA2dpConfigStore) {
        super(context, lifecycle, bluetoothA2dpConfigStore);
    }
    
    @Override
    protected int getCurrentA2dpSettingIndex(final BluetoothCodecConfig bluetoothCodecConfig) {
        final int codecType = bluetoothCodecConfig.getCodecType();
        int n = 0;
        switch (codecType) {
            case 4: {
                n = 5;
                break;
            }
            case 3: {
                n = 4;
                break;
            }
            case 2: {
                n = 3;
                break;
            }
            case 1: {
                n = 2;
                break;
            }
            case 0: {
                n = 1;
                break;
            }
        }
        return n;
    }
    
    @Override
    protected int getDefaultIndex() {
        return 0;
    }
    
    @Override
    protected String[] getListSummaries() {
        return this.mContext.getResources().getStringArray(2130903075);
    }
    
    @Override
    protected String[] getListValues() {
        return this.mContext.getResources().getStringArray(2130903077);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_select_a2dp_codec";
    }
    
    @Override
    protected void writeConfigurationValues(final Object o) {
        final int indexOfValue = this.mPreference.findIndexOfValue(o.toString());
        int codecType = 0;
        int codecPriority = 0;
        Label_0260: {
            switch (indexOfValue) {
                case 7: {
                    synchronized (this.mBluetoothA2dpConfigStore) {
                        if (this.mBluetoothA2dp != null) {
                            this.mBluetoothA2dp.disableOptionalCodecs((BluetoothDevice)null);
                        }
                        return;
                    }
                }
                case 6: {
                    synchronized (this.mBluetoothA2dpConfigStore) {
                        if (this.mBluetoothA2dp != null) {
                            this.mBluetoothA2dp.enableOptionalCodecs((BluetoothDevice)null);
                        }
                        return;
                    }
                }
                case 5: {
                    codecType = 4;
                    codecPriority = 1000000;
                    break;
                }
                case 4: {
                    codecType = 3;
                    codecPriority = 1000000;
                    break;
                }
                case 3: {
                    codecType = 2;
                    codecPriority = 1000000;
                    break;
                }
                case 2: {
                    codecType = 1;
                    codecPriority = 1000000;
                    break;
                }
                case 1: {
                    codecType = 0;
                    codecPriority = 1000000;
                    break;
                }
                case 0: {
                    switch (this.mPreference.findIndexOfValue(this.mPreference.getValue())) {
                        default: {
                            break Label_0260;
                        }
                        case 0: {
                            break Label_0260;
                        }
                        case 5: {
                            codecType = 4;
                            break Label_0260;
                        }
                        case 4: {
                            codecType = 3;
                            break Label_0260;
                        }
                        case 3: {
                            codecType = 2;
                            break Label_0260;
                        }
                        case 2: {
                            codecType = 1;
                            break Label_0260;
                        }
                        case 1: {
                            codecType = 0;
                            break Label_0260;
                        }
                    }
                    break;
                }
            }
        }
        this.mBluetoothA2dpConfigStore.setCodecType(codecType);
        this.mBluetoothA2dpConfigStore.setCodecPriority(codecPriority);
    }
}
