package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.view.IWindowManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.view.IWindowManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class StrictModePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String STRICT_MODE_DISABLED = "";
    static final String STRICT_MODE_ENABLED = "1";
    private final IWindowManager mWindowManager;
    
    public StrictModePreferenceController(final Context context) {
        super(context);
        this.mWindowManager = IWindowManager$Stub.asInterface(ServiceManager.getService("window"));
    }
    
    private boolean isStrictModeEnabled() {
        return SystemProperties.getBoolean("persist.sys.strictmode.visual", false);
    }
    
    private void writeStrictModeVisualOptions(final boolean b) {
        try {
            final IWindowManager mWindowManager = this.mWindowManager;
            String strictModeVisualIndicatorPreference;
            if (b) {
                strictModeVisualIndicatorPreference = "1";
            }
            else {
                strictModeVisualIndicatorPreference = "";
            }
            mWindowManager.setStrictModeVisualIndicatorPreference(strictModeVisualIndicatorPreference);
        }
        catch (RemoteException ex) {}
    }
    
    @Override
    public String getPreferenceKey() {
        return "strict_mode";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeStrictModeVisualOptions(false);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeStrictModeVisualOptions((boolean)o);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(this.isStrictModeEnabled());
    }
}
