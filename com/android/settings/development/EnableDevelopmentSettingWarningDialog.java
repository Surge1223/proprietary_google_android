package com.android.settings.development;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class EnableDevelopmentSettingWarningDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    public static void show(final DevelopmentSettingsDashboardFragment developmentSettingsDashboardFragment) {
        final EnableDevelopmentSettingWarningDialog enableDevelopmentSettingWarningDialog = new EnableDevelopmentSettingWarningDialog();
        enableDevelopmentSettingWarningDialog.setTargetFragment((Fragment)developmentSettingsDashboardFragment, 0);
        final FragmentManager fragmentManager = developmentSettingsDashboardFragment.getActivity().getFragmentManager();
        if (fragmentManager.findFragmentByTag("EnableDevSettingDlg") == null) {
            enableDevelopmentSettingWarningDialog.show(fragmentManager, "EnableDevSettingDlg");
        }
    }
    
    public int getMetricsCategory() {
        return 1219;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final DevelopmentSettingsDashboardFragment developmentSettingsDashboardFragment = (DevelopmentSettingsDashboardFragment)this.getTargetFragment();
        if (n == -1) {
            developmentSettingsDashboardFragment.onEnableDevelopmentOptionsConfirmed();
        }
        else {
            developmentSettingsDashboardFragment.onEnableDevelopmentOptionsRejected();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage(2131887375).setTitle(2131887376).setPositiveButton(17039379, (DialogInterface$OnClickListener)this).setNegativeButton(17039369, (DialogInterface$OnClickListener)this).create();
    }
}
