package com.android.settings.development;

import com.android.settingslib.development.DevelopmentSettingsEnabler;
import com.android.settings.Utils;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.widget.SwitchBar;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class DevelopmentSwitchBarController implements LifecycleObserver, OnStart, OnStop
{
    private final boolean mIsAvailable;
    private final DevelopmentSettingsDashboardFragment mSettings;
    private final SwitchBar mSwitchBar;
    
    public DevelopmentSwitchBarController(final DevelopmentSettingsDashboardFragment mSettings, final SwitchBar mSwitchBar, final boolean b, final Lifecycle lifecycle) {
        this.mSwitchBar = mSwitchBar;
        this.mIsAvailable = (b && !Utils.isMonkeyRunning());
        this.mSettings = mSettings;
        if (this.mIsAvailable) {
            lifecycle.addObserver(this);
        }
        else {
            this.mSwitchBar.setEnabled(false);
        }
    }
    
    @Override
    public void onStart() {
        this.mSwitchBar.setChecked(DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(this.mSettings.getContext()));
        this.mSwitchBar.addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this.mSettings);
    }
    
    @Override
    public void onStop() {
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this.mSettings);
    }
}
