package com.android.settings.development;

import android.util.Log;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.content.pm.PackageInfo;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.Context;
import com.android.settings.webview.WebViewUpdateServiceWrapper;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class WebViewAppPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    private final PackageManagerWrapper mPackageManager;
    private final WebViewUpdateServiceWrapper mWebViewUpdateServiceWrapper;
    
    public WebViewAppPreferenceController(final Context context) {
        super(context);
        this.mPackageManager = new PackageManagerWrapper(context.getPackageManager());
        this.mWebViewUpdateServiceWrapper = new WebViewUpdateServiceWrapper();
    }
    
    private CharSequence getDefaultAppLabel() {
        return this.getDefaultAppInfo().loadLabel();
    }
    
    DefaultAppInfo getDefaultAppInfo() {
        final PackageInfo currentWebViewPackage = this.mWebViewUpdateServiceWrapper.getCurrentWebViewPackage();
        final Context mContext = this.mContext;
        final PackageManagerWrapper mPackageManager = this.mPackageManager;
        Object applicationInfo;
        if (currentWebViewPackage == null) {
            applicationInfo = null;
        }
        else {
            applicationInfo = currentWebViewPackage.applicationInfo;
        }
        return new DefaultAppInfo(mContext, mPackageManager, (PackageItemInfo)applicationInfo);
    }
    
    @Override
    public String getPreferenceKey() {
        return "select_webview_provider";
    }
    
    @Override
    public void updateState(final Preference preference) {
        final CharSequence defaultAppLabel = this.getDefaultAppLabel();
        if (!TextUtils.isEmpty(defaultAppLabel)) {
            this.mPreference.setSummary(defaultAppLabel);
        }
        else {
            Log.d("WebViewAppPrefCtrl", "No default app");
            this.mPreference.setSummary(2131886368);
        }
    }
}
