package com.android.settings.development;

import android.bluetooth.BluetoothCodecConfig;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class BluetoothAudioChannelModePreferenceController extends AbstractBluetoothA2dpPreferenceController
{
    public BluetoothAudioChannelModePreferenceController(final Context context, final Lifecycle lifecycle, final BluetoothA2dpConfigStore bluetoothA2dpConfigStore) {
        super(context, lifecycle, bluetoothA2dpConfigStore);
    }
    
    @Override
    protected int getCurrentA2dpSettingIndex(final BluetoothCodecConfig bluetoothCodecConfig) {
        final int channelMode = bluetoothCodecConfig.getChannelMode();
        int n = 0;
        switch (channelMode) {
            case 2: {
                n = 2;
                break;
            }
            case 1: {
                n = 1;
                break;
            }
        }
        return n;
    }
    
    @Override
    protected int getDefaultIndex() {
        return 0;
    }
    
    @Override
    protected String[] getListSummaries() {
        return this.mContext.getResources().getStringArray(2130903066);
    }
    
    @Override
    protected String[] getListValues() {
        return this.mContext.getResources().getStringArray(2130903068);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_select_a2dp_channel_mode";
    }
    
    @Override
    protected void writeConfigurationValues(final Object o) {
        final int indexOfValue = this.mPreference.findIndexOfValue(o.toString());
        int channelMode = 0;
        while (true) {
            switch (indexOfValue) {
                default: {}
                case 0: {
                    this.mBluetoothA2dpConfigStore.setChannelMode(channelMode);
                }
                case 2: {
                    channelMode = 2;
                    continue;
                }
                case 1: {
                    channelMode = 1;
                    continue;
                }
            }
            break;
        }
    }
}
