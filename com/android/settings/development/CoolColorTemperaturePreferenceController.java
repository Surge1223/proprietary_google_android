package com.android.settings.development;

import com.android.settingslib.development.SystemPropPoker;
import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.widget.Toast;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class CoolColorTemperaturePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String COLOR_TEMPERATURE_PROPERTY = "persist.sys.debug.color_temp";
    
    public CoolColorTemperaturePreferenceController(final Context context) {
        super(context);
    }
    
    void displayColorTemperatureToast() {
        Toast.makeText(this.mContext, 2131887043, 1).show();
    }
    
    @Override
    public String getPreferenceKey() {
        return "color_temperature";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034120);
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("persist.sys.debug.color_temp", Boolean.toString(false));
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        SystemProperties.set("persist.sys.debug.color_temp", Boolean.toString((boolean)o));
        SystemPropPoker.getInstance().poke();
        this.displayColorTemperatureToast();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("persist.sys.debug.color_temp", false));
    }
}
