package com.android.settings.development;

import com.android.settingslib.development.SystemPropPoker;
import android.os.Build;
import android.text.TextUtils;
import android.os.SystemProperties;
import android.support.v7.preference.ListPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class HdcpCheckingPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String HDCP_CHECKING_PROPERTY = "persist.sys.hdcp_checking";
    static final String USER_BUILD_TYPE = "user";
    private final String[] mListSummaries;
    private final String[] mListValues;
    
    public HdcpCheckingPreferenceController(final Context context) {
        super(context);
        this.mListValues = this.mContext.getResources().getStringArray(2130903124);
        this.mListSummaries = this.mContext.getResources().getStringArray(2130903122);
    }
    
    private void updateHdcpValues(final ListPreference listPreference) {
        final String value = SystemProperties.get("persist.sys.hdcp_checking");
        final int n = 1;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= this.mListValues.length) {
                break;
            }
            if (TextUtils.equals((CharSequence)value, (CharSequence)this.mListValues[n2])) {
                n3 = n2;
                break;
            }
            ++n2;
        }
        listPreference.setValue(this.mListValues[n3]);
        listPreference.setSummary(this.mListSummaries[n3]);
    }
    
    public String getBuildType() {
        return Build.TYPE;
    }
    
    @Override
    public String getPreferenceKey() {
        return "hdcp_checking";
    }
    
    @Override
    public boolean isAvailable() {
        return TextUtils.equals((CharSequence)"user", (CharSequence)this.getBuildType()) ^ true;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        SystemProperties.set("persist.sys.hdcp_checking", o.toString());
        this.updateHdcpValues((ListPreference)this.mPreference);
        SystemPropPoker.getInstance().poke();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateHdcpValues((ListPreference)this.mPreference);
    }
}
