package com.android.settings.development;

import android.content.Context;
import android.widget.Toast;
import android.os.Bundle;
import android.app.Activity;

public class DevelopmentSettingsDisabledActivity extends Activity
{
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        Toast.makeText((Context)this, 2131887374, 0).show();
        this.finish();
    }
}
