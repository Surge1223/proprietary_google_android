package com.android.settings.development;

import android.os.RemoteException;
import android.util.Log;
import android.os.SystemProperties;
import android.os.UserManager;
import android.app.Fragment;
import android.text.TextUtils;
import com.android.settings.Utils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.hardware.usb.IUsbManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.hardware.usb.IUsbManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class ClearAdbKeysPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    static final String RO_ADB_SECURE_PROPERTY_KEY = "ro.adb.secure";
    private final DevelopmentSettingsDashboardFragment mFragment;
    private final IUsbManager mUsbManager;
    
    public ClearAdbKeysPreferenceController(final Context context, final DevelopmentSettingsDashboardFragment mFragment) {
        super(context);
        this.mFragment = mFragment;
        this.mUsbManager = IUsbManager$Stub.asInterface(ServiceManager.getService("usb"));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.mPreference != null && !this.isAdminUser()) {
            this.mPreference.setEnabled(false);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "clear_adb_keys";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (Utils.isMonkeyRunning()) {
            return false;
        }
        if (TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            ClearAdbKeysWarningDialog.show(this.mFragment);
            return true;
        }
        return false;
    }
    
    boolean isAdminUser() {
        return ((UserManager)this.mContext.getSystemService("user")).isAdminUser();
    }
    
    @Override
    public boolean isAvailable() {
        return SystemProperties.getBoolean("ro.adb.secure", false);
    }
    
    public void onClearAdbKeysConfirmed() {
        try {
            this.mUsbManager.clearUsbDebuggingKeys();
        }
        catch (RemoteException ex) {
            Log.e("ClearAdbPrefCtrl", "Unable to clear adb keys", (Throwable)ex);
        }
    }
    
    @Override
    protected void onDeveloperOptionsSwitchEnabled() {
        if (this.isAdminUser()) {
            this.mPreference.setEnabled(true);
        }
    }
}
