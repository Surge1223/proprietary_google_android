package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.text.TextUtils;
import android.os.SystemProperties;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class CameraLaserSensorPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    @VisibleForTesting
    static final String BUILD_TYPE = "ro.build.type";
    @VisibleForTesting
    static final int DISABLED = 2;
    @VisibleForTesting
    static final int ENABLED = 0;
    @VisibleForTesting
    static final String ENG_BUILD = "eng";
    @VisibleForTesting
    static final String PROPERTY_CAMERA_LASER_SENSOR = "persist.camera.stats.disablehaf";
    @VisibleForTesting
    static final String USERDEBUG_BUILD = "userdebug";
    @VisibleForTesting
    static final String USER_BUILD = "user";
    
    public CameraLaserSensorPreferenceController(final Context context) {
        super(context);
    }
    
    private boolean isLaserSensorEnabled() {
        return TextUtils.equals((CharSequence)Integer.toString(0), (CharSequence)SystemProperties.get("persist.camera.stats.disablehaf", Integer.toString(0)));
    }
    
    @Override
    public String getPreferenceKey() {
        return "camera_laser_sensor_switch";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034137);
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("persist.camera.stats.disablehaf", Integer.toString(2));
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        int n;
        if (o) {
            n = 0;
        }
        else {
            n = 2;
        }
        SystemProperties.set("persist.camera.stats.disablehaf", Integer.toString(n));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(this.isLaserSensorEnabled());
    }
}
