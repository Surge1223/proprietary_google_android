package com.android.settings.development;

import android.bluetooth.BluetoothCodecConfig;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class BluetoothAudioBitsPerSamplePreferenceController extends AbstractBluetoothA2dpPreferenceController
{
    public BluetoothAudioBitsPerSamplePreferenceController(final Context context, final Lifecycle lifecycle, final BluetoothA2dpConfigStore bluetoothA2dpConfigStore) {
        super(context, lifecycle, bluetoothA2dpConfigStore);
    }
    
    @Override
    protected int getCurrentA2dpSettingIndex(final BluetoothCodecConfig bluetoothCodecConfig) {
        final int bitsPerSample = bluetoothCodecConfig.getBitsPerSample();
        int n = 0;
        if (bitsPerSample != 4) {
            switch (bitsPerSample) {
                case 2: {
                    n = 2;
                    break;
                }
                case 1: {
                    n = 1;
                    break;
                }
            }
        }
        else {
            n = 3;
        }
        return n;
    }
    
    @Override
    protected int getDefaultIndex() {
        return 0;
    }
    
    @Override
    protected String[] getListSummaries() {
        return this.mContext.getResources().getStringArray(2130903063);
    }
    
    @Override
    protected String[] getListValues() {
        return this.mContext.getResources().getStringArray(2130903065);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_select_a2dp_bits_per_sample";
    }
    
    @Override
    protected void writeConfigurationValues(final Object o) {
        final int indexOfValue = this.mPreference.findIndexOfValue(o.toString());
        int bitsPerSample = 0;
        while (true) {
            switch (indexOfValue) {
                default: {}
                case 0: {
                    this.mBluetoothA2dpConfigStore.setBitsPerSample(bitsPerSample);
                }
                case 3: {
                    bitsPerSample = 4;
                    continue;
                }
                case 2: {
                    bitsPerSample = 2;
                    continue;
                }
                case 1: {
                    bitsPerSample = 1;
                    continue;
                }
            }
            break;
        }
    }
}
