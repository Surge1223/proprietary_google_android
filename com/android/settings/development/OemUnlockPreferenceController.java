package com.android.settings.development;

import com.android.settingslib.RestrictedLockUtils;
import android.content.res.Resources;
import android.content.Intent;
import android.os.UserHandle;
import android.support.v7.preference.PreferenceScreen;
import android.app.Fragment;
import android.app.Activity;
import android.content.Context;
import android.os.UserManager;
import android.telephony.TelephonyManager;
import com.android.settingslib.RestrictedSwitchPreference;
import android.service.oemlock.OemLockManager;
import com.android.settings.password.ChooseLockSettingsHelper;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class OemUnlockPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, OnActivityResultListener
{
    private final ChooseLockSettingsHelper mChooseLockSettingsHelper;
    private final DevelopmentSettingsDashboardFragment mFragment;
    private final OemLockManager mOemLockManager;
    private RestrictedSwitchPreference mPreference;
    private final TelephonyManager mTelephonyManager;
    private final UserManager mUserManager;
    
    public OemUnlockPreferenceController(final Context context, final Activity activity, final DevelopmentSettingsDashboardFragment mFragment) {
        super(context);
        this.mOemLockManager = (OemLockManager)context.getSystemService("oem_lock");
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
        this.mFragment = mFragment;
        if (activity == null && this.mFragment == null) {
            this.mChooseLockSettingsHelper = null;
        }
        else {
            this.mChooseLockSettingsHelper = new ChooseLockSettingsHelper(activity, this.mFragment);
        }
    }
    
    private boolean enableOemUnlockPreference() {
        return !this.isBootloaderUnlocked() && this.isOemUnlockAllowedByUserAndCarrier();
    }
    
    private void handleDeveloperOptionsToggled() {
        this.mPreference.setEnabled(this.enableOemUnlockPreference());
        if (this.mPreference.isEnabled()) {
            this.mPreference.checkRestrictionAndSetDisabled("no_factory_reset");
        }
    }
    
    private boolean isSimLockedDevice() {
        for (int phoneCount = this.mTelephonyManager.getPhoneCount(), i = 0; i < phoneCount; ++i) {
            if (this.mTelephonyManager.getAllowedCarriers(i).size() > 0) {
                return true;
            }
        }
        return false;
    }
    
    private void updateOemUnlockSettingDescription() {
        int n = 2131888532;
        if (this.isBootloaderUnlocked()) {
            n = 2131888528;
        }
        else if (this.isSimLockedDevice()) {
            n = 2131888531;
        }
        else if (!this.isOemUnlockAllowedByUserAndCarrier()) {
            n = 2131888530;
        }
        this.mPreference.setSummary(this.mContext.getResources().getString(n));
    }
    
    void confirmEnableOemUnlock() {
        EnableOemUnlockSettingWarningDialog.show(this.mFragment);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (RestrictedSwitchPreference)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "oem_unlock_enable";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mOemLockManager != null;
    }
    
    boolean isBootloaderUnlocked() {
        return this.mOemLockManager.isDeviceOemUnlocked();
    }
    
    boolean isOemUnlockAllowedByUserAndCarrier() {
        final UserHandle of = UserHandle.of(UserHandle.myUserId());
        return this.mOemLockManager.isOemUnlockAllowedByCarrier() && !this.mUserManager.hasBaseUserRestriction("no_factory_reset", of);
    }
    
    boolean isOemUnlockedAllowed() {
        return this.mOemLockManager.isOemUnlockAllowed();
    }
    
    @Override
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 0) {
            if (n2 == -1) {
                if (this.mPreference.isChecked()) {
                    this.confirmEnableOemUnlock();
                }
                else {
                    this.mOemLockManager.setOemUnlockAllowedByUser(false);
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchEnabled() {
        this.handleDeveloperOptionsToggled();
    }
    
    public void onOemUnlockConfirmed() {
        this.mOemLockManager.setOemUnlockAllowedByUser(true);
    }
    
    public void onOemUnlockDismissed() {
        if (this.mPreference == null) {
            return;
        }
        this.updateState(this.mPreference);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (o) {
            if (!this.showKeyguardConfirmation(this.mContext.getResources(), 0)) {
                this.confirmEnableOemUnlock();
            }
        }
        else {
            this.mOemLockManager.setOemUnlockAllowedByUser(false);
            OemLockInfoDialog.show(this.mFragment);
        }
        return true;
    }
    
    boolean showKeyguardConfirmation(final Resources resources, final int n) {
        return this.mChooseLockSettingsHelper.launchConfirmationActivity(n, resources.getString(2131888527));
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.mPreference.setChecked(this.isOemUnlockedAllowed());
        this.updateOemUnlockSettingDescription();
        this.mPreference.setDisabledByAdmin(null);
        this.mPreference.setEnabled(this.enableOemUnlockPreference());
        if (this.mPreference.isEnabled()) {
            this.mPreference.checkRestrictionAndSetDisabled("no_factory_reset");
        }
    }
}
