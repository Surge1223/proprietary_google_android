package com.android.settings.development;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class EnableOemUnlockSettingWarningDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    public static void show(final Fragment fragment) {
        final FragmentManager fragmentManager = fragment.getActivity().getFragmentManager();
        if (fragmentManager.findFragmentByTag("EnableOemUnlockDlg") == null) {
            final EnableOemUnlockSettingWarningDialog enableOemUnlockSettingWarningDialog = new EnableOemUnlockSettingWarningDialog();
            enableOemUnlockSettingWarningDialog.setTargetFragment(fragment, 0);
            enableOemUnlockSettingWarningDialog.show(fragmentManager, "EnableOemUnlockDlg");
        }
    }
    
    public int getMetricsCategory() {
        return 1220;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final OemUnlockDialogHost oemUnlockDialogHost = (OemUnlockDialogHost)this.getTargetFragment();
        if (oemUnlockDialogHost == null) {
            return;
        }
        if (n == -1) {
            oemUnlockDialogHost.onOemUnlockDialogConfirmed();
        }
        else {
            oemUnlockDialogHost.onOemUnlockDialogDismissed();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887108).setMessage(2131887107).setPositiveButton(2131887538, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        final OemUnlockDialogHost oemUnlockDialogHost = (OemUnlockDialogHost)this.getTargetFragment();
        if (oemUnlockDialogHost == null) {
            return;
        }
        oemUnlockDialogHost.onOemUnlockDialogDismissed();
    }
}
