package com.android.settings.development;

import android.content.Intent;
import android.app.ActivityManager;
import android.app.IActivityManager;
import android.os.RemoteException;
import android.content.ContentResolver;
import android.text.TextUtils;
import android.provider.Settings;
import android.support.v14.preference.SwitchPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class WaitForDebuggerPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, OnActivityResultListener
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    
    public WaitForDebuggerPreferenceController(final Context context) {
        super(context);
    }
    
    private void updateState(final Preference preference, final String s) {
        final SwitchPreference switchPreference = (SwitchPreference)preference;
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        if (Settings.Global.getInt(contentResolver, "wait_for_debugger", 0) != 0) {
            checked = true;
        }
        this.writeDebuggerAppOptions(s, checked, true);
        switchPreference.setChecked(checked);
        switchPreference.setEnabled(true ^ TextUtils.isEmpty((CharSequence)s));
    }
    
    private void writeDebuggerAppOptions(final String s, final boolean b, final boolean b2) {
        try {
            this.getActivityManagerService().setDebugApp(s, b, b2);
        }
        catch (RemoteException ex) {}
    }
    
    IActivityManager getActivityManagerService() {
        return ActivityManager.getService();
    }
    
    @Override
    public String getPreferenceKey() {
        return "wait_for_debugger";
    }
    
    @Override
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1 && n2 == -1) {
            this.updateState(this.mPreference, intent.getAction());
            return true;
        }
        return false;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeDebuggerAppOptions(null, false, false);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeDebuggerAppOptions(Settings.Global.getString(this.mContext.getContentResolver(), "debug_app"), (boolean)o, true);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateState(this.mPreference, Settings.Global.getString(this.mContext.getContentResolver(), "debug_app"));
    }
}
