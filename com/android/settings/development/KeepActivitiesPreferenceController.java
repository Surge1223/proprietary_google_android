package com.android.settings.development;

import android.content.ContentResolver;
import android.provider.Settings;
import android.support.v14.preference.SwitchPreference;
import android.app.ActivityManager;
import android.support.v7.preference.PreferenceScreen;
import android.os.RemoteException;
import android.content.Context;
import android.app.IActivityManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class KeepActivitiesPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SETTING_VALUE_OFF = 0;
    private IActivityManager mActivityManager;
    
    public KeepActivitiesPreferenceController(final Context context) {
        super(context);
    }
    
    private void writeImmediatelyDestroyActivitiesOptions(final boolean alwaysFinish) {
        try {
            this.mActivityManager.setAlwaysFinish(alwaysFinish);
        }
        catch (RemoteException ex) {}
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mActivityManager = this.getActivityManager();
    }
    
    IActivityManager getActivityManager() {
        return ActivityManager.getService();
    }
    
    @Override
    public String getPreferenceKey() {
        return "immediately_destroy_activities";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeImmediatelyDestroyActivitiesOptions(false);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeImmediatelyDestroyActivitiesOptions((boolean)o);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Global.getInt(contentResolver, "always_finish_activities", 0);
        final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
        if (int1 != 0) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
