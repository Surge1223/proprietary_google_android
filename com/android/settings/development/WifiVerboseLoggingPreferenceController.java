package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.content.Context;
import android.net.wifi.WifiManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class WifiVerboseLoggingPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    private final WifiManager mWifiManager;
    
    public WifiVerboseLoggingPreferenceController(final Context context) {
        super(context);
        this.mWifiManager = (WifiManager)context.getSystemService("wifi");
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_verbose_logging";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.mWifiManager.enableVerboseLogging(0);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mWifiManager.enableVerboseLogging((int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(this.mWifiManager.getVerboseLoggingLevel() > 0);
    }
}
