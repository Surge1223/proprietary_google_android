package com.android.settings.development;

import android.app.Fragment;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.AbstractEnableAdbPreferenceController;

public class AdbPreferenceController extends AbstractEnableAdbPreferenceController implements PreferenceControllerMixin
{
    private final DevelopmentSettingsDashboardFragment mFragment;
    
    public AdbPreferenceController(final Context context, final DevelopmentSettingsDashboardFragment mFragment) {
        super(context);
        this.mFragment = mFragment;
    }
    
    public void onAdbDialogConfirmed() {
        this.writeAdbSetting(true);
    }
    
    public void onAdbDialogDismissed() {
        this.updateState(this.mPreference);
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeAdbSetting(false);
        this.mPreference.setChecked(false);
    }
    
    @Override
    public void showConfirmationDialog(final Preference preference) {
        EnableAdbWarningDialog.show(this.mFragment);
    }
}
