package com.android.settings.development;

import android.support.v7.preference.Preference;
import com.android.settings.applications.ProcessStatsBase;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.utils.ThreadUtils;
import android.text.format.Formatter;
import android.content.Context;
import com.android.settings.applications.ProcStatsData;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public final class _$$Lambda$MemoryUsagePreferenceController$jVfwyLcntt7OQNk4ZzyeXShgglc implements Runnable
{
    @Override
    public final void run() {
        MemoryUsagePreferenceController.lambda$updateState$0(this.f$0, this.f$1, this.f$2);
    }
}
