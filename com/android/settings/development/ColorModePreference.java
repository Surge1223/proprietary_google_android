package com.android.settings.development;

import android.os.Handler;
import android.os.Looper;
import android.content.res.Resources;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.view.Display;
import java.util.List;
import android.hardware.display.DisplayManager$DisplayListener;
import android.support.v14.preference.SwitchPreference;

public class ColorModePreference extends SwitchPreference implements DisplayManager$DisplayListener
{
    private int mCurrentIndex;
    private List<ColorModeDescription> mDescriptions;
    private Display mDisplay;
    private DisplayManager mDisplayManager;
    
    public ColorModePreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mDisplayManager = (DisplayManager)this.getContext().getSystemService((Class)DisplayManager.class);
    }
    
    public static List<ColorModeDescription> getColorModeDescriptions(final Context context) {
        final ArrayList<ColorModeDescription> list = new ArrayList<ColorModeDescription>();
        final Resources resources = context.getResources();
        final int[] intArray = resources.getIntArray(2130903098);
        final String[] stringArray = resources.getStringArray(2130903099);
        final String[] stringArray2 = resources.getStringArray(2130903097);
        for (int i = 0; i < intArray.length; ++i) {
            if (intArray[i] != -1 && i != 1) {
                final ColorModeDescription colorModeDescription = new ColorModeDescription();
                colorModeDescription.colorMode = intArray[i];
                colorModeDescription.title = stringArray[i];
                colorModeDescription.summary = stringArray2[i];
                list.add(colorModeDescription);
            }
        }
        return list;
    }
    
    public void onDisplayAdded(final int n) {
        if (n == 0) {
            this.updateCurrentAndSupported();
        }
    }
    
    public void onDisplayChanged(final int n) {
        if (n == 0) {
            this.updateCurrentAndSupported();
        }
    }
    
    public void onDisplayRemoved(final int n) {
    }
    
    protected boolean persistBoolean(final boolean b) {
        if (this.mDescriptions.size() == 2) {
            final ColorModeDescription colorModeDescription = this.mDescriptions.get(b ? 1 : 0);
            this.mDisplay.requestColorMode(colorModeDescription.colorMode);
            this.mCurrentIndex = this.mDescriptions.indexOf(colorModeDescription);
        }
        return true;
    }
    
    public void startListening() {
        this.mDisplayManager.registerDisplayListener((DisplayManager$DisplayListener)this, new Handler(Looper.getMainLooper()));
    }
    
    public void stopListening() {
        this.mDisplayManager.unregisterDisplayListener((DisplayManager$DisplayListener)this);
    }
    
    public void updateCurrentAndSupported() {
        final DisplayManager mDisplayManager = this.mDisplayManager;
        boolean checked = false;
        this.mDisplay = mDisplayManager.getDisplay(0);
        this.mDescriptions = getColorModeDescriptions(this.getContext());
        final int colorMode = this.mDisplay.getColorMode();
        this.mCurrentIndex = -1;
        for (int i = 0; i < this.mDescriptions.size(); ++i) {
            if (this.mDescriptions.get(i).colorMode == colorMode) {
                this.mCurrentIndex = i;
                break;
            }
        }
        if (this.mCurrentIndex == 1) {
            checked = true;
        }
        this.setChecked(checked);
    }
    
    private static class ColorModeDescription
    {
        private int colorMode;
        private String summary;
        private String title;
    }
}
