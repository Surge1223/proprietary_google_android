package com.android.settings.development;

import android.os.SystemProperties;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BluetoothMaxConnectedAudioDevicesPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String MAX_CONNECTED_AUDIO_DEVICES_PROPERTY = "persist.bluetooth.maxconnectedaudiodevices";
    private final int mDefaultMaxConnectedAudioDevices;
    
    public BluetoothMaxConnectedAudioDevicesPreferenceController(final Context context) {
        super(context);
        this.mDefaultMaxConnectedAudioDevices = this.mContext.getResources().getInteger(17694740);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final ListPreference listPreference = (ListPreference)this.mPreference;
        final CharSequence[] entries = listPreference.getEntries();
        entries[0] = String.format(entries[0].toString(), this.mDefaultMaxConnectedAudioDevices);
        listPreference.setEntries(entries);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_max_connected_audio_devices";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("persist.bluetooth.maxconnectedaudiodevices", "");
        this.updateState(this.mPreference);
    }
    
    @Override
    protected void onDeveloperOptionsSwitchEnabled() {
        super.onDeveloperOptionsSwitchEnabled();
        this.updateState(this.mPreference);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        String string;
        if (((ListPreference)preference).findIndexOfValue(string = o.toString()) <= 0) {
            string = "";
        }
        SystemProperties.set("persist.bluetooth.maxconnectedaudiodevices", string);
        this.updateState(preference);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ListPreference listPreference = (ListPreference)preference;
        final CharSequence[] entries = listPreference.getEntries();
        final String value = SystemProperties.get("persist.bluetooth.maxconnectedaudiodevices");
        int indexOfValue = 0;
        if (!value.isEmpty() && (indexOfValue = listPreference.findIndexOfValue(value)) < 0) {
            SystemProperties.set("persist.bluetooth.maxconnectedaudiodevices", "");
            indexOfValue = 0;
        }
        listPreference.setValueIndex(indexOfValue);
        listPreference.setSummary(entries[indexOfValue]);
    }
}
