package com.android.settings.development;

import android.text.TextUtils;
import android.support.v7.preference.ListPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BluetoothAvrcpVersionPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String BLUETOOTH_AVRCP_VERSION_PROPERTY = "persist.bluetooth.avrcpversion";
    private final String[] mListSummaries;
    private final String[] mListValues;
    
    public BluetoothAvrcpVersionPreferenceController(final Context context) {
        super(context);
        this.mListValues = context.getResources().getStringArray(2130903079);
        this.mListSummaries = context.getResources().getStringArray(2130903080);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_select_avrcp_version";
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        SystemProperties.set("persist.bluetooth.avrcpversion", o.toString());
        this.updateState(this.mPreference);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ListPreference listPreference = (ListPreference)preference;
        final String value = SystemProperties.get("persist.bluetooth.avrcpversion");
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= this.mListValues.length) {
                break;
            }
            if (TextUtils.equals((CharSequence)value, (CharSequence)this.mListValues[n2])) {
                n3 = n2;
                break;
            }
            ++n2;
        }
        listPreference.setValue(this.mListValues[n3]);
        listPreference.setSummary(this.mListSummaries[n3]);
    }
}
