package com.android.settings.development;

import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import java.util.List;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.content.pm.PackageManager;
import java.util.Comparator;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;
import com.android.settings.wrapper.OverlayManagerWrapper;
import java.util.function.ToIntFunction;

public final class _$$Lambda$EmulateDisplayCutoutPreferenceController$M0LG4OIyq1gHZ_QtkGbuUJde4QM implements ToIntFunction
{
    @Override
    public final int applyAsInt(final Object o) {
        return ((OverlayManagerWrapper.OverlayInfo)o).priority;
    }
}
