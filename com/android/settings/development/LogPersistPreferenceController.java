package com.android.settings.development;

import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.AbstractLogpersistPreferenceController;

public class LogPersistPreferenceController extends AbstractLogpersistPreferenceController implements PreferenceControllerMixin
{
    private final DevelopmentSettingsDashboardFragment mFragment;
    
    public LogPersistPreferenceController(final Context context, final DevelopmentSettingsDashboardFragment mFragment, final Lifecycle lifecycle) {
        super(context, lifecycle);
        this.mFragment = mFragment;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeLogpersistOption(null, true);
    }
    
    public void onDisableLogPersistDialogConfirmed() {
        this.setLogpersistOff(true);
        this.updateLogpersistValues();
    }
    
    public void onDisableLogPersistDialogRejected() {
        this.updateLogpersistValues();
    }
    
    @Override
    public void showConfirmationDialog(final Preference preference) {
        DisableLogPersistWarningDialog.show(this.mFragment);
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateLogpersistValues();
    }
}
