package com.android.settings.development;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class DisableAutomaticUpdatesPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int DISABLE_UPDATES_SETTING = 1;
    static final int ENABLE_UPDATES_SETTING = 0;
    
    public DisableAutomaticUpdatesPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "ota_disable_automatic_update";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        Settings.Global.putInt(this.mContext.getContentResolver(), "ota_disable_automatic_update", 1);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "ota_disable_automatic_update", (int)(((boolean)o ^ true) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Global.getInt(contentResolver, "ota_disable_automatic_update", 0);
        final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
        if (int1 != 1) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
