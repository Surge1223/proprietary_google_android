package com.android.settings.development;

import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import java.util.List;
import android.content.Context;
import java.util.function.ToIntFunction;
import android.support.v7.preference.ListPreference;
import android.content.pm.PackageManager;
import com.android.settings.wrapper.OverlayManagerWrapper;
import java.util.Comparator;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class EmulateDisplayCutoutPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private static final Comparator<OverlayManagerWrapper.OverlayInfo> OVERLAY_INFO_COMPARATOR;
    private final boolean mAvailable;
    private final OverlayManagerWrapper mOverlayManager;
    private PackageManager mPackageManager;
    private ListPreference mPreference;
    
    static {
        OVERLAY_INFO_COMPARATOR = Comparator.comparingInt((ToIntFunction<? super OverlayManagerWrapper.OverlayInfo>)_$$Lambda$EmulateDisplayCutoutPreferenceController$M0LG4OIyq1gHZ_QtkGbuUJde4QM.INSTANCE);
    }
    
    public EmulateDisplayCutoutPreferenceController(final Context context) {
        this(context, context.getPackageManager(), new OverlayManagerWrapper());
    }
    
    EmulateDisplayCutoutPreferenceController(final Context context, final PackageManager mPackageManager, final OverlayManagerWrapper mOverlayManager) {
        super(context);
        this.mOverlayManager = mOverlayManager;
        this.mPackageManager = mPackageManager;
        this.mAvailable = (mOverlayManager != null && this.getOverlayInfos().length > 0);
    }
    
    private OverlayManagerWrapper.OverlayInfo[] getOverlayInfos() {
        final List<OverlayManagerWrapper.OverlayInfo> overlayInfosForTarget = this.mOverlayManager.getOverlayInfosForTarget("android", 0);
        for (int i = overlayInfosForTarget.size() - 1; i >= 0; --i) {
            if (!"com.android.internal.display_cutout_emulation".equals(overlayInfosForTarget.get(i).category)) {
                overlayInfosForTarget.remove(i);
            }
        }
        overlayInfosForTarget.sort((Comparator<? super OverlayManagerWrapper.OverlayInfo>)EmulateDisplayCutoutPreferenceController.OVERLAY_INFO_COMPARATOR);
        return overlayInfosForTarget.toArray(new OverlayManagerWrapper.OverlayInfo[overlayInfosForTarget.size()]);
    }
    
    private boolean setEmulationOverlay(final String s) {
        final OverlayManagerWrapper.OverlayInfo[] overlayInfos = this.getOverlayInfos();
        final int length = overlayInfos.length;
        String packageName = null;
        for (final OverlayManagerWrapper.OverlayInfo overlayInfo : overlayInfos) {
            if (overlayInfo.isEnabled()) {
                packageName = overlayInfo.packageName;
            }
        }
        if ((TextUtils.isEmpty((CharSequence)s) && TextUtils.isEmpty((CharSequence)packageName)) || TextUtils.equals((CharSequence)s, (CharSequence)packageName)) {
            return true;
        }
        boolean b;
        if (TextUtils.isEmpty((CharSequence)s)) {
            b = this.mOverlayManager.setEnabled(packageName, false, 0);
        }
        else {
            b = this.mOverlayManager.setEnabledExclusiveInCategory(s, 0);
        }
        this.updateState(this.mPreference);
        return b;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.setPreference((ListPreference)preferenceScreen.findPreference(this.getPreferenceKey()));
    }
    
    @Override
    public String getPreferenceKey() {
        return "display_cutout_emulation";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mAvailable;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.setEmulationOverlay("");
        this.updateState(this.mPreference);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return this.setEmulationOverlay((String)o);
    }
    
    void setPreference(final ListPreference mPreference) {
        this.mPreference = mPreference;
    }
    
    @Override
    public void updateState(final Preference p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   com/android/settings/development/EmulateDisplayCutoutPreferenceController.getOverlayInfos:()[Lcom/android/settings/wrapper/OverlayManagerWrapper$OverlayInfo;
        //     4: astore_2       
        //     5: aload_2        
        //     6: arraylength    
        //     7: istore_3       
        //     8: iconst_1       
        //     9: istore          4
        //    11: iload_3        
        //    12: iconst_1       
        //    13: iadd           
        //    14: anewarray       Ljava/lang/CharSequence;
        //    17: astore          5
        //    19: aload           5
        //    21: arraylength    
        //    22: anewarray       Ljava/lang/CharSequence;
        //    25: astore_1       
        //    26: aload           5
        //    28: iconst_0       
        //    29: ldc             ""
        //    31: aastore        
        //    32: aload_1        
        //    33: iconst_0       
        //    34: aload_0        
        //    35: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mContext:Landroid/content/Context;
        //    38: ldc             2131887442
        //    40: invokevirtual   android/content/Context.getString:(I)Ljava/lang/String;
        //    43: aastore        
        //    44: iconst_0       
        //    45: istore_3       
        //    46: iconst_0       
        //    47: istore          6
        //    49: iload           6
        //    51: aload_2        
        //    52: arraylength    
        //    53: if_icmpge       93
        //    56: aload_2        
        //    57: iload           6
        //    59: aaload         
        //    60: astore          7
        //    62: aload           5
        //    64: iload           6
        //    66: iconst_1       
        //    67: iadd           
        //    68: aload           7
        //    70: getfield        com/android/settings/wrapper/OverlayManagerWrapper$OverlayInfo.packageName:Ljava/lang/String;
        //    73: aastore        
        //    74: aload           7
        //    76: invokevirtual   com/android/settings/wrapper/OverlayManagerWrapper$OverlayInfo.isEnabled:()Z
        //    79: ifeq            87
        //    82: iload           6
        //    84: iconst_1       
        //    85: iadd           
        //    86: istore_3       
        //    87: iinc            6, 1
        //    90: goto            49
        //    93: iload           4
        //    95: istore          6
        //    97: iload           6
        //    99: aload           5
        //   101: arraylength    
        //   102: if_icmpge       153
        //   105: aload_1        
        //   106: iload           6
        //   108: aload_0        
        //   109: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mPackageManager:Landroid/content/pm/PackageManager;
        //   112: aload           5
        //   114: iload           6
        //   116: aaload         
        //   117: invokeinterface java/lang/CharSequence.toString:()Ljava/lang/String;
        //   122: iconst_0       
        //   123: invokevirtual   android/content/pm/PackageManager.getApplicationInfo:(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //   126: aload_0        
        //   127: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mPackageManager:Landroid/content/pm/PackageManager;
        //   130: invokevirtual   android/content/pm/ApplicationInfo.loadLabel:(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
        //   133: aastore        
        //   134: goto            147
        //   137: astore_2       
        //   138: aload_1        
        //   139: iload           6
        //   141: aload           5
        //   143: iload           6
        //   145: aaload         
        //   146: aastore        
        //   147: iinc            6, 1
        //   150: goto            97
        //   153: aload_0        
        //   154: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mPreference:Landroid/support/v7/preference/ListPreference;
        //   157: aload_1        
        //   158: invokevirtual   android/support/v7/preference/ListPreference.setEntries:([Ljava/lang/CharSequence;)V
        //   161: aload_0        
        //   162: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mPreference:Landroid/support/v7/preference/ListPreference;
        //   165: aload           5
        //   167: invokevirtual   android/support/v7/preference/ListPreference.setEntryValues:([Ljava/lang/CharSequence;)V
        //   170: aload_0        
        //   171: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mPreference:Landroid/support/v7/preference/ListPreference;
        //   174: iload_3        
        //   175: invokevirtual   android/support/v7/preference/ListPreference.setValueIndex:(I)V
        //   178: aload_0        
        //   179: getfield        com/android/settings/development/EmulateDisplayCutoutPreferenceController.mPreference:Landroid/support/v7/preference/ListPreference;
        //   182: aload_1        
        //   183: iload_3        
        //   184: aaload         
        //   185: invokevirtual   android/support/v7/preference/ListPreference.setSummary:(Ljava/lang/CharSequence;)V
        //   188: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                     
        //  -----  -----  -----  -----  ---------------------------------------------------------
        //  105    134    137    147    Landroid.content.pm.PackageManager;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Thread.java:745)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
