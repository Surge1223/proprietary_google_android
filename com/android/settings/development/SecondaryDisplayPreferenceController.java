package com.android.settings.development;

import android.support.v7.preference.ListPreference;
import android.text.TextUtils;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class SecondaryDisplayPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final String[] mListSummaries;
    private final String[] mListValues;
    
    public SecondaryDisplayPreferenceController(final Context context) {
        super(context);
        this.mListValues = context.getResources().getStringArray(2130903134);
        this.mListSummaries = context.getResources().getStringArray(2130903133);
    }
    
    private void updateSecondaryDisplayDevicesOptions() {
        final String string = Settings.Global.getString(this.mContext.getContentResolver(), "overlay_display_devices");
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= this.mListValues.length) {
                break;
            }
            if (TextUtils.equals((CharSequence)string, (CharSequence)this.mListValues[n2])) {
                n3 = n2;
                break;
            }
            ++n2;
        }
        final ListPreference listPreference = (ListPreference)this.mPreference;
        listPreference.setValue(this.mListValues[n3]);
        listPreference.setSummary(this.mListSummaries[n3]);
    }
    
    private void writeSecondaryDisplayDevicesOption(final String s) {
        Settings.Global.putString(this.mContext.getContentResolver(), "overlay_display_devices", s);
        this.updateSecondaryDisplayDevicesOptions();
    }
    
    @Override
    public String getPreferenceKey() {
        return "overlay_display_devices";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeSecondaryDisplayDevicesOption(null);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeSecondaryDisplayDevicesOption(o.toString());
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateSecondaryDisplayDevicesOptions();
    }
}
