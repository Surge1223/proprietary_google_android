package com.android.settings.development;

import com.android.settingslib.development.SystemPropPoker;
import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class ForceMSAAPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String MSAA_PROPERTY = "debug.egl.force_msaa";
    
    public ForceMSAAPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "force_msaa";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("debug.egl.force_msaa", Boolean.toString(false));
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        String s;
        if (o) {
            s = Boolean.toString(true);
        }
        else {
            s = Boolean.toString(false);
        }
        SystemProperties.set("debug.egl.force_msaa", s);
        SystemPropPoker.getInstance().poke();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("debug.egl.force_msaa", false));
    }
}
