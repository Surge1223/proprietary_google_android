package com.android.settings.development;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class EnableAdbWarningDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    public static void show(final Fragment fragment) {
        final FragmentManager fragmentManager = fragment.getActivity().getFragmentManager();
        if (fragmentManager.findFragmentByTag("EnableAdbDialog") == null) {
            final EnableAdbWarningDialog enableAdbWarningDialog = new EnableAdbWarningDialog();
            enableAdbWarningDialog.setTargetFragment(fragment, 0);
            enableAdbWarningDialog.show(fragmentManager, "EnableAdbDialog");
        }
    }
    
    public int getMetricsCategory() {
        return 1222;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final AdbDialogHost adbDialogHost = (AdbDialogHost)this.getTargetFragment();
        if (adbDialogHost == null) {
            return;
        }
        if (n == -1) {
            adbDialogHost.onEnableAdbDialogConfirmed();
        }
        else {
            adbDialogHost.onEnableAdbDialogDismissed();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131886252).setMessage(2131886251).setPositiveButton(17039379, (DialogInterface$OnClickListener)this).setNegativeButton(17039369, (DialogInterface$OnClickListener)this).create();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        final AdbDialogHost adbDialogHost = (AdbDialogHost)this.getTargetFragment();
        if (adbDialogHost == null) {
            return;
        }
        adbDialogHost.onEnableAdbDialogDismissed();
    }
}
