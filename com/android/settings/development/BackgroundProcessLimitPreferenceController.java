package com.android.settings.development;

import android.app.ActivityManager;
import android.app.IActivityManager;
import android.os.RemoteException;
import android.support.v7.preference.ListPreference;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BackgroundProcessLimitPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final String[] mListSummaries;
    private final String[] mListValues;
    
    public BackgroundProcessLimitPreferenceController(final Context context) {
        super(context);
        this.mListValues = context.getResources().getStringArray(2130903052);
        this.mListSummaries = context.getResources().getStringArray(2130903051);
    }
    
    private void updateAppProcessLimitOptions() {
        try {
            final int processLimit = this.getActivityManagerService().getProcessLimit();
            final int n = 0;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= this.mListValues.length) {
                    break;
                }
                if (Integer.parseInt(this.mListValues[n2]) >= processLimit) {
                    n3 = n2;
                    break;
                }
                ++n2;
            }
            final ListPreference listPreference = (ListPreference)this.mPreference;
            listPreference.setValue(this.mListValues[n3]);
            listPreference.setSummary(this.mListSummaries[n3]);
        }
        catch (RemoteException ex) {}
    }
    
    private void writeAppProcessLimitOptions(final Object o) {
        int int1 = 0;
        Label_0021: {
            if (o != null) {
                try {
                    int1 = Integer.parseInt(o.toString());
                    break Label_0021;
                }
                catch (RemoteException ex) {
                    return;
                }
            }
            int1 = -1;
        }
        this.getActivityManagerService().setProcessLimit(int1);
        this.updateAppProcessLimitOptions();
    }
    
    IActivityManager getActivityManagerService() {
        return ActivityManager.getService();
    }
    
    @Override
    public String getPreferenceKey() {
        return "app_process_limit";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeAppProcessLimitOptions(null);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeAppProcessLimitOptions(o);
        this.updateAppProcessLimitOptions();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateAppProcessLimitOptions();
    }
}
