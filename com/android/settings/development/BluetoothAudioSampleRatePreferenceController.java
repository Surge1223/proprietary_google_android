package com.android.settings.development;

import android.bluetooth.BluetoothCodecConfig;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class BluetoothAudioSampleRatePreferenceController extends AbstractBluetoothA2dpPreferenceController
{
    public BluetoothAudioSampleRatePreferenceController(final Context context, final Lifecycle lifecycle, final BluetoothA2dpConfigStore bluetoothA2dpConfigStore) {
        super(context, lifecycle, bluetoothA2dpConfigStore);
    }
    
    @Override
    protected int getCurrentA2dpSettingIndex(final BluetoothCodecConfig bluetoothCodecConfig) {
        final int sampleRate = bluetoothCodecConfig.getSampleRate();
        int n = 0;
        if (sampleRate != 4) {
            if (sampleRate != 8) {
                switch (sampleRate) {
                    case 2: {
                        n = 2;
                        break;
                    }
                    case 1: {
                        n = 1;
                        break;
                    }
                }
            }
            else {
                n = 4;
            }
        }
        else {
            n = 3;
        }
        return n;
    }
    
    @Override
    protected int getDefaultIndex() {
        return 0;
    }
    
    @Override
    protected String[] getListSummaries() {
        return this.mContext.getResources().getStringArray(2130903072);
    }
    
    @Override
    protected String[] getListValues() {
        return this.mContext.getResources().getStringArray(2130903074);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_select_a2dp_sample_rate";
    }
    
    @Override
    protected void writeConfigurationValues(final Object o) {
        final int indexOfValue = this.mPreference.findIndexOfValue(o.toString());
        int sampleRate = 0;
        switch (indexOfValue) {
            case 4: {
                sampleRate = 8;
                break;
            }
            case 3: {
                sampleRate = 4;
                break;
            }
            case 2: {
                sampleRate = 2;
                break;
            }
            case 1: {
                sampleRate = 1;
                break;
            }
            case 0: {
                sampleRate = 0;
                break;
            }
        }
        this.mBluetoothA2dpConfigStore.setSampleRate(sampleRate);
    }
}
