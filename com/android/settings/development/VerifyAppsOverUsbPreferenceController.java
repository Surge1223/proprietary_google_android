package com.android.settings.development;

import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedSwitchPreference;
import android.content.ContentResolver;
import android.content.Intent;
import android.provider.Settings;
import android.content.Context;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class VerifyAppsOverUsbPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, AdbOnChangeListener
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    private final PackageManagerWrapper mPackageManager;
    private final RestrictedLockUtilsDelegate mRestrictedLockUtils;
    
    public VerifyAppsOverUsbPreferenceController(final Context context) {
        super(context);
        this.mRestrictedLockUtils = new RestrictedLockUtilsDelegate();
        this.mPackageManager = new PackageManagerWrapper(context.getPackageManager());
    }
    
    private boolean shouldBeEnabled() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        if (Settings.Global.getInt(contentResolver, "adb_enabled", 0) == 0) {
            return false;
        }
        if (Settings.Global.getInt(contentResolver, "package_verifier_enable", 1) == 0) {
            return false;
        }
        final Intent intent = new Intent("android.intent.action.PACKAGE_NEEDS_VERIFICATION");
        intent.setType("application/vnd.android.package-archive");
        intent.addFlags(1);
        return this.mPackageManager.queryBroadcastReceivers(intent, 0).size() != 0;
    }
    
    @Override
    public String getPreferenceKey() {
        return "verify_apps_over_usb";
    }
    
    @Override
    public boolean isAvailable() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Global.getInt(contentResolver, "verifier_setting_visible", 1) <= 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void onAdbSettingChanged() {
        if (this.isAvailable()) {
            this.updateState(this.mPreference);
        }
    }
    
    @Override
    protected void onDeveloperOptionsSwitchEnabled() {
        super.onDeveloperOptionsSwitchEnabled();
        this.updateState(this.mPreference);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "verifier_verify_adb_installs", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final RestrictedSwitchPreference restrictedSwitchPreference = (RestrictedSwitchPreference)preference;
        final boolean shouldBeEnabled = this.shouldBeEnabled();
        boolean checked = false;
        if (!shouldBeEnabled) {
            restrictedSwitchPreference.setChecked(false);
            restrictedSwitchPreference.setDisabledByAdmin(null);
            restrictedSwitchPreference.setEnabled(false);
            return;
        }
        final RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced = this.mRestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "ensure_verify_apps", UserHandle.myUserId());
        if (checkIfRestrictionEnforced != null) {
            restrictedSwitchPreference.setChecked(true);
            restrictedSwitchPreference.setDisabledByAdmin(checkIfRestrictionEnforced);
            return;
        }
        restrictedSwitchPreference.setEnabled(true);
        if (Settings.Global.getInt(this.mContext.getContentResolver(), "verifier_verify_adb_installs", 1) != 0) {
            checked = true;
        }
        restrictedSwitchPreference.setChecked(checked);
    }
    
    class RestrictedLockUtilsDelegate
    {
        public RestrictedLockUtils.EnforcedAdmin checkIfRestrictionEnforced(final Context context, final String s, final int n) {
            return RestrictedLockUtils.checkIfRestrictionEnforced(context, s, n);
        }
    }
}
