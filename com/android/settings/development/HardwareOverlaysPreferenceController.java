package com.android.settings.development;

import android.os.RemoteException;
import android.os.Parcel;
import android.support.v14.preference.SwitchPreference;
import android.os.ServiceManager;
import android.content.Context;
import android.os.IBinder;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class HardwareOverlaysPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SURFACE_FLINGER_READ_CODE = 1010;
    private final IBinder mSurfaceFlinger;
    
    public HardwareOverlaysPreferenceController(final Context context) {
        super(context);
        this.mSurfaceFlinger = ServiceManager.getService("SurfaceFlinger");
    }
    
    @Override
    public String getPreferenceKey() {
        return "disable_overlays";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
        if (switchPreference.isChecked()) {
            this.writeHardwareOverlaysSetting(false);
            switchPreference.setChecked(false);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeHardwareOverlaysSetting((boolean)o);
        return true;
    }
    
    void updateHardwareOverlaysSetting() {
        if (this.mSurfaceFlinger == null) {
            return;
        }
        try {
            final Parcel obtain = Parcel.obtain();
            final Parcel obtain2 = Parcel.obtain();
            obtain.writeInterfaceToken("android.ui.ISurfaceComposer");
            final IBinder mSurfaceFlinger = this.mSurfaceFlinger;
            boolean checked = false;
            mSurfaceFlinger.transact(1010, obtain, obtain2, 0);
            obtain2.readInt();
            obtain2.readInt();
            obtain2.readInt();
            obtain2.readInt();
            final int int1 = obtain2.readInt();
            final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
            if (int1 != 0) {
                checked = true;
            }
            switchPreference.setChecked(checked);
            obtain2.recycle();
            obtain.recycle();
        }
        catch (RemoteException ex) {}
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateHardwareOverlaysSetting();
    }
    
    void writeHardwareOverlaysSetting(final boolean b) {
        if (this.mSurfaceFlinger == null) {
            return;
        }
        try {
            final Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("android.ui.ISurfaceComposer");
            obtain.writeInt((int)(b ? 1 : 0));
            this.mSurfaceFlinger.transact(1008, obtain, (Parcel)null, 0);
            obtain.recycle();
        }
        catch (RemoteException ex) {}
        this.updateHardwareOverlaysSetting();
    }
}
