package com.android.settings.development;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class EnableGpuDebugLayersPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    
    public EnableGpuDebugLayersPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "enable_gpu_debug_layers";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        Settings.Global.putInt(this.mContext.getContentResolver(), "enable_gpu_debug_layers", 0);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "enable_gpu_debug_layers", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Global.getInt(contentResolver, "enable_gpu_debug_layers", 0);
        final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
        if (int1 != 0) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
