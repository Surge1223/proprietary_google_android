package com.android.settings.development;

import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class DefaultLaunchPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    private final String mPreferenceKey;
    
    public DefaultLaunchPreferenceController(final Context context, final String mPreferenceKey) {
        super(context);
        this.mPreferenceKey = mPreferenceKey;
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPreferenceKey;
    }
}
