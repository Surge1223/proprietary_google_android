package com.android.settings.development.featureflags;

import java.util.Iterator;
import java.util.Map;
import android.support.v7.preference.Preference;
import android.util.FeatureFlagUtils;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class FeatureFlagsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart
{
    private PreferenceScreen mScreen;
    
    public FeatureFlagsPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        super.displayPreference(mScreen);
        this.mScreen = mScreen;
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onStart() {
        if (this.mScreen == null) {
            return;
        }
        final Map allFeatureFlags = FeatureFlagUtils.getAllFeatureFlags();
        if (allFeatureFlags == null) {
            return;
        }
        this.mScreen.removeAll();
        final Context context = this.mScreen.getContext();
        final Iterator<String> iterator = allFeatureFlags.keySet().iterator();
        while (iterator.hasNext()) {
            this.mScreen.addPreference(new FeatureFlagPreference(context, iterator.next()));
        }
    }
}
