package com.android.settings.development.featureflags;

import com.android.settingslib.core.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.dashboard.DashboardFragment;

public class FeatureFlagsDashboard extends DashboardFragment
{
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<FeatureFlagsPreferenceController> list = (ArrayList<FeatureFlagsPreferenceController>)new ArrayList<AbstractPreferenceController>();
        final Lifecycle lifecycle = this.getLifecycle();
        final FeatureFlagFooterPreferenceController featureFlagFooterPreferenceController = new FeatureFlagFooterPreferenceController(context);
        list.add(new FeatureFlagsPreferenceController(context, lifecycle));
        list.add((FeatureFlagsPreferenceController)featureFlagFooterPreferenceController);
        lifecycle.addObserver(featureFlagFooterPreferenceController);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getHelpResource() {
        return 0;
    }
    
    @Override
    protected String getLogTag() {
        return "FeatureFlagsDashboard";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1217;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082774;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(FeatureFlagFooterPreferenceController.class).setFooterMixin(this.mFooterPreferenceMixin);
    }
}
