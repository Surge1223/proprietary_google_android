package com.android.settings.development.featureflags;

import android.util.FeatureFlagUtils;
import android.text.TextUtils;
import android.os.SystemProperties;
import android.content.Context;
import java.util.HashSet;

public class FeatureFlagPersistent
{
    private static final HashSet<String> PERSISTENT_FLAGS;
    
    static {
        (PERSISTENT_FLAGS = new HashSet<String>()).add("settings_bluetooth_hearing_aid");
    }
    
    static HashSet<String> getAllPersistentFlags() {
        return FeatureFlagPersistent.PERSISTENT_FLAGS;
    }
    
    public static boolean isEnabled(final Context context, final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("persist.sys.fflag.override.");
        sb.append(s);
        final String value = SystemProperties.get(sb.toString());
        if (!TextUtils.isEmpty((CharSequence)value)) {
            return Boolean.parseBoolean(value);
        }
        return FeatureFlagUtils.isEnabled(context, s);
    }
    
    public static boolean isPersistent(final String s) {
        return FeatureFlagPersistent.PERSISTENT_FLAGS.contains(s);
    }
    
    public static void setEnabled(final Context context, final String s, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("persist.sys.fflag.override.");
        sb.append(s);
        final String string = sb.toString();
        String s2;
        if (b) {
            s2 = "true";
        }
        else {
            s2 = "false";
        }
        SystemProperties.set(string, s2);
        FeatureFlagUtils.setEnabled(context, s, b);
    }
}
