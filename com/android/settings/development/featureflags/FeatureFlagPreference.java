package com.android.settings.development.featureflags;

import android.util.FeatureFlagUtils;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;

public class FeatureFlagPreference extends SwitchPreference
{
    private final boolean mIsPersistent;
    private final String mKey;
    
    public FeatureFlagPreference(final Context context, final String s) {
        super(context);
        this.setKey(this.mKey = s);
        this.setTitle(s);
        this.mIsPersistent = FeatureFlagPersistent.isPersistent(s);
        boolean checkedInternal;
        if (this.mIsPersistent) {
            checkedInternal = FeatureFlagPersistent.isEnabled(context, s);
        }
        else {
            checkedInternal = FeatureFlagUtils.isEnabled(context, s);
        }
        this.setCheckedInternal(checkedInternal);
    }
    
    private void setCheckedInternal(final boolean checked) {
        super.setChecked(checked);
        this.setSummary(Boolean.toString(checked));
    }
    
    @Override
    public void setChecked(final boolean checkedInternal) {
        this.setCheckedInternal(checkedInternal);
        if (this.mIsPersistent) {
            FeatureFlagPersistent.setEnabled(this.getContext(), this.mKey, checkedInternal);
        }
        else {
            FeatureFlagUtils.setEnabled(this.getContext(), this.mKey, checkedInternal);
        }
    }
}
