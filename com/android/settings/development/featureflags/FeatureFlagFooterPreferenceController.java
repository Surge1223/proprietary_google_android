package com.android.settings.development.featureflags;

import android.content.Context;
import com.android.settingslib.widget.FooterPreferenceMixin;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class FeatureFlagFooterPreferenceController extends BasePreferenceController implements LifecycleObserver, OnStart
{
    private FooterPreferenceMixin mFooterMixin;
    
    public FeatureFlagFooterPreferenceController(final Context context) {
        super(context, "feature_flag_footer_pref");
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public void onStart() {
        this.mFooterMixin.createFooterPreference().setTitle(2131887614);
    }
    
    public void setFooterMixin(final FooterPreferenceMixin mFooterMixin) {
        this.mFooterMixin = mFooterMixin;
    }
}
