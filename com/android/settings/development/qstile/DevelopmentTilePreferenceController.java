package com.android.settings.development.qstile;

import android.os.RemoteException;
import android.util.Log;
import com.android.internal.statusbar.IStatusBarService$Stub;
import android.os.ServiceManager;
import com.android.internal.statusbar.IStatusBarService;
import android.content.pm.ServiceInfo;
import java.util.Iterator;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.settingslib.core.AbstractPreferenceController;

public class DevelopmentTilePreferenceController extends AbstractPreferenceController
{
    private final OnChangeHandler mOnChangeHandler;
    private final PackageManager mPackageManager;
    
    public DevelopmentTilePreferenceController(final Context context) {
        super(context);
        this.mOnChangeHandler = new OnChangeHandler(context);
        this.mPackageManager = context.getPackageManager();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Context context = preferenceScreen.getContext();
        final Iterator iterator = this.mPackageManager.queryIntentServices(new Intent("android.service.quicksettings.action.QS_TILE").setPackage(context.getPackageName()), 512).iterator();
        while (iterator.hasNext()) {
            final ServiceInfo serviceInfo = iterator.next().serviceInfo;
            final int componentEnabledSetting = this.mPackageManager.getComponentEnabledSetting(new ComponentName(serviceInfo.packageName, serviceInfo.name));
            boolean checked = true;
            if (componentEnabledSetting != 1) {
                checked = (componentEnabledSetting == 0 && serviceInfo.enabled && checked);
            }
            final SwitchPreference switchPreference = new SwitchPreference(context);
            switchPreference.setTitle(serviceInfo.loadLabel(this.mPackageManager));
            switchPreference.setIcon(serviceInfo.icon);
            switchPreference.setKey(serviceInfo.name);
            switchPreference.setChecked(checked);
            switchPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this.mOnChangeHandler);
            preferenceScreen.addPreference(switchPreference);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    static class OnChangeHandler implements OnPreferenceChangeListener
    {
        private final Context mContext;
        private final PackageManager mPackageManager;
        private IStatusBarService mStatusBarService;
        
        public OnChangeHandler(final Context mContext) {
            this.mContext = mContext;
            this.mPackageManager = mContext.getPackageManager();
            this.mStatusBarService = IStatusBarService$Stub.asInterface(ServiceManager.checkService("statusbar"));
        }
        
        @Override
        public boolean onPreferenceChange(Preference preference, final Object o) {
            final boolean booleanValue = (boolean)o;
            preference = (Preference)new ComponentName(this.mContext.getPackageName(), preference.getKey());
            final PackageManager mPackageManager = this.mPackageManager;
            int n;
            if (booleanValue) {
                n = 1;
            }
            else {
                n = 2;
            }
            mPackageManager.setComponentEnabledSetting((ComponentName)preference, n, 1);
            try {
                if (this.mStatusBarService != null) {
                    if (booleanValue) {
                        this.mStatusBarService.addTile((ComponentName)preference);
                    }
                    else {
                        this.mStatusBarService.remTile((ComponentName)preference);
                    }
                }
            }
            catch (RemoteException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to modify QS tile for component ");
                sb.append(((ComponentName)preference).toString());
                Log.e("DevTilePrefController", sb.toString(), (Throwable)ex);
            }
            return true;
        }
    }
}
