package com.android.settings.development.qstile;

import android.os.Parcel;
import android.widget.Toast;
import android.os.IBinder;
import com.android.internal.app.LocalePicker;
import android.os.SystemProperties;
import android.content.ContentResolver;
import android.provider.Settings;
import android.view.IWindowManager;
import android.view.WindowManagerGlobal;
import com.android.internal.statusbar.IStatusBarService;
import android.os.RemoteException;
import android.util.Log;
import android.util.EventLog;
import com.android.internal.statusbar.IStatusBarService$Stub;
import android.os.ServiceManager;
import android.content.ComponentName;
import android.content.Context;
import com.android.settingslib.development.DevelopmentSettingsEnabler;
import com.android.settingslib.development.SystemPropPoker;
import android.service.quicksettings.TileService;

public abstract class DevelopmentTiles extends TileService
{
    protected abstract boolean isEnabled();
    
    public void onClick() {
        final int state = this.getQsTile().getState();
        boolean isEnabled = true;
        if (state != 1) {
            isEnabled = false;
        }
        this.setIsEnabled(isEnabled);
        SystemPropPoker.getInstance().poke();
        this.refresh();
    }
    
    public void onStartListening() {
        super.onStartListening();
        this.refresh();
    }
    
    public void refresh() {
        int state;
        if (!DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled((Context)this)) {
            if (this.isEnabled()) {
                this.setIsEnabled(false);
                SystemPropPoker.getInstance().poke();
            }
            final ComponentName componentName = new ComponentName(this.getPackageName(), this.getClass().getName());
            try {
                this.getPackageManager().setComponentEnabledSetting(componentName, 2, 1);
                final IStatusBarService interface1 = IStatusBarService$Stub.asInterface(ServiceManager.checkService("statusbar"));
                if (interface1 != null) {
                    EventLog.writeEvent(1397638484, "117770924");
                    interface1.remTile(componentName);
                }
            }
            catch (RemoteException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to modify QS tile for component ");
                sb.append(componentName.toString());
                Log.e("DevelopmentTiles", sb.toString(), (Throwable)ex);
            }
            state = 0;
        }
        else if (this.isEnabled()) {
            state = 2;
        }
        else {
            state = 1;
        }
        this.getQsTile().setState(state);
        this.getQsTile().updateTile();
    }
    
    protected abstract void setIsEnabled(final boolean p0);
    
    public static class AnimationSpeed extends DevelopmentTiles
    {
        @Override
        protected boolean isEnabled() {
            final IWindowManager windowManagerService = WindowManagerGlobal.getWindowManagerService();
            boolean b = false;
            try {
                if (windowManagerService.getAnimationScale(0) != 1.0f) {
                    b = true;
                }
                return b;
            }
            catch (RemoteException ex) {
                return false;
            }
        }
        
        @Override
        protected void setIsEnabled(final boolean b) {
            final IWindowManager windowManagerService = WindowManagerGlobal.getWindowManagerService();
            float n;
            if (b) {
                n = 10.0f;
            }
            else {
                n = 1.0f;
            }
            try {
                windowManagerService.setAnimationScale(0, n);
                windowManagerService.setAnimationScale(1, n);
                windowManagerService.setAnimationScale(2, n);
            }
            catch (RemoteException ex) {}
        }
    }
    
    public static class ForceRTL extends DevelopmentTiles
    {
        @Override
        protected boolean isEnabled() {
            final ContentResolver contentResolver = this.getContentResolver();
            boolean b = false;
            if (Settings.Global.getInt(contentResolver, "debug.force_rtl", 0) != 0) {
                b = true;
            }
            return b;
        }
        
        @Override
        protected void setIsEnabled(final boolean b) {
            Settings.Global.putInt(this.getContentResolver(), "debug.force_rtl", (int)(b ? 1 : 0));
            String s;
            if ((b ? 1 : 0) != 0) {
                s = "1";
            }
            else {
                s = "0";
            }
            SystemProperties.set("debug.force_rtl", s);
            LocalePicker.updateLocales(this.getResources().getConfiguration().getLocales());
        }
    }
    
    public static class GPUProfiling extends DevelopmentTiles
    {
        @Override
        protected boolean isEnabled() {
            return SystemProperties.get("debug.hwui.profile").equals("visual_bars");
        }
        
        @Override
        protected void setIsEnabled(final boolean b) {
            String s;
            if (b) {
                s = "visual_bars";
            }
            else {
                s = "";
            }
            SystemProperties.set("debug.hwui.profile", s);
        }
    }
    
    public static class ShowLayout extends DevelopmentTiles
    {
        @Override
        protected boolean isEnabled() {
            return SystemProperties.getBoolean("debug.layout", false);
        }
        
        @Override
        protected void setIsEnabled(final boolean b) {
            String s;
            if (b) {
                s = "true";
            }
            else {
                s = "false";
            }
            SystemProperties.set("debug.layout", s);
        }
    }
    
    public static class WinscopeTrace extends DevelopmentTiles
    {
        static final int SURFACE_FLINGER_LAYER_TRACE_CONTROL_CODE = 1025;
        static final int SURFACE_FLINGER_LAYER_TRACE_STATUS_CODE = 1026;
        private IBinder mSurfaceFlinger;
        private Toast mToast;
        private IWindowManager mWindowManager;
        
        private boolean isLayerTraceEnabled() {
            final boolean b = false;
            boolean b2 = false;
            boolean boolean1 = false;
            final Parcel parcel = null;
            final Parcel parcel2 = null;
            Parcel parcel3 = null;
            final Parcel parcel4 = null;
            Parcel obtain = null;
            Parcel obtain2 = parcel;
            Parcel parcel5 = parcel4;
            Parcel parcel6 = parcel2;
            try {
                try {
                    if (this.mSurfaceFlinger != null) {
                        obtain2 = parcel;
                        parcel5 = parcel4;
                        parcel6 = parcel2;
                        parcel3 = (obtain2 = Parcel.obtain());
                        parcel5 = parcel4;
                        parcel6 = parcel3;
                        obtain = Parcel.obtain();
                        obtain2 = parcel3;
                        parcel5 = obtain;
                        parcel6 = parcel3;
                        obtain.writeInterfaceToken("android.ui.ISurfaceComposer");
                        obtain2 = parcel3;
                        parcel5 = obtain;
                        parcel6 = parcel3;
                        this.mSurfaceFlinger.transact(1026, obtain, parcel3, 0);
                        obtain2 = parcel3;
                        parcel5 = obtain;
                        parcel6 = parcel3;
                        boolean1 = parcel3.readBoolean();
                    }
                    b2 = boolean1;
                    if (obtain != null) {
                        final Parcel parcel7 = obtain;
                        parcel6 = parcel3;
                        parcel7.recycle();
                        parcel6.recycle();
                        b2 = boolean1;
                        return b2;
                    }
                    return b2;
                }
                finally {
                    if (parcel5 != null) {
                        parcel5.recycle();
                        obtain2.recycle();
                    }
                    return b2;
                    boolean1 = b;
                }
            }
            catch (RemoteException ex) {}
        }
        
        private boolean isWindowTraceEnabled() {
            try {
                return this.mWindowManager.isWindowTraceEnabled();
            }
            catch (RemoteException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not get window trace status, defaulting to false.");
                sb.append(ex.toString());
                Log.e("DevelopmentTiles", sb.toString());
                return false;
            }
        }
        
        private void setLayerTraceEnabled(final boolean b) {
            final Parcel parcel = null;
            Parcel parcel2 = null;
            Parcel obtain = parcel;
            try {
                while (true) {
                    try {
                        if (this.mSurfaceFlinger != null) {
                            obtain = parcel;
                            parcel2 = (obtain = Parcel.obtain());
                            parcel2.writeInterfaceToken("android.ui.ISurfaceComposer");
                            obtain = parcel2;
                            parcel2.writeInt((int)(b ? 1 : 0));
                            obtain = parcel2;
                            this.mSurfaceFlinger.transact(1025, parcel2, (Parcel)null, 0);
                        }
                        if (parcel2 != null) {
                            final Parcel parcel3 = parcel2;
                            parcel3.recycle();
                        }
                    }
                    finally {
                        if (obtain != null) {
                            obtain.recycle();
                        }
                        continue;
                    }
                    break;
                }
            }
            catch (RemoteException ex) {}
        }
        
        private void setWindowTraceEnabled(final boolean b) {
            Label_0020: {
                if (b) {
                    Label_0032: {
                        try {
                            this.mWindowManager.startWindowTrace();
                            return;
                        }
                        catch (RemoteException ex) {
                            break Label_0032;
                        }
                        break Label_0020;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Could not set window trace status.");
                    final RemoteException ex;
                    sb.append(ex.toString());
                    Log.e("DevelopmentTiles", sb.toString());
                    return;
                }
            }
            this.mWindowManager.stopWindowTrace();
        }
        
        @Override
        protected boolean isEnabled() {
            return this.isWindowTraceEnabled() || this.isLayerTraceEnabled();
        }
        
        public void onCreate() {
            super.onCreate();
            this.mWindowManager = WindowManagerGlobal.getWindowManagerService();
            this.mSurfaceFlinger = ServiceManager.getService("SurfaceFlinger");
            this.mToast = Toast.makeText(this.getApplicationContext(), (CharSequence)"Trace files written to /data/misc/wmtrace", 1);
        }
        
        @Override
        protected void setIsEnabled(final boolean b) {
            this.setWindowTraceEnabled(b);
            this.setLayerTraceEnabled(b);
            if (!b) {
                this.mToast.show();
            }
        }
    }
}
