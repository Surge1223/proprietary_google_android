package com.android.settings.development.qstile;

import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.dashboard.DashboardFragment;

public class DevelopmentTileConfigFragment extends DashboardFragment
{
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<DevelopmentTilePreferenceController> list = (ArrayList<DevelopmentTilePreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new DevelopmentTilePreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "DevelopmentTileConfig";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1224;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082756;
    }
}
