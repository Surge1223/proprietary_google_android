package com.android.settings.development;

import android.content.res.Resources;
import android.content.ContentResolver;
import android.support.v7.preference.ListPreference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class SimulateColorSpacePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    
    public SimulateColorSpacePreferenceController(final Context context) {
        super(context);
    }
    
    private void updateSimulateColorSpace() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        final boolean b = Settings.Secure.getInt(contentResolver, "accessibility_display_daltonizer_enabled", 0) != 0;
        final ListPreference listPreference = (ListPreference)this.mPreference;
        if (b) {
            final String string = Integer.toString(Settings.Secure.getInt(contentResolver, "accessibility_display_daltonizer", -1));
            listPreference.setValue(string);
            if (listPreference.findIndexOfValue(string) < 0) {
                final Resources resources = this.mContext.getResources();
                listPreference.setSummary(resources.getString(2131887198, new Object[] { resources.getString(2131886151) }));
            }
            else {
                listPreference.setSummary("%s");
            }
        }
        else {
            listPreference.setValue(Integer.toString(-1));
        }
    }
    
    private boolean usingDevelopmentColorSpace() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        return Settings.Secure.getInt(contentResolver, "accessibility_display_daltonizer_enabled", 0) != 0 && ((ListPreference)this.mPreference).findIndexOfValue(Integer.toString(Settings.Secure.getInt(contentResolver, "accessibility_display_daltonizer", -1))) >= 0;
    }
    
    private void writeSimulateColorSpace(final Object o) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        final int int1 = Integer.parseInt(o.toString());
        if (int1 < 0) {
            Settings.Secure.putInt(contentResolver, "accessibility_display_daltonizer_enabled", 0);
        }
        else {
            Settings.Secure.putInt(contentResolver, "accessibility_display_daltonizer_enabled", 1);
            Settings.Secure.putInt(contentResolver, "accessibility_display_daltonizer", int1);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "simulate_color_space";
    }
    
    @Override
    public void onDeveloperOptionsDisabled() {
        super.onDeveloperOptionsDisabled();
        if (this.usingDevelopmentColorSpace()) {
            this.writeSimulateColorSpace(-1);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeSimulateColorSpace(o);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateSimulateColorSpace();
    }
}
