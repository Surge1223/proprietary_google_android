package com.android.settings.development;

import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;
import android.content.pm.IShortcutService$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.content.pm.IShortcutService;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class ShortcutManagerThrottlingPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    private final IShortcutService mShortcutService;
    
    public ShortcutManagerThrottlingPreferenceController(final Context context) {
        super(context);
        this.mShortcutService = this.getShortCutService();
    }
    
    private IShortcutService getShortCutService() {
        try {
            return IShortcutService$Stub.asInterface(ServiceManager.getService("shortcut"));
        }
        catch (VerifyError verifyError) {
            return null;
        }
    }
    
    private void resetShortcutManagerThrottling() {
        if (this.mShortcutService == null) {
            return;
        }
        try {
            this.mShortcutService.resetThrottling();
            Toast.makeText(this.mContext, 2131888825, 0).show();
        }
        catch (RemoteException ex) {
            Log.e("ShortcutMgrPrefCtrl", "Failed to reset rate limiting", (Throwable)ex);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "reset_shortcut_manager_throttling";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)"reset_shortcut_manager_throttling", (CharSequence)preference.getKey())) {
            return false;
        }
        this.resetShortcutManagerThrottling();
        return true;
    }
}
