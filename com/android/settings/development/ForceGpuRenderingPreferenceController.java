package com.android.settings.development;

import com.android.settingslib.development.SystemPropPoker;
import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class ForceGpuRenderingPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String HARDWARE_UI_PROPERTY = "persist.sys.ui.hw";
    
    public ForceGpuRenderingPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "force_hw_ui";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("persist.sys.ui.hw", Boolean.toString(false));
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        String s;
        if (o) {
            s = Boolean.toString(true);
        }
        else {
            s = Boolean.toString(false);
        }
        SystemProperties.set("persist.sys.ui.hw", s);
        SystemPropPoker.getInstance().poke();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("persist.sys.ui.hw", false));
    }
}
