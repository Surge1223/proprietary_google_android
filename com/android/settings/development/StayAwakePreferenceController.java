package com.android.settings.development;

import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;
import android.content.ContentResolver;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.RestrictedSwitchPreference;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class StayAwakePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 7;
    private RestrictedSwitchPreference mPreference;
    SettingsObserver mSettingsObserver;
    
    public StayAwakePreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    RestrictedLockUtils.EnforcedAdmin checkIfMaximumTimeToLockSetByAdmin() {
        return RestrictedLockUtils.checkIfMaximumTimeToLockIsSet(this.mContext);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (RestrictedSwitchPreference)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "keep_screen_on";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        Settings.Global.putInt(this.mContext.getContentResolver(), "stay_on_while_plugged_in", 0);
        this.mPreference.setChecked(false);
    }
    
    @Override
    public void onPause() {
        if (this.mPreference != null && this.mSettingsObserver != null) {
            this.mSettingsObserver.register(false);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        int n;
        if (booleanValue) {
            n = 7;
        }
        else {
            n = 0;
        }
        Settings.Global.putInt(contentResolver, "stay_on_while_plugged_in", n);
        return true;
    }
    
    @Override
    public void onResume() {
        if (this.mPreference == null) {
            return;
        }
        if (this.mSettingsObserver == null) {
            this.mSettingsObserver = new SettingsObserver();
        }
        this.mSettingsObserver.register(true);
    }
    
    @Override
    public void updateState(final Preference preference) {
        final RestrictedLockUtils.EnforcedAdmin checkIfMaximumTimeToLockSetByAdmin = this.checkIfMaximumTimeToLockSetByAdmin();
        if (checkIfMaximumTimeToLockSetByAdmin != null) {
            this.mPreference.setDisabledByAdmin(checkIfMaximumTimeToLockSetByAdmin);
            return;
        }
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Global.getInt(contentResolver, "stay_on_while_plugged_in", 0);
        final RestrictedSwitchPreference mPreference = this.mPreference;
        if (int1 != 0) {
            checked = true;
        }
        mPreference.setChecked(checked);
    }
    
    class SettingsObserver extends ContentObserver
    {
        private final Uri mStayAwakeUri;
        
        public SettingsObserver() {
            super(new Handler());
            this.mStayAwakeUri = Settings.Global.getUriFor("stay_on_while_plugged_in");
        }
        
        public void onChange(final boolean b, final Uri uri) {
            super.onChange(b, uri);
            if (this.mStayAwakeUri.equals((Object)uri)) {
                StayAwakePreferenceController.this.updateState(StayAwakePreferenceController.this.mPreference);
            }
        }
        
        public void register(final boolean b) {
            final ContentResolver contentResolver = StayAwakePreferenceController.this.mContext.getContentResolver();
            if (b) {
                contentResolver.registerContentObserver(this.mStayAwakeUri, false, (ContentObserver)this);
            }
            else {
                contentResolver.unregisterContentObserver((ContentObserver)this);
            }
        }
    }
}
