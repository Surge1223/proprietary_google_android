package com.android.settings.development;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.content.Context;
import android.os.UserManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BugReportInPowerPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static int SETTING_VALUE_OFF;
    static int SETTING_VALUE_ON;
    private final UserManager mUserManager;
    
    static {
        BugReportInPowerPreferenceController.SETTING_VALUE_ON = 1;
        BugReportInPowerPreferenceController.SETTING_VALUE_OFF = 0;
    }
    
    public BugReportInPowerPreferenceController(final Context context) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
    }
    
    @Override
    public String getPreferenceKey() {
        return "bugreport_in_power";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUserManager.hasUserRestriction("no_debugging_features") ^ true;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        Settings.Secure.putInt(this.mContext.getContentResolver(), "bugreport_in_power_menu", BugReportInPowerPreferenceController.SETTING_VALUE_OFF);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        int n;
        if (booleanValue) {
            n = BugReportInPowerPreferenceController.SETTING_VALUE_ON;
        }
        else {
            n = BugReportInPowerPreferenceController.SETTING_VALUE_OFF;
        }
        Settings.Secure.putInt(contentResolver, "bugreport_in_power_menu", n);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(Settings.Secure.getInt(this.mContext.getContentResolver(), "bugreport_in_power_menu", BugReportInPowerPreferenceController.SETTING_VALUE_OFF) != BugReportInPowerPreferenceController.SETTING_VALUE_OFF);
    }
}
