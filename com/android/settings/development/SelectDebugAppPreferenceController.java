package com.android.settings.development;

import android.support.v7.preference.Preference;
import android.content.Intent;
import android.provider.Settings;
import android.content.pm.PackageManager;
import android.content.Context;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class SelectDebugAppPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin, OnActivityResultListener
{
    private final DevelopmentSettingsDashboardFragment mFragment;
    private final PackageManagerWrapper mPackageManager;
    
    public SelectDebugAppPreferenceController(final Context context, final DevelopmentSettingsDashboardFragment mFragment) {
        super(context);
        this.mFragment = mFragment;
        this.mPackageManager = new PackageManagerWrapper(this.mContext.getPackageManager());
    }
    
    private String getAppLabel(String string) {
        try {
            final CharSequence applicationLabel = this.mPackageManager.getApplicationLabel(this.mPackageManager.getApplicationInfo(string, 512));
            if (applicationLabel != null) {
                string = applicationLabel.toString();
            }
            return string;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return string;
        }
    }
    
    private void updatePreferenceSummary() {
        final String string = Settings.Global.getString(this.mContext.getContentResolver(), "debug_app");
        if (string != null && string.length() > 0) {
            this.mPreference.setSummary(this.mContext.getResources().getString(2131887332, new Object[] { this.getAppLabel(string) }));
        }
        else {
            this.mPreference.setSummary(this.mContext.getResources().getString(2131887331));
        }
    }
    
    Intent getActivityStartIntent() {
        return new Intent(this.mContext, (Class)AppPicker.class);
    }
    
    @Override
    public String getPreferenceKey() {
        return "debug_app";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("debug_app".equals(preference.getKey())) {
            final Intent activityStartIntent = this.getActivityStartIntent();
            activityStartIntent.putExtra("com.android.settings.extra.DEBUGGABLE", true);
            this.mFragment.startActivityForResult(activityStartIntent, 1);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1 && n2 == -1) {
            Settings.Global.putString(this.mContext.getContentResolver(), "debug_app", intent.getAction());
            this.updatePreferenceSummary();
            return true;
        }
        return false;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.mPreference.setSummary(this.mContext.getResources().getString(2131887331));
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updatePreferenceSummary();
    }
}
