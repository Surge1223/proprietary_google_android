package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BluetoothSnoopLogPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String BLUETOOTH_BTSNOOP_ENABLE_PROPERTY = "persist.bluetooth.btsnoopenable";
    
    public BluetoothSnoopLogPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bt_hci_snoop_log";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("persist.bluetooth.btsnoopenable", Boolean.toString(false));
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        SystemProperties.set("persist.bluetooth.btsnoopenable", Boolean.toString((boolean)o));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("persist.bluetooth.btsnoopenable", false));
    }
}
