package com.android.settings.development;

import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.AbstractLogdSizePreferenceController;

public class LogdSizePreferenceController extends AbstractLogdSizePreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public LogdSizePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeLogdSizeOption(null);
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateLogdSizeValues();
    }
}
