package com.android.settings.development;

import android.content.Intent;

public interface OnActivityResultListener
{
    boolean onActivityResult(final int p0, final int p1, final Intent p2);
}
