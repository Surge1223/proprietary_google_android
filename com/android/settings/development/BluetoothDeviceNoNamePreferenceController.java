package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BluetoothDeviceNoNamePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String BLUETOOTH_SHOW_DEVICES_WITHOUT_NAMES_PROPERTY = "persist.bluetooth.showdeviceswithoutnames";
    
    public BluetoothDeviceNoNamePreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_show_devices_without_names";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("persist.bluetooth.showdeviceswithoutnames", "false");
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        String s;
        if (o) {
            s = "true";
        }
        else {
            s = "false";
        }
        SystemProperties.set("persist.bluetooth.showdeviceswithoutnames", s);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("persist.bluetooth.showdeviceswithoutnames", false));
    }
}
