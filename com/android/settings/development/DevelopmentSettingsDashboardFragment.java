package com.android.settings.development;

import android.widget.Switch;
import android.bluetooth.BluetoothAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;
import com.android.settingslib.development.SystemPropPoker;
import com.android.settings.Utils;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothCodecStatus;
import android.util.Log;
import java.util.Iterator;
import android.content.Intent;
import java.util.ArrayList;
import com.android.settingslib.development.DevelopmentSettingsEnabler;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Activity;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.BroadcastReceiver;
import android.bluetooth.BluetoothA2dp;
import com.android.settings.search.Indexable;
import com.android.settings.widget.SwitchBar;
import com.android.settings.dashboard.RestrictedDashboardFragment;

public class DevelopmentSettingsDashboardFragment extends RestrictedDashboardFragment implements AdbClearKeysDialogHost, AdbDialogHost, OnA2dpHwDialogConfirmedListener, LogPersistDialogHost, OemUnlockDialogHost, OnSwitchChangeListener
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private BluetoothA2dp mBluetoothA2dp;
    private final BluetoothA2dpConfigStore mBluetoothA2dpConfigStore;
    private final BroadcastReceiver mBluetoothA2dpReceiver;
    private final BluetoothProfile$ServiceListener mBluetoothA2dpServiceListener;
    private final BroadcastReceiver mEnableAdbReceiver;
    private boolean mIsAvailable;
    private List<AbstractPreferenceController> mPreferenceControllers;
    private SwitchBar mSwitchBar;
    private DevelopmentSwitchBarController mSwitchBarController;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, null, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082755;
                return Arrays.asList(searchIndexableResource);
            }
            
            @Override
            protected boolean isPageSearchEnabled(final Context context) {
                return DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(context);
            }
        };
    }
    
    public DevelopmentSettingsDashboardFragment() {
        super("no_debugging_features");
        this.mBluetoothA2dpConfigStore = new BluetoothA2dpConfigStore();
        this.mIsAvailable = true;
        this.mPreferenceControllers = new ArrayList<AbstractPreferenceController>();
        this.mEnableAdbReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                for (final AbstractPreferenceController abstractPreferenceController : DevelopmentSettingsDashboardFragment.this.mPreferenceControllers) {
                    if (abstractPreferenceController instanceof AdbOnChangeListener) {
                        ((AdbOnChangeListener)abstractPreferenceController).onAdbSettingChanged();
                    }
                }
            }
        };
        this.mBluetoothA2dpReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final StringBuilder sb = new StringBuilder();
                sb.append("mBluetoothA2dpReceiver.onReceive intent=");
                sb.append(intent);
                Log.d("DevSettingsDashboard", sb.toString());
                if ("android.bluetooth.a2dp.profile.action.CODEC_CONFIG_CHANGED".equals(intent.getAction())) {
                    final BluetoothCodecStatus bluetoothCodecStatus = (BluetoothCodecStatus)intent.getParcelableExtra("android.bluetooth.codec.extra.CODEC_STATUS");
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Received BluetoothCodecStatus=");
                    sb2.append(bluetoothCodecStatus);
                    Log.d("DevSettingsDashboard", sb2.toString());
                    for (final AbstractPreferenceController abstractPreferenceController : DevelopmentSettingsDashboardFragment.this.mPreferenceControllers) {
                        if (abstractPreferenceController instanceof BluetoothServiceConnectionListener) {
                            ((BluetoothServiceConnectionListener)abstractPreferenceController).onBluetoothCodecUpdated();
                        }
                    }
                }
            }
        };
        this.mBluetoothA2dpServiceListener = (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
            public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                Object o = DevelopmentSettingsDashboardFragment.this.mBluetoothA2dpConfigStore;
                synchronized (o) {
                    DevelopmentSettingsDashboardFragment.this.mBluetoothA2dp = (BluetoothA2dp)bluetoothProfile;
                    // monitorexit(o)
                    o = DevelopmentSettingsDashboardFragment.this.mPreferenceControllers.iterator();
                    while (((Iterator)o).hasNext()) {
                        final AbstractPreferenceController abstractPreferenceController = ((Iterator<AbstractPreferenceController>)o).next();
                        if (abstractPreferenceController instanceof BluetoothServiceConnectionListener) {
                            ((BluetoothServiceConnectionListener)abstractPreferenceController).onBluetoothServiceConnected(DevelopmentSettingsDashboardFragment.this.mBluetoothA2dp);
                        }
                    }
                }
            }
            
            public void onServiceDisconnected(final int n) {
                Object access$100 = DevelopmentSettingsDashboardFragment.this.mBluetoothA2dpConfigStore;
                synchronized (access$100) {
                    DevelopmentSettingsDashboardFragment.this.mBluetoothA2dp = null;
                    // monitorexit(access$100)
                    final Iterator<AbstractPreferenceController> iterator = DevelopmentSettingsDashboardFragment.this.mPreferenceControllers.iterator();
                    while (iterator.hasNext()) {
                        access$100 = iterator.next();
                        if (access$100 instanceof BluetoothServiceConnectionListener) {
                            ((BluetoothServiceConnectionListener)access$100).onBluetoothServiceDisconnected();
                        }
                    }
                }
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Activity activity, final Lifecycle lifecycle, final DevelopmentSettingsDashboardFragment developmentSettingsDashboardFragment, final BluetoothA2dpConfigStore bluetoothA2dpConfigStore) {
        final ArrayList<BugReportPreferenceController> list = (ArrayList<BugReportPreferenceController>)new ArrayList<DefaultLaunchPreferenceController>();
        list.add((DefaultLaunchPreferenceController)new MemoryUsagePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BugReportPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new LocalBackupPasswordPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new StayAwakePreferenceController(context, lifecycle));
        list.add((DefaultLaunchPreferenceController)new HdcpCheckingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new DarkUIPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BluetoothSnoopLogPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new OemUnlockPreferenceController(context, activity, developmentSettingsDashboardFragment));
        list.add((DefaultLaunchPreferenceController)new FileEncryptionPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new PictureColorModePreferenceController(context, lifecycle));
        list.add((DefaultLaunchPreferenceController)new WebViewAppPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new CoolColorTemperaturePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new DisableAutomaticUpdatesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new AdbPreferenceController(context, developmentSettingsDashboardFragment));
        list.add((DefaultLaunchPreferenceController)new ClearAdbKeysPreferenceController(context, developmentSettingsDashboardFragment));
        list.add((DefaultLaunchPreferenceController)new LocalTerminalPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BugReportInPowerPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new MockLocationAppPreferenceController(context, developmentSettingsDashboardFragment));
        list.add((DefaultLaunchPreferenceController)new DebugViewAttributesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new SelectDebugAppPreferenceController(context, developmentSettingsDashboardFragment));
        list.add((DefaultLaunchPreferenceController)new WaitForDebuggerPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new EnableGpuDebugLayersPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new VerifyAppsOverUsbPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new LogdSizePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new LogPersistPreferenceController(context, developmentSettingsDashboardFragment, lifecycle));
        list.add((DefaultLaunchPreferenceController)new CameraLaserSensorPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new WifiDisplayCertificationPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new WifiVerboseLoggingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new WifiConnectedMacRandomizationPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new MobileDataAlwaysOnPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new TetheringHardwareAccelPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BluetoothDeviceNoNamePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BluetoothAbsoluteVolumePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BluetoothAvrcpVersionPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BluetoothA2dpHwOffloadPreferenceController(context, developmentSettingsDashboardFragment));
        list.add((DefaultLaunchPreferenceController)new BluetoothAudioCodecPreferenceController(context, lifecycle, bluetoothA2dpConfigStore));
        list.add((DefaultLaunchPreferenceController)new BluetoothAudioSampleRatePreferenceController(context, lifecycle, bluetoothA2dpConfigStore));
        list.add((DefaultLaunchPreferenceController)new BluetoothAudioBitsPerSamplePreferenceController(context, lifecycle, bluetoothA2dpConfigStore));
        list.add((DefaultLaunchPreferenceController)new BluetoothAudioChannelModePreferenceController(context, lifecycle, bluetoothA2dpConfigStore));
        list.add((DefaultLaunchPreferenceController)new BluetoothAudioQualityPreferenceController(context, lifecycle, bluetoothA2dpConfigStore));
        list.add((DefaultLaunchPreferenceController)new BluetoothMaxConnectedAudioDevicesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ShowTapsPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new PointerLocationPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ShowSurfaceUpdatesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ShowLayoutBoundsPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new RtlLayoutPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new WindowAnimationScalePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new EmulateDisplayCutoutPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new TransitionAnimationScalePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new AnimatorDurationScalePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new SecondaryDisplayPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ForceGpuRenderingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new GpuViewUpdatesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new HardwareLayersUpdatesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new DebugGpuOverdrawPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new DebugNonRectClipOperationsPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ForceMSAAPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new HardwareOverlaysPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new SimulateColorSpacePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new UsbAudioRoutingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new StrictModePreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ProfileGpuRenderingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new KeepActivitiesPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new BackgroundProcessLimitPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ShowFirstCrashDialogPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new AppsNotRespondingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new NotificationChannelWarningsPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new AllowAppsOnExternalPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ResizableActivityPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new FreeformWindowsPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new ShortcutManagerThrottlingPreferenceController(context));
        list.add((DefaultLaunchPreferenceController)new EnableGnssRawMeasFullTrackingPreferenceController(context));
        list.add(new DefaultLaunchPreferenceController(context, "running_apps"));
        list.add(new DefaultLaunchPreferenceController(context, "demo_mode"));
        list.add(new DefaultLaunchPreferenceController(context, "quick_settings_tiles"));
        list.add(new DefaultLaunchPreferenceController(context, "feature_flags_dashboard"));
        list.add(new DefaultLaunchPreferenceController(context, "default_usb_configuration"));
        list.add(new DefaultLaunchPreferenceController(context, "density"));
        list.add(new DefaultLaunchPreferenceController(context, "background_check"));
        list.add(new DefaultLaunchPreferenceController(context, "inactive_apps"));
        return (List<AbstractPreferenceController>)list;
    }
    
    private void disableDeveloperOptions() {
        if (Utils.isMonkeyRunning()) {
            return;
        }
        DevelopmentSettingsEnabler.setDevelopmentSettingsEnabled(this.getContext(), false);
        final SystemPropPoker instance = SystemPropPoker.getInstance();
        instance.blockPokes();
        for (final AbstractPreferenceController abstractPreferenceController : this.mPreferenceControllers) {
            if (abstractPreferenceController instanceof DeveloperOptionsPreferenceController) {
                ((DeveloperOptionsPreferenceController)abstractPreferenceController).onDeveloperOptionsDisabled();
            }
        }
        instance.unblockPokes();
        instance.poke();
    }
    
    private void enableDeveloperOptions() {
        if (Utils.isMonkeyRunning()) {
            return;
        }
        DevelopmentSettingsEnabler.setDevelopmentSettingsEnabled(this.getContext(), true);
        for (final AbstractPreferenceController abstractPreferenceController : this.mPreferenceControllers) {
            if (abstractPreferenceController instanceof DeveloperOptionsPreferenceController) {
                ((DeveloperOptionsPreferenceController)abstractPreferenceController).onDeveloperOptionsEnabled();
            }
        }
    }
    
    private void registerReceivers() {
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(this.mEnableAdbReceiver, new IntentFilter("com.android.settingslib.development.AbstractEnableAdbController.ENABLE_ADB_STATE_CHANGED"));
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.a2dp.profile.action.CODEC_CONFIG_CHANGED");
        this.getActivity().registerReceiver(this.mBluetoothA2dpReceiver, intentFilter);
    }
    
    private void unregisterReceivers() {
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(this.mEnableAdbReceiver);
        this.getActivity().unregisterReceiver(this.mBluetoothA2dpReceiver);
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        if (Utils.isMonkeyRunning()) {
            this.mPreferenceControllers = new ArrayList<AbstractPreferenceController>();
            return null;
        }
        return this.mPreferenceControllers = buildPreferenceControllers(context, this.getActivity(), this.getLifecycle(), this, new BluetoothA2dpConfigStore());
    }
    
     <T extends AbstractPreferenceController> T getDevelopmentOptionsController(final Class<T> clazz) {
        return this.use(clazz);
    }
    
    @Override
    public int getHelpResource() {
        return 0;
    }
    
    @Override
    protected String getLogTag() {
        return "DevSettingsDashboard";
    }
    
    @Override
    public int getMetricsCategory() {
        return 39;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        int n;
        if (Utils.isMonkeyRunning()) {
            n = 2132082805;
        }
        else {
            n = 2132082755;
        }
        return n;
    }
    
    @Override
    public void onA2dpHwDialogConfirmed() {
        this.getDevelopmentOptionsController(BluetoothA2dpHwOffloadPreferenceController.class).onA2dpHwDialogConfirmed();
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.setIfOnlyAvailableForAdmins(true);
        if (!this.isUiRestricted() && Utils.isDeviceProvisioned((Context)this.getActivity())) {
            this.mSwitchBar = ((SettingsActivity)this.getActivity()).getSwitchBar();
            this.mSwitchBarController = new DevelopmentSwitchBarController(this, this.mSwitchBar, this.mIsAvailable, this.getLifecycle());
            this.mSwitchBar.show();
            if (DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(this.getContext())) {
                this.enableDeveloperOptions();
            }
            else {
                this.disableDeveloperOptions();
            }
            return;
        }
        this.mIsAvailable = false;
        if (!this.isUiRestrictedByOnlyAdmin()) {
            this.getEmptyTextView().setText(2131887379);
        }
        this.getPreferenceScreen().removeAll();
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        boolean b = false;
        for (final AbstractPreferenceController abstractPreferenceController : this.mPreferenceControllers) {
            boolean b2 = b;
            if (abstractPreferenceController instanceof OnActivityResultListener) {
                b2 = (b | ((OnActivityResultListener)abstractPreferenceController).onActivityResult(n, n2, intent));
            }
            b = b2;
        }
        if (!b) {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Override
    public void onAdbClearKeysDialogConfirmed() {
        this.getDevelopmentOptionsController(ClearAdbKeysPreferenceController.class).onClearAdbKeysConfirmed();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (Utils.isMonkeyRunning()) {
            this.getActivity().finish();
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.registerReceivers();
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            defaultAdapter.getProfileProxy((Context)this.getActivity(), this.mBluetoothA2dpServiceListener, 2);
        }
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.unregisterReceivers();
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            defaultAdapter.closeProfileProxy(2, (BluetoothProfile)this.mBluetoothA2dp);
            this.mBluetoothA2dp = null;
        }
    }
    
    @Override
    public void onDisableLogPersistDialogConfirmed() {
        this.getDevelopmentOptionsController(LogPersistPreferenceController.class).onDisableLogPersistDialogConfirmed();
    }
    
    @Override
    public void onDisableLogPersistDialogRejected() {
        this.getDevelopmentOptionsController(LogPersistPreferenceController.class).onDisableLogPersistDialogRejected();
    }
    
    @Override
    public void onEnableAdbDialogConfirmed() {
        this.getDevelopmentOptionsController(AdbPreferenceController.class).onAdbDialogConfirmed();
    }
    
    @Override
    public void onEnableAdbDialogDismissed() {
        this.getDevelopmentOptionsController(AdbPreferenceController.class).onAdbDialogDismissed();
    }
    
    void onEnableDevelopmentOptionsConfirmed() {
        this.enableDeveloperOptions();
    }
    
    void onEnableDevelopmentOptionsRejected() {
        this.mSwitchBar.setChecked(false);
    }
    
    @Override
    public void onOemUnlockDialogConfirmed() {
        this.getDevelopmentOptionsController(OemUnlockPreferenceController.class).onOemUnlockConfirmed();
    }
    
    @Override
    public void onOemUnlockDialogDismissed() {
        this.getDevelopmentOptionsController(OemUnlockPreferenceController.class).onOemUnlockDismissed();
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean b) {
        if (switch1 != this.mSwitchBar.getSwitch()) {
            return;
        }
        if (b != DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(this.getContext())) {
            if (b) {
                EnableDevelopmentSettingWarningDialog.show(this);
            }
            else {
                this.disableDeveloperOptions();
            }
        }
    }
}
