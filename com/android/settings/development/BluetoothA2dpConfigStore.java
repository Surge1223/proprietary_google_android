package com.android.settings.development;

import android.bluetooth.BluetoothCodecConfig;

public class BluetoothA2dpConfigStore
{
    private int mBitsPerSample;
    private int mChannelMode;
    private int mCodecPriority;
    private long mCodecSpecific1Value;
    private long mCodecSpecific2Value;
    private long mCodecSpecific3Value;
    private long mCodecSpecific4Value;
    private int mCodecType;
    private int mSampleRate;
    
    public BluetoothA2dpConfigStore() {
        this.mCodecType = 1000000;
        this.mCodecPriority = 0;
        this.mSampleRate = 0;
        this.mBitsPerSample = 0;
        this.mChannelMode = 0;
    }
    
    public BluetoothCodecConfig createCodecConfig() {
        return new BluetoothCodecConfig(this.mCodecType, this.mCodecPriority, this.mSampleRate, this.mBitsPerSample, this.mChannelMode, this.mCodecSpecific1Value, this.mCodecSpecific2Value, this.mCodecSpecific3Value, this.mCodecSpecific4Value);
    }
    
    public void setBitsPerSample(final int mBitsPerSample) {
        this.mBitsPerSample = mBitsPerSample;
    }
    
    public void setChannelMode(final int mChannelMode) {
        this.mChannelMode = mChannelMode;
    }
    
    public void setCodecPriority(final int mCodecPriority) {
        this.mCodecPriority = mCodecPriority;
    }
    
    public void setCodecSpecific1Value(final int n) {
        this.mCodecSpecific1Value = n;
    }
    
    public void setCodecType(final int mCodecType) {
        this.mCodecType = mCodecType;
    }
    
    public void setSampleRate(final int mSampleRate) {
        this.mSampleRate = mSampleRate;
    }
}
