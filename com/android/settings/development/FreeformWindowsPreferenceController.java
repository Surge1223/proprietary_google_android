package com.android.settings.development;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.text.TextUtils;
import android.os.Build;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class FreeformWindowsPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    static final String USER_BUILD_TYPE = "user";
    
    public FreeformWindowsPreferenceController(final Context context) {
        super(context);
    }
    
    String getBuildType() {
        return Build.TYPE;
    }
    
    @Override
    public String getPreferenceKey() {
        return "enable_freeform_support";
    }
    
    @Override
    public boolean isAvailable() {
        return TextUtils.equals((CharSequence)"user", (CharSequence)this.getBuildType()) ^ true;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        Settings.Global.putInt(this.mContext.getContentResolver(), "enable_freeform_support", 0);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "enable_freeform_support", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Global.getInt(contentResolver, "enable_freeform_support", 0);
        final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
        if (int1 != 0) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
