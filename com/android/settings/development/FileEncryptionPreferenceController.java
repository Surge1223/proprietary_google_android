package com.android.settings.development;

import android.text.TextUtils;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.os.RemoteException;
import android.os.storage.IStorageManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.os.storage.IStorageManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class FileEncryptionPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    static final String FILE_ENCRYPTION_PROPERTY_KEY = "ro.crypto.type";
    private final IStorageManager mStorageManager;
    
    public FileEncryptionPreferenceController(final Context context) {
        super(context);
        this.mStorageManager = this.getStorageManager();
    }
    
    private IStorageManager getStorageManager() {
        try {
            return IStorageManager$Stub.asInterface(ServiceManager.getService("mount"));
        }
        catch (VerifyError verifyError) {
            return null;
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "convert_to_file_encryption";
    }
    
    @Override
    public boolean isAvailable() {
        if (this.mStorageManager == null) {
            return false;
        }
        try {
            return this.mStorageManager.isConvertibleToFBE();
        }
        catch (RemoteException ex) {
            return false;
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!TextUtils.equals((CharSequence)"file", (CharSequence)SystemProperties.get("ro.crypto.type", "none"))) {
            return;
        }
        this.mPreference.setEnabled(false);
        this.mPreference.setSummary(this.mContext.getResources().getString(2131887134));
    }
}
