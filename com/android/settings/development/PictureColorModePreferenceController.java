package com.android.settings.development;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class PictureColorModePreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private ColorModePreference mPreference;
    
    public PictureColorModePreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (ColorModePreference)preferenceScreen.findPreference(this.getPreferenceKey());
        if (this.mPreference != null) {
            this.mPreference.updateCurrentAndSupported();
        }
    }
    
    int getColorModeDescriptionsSize() {
        return ColorModePreference.getColorModeDescriptions(this.mContext).size();
    }
    
    @Override
    public String getPreferenceKey() {
        return "picture_color_mode";
    }
    
    @Override
    public boolean isAvailable() {
        final int colorModeDescriptionsSize = this.getColorModeDescriptionsSize();
        boolean b = true;
        if (colorModeDescriptionsSize <= 1 || this.isWideColorGamut()) {
            b = false;
        }
        return b;
    }
    
    boolean isWideColorGamut() {
        return this.mContext.getResources().getConfiguration().isScreenWideColorGamut();
    }
    
    @Override
    public void onPause() {
        if (this.mPreference == null) {
            return;
        }
        this.mPreference.stopListening();
    }
    
    @Override
    public void onResume() {
        if (this.mPreference == null) {
            return;
        }
        this.mPreference.startListening();
        this.mPreference.updateCurrentAndSupported();
    }
}
