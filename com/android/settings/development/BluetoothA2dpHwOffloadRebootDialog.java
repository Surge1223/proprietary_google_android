package com.android.settings.development;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.os.PowerManager;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class BluetoothA2dpHwOffloadRebootDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    public static void show(final DevelopmentSettingsDashboardFragment developmentSettingsDashboardFragment, final BluetoothA2dpHwOffloadPreferenceController bluetoothA2dpHwOffloadPreferenceController) {
        final FragmentManager fragmentManager = developmentSettingsDashboardFragment.getActivity().getFragmentManager();
        if (fragmentManager.findFragmentByTag("BluetoothA2dpHwOffloadReboot") == null) {
            final BluetoothA2dpHwOffloadRebootDialog bluetoothA2dpHwOffloadRebootDialog = new BluetoothA2dpHwOffloadRebootDialog();
            bluetoothA2dpHwOffloadRebootDialog.setTargetFragment((Fragment)developmentSettingsDashboardFragment, 0);
            bluetoothA2dpHwOffloadRebootDialog.show(fragmentManager, "BluetoothA2dpHwOffloadReboot");
        }
    }
    
    public int getMetricsCategory() {
        return 1441;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final OnA2dpHwDialogConfirmedListener onA2dpHwDialogConfirmedListener = (OnA2dpHwDialogConfirmedListener)this.getTargetFragment();
        if (onA2dpHwDialogConfirmedListener == null) {
            return;
        }
        if (n == -1) {
            onA2dpHwDialogConfirmedListener.onA2dpHwDialogConfirmed();
            ((PowerManager)this.getContext().getSystemService((Class)PowerManager.class)).reboot((String)null);
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage(2131886747).setTitle(2131886748).setPositiveButton(2131886746, (DialogInterface$OnClickListener)this).setNegativeButton(2131886745, (DialogInterface$OnClickListener)this).create();
    }
    
    public interface OnA2dpHwDialogConfirmedListener
    {
        void onA2dpHwDialogConfirmed();
    }
}
