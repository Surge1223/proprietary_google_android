package com.android.settings.development;

import android.support.v7.preference.ListPreference;
import android.content.Context;
import android.app.UiModeManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class DarkUIPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final UiModeManager mUiModeManager;
    
    public DarkUIPreferenceController(final Context context) {
        this(context, (UiModeManager)context.getSystemService((Class)UiModeManager.class));
    }
    
    DarkUIPreferenceController(final Context context, final UiModeManager mUiModeManager) {
        super(context);
        this.mUiModeManager = mUiModeManager;
    }
    
    private String modeToDescription(final int n) {
        final String[] stringArray = this.mContext.getResources().getStringArray(2130903108);
        if (n == 0) {
            return stringArray[0];
        }
        if (n != 2) {
            return stringArray[2];
        }
        return stringArray[1];
    }
    
    private int modeToInt(final String s) {
        final int hashCode = s.hashCode();
        int n = 0;
        Label_0071: {
            if (hashCode != 3521) {
                if (hashCode != 119527) {
                    if (hashCode == 3005871) {
                        if (s.equals("auto")) {
                            n = 0;
                            break Label_0071;
                        }
                    }
                }
                else if (s.equals("yes")) {
                    n = 1;
                    break Label_0071;
                }
            }
            else if (s.equals("no")) {
                n = 2;
                break Label_0071;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return 1;
            }
            case 1: {
                return 2;
            }
            case 0: {
                return 0;
            }
        }
    }
    
    private String modeToString(final int n) {
        if (n == 0) {
            return "auto";
        }
        if (n != 2) {
            return "no";
        }
        return "yes";
    }
    
    private void updateSummary(final Preference preference) {
        final int nightMode = this.mUiModeManager.getNightMode();
        ((ListPreference)preference).setValue(this.modeToString(nightMode));
        preference.setSummary(this.modeToDescription(nightMode));
    }
    
    @Override
    public String getPreferenceKey() {
        return "dark_ui_mode";
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mUiModeManager.setNightMode(this.modeToInt((String)o));
        this.updateSummary(preference);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateSummary(preference);
    }
}
