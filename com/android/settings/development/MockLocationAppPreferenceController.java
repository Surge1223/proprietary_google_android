package com.android.settings.development;

import android.content.Intent;
import android.support.v7.preference.Preference;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;
import android.app.AppOpsManager$PackageOps;
import android.app.AppOpsManager$OpEntry;
import android.content.pm.PackageManager;
import android.content.Context;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.app.AppOpsManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class MockLocationAppPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin, OnActivityResultListener
{
    private static final int[] MOCK_LOCATION_APP_OPS;
    private final AppOpsManager mAppsOpsManager;
    private final DevelopmentSettingsDashboardFragment mFragment;
    private final PackageManagerWrapper mPackageManager;
    
    static {
        MOCK_LOCATION_APP_OPS = new int[] { 58 };
    }
    
    public MockLocationAppPreferenceController(final Context context, final DevelopmentSettingsDashboardFragment mFragment) {
        super(context);
        this.mFragment = mFragment;
        this.mAppsOpsManager = (AppOpsManager)context.getSystemService("appops");
        this.mPackageManager = new PackageManagerWrapper(context.getPackageManager());
    }
    
    private String getAppLabel(String string) {
        try {
            final CharSequence applicationLabel = this.mPackageManager.getApplicationLabel(this.mPackageManager.getApplicationInfo(string, 512));
            if (applicationLabel != null) {
                string = applicationLabel.toString();
            }
            return string;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return string;
        }
    }
    
    private String getCurrentMockLocationApp() {
        final List packagesForOps = this.mAppsOpsManager.getPackagesForOps(MockLocationAppPreferenceController.MOCK_LOCATION_APP_OPS);
        if (packagesForOps != null) {
            final Iterator<AppOpsManager$PackageOps> iterator = packagesForOps.iterator();
            while (iterator.hasNext()) {
                if (((AppOpsManager$OpEntry)iterator.next().getOps().get(0)).getMode() == 0) {
                    return packagesForOps.get(0).getPackageName();
                }
            }
        }
        return null;
    }
    
    private void removeAllMockLocations() {
        final List packagesForOps = this.mAppsOpsManager.getPackagesForOps(MockLocationAppPreferenceController.MOCK_LOCATION_APP_OPS);
        if (packagesForOps == null) {
            return;
        }
        for (final AppOpsManager$PackageOps appOpsManager$PackageOps : packagesForOps) {
            if (((AppOpsManager$OpEntry)appOpsManager$PackageOps.getOps().get(0)).getMode() != 2) {
                this.removeMockLocationForApp(appOpsManager$PackageOps.getPackageName());
            }
        }
    }
    
    private void removeMockLocationForApp(final String s) {
        try {
            this.mAppsOpsManager.setMode(58, this.mPackageManager.getApplicationInfo(s, 512).uid, s, 2);
        }
        catch (PackageManager$NameNotFoundException ex) {}
    }
    
    private void updateMockLocation() {
        final String currentMockLocationApp = this.getCurrentMockLocationApp();
        if (!TextUtils.isEmpty((CharSequence)currentMockLocationApp)) {
            this.mPreference.setSummary(this.mContext.getResources().getString(2131888322, new Object[] { this.getAppLabel(currentMockLocationApp) }));
        }
        else {
            this.mPreference.setSummary(this.mContext.getResources().getString(2131888321));
        }
    }
    
    private void writeMockLocation(final String s) {
        this.removeAllMockLocations();
        if (!TextUtils.isEmpty((CharSequence)s)) {
            try {
                this.mAppsOpsManager.setMode(58, this.mPackageManager.getApplicationInfo(s, 512).uid, s, 0);
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "mock_location_app";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return false;
        }
        final Intent intent = new Intent(this.mContext, (Class)AppPicker.class);
        intent.putExtra("com.android.settings.extra.REQUESTIING_PERMISSION", "android.permission.ACCESS_MOCK_LOCATION");
        this.mFragment.startActivityForResult(intent, 2);
        return true;
    }
    
    @Override
    public boolean onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 2 && n2 == -1) {
            this.writeMockLocation(intent.getAction());
            this.updateMockLocation();
            return true;
        }
        return false;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateMockLocation();
    }
}
