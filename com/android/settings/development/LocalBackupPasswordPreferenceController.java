package com.android.settings.development;

import android.os.RemoteException;
import android.support.v7.preference.Preference;
import android.app.backup.IBackupManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.os.UserManager;
import android.app.backup.IBackupManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class LocalBackupPasswordPreferenceController extends DeveloperOptionsPreferenceController implements PreferenceControllerMixin
{
    private final IBackupManager mBackupManager;
    private final UserManager mUserManager;
    
    public LocalBackupPasswordPreferenceController(final Context context) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mBackupManager = IBackupManager$Stub.asInterface(ServiceManager.getService("backup"));
    }
    
    private void updatePasswordSummary(final Preference preference) {
        preference.setEnabled(this.isAdminUser() && this.mBackupManager != null);
        if (this.mBackupManager == null) {
            return;
        }
        try {
            if (this.mBackupManager.hasBackupPassword()) {
                preference.setSummary(2131888029);
            }
            else {
                preference.setSummary(2131888030);
            }
        }
        catch (RemoteException ex) {}
    }
    
    @Override
    public String getPreferenceKey() {
        return "local_backup_password";
    }
    
    boolean isAdminUser() {
        return this.mUserManager.isAdminUser();
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updatePasswordSummary(preference);
    }
}
