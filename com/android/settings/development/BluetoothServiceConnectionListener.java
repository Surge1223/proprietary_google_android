package com.android.settings.development;

import android.bluetooth.BluetoothA2dp;

public interface BluetoothServiceConnectionListener
{
    void onBluetoothCodecUpdated();
    
    void onBluetoothServiceConnected(final BluetoothA2dp p0);
    
    void onBluetoothServiceDisconnected();
}
