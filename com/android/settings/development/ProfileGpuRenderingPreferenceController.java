package com.android.settings.development;

import com.android.settingslib.development.SystemPropPoker;
import android.support.v7.preference.ListPreference;
import android.text.TextUtils;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class ProfileGpuRenderingPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final String[] mListSummaries;
    private final String[] mListValues;
    
    public ProfileGpuRenderingPreferenceController(final Context context) {
        super(context);
        this.mListValues = context.getResources().getStringArray(2130903156);
        this.mListSummaries = context.getResources().getStringArray(2130903155);
    }
    
    private void updateTrackFrameTimeOptions() {
        final String value = SystemProperties.get("debug.hwui.profile", "");
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= this.mListValues.length) {
                break;
            }
            if (TextUtils.equals((CharSequence)value, (CharSequence)this.mListValues[n2])) {
                n3 = n2;
                break;
            }
            ++n2;
        }
        final ListPreference listPreference = (ListPreference)this.mPreference;
        listPreference.setValue(this.mListValues[n3]);
        listPreference.setSummary(this.mListSummaries[n3]);
    }
    
    private void writeTrackFrameTimeOptions(final Object o) {
        String string;
        if (o == null) {
            string = "";
        }
        else {
            string = o.toString();
        }
        SystemProperties.set("debug.hwui.profile", string);
        SystemPropPoker.getInstance().poke();
    }
    
    @Override
    public String getPreferenceKey() {
        return "track_frame_time";
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeTrackFrameTimeOptions(o);
        this.updateTrackFrameTimeOptions();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateTrackFrameTimeOptions();
    }
}
