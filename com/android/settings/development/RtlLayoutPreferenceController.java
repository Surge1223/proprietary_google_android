package com.android.settings.development;

import android.content.ContentResolver;
import com.android.internal.app.LocalePicker;
import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class RtlLayoutPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    
    public RtlLayoutPreferenceController(final Context context) {
        super(context);
    }
    
    private void writeToForceRtlLayoutSetting(final boolean b) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "debug.force_rtl", (int)(b ? 1 : 0));
        String s;
        if ((b ? 1 : 0) != 0) {
            s = Integer.toString(1);
        }
        else {
            s = Integer.toString(0);
        }
        SystemProperties.set("debug.force_rtl", s);
    }
    
    @Override
    public String getPreferenceKey() {
        return "force_rtl_layout_all_locales";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.writeToForceRtlLayoutSetting(false);
        this.updateLocales();
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.writeToForceRtlLayoutSetting((boolean)o);
        this.updateLocales();
        return true;
    }
    
    void updateLocales() {
        LocalePicker.updateLocales(this.mContext.getResources().getConfiguration().getLocales());
    }
    
    @Override
    public void updateState(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = false;
        final int int1 = Settings.Global.getInt(contentResolver, "debug.force_rtl", 0);
        final SwitchPreference switchPreference = (SwitchPreference)this.mPreference;
        if (int1 != 0) {
            checked = true;
        }
        switchPreference.setChecked(checked);
    }
}
