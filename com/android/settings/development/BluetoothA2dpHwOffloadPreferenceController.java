package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class BluetoothA2dpHwOffloadPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final DevelopmentSettingsDashboardFragment mFragment;
    
    public BluetoothA2dpHwOffloadPreferenceController(final Context context, final DevelopmentSettingsDashboardFragment mFragment) {
        super(context);
        this.mFragment = mFragment;
    }
    
    @Override
    public String getPreferenceKey() {
        return "bluetooth_disable_a2dp_hw_offload";
    }
    
    public void onA2dpHwDialogConfirmed() {
        SystemProperties.set("persist.bluetooth.a2dp_offload.disabled", Boolean.toString(SystemProperties.getBoolean("persist.bluetooth.a2dp_offload.disabled", false) ^ true));
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        if (SystemProperties.getBoolean("ro.bluetooth.a2dp_offload.supported", false)) {
            ((SwitchPreference)this.mPreference).setChecked(false);
            SystemProperties.set("persist.bluetooth.a2dp_offload.disabled", "false");
        }
        else {
            ((SwitchPreference)this.mPreference).setChecked(true);
            SystemProperties.set("persist.bluetooth.a2dp_offload.disabled", "true");
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        BluetoothA2dpHwOffloadRebootDialog.show(this.mFragment, this);
        return false;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (SystemProperties.getBoolean("ro.bluetooth.a2dp_offload.supported", false)) {
            ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("persist.bluetooth.a2dp_offload.disabled", false));
        }
        else {
            this.mPreference.setEnabled(false);
            ((SwitchPreference)this.mPreference).setChecked(true);
        }
    }
}
