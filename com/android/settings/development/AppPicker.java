package com.android.settings.development;

import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.content.pm.PackageInfo;
import java.util.Collection;
import java.util.Collections;
import android.content.pm.PackageManager;
import android.os.Build;
import android.content.pm.ApplicationInfo;
import java.util.ArrayList;
import java.util.List;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.content.Intent;
import android.view.View;
import android.widget.ListView;
import android.widget.ListAdapter;
import android.content.Context;
import android.os.Bundle;
import java.text.Collator;
import java.util.Comparator;
import android.app.ListActivity;

public class AppPicker extends ListActivity
{
    private static final Comparator<MyApplicationInfo> sDisplayNameComparator;
    private AppListAdapter mAdapter;
    private boolean mDebuggableOnly;
    private String mPermissionName;
    
    static {
        sDisplayNameComparator = new Comparator<MyApplicationInfo>() {
            private final Collator collator = Collator.getInstance();
            
            @Override
            public final int compare(final MyApplicationInfo myApplicationInfo, final MyApplicationInfo myApplicationInfo2) {
                return this.collator.compare(myApplicationInfo.label, myApplicationInfo2.label);
            }
        };
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mPermissionName = this.getIntent().getStringExtra("com.android.settings.extra.REQUESTIING_PERMISSION");
        this.mDebuggableOnly = this.getIntent().getBooleanExtra("com.android.settings.extra.DEBUGGABLE", false);
        this.mAdapter = new AppListAdapter((Context)this);
        if (this.mAdapter.getCount() <= 0) {
            this.finish();
        }
        else {
            this.setListAdapter((ListAdapter)this.mAdapter);
        }
    }
    
    protected void onListItemClick(final ListView listView, final View view, final int n, final long n2) {
        final MyApplicationInfo myApplicationInfo = (MyApplicationInfo)this.mAdapter.getItem(n);
        final Intent intent = new Intent();
        if (myApplicationInfo.info != null) {
            intent.setAction(myApplicationInfo.info.packageName);
        }
        this.setResult(-1, intent);
        this.finish();
    }
    
    protected void onResume() {
        super.onResume();
    }
    
    protected void onStop() {
        super.onStop();
    }
    
    public class AppListAdapter extends ArrayAdapter<MyApplicationInfo>
    {
        private final LayoutInflater mInflater;
        private final List<MyApplicationInfo> mPackageInfoList;
        
        public AppListAdapter(final Context context) {
            super(context, 0);
            this.mPackageInfoList = new ArrayList<MyApplicationInfo>();
            this.mInflater = (LayoutInflater)context.getSystemService("layout_inflater");
            final List installedApplications = context.getPackageManager().getInstalledApplications(0);
            for (int i = 0; i < installedApplications.size(); ++i) {
                final ApplicationInfo info = installedApplications.get(i);
                if (info.uid != 1000) {
                    if (!AppPicker.this.mDebuggableOnly || (info.flags & 0x2) != 0x0 || !"user".equals(Build.TYPE)) {
                        if (AppPicker.this.mPermissionName != null) {
                            final boolean b = false;
                            try {
                                final PackageInfo packageInfo = AppPicker.this.getPackageManager().getPackageInfo(info.packageName, 4096);
                                if (packageInfo.requestedPermissions == null) {
                                    continue;
                                }
                                final String[] requestedPermissions = packageInfo.requestedPermissions;
                                final int length = requestedPermissions.length;
                                int n = 0;
                                boolean b2;
                                while (true) {
                                    b2 = b;
                                    if (n >= length) {
                                        break;
                                    }
                                    if (requestedPermissions[n].equals(AppPicker.this.mPermissionName)) {
                                        b2 = true;
                                        break;
                                    }
                                    ++n;
                                }
                                if (!b2) {
                                    continue;
                                }
                            }
                            catch (PackageManager$NameNotFoundException ex) {
                                continue;
                            }
                        }
                        final MyApplicationInfo myApplicationInfo = new MyApplicationInfo();
                        myApplicationInfo.info = info;
                        myApplicationInfo.label = myApplicationInfo.info.loadLabel(AppPicker.this.getPackageManager()).toString();
                        this.mPackageInfoList.add(myApplicationInfo);
                    }
                }
            }
            Collections.sort(this.mPackageInfoList, AppPicker.sDisplayNameComparator);
            final MyApplicationInfo myApplicationInfo2 = new MyApplicationInfo();
            myApplicationInfo2.label = context.getText(2131888407);
            this.mPackageInfoList.add(0, myApplicationInfo2);
            this.addAll((Collection)this.mPackageInfoList);
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final AppViewHolder orRecycle = AppViewHolder.createOrRecycle(this.mInflater, view);
            final View rootView = orRecycle.rootView;
            final MyApplicationInfo myApplicationInfo = (MyApplicationInfo)this.getItem(n);
            orRecycle.appName.setText(myApplicationInfo.label);
            if (myApplicationInfo.info != null) {
                orRecycle.appIcon.setImageDrawable(myApplicationInfo.info.loadIcon(AppPicker.this.getPackageManager()));
                orRecycle.summary.setText((CharSequence)myApplicationInfo.info.packageName);
            }
            else {
                orRecycle.appIcon.setImageDrawable((Drawable)null);
                orRecycle.summary.setText((CharSequence)"");
            }
            orRecycle.disabled.setVisibility(8);
            return rootView;
        }
    }
    
    class MyApplicationInfo
    {
        ApplicationInfo info;
        CharSequence label;
    }
}
