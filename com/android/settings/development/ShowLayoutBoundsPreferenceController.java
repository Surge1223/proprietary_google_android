package com.android.settings.development;

import com.android.settingslib.development.SystemPropPoker;
import android.support.v14.preference.SwitchPreference;
import android.os.SystemProperties;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class ShowLayoutBoundsPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    public ShowLayoutBoundsPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "debug_layout";
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        SystemProperties.set("debug.layout", Boolean.toString(false));
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        String s;
        if (o) {
            s = Boolean.toString(true);
        }
        else {
            s = Boolean.toString(false);
        }
        SystemProperties.set("debug.layout", s);
        SystemPropPoker.getInstance().poke();
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(SystemProperties.getBoolean("debug.layout", false));
    }
}
