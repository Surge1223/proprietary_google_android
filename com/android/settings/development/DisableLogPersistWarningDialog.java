package com.android.settings.development;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class DisableLogPersistWarningDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    public static void show(final LogPersistDialogHost logPersistDialogHost) {
        if (!(logPersistDialogHost instanceof Fragment)) {
            return;
        }
        final Fragment fragment = (Fragment)logPersistDialogHost;
        final FragmentManager fragmentManager = fragment.getActivity().getFragmentManager();
        if (fragmentManager.findFragmentByTag("DisableLogPersistDlg") == null) {
            final DisableLogPersistWarningDialog disableLogPersistWarningDialog = new DisableLogPersistWarningDialog();
            disableLogPersistWarningDialog.setTargetFragment(fragment, 0);
            disableLogPersistWarningDialog.show(fragmentManager, "DisableLogPersistDlg");
        }
    }
    
    public int getMetricsCategory() {
        return 1225;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final LogPersistDialogHost logPersistDialogHost = (LogPersistDialogHost)this.getTargetFragment();
        if (logPersistDialogHost == null) {
            return;
        }
        if (n == -1) {
            logPersistDialogHost.onDisableLogPersistDialogConfirmed();
        }
        else {
            logPersistDialogHost.onDisableLogPersistDialogRejected();
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887373).setMessage(2131887372).setPositiveButton(17039379, (DialogInterface$OnClickListener)this).setNegativeButton(17039369, (DialogInterface$OnClickListener)this).create();
    }
}
