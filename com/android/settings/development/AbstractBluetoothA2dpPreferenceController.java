package com.android.settings.development;

import android.bluetooth.BluetoothCodecStatus;
import android.bluetooth.BluetoothCodecConfig;
import android.bluetooth.BluetoothDevice;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.bluetooth.BluetoothA2dp;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public abstract class AbstractBluetoothA2dpPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, BluetoothServiceConnectionListener, LifecycleObserver, OnDestroy
{
    static final int STREAMING_LABEL_ID = 2131886889;
    protected BluetoothA2dp mBluetoothA2dp;
    protected final BluetoothA2dpConfigStore mBluetoothA2dpConfigStore;
    private final String[] mListSummaries;
    private final String[] mListValues;
    protected ListPreference mPreference;
    
    public AbstractBluetoothA2dpPreferenceController(final Context context, final Lifecycle lifecycle, final BluetoothA2dpConfigStore mBluetoothA2dpConfigStore) {
        super(context);
        this.mBluetoothA2dpConfigStore = mBluetoothA2dpConfigStore;
        this.mListValues = this.getListValues();
        this.mListSummaries = this.getListSummaries();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = (ListPreference)preferenceScreen.findPreference(this.getPreferenceKey())).setValue(this.mListValues[this.getDefaultIndex()]);
        this.mPreference.setSummary(this.mListSummaries[this.getDefaultIndex()]);
    }
    
    BluetoothCodecConfig getCodecConfig(final BluetoothDevice bluetoothDevice) {
        if (this.mBluetoothA2dp != null) {
            final BluetoothCodecStatus codecStatus = this.mBluetoothA2dp.getCodecStatus(bluetoothDevice);
            if (codecStatus != null) {
                return codecStatus.getCodecConfig();
            }
        }
        return null;
    }
    
    protected abstract int getCurrentA2dpSettingIndex(final BluetoothCodecConfig p0);
    
    protected abstract int getDefaultIndex();
    
    protected abstract String[] getListSummaries();
    
    protected abstract String[] getListValues();
    
    @Override
    public void onBluetoothCodecUpdated() {
    }
    
    @Override
    public void onBluetoothServiceConnected(final BluetoothA2dp mBluetoothA2dp) {
        this.mBluetoothA2dp = mBluetoothA2dp;
        this.updateState(this.mPreference);
    }
    
    @Override
    public void onBluetoothServiceDisconnected() {
        this.mBluetoothA2dp = null;
    }
    
    @Override
    public void onDestroy() {
        this.mBluetoothA2dp = null;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (this.mBluetoothA2dp == null) {
            return false;
        }
        this.writeConfigurationValues(o);
        final BluetoothCodecConfig codecConfig = this.mBluetoothA2dpConfigStore.createCodecConfig();
        synchronized (this.mBluetoothA2dpConfigStore) {
            if (this.mBluetoothA2dp != null) {
                this.setCodecConfigPreference(null, codecConfig);
            }
            // monitorexit(this.mBluetoothA2dpConfigStore)
            final int indexOfValue = this.mPreference.findIndexOfValue(o.toString());
            if (indexOfValue == this.getDefaultIndex()) {
                this.mPreference.setSummary(this.mListSummaries[indexOfValue]);
            }
            else {
                this.mPreference.setSummary(this.mContext.getResources().getString(2131886889, new Object[] { this.mListSummaries[indexOfValue] }));
            }
            return true;
        }
    }
    
    void setCodecConfigPreference(final BluetoothDevice bluetoothDevice, final BluetoothCodecConfig bluetoothCodecConfig) {
        this.mBluetoothA2dp.setCodecConfigPreference(bluetoothDevice, bluetoothCodecConfig);
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.getCodecConfig(null) != null) {
            if (this.mPreference != null) {
                synchronized (this.mBluetoothA2dpConfigStore) {
                    final BluetoothCodecConfig codecConfig = this.getCodecConfig(null);
                    // monitorexit(this.mBluetoothA2dpConfigStore)
                    final int currentA2dpSettingIndex = this.getCurrentA2dpSettingIndex(codecConfig);
                    this.mPreference.setValue(this.mListValues[currentA2dpSettingIndex]);
                    if (currentA2dpSettingIndex == this.getDefaultIndex()) {
                        this.mPreference.setSummary(this.mListSummaries[currentA2dpSettingIndex]);
                    }
                    else {
                        this.mPreference.setSummary(this.mContext.getResources().getString(2131886889, new Object[] { this.mListSummaries[currentA2dpSettingIndex] }));
                    }
                    this.writeConfigurationValues(this.mListValues[currentA2dpSettingIndex]);
                }
            }
        }
    }
    
    protected abstract void writeConfigurationValues(final Object p0);
}
