package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import android.os.Build;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class NotificationChannelWarningsPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final int DEBUGGING_DISABLED = 0;
    static final int DEBUGGING_ENABLED = 1;
    static final int SETTING_VALUE_OFF = 0;
    static final int SETTING_VALUE_ON = 1;
    
    public NotificationChannelWarningsPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "show_notification_channel_warnings";
    }
    
    boolean isDebuggable() {
        return Build.IS_DEBUGGABLE;
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        Settings.Global.putInt(this.mContext.getContentResolver(), "show_notification_channel_warnings", 0);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "show_notification_channel_warnings", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((SwitchPreference)this.mPreference).setChecked(Settings.Global.getInt(this.mContext.getContentResolver(), "show_notification_channel_warnings", (int)(this.isDebuggable() ? 1 : 0)) != 0);
    }
}
