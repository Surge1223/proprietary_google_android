package com.android.settings.development;

import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceScreen;
import android.content.pm.PackageManager;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.development.DeveloperOptionsPreferenceController;

public class LocalTerminalPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    static final String TERMINAL_APP_PACKAGE = "com.android.terminal";
    private PackageManagerWrapper mPackageManager;
    private UserManager mUserManager;
    
    public LocalTerminalPreferenceController(final Context context) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
    }
    
    private boolean isEnabled() {
        return this.mUserManager.isAdminUser();
    }
    
    private boolean isPackageInstalled(final String s) {
        boolean b = false;
        try {
            if (this.mContext.getPackageManager().getPackageInfo(s, 0) != null) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPackageManager = this.getPackageManagerWrapper();
        if (this.isAvailable() && !this.isEnabled()) {
            this.mPreference.setEnabled(false);
        }
    }
    
    PackageManagerWrapper getPackageManagerWrapper() {
        return new PackageManagerWrapper(this.mContext.getPackageManager());
    }
    
    @Override
    public String getPreferenceKey() {
        return "enable_terminal";
    }
    
    @Override
    public boolean isAvailable() {
        return this.isPackageInstalled("com.android.terminal");
    }
    
    @Override
    protected void onDeveloperOptionsSwitchDisabled() {
        super.onDeveloperOptionsSwitchDisabled();
        this.mPackageManager.setApplicationEnabledSetting("com.android.terminal", 0, 0);
        ((SwitchPreference)this.mPreference).setChecked(false);
    }
    
    @Override
    protected void onDeveloperOptionsSwitchEnabled() {
        if (this.isEnabled()) {
            this.mPreference.setEnabled(true);
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        final PackageManagerWrapper mPackageManager = this.mPackageManager;
        int n;
        if (booleanValue) {
            n = 1;
        }
        else {
            n = 0;
        }
        mPackageManager.setApplicationEnabledSetting("com.android.terminal", n, 0);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int applicationEnabledSetting = this.mPackageManager.getApplicationEnabledSetting("com.android.terminal");
        boolean checked = true;
        if (applicationEnabledSetting != 1) {
            checked = false;
        }
        ((SwitchPreference)this.mPreference).setChecked(checked);
    }
}
