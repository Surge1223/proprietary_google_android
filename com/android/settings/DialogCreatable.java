package com.android.settings;

import android.app.Dialog;

public interface DialogCreatable
{
    int getDialogMetricsCategory(final int p0);
    
    Dialog onCreateDialog(final int p0);
}
