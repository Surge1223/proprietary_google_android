package com.android.settings.survey;

import android.content.Context;
import com.android.settings.overlay.FeatureFactory;
import android.app.Activity;
import com.android.settings.overlay.SurveyFeatureProvider;
import android.content.BroadcastReceiver;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class SurveyMixin implements LifecycleObserver, OnPause, OnResume
{
    private Fragment mFragment;
    private String mName;
    private BroadcastReceiver mReceiver;
    
    public SurveyMixin(final Fragment mFragment, final String mName) {
        this.mName = mName;
        this.mFragment = mFragment;
    }
    
    @Override
    public void onPause() {
        final Activity activity = this.mFragment.getActivity();
        if (this.mReceiver != null && activity != null) {
            SurveyFeatureProvider.unregisterReceiver(activity, this.mReceiver);
            this.mReceiver = null;
        }
    }
    
    @Override
    public void onResume() {
        final Activity activity = this.mFragment.getActivity();
        if (activity != null) {
            final SurveyFeatureProvider surveyFeatureProvider = FeatureFactory.getFactory((Context)activity).getSurveyFeatureProvider((Context)activity);
            if (surveyFeatureProvider != null) {
                final String surveyId = surveyFeatureProvider.getSurveyId((Context)activity, this.mName);
                if (surveyFeatureProvider.getSurveyExpirationDate((Context)activity, surveyId) <= -1L) {
                    this.mReceiver = surveyFeatureProvider.createAndRegisterReceiver(activity);
                    surveyFeatureProvider.downloadSurvey(activity, surveyId, null);
                }
                else {
                    surveyFeatureProvider.showSurveyIfAvailable(activity, surveyId);
                }
            }
        }
    }
}
