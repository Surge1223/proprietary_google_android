package com.android.settings.fuelgauge;

import com.android.internal.app.AlertController$AlertParams;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.os.IDeviceIdleController$Stub;
import android.os.ServiceManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.content.DialogInterface;
import android.os.IDeviceIdleController;
import android.content.DialogInterface$OnClickListener;
import com.android.internal.app.AlertActivity;

public class RequestIgnoreBatteryOptimizations extends AlertActivity implements DialogInterface$OnClickListener
{
    IDeviceIdleController mDeviceIdleService;
    String mPackageName;
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            try {
                this.mDeviceIdleService.addPowerSaveWhitelistApp(this.mPackageName);
            }
            catch (RemoteException ex) {
                Log.w("RequestIgnoreBatteryOptimizations", "Unable to reach IDeviceIdleController", (Throwable)ex);
            }
            this.setResult(-1);
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mDeviceIdleService = IDeviceIdleController$Stub.asInterface(ServiceManager.getService("deviceidle"));
        final Uri data = this.getIntent().getData();
        if (data == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No data supplied for IGNORE_BATTERY_OPTIMIZATION_SETTINGS in: ");
            sb.append(this.getIntent());
            Log.w("RequestIgnoreBatteryOptimizations", sb.toString());
            this.finish();
            return;
        }
        this.mPackageName = data.getSchemeSpecificPart();
        if (this.mPackageName == null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("No data supplied for IGNORE_BATTERY_OPTIMIZATION_SETTINGS in: ");
            sb2.append(this.getIntent());
            Log.w("RequestIgnoreBatteryOptimizations", sb2.toString());
            this.finish();
            return;
        }
        if (((PowerManager)this.getSystemService((Class)PowerManager.class)).isIgnoringBatteryOptimizations(this.mPackageName)) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Not should prompt, already ignoring optimizations: ");
            sb3.append(this.mPackageName);
            Log.i("RequestIgnoreBatteryOptimizations", sb3.toString());
            this.finish();
            return;
        }
        try {
            final ApplicationInfo applicationInfo = this.getPackageManager().getApplicationInfo(this.mPackageName, 0);
            if (this.getPackageManager().checkPermission("android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS", this.mPackageName) != 0) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Requested package ");
                sb4.append(this.mPackageName);
                sb4.append(" does not hold permission ");
                sb4.append("android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS");
                Log.w("RequestIgnoreBatteryOptimizations", sb4.toString());
                this.finish();
                return;
            }
            final AlertController$AlertParams mAlertParams = this.mAlertParams;
            mAlertParams.mTitle = this.getText(2131887850);
            mAlertParams.mMessage = this.getString(2131887849, new Object[] { applicationInfo.loadLabel(this.getPackageManager()) });
            mAlertParams.mPositiveButtonText = this.getText(2131886285);
            mAlertParams.mNegativeButtonText = this.getText(2131887370);
            mAlertParams.mPositiveButtonListener = (DialogInterface$OnClickListener)this;
            ((RequestIgnoreBatteryOptimizations)(mAlertParams.mNegativeButtonListener = (DialogInterface$OnClickListener)this)).setupAlert();
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Requested package doesn't exist: ");
            sb5.append(this.mPackageName);
            Log.w("RequestIgnoreBatteryOptimizations", sb5.toString());
            this.finish();
        }
    }
}
