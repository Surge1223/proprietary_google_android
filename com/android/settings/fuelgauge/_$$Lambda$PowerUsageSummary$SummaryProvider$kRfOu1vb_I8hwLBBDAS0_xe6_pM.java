package com.android.settings.fuelgauge;

import android.content.Context;
import com.android.settings.dashboard.SummaryLoader;

public final class _$$Lambda$PowerUsageSummary$SummaryProvider$kRfOu1vb_I8hwLBBDAS0_xe6_pM implements OnBatteryChangedListener
{
    @Override
    public final void onBatteryChanged(final int n) {
        PowerUsageSummary.SummaryProvider.lambda$new$0(this.f$0, n);
    }
}
