package com.android.settings.fuelgauge;

import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.graph.UsageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.support.v7.preference.Preference;

public class BatteryHistoryPreference extends Preference
{
    boolean hideSummary;
    BatteryInfo mBatteryInfo;
    private CharSequence mSummary;
    private TextView mSummaryView;
    
    public BatteryHistoryPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setLayoutResource(2131558471);
        this.setSelectable(false);
    }
    
    public void hideBottomSummary() {
        if (this.mSummaryView != null) {
            this.mSummaryView.setVisibility(8);
        }
        this.hideSummary = true;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final long currentTimeMillis = System.currentTimeMillis();
        if (this.mBatteryInfo == null) {
            return;
        }
        ((TextView)preferenceViewHolder.findViewById(2131361978)).setText((CharSequence)this.mBatteryInfo.batteryPercentString);
        this.mSummaryView = (TextView)preferenceViewHolder.findViewById(2131361922);
        if (this.mSummary != null) {
            this.mSummaryView.setText(this.mSummary);
        }
        if (this.hideSummary) {
            this.mSummaryView.setVisibility(8);
        }
        final UsageView usageView = (UsageView)preferenceViewHolder.findViewById(2131361909);
        usageView.findViewById(2131362315).setAlpha(0.7f);
        this.mBatteryInfo.bindHistory(usageView, new BatteryInfo.BatteryDataParser[0]);
        BatteryUtils.logRuntime("BatteryHistoryPreference", "onBindViewHolder", currentTimeMillis);
    }
    
    public void setBottomSummary(final CharSequence mSummary) {
        this.mSummary = mSummary;
        if (this.mSummaryView != null) {
            this.mSummaryView.setVisibility(0);
            this.mSummaryView.setText(this.mSummary);
        }
        this.hideSummary = false;
    }
    
    public void setStats(final BatteryStatsHelper batteryStatsHelper) {
        BatteryInfo.getBatteryInfo(this.getContext(), (BatteryInfo.Callback)new _$$Lambda$BatteryHistoryPreference$OfN0YWKsw9YRrCqoEdP8dybAPU0(this), batteryStatsHelper.getStats(), false);
    }
}
