package com.android.settings.fuelgauge;

import android.content.Context;
import android.os.Bundle;
import android.content.res.Resources;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.app.Activity;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.ListPreference;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.app.usage.UsageStatsManager;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class InactiveApps extends SettingsPreferenceFragment implements OnPreferenceChangeListener
{
    private static final CharSequence[] SETTABLE_BUCKETS_NAMES;
    private static final CharSequence[] SETTABLE_BUCKETS_VALUES;
    private UsageStatsManager mUsageStats;
    
    static {
        SETTABLE_BUCKETS_NAMES = new CharSequence[] { "ACTIVE", "WORKING_SET", "FREQUENT", "RARE" };
        SETTABLE_BUCKETS_VALUES = new CharSequence[] { Integer.toString(10), Integer.toString(20), Integer.toString(30), Integer.toString(40) };
    }
    
    static String bucketToName(final int n) {
        if (n == 5) {
            return "EXEMPTED";
        }
        if (n == 10) {
            return "ACTIVE";
        }
        if (n == 20) {
            return "WORKING_SET";
        }
        if (n == 30) {
            return "FREQUENT";
        }
        if (n == 40) {
            return "RARE";
        }
        if (n != 50) {
            return "";
        }
        return "NEVER";
    }
    
    private void init() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        preferenceScreen.setOrderingAsAdded(false);
        final Activity activity = this.getActivity();
        final PackageManager packageManager = ((Context)activity).getPackageManager();
        ((Context)activity).getSystemService((Class)UsageStatsManager.class);
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        for (final ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
            final String packageName = resolveInfo.activityInfo.applicationInfo.packageName;
            final ListPreference listPreference = new ListPreference(this.getPrefContext());
            listPreference.setTitle(resolveInfo.loadLabel(packageManager));
            listPreference.setIcon(resolveInfo.loadIcon(packageManager));
            listPreference.setKey(packageName);
            listPreference.setEntries(InactiveApps.SETTABLE_BUCKETS_NAMES);
            listPreference.setEntryValues(InactiveApps.SETTABLE_BUCKETS_VALUES);
            this.updateSummary(listPreference);
            listPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            preferenceScreen.addPreference(listPreference);
        }
    }
    
    private void updateSummary(final ListPreference listPreference) {
        final Resources resources = this.getActivity().getResources();
        final int appStandbyBucket = this.mUsageStats.getAppStandbyBucket(listPreference.getKey());
        final String bucketToName = bucketToName(appStandbyBucket);
        boolean enabled = true;
        listPreference.setSummary(resources.getString(2131889220, new Object[] { bucketToName }));
        if (appStandbyBucket < 10 || appStandbyBucket > 40) {
            enabled = false;
        }
        if (enabled) {
            listPreference.setValue(Integer.toString(appStandbyBucket));
        }
        listPreference.setEnabled(enabled);
    }
    
    @Override
    public int getMetricsCategory() {
        return 238;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mUsageStats = (UsageStatsManager)this.getActivity().getSystemService((Class)UsageStatsManager.class);
        this.addPreferencesFromResource(2132082778);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mUsageStats.setAppStandbyBucket(preference.getKey(), Integer.parseInt((String)o));
        this.updateSummary((ListPreference)preference);
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.init();
    }
}
