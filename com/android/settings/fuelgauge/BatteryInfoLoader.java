package com.android.settings.fuelgauge;

import android.content.Context;
import com.android.internal.os.BatteryStatsHelper;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.utils.AsyncLoader;

public class BatteryInfoLoader extends AsyncLoader<BatteryInfo>
{
    @VisibleForTesting
    BatteryUtils batteryUtils;
    BatteryStatsHelper mStatsHelper;
    
    public BatteryInfoLoader(final Context context, final BatteryStatsHelper mStatsHelper) {
        super(context);
        this.mStatsHelper = mStatsHelper;
        this.batteryUtils = BatteryUtils.getInstance(context);
    }
    
    public BatteryInfo loadInBackground() {
        return this.batteryUtils.getBatteryInfo(this.mStatsHelper, "BatteryInfoLoader");
    }
    
    @Override
    protected void onDiscardResult(final BatteryInfo batteryInfo) {
    }
}
