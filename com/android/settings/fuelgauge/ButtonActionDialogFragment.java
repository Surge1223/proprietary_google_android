package com.android.settings.fuelgauge;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ButtonActionDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    int mId;
    
    private AlertDialog createDialog(final int n) {
        final Context context = this.getContext();
        switch (n) {
            default: {
                return null;
            }
            case 2: {
                return new AlertDialog$Builder(context).setTitle(2131887696).setMessage(2131887695).setPositiveButton(2131887460, (DialogInterface$OnClickListener)this).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
            case 0:
            case 1: {
                return new AlertDialog$Builder(context).setMessage(2131886347).setPositiveButton(2131886346, (DialogInterface$OnClickListener)this).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
        }
    }
    
    public static ButtonActionDialogFragment newInstance(final int n) {
        final ButtonActionDialogFragment buttonActionDialogFragment = new ButtonActionDialogFragment();
        final Bundle arguments = new Bundle(1);
        arguments.putInt("id", n);
        buttonActionDialogFragment.setArguments(arguments);
        return buttonActionDialogFragment;
    }
    
    public int getMetricsCategory() {
        return 558;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        ((AppButtonsDialogListener)this.getTargetFragment()).handleDialogClick(this.mId);
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        this.mId = this.getArguments().getInt("id");
        final AlertDialog dialog = this.createDialog(this.mId);
        if (dialog != null) {
            return (Dialog)dialog;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown id ");
        sb.append(this.mId);
        throw new IllegalArgumentException(sb.toString());
    }
    
    interface AppButtonsDialogListener
    {
        void handleDialogClick(final int p0);
    }
}
