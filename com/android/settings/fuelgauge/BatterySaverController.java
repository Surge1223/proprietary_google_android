package com.android.settings.fuelgauge;

import com.android.settings.dashboard.conditional.ConditionManager;
import com.android.settings.dashboard.conditional.BatterySaverCondition;
import com.android.settingslib.Utils;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.os.Handler;
import android.os.Looper;
import android.content.Context;
import android.os.PowerManager;
import android.database.ContentObserver;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class BatterySaverController extends BasePreferenceController implements BatterySaverListener, LifecycleObserver, OnStart, OnStop
{
    private static final String KEY_BATTERY_SAVER = "battery_saver_summary";
    private Preference mBatterySaverPref;
    private final BatterySaverReceiver mBatteryStateChangeReceiver;
    private final ContentObserver mObserver;
    private final PowerManager mPowerManager;
    
    public BatterySaverController(final Context context) {
        super(context, "battery_saver_summary");
        this.mObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
            public void onChange(final boolean b) {
                BatterySaverController.this.updateSummary();
            }
        };
        this.mPowerManager = (PowerManager)this.mContext.getSystemService("power");
        (this.mBatteryStateChangeReceiver = new BatterySaverReceiver(context)).setBatterySaverListener((BatterySaverReceiver.BatterySaverListener)this);
    }
    
    private void updateSummary() {
        this.mBatterySaverPref.setSummary(this.getSummary());
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mBatterySaverPref = preferenceScreen.findPreference("battery_saver_summary");
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public String getPreferenceKey() {
        return "battery_saver_summary";
    }
    
    @Override
    public CharSequence getSummary() {
        final boolean powerSaveMode = this.mPowerManager.isPowerSaveMode();
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "low_power_trigger_level", 0);
        if (powerSaveMode) {
            return this.mContext.getString(2131886624);
        }
        if (int1 != 0) {
            return this.mContext.getString(2131886622, new Object[] { Utils.formatPercentage(int1) });
        }
        return this.mContext.getString(2131886623);
    }
    
    @Override
    public void onBatteryChanged(final boolean b) {
    }
    
    @Override
    public void onPowerSaveModeChanged() {
        this.updateSummary();
    }
    
    @Override
    public void onStart() {
        this.mContext.getContentResolver().registerContentObserver(Settings.Global.getUriFor("low_power_trigger_level"), true, this.mObserver);
        this.mBatteryStateChangeReceiver.setListening(true);
        this.updateSummary();
    }
    
    @Override
    public void onStop() {
        this.mContext.getContentResolver().unregisterContentObserver(this.mObserver);
        this.mBatteryStateChangeReceiver.setListening(false);
    }
    
    void refreshConditionManager() {
        ConditionManager.get(this.mContext).getCondition(BatterySaverCondition.class).refreshState();
    }
}
