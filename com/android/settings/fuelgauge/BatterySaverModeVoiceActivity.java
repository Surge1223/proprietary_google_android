package com.android.settings.fuelgauge;

import android.util.Log;
import android.content.Context;
import com.android.settingslib.fuelgauge.BatterySaverUtils;
import android.content.Intent;
import com.android.settings.utils.VoiceSettingsActivity;

public class BatterySaverModeVoiceActivity extends VoiceSettingsActivity
{
    @Override
    protected boolean onVoiceSettingInteraction(final Intent intent) {
        if (intent.hasExtra("android.settings.extra.battery_saver_mode_enabled")) {
            if (BatterySaverUtils.setPowerSaveMode((Context)this, intent.getBooleanExtra("android.settings.extra.battery_saver_mode_enabled", false), true)) {
                this.notifySuccess(null);
            }
            else {
                Log.v("BatterySaverModeVoiceActivity", "Unable to set power mode");
                this.notifyFailure(null);
            }
        }
        else {
            Log.v("BatterySaverModeVoiceActivity", "Missing battery saver mode extra");
        }
        return true;
    }
}
