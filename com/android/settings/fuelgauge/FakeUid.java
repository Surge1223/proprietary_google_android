package com.android.settings.fuelgauge;

import android.os.BatteryStats$Uid$Wakelock;
import android.os.BatteryStats$Uid$Sensor;
import android.os.BatteryStats$Uid$Proc;
import android.os.BatteryStats$Uid$Pid;
import android.util.SparseArray;
import android.os.BatteryStats$Uid$Pkg;
import android.util.SparseIntArray;
import android.util.ArrayMap;
import android.os.BatteryStats$Counter;
import android.os.BatteryStats$ControllerActivityCounter;
import android.os.BatteryStats$Timer;
import android.os.BatteryStats$Uid;

public class FakeUid extends BatteryStats$Uid
{
    private final int mUid;
    
    public FakeUid(final int mUid) {
        this.mUid = mUid;
    }
    
    public BatteryStats$Timer getAggregatedPartialWakelockTimer() {
        return null;
    }
    
    public BatteryStats$Timer getAudioTurnedOnTimer() {
        return null;
    }
    
    public BatteryStats$ControllerActivityCounter getBluetoothControllerActivity() {
        return null;
    }
    
    public BatteryStats$Timer getBluetoothScanBackgroundTimer() {
        return null;
    }
    
    public BatteryStats$Counter getBluetoothScanResultBgCounter() {
        return null;
    }
    
    public BatteryStats$Counter getBluetoothScanResultCounter() {
        return null;
    }
    
    public BatteryStats$Timer getBluetoothScanTimer() {
        return null;
    }
    
    public BatteryStats$Timer getBluetoothUnoptimizedScanBackgroundTimer() {
        return null;
    }
    
    public BatteryStats$Timer getBluetoothUnoptimizedScanTimer() {
        return null;
    }
    
    public BatteryStats$Timer getCameraTurnedOnTimer() {
        return null;
    }
    
    public long getCpuActiveTime() {
        return 0L;
    }
    
    public long[] getCpuClusterTimes() {
        return null;
    }
    
    public long[] getCpuFreqTimes(final int n) {
        return null;
    }
    
    public long[] getCpuFreqTimes(final int n, final int n2) {
        return null;
    }
    
    public void getDeferredJobsCheckinLineLocked(final StringBuilder sb, final int n) {
    }
    
    public void getDeferredJobsLineLocked(final StringBuilder sb, final int n) {
    }
    
    public BatteryStats$Timer getFlashlightTurnedOnTimer() {
        return null;
    }
    
    public BatteryStats$Timer getForegroundActivityTimer() {
        return null;
    }
    
    public BatteryStats$Timer getForegroundServiceTimer() {
        return null;
    }
    
    public long getFullWifiLockTime(final long n, final int n2) {
        return 0L;
    }
    
    public ArrayMap<String, SparseIntArray> getJobCompletionStats() {
        return null;
    }
    
    public ArrayMap<String, ? extends BatteryStats$Timer> getJobStats() {
        return null;
    }
    
    public int getMobileRadioActiveCount(final int n) {
        return 0;
    }
    
    public long getMobileRadioActiveTime(final int n) {
        return 0L;
    }
    
    public long getMobileRadioApWakeupCount(final int n) {
        return 0L;
    }
    
    public BatteryStats$ControllerActivityCounter getModemControllerActivity() {
        return null;
    }
    
    public BatteryStats$Timer getMulticastWakelockStats() {
        return null;
    }
    
    public long getNetworkActivityBytes(final int n, final int n2) {
        return 0L;
    }
    
    public long getNetworkActivityPackets(final int n, final int n2) {
        return 0L;
    }
    
    public ArrayMap<String, ? extends BatteryStats$Uid$Pkg> getPackageStats() {
        return null;
    }
    
    public SparseArray<? extends BatteryStats$Uid$Pid> getPidStats() {
        return null;
    }
    
    public long getProcessStateTime(final int n, final long n2, final int n3) {
        return 0L;
    }
    
    public BatteryStats$Timer getProcessStateTimer(final int n) {
        return null;
    }
    
    public ArrayMap<String, ? extends BatteryStats$Uid$Proc> getProcessStats() {
        return null;
    }
    
    public long[] getScreenOffCpuFreqTimes(final int n) {
        return null;
    }
    
    public long[] getScreenOffCpuFreqTimes(final int n, final int n2) {
        return null;
    }
    
    public SparseArray<? extends BatteryStats$Uid$Sensor> getSensorStats() {
        return null;
    }
    
    public ArrayMap<String, ? extends BatteryStats$Timer> getSyncStats() {
        return null;
    }
    
    public long getSystemCpuTimeUs(final int n) {
        return 0L;
    }
    
    public long getTimeAtCpuSpeed(final int n, final int n2, final int n3) {
        return 0L;
    }
    
    public int getUid() {
        return this.mUid;
    }
    
    public int getUserActivityCount(final int n, final int n2) {
        return 0;
    }
    
    public long getUserCpuTimeUs(final int n) {
        return 0L;
    }
    
    public BatteryStats$Timer getVibratorOnTimer() {
        return null;
    }
    
    public BatteryStats$Timer getVideoTurnedOnTimer() {
        return null;
    }
    
    public ArrayMap<String, ? extends BatteryStats$Uid$Wakelock> getWakelockStats() {
        return null;
    }
    
    public int getWifiBatchedScanCount(final int n, final int n2) {
        return 0;
    }
    
    public long getWifiBatchedScanTime(final int n, final long n2, final int n3) {
        return 0L;
    }
    
    public BatteryStats$ControllerActivityCounter getWifiControllerActivity() {
        return null;
    }
    
    public long getWifiMulticastTime(final long n, final int n2) {
        return 0L;
    }
    
    public long getWifiRadioApWakeupCount(final int n) {
        return 0L;
    }
    
    public long getWifiRunningTime(final long n, final int n2) {
        return 0L;
    }
    
    public long getWifiScanActualTime(final long n) {
        return 0L;
    }
    
    public int getWifiScanBackgroundCount(final int n) {
        return 0;
    }
    
    public long getWifiScanBackgroundTime(final long n) {
        return 0L;
    }
    
    public BatteryStats$Timer getWifiScanBackgroundTimer() {
        return null;
    }
    
    public int getWifiScanCount(final int n) {
        return 0;
    }
    
    public long getWifiScanTime(final long n, final int n2) {
        return 0L;
    }
    
    public BatteryStats$Timer getWifiScanTimer() {
        return null;
    }
    
    public boolean hasNetworkActivity() {
        return false;
    }
    
    public boolean hasUserActivity() {
        return false;
    }
    
    public void noteActivityPausedLocked(final long n) {
    }
    
    public void noteActivityResumedLocked(final long n) {
    }
    
    public void noteFullWifiLockAcquiredLocked(final long n) {
    }
    
    public void noteFullWifiLockReleasedLocked(final long n) {
    }
    
    public void noteUserActivityLocked(final int n) {
    }
    
    public void noteWifiBatchedScanStartedLocked(final int n, final long n2) {
    }
    
    public void noteWifiBatchedScanStoppedLocked(final long n) {
    }
    
    public void noteWifiMulticastDisabledLocked(final long n) {
    }
    
    public void noteWifiMulticastEnabledLocked(final long n) {
    }
    
    public void noteWifiRunningLocked(final long n) {
    }
    
    public void noteWifiScanStartedLocked(final long n) {
    }
    
    public void noteWifiScanStoppedLocked(final long n) {
    }
    
    public void noteWifiStoppedLocked(final long n) {
    }
}
