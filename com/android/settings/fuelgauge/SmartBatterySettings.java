package com.android.settings.fuelgauge;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.SettingsActivity;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class SmartBatterySettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082832;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final SettingsActivity settingsActivity, final InstrumentedPreferenceFragment instrumentedPreferenceFragment) {
        final ArrayList<RestrictAppPreferenceController> list = (ArrayList<RestrictAppPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(new SmartBatteryPreferenceController(context));
        if (settingsActivity != null && instrumentedPreferenceFragment != null) {
            list.add(new RestrictAppPreferenceController(instrumentedPreferenceFragment));
        }
        else {
            list.add(new RestrictAppPreferenceController(context));
        }
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, (SettingsActivity)this.getActivity(), this);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887786;
    }
    
    @Override
    protected String getLogTag() {
        return "SmartBatterySettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1281;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082832;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131889177);
    }
}
