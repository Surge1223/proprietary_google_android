package com.android.settings.fuelgauge;

public class Estimate
{
    public final long averageDischargeTime;
    public final long estimateMillis;
    public final boolean isBasedOnUsage;
    
    public Estimate(final long estimateMillis, final boolean isBasedOnUsage, final long averageDischargeTime) {
        this.estimateMillis = estimateMillis;
        this.isBasedOnUsage = isBasedOnUsage;
        this.averageDischargeTime = averageDischargeTime;
    }
}
