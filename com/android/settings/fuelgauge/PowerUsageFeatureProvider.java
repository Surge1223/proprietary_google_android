package com.android.settings.fuelgauge;

import com.android.internal.os.BatterySipper;
import android.util.SparseIntArray;
import android.content.Context;

public interface PowerUsageFeatureProvider
{
    String getAdvancedUsageScreenInfoString();
    
    boolean getEarlyWarningSignal(final Context p0, final String p1);
    
    Estimate getEnhancedBatteryPrediction(final Context p0);
    
    SparseIntArray getEnhancedBatteryPredictionCurve(final Context p0, final long p1);
    
    String getEnhancedEstimateDebugString(final String p0);
    
    String getOldEstimateDebugString(final String p0);
    
    boolean isEnhancedBatteryPredictionEnabled(final Context p0);
    
    boolean isEstimateDebugEnabled();
    
    boolean isSmartBatterySupported();
    
    boolean isTypeService(final BatterySipper p0);
    
    boolean isTypeSystem(final BatterySipper p0);
}
