package com.android.settings.fuelgauge;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.android.settings.Utils;
import android.os.UserHandle;
import com.android.settings.widget.AppCheckBoxPreference;
import android.support.v7.preference.CheckBoxPreference;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.fuelgauge.batterytip.tips.UnrestrictAppTip;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.fuelgauge.batterytip.BatteryTipDialogFragment;
import android.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.PreferenceGroup;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import com.android.settingslib.widget.FooterPreferenceMixin;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.List;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settings.fuelgauge.batterytip.BatteryTipPreferenceController;
import com.android.settings.dashboard.DashboardFragment;

public class RestrictedAppDetails extends DashboardFragment implements BatteryTipListener
{
    @VisibleForTesting
    static final String EXTRA_APP_INFO_LIST = "app_info_list";
    @VisibleForTesting
    List<AppInfo> mAppInfos;
    @VisibleForTesting
    BatteryUtils mBatteryUtils;
    private final FooterPreferenceMixin mFooterPreferenceMixin;
    @VisibleForTesting
    IconDrawableFactory mIconDrawableFactory;
    @VisibleForTesting
    PackageManager mPackageManager;
    @VisibleForTesting
    PreferenceGroup mRestrictedAppListGroup;
    
    public RestrictedAppDetails() {
        this.mFooterPreferenceMixin = new FooterPreferenceMixin(this, this.getLifecycle());
    }
    
    public static void startRestrictedAppDetails(final InstrumentedPreferenceFragment instrumentedPreferenceFragment, final List<AppInfo> list) {
        final Bundle arguments = new Bundle();
        arguments.putParcelableList("app_info_list", (List)list);
        new SubSettingLauncher(instrumentedPreferenceFragment.getContext()).setDestination(RestrictedAppDetails.class.getName()).setArguments(arguments).setTitle(2131888831).setSourceMetricsCategory(instrumentedPreferenceFragment.getMetricsCategory()).launch();
    }
    
    @VisibleForTesting
    BatteryTipDialogFragment createDialogFragment(final AppInfo appInfo, final boolean b) {
        BatteryTip batteryTip;
        if (b) {
            batteryTip = new RestrictAppTip(0, appInfo);
        }
        else {
            batteryTip = new UnrestrictAppTip(0, appInfo);
        }
        return BatteryTipDialogFragment.newInstance(batteryTip, this.getMetricsCategory());
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return null;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887785;
    }
    
    @VisibleForTesting
    String getKeyFromAppInfo(final AppInfo appInfo) {
        final StringBuilder sb = new StringBuilder();
        sb.append(appInfo.uid);
        sb.append(",");
        sb.append(appInfo.packageName);
        return sb.toString();
    }
    
    @Override
    protected String getLogTag() {
        return "RestrictedAppDetails";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1285;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082821;
    }
    
    @Override
    public void onBatteryTipHandled(final BatteryTip batteryTip) {
        final boolean checked = batteryTip instanceof RestrictAppTip;
        AppInfo unrestrictAppInfo;
        if (checked) {
            unrestrictAppInfo = ((RestrictAppTip)batteryTip).getRestrictAppList().get(0);
        }
        else {
            unrestrictAppInfo = ((UnrestrictAppTip)batteryTip).getUnrestrictAppInfo();
        }
        final CheckBoxPreference checkBoxPreference = (CheckBoxPreference)this.mRestrictedAppListGroup.findPreference(this.getKeyFromAppInfo(unrestrictAppInfo));
        if (checkBoxPreference != null) {
            checkBoxPreference.setChecked(checked);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131888830);
        this.mRestrictedAppListGroup = (PreferenceGroup)this.findPreference("restrict_app_list");
        this.mAppInfos = (List<AppInfo>)this.getArguments().getParcelableArrayList("app_info_list");
        this.mPackageManager = context.getPackageManager();
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(context);
        this.mBatteryUtils = BatteryUtils.getInstance(context);
        this.refreshUi();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        return super.onPreferenceTreeClick(preference);
    }
    
    @VisibleForTesting
    void refreshUi() {
        this.mRestrictedAppListGroup.removeAll();
        final Context prefContext = this.getPrefContext();
        for (int i = 0; i < this.mAppInfos.size(); ++i) {
            final AppCheckBoxPreference appCheckBoxPreference = new AppCheckBoxPreference(prefContext);
            final AppInfo appInfo = this.mAppInfos.get(i);
            try {
                final ApplicationInfo applicationInfoAsUser = this.mPackageManager.getApplicationInfoAsUser(appInfo.packageName, 0, UserHandle.getUserId(appInfo.uid));
                appCheckBoxPreference.setChecked(this.mBatteryUtils.isForceAppStandbyEnabled(appInfo.uid, appInfo.packageName));
                appCheckBoxPreference.setTitle(this.mPackageManager.getApplicationLabel(applicationInfoAsUser));
                appCheckBoxPreference.setIcon(Utils.getBadgedIcon(this.mIconDrawableFactory, this.mPackageManager, appInfo.packageName, UserHandle.getUserId(appInfo.uid)));
                appCheckBoxPreference.setKey(this.getKeyFromAppInfo(appInfo));
                appCheckBoxPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new _$$Lambda$RestrictedAppDetails$9OOxuAylZQQH_NDtRgh0ZoFLi_8(this, appInfo));
                this.mRestrictedAppListGroup.addPreference(appCheckBoxPreference);
            }
            catch (PackageManager$NameNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }
}
