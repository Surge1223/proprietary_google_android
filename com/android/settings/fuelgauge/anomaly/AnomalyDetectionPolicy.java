package com.android.settings.fuelgauge.anomaly;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.Arrays;
import android.net.Uri;
import android.util.Log;
import android.provider.Settings;
import android.content.Context;
import java.util.Set;
import android.util.KeyValueListParser;

public class AnomalyDetectionPolicy
{
    static final String KEY_ANOMALY_DETECTION_ENABLED = "anomaly_detection_enabled";
    static final String KEY_BLUETOOTH_SCAN_DETECTION_ENABLED = "bluetooth_scan_enabled";
    static final String KEY_BLUETOOTH_SCAN_THRESHOLD = "bluetooth_scan_threshold";
    static final String KEY_WAKELOCK_DETECTION_ENABLED = "wakelock_enabled";
    static final String KEY_WAKELOCK_THRESHOLD = "wakelock_threshold";
    static final String KEY_WAKEUP_ALARM_DETECTION_ENABLED = "wakeup_alarm_enabled";
    static final String KEY_WAKEUP_ALARM_THRESHOLD = "wakeup_alarm_threshold";
    static final String KEY_WAKEUP_BLACKLISTED_TAGS = "wakeup_blacklisted_tags";
    final boolean anomalyDetectionEnabled;
    final boolean bluetoothScanDetectionEnabled;
    public final long bluetoothScanThreshold;
    private final KeyValueListParser mParser;
    final boolean wakeLockDetectionEnabled;
    public final long wakeLockThreshold;
    final boolean wakeupAlarmDetectionEnabled;
    public final long wakeupAlarmThreshold;
    public final Set<String> wakeupBlacklistedTags;
    
    public AnomalyDetectionPolicy(final Context context) {
        this.mParser = new KeyValueListParser(',');
        final String string = Settings.Global.getString(context.getContentResolver(), "anomaly_detection_constants");
        try {
            this.mParser.setString(string);
        }
        catch (IllegalArgumentException ex) {
            Log.e("AnomalyDetectionPolicy", "Bad anomaly detection constants");
        }
        this.anomalyDetectionEnabled = this.mParser.getBoolean("anomaly_detection_enabled", false);
        this.wakeLockDetectionEnabled = this.mParser.getBoolean("wakelock_enabled", false);
        this.wakeupAlarmDetectionEnabled = this.mParser.getBoolean("wakeup_alarm_enabled", false);
        this.bluetoothScanDetectionEnabled = this.mParser.getBoolean("bluetooth_scan_enabled", false);
        this.wakeLockThreshold = this.mParser.getLong("wakelock_threshold", 3600000L);
        this.wakeupAlarmThreshold = this.mParser.getLong("wakeup_alarm_threshold", 10L);
        this.wakeupBlacklistedTags = this.parseStringSet("wakeup_blacklisted_tags", null);
        this.bluetoothScanThreshold = this.mParser.getLong("bluetooth_scan_threshold", 1800000L);
    }
    
    private Set<String> parseStringSet(String string, final Set<String> set) {
        string = this.mParser.getString(string, (String)null);
        if (string != null) {
            return Arrays.stream(string.split(":")).map((Function<? super String, ?>)_$$Lambda$AnomalyDetectionPolicy$MGZTkxm_LWhWFo0_u65o5bz97bA.INSTANCE).map((Function<? super Object, ?>)_$$Lambda$AnomalyDetectionPolicy$xFZhNZfuK_aveGITeM1VIXBhSVQ.INSTANCE).collect((Collector<? super Object, ?, Set<String>>)Collectors.toSet());
        }
        return set;
    }
    
    public boolean isAnomalyDetectorEnabled(final int n) {
        switch (n) {
            default: {
                return false;
            }
            case 2: {
                return this.bluetoothScanDetectionEnabled;
            }
            case 1: {
                return this.wakeupAlarmDetectionEnabled;
            }
            case 0: {
                return this.wakeLockDetectionEnabled;
            }
        }
    }
}
