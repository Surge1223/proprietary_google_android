package com.android.settings.fuelgauge.anomaly;

import android.os.Bundle;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.ArrayList;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.os.BatteryStatsHelper;
import java.util.List;
import com.android.settingslib.utils.AsyncLoader;

public class AnomalyLoader extends AsyncLoader<List<Anomaly>>
{
    AnomalyUtils mAnomalyUtils;
    private BatteryStatsHelper mBatteryStatsHelper;
    private String mPackageName;
    AnomalyDetectionPolicy mPolicy;
    private UserManager mUserManager;
    
    AnomalyLoader(final Context context, final BatteryStatsHelper mBatteryStatsHelper, final String mPackageName, final AnomalyDetectionPolicy mPolicy) {
        super(context);
        this.mBatteryStatsHelper = mBatteryStatsHelper;
        this.mPackageName = mPackageName;
        this.mAnomalyUtils = AnomalyUtils.getInstance(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mPolicy = mPolicy;
    }
    
    public AnomalyLoader(final Context context, final String s) {
        this(context, null, s, new AnomalyDetectionPolicy(context));
    }
    
    List<Anomaly> generateFakeData() {
        final ArrayList<Anomaly> list = new ArrayList<Anomaly>();
        final Context context = this.getContext();
        try {
            final int packageUid = context.getPackageManager().getPackageUid("com.android.settings", 0);
            list.add(new Anomaly.Builder().setUid(packageUid).setType(0).setPackageName("com.android.settings").setDisplayName("Settings").build());
            list.add(new Anomaly.Builder().setUid(packageUid).setType(1).setPackageName("com.android.settings").setDisplayName("Settings").build());
            list.add(new Anomaly.Builder().setUid(packageUid).setType(2).setPackageName("com.android.settings").setDisplayName("Settings").build());
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e("AnomalyLoader", "Cannot find package by name: com.android.settings", (Throwable)ex);
        }
        return list;
    }
    
    public List<Anomaly> loadInBackground() {
        if (this.mBatteryStatsHelper == null) {
            (this.mBatteryStatsHelper = new BatteryStatsHelper(this.getContext())).create((Bundle)null);
            this.mBatteryStatsHelper.refreshStats(0, this.mUserManager.getUserProfiles());
        }
        return this.mAnomalyUtils.detectAnomalies(this.mBatteryStatsHelper, this.mPolicy, this.mPackageName);
    }
    
    @Override
    protected void onDiscardResult(final List<Anomaly> list) {
    }
}
