package com.android.settings.fuelgauge.anomaly;

import android.content.Context;
import com.android.settings.fuelgauge.PowerUsageAnomalyDetails;
import android.app.Fragment;
import com.android.settings.SettingsActivity;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.fuelgauge.BatteryUtils;
import android.support.v7.preference.Preference;
import java.util.List;

public class AnomalySummaryPreferenceController
{
    List<Anomaly> mAnomalies;
    Preference mAnomalyPreference;
    BatteryUtils mBatteryUtils;
    private InstrumentedPreferenceFragment mFragment;
    private int mMetricsKey;
    private SettingsActivity mSettingsActivity;
    
    public AnomalySummaryPreferenceController(final SettingsActivity mSettingsActivity, final InstrumentedPreferenceFragment mFragment) {
        this.mFragment = mFragment;
        this.mSettingsActivity = mSettingsActivity;
        this.mAnomalyPreference = this.mFragment.getPreferenceScreen().findPreference("high_usage");
        this.mMetricsKey = mFragment.getMetricsCategory();
        this.mBatteryUtils = BatteryUtils.getInstance(mSettingsActivity.getApplicationContext());
        this.hideHighUsagePreference();
    }
    
    public void hideHighUsagePreference() {
        this.mAnomalyPreference.setVisible(false);
    }
    
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (this.mAnomalies != null && "high_usage".equals(preference.getKey())) {
            if (this.mAnomalies.size() == 1) {
                final AnomalyDialogFragment instance = AnomalyDialogFragment.newInstance(this.mAnomalies.get(0), this.mMetricsKey);
                instance.setTargetFragment((Fragment)this.mFragment, 0);
                instance.show(this.mFragment.getFragmentManager(), "HighUsagePreferenceController");
            }
            else {
                PowerUsageAnomalyDetails.startBatteryAbnormalPage(this.mSettingsActivity, this.mFragment, this.mAnomalies);
            }
            return true;
        }
        return false;
    }
    
    public void updateAnomalySummaryPreference(final List<Anomaly> mAnomalies) {
        final Context context = this.mFragment.getContext();
        this.mAnomalies = mAnomalies;
        if (!this.mAnomalies.isEmpty()) {
            this.mAnomalyPreference.setVisible(true);
            final int size = this.mAnomalies.size();
            final String quantityString = context.getResources().getQuantityString(2131755053, size, new Object[] { this.mAnomalies.get(0).displayName });
            String summary;
            if (size > 1) {
                summary = context.getString(2131886566, new Object[] { size });
            }
            else {
                summary = context.getString(this.mBatteryUtils.getSummaryResIdFromAnomalyType(this.mAnomalies.get(0).type));
            }
            this.mAnomalyPreference.setTitle(quantityString);
            this.mAnomalyPreference.setSummary(summary);
        }
        else {
            this.mAnomalyPreference.setVisible(false);
        }
    }
}
