package com.android.settings.fuelgauge.anomaly;

import android.content.Context;
import android.support.v7.preference.Preference;

public class AnomalyPreference extends Preference
{
    private Anomaly mAnomaly;
    
    public AnomalyPreference(final Context context, final Anomaly mAnomaly) {
        super(context);
        this.mAnomaly = mAnomaly;
        if (mAnomaly != null) {
            this.setTitle(mAnomaly.displayName);
        }
    }
    
    public Anomaly getAnomaly() {
        return this.mAnomaly;
    }
}
