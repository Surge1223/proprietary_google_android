package com.android.settings.fuelgauge.anomaly;

import android.util.Pair;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.fuelgauge.anomaly.checker.WakeLockAnomalyDetector;
import com.android.settings.fuelgauge.anomaly.checker.WakeupAlarmAnomalyDetector;
import com.android.settings.fuelgauge.anomaly.checker.BluetoothScanAnomalyDetector;
import com.android.settings.fuelgauge.anomaly.checker.AnomalyDetector;
import com.android.settings.fuelgauge.anomaly.action.ForceStopAction;
import com.android.settings.fuelgauge.anomaly.action.StopAndBackgroundCheckAction;
import com.android.settings.fuelgauge.anomaly.action.LocationCheckAction;
import com.android.settings.fuelgauge.anomaly.action.AnomalyAction;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import com.android.internal.os.BatteryStatsHelper;
import android.content.Context;
import android.util.SparseIntArray;

public class AnomalyUtils
{
    private static final SparseIntArray mMetricArray;
    private static AnomalyUtils sInstance;
    private Context mContext;
    
    static {
        (mMetricArray = new SparseIntArray()).append(0, 1235);
        AnomalyUtils.mMetricArray.append(1, 1236);
        AnomalyUtils.mMetricArray.append(2, 1237);
    }
    
    AnomalyUtils(final Context context) {
        this.mContext = context.getApplicationContext();
    }
    
    public static AnomalyUtils getInstance(final Context context) {
        if (AnomalyUtils.sInstance == null) {
            AnomalyUtils.sInstance = new AnomalyUtils(context);
        }
        return AnomalyUtils.sInstance;
    }
    
    public List<Anomaly> detectAnomalies(final BatteryStatsHelper batteryStatsHelper, final AnomalyDetectionPolicy anomalyDetectionPolicy, final String s) {
        final ArrayList<Anomaly> list = new ArrayList<Anomaly>();
        for (final int n : Anomaly.ANOMALY_TYPE_LIST) {
            if (anomalyDetectionPolicy.isAnomalyDetectorEnabled(n)) {
                list.addAll((Collection<?>)this.getAnomalyDetector(n).detectAnomalies(batteryStatsHelper, s));
            }
        }
        return list;
    }
    
    public AnomalyAction getAnomalyAction(final Anomaly anomaly) {
        switch (anomaly.type) {
            default: {
                return null;
            }
            case 2: {
                return new LocationCheckAction(this.mContext);
            }
            case 1: {
                if (anomaly.targetSdkVersion < 26 && (anomaly.targetSdkVersion >= 26 || !anomaly.backgroundRestrictionEnabled)) {
                    return new StopAndBackgroundCheckAction(this.mContext);
                }
                return new ForceStopAction(this.mContext);
            }
            case 0: {
                return new ForceStopAction(this.mContext);
            }
        }
    }
    
    public AnomalyDetector getAnomalyDetector(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 2: {
                return new BluetoothScanAnomalyDetector(this.mContext);
            }
            case 1: {
                return new WakeupAlarmAnomalyDetector(this.mContext);
            }
            case 0: {
                return new WakeLockAnomalyDetector(this.mContext);
            }
        }
    }
    
    public void logAnomalies(final MetricsFeatureProvider metricsFeatureProvider, final List<Anomaly> list, final int n) {
        for (int i = 0; i < list.size(); ++i) {
            this.logAnomaly(metricsFeatureProvider, list.get(i), n);
        }
    }
    
    public void logAnomaly(final MetricsFeatureProvider metricsFeatureProvider, final Anomaly anomaly, final int n) {
        metricsFeatureProvider.action(this.mContext, AnomalyUtils.mMetricArray.get(anomaly.type, 0), anomaly.packageName, Pair.create((Object)833, (Object)n), Pair.create((Object)1234, (Object)this.getAnomalyAction(anomaly).getActionType()));
    }
}
