package com.android.settings.fuelgauge.anomaly;

import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settings.fuelgauge.anomaly.action.AnomalyAction;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class AnomalyDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    Anomaly mAnomaly;
    AnomalyUtils mAnomalyUtils;
    
    public static AnomalyDialogFragment newInstance(final Anomaly anomaly, final int n) {
        final AnomalyDialogFragment anomalyDialogFragment = new AnomalyDialogFragment();
        final Bundle arguments = new Bundle(2);
        arguments.putParcelable("anomaly", (Parcelable)anomaly);
        arguments.putInt("metrics_key", n);
        anomalyDialogFragment.setArguments(arguments);
        return anomalyDialogFragment;
    }
    
    public int getMetricsCategory() {
        return 988;
    }
    
    void initAnomalyUtils() {
        this.mAnomalyUtils = AnomalyUtils.getInstance(this.getContext());
    }
    
    public void onClick(final DialogInterface dialogInterface, int int1) {
        final AnomalyDialogListener anomalyDialogListener = (AnomalyDialogListener)this.getTargetFragment();
        if (anomalyDialogListener == null) {
            return;
        }
        final AnomalyAction anomalyAction = this.mAnomalyUtils.getAnomalyAction(this.mAnomaly);
        int1 = this.getArguments().getInt("metrics_key");
        anomalyAction.handlePositiveAction(this.mAnomaly, int1);
        anomalyDialogListener.onAnomalyHandled(this.mAnomaly);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.initAnomalyUtils();
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Bundle arguments = this.getArguments();
        final Context context = this.getContext();
        final AnomalyUtils instance = AnomalyUtils.getInstance(context);
        this.mAnomaly = (Anomaly)arguments.getParcelable("anomaly");
        instance.logAnomaly(this.mMetricsFeatureProvider, this.mAnomaly, 988);
        final int actionType = this.mAnomalyUtils.getAnomalyAction(this.mAnomaly).getActionType();
        if (actionType == 0) {
            final AlertDialog$Builder setTitle = new AlertDialog$Builder(context).setTitle(2131887412);
            int n;
            if (this.mAnomaly.type == 0) {
                n = 2131887409;
            }
            else {
                n = 2131887410;
            }
            return (Dialog)setTitle.setMessage((CharSequence)this.getString(n, new Object[] { this.mAnomaly.displayName })).setPositiveButton(2131887411, (DialogInterface$OnClickListener)this).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
        }
        switch (actionType) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown type ");
                sb.append(this.mAnomaly.type);
                throw new IllegalArgumentException(sb.toString());
            }
            case 3: {
                return (Dialog)new AlertDialog$Builder(context).setTitle(2131887405).setMessage((CharSequence)this.getString(2131887403, new Object[] { this.mAnomaly.displayName })).setPositiveButton(2131887404, (DialogInterface$OnClickListener)this).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
            case 2: {
                return (Dialog)new AlertDialog$Builder(context).setTitle(2131887408).setMessage((CharSequence)this.getString(2131887406, new Object[] { this.mAnomaly.displayName })).setPositiveButton(2131887407, (DialogInterface$OnClickListener)this).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
        }
    }
    
    public interface AnomalyDialogListener
    {
        void onAnomalyHandled(final Anomaly p0);
    }
}
