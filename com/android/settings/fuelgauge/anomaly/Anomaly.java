package com.android.settings.fuelgauge.anomaly;

import java.util.Objects;
import android.text.TextUtils;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class Anomaly implements Parcelable
{
    public static final int[] ANOMALY_TYPE_LIST;
    public static final Parcelable.Creator CREATOR;
    public final boolean backgroundRestrictionEnabled;
    public final long bluetoothScanningTimeMs;
    public final CharSequence displayName;
    public final String packageName;
    public final int targetSdkVersion;
    public final int type;
    public final int uid;
    public final long wakelockTimeMs;
    public final int wakeupAlarmCount;
    
    static {
        ANOMALY_TYPE_LIST = new int[] { 0, 1, 2 };
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public Anomaly createFromParcel(final Parcel parcel) {
                return new Anomaly(parcel, null);
            }
            
            public Anomaly[] newArray(final int n) {
                return new Anomaly[n];
            }
        };
    }
    
    private Anomaly(final Parcel parcel) {
        this.type = parcel.readInt();
        this.uid = parcel.readInt();
        this.displayName = parcel.readCharSequence();
        this.packageName = parcel.readString();
        this.wakelockTimeMs = parcel.readLong();
        this.targetSdkVersion = parcel.readInt();
        this.backgroundRestrictionEnabled = parcel.readBoolean();
        this.wakeupAlarmCount = parcel.readInt();
        this.bluetoothScanningTimeMs = parcel.readLong();
    }
    
    private Anomaly(final Builder builder) {
        this.type = builder.mType;
        this.uid = builder.mUid;
        this.displayName = builder.mDisplayName;
        this.packageName = builder.mPackageName;
        this.wakelockTimeMs = builder.mWakeLockTimeMs;
        this.targetSdkVersion = builder.mTargetSdkVersion;
        this.backgroundRestrictionEnabled = builder.mBgRestrictionEnabled;
        this.bluetoothScanningTimeMs = builder.mBluetoothScanningTimeMs;
        this.wakeupAlarmCount = builder.mWakeupAlarmCount;
    }
    
    private String toAnomalyTypeText(final int n) {
        switch (n) {
            default: {
                return "";
            }
            case 2: {
                return "unoptimizedBluetoothScan";
            }
            case 1: {
                return "wakeupAlarm";
            }
            case 0: {
                return "wakelock";
            }
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof Anomaly)) {
            return false;
        }
        final Anomaly anomaly = (Anomaly)o;
        if (this.type != anomaly.type || this.uid != anomaly.uid || this.wakelockTimeMs != anomaly.wakelockTimeMs || !TextUtils.equals(this.displayName, anomaly.displayName) || !TextUtils.equals((CharSequence)this.packageName, (CharSequence)anomaly.packageName) || this.targetSdkVersion != anomaly.targetSdkVersion || this.backgroundRestrictionEnabled != anomaly.backgroundRestrictionEnabled || this.wakeupAlarmCount != anomaly.wakeupAlarmCount || this.bluetoothScanningTimeMs != anomaly.bluetoothScanningTimeMs) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.type, this.uid, this.displayName, this.packageName, this.wakelockTimeMs, this.targetSdkVersion, this.backgroundRestrictionEnabled, this.wakeupAlarmCount, this.bluetoothScanningTimeMs);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("type=");
        sb.append(this.toAnomalyTypeText(this.type));
        sb.append(" uid=");
        sb.append(this.uid);
        sb.append(" package=");
        sb.append(this.packageName);
        sb.append(" displayName=");
        sb.append((Object)this.displayName);
        sb.append(" wakelockTimeMs=");
        sb.append(this.wakelockTimeMs);
        sb.append(" wakeupAlarmCount=");
        sb.append(this.wakeupAlarmCount);
        sb.append(" bluetoothTimeMs=");
        sb.append(this.bluetoothScanningTimeMs);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.type);
        parcel.writeInt(this.uid);
        parcel.writeCharSequence(this.displayName);
        parcel.writeString(this.packageName);
        parcel.writeLong(this.wakelockTimeMs);
        parcel.writeInt(this.targetSdkVersion);
        parcel.writeBoolean(this.backgroundRestrictionEnabled);
        parcel.writeInt(this.wakeupAlarmCount);
        parcel.writeLong(this.bluetoothScanningTimeMs);
    }
    
    public static final class Builder
    {
        private boolean mBgRestrictionEnabled;
        private long mBluetoothScanningTimeMs;
        private CharSequence mDisplayName;
        private String mPackageName;
        private int mTargetSdkVersion;
        private int mType;
        private int mUid;
        private long mWakeLockTimeMs;
        private int mWakeupAlarmCount;
        
        public Anomaly build() {
            return new Anomaly(this, null);
        }
        
        public Builder setBackgroundRestrictionEnabled(final boolean mBgRestrictionEnabled) {
            this.mBgRestrictionEnabled = mBgRestrictionEnabled;
            return this;
        }
        
        public Builder setBluetoothScanningTimeMs(final long mBluetoothScanningTimeMs) {
            this.mBluetoothScanningTimeMs = mBluetoothScanningTimeMs;
            return this;
        }
        
        public Builder setDisplayName(final CharSequence mDisplayName) {
            this.mDisplayName = mDisplayName;
            return this;
        }
        
        public Builder setPackageName(final String mPackageName) {
            this.mPackageName = mPackageName;
            return this;
        }
        
        public Builder setTargetSdkVersion(final int mTargetSdkVersion) {
            this.mTargetSdkVersion = mTargetSdkVersion;
            return this;
        }
        
        public Builder setType(final int mType) {
            this.mType = mType;
            return this;
        }
        
        public Builder setUid(final int mUid) {
            this.mUid = mUid;
            return this;
        }
        
        public Builder setWakeLockTimeMs(final long mWakeLockTimeMs) {
            this.mWakeLockTimeMs = mWakeLockTimeMs;
            return this;
        }
        
        public Builder setWakeupAlarmCount(final int mWakeupAlarmCount) {
            this.mWakeupAlarmCount = mWakeupAlarmCount;
            return this;
        }
    }
}
