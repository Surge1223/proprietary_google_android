package com.android.settings.fuelgauge.anomaly.checker;

import java.util.Iterator;
import android.util.ArrayMap;
import android.os.BatteryStats$Counter;
import java.util.Map;
import android.os.BatteryStats$Uid$Pkg;
import android.os.BatteryStats$Uid;
import com.android.settings.Utils;
import com.android.internal.os.BatterySipper;
import java.util.ArrayList;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.fuelgauge.anomaly.AnomalyDetectionPolicy;
import java.util.Set;
import android.content.Context;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.settings.fuelgauge.anomaly.AnomalyUtils;

public class WakeupAlarmAnomalyDetector implements AnomalyDetector
{
    private AnomalyUtils mAnomalyUtils;
    BatteryUtils mBatteryUtils;
    private Context mContext;
    private long mWakeupAlarmThreshold;
    private Set<String> mWakeupBlacklistedTags;
    
    public WakeupAlarmAnomalyDetector(final Context context) {
        this(context, new AnomalyDetectionPolicy(context), AnomalyUtils.getInstance(context));
    }
    
    WakeupAlarmAnomalyDetector(final Context mContext, final AnomalyDetectionPolicy anomalyDetectionPolicy, final AnomalyUtils mAnomalyUtils) {
        this.mContext = mContext;
        this.mBatteryUtils = BatteryUtils.getInstance(mContext);
        this.mAnomalyUtils = mAnomalyUtils;
        this.mWakeupAlarmThreshold = anomalyDetectionPolicy.wakeupAlarmThreshold;
        this.mWakeupBlacklistedTags = anomalyDetectionPolicy.wakeupBlacklistedTags;
    }
    
    @Override
    public List<Anomaly> detectAnomalies(final BatteryStatsHelper batteryStatsHelper, final String s) {
        final List usageList = batteryStatsHelper.getUsageList();
        final ArrayList<Anomaly> list = new ArrayList<Anomaly>();
        final double n = this.mBatteryUtils.calculateRunningTimeBasedOnStatsType(batteryStatsHelper, 0) / 3600000.0;
        final int packageUid = this.mBatteryUtils.getPackageUid(s);
        if (n >= 1.0) {
            int i = 0;
            final int size = usageList.size();
            final List<BatterySipper> list2 = (List<BatterySipper>)usageList;
            while (i < size) {
                final BatterySipper batterySipper = list2.get(i);
                final BatteryStats$Uid uidObj = batterySipper.uidObj;
                if (uidObj != null && !this.mBatteryUtils.shouldHideSipper(batterySipper)) {
                    if (packageUid == -1 || packageUid == uidObj.getUid()) {
                        final int wakeupAlarmCount = (int)(this.getWakeupAlarmCountFromUid(uidObj) / n);
                        if (wakeupAlarmCount > this.mWakeupAlarmThreshold) {
                            final String packageName = this.mBatteryUtils.getPackageName(uidObj.getUid());
                            final CharSequence applicationLabel = Utils.getApplicationLabel(this.mContext, packageName);
                            final int targetSdkVersion = this.mBatteryUtils.getTargetSdkVersion(packageName);
                            final Anomaly build = new Anomaly.Builder().setUid(uidObj.getUid()).setType(1).setDisplayName(applicationLabel).setPackageName(packageName).setTargetSdkVersion(targetSdkVersion).setBackgroundRestrictionEnabled(this.mBatteryUtils.isBackgroundRestrictionEnabled(targetSdkVersion, uidObj.getUid(), packageName)).setWakeupAlarmCount(wakeupAlarmCount).build();
                            if (this.mAnomalyUtils.getAnomalyAction(build).isActionActive(build)) {
                                list.add(build);
                            }
                        }
                    }
                }
                ++i;
            }
        }
        return list;
    }
    
    int getWakeupAlarmCountFromUid(final BatteryStats$Uid batteryStats$Uid) {
        int n = 0;
        final ArrayMap packageStats = batteryStats$Uid.getPackageStats();
        for (int i = packageStats.size() - 1; i >= 0; --i) {
            for (final Map.Entry<Object, V> entry : ((BatteryStats$Uid$Pkg)packageStats.valueAt(i)).getWakeupAlarmStats().entrySet()) {
                if (this.mWakeupBlacklistedTags != null && this.mWakeupBlacklistedTags.contains(entry.getKey())) {
                    continue;
                }
                n += ((BatteryStats$Counter)entry.getValue()).getCountLocked(0);
            }
        }
        return n;
    }
}
