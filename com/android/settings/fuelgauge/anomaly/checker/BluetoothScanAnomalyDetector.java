package com.android.settings.fuelgauge.anomaly.checker;

import android.os.BatteryStats$Timer;
import android.os.BatteryStats$Uid;
import com.android.settings.Utils;
import com.android.internal.os.BatterySipper;
import android.os.SystemClock;
import java.util.ArrayList;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.fuelgauge.anomaly.AnomalyDetectionPolicy;
import android.content.Context;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.settings.fuelgauge.anomaly.AnomalyUtils;

public class BluetoothScanAnomalyDetector implements AnomalyDetector
{
    private AnomalyUtils mAnomalyUtils;
    BatteryUtils mBatteryUtils;
    private long mBluetoothScanningThreshold;
    private Context mContext;
    
    public BluetoothScanAnomalyDetector(final Context context) {
        this(context, new AnomalyDetectionPolicy(context), AnomalyUtils.getInstance(context));
    }
    
    BluetoothScanAnomalyDetector(final Context mContext, final AnomalyDetectionPolicy anomalyDetectionPolicy, final AnomalyUtils mAnomalyUtils) {
        this.mContext = mContext;
        this.mBatteryUtils = BatteryUtils.getInstance(mContext);
        this.mBluetoothScanningThreshold = anomalyDetectionPolicy.bluetoothScanThreshold;
        this.mAnomalyUtils = mAnomalyUtils;
    }
    
    @Override
    public List<Anomaly> detectAnomalies(final BatteryStatsHelper batteryStatsHelper, final String s) {
        final List usageList = batteryStatsHelper.getUsageList();
        final ArrayList<Anomaly> list = new ArrayList<Anomaly>();
        final int packageUid = this.mBatteryUtils.getPackageUid(s);
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        for (int i = 0; i < usageList.size(); ++i) {
            final BatterySipper batterySipper = usageList.get(i);
            final BatteryStats$Uid uidObj = batterySipper.uidObj;
            if (uidObj != null && !this.mBatteryUtils.shouldHideSipper(batterySipper)) {
                if (packageUid == -1 || packageUid == uidObj.getUid()) {
                    final long bluetoothUnoptimizedBgTimeMs = this.getBluetoothUnoptimizedBgTimeMs(uidObj, elapsedRealtime);
                    if (bluetoothUnoptimizedBgTimeMs > this.mBluetoothScanningThreshold) {
                        final String packageName = this.mBatteryUtils.getPackageName(uidObj.getUid());
                        final Anomaly build = new Anomaly.Builder().setUid(uidObj.getUid()).setType(2).setDisplayName(Utils.getApplicationLabel(this.mContext, packageName)).setPackageName(packageName).setBluetoothScanningTimeMs(bluetoothUnoptimizedBgTimeMs).build();
                        if (this.mAnomalyUtils.getAnomalyAction(build).isActionActive(build)) {
                            list.add(build);
                        }
                    }
                }
            }
        }
        return list;
    }
    
    public long getBluetoothUnoptimizedBgTimeMs(final BatteryStats$Uid batteryStats$Uid, long totalDurationMsLocked) {
        final BatteryStats$Timer bluetoothUnoptimizedScanBackgroundTimer = batteryStats$Uid.getBluetoothUnoptimizedScanBackgroundTimer();
        if (bluetoothUnoptimizedScanBackgroundTimer != null) {
            totalDurationMsLocked = bluetoothUnoptimizedScanBackgroundTimer.getTotalDurationMsLocked(totalDurationMsLocked);
        }
        else {
            totalDurationMsLocked = 0L;
        }
        return totalDurationMsLocked;
    }
}
