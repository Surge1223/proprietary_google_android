package com.android.settings.fuelgauge.anomaly.checker;

import android.os.BatteryStats$Timer;
import android.os.BatteryStats$Uid;
import com.android.settings.Utils;
import com.android.internal.os.BatterySipper;
import android.os.SystemClock;
import java.util.ArrayList;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.fuelgauge.anomaly.AnomalyDetectionPolicy;
import android.content.pm.PackageManager;
import android.content.Context;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.settings.fuelgauge.anomaly.AnomalyUtils;

public class WakeLockAnomalyDetector implements AnomalyDetector
{
    private AnomalyUtils mAnomalyUtils;
    BatteryUtils mBatteryUtils;
    private Context mContext;
    private PackageManager mPackageManager;
    long mWakeLockThresholdMs;
    
    public WakeLockAnomalyDetector(final Context context) {
        this(context, new AnomalyDetectionPolicy(context), AnomalyUtils.getInstance(context));
    }
    
    WakeLockAnomalyDetector(final Context mContext, final AnomalyDetectionPolicy anomalyDetectionPolicy, final AnomalyUtils mAnomalyUtils) {
        this.mContext = mContext;
        this.mPackageManager = mContext.getPackageManager();
        this.mBatteryUtils = BatteryUtils.getInstance(mContext);
        this.mAnomalyUtils = mAnomalyUtils;
        this.mWakeLockThresholdMs = anomalyDetectionPolicy.wakeLockThreshold;
    }
    
    @Override
    public List<Anomaly> detectAnomalies(final BatteryStatsHelper batteryStatsHelper, final String s) {
        final List usageList = batteryStatsHelper.getUsageList();
        final ArrayList<Anomaly> list = new ArrayList<Anomaly>();
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final int packageUid = this.mBatteryUtils.getPackageUid(s);
        for (int i = 0; i < usageList.size(); ++i) {
            final BatterySipper batterySipper = usageList.get(i);
            final BatteryStats$Uid uidObj = batterySipper.uidObj;
            if (uidObj != null && !this.mBatteryUtils.shouldHideSipper(batterySipper)) {
                if (packageUid == -1 || packageUid == uidObj.getUid()) {
                    final long currentDurationMs = this.getCurrentDurationMs(uidObj, elapsedRealtime);
                    final long backgroundTotalDurationMs = this.getBackgroundTotalDurationMs(uidObj, elapsedRealtime);
                    if (backgroundTotalDurationMs > this.mWakeLockThresholdMs && currentDurationMs != 0L) {
                        final String packageName = this.mBatteryUtils.getPackageName(uidObj.getUid());
                        final Anomaly build = new Anomaly.Builder().setUid(uidObj.getUid()).setType(0).setDisplayName(Utils.getApplicationLabel(this.mContext, packageName)).setPackageName(packageName).setWakeLockTimeMs(backgroundTotalDurationMs).build();
                        if (this.mAnomalyUtils.getAnomalyAction(build).isActionActive(build)) {
                            list.add(build);
                        }
                    }
                }
            }
        }
        return list;
    }
    
    long getBackgroundTotalDurationMs(final BatteryStats$Uid batteryStats$Uid, long totalDurationMsLocked) {
        final BatteryStats$Timer aggregatedPartialWakelockTimer = batteryStats$Uid.getAggregatedPartialWakelockTimer();
        BatteryStats$Timer subTimer;
        if (aggregatedPartialWakelockTimer != null) {
            subTimer = aggregatedPartialWakelockTimer.getSubTimer();
        }
        else {
            subTimer = null;
        }
        if (subTimer != null) {
            totalDurationMsLocked = subTimer.getTotalDurationMsLocked(totalDurationMsLocked);
        }
        else {
            totalDurationMsLocked = 0L;
        }
        return totalDurationMsLocked;
    }
    
    long getCurrentDurationMs(final BatteryStats$Uid batteryStats$Uid, long currentDurationMsLocked) {
        final BatteryStats$Timer aggregatedPartialWakelockTimer = batteryStats$Uid.getAggregatedPartialWakelockTimer();
        if (aggregatedPartialWakelockTimer != null) {
            currentDurationMsLocked = aggregatedPartialWakelockTimer.getCurrentDurationMsLocked(currentDurationMsLocked);
        }
        else {
            currentDurationMsLocked = 0L;
        }
        return currentDurationMsLocked;
    }
}
