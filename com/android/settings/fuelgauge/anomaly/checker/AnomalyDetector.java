package com.android.settings.fuelgauge.anomaly.checker;

import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import com.android.internal.os.BatteryStatsHelper;

public interface AnomalyDetector
{
    List<Anomaly> detectAnomalies(final BatteryStatsHelper p0, final String p1);
}
