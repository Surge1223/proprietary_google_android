package com.android.settings.fuelgauge.anomaly.action;

import android.util.Pair;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;

public abstract class AnomalyAction
{
    protected int mActionMetricKey;
    protected Context mContext;
    private MetricsFeatureProvider mMetricsFeatureProvider;
    
    public AnomalyAction(final Context mContext) {
        this.mContext = mContext;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(mContext).getMetricsFeatureProvider();
    }
    
    public abstract int getActionType();
    
    public void handlePositiveAction(final Anomaly anomaly, final int n) {
        this.mMetricsFeatureProvider.action(this.mContext, this.mActionMetricKey, anomaly.packageName, Pair.create((Object)833, (Object)n));
    }
    
    public abstract boolean isActionActive(final Anomaly p0);
}
