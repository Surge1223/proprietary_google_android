package com.android.settings.fuelgauge.anomaly.action;

import android.content.pm.PackageManager;
import android.util.Log;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import android.content.Context;
import android.content.pm.PackageManager;
import android.app.ActivityManager;

public class ForceStopAction extends AnomalyAction
{
    private ActivityManager mActivityManager;
    private PackageManager mPackageManager;
    
    public ForceStopAction(final Context context) {
        super(context);
        this.mActivityManager = (ActivityManager)context.getSystemService("activity");
        this.mPackageManager = context.getPackageManager();
        this.mActionMetricKey = 807;
    }
    
    @Override
    public int getActionType() {
        return 0;
    }
    
    @Override
    public void handlePositiveAction(final Anomaly anomaly, final int n) {
        super.handlePositiveAction(anomaly, n);
        this.mActivityManager.forceStopPackage(anomaly.packageName);
    }
    
    @Override
    public boolean isActionActive(final Anomaly anomaly) {
        boolean b = false;
        try {
            if ((this.mPackageManager.getApplicationInfo(anomaly.packageName, 128).flags & 0x200000) == 0x0) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find info for app: ");
            sb.append(anomaly.packageName);
            Log.e("ForceStopAction", sb.toString());
            return false;
        }
    }
}
