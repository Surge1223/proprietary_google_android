package com.android.settings.fuelgauge.anomaly.action;

import com.android.settings.fuelgauge.anomaly.Anomaly;
import android.content.Context;

public class StopAndBackgroundCheckAction extends AnomalyAction
{
    BackgroundCheckAction mBackgroundCheckAction;
    ForceStopAction mForceStopAction;
    
    public StopAndBackgroundCheckAction(final Context context) {
        this(context, new ForceStopAction(context), new BackgroundCheckAction(context));
        this.mActionMetricKey = 1233;
    }
    
    StopAndBackgroundCheckAction(final Context context, final ForceStopAction mForceStopAction, final BackgroundCheckAction mBackgroundCheckAction) {
        super(context);
        this.mForceStopAction = mForceStopAction;
        this.mBackgroundCheckAction = mBackgroundCheckAction;
    }
    
    @Override
    public int getActionType() {
        return 3;
    }
    
    @Override
    public void handlePositiveAction(final Anomaly anomaly, final int n) {
        super.handlePositiveAction(anomaly, n);
        this.mForceStopAction.handlePositiveAction(anomaly, n);
        this.mBackgroundCheckAction.handlePositiveAction(anomaly, n);
    }
    
    @Override
    public boolean isActionActive(final Anomaly anomaly) {
        return this.mForceStopAction.isActionActive(anomaly) && this.mBackgroundCheckAction.isActionActive(anomaly);
    }
}
