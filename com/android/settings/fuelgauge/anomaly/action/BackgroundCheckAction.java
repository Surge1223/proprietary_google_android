package com.android.settings.fuelgauge.anomaly.action;

import com.android.settings.fuelgauge.anomaly.Anomaly;
import android.content.Context;
import com.android.settings.fuelgauge.BatteryUtils;
import android.app.AppOpsManager;

public class BackgroundCheckAction extends AnomalyAction
{
    private AppOpsManager mAppOpsManager;
    BatteryUtils mBatteryUtils;
    
    public BackgroundCheckAction(final Context context) {
        super(context);
        this.mAppOpsManager = (AppOpsManager)context.getSystemService("appops");
        this.mActionMetricKey = 1020;
        this.mBatteryUtils = BatteryUtils.getInstance(context);
    }
    
    @Override
    public int getActionType() {
        return 1;
    }
    
    @Override
    public void handlePositiveAction(final Anomaly anomaly, final int n) {
        super.handlePositiveAction(anomaly, n);
        if (anomaly.targetSdkVersion < 26) {
            this.mAppOpsManager.setMode(63, anomaly.uid, anomaly.packageName, 1);
        }
    }
    
    @Override
    public boolean isActionActive(final Anomaly anomaly) {
        return this.mBatteryUtils.isBackgroundRestrictionEnabled(anomaly.targetSdkVersion, anomaly.uid, anomaly.packageName) ^ true;
    }
}
