package com.android.settings.fuelgauge.anomaly.action;

import android.support.v4.content.PermissionChecker;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import android.content.Context;
import android.content.pm.permission.RuntimePermissionPresenter;

public class LocationCheckAction extends AnomalyAction
{
    private final RuntimePermissionPresenter mRuntimePermissionPresenter;
    
    public LocationCheckAction(final Context context) {
        this(context, RuntimePermissionPresenter.getInstance(context));
    }
    
    LocationCheckAction(final Context context, final RuntimePermissionPresenter mRuntimePermissionPresenter) {
        super(context);
        this.mRuntimePermissionPresenter = mRuntimePermissionPresenter;
        this.mActionMetricKey = 1021;
    }
    
    private boolean isPermissionGranted(final Anomaly anomaly, final String s) {
        return PermissionChecker.checkPermission(this.mContext, s, -1, anomaly.uid, anomaly.packageName) == 0;
    }
    
    @Override
    public int getActionType() {
        return 2;
    }
    
    @Override
    public void handlePositiveAction(final Anomaly anomaly, final int n) {
        super.handlePositiveAction(anomaly, n);
        this.mRuntimePermissionPresenter.revokeRuntimePermission(anomaly.packageName, "android.permission.ACCESS_COARSE_LOCATION");
        this.mRuntimePermissionPresenter.revokeRuntimePermission(anomaly.packageName, "android.permission.ACCESS_FINE_LOCATION");
    }
    
    @Override
    public boolean isActionActive(final Anomaly anomaly) {
        return this.isPermissionGranted(anomaly, "android.permission.ACCESS_COARSE_LOCATION") || this.isPermissionGranted(anomaly, "android.permission.ACCESS_FINE_LOCATION");
    }
}
