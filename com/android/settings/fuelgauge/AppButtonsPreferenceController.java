package com.android.settings.fuelgauge;

import com.android.settings.DeviceAdminAdd;
import android.view.View;
import android.content.ComponentName;
import android.os.Handler;
import android.net.Uri;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.content.pm.ResolveInfo;
import java.util.List;
import java.util.ArrayList;
import com.android.settingslib.Utils;
import android.content.res.Resources;
import android.os.RemoteException;
import android.webkit.IWebViewUpdateService$Stub;
import android.os.ServiceManager;
import com.android.settingslib.applications.AppUtils;
import android.os.AsyncTask;
import android.app.ActivityManager;
import android.util.Pair;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceScreen;
import android.content.IntentFilter;
import android.app.FragmentManager;
import android.os.UserHandle;
import com.android.settings.overlay.FeatureFactory;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.os.UserManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import java.util.HashSet;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.SettingsActivity;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AppButtonsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, Callbacks, LifecycleObserver, OnDestroy, OnResume
{
    private final SettingsActivity mActivity;
    AppEntry mAppEntry;
    private final ApplicationFeatureProvider mApplicationFeatureProvider;
    private RestrictedLockUtils.EnforcedAdmin mAppsControlDisallowedAdmin;
    private boolean mAppsControlDisallowedBySystem;
    ActionButtonPreference mButtonsPref;
    private final BroadcastReceiver mCheckKillProcessesReceiver;
    boolean mDisableAfterUninstall;
    private final DevicePolicyManager mDpm;
    private boolean mFinishing;
    private final Fragment mFragment;
    final HashSet<String> mHomePackages;
    private boolean mListeningToPackageRemove;
    private final MetricsFeatureProvider mMetricsFeatureProvider;
    PackageInfo mPackageInfo;
    String mPackageName;
    private final BroadcastReceiver mPackageRemovedReceiver;
    private final PackageManager mPm;
    private final int mRequestRemoveDeviceAdmin;
    private final int mRequestUninstall;
    private Session mSession;
    ApplicationsState mState;
    private boolean mUpdatedSysApp;
    private final int mUserId;
    private final UserManager mUserManager;
    
    public AppButtonsPreferenceController(final SettingsActivity mActivity, final Fragment mFragment, final Lifecycle lifecycle, final String mPackageName, final ApplicationsState mState, final DevicePolicyManager mDpm, final UserManager mUserManager, final PackageManager mPm, final int mRequestUninstall, final int mRequestRemoveDeviceAdmin) {
        super((Context)mActivity);
        this.mHomePackages = new HashSet<String>();
        this.mDisableAfterUninstall = false;
        this.mUpdatedSysApp = false;
        this.mListeningToPackageRemove = false;
        this.mFinishing = false;
        this.mCheckKillProcessesReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final boolean b = this.getResultCode() != 0;
                final StringBuilder sb = new StringBuilder();
                sb.append("Got broadcast response: Restart status for ");
                sb.append(AppButtonsPreferenceController.this.mAppEntry.info.packageName);
                sb.append(" ");
                sb.append(b);
                Log.d("AppButtonsPrefCtl", sb.toString());
                AppButtonsPreferenceController.this.updateForceStopButtonInner(b);
            }
        };
        this.mPackageRemovedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
                if (!AppButtonsPreferenceController.this.mFinishing && AppButtonsPreferenceController.this.mAppEntry.info.packageName.equals(schemeSpecificPart)) {
                    AppButtonsPreferenceController.this.mActivity.finishAndRemoveTask();
                }
            }
        };
        if (mFragment instanceof ButtonActionDialogFragment.AppButtonsDialogListener) {
            final FeatureFactory factory = FeatureFactory.getFactory((Context)mActivity);
            this.mMetricsFeatureProvider = factory.getMetricsFeatureProvider();
            this.mApplicationFeatureProvider = factory.getApplicationFeatureProvider((Context)mActivity);
            this.mState = mState;
            this.mDpm = mDpm;
            this.mUserManager = mUserManager;
            this.mPm = mPm;
            this.mPackageName = mPackageName;
            this.mActivity = mActivity;
            this.mFragment = mFragment;
            this.mUserId = UserHandle.myUserId();
            this.mRequestUninstall = mRequestUninstall;
            this.mRequestRemoveDeviceAdmin = mRequestRemoveDeviceAdmin;
            if (mPackageName != null) {
                this.mAppEntry = this.mState.getEntry(mPackageName, this.mUserId);
                this.mSession = this.mState.newSession((ApplicationsState.Callbacks)this, lifecycle);
                lifecycle.addObserver(this);
            }
            else {
                this.mFinishing = true;
            }
            return;
        }
        throw new IllegalArgumentException("Fragment should implement AppButtonsDialogListener");
    }
    
    private boolean isDisabledUntilUsed() {
        return this.mAppEntry.info.enabledSetting == 4;
    }
    
    private boolean isSingleUser() {
        final int userCount = this.mUserManager.getUserCount();
        boolean b = true;
        if (userCount != 1) {
            final UserManager mUserManager = this.mUserManager;
            b = (UserManager.isSplitSystemUser() && userCount == 2 && b);
        }
        return b;
    }
    
    private void refreshAndFinishIfPossible() {
        if (!this.refreshUi()) {
            this.setIntentAndFinish(true);
        }
        else {
            this.startListeningToPackageRemove();
        }
    }
    
    private void setIntentAndFinish(final boolean b) {
        final Intent intent = new Intent();
        intent.putExtra("chg", b);
        this.mActivity.finishPreferencePanel(-1, intent);
        this.mFinishing = true;
    }
    
    private void showDialogInner(final int n) {
        final ButtonActionDialogFragment instance = ButtonActionDialogFragment.newInstance(n);
        instance.setTargetFragment(this.mFragment, 0);
        final FragmentManager fragmentManager = this.mActivity.getFragmentManager();
        final StringBuilder sb = new StringBuilder();
        sb.append("dialog ");
        sb.append(n);
        instance.show(fragmentManager, sb.toString());
    }
    
    private boolean signaturesMatch(final String s, final String s2) {
        if (s != null && s2 != null) {
            try {
                if (this.mPm.checkSignatures(s, s2) >= 0) {
                    return true;
                }
            }
            catch (Exception ex) {}
        }
        return false;
    }
    
    private void startListeningToPackageRemove() {
        if (this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = true;
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.mActivity.registerReceiver(this.mPackageRemovedReceiver, intentFilter);
    }
    
    private void stopListeningToPackageRemove() {
        if (!this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = false;
        this.mActivity.unregisterReceiver(this.mPackageRemovedReceiver);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mButtonsPref = ((ActionButtonPreference)preferenceScreen.findPreference("action_buttons")).setButton1Text(2131889544).setButton2Text(2131887694).setButton1OnClickListener((View.OnClickListener)new UninstallAndDisableButtonListener()).setButton2OnClickListener((View.OnClickListener)new ForceStopButtonListener()).setButton1Positive(false).setButton2Positive(false).setButton2Enabled(false);
        }
    }
    
    void forceStopPackage(final String s) {
        FeatureFactory.getFactory(this.mContext).getMetricsFeatureProvider().action(this.mContext, 807, s, (Pair<Integer, Object>[])new Pair[0]);
        final ActivityManager activityManager = (ActivityManager)this.mActivity.getSystemService("activity");
        final StringBuilder sb = new StringBuilder();
        sb.append("Stopping package ");
        sb.append(s);
        Log.d("AppButtonsPrefCtl", sb.toString());
        activityManager.forceStopPackage(s);
        final int userId = UserHandle.getUserId(this.mAppEntry.info.uid);
        this.mState.invalidatePackage(s, userId);
        final ApplicationsState.AppEntry entry = this.mState.getEntry(s, userId);
        if (entry != null) {
            this.mAppEntry = entry;
        }
        this.updateForceStopButton();
    }
    
    @Override
    public String getPreferenceKey() {
        return "action_buttons";
    }
    
    public void handleActivityResult(final int n, final int n2, final Intent intent) {
        if (n == this.mRequestUninstall) {
            if (this.mDisableAfterUninstall) {
                this.mDisableAfterUninstall = false;
                AsyncTask.execute((Runnable)new DisableChangerRunnable(this.mPm, this.mAppEntry.info.packageName, 3));
            }
            this.refreshAndFinishIfPossible();
        }
        else if (n == this.mRequestRemoveDeviceAdmin) {
            this.refreshAndFinishIfPossible();
        }
    }
    
    public void handleDialogClick(final int n) {
        switch (n) {
            case 2: {
                this.forceStopPackage(this.mAppEntry.info.packageName);
                break;
            }
            case 1: {
                this.mMetricsFeatureProvider.action((Context)this.mActivity, 874, (Pair<Integer, Object>[])new Pair[0]);
                this.uninstallPkg(this.mAppEntry.info.packageName, false, true);
                break;
            }
            case 0: {
                this.mMetricsFeatureProvider.action((Context)this.mActivity, 874, (Pair<Integer, Object>[])new Pair[0]);
                AsyncTask.execute((Runnable)new DisableChangerRunnable(this.mPm, this.mAppEntry.info.packageName, 3));
                break;
            }
        }
    }
    
    boolean handleDisableable() {
        boolean b = false;
        if (!this.mHomePackages.contains(this.mAppEntry.info.packageName) && !this.isSystemPackage(this.mActivity.getResources(), this.mPm, this.mPackageInfo)) {
            if (this.mAppEntry.info.enabled && !this.isDisabledUntilUsed()) {
                this.mButtonsPref.setButton1Text(2131887421).setButton1Positive(false);
                b = (this.mApplicationFeatureProvider.getKeepEnabledPackages().contains(this.mAppEntry.info.packageName) ^ true);
            }
            else {
                this.mButtonsPref.setButton1Text(2131887538).setButton1Positive(true);
                b = true;
            }
        }
        else {
            this.mButtonsPref.setButton1Text(2131887421).setButton1Positive(false);
        }
        return b;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mAppEntry != null && !AppUtils.isInstant(this.mAppEntry.info);
    }
    
    boolean isFallbackPackage(final String s) {
        try {
            return IWebViewUpdateService$Stub.asInterface(ServiceManager.getService("webviewupdate")).isFallbackPackage(s);
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    boolean isSystemPackage(final Resources resources, final PackageManager packageManager, final PackageInfo packageInfo) {
        return Utils.isSystemPackage(resources, packageManager, packageInfo);
    }
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onDestroy() {
        this.stopListeningToPackageRemove();
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
        if (this.isAvailable()) {
            this.refreshUi();
        }
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
    }
    
    @Override
    public void onResume() {
        if (this.isAvailable() && !this.mFinishing) {
            this.mAppsControlDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction((Context)this.mActivity, "no_control_apps", this.mUserId);
            this.mAppsControlDisallowedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.mActivity, "no_control_apps", this.mUserId);
            if (!this.refreshUi()) {
                this.setIntentAndFinish(true);
            }
        }
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    boolean refreshUi() {
        if (this.mPackageName == null) {
            return false;
        }
        this.retrieveAppEntry();
        if (this.mAppEntry != null && this.mPackageInfo != null) {
            final ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>)new ArrayList<Object>();
            this.mPm.getHomeActivities((List)list);
            this.mHomePackages.clear();
            for (int i = 0; i < list.size(); ++i) {
                final ResolveInfo resolveInfo = list.get(i);
                final String packageName = resolveInfo.activityInfo.packageName;
                this.mHomePackages.add(packageName);
                final Bundle metaData = resolveInfo.activityInfo.metaData;
                if (metaData != null) {
                    final String string = metaData.getString("android.app.home.alternate");
                    if (this.signaturesMatch(string, packageName)) {
                        this.mHomePackages.add(string);
                    }
                }
            }
            this.updateUninstallButton();
            this.updateForceStopButton();
            return true;
        }
        return false;
    }
    
    void retrieveAppEntry() {
        this.mAppEntry = this.mState.getEntry(this.mPackageName, this.mUserId);
        if (this.mAppEntry != null) {
            try {
                this.mPackageInfo = this.mPm.getPackageInfo(this.mAppEntry.info.packageName, 4198976);
                this.mPackageName = this.mAppEntry.info.packageName;
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Exception when retrieving package:");
                sb.append(this.mAppEntry.info.packageName);
                Log.e("AppButtonsPrefCtl", sb.toString(), (Throwable)ex);
                this.mPackageInfo = null;
            }
        }
        else {
            this.mPackageInfo = null;
        }
    }
    
    void uninstallPkg(final String s, final boolean b, final boolean mDisableAfterUninstall) {
        this.stopListeningToPackageRemove();
        final StringBuilder sb = new StringBuilder();
        sb.append("package:");
        sb.append(s);
        final Intent intent = new Intent("android.intent.action.UNINSTALL_PACKAGE", Uri.parse(sb.toString()));
        intent.putExtra("android.intent.extra.UNINSTALL_ALL_USERS", b);
        this.mMetricsFeatureProvider.action((Context)this.mActivity, 872, (Pair<Integer, Object>[])new Pair[0]);
        this.mFragment.startActivityForResult(intent, this.mRequestUninstall);
        this.mDisableAfterUninstall = mDisableAfterUninstall;
    }
    
    void updateForceStopButton() {
        if (this.mDpm.packageHasActiveAdmins(this.mPackageInfo.packageName)) {
            Log.w("AppButtonsPrefCtl", "User can't force stop device admin");
            this.updateForceStopButtonInner(false);
        }
        else if ((this.mAppEntry.info.flags & 0x200000) == 0x0) {
            Log.w("AppButtonsPrefCtl", "App is not explicitly stopped");
            this.updateForceStopButtonInner(true);
        }
        else {
            final Intent intent = new Intent("android.intent.action.QUERY_PACKAGE_RESTART", Uri.fromParts("package", this.mAppEntry.info.packageName, (String)null));
            intent.putExtra("android.intent.extra.PACKAGES", new String[] { this.mAppEntry.info.packageName });
            intent.putExtra("android.intent.extra.UID", this.mAppEntry.info.uid);
            intent.putExtra("android.intent.extra.user_handle", UserHandle.getUserId(this.mAppEntry.info.uid));
            final StringBuilder sb = new StringBuilder();
            sb.append("Sending broadcast to query restart status for ");
            sb.append(this.mAppEntry.info.packageName);
            Log.d("AppButtonsPrefCtl", sb.toString());
            this.mActivity.sendOrderedBroadcastAsUser(intent, UserHandle.CURRENT, (String)null, this.mCheckKillProcessesReceiver, (Handler)null, 0, (String)null, (Bundle)null);
        }
    }
    
    void updateForceStopButtonInner(final boolean button2Enabled) {
        if (this.mAppsControlDisallowedBySystem) {
            this.mButtonsPref.setButton2Enabled(false);
        }
        else {
            this.mButtonsPref.setButton2Enabled(button2Enabled);
        }
    }
    
    void updateUninstallButton() {
        final int flags = this.mAppEntry.info.flags;
        final boolean b = true;
        final boolean b2 = (flags & 0x1) != 0x0;
        final boolean b3 = true;
        boolean handleDisableable;
        if (b2) {
            handleDisableable = this.handleDisableable();
        }
        else {
            handleDisableable = b3;
            if ((this.mPackageInfo.applicationInfo.flags & 0x800000) == 0x0) {
                handleDisableable = b3;
                if (this.mUserManager.getUsers().size() >= 2) {
                    handleDisableable = false;
                }
            }
        }
        boolean b4 = handleDisableable;
        if (b2) {
            b4 = handleDisableable;
            if (this.mDpm.packageHasActiveAdmins(this.mPackageInfo.packageName)) {
                b4 = false;
            }
        }
        if (com.android.settings.Utils.isProfileOrDeviceOwner(this.mUserManager, this.mDpm, this.mPackageInfo.packageName)) {
            b4 = false;
        }
        if (Utils.isDeviceProvisioningPackage(this.mContext.getResources(), this.mAppEntry.info.packageName)) {
            b4 = false;
        }
        if (this.mDpm.isUninstallInQueue(this.mPackageName)) {
            b4 = false;
        }
        boolean button1Enabled = b4;
        if (b4) {
            button1Enabled = b4;
            if (this.mHomePackages.contains(this.mPackageInfo.packageName)) {
                if (b2) {
                    button1Enabled = false;
                }
                else {
                    final ComponentName homeActivities = this.mPm.getHomeActivities((List)new ArrayList());
                    if (homeActivities == null) {
                        button1Enabled = (this.mHomePackages.size() > 1 && b);
                    }
                    else {
                        button1Enabled = (true ^ this.mPackageInfo.packageName.equals(homeActivities.getPackageName()));
                    }
                }
            }
        }
        if (this.mAppsControlDisallowedBySystem) {
            button1Enabled = false;
        }
        if (this.isFallbackPackage(this.mAppEntry.info.packageName)) {
            button1Enabled = false;
        }
        this.mButtonsPref.setButton1Enabled(button1Enabled);
    }
    
    private class DisableChangerRunnable implements Runnable
    {
        final String mPackageName;
        final PackageManager mPm;
        final int mState;
        
        public DisableChangerRunnable(final PackageManager mPm, final String mPackageName, final int mState) {
            this.mPm = mPm;
            this.mPackageName = mPackageName;
            this.mState = mState;
        }
        
        @Override
        public void run() {
            this.mPm.setApplicationEnabledSetting(this.mPackageName, this.mState, 0);
        }
    }
    
    private class ForceStopButtonListener implements View.OnClickListener
    {
        public void onClick(final View view) {
            if (AppButtonsPreferenceController.this.mAppsControlDisallowedAdmin != null && !AppButtonsPreferenceController.this.mAppsControlDisallowedBySystem) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)AppButtonsPreferenceController.this.mActivity, AppButtonsPreferenceController.this.mAppsControlDisallowedAdmin);
            }
            else {
                AppButtonsPreferenceController.this.showDialogInner(2);
            }
        }
    }
    
    private class UninstallAndDisableButtonListener implements View.OnClickListener
    {
        public void onClick(final View view) {
            final String packageName = AppButtonsPreferenceController.this.mAppEntry.info.packageName;
            if (AppButtonsPreferenceController.this.mDpm.packageHasActiveAdmins(AppButtonsPreferenceController.this.mPackageInfo.packageName)) {
                AppButtonsPreferenceController.this.stopListeningToPackageRemove();
                final Intent intent = new Intent((Context)AppButtonsPreferenceController.this.mActivity, (Class)DeviceAdminAdd.class);
                intent.putExtra("android.app.extra.DEVICE_ADMIN_PACKAGE_NAME", packageName);
                AppButtonsPreferenceController.this.mMetricsFeatureProvider.action((Context)AppButtonsPreferenceController.this.mActivity, 873, (Pair<Integer, Object>[])new Pair[0]);
                AppButtonsPreferenceController.this.mFragment.startActivityForResult(intent, AppButtonsPreferenceController.this.mRequestRemoveDeviceAdmin);
                return;
            }
            final RestrictedLockUtils.EnforcedAdmin checkIfUninstallBlocked = RestrictedLockUtils.checkIfUninstallBlocked((Context)AppButtonsPreferenceController.this.mActivity, packageName, AppButtonsPreferenceController.this.mUserId);
            final boolean b = AppButtonsPreferenceController.this.mAppsControlDisallowedBySystem || RestrictedLockUtils.hasBaseUserRestriction((Context)AppButtonsPreferenceController.this.mActivity, packageName, AppButtonsPreferenceController.this.mUserId);
            if (checkIfUninstallBlocked != null && !b) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)AppButtonsPreferenceController.this.mActivity, checkIfUninstallBlocked);
            }
            else if ((AppButtonsPreferenceController.this.mAppEntry.info.flags & 0x1) != 0x0) {
                if (AppButtonsPreferenceController.this.mAppEntry.info.enabled && !AppButtonsPreferenceController.this.isDisabledUntilUsed()) {
                    if (AppButtonsPreferenceController.this.mUpdatedSysApp && AppButtonsPreferenceController.this.isSingleUser()) {
                        AppButtonsPreferenceController.this.showDialogInner(1);
                    }
                    else {
                        AppButtonsPreferenceController.this.showDialogInner(0);
                    }
                }
                else {
                    final MetricsFeatureProvider access$500 = AppButtonsPreferenceController.this.mMetricsFeatureProvider;
                    final SettingsActivity access$501 = AppButtonsPreferenceController.this.mActivity;
                    int n;
                    if (AppButtonsPreferenceController.this.mAppEntry.info.enabled) {
                        n = 874;
                    }
                    else {
                        n = 875;
                    }
                    access$500.action((Context)access$501, n, (Pair<Integer, Object>[])new Pair[0]);
                    AsyncTask.execute((Runnable)new DisableChangerRunnable(AppButtonsPreferenceController.this.mPm, AppButtonsPreferenceController.this.mAppEntry.info.packageName, 0));
                }
            }
            else if ((AppButtonsPreferenceController.this.mAppEntry.info.flags & 0x800000) == 0x0) {
                AppButtonsPreferenceController.this.uninstallPkg(packageName, true, false);
            }
            else {
                AppButtonsPreferenceController.this.uninstallPkg(packageName, false, false);
            }
        }
    }
}
