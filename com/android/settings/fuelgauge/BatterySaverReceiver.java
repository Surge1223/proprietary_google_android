package com.android.settings.fuelgauge;

import android.content.IntentFilter;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class BatterySaverReceiver extends BroadcastReceiver
{
    private BatterySaverListener mBatterySaverListener;
    private Context mContext;
    private boolean mRegistered;
    
    public BatterySaverReceiver(final Context mContext) {
        this.mContext = mContext;
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final String action = intent.getAction();
        if ("android.os.action.POWER_SAVE_MODE_CHANGING".equals(action)) {
            if (this.mBatterySaverListener != null) {
                this.mBatterySaverListener.onPowerSaveModeChanged();
            }
        }
        else if ("android.intent.action.BATTERY_CHANGED".equals(action) && this.mBatterySaverListener != null) {
            boolean b = false;
            if (intent.getIntExtra("plugged", 0) != 0) {
                b = true;
            }
            this.mBatterySaverListener.onBatteryChanged(b);
        }
    }
    
    public void setBatterySaverListener(final BatterySaverListener mBatterySaverListener) {
        this.mBatterySaverListener = mBatterySaverListener;
    }
    
    public void setListening(final boolean b) {
        if (b && !this.mRegistered) {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGING");
            intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
            this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter);
            this.mRegistered = true;
        }
        else if (!b && this.mRegistered) {
            this.mContext.unregisterReceiver((BroadcastReceiver)this);
            this.mRegistered = false;
        }
    }
    
    public interface BatterySaverListener
    {
        void onBatteryChanged(final boolean p0);
        
        void onPowerSaveModeChanged();
    }
}
