package com.android.settings.fuelgauge;

import android.os.BatteryStats;
import android.content.Intent;
import java.util.ArrayList;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settingslib.utils.PowerUtil;
import android.os.SystemClock;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.internal.os.BatteryStatsHelper;
import java.util.List;
import com.android.settingslib.utils.AsyncLoader;

public class DebugEstimatesLoader extends AsyncLoader<List<BatteryInfo>>
{
    private BatteryStatsHelper mStatsHelper;
    
    public DebugEstimatesLoader(final Context context, final BatteryStatsHelper mStatsHelper) {
        super(context);
        this.mStatsHelper = mStatsHelper;
    }
    
    public List<BatteryInfo> loadInBackground() {
        final Context context = this.getContext();
        final PowerUsageFeatureProvider powerUsageFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
        final long convertMsToUs = PowerUtil.convertMsToUs(SystemClock.elapsedRealtime());
        final Intent registerReceiver = this.getContext().registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        final BatteryStats stats = this.mStatsHelper.getStats();
        final BatteryInfo batteryInfoOld = BatteryInfo.getBatteryInfoOld(this.getContext(), registerReceiver, stats, convertMsToUs, false);
        Estimate enhancedBatteryPrediction = powerUsageFeatureProvider.getEnhancedBatteryPrediction(context);
        if (enhancedBatteryPrediction == null) {
            enhancedBatteryPrediction = new Estimate(0L, false, -1L);
        }
        final BatteryInfo batteryInfo = BatteryInfo.getBatteryInfo(this.getContext(), registerReceiver, stats, enhancedBatteryPrediction, convertMsToUs, false);
        final ArrayList<BatteryInfo> list = new ArrayList<BatteryInfo>();
        list.add(batteryInfoOld);
        list.add(batteryInfo);
        return list;
    }
    
    @Override
    protected void onDiscardResult(final List<BatteryInfo> list) {
    }
}
