package com.android.settings.fuelgauge;

import com.android.internal.R$styleable;
import android.graphics.Typeface;
import android.content.res.ColorStateList;
import java.util.Locale;
import android.text.format.Formatter;
import android.os.BatteryStats$HistoryItem;
import android.graphics.Paint$Align;
import java.util.Calendar;
import libcore.icu.LocaleData;
import android.text.format.DateFormat;
import android.content.res.TypedArray;
import android.graphics.PathEffect;
import android.graphics.DashPathEffect;
import com.android.settings.R;
import android.graphics.Paint.Style;
import com.android.settingslib.Utils;
import android.util.TypedValue;
import android.util.AttributeSet;
import android.content.Context;
import android.os.BatteryStats;
import android.text.TextPaint;
import java.util.ArrayList;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

public class BatteryHistoryChart extends View
{
    final Path mBatCriticalPath;
    final Path mBatGoodPath;
    int mBatHigh;
    final Path mBatLevelPath;
    int mBatLow;
    final Path mBatWarnPath;
    final Paint mBatteryBackgroundPaint;
    int mBatteryCriticalLevel;
    final Paint mBatteryCriticalPaint;
    final Paint mBatteryGoodPaint;
    int mBatteryWarnLevel;
    final Paint mBatteryWarnPaint;
    Bitmap mBitmap;
    String mCameraOnLabel;
    int mCameraOnOffset;
    final Paint mCameraOnPaint;
    final Path mCameraOnPath;
    Canvas mCanvas;
    String mChargeDurationString;
    int mChargeDurationStringWidth;
    int mChargeLabelStringWidth;
    String mChargingLabel;
    int mChargingOffset;
    final Paint mChargingPaint;
    final Path mChargingPath;
    int mChartMinHeight;
    String mCpuRunningLabel;
    int mCpuRunningOffset;
    final Paint mCpuRunningPaint;
    final Path mCpuRunningPath;
    final ArrayList<DateLabel> mDateLabels;
    final Paint mDateLinePaint;
    final Path mDateLinePath;
    final Paint mDebugRectPaint;
    String mDrainString;
    int mDrainStringWidth;
    String mDurationString;
    int mDurationStringWidth;
    long mEndDataWallTime;
    long mEndWallTime;
    String mFlashlightOnLabel;
    int mFlashlightOnOffset;
    final Paint mFlashlightOnPaint;
    final Path mFlashlightOnPath;
    String mGpsOnLabel;
    int mGpsOnOffset;
    final Paint mGpsOnPaint;
    final Path mGpsOnPath;
    boolean mHaveCamera;
    boolean mHaveFlashlight;
    boolean mHaveGps;
    boolean mHavePhoneSignal;
    boolean mHaveWifi;
    int mHeaderHeight;
    int mHeaderTextAscent;
    int mHeaderTextDescent;
    final TextPaint mHeaderTextPaint;
    long mHistStart;
    BatteryInfo mInfo;
    boolean mLargeMode;
    int mLastHeight;
    int mLastWidth;
    int mLevelBottom;
    int mLevelLeft;
    int mLevelOffset;
    int mLevelRight;
    int mLevelTop;
    int mLineWidth;
    String mMaxPercentLabelString;
    int mMaxPercentLabelStringWidth;
    String mMinPercentLabelString;
    int mMinPercentLabelStringWidth;
    int mNumHist;
    final ChartData mPhoneSignalChart;
    String mPhoneSignalLabel;
    int mPhoneSignalOffset;
    String mScreenOnLabel;
    int mScreenOnOffset;
    final Paint mScreenOnPaint;
    final Path mScreenOnPath;
    long mStartWallTime;
    BatteryStats mStats;
    int mTextAscent;
    int mTextDescent;
    final TextPaint mTextPaint;
    int mThinLineWidth;
    final ArrayList<TimeLabel> mTimeLabels;
    final Paint mTimeRemainPaint;
    final Path mTimeRemainPath;
    String mWifiRunningLabel;
    int mWifiRunningOffset;
    final Paint mWifiRunningPaint;
    final Path mWifiRunningPath;
    
    public BatteryHistoryChart(final Context context, final AttributeSet set) {
        super(context, set);
        this.mBatteryBackgroundPaint = new Paint(1);
        this.mBatteryGoodPaint = new Paint(1);
        this.mBatteryWarnPaint = new Paint(1);
        this.mBatteryCriticalPaint = new Paint(1);
        this.mTimeRemainPaint = new Paint(1);
        this.mChargingPaint = new Paint();
        this.mScreenOnPaint = new Paint();
        this.mGpsOnPaint = new Paint();
        this.mFlashlightOnPaint = new Paint();
        this.mCameraOnPaint = new Paint();
        this.mWifiRunningPaint = new Paint();
        this.mCpuRunningPaint = new Paint();
        this.mDateLinePaint = new Paint();
        this.mPhoneSignalChart = new ChartData();
        this.mTextPaint = new TextPaint(1);
        this.mHeaderTextPaint = new TextPaint(1);
        this.mDebugRectPaint = new Paint();
        this.mBatLevelPath = new Path();
        this.mBatGoodPath = new Path();
        this.mBatWarnPath = new Path();
        this.mBatCriticalPath = new Path();
        this.mTimeRemainPath = new Path();
        this.mChargingPath = new Path();
        this.mScreenOnPath = new Path();
        this.mGpsOnPath = new Path();
        this.mFlashlightOnPath = new Path();
        this.mCameraOnPath = new Path();
        this.mWifiRunningPath = new Path();
        this.mCpuRunningPath = new Path();
        this.mDateLinePath = new Path();
        this.mLastWidth = -1;
        this.mLastHeight = -1;
        this.mTimeLabels = new ArrayList<TimeLabel>();
        this.mDateLabels = new ArrayList<DateLabel>();
        this.mBatteryWarnLevel = this.mContext.getResources().getInteger(17694805);
        this.mBatteryCriticalLevel = this.mContext.getResources().getInteger(17694757);
        this.mThinLineWidth = (int)TypedValue.applyDimension(1, 2.0f, this.getResources().getDisplayMetrics());
        final int colorAccent = Utils.getColorAccent(this.mContext);
        this.mBatteryBackgroundPaint.setColor(colorAccent);
        this.mBatteryBackgroundPaint.setStyle(Paint.Style.FILL);
        this.mBatteryGoodPaint.setARGB(128, 0, 128, 0);
        this.mBatteryGoodPaint.setStyle(Paint.Style.STROKE);
        this.mBatteryWarnPaint.setARGB(128, 128, 128, 0);
        this.mBatteryWarnPaint.setStyle(Paint.Style.STROKE);
        this.mBatteryCriticalPaint.setARGB(192, 128, 0, 0);
        this.mBatteryCriticalPaint.setStyle(Paint.Style.STROKE);
        this.mTimeRemainPaint.setColor(-3221573);
        this.mTimeRemainPaint.setStyle(Paint.Style.FILL);
        this.mChargingPaint.setStyle(Paint.Style.STROKE);
        this.mScreenOnPaint.setStyle(Paint.Style.STROKE);
        this.mGpsOnPaint.setStyle(Paint.Style.STROKE);
        this.mCameraOnPaint.setStyle(Paint.Style.STROKE);
        this.mFlashlightOnPaint.setStyle(Paint.Style.STROKE);
        this.mWifiRunningPaint.setStyle(Paint.Style.STROKE);
        this.mCpuRunningPaint.setStyle(Paint.Style.STROKE);
        this.mPhoneSignalChart.setColors(com.android.settings.Utils.BADNESS_COLORS);
        this.mDebugRectPaint.setARGB(255, 255, 0, 0);
        this.mDebugRectPaint.setStyle(Paint.Style.STROKE);
        this.mScreenOnPaint.setColor(colorAccent);
        this.mGpsOnPaint.setColor(colorAccent);
        this.mCameraOnPaint.setColor(colorAccent);
        this.mFlashlightOnPaint.setColor(colorAccent);
        this.mWifiRunningPaint.setColor(colorAccent);
        this.mCpuRunningPaint.setColor(colorAccent);
        this.mChargingPaint.setColor(colorAccent);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.BatteryHistoryChart, 0, 0);
        final TextAttrs textAttrs = new TextAttrs();
        final TextAttrs textAttrs2 = new TextAttrs();
        textAttrs.retrieve(context, obtainStyledAttributes, 0);
        textAttrs2.retrieve(context, obtainStyledAttributes, 12);
        final int indexCount = obtainStyledAttributes.getIndexCount();
        float float1 = 0.0f;
        float float2 = 0.0f;
        float float3 = 0.0f;
        int int1 = 0;
        for (int i = 0; i < indexCount; ++i) {
            final int index = obtainStyledAttributes.getIndex(i);
            switch (index) {
                case 11: {
                    this.mChartMinHeight = obtainStyledAttributes.getDimensionPixelSize(index, 0);
                    break;
                }
                case 10: {
                    this.mBatteryBackgroundPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mScreenOnPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mGpsOnPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mCameraOnPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mFlashlightOnPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mWifiRunningPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mCpuRunningPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    this.mChargingPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    break;
                }
                case 9: {
                    this.mTimeRemainPaint.setColor(obtainStyledAttributes.getInt(index, 0));
                    break;
                }
                case 8: {
                    float1 = obtainStyledAttributes.getFloat(index, 0.0f);
                    break;
                }
                case 7: {
                    float2 = obtainStyledAttributes.getFloat(index, 0.0f);
                    break;
                }
                case 6: {
                    float3 = obtainStyledAttributes.getFloat(index, 0.0f);
                    break;
                }
                case 5: {
                    int1 = obtainStyledAttributes.getInt(index, 0);
                    break;
                }
                case 4: {
                    textAttrs.textColor = obtainStyledAttributes.getColorStateList(index);
                    textAttrs2.textColor = obtainStyledAttributes.getColorStateList(index);
                    break;
                }
                case 3: {
                    textAttrs.styleIndex = obtainStyledAttributes.getInt(index, textAttrs.styleIndex);
                    textAttrs2.styleIndex = obtainStyledAttributes.getInt(index, textAttrs2.styleIndex);
                    break;
                }
                case 2: {
                    textAttrs.typefaceIndex = obtainStyledAttributes.getInt(index, textAttrs.typefaceIndex);
                    textAttrs2.typefaceIndex = obtainStyledAttributes.getInt(index, textAttrs2.typefaceIndex);
                    break;
                }
                case 1: {
                    textAttrs.textSize = obtainStyledAttributes.getDimensionPixelSize(index, textAttrs.textSize);
                    textAttrs2.textSize = obtainStyledAttributes.getDimensionPixelSize(index, textAttrs2.textSize);
                    break;
                }
            }
        }
        obtainStyledAttributes.recycle();
        textAttrs.apply(context, this.mTextPaint);
        textAttrs2.apply(context, this.mHeaderTextPaint);
        this.mDateLinePaint.set((Paint)this.mTextPaint);
        this.mDateLinePaint.setStyle(Paint.Style.STROKE);
        int n;
        if ((n = this.mThinLineWidth / 2) < 1) {
            n = 1;
        }
        this.mDateLinePaint.setStrokeWidth((float)n);
        this.mDateLinePaint.setPathEffect((PathEffect)new DashPathEffect(new float[] { this.mThinLineWidth * 2, this.mThinLineWidth * 2 }, 0.0f));
        if (int1 != 0) {
            this.mTextPaint.setShadowLayer(float1, float3, float2, int1);
            this.mHeaderTextPaint.setShadowLayer(float1, float3, float2, int1);
        }
    }
    
    private boolean is24Hour() {
        return DateFormat.is24HourFormat(this.getContext());
    }
    
    private boolean isDayFirst() {
        final String dateFormat = LocaleData.get(this.getResources().getConfiguration().locale).getDateFormat(3);
        return dateFormat.indexOf(77) > dateFormat.indexOf(100);
    }
    
    void addDateLabel(final Calendar calendar, final int n, final int n2, final boolean b) {
        final long mStartWallTime = this.mStartWallTime;
        this.mDateLabels.add(new DateLabel(this.mTextPaint, (int)((calendar.getTimeInMillis() - mStartWallTime) * (n2 - n) / (this.mEndWallTime - mStartWallTime)) + n, calendar, b));
    }
    
    void addTimeLabel(final Calendar calendar, final int n, final int n2, final boolean b) {
        final long mStartWallTime = this.mStartWallTime;
        this.mTimeLabels.add(new TimeLabel(this.mTextPaint, (int)((calendar.getTimeInMillis() - mStartWallTime) * (n2 - n) / (this.mEndWallTime - mStartWallTime)) + n, calendar, b));
    }
    
    void drawChart(final Canvas canvas, int n, int i) {
        final boolean layoutRtl = this.isLayoutRtl();
        int n2;
        if (layoutRtl) {
            n2 = n;
        }
        else {
            n2 = 0;
        }
        int n3;
        if (layoutRtl) {
            n3 = 0;
        }
        else {
            n3 = n;
        }
        Paint$Align paint$Align;
        if (layoutRtl) {
            paint$Align = Paint$Align.RIGHT;
        }
        else {
            paint$Align = Paint$Align.LEFT;
        }
        Paint$Align textAlign;
        if (layoutRtl) {
            textAlign = Paint$Align.LEFT;
        }
        else {
            textAlign = Paint$Align.RIGHT;
        }
        canvas.drawPath(this.mBatLevelPath, this.mBatteryBackgroundPaint);
        if (!this.mTimeRemainPath.isEmpty()) {
            canvas.drawPath(this.mTimeRemainPath, this.mTimeRemainPaint);
        }
        int n13;
        if (this.mTimeLabels.size() > 1) {
            final int n4 = this.mLevelBottom - this.mTextAscent + this.mThinLineWidth * 4;
            final int n5 = this.mLevelBottom + this.mThinLineWidth + this.mThinLineWidth / 2;
            this.mTextPaint.setTextAlign(Paint$Align.LEFT);
            int n6 = 0;
            for (int j = 0; j < this.mTimeLabels.size(); ++j) {
                final TimeLabel timeLabel = this.mTimeLabels.get(j);
                if (j == 0) {
                    int n7;
                    if ((n7 = timeLabel.x - timeLabel.width / 2) < 0) {
                        n7 = 0;
                    }
                    canvas.drawText(timeLabel.label, (float)n7, (float)n4, (Paint)this.mTextPaint);
                    canvas.drawLine((float)timeLabel.x, (float)n5, (float)timeLabel.x, (float)(this.mThinLineWidth + n5), (Paint)this.mTextPaint);
                    n6 = n7 + timeLabel.width;
                }
                else {
                    final int n8 = n4;
                    final int n9 = j;
                    final int n10 = n5;
                    if (n9 < this.mTimeLabels.size() - 1) {
                        final int n11 = timeLabel.x - timeLabel.width / 2;
                        if (n11 >= n6 + this.mTextAscent) {
                            if (n11 <= n - this.mTimeLabels.get(n9 + 1).width - this.mTextAscent) {
                                canvas.drawText(timeLabel.label, (float)n11, (float)n8, (Paint)this.mTextPaint);
                                canvas.drawLine((float)timeLabel.x, (float)n10, (float)timeLabel.x, (float)(this.mThinLineWidth + n10), (Paint)this.mTextPaint);
                                n6 = n11 + timeLabel.width;
                            }
                        }
                    }
                    else {
                        int n12;
                        if (timeLabel.width + (n12 = timeLabel.x - timeLabel.width / 2) >= n) {
                            n12 = n - 1 - timeLabel.width;
                        }
                        canvas.drawText(timeLabel.label, (float)n12, (float)n8, (Paint)this.mTextPaint);
                        canvas.drawLine((float)timeLabel.x, (float)n10, (float)timeLabel.x, (float)(this.mThinLineWidth + n10), (Paint)this.mTextPaint);
                    }
                }
            }
            n13 = n3;
        }
        else {
            final Paint$Align paint$Align2 = textAlign;
            n13 = n3;
            textAlign = paint$Align2;
            if (this.mDurationString != null) {
                final int mLevelBottom = this.mLevelBottom;
                final int mTextAscent = this.mTextAscent;
                final int mThinLineWidth = this.mThinLineWidth;
                this.mTextPaint.setTextAlign(Paint$Align.LEFT);
                canvas.drawText(this.mDurationString, (float)(this.mLevelLeft + (this.mLevelRight - this.mLevelLeft) / 2 - this.mDurationStringWidth / 2), (float)(mLevelBottom - mTextAscent + mThinLineWidth * 4), (Paint)this.mTextPaint);
                textAlign = paint$Align2;
                n13 = n3;
            }
        }
        final int n14 = -this.mHeaderTextAscent + (this.mHeaderTextDescent - this.mHeaderTextAscent) / 3;
        this.mHeaderTextPaint.setTextAlign(paint$Align);
        canvas.drawText(this.mInfo.chargeLabel.toString(), (float)n2, (float)n14, (Paint)this.mHeaderTextPaint);
        int n15 = this.mChargeDurationStringWidth / 2;
        if (layoutRtl) {
            n15 = -n15;
        }
        final int n16 = (n - this.mChargeDurationStringWidth - this.mDrainStringWidth) / 2;
        int n17;
        if (layoutRtl) {
            n17 = this.mDrainStringWidth;
        }
        else {
            n17 = this.mChargeLabelStringWidth;
        }
        canvas.drawText(this.mChargeDurationString, (float)(n16 + n17 - n15), (float)n14, (Paint)this.mHeaderTextPaint);
        this.mHeaderTextPaint.setTextAlign(textAlign);
        canvas.drawText(this.mDrainString, (float)n13, (float)n14, (Paint)this.mHeaderTextPaint);
        if (!this.mBatGoodPath.isEmpty()) {
            canvas.drawPath(this.mBatGoodPath, this.mBatteryGoodPaint);
        }
        if (!this.mBatWarnPath.isEmpty()) {
            canvas.drawPath(this.mBatWarnPath, this.mBatteryWarnPaint);
        }
        if (!this.mBatCriticalPath.isEmpty()) {
            canvas.drawPath(this.mBatCriticalPath, this.mBatteryCriticalPaint);
        }
        if (this.mHavePhoneSignal) {
            this.mPhoneSignalChart.draw(canvas, i - this.mPhoneSignalOffset - this.mLineWidth / 2, this.mLineWidth);
        }
        if (!this.mScreenOnPath.isEmpty()) {
            canvas.drawPath(this.mScreenOnPath, this.mScreenOnPaint);
        }
        if (!this.mChargingPath.isEmpty()) {
            canvas.drawPath(this.mChargingPath, this.mChargingPaint);
        }
        if (this.mHaveGps && !this.mGpsOnPath.isEmpty()) {
            canvas.drawPath(this.mGpsOnPath, this.mGpsOnPaint);
        }
        if (this.mHaveFlashlight && !this.mFlashlightOnPath.isEmpty()) {
            canvas.drawPath(this.mFlashlightOnPath, this.mFlashlightOnPaint);
        }
        if (this.mHaveCamera && !this.mCameraOnPath.isEmpty()) {
            canvas.drawPath(this.mCameraOnPath, this.mCameraOnPaint);
        }
        if (this.mHaveWifi && !this.mWifiRunningPath.isEmpty()) {
            canvas.drawPath(this.mWifiRunningPath, this.mWifiRunningPaint);
        }
        if (!this.mCpuRunningPath.isEmpty()) {
            canvas.drawPath(this.mCpuRunningPath, this.mCpuRunningPaint);
        }
        if (this.mLargeMode) {
            final Paint$Align textAlign2 = this.mTextPaint.getTextAlign();
            this.mTextPaint.setTextAlign(paint$Align);
            if (this.mHavePhoneSignal) {
                canvas.drawText(this.mPhoneSignalLabel, (float)n2, (float)(i - this.mPhoneSignalOffset - this.mTextDescent), (Paint)this.mTextPaint);
            }
            if (this.mHaveGps) {
                canvas.drawText(this.mGpsOnLabel, (float)n2, (float)(i - this.mGpsOnOffset - this.mTextDescent), (Paint)this.mTextPaint);
            }
            if (this.mHaveFlashlight) {
                canvas.drawText(this.mFlashlightOnLabel, (float)n2, (float)(i - this.mFlashlightOnOffset - this.mTextDescent), (Paint)this.mTextPaint);
            }
            if (this.mHaveCamera) {
                canvas.drawText(this.mCameraOnLabel, (float)n2, (float)(i - this.mCameraOnOffset - this.mTextDescent), (Paint)this.mTextPaint);
            }
            if (this.mHaveWifi) {
                canvas.drawText(this.mWifiRunningLabel, (float)n2, (float)(i - this.mWifiRunningOffset - this.mTextDescent), (Paint)this.mTextPaint);
            }
            canvas.drawText(this.mCpuRunningLabel, (float)n2, (float)(i - this.mCpuRunningOffset - this.mTextDescent), (Paint)this.mTextPaint);
            canvas.drawText(this.mChargingLabel, (float)n2, (float)(i - this.mChargingOffset - this.mTextDescent), (Paint)this.mTextPaint);
            canvas.drawText(this.mScreenOnLabel, (float)n2, (float)(i - this.mScreenOnOffset - this.mTextDescent), (Paint)this.mTextPaint);
            this.mTextPaint.setTextAlign(textAlign2);
        }
        canvas.drawLine((float)(this.mLevelLeft - this.mThinLineWidth), (float)this.mLevelTop, (float)(this.mLevelLeft - this.mThinLineWidth), (float)(this.mLevelBottom + this.mThinLineWidth / 2), (Paint)this.mTextPaint);
        if (this.mLargeMode) {
            int n18;
            for (i = 0; i < 10; ++i) {
                n18 = this.mLevelTop + this.mThinLineWidth / 2 + (this.mLevelBottom - this.mLevelTop) * i / 10;
                canvas.drawLine((float)(this.mLevelLeft - this.mThinLineWidth * 2 - this.mThinLineWidth / 2), (float)n18, (float)(this.mLevelLeft - this.mThinLineWidth - this.mThinLineWidth / 2), (float)n18, (Paint)this.mTextPaint);
            }
        }
        canvas.drawText(this.mMaxPercentLabelString, 0.0f, (float)this.mLevelTop, (Paint)this.mTextPaint);
        canvas.drawText(this.mMinPercentLabelString, (float)(this.mMaxPercentLabelStringWidth - this.mMinPercentLabelStringWidth), (float)(this.mLevelBottom - this.mThinLineWidth), (Paint)this.mTextPaint);
        canvas.drawLine((float)(this.mLevelLeft / 2), (float)(this.mLevelBottom + this.mThinLineWidth), (float)n, (float)(this.mLevelBottom + this.mThinLineWidth), (Paint)this.mTextPaint);
        if (this.mDateLabels.size() > 0) {
            i = this.mLevelTop + this.mTextAscent;
            final int mLevelBottom2 = this.mLevelBottom;
            final int mLevelRight = this.mLevelRight;
            this.mTextPaint.setTextAlign(Paint$Align.LEFT);
            final int n19 = this.mDateLabels.size() - 1;
            n = n14;
            for (int k = n19; k >= 0; --k) {
                final DateLabel dateLabel = this.mDateLabels.get(k);
                final int x = dateLabel.x;
                final int mThinLineWidth2 = this.mThinLineWidth;
                int n20 = dateLabel.x + this.mThinLineWidth * 2;
                int n21 = 0;
                Label_1818: {
                    if (dateLabel.width + n20 < mLevelRight) {
                        n21 = x - mThinLineWidth2;
                        break Label_1818;
                    }
                    n20 = dateLabel.x - this.mThinLineWidth * 2 - dateLabel.width;
                    if ((n21 = n20 - this.mThinLineWidth) < mLevelRight) {
                        break Label_1818;
                    }
                    continue;
                }
                if (n21 >= this.mLevelLeft) {
                    this.mDateLinePath.reset();
                    this.mDateLinePath.moveTo((float)dateLabel.x, (float)i);
                    this.mDateLinePath.lineTo((float)dateLabel.x, (float)mLevelBottom2);
                    canvas.drawPath(this.mDateLinePath, this.mDateLinePaint);
                    canvas.drawText(dateLabel.label, (float)n20, (float)(i - this.mTextAscent), (Paint)this.mTextPaint);
                }
            }
        }
    }
    
    void finishPaths(final int n, final int n2, final int n3, final int n4, final int n5, final Path path, final int n6, final boolean b, final boolean b2, final boolean b3, final boolean b4, final boolean b5, final boolean b6, final boolean b7, final Path path2) {
        if (path != null) {
            if (n6 >= 0 && n6 < n) {
                if (path2 != null) {
                    path2.lineTo((float)n, (float)n5);
                }
                path.lineTo((float)n, (float)n5);
            }
            path.lineTo((float)n, (float)(this.mLevelTop + n3));
            path.lineTo((float)n4, (float)(this.mLevelTop + n3));
            path.close();
        }
        if (b) {
            this.mChargingPath.lineTo((float)n, (float)(n2 - this.mChargingOffset));
        }
        if (b2) {
            this.mScreenOnPath.lineTo((float)n, (float)(n2 - this.mScreenOnOffset));
        }
        if (b3) {
            this.mGpsOnPath.lineTo((float)n, (float)(n2 - this.mGpsOnOffset));
        }
        if (b4) {
            this.mFlashlightOnPath.lineTo((float)n, (float)(n2 - this.mFlashlightOnOffset));
        }
        if (b5) {
            this.mCameraOnPath.lineTo((float)n, (float)(n2 - this.mCameraOnOffset));
        }
        if (b6) {
            this.mWifiRunningPath.lineTo((float)n, (float)(n2 - this.mWifiRunningOffset));
        }
        if (b7) {
            this.mCpuRunningPath.lineTo((float)n, (float)(n2 - this.mCpuRunningOffset));
        }
        if (this.mHavePhoneSignal) {
            this.mPhoneSignalChart.finish(n);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        this.drawChart(canvas, this.getWidth(), this.getHeight());
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.mMaxPercentLabelStringWidth = (int)this.mTextPaint.measureText(this.mMaxPercentLabelString);
        this.mMinPercentLabelStringWidth = (int)this.mTextPaint.measureText(this.mMinPercentLabelString);
        this.mDrainStringWidth = (int)this.mHeaderTextPaint.measureText(this.mDrainString);
        this.mChargeLabelStringWidth = (int)this.mHeaderTextPaint.measureText(this.mInfo.chargeLabel.toString());
        this.mChargeDurationStringWidth = (int)this.mHeaderTextPaint.measureText(this.mChargeDurationString);
        this.mTextAscent = (int)this.mTextPaint.ascent();
        this.mTextDescent = (int)this.mTextPaint.descent();
        this.mHeaderTextAscent = (int)this.mHeaderTextPaint.ascent();
        this.mHeaderTextDescent = (int)this.mHeaderTextPaint.descent();
        this.mHeaderHeight = (this.mHeaderTextDescent - this.mHeaderTextAscent) * 2 - this.mTextAscent;
        this.setMeasuredDimension(getDefaultSize(this.getSuggestedMinimumWidth(), n), getDefaultSize(this.mChartMinHeight + this.mHeaderHeight, n2));
    }
    
    protected void onSizeChanged(int mLevelTop, int n, int mLevelLeft, int mPhoneSignalOffset) {
        final int mLastHeight = n;
        super.onSizeChanged(mLevelTop, n, mLevelLeft, mPhoneSignalOffset);
        if (this.mLastWidth == mLevelTop && this.mLastHeight == mLastHeight) {
            return;
        }
        if (this.mLastWidth != 0 && this.mLastHeight != 0) {
            this.mLastWidth = mLevelTop;
            this.mLastHeight = mLastHeight;
            this.mBitmap = null;
            this.mCanvas = null;
            mLevelLeft = this.mTextDescent - this.mTextAscent;
            if (mLastHeight > mLevelLeft * 10 + this.mChartMinHeight) {
                this.mLargeMode = true;
                if (mLastHeight > mLevelLeft * 15) {
                    this.mLineWidth = mLevelLeft / 2;
                }
                else {
                    this.mLineWidth = mLevelLeft / 3;
                }
            }
            else {
                this.mLargeMode = false;
                this.mLineWidth = this.mThinLineWidth;
            }
            if (this.mLineWidth <= 0) {
                this.mLineWidth = 1;
            }
            this.mLevelTop = this.mHeaderHeight;
            this.mLevelLeft = this.mMaxPercentLabelStringWidth + this.mThinLineWidth * 3;
            this.mLevelRight = mLevelTop;
            final int n2 = this.mLevelRight - this.mLevelLeft;
            this.mTextPaint.setStrokeWidth((float)this.mThinLineWidth);
            this.mBatteryGoodPaint.setStrokeWidth((float)this.mThinLineWidth);
            this.mBatteryWarnPaint.setStrokeWidth((float)this.mThinLineWidth);
            this.mBatteryCriticalPaint.setStrokeWidth((float)this.mThinLineWidth);
            this.mChargingPaint.setStrokeWidth((float)this.mLineWidth);
            this.mScreenOnPaint.setStrokeWidth((float)this.mLineWidth);
            this.mGpsOnPaint.setStrokeWidth((float)this.mLineWidth);
            this.mCameraOnPaint.setStrokeWidth((float)this.mLineWidth);
            this.mFlashlightOnPaint.setStrokeWidth((float)this.mLineWidth);
            this.mWifiRunningPaint.setStrokeWidth((float)this.mLineWidth);
            this.mCpuRunningPaint.setStrokeWidth((float)this.mLineWidth);
            this.mDebugRectPaint.setStrokeWidth(1.0f);
            mLevelLeft += this.mLineWidth;
            if (this.mLargeMode) {
                this.mChargingOffset = this.mLineWidth;
                this.mScreenOnOffset = this.mChargingOffset + mLevelLeft;
                this.mCpuRunningOffset = this.mScreenOnOffset + mLevelLeft;
                this.mWifiRunningOffset = this.mCpuRunningOffset + mLevelLeft;
                final int mWifiRunningOffset = this.mWifiRunningOffset;
                if (this.mHaveWifi) {
                    mPhoneSignalOffset = mLevelLeft;
                }
                else {
                    mPhoneSignalOffset = 0;
                }
                this.mGpsOnOffset = mWifiRunningOffset + mPhoneSignalOffset;
                final int mGpsOnOffset = this.mGpsOnOffset;
                if (this.mHaveGps) {
                    mPhoneSignalOffset = mLevelLeft;
                }
                else {
                    mPhoneSignalOffset = 0;
                }
                this.mFlashlightOnOffset = mGpsOnOffset + mPhoneSignalOffset;
                final int mFlashlightOnOffset = this.mFlashlightOnOffset;
                if (this.mHaveFlashlight) {
                    mPhoneSignalOffset = mLevelLeft;
                }
                else {
                    mPhoneSignalOffset = 0;
                }
                this.mCameraOnOffset = mFlashlightOnOffset + mPhoneSignalOffset;
                final int mCameraOnOffset = this.mCameraOnOffset;
                if (this.mHaveCamera) {
                    mPhoneSignalOffset = mLevelLeft;
                }
                else {
                    mPhoneSignalOffset = 0;
                }
                this.mPhoneSignalOffset = mCameraOnOffset + mPhoneSignalOffset;
                mPhoneSignalOffset = this.mPhoneSignalOffset;
                if (!this.mHavePhoneSignal) {
                    mLevelLeft = 0;
                }
                this.mLevelOffset = mPhoneSignalOffset + mLevelLeft + this.mLineWidth * 2 + this.mLineWidth / 2;
                if (this.mHavePhoneSignal) {
                    this.mPhoneSignalChart.init(mLevelTop);
                }
            }
            else {
                this.mPhoneSignalOffset = 0;
                this.mChargingOffset = 0;
                this.mCpuRunningOffset = 0;
                this.mWifiRunningOffset = 0;
                this.mFlashlightOnOffset = 0;
                this.mCameraOnOffset = 0;
                this.mGpsOnOffset = 0;
                this.mScreenOnOffset = 0;
                this.mLevelOffset = mLevelLeft + this.mThinLineWidth * 4;
                if (this.mHavePhoneSignal) {
                    this.mPhoneSignalChart.init(0);
                }
            }
            this.mBatLevelPath.reset();
            this.mBatGoodPath.reset();
            this.mBatWarnPath.reset();
            this.mTimeRemainPath.reset();
            this.mBatCriticalPath.reset();
            this.mScreenOnPath.reset();
            this.mGpsOnPath.reset();
            this.mFlashlightOnPath.reset();
            this.mCameraOnPath.reset();
            this.mWifiRunningPath.reset();
            this.mCpuRunningPath.reset();
            this.mChargingPath.reset();
            this.mTimeLabels.clear();
            this.mDateLabels.clear();
            final long mStartWallTime = this.mStartWallTime;
            long n3;
            if (this.mEndWallTime > mStartWallTime) {
                n3 = this.mEndWallTime - mStartWallTime;
            }
            else {
                n3 = 1L;
            }
            long mStartWallTime2 = this.mStartWallTime;
            final int mBatLow = this.mBatLow;
            final int n4 = this.mBatHigh - this.mBatLow;
            final int n5 = mLastHeight - this.mLevelOffset - this.mLevelTop;
            this.mLevelBottom = this.mLevelTop + n5;
            final int mLevelLeft2 = this.mLevelLeft;
            mLevelLeft = this.mLevelLeft;
            mPhoneSignalOffset = -1;
            final int n6 = -1;
            Path mBatLevelPath = null;
            final Path path = null;
            boolean b = false;
            boolean b2 = false;
            boolean b3 = false;
            boolean b4 = false;
            boolean b5 = false;
            boolean b6 = false;
            final int mNumHist = this.mNumHist;
            boolean b9;
            boolean b10;
            byte b21;
            Path path6;
            int n23;
            int n24;
            if (this.mEndDataWallTime > this.mStartWallTime && this.mStats.startIteratingHistoryLocked()) {
                final BatteryStats$HistoryItem batteryStats$HistoryItem = new BatteryStats$HistoryItem();
                int n7 = -1;
                boolean b7 = false;
                boolean b8 = false;
                b9 = false;
                b10 = false;
                boolean b11 = false;
                boolean b12 = false;
                int n8 = 0;
                int n9 = mLevelLeft2;
                Path path2 = null;
                int n10 = -1;
                final int n11 = 0;
                int n12 = 0;
                Path mBatLevelPath2 = null;
                long time = 0L;
                boolean b13 = false;
                mPhoneSignalOffset = mLevelLeft;
                mLevelLeft = n11;
                while (true) {
                    final int n13 = n;
                    if (!this.mStats.getNextHistoryLocked(batteryStats$HistoryItem) || n12 >= mNumHist) {
                        break;
                    }
                    long currentTime;
                    long time2;
                    int n17;
                    int n19;
                    if (batteryStats$HistoryItem.isDeltaData()) {
                        currentTime = mStartWallTime2 + (batteryStats$HistoryItem.time - time);
                        time2 = batteryStats$HistoryItem.time;
                        if ((mLevelLeft = this.mLevelLeft + (int)((currentTime - mStartWallTime) * n2 / n3)) < 0) {
                            mLevelLeft = 0;
                        }
                        final int n14 = this.mLevelTop + n5 - (batteryStats$HistoryItem.batteryLevel - mBatLow) * (n5 - 1) / n4;
                        int n15;
                        if (n7 != mLevelLeft && n10 != n14) {
                            final byte batteryLevel = batteryStats$HistoryItem.batteryLevel;
                            Path path3;
                            if (batteryLevel <= this.mBatteryCriticalLevel) {
                                path3 = this.mBatCriticalPath;
                            }
                            else if (batteryLevel <= this.mBatteryWarnLevel) {
                                path3 = this.mBatWarnPath;
                            }
                            else {
                                path3 = null;
                            }
                            Path path4;
                            if (path3 != path2) {
                                if (path2 != null) {
                                    path2.lineTo((float)mLevelLeft, (float)n14);
                                }
                                if (path3 != null) {
                                    path3.moveTo((float)mLevelLeft, (float)n14);
                                }
                                path4 = path3;
                            }
                            else {
                                path4 = path2;
                                if (path3 != null) {
                                    path3.lineTo((float)mLevelLeft, (float)n14);
                                    path4 = path2;
                                }
                            }
                            if (mBatLevelPath2 == null) {
                                mBatLevelPath2 = this.mBatLevelPath;
                                mBatLevelPath2.moveTo((float)mLevelLeft, (float)n14);
                                mPhoneSignalOffset = mLevelLeft;
                            }
                            else {
                                mBatLevelPath2.lineTo((float)mLevelLeft, (float)n14);
                            }
                            n7 = mLevelLeft;
                            n10 = n14;
                            path2 = path4;
                            n15 = mPhoneSignalOffset;
                        }
                        else {
                            n15 = mPhoneSignalOffset;
                        }
                        if (this.mLargeMode) {
                            final boolean b14 = (batteryStats$HistoryItem.states & 0x80000) != 0x0;
                            if (b14 != b13) {
                                if (b14) {
                                    this.mChargingPath.moveTo((float)mLevelLeft, (float)(n13 - this.mChargingOffset));
                                }
                                else {
                                    this.mChargingPath.lineTo((float)mLevelLeft, (float)(n13 - this.mChargingOffset));
                                }
                                b13 = b14;
                            }
                            final boolean b15 = (batteryStats$HistoryItem.states & 0x100000) != 0x0;
                            if (b15 != b7) {
                                if (b15) {
                                    this.mScreenOnPath.moveTo((float)mLevelLeft, (float)(n13 - this.mScreenOnOffset));
                                }
                                else {
                                    this.mScreenOnPath.lineTo((float)mLevelLeft, (float)(n13 - this.mScreenOnOffset));
                                }
                                b7 = b15;
                            }
                            final boolean b16 = (batteryStats$HistoryItem.states & 0x20000000) != 0x0;
                            if (b16 != b8) {
                                if (b16) {
                                    this.mGpsOnPath.moveTo((float)mLevelLeft, (float)(n13 - this.mGpsOnOffset));
                                }
                                else {
                                    this.mGpsOnPath.lineTo((float)mLevelLeft, (float)(n13 - this.mGpsOnOffset));
                                }
                                b8 = b16;
                            }
                            boolean b17 = (batteryStats$HistoryItem.states2 & 0x8000000) != 0x0;
                            if (b17 != b9) {
                                if (b17) {
                                    this.mFlashlightOnPath.moveTo((float)mLevelLeft, (float)(n13 - this.mFlashlightOnOffset));
                                }
                                else {
                                    this.mFlashlightOnPath.lineTo((float)mLevelLeft, (float)(n13 - this.mFlashlightOnOffset));
                                }
                            }
                            else {
                                b17 = b9;
                            }
                            final boolean b18 = (batteryStats$HistoryItem.states2 & 0x200000) != 0x0;
                            if (b18 != b10) {
                                if (b18) {
                                    this.mCameraOnPath.moveTo((float)mLevelLeft, (float)(n13 - this.mCameraOnOffset));
                                }
                                else {
                                    this.mCameraOnPath.lineTo((float)mLevelLeft, (float)(n13 - this.mCameraOnOffset));
                                }
                                b10 = b18;
                            }
                            mPhoneSignalOffset = (batteryStats$HistoryItem.states2 & 0xF) >> 0;
                            boolean b19 = false;
                            Label_1894: {
                                if (n8 != mPhoneSignalOffset) {
                                    Label_1877: {
                                        switch (mPhoneSignalOffset) {
                                            default: {
                                                switch (mPhoneSignalOffset) {
                                                    default: {
                                                        b19 = true;
                                                        b5 = true;
                                                        break Label_1894;
                                                    }
                                                    case 11:
                                                    case 12: {
                                                        break Label_1877;
                                                    }
                                                }
                                                break;
                                            }
                                            case 0:
                                            case 1:
                                            case 2:
                                            case 3: {
                                                b19 = false;
                                                b5 = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    mPhoneSignalOffset = n8;
                                    b19 = b5;
                                }
                            }
                            if ((batteryStats$HistoryItem.states & 0x18010000) != 0x0) {
                                b5 = true;
                            }
                            if (b5 != b11) {
                                if (b5) {
                                    this.mWifiRunningPath.moveTo((float)mLevelLeft, (float)(n13 - this.mWifiRunningOffset));
                                }
                                else {
                                    this.mWifiRunningPath.lineTo((float)mLevelLeft, (float)(n13 - this.mWifiRunningOffset));
                                }
                                b11 = b5;
                            }
                            final boolean b20 = (batteryStats$HistoryItem.states & Integer.MIN_VALUE) != 0x0;
                            if (b20 != b12) {
                                if (b20) {
                                    this.mCpuRunningPath.moveTo((float)mLevelLeft, (float)(n13 - this.mCpuRunningOffset));
                                }
                                else {
                                    this.mCpuRunningPath.lineTo((float)mLevelLeft, (float)(n13 - this.mCpuRunningOffset));
                                }
                                b12 = b20;
                            }
                            if (this.mLargeMode && this.mHavePhoneSignal) {
                                int n16;
                                if ((batteryStats$HistoryItem.states & 0x1C0) >> 6 == 3) {
                                    n16 = 0;
                                }
                                else if ((batteryStats$HistoryItem.states & 0x200000) != 0x0) {
                                    n16 = 1;
                                }
                                else {
                                    n16 = ((batteryStats$HistoryItem.states & 0x38) >> 3) + 2;
                                }
                                this.mPhoneSignalChart.addTick(mLevelLeft, n16);
                            }
                            n17 = n14;
                            final int n18 = mLevelLeft;
                            mLevelLeft = mPhoneSignalOffset;
                            b5 = b19;
                            b9 = b17;
                            mPhoneSignalOffset = n18;
                            n19 = n15;
                        }
                        else {
                            final int n20 = n8;
                            n17 = n14;
                            mPhoneSignalOffset = mLevelLeft;
                            mLevelLeft = n20;
                            n19 = n15;
                        }
                    }
                    else {
                        if (batteryStats$HistoryItem.cmd != 5 && batteryStats$HistoryItem.cmd != 7) {
                            currentTime = mStartWallTime2;
                        }
                        else {
                            if (batteryStats$HistoryItem.currentTime >= this.mStartWallTime) {
                                currentTime = batteryStats$HistoryItem.currentTime;
                            }
                            else {
                                currentTime = this.mStartWallTime + (batteryStats$HistoryItem.time - this.mHistStart);
                            }
                            time = batteryStats$HistoryItem.time;
                        }
                        if (batteryStats$HistoryItem.cmd != 6) {
                            if (batteryStats$HistoryItem.cmd != 5 || Math.abs(mStartWallTime2 - currentTime) > 3600000L) {
                                if (mBatLevelPath2 != null) {
                                    this.finishPaths(n9 + 1, n13, n5, mPhoneSignalOffset, n10, mBatLevelPath2, n7, b13, b7, b8, b9, b10, b11, b12, path2);
                                    n10 = -1;
                                    n7 = -1;
                                    mBatLevelPath2 = null;
                                    path2 = null;
                                    b10 = false;
                                    b9 = false;
                                    b8 = false;
                                    b7 = false;
                                    b13 = false;
                                    b12 = false;
                                }
                            }
                        }
                        final int n21 = n8;
                        time2 = time;
                        n19 = mPhoneSignalOffset;
                        n17 = mLevelLeft;
                        mPhoneSignalOffset = n9;
                        mLevelLeft = n21;
                    }
                    ++n12;
                    final int n22 = mLevelLeft;
                    n9 = mPhoneSignalOffset;
                    mLevelLeft = n17;
                    mPhoneSignalOffset = n19;
                    time = time2;
                    mStartWallTime2 = currentTime;
                    n8 = n22;
                }
                b21 = (byte)mBatLow;
                this.mStats.finishIteratingHistoryLocked();
                final boolean b22 = b8;
                mLevelLeft = n7;
                final boolean b23 = b7;
                b4 = b11;
                b = b13;
                b6 = b12;
                final Path path5 = mBatLevelPath2;
                path6 = path2;
                n23 = n10;
                n24 = mPhoneSignalOffset;
                mBatLevelPath = path5;
                b2 = b23;
                b3 = b22;
            }
            else {
                b9 = false;
                b10 = false;
                b21 = (byte)mBatLow;
                path6 = path;
                n24 = mLevelLeft;
                n23 = n6;
                mLevelLeft = mPhoneSignalOffset;
            }
            int mLevelLeft3;
            if (n23 >= 0 && mLevelLeft >= 0) {
                if ((mPhoneSignalOffset = this.mLevelLeft + (int)((this.mEndDataWallTime - mStartWallTime) * n2 / n3)) < 0) {
                    mPhoneSignalOffset = 0;
                }
                mLevelLeft3 = mLevelLeft;
            }
            else {
                mLevelLeft3 = this.mLevelLeft;
                mLevelLeft = this.mLevelTop + n5 - (this.mInfo.batteryLevel - b21) * (n5 - 1) / n4;
                mPhoneSignalOffset = (byte)this.mInfo.batteryLevel;
                Path path7;
                if (mPhoneSignalOffset <= this.mBatteryCriticalLevel) {
                    path7 = this.mBatCriticalPath;
                }
                else if (mPhoneSignalOffset <= this.mBatteryWarnLevel) {
                    path7 = this.mBatWarnPath;
                }
                else {
                    path7 = null;
                }
                if (path7 != null) {
                    path7.moveTo((float)mLevelLeft3, (float)mLevelLeft);
                    path6 = path7;
                }
                this.mBatLevelPath.moveTo((float)mLevelLeft3, (float)mLevelLeft);
                mBatLevelPath = this.mBatLevelPath;
                mPhoneSignalOffset = mLevelTop;
                n23 = mLevelLeft;
            }
            this.finishPaths(mPhoneSignalOffset, n, n5, n24, n23, mBatLevelPath, mLevelLeft3, b, b2, b3, b9, b10, b4, b6, path6);
            if (mPhoneSignalOffset < mLevelTop) {
                this.mTimeRemainPath.moveTo((float)mPhoneSignalOffset, (float)n23);
                mLevelTop = this.mLevelTop;
                n = (100 - b21) * (n5 - 1) / n4;
                mLevelLeft = this.mLevelTop + n5 - (0 - b21) * (n5 - 1) / n4;
                if (this.mInfo.discharging) {
                    this.mTimeRemainPath.lineTo((float)this.mLevelRight, (float)mLevelLeft);
                }
                else {
                    this.mTimeRemainPath.lineTo((float)this.mLevelRight, (float)(mLevelTop + n5 - n));
                    this.mTimeRemainPath.lineTo((float)this.mLevelRight, (float)mLevelLeft);
                }
                this.mTimeRemainPath.lineTo((float)mPhoneSignalOffset, (float)mLevelLeft);
                this.mTimeRemainPath.close();
            }
            mLevelTop = 0;
            if (this.mStartWallTime > 0L && this.mEndWallTime > this.mStartWallTime) {
                final boolean is24Hour = this.is24Hour();
                final Calendar instance = Calendar.getInstance();
                instance.setTimeInMillis(this.mStartWallTime);
                instance.set(14, mLevelTop);
                instance.set(13, mLevelTop);
                instance.set(12, mLevelTop);
                long n25 = instance.getTimeInMillis();
                if (n25 < this.mStartWallTime) {
                    instance.set(11, instance.get(11) + 1);
                    n25 = instance.getTimeInMillis();
                }
                final Calendar instance2 = Calendar.getInstance();
                instance2.setTimeInMillis(this.mEndWallTime);
                instance2.set(14, mLevelTop);
                instance2.set(13, mLevelTop);
                instance2.set(12, mLevelTop);
                final long timeInMillis = instance2.getTimeInMillis();
                if (n25 < timeInMillis) {
                    this.addTimeLabel(instance, this.mLevelLeft, this.mLevelRight, is24Hour);
                    final Calendar instance3 = Calendar.getInstance();
                    instance3.setTimeInMillis(this.mStartWallTime + (this.mEndWallTime - this.mStartWallTime) / 2L);
                    instance3.set(14, 0);
                    instance3.set(13, 0);
                    instance3.set(12, 0);
                    final long timeInMillis2 = instance3.getTimeInMillis();
                    if (timeInMillis2 > n25 && timeInMillis2 < timeInMillis) {
                        this.addTimeLabel(instance3, this.mLevelLeft, this.mLevelRight, is24Hour);
                    }
                    this.addTimeLabel(instance2, this.mLevelLeft, this.mLevelRight, is24Hour);
                }
                if (instance.get(6) != instance2.get(6) || instance.get(1) != instance2.get(1)) {
                    final boolean dayFirst = this.isDayFirst();
                    instance.set(11, 0);
                    long n26;
                    if ((n26 = instance.getTimeInMillis()) < this.mStartWallTime) {
                        instance.set(6, instance.get(6) + 1);
                        n26 = instance.getTimeInMillis();
                    }
                    instance2.set(11, 0);
                    final long timeInMillis3 = instance2.getTimeInMillis();
                    if (n26 < timeInMillis3) {
                        this.addDateLabel(instance, this.mLevelLeft, this.mLevelRight, dayFirst);
                        final Calendar instance4 = Calendar.getInstance();
                        instance4.setTimeInMillis((timeInMillis3 - n26) / 2L + n26 + 7200000L);
                        instance4.set(11, 0);
                        instance4.set(12, 0);
                        final long timeInMillis4 = instance4.getTimeInMillis();
                        if (timeInMillis4 > n26 && timeInMillis4 < timeInMillis3) {
                            this.addDateLabel(instance4, this.mLevelLeft, this.mLevelRight, dayFirst);
                        }
                    }
                    this.addDateLabel(instance2, this.mLevelLeft, this.mLevelRight, dayFirst);
                }
            }
            if (this.mTimeLabels.size() < 2) {
                this.mDurationString = Formatter.formatShortElapsedTime(this.getContext(), this.mEndWallTime - this.mStartWallTime);
                this.mDurationStringWidth = (int)this.mTextPaint.measureText(this.mDurationString);
            }
            else {
                this.mDurationString = null;
                this.mDurationStringWidth = 0;
            }
        }
    }
    
    static class ChartData
    {
        int[] mColors;
        int mLastBin;
        int mNumTicks;
        Paint[] mPaints;
        int[] mTicks;
        
        void addTick(final int n, final int mLastBin) {
            if (mLastBin != this.mLastBin && this.mNumTicks < this.mTicks.length) {
                this.mTicks[this.mNumTicks] = ((0xFFFF & n) | mLastBin << 16);
                ++this.mNumTicks;
                this.mLastBin = mLastBin;
            }
        }
        
        void draw(final Canvas canvas, final int n, final int n2) {
            int n3 = 0;
            int n4 = 0;
            for (int i = 0; i < this.mNumTicks; ++i) {
                final int n5 = this.mTicks[i];
                final int n6 = 0xFFFF & n5;
                if (n3 != 0) {
                    canvas.drawRect((float)n4, (float)n, (float)n6, (float)(n + n2), this.mPaints[n3]);
                }
                n3 = (0xFFFF0000 & n5) >> 16;
                n4 = n6;
            }
        }
        
        void finish(final int n) {
            if (this.mLastBin != 0) {
                this.addTick(n, 0);
            }
        }
        
        void init(final int n) {
            if (n > 0) {
                this.mTicks = new int[n * 2];
            }
            else {
                this.mTicks = null;
            }
            this.mNumTicks = 0;
            this.mLastBin = 0;
        }
        
        void setColors(final int[] mColors) {
            this.mColors = mColors;
            this.mPaints = new Paint[mColors.length];
            for (int i = 0; i < mColors.length; ++i) {
                (this.mPaints[i] = new Paint()).setColor(mColors[i]);
                this.mPaints[i].setStyle(Paint.Style.FILL);
            }
        }
    }
    
    static class DateLabel
    {
        final String label;
        final int width;
        final int x;
        
        DateLabel(final TextPaint textPaint, final int x, final Calendar calendar, final boolean b) {
            this.x = x;
            final Locale default1 = Locale.getDefault();
            String s;
            if (b) {
                s = "dM";
            }
            else {
                s = "Md";
            }
            this.label = DateFormat.format((CharSequence)DateFormat.getBestDateTimePattern(default1, s), calendar).toString();
            this.width = (int)textPaint.measureText(this.label);
        }
    }
    
    static class TextAttrs
    {
        int styleIndex;
        ColorStateList textColor;
        int textSize;
        int typefaceIndex;
        
        TextAttrs() {
            this.textColor = null;
            this.textSize = 15;
            this.typefaceIndex = -1;
            this.styleIndex = -1;
        }
        
        void apply(final Context context, final TextPaint textPaint) {
            textPaint.density = context.getResources().getDisplayMetrics().density;
            textPaint.setCompatibilityScaling(context.getResources().getCompatibilityInfo().applicationScale);
            textPaint.setColor(this.textColor.getDefaultColor());
            textPaint.setTextSize((float)this.textSize);
            Typeface typeface = null;
            switch (this.typefaceIndex) {
                case 3: {
                    typeface = Typeface.MONOSPACE;
                    break;
                }
                case 2: {
                    typeface = Typeface.SERIF;
                    break;
                }
                case 1: {
                    typeface = Typeface.SANS_SERIF;
                    break;
                }
            }
            this.setTypeface(textPaint, typeface, this.styleIndex);
        }
        
        void retrieve(final Context context, TypedArray obtainStyledAttributes, int i) {
            final TypedArray typedArray = null;
            i = obtainStyledAttributes.getResourceId(i, -1);
            obtainStyledAttributes = typedArray;
            if (i != -1) {
                obtainStyledAttributes = context.obtainStyledAttributes(i, R$styleable.TextAppearance);
            }
            if (obtainStyledAttributes != null) {
                int indexCount;
                int index;
                for (indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                    index = obtainStyledAttributes.getIndex(i);
                    switch (index) {
                        case 3: {
                            this.textColor = obtainStyledAttributes.getColorStateList(index);
                            break;
                        }
                        case 2: {
                            this.styleIndex = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                        case 1: {
                            this.typefaceIndex = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                        case 0: {
                            this.textSize = obtainStyledAttributes.getDimensionPixelSize(index, this.textSize);
                            break;
                        }
                    }
                }
                obtainStyledAttributes.recycle();
            }
        }
        
        public void setTypeface(final TextPaint textPaint, Typeface typeface, int n) {
            float textSkewX = 0.0f;
            boolean fakeBoldText = false;
            if (n > 0) {
                if (typeface == null) {
                    typeface = Typeface.defaultFromStyle(n);
                }
                else {
                    typeface = Typeface.create(typeface, n);
                }
                textPaint.setTypeface(typeface);
                int style;
                if (typeface != null) {
                    style = typeface.getStyle();
                }
                else {
                    style = 0;
                }
                n &= style;
                if ((n & 0x1) != 0x0) {
                    fakeBoldText = true;
                }
                textPaint.setFakeBoldText(fakeBoldText);
                if ((n & 0x2) != 0x0) {
                    textSkewX = -0.25f;
                }
                textPaint.setTextSkewX(textSkewX);
            }
            else {
                textPaint.setFakeBoldText(false);
                textPaint.setTextSkewX(0.0f);
                textPaint.setTypeface(typeface);
            }
        }
    }
    
    static class TimeLabel
    {
        final String label;
        final int width;
        final int x;
        
        TimeLabel(final TextPaint textPaint, final int x, final Calendar calendar, final boolean b) {
            this.x = x;
            final Locale default1 = Locale.getDefault();
            String s;
            if (b) {
                s = "km";
            }
            else {
                s = "ha";
            }
            this.label = DateFormat.format((CharSequence)DateFormat.getBestDateTimePattern(default1, s), calendar).toString();
            this.width = (int)textPaint.measureText(this.label);
        }
    }
}
