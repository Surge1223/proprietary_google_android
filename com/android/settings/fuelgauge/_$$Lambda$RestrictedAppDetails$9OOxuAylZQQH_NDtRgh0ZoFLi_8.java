package com.android.settings.fuelgauge;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.android.settings.Utils;
import android.os.UserHandle;
import com.android.settings.widget.AppCheckBoxPreference;
import android.support.v7.preference.CheckBoxPreference;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.fuelgauge.batterytip.tips.UnrestrictAppTip;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.fuelgauge.batterytip.BatteryTipDialogFragment;
import android.app.Fragment;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.PreferenceGroup;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import com.android.settingslib.widget.FooterPreferenceMixin;
import java.util.List;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settings.fuelgauge.batterytip.BatteryTipPreferenceController;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import android.support.v7.preference.Preference;

public final class _$$Lambda$RestrictedAppDetails$9OOxuAylZQQH_NDtRgh0ZoFLi_8 implements OnPreferenceChangeListener
{
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return RestrictedAppDetails.lambda$refreshUi$0(this.f$0, this.f$1, preference, o);
    }
}
