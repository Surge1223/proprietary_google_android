package com.android.settings.fuelgauge;

import com.android.settings.applications.manageapplications.ManageApplications;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.Settings;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.SettingsActivity;
import com.android.settings.dashboard.DashboardFragment;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BatteryOptimizationPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private PowerWhitelistBackend mBackend;
    private DashboardFragment mFragment;
    private String mPackageName;
    private SettingsActivity mSettingsActivity;
    
    public BatteryOptimizationPreferenceController(final SettingsActivity mSettingsActivity, final DashboardFragment mFragment, final String mPackageName) {
        super((Context)mSettingsActivity);
        this.mFragment = mFragment;
        this.mSettingsActivity = mSettingsActivity;
        this.mPackageName = mPackageName;
        this.mBackend = PowerWhitelistBackend.getInstance((Context)this.mSettingsActivity);
    }
    
    BatteryOptimizationPreferenceController(final SettingsActivity mSettingsActivity, final DashboardFragment mFragment, final String mPackageName, final PowerWhitelistBackend mBackend) {
        super((Context)mSettingsActivity);
        this.mFragment = mFragment;
        this.mSettingsActivity = mSettingsActivity;
        this.mPackageName = mPackageName;
        this.mBackend = mBackend;
    }
    
    @Override
    public String getPreferenceKey() {
        return "battery_optimization";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!"battery_optimization".equals(preference.getKey())) {
            return false;
        }
        final Bundle arguments = new Bundle();
        arguments.putString("classname", Settings.HighPowerApplicationsActivity.class.getName());
        new SubSettingLauncher((Context)this.mSettingsActivity).setDestination(ManageApplications.class.getName()).setArguments(arguments).setTitle(2131887844).setSourceMetricsCategory(this.mFragment.getMetricsCategory()).launch();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        int summary;
        if (this.mBackend.isWhitelisted(this.mPackageName)) {
            summary = 2131887848;
        }
        else {
            summary = 2131887847;
        }
        preference.setSummary(summary);
    }
}
