package com.android.settings.fuelgauge;

import android.text.format.Formatter;
import com.android.settingslib.utils.PowerUtil;
import com.android.settingslib.Utils;
import android.widget.TextView;
import java.util.Iterator;
import com.android.settingslib.utils.StringUtil;
import com.android.settings.core.SubSettingLauncher;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.fuelgauge.anomaly.AnomalyDetectionPolicy;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.core.InstrumentedPreferenceFragment;
import android.support.v14.preference.PreferenceFragment;
import java.util.ArrayList;
import com.android.settings.SettingsActivity;
import com.android.settingslib.core.AbstractPreferenceController;
import android.text.BidiFormatter;
import com.android.settings.fuelgauge.batterytip.BatteryTipLoader;
import android.content.Loader;
import android.os.Bundle;
import android.app.Activity;
import java.util.Collections;
import android.provider.SearchIndexableResource;
import com.android.settings.display.BatteryPercentagePreferenceController;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.applications.LayoutPreference;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import android.util.SparseArray;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.fuelgauge.batterytip.BatteryTipPreferenceController;
import android.view.View$OnLongClickListener;

public class PowerUsageSummary extends PowerUsageBase implements View$OnLongClickListener, BatteryTipListener
{
    static final int BATTERY_INFO_LOADER = 1;
    static final int BATTERY_TIP_LOADER = 2;
    static final int MENU_ADVANCED_BATTERY = 2;
    static final int MENU_STATS_TYPE = 1;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    SparseArray<List<Anomaly>> mAnomalySparseArray;
    BatteryHeaderPreferenceController mBatteryHeaderPreferenceController;
    BatteryInfo mBatteryInfo;
    LoaderManager$LoaderCallbacks<List<BatteryInfo>> mBatteryInfoDebugLoaderCallbacks;
    LoaderManager$LoaderCallbacks<BatteryInfo> mBatteryInfoLoaderCallbacks;
    LayoutPreference mBatteryLayoutPref;
    BatteryTipPreferenceController mBatteryTipPreferenceController;
    private LoaderManager$LoaderCallbacks<List<BatteryTip>> mBatteryTipsCallbacks;
    BatteryUtils mBatteryUtils;
    PowerGaugePreference mLastFullChargePref;
    boolean mNeedUpdateBatteryTip;
    PowerUsageFeatureProvider mPowerFeatureProvider;
    PowerGaugePreference mScreenUsagePref;
    private int mStatsType;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                final BatteryPercentagePreferenceController batteryPercentagePreferenceController = new BatteryPercentagePreferenceController(context);
                if (!batteryPercentagePreferenceController.isAvailable()) {
                    nonIndexableKeys.add(batteryPercentagePreferenceController.getPreferenceKey());
                }
                nonIndexableKeys.add("battery_saver_summary");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082810;
                return Collections.singletonList(searchIndexableResource);
            }
        };
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new PowerUsageSummary.SummaryProvider((Context)activity, summaryLoader);
            }
        };
    }
    
    public PowerUsageSummary() {
        this.mStatsType = 0;
        this.mBatteryInfoLoaderCallbacks = (LoaderManager$LoaderCallbacks<BatteryInfo>)new LoaderManager$LoaderCallbacks<BatteryInfo>() {
            public Loader<BatteryInfo> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<BatteryInfo>)new BatteryInfoLoader(PowerUsageSummary.this.getContext(), PowerUsageSummary.this.mStatsHelper);
            }
            
            public void onLoadFinished(final Loader<BatteryInfo> loader, final BatteryInfo mBatteryInfo) {
                PowerUsageSummary.this.mBatteryHeaderPreferenceController.updateHeaderPreference(mBatteryInfo);
                PowerUsageSummary.this.mBatteryInfo = mBatteryInfo;
                PowerUsageSummary.this.updateLastFullChargePreference();
            }
            
            public void onLoaderReset(final Loader<BatteryInfo> loader) {
            }
        };
        this.mBatteryInfoDebugLoaderCallbacks = (LoaderManager$LoaderCallbacks<List<BatteryInfo>>)new LoaderManager$LoaderCallbacks<List<BatteryInfo>>() {
            public Loader<List<BatteryInfo>> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<List<BatteryInfo>>)new DebugEstimatesLoader(PowerUsageSummary.this.getContext(), PowerUsageSummary.this.mStatsHelper);
            }
            
            public void onLoadFinished(final Loader<List<BatteryInfo>> loader, final List<BatteryInfo> list) {
                PowerUsageSummary.this.updateViews(list);
            }
            
            public void onLoaderReset(final Loader<List<BatteryInfo>> loader) {
            }
        };
        this.mBatteryTipsCallbacks = (LoaderManager$LoaderCallbacks<List<BatteryTip>>)new LoaderManager$LoaderCallbacks<List<BatteryTip>>() {
            public Loader<List<BatteryTip>> onCreateLoader(final int n, final Bundle bundle) {
                return (Loader<List<BatteryTip>>)new BatteryTipLoader(PowerUsageSummary.this.getContext(), PowerUsageSummary.this.mStatsHelper);
            }
            
            public void onLoadFinished(final Loader<List<BatteryTip>> loader, final List<BatteryTip> list) {
                PowerUsageSummary.this.mBatteryTipPreferenceController.updateBatteryTips(list);
            }
            
            public void onLoaderReset(final Loader<List<BatteryTip>> loader) {
            }
        };
    }
    
    static CharSequence getDashboardLabel(final Context context, final BatteryInfo batteryInfo) {
        final BidiFormatter instance = BidiFormatter.getInstance();
        String s;
        if (batteryInfo.remainingLabel == null) {
            s = batteryInfo.batteryPercentString;
        }
        else {
            s = context.getString(2131888608, new Object[] { instance.unicodeWrap(batteryInfo.batteryPercentString), instance.unicodeWrap(batteryInfo.remainingLabel) });
        }
        return s;
    }
    
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final Lifecycle lifecycle = this.getLifecycle();
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        final ArrayList<BatteryPercentagePreferenceController> list = (ArrayList<BatteryPercentagePreferenceController>)new ArrayList<BatteryTipPreferenceController>();
        list.add((BatteryTipPreferenceController)(this.mBatteryHeaderPreferenceController = new BatteryHeaderPreferenceController(context, settingsActivity, this, lifecycle)));
        list.add(this.mBatteryTipPreferenceController = new BatteryTipPreferenceController(context, "battery_tip", (SettingsActivity)this.getActivity(), this, (BatteryTipPreferenceController.BatteryTipListener)this));
        list.add(new BatteryPercentagePreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    AnomalyDetectionPolicy getAnomalyDetectionPolicy() {
        return new AnomalyDetectionPolicy(this.getContext());
    }
    
    public int getHelpResource() {
        return 2131887799;
    }
    
    protected String getLogTag() {
        return "PowerUsageSummary";
    }
    
    public int getMetricsCategory() {
        return 1263;
    }
    
    protected int getPreferenceScreenResId() {
        return 2132082810;
    }
    
    void initFeatureProvider() {
        final Context context = this.getContext();
        this.mPowerFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
    }
    
    public void onBatteryTipHandled(final BatteryTip batteryTip) {
        this.restartBatteryTipLoader();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setAnimationAllowed(true);
        this.initFeatureProvider();
        this.mBatteryLayoutPref = (LayoutPreference)this.findPreference("battery_header");
        this.mScreenUsagePref = (PowerGaugePreference)this.findPreference("screen_usage");
        this.mLastFullChargePref = (PowerGaugePreference)this.findPreference("last_full_charge");
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131886599);
        this.mBatteryUtils = BatteryUtils.getInstance(this.getContext());
        this.mAnomalySparseArray = (SparseArray<List<Anomaly>>)new SparseArray();
        this.restartBatteryInfoLoader();
        this.mBatteryTipPreferenceController.restoreInstanceState(bundle);
        this.updateBatteryTipFlag(bundle);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        menu.add(0, 2, 0, 2131886273);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    public boolean onLongClick(final View view) {
        this.showBothEstimates();
        view.setOnLongClickListener((View$OnLongClickListener)null);
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2: {
                new SubSettingLauncher(this.getContext()).setDestination(PowerUsageAdvanced.class.getName()).setSourceMetricsCategory(this.getMetricsCategory()).setTitle(2131886273).launch();
                return true;
            }
            case 1: {
                if (this.mStatsType == 0) {
                    this.mStatsType = 2;
                }
                else {
                    this.mStatsType = 0;
                }
                this.refreshUi(0);
                return true;
            }
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mBatteryTipPreferenceController.saveInstanceState(bundle);
    }
    
    @Override
    protected void refreshUi(final int n) {
        if (this.getContext() == null) {
            return;
        }
        if (this.mNeedUpdateBatteryTip && n != 1) {
            this.restartBatteryTipLoader();
        }
        else {
            this.mNeedUpdateBatteryTip = true;
        }
        this.restartBatteryInfoLoader();
        this.updateLastFullChargePreference();
        this.mScreenUsagePref.setSubtitle(StringUtil.formatElapsedTime(this.getContext(), this.mBatteryUtils.calculateScreenUsageTime(this.mStatsHelper), false));
    }
    
    void restartBatteryInfoLoader() {
        this.getLoaderManager().restartLoader(1, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this.mBatteryInfoLoaderCallbacks);
        if (this.mPowerFeatureProvider.isEstimateDebugEnabled()) {
            this.mBatteryLayoutPref.findViewById(2131362669).setOnLongClickListener((View$OnLongClickListener)this);
        }
    }
    
    @Override
    protected void restartBatteryStatsLoader(final int n) {
        super.restartBatteryStatsLoader(n);
        this.mBatteryHeaderPreferenceController.quickUpdateHeaderPreference();
    }
    
    void restartBatteryTipLoader() {
        this.getLoaderManager().restartLoader(2, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this.mBatteryTipsCallbacks);
    }
    
    void setBatteryLayoutPreference(final LayoutPreference mBatteryLayoutPref) {
        this.mBatteryLayoutPref = mBatteryLayoutPref;
    }
    
    void showBothEstimates() {
        final Context context = this.getContext();
        if (context != null && this.mPowerFeatureProvider.isEnhancedBatteryPredictionEnabled(context)) {
            this.getLoaderManager().restartLoader(3, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this.mBatteryInfoDebugLoaderCallbacks);
        }
    }
    
    void updateAnomalySparseArray(final List<Anomaly> list) {
        this.mAnomalySparseArray.clear();
        for (final Anomaly anomaly : list) {
            if (this.mAnomalySparseArray.get(anomaly.uid) == null) {
                this.mAnomalySparseArray.append(anomaly.uid, (Object)new ArrayList());
            }
            ((List)this.mAnomalySparseArray.get(anomaly.uid)).add(anomaly);
        }
    }
    
    void updateBatteryTipFlag(final Bundle bundle) {
        this.mNeedUpdateBatteryTip = (bundle == null || this.mBatteryTipPreferenceController.needUpdate());
    }
    
    void updateLastFullChargePreference() {
        if (this.mBatteryInfo != null && this.mBatteryInfo.averageTimeToDischarge != -1L) {
            this.mLastFullChargePref.setTitle(2131886600);
            this.mLastFullChargePref.setSubtitle(StringUtil.formatElapsedTime(this.getContext(), this.mBatteryInfo.averageTimeToDischarge, false));
        }
        else {
            final long calculateLastFullChargeTime = this.mBatteryUtils.calculateLastFullChargeTime(this.mStatsHelper, System.currentTimeMillis());
            this.mLastFullChargePref.setTitle(2131886607);
            this.mLastFullChargePref.setSubtitle(StringUtil.formatRelativeTime(this.getContext(), calculateLastFullChargeTime, false));
        }
    }
    
    protected void updateViews(final List<BatteryInfo> list) {
        final BatteryMeterView batteryMeterView = this.mBatteryLayoutPref.findViewById(2131361906);
        final TextView textView = this.mBatteryLayoutPref.findViewById(2131361908);
        final TextView textView2 = this.mBatteryLayoutPref.findViewById(2131362669);
        final TextView textView3 = this.mBatteryLayoutPref.findViewById(2131362670);
        final BatteryInfo batteryInfo = list.get(0);
        final BatteryInfo batteryInfo2 = list.get(1);
        textView.setText((CharSequence)Utils.formatPercentage(batteryInfo.batteryLevel));
        textView2.setText((CharSequence)this.mPowerFeatureProvider.getOldEstimateDebugString(Formatter.formatShortElapsedTime(this.getContext(), PowerUtil.convertUsToMs(batteryInfo.remainingTimeUs))));
        textView3.setText((CharSequence)this.mPowerFeatureProvider.getEnhancedEstimateDebugString(Formatter.formatShortElapsedTime(this.getContext(), PowerUtil.convertUsToMs(batteryInfo2.remainingTimeUs))));
        batteryMeterView.setBatteryLevel(batteryInfo.batteryLevel);
        batteryMeterView.setCharging(true ^ batteryInfo.discharging);
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final BatteryBroadcastReceiver mBatteryBroadcastReceiver;
        private final Context mContext;
        private final SummaryLoader mLoader;
        
        private SummaryProvider(final Context mContext, final SummaryLoader mLoader) {
            this.mContext = mContext;
            this.mLoader = mLoader;
            (this.mBatteryBroadcastReceiver = new BatteryBroadcastReceiver(this.mContext)).setBatteryChangedListener((BatteryBroadcastReceiver.OnBatteryChangedListener)new _$$Lambda$PowerUsageSummary$SummaryProvider$kRfOu1vb_I8hwLBBDAS0_xe6_pM(this));
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                this.mBatteryBroadcastReceiver.register();
            }
            else {
                this.mBatteryBroadcastReceiver.unRegister();
            }
        }
    }
}
