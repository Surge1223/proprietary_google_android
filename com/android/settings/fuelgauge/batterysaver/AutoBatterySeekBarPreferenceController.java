package com.android.settings.fuelgauge.batterysaver;

import android.net.Uri;
import android.database.ContentObserver;
import android.content.ContentResolver;
import com.android.settingslib.Utils;
import android.util.Log;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import android.os.Handler;
import android.os.Looper;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.widget.SeekBarPreference;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class AutoBatterySeekBarPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener, LifecycleObserver, OnStart, OnStop
{
    static final String KEY_AUTO_BATTERY_SEEK_BAR = "battery_saver_seek_bar";
    private static final String TAG = "AutoBatterySeekBarPreferenceController";
    private AutoBatterySaverSettingObserver mContentObserver;
    private SeekBarPreference mPreference;
    
    public AutoBatterySeekBarPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "battery_saver_seek_bar");
        this.mContentObserver = new AutoBatterySaverSettingObserver(new Handler(Looper.getMainLooper()));
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = (SeekBarPreference)preferenceScreen.findPreference("battery_saver_seek_bar")).setContinuousUpdates(true);
        this.mPreference.setAccessibilityRangeInfoType(2);
        this.updatePreference(this.mPreference);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "low_power_trigger_level", (int)o);
        return true;
    }
    
    @Override
    public void onStart() {
        this.mContentObserver.registerContentObserver();
    }
    
    @Override
    public void onStop() {
        this.mContentObserver.unRegisterContentObserver();
    }
    
    void updatePreference(final Preference preference) {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        final int int1 = Settings.Global.getInt(contentResolver, "low_power_trigger_level_max", 0);
        if (int1 > 0) {
            if (!(preference instanceof SeekBarPreference)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected preference class: ");
                sb.append(preference.getClass());
                Log.e("AutoBatterySeekBarPreferenceController", sb.toString());
            }
            else {
                final SeekBarPreference seekBarPreference = (SeekBarPreference)preference;
                if (int1 < seekBarPreference.getMin()) {
                    Log.e("AutoBatterySeekBarPreferenceController", "LOW_POWER_MODE_TRIGGER_LEVEL_MAX too low; ignored.");
                }
                else {
                    seekBarPreference.setMax(int1);
                }
            }
        }
        final int int2 = Settings.Global.getInt(contentResolver, "low_power_trigger_level", 0);
        if (int2 == 0) {
            preference.setVisible(false);
        }
        else {
            preference.setVisible(true);
            preference.setTitle(this.mContext.getString(2131886625, new Object[] { Utils.formatPercentage(int2) }));
            final SeekBarPreference seekBarPreference2 = (SeekBarPreference)preference;
            seekBarPreference2.setProgress(int2);
            seekBarPreference2.setSeekBarContentDescription(this.mContext.getString(2131886629));
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.updatePreference(preference);
    }
    
    private final class AutoBatterySaverSettingObserver extends ContentObserver
    {
        private final ContentResolver mContentResolver;
        private final Uri mUri;
        
        public AutoBatterySaverSettingObserver(final Handler handler) {
            super(handler);
            this.mUri = Settings.Global.getUriFor("low_power_trigger_level");
            this.mContentResolver = AutoBatterySeekBarPreferenceController.this.mContext.getContentResolver();
        }
        
        public void onChange(final boolean b, final Uri uri, final int n) {
            if (this.mUri.equals((Object)uri)) {
                AutoBatterySeekBarPreferenceController.this.updatePreference(AutoBatterySeekBarPreferenceController.this.mPreference);
            }
        }
        
        public void registerContentObserver() {
            this.mContentResolver.registerContentObserver(this.mUri, false, (ContentObserver)this);
        }
        
        public void unRegisterContentObserver() {
            this.mContentResolver.unregisterContentObserver((ContentObserver)this);
        }
    }
}
