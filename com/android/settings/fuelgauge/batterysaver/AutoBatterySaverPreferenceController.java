package com.android.settings.fuelgauge.batterysaver;

import com.android.settingslib.fuelgauge.BatterySaverUtils;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.core.TogglePreferenceController;

public class AutoBatterySaverPreferenceController extends TogglePreferenceController implements OnPreferenceChangeListener
{
    static final int DEFAULT_TRIGGER_LEVEL = 0;
    static final String KEY_AUTO_BATTERY_SAVER = "auto_battery_saver";
    private final int mDefaultTriggerLevelForOn;
    
    public AutoBatterySaverPreferenceController(final Context context) {
        super(context, "auto_battery_saver");
        this.mDefaultTriggerLevelForOn = this.mContext.getResources().getInteger(17694805);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "low_power_trigger_level", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        final Context mContext = this.mContext;
        int mDefaultTriggerLevelForOn;
        if (b) {
            mDefaultTriggerLevelForOn = this.mDefaultTriggerLevelForOn;
        }
        else {
            mDefaultTriggerLevelForOn = 0;
        }
        BatterySaverUtils.setAutoBatterySaverTriggerLevel(mContext, mDefaultTriggerLevelForOn);
        return true;
    }
}
