package com.android.settings.fuelgauge.batterysaver;

import android.support.v7.preference.Preference;
import com.android.settingslib.fuelgauge.BatterySaverUtils;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settings.widget.TwoStateButtonPreference;
import android.os.PowerManager;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.fuelgauge.BatterySaverReceiver;
import com.android.settings.core.TogglePreferenceController;

public class BatterySaverButtonPreferenceController extends TogglePreferenceController implements BatterySaverListener, LifecycleObserver, OnStart, OnStop
{
    private final BatterySaverReceiver mBatterySaverReceiver;
    private final PowerManager mPowerManager;
    private TwoStateButtonPreference mPreference;
    
    public BatterySaverButtonPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mPowerManager = (PowerManager)context.getSystemService("power");
        (this.mBatterySaverReceiver = new BatterySaverReceiver(context)).setBatterySaverListener((BatterySaverReceiver.BatterySaverListener)this);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (TwoStateButtonPreference)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean isChecked() {
        return this.mPowerManager.isPowerSaveMode();
    }
    
    @Override
    public boolean isSliceable() {
        return true;
    }
    
    @Override
    public void onBatteryChanged(final boolean b) {
        if (this.mPreference != null) {
            this.mPreference.setButtonEnabled(b ^ true);
        }
    }
    
    @Override
    public void onPowerSaveModeChanged() {
        final boolean checked = this.isChecked();
        if (this.mPreference != null && this.mPreference.isChecked() != checked) {
            this.mPreference.setChecked(checked);
        }
    }
    
    @Override
    public void onStart() {
        this.mBatterySaverReceiver.setListening(true);
    }
    
    @Override
    public void onStop() {
        this.mBatterySaverReceiver.setListening(false);
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        return BatterySaverUtils.setPowerSaveMode(this.mContext, b, false);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        if (this.mPreference != null) {
            this.mPreference.setChecked(this.isChecked());
        }
    }
}
