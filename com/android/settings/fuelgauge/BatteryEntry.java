package com.android.settings.fuelgauge;

import com.android.internal.os.BatterySipper;
import android.content.pm.PackageInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.os.RemoteException;
import android.os.UserHandle;
import android.app.AppGlobals;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.android.settingslib.Utils;
import android.os.UserManager;
import com.android.internal.os.BatterySipper;
import android.graphics.drawable.Drawable;
import android.content.Context;
import java.util.HashMap;
import android.os.Handler;
import java.util.Locale;
import java.util.ArrayList;

public class BatteryEntry
{
    static final ArrayList<BatteryEntry> mRequestQueue;
    private static NameAndIconLoader mRequestThread;
    static Locale sCurrentLocale;
    static Handler sHandler;
    static final HashMap<String, UidToDetail> sUidCache;
    public final Context context;
    public String defaultPackageName;
    public Drawable icon;
    public int iconId;
    public String name;
    public final BatterySipper sipper;
    
    static {
        sUidCache = new HashMap<String, UidToDetail>();
        mRequestQueue = new ArrayList<BatteryEntry>();
        BatteryEntry.sCurrentLocale = null;
    }
    
    public BatteryEntry(final Context context, final Handler sHandler, final UserManager userManager, final BatterySipper sipper) {
        BatteryEntry.sHandler = sHandler;
        this.context = context;
        this.sipper = sipper;
        switch (sipper.drainType) {
            case AMBIENT_DISPLAY: {
                this.name = context.getResources().getString(2131886304);
                this.iconId = 2131231112;
                break;
            }
            case CAMERA: {
                this.name = context.getResources().getString(2131888581);
                this.iconId = 2131231119;
                break;
            }
            case OVERCOUNTED: {
                this.name = context.getResources().getString(2131888596);
                this.iconId = 2131231097;
                break;
            }
            case UNACCOUNTED: {
                this.name = context.getResources().getString(2131888610);
                this.iconId = 2131231097;
                break;
            }
            case USER: {
                final UserInfo userInfo = userManager.getUserInfo(sipper.userId);
                if (userInfo != null) {
                    this.icon = Utils.getUserIcon(context, userManager, userInfo);
                    this.name = Utils.getUserLabel(context, userInfo);
                }
                else {
                    this.icon = null;
                    this.name = context.getResources().getString(2131888865);
                }
                break;
            }
            case APP: {
                final PackageManager packageManager = context.getPackageManager();
                sipper.mPackages = packageManager.getPackagesForUid(sipper.uidObj.getUid());
                if (sipper.mPackages != null && sipper.mPackages.length == 1) {
                    this.defaultPackageName = packageManager.getPackagesForUid(sipper.uidObj.getUid())[0];
                    try {
                        this.name = packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.defaultPackageName, 0)).toString();
                    }
                    catch (PackageManager$NameNotFoundException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("PackageManager failed to retrieve ApplicationInfo for: ");
                        sb.append(this.defaultPackageName);
                        Log.d("BatteryEntry", sb.toString());
                        this.name = this.defaultPackageName;
                    }
                    break;
                }
                this.name = sipper.packageWithHighestDrain;
                break;
            }
            case FLASHLIGHT: {
                this.name = context.getResources().getString(2131888593);
                this.iconId = 2131231125;
                break;
            }
            case SCREEN: {
                this.name = context.getResources().getString(2131888609);
                this.iconId = 2131231125;
                break;
            }
            case BLUETOOTH: {
                this.name = context.getResources().getString(2131888580);
                this.iconId = R.drawable.ic_settings_bluetooth;
                break;
            }
            case WIFI: {
                this.name = context.getResources().getString(2131888618);
                this.iconId = 2131231146;
                break;
            }
            case PHONE: {
                this.name = context.getResources().getString(2131888597);
                this.iconId = 2131231144;
                break;
            }
            case CELL: {
                this.name = context.getResources().getString(2131888582);
                this.iconId = 2131231120;
                break;
            }
            case IDLE: {
                this.name = context.getResources().getString(2131888595);
                this.iconId = 2131231136;
                break;
            }
        }
        if (this.iconId > 0) {
            this.icon = context.getDrawable(this.iconId);
        }
        if ((this.name == null || this.iconId == 0) && this.sipper.uidObj != null) {
            this.getQuickNameIconForUid(this.sipper.uidObj.getUid());
        }
    }
    
    public static void clearUidCache() {
        BatteryEntry.sUidCache.clear();
    }
    
    public static void startRequestQueue() {
        if (BatteryEntry.sHandler != null) {
            synchronized (BatteryEntry.mRequestQueue) {
                if (!BatteryEntry.mRequestQueue.isEmpty()) {
                    if (BatteryEntry.mRequestThread != null) {
                        BatteryEntry.mRequestThread.abort();
                    }
                    (BatteryEntry.mRequestThread = new NameAndIconLoader()).setPriority(1);
                    BatteryEntry.mRequestThread.start();
                    BatteryEntry.mRequestQueue.notify();
                }
            }
        }
    }
    
    public static void stopRequestQueue() {
        synchronized (BatteryEntry.mRequestQueue) {
            if (BatteryEntry.mRequestThread != null) {
                BatteryEntry.mRequestThread.abort();
                BatteryEntry.mRequestThread = null;
                BatteryEntry.sHandler = null;
            }
        }
    }
    
    String[] extractPackagesFromSipper(final BatterySipper batterySipper) {
        String[] mPackages;
        if (batterySipper.getUid() == 1000) {
            mPackages = new String[] { "android" };
        }
        else {
            mPackages = batterySipper.mPackages;
        }
        return mPackages;
    }
    
    public Drawable getIcon() {
        return this.icon;
    }
    
    public String getLabel() {
        return this.name;
    }
    
    void getQuickNameIconForUid(final int n) {
        final Locale default1 = Locale.getDefault();
        if (BatteryEntry.sCurrentLocale != default1) {
            clearUidCache();
            BatteryEntry.sCurrentLocale = default1;
        }
        final String string = Integer.toString(n);
        if (BatteryEntry.sUidCache.containsKey(string)) {
            final UidToDetail uidToDetail = BatteryEntry.sUidCache.get(string);
            this.defaultPackageName = uidToDetail.packageName;
            this.name = uidToDetail.name;
            this.icon = uidToDetail.icon;
            return;
        }
        final PackageManager packageManager = this.context.getPackageManager();
        this.icon = packageManager.getDefaultActivityIcon();
        if (packageManager.getPackagesForUid(n) == null) {
            if (n == 0) {
                this.name = this.context.getResources().getString(R.string.process_kernel_label);
            }
            else if ("mediaserver".equals(this.name)) {
                this.name = this.context.getResources().getString(2131888678);
            }
            else if ("dex2oat".equals(this.name)) {
                this.name = this.context.getResources().getString(2131888675);
            }
            this.iconId = 2131231097;
            this.icon = this.context.getDrawable(this.iconId);
        }
        if (BatteryEntry.sHandler != null) {
            synchronized (BatteryEntry.mRequestQueue) {
                BatteryEntry.mRequestQueue.add(this);
            }
        }
    }
    
    public void loadNameAndIcon() {
        if (this.sipper.uidObj == null) {
            return;
        }
        final PackageManager packageManager = this.context.getPackageManager();
        final int uid = this.sipper.uidObj.getUid();
        if (this.sipper.mPackages == null) {
            this.sipper.mPackages = packageManager.getPackagesForUid(uid);
        }
        final String[] packagesFromSipper = this.extractPackagesFromSipper(this.sipper);
        if (packagesFromSipper != null) {
            final String[] array = new String[packagesFromSipper.length];
            System.arraycopy(packagesFromSipper, 0, array, 0, packagesFromSipper.length);
            final IPackageManager packageManager2 = AppGlobals.getPackageManager();
            final int userId = UserHandle.getUserId(uid);
            for (int i = 0; i < array.length; ++i) {
                try {
                    final ApplicationInfo applicationInfo = packageManager2.getApplicationInfo(array[i], 0, userId);
                    if (applicationInfo == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Retrieving null app info for package ");
                        sb.append(array[i]);
                        sb.append(", user ");
                        sb.append(userId);
                        Log.d("BatteryEntry", sb.toString());
                    }
                    else {
                        final CharSequence loadLabel = applicationInfo.loadLabel(packageManager);
                        if (loadLabel != null) {
                            array[i] = loadLabel.toString();
                        }
                        if (applicationInfo.icon != 0) {
                            this.defaultPackageName = packagesFromSipper[i];
                            this.icon = applicationInfo.loadIcon(packageManager);
                            break;
                        }
                    }
                }
                catch (RemoteException ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Error while retrieving app info for package ");
                    sb2.append(array[i]);
                    sb2.append(", user ");
                    sb2.append(userId);
                    Log.d("BatteryEntry", sb2.toString(), (Throwable)ex);
                }
            }
            if (array.length == 1) {
                this.name = array[0];
            }
            else {
                for (final String defaultPackageName : packagesFromSipper) {
                    try {
                        final PackageInfo packageInfo = packageManager2.getPackageInfo(defaultPackageName, 0, userId);
                        if (packageInfo == null) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Retrieving null package info for package ");
                            sb3.append(defaultPackageName);
                            sb3.append(", user ");
                            sb3.append(userId);
                            Log.d("BatteryEntry", sb3.toString());
                        }
                        else if (packageInfo.sharedUserLabel != 0) {
                            final CharSequence text = packageManager.getText(defaultPackageName, packageInfo.sharedUserLabel, packageInfo.applicationInfo);
                            if (text != null) {
                                this.name = text.toString();
                                if (packageInfo.applicationInfo.icon != 0) {
                                    this.defaultPackageName = defaultPackageName;
                                    this.icon = packageInfo.applicationInfo.loadIcon(packageManager);
                                }
                                break;
                            }
                        }
                    }
                    catch (RemoteException ex2) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Error while retrieving package info for package ");
                        sb4.append(defaultPackageName);
                        sb4.append(", user ");
                        sb4.append(userId);
                        Log.d("BatteryEntry", sb4.toString(), (Throwable)ex2);
                    }
                }
            }
        }
        final String string = Integer.toString(uid);
        if (this.name == null) {
            this.name = string;
        }
        if (this.icon == null) {
            this.icon = packageManager.getDefaultActivityIcon();
        }
        final UidToDetail uidToDetail = new UidToDetail();
        uidToDetail.name = this.name;
        uidToDetail.icon = this.icon;
        uidToDetail.packageName = this.defaultPackageName;
        BatteryEntry.sUidCache.put(string, uidToDetail);
        if (BatteryEntry.sHandler != null) {
            BatteryEntry.sHandler.sendMessage(BatteryEntry.sHandler.obtainMessage(1, (Object)this));
        }
    }
    
    private static class NameAndIconLoader extends Thread
    {
        private boolean mAbort;
        
        public NameAndIconLoader() {
            super("BatteryUsage Icon Loader");
            this.mAbort = false;
        }
        
        public void abort() {
            this.mAbort = true;
        }
        
        @Override
        public void run() {
            while (true) {
                synchronized (BatteryEntry.mRequestQueue) {
                    if (BatteryEntry.mRequestQueue.isEmpty() || this.mAbort) {
                        if (BatteryEntry.sHandler != null) {
                            BatteryEntry.sHandler.sendEmptyMessage(2);
                        }
                        BatteryEntry.mRequestQueue.clear();
                        return;
                    }
                    final BatteryEntry batteryEntry = BatteryEntry.mRequestQueue.remove(0);
                    // monitorexit(BatteryEntry.mRequestQueue)
                    batteryEntry.loadNameAndIcon();
                }
            }
        }
    }
    
    static class UidToDetail
    {
        Drawable icon;
        String name;
        String packageName;
    }
}
