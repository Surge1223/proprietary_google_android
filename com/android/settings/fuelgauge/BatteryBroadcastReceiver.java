package com.android.settings.fuelgauge;

import android.content.IntentFilter;
import com.android.settings.Utils;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class BatteryBroadcastReceiver extends BroadcastReceiver
{
    String mBatteryLevel;
    private OnBatteryChangedListener mBatteryListener;
    String mBatteryStatus;
    private Context mContext;
    
    public BatteryBroadcastReceiver(final Context mContext) {
        this.mContext = mContext;
    }
    
    private void updateBatteryStatus(final Intent intent, final boolean b) {
        if (intent != null && this.mBatteryListener != null) {
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                final String batteryPercentage = Utils.getBatteryPercentage(intent);
                final String batteryStatus = com.android.settingslib.Utils.getBatteryStatus(this.mContext.getResources(), intent);
                if (b) {
                    this.mBatteryListener.onBatteryChanged(0);
                }
                else if (!batteryPercentage.equals(this.mBatteryLevel)) {
                    this.mBatteryListener.onBatteryChanged(1);
                }
                else if (!batteryStatus.equals(this.mBatteryStatus)) {
                    this.mBatteryListener.onBatteryChanged(3);
                }
                this.mBatteryLevel = batteryPercentage;
                this.mBatteryStatus = batteryStatus;
            }
            else if ("android.os.action.POWER_SAVE_MODE_CHANGED".equals(intent.getAction())) {
                this.mBatteryListener.onBatteryChanged(2);
            }
        }
    }
    
    public void onReceive(final Context context, final Intent intent) {
        this.updateBatteryStatus(intent, false);
    }
    
    public void register() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
        this.updateBatteryStatus(this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter), true);
    }
    
    public void setBatteryChangedListener(final OnBatteryChangedListener mBatteryListener) {
        this.mBatteryListener = mBatteryListener;
    }
    
    public void unRegister() {
        this.mContext.unregisterReceiver((BroadcastReceiver)this);
    }
    
    public interface OnBatteryChangedListener
    {
        void onBatteryChanged(final int p0);
    }
}
