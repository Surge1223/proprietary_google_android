package com.android.settings.fuelgauge;

import android.text.format.Formatter;
import android.util.SparseIntArray;
import com.android.settings.graph.UsageView;
import android.content.res.Resources;
import com.android.settingslib.utils.StringUtil;
import android.os.BatteryStats$HistoryItem;
import android.os.Bundle;
import com.android.internal.os.BatteryStatsHelper;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settingslib.utils.PowerUtil;
import android.os.SystemClock;
import com.android.settings.overlay.FeatureFactory;
import android.os.AsyncTask;
import com.android.settingslib.Utils;
import android.content.Intent;
import android.content.Context;
import android.os.BatteryStats;

public class BatteryInfo
{
    public long averageTimeToDischarge;
    public int batteryLevel;
    public String batteryPercentString;
    public CharSequence chargeLabel;
    public boolean discharging;
    private boolean mCharging;
    private BatteryStats mStats;
    public CharSequence remainingLabel;
    public long remainingTimeUs;
    public String statusLabel;
    private long timePeriod;
    
    public BatteryInfo() {
        this.discharging = true;
        this.remainingTimeUs = 0L;
        this.averageTimeToDischarge = -1L;
    }
    
    public static BatteryInfo getBatteryInfo(final Context context, final Intent intent, final BatteryStats mStats, final Estimate estimate, final long n, final boolean b) {
        final long currentTimeMillis = System.currentTimeMillis();
        final BatteryInfo batteryInfo = new BatteryInfo();
        batteryInfo.mStats = mStats;
        batteryInfo.batteryLevel = Utils.getBatteryLevel(intent);
        batteryInfo.batteryPercentString = Utils.formatPercentage(batteryInfo.batteryLevel);
        boolean mCharging = false;
        if (intent.getIntExtra("plugged", 0) != 0) {
            mCharging = true;
        }
        batteryInfo.mCharging = mCharging;
        batteryInfo.averageTimeToDischarge = estimate.averageDischargeTime;
        batteryInfo.statusLabel = Utils.getBatteryStatus(context.getResources(), intent);
        if (!batteryInfo.mCharging) {
            updateBatteryInfoDischarging(context, b, estimate, batteryInfo);
        }
        else {
            updateBatteryInfoCharging(context, intent, mStats, n, batteryInfo);
        }
        BatteryUtils.logRuntime("BatteryInfo", "time for getBatteryInfo", currentTimeMillis);
        return batteryInfo;
    }
    
    public static void getBatteryInfo(final Context context, final Callback callback, final BatteryStats batteryStats, final boolean b) {
        new AsyncTask<Void, Void, BatteryInfo>() {
            protected BatteryInfo doInBackground(final Void... array) {
                final long currentTimeMillis = System.currentTimeMillis();
                final PowerUsageFeatureProvider powerUsageFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
                final long convertMsToUs = PowerUtil.convertMsToUs(SystemClock.elapsedRealtime());
                final Intent registerReceiver = context.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                final boolean b = registerReceiver.getIntExtra("plugged", -1) == 0;
                if (b && powerUsageFeatureProvider != null && powerUsageFeatureProvider.isEnhancedBatteryPredictionEnabled(context)) {
                    final Estimate enhancedBatteryPrediction = powerUsageFeatureProvider.getEnhancedBatteryPrediction(context);
                    if (enhancedBatteryPrediction != null) {
                        BatteryUtils.logRuntime("BatteryInfo", "time for enhanced BatteryInfo", currentTimeMillis);
                        return BatteryInfo.getBatteryInfo(context, registerReceiver, batteryStats, enhancedBatteryPrediction, convertMsToUs, b);
                    }
                }
                long computeBatteryTimeRemaining;
                if (b) {
                    computeBatteryTimeRemaining = batteryStats.computeBatteryTimeRemaining(convertMsToUs);
                }
                else {
                    computeBatteryTimeRemaining = 0L;
                }
                final Estimate estimate = new Estimate(PowerUtil.convertUsToMs(computeBatteryTimeRemaining), false, -1L);
                BatteryUtils.logRuntime("BatteryInfo", "time for regular BatteryInfo", currentTimeMillis);
                return BatteryInfo.getBatteryInfo(context, registerReceiver, batteryStats, estimate, convertMsToUs, b);
            }
            
            protected void onPostExecute(final BatteryInfo batteryInfo) {
                final long currentTimeMillis = System.currentTimeMillis();
                callback.onBatteryInfoLoaded(batteryInfo);
                BatteryUtils.logRuntime("BatteryInfo", "time for callback", currentTimeMillis);
            }
        }.execute((Object[])new Void[0]);
    }
    
    public static void getBatteryInfo(final Context context, final Callback callback, final BatteryStatsHelper batteryStatsHelper, final boolean b) {
        final long currentTimeMillis = System.currentTimeMillis();
        final BatteryStats stats = batteryStatsHelper.getStats();
        BatteryUtils.logRuntime("BatteryInfo", "time for getStats", currentTimeMillis);
        getBatteryInfo(context, callback, stats, b);
    }
    
    public static void getBatteryInfo(final Context context, final Callback callback, final boolean b) {
        final long currentTimeMillis = System.currentTimeMillis();
        final BatteryStatsHelper batteryStatsHelper = new BatteryStatsHelper(context, true);
        batteryStatsHelper.create((Bundle)null);
        BatteryUtils.logRuntime("BatteryInfo", "time to make batteryStatsHelper", currentTimeMillis);
        getBatteryInfo(context, callback, batteryStatsHelper, b);
    }
    
    public static BatteryInfo getBatteryInfoOld(final Context context, final Intent intent, final BatteryStats batteryStats, final long n, final boolean b) {
        return getBatteryInfo(context, intent, batteryStats, new Estimate(PowerUtil.convertUsToMs(batteryStats.computeBatteryTimeRemaining(n)), false, -1L), n, b);
    }
    
    public static void parse(final BatteryStats batteryStats, final BatteryDataParser... array) {
        long time = 0L;
        long time2 = 0L;
        final long n = 0L;
        long n2 = 0L;
        long n3 = 0L;
        int n4 = 0;
        int n5 = 0;
        int n6 = 0;
        int n7 = 1;
        final boolean startIteratingHistoryLocked = batteryStats.startIteratingHistoryLocked();
        long n8 = 0L;
        long n9 = time;
        long n10 = time2;
        long n11 = n2;
        long n12 = n3;
        long n13 = n8;
        if (startIteratingHistoryLocked) {
            final BatteryStats$HistoryItem batteryStats$HistoryItem = new BatteryStats$HistoryItem();
            while (true) {
                n9 = time;
                n10 = time2;
                n11 = n2;
                n12 = n3;
                n4 = n5;
                n13 = n8;
                if (!batteryStats.getNextHistoryLocked(batteryStats$HistoryItem)) {
                    break;
                }
                ++n6;
                int n14;
                if ((n14 = n7) != 0) {
                    n14 = 0;
                    time = batteryStats$HistoryItem.time;
                }
                long n15 = 0L;
                long n16 = 0L;
                Label_0244: {
                    if (batteryStats$HistoryItem.cmd != 5) {
                        n15 = n2;
                        n16 = n8;
                        if (batteryStats$HistoryItem.cmd != 7) {
                            break Label_0244;
                        }
                    }
                    if (batteryStats$HistoryItem.currentTime > n2 + 15552000000L || batteryStats$HistoryItem.time < time + 300000L) {
                        n8 = 0L;
                    }
                    final long currentTime = batteryStats$HistoryItem.currentTime;
                    final long time3 = batteryStats$HistoryItem.time;
                    n15 = currentTime;
                    n3 = time3;
                    n16 = n8;
                    if (n8 == 0L) {
                        n16 = currentTime - (time3 - time);
                        n3 = time3;
                        n15 = currentTime;
                    }
                }
                if (batteryStats$HistoryItem.isDeltaData()) {
                    n5 = n6;
                    time2 = batteryStats$HistoryItem.time;
                }
                n2 = n15;
                n7 = n14;
                n8 = n16;
            }
        }
        batteryStats.finishIteratingHistoryLocked();
        final long n17 = n11 + n10 - n12;
        final boolean b = false;
        final int n18 = n4;
        final int n19 = 0;
        int i = 0;
        final long n20 = n;
        final int n21 = b ? 1 : 0;
        while (i < array.length) {
            array[i].onParsingStarted(n13, n17);
            ++i;
        }
        if (n17 > n13 && batteryStats.startIteratingHistoryLocked()) {
            final BatteryStats$HistoryItem batteryStats$HistoryItem2 = new BatteryStats$HistoryItem();
            long n22 = n20;
            long n23 = n12;
            long n28;
            long n29;
            for (int n24 = n21, n25 = n18; batteryStats.getNextHistoryLocked(batteryStats$HistoryItem2) && n24 < n25; ++n24, n23 = n29, n22 = n28) {
                if (batteryStats$HistoryItem2.isDeltaData()) {
                    final long n26 = n22 + (batteryStats$HistoryItem2.time - n23);
                    final long time4 = batteryStats$HistoryItem2.time;
                    long n27;
                    if ((n27 = n26 - n13) < 0L) {
                        n27 = 0L;
                    }
                    for (int j = 0; j < array.length; ++j) {
                        array[j].onDataPoint(n27, batteryStats$HistoryItem2);
                    }
                    n28 = n26;
                    n29 = time4;
                }
                else {
                    long n30;
                    if (batteryStats$HistoryItem2.cmd != 5 && batteryStats$HistoryItem2.cmd != 7) {
                        n30 = n22;
                    }
                    else {
                        long currentTime2;
                        if (batteryStats$HistoryItem2.currentTime >= n13) {
                            currentTime2 = batteryStats$HistoryItem2.currentTime;
                        }
                        else {
                            currentTime2 = batteryStats$HistoryItem2.time - n9 + n13;
                        }
                        final long time5 = batteryStats$HistoryItem2.time;
                        n30 = currentTime2;
                        n23 = time5;
                    }
                    n29 = n23;
                    n28 = n30;
                    if (batteryStats$HistoryItem2.cmd != 6) {
                        if (batteryStats$HistoryItem2.cmd == 5) {
                            n29 = n23;
                            n28 = n30;
                            if (Math.abs(n22 - n30) <= 3600000L) {
                                continue;
                            }
                        }
                        int n31 = 0;
                        while (true) {
                            n29 = n23;
                            n28 = n30;
                            if (n31 >= array.length) {
                                break;
                            }
                            array[n31].onDataGap();
                            ++n31;
                        }
                    }
                }
            }
        }
        batteryStats.finishIteratingHistoryLocked();
        for (int k = n19; k < array.length; ++k) {
            array[k].onParsingDone();
        }
    }
    
    private static void updateBatteryInfoCharging(final Context context, final Intent intent, final BatteryStats batteryStats, long computeChargeTimeRemaining, final BatteryInfo batteryInfo) {
        final Resources resources = context.getResources();
        computeChargeTimeRemaining = batteryStats.computeChargeTimeRemaining(computeChargeTimeRemaining);
        final int intExtra = intent.getIntExtra("status", 1);
        batteryInfo.discharging = false;
        if (computeChargeTimeRemaining > 0L && intExtra != 5) {
            batteryInfo.remainingTimeUs = computeChargeTimeRemaining;
            final CharSequence formatElapsedTime = StringUtil.formatElapsedTime(context, PowerUtil.convertUsToMs(batteryInfo.remainingTimeUs), false);
            batteryInfo.remainingLabel = context.getString(2131888598, new Object[] { formatElapsedTime });
            batteryInfo.chargeLabel = context.getString(2131888585, new Object[] { batteryInfo.batteryPercentString, formatElapsedTime });
        }
        else {
            final String string = resources.getString(2131886602);
            batteryInfo.remainingLabel = null;
            String chargeLabel;
            if (batteryInfo.batteryLevel == 100) {
                chargeLabel = batteryInfo.batteryPercentString;
            }
            else {
                chargeLabel = resources.getString(2131888584, new Object[] { batteryInfo.batteryPercentString, string });
            }
            batteryInfo.chargeLabel = chargeLabel;
        }
    }
    
    private static void updateBatteryInfoDischarging(final Context context, final boolean b, final Estimate estimate, final BatteryInfo batteryInfo) {
        final long convertMsToUs = PowerUtil.convertMsToUs(estimate.estimateMillis);
        if (convertMsToUs > 0L) {
            batteryInfo.remainingTimeUs = convertMsToUs;
            final long convertUsToMs = PowerUtil.convertUsToMs(convertMsToUs);
            final boolean isBasedOnUsage = estimate.isBasedOnUsage;
            final boolean b2 = false;
            batteryInfo.remainingLabel = PowerUtil.getBatteryRemainingStringFormatted(context, convertUsToMs, null, isBasedOnUsage && !b);
            batteryInfo.chargeLabel = PowerUtil.getBatteryRemainingStringFormatted(context, PowerUtil.convertUsToMs(convertMsToUs), batteryInfo.batteryPercentString, (estimate.isBasedOnUsage && !b) || b2);
        }
        else {
            batteryInfo.remainingLabel = null;
            batteryInfo.chargeLabel = batteryInfo.batteryPercentString;
        }
    }
    
    public void bindHistory(final UsageView usageView, final BatteryDataParser... array) {
        final Context context = usageView.getContext();
        final BatteryDataParser batteryDataParser = new BatteryDataParser() {
            byte lastLevel;
            int lastTime = -1;
            SparseIntArray points = new SparseIntArray();
            long startTime;
            
            @Override
            public void onDataGap() {
                if (this.points.size() > 1) {
                    usageView.addPath(this.points);
                }
                this.points.clear();
            }
            
            @Override
            public void onDataPoint(final long n, final BatteryStats$HistoryItem batteryStats$HistoryItem) {
                this.lastTime = (int)n;
                this.lastLevel = batteryStats$HistoryItem.batteryLevel;
                this.points.put(this.lastTime, (int)this.lastLevel);
            }
            
            @Override
            public void onParsingDone() {
                this.onDataGap();
                if (BatteryInfo.this.remainingTimeUs != 0L) {
                    final PowerUsageFeatureProvider powerUsageFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
                    if (!BatteryInfo.this.mCharging && powerUsageFeatureProvider.isEnhancedBatteryPredictionEnabled(context)) {
                        this.points = powerUsageFeatureProvider.getEnhancedBatteryPredictionCurve(context, this.startTime);
                    }
                    else if (this.lastTime >= 0) {
                        this.points.put(this.lastTime, (int)this.lastLevel);
                        final SparseIntArray points = this.points;
                        final int n = (int)(BatteryInfo.this.timePeriod + PowerUtil.convertUsToMs(BatteryInfo.this.remainingTimeUs));
                        int n2;
                        if (BatteryInfo.this.mCharging) {
                            n2 = 100;
                        }
                        else {
                            n2 = 0;
                        }
                        points.put(n, n2);
                    }
                }
                if (this.points != null && this.points.size() > 0) {
                    usageView.configureGraph(this.points.keyAt(this.points.size() - 1), 100);
                    usageView.addProjectedPath(this.points);
                }
            }
            
            @Override
            public void onParsingStarted(final long startTime, final long n) {
                this.startTime = startTime;
                BatteryInfo.this.timePeriod = n - startTime;
                usageView.clearPaths();
                usageView.configureGraph((int)BatteryInfo.this.timePeriod, 100);
            }
        };
        final BatteryDataParser[] array2 = new BatteryDataParser[array.length + 1];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        array2[array.length] = (BatteryDataParser)batteryDataParser;
        parse(this.mStats, array2);
        final String string = context.getString(2131887005, new Object[] { Formatter.formatShortElapsedTime(context, this.timePeriod) });
        String string2 = "";
        if (this.remainingTimeUs != 0L) {
            string2 = context.getString(2131888800, new Object[] { Formatter.formatShortElapsedTime(context, this.remainingTimeUs / 1000L) });
        }
        usageView.setBottomLabels(new CharSequence[] { string, string2 });
    }
    
    public interface BatteryDataParser
    {
        void onDataGap();
        
        void onDataPoint(final long p0, final BatteryStats$HistoryItem p1);
        
        void onParsingDone();
        
        void onParsingStarted(final long p0, final long p1);
    }
    
    public interface Callback
    {
        void onBatteryInfoLoaded(final BatteryInfo p0);
    }
}
