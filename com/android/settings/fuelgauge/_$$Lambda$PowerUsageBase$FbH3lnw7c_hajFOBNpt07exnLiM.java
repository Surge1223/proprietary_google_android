package com.android.settings.fuelgauge;

import android.content.Loader;
import android.app.LoaderManager$LoaderCallbacks;
import android.os.Bundle;
import android.content.Context;
import android.app.Activity;
import android.os.UserManager;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.dashboard.DashboardFragment;

public final class _$$Lambda$PowerUsageBase$FbH3lnw7c_hajFOBNpt07exnLiM implements OnBatteryChangedListener
{
    @Override
    public final void onBatteryChanged(final int n) {
        this.f$0.restartBatteryStatsLoader(n);
    }
}
