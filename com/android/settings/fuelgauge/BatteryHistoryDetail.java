package com.android.settings.fuelgauge;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.util.TypedValue;
import android.content.Context;
import com.android.internal.os.BatteryStatsHelper;
import android.os.Bundle;
import com.android.settings.graph.UsageView;
import android.view.View;
import android.widget.TextView;
import android.os.BatteryStats;
import android.content.Intent;
import com.android.settings.SettingsPreferenceFragment;

public class BatteryHistoryDetail extends SettingsPreferenceFragment
{
    private Intent mBatteryBroadcast;
    private BatteryFlagParser mCameraParser;
    private BatteryFlagParser mChargingParser;
    private BatteryFlagParser mCpuParser;
    private BatteryFlagParser mFlashlightParser;
    private BatteryFlagParser mGpsParser;
    private BatteryCellParser mPhoneParser;
    private BatteryFlagParser mScreenOn;
    private BatteryStats mStats;
    private BatteryWifiParser mWifiParser;
    
    private void bindData(final BatteryActiveView.BatteryActiveProvider provider, final int text, int visibility) {
        final View viewById = this.getView().findViewById(visibility);
        if (provider.hasData()) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        viewById.setVisibility(visibility);
        ((TextView)viewById.findViewById(16908310)).setText(text);
        ((BatteryActiveView)viewById.findViewById(2131361904)).setProvider(provider);
    }
    
    private void updateEverything() {
        BatteryInfo.getBatteryInfo(this.getContext(), (BatteryInfo.Callback)new _$$Lambda$BatteryHistoryDetail$ZIvw_m8MPrnAuz9tJSzFmSFxa_8(this), this.mStats, false);
    }
    
    @Override
    public int getMetricsCategory() {
        return 51;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mStats = BatteryStatsHelper.statsFromFile((Context)this.getActivity(), this.getArguments().getString("stats"));
        this.mBatteryBroadcast = (Intent)this.getArguments().getParcelable("broadcast");
        final TypedValue typedValue = new TypedValue();
        this.getContext().getTheme().resolveAttribute(16843829, typedValue, true);
        final int color = this.getContext().getColor(typedValue.resourceId);
        this.mChargingParser = new BatteryFlagParser(color, false, 524288);
        this.mScreenOn = new BatteryFlagParser(color, false, 1048576);
        this.mGpsParser = new BatteryFlagParser(color, false, 536870912);
        this.mFlashlightParser = new BatteryFlagParser(color, true, 134217728);
        this.mCameraParser = new BatteryFlagParser(color, true, 2097152);
        this.mWifiParser = new BatteryWifiParser(color);
        this.mCpuParser = new BatteryFlagParser(color, false, Integer.MIN_VALUE);
        this.mPhoneParser = new BatteryCellParser();
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        return layoutInflater.inflate(2131558470, viewGroup, false);
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.updateEverything();
    }
}
