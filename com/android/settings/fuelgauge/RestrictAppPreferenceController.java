package com.android.settings.fuelgauge;

import com.android.settings.fuelgauge.batterytip.BatteryTipUtils;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.os.UserManager;
import com.android.settings.core.InstrumentedPreferenceFragment;
import android.app.AppOpsManager;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.List;
import com.android.settings.core.BasePreferenceController;

public class RestrictAppPreferenceController extends BasePreferenceController
{
    static final String KEY_RESTRICT_APP = "restricted_app";
    List<AppInfo> mAppInfos;
    private AppOpsManager mAppOpsManager;
    private InstrumentedPreferenceFragment mPreferenceFragment;
    private UserManager mUserManager;
    
    public RestrictAppPreferenceController(final Context context) {
        super(context, "restricted_app");
        this.mAppOpsManager = (AppOpsManager)context.getSystemService("appops");
        this.mUserManager = (UserManager)context.getSystemService((Class)UserManager.class);
    }
    
    public RestrictAppPreferenceController(final InstrumentedPreferenceFragment mPreferenceFragment) {
        this(mPreferenceFragment.getContext());
        this.mPreferenceFragment = mPreferenceFragment;
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.getPreferenceKey().equals(preference.getKey())) {
            RestrictedAppDetails.startRestrictedAppDetails(this.mPreferenceFragment, this.mAppInfos);
            return true;
        }
        return super.handlePreferenceTreeClick(preference);
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.mAppInfos = BatteryTipUtils.getRestrictedAppsList(this.mAppOpsManager, this.mUserManager);
        final int size = this.mAppInfos.size();
        preference.setVisible(size > 0);
        preference.setSummary(this.mContext.getResources().getQuantityString(2131755056, size, new Object[] { size }));
    }
}
