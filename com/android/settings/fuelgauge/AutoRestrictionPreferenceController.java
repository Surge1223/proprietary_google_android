package com.android.settings.fuelgauge;

import android.content.ContentResolver;
import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.core.BasePreferenceController;

public class AutoRestrictionPreferenceController extends BasePreferenceController implements OnPreferenceChangeListener
{
    private static final String KEY_SMART_BATTERY = "auto_restriction";
    private static final int OFF = 0;
    private static final int ON = 1;
    private PowerUsageFeatureProvider mPowerUsageFeatureProvider;
    
    public AutoRestrictionPreferenceController(final Context context) {
        super(context, "auto_restriction");
        this.mPowerUsageFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mPowerUsageFeatureProvider.isSmartBatterySupported()) {
            n = 2;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "app_auto_restriction_enabled", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean checked = true;
        if (Settings.Global.getInt(contentResolver, "app_auto_restriction_enabled", 1) != 1) {
            checked = false;
        }
        ((SwitchPreference)preference).setChecked(checked);
    }
}
