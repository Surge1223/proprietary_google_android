package com.android.settings.fuelgauge;

import com.android.internal.util.ArrayUtils;
import com.android.internal.os.BatterySipper;
import android.util.SparseIntArray;
import android.content.pm.PackageManager;
import android.content.Context;

public class PowerUsageFeatureProviderImpl implements PowerUsageFeatureProvider
{
    private static final String[] PACKAGES_SYSTEM;
    protected Context mContext;
    protected PackageManager mPackageManager;
    
    static {
        PACKAGES_SYSTEM = new String[] { "com.android.providers.media", "com.android.providers.calendar", "com.android.systemui" };
    }
    
    public PowerUsageFeatureProviderImpl(final Context context) {
        this.mPackageManager = context.getPackageManager();
        this.mContext = context.getApplicationContext();
    }
    
    @Override
    public String getAdvancedUsageScreenInfoString() {
        return null;
    }
    
    @Override
    public boolean getEarlyWarningSignal(final Context context, final String s) {
        return false;
    }
    
    @Override
    public Estimate getEnhancedBatteryPrediction(final Context context) {
        return null;
    }
    
    @Override
    public SparseIntArray getEnhancedBatteryPredictionCurve(final Context context, final long n) {
        return null;
    }
    
    @Override
    public String getEnhancedEstimateDebugString(final String s) {
        return null;
    }
    
    @Override
    public String getOldEstimateDebugString(final String s) {
        return null;
    }
    
    @Override
    public boolean isEnhancedBatteryPredictionEnabled(final Context context) {
        return false;
    }
    
    @Override
    public boolean isEstimateDebugEnabled() {
        return false;
    }
    
    @Override
    public boolean isSmartBatterySupported() {
        return this.mContext.getResources().getBoolean(17957029);
    }
    
    @Override
    public boolean isTypeService(final BatterySipper batterySipper) {
        return false;
    }
    
    @Override
    public boolean isTypeSystem(final BatterySipper batterySipper) {
        int uid;
        if (batterySipper.uidObj == null) {
            uid = -1;
        }
        else {
            uid = batterySipper.getUid();
        }
        batterySipper.mPackages = this.mPackageManager.getPackagesForUid(uid);
        if (uid >= 0 && uid < 10000) {
            return true;
        }
        if (batterySipper.mPackages != null) {
            final String[] mPackages = batterySipper.mPackages;
            for (int length = mPackages.length, i = 0; i < length; ++i) {
                if (ArrayUtils.contains((Object[])PowerUsageFeatureProviderImpl.PACKAGES_SYSTEM, (Object)mPackages[i])) {
                    return true;
                }
            }
        }
        return false;
    }
}
