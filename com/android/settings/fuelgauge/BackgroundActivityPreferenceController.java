package com.android.settings.fuelgauge;

import com.android.settings.Utils;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import android.app.Fragment;
import com.android.settings.fuelgauge.batterytip.BatteryTipDialogFragment;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.fuelgauge.batterytip.tips.UnrestrictAppTip;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import com.android.settings.core.InstrumentedPreferenceFragment;
import android.app.admin.DevicePolicyManager;
import android.app.AppOpsManager;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BackgroundActivityPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    static final String KEY_BACKGROUND_ACTIVITY = "background_activity";
    private final AppOpsManager mAppOpsManager;
    BatteryUtils mBatteryUtils;
    DevicePolicyManager mDpm;
    private InstrumentedPreferenceFragment mFragment;
    private PowerWhitelistBackend mPowerWhitelistBackend;
    private String mTargetPackage;
    private final int mUid;
    private final UserManager mUserManager;
    
    public BackgroundActivityPreferenceController(final Context context, final InstrumentedPreferenceFragment instrumentedPreferenceFragment, final int n, final String s) {
        this(context, instrumentedPreferenceFragment, n, s, PowerWhitelistBackend.getInstance(context));
    }
    
    BackgroundActivityPreferenceController(final Context context, final InstrumentedPreferenceFragment mFragment, final int mUid, final String mTargetPackage, final PowerWhitelistBackend mPowerWhitelistBackend) {
        super(context);
        this.mPowerWhitelistBackend = mPowerWhitelistBackend;
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mDpm = (DevicePolicyManager)context.getSystemService("device_policy");
        this.mAppOpsManager = (AppOpsManager)context.getSystemService("appops");
        this.mUid = mUid;
        this.mFragment = mFragment;
        this.mTargetPackage = mTargetPackage;
        this.mBatteryUtils = BatteryUtils.getInstance(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "background_activity";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("background_activity".equals(preference.getKey())) {
            final int checkOpNoThrow = this.mAppOpsManager.checkOpNoThrow(70, this.mUid, this.mTargetPackage);
            boolean b = true;
            if (checkOpNoThrow != 1) {
                b = false;
            }
            this.showDialog(b);
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mTargetPackage != null;
    }
    
    void showDialog(final boolean b) {
        final AppInfo build = new AppInfo.Builder().setUid(this.mUid).setPackageName(this.mTargetPackage).build();
        BatteryTip batteryTip;
        if (b) {
            batteryTip = new UnrestrictAppTip(0, build);
        }
        else {
            batteryTip = new RestrictAppTip(0, build);
        }
        final BatteryTipDialogFragment instance = BatteryTipDialogFragment.newInstance(batteryTip, this.mFragment.getMetricsCategory());
        instance.setTargetFragment((Fragment)this.mFragment, 0);
        instance.show(this.mFragment.getFragmentManager(), "BgActivityPrefContr");
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int checkOpNoThrow = this.mAppOpsManager.checkOpNoThrow(70, this.mUid, this.mTargetPackage);
        if (!this.mPowerWhitelistBackend.isWhitelisted(this.mTargetPackage) && checkOpNoThrow != 2 && !Utils.isProfileOrDeviceOwner(this.mUserManager, this.mDpm, this.mTargetPackage)) {
            preference.setEnabled(true);
        }
        else {
            preference.setEnabled(false);
        }
        this.updateSummary(preference);
    }
    
    public void updateSummary(final Preference preference) {
        if (this.mPowerWhitelistBackend.isWhitelisted(this.mTargetPackage)) {
            preference.setSummary(2131886524);
            return;
        }
        final int checkOpNoThrow = this.mAppOpsManager.checkOpNoThrow(70, this.mUid, this.mTargetPackage);
        if (checkOpNoThrow == 2) {
            preference.setSummary(2131886523);
        }
        else {
            boolean b = true;
            if (checkOpNoThrow != 1) {
                b = false;
            }
            int summary;
            if (b) {
                summary = 2131888833;
            }
            else {
                summary = 2131888832;
            }
            preference.setSummary(summary);
        }
    }
}
