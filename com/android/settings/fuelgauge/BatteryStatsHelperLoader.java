package com.android.settings.fuelgauge;

import android.os.Bundle;
import android.content.Context;
import android.os.UserManager;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settingslib.utils.AsyncLoader;

public class BatteryStatsHelperLoader extends AsyncLoader<BatteryStatsHelper>
{
    BatteryUtils mBatteryUtils;
    UserManager mUserManager;
    
    public BatteryStatsHelperLoader(final Context context) {
        super(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mBatteryUtils = BatteryUtils.getInstance(context);
    }
    
    public BatteryStatsHelper loadInBackground() {
        final BatteryStatsHelper batteryStatsHelper = new BatteryStatsHelper(this.getContext(), true);
        this.mBatteryUtils.initBatteryStatsHelper(batteryStatsHelper, null, this.mUserManager);
        return batteryStatsHelper;
    }
    
    @Override
    protected void onDiscardResult(final BatteryStatsHelper batteryStatsHelper) {
    }
}
