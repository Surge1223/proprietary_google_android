package com.android.settings.fuelgauge;

import android.content.Intent;
import com.android.settingslib.Utils;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v14.preference.PreferenceFragment;
import android.widget.TextView;
import com.android.settings.applications.LayoutPreference;
import android.app.Activity;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BatteryHeaderPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnStart
{
    static final String KEY_BATTERY_HEADER = "battery_header";
    private final Activity mActivity;
    private LayoutPreference mBatteryLayoutPref;
    BatteryMeterView mBatteryMeterView;
    TextView mBatteryPercentText;
    private final PreferenceFragment mHost;
    private final Lifecycle mLifecycle;
    TextView mSummary1;
    TextView mSummary2;
    
    public BatteryHeaderPreferenceController(final Context context, final Activity mActivity, final PreferenceFragment mHost, final Lifecycle mLifecycle) {
        super(context);
        this.mActivity = mActivity;
        this.mHost = mHost;
        this.mLifecycle = mLifecycle;
        if (this.mLifecycle != null) {
            this.mLifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mBatteryLayoutPref = (LayoutPreference)preferenceScreen.findPreference("battery_header");
        this.mBatteryMeterView = this.mBatteryLayoutPref.findViewById(2131361906);
        this.mBatteryPercentText = this.mBatteryLayoutPref.findViewById(2131361908);
        this.mSummary1 = this.mBatteryLayoutPref.findViewById(2131362669);
        this.mSummary2 = this.mBatteryLayoutPref.findViewById(2131362670);
        this.quickUpdateHeaderPreference();
    }
    
    @Override
    public String getPreferenceKey() {
        return "battery_header";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onStart() {
        EntityHeaderController.newInstance(this.mActivity, this.mHost, this.mBatteryLayoutPref.findViewById(2131361905)).setRecyclerView(this.mHost.getListView(), this.mLifecycle).styleActionBar(this.mActivity);
    }
    
    public void quickUpdateHeaderPreference() {
        final Intent registerReceiver = this.mContext.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        final int batteryLevel = Utils.getBatteryLevel(registerReceiver);
        final int intExtra = registerReceiver.getIntExtra("plugged", -1);
        boolean charging = false;
        final boolean b = intExtra == 0;
        this.mBatteryMeterView.setBatteryLevel(batteryLevel);
        final BatteryMeterView mBatteryMeterView = this.mBatteryMeterView;
        if (!b) {
            charging = true;
        }
        mBatteryMeterView.setCharging(charging);
        this.mBatteryPercentText.setText((CharSequence)Utils.formatPercentage(batteryLevel));
        this.mSummary1.setText((CharSequence)"");
        this.mSummary2.setText((CharSequence)"");
    }
    
    public void updateHeaderPreference(final BatteryInfo batteryInfo) {
        this.mBatteryPercentText.setText((CharSequence)Utils.formatPercentage(batteryInfo.batteryLevel));
        if (batteryInfo.remainingLabel == null) {
            this.mSummary1.setText((CharSequence)batteryInfo.statusLabel);
        }
        else {
            this.mSummary1.setText(batteryInfo.remainingLabel);
        }
        this.mSummary2.setText((CharSequence)"");
        this.mBatteryMeterView.setBatteryLevel(batteryInfo.batteryLevel);
        this.mBatteryMeterView.setCharging(batteryInfo.discharging ^ true);
    }
}
