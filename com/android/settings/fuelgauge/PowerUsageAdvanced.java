package com.android.settings.fuelgauge;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settings.overlay.FeatureFactory;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.SettingsActivity;
import com.android.settingslib.core.lifecycle.Lifecycle;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;

public class PowerUsageAdvanced extends PowerUsageBase
{
    static final int MENU_TOGGLE_APPS = 2;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private BatteryAppListPreferenceController mBatteryAppListPreferenceController;
    private BatteryUtils mBatteryUtils;
    BatteryHistoryPreference mHistPref;
    private PowerUsageFeatureProvider mPowerUsageFeatureProvider;
    boolean mShowAllApps;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                final ArrayList<BatteryAppListPreferenceController> list = (ArrayList<BatteryAppListPreferenceController>)new ArrayList<AbstractPreferenceController>();
                list.add(new BatteryAppListPreferenceController(context, "app_list", null, null, null));
                return (List<AbstractPreferenceController>)list;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082807;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    public PowerUsageAdvanced() {
        this.mShowAllApps = false;
    }
    
    private void updateHistPrefSummary(final Context context) {
        final boolean b = context.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("plugged", -1) != 0;
        if (this.mPowerUsageFeatureProvider.isEnhancedBatteryPredictionEnabled(context) && !b) {
            this.mHistPref.setBottomSummary(this.mPowerUsageFeatureProvider.getAdvancedUsageScreenInfoString());
        }
        else {
            this.mHistPref.hideBottomSummary();
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<BatteryAppListPreferenceController> list = (ArrayList<BatteryAppListPreferenceController>)new ArrayList<AbstractPreferenceController>();
        list.add(this.mBatteryAppListPreferenceController = new BatteryAppListPreferenceController(context, "app_list", this.getLifecycle(), (SettingsActivity)this.getActivity(), this));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "AdvancedBatteryUsage";
    }
    
    @Override
    public int getMetricsCategory() {
        return 51;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082807;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        this.mHistPref = (BatteryHistoryPreference)this.findPreference("battery_graph");
        this.mPowerUsageFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
        this.mBatteryUtils = BatteryUtils.getInstance(context);
        this.updateHistPrefSummary(context);
        this.restoreSavedInstance(bundle);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        int n;
        if (this.mShowAllApps) {
            n = 2131887843;
        }
        else {
            n = 2131889087;
        }
        menu.add(0, 2, 0, n);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.getActivity().isChangingConfigurations()) {
            BatteryEntry.clearUidCache();
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 2) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.mShowAllApps ^= true;
        int title;
        if (this.mShowAllApps) {
            title = 2131887843;
        }
        else {
            title = 2131889087;
        }
        menuItem.setTitle(title);
        this.mMetricsFeatureProvider.action(this.getContext(), 852, this.mShowAllApps);
        this.restartBatteryStatsLoader(0);
        return true;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("show_all_apps", this.mShowAllApps);
    }
    
    @Override
    protected void refreshUi(final int n) {
        final Context context = this.getContext();
        if (context == null) {
            return;
        }
        this.updatePreference(this.mHistPref);
        this.updateHistPrefSummary(context);
        this.mBatteryAppListPreferenceController.refreshAppListGroup(this.mStatsHelper, this.mShowAllApps);
    }
    
    void restoreSavedInstance(final Bundle bundle) {
        if (bundle != null) {
            this.mShowAllApps = bundle.getBoolean("show_all_apps", false);
        }
    }
}
