package com.android.settings.fuelgauge;

import android.os.BatteryStats$HistoryItem;

public class BatteryWifiParser extends BatteryFlagParser
{
    public BatteryWifiParser(final int n) {
        super(n, false, 0);
    }
    
    @Override
    protected boolean isSet(final BatteryStats$HistoryItem batteryStats$HistoryItem) {
        final int n = (batteryStats$HistoryItem.states2 & 0xF) >> 0;
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        return true;
                    }
                    case 11:
                    case 12: {
                        return false;
                    }
                }
                break;
            }
            case 0:
            case 1:
            case 2:
            case 3: {
                return false;
            }
        }
    }
}
