package com.android.settings.fuelgauge;

import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.graph.UsageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.support.v7.preference.Preference;

public final class _$$Lambda$BatteryHistoryPreference$OfN0YWKsw9YRrCqoEdP8dybAPU0 implements Callback
{
    @Override
    public final void onBatteryInfoLoaded(final BatteryInfo batteryInfo) {
        BatteryHistoryPreference.lambda$setStats$0(this.f$0, batteryInfo);
    }
}
