package com.android.settings.fuelgauge.batterytip;

import android.text.TextUtils;
import java.util.Objects;
import android.os.Parcel;
import android.util.ArraySet;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class AppInfo implements Parcelable, Comparable<AppInfo>
{
    public static final Parcelable.Creator CREATOR;
    public final ArraySet<Integer> anomalyTypes;
    public final String packageName;
    public final long screenOnTimeMs;
    public final int uid;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public AppInfo createFromParcel(final Parcel parcel) {
                return new AppInfo(parcel);
            }
            
            public AppInfo[] newArray(final int n) {
                return new AppInfo[n];
            }
        };
    }
    
    AppInfo(final Parcel parcel) {
        this.packageName = parcel.readString();
        this.anomalyTypes = (ArraySet<Integer>)parcel.readArraySet((ClassLoader)null);
        this.screenOnTimeMs = parcel.readLong();
        this.uid = parcel.readInt();
    }
    
    private AppInfo(final Builder builder) {
        this.packageName = builder.mPackageName;
        this.anomalyTypes = builder.mAnomalyTypes;
        this.screenOnTimeMs = builder.mScreenOnTimeMs;
        this.uid = builder.mUid;
    }
    
    public int compareTo(final AppInfo appInfo) {
        return Long.compare(this.screenOnTimeMs, appInfo.screenOnTimeMs);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AppInfo)) {
            return false;
        }
        final AppInfo appInfo = (AppInfo)o;
        if (!Objects.equals(this.anomalyTypes, appInfo.anomalyTypes) || this.uid != appInfo.uid || this.screenOnTimeMs != appInfo.screenOnTimeMs || !TextUtils.equals((CharSequence)this.packageName, (CharSequence)appInfo.packageName)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("packageName=");
        sb.append(this.packageName);
        sb.append(",anomalyTypes=");
        sb.append(this.anomalyTypes);
        sb.append(",screenTime=");
        sb.append(this.screenOnTimeMs);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.packageName);
        parcel.writeArraySet((ArraySet)this.anomalyTypes);
        parcel.writeLong(this.screenOnTimeMs);
        parcel.writeInt(this.uid);
    }
    
    public static final class Builder
    {
        private ArraySet<Integer> mAnomalyTypes;
        private String mPackageName;
        private long mScreenOnTimeMs;
        private int mUid;
        
        public Builder() {
            this.mAnomalyTypes = (ArraySet<Integer>)new ArraySet();
        }
        
        public Builder addAnomalyType(final int n) {
            this.mAnomalyTypes.add((Object)n);
            return this;
        }
        
        public AppInfo build() {
            return new AppInfo(this, null);
        }
        
        public Builder setPackageName(final String mPackageName) {
            this.mPackageName = mPackageName;
            return this;
        }
        
        public Builder setScreenOnTimeMs(final long mScreenOnTimeMs) {
            this.mScreenOnTimeMs = mScreenOnTimeMs;
            return this;
        }
        
        public Builder setUid(final int mUid) {
            this.mUid = mUid;
            return this;
        }
    }
}
