package com.android.settings.fuelgauge.batterytip.tips;

import java.util.Iterator;
import android.util.Pair;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.res.Resources;
import java.util.Collection;
import android.icu.text.ListFormatter;
import com.android.settings.Utils;
import android.content.Context;
import java.util.ArrayList;
import android.os.Parcel;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.List;
import android.os.Parcelable.Creator;

public class RestrictAppTip extends BatteryTip
{
    public static final Parcelable.Creator CREATOR;
    private List<AppInfo> mRestrictAppList;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new RestrictAppTip(parcel);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new RestrictAppTip[n];
            }
        };
    }
    
    public RestrictAppTip(final int n, final AppInfo appInfo) {
        super(1, n, n == 0);
        (this.mRestrictAppList = new ArrayList<AppInfo>()).add(appInfo);
        this.mNeedUpdate = false;
    }
    
    public RestrictAppTip(final int n, final List<AppInfo> mRestrictAppList) {
        super(1, n, n == 0);
        this.mRestrictAppList = mRestrictAppList;
        this.mNeedUpdate = false;
    }
    
    RestrictAppTip(final Parcel parcel) {
        super(parcel);
        this.mRestrictAppList = (List<AppInfo>)parcel.createTypedArrayList(AppInfo.CREATOR);
    }
    
    @Override
    public int getIconId() {
        int n;
        if (this.mState == 1) {
            n = 2131231091;
        }
        else {
            n = 2131230972;
        }
        return n;
    }
    
    public List<AppInfo> getRestrictAppList() {
        return this.mRestrictAppList;
    }
    
    public CharSequence getRestrictAppsString(final Context context) {
        final ArrayList<CharSequence> list = new ArrayList<CharSequence>();
        for (int i = 0; i < this.mRestrictAppList.size(); ++i) {
            list.add(Utils.getApplicationLabel(context, this.mRestrictAppList.get(i).packageName));
        }
        return ListFormatter.getInstance().format((Collection)list);
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        final int size = this.mRestrictAppList.size();
        CharSequence applicationLabel;
        if (size > 0) {
            applicationLabel = Utils.getApplicationLabel(context, this.mRestrictAppList.get(0).packageName);
        }
        else {
            applicationLabel = "";
        }
        int n;
        if (this.mState == 1) {
            n = 2131755019;
        }
        else {
            n = 2131755021;
        }
        return context.getResources().getQuantityString(n, size, new Object[] { applicationLabel, size });
    }
    
    @Override
    public CharSequence getTitle(final Context context) {
        final int size = this.mRestrictAppList.size();
        CharSequence applicationLabel;
        if (size > 0) {
            applicationLabel = Utils.getApplicationLabel(context, this.mRestrictAppList.get(0).packageName);
        }
        else {
            applicationLabel = "";
        }
        final Resources resources = context.getResources();
        String s;
        if (this.mState == 1) {
            s = resources.getQuantityString(2131755020, size, new Object[] { applicationLabel, size });
        }
        else {
            s = resources.getQuantityString(2131755022, size, new Object[] { size });
        }
        return s;
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
        metricsFeatureProvider.action(context, 1347, this.mState);
        if (this.mState == 0) {
            for (int i = 0; i < this.mRestrictAppList.size(); ++i) {
                final AppInfo appInfo = this.mRestrictAppList.get(i);
                final Iterator iterator = appInfo.anomalyTypes.iterator();
                while (iterator.hasNext()) {
                    metricsFeatureProvider.action(context, 1353, appInfo.packageName, Pair.create((Object)1366, (Object)iterator.next()));
                }
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(" {");
        for (int i = 0; i < this.mRestrictAppList.size(); ++i) {
            final AppInfo appInfo = this.mRestrictAppList.get(i);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" ");
            sb2.append(appInfo.toString());
            sb2.append(" ");
            sb.append(sb2.toString());
        }
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public void updateState(final BatteryTip batteryTip) {
        if (batteryTip.mState == 0) {
            this.mState = 0;
            this.mRestrictAppList = ((RestrictAppTip)batteryTip).mRestrictAppList;
            this.mShowDialog = true;
        }
        else if (this.mState == 0 && batteryTip.mState == 2) {
            this.mState = 1;
            this.mShowDialog = false;
        }
        else {
            this.mState = batteryTip.getState();
            this.mShowDialog = batteryTip.shouldShowDialog();
            this.mRestrictAppList = ((RestrictAppTip)batteryTip).mRestrictAppList;
        }
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeTypedList((List)this.mRestrictAppList);
    }
}
