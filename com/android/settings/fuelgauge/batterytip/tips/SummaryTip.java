package com.android.settings.fuelgauge.batterytip.tips;

import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class SummaryTip extends BatteryTip
{
    public static final Parcelable.Creator CREATOR;
    private long mAverageTimeMs;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new SummaryTip(parcel);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new SummaryTip[n];
            }
        };
    }
    
    public SummaryTip(final int n, final long mAverageTimeMs) {
        super(6, n, true);
        this.mAverageTimeMs = mAverageTimeMs;
    }
    
    SummaryTip(final Parcel parcel) {
        super(parcel);
        this.mAverageTimeMs = parcel.readLong();
    }
    
    @Override
    public int getIconId() {
        return 2131230977;
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        return context.getString(2131886672);
    }
    
    @Override
    public CharSequence getTitle(final Context context) {
        return context.getString(2131886673);
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
        metricsFeatureProvider.action(context, 1349, this.mState);
    }
    
    @Override
    public void updateState(final BatteryTip batteryTip) {
        this.mState = batteryTip.mState;
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeLong(this.mAverageTimeMs);
    }
}
