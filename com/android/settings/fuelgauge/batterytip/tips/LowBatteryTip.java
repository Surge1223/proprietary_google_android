package com.android.settings.fuelgauge.batterytip.tips;

import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class LowBatteryTip extends EarlyWarningTip
{
    public static final Parcelable.Creator CREATOR;
    private CharSequence mSummary;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new LowBatteryTip(parcel);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new LowBatteryTip[n];
            }
        };
    }
    
    public LowBatteryTip(final int n, final boolean b, final CharSequence mSummary) {
        super(n, b);
        this.mType = 5;
        this.mSummary = mSummary;
    }
    
    public LowBatteryTip(final Parcel parcel) {
        super(parcel);
        this.mSummary = parcel.readCharSequence();
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        CharSequence charSequence;
        if (this.mState == 1) {
            charSequence = context.getString(2131886658);
        }
        else {
            charSequence = this.mSummary;
        }
        return charSequence;
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
        metricsFeatureProvider.action(context, 1352, this.mState);
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeCharSequence(this.mSummary);
    }
}
