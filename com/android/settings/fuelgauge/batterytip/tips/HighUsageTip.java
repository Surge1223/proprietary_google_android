package com.android.settings.fuelgauge.batterytip.tips;

import android.util.Pair;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import android.os.Parcel;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.List;
import android.os.Parcelable.Creator;

public class HighUsageTip extends BatteryTip
{
    public static final Parcelable.Creator CREATOR;
    final List<AppInfo> mHighUsageAppList;
    private final long mLastFullChargeTimeMs;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new HighUsageTip(parcel);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new HighUsageTip[n];
            }
        };
    }
    
    public HighUsageTip(final long mLastFullChargeTimeMs, final List<AppInfo> mHighUsageAppList) {
        int n;
        if (mHighUsageAppList.isEmpty()) {
            n = 2;
        }
        else {
            n = 0;
        }
        super(2, n, true);
        this.mLastFullChargeTimeMs = mLastFullChargeTimeMs;
        this.mHighUsageAppList = mHighUsageAppList;
    }
    
    HighUsageTip(final Parcel parcel) {
        super(parcel);
        this.mLastFullChargeTimeMs = parcel.readLong();
        this.mHighUsageAppList = (List<AppInfo>)parcel.createTypedArrayList(AppInfo.CREATOR);
    }
    
    public List<AppInfo> getHighUsageAppList() {
        return this.mHighUsageAppList;
    }
    
    @Override
    public int getIconId() {
        return 2131231092;
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        return context.getString(2131886662);
    }
    
    @Override
    public CharSequence getTitle(final Context context) {
        return context.getString(2131886663);
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
        metricsFeatureProvider.action(context, 1348, this.mState);
        for (int i = 0; i < this.mHighUsageAppList.size(); ++i) {
            metricsFeatureProvider.action(context, 1354, this.mHighUsageAppList.get(i).packageName, (Pair<Integer, Object>[])new Pair[0]);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(" {");
        for (int i = 0; i < this.mHighUsageAppList.size(); ++i) {
            final AppInfo appInfo = this.mHighUsageAppList.get(i);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" ");
            sb2.append(appInfo.toString());
            sb2.append(" ");
            sb.append(sb2.toString());
        }
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public void updateState(final BatteryTip batteryTip) {
        this.mState = batteryTip.mState;
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeLong(this.mLastFullChargeTimeMs);
        parcel.writeTypedList((List)this.mHighUsageAppList);
    }
}
