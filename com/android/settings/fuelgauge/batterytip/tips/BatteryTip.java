package com.android.settings.fuelgauge.batterytip.tips;

import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.support.v7.preference.Preference;
import android.content.Context;
import android.os.Parcel;
import android.util.SparseIntArray;
import android.os.Parcelable;

public abstract class BatteryTip implements Parcelable, Comparable<BatteryTip>
{
    static final SparseIntArray TIP_ORDER;
    protected boolean mNeedUpdate;
    protected boolean mShowDialog;
    protected int mState;
    protected int mType;
    
    static {
        (TIP_ORDER = new SparseIntArray()).append(1, 0);
        BatteryTip.TIP_ORDER.append(3, 1);
        BatteryTip.TIP_ORDER.append(2, 2);
        BatteryTip.TIP_ORDER.append(5, 3);
        BatteryTip.TIP_ORDER.append(6, 4);
        BatteryTip.TIP_ORDER.append(0, 5);
        BatteryTip.TIP_ORDER.append(4, 6);
        BatteryTip.TIP_ORDER.append(7, 7);
    }
    
    BatteryTip(final int mType, final int mState, final boolean mShowDialog) {
        this.mType = mType;
        this.mState = mState;
        this.mShowDialog = mShowDialog;
        this.mNeedUpdate = true;
    }
    
    BatteryTip(final Parcel parcel) {
        this.mType = parcel.readInt();
        this.mState = parcel.readInt();
        this.mShowDialog = parcel.readBoolean();
        this.mNeedUpdate = parcel.readBoolean();
    }
    
    public Preference buildPreference(final Context context) {
        final Preference preference = new Preference(context);
        preference.setKey(this.getKey());
        preference.setTitle(this.getTitle(context));
        preference.setSummary(this.getSummary(context));
        preference.setIcon(this.getIconId());
        return preference;
    }
    
    public int compareTo(final BatteryTip batteryTip) {
        return BatteryTip.TIP_ORDER.get(this.mType) - BatteryTip.TIP_ORDER.get(batteryTip.mType);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public abstract int getIconId();
    
    public String getKey() {
        final StringBuilder sb = new StringBuilder();
        sb.append("key_battery_tip");
        sb.append(this.mType);
        return sb.toString();
    }
    
    public int getState() {
        return this.mState;
    }
    
    public abstract CharSequence getSummary(final Context p0);
    
    public abstract CharSequence getTitle(final Context p0);
    
    public int getType() {
        return this.mType;
    }
    
    public abstract void log(final Context p0, final MetricsFeatureProvider p1);
    
    public boolean needUpdate() {
        return this.mNeedUpdate;
    }
    
    public boolean shouldShowDialog() {
        return this.mShowDialog;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("type=");
        sb.append(this.mType);
        sb.append(" state=");
        sb.append(this.mState);
        return sb.toString();
    }
    
    public abstract void updateState(final BatteryTip p0);
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.mType);
        parcel.writeInt(this.mState);
        parcel.writeBoolean(this.mShowDialog);
        parcel.writeBoolean(this.mNeedUpdate);
    }
}
