package com.android.settings.fuelgauge.batterytip.tips;

import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class SmartBatteryTip extends BatteryTip
{
    public static final Parcelable.Creator CREATOR;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new SmartBatteryTip(parcel, null);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new SmartBatteryTip[n];
            }
        };
    }
    
    public SmartBatteryTip(final int n) {
        super(0, n, false);
    }
    
    private SmartBatteryTip(final Parcel parcel) {
        super(parcel);
    }
    
    @Override
    public int getIconId() {
        return 2131231092;
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        return context.getString(2131886670);
    }
    
    @Override
    public CharSequence getTitle(final Context context) {
        return context.getString(2131886671);
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
        metricsFeatureProvider.action(context, 1350, this.mState);
    }
    
    @Override
    public void updateState(final BatteryTip batteryTip) {
        this.mState = batteryTip.mState;
    }
}
