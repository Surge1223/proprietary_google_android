package com.android.settings.fuelgauge.batterytip.tips;

import android.os.Parcelable;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import android.os.Parcel;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import android.os.Parcelable.Creator;

public class UnrestrictAppTip extends BatteryTip
{
    public static final Parcelable.Creator CREATOR;
    private AppInfo mAppInfo;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new UnrestrictAppTip(parcel);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new UnrestrictAppTip[n];
            }
        };
    }
    
    public UnrestrictAppTip(final int n, final AppInfo mAppInfo) {
        super(7, n, true);
        this.mAppInfo = mAppInfo;
    }
    
    UnrestrictAppTip(final Parcel parcel) {
        super(parcel);
        this.mAppInfo = (AppInfo)parcel.readParcelable(this.getClass().getClassLoader());
    }
    
    @Override
    public int getIconId() {
        return 0;
    }
    
    public String getPackageName() {
        return this.mAppInfo.packageName;
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        return null;
    }
    
    @Override
    public CharSequence getTitle(final Context context) {
        return null;
    }
    
    public AppInfo getUnrestrictAppInfo() {
        return this.mAppInfo;
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
    }
    
    @Override
    public void updateState(final BatteryTip batteryTip) {
        this.mState = batteryTip.mState;
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeParcelable((Parcelable)this.mAppInfo, n);
    }
}
