package com.android.settings.fuelgauge.batterytip.tips;

import android.content.Context;
import android.app.AppOpsManager;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.function.Predicate;

public class AppRestrictionPredicate implements Predicate<AppInfo>
{
    private AppOpsManager mAppOpsManager;
    
    public AppRestrictionPredicate(final Context context) {
        this.mAppOpsManager = (AppOpsManager)context.getSystemService((Class)AppOpsManager.class);
    }
    
    @Override
    public boolean test(final AppInfo appInfo) {
        final int checkOpNoThrow = this.mAppOpsManager.checkOpNoThrow(70, appInfo.uid, appInfo.packageName);
        boolean b = true;
        if (checkOpNoThrow != 1) {
            b = false;
        }
        return b;
    }
}
