package com.android.settings.fuelgauge.batterytip.tips;

import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class EarlyWarningTip extends BatteryTip
{
    public static final Parcelable.Creator CREATOR;
    private boolean mPowerSaveModeOn;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BatteryTip createFromParcel(final Parcel parcel) {
                return new EarlyWarningTip(parcel);
            }
            
            public BatteryTip[] newArray(final int n) {
                return new EarlyWarningTip[n];
            }
        };
    }
    
    public EarlyWarningTip(final int n, final boolean mPowerSaveModeOn) {
        super(3, n, false);
        this.mPowerSaveModeOn = mPowerSaveModeOn;
    }
    
    public EarlyWarningTip(final Parcel parcel) {
        super(parcel);
        this.mPowerSaveModeOn = parcel.readBoolean();
    }
    
    @Override
    public int getIconId() {
        int n;
        if (this.mState == 1) {
            n = 2131230978;
        }
        else {
            n = 2131230976;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary(final Context context) {
        int n;
        if (this.mState == 1) {
            n = 2131886658;
        }
        else {
            n = 2131886660;
        }
        return context.getString(n);
    }
    
    @Override
    public CharSequence getTitle(final Context context) {
        int n;
        if (this.mState == 1) {
            n = 2131886659;
        }
        else {
            n = 2131886661;
        }
        return context.getString(n);
    }
    
    @Override
    public void log(final Context context, final MetricsFeatureProvider metricsFeatureProvider) {
        metricsFeatureProvider.action(context, 1351, this.mState);
    }
    
    @Override
    public void updateState(final BatteryTip batteryTip) {
        final EarlyWarningTip earlyWarningTip = (EarlyWarningTip)batteryTip;
        Label_0064: {
            if (earlyWarningTip.mState == 0) {
                this.mState = 0;
            }
            else {
                if (this.mState == 0) {
                    final int mState = earlyWarningTip.mState;
                    int mState2 = 2;
                    if (mState == 2) {
                        if (earlyWarningTip.mPowerSaveModeOn) {
                            mState2 = 1;
                        }
                        this.mState = mState2;
                        break Label_0064;
                    }
                }
                this.mState = earlyWarningTip.getState();
            }
        }
        this.mPowerSaveModeOn = earlyWarningTip.mPowerSaveModeOn;
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeBoolean(this.mPowerSaveModeOn);
    }
}
