package com.android.settings.fuelgauge.batterytip.tips;

import com.android.settings.Utils;
import android.content.Context;
import android.app.AppOpsManager;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.function.Predicate;

public class AppLabelPredicate implements Predicate<AppInfo>
{
    private AppOpsManager mAppOpsManager;
    private Context mContext;
    
    public AppLabelPredicate(final Context mContext) {
        this.mContext = mContext;
        this.mAppOpsManager = (AppOpsManager)mContext.getSystemService((Class)AppOpsManager.class);
    }
    
    @Override
    public boolean test(final AppInfo appInfo) {
        return Utils.getApplicationLabel(this.mContext, appInfo.packageName) == null;
    }
}
