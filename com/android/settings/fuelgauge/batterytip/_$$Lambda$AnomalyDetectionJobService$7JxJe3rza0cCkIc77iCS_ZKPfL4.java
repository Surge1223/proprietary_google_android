package com.android.settings.fuelgauge.batterytip;

import java.util.ArrayList;
import android.util.Pair;
import android.provider.Settings;
import java.util.Collection;
import com.android.internal.util.ArrayUtils;
import android.os.Bundle;
import com.android.settingslib.utils.ThreadUtils;
import java.util.List;
import android.os.StatsDimensionsValue;
import android.util.Log;
import android.app.job.JobInfo$Builder;
import android.content.ComponentName;
import android.app.job.JobScheduler;
import android.content.Intent;
import android.app.job.JobWorkItem;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.fuelgauge.PowerUsageFeatureProvider;
import android.content.ContentResolver;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import android.os.UserManager;
import com.android.settings.fuelgauge.BatteryUtils;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.app.job.JobService;
import android.app.job.JobParameters;

public final class _$$Lambda$AnomalyDetectionJobService$7JxJe3rza0cCkIc77iCS_ZKPfL4 implements Runnable
{
    @Override
    public final void run() {
        AnomalyDetectionJobService.lambda$onStartJob$0(this.f$0, this.f$1);
    }
}
