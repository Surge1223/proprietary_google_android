package com.android.settings.fuelgauge.batterytip;

import com.android.settingslib.utils.ThreadUtils;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.util.Base64;
import android.text.TextUtils;
import android.provider.Settings;
import android.app.job.JobInfo$Builder;
import android.content.ComponentName;
import android.app.job.JobScheduler;
import android.app.StatsManager$StatsUnavailableException;
import android.util.Log;
import android.content.Context;
import android.app.StatsManager;
import java.util.concurrent.TimeUnit;
import android.app.job.JobService;
import android.app.job.JobParameters;

public final class _$$Lambda$AnomalyConfigJobService$ABo24_XwFDn4e3D3k2rc6z_5bdU implements Runnable
{
    @Override
    public final void run() {
        AnomalyConfigJobService.lambda$onStartJob$0(this.f$0, this.f$1);
    }
}
