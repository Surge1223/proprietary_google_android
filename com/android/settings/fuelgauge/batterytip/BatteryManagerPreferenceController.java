package com.android.settings.fuelgauge.batterytip;

import android.content.ContentResolver;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.UserManager;
import com.android.settings.fuelgauge.PowerUsageFeatureProvider;
import android.app.AppOpsManager;
import com.android.settings.core.BasePreferenceController;

public class BatteryManagerPreferenceController extends BasePreferenceController
{
    private static final String KEY_BATTERY_MANAGER = "smart_battery_manager";
    private static final int ON = 1;
    private AppOpsManager mAppOpsManager;
    private PowerUsageFeatureProvider mPowerUsageFeatureProvider;
    private UserManager mUserManager;
    
    public BatteryManagerPreferenceController(final Context context) {
        super(context, "smart_battery_manager");
        this.mPowerUsageFeatureProvider = FeatureFactory.getFactory(context).getPowerUsageFeatureProvider(context);
        this.mAppOpsManager = (AppOpsManager)context.getSystemService((Class)AppOpsManager.class);
        this.mUserManager = (UserManager)context.getSystemService((Class)UserManager.class);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final int size = BatteryTipUtils.getRestrictedAppsList(this.mAppOpsManager, this.mUserManager).size();
        String s;
        if (this.mPowerUsageFeatureProvider.isSmartBatterySupported()) {
            s = "adaptive_battery_management_enabled";
        }
        else {
            s = "app_auto_restriction_enabled";
        }
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.Global.getInt(contentResolver, s, 1) != 1) {
            b = false;
        }
        this.updateSummary(preference, b, size);
    }
    
    void updateSummary(final Preference preference, final boolean b, final int n) {
        if (n > 0) {
            preference.setSummary(this.mContext.getResources().getQuantityString(2131755017, n, new Object[] { n }));
        }
        else if (b) {
            preference.setSummary(2131886610);
        }
        else {
            preference.setSummary(2131886609);
        }
    }
}
