package com.android.settings.fuelgauge.batterytip;

import java.util.List;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import android.view.View;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.support.v7.widget.RecyclerView;
import com.android.settings.fuelgauge.batterytip.tips.HighUsageTip;
import android.app.AlertDialog$Builder;
import com.android.settings.Utils;
import com.android.settings.fuelgauge.batterytip.tips.UnrestrictAppTip;
import android.app.Dialog;
import com.android.settings.fuelgauge.batterytip.actions.BatteryTipAction;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.SettingsActivity;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.os.Bundle;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class BatteryTipDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
{
    BatteryTip mBatteryTip;
    int mMetricsKey;
    
    public static BatteryTipDialogFragment newInstance(final BatteryTip batteryTip, final int n) {
        final BatteryTipDialogFragment batteryTipDialogFragment = new BatteryTipDialogFragment();
        final Bundle arguments = new Bundle(1);
        arguments.putParcelable("battery_tip", (Parcelable)batteryTip);
        arguments.putInt("metrics_key", n);
        batteryTipDialogFragment.setArguments(arguments);
        return batteryTipDialogFragment;
    }
    
    public int getMetricsCategory() {
        return 1323;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final BatteryTipPreferenceController.BatteryTipListener batteryTipListener = (BatteryTipPreferenceController.BatteryTipListener)this.getTargetFragment();
        if (batteryTipListener == null) {
            return;
        }
        final BatteryTipAction actionForBatteryTip = BatteryTipUtils.getActionForBatteryTip(this.mBatteryTip, (SettingsActivity)this.getActivity(), (InstrumentedPreferenceFragment)this.getTargetFragment());
        if (actionForBatteryTip != null) {
            actionForBatteryTip.handlePositiveAction(this.mMetricsKey);
        }
        batteryTipListener.onBatteryTipHandled(this.mBatteryTip);
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Bundle arguments = this.getArguments();
        final Context context = this.getContext();
        this.mBatteryTip = (BatteryTip)arguments.getParcelable("battery_tip");
        this.mMetricsKey = arguments.getInt("metrics_key");
        switch (this.mBatteryTip.getType()) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown type ");
                sb.append(this.mBatteryTip.getType());
                throw new IllegalArgumentException(sb.toString());
            }
            case 7: {
                Utils.getApplicationLabel(context, ((UnrestrictAppTip)this.mBatteryTip).getPackageName());
                return (Dialog)new AlertDialog$Builder(context).setTitle((CharSequence)this.getString(2131886677)).setMessage(2131886675).setPositiveButton(2131886676, (DialogInterface$OnClickListener)this).setNegativeButton(2131886674, (DialogInterface$OnClickListener)null).create();
            }
            case 6: {
                return (Dialog)new AlertDialog$Builder(context).setMessage(2131886657).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
            }
            case 2: {
                final HighUsageTip highUsageTip = (HighUsageTip)this.mBatteryTip;
                final RecyclerView view = (RecyclerView)LayoutInflater.from(context).inflate(2131558703, (ViewGroup)null);
                view.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(context));
                view.setAdapter((RecyclerView.Adapter)new HighUsageAdapter(context, highUsageTip.getHighUsageAppList()));
                return (Dialog)new AlertDialog$Builder(context).setMessage((CharSequence)this.getString(2131886656, new Object[] { highUsageTip.getHighUsageAppList().size() })).setView((View)view).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create();
            }
            case 1: {
                final RestrictAppTip restrictAppTip = (RestrictAppTip)this.mBatteryTip;
                final List<AppInfo> restrictAppList = restrictAppTip.getRestrictAppList();
                final int size = restrictAppList.size();
                final CharSequence applicationLabel = Utils.getApplicationLabel(context, restrictAppList.get(0).packageName);
                final AlertDialog$Builder setNegativeButton = new AlertDialog$Builder(context).setTitle((CharSequence)context.getResources().getQuantityString(2131755018, size, new Object[] { size })).setPositiveButton(2131886667, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)null);
                if (size == 1) {
                    setNegativeButton.setMessage((CharSequence)this.getString(2131886666, new Object[] { applicationLabel }));
                }
                else if (size <= 5) {
                    setNegativeButton.setMessage((CharSequence)this.getString(2131886668));
                    final RecyclerView view2 = (RecyclerView)LayoutInflater.from(context).inflate(2131558703, (ViewGroup)null);
                    view2.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(context));
                    view2.setAdapter((RecyclerView.Adapter)new HighUsageAdapter(context, restrictAppList));
                    setNegativeButton.setView((View)view2);
                }
                else {
                    setNegativeButton.setMessage((CharSequence)context.getString(2131886669, new Object[] { restrictAppTip.getRestrictAppsString(context) }));
                }
                return (Dialog)setNegativeButton.create();
            }
        }
    }
}
