package com.android.settings.fuelgauge.batterytip;

import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.android.settingslib.utils.StringUtil;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import java.util.List;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

public class HighUsageAdapter extends Adapter<ViewHolder>
{
    private final Context mContext;
    private final List<AppInfo> mHighUsageAppList;
    private final IconDrawableFactory mIconDrawableFactory;
    private final PackageManager mPackageManager;
    
    public HighUsageAdapter(final Context mContext, final List<AppInfo> mHighUsageAppList) {
        this.mContext = mContext;
        this.mHighUsageAppList = mHighUsageAppList;
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(mContext);
        this.mPackageManager = mContext.getPackageManager();
    }
    
    @Override
    public int getItemCount() {
        return this.mHighUsageAppList.size();
    }
    
    public void onBindViewHolder(final ViewHolder viewHolder, final int n) {
        final AppInfo appInfo = this.mHighUsageAppList.get(n);
        viewHolder.appIcon.setImageDrawable(Utils.getBadgedIcon(this.mIconDrawableFactory, this.mPackageManager, appInfo.packageName, UserHandle.getUserId(appInfo.uid)));
        viewHolder.appName.setText(Utils.getApplicationLabel(this.mContext, appInfo.packageName));
        if (appInfo.screenOnTimeMs != 0L) {
            viewHolder.appTime.setText(StringUtil.formatElapsedTime(this.mContext, appInfo.screenOnTimeMs, false));
        }
    }
    
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        return new ViewHolder(LayoutInflater.from(this.mContext).inflate(2131558448, viewGroup, false));
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView appIcon;
        public TextView appName;
        public TextView appTime;
        public View view;
        
        public ViewHolder(final View view) {
            super(view);
            this.view = view;
            this.appIcon = (ImageView)view.findViewById(2131361874);
            this.appName = (TextView)view.findViewById(2131361875);
            this.appTime = (TextView)view.findViewById(2131361878);
        }
    }
}
