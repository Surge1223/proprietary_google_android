package com.android.settings.fuelgauge.batterytip;

import android.app.StatsManager$StatsUnavailableException;
import android.util.Log;
import android.app.StatsManager;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class AnomalyConfigReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        if ("android.app.action.STATSD_STARTED".equals(intent.getAction()) || "android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            final StatsManager statsManager = (StatsManager)context.getSystemService((Class)StatsManager.class);
            AnomalyConfigJobService.scheduleConfigUpdate(context);
            try {
                BatteryTipUtils.uploadAnomalyPendingIntent(context, statsManager);
            }
            catch (StatsManager$StatsUnavailableException ex) {
                Log.w("AnomalyConfigReceiver", "Failed to uploadAnomalyPendingIntent.", (Throwable)ex);
            }
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                AnomalyCleanupJobService.scheduleCleanUp(context);
            }
        }
    }
}
