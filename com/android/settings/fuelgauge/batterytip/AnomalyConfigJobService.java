package com.android.settings.fuelgauge.batterytip;

import com.android.settingslib.utils.ThreadUtils;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.util.Base64;
import android.text.TextUtils;
import android.provider.Settings;
import android.app.job.JobInfo$Builder;
import android.content.ComponentName;
import android.app.job.JobScheduler;
import android.app.StatsManager$StatsUnavailableException;
import android.util.Log;
import android.content.Context;
import android.app.StatsManager;
import android.app.job.JobParameters;
import java.util.concurrent.TimeUnit;
import android.app.job.JobService;

public class AnomalyConfigJobService extends JobService
{
    static final long CONFIG_UPDATE_FREQUENCY_MS;
    
    static {
        CONFIG_UPDATE_FREQUENCY_MS = TimeUnit.DAYS.toMillis(1L);
    }
    
    public static void scheduleConfigUpdate(final Context context) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService((Class)JobScheduler.class);
        final JobInfo$Builder setPersisted = new JobInfo$Builder(2131427339, new ComponentName(context, (Class)AnomalyConfigJobService.class)).setPeriodic(AnomalyConfigJobService.CONFIG_UPDATE_FREQUENCY_MS).setRequiresDeviceIdle(true).setRequiresCharging(true).setPersisted(true);
        if (jobScheduler.getPendingJob(2131427339) == null && jobScheduler.schedule(setPersisted.build()) != 1) {
            Log.i("AnomalyConfigJobService", "Anomaly config update job service schedule failed.");
        }
    }
    
    void checkAnomalyConfig(final StatsManager statsManager) {
        synchronized (this) {
            final SharedPreferences sharedPreferences = this.getSharedPreferences("anomaly_pref", 0);
            final int int1 = sharedPreferences.getInt("anomaly_config_version", 0);
            final int int2 = Settings.Global.getInt(this.getContentResolver(), "anomaly_config_version", 0);
            final String string = Settings.Global.getString(this.getContentResolver(), "anomaly_config");
            final StringBuilder sb = new StringBuilder();
            sb.append("CurrentVersion: ");
            sb.append(int1);
            sb.append(" new version: ");
            sb.append(int2);
            Log.i("AnomalyConfigJobService", sb.toString());
            if (int2 > int1) {
                try {
                    statsManager.removeConfig(1L);
                }
                catch (StatsManager$StatsUnavailableException ex) {
                    Log.i("AnomalyConfigJobService", "When updating anomaly config, failed to first remove the old config 1", (Throwable)ex);
                }
                if (!TextUtils.isEmpty((CharSequence)string)) {
                    try {
                        statsManager.addConfig(1L, Base64.decode(string, 0));
                        Log.i("AnomalyConfigJobService", "Upload the anomaly config. configKey: 1");
                        final SharedPreferences$Editor edit = sharedPreferences.edit();
                        edit.putInt("anomaly_config_version", int2);
                        edit.commit();
                    }
                    catch (StatsManager$StatsUnavailableException ex2) {
                        Log.i("AnomalyConfigJobService", "Upload of anomaly config failed for configKey 1", (Throwable)ex2);
                    }
                    catch (IllegalArgumentException ex3) {
                        Log.e("AnomalyConfigJobService", "Anomaly raw config is in wrong format", (Throwable)ex3);
                    }
                }
            }
        }
    }
    
    public boolean onStartJob(final JobParameters jobParameters) {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$AnomalyConfigJobService$ABo24_XwFDn4e3D3k2rc6z_5bdU(this, jobParameters));
        return true;
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        return false;
    }
}
