package com.android.settings.fuelgauge.batterytip;

import com.android.settings.fuelgauge.BatteryInfo;
import java.util.Collections;
import com.android.settings.fuelgauge.batterytip.detectors.RestrictAppDetector;
import com.android.settings.fuelgauge.batterytip.detectors.SummaryDetector;
import com.android.settings.fuelgauge.batterytip.detectors.EarlyWarningDetector;
import com.android.settings.fuelgauge.batterytip.detectors.SmartBatteryDetector;
import com.android.settings.fuelgauge.batterytip.detectors.HighUsageDetector;
import com.android.settings.fuelgauge.batterytip.detectors.LowBatteryDetector;
import java.util.ArrayList;
import android.content.Context;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import java.util.List;
import com.android.settingslib.utils.AsyncLoader;

public class BatteryTipLoader extends AsyncLoader<List<BatteryTip>>
{
    private BatteryStatsHelper mBatteryStatsHelper;
    BatteryUtils mBatteryUtils;
    
    public BatteryTipLoader(final Context context, final BatteryStatsHelper mBatteryStatsHelper) {
        super(context);
        this.mBatteryStatsHelper = mBatteryStatsHelper;
        this.mBatteryUtils = BatteryUtils.getInstance(context);
    }
    
    public List<BatteryTip> loadInBackground() {
        final ArrayList<BatteryTip> list = (ArrayList<BatteryTip>)new ArrayList<Comparable>();
        final BatteryTipPolicy batteryTipPolicy = new BatteryTipPolicy(this.getContext());
        final BatteryInfo batteryInfo = this.mBatteryUtils.getBatteryInfo(this.mBatteryStatsHelper, "BatteryTipLoader");
        final Context context = this.getContext();
        list.add(new LowBatteryDetector(context, batteryTipPolicy, batteryInfo).detect());
        list.add(new HighUsageDetector(context, batteryTipPolicy, this.mBatteryStatsHelper, batteryInfo.discharging).detect());
        list.add(new SmartBatteryDetector(batteryTipPolicy, context.getContentResolver()).detect());
        list.add(new EarlyWarningDetector(batteryTipPolicy, context).detect());
        list.add(new SummaryDetector(batteryTipPolicy, batteryInfo.averageTimeToDischarge).detect());
        list.add(new RestrictAppDetector(context, batteryTipPolicy).detect());
        Collections.sort((List<Comparable>)list);
        return list;
    }
    
    @Override
    protected void onDiscardResult(final List<BatteryTip> list) {
    }
}
