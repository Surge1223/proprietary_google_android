package com.android.settings.fuelgauge.batterytip;

import android.os.Bundle;
import com.android.settings.fuelgauge.batterytip.actions.BatteryTipAction;
import android.app.Fragment;
import android.support.v7.preference.Preference;
import com.android.settings.fuelgauge.batterytip.tips.SummaryTip;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.overlay.FeatureFactory;
import java.util.HashMap;
import com.android.settings.SettingsActivity;
import android.support.v7.preference.PreferenceGroup;
import android.content.Context;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.core.InstrumentedPreferenceFragment;
import java.util.List;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import java.util.Map;
import com.android.settings.core.BasePreferenceController;

public class BatteryTipPreferenceController extends BasePreferenceController
{
    private static final String KEY_BATTERY_TIPS = "key_battery_tips";
    private static final int REQUEST_ANOMALY_ACTION = 0;
    private static final String TAG = "BatteryTipPreferenceController";
    private BatteryTipListener mBatteryTipListener;
    private Map<String, BatteryTip> mBatteryTipMap;
    private List<BatteryTip> mBatteryTips;
    InstrumentedPreferenceFragment mFragment;
    private MetricsFeatureProvider mMetricsFeatureProvider;
    private boolean mNeedUpdate;
    Context mPrefContext;
    PreferenceGroup mPreferenceGroup;
    private SettingsActivity mSettingsActivity;
    
    public BatteryTipPreferenceController(final Context context, final String s) {
        this(context, s, null, null, null);
    }
    
    public BatteryTipPreferenceController(final Context context, final String s, final SettingsActivity mSettingsActivity, final InstrumentedPreferenceFragment mFragment, final BatteryTipListener mBatteryTipListener) {
        super(context, s);
        this.mBatteryTipListener = mBatteryTipListener;
        this.mBatteryTipMap = new HashMap<String, BatteryTip>();
        this.mFragment = mFragment;
        this.mSettingsActivity = mSettingsActivity;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(context).getMetricsFeatureProvider();
        this.mNeedUpdate = true;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPrefContext = preferenceScreen.getContext();
        (this.mPreferenceGroup = (PreferenceGroup)preferenceScreen.findPreference(this.getPreferenceKey())).addPreference(new SummaryTip(0, -1L).buildPreference(this.mPrefContext));
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        final BatteryTip batteryTip = this.mBatteryTipMap.get(preference.getKey());
        if (batteryTip != null) {
            if (batteryTip.shouldShowDialog()) {
                final BatteryTipDialogFragment instance = BatteryTipDialogFragment.newInstance(batteryTip, this.mFragment.getMetricsCategory());
                instance.setTargetFragment((Fragment)this.mFragment, 0);
                instance.show(this.mFragment.getFragmentManager(), "BatteryTipPreferenceController");
            }
            else {
                final BatteryTipAction actionForBatteryTip = BatteryTipUtils.getActionForBatteryTip(batteryTip, this.mSettingsActivity, this.mFragment);
                if (actionForBatteryTip != null) {
                    actionForBatteryTip.handlePositiveAction(this.mFragment.getMetricsCategory());
                }
                if (this.mBatteryTipListener != null) {
                    this.mBatteryTipListener.onBatteryTipHandled(batteryTip);
                }
            }
            return true;
        }
        return super.handlePreferenceTreeClick(preference);
    }
    
    public boolean needUpdate() {
        return this.mNeedUpdate;
    }
    
    public void restoreInstanceState(final Bundle bundle) {
        if (bundle != null) {
            this.updateBatteryTips(bundle.getParcelableArrayList("key_battery_tips"));
        }
    }
    
    public void saveInstanceState(final Bundle bundle) {
        bundle.putParcelableList("key_battery_tips", (List)this.mBatteryTips);
    }
    
    public void updateBatteryTips(final List<BatteryTip> mBatteryTips) {
        if (mBatteryTips == null) {
            return;
        }
        if (this.mBatteryTips == null) {
            this.mBatteryTips = mBatteryTips;
        }
        else {
            for (int i = 0; i < mBatteryTips.size(); ++i) {
                this.mBatteryTips.get(i).updateState(mBatteryTips.get(i));
            }
        }
        this.mPreferenceGroup.removeAll();
        for (int j = 0; j < mBatteryTips.size(); ++j) {
            final BatteryTip batteryTip = this.mBatteryTips.get(j);
            if (batteryTip.getState() != 2) {
                final Preference buildPreference = batteryTip.buildPreference(this.mPrefContext);
                this.mBatteryTipMap.put(buildPreference.getKey(), batteryTip);
                this.mPreferenceGroup.addPreference(buildPreference);
                batteryTip.log(this.mContext, this.mMetricsFeatureProvider);
                this.mNeedUpdate = batteryTip.needUpdate();
                break;
            }
        }
    }
    
    public interface BatteryTipListener
    {
        void onBatteryTipHandled(final BatteryTip p0);
    }
}
