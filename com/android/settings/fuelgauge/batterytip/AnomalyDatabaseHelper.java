package com.android.settings.fuelgauge.batterytip;

import android.util.Log;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public class AnomalyDatabaseHelper extends SQLiteOpenHelper
{
    private static AnomalyDatabaseHelper sSingleton;
    
    private AnomalyDatabaseHelper(final Context context) {
        super(context, "battery_settings.db", (SQLiteDatabase$CursorFactory)null, 4);
    }
    
    private void bootstrapDB(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE anomaly(uid INTEGER NOT NULL, package_name TEXT, anomaly_type INTEGER NOT NULL, anomaly_state INTEGER NOT NULL, time_stamp_ms INTEGER NOT NULL,  PRIMARY KEY (uid,anomaly_type,anomaly_state,time_stamp_ms))");
        Log.i("BatteryDatabaseHelper", "Bootstrapped database");
    }
    
    private void dropTables(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS anomaly");
    }
    
    public static AnomalyDatabaseHelper getInstance(final Context context) {
        synchronized (AnomalyDatabaseHelper.class) {
            if (AnomalyDatabaseHelper.sSingleton == null) {
                AnomalyDatabaseHelper.sSingleton = new AnomalyDatabaseHelper(context.getApplicationContext());
            }
            return AnomalyDatabaseHelper.sSingleton;
        }
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        this.bootstrapDB(sqLiteDatabase);
    }
    
    public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Detected schema version '");
        sb.append(n);
        sb.append("'. Index needs to be rebuilt for schema version '");
        sb.append(n2);
        sb.append("'.");
        Log.w("BatteryDatabaseHelper", sb.toString());
        this.reconstruct(sqLiteDatabase);
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        if (n < 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Detected schema version '");
            sb.append(n);
            sb.append("'. Index needs to be rebuilt for schema version '");
            sb.append(n2);
            sb.append("'.");
            Log.w("BatteryDatabaseHelper", sb.toString());
            this.reconstruct(sqLiteDatabase);
        }
    }
    
    public void reconstruct(final SQLiteDatabase sqLiteDatabase) {
        this.dropTables(sqLiteDatabase);
        this.bootstrapDB(sqLiteDatabase);
    }
}
