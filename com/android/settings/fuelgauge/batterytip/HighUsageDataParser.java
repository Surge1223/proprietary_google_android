package com.android.settings.fuelgauge.batterytip;

import android.os.BatteryStats$HistoryItem;
import com.android.settings.fuelgauge.BatteryInfo;

public class HighUsageDataParser implements BatteryDataParser
{
    private int mBatteryDrain;
    private byte mEndBatteryLevel;
    private long mEndTimeMs;
    private byte mLastPeriodBatteryLevel;
    private int mThreshold;
    private final long mTimePeriodMs;
    
    public HighUsageDataParser(final long mTimePeriodMs, final int mThreshold) {
        this.mTimePeriodMs = mTimePeriodMs;
        this.mThreshold = mThreshold;
    }
    
    public boolean isDeviceHeavilyUsed() {
        return this.mBatteryDrain > this.mThreshold;
    }
    
    @Override
    public void onDataGap() {
    }
    
    @Override
    public void onDataPoint(final long n, final BatteryStats$HistoryItem batteryStats$HistoryItem) {
        if (n == 0L || batteryStats$HistoryItem.currentTime <= this.mEndTimeMs - this.mTimePeriodMs) {
            this.mLastPeriodBatteryLevel = batteryStats$HistoryItem.batteryLevel;
        }
        this.mEndBatteryLevel = batteryStats$HistoryItem.batteryLevel;
    }
    
    @Override
    public void onParsingDone() {
        this.mBatteryDrain = this.mLastPeriodBatteryLevel - this.mEndBatteryLevel;
    }
    
    @Override
    public void onParsingStarted(final long n, final long mEndTimeMs) {
        this.mEndTimeMs = mEndTimeMs;
    }
}
