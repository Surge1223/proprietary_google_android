package com.android.settings.fuelgauge.batterytip.detectors;

import com.android.settings.fuelgauge.batterytip.tips.SmartBatteryTip;
import android.provider.Settings;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.fuelgauge.batterytip.BatteryTipPolicy;
import android.content.ContentResolver;

public class SmartBatteryDetector
{
    private ContentResolver mContentResolver;
    private BatteryTipPolicy mPolicy;
    
    public SmartBatteryDetector(final BatteryTipPolicy mPolicy, final ContentResolver mContentResolver) {
        this.mPolicy = mPolicy;
        this.mContentResolver = mContentResolver;
    }
    
    public BatteryTip detect() {
        final ContentResolver mContentResolver = this.mContentResolver;
        final boolean b = true;
        final int int1 = Settings.Global.getInt(mContentResolver, "adaptive_battery_management_enabled", 1);
        final boolean b2 = false;
        boolean b3 = b;
        if (int1 != 0) {
            b3 = (this.mPolicy.testSmartBatteryTip && b);
        }
        int n;
        if (b3) {
            n = (b2 ? 1 : 0);
        }
        else {
            n = 2;
        }
        return new SmartBatteryTip(n);
    }
}
