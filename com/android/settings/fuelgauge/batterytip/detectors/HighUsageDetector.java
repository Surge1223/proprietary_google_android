package com.android.settings.fuelgauge.batterytip.detectors;

import com.android.settings.fuelgauge.BatteryInfo;
import com.android.settings.fuelgauge.batterytip.tips.HighUsageTip;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import com.android.internal.os.BatterySipper;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import java.util.ArrayList;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.BatteryTipPolicy;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.List;
import com.android.settings.fuelgauge.batterytip.HighUsageDataParser;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.internal.os.BatteryStatsHelper;

public class HighUsageDetector
{
    private BatteryStatsHelper mBatteryStatsHelper;
    BatteryUtils mBatteryUtils;
    HighUsageDataParser mDataParser;
    boolean mDischarging;
    private List<AppInfo> mHighUsageAppList;
    private BatteryTipPolicy mPolicy;
    
    public HighUsageDetector(final Context context, final BatteryTipPolicy mPolicy, final BatteryStatsHelper mBatteryStatsHelper, final boolean mDischarging) {
        this.mPolicy = mPolicy;
        this.mBatteryStatsHelper = mBatteryStatsHelper;
        this.mHighUsageAppList = new ArrayList<AppInfo>();
        this.mBatteryUtils = BatteryUtils.getInstance(context);
        this.mDataParser = new HighUsageDataParser(this.mPolicy.highUsagePeriodMs, this.mPolicy.highUsageBatteryDraining);
        this.mDischarging = mDischarging;
    }
    
    public BatteryTip detect() {
        final long calculateLastFullChargeTime = this.mBatteryUtils.calculateLastFullChargeTime(this.mBatteryStatsHelper, System.currentTimeMillis());
        if (this.mPolicy.highUsageEnabled && this.mDischarging) {
            this.parseBatteryData();
            if (this.mDataParser.isDeviceHeavilyUsed() || this.mPolicy.testHighUsageTip) {
                final List usageList = this.mBatteryStatsHelper.getUsageList();
                for (int i = 0; i < usageList.size(); ++i) {
                    final BatterySipper batterySipper = usageList.get(i);
                    if (!this.mBatteryUtils.shouldHideSipper(batterySipper)) {
                        final long processTimeMs = this.mBatteryUtils.getProcessTimeMs(1, batterySipper.uidObj, 0);
                        if (processTimeMs >= 60000L) {
                            this.mHighUsageAppList.add(new AppInfo.Builder().setUid(batterySipper.getUid()).setPackageName(this.mBatteryUtils.getPackageName(batterySipper.getUid())).setScreenOnTimeMs(processTimeMs).build());
                        }
                    }
                }
                if (this.mPolicy.testHighUsageTip && this.mHighUsageAppList.isEmpty()) {
                    this.mHighUsageAppList.add(new AppInfo.Builder().setPackageName("com.android.settings").setScreenOnTimeMs(TimeUnit.HOURS.toMillis(3L)).build());
                }
                Collections.sort(this.mHighUsageAppList, Collections.reverseOrder());
                this.mHighUsageAppList = this.mHighUsageAppList.subList(0, Math.min(this.mPolicy.highUsageAppCount, this.mHighUsageAppList.size()));
            }
        }
        return new HighUsageTip(calculateLastFullChargeTime, this.mHighUsageAppList);
    }
    
    void parseBatteryData() {
        BatteryInfo.parse(this.mBatteryStatsHelper.getStats(), this.mDataParser);
    }
}
