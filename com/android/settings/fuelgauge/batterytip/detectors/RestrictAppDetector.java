package com.android.settings.fuelgauge.batterytip.detectors;

import java.util.function.Predicate;
import java.util.List;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.ArrayList;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.fuelgauge.batterytip.BatteryTipPolicy;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.BatteryDatabaseManager;
import com.android.settings.fuelgauge.batterytip.tips.AppRestrictionPredicate;
import com.android.settings.fuelgauge.batterytip.tips.AppLabelPredicate;

public class RestrictAppDetector
{
    static final boolean USE_FAKE_DATA = false;
    private AppLabelPredicate mAppLabelPredicate;
    private AppRestrictionPredicate mAppRestrictionPredicate;
    BatteryDatabaseManager mBatteryDatabaseManager;
    private Context mContext;
    private BatteryTipPolicy mPolicy;
    
    public RestrictAppDetector(final Context mContext, final BatteryTipPolicy mPolicy) {
        this.mContext = mContext;
        this.mPolicy = mPolicy;
        this.mBatteryDatabaseManager = BatteryDatabaseManager.getInstance(mContext);
        this.mAppRestrictionPredicate = new AppRestrictionPredicate(mContext);
        this.mAppLabelPredicate = new AppLabelPredicate(mContext);
    }
    
    public BatteryTip detect() {
        final boolean appRestrictionEnabled = this.mPolicy.appRestrictionEnabled;
        int n = 2;
        if (!appRestrictionEnabled) {
            return new RestrictAppTip(2, new ArrayList<AppInfo>());
        }
        final long n2 = System.currentTimeMillis() - 86400000L;
        final List<AppInfo> queryAllAnomalies = this.mBatteryDatabaseManager.queryAllAnomalies(n2, 0);
        queryAllAnomalies.removeIf(((Predicate<? super Object>)this.mAppLabelPredicate).or(this.mAppRestrictionPredicate));
        if (!queryAllAnomalies.isEmpty()) {
            return new RestrictAppTip(0, queryAllAnomalies);
        }
        final List<AppInfo> queryAllAnomalies2 = this.mBatteryDatabaseManager.queryAllAnomalies(n2, 2);
        queryAllAnomalies2.removeIf(((Predicate<? super Object>)this.mAppLabelPredicate).or(((Predicate<? super Object>)this.mAppRestrictionPredicate).negate()));
        if (!queryAllAnomalies2.isEmpty()) {
            n = 1;
        }
        return new RestrictAppTip(n, queryAllAnomalies2);
    }
}
