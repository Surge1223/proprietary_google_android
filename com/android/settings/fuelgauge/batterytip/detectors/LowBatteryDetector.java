package com.android.settings.fuelgauge.batterytip.detectors;

import com.android.settings.fuelgauge.batterytip.tips.LowBatteryTip;
import java.util.concurrent.TimeUnit;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import android.content.Context;
import android.os.PowerManager;
import com.android.settings.fuelgauge.batterytip.BatteryTipPolicy;
import com.android.settings.fuelgauge.BatteryInfo;

public class LowBatteryDetector
{
    private BatteryInfo mBatteryInfo;
    private BatteryTipPolicy mPolicy;
    private PowerManager mPowerManager;
    private int mWarningLevel;
    
    public LowBatteryDetector(final Context context, final BatteryTipPolicy mPolicy, final BatteryInfo mBatteryInfo) {
        this.mPolicy = mPolicy;
        this.mBatteryInfo = mBatteryInfo;
        this.mPowerManager = (PowerManager)context.getSystemService("power");
        this.mWarningLevel = context.getResources().getInteger(17694805);
    }
    
    public BatteryTip detect() {
        final boolean powerSaveMode = this.mPowerManager.isPowerSaveMode();
        final boolean b = this.mBatteryInfo.batteryLevel <= this.mWarningLevel || (this.mBatteryInfo.discharging && this.mBatteryInfo.remainingTimeUs < TimeUnit.HOURS.toMicros(this.mPolicy.lowBatteryHour));
        int n2;
        final int n = n2 = 2;
        if (this.mPolicy.lowBatteryEnabled) {
            if (powerSaveMode) {
                n2 = 1;
            }
            else {
                if (!this.mPolicy.testLowBatteryTip) {
                    n2 = n;
                    if (!this.mBatteryInfo.discharging) {
                        return new LowBatteryTip(n2, powerSaveMode, this.mBatteryInfo.remainingLabel);
                    }
                    n2 = n;
                    if (!b) {
                        return new LowBatteryTip(n2, powerSaveMode, this.mBatteryInfo.remainingLabel);
                    }
                }
                n2 = 0;
            }
        }
        return new LowBatteryTip(n2, powerSaveMode, this.mBatteryInfo.remainingLabel);
    }
}
