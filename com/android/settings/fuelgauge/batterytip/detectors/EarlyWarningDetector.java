package com.android.settings.fuelgauge.batterytip.detectors;

import com.android.settings.fuelgauge.batterytip.tips.EarlyWarningTip;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.fuelgauge.PowerUsageFeatureProvider;
import android.os.PowerManager;
import com.android.settings.fuelgauge.batterytip.BatteryTipPolicy;
import android.content.Context;

public class EarlyWarningDetector
{
    private Context mContext;
    private BatteryTipPolicy mPolicy;
    private PowerManager mPowerManager;
    private PowerUsageFeatureProvider mPowerUsageFeatureProvider;
    
    public EarlyWarningDetector(final BatteryTipPolicy mPolicy, final Context mContext) {
        this.mPolicy = mPolicy;
        this.mPowerManager = (PowerManager)mContext.getSystemService("power");
        this.mContext = mContext;
        this.mPowerUsageFeatureProvider = FeatureFactory.getFactory(mContext).getPowerUsageFeatureProvider(mContext);
    }
    
    public BatteryTip detect() {
        final int intExtra = this.mContext.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("plugged", -1);
        final boolean b = false;
        final boolean b2 = intExtra == 0;
        final boolean powerSaveMode = this.mPowerManager.isPowerSaveMode();
        final boolean b3 = this.mPowerUsageFeatureProvider.getEarlyWarningSignal(this.mContext, EarlyWarningDetector.class.getName()) || this.mPolicy.testBatterySaverTip;
        int n;
        if (powerSaveMode) {
            n = 1;
        }
        else if (this.mPolicy.batterySaverTipEnabled && b2 && b3) {
            n = (b ? 1 : 0);
        }
        else {
            n = 2;
        }
        return new EarlyWarningTip(n, powerSaveMode);
    }
}
