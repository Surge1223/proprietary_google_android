package com.android.settings.fuelgauge.batterytip.detectors;

import com.android.settings.fuelgauge.batterytip.tips.SummaryTip;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import com.android.settings.fuelgauge.batterytip.BatteryTipPolicy;

public class SummaryDetector
{
    private long mAverageTimeMs;
    private BatteryTipPolicy mPolicy;
    
    public SummaryDetector(final BatteryTipPolicy mPolicy, final long mAverageTimeMs) {
        this.mPolicy = mPolicy;
        this.mAverageTimeMs = mAverageTimeMs;
    }
    
    public BatteryTip detect() {
        int n;
        if (this.mPolicy.summaryEnabled) {
            n = 0;
        }
        else {
            n = 2;
        }
        return new SummaryTip(n, this.mAverageTimeMs);
    }
}
