package com.android.settings.fuelgauge.batterytip;

import android.app.StatsManager$StatsUnavailableException;
import android.app.PendingIntent;
import android.content.Intent;
import android.app.StatsManager;
import android.os.UserHandle;
import android.app.AppOpsManager$OpEntry;
import android.app.AppOpsManager$PackageOps;
import java.util.Collection;
import com.android.internal.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import android.os.UserManager;
import android.app.AppOpsManager;
import com.android.settings.fuelgauge.batterytip.actions.BatterySaverAction;
import com.android.settings.fuelgauge.batterytip.actions.OpenBatterySaverAction;
import android.app.Fragment;
import com.android.settings.fuelgauge.batterytip.actions.SmartBatteryAction;
import com.android.settings.fuelgauge.batterytip.actions.RestrictAppAction;
import com.android.settings.fuelgauge.batterytip.actions.OpenRestrictAppFragmentAction;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.actions.UnrestrictAppAction;
import com.android.settings.fuelgauge.batterytip.tips.UnrestrictAppTip;
import com.android.settings.fuelgauge.batterytip.actions.BatteryTipAction;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.SettingsActivity;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;

public class BatteryTipUtils
{
    public static BatteryTipAction getActionForBatteryTip(final BatteryTip batteryTip, final SettingsActivity settingsActivity, final InstrumentedPreferenceFragment instrumentedPreferenceFragment) {
        final int type = batteryTip.getType();
        if (type != 3 && type != 5) {
            if (type == 7) {
                return new UnrestrictAppAction((Context)settingsActivity, (UnrestrictAppTip)batteryTip);
            }
            switch (type) {
                default: {
                    return null;
                }
                case 1: {
                    if (batteryTip.getState() == 1) {
                        return new OpenRestrictAppFragmentAction(instrumentedPreferenceFragment, (RestrictAppTip)batteryTip);
                    }
                    return new RestrictAppAction((Context)settingsActivity, (RestrictAppTip)batteryTip);
                }
                case 0: {
                    return new SmartBatteryAction(settingsActivity, instrumentedPreferenceFragment);
                }
            }
        }
        else {
            if (batteryTip.getState() == 1) {
                return new OpenBatterySaverAction((Context)settingsActivity);
            }
            return new BatterySaverAction((Context)settingsActivity);
        }
    }
    
    public static List<AppInfo> getRestrictedAppsList(final AppOpsManager appOpsManager, final UserManager userManager) {
        final List userProfiles = userManager.getUserProfiles();
        final List packagesForOps = appOpsManager.getPackagesForOps(new int[] { 70 });
        final ArrayList<AppInfo> list = new ArrayList<AppInfo>();
        for (int i = 0; i < CollectionUtils.size((Collection)packagesForOps); ++i) {
            final AppOpsManager$PackageOps appOpsManager$PackageOps = packagesForOps.get(i);
            final List ops = appOpsManager$PackageOps.getOps();
            for (int j = 0; j < ops.size(); ++j) {
                final AppOpsManager$OpEntry appOpsManager$OpEntry = ops.get(j);
                if (appOpsManager$OpEntry.getOp() == 70) {
                    if (appOpsManager$OpEntry.getMode() != 0 && userProfiles.contains(new UserHandle(UserHandle.getUserId(appOpsManager$PackageOps.getUid())))) {
                        list.add(new AppInfo.Builder().setPackageName(appOpsManager$PackageOps.getPackageName()).setUid(appOpsManager$PackageOps.getUid()).build());
                    }
                }
            }
        }
        return list;
    }
    
    public static void uploadAnomalyPendingIntent(final Context context, final StatsManager statsManager) throws StatsManager$StatsUnavailableException {
        statsManager.setBroadcastSubscriber(PendingIntent.getBroadcast(context, 0, new Intent(context, (Class)AnomalyDetectionReceiver.class), 134217728), 1L, 1L);
    }
}
