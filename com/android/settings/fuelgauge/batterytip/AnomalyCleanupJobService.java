package com.android.settings.fuelgauge.batterytip;

import com.android.settingslib.utils.ThreadUtils;
import android.util.Log;
import android.app.job.JobInfo$Builder;
import android.content.ComponentName;
import android.app.job.JobScheduler;
import android.content.Context;
import android.app.job.JobParameters;
import java.util.concurrent.TimeUnit;
import android.app.job.JobService;

public class AnomalyCleanupJobService extends JobService
{
    static final long CLEAN_UP_FREQUENCY_MS;
    
    static {
        CLEAN_UP_FREQUENCY_MS = TimeUnit.DAYS.toMillis(1L);
    }
    
    public static void scheduleCleanUp(final Context context) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService((Class)JobScheduler.class);
        final JobInfo$Builder setPersisted = new JobInfo$Builder(2131427338, new ComponentName(context, (Class)AnomalyCleanupJobService.class)).setPeriodic(AnomalyCleanupJobService.CLEAN_UP_FREQUENCY_MS).setRequiresDeviceIdle(true).setRequiresCharging(true).setPersisted(true);
        if (jobScheduler.getPendingJob(2131427338) == null && jobScheduler.schedule(setPersisted.build()) != 1) {
            Log.i("AnomalyCleanUpJobService", "Anomaly clean up job service schedule failed.");
        }
    }
    
    public boolean onStartJob(final JobParameters jobParameters) {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$AnomalyCleanupJobService$Wvu3W97OjsnNVurAIkZXTma9fMg(this, BatteryDatabaseManager.getInstance((Context)this), new BatteryTipPolicy((Context)this), jobParameters));
        return true;
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        return false;
    }
}
