package com.android.settings.fuelgauge.batterytip;

import android.util.KeyValueListParser;
import android.util.Log;

public class AnomalyInfo
{
    public final Integer anomalyType;
    public final boolean autoRestriction;
    
    public AnomalyInfo(final String string) {
        final StringBuilder sb = new StringBuilder();
        sb.append("anomalyInfo: ");
        sb.append(string);
        Log.i("AnomalyInfo", sb.toString());
        final KeyValueListParser keyValueListParser = new KeyValueListParser(',');
        keyValueListParser.setString(string);
        this.anomalyType = keyValueListParser.getInt("anomaly_type", -1);
        this.autoRestriction = keyValueListParser.getBoolean("auto_restriction", false);
    }
}
