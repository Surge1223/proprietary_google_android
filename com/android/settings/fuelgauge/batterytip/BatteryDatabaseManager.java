package com.android.settings.fuelgauge.batterytip;

import java.util.Iterator;
import java.util.Map;
import android.database.Cursor;
import android.text.TextUtils;
import java.util.Collections;
import android.util.ArrayMap;
import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;

public class BatteryDatabaseManager
{
    private static BatteryDatabaseManager sSingleton;
    private AnomalyDatabaseHelper mDatabaseHelper;
    
    private static /* synthetic */ void $closeResource(final Throwable t, final AutoCloseable autoCloseable) {
        if (t != null) {
            try {
                autoCloseable.close();
            }
            catch (Throwable t2) {
                t.addSuppressed(t2);
            }
        }
        else {
            autoCloseable.close();
        }
    }
    
    private BatteryDatabaseManager(final Context context) {
        this.mDatabaseHelper = AnomalyDatabaseHelper.getInstance(context);
    }
    
    public static BatteryDatabaseManager getInstance(final Context context) {
        if (BatteryDatabaseManager.sSingleton == null) {
            BatteryDatabaseManager.sSingleton = new BatteryDatabaseManager(context);
        }
        return BatteryDatabaseManager.sSingleton;
    }
    
    public void deleteAllAnomaliesBeforeTimeStamp(final long n) {
        synchronized (this) {
            final SQLiteDatabase writableDatabase = this.mDatabaseHelper.getWritableDatabase();
            final Throwable t = null;
            try {
                try {
                    writableDatabase.delete("anomaly", "time_stamp_ms < ?", new String[] { String.valueOf(n) });
                    if (writableDatabase != null) {
                        $closeResource(null, (AutoCloseable)writableDatabase);
                    }
                }
                finally {
                    if (writableDatabase != null) {
                        $closeResource(t, (AutoCloseable)writableDatabase);
                    }
                }
            }
            catch (Throwable t2) {}
        }
    }
    
    public boolean insertAnomaly(final int n, final String s, final int n2, final int n3, long insertWithOnConflict) {
        synchronized (this) {
            final SQLiteDatabase writableDatabase = this.mDatabaseHelper.getWritableDatabase();
            Throwable t2;
            final Throwable t = t2 = null;
            try {
                try {
                    t2 = t;
                    final ContentValues contentValues = new ContentValues();
                    t2 = t;
                    contentValues.put("uid", n);
                    t2 = t;
                    contentValues.put("package_name", s);
                    t2 = t;
                    contentValues.put("anomaly_type", n2);
                    t2 = t;
                    contentValues.put("anomaly_state", n3);
                    t2 = t;
                    contentValues.put("time_stamp_ms", insertWithOnConflict);
                    t2 = t;
                    insertWithOnConflict = writableDatabase.insertWithOnConflict("anomaly", (String)null, contentValues, 4);
                    final boolean b = insertWithOnConflict != -1L;
                    if (writableDatabase != null) {
                        $closeResource(null, (AutoCloseable)writableDatabase);
                    }
                    return b;
                }
                finally {
                    if (writableDatabase != null) {
                        $closeResource(t2, (AutoCloseable)writableDatabase);
                    }
                }
            }
            catch (Throwable t3) {}
        }
    }
    
    public List<AppInfo> queryAllAnomalies(final long n, int int1) {
        synchronized (this) {
            final ArrayList<AppInfo> list = new ArrayList<AppInfo>();
            final SQLiteDatabase readableDatabase = this.mDatabaseHelper.getReadableDatabase();
            Object o2;
            final Object o = o2 = null;
            try {
                o2 = o;
                final ArrayMap arrayMap = new ArrayMap();
                o2 = o;
                Object o3 = readableDatabase.query("anomaly", new String[] { "package_name", "anomaly_type", "uid" }, "time_stamp_ms > ? AND anomaly_state = ? ", new String[] { String.valueOf(n), String.valueOf(int1) }, (String)null, (String)null, "time_stamp_ms DESC");
                Throwable t = null;
                try {
                    while (((Cursor)o3).moveToNext()) {
                        int1 = ((Cursor)o3).getInt(((Cursor)o3).getColumnIndex("uid"));
                        if (!((Map)arrayMap).containsKey(int1)) {
                            o2 = new AppInfo.Builder();
                            ((Map<Integer, AppInfo.Builder>)arrayMap).put(int1, ((AppInfo.Builder)o2).setUid(int1).setPackageName(((Cursor)o3).getString(((Cursor)o3).getColumnIndex("package_name"))));
                        }
                        ((Map<Integer, AppInfo.Builder>)arrayMap).get(int1).addAnomalyType(((Cursor)o3).getInt(((Cursor)o3).getColumnIndex("anomaly_type")));
                    }
                    if (o3 != null) {
                        o2 = o;
                        $closeResource(null, (AutoCloseable)o3);
                    }
                    o2 = o;
                    o3 = ((Map<Integer, AppInfo.Builder>)arrayMap).keySet().iterator();
                    while (true) {
                        o2 = o;
                        if (!((Iterator)o3).hasNext()) {
                            break;
                        }
                        o2 = o;
                        list.add(((Map<Integer, AppInfo.Builder>)arrayMap).get(((Iterator<Integer>)o3).next()).build());
                    }
                    if (readableDatabase != null) {
                        $closeResource(null, (AutoCloseable)readableDatabase);
                    }
                    return list;
                }
                catch (Throwable t) {
                    try {
                        throw t;
                    }
                    finally {}
                }
                finally {
                    t = null;
                }
                if (o3 != null) {
                    o2 = o;
                    $closeResource(t, (AutoCloseable)o3);
                }
                o2 = o;
                throw;
            }
            catch (Throwable t3) {}
            finally {
                if (readableDatabase != null) {
                    $closeResource((Throwable)o2, (AutoCloseable)readableDatabase);
                }
            }
        }
    }
    
    public void updateAnomalies(final List<AppInfo> list, final int n) {
        synchronized (this) {
            if (!list.isEmpty()) {
                final int size = list.size();
                final String[] array = new String[size];
                for (int i = 0; i < size; ++i) {
                    array[i] = list.get(i).packageName;
                }
                final SQLiteDatabase writableDatabase = this.mDatabaseHelper.getWritableDatabase();
                Throwable t2;
                final Throwable t = t2 = null;
                try {
                    try {
                        t2 = t;
                        final ContentValues contentValues = new ContentValues();
                        t2 = t;
                        contentValues.put("anomaly_state", n);
                        t2 = t;
                        t2 = t;
                        final StringBuilder sb = new StringBuilder();
                        t2 = t;
                        sb.append("package_name IN (");
                        t2 = t;
                        sb.append(TextUtils.join((CharSequence)",", (Iterable)Collections.nCopies(list.size(), "?")));
                        t2 = t;
                        sb.append(")");
                        t2 = t;
                        writableDatabase.update("anomaly", contentValues, sb.toString(), array);
                        if (writableDatabase != null) {
                            $closeResource(null, (AutoCloseable)writableDatabase);
                        }
                    }
                    finally {
                        if (writableDatabase != null) {
                            $closeResource(t2, (AutoCloseable)writableDatabase);
                        }
                    }
                }
                catch (Throwable t3) {}
            }
        }
    }
}
