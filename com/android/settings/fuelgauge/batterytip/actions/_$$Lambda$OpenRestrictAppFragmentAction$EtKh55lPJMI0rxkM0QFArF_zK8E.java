package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.fuelgauge.RestrictedAppDetails;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.fuelgauge.batterytip.BatteryDatabaseManager;
import java.util.List;

public final class _$$Lambda$OpenRestrictAppFragmentAction$EtKh55lPJMI0rxkM0QFArF_zK8E implements Runnable
{
    @Override
    public final void run() {
        OpenRestrictAppFragmentAction.lambda$handlePositiveAction$0(this.f$0, this.f$1);
    }
}
