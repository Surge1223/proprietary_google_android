package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.content.Context;

public abstract class BatteryTipAction
{
    protected Context mContext;
    protected MetricsFeatureProvider mMetricsFeatureProvider;
    
    public BatteryTipAction(final Context mContext) {
        this.mContext = mContext;
        this.mMetricsFeatureProvider = FeatureFactory.getFactory(mContext).getMetricsFeatureProvider();
    }
    
    public abstract void handlePositiveAction(final int p0);
}
