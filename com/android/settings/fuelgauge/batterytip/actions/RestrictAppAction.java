package com.android.settings.fuelgauge.batterytip.actions;

import java.util.Iterator;
import java.util.List;
import android.util.Pair;
import java.util.Collection;
import com.android.internal.util.CollectionUtils;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.settings.fuelgauge.batterytip.BatteryDatabaseManager;

public class RestrictAppAction extends BatteryTipAction
{
    BatteryDatabaseManager mBatteryDatabaseManager;
    BatteryUtils mBatteryUtils;
    private RestrictAppTip mRestrictAppTip;
    
    public RestrictAppAction(final Context context, final RestrictAppTip mRestrictAppTip) {
        super(context);
        this.mRestrictAppTip = mRestrictAppTip;
        this.mBatteryUtils = BatteryUtils.getInstance(context);
        this.mBatteryDatabaseManager = BatteryDatabaseManager.getInstance(context);
    }
    
    @Override
    public void handlePositiveAction(final int n) {
        final List<AppInfo> restrictAppList = this.mRestrictAppTip.getRestrictAppList();
        for (int i = 0; i < restrictAppList.size(); ++i) {
            final AppInfo appInfo = restrictAppList.get(i);
            final String packageName = appInfo.packageName;
            this.mBatteryUtils.setForceAppStandby(appInfo.uid, packageName, 1);
            if (CollectionUtils.isEmpty((Collection)appInfo.anomalyTypes)) {
                this.mMetricsFeatureProvider.action(this.mContext, 1362, packageName, Pair.create((Object)833, (Object)n));
            }
            else {
                final Iterator iterator = appInfo.anomalyTypes.iterator();
                while (iterator.hasNext()) {
                    this.mMetricsFeatureProvider.action(this.mContext, 1362, packageName, Pair.create((Object)833, (Object)n), Pair.create((Object)1366, (Object)(int)iterator.next()));
                }
            }
        }
        this.mBatteryDatabaseManager.updateAnomalies(restrictAppList, 1);
    }
}
