package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settings.fuelgauge.batterytip.AppInfo;
import android.util.Pair;
import android.content.Context;
import com.android.settings.fuelgauge.batterytip.tips.UnrestrictAppTip;
import com.android.settings.fuelgauge.BatteryUtils;

public class UnrestrictAppAction extends BatteryTipAction
{
    BatteryUtils mBatteryUtils;
    private UnrestrictAppTip mUnRestrictAppTip;
    
    public UnrestrictAppAction(final Context context, final UnrestrictAppTip mUnRestrictAppTip) {
        super(context);
        this.mUnRestrictAppTip = mUnRestrictAppTip;
        this.mBatteryUtils = BatteryUtils.getInstance(context);
    }
    
    @Override
    public void handlePositiveAction(final int n) {
        final AppInfo unrestrictAppInfo = this.mUnRestrictAppTip.getUnrestrictAppInfo();
        this.mBatteryUtils.setForceAppStandby(unrestrictAppInfo.uid, unrestrictAppInfo.packageName, 0);
        this.mMetricsFeatureProvider.action(this.mContext, 1363, unrestrictAppInfo.packageName, Pair.create((Object)833, (Object)n));
    }
}
