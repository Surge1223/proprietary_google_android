package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settings.fuelgauge.SmartBatterySettings;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.content.Context;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.SettingsActivity;
import android.app.Fragment;

public class SmartBatteryAction extends BatteryTipAction
{
    private Fragment mFragment;
    private SettingsActivity mSettingsActivity;
    
    public SmartBatteryAction(final SettingsActivity mSettingsActivity, final Fragment mFragment) {
        super(mSettingsActivity.getApplicationContext());
        this.mSettingsActivity = mSettingsActivity;
        this.mFragment = mFragment;
    }
    
    @Override
    public void handlePositiveAction(int metricsCategory) {
        this.mMetricsFeatureProvider.action(this.mContext, 1364, metricsCategory);
        final SubSettingLauncher subSettingLauncher = new SubSettingLauncher((Context)this.mSettingsActivity);
        if (this.mFragment instanceof Instrumentable) {
            metricsCategory = ((Instrumentable)this.mFragment).getMetricsCategory();
        }
        else {
            metricsCategory = 0;
        }
        subSettingLauncher.setSourceMetricsCategory(metricsCategory).setDestination(SmartBatterySettings.class.getName()).setTitle(2131889178).launch();
    }
}
