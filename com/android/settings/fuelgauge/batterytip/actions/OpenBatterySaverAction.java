package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settings.fuelgauge.batterysaver.BatterySaverSettings;
import com.android.settings.core.SubSettingLauncher;
import android.content.Context;

public class OpenBatterySaverAction extends BatteryTipAction
{
    public OpenBatterySaverAction(final Context context) {
        super(context);
    }
    
    @Override
    public void handlePositiveAction(final int sourceMetricsCategory) {
        this.mMetricsFeatureProvider.action(this.mContext, 1388, sourceMetricsCategory);
        new SubSettingLauncher(this.mContext).setDestination(BatterySaverSettings.class.getName()).setSourceMetricsCategory(sourceMetricsCategory).launch();
    }
}
