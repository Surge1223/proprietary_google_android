package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settingslib.fuelgauge.BatterySaverUtils;
import android.content.Context;

public class BatterySaverAction extends BatteryTipAction
{
    public BatterySaverAction(final Context context) {
        super(context);
    }
    
    @Override
    public void handlePositiveAction(final int n) {
        BatterySaverUtils.setPowerSaveMode(this.mContext, true, true);
        this.mMetricsFeatureProvider.action(this.mContext, 1365, n);
    }
}
