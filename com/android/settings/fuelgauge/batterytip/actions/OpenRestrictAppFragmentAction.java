package com.android.settings.fuelgauge.batterytip.actions;

import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.fuelgauge.RestrictedAppDetails;
import com.android.settings.fuelgauge.batterytip.AppInfo;
import java.util.List;
import com.android.settings.fuelgauge.batterytip.tips.RestrictAppTip;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.fuelgauge.batterytip.BatteryDatabaseManager;

public class OpenRestrictAppFragmentAction extends BatteryTipAction
{
    BatteryDatabaseManager mBatteryDatabaseManager;
    private final InstrumentedPreferenceFragment mFragment;
    private final RestrictAppTip mRestrictAppTip;
    
    public OpenRestrictAppFragmentAction(final InstrumentedPreferenceFragment mFragment, final RestrictAppTip mRestrictAppTip) {
        super(mFragment.getContext());
        this.mFragment = mFragment;
        this.mRestrictAppTip = mRestrictAppTip;
        this.mBatteryDatabaseManager = BatteryDatabaseManager.getInstance(this.mContext);
    }
    
    @Override
    public void handlePositiveAction(final int n) {
        this.mMetricsFeatureProvider.action(this.mContext, 1361, n);
        final List<AppInfo> restrictAppList = this.mRestrictAppTip.getRestrictAppList();
        RestrictedAppDetails.startRestrictedAppDetails(this.mFragment, restrictAppList);
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$OpenRestrictAppFragmentAction$EtKh55lPJMI0rxkM0QFArF_zK8E(this, restrictAppList));
    }
}
