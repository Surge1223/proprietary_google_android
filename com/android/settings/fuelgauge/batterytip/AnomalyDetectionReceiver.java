package com.android.settings.fuelgauge.batterytip;

import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class AnomalyDetectionReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final long longExtra = intent.getLongExtra("android.app.extra.STATS_CONFIG_UID", -1L);
        final long longExtra2 = intent.getLongExtra("android.app.extra.STATS_CONFIG_KEY", -1L);
        final long longExtra3 = intent.getLongExtra("android.app.extra.STATS_SUBSCRIPTION_ID", -1L);
        final StringBuilder sb = new StringBuilder();
        sb.append("Anomaly intent received.  configUid = ");
        sb.append(longExtra);
        sb.append(" configKey = ");
        sb.append(longExtra2);
        sb.append(" subscriptionId = ");
        sb.append(longExtra3);
        Log.i("SettingsAnomalyReceiver", sb.toString());
        intent.getExtras().putLong("key_anomaly_timestamp", System.currentTimeMillis());
        AnomalyDetectionJobService.scheduleAnomalyDetection(context, intent);
    }
}
