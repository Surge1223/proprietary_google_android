package com.android.settings.fuelgauge.batterytip;

import java.util.ArrayList;
import android.util.Pair;
import android.provider.Settings;
import java.util.Collection;
import com.android.internal.util.ArrayUtils;
import android.os.Bundle;
import com.android.settingslib.utils.ThreadUtils;
import java.util.List;
import android.os.StatsDimensionsValue;
import android.util.Log;
import android.app.job.JobInfo$Builder;
import android.content.ComponentName;
import android.app.job.JobScheduler;
import android.content.Intent;
import android.app.job.JobWorkItem;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.fuelgauge.PowerUsageFeatureProvider;
import android.content.ContentResolver;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import android.os.UserManager;
import com.android.settings.fuelgauge.BatteryUtils;
import android.content.Context;
import android.app.job.JobParameters;
import java.util.concurrent.TimeUnit;
import android.app.job.JobService;

public class AnomalyDetectionJobService extends JobService
{
    static final long MAX_DELAY_MS;
    static final int STATSD_UID_FILED = 1;
    static final int UID_NULL = -1;
    boolean mIsJobCanceled;
    private final Object mLock;
    
    static {
        MAX_DELAY_MS = TimeUnit.MINUTES.toMillis(30L);
    }
    
    public AnomalyDetectionJobService() {
        this.mLock = new Object();
        this.mIsJobCanceled = false;
    }
    
    public static void scheduleAnomalyDetection(final Context context, final Intent intent) {
        if (((JobScheduler)context.getSystemService((Class)JobScheduler.class)).enqueue(new JobInfo$Builder(2131427340, new ComponentName(context, (Class)AnomalyDetectionJobService.class)).setOverrideDeadline(AnomalyDetectionJobService.MAX_DELAY_MS).build(), new JobWorkItem(intent)) != 1) {
            Log.i("AnomalyDetectionService", "Anomaly detection job service enqueue failed.");
        }
    }
    
    void completeWork(final JobParameters jobParameters, final JobWorkItem jobWorkItem) {
        synchronized (this.mLock) {
            if (this.mIsJobCanceled) {
                return;
            }
            jobParameters.completeWork(jobWorkItem);
        }
    }
    
    JobWorkItem dequeueWork(final JobParameters jobParameters) {
        synchronized (this.mLock) {
            if (this.mIsJobCanceled) {
                return null;
            }
            return jobParameters.dequeueWork();
        }
    }
    
    int extractUidFromStatsDimensionsValue(final StatsDimensionsValue statsDimensionsValue) {
        if (statsDimensionsValue == null) {
            return -1;
        }
        if (statsDimensionsValue.isValueType(3) && statsDimensionsValue.getField() == 1) {
            return statsDimensionsValue.getIntValue();
        }
        if (statsDimensionsValue.isValueType(7)) {
            final List tupleValueList = statsDimensionsValue.getTupleValueList();
            for (int i = 0; i < tupleValueList.size(); ++i) {
                final int uidFromStatsDimensionsValue = this.extractUidFromStatsDimensionsValue(tupleValueList.get(i));
                if (uidFromStatsDimensionsValue != -1) {
                    return uidFromStatsDimensionsValue;
                }
            }
        }
        return -1;
    }
    
    public boolean onStartJob(final JobParameters jobParameters) {
        synchronized (this.mLock) {
            this.mIsJobCanceled = false;
            // monitorexit(this.mLock)
            ThreadUtils.postOnBackgroundThread(new _$$Lambda$AnomalyDetectionJobService$7JxJe3rza0cCkIc77iCS_ZKPfL4(this, jobParameters));
            return true;
        }
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        synchronized (this.mLock) {
            return this.mIsJobCanceled = true;
        }
    }
    
    void saveAnomalyToDatabase(Context context, final UserManager userManager, final BatteryDatabaseManager batteryDatabaseManager, final BatteryUtils batteryUtils, final BatteryTipPolicy batteryTipPolicy, final PowerWhitelistBackend powerWhitelistBackend, final ContentResolver contentResolver, final PowerUsageFeatureProvider powerUsageFeatureProvider, final MetricsFeatureProvider metricsFeatureProvider, final Bundle bundle) {
        final StatsDimensionsValue statsDimensionsValue = (StatsDimensionsValue)bundle.getParcelable("android.app.extra.STATS_DIMENSIONS_VALUE");
        final long long1 = bundle.getLong("key_anomaly_timestamp", System.currentTimeMillis());
        final ArrayList stringArrayList = bundle.getStringArrayList("android.app.extra.STATS_BROADCAST_SUBSCRIBER_COOKIES");
        String s;
        if (!ArrayUtils.isEmpty((Collection)stringArrayList)) {
            s = stringArrayList.get(0);
        }
        else {
            s = "";
        }
        final AnomalyInfo anomalyInfo = new AnomalyInfo(s);
        final StringBuilder sb = new StringBuilder();
        sb.append("Extra stats value: ");
        sb.append(statsDimensionsValue.toString());
        Log.i("AnomalyDetectionService", sb.toString());
        Label_0400: {
            try {
                final int uidFromStatsDimensionsValue = this.extractUidFromStatsDimensionsValue(statsDimensionsValue);
                boolean b = false;
                Label_0177: {
                    Label_0148: {
                        if (!powerUsageFeatureProvider.isSmartBatterySupported()) {
                            if (Settings.Global.getInt(contentResolver, "app_auto_restriction_enabled", 1) == 1) {
                                break Label_0148;
                            }
                            break Label_0148;
                        }
                        try {
                            if (Settings.Global.getInt(contentResolver, "adaptive_battery_management_enabled", 1) == 1) {
                                b = true;
                                break Label_0177;
                            }
                            b = false;
                            break Label_0177;
                        }
                        catch (NullPointerException ex) {}
                        catch (IndexOutOfBoundsException ex2) {}
                    }
                    break Label_0400;
                }
                final String packageName = batteryUtils.getPackageName(uidFromStatsDimensionsValue);
                final long appLongVersionCode = batteryUtils.getAppLongVersionCode(packageName);
                if (batteryUtils.shouldHideAnomaly(powerWhitelistBackend, uidFromStatsDimensionsValue, anomalyInfo)) {
                    try {
                        final Pair create = Pair.create((Object)833, (Object)anomalyInfo.anomalyType);
                        try {
                            metricsFeatureProvider.action(context, 1387, packageName, create, Pair.create((Object)1389, (Object)appLongVersionCode));
                        }
                        catch (NullPointerException | IndexOutOfBoundsException ex3) {
                            final Context context2;
                            context = context2;
                        }
                    }
                    catch (NullPointerException | IndexOutOfBoundsException ex4) {
                        final Context context3;
                        context = context3;
                        break Label_0400;
                    }
                }
                Label_0325: {
                    if (!b) {
                        break Label_0325;
                    }
                    Label_0344: {
                        int intValue;
                        try {
                            if (anomalyInfo.autoRestriction) {
                                batteryUtils.setForceAppStandby(uidFromStatsDimensionsValue, packageName, 1);
                                intValue = anomalyInfo.anomalyType;
                                final BatteryDatabaseManager batteryDatabaseManager2 = batteryDatabaseManager;
                                final int n = uidFromStatsDimensionsValue;
                                final String s2 = packageName;
                                final int n2 = intValue;
                                final int n3 = 2;
                                final long n4 = long1;
                                batteryDatabaseManager2.insertAnomaly(n, s2, n2, n3, n4);
                                break Label_0344;
                            }
                            break Label_0325;
                        }
                        catch (NullPointerException | IndexOutOfBoundsException ex5) {
                            final Context context4;
                            context = context4;
                            break Label_0400;
                        }
                        try {
                            final BatteryDatabaseManager batteryDatabaseManager2 = batteryDatabaseManager;
                            final int n = uidFromStatsDimensionsValue;
                            final String s2 = packageName;
                            final int n2 = intValue;
                            final int n3 = 2;
                            final long n4 = long1;
                            batteryDatabaseManager2.insertAnomaly(n, s2, n2, n3, n4);
                            metricsFeatureProvider.action(context, 1367, packageName, Pair.create((Object)1366, (Object)anomalyInfo.anomalyType), Pair.create((Object)1389, (Object)appLongVersionCode));
                            return;
                            batteryDatabaseManager.insertAnomaly(uidFromStatsDimensionsValue, packageName, anomalyInfo.anomalyType, 0, long1);
                        }
                        catch (NullPointerException | IndexOutOfBoundsException ex6) {
                            final Context context5;
                            context = context5;
                        }
                    }
                }
            }
            catch (NullPointerException ex7) {}
            catch (IndexOutOfBoundsException ex8) {}
        }
        Log.e("AnomalyDetectionService", "Parse stats dimensions value error.", (Throwable)context);
    }
}
