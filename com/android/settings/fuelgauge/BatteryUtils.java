package com.android.settings.fuelgauge;

import java.util.Collections;
import java.util.Comparator;
import android.util.SparseLongArray;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import android.os.UserManager;
import android.os.Bundle;
import android.os.BatteryStats$Timer;
import android.os.BatteryStats;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.android.internal.os.BatterySipper;
import com.android.internal.os.BatterySipper;
import com.android.internal.os.BatteryStatsHelper;
import android.os.UserHandle;
import android.content.pm.PackageManager;
import com.android.settings.fuelgauge.batterytip.AnomalyInfo;
import java.util.List;
import com.android.internal.util.ArrayUtils;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.content.Intent;
import android.util.Log;
import com.android.settingslib.utils.PowerUtil;
import android.os.SystemClock;
import android.os.BatteryStats$Uid;
import com.android.settings.overlay.FeatureFactory;
import android.content.pm.PackageManager;
import android.content.Context;
import android.app.AppOpsManager;

public class BatteryUtils
{
    private static BatteryUtils sInstance;
    private AppOpsManager mAppOpsManager;
    private Context mContext;
    private PackageManager mPackageManager;
    PowerUsageFeatureProvider mPowerUsageFeatureProvider;
    
    BatteryUtils(final Context mContext) {
        this.mContext = mContext;
        this.mPackageManager = mContext.getPackageManager();
        this.mAppOpsManager = (AppOpsManager)mContext.getSystemService("appops");
        this.mPowerUsageFeatureProvider = FeatureFactory.getFactory(mContext).getPowerUsageFeatureProvider(mContext);
    }
    
    public static BatteryUtils getInstance(final Context context) {
        if (BatteryUtils.sInstance == null || BatteryUtils.sInstance.isDataCorrupted()) {
            BatteryUtils.sInstance = new BatteryUtils(context.getApplicationContext());
        }
        return BatteryUtils.sInstance;
    }
    
    private long getProcessBackgroundTimeMs(final BatteryStats$Uid batteryStats$Uid, final int n) {
        final long processStateTime = batteryStats$Uid.getProcessStateTime(3, PowerUtil.convertMsToUs(SystemClock.elapsedRealtime()), n);
        final StringBuilder sb = new StringBuilder();
        sb.append("package: ");
        sb.append(this.mPackageManager.getNameForUid(batteryStats$Uid.getUid()));
        Log.v("BatteryUtils", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("background time(us): ");
        sb2.append(processStateTime);
        Log.v("BatteryUtils", sb2.toString());
        return PowerUtil.convertUsToMs(processStateTime);
    }
    
    private long getProcessForegroundTimeMs(final BatteryStats$Uid batteryStats$Uid, final int n) {
        final long convertMsToUs = PowerUtil.convertMsToUs(SystemClock.elapsedRealtime());
        return this.getScreenUsageTimeMs(batteryStats$Uid, n, convertMsToUs) + PowerUtil.convertUsToMs(this.getForegroundServiceTotalTimeUs(batteryStats$Uid, convertMsToUs));
    }
    
    private long getScreenUsageTimeMs(final BatteryStats$Uid batteryStats$Uid, final int n) {
        return this.getScreenUsageTimeMs(batteryStats$Uid, n, PowerUtil.convertMsToUs(SystemClock.elapsedRealtime()));
    }
    
    private long getScreenUsageTimeMs(final BatteryStats$Uid batteryStats$Uid, final int n, final long n2) {
        final int[] array = { 0 };
        int i = 0;
        array[0] = 0;
        final StringBuilder sb = new StringBuilder();
        sb.append("package: ");
        sb.append(this.mPackageManager.getNameForUid(batteryStats$Uid.getUid()));
        Log.v("BatteryUtils", sb.toString());
        long n3 = 0L;
        while (i < array.length) {
            final int n4 = array[i];
            final long processStateTime = batteryStats$Uid.getProcessStateTime(n4, n2, n);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("type: ");
            sb2.append(n4);
            sb2.append(" time(us): ");
            sb2.append(processStateTime);
            Log.v("BatteryUtils", sb2.toString());
            n3 += processStateTime;
            ++i;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("foreground time(us): ");
        sb3.append(n3);
        Log.v("BatteryUtils", sb3.toString());
        return PowerUtil.convertUsToMs(Math.min(n3, this.getForegroundActivityTotalTimeUs(batteryStats$Uid, n2)));
    }
    
    private boolean hasLauncherEntry(final String[] array) {
        final Intent intent = new Intent("android.intent.action.MAIN", (Uri)null);
        intent.addCategory("android.intent.category.LAUNCHER");
        final List queryIntentActivities = this.mPackageManager.queryIntentActivities(intent, 1835520);
        for (int i = 0; i < queryIntentActivities.size(); ++i) {
            if (ArrayUtils.contains((Object[])array, (Object)queryIntentActivities.get(i).activityInfo.packageName)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isDataCorrupted() {
        return this.mPackageManager == null || this.mAppOpsManager == null;
    }
    
    private boolean isExcessiveBackgroundAnomaly(final AnomalyInfo anomalyInfo) {
        return anomalyInfo.anomalyType == 4;
    }
    
    private boolean isSystemApp(final PackageManager packageManager, final String[] array) {
        for (final String s : array) {
            try {
                if ((packageManager.getApplicationInfo(s, 0).flags & 0x1) != 0x0) {
                    return true;
                }
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Package not found: ");
                sb.append(s);
                Log.e("BatteryUtils", sb.toString(), (Throwable)ex);
            }
        }
        return false;
    }
    
    private boolean isSystemUid(int appId) {
        appId = UserHandle.getAppId(appId);
        return appId >= 0 && appId < 10000;
    }
    
    public static void logRuntime(final String s, final String s2, final long n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s2);
        sb.append(": ");
        sb.append(System.currentTimeMillis() - n);
        sb.append("ms");
        Log.d(s, sb.toString());
    }
    
    public double calculateBatteryPercent(final double n, final double n2, final double n3, final int n4) {
        if (n2 == 0.0) {
            return 0.0;
        }
        return n / (n2 - n3) * n4;
    }
    
    public long calculateLastFullChargeTime(final BatteryStatsHelper batteryStatsHelper, final long n) {
        return n - batteryStatsHelper.getStats().getStartClockTime();
    }
    
    public long calculateRunningTimeBasedOnStatsType(final BatteryStatsHelper batteryStatsHelper, final int n) {
        return PowerUtil.convertUsToMs(batteryStatsHelper.getStats().computeBatteryRealtime(PowerUtil.convertMsToUs(SystemClock.elapsedRealtime()), n));
    }
    
    public long calculateScreenUsageTime(final BatteryStatsHelper batteryStatsHelper) {
        final BatterySipper batterySipperByType = this.findBatterySipperByType(batteryStatsHelper.getUsageList(), BatterySipper$DrainType.SCREEN);
        long usageTimeMs;
        if (batterySipperByType != null) {
            usageTimeMs = batterySipperByType.usageTimeMs;
        }
        else {
            usageTimeMs = 0L;
        }
        return usageTimeMs;
    }
    
    public BatterySipper findBatterySipperByType(final List<BatterySipper> list, final BatterySipper$DrainType batterySipper$DrainType) {
        for (int i = 0; i < list.size(); ++i) {
            final BatterySipper batterySipper = list.get(i);
            if (batterySipper.drainType == batterySipper$DrainType) {
                return batterySipper;
            }
        }
        return null;
    }
    
    public long getAppLongVersionCode(final String s) {
        try {
            return this.mPackageManager.getPackageInfo(s, 0).getLongVersionCode();
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find package: ");
            sb.append(s);
            Log.e("BatteryUtils", sb.toString(), (Throwable)ex);
            return -1L;
        }
    }
    
    public BatteryInfo getBatteryInfo(final BatteryStatsHelper batteryStatsHelper, final String s) {
        final long currentTimeMillis = System.currentTimeMillis();
        final Intent registerReceiver = this.mContext.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        final long convertMsToUs = PowerUtil.convertMsToUs(SystemClock.elapsedRealtime());
        final BatteryStats stats = batteryStatsHelper.getStats();
        Estimate enhancedBatteryPrediction;
        if (this.mPowerUsageFeatureProvider != null && this.mPowerUsageFeatureProvider.isEnhancedBatteryPredictionEnabled(this.mContext)) {
            enhancedBatteryPrediction = this.mPowerUsageFeatureProvider.getEnhancedBatteryPrediction(this.mContext);
        }
        else {
            enhancedBatteryPrediction = new Estimate(PowerUtil.convertUsToMs(stats.computeBatteryTimeRemaining(convertMsToUs)), false, -1L);
        }
        logRuntime(s, "BatteryInfoLoader post query", currentTimeMillis);
        final BatteryInfo batteryInfo = BatteryInfo.getBatteryInfo(this.mContext, registerReceiver, stats, enhancedBatteryPrediction, convertMsToUs, false);
        logRuntime(s, "BatteryInfoLoader.loadInBackground", currentTimeMillis);
        return batteryInfo;
    }
    
    long getForegroundActivityTotalTimeUs(final BatteryStats$Uid batteryStats$Uid, final long n) {
        final BatteryStats$Timer foregroundActivityTimer = batteryStats$Uid.getForegroundActivityTimer();
        if (foregroundActivityTimer != null) {
            return foregroundActivityTimer.getTotalTimeLocked(n, 0);
        }
        return 0L;
    }
    
    long getForegroundServiceTotalTimeUs(final BatteryStats$Uid batteryStats$Uid, final long n) {
        final BatteryStats$Timer foregroundServiceTimer = batteryStats$Uid.getForegroundServiceTimer();
        if (foregroundServiceTimer != null) {
            return foregroundServiceTimer.getTotalTimeLocked(n, 0);
        }
        return 0L;
    }
    
    public String getPackageName(final int n) {
        final String[] packagesForUid = this.mPackageManager.getPackagesForUid(n);
        String s;
        if (ArrayUtils.isEmpty((Object[])packagesForUid)) {
            s = null;
        }
        else {
            s = packagesForUid[0];
        }
        return s;
    }
    
    public int getPackageUid(final String s) {
        int packageUid = -1;
        if (s == null) {
            return packageUid;
        }
        try {
            packageUid = this.mPackageManager.getPackageUid(s, 128);
            return packageUid;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return -1;
        }
    }
    
    public long getProcessTimeMs(final int n, final BatteryStats$Uid batteryStats$Uid, final int n2) {
        if (batteryStats$Uid == null) {
            return 0L;
        }
        switch (n) {
            default: {
                return 0L;
            }
            case 3: {
                return this.getProcessForegroundTimeMs(batteryStats$Uid, n2) + this.getProcessBackgroundTimeMs(batteryStats$Uid, n2);
            }
            case 2: {
                return this.getProcessBackgroundTimeMs(batteryStats$Uid, n2);
            }
            case 1: {
                return this.getProcessForegroundTimeMs(batteryStats$Uid, n2);
            }
            case 0: {
                return this.getScreenUsageTimeMs(batteryStats$Uid, n2);
            }
        }
    }
    
    public int getSummaryResIdFromAnomalyType(final int n) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Incorrect anomaly type: ");
                sb.append(n);
                throw new IllegalArgumentException(sb.toString());
            }
            case 2: {
                return 2131886568;
            }
            case 1: {
                return 2131886570;
            }
            case 0: {
                return 2131886569;
            }
        }
    }
    
    public int getTargetSdkVersion(final String s) {
        try {
            return this.mPackageManager.getApplicationInfo(s, 128).targetSdkVersion;
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find package: ");
            sb.append(s);
            Log.e("BatteryUtils", sb.toString(), (Throwable)ex);
            return -1;
        }
    }
    
    public void initBatteryStatsHelper(final BatteryStatsHelper batteryStatsHelper, final Bundle bundle, final UserManager userManager) {
        batteryStatsHelper.create(bundle);
        batteryStatsHelper.clearStats();
        batteryStatsHelper.refreshStats(0, userManager.getUserProfiles());
    }
    
    public boolean isBackgroundRestrictionEnabled(int checkOpNoThrow, final int n, final String s) {
        final boolean b = true;
        if (checkOpNoThrow >= 26) {
            return true;
        }
        checkOpNoThrow = this.mAppOpsManager.checkOpNoThrow(63, n, s);
        boolean b2 = b;
        if (checkOpNoThrow != 1) {
            b2 = (checkOpNoThrow == 2 && b);
        }
        return b2;
    }
    
    public boolean isForceAppStandbyEnabled(int checkOpNoThrow, final String s) {
        checkOpNoThrow = this.mAppOpsManager.checkOpNoThrow(70, checkOpNoThrow, s);
        boolean b = true;
        if (checkOpNoThrow != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean isPreOApp(final String s) {
        boolean b = false;
        try {
            if (this.mPackageManager.getApplicationInfo(s, 128).targetSdkVersion < 26) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find package: ");
            sb.append(s);
            Log.e("BatteryUtils", sb.toString(), (Throwable)ex);
            return false;
        }
    }
    
    public boolean isPreOApp(final String[] array) {
        if (ArrayUtils.isEmpty((Object[])array)) {
            return false;
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            if (this.isPreOApp(array[i])) {
                return true;
            }
        }
        return false;
    }
    
    public double removeHiddenBatterySippers(final List<BatterySipper> list) {
        double n = 0.0;
        BatterySipper batterySipper = null;
        double n2;
        for (int i = list.size() - 1; i >= 0; --i, n = n2) {
            final BatterySipper batterySipper2 = list.get(i);
            n2 = n;
            if (this.shouldHideSipper(batterySipper2)) {
                list.remove(i);
                n2 = n;
                if (batterySipper2.drainType != BatterySipper$DrainType.OVERCOUNTED) {
                    n2 = n;
                    if (batterySipper2.drainType != BatterySipper$DrainType.SCREEN) {
                        n2 = n;
                        if (batterySipper2.drainType != BatterySipper$DrainType.UNACCOUNTED) {
                            n2 = n;
                            if (batterySipper2.drainType != BatterySipper$DrainType.BLUETOOTH) {
                                n2 = n;
                                if (batterySipper2.drainType != BatterySipper$DrainType.WIFI) {
                                    n2 = n;
                                    if (batterySipper2.drainType != BatterySipper$DrainType.IDLE) {
                                        n2 = n + batterySipper2.totalPowerMah;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (batterySipper2.drainType == BatterySipper$DrainType.SCREEN) {
                batterySipper = batterySipper2;
            }
        }
        this.smearScreenBatterySipper(list, batterySipper);
        return n;
    }
    
    public void setForceAppStandby(final int n, final String s, final int n2) {
        if (this.isPreOApp(s)) {
            this.mAppOpsManager.setMode(63, n, s, n2);
        }
        this.mAppOpsManager.setMode(70, n, s, n2);
    }
    
    public boolean shouldHideAnomaly(final PowerWhitelistBackend powerWhitelistBackend, final int n, final AnomalyInfo anomalyInfo) {
        final String[] packagesForUid = this.mPackageManager.getPackagesForUid(n);
        final boolean empty = ArrayUtils.isEmpty((Object[])packagesForUid);
        final boolean b = true;
        if (empty) {
            return true;
        }
        boolean b2 = b;
        if (!this.isSystemUid(n)) {
            b2 = b;
            if (!powerWhitelistBackend.isWhitelisted(packagesForUid)) {
                if (this.isSystemApp(this.mPackageManager, packagesForUid)) {
                    b2 = b;
                    if (!this.hasLauncherEntry(packagesForUid)) {
                        return b2;
                    }
                }
                b2 = (this.isExcessiveBackgroundAnomaly(anomalyInfo) && !this.isPreOApp(packagesForUid) && b);
            }
        }
        return b2;
    }
    
    public boolean shouldHideSipper(final BatterySipper batterySipper) {
        final BatterySipper$DrainType drainType = batterySipper.drainType;
        return drainType == BatterySipper$DrainType.IDLE || drainType == BatterySipper$DrainType.CELL || drainType == BatterySipper$DrainType.SCREEN || drainType == BatterySipper$DrainType.UNACCOUNTED || drainType == BatterySipper$DrainType.OVERCOUNTED || drainType == BatterySipper$DrainType.BLUETOOTH || drainType == BatterySipper$DrainType.WIFI || batterySipper.totalPowerMah * 3600.0 < 5.0 || this.mPowerUsageFeatureProvider.isTypeService(batterySipper) || this.mPowerUsageFeatureProvider.isTypeSystem(batterySipper);
    }
    
    void smearScreenBatterySipper(final List<BatterySipper> list, BatterySipper batterySipper) {
        long n = 0L;
        final SparseLongArray sparseLongArray = new SparseLongArray();
        for (int i = 0; i < list.size(); ++i) {
            final BatteryStats$Uid uidObj = list.get(i).uidObj;
            if (uidObj != null) {
                final long processTimeMs = this.getProcessTimeMs(0, uidObj, 0);
                sparseLongArray.put(uidObj.getUid(), processTimeMs);
                n += processTimeMs;
            }
        }
        if (n >= 600000L) {
            if (batterySipper == null) {
                Log.e("BatteryUtils", "screen sipper is null even when app screen time is not zero");
                return;
            }
            final double totalPowerMah = batterySipper.totalPowerMah;
            for (int j = 0; j < list.size(); ++j) {
                batterySipper = list.get(j);
                batterySipper.totalPowerMah += sparseLongArray.get(batterySipper.getUid(), 0L) * totalPowerMah / n;
            }
        }
    }
    
    public void sortUsageList(final List<BatterySipper> list) {
        Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<BatterySipper>() {
            @Override
            public int compare(final BatterySipper batterySipper, final BatterySipper batterySipper2) {
                return Double.compare(batterySipper2.totalPowerMah, batterySipper.totalPowerMah);
            }
        });
    }
}
