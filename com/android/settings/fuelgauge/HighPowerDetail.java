package com.android.settings.fuelgauge;

import android.widget.TextView;
import android.content.Intent;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager;
import android.view.View;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.applications.ApplicationsState;
import android.content.Context;
import android.widget.Checkable;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import android.view.View.OnClickListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class HighPowerDetail extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, View.OnClickListener
{
    @VisibleForTesting
    PowerWhitelistBackend mBackend;
    @VisibleForTesting
    BatteryUtils mBatteryUtils;
    private boolean mDefaultOn;
    @VisibleForTesting
    boolean mIsEnabled;
    private CharSequence mLabel;
    private Checkable mOptionOff;
    private Checkable mOptionOn;
    @VisibleForTesting
    String mPackageName;
    @VisibleForTesting
    int mPackageUid;
    
    public static CharSequence getSummary(final Context context, final ApplicationsState.AppEntry appEntry) {
        return getSummary(context, appEntry.info.packageName);
    }
    
    public static CharSequence getSummary(final Context context, final String s) {
        final PowerWhitelistBackend instance = PowerWhitelistBackend.getInstance(context);
        int n;
        if (instance.isSysWhitelisted(s)) {
            n = 2131887851;
        }
        else if (instance.isWhitelisted(s)) {
            n = 2131887848;
        }
        else {
            n = 2131887847;
        }
        return context.getString(n);
    }
    
    @VisibleForTesting
    static void logSpecialPermissionChange(final boolean b, final String s, final Context context) {
        int n;
        if (b) {
            n = 765;
        }
        else {
            n = 764;
        }
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().action(context, n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    public static void show(final Fragment fragment, final int n, final String s, final int n2) {
        final HighPowerDetail highPowerDetail = new HighPowerDetail();
        final Bundle arguments = new Bundle();
        arguments.putString("package", s);
        arguments.putInt("uid", n);
        highPowerDetail.setArguments(arguments);
        highPowerDetail.setTargetFragment(fragment, n2);
        highPowerDetail.show(fragment.getFragmentManager(), HighPowerDetail.class.getSimpleName());
    }
    
    private void updateViews() {
        this.mOptionOn.setChecked(this.mIsEnabled);
        this.mOptionOff.setChecked(this.mIsEnabled ^ true);
    }
    
    public int getMetricsCategory() {
        return 540;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (n == -1) {
            final boolean mIsEnabled = this.mIsEnabled;
            if (mIsEnabled != this.mBackend.isWhitelisted(this.mPackageName)) {
                logSpecialPermissionChange(mIsEnabled, this.mPackageName, this.getContext());
                if (mIsEnabled) {
                    this.mBatteryUtils.setForceAppStandby(this.mPackageUid, this.mPackageName, 0);
                    this.mBackend.addApp(this.mPackageName);
                }
                else {
                    this.mBackend.removeApp(this.mPackageName);
                }
            }
        }
    }
    
    public void onClick(final View view) {
        if (view == this.mOptionOn) {
            this.mIsEnabled = true;
            this.updateViews();
        }
        else if (view == this.mOptionOff) {
            this.mIsEnabled = false;
            this.updateViews();
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        this.mBatteryUtils = BatteryUtils.getInstance(context);
        this.mBackend = PowerWhitelistBackend.getInstance(context);
        this.mPackageName = this.getArguments().getString("package");
        this.mPackageUid = this.getArguments().getInt("uid");
        final PackageManager packageManager = context.getPackageManager();
        boolean mIsEnabled = false;
        try {
            this.mLabel = packageManager.getApplicationInfo(this.mPackageName, 0).loadLabel(packageManager);
        }
        catch (PackageManager$NameNotFoundException ex) {
            this.mLabel = this.mPackageName;
        }
        this.mDefaultOn = this.getArguments().getBoolean("default_on");
        if (this.mDefaultOn || this.mBackend.isWhitelisted(this.mPackageName)) {
            mIsEnabled = true;
        }
        this.mIsEnabled = mIsEnabled;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final AlertDialog$Builder setView = new AlertDialog$Builder(this.getContext()).setTitle(this.mLabel).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setView(2131558593);
        if (!this.mBackend.isSysWhitelisted(this.mPackageName)) {
            setView.setPositiveButton(2131887493, (DialogInterface$OnClickListener)this);
        }
        return (Dialog)setView.create();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        final Fragment targetFragment = this.getTargetFragment();
        if (targetFragment != null && targetFragment.getActivity() != null) {
            targetFragment.onActivityResult(this.getTargetRequestCode(), 0, (Intent)null);
        }
    }
    
    public void onStart() {
        super.onStart();
        this.mOptionOn = this.setup(this.getDialog().findViewById(2131362246), true);
        this.mOptionOff = this.setup(this.getDialog().findViewById(2131362245), false);
        this.updateViews();
    }
    
    public Checkable setup(final View view, final boolean b) {
        final TextView textView = (TextView)view.findViewById(16908310);
        int text;
        if (b) {
            text = 2131887858;
        }
        else {
            text = 2131887856;
        }
        textView.setText(text);
        final TextView textView2 = (TextView)view.findViewById(16908304);
        int text2;
        if (b) {
            text2 = 2131887859;
        }
        else {
            text2 = 2131887857;
        }
        textView2.setText(text2);
        view.setClickable(true);
        view.setOnClickListener((View.OnClickListener)this);
        if (!b && this.mBackend.isSysWhitelisted(this.mPackageName)) {
            view.setEnabled(false);
        }
        return (Checkable)view;
    }
}
