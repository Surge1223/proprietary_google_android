package com.android.settings.fuelgauge;

import com.android.settingslib.utils.StringUtil;
import android.graphics.drawable.Drawable;
import android.os.BatteryStats;
import com.android.internal.os.PowerProfile;
import android.app.Activity;
import android.util.Log;
import android.support.v7.preference.PreferenceScreen;
import java.util.Iterator;
import android.os.BatteryStats$Uid;
import java.util.ArrayList;
import com.android.internal.os.BatterySipper;
import android.text.TextUtils;
import com.android.internal.os.BatterySipper;
import android.os.UserHandle;
import android.os.Message;
import android.os.Looper;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import android.util.ArrayMap;
import android.content.Context;
import android.os.Handler;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.internal.os.BatteryStatsHelper;
import android.support.v7.preference.PreferenceGroup;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import android.util.SparseArray;
import com.android.settings.SettingsActivity;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class BatteryAppListPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnDestroy, OnPause
{
    static final boolean USE_FAKE_DATA = false;
    private SettingsActivity mActivity;
    SparseArray<List<Anomaly>> mAnomalySparseArray;
    PreferenceGroup mAppListGroup;
    private BatteryStatsHelper mBatteryStatsHelper;
    BatteryUtils mBatteryUtils;
    private InstrumentedPreferenceFragment mFragment;
    private Handler mHandler;
    private Context mPrefContext;
    private ArrayMap<String, Preference> mPreferenceCache;
    private final String mPreferenceKey;
    private UserManager mUserManager;
    
    public BatteryAppListPreferenceController(final Context context, final String mPreferenceKey, final Lifecycle lifecycle, final SettingsActivity mActivity, final InstrumentedPreferenceFragment mFragment) {
        super(context);
        this.mHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case 2: {
                        final SettingsActivity access$100 = BatteryAppListPreferenceController.this.mActivity;
                        if (access$100 != null) {
                            access$100.reportFullyDrawn();
                            break;
                        }
                        break;
                    }
                    case 1: {
                        final BatteryEntry batteryEntry = (BatteryEntry)message.obj;
                        final PowerGaugePreference powerGaugePreference = (PowerGaugePreference)BatteryAppListPreferenceController.this.mAppListGroup.findPreference(Integer.toString(batteryEntry.sipper.uidObj.getUid()));
                        if (powerGaugePreference == null) {
                            break;
                        }
                        powerGaugePreference.setIcon(BatteryAppListPreferenceController.this.mUserManager.getBadgedIconForUser(batteryEntry.getIcon(), new UserHandle(UserHandle.getUserId(batteryEntry.sipper.getUid()))));
                        powerGaugePreference.setTitle(batteryEntry.name);
                        if (batteryEntry.sipper.drainType == BatterySipper$DrainType.APP) {
                            powerGaugePreference.setContentDescription(batteryEntry.name);
                            break;
                        }
                        break;
                    }
                }
                super.handleMessage(message);
            }
        };
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
        this.mPreferenceKey = mPreferenceKey;
        this.mBatteryUtils = BatteryUtils.getInstance(context);
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mActivity = mActivity;
        this.mFragment = mFragment;
    }
    
    private void addNotAvailableMessage() {
        if (this.getCachedPreference("not_available") == null) {
            final Preference preference = new Preference(this.mPrefContext);
            preference.setKey("not_available");
            preference.setTitle(2131888614);
            preference.setSelectable(false);
            this.mAppListGroup.addPreference(preference);
        }
    }
    
    private void cacheRemoveAllPrefs(final PreferenceGroup preferenceGroup) {
        this.mPreferenceCache = (ArrayMap<String, Preference>)new ArrayMap();
        for (int preferenceCount = preferenceGroup.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (!TextUtils.isEmpty((CharSequence)preference.getKey())) {
                this.mPreferenceCache.put((Object)preference.getKey(), (Object)preference);
            }
        }
    }
    
    private int getCachedCount() {
        int size;
        if (this.mPreferenceCache != null) {
            size = this.mPreferenceCache.size();
        }
        else {
            size = 0;
        }
        return size;
    }
    
    private Preference getCachedPreference(final String s) {
        Preference preference;
        if (this.mPreferenceCache != null) {
            preference = (Preference)this.mPreferenceCache.remove((Object)s);
        }
        else {
            preference = null;
        }
        return preference;
    }
    
    private List<BatterySipper> getCoalescedUsageList(final List<BatterySipper> list) {
        final SparseArray sparseArray = new SparseArray();
        final ArrayList<BatterySipper> list2 = new ArrayList<BatterySipper>();
        final int size = list.size();
        final int n = 0;
        for (int i = 0; i < size; ++i) {
            final BatterySipper batterySipper = list.get(i);
            if (batterySipper.getUid() > 0) {
                int n2 = batterySipper.getUid();
                if (isSharedGid(batterySipper.getUid())) {
                    n2 = UserHandle.getUid(0, UserHandle.getAppIdFromSharedAppGid(batterySipper.getUid()));
                }
                int n3 = n2;
                if (isSystemUid(n2)) {
                    n3 = n2;
                    if (!"mediaserver".equals(batterySipper.packageWithHighestDrain)) {
                        n3 = 1000;
                    }
                }
                BatterySipper batterySipper2 = batterySipper;
                if (n3 != batterySipper.getUid()) {
                    batterySipper2 = new BatterySipper(batterySipper.drainType, (BatteryStats$Uid)new FakeUid(n3), 0.0);
                    batterySipper2.add(batterySipper);
                    batterySipper2.packageWithHighestDrain = batterySipper.packageWithHighestDrain;
                    batterySipper2.mPackages = batterySipper.mPackages;
                }
                final int indexOfKey = sparseArray.indexOfKey(n3);
                if (indexOfKey < 0) {
                    sparseArray.put(n3, (Object)batterySipper2);
                }
                else {
                    final BatterySipper batterySipper3 = (BatterySipper)sparseArray.valueAt(indexOfKey);
                    batterySipper3.add(batterySipper2);
                    if (batterySipper3.packageWithHighestDrain == null && batterySipper2.packageWithHighestDrain != null) {
                        batterySipper3.packageWithHighestDrain = batterySipper2.packageWithHighestDrain;
                    }
                    int length;
                    if (batterySipper3.mPackages != null) {
                        length = batterySipper3.mPackages.length;
                    }
                    else {
                        length = 0;
                    }
                    int length2;
                    if (batterySipper2.mPackages != null) {
                        length2 = batterySipper2.mPackages.length;
                    }
                    else {
                        length2 = 0;
                    }
                    if (length2 > 0) {
                        final String[] mPackages = new String[length + length2];
                        if (length > 0) {
                            System.arraycopy(batterySipper3.mPackages, 0, mPackages, 0, length);
                        }
                        System.arraycopy(batterySipper2.mPackages, 0, mPackages, length, length2);
                        batterySipper3.mPackages = mPackages;
                    }
                }
            }
            else {
                list2.add(batterySipper);
            }
        }
        for (int size2 = sparseArray.size(), j = n; j < size2; ++j) {
            list2.add((BatterySipper)sparseArray.valueAt(j));
        }
        this.mBatteryUtils.sortUsageList(list2);
        return list2;
    }
    
    private static boolean isSharedGid(final int n) {
        return UserHandle.getAppIdFromSharedAppGid(n) > 0;
    }
    
    private static boolean isSystemUid(int appId) {
        appId = UserHandle.getAppId(appId);
        return appId >= 1000 && appId < 10000;
    }
    
    private void removeCachedPrefs(final PreferenceGroup preferenceGroup) {
        final Iterator<Preference> iterator = this.mPreferenceCache.values().iterator();
        while (iterator.hasNext()) {
            preferenceGroup.removePreference(iterator.next());
        }
        this.mPreferenceCache = null;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPrefContext = preferenceScreen.getContext();
        this.mAppListGroup = (PreferenceGroup)preferenceScreen.findPreference(this.mPreferenceKey);
    }
    
    String extractKeyFromSipper(final BatterySipper batterySipper) {
        if (batterySipper.uidObj != null) {
            return this.extractKeyFromUid(batterySipper.getUid());
        }
        if (batterySipper.drainType == BatterySipper$DrainType.USER) {
            final StringBuilder sb = new StringBuilder();
            sb.append(batterySipper.drainType.toString());
            sb.append(batterySipper.userId);
            return sb.toString();
        }
        if (batterySipper.drainType != BatterySipper$DrainType.APP) {
            return batterySipper.drainType.toString();
        }
        if (batterySipper.getPackages() != null) {
            return TextUtils.concat((CharSequence[])batterySipper.getPackages()).toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Inappropriate BatterySipper without uid and package names: ");
        sb2.append(batterySipper);
        Log.w("PrefControllerMixin", sb2.toString());
        return "-1";
    }
    
    String extractKeyFromUid(final int n) {
        return Integer.toString(n);
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPreferenceKey;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (preference instanceof PowerGaugePreference) {
            final PowerGaugePreference powerGaugePreference = (PowerGaugePreference)preference;
            final BatteryEntry info = powerGaugePreference.getInfo();
            final SettingsActivity mActivity = this.mActivity;
            final InstrumentedPreferenceFragment mFragment = this.mFragment;
            final BatteryStatsHelper mBatteryStatsHelper = this.mBatteryStatsHelper;
            final String percent = powerGaugePreference.getPercent();
            List<Anomaly> list;
            if (this.mAnomalySparseArray != null) {
                list = (List<Anomaly>)this.mAnomalySparseArray.get(info.sipper.getUid());
            }
            else {
                list = null;
            }
            AdvancedPowerUsageDetail.startBatteryDetailPage(mActivity, mFragment, mBatteryStatsHelper, 0, info, percent, list);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onDestroy() {
        if (this.mActivity.isChangingConfigurations()) {
            BatteryEntry.clearUidCache();
        }
    }
    
    @Override
    public void onPause() {
        BatteryEntry.stopRequestQueue();
        this.mHandler.removeMessages(1);
    }
    
    public void refreshAppListGroup(final BatteryStatsHelper mBatteryStatsHelper, final boolean b) {
        if (!this.isAvailable()) {
            return;
        }
        this.mBatteryStatsHelper = mBatteryStatsHelper;
        this.mAppListGroup.setTitle(2131888613);
        final PowerProfile powerProfile = mBatteryStatsHelper.getPowerProfile();
        final BatteryStats stats = mBatteryStatsHelper.getStats();
        final double averagePower = powerProfile.getAveragePower("screen.full");
        int dischargeAmount;
        if (stats != null) {
            dischargeAmount = stats.getDischargeAmount(0);
        }
        else {
            dischargeAmount = 0;
        }
        this.cacheRemoveAllPrefs(this.mAppListGroup);
        this.mAppListGroup.setOrderingAsAdded(false);
        boolean b2;
        if (averagePower < 10.0) {
            b2 = false;
        }
        else {
            final List<BatterySipper> coalescedUsageList = this.getCoalescedUsageList(mBatteryStatsHelper.getUsageList());
            double removeHiddenBatterySippers;
            if (b) {
                removeHiddenBatterySippers = 0.0;
            }
            else {
                removeHiddenBatterySippers = this.mBatteryUtils.removeHiddenBatterySippers(coalescedUsageList);
            }
            this.mBatteryUtils.sortUsageList(coalescedUsageList);
            final int size = coalescedUsageList.size();
            b2 = false;
            for (int i = 0; i < size; ++i) {
                final BatterySipper batterySipper = coalescedUsageList.get(i);
                final double calculateBatteryPercent = this.mBatteryUtils.calculateBatteryPercent(batterySipper.totalPowerMah, mBatteryStatsHelper.getTotalPower(), removeHiddenBatterySippers, dischargeAmount);
                if ((int)(0.5 + calculateBatteryPercent) >= 1) {
                    if (!this.shouldHideSipper(batterySipper)) {
                        final UserHandle userHandle = new UserHandle(UserHandle.getUserId(batterySipper.getUid()));
                        final BatteryEntry batteryEntry = new BatteryEntry((Context)this.mActivity, this.mHandler, this.mUserManager, batterySipper);
                        final Drawable badgedIconForUser = this.mUserManager.getBadgedIconForUser(batteryEntry.getIcon(), userHandle);
                        final CharSequence badgedLabelForUser = this.mUserManager.getBadgedLabelForUser((CharSequence)batteryEntry.getLabel(), userHandle);
                        final String keyFromSipper = this.extractKeyFromSipper(batterySipper);
                        PowerGaugePreference powerGaugePreference = (PowerGaugePreference)this.getCachedPreference(keyFromSipper);
                        if (powerGaugePreference == null) {
                            powerGaugePreference = new PowerGaugePreference(this.mPrefContext, badgedIconForUser, badgedLabelForUser, batteryEntry);
                            powerGaugePreference.setKey(keyFromSipper);
                        }
                        batterySipper.percent = calculateBatteryPercent;
                        powerGaugePreference.setTitle(batteryEntry.getLabel());
                        powerGaugePreference.setOrder(i + 1);
                        powerGaugePreference.setPercent(calculateBatteryPercent);
                        powerGaugePreference.shouldShowAnomalyIcon(false);
                        if (batterySipper.usageTimeMs == 0L && batterySipper.drainType == BatterySipper$DrainType.APP) {
                            batterySipper.usageTimeMs = this.mBatteryUtils.getProcessTimeMs(1, batterySipper.uidObj, 0);
                        }
                        this.setUsageSummary(powerGaugePreference, batterySipper);
                        this.mAppListGroup.addPreference(powerGaugePreference);
                        if (this.mAppListGroup.getPreferenceCount() - this.getCachedCount() > 11) {
                            b2 = true;
                            break;
                        }
                        b2 = true;
                    }
                }
            }
        }
        if (!b2) {
            this.addNotAvailableMessage();
        }
        this.removeCachedPrefs(this.mAppListGroup);
        BatteryEntry.startRequestQueue();
    }
    
    void setUsageSummary(final Preference preference, final BatterySipper batterySipper) {
        final long usageTimeMs = batterySipper.usageTimeMs;
        if (usageTimeMs >= 60000L) {
            final CharSequence formatElapsedTime = StringUtil.formatElapsedTime(this.mContext, usageTimeMs, false);
            CharSequence expandTemplate;
            if (batterySipper.drainType == BatterySipper$DrainType.APP && !this.mBatteryUtils.shouldHideSipper(batterySipper)) {
                expandTemplate = TextUtils.expandTemplate(this.mContext.getText(2131886679), new CharSequence[] { formatElapsedTime });
            }
            else {
                expandTemplate = formatElapsedTime;
            }
            preference.setSummary(expandTemplate);
        }
    }
    
    boolean shouldHideSipper(final BatterySipper batterySipper) {
        return batterySipper.drainType == BatterySipper$DrainType.OVERCOUNTED || batterySipper.drainType == BatterySipper$DrainType.UNACCOUNTED;
    }
}
