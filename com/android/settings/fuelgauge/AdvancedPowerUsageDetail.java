package com.android.settings.fuelgauge;

import com.android.settings.fuelgauge.anomaly.AnomalyUtils;
import com.android.settings.fuelgauge.anomaly.AnomalyLoader;
import android.content.Loader;
import com.android.settings.fuelgauge.batterytip.tips.BatteryTip;
import android.content.Intent;
import android.text.TextUtils;
import com.android.settingslib.utils.StringUtil;
import android.view.View;
import com.android.settingslib.applications.AppUtils;
import com.android.settings.widget.EntityHeaderController;
import android.app.Fragment;
import com.android.settings.SettingsActivity;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import android.os.BatteryStats$Uid;
import com.android.internal.util.ArrayUtils;
import com.android.settings.core.SubSettingLauncher;
import android.content.pm.PackageManager;
import android.util.Log;
import com.android.settingslib.Utils;
import android.os.Bundle;
import android.content.Context;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.core.InstrumentedPreferenceFragment;
import android.app.Activity;
import android.os.UserHandle;
import android.app.ActivityManager;
import com.android.internal.os.BatterySipper;
import com.android.internal.os.BatterySipper;
import android.os.UserManager;
import android.content.pm.PackageManager;
import com.android.settings.applications.LayoutPreference;
import android.app.admin.DevicePolicyManager;
import android.support.v7.preference.Preference;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.fuelgauge.anomaly.AnomalySummaryPreferenceController;
import com.android.settings.fuelgauge.batterytip.BatteryTipPreferenceController;
import com.android.settings.fuelgauge.anomaly.AnomalyDialogFragment;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.dashboard.DashboardFragment;

public class AdvancedPowerUsageDetail extends DashboardFragment implements LoaderManager$LoaderCallbacks<List<Anomaly>>, AppButtonsDialogListener, AnomalyDialogListener, BatteryTipListener
{
    private List<Anomaly> mAnomalies;
    AnomalySummaryPreferenceController mAnomalySummaryPreferenceController;
    private AppButtonsPreferenceController mAppButtonsPreferenceController;
    ApplicationsState.AppEntry mAppEntry;
    private BackgroundActivityPreferenceController mBackgroundActivityPreferenceController;
    Preference mBackgroundPreference;
    BatteryUtils mBatteryUtils;
    private DevicePolicyManager mDpm;
    Preference mForegroundPreference;
    LayoutPreference mHeaderPreference;
    private PackageManager mPackageManager;
    private String mPackageName;
    ApplicationsState mState;
    private UserManager mUserManager;
    
    private static int getUserIdToLaunchAdvancePowerUsageDetail(final BatterySipper batterySipper) {
        if (batterySipper.drainType == BatterySipper$DrainType.USER) {
            return ActivityManager.getCurrentUser();
        }
        return UserHandle.getUserId(batterySipper.getUid());
    }
    
    public static void startBatteryDetailPage(final Activity activity, final InstrumentedPreferenceFragment instrumentedPreferenceFragment, final BatteryStatsHelper batteryStatsHelper, final int n, final BatteryEntry batteryEntry, final String s, final List<Anomaly> list) {
        startBatteryDetailPage(activity, BatteryUtils.getInstance((Context)activity), instrumentedPreferenceFragment, batteryStatsHelper, n, batteryEntry, s, list);
    }
    
    public static void startBatteryDetailPage(final Activity activity, final InstrumentedPreferenceFragment instrumentedPreferenceFragment, final String s) {
        final Bundle arguments = new Bundle(3);
        final PackageManager packageManager = activity.getPackageManager();
        arguments.putString("extra_package_name", s);
        arguments.putString("extra_power_usage_percent", Utils.formatPercentage(0));
        try {
            arguments.putInt("extra_uid", packageManager.getPackageUid(s, 0));
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find package: ");
            sb.append(s);
            Log.e("AdvancedPowerDetail", sb.toString(), (Throwable)ex);
        }
        new SubSettingLauncher((Context)activity).setDestination(AdvancedPowerUsageDetail.class.getName()).setTitle(2131886598).setArguments(arguments).setSourceMetricsCategory(instrumentedPreferenceFragment.getMetricsCategory()).launch();
    }
    
    static void startBatteryDetailPage(final Activity activity, final BatteryUtils batteryUtils, final InstrumentedPreferenceFragment instrumentedPreferenceFragment, final BatteryStatsHelper batteryStatsHelper, final int n, final BatteryEntry batteryEntry, final String s, final List<Anomaly> list) {
        batteryStatsHelper.getStats();
        final Bundle arguments = new Bundle();
        final BatterySipper sipper = batteryEntry.sipper;
        final BatteryStats$Uid uidObj = sipper.uidObj;
        final boolean b = sipper.drainType == BatterySipper$DrainType.APP;
        long n2;
        if (b) {
            n2 = batteryUtils.getProcessTimeMs(1, uidObj, n);
        }
        else {
            n2 = sipper.usageTimeMs;
        }
        long processTimeMs;
        if (b) {
            processTimeMs = batteryUtils.getProcessTimeMs(2, uidObj, n);
        }
        else {
            processTimeMs = 0L;
        }
        if (ArrayUtils.isEmpty((Object[])sipper.mPackages)) {
            arguments.putString("extra_label", batteryEntry.getLabel());
            arguments.putInt("extra_icon_id", batteryEntry.iconId);
            arguments.putString("extra_package_name", (String)null);
        }
        else {
            String defaultPackageName;
            if (batteryEntry.defaultPackageName != null) {
                defaultPackageName = batteryEntry.defaultPackageName;
            }
            else {
                defaultPackageName = sipper.mPackages[0];
            }
            arguments.putString("extra_package_name", defaultPackageName);
        }
        arguments.putInt("extra_uid", sipper.getUid());
        arguments.putLong("extra_background_time", processTimeMs);
        arguments.putLong("extra_foreground_time", n2);
        arguments.putString("extra_power_usage_percent", s);
        arguments.putInt("extra_power_usage_amount", (int)sipper.totalPowerMah);
        arguments.putParcelableList("extra_anomaly_list", (List)list);
        new SubSettingLauncher((Context)activity).setDestination(AdvancedPowerUsageDetail.class.getName()).setTitle(2131886598).setArguments(arguments).setSourceMetricsCategory(instrumentedPreferenceFragment.getMetricsCategory()).setUserHandle(new UserHandle(getUserIdToLaunchAdvancePowerUsageDetail(sipper))).launch();
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<BatteryOptimizationPreferenceController> list = (ArrayList<BatteryOptimizationPreferenceController>)new ArrayList<AppButtonsPreferenceController>();
        final Bundle arguments = this.getArguments();
        final int int1 = arguments.getInt("extra_uid", 0);
        final String string = arguments.getString("extra_package_name");
        list.add((AppButtonsPreferenceController)(this.mBackgroundActivityPreferenceController = new BackgroundActivityPreferenceController(context, this, int1, string)));
        list.add((AppButtonsPreferenceController)new BatteryOptimizationPreferenceController((SettingsActivity)this.getActivity(), this, string));
        list.add(this.mAppButtonsPreferenceController = new AppButtonsPreferenceController((SettingsActivity)this.getActivity(), this, this.getLifecycle(), string, this.mState, this.mDpm, this.mUserManager, this.mPackageManager, 0, 1));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected String getLogTag() {
        return "AdvancedPowerDetail";
    }
    
    public int getMetricsCategory() {
        return 53;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082808;
    }
    
    public void handleDialogClick(final int n) {
        if (this.mAppButtonsPreferenceController != null) {
            this.mAppButtonsPreferenceController.handleDialogClick(n);
        }
    }
    
    void initAnomalyInfo() {
        this.mAnomalies = (List<Anomaly>)this.getArguments().getParcelableArrayList("extra_anomaly_list");
        if (this.mAnomalies == null) {
            this.getLoaderManager().initLoader(0, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
        }
        else if (this.mAnomalies != null) {
            this.mAnomalySummaryPreferenceController.updateAnomalySummaryPreference(this.mAnomalies);
        }
    }
    
    void initHeader() {
        final View viewById = this.mHeaderPreference.findViewById(2131362112);
        final Activity activity = this.getActivity();
        final Bundle arguments = this.getArguments();
        final EntityHeaderController setButtonActions = EntityHeaderController.newInstance(activity, this, viewById).setRecyclerView(this.getListView(), this.getLifecycle()).setButtonActions(0, 0);
        if (this.mAppEntry == null) {
            setButtonActions.setLabel(arguments.getString("extra_label"));
            if (arguments.getInt("extra_icon_id", 0) == 0) {
                setButtonActions.setIcon(activity.getPackageManager().getDefaultActivityIcon());
            }
            else {
                setButtonActions.setIcon(activity.getDrawable(arguments.getInt("extra_icon_id")));
            }
        }
        else {
            this.mState.ensureIcon(this.mAppEntry);
            setButtonActions.setLabel(this.mAppEntry);
            setButtonActions.setIcon(this.mAppEntry);
            CharSequence string;
            if (AppUtils.isInstant(this.mAppEntry.info)) {
                string = null;
            }
            else {
                string = this.getString(com.android.settings.Utils.getInstallationStatus(this.mAppEntry.info));
            }
            setButtonActions.setIsInstantApp(AppUtils.isInstant(this.mAppEntry.info));
            setButtonActions.setSummary(string);
        }
        setButtonActions.done(activity, true);
    }
    
    void initPreference() {
        final Bundle arguments = this.getArguments();
        final Context context = this.getContext();
        final long long1 = arguments.getLong("extra_foreground_time");
        final long long2 = arguments.getLong("extra_background_time");
        arguments.getString("extra_power_usage_percent");
        arguments.getInt("extra_power_usage_amount");
        this.mForegroundPreference.setSummary(TextUtils.expandTemplate(this.getText(2131886679), new CharSequence[] { StringUtil.formatElapsedTime(context, long1, false) }));
        this.mBackgroundPreference.setSummary(TextUtils.expandTemplate(this.getText(2131886577), new CharSequence[] { StringUtil.formatElapsedTime(context, long2, false) }));
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (this.mAppButtonsPreferenceController != null) {
            this.mAppButtonsPreferenceController.handleActivityResult(n, n2, intent);
        }
    }
    
    public void onAnomalyHandled(final Anomaly anomaly) {
        this.mAnomalySummaryPreferenceController.hideHighUsagePreference();
    }
    
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        this.mState = ApplicationsState.getInstance(this.getActivity().getApplication());
        this.mDpm = (DevicePolicyManager)activity.getSystemService("device_policy");
        this.mUserManager = (UserManager)activity.getSystemService("user");
        this.mPackageManager = activity.getPackageManager();
        this.mBatteryUtils = BatteryUtils.getInstance(this.getContext());
    }
    
    public void onBatteryTipHandled(final BatteryTip batteryTip) {
        this.mBackgroundActivityPreferenceController.updateSummary(this.findPreference(this.mBackgroundActivityPreferenceController.getPreferenceKey()));
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mPackageName = this.getArguments().getString("extra_package_name");
        this.mAnomalySummaryPreferenceController = new AnomalySummaryPreferenceController((SettingsActivity)this.getActivity(), this);
        this.mForegroundPreference = this.findPreference("app_usage_foreground");
        this.mBackgroundPreference = this.findPreference("app_usage_background");
        this.mHeaderPreference = (LayoutPreference)this.findPreference("header_view");
        if (this.mPackageName != null) {
            this.mAppEntry = this.mState.getEntry(this.mPackageName, UserHandle.myUserId());
            this.initAnomalyInfo();
        }
    }
    
    public Loader<List<Anomaly>> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<List<Anomaly>>)new AnomalyLoader(this.getContext(), this.mPackageName);
    }
    
    public void onLoadFinished(final Loader<List<Anomaly>> loader, final List<Anomaly> list) {
        AnomalyUtils.getInstance(this.getContext()).logAnomalies(this.mMetricsFeatureProvider, list, 53);
        this.mAnomalySummaryPreferenceController.updateAnomalySummaryPreference(list);
    }
    
    public void onLoaderReset(final Loader<List<Anomaly>> loader) {
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"high_usage")) {
            this.mAnomalySummaryPreferenceController.onPreferenceTreeClick(preference);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.initHeader();
        this.initPreference();
    }
}
