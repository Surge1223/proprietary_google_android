package com.android.settings.fuelgauge;

import android.util.SparseIntArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;

public class BatteryActiveView extends View
{
    private final Paint mPaint;
    private BatteryActiveProvider mProvider;
    
    public BatteryActiveView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPaint = new Paint();
    }
    
    private void drawColor(final Canvas canvas, final int n, final int n2, final int color, final float n3) {
        if (color == 0) {
            return;
        }
        this.mPaint.setColor(color);
        canvas.drawRect(n / n3 * this.getWidth(), 0.0f, n2 / n3 * this.getWidth(), (float)this.getHeight(), this.mPaint);
    }
    
    protected void onDraw(final Canvas canvas) {
        if (this.mProvider == null) {
            return;
        }
        final SparseIntArray colorArray = this.mProvider.getColorArray();
        final float n = this.mProvider.getPeriod();
        for (int i = 0; i < colorArray.size() - 1; ++i) {
            this.drawColor(canvas, colorArray.keyAt(i), colorArray.keyAt(i + 1), colorArray.valueAt(i), n);
        }
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        if (this.getWidth() != 0) {
            this.postInvalidate();
        }
    }
    
    public void setProvider(final BatteryActiveProvider mProvider) {
        this.mProvider = mProvider;
        if (this.getWidth() != 0) {
            this.postInvalidate();
        }
    }
    
    public interface BatteryActiveProvider
    {
        SparseIntArray getColorArray();
        
        long getPeriod();
        
        boolean hasData();
    }
}
