package com.android.settings.fuelgauge;

import com.android.settingslib.graph.BatteryMeterDrawableBase;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff.Mode;
import com.android.settingslib.Utils;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.ColorFilter;
import android.widget.ImageView;

public class BatteryMeterView extends ImageView
{
    ColorFilter mAccentColorFilter;
    BatteryMeterDrawable mDrawable;
    ColorFilter mErrorColorFilter;
    
    public BatteryMeterView(final Context context) {
        this(context, null, 0);
    }
    
    public BatteryMeterView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public BatteryMeterView(final Context context, final AttributeSet set, int color) {
        super(context, set, color);
        color = context.getColor(R.color.meter_background_color);
        this.mAccentColorFilter = (ColorFilter)new PorterDuffColorFilter(Utils.getColorAttr(context, 16843829), PorterDuff.Mode.SRC_IN);
        this.mErrorColorFilter = (ColorFilter)new PorterDuffColorFilter(context.getColor(2131099683), PorterDuff.Mode.SRC_IN);
        (this.mDrawable = new BatteryMeterDrawable(context, color)).setShowPercent(false);
        this.mDrawable.setBatteryColorFilter(this.mAccentColorFilter);
        this.mDrawable.setWarningColorFilter((ColorFilter)new PorterDuffColorFilter(-1, PorterDuff.Mode.SRC_IN));
        this.setImageDrawable((Drawable)this.mDrawable);
    }
    
    public int getBatteryLevel() {
        return this.mDrawable.getBatteryLevel();
    }
    
    public boolean getCharging() {
        return this.mDrawable.getCharging();
    }
    
    public void setBatteryLevel(final int batteryLevel) {
        this.mDrawable.setBatteryLevel(batteryLevel);
        if (batteryLevel < this.mDrawable.getCriticalLevel()) {
            this.mDrawable.setBatteryColorFilter(this.mErrorColorFilter);
        }
        else {
            this.mDrawable.setBatteryColorFilter(this.mAccentColorFilter);
        }
    }
    
    public void setCharging(final boolean charging) {
        this.mDrawable.setCharging(charging);
        this.postInvalidate();
    }
    
    public static class BatteryMeterDrawable extends BatteryMeterDrawableBase
    {
        private final int mIntrinsicHeight;
        private final int mIntrinsicWidth;
        
        public BatteryMeterDrawable(final Context context, final int n) {
            super(context, n);
            this.mIntrinsicWidth = context.getResources().getDimensionPixelSize(2131165312);
            this.mIntrinsicHeight = context.getResources().getDimensionPixelSize(2131165311);
        }
        
        @Override
        public int getIntrinsicHeight() {
            return this.mIntrinsicHeight;
        }
        
        @Override
        public int getIntrinsicWidth() {
            return this.mIntrinsicWidth;
        }
        
        public void setBatteryColorFilter(final ColorFilter colorFilter) {
            this.mFramePaint.setColorFilter(colorFilter);
            this.mBatteryPaint.setColorFilter(colorFilter);
            this.mBoltPaint.setColorFilter(colorFilter);
        }
        
        public void setWarningColorFilter(final ColorFilter colorFilter) {
            this.mWarningTextPaint.setColorFilter(colorFilter);
        }
    }
}
