package com.android.settings.fuelgauge;

import android.os.UserHandle;
import android.app.Fragment;
import com.android.settings.fuelgauge.anomaly.AnomalyPreference;
import android.support.v7.preference.Preference;
import com.android.settings.Utils;
import android.graphics.drawable.Drawable;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Context;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import com.android.settings.core.InstrumentedPreferenceFragment;
import com.android.settings.SettingsActivity;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import java.util.List;
import android.support.v7.preference.PreferenceGroup;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settings.fuelgauge.anomaly.AnomalyDialogFragment;
import com.android.settings.dashboard.DashboardFragment;

public class PowerUsageAnomalyDetails extends DashboardFragment implements AnomalyDialogListener
{
    @VisibleForTesting
    static final String EXTRA_ANOMALY_LIST = "anomaly_list";
    @VisibleForTesting
    PreferenceGroup mAbnormalListGroup;
    @VisibleForTesting
    List<Anomaly> mAnomalies;
    @VisibleForTesting
    BatteryUtils mBatteryUtils;
    @VisibleForTesting
    IconDrawableFactory mIconDrawableFactory;
    @VisibleForTesting
    PackageManager mPackageManager;
    
    public static void startBatteryAbnormalPage(final SettingsActivity settingsActivity, final InstrumentedPreferenceFragment instrumentedPreferenceFragment, final List<Anomaly> list) {
        final Bundle arguments = new Bundle();
        arguments.putParcelableList("anomaly_list", (List)list);
        new SubSettingLauncher((Context)settingsActivity).setDestination(PowerUsageAnomalyDetails.class.getName()).setTitle(2131886567).setArguments(arguments).setSourceMetricsCategory(instrumentedPreferenceFragment.getMetricsCategory()).launch();
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return null;
    }
    
    @VisibleForTesting
    Drawable getBadgedIcon(final String s, final int n) {
        return Utils.getBadgedIcon(this.mIconDrawableFactory, this.mPackageManager, s, n);
    }
    
    @Override
    protected String getLogTag() {
        return "PowerAbnormalUsageDetail";
    }
    
    @Override
    public int getMetricsCategory() {
        return 987;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082806;
    }
    
    @Override
    public void onAnomalyHandled(final Anomaly anomaly) {
        this.mAnomalies.remove(anomaly);
        this.refreshUi();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Context context = this.getContext();
        this.mAnomalies = (List<Anomaly>)this.getArguments().getParcelableArrayList("anomaly_list");
        this.mAbnormalListGroup = (PreferenceGroup)this.findPreference("app_abnormal_list");
        this.mPackageManager = context.getPackageManager();
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(context);
        this.mBatteryUtils = BatteryUtils.getInstance(context);
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference instanceof AnomalyPreference) {
            final AnomalyDialogFragment instance = AnomalyDialogFragment.newInstance(((AnomalyPreference)preference).getAnomaly(), 987);
            instance.setTargetFragment((Fragment)this, 0);
            instance.show(this.getFragmentManager(), "PowerAbnormalUsageDetail");
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.refreshUi();
    }
    
    void refreshUi() {
        this.mAbnormalListGroup.removeAll();
        for (int i = 0; i < this.mAnomalies.size(); ++i) {
            final Anomaly anomaly = this.mAnomalies.get(i);
            final AnomalyPreference anomalyPreference = new AnomalyPreference(this.getPrefContext(), anomaly);
            anomalyPreference.setSummary(this.mBatteryUtils.getSummaryResIdFromAnomalyType(anomaly.type));
            final Drawable badgedIcon = this.getBadgedIcon(anomaly.packageName, UserHandle.getUserId(anomaly.uid));
            if (badgedIcon != null) {
                anomalyPreference.setIcon(badgedIcon);
            }
            this.mAbnormalListGroup.addPreference(anomalyPreference);
        }
    }
}
