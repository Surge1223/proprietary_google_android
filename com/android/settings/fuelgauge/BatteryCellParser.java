package com.android.settings.fuelgauge;

import android.os.BatteryStats$HistoryItem;
import com.android.settings.Utils;
import android.util.SparseIntArray;

public class BatteryCellParser implements BatteryActiveProvider, BatteryDataParser
{
    private final SparseIntArray mData;
    private long mLastTime;
    private int mLastValue;
    private long mLength;
    
    public BatteryCellParser() {
        this.mData = new SparseIntArray();
    }
    
    private int getColor(final int n) {
        return Utils.BADNESS_COLORS[n];
    }
    
    @Override
    public SparseIntArray getColorArray() {
        final SparseIntArray sparseIntArray = new SparseIntArray();
        for (int i = 0; i < this.mData.size(); ++i) {
            sparseIntArray.put(this.mData.keyAt(i), this.getColor(this.mData.valueAt(i)));
        }
        return sparseIntArray;
    }
    
    @Override
    public long getPeriod() {
        return this.mLength;
    }
    
    protected int getValue(final BatteryStats$HistoryItem batteryStats$HistoryItem) {
        int n;
        if ((batteryStats$HistoryItem.states & 0x1C0) >> 6 == 3) {
            n = 0;
        }
        else if ((batteryStats$HistoryItem.states & 0x200000) != 0x0) {
            n = 1;
        }
        else {
            n = ((batteryStats$HistoryItem.states & 0x38) >> 3) + 2;
        }
        return n;
    }
    
    @Override
    public boolean hasData() {
        final int size = this.mData.size();
        boolean b = true;
        if (size <= 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void onDataGap() {
        if (this.mLastValue != 0) {
            this.mData.put((int)this.mLastTime, 0);
            this.mLastValue = 0;
        }
    }
    
    @Override
    public void onDataPoint(final long mLastTime, final BatteryStats$HistoryItem batteryStats$HistoryItem) {
        final int value = this.getValue(batteryStats$HistoryItem);
        if (value != this.mLastValue) {
            this.mData.put((int)mLastTime, value);
            this.mLastValue = value;
        }
        this.mLastTime = mLastTime;
    }
    
    @Override
    public void onParsingDone() {
        if (this.mLastValue != 0) {
            this.mData.put((int)this.mLastTime, 0);
            this.mLastValue = 0;
        }
    }
    
    @Override
    public void onParsingStarted(final long n, final long n2) {
        this.mLength = n2 - n;
    }
}
