package com.android.settings.fuelgauge;

import com.android.settingslib.Utils;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.widget.AppPreference;

public class PowerGaugePreference extends AppPreference
{
    private CharSequence mContentDescription;
    private BatteryEntry mInfo;
    private CharSequence mProgress;
    private boolean mShowAnomalyIcon;
    
    public PowerGaugePreference(final Context context) {
        this(context, null, null, null, null);
    }
    
    public PowerGaugePreference(final Context context, final Drawable drawable, final CharSequence charSequence, final BatteryEntry batteryEntry) {
        this(context, null, drawable, charSequence, batteryEntry);
    }
    
    public PowerGaugePreference(final Context context, final AttributeSet set) {
        this(context, set, null, null, null);
    }
    
    private PowerGaugePreference(final Context context, final AttributeSet set, final Drawable drawable, final CharSequence mContentDescription, final BatteryEntry mInfo) {
        super(context, set);
        Object icon;
        if (drawable != null) {
            icon = drawable;
        }
        else {
            icon = new ColorDrawable(0);
        }
        this.setIcon((Drawable)icon);
        this.setWidgetLayoutResource(2131558688);
        this.mInfo = mInfo;
        this.mContentDescription = mContentDescription;
        this.mShowAnomalyIcon = false;
    }
    
    BatteryEntry getInfo() {
        return this.mInfo;
    }
    
    public String getPercent() {
        return this.mProgress.toString();
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(2131362820);
        textView.setText(this.mProgress);
        if (this.mShowAnomalyIcon) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(2131231193, 0, 0, 0);
        }
        else {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        }
        if (this.mContentDescription != null) {
            ((TextView)preferenceViewHolder.findViewById(16908310)).setContentDescription(this.mContentDescription);
        }
    }
    
    public void setContentDescription(final String mContentDescription) {
        this.mContentDescription = mContentDescription;
        this.notifyChanged();
    }
    
    public void setPercent(final double n) {
        this.mProgress = Utils.formatPercentage(n, true);
        this.notifyChanged();
    }
    
    public void setSubtitle(final CharSequence mProgress) {
        this.mProgress = mProgress;
        this.notifyChanged();
    }
    
    public void shouldShowAnomalyIcon(final boolean mShowAnomalyIcon) {
        this.mShowAnomalyIcon = mShowAnomalyIcon;
        this.notifyChanged();
    }
}
