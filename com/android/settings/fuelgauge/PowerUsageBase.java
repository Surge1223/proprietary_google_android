package com.android.settings.fuelgauge;

import android.content.Loader;
import android.app.LoaderManager$LoaderCallbacks;
import android.os.Bundle;
import android.content.Context;
import android.app.Activity;
import android.os.UserManager;
import com.android.internal.os.BatteryStatsHelper;
import com.android.settings.dashboard.DashboardFragment;

public abstract class PowerUsageBase extends DashboardFragment
{
    static final int MENU_STATS_REFRESH = 2;
    private BatteryBroadcastReceiver mBatteryBroadcastReceiver;
    protected BatteryStatsHelper mStatsHelper;
    protected UserManager mUm;
    
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        this.mUm = (UserManager)activity.getSystemService("user");
        this.mStatsHelper = new BatteryStatsHelper((Context)activity, true);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mStatsHelper.create(bundle);
        this.setHasOptionsMenu(true);
        (this.mBatteryBroadcastReceiver = new BatteryBroadcastReceiver(this.getContext())).setBatteryChangedListener((BatteryBroadcastReceiver.OnBatteryChangedListener)new _$$Lambda$PowerUsageBase$FbH3lnw7c_hajFOBNpt07exnLiM(this));
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mBatteryBroadcastReceiver.unRegister();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        BatteryStatsHelper.dropFile((Context)this.getActivity(), "tmp_bat_history.bin");
        this.mBatteryBroadcastReceiver.register();
    }
    
    @Override
    public void onStart() {
        super.onStart();
    }
    
    protected abstract void refreshUi(final int p0);
    
    protected void restartBatteryStatsLoader(final int n) {
        final Bundle bundle = new Bundle();
        bundle.putInt("refresh_type", n);
        this.getLoaderManager().restartLoader(0, bundle, (LoaderManager$LoaderCallbacks)new PowerLoaderCallback());
    }
    
    protected void updatePreference(final BatteryHistoryPreference batteryHistoryPreference) {
        final long currentTimeMillis = System.currentTimeMillis();
        batteryHistoryPreference.setStats(this.mStatsHelper);
        BatteryUtils.logRuntime("PowerUsageBase", "updatePreference", currentTimeMillis);
    }
    
    public class PowerLoaderCallback implements LoaderManager$LoaderCallbacks<BatteryStatsHelper>
    {
        private int mRefreshType;
        
        public Loader<BatteryStatsHelper> onCreateLoader(final int n, final Bundle bundle) {
            this.mRefreshType = bundle.getInt("refresh_type");
            return (Loader<BatteryStatsHelper>)new BatteryStatsHelperLoader(PowerUsageBase.this.getContext());
        }
        
        public void onLoadFinished(final Loader<BatteryStatsHelper> loader, final BatteryStatsHelper mStatsHelper) {
            PowerUsageBase.this.mStatsHelper = mStatsHelper;
            PowerUsageBase.this.refreshUi(this.mRefreshType);
        }
        
        public void onLoaderReset(final Loader<BatteryStatsHelper> loader) {
        }
    }
}
