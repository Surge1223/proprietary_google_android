package com.android.settings.fuelgauge;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.util.TypedValue;
import android.content.Context;
import com.android.internal.os.BatteryStatsHelper;
import android.os.Bundle;
import com.android.settings.graph.UsageView;
import android.view.View;
import android.widget.TextView;
import android.os.BatteryStats;
import android.content.Intent;
import com.android.settings.SettingsPreferenceFragment;

public final class _$$Lambda$BatteryHistoryDetail$ZIvw_m8MPrnAuz9tJSzFmSFxa_8 implements Callback
{
    @Override
    public final void onBatteryInfoLoaded(final BatteryInfo batteryInfo) {
        BatteryHistoryDetail.lambda$updateEverything$0(this.f$0, batteryInfo);
    }
}
