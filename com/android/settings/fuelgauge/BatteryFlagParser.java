package com.android.settings.fuelgauge;

import android.os.BatteryStats$HistoryItem;
import android.util.SparseIntArray;
import android.util.SparseBooleanArray;

public class BatteryFlagParser implements BatteryActiveProvider, BatteryDataParser
{
    private final int mAccentColor;
    private final SparseBooleanArray mData;
    private final int mFlag;
    private boolean mLastSet;
    private long mLastTime;
    private long mLength;
    private final boolean mState2;
    
    public BatteryFlagParser(final int mAccentColor, final boolean mState2, final int mFlag) {
        this.mData = new SparseBooleanArray();
        this.mAccentColor = mAccentColor;
        this.mFlag = mFlag;
        this.mState2 = mState2;
    }
    
    private int getColor(final boolean b) {
        if (b) {
            return this.mAccentColor;
        }
        return 0;
    }
    
    @Override
    public SparseIntArray getColorArray() {
        final SparseIntArray sparseIntArray = new SparseIntArray();
        for (int i = 0; i < this.mData.size(); ++i) {
            sparseIntArray.put(this.mData.keyAt(i), this.getColor(this.mData.valueAt(i)));
        }
        return sparseIntArray;
    }
    
    @Override
    public long getPeriod() {
        return this.mLength;
    }
    
    @Override
    public boolean hasData() {
        final int size = this.mData.size();
        boolean b = true;
        if (size <= 1) {
            b = false;
        }
        return b;
    }
    
    protected boolean isSet(final BatteryStats$HistoryItem batteryStats$HistoryItem) {
        int n;
        if (this.mState2) {
            n = batteryStats$HistoryItem.states2;
        }
        else {
            n = batteryStats$HistoryItem.states;
        }
        return (n & this.mFlag) != 0x0;
    }
    
    @Override
    public void onDataGap() {
        if (this.mLastSet) {
            this.mData.put((int)this.mLastTime, false);
            this.mLastSet = false;
        }
    }
    
    @Override
    public void onDataPoint(final long mLastTime, final BatteryStats$HistoryItem batteryStats$HistoryItem) {
        final boolean set = this.isSet(batteryStats$HistoryItem);
        if (set != this.mLastSet) {
            this.mData.put((int)mLastTime, set);
            this.mLastSet = set;
        }
        this.mLastTime = mLastTime;
    }
    
    @Override
    public void onParsingDone() {
        if (this.mLastSet) {
            this.mData.put((int)this.mLastTime, false);
            this.mLastSet = false;
        }
    }
    
    @Override
    public void onParsingStarted(final long n, final long n2) {
        this.mLength = n2 - n;
    }
}
