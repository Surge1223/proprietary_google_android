package com.android.settings;

import com.android.settingslib.license.LicenseHtmlLoader;
import android.content.Loader;
import android.os.SystemProperties;
import android.support.v4.content.FileProvider;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;
import android.widget.Toast;
import android.text.TextUtils;
import java.io.File;
import android.app.LoaderManager$LoaderCallbacks;
import android.app.Activity;

public class SettingsLicenseActivity extends Activity implements LoaderManager$LoaderCallbacks<File>
{
    private boolean isFilePathValid(final String s) {
        return !TextUtils.isEmpty((CharSequence)s) && this.isFileValid(new File(s));
    }
    
    private void showErrorAndFinish() {
        Toast.makeText((Context)this, 2131889058, 1).show();
        this.finish();
    }
    
    private void showGeneratedHtmlFile(final File file) {
        if (file != null) {
            this.showHtmlFromUri(this.getUriFromGeneratedHtmlFile(file));
        }
        else {
            Log.e("SettingsLicenseActivity", "Failed to generate.");
            this.showErrorAndFinish();
        }
    }
    
    private void showHtmlFromDefaultXmlFiles() {
        this.getLoaderManager().initLoader(0, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
    }
    
    private void showHtmlFromUri(final Uri uri) {
        final Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, "text/html");
        intent.putExtra("android.intent.extra.TITLE", this.getString(2131889057));
        if ("content".equals(uri.getScheme())) {
            intent.addFlags(1);
        }
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setPackage("com.android.htmlviewer");
        try {
            this.startActivity(intent);
            this.finish();
        }
        catch (ActivityNotFoundException ex) {
            Log.e("SettingsLicenseActivity", "Failed to find viewer", (Throwable)ex);
            this.showErrorAndFinish();
        }
    }
    
    private void showSelectedFile(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            Log.e("SettingsLicenseActivity", "The system property for the license file is empty");
            this.showErrorAndFinish();
            return;
        }
        final File file = new File(s);
        if (!this.isFileValid(file)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("License file ");
            sb.append(s);
            sb.append(" does not exist");
            Log.e("SettingsLicenseActivity", sb.toString());
            this.showErrorAndFinish();
            return;
        }
        this.showHtmlFromUri(Uri.fromFile(file));
    }
    
    Uri getUriFromGeneratedHtmlFile(final File file) {
        return FileProvider.getUriForFile((Context)this, "com.android.settings.files", file);
    }
    
    boolean isFileValid(final File file) {
        return file.exists() && file.length() != 0L;
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String value = SystemProperties.get("ro.config.license_path", "/system/etc/NOTICE.html.gz");
        if (this.isFilePathValid(value)) {
            this.showSelectedFile(value);
        }
        else {
            this.showHtmlFromDefaultXmlFiles();
        }
    }
    
    public Loader<File> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<File>)new LicenseHtmlLoader((Context)this);
    }
    
    public void onLoadFinished(final Loader<File> loader, final File file) {
        this.showGeneratedHtmlFile(file);
    }
    
    public void onLoaderReset(final Loader<File> loader) {
    }
}
