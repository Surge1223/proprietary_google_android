package com.android.settings.search;

import java.util.Iterator;
import java.util.List;
import com.android.settings.core.BasePreferenceController;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import android.util.ArrayMap;
import java.util.Map;
import java.io.Serializable;
import android.util.Log;
import android.content.ComponentName;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context;

public class DatabaseIndexingUtils
{
    public static Intent buildSearchResultPageIntent(final Context context, final String s, final String s2, final String s3) {
        return buildSearchResultPageIntent(context, s, s2, s3, 34);
    }
    
    public static Intent buildSearchResultPageIntent(final Context context, final String destination, final String s, final String title, final int sourceMetricsCategory) {
        final Bundle arguments = new Bundle();
        arguments.putString(":settings:fragment_args_key", s);
        final Intent intent = new SubSettingLauncher(context).setDestination(destination).setArguments(arguments).setTitle(title).setSourceMetricsCategory(sourceMetricsCategory).toIntent();
        intent.putExtra(":settings:fragment_args_key", s).setAction("com.android.settings.SEARCH_RESULT_TRAMPOLINE").setComponent((ComponentName)null);
        return intent;
    }
    
    public static Class<?> getIndexableClass(String s) {
        final Serializable s2 = null;
        try {
            final Class<?> forName = Class.forName((String)s);
            s = s2;
            if (isIndexableClass(forName)) {
                s = forName;
            }
            return (Class<?>)s;
        }
        catch (ClassNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find class: ");
            sb.append((String)s);
            Log.d("IndexingUtil", sb.toString());
            return null;
        }
    }
    
    public static Map<String, ResultPayload> getPayloadKeyMap(final String s, final Context context) {
        final ArrayMap arrayMap = new ArrayMap();
        if (context == null) {
            return (Map<String, ResultPayload>)arrayMap;
        }
        final Class<?> indexableClass = getIndexableClass(s);
        if (indexableClass == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SearchIndexableResource '");
            sb.append(s);
            sb.append("' should implement the ");
            sb.append(Indexable.class.getName());
            sb.append(" interface!");
            Log.d("IndexingUtil", sb.toString());
            return (Map<String, ResultPayload>)arrayMap;
        }
        final List<AbstractPreferenceController> preferenceControllers = getSearchIndexProvider(indexableClass).getPreferenceControllers(context);
        if (preferenceControllers == null) {
            return (Map<String, ResultPayload>)arrayMap;
        }
        for (final AbstractPreferenceController abstractPreferenceController : preferenceControllers) {
            ResultPayload resultPayload;
            if (abstractPreferenceController instanceof PreferenceControllerMixin) {
                resultPayload = ((PreferenceControllerMixin)abstractPreferenceController).getResultPayload();
            }
            else {
                if (!(abstractPreferenceController instanceof BasePreferenceController)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(((BasePreferenceController)abstractPreferenceController).getClass().getName());
                    sb2.append(" must implement ");
                    sb2.append(PreferenceControllerMixin.class.getName());
                    throw new IllegalStateException(sb2.toString());
                }
                resultPayload = ((BasePreferenceController)abstractPreferenceController).getResultPayload();
            }
            if (resultPayload != null) {
                arrayMap.put((Object)abstractPreferenceController.getPreferenceKey(), (Object)resultPayload);
            }
        }
        return (Map<String, ResultPayload>)arrayMap;
    }
    
    public static Indexable.SearchIndexProvider getSearchIndexProvider(final Class<?> clazz) {
        try {
            return (Indexable.SearchIndexProvider)clazz.getField("SEARCH_INDEX_DATA_PROVIDER").get(null);
        }
        catch (IllegalArgumentException ex) {
            Log.d("IndexingUtil", "Illegal argument when accessing field 'SEARCH_INDEX_DATA_PROVIDER'");
        }
        catch (IllegalAccessException ex2) {
            Log.d("IndexingUtil", "Illegal access to field 'SEARCH_INDEX_DATA_PROVIDER'");
        }
        catch (SecurityException ex3) {
            Log.d("IndexingUtil", "Security exception for field 'SEARCH_INDEX_DATA_PROVIDER'");
        }
        catch (NoSuchFieldException ex4) {
            Log.d("IndexingUtil", "Cannot find field 'SEARCH_INDEX_DATA_PROVIDER'");
        }
        return null;
    }
    
    public static boolean isIndexableClass(final Class<?> clazz) {
        return clazz != null && Indexable.class.isAssignableFrom(clazz);
    }
}
