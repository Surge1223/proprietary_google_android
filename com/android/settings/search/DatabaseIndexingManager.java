package com.android.settings.search;

import android.database.Cursor;
import java.util.Set;
import java.util.Map;
import android.os.Build;
import java.util.Locale;
import android.content.Intent;
import java.util.Iterator;
import android.content.ContentValues;
import android.text.TextUtils;
import android.content.pm.ResolveInfo;
import com.android.settings.search.indexing.IndexData;
import java.util.List;
import com.android.settings.search.indexing.PreIndexData;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.database.sqlite.SQLiteDatabase;
import com.android.settings.search.indexing.IndexDataConverter;
import android.content.Context;
import com.android.settings.search.indexing.PreIndexDataCollector;

public class DatabaseIndexingManager
{
    private PreIndexDataCollector mCollector;
    private Context mContext;
    private IndexDataConverter mConverter;
    
    public DatabaseIndexingManager(final Context mContext) {
        this.mContext = mContext;
    }
    
    private SQLiteDatabase getWritableDatabase() {
        try {
            return IndexDatabaseHelper.getInstance(this.mContext).getWritableDatabase();
        }
        catch (SQLiteException ex) {
            Log.e("DatabaseIndexingManager", "Cannot open writable database", (Throwable)ex);
            return null;
        }
    }
    
    private void rebuildDatabase() {
        IndexDatabaseHelper.getInstance(this.mContext).reconstruct(this.getWritableDatabase());
    }
    
    List<IndexData> getIndexData(final PreIndexData preIndexData) {
        if (this.mConverter == null) {
            this.mConverter = new IndexDataConverter(this.mContext);
        }
        return this.mConverter.convertPreIndexDataToIndexData(preIndexData);
    }
    
    PreIndexData getIndexDataFromProviders(final List<ResolveInfo> list, final boolean b) {
        if (this.mCollector == null) {
            this.mCollector = new PreIndexDataCollector(this.mContext);
        }
        return this.mCollector.collectIndexableData(list, b);
    }
    
    void insertIndexData(final SQLiteDatabase sqLiteDatabase, final List<IndexData> list) {
        for (final IndexData indexData : list) {
            if (TextUtils.isEmpty((CharSequence)indexData.normalizedTitle)) {
                continue;
            }
            final ContentValues contentValues = new ContentValues();
            contentValues.put("docid", indexData.getDocId());
            contentValues.put("locale", indexData.locale);
            contentValues.put("data_title", indexData.updatedTitle);
            contentValues.put("data_title_normalized", indexData.normalizedTitle);
            contentValues.put("data_summary_on", indexData.updatedSummaryOn);
            contentValues.put("data_summary_on_normalized", indexData.normalizedSummaryOn);
            contentValues.put("data_entries", indexData.entries);
            contentValues.put("data_keywords", indexData.spaceDelimitedKeywords);
            contentValues.put("class_name", indexData.className);
            contentValues.put("screen_title", indexData.screenTitle);
            contentValues.put("intent_action", indexData.intentAction);
            contentValues.put("intent_target_package", indexData.intentTargetPackage);
            contentValues.put("intent_target_class", indexData.intentTargetClass);
            contentValues.put("icon", indexData.iconResId);
            contentValues.put("enabled", indexData.enabled);
            contentValues.put("data_key_reference", indexData.key);
            contentValues.put("user_id", indexData.userId);
            contentValues.put("payload_type", indexData.payloadType);
            contentValues.put("payload", indexData.payload);
            sqLiteDatabase.replaceOrThrow("prefs_index", (String)null, contentValues);
            if (TextUtils.isEmpty((CharSequence)indexData.className) || TextUtils.isEmpty((CharSequence)indexData.childClassName)) {
                continue;
            }
            final ContentValues contentValues2 = new ContentValues();
            contentValues2.put("parent_class", indexData.className);
            contentValues2.put("parent_title", indexData.screenTitle);
            contentValues2.put("child_class", indexData.childClassName);
            contentValues2.put("child_title", indexData.updatedTitle);
            sqLiteDatabase.replaceOrThrow("site_map", (String)null, contentValues2);
        }
    }
    
    boolean isFullIndex(final Context context, final String s, final String s2, final String s3) {
        final boolean localeAlreadyIndexed = IndexDatabaseHelper.isLocaleAlreadyIndexed(context, s);
        final boolean buildIndexed = IndexDatabaseHelper.isBuildIndexed(context, s2);
        final boolean providersIndexed = IndexDatabaseHelper.areProvidersIndexed(context, s3);
        return !localeAlreadyIndexed || !buildIndexed || !providersIndexed;
    }
    
    public void performIndexing() {
        System.currentTimeMillis();
        final List queryIntentContentProviders = this.mContext.getPackageManager().queryIntentContentProviders(new Intent("android.content.action.SEARCH_INDEXABLES_PROVIDER"), 0);
        final String string = Locale.getDefault().toString();
        final String fingerprint = Build.FINGERPRINT;
        final String buildProviderVersionedNames = IndexDatabaseHelper.buildProviderVersionedNames(queryIntentContentProviders);
        final boolean fullIndex = this.isFullIndex(this.mContext, string, fingerprint, buildProviderVersionedNames);
        if (fullIndex) {
            this.rebuildDatabase();
        }
        final PreIndexData indexDataFromProviders = this.getIndexDataFromProviders(queryIntentContentProviders, fullIndex);
        System.currentTimeMillis();
        this.updateDatabase(indexDataFromProviders, fullIndex);
        IndexDatabaseHelper.setLocaleIndexed(this.mContext, string);
        IndexDatabaseHelper.setBuildIndexed(this.mContext, fingerprint);
        IndexDatabaseHelper.setProvidersIndexed(this.mContext, buildProviderVersionedNames);
    }
    
    void updateDataInDatabase(final SQLiteDatabase sqLiteDatabase, final Map<String, Set<String>> map) {
        final Cursor query = sqLiteDatabase.query("prefs_index", DatabaseResultLoader.SELECT_COLUMNS, "enabled = 1", (String[])null, (String)null, (String)null, (String)null);
        final ContentValues contentValues = new ContentValues();
        contentValues.put("enabled", 0);
        while (query.moveToNext()) {
            String s;
            if ((s = query.getString(8)) == null) {
                s = this.mContext.getPackageName();
            }
            final String string = query.getString(10);
            final Set<String> set = map.get(s);
            if (set != null && set.contains(string)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("docid = ");
                sb.append(query.getInt(0));
                sqLiteDatabase.update("prefs_index", contentValues, sb.toString(), (String[])null);
            }
        }
        query.close();
        final Cursor query2 = sqLiteDatabase.query("prefs_index", DatabaseResultLoader.SELECT_COLUMNS, "enabled = 0", (String[])null, (String)null, (String)null, (String)null);
        final ContentValues contentValues2 = new ContentValues();
        contentValues2.put("enabled", 1);
        while (query2.moveToNext()) {
            String s2;
            if ((s2 = query2.getString(8)) == null) {
                s2 = this.mContext.getPackageName();
            }
            final String string2 = query2.getString(10);
            final Set<String> set2 = map.get(s2);
            if (set2 != null && !set2.contains(string2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("docid = ");
                sb2.append(query2.getInt(0));
                sqLiteDatabase.update("prefs_index", contentValues2, sb2.toString(), (String[])null);
            }
        }
        query2.close();
    }
    
    void updateDatabase(final PreIndexData preIndexData, final boolean b) {
        final Map<String, Set<String>> nonIndexableKeys = preIndexData.nonIndexableKeys;
        final SQLiteDatabase writableDatabase = this.getWritableDatabase();
        if (writableDatabase == null) {
            Log.w("DatabaseIndexingManager", "Cannot indexDatabase Index as I cannot get a writable database");
            return;
        }
        try {
            writableDatabase.beginTransaction();
            this.insertIndexData(writableDatabase, this.getIndexData(preIndexData));
            if (!b) {
                this.updateDataInDatabase(writableDatabase, nonIndexableKeys);
            }
            writableDatabase.setTransactionSuccessful();
        }
        finally {
            writableDatabase.endTransaction();
        }
    }
}
