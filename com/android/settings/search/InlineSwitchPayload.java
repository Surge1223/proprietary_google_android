package com.android.settings.search;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class InlineSwitchPayload extends InlinePayload
{
    public static final Parcelable.Creator<InlineSwitchPayload> CREATOR;
    private boolean mIsStandard;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<InlineSwitchPayload>() {
            public InlineSwitchPayload createFromParcel(final Parcel parcel) {
                return new InlineSwitchPayload(parcel, (InlineSwitchPayload$1)null);
            }
            
            public InlineSwitchPayload[] newArray(final int n) {
                return new InlineSwitchPayload[n];
            }
        };
    }
    
    private InlineSwitchPayload(final Parcel parcel) {
        super(parcel);
        final int int1 = parcel.readInt();
        boolean mIsStandard = true;
        if (int1 != 1) {
            mIsStandard = false;
        }
        this.mIsStandard = mIsStandard;
    }
    
    public InlineSwitchPayload(final String s, final int n, final int n2, final Intent intent, final boolean b, final int n3) {
        super(s, n, intent, b, n3);
        boolean mIsStandard = true;
        if (n2 != 1) {
            mIsStandard = false;
        }
        this.mIsStandard = mIsStandard;
    }
    
    @Override
    public int getType() {
        return 2;
    }
    
    @Override
    protected int standardizeInput(int n) {
        if (n != 0 && n != 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid input for InlineSwitch. Expected: 1 or 0 but found: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        if (!this.mIsStandard) {
            n = 1 - n;
        }
        return n;
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeInt((int)(this.mIsStandard ? 1 : 0));
    }
}
