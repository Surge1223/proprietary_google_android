package com.android.settings.search;

import java.util.Iterator;
import android.content.Intent;
import java.util.Collection;
import android.net.Uri.Builder;
import com.android.settings.overlay.FeatureFactory;
import androidx.slice.core.SliceQuery;
import androidx.slice.SliceItem;
import androidx.slice.widget.ListContent;
import android.util.Log;
import android.content.Context;
import androidx.slice.SliceMetadata;
import java.util.concurrent.CountDownLatch;
import androidx.slice.Slice;
import android.net.Uri;
import androidx.slice.SliceViewManager;
import com.android.internal.annotations.VisibleForTesting;
import android.app.job.JobService;
import android.app.job.JobParameters;

public final class _$$Lambda$DeviceIndexUpdateJobService$CyjXGsZVpAu5iTckScg1Ee8_bGU implements Runnable
{
    @Override
    public final void run() {
        this.f$0.updateIndex(this.f$1);
    }
}
