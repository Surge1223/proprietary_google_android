package com.android.settings.search;

import android.os.Parcel;
import android.content.Intent;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class ResultPayload implements Parcelable
{
    public static final Parcelable.Creator<ResultPayload> CREATOR;
    protected final Intent mIntent;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<ResultPayload>() {
            public ResultPayload createFromParcel(final Parcel parcel) {
                return new ResultPayload(parcel, null);
            }
            
            public ResultPayload[] newArray(final int n) {
                return new ResultPayload[n];
            }
        };
    }
    
    public ResultPayload(final Intent mIntent) {
        this.mIntent = mIntent;
    }
    
    private ResultPayload(final Parcel parcel) {
        this.mIntent = (Intent)parcel.readParcelable(ResultPayload.class.getClassLoader());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public int getType() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeParcelable((Parcelable)this.mIntent, n);
    }
}
