package com.android.settings.search;

import android.provider.Settings;
import android.provider.Settings;
import android.provider.Settings;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;

public abstract class InlinePayload extends ResultPayload
{
    final int mDefaultvalue;
    final boolean mIsDeviceSupported;
    private final String mSettingKey;
    final int mSettingSource;
    
    InlinePayload(final Parcel parcel) {
        super((Intent)parcel.readParcelable(Intent.class.getClassLoader()));
        this.mSettingKey = parcel.readString();
        this.mSettingSource = parcel.readInt();
        final int int1 = parcel.readInt();
        boolean mIsDeviceSupported = true;
        if (int1 != 1) {
            mIsDeviceSupported = false;
        }
        this.mIsDeviceSupported = mIsDeviceSupported;
        this.mDefaultvalue = parcel.readInt();
    }
    
    public InlinePayload(final String mSettingKey, final int mSettingSource, final Intent intent, final boolean mIsDeviceSupported, final int mDefaultvalue) {
        super(intent);
        this.mSettingKey = mSettingKey;
        this.mSettingSource = mSettingSource;
        this.mIsDeviceSupported = mIsDeviceSupported;
        this.mDefaultvalue = mDefaultvalue;
    }
    
    public int getAvailability() {
        if (this.mIsDeviceSupported) {
            return 0;
        }
        return 2;
    }
    
    public int getValue(final Context context) {
        int n = -1;
        switch (this.mSettingSource) {
            case 3: {
                n = Settings.Global.getInt(context.getContentResolver(), this.mSettingKey, this.mDefaultvalue);
                break;
            }
            case 2: {
                n = Settings.Secure.getInt(context.getContentResolver(), this.mSettingKey, this.mDefaultvalue);
                break;
            }
            case 1: {
                n = Settings.System.getInt(context.getContentResolver(), this.mSettingKey, this.mDefaultvalue);
                break;
            }
        }
        return this.standardizeInput(n);
    }
    
    public boolean setValue(final Context context, int standardizeInput) {
        standardizeInput = this.standardizeInput(standardizeInput);
        switch (this.mSettingSource) {
            default: {
                return false;
            }
            case 3: {
                return Settings.Global.putInt(context.getContentResolver(), this.mSettingKey, standardizeInput);
            }
            case 2: {
                return Settings.Secure.putInt(context.getContentResolver(), this.mSettingKey, standardizeInput);
            }
            case 1: {
                return Settings.System.putInt(context.getContentResolver(), this.mSettingKey, standardizeInput);
            }
            case 0: {
                return false;
            }
        }
    }
    
    protected abstract int standardizeInput(final int p0) throws IllegalArgumentException;
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeString(this.mSettingKey);
        parcel.writeInt(this.mSettingSource);
        parcel.writeInt((int)(this.mIsDeviceSupported ? 1 : 0));
        parcel.writeInt(this.mDefaultvalue);
    }
}
