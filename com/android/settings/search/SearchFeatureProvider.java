package com.android.settings.search;

import android.content.ComponentName;
import android.content.Context;
import android.view.View.OnClickListener;
import android.widget.Toolbar;
import com.android.settings.overlay.FeatureFactory;
import android.view.View;
import android.app.Activity;
import android.content.Intent;

public interface SearchFeatureProvider
{
    public static final Intent SEARCH_UI_INTENT = new Intent("com.android.settings.action.SETTINGS_SEARCH");
    
    SearchIndexableResources getSearchIndexableResources();
    
    default String getSettingsIntelligencePkgName() {
        return "com.android.settings.intelligence";
    }
    
    default void initSearchToolbar(final Activity activity, final Toolbar toolbar) {
        if (activity != null && toolbar != null) {
            toolbar.setOnClickListener((View.OnClickListener)new _$$Lambda$SearchFeatureProvider$7ZGLG3tZpGqHgt7m_jMbwikwfJM(this, activity));
        }
    }
    
    void updateIndex(final Context p0);
    
    void verifyLaunchSearchResultPageCaller(final Context p0, final ComponentName p1) throws SecurityException, IllegalArgumentException;
}
