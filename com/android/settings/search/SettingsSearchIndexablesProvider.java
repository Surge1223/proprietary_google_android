package com.android.settings.search;

import com.android.settingslib.drawer.Tile;
import com.android.settings.dashboard.DashboardFragmentRegistry;
import com.android.settingslib.drawer.DashboardCategory;
import android.database.MatrixCursor;
import android.provider.SearchIndexablesContract;
import android.database.Cursor;
import android.text.TextUtils;
import android.provider.SearchIndexableResource;
import java.io.Serializable;
import java.util.Iterator;
import android.util.Log;
import java.util.ArrayList;
import com.android.settings.overlay.FeatureFactory;
import java.util.List;
import android.content.Context;
import android.util.ArraySet;
import java.util.Collection;
import android.provider.SearchIndexablesProvider;

public class SettingsSearchIndexablesProvider extends SearchIndexablesProvider
{
    private static final Collection<String> INVALID_KEYS;
    
    static {
        (INVALID_KEYS = (Collection)new ArraySet()).add(null);
        SettingsSearchIndexablesProvider.INVALID_KEYS.add("");
    }
    
    private List<String> getNonIndexableKeysFromProvider(final Context context) {
        final Collection<Class> providerValues = FeatureFactory.getFactory(context).getSearchFeatureProvider().getSearchIndexableResources().getProviderValues();
        final ArrayList<Object> list = new ArrayList<Object>();
        for (Serializable s : providerValues) {
            System.currentTimeMillis();
            final Indexable.SearchIndexProvider searchIndexProvider = DatabaseIndexingUtils.getSearchIndexProvider((Class<?>)s);
            try {
                final List<String> nonIndexableKeys = searchIndexProvider.getNonIndexableKeys(context);
                if (nonIndexableKeys == null) {
                    continue;
                }
                if (nonIndexableKeys.isEmpty()) {
                    continue;
                }
                if (nonIndexableKeys.removeAll(SettingsSearchIndexablesProvider.INVALID_KEYS)) {
                    s = new StringBuilder();
                    ((StringBuilder)s).append(searchIndexProvider);
                    ((StringBuilder)s).append(" tried to add an empty non-indexable key");
                    Log.v("SettingsSearchProvider", ((StringBuilder)s).toString());
                }
                list.addAll(nonIndexableKeys);
                continue;
            }
            catch (Exception ex) {
                if (System.getProperty("debug.com.android.settings.search.crash_on_error") == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Error trying to get non-indexable keys from: ");
                    sb.append(((Class)s).getName());
                    Log.e("SettingsSearchProvider", sb.toString(), (Throwable)ex);
                    continue;
                }
                throw new RuntimeException(ex);
            }
            break;
        }
        return (List<String>)list;
    }
    
    private List<SearchIndexableRaw> getSearchIndexableRawFromProvider(final Context context) {
        final Collection<Class> providerValues = FeatureFactory.getFactory(context).getSearchFeatureProvider().getSearchIndexableResources().getProviderValues();
        final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<SearchIndexableRaw>();
        for (final Class<?> clazz : providerValues) {
            final List<SearchIndexableRaw> rawDataToIndex = DatabaseIndexingUtils.getSearchIndexProvider(clazz).getRawDataToIndex(context, true);
            if (rawDataToIndex == null) {
                continue;
            }
            final Iterator<SearchIndexableRaw> iterator2 = rawDataToIndex.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().className = clazz.getName();
            }
            list.addAll(rawDataToIndex);
        }
        return (List<SearchIndexableRaw>)list;
    }
    
    private List<SearchIndexableResource> getSearchIndexableResourcesFromProvider(final Context context) {
        final Collection<Class> providerValues = FeatureFactory.getFactory(context).getSearchFeatureProvider().getSearchIndexableResources().getProviderValues();
        final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
        for (final Class<?> clazz : providerValues) {
            final List<SearchIndexableResource> xmlResourcesToIndex = DatabaseIndexingUtils.getSearchIndexProvider(clazz).getXmlResourcesToIndex(context, true);
            if (xmlResourcesToIndex == null) {
                continue;
            }
            for (final SearchIndexableResource searchIndexableResource : xmlResourcesToIndex) {
                String className;
                if (TextUtils.isEmpty((CharSequence)searchIndexableResource.className)) {
                    className = clazz.getName();
                }
                else {
                    className = searchIndexableResource.className;
                }
                searchIndexableResource.className = className;
            }
            list.addAll((Collection<?>)xmlResourcesToIndex);
        }
        return list;
    }
    
    public boolean onCreate() {
        return true;
    }
    
    public Cursor queryNonIndexableKeys(final String[] array) {
        final MatrixCursor matrixCursor = new MatrixCursor(SearchIndexablesContract.NON_INDEXABLES_KEYS_COLUMNS);
        for (final String s : this.getNonIndexableKeysFromProvider(this.getContext())) {
            final Object[] array2 = new Object[SearchIndexablesContract.NON_INDEXABLES_KEYS_COLUMNS.length];
            array2[0] = s;
            matrixCursor.addRow(array2);
        }
        return (Cursor)matrixCursor;
    }
    
    public Cursor queryRawData(final String[] array) {
        final MatrixCursor matrixCursor = new MatrixCursor(SearchIndexablesContract.INDEXABLES_RAW_COLUMNS);
        for (final SearchIndexableRaw searchIndexableRaw : this.getSearchIndexableRawFromProvider(this.getContext())) {
            final Object[] array2 = new Object[SearchIndexablesContract.INDEXABLES_RAW_COLUMNS.length];
            array2[1] = searchIndexableRaw.title;
            array2[2] = searchIndexableRaw.summaryOn;
            array2[3] = searchIndexableRaw.summaryOff;
            array2[4] = searchIndexableRaw.entries;
            array2[5] = searchIndexableRaw.keywords;
            array2[6] = searchIndexableRaw.screenTitle;
            array2[7] = searchIndexableRaw.className;
            array2[8] = searchIndexableRaw.iconResId;
            array2[9] = searchIndexableRaw.intentAction;
            array2[10] = searchIndexableRaw.intentTargetPackage;
            array2[11] = searchIndexableRaw.intentTargetClass;
            array2[12] = searchIndexableRaw.key;
            array2[13] = searchIndexableRaw.userId;
            matrixCursor.addRow(array2);
        }
        return (Cursor)matrixCursor;
    }
    
    public Cursor querySiteMapPairs() {
        final MatrixCursor matrixCursor = new MatrixCursor(SearchIndexablesContract.SITE_MAP_COLUMNS);
        final Context context = this.getContext();
        for (final DashboardCategory dashboardCategory : FeatureFactory.getFactory(context).getDashboardFeatureProvider(context).getAllCategories()) {
            final String s = DashboardFragmentRegistry.CATEGORY_KEY_TO_PARENT_MAP.get(dashboardCategory.key);
            if (s == null) {
                continue;
            }
            for (final Tile tile : dashboardCategory.getTiles()) {
                Object string = null;
                if (tile.metaData != null) {
                    string = tile.metaData.getString("com.android.settings.FRAGMENT_CLASS");
                }
                if (string == null) {
                    continue;
                }
                matrixCursor.newRow().add("parent_class", (Object)s).add("child_class", string);
            }
        }
        return (Cursor)matrixCursor;
    }
    
    public Cursor queryXmlResources(final String[] array) {
        final MatrixCursor matrixCursor = new MatrixCursor(SearchIndexablesContract.INDEXABLES_XML_RES_COLUMNS);
        for (final SearchIndexableResource searchIndexableResource : this.getSearchIndexableResourcesFromProvider(this.getContext())) {
            final Object[] array2 = new Object[SearchIndexablesContract.INDEXABLES_XML_RES_COLUMNS.length];
            array2[0] = searchIndexableResource.rank;
            array2[1] = searchIndexableResource.xmlResId;
            array2[2] = searchIndexableResource.className;
            array2[3] = searchIndexableResource.iconResId;
            array2[4] = searchIndexableResource.intentAction;
            array2[5] = searchIndexableResource.intentTargetPackage;
            array2[6] = null;
            matrixCursor.addRow(array2);
        }
        return (Cursor)matrixCursor;
    }
}
