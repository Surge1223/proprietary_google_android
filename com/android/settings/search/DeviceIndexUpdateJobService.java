package com.android.settings.search;

import java.util.Iterator;
import android.content.Intent;
import java.util.Collection;
import android.net.Uri.Builder;
import com.android.settings.overlay.FeatureFactory;
import androidx.slice.core.SliceQuery;
import androidx.slice.SliceItem;
import androidx.slice.widget.ListContent;
import android.util.Log;
import android.content.Context;
import androidx.slice.SliceMetadata;
import java.util.concurrent.CountDownLatch;
import androidx.slice.Slice;
import android.net.Uri;
import androidx.slice.SliceViewManager;
import android.app.job.JobParameters;
import com.android.internal.annotations.VisibleForTesting;
import android.app.job.JobService;

public class DeviceIndexUpdateJobService extends JobService
{
    @VisibleForTesting
    protected boolean mRunningJob;
    
    protected Slice bindSliceSynchronous(final SliceViewManager sliceViewManager, final Uri uri) {
        final Slice[] array = { null };
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final SliceViewManager.SliceCallback sliceCallback = new SliceViewManager.SliceCallback() {
            @Override
            public void onSliceUpdated(final Slice slice) {
                try {
                    if (SliceMetadata.from((Context)DeviceIndexUpdateJobService.this, slice).getLoadingState() == 2) {
                        array[0] = slice;
                        countDownLatch.countDown();
                        sliceViewManager.unregisterSliceCallback(uri, (SliceViewManager.SliceCallback)this);
                    }
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(uri);
                    sb.append(" cannot be indexed");
                    Log.w("DeviceIndexUpdate", sb.toString(), (Throwable)ex);
                    array[0] = slice;
                }
            }
        };
        sliceViewManager.registerSliceCallback(uri, (SliceViewManager.SliceCallback)sliceCallback);
        ((SliceViewManager.SliceCallback)sliceCallback).onSliceUpdated(sliceViewManager.bindSlice(uri));
        try {
            countDownLatch.await();
        }
        catch (InterruptedException ex) {}
        return array[0];
    }
    
    protected CharSequence findTitle(final Slice slice, final SliceMetadata sliceMetadata) {
        final ListContent listContent = new ListContent(null, slice);
        SliceItem headerItem;
        if ((headerItem = listContent.getHeaderItem()) == null) {
            if (listContent.getRowItems().size() == 0) {
                return null;
            }
            headerItem = listContent.getRowItems().get(0);
        }
        final SliceItem find = SliceQuery.find(headerItem, "text", "title", null);
        if (find != null) {
            return find.getText();
        }
        final SliceItem find2 = SliceQuery.find(headerItem, "text", "large", null);
        if (find2 != null) {
            return find2.getText();
        }
        final SliceItem find3 = SliceQuery.find(headerItem, "text");
        if (find3 != null) {
            return find3.getText();
        }
        return null;
    }
    
    protected SliceMetadata getMetadata(final Slice slice) {
        return SliceMetadata.from((Context)this, slice);
    }
    
    protected SliceViewManager getSliceViewManager() {
        return SliceViewManager.getInstance((Context)this);
    }
    
    public boolean onStartJob(final JobParameters jobParameters) {
        if (!this.mRunningJob) {
            this.mRunningJob = true;
            final Thread thread = new Thread(new _$$Lambda$DeviceIndexUpdateJobService$CyjXGsZVpAu5iTckScg1Ee8_bGU(this, jobParameters));
            thread.setPriority(1);
            thread.start();
        }
        return true;
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        if (this.mRunningJob) {
            this.mRunningJob = false;
            return true;
        }
        return false;
    }
    
    @VisibleForTesting
    protected void updateIndex(final JobParameters jobParameters) {
        final DeviceIndexFeatureProvider deviceIndexFeatureProvider = FeatureFactory.getFactory((Context)this).getDeviceIndexFeatureProvider();
        final SliceViewManager sliceViewManager = this.getSliceViewManager();
        final Uri build = new Uri.Builder().scheme("content").authority("com.android.settings.slices").build();
        final Uri build2 = new Uri.Builder().scheme("content").authority("android.settings.slices").build();
        final Collection<Uri> sliceDescendants = sliceViewManager.getSliceDescendants(build);
        sliceDescendants.addAll(sliceViewManager.getSliceDescendants(build2));
        deviceIndexFeatureProvider.clearIndex((Context)this);
        for (final Uri uri : sliceDescendants) {
            if (!this.mRunningJob) {
                return;
            }
            final Slice bindSliceSynchronous = this.bindSliceSynchronous(sliceViewManager, uri);
            final SliceMetadata metadata = this.getMetadata(bindSliceSynchronous);
            final CharSequence title = this.findTitle(bindSliceSynchronous, metadata);
            if (title == null) {
                continue;
            }
            deviceIndexFeatureProvider.index((Context)this, title, uri, DeviceIndexFeatureProvider.createDeepLink(new Intent("com.android.settings.action.VIEW_SLICE").setPackage(this.getPackageName()).putExtra("slice", uri.toString()).toUri(2)), metadata.getSliceKeywords());
        }
        this.jobFinished(jobParameters, false);
    }
}
