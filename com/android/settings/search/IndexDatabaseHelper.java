package com.android.settings.search;

import android.database.Cursor;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.util.Log;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.os.Build.VERSION;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public class IndexDatabaseHelper extends SQLiteOpenHelper
{
    private static final String INSERT_BUILD_VERSION;
    private static IndexDatabaseHelper sSingleton;
    private final Context mContext;
    
    static {
        final StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO meta_index VALUES ('");
        sb.append(Build.VERSION.INCREMENTAL);
        sb.append("');");
        INSERT_BUILD_VERSION = sb.toString();
    }
    
    public IndexDatabaseHelper(final Context context) {
        super(context, "search_index.db", (SQLiteDatabase$CursorFactory)null, 118);
        this.mContext = context.getApplicationContext();
    }
    
    static boolean areProvidersIndexed(final Context context, final String s) {
        return TextUtils.equals((CharSequence)context.getSharedPreferences("indexing_manager", 0).getString("indexed_providers", (String)null), (CharSequence)s);
    }
    
    private void bootstrapDB(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE VIRTUAL TABLE prefs_index USING fts4(locale, data_rank, data_title, data_title_normalized, data_summary_on, data_summary_on_normalized, data_summary_off, data_summary_off_normalized, data_entries, data_keywords, screen_title, class_name, icon, intent_action, intent_target_package, intent_target_class, enabled, data_key_reference, user_id, payload_type, payload);");
        sqLiteDatabase.execSQL("CREATE TABLE meta_index(build VARCHAR(32) NOT NULL)");
        sqLiteDatabase.execSQL("CREATE TABLE saved_queries(query VARCHAR(64) NOT NULL, timestamp INTEGER)");
        sqLiteDatabase.execSQL("CREATE VIRTUAL TABLE site_map USING fts4(parent_class, child_class, parent_title, child_title)");
        sqLiteDatabase.execSQL(IndexDatabaseHelper.INSERT_BUILD_VERSION);
        Log.i("IndexDatabaseHelper", "Bootstrapped database");
    }
    
    static String buildProviderVersionedNames(final List<ResolveInfo> list) {
        final StringBuilder sb = new StringBuilder();
        for (final ResolveInfo resolveInfo : list) {
            sb.append(resolveInfo.providerInfo.packageName);
            sb.append(':');
            sb.append(resolveInfo.providerInfo.applicationInfo.longVersionCode);
            sb.append(',');
        }
        return sb.toString();
    }
    
    private void dropTables(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS meta_index");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS prefs_index");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS saved_queries");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS site_map");
    }
    
    private String getBuildVersion(final SQLiteDatabase sqLiteDatabase) {
        final Cursor cursor = null;
        final Cursor cursor2 = null;
        Cursor cursor3 = null;
        Object o = null;
        try {
            try {
                final Cursor rawQuery = sqLiteDatabase.rawQuery("SELECT build FROM meta_index LIMIT 1;", (String[])null);
                Object string = cursor2;
                o = rawQuery;
                cursor3 = rawQuery;
                if (rawQuery.moveToFirst()) {
                    o = rawQuery;
                    cursor3 = rawQuery;
                    string = rawQuery.getString(0);
                }
                o = string;
                if (rawQuery != null) {
                    cursor3 = rawQuery;
                    cursor3.close();
                    o = string;
                    return (String)o;
                }
                return (String)o;
            }
            finally {
                if (o != null) {
                    ((Cursor)o).close();
                }
                return (String)o;
                final Object string = cursor;
            }
        }
        catch (Exception ex) {}
    }
    
    public static IndexDatabaseHelper getInstance(final Context context) {
        synchronized (IndexDatabaseHelper.class) {
            if (IndexDatabaseHelper.sSingleton == null) {
                IndexDatabaseHelper.sSingleton = new IndexDatabaseHelper(context);
            }
            return IndexDatabaseHelper.sSingleton;
        }
    }
    
    static boolean isBuildIndexed(final Context context, final String s) {
        return context.getSharedPreferences("indexing_manager", 0).getBoolean(s, false);
    }
    
    static boolean isLocaleAlreadyIndexed(final Context context, final String s) {
        return context.getSharedPreferences("indexing_manager", 0).getBoolean(s, false);
    }
    
    static void setBuildIndexed(final Context context, final String s) {
        context.getSharedPreferences("indexing_manager", 0).edit().putBoolean(s, true).apply();
    }
    
    static void setLocaleIndexed(final Context context, final String s) {
        context.getSharedPreferences("indexing_manager", 0).edit().putBoolean(s, true).apply();
    }
    
    static void setProvidersIndexed(final Context context, final String s) {
        context.getSharedPreferences("indexing_manager", 0).edit().putString("indexed_providers", s).apply();
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        this.bootstrapDB(sqLiteDatabase);
    }
    
    public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Detected schema version '");
        sb.append(n);
        sb.append("'. Index needs to be rebuilt for schema version '");
        sb.append(n2);
        sb.append("'.");
        Log.w("IndexDatabaseHelper", sb.toString());
        this.reconstruct(sqLiteDatabase);
    }
    
    public void onOpen(final SQLiteDatabase sqLiteDatabase) {
        super.onOpen(sqLiteDatabase);
        final StringBuilder sb = new StringBuilder();
        sb.append("Using schema version: ");
        sb.append(sqLiteDatabase.getVersion());
        Log.i("IndexDatabaseHelper", sb.toString());
        if (!Build.VERSION.INCREMENTAL.equals(this.getBuildVersion(sqLiteDatabase))) {
            Log.w("IndexDatabaseHelper", "Index needs to be rebuilt as build-version is not the same");
            this.reconstruct(sqLiteDatabase);
        }
        else {
            Log.i("IndexDatabaseHelper", "Index is fine");
        }
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        if (n < 118) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Detected schema version '");
            sb.append(n);
            sb.append("'. Index needs to be rebuilt for schema version '");
            sb.append(n2);
            sb.append("'.");
            Log.w("IndexDatabaseHelper", sb.toString());
            this.reconstruct(sqLiteDatabase);
        }
    }
    
    public void reconstruct(final SQLiteDatabase sqLiteDatabase) {
        this.mContext.getSharedPreferences("indexing_manager", 0).edit().clear().commit();
        this.dropTables(sqLiteDatabase);
        this.bootstrapDB(sqLiteDatabase);
    }
}
