package com.android.settings.search;

public class DatabaseResultLoader
{
    public static final String[] SELECT_COLUMNS;
    
    static {
        SELECT_COLUMNS = new String[] { "docid", "data_title", "data_summary_on", "data_summary_off", "class_name", "screen_title", "icon", "intent_action", "intent_target_package", "intent_target_class", "data_key_reference", "payload_type", "payload" };
    }
}
