package com.android.settings.search;

import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager;
import android.app.job.JobInfo$Builder;
import android.app.job.JobScheduler;
import android.os.Binder;
import android.content.ComponentName;
import com.android.settings.Utils;
import android.util.Log;
import java.util.List;
import android.text.TextUtils;
import android.provider.Settings;
import android.content.Context;
import android.net.Uri.Builder;
import android.net.Uri;
import android.os.Build;
import java.util.Locale;

public interface DeviceIndexFeatureProvider
{
    public static final Locale LANGUAGE = Locale.getDefault();
    public static final String VERSION = Build.FINGERPRINT;
    
    default Uri createDeepLink(final String s) {
        return new Uri.Builder().scheme("settings").authority("com.android.settings.slices").appendQueryParameter("intent", s).build();
    }
    
    default void setIndexState(final Context context) {
        Settings.Secure.putString(context.getContentResolver(), "settings:index_version", DeviceIndexFeatureProvider.VERSION);
        Settings.Secure.putString(context.getContentResolver(), "settings:language", DeviceIndexFeatureProvider.LANGUAGE.toString());
    }
    
    default boolean skipIndex(final Context context) {
        final boolean equals = TextUtils.equals((CharSequence)Settings.Secure.getString(context.getContentResolver(), "settings:index_version"), (CharSequence)DeviceIndexFeatureProvider.VERSION);
        return TextUtils.equals((CharSequence)Settings.Secure.getString(context.getContentResolver(), "settings:language"), (CharSequence)DeviceIndexFeatureProvider.LANGUAGE.toString()) && equals;
    }
    
    void clearIndex(final Context p0);
    
    void index(final Context p0, final CharSequence p1, final Uri p2, final Uri p3, final List<String> p4);
    
    boolean isIndexingEnabled();
    
    default void updateIndex(final Context indexState, final boolean b) {
        if (!this.isIndexingEnabled()) {
            Log.i("DeviceIndex", "Skipping: device index is not enabled");
            return;
        }
        if (!Utils.isDeviceProvisioned(indexState)) {
            Log.w("DeviceIndex", "Skipping: device is not provisioned");
            return;
        }
        final ComponentName componentName = new ComponentName(indexState.getPackageName(), DeviceIndexUpdateJobService.class.getName());
        try {
            final int callingUid = Binder.getCallingUid();
            final ServiceInfo serviceInfo = indexState.getPackageManager().getServiceInfo(componentName, 786432);
            if (serviceInfo == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping: No such service ");
                sb.append(componentName);
                Log.w("DeviceIndex", sb.toString());
                return;
            }
            if (serviceInfo.applicationInfo.uid != callingUid) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Skipping: Uid cannot schedule DeviceIndexUpdate: ");
                sb2.append(callingUid);
                Log.w("DeviceIndex", sb2.toString());
                return;
            }
            if (!b && skipIndex(indexState)) {
                Log.i("DeviceIndex", "Skipping: already indexed.");
                return;
            }
            setIndexState(indexState);
            ((JobScheduler)indexState.getSystemService((Class)JobScheduler.class)).schedule(new JobInfo$Builder(indexState.getResources().getInteger(2131427334), componentName).setPersisted(true).setMinimumLatency(1000L).setOverrideDeadline(1L).build());
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.w("DeviceIndex", "Skipping: error finding DeviceIndexUpdateJobService from packageManager");
        }
    }
}
