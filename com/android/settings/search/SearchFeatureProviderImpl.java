package com.android.settings.search;

import android.content.ComponentName;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import com.android.settings.search.indexing.IndexData;
import java.util.Locale;
import android.text.TextUtils;

public class SearchFeatureProviderImpl implements SearchFeatureProvider
{
    private DatabaseIndexingManager mDatabaseIndexingManager;
    private SearchIndexableResources mSearchIndexableResources;
    
    @VisibleForTesting
    String cleanQuery(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        String normalizeJapaneseString = s;
        if (Locale.getDefault().equals(Locale.JAPAN)) {
            normalizeJapaneseString = IndexData.normalizeJapaneseString(s);
        }
        return normalizeJapaneseString.trim();
    }
    
    public DatabaseIndexingManager getIndexingManager(final Context context) {
        if (this.mDatabaseIndexingManager == null) {
            this.mDatabaseIndexingManager = new DatabaseIndexingManager(context.getApplicationContext());
        }
        return this.mDatabaseIndexingManager;
    }
    
    @Override
    public SearchIndexableResources getSearchIndexableResources() {
        if (this.mSearchIndexableResources == null) {
            this.mSearchIndexableResources = new SearchIndexableResourcesImpl();
        }
        return this.mSearchIndexableResources;
    }
    
    protected boolean isSignatureWhitelisted(final Context context, final String s) {
        return false;
    }
    
    @Override
    public void updateIndex(final Context context) {
        final long currentTimeMillis = System.currentTimeMillis();
        this.getIndexingManager(context).performIndexing();
        FeatureFactory.getFactory(context).getMetricsFeatureProvider().histogram(context, "search_synchronous_indexing", (int)(System.currentTimeMillis() - currentTimeMillis));
    }
    
    @Override
    public void verifyLaunchSearchResultPageCaller(final Context context, final ComponentName componentName) {
        if (componentName == null) {
            throw new IllegalArgumentException("ExternalSettingsTrampoline intents must be called with startActivityForResult");
        }
        final String packageName = componentName.getPackageName();
        final boolean b = TextUtils.equals((CharSequence)packageName, (CharSequence)context.getPackageName()) || TextUtils.equals((CharSequence)this.getSettingsIntelligencePkgName(), (CharSequence)packageName);
        final boolean signatureWhitelisted = this.isSignatureWhitelisted(context, componentName.getPackageName());
        if (!b && !signatureWhitelisted) {
            throw new SecurityException("Search result intents must be called with from a whitelisted package.");
        }
    }
}
