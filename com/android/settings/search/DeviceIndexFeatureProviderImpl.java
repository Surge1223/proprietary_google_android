package com.android.settings.search;

import java.util.List;
import android.net.Uri;
import android.content.Context;

public class DeviceIndexFeatureProviderImpl implements DeviceIndexFeatureProvider
{
    @Override
    public void clearIndex(final Context context) {
    }
    
    @Override
    public void index(final Context context, final CharSequence charSequence, final Uri uri, final Uri uri2, final List<String> list) {
    }
    
    @Override
    public boolean isIndexingEnabled() {
        return false;
    }
}
