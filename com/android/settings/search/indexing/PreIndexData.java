package com.android.settings.search.indexing;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map;
import android.provider.SearchIndexableData;
import java.util.List;

public class PreIndexData
{
    public List<SearchIndexableData> dataToUpdate;
    public Map<String, Set<String>> nonIndexableKeys;
    
    public PreIndexData() {
        this.dataToUpdate = new ArrayList<SearchIndexableData>();
        this.nonIndexableKeys = new HashMap<String, Set<String>>();
    }
}
