package com.android.settings.search.indexing;

import android.database.Cursor;
import android.provider.SearchIndexableResource;
import com.android.settings.search.SearchIndexableRaw;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import com.android.internal.annotations.VisibleForTesting;
import android.content.pm.PackageManager;
import android.content.ContentResolver;
import android.text.TextUtils;
import java.util.ArrayList;
import android.net.Uri;
import java.util.Set;
import android.util.ArraySet;
import android.content.pm.PackageManager;
import android.util.Log;
import android.provider.SearchIndexableData;
import java.util.Collection;
import android.provider.SearchIndexablesContract;
import java.util.Collections;
import android.content.Context;
import java.util.List;

public class PreIndexDataCollector
{
    private static final List<String> EMPTY_LIST;
    private final String BASE_AUTHORITY;
    private Context mContext;
    private PreIndexData mIndexData;
    
    static {
        EMPTY_LIST = Collections.emptyList();
    }
    
    public PreIndexDataCollector(final Context mContext) {
        this.BASE_AUTHORITY = "com.android.settings";
        this.mContext = mContext;
    }
    
    private boolean addIndexablesFromRemoteProvider(final String s, final String s2) {
        try {
            Context context;
            if ("com.android.settings".equals(s2)) {
                context = this.mContext;
            }
            else {
                context = this.mContext.createPackageContext(s, 0);
            }
            this.mIndexData.dataToUpdate.addAll((Collection<? extends SearchIndexableData>)this.getIndexablesForXmlResourceUri(context, s, this.buildUriForXmlResources(s2), SearchIndexablesContract.INDEXABLES_XML_RES_COLUMNS));
            this.mIndexData.dataToUpdate.addAll(this.getIndexablesForRawDataUri(context, s, this.buildUriForRawData(s2), SearchIndexablesContract.INDEXABLES_RAW_COLUMNS));
            return true;
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not create context for ");
            sb.append(s);
            sb.append(": ");
            sb.append(Log.getStackTraceString((Throwable)ex));
            Log.w("IndexableDataCollector", sb.toString());
            return false;
        }
    }
    
    private void addNonIndexablesKeysFromRemoteProvider(final String s, final String s2) {
        final List<String> nonIndexablesKeysFromRemoteProvider = this.getNonIndexablesKeysFromRemoteProvider(s, s2);
        if (nonIndexablesKeysFromRemoteProvider != null && !nonIndexablesKeysFromRemoteProvider.isEmpty()) {
            this.mIndexData.nonIndexableKeys.put(s2, (Set<String>)new ArraySet((Collection)nonIndexablesKeysFromRemoteProvider));
        }
    }
    
    private List<String> getNonIndexablesKeys(final Context context, final Uri uri, String[] query) {
        final ContentResolver contentResolver = context.getContentResolver();
        final ArrayList<String> list = new ArrayList<String>();
        try {
            query = (String[])(Object)contentResolver.query(uri, query, (String)null, (String[])null, (String)null);
            if (query == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot add index data for Uri: ");
                sb.append(uri.toString());
                Log.w("IndexableDataCollector", sb.toString());
                return list;
            }
            try {
                if (((Cursor)(Object)query).getCount() > 0) {
                    while (((Cursor)(Object)query).moveToNext()) {
                        final String string = ((Cursor)(Object)query).getString(0);
                        if (TextUtils.isEmpty((CharSequence)string) && Log.isLoggable("IndexableDataCollector", 2)) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Empty non-indexable key from: ");
                            sb2.append(context.getPackageName());
                            Log.v("IndexableDataCollector", sb2.toString());
                        }
                        else {
                            list.add(string);
                        }
                    }
                }
                return list;
            }
            finally {
                ((Cursor)(Object)query).close();
            }
        }
        catch (NullPointerException ex) {
            Log.e("IndexableDataCollector", "Exception querying the keys!", (Throwable)ex);
            return list;
        }
    }
    
    private boolean isPrivilegedPackage(final String s, final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        boolean b = false;
        try {
            if ((packageManager.getPackageInfo(s, 0).applicationInfo.privateFlags & 0x8) != 0x0) {
                b = true;
            }
            return b;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    @VisibleForTesting
    Uri buildUriForNonIndexableKeys(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("content://");
        sb.append(s);
        sb.append("/");
        sb.append("settings/non_indexables_key");
        return Uri.parse(sb.toString());
    }
    
    @VisibleForTesting
    Uri buildUriForRawData(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("content://");
        sb.append(s);
        sb.append("/");
        sb.append("settings/indexables_raw");
        return Uri.parse(sb.toString());
    }
    
    @VisibleForTesting
    Uri buildUriForXmlResources(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("content://");
        sb.append(s);
        sb.append("/");
        sb.append("settings/indexables_xml_res");
        return Uri.parse(sb.toString());
    }
    
    public PreIndexData collectIndexableData(final List<ResolveInfo> list, final boolean b) {
        this.mIndexData = new PreIndexData();
        for (final ResolveInfo resolveInfo : list) {
            if (!this.isWellKnownProvider(resolveInfo)) {
                continue;
            }
            final String authority = resolveInfo.providerInfo.authority;
            final String packageName = resolveInfo.providerInfo.packageName;
            if (b) {
                this.addIndexablesFromRemoteProvider(packageName, authority);
            }
            System.currentTimeMillis();
            this.addNonIndexablesKeysFromRemoteProvider(packageName, authority);
        }
        return this.mIndexData;
    }
    
    @VisibleForTesting
    List<SearchIndexableRaw> getIndexablesForRawDataUri(final Context context, final String packageName, Uri uri, String[] array) {
        final ContentResolver contentResolver = context.getContentResolver();
        Object query = contentResolver.query(uri, array, (String)null, (String[])null, (String)null);
        final ArrayList<SearchIndexableRaw> list = new ArrayList<SearchIndexableRaw>();
        if (query == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot add index data for Uri: ");
            sb.append(uri.toString());
            Log.w("IndexableDataCollector", sb.toString());
            return list;
        }
        SearchIndexableRaw searchIndexableRaw = null;
        Label_0405: {
            try {
                final int count = ((Cursor)query).getCount();
                Object string = query;
                if (count > 0) {
                    array = (String[])(Object)contentResolver;
                    uri = (Uri)query;
                    while (true) {
                        string = uri;
                        try {
                            if (((Cursor)uri).moveToNext()) {
                                ((Cursor)uri).getInt(0);
                                final String string2 = ((Cursor)uri).getString(1);
                                final String string3 = ((Cursor)uri).getString(2);
                                final String string4 = ((Cursor)uri).getString(3);
                                final String string5 = ((Cursor)uri).getString(4);
                                final String string6 = ((Cursor)uri).getString(5);
                                string = ((Cursor)uri).getString(6);
                                final String string7 = ((Cursor)uri).getString(7);
                                final int int1 = ((Cursor)uri).getInt(8);
                                final String string8 = ((Cursor)uri).getString(9);
                                final String string9 = ((Cursor)uri).getString(10);
                                final String string10 = ((Cursor)uri).getString(11);
                                final String string11 = ((Cursor)uri).getString(12);
                                final int int2 = ((Cursor)uri).getInt(13);
                                try {
                                    try {
                                        query = new SearchIndexableRaw(context);
                                        ((SearchIndexableRaw)query).title = string2;
                                        ((SearchIndexableRaw)query).summaryOn = string3;
                                        ((SearchIndexableRaw)query).summaryOff = string4;
                                        ((SearchIndexableRaw)query).entries = string5;
                                        ((SearchIndexableRaw)query).keywords = string6;
                                        ((SearchIndexableRaw)query).screenTitle = (String)string;
                                        ((SearchIndexableRaw)query).className = string7;
                                        try {
                                            ((SearchIndexableRaw)query).packageName = packageName;
                                            ((SearchIndexableRaw)query).iconResId = int1;
                                            ((SearchIndexableRaw)query).intentAction = string8;
                                            ((SearchIndexableRaw)query).intentTargetPackage = string9;
                                            ((SearchIndexableRaw)query).intentTargetClass = string10;
                                            ((SearchIndexableRaw)query).key = string11;
                                            ((SearchIndexableRaw)query).userId = int2;
                                            list.add((SearchIndexableRaw)query);
                                        }
                                        finally {}
                                    }
                                    finally {}
                                }
                                finally {}
                            }
                        }
                        finally {
                            break Label_0405;
                        }
                        break;
                    }
                }
                ((Cursor)string).close();
                return list;
            }
            finally {
                searchIndexableRaw = (SearchIndexableRaw)query;
            }
        }
        ((Cursor)searchIndexableRaw).close();
    }
    
    @VisibleForTesting
    List<SearchIndexableResource> getIndexablesForXmlResourceUri(final Context context, final String packageName, final Uri uri, final String[] array) {
        final Cursor query = context.getContentResolver().query(uri, array, (String)null, (String[])null, (String)null);
        final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
        if (query == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot add index data for Uri: ");
            sb.append(uri.toString());
            Log.w("IndexableDataCollector", sb.toString());
            return list;
        }
        try {
            if (query.getCount() > 0) {
                while (query.moveToNext()) {
                    final int int1 = query.getInt(1);
                    final String string = query.getString(2);
                    final int int2 = query.getInt(3);
                    final String string2 = query.getString(4);
                    final String string3 = query.getString(5);
                    final String string4 = query.getString(6);
                    final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                    searchIndexableResource.xmlResId = int1;
                    searchIndexableResource.className = string;
                    searchIndexableResource.packageName = packageName;
                    searchIndexableResource.iconResId = int2;
                    searchIndexableResource.intentAction = string2;
                    searchIndexableResource.intentTargetPackage = string3;
                    searchIndexableResource.intentTargetClass = string4;
                    list.add(searchIndexableResource);
                }
            }
            return list;
        }
        finally {
            query.close();
        }
    }
    
    @VisibleForTesting
    List<String> getNonIndexablesKeysFromRemoteProvider(final String s, final String s2) {
        try {
            return this.getNonIndexablesKeys(this.mContext.createPackageContext(s, 0), this.buildUriForNonIndexableKeys(s2), SearchIndexablesContract.NON_INDEXABLES_KEYS_COLUMNS);
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not create context for ");
            sb.append(s);
            sb.append(": ");
            sb.append(Log.getStackTraceString((Throwable)ex));
            Log.w("IndexableDataCollector", sb.toString());
            return PreIndexDataCollector.EMPTY_LIST;
        }
    }
    
    @VisibleForTesting
    boolean isWellKnownProvider(final ResolveInfo resolveInfo) {
        final String authority = resolveInfo.providerInfo.authority;
        final String packageName = resolveInfo.providerInfo.applicationInfo.packageName;
        if (!TextUtils.isEmpty((CharSequence)authority) && !TextUtils.isEmpty((CharSequence)packageName)) {
            final String readPermission = resolveInfo.providerInfo.readPermission;
            final String writePermission = resolveInfo.providerInfo.writePermission;
            return !TextUtils.isEmpty((CharSequence)readPermission) && !TextUtils.isEmpty((CharSequence)writePermission) && ("android.permission.READ_SEARCH_INDEXABLES".equals(readPermission) && "android.permission.READ_SEARCH_INDEXABLES".equals(writePermission)) && this.isPrivilegedPackage(packageName, this.mContext);
        }
        return false;
    }
}
