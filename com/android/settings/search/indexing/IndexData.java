package com.android.settings.search.indexing;

import android.content.ComponentName;
import com.android.settings.search.DatabaseIndexingUtils;
import android.content.Intent;
import android.content.Context;
import com.android.settings.search.ResultPayload;
import java.util.Objects;
import android.text.TextUtils;
import java.text.Normalizer;
import com.android.settings.search.ResultPayloadUtils;
import java.util.Locale;
import java.util.regex.Pattern;

public class IndexData
{
    private static final Pattern REMOVE_DIACRITICALS_PATTERN;
    public final String childClassName;
    public final String className;
    public final boolean enabled;
    public final String entries;
    public final int iconResId;
    public final String intentAction;
    public final String intentTargetClass;
    public final String intentTargetPackage;
    public final String key;
    public final String locale;
    public final String normalizedSummaryOn;
    public final String normalizedTitle;
    public final byte[] payload;
    public final int payloadType;
    public final String screenTitle;
    public final String spaceDelimitedKeywords;
    public final String updatedSummaryOn;
    public final String updatedTitle;
    public final int userId;
    
    static {
        REMOVE_DIACRITICALS_PATTERN = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    }
    
    private IndexData(final Builder builder) {
        this.locale = Locale.getDefault().toString();
        this.updatedTitle = normalizeHyphen(builder.mTitle);
        this.updatedSummaryOn = normalizeHyphen(builder.mSummaryOn);
        if (Locale.JAPAN.toString().equalsIgnoreCase(this.locale)) {
            this.normalizedTitle = normalizeJapaneseString(builder.mTitle);
            this.normalizedSummaryOn = normalizeJapaneseString(builder.mSummaryOn);
        }
        else {
            this.normalizedTitle = normalizeString(builder.mTitle);
            this.normalizedSummaryOn = normalizeString(builder.mSummaryOn);
        }
        this.entries = builder.mEntries;
        this.className = builder.mClassName;
        this.childClassName = builder.mChildClassName;
        this.screenTitle = builder.mScreenTitle;
        this.iconResId = builder.mIconResId;
        this.spaceDelimitedKeywords = normalizeKeywords(builder.mKeywords);
        this.intentAction = builder.mIntentAction;
        this.intentTargetPackage = builder.mIntentTargetPackage;
        this.intentTargetClass = builder.mIntentTargetClass;
        this.enabled = builder.mEnabled;
        this.key = builder.mKey;
        this.userId = builder.mUserId;
        this.payloadType = builder.mPayloadType;
        byte[] marshall;
        if (builder.mPayload != null) {
            marshall = ResultPayloadUtils.marshall(builder.mPayload);
        }
        else {
            marshall = null;
        }
        this.payload = marshall;
    }
    
    public static String normalizeHyphen(String replaceAll) {
        if (replaceAll != null) {
            replaceAll = replaceAll.replaceAll("\u2011", "-");
        }
        else {
            replaceAll = "";
        }
        return replaceAll;
    }
    
    public static String normalizeJapaneseString(String s) {
        if (s != null) {
            s = s.replaceAll("-", "");
        }
        else {
            s = "";
        }
        s = Normalizer.normalize(s, Normalizer.Form.NFKD);
        final StringBuffer sb = new StringBuffer();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 >= '\u3041' && char1 <= '\u3096') {
                sb.append((char)(char1 - '\u3041' + '\u30a1'));
            }
            else {
                sb.append(char1);
            }
        }
        return IndexData.REMOVE_DIACRITICALS_PATTERN.matcher(sb.toString()).replaceAll("").toLowerCase();
    }
    
    public static String normalizeKeywords(String replaceAll) {
        if (replaceAll != null) {
            replaceAll = replaceAll.replaceAll("[,]\\s*", " ");
        }
        else {
            replaceAll = "";
        }
        return replaceAll;
    }
    
    public static String normalizeString(String s) {
        final String normalizeHyphen = normalizeHyphen(s);
        if (s != null) {
            s = normalizeHyphen.replaceAll("-", "");
        }
        else {
            s = "";
        }
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        return IndexData.REMOVE_DIACRITICALS_PATTERN.matcher(s).replaceAll("").toLowerCase();
    }
    
    public int getDocId() {
        int n;
        if (TextUtils.isEmpty((CharSequence)this.key)) {
            n = Objects.hash(this.updatedTitle, this.className, this.screenTitle, this.intentTargetClass);
        }
        else {
            n = this.key.hashCode();
        }
        return n;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.updatedTitle);
        sb.append(": ");
        sb.append(this.updatedSummaryOn);
        return sb.toString();
    }
    
    public static class Builder
    {
        private String mChildClassName;
        private String mClassName;
        private boolean mEnabled;
        private String mEntries;
        private int mIconResId;
        private String mIntentAction;
        private String mIntentTargetClass;
        private String mIntentTargetPackage;
        private String mKey;
        private String mKeywords;
        private ResultPayload mPayload;
        private int mPayloadType;
        private String mScreenTitle;
        private String mSummaryOn;
        private String mTitle;
        private int mUserId;
        
        private Intent buildIntent(final Context context) {
            Intent buildSearchResultPageIntent;
            if (TextUtils.isEmpty((CharSequence)this.mIntentAction)) {
                buildSearchResultPageIntent = DatabaseIndexingUtils.buildSearchResultPageIntent(context, this.mClassName, this.mKey, this.mScreenTitle);
            }
            else {
                buildSearchResultPageIntent = new Intent(this.mIntentAction);
                final String mIntentTargetClass = this.mIntentTargetClass;
                if (!TextUtils.isEmpty((CharSequence)this.mIntentTargetPackage) && !TextUtils.isEmpty((CharSequence)mIntentTargetClass)) {
                    buildSearchResultPageIntent.setComponent(new ComponentName(this.mIntentTargetPackage, mIntentTargetClass));
                }
                buildSearchResultPageIntent.putExtra(":settings:fragment_args_key", this.mKey);
            }
            return buildSearchResultPageIntent;
        }
        
        private void setIntent(final Context context) {
            if (this.mPayload != null) {
                return;
            }
            this.mPayload = new ResultPayload(this.buildIntent(context));
            this.mPayloadType = 0;
        }
        
        private Builder setPayloadType(final int mPayloadType) {
            this.mPayloadType = mPayloadType;
            return this;
        }
        
        public IndexData build(final Context intent) {
            this.setIntent(intent);
            return new IndexData(this, null);
        }
        
        public Builder setChildClassName(final String mChildClassName) {
            this.mChildClassName = mChildClassName;
            return this;
        }
        
        public Builder setClassName(final String mClassName) {
            this.mClassName = mClassName;
            return this;
        }
        
        public Builder setEnabled(final boolean mEnabled) {
            this.mEnabled = mEnabled;
            return this;
        }
        
        public Builder setEntries(final String mEntries) {
            this.mEntries = mEntries;
            return this;
        }
        
        public Builder setIconResId(final int mIconResId) {
            this.mIconResId = mIconResId;
            return this;
        }
        
        public Builder setIntentAction(final String mIntentAction) {
            this.mIntentAction = mIntentAction;
            return this;
        }
        
        public Builder setIntentTargetClass(final String mIntentTargetClass) {
            this.mIntentTargetClass = mIntentTargetClass;
            return this;
        }
        
        public Builder setIntentTargetPackage(final String mIntentTargetPackage) {
            this.mIntentTargetPackage = mIntentTargetPackage;
            return this;
        }
        
        public Builder setKey(final String mKey) {
            this.mKey = mKey;
            return this;
        }
        
        public Builder setKeywords(final String mKeywords) {
            this.mKeywords = mKeywords;
            return this;
        }
        
        public Builder setPayload(final ResultPayload mPayload) {
            this.mPayload = mPayload;
            if (this.mPayload != null) {
                this.setPayloadType(this.mPayload.getType());
            }
            return this;
        }
        
        public Builder setScreenTitle(final String mScreenTitle) {
            this.mScreenTitle = mScreenTitle;
            return this;
        }
        
        public Builder setSummaryOn(final String mSummaryOn) {
            this.mSummaryOn = mSummaryOn;
            return this;
        }
        
        public Builder setTitle(final String mTitle) {
            this.mTitle = mTitle;
            return this;
        }
        
        public Builder setUserId(final int mUserId) {
            this.mUserId = mUserId;
            return this;
        }
    }
}
