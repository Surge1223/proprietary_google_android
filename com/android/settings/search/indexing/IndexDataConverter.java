package com.android.settings.search.indexing;

import java.util.Iterator;
import android.util.Log;
import java.util.Collection;
import android.provider.SearchIndexableData;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.List;
import android.provider.SearchIndexableResource;
import java.util.Set;
import com.android.settings.search.SearchIndexableRaw;
import android.content.Context;

public class IndexDataConverter
{
    private final Context mContext;
    
    public IndexDataConverter(final Context mContext) {
        this.mContext = mContext;
    }
    
    private IndexData.Builder convertRaw(final SearchIndexableRaw searchIndexableRaw, final Set<String> set) {
        final boolean enabled = set == null || !set.contains(searchIndexableRaw.key);
        final IndexData.Builder builder = new IndexData.Builder();
        builder.setTitle(searchIndexableRaw.title).setSummaryOn(searchIndexableRaw.summaryOn).setEntries(searchIndexableRaw.entries).setKeywords(searchIndexableRaw.keywords).setClassName(searchIndexableRaw.className).setScreenTitle(searchIndexableRaw.screenTitle).setIconResId(searchIndexableRaw.iconResId).setIntentAction(searchIndexableRaw.intentAction).setIntentTargetPackage(searchIndexableRaw.intentTargetPackage).setIntentTargetClass(searchIndexableRaw.intentTargetClass).setEnabled(enabled).setKey(searchIndexableRaw.key).setUserId(searchIndexableRaw.userId);
        return builder;
    }
    
    private List<IndexData> convertResource(final SearchIndexableResource p0, final Set<String> p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        android/provider/SearchIndexableResource.context:Landroid/content/Context;
        //     4: astore_3       
        //     5: aconst_null    
        //     6: astore          4
        //     8: aconst_null    
        //     9: astore          5
        //    11: aconst_null    
        //    12: astore          6
        //    14: aconst_null    
        //    15: astore          7
        //    17: new             Ljava/util/ArrayList;
        //    20: dup            
        //    21: invokespecial   java/util/ArrayList.<init>:()V
        //    24: astore          8
        //    26: aload_3        
        //    27: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //    30: aload_1        
        //    31: getfield        android/provider/SearchIndexableResource.xmlResId:I
        //    34: invokevirtual   android/content/res/Resources.getXml:(I)Landroid/content/res/XmlResourceParser;
        //    37: astore          9
        //    39: aload           9
        //    41: astore          7
        //    43: aload           9
        //    45: astore          4
        //    47: aload           9
        //    49: astore          5
        //    51: aload           9
        //    53: astore          6
        //    55: aload           9
        //    57: invokeinterface android/content/res/XmlResourceParser.next:()I
        //    62: istore          10
        //    64: iload           10
        //    66: iconst_1       
        //    67: if_icmpeq       79
        //    70: iload           10
        //    72: iconst_2       
        //    73: if_icmpeq       79
        //    76: goto            39
        //    79: aload           9
        //    81: astore          7
        //    83: aload           9
        //    85: astore          4
        //    87: aload           9
        //    89: astore          5
        //    91: aload           9
        //    93: astore          6
        //    95: aload           9
        //    97: invokeinterface android/content/res/XmlResourceParser.getName:()Ljava/lang/String;
        //   102: astore          11
        //   104: aload           9
        //   106: astore          7
        //   108: aload           9
        //   110: astore          4
        //   112: aload           9
        //   114: astore          5
        //   116: aload           9
        //   118: astore          6
        //   120: ldc             "PreferenceScreen"
        //   122: aload           11
        //   124: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   127: ifeq            1451
        //   130: aload           9
        //   132: astore          7
        //   134: aload           9
        //   136: astore          4
        //   138: aload           9
        //   140: astore          5
        //   142: aload           9
        //   144: astore          6
        //   146: aload           9
        //   148: invokeinterface android/content/res/XmlResourceParser.getDepth:()I
        //   153: istore          10
        //   155: aload           9
        //   157: astore          7
        //   159: aload           9
        //   161: astore          4
        //   163: aload           9
        //   165: astore          5
        //   167: aload           9
        //   169: astore          6
        //   171: aload           9
        //   173: invokestatic    android/util/Xml.asAttributeSet:(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //   176: astore          12
        //   178: aload           9
        //   180: astore          7
        //   182: aload           9
        //   184: astore          4
        //   186: aload           9
        //   188: astore          5
        //   190: aload           9
        //   192: astore          6
        //   194: aload_3        
        //   195: aload           12
        //   197: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataTitle:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   200: astore          13
        //   202: aload           9
        //   204: astore          7
        //   206: aload           9
        //   208: astore          4
        //   210: aload           9
        //   212: astore          5
        //   214: aload           9
        //   216: astore          6
        //   218: aload_3        
        //   219: aload           12
        //   221: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataKey:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   224: astore          14
        //   226: aload           9
        //   228: astore          7
        //   230: aload           9
        //   232: astore          4
        //   234: aload           9
        //   236: astore          5
        //   238: aload           9
        //   240: astore          6
        //   242: aload_1        
        //   243: getfield        android/provider/SearchIndexableResource.className:Ljava/lang/String;
        //   246: astore          15
        //   248: aload           9
        //   250: astore          7
        //   252: aload           9
        //   254: astore          4
        //   256: aload           9
        //   258: astore          5
        //   260: aload           9
        //   262: astore          6
        //   264: aload_1        
        //   265: getfield        android/provider/SearchIndexableResource.intentAction:Ljava/lang/String;
        //   268: astore          16
        //   270: aload           9
        //   272: astore          7
        //   274: aload           9
        //   276: astore          4
        //   278: aload           9
        //   280: astore          5
        //   282: aload           9
        //   284: astore          6
        //   286: aload_1        
        //   287: getfield        android/provider/SearchIndexableResource.intentTargetPackage:Ljava/lang/String;
        //   290: astore          17
        //   292: aload           9
        //   294: astore          7
        //   296: aload           9
        //   298: astore          4
        //   300: aload           9
        //   302: astore          5
        //   304: aload           9
        //   306: astore          6
        //   308: aload_1        
        //   309: getfield        android/provider/SearchIndexableResource.intentTargetClass:Ljava/lang/String;
        //   312: astore          18
        //   314: aload           9
        //   316: astore          7
        //   318: aload           9
        //   320: astore          4
        //   322: aload           9
        //   324: astore          5
        //   326: aload           9
        //   328: astore          6
        //   330: new             Ljava/util/HashMap;
        //   333: astore          11
        //   335: aload           9
        //   337: astore          7
        //   339: aload           9
        //   341: astore          4
        //   343: aload           9
        //   345: astore          5
        //   347: aload           9
        //   349: astore          6
        //   351: aload           11
        //   353: invokespecial   java/util/HashMap.<init>:()V
        //   356: aload           15
        //   358: ifnull          397
        //   361: aload           15
        //   363: aload_3        
        //   364: invokestatic    com/android/settings/search/DatabaseIndexingUtils.getPayloadKeyMap:(Ljava/lang/String;Landroid/content/Context;)Ljava/util/Map;
        //   367: astore          11
        //   369: goto            397
        //   372: astore_1       
        //   373: goto            1770
        //   376: astore_2       
        //   377: aload           8
        //   379: astore_1       
        //   380: goto            1672
        //   383: astore_2       
        //   384: aload           8
        //   386: astore_1       
        //   387: goto            1705
        //   390: astore_2       
        //   391: aload           8
        //   393: astore_1       
        //   394: goto            1738
        //   397: aload           9
        //   399: astore          7
        //   401: aload           9
        //   403: astore          4
        //   405: aload           9
        //   407: astore          5
        //   409: aload           9
        //   411: astore          6
        //   413: aload_3        
        //   414: aload           12
        //   416: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataTitle:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   419: astore          19
        //   421: aload           9
        //   423: astore          7
        //   425: aload           9
        //   427: astore          4
        //   429: aload           9
        //   431: astore          5
        //   433: aload           9
        //   435: astore          6
        //   437: aload_3        
        //   438: aload           12
        //   440: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataSummary:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   443: astore          20
        //   445: aload           9
        //   447: astore          7
        //   449: aload           9
        //   451: astore          4
        //   453: aload           9
        //   455: astore          5
        //   457: aload           9
        //   459: astore          6
        //   461: aload_3        
        //   462: aload           12
        //   464: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataKeywords:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   467: astore          21
        //   469: aload           9
        //   471: astore          7
        //   473: aload           9
        //   475: astore          4
        //   477: aload           9
        //   479: astore          5
        //   481: aload           9
        //   483: astore          6
        //   485: aload_2        
        //   486: aload           14
        //   488: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //   493: istore          22
        //   495: aload           9
        //   497: astore          7
        //   499: aload           9
        //   501: astore          4
        //   503: aload           9
        //   505: astore          5
        //   507: aload           9
        //   509: astore          6
        //   511: new             Lcom/android/settings/search/indexing/IndexData$Builder;
        //   514: astore          23
        //   516: aload           9
        //   518: astore          7
        //   520: aload           9
        //   522: astore          4
        //   524: aload           9
        //   526: astore          5
        //   528: aload           9
        //   530: astore          6
        //   532: aload           23
        //   534: invokespecial   com/android/settings/search/indexing/IndexData$Builder.<init>:()V
        //   537: aload           8
        //   539: astore_1       
        //   540: aload_1        
        //   541: astore          8
        //   543: aload_1        
        //   544: astore          8
        //   546: aload_1        
        //   547: astore          7
        //   549: aload_1        
        //   550: astore          4
        //   552: aload           23
        //   554: aload           19
        //   556: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setTitle:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   559: aload           20
        //   561: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setSummaryOn:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   564: aload           21
        //   566: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setKeywords:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   569: aload           15
        //   571: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setClassName:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   574: aload           13
        //   576: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setScreenTitle:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   579: aload           16
        //   581: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIntentAction:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   584: aload           17
        //   586: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIntentTargetPackage:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   589: aload           18
        //   591: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIntentTargetClass:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   594: iload           22
        //   596: iconst_1       
        //   597: ixor           
        //   598: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setEnabled:(Z)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   601: aload           14
        //   603: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setKey:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   606: iconst_m1      
        //   607: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setUserId:(I)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   610: pop            
        //   611: iconst_1       
        //   612: istore          24
        //   614: aload           14
        //   616: astore          5
        //   618: aload_1        
        //   619: astore          8
        //   621: aload_1        
        //   622: astore          8
        //   624: aload_1        
        //   625: astore          7
        //   627: aload_1        
        //   628: astore          4
        //   630: aload           9
        //   632: invokeinterface android/content/res/XmlResourceParser.next:()I
        //   637: istore          25
        //   639: iload           25
        //   641: iconst_1       
        //   642: if_icmpeq       1373
        //   645: iload           25
        //   647: iconst_3       
        //   648: if_icmpne       689
        //   651: aload           9
        //   653: invokeinterface android/content/res/XmlResourceParser.getDepth:()I
        //   658: istore          26
        //   660: iload           26
        //   662: iload           10
        //   664: if_icmple       670
        //   667: goto            689
        //   670: goto            1373
        //   673: astore_1       
        //   674: goto            1770
        //   677: astore_2       
        //   678: goto            1672
        //   681: astore_2       
        //   682: goto            1705
        //   685: astore_2       
        //   686: goto            1738
        //   689: iload           25
        //   691: iconst_3       
        //   692: if_icmpeq       1370
        //   695: iload           25
        //   697: iconst_4       
        //   698: if_icmpne       704
        //   701: goto            1370
        //   704: aload_1        
        //   705: astore          8
        //   707: aload_1        
        //   708: astore          8
        //   710: aload_1        
        //   711: astore          7
        //   713: aload_1        
        //   714: astore          4
        //   716: aload           9
        //   718: invokeinterface android/content/res/XmlResourceParser.getName:()Ljava/lang/String;
        //   723: astore          5
        //   725: aload_1        
        //   726: astore          8
        //   728: aload_1        
        //   729: astore          8
        //   731: aload_1        
        //   732: astore          7
        //   734: aload_1        
        //   735: astore          4
        //   737: aload_3        
        //   738: aload           12
        //   740: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataTitle:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   743: astore          14
        //   745: aload_1        
        //   746: astore          8
        //   748: aload_1        
        //   749: astore          8
        //   751: aload_1        
        //   752: astore          7
        //   754: aload_1        
        //   755: astore          4
        //   757: aload_3        
        //   758: aload           12
        //   760: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataKey:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   763: astore          20
        //   765: aload_1        
        //   766: astore          8
        //   768: aload_1        
        //   769: astore          8
        //   771: aload_1        
        //   772: astore          7
        //   774: aload_1        
        //   775: astore          4
        //   777: aload_2        
        //   778: aload           20
        //   780: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //   785: istore          22
        //   787: aload_1        
        //   788: astore          8
        //   790: aload_1        
        //   791: astore          8
        //   793: aload_1        
        //   794: astore          7
        //   796: aload_1        
        //   797: astore          4
        //   799: aload_3        
        //   800: aload           12
        //   802: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataKeywords:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   805: astore          6
        //   807: aload_1        
        //   808: astore          8
        //   810: aload_1        
        //   811: astore          8
        //   813: aload_1        
        //   814: astore          7
        //   816: aload_1        
        //   817: astore          4
        //   819: aload_3        
        //   820: aload           12
        //   822: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataIcon:(Landroid/content/Context;Landroid/util/AttributeSet;)I
        //   825: istore          26
        //   827: iload           24
        //   829: ifeq            852
        //   832: aload           19
        //   834: aload           14
        //   836: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   839: istore          27
        //   841: iload           27
        //   843: ifeq            852
        //   846: iconst_0       
        //   847: istore          24
        //   849: goto            852
        //   852: aload_1        
        //   853: astore          8
        //   855: aload_1        
        //   856: astore          8
        //   858: aload_1        
        //   859: astore          7
        //   861: aload_1        
        //   862: astore          4
        //   864: new             Lcom/android/settings/search/indexing/IndexData$Builder;
        //   867: astore          28
        //   869: aload_1        
        //   870: astore          8
        //   872: aload_1        
        //   873: astore          8
        //   875: aload_1        
        //   876: astore          7
        //   878: aload_1        
        //   879: astore          4
        //   881: aload           28
        //   883: invokespecial   com/android/settings/search/indexing/IndexData$Builder.<init>:()V
        //   886: aload_1        
        //   887: astore          8
        //   889: aload_1        
        //   890: astore          8
        //   892: aload_1        
        //   893: astore          7
        //   895: aload_1        
        //   896: astore          4
        //   898: aload           28
        //   900: aload           14
        //   902: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setTitle:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   905: aload           6
        //   907: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setKeywords:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   910: aload           15
        //   912: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setClassName:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   915: aload           13
        //   917: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setScreenTitle:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   920: iload           26
        //   922: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIconResId:(I)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   925: aload           16
        //   927: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIntentAction:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   930: aload           17
        //   932: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIntentTargetPackage:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   935: aload           18
        //   937: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setIntentTargetClass:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   940: iload           22
        //   942: iconst_1       
        //   943: ixor           
        //   944: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setEnabled:(Z)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   947: aload           20
        //   949: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setKey:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   952: iconst_m1      
        //   953: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setUserId:(I)Lcom/android/settings/search/indexing/IndexData$Builder;
        //   956: pop            
        //   957: aload_1        
        //   958: astore          8
        //   960: aload_1        
        //   961: astore          8
        //   963: aload_1        
        //   964: astore          7
        //   966: aload_1        
        //   967: astore          4
        //   969: aload           5
        //   971: ldc             "CheckBoxPreference"
        //   973: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   976: ifne            1175
        //   979: aload_1        
        //   980: astore          8
        //   982: aload_1        
        //   983: astore          8
        //   985: aload_1        
        //   986: astore          7
        //   988: aload_1        
        //   989: astore          4
        //   991: aload_3        
        //   992: aload           12
        //   994: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataSummary:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //   997: astore          6
        //   999: aload_1        
        //  1000: astore          8
        //  1002: aload_1        
        //  1003: astore          8
        //  1005: aload_1        
        //  1006: astore          7
        //  1008: aload_1        
        //  1009: astore          4
        //  1011: aload           5
        //  1013: ldc             "ListPreference"
        //  1015: invokevirtual   java/lang/String.endsWith:(Ljava/lang/String;)Z
        //  1018: istore          22
        //  1020: iload           22
        //  1022: ifeq            1036
        //  1025: aload_3        
        //  1026: aload           12
        //  1028: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataEntries:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //  1031: astore          5
        //  1033: goto            1039
        //  1036: aconst_null    
        //  1037: astore          5
        //  1039: aload_1        
        //  1040: astore          8
        //  1042: aload_1        
        //  1043: astore          8
        //  1045: aload_1        
        //  1046: astore          7
        //  1048: aload_1        
        //  1049: astore          4
        //  1051: aload           11
        //  1053: aload           20
        //  1055: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //  1060: checkcast       Lcom/android/settings/search/ResultPayload;
        //  1063: astore          14
        //  1065: aload_1        
        //  1066: astore          8
        //  1068: aload_1        
        //  1069: astore          8
        //  1071: aload_1        
        //  1072: astore          7
        //  1074: aload_1        
        //  1075: astore          4
        //  1077: aload_3        
        //  1078: aload           12
        //  1080: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataChildFragment:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //  1083: astore          21
        //  1085: aload_1        
        //  1086: astore          8
        //  1088: aload_1        
        //  1089: astore          8
        //  1091: aload_1        
        //  1092: astore          7
        //  1094: aload_1        
        //  1095: astore          4
        //  1097: aload           28
        //  1099: aload           6
        //  1101: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setSummaryOn:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //  1104: aload           5
        //  1106: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setEntries:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //  1109: aload           21
        //  1111: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setChildClassName:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //  1114: aload           14
        //  1116: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setPayload:(Lcom/android/settings/search/ResultPayload;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //  1119: pop            
        //  1120: aload_1        
        //  1121: astore          8
        //  1123: aload_1        
        //  1124: astore          8
        //  1126: aload_1        
        //  1127: astore          7
        //  1129: aload_1        
        //  1130: astore          4
        //  1132: aload           28
        //  1134: aload_0        
        //  1135: getfield        com/android/settings/search/indexing/IndexDataConverter.mContext:Landroid/content/Context;
        //  1138: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.build:(Landroid/content/Context;)Lcom/android/settings/search/indexing/IndexData;
        //  1141: astore          14
        //  1143: aload_1        
        //  1144: astore          6
        //  1146: aload           6
        //  1148: astore          5
        //  1150: aload           6
        //  1152: astore          7
        //  1154: aload           6
        //  1156: astore          4
        //  1158: aload           9
        //  1160: astore          8
        //  1162: aload           6
        //  1164: aload           14
        //  1166: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //  1171: pop            
        //  1172: goto            1363
        //  1175: aload_1        
        //  1176: astore          6
        //  1178: aload           6
        //  1180: astore          5
        //  1182: aload           6
        //  1184: astore          7
        //  1186: aload           6
        //  1188: astore          4
        //  1190: aload           9
        //  1192: astore          8
        //  1194: aload_3        
        //  1195: aload           12
        //  1197: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataSummaryOn:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //  1200: astore          21
        //  1202: aload           6
        //  1204: astore          5
        //  1206: aload           6
        //  1208: astore          7
        //  1210: aload           6
        //  1212: astore          4
        //  1214: aload           9
        //  1216: astore          8
        //  1218: aload_3        
        //  1219: aload           12
        //  1221: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataSummaryOff:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //  1224: astore          29
        //  1226: aload           21
        //  1228: astore          14
        //  1230: aload           6
        //  1232: astore          5
        //  1234: aload           6
        //  1236: astore          7
        //  1238: aload           6
        //  1240: astore          4
        //  1242: aload           9
        //  1244: astore          8
        //  1246: aload           21
        //  1248: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //  1251: ifeq            1306
        //  1254: aload           21
        //  1256: astore          14
        //  1258: aload           6
        //  1260: astore          5
        //  1262: aload           6
        //  1264: astore          7
        //  1266: aload           6
        //  1268: astore          4
        //  1270: aload           9
        //  1272: astore          8
        //  1274: aload           29
        //  1276: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //  1279: ifeq            1306
        //  1282: aload           6
        //  1284: astore          5
        //  1286: aload           6
        //  1288: astore          7
        //  1290: aload           6
        //  1292: astore          4
        //  1294: aload           9
        //  1296: astore          8
        //  1298: aload_3        
        //  1299: aload           12
        //  1301: invokestatic    com/android/settings/core/PreferenceXmlParserUtils.getDataSummary:(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
        //  1304: astore          14
        //  1306: aload           6
        //  1308: astore          5
        //  1310: aload           6
        //  1312: astore          7
        //  1314: aload           6
        //  1316: astore          4
        //  1318: aload           9
        //  1320: astore          8
        //  1322: aload           28
        //  1324: aload           14
        //  1326: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.setSummaryOn:(Ljava/lang/String;)Lcom/android/settings/search/indexing/IndexData$Builder;
        //  1329: pop            
        //  1330: aload           6
        //  1332: astore          5
        //  1334: aload           6
        //  1336: astore          7
        //  1338: aload           6
        //  1340: astore          4
        //  1342: aload           9
        //  1344: astore          8
        //  1346: aload           6
        //  1348: aload           28
        //  1350: aload_0        
        //  1351: getfield        com/android/settings/search/indexing/IndexDataConverter.mContext:Landroid/content/Context;
        //  1354: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.build:(Landroid/content/Context;)Lcom/android/settings/search/indexing/IndexData;
        //  1357: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //  1362: pop            
        //  1363: aload           20
        //  1365: astore          5
        //  1367: goto            618
        //  1370: goto            618
        //  1373: iload           24
        //  1375: ifeq            1407
        //  1378: aload_1        
        //  1379: astore          5
        //  1381: aload_1        
        //  1382: astore          7
        //  1384: aload_1        
        //  1385: astore          4
        //  1387: aload           9
        //  1389: astore          8
        //  1391: aload_1        
        //  1392: aload           23
        //  1394: aload_0        
        //  1395: getfield        com/android/settings/search/indexing/IndexDataConverter.mContext:Landroid/content/Context;
        //  1398: invokevirtual   com/android/settings/search/indexing/IndexData$Builder.build:(Landroid/content/Context;)Lcom/android/settings/search/indexing/IndexData;
        //  1401: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //  1406: pop            
        //  1407: aload_1        
        //  1408: astore_2       
        //  1409: aload           9
        //  1411: ifnull          1763
        //  1414: aload           9
        //  1416: invokeinterface android/content/res/XmlResourceParser.close:()V
        //  1421: aload_1        
        //  1422: astore_2       
        //  1423: goto            1763
        //  1426: astore_1       
        //  1427: goto            1770
        //  1430: astore_2       
        //  1431: aload           8
        //  1433: astore_1       
        //  1434: goto            1672
        //  1437: astore_2       
        //  1438: aload           7
        //  1440: astore_1       
        //  1441: goto            1705
        //  1444: astore_2       
        //  1445: aload           4
        //  1447: astore_1       
        //  1448: goto            1738
        //  1451: aload           8
        //  1453: astore_1       
        //  1454: aload_1        
        //  1455: astore          5
        //  1457: aload_1        
        //  1458: astore          7
        //  1460: aload_1        
        //  1461: astore          4
        //  1463: aload           9
        //  1465: astore          8
        //  1467: new             Ljava/lang/RuntimeException;
        //  1470: astore_2       
        //  1471: aload_1        
        //  1472: astore          5
        //  1474: aload_1        
        //  1475: astore          7
        //  1477: aload_1        
        //  1478: astore          4
        //  1480: aload           9
        //  1482: astore          8
        //  1484: new             Ljava/lang/StringBuilder;
        //  1487: astore          6
        //  1489: aload_1        
        //  1490: astore          5
        //  1492: aload_1        
        //  1493: astore          7
        //  1495: aload_1        
        //  1496: astore          4
        //  1498: aload           9
        //  1500: astore          8
        //  1502: aload           6
        //  1504: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1507: aload_1        
        //  1508: astore          5
        //  1510: aload_1        
        //  1511: astore          7
        //  1513: aload_1        
        //  1514: astore          4
        //  1516: aload           9
        //  1518: astore          8
        //  1520: aload           6
        //  1522: ldc_w           "XML document must start with <PreferenceScreen> tag; found"
        //  1525: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1528: pop            
        //  1529: aload_1        
        //  1530: astore          5
        //  1532: aload_1        
        //  1533: astore          7
        //  1535: aload_1        
        //  1536: astore          4
        //  1538: aload           9
        //  1540: astore          8
        //  1542: aload           6
        //  1544: aload           11
        //  1546: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1549: pop            
        //  1550: aload_1        
        //  1551: astore          5
        //  1553: aload_1        
        //  1554: astore          7
        //  1556: aload_1        
        //  1557: astore          4
        //  1559: aload           9
        //  1561: astore          8
        //  1563: aload           6
        //  1565: ldc_w           " at "
        //  1568: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1571: pop            
        //  1572: aload_1        
        //  1573: astore          5
        //  1575: aload_1        
        //  1576: astore          7
        //  1578: aload_1        
        //  1579: astore          4
        //  1581: aload           9
        //  1583: astore          8
        //  1585: aload           6
        //  1587: aload           9
        //  1589: invokeinterface android/content/res/XmlResourceParser.getPositionDescription:()Ljava/lang/String;
        //  1594: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1597: pop            
        //  1598: aload_1        
        //  1599: astore          5
        //  1601: aload_1        
        //  1602: astore          7
        //  1604: aload_1        
        //  1605: astore          4
        //  1607: aload           9
        //  1609: astore          8
        //  1611: aload_2        
        //  1612: aload           6
        //  1614: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1617: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //  1620: aload_1        
        //  1621: astore          5
        //  1623: aload_1        
        //  1624: astore          7
        //  1626: aload_1        
        //  1627: astore          4
        //  1629: aload           9
        //  1631: astore          8
        //  1633: aload_2        
        //  1634: athrow         
        //  1635: astore_2       
        //  1636: aload           5
        //  1638: astore_1       
        //  1639: goto            1672
        //  1642: astore_2       
        //  1643: aload           7
        //  1645: astore_1       
        //  1646: goto            1705
        //  1649: astore_2       
        //  1650: aload           4
        //  1652: astore_1       
        //  1653: goto            1738
        //  1656: astore_1       
        //  1657: aload           7
        //  1659: astore          9
        //  1661: goto            1770
        //  1664: astore_2       
        //  1665: aload           8
        //  1667: astore_1       
        //  1668: aload           4
        //  1670: astore          9
        //  1672: aload           9
        //  1674: astore          8
        //  1676: ldc_w           "IndexDataConverter"
        //  1679: ldc_w           "Resoucre not found error parsing PreferenceScreen: "
        //  1682: aload_2        
        //  1683: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //  1686: pop            
        //  1687: aload_1        
        //  1688: astore_2       
        //  1689: aload           9
        //  1691: ifnull          1763
        //  1694: goto            1414
        //  1697: astore_2       
        //  1698: aload           8
        //  1700: astore_1       
        //  1701: aload           5
        //  1703: astore          9
        //  1705: aload           9
        //  1707: astore          8
        //  1709: ldc_w           "IndexDataConverter"
        //  1712: ldc_w           "IO Error parsing PreferenceScreen: "
        //  1715: aload_2        
        //  1716: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //  1719: pop            
        //  1720: aload_1        
        //  1721: astore_2       
        //  1722: aload           9
        //  1724: ifnull          1763
        //  1727: goto            1414
        //  1730: astore_2       
        //  1731: aload           8
        //  1733: astore_1       
        //  1734: aload           6
        //  1736: astore          9
        //  1738: aload           9
        //  1740: astore          8
        //  1742: ldc_w           "IndexDataConverter"
        //  1745: ldc_w           "XML Error parsing PreferenceScreen: "
        //  1748: aload_2        
        //  1749: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //  1752: pop            
        //  1753: aload_1        
        //  1754: astore_2       
        //  1755: aload           9
        //  1757: ifnull          1763
        //  1760: goto            1414
        //  1763: aload_2        
        //  1764: areturn        
        //  1765: astore_1       
        //  1766: aload           8
        //  1768: astore          9
        //  1770: aload           9
        //  1772: ifnull          1782
        //  1775: aload           9
        //  1777: invokeinterface android/content/res/XmlResourceParser.close:()V
        //  1782: aload_1        
        //  1783: athrow         
        //    Signature:
        //  (Landroid/provider/SearchIndexableResource;Ljava/util/Set<Ljava/lang/String;>;)Ljava/util/List<Lcom/android/settings/search/indexing/IndexData;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                             
        //  -----  -----  -----  -----  -------------------------------------------------
        //  26     39     1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  26     39     1697   1705   Ljava/io/IOException;
        //  26     39     1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  26     39     1656   1664   Any
        //  55     64     1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  55     64     1697   1705   Ljava/io/IOException;
        //  55     64     1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  55     64     1656   1664   Any
        //  95     104    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  95     104    1697   1705   Ljava/io/IOException;
        //  95     104    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  95     104    1656   1664   Any
        //  120    130    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  120    130    1697   1705   Ljava/io/IOException;
        //  120    130    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  120    130    1656   1664   Any
        //  146    155    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  146    155    1697   1705   Ljava/io/IOException;
        //  146    155    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  146    155    1656   1664   Any
        //  171    178    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  171    178    1697   1705   Ljava/io/IOException;
        //  171    178    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  171    178    1656   1664   Any
        //  194    202    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  194    202    1697   1705   Ljava/io/IOException;
        //  194    202    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  194    202    1656   1664   Any
        //  218    226    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  218    226    1697   1705   Ljava/io/IOException;
        //  218    226    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  218    226    1656   1664   Any
        //  242    248    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  242    248    1697   1705   Ljava/io/IOException;
        //  242    248    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  242    248    1656   1664   Any
        //  264    270    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  264    270    1697   1705   Ljava/io/IOException;
        //  264    270    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  264    270    1656   1664   Any
        //  286    292    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  286    292    1697   1705   Ljava/io/IOException;
        //  286    292    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  286    292    1656   1664   Any
        //  308    314    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  308    314    1697   1705   Ljava/io/IOException;
        //  308    314    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  308    314    1656   1664   Any
        //  330    335    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  330    335    1697   1705   Ljava/io/IOException;
        //  330    335    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  330    335    1656   1664   Any
        //  351    356    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  351    356    1697   1705   Ljava/io/IOException;
        //  351    356    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  351    356    1656   1664   Any
        //  361    369    390    397    Lorg/xmlpull/v1/XmlPullParserException;
        //  361    369    383    390    Ljava/io/IOException;
        //  361    369    376    383    Landroid/content/res/Resources$NotFoundException;
        //  361    369    372    376    Any
        //  413    421    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  413    421    1697   1705   Ljava/io/IOException;
        //  413    421    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  413    421    1656   1664   Any
        //  437    445    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  437    445    1697   1705   Ljava/io/IOException;
        //  437    445    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  437    445    1656   1664   Any
        //  461    469    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  461    469    1697   1705   Ljava/io/IOException;
        //  461    469    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  461    469    1656   1664   Any
        //  485    495    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  485    495    1697   1705   Ljava/io/IOException;
        //  485    495    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  485    495    1656   1664   Any
        //  511    516    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  511    516    1697   1705   Ljava/io/IOException;
        //  511    516    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  511    516    1656   1664   Any
        //  532    537    1730   1738   Lorg/xmlpull/v1/XmlPullParserException;
        //  532    537    1697   1705   Ljava/io/IOException;
        //  532    537    1664   1672   Landroid/content/res/Resources$NotFoundException;
        //  532    537    1656   1664   Any
        //  552    611    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  552    611    1437   1444   Ljava/io/IOException;
        //  552    611    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  552    611    1426   1430   Any
        //  630    639    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  630    639    1437   1444   Ljava/io/IOException;
        //  630    639    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  630    639    1426   1430   Any
        //  651    660    685    689    Lorg/xmlpull/v1/XmlPullParserException;
        //  651    660    681    685    Ljava/io/IOException;
        //  651    660    677    681    Landroid/content/res/Resources$NotFoundException;
        //  651    660    673    677    Any
        //  716    725    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  716    725    1437   1444   Ljava/io/IOException;
        //  716    725    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  716    725    1426   1430   Any
        //  737    745    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  737    745    1437   1444   Ljava/io/IOException;
        //  737    745    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  737    745    1426   1430   Any
        //  757    765    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  757    765    1437   1444   Ljava/io/IOException;
        //  757    765    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  757    765    1426   1430   Any
        //  777    787    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  777    787    1437   1444   Ljava/io/IOException;
        //  777    787    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  777    787    1426   1430   Any
        //  799    807    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  799    807    1437   1444   Ljava/io/IOException;
        //  799    807    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  799    807    1426   1430   Any
        //  819    827    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  819    827    1437   1444   Ljava/io/IOException;
        //  819    827    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  819    827    1426   1430   Any
        //  832    841    685    689    Lorg/xmlpull/v1/XmlPullParserException;
        //  832    841    681    685    Ljava/io/IOException;
        //  832    841    677    681    Landroid/content/res/Resources$NotFoundException;
        //  832    841    673    677    Any
        //  864    869    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  864    869    1437   1444   Ljava/io/IOException;
        //  864    869    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  864    869    1426   1430   Any
        //  881    886    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  881    886    1437   1444   Ljava/io/IOException;
        //  881    886    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  881    886    1426   1430   Any
        //  898    957    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  898    957    1437   1444   Ljava/io/IOException;
        //  898    957    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  898    957    1426   1430   Any
        //  969    979    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  969    979    1437   1444   Ljava/io/IOException;
        //  969    979    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  969    979    1426   1430   Any
        //  991    999    1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  991    999    1437   1444   Ljava/io/IOException;
        //  991    999    1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  991    999    1426   1430   Any
        //  1011   1020   1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  1011   1020   1437   1444   Ljava/io/IOException;
        //  1011   1020   1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  1011   1020   1426   1430   Any
        //  1025   1033   685    689    Lorg/xmlpull/v1/XmlPullParserException;
        //  1025   1033   681    685    Ljava/io/IOException;
        //  1025   1033   677    681    Landroid/content/res/Resources$NotFoundException;
        //  1025   1033   673    677    Any
        //  1051   1065   1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  1051   1065   1437   1444   Ljava/io/IOException;
        //  1051   1065   1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  1051   1065   1426   1430   Any
        //  1077   1085   1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  1077   1085   1437   1444   Ljava/io/IOException;
        //  1077   1085   1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  1077   1085   1426   1430   Any
        //  1097   1120   1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  1097   1120   1437   1444   Ljava/io/IOException;
        //  1097   1120   1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  1097   1120   1426   1430   Any
        //  1132   1143   1444   1451   Lorg/xmlpull/v1/XmlPullParserException;
        //  1132   1143   1437   1444   Ljava/io/IOException;
        //  1132   1143   1430   1437   Landroid/content/res/Resources$NotFoundException;
        //  1132   1143   1426   1430   Any
        //  1162   1172   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1162   1172   1642   1649   Ljava/io/IOException;
        //  1162   1172   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1162   1172   1765   1770   Any
        //  1194   1202   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1194   1202   1642   1649   Ljava/io/IOException;
        //  1194   1202   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1194   1202   1765   1770   Any
        //  1218   1226   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1218   1226   1642   1649   Ljava/io/IOException;
        //  1218   1226   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1218   1226   1765   1770   Any
        //  1246   1254   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1246   1254   1642   1649   Ljava/io/IOException;
        //  1246   1254   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1246   1254   1765   1770   Any
        //  1274   1282   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1274   1282   1642   1649   Ljava/io/IOException;
        //  1274   1282   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1274   1282   1765   1770   Any
        //  1298   1306   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1298   1306   1642   1649   Ljava/io/IOException;
        //  1298   1306   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1298   1306   1765   1770   Any
        //  1322   1330   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1322   1330   1642   1649   Ljava/io/IOException;
        //  1322   1330   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1322   1330   1765   1770   Any
        //  1346   1363   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1346   1363   1642   1649   Ljava/io/IOException;
        //  1346   1363   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1346   1363   1765   1770   Any
        //  1391   1407   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1391   1407   1642   1649   Ljava/io/IOException;
        //  1391   1407   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1391   1407   1765   1770   Any
        //  1467   1471   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1467   1471   1642   1649   Ljava/io/IOException;
        //  1467   1471   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1467   1471   1765   1770   Any
        //  1484   1489   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1484   1489   1642   1649   Ljava/io/IOException;
        //  1484   1489   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1484   1489   1765   1770   Any
        //  1502   1507   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1502   1507   1642   1649   Ljava/io/IOException;
        //  1502   1507   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1502   1507   1765   1770   Any
        //  1520   1529   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1520   1529   1642   1649   Ljava/io/IOException;
        //  1520   1529   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1520   1529   1765   1770   Any
        //  1542   1550   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1542   1550   1642   1649   Ljava/io/IOException;
        //  1542   1550   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1542   1550   1765   1770   Any
        //  1563   1572   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1563   1572   1642   1649   Ljava/io/IOException;
        //  1563   1572   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1563   1572   1765   1770   Any
        //  1585   1598   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1585   1598   1642   1649   Ljava/io/IOException;
        //  1585   1598   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1585   1598   1765   1770   Any
        //  1611   1620   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1611   1620   1642   1649   Ljava/io/IOException;
        //  1611   1620   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1611   1620   1765   1770   Any
        //  1633   1635   1649   1656   Lorg/xmlpull/v1/XmlPullParserException;
        //  1633   1635   1642   1649   Ljava/io/IOException;
        //  1633   1635   1635   1642   Landroid/content/res/Resources$NotFoundException;
        //  1633   1635   1765   1770   Any
        //  1676   1687   1765   1770   Any
        //  1709   1720   1765   1770   Any
        //  1742   1753   1765   1770   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_1175:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Thread.java:745)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private Set<String> getNonIndexableKeysForResource(final Map<String, Set<String>> map, final String s) {
        Set<String> set;
        if (map.containsKey(s)) {
            set = map.get(s);
        }
        else {
            set = new HashSet<String>();
        }
        return set;
    }
    
    public List<IndexData> convertPreIndexDataToIndexData(final PreIndexData preIndexData) {
        final long currentTimeMillis = System.currentTimeMillis();
        final List<SearchIndexableData> dataToUpdate = preIndexData.dataToUpdate;
        final Map<String, Set<String>> nonIndexableKeys = preIndexData.nonIndexableKeys;
        final ArrayList<IndexData> list = new ArrayList<IndexData>();
        for (final SearchIndexableData searchIndexableData : dataToUpdate) {
            if (searchIndexableData instanceof SearchIndexableRaw) {
                final SearchIndexableRaw searchIndexableRaw = (SearchIndexableRaw)searchIndexableData;
                final IndexData.Builder convertRaw = this.convertRaw(searchIndexableRaw, nonIndexableKeys.get(searchIndexableRaw.intentTargetPackage));
                if (convertRaw == null) {
                    continue;
                }
                list.add(convertRaw.build(this.mContext));
            }
            else {
                if (!(searchIndexableData instanceof SearchIndexableResource)) {
                    continue;
                }
                final SearchIndexableResource searchIndexableResource = (SearchIndexableResource)searchIndexableData;
                list.addAll((Collection<?>)this.convertResource(searchIndexableResource, this.getNonIndexableKeysForResource(nonIndexableKeys, searchIndexableResource.packageName)));
            }
        }
        final long currentTimeMillis2 = System.currentTimeMillis();
        final StringBuilder sb = new StringBuilder();
        sb.append("Converting pre-index data to index data took: ");
        sb.append(currentTimeMillis2 - currentTimeMillis);
        Log.d("IndexDataConverter", sb.toString());
        return list;
    }
}
