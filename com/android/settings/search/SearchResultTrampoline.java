package com.android.settings.search;

import android.content.Intent;
import com.android.settings.SubSettings;
import android.content.Context;
import com.android.settings.overlay.FeatureFactory;
import android.os.Bundle;
import android.app.Activity;

public class SearchResultTrampoline extends Activity
{
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        FeatureFactory.getFactory((Context)this).getSearchFeatureProvider().verifyLaunchSearchResultPageCaller((Context)this, this.getCallingActivity());
        final Intent intent = this.getIntent();
        final String stringExtra = intent.getStringExtra(":settings:fragment_args_key");
        bundle = new Bundle();
        bundle.putString(":settings:fragment_args_key", stringExtra);
        intent.putExtra(":settings:show_fragment_args", bundle);
        intent.setClass((Context)this, (Class)SubSettings.class).addFlags(33554432);
        this.startActivity(intent);
        this.finish();
    }
}
