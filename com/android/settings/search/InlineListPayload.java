package com.android.settings.search;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class InlineListPayload extends InlinePayload
{
    public static final Parcelable.Creator<InlineListPayload> CREATOR;
    private int mNumOptions;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<InlineListPayload>() {
            public InlineListPayload createFromParcel(final Parcel parcel) {
                return new InlineListPayload(parcel, (InlineListPayload$1)null);
            }
            
            public InlineListPayload[] newArray(final int n) {
                return new InlineListPayload[n];
            }
        };
    }
    
    private InlineListPayload(final Parcel parcel) {
        super(parcel);
        this.mNumOptions = parcel.readInt();
    }
    
    public InlineListPayload(final String s, final int n, final Intent intent, final boolean b, final int mNumOptions, final int n2) {
        super(s, n, intent, b, n2);
        this.mNumOptions = mNumOptions;
    }
    
    @Override
    public int getType() {
        return 3;
    }
    
    @Override
    protected int standardizeInput(final int n) throws IllegalArgumentException {
        if (n >= 0 && n < this.mNumOptions) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid argument for ListSelect. Expected between 0 and ");
        sb.append(this.mNumOptions);
        sb.append(" but found: ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void writeToParcel(final Parcel parcel, final int n) {
        super.writeToParcel(parcel, n);
        parcel.writeInt(this.mNumOptions);
    }
}
