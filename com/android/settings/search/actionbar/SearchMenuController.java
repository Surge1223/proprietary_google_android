package com.android.settings.search.actionbar;

import android.os.Bundle;
import android.view.MenuItem$OnMenuItemClickListener;
import com.android.settings.Utils;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.Intent;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.search.SearchFeatureProvider;
import android.view.MenuItem;
import com.android.settingslib.core.lifecycle.ObservablePreferenceFragment;
import com.android.settingslib.core.lifecycle.ObservableFragment;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class SearchMenuController implements LifecycleObserver, OnCreateOptionsMenu
{
    private final Fragment mHost;
    
    private SearchMenuController(final Fragment mHost) {
        this.mHost = mHost;
    }
    
    public static void init(final ObservableFragment observableFragment) {
        observableFragment.getLifecycle().addObserver(new SearchMenuController(observableFragment));
    }
    
    public static void init(final ObservablePreferenceFragment observablePreferenceFragment) {
        observablePreferenceFragment.getLifecycle().addObserver(new SearchMenuController(observablePreferenceFragment));
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        if (!Utils.isDeviceProvisioned(this.mHost.getContext())) {
            return;
        }
        if (menu == null) {
            return;
        }
        final Bundle arguments = this.mHost.getArguments();
        if (arguments != null && !arguments.getBoolean("need_search_icon_in_action_bar", true)) {
            return;
        }
        final MenuItem add = menu.add(0, 0, 0, 2131888968);
        add.setIcon(2131231105);
        add.setShowAsAction(2);
        add.setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)new _$$Lambda$SearchMenuController$5lHWir39yWMPpFtqgtH1CYNgf1M(this));
    }
}
