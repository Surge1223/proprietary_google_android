package com.android.settings.search.actionbar;

import android.os.Bundle;
import com.android.settings.Utils;
import android.view.MenuInflater;
import android.view.Menu;
import android.content.Intent;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.search.SearchFeatureProvider;
import com.android.settingslib.core.lifecycle.ObservablePreferenceFragment;
import com.android.settingslib.core.lifecycle.ObservableFragment;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.view.MenuItem;
import android.view.MenuItem$OnMenuItemClickListener;

public final class _$$Lambda$SearchMenuController$5lHWir39yWMPpFtqgtH1CYNgf1M implements MenuItem$OnMenuItemClickListener
{
    public final boolean onMenuItemClick(final MenuItem menuItem) {
        return SearchMenuController.lambda$onCreateOptionsMenu$0(this.f$0, menuItem);
    }
}
