package com.android.settings.search;

import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;

public interface Indexable
{
    public interface SearchIndexProvider
    {
        List<String> getNonIndexableKeys(final Context p0);
        
        List<AbstractPreferenceController> getPreferenceControllers(final Context p0);
        
        List<SearchIndexableRaw> getRawDataToIndex(final Context p0, final boolean p1);
        
        List<SearchIndexableResource> getXmlResourcesToIndex(final Context p0, final boolean p1);
    }
}
