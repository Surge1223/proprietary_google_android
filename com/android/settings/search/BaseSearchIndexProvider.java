package com.android.settings.search;

import com.android.settings.core.PreferenceControllerListHelper;
import android.util.AttributeSet;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.text.TextUtils;
import com.android.settings.core.PreferenceXmlParserUtils;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.util.Log;
import com.android.settings.core.BasePreferenceController;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.Iterator;
import java.util.Collection;
import android.provider.SearchIndexableResource;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;

public class BaseSearchIndexProvider implements SearchIndexProvider
{
    private List<String> getNonIndexableKeysFromXml(final Context context) {
        final List<SearchIndexableResource> xmlResourcesToIndex = this.getXmlResourcesToIndex(context, true);
        if (xmlResourcesToIndex != null && !xmlResourcesToIndex.isEmpty()) {
            final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<String>();
            final Iterator<SearchIndexableResource> iterator = xmlResourcesToIndex.iterator();
            while (iterator.hasNext()) {
                list.addAll(this.getNonIndexableKeysFromXml(context, iterator.next().xmlResId));
            }
            return (List<String>)list;
        }
        return new ArrayList<String>();
    }
    
    public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return null;
    }
    
    @Override
    public List<String> getNonIndexableKeys(final Context context) {
        if (!this.isPageSearchEnabled(context)) {
            return this.getNonIndexableKeysFromXml(context);
        }
        final List<AbstractPreferenceController> preferenceControllers = this.getPreferenceControllers(context);
        if (preferenceControllers != null && !preferenceControllers.isEmpty()) {
            final ArrayList<String> list = new ArrayList<String>();
            for (final AbstractPreferenceController abstractPreferenceController : preferenceControllers) {
                if (abstractPreferenceController instanceof PreferenceControllerMixin) {
                    ((PreferenceControllerMixin)abstractPreferenceController).updateNonIndexableKeys(list);
                }
                else if (abstractPreferenceController instanceof BasePreferenceController) {
                    ((BasePreferenceController)abstractPreferenceController).updateNonIndexableKeys(list);
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(((BasePreferenceController)abstractPreferenceController).getClass().getName());
                    sb.append(" must implement ");
                    sb.append(PreferenceControllerMixin.class.getName());
                    sb.append(" treating the key non-indexable");
                    Log.e("BaseSearchIndex", sb.toString());
                    list.add(abstractPreferenceController.getPreferenceKey());
                }
            }
            return list;
        }
        return new ArrayList<String>();
    }
    
    public List<String> getNonIndexableKeysFromXml(final Context context, final int n) {
        final ArrayList<String> list = new ArrayList<String>();
        final XmlResourceParser xml = context.getResources().getXml(n);
        final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
        try {
            while (xml.next() != 1) {
                final String dataKey = PreferenceXmlParserUtils.getDataKey(context, attributeSet);
                if (!TextUtils.isEmpty((CharSequence)dataKey)) {
                    list.add(dataKey);
                }
            }
        }
        catch (IOException | XmlPullParserException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error parsing non-indexable from xml ");
            sb.append(n);
            Log.w("BaseSearchIndex", sb.toString());
        }
        return list;
    }
    
    @Override
    public List<AbstractPreferenceController> getPreferenceControllers(final Context context) {
        final List<AbstractPreferenceController> preferenceControllers = this.createPreferenceControllers(context);
        final List<SearchIndexableResource> xmlResourcesToIndex = this.getXmlResourcesToIndex(context, true);
        if (xmlResourcesToIndex != null && !xmlResourcesToIndex.isEmpty()) {
            final ArrayList<Object> list = new ArrayList<Object>();
            final Iterator<SearchIndexableResource> iterator = xmlResourcesToIndex.iterator();
            while (iterator.hasNext()) {
                list.addAll(PreferenceControllerListHelper.getPreferenceControllersFromXml(context, iterator.next().xmlResId));
            }
            final List<BasePreferenceController> filterControllers = PreferenceControllerListHelper.filterControllers((List<BasePreferenceController>)list, preferenceControllers);
            final ArrayList<Object> list2 = (ArrayList<Object>)new ArrayList<AbstractPreferenceController>();
            if (preferenceControllers != null) {
                list2.addAll(preferenceControllers);
            }
            list2.addAll(filterControllers);
            return (List<AbstractPreferenceController>)list2;
        }
        return preferenceControllers;
    }
    
    @Override
    public List<SearchIndexableRaw> getRawDataToIndex(final Context context, final boolean b) {
        return null;
    }
    
    @Override
    public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
        return null;
    }
    
    protected boolean isPageSearchEnabled(final Context context) {
        return true;
    }
}
