package com.android.settings.search;

import android.os.Parcelable.Creator;
import android.os.Parcel;

public class ResultPayloadUtils
{
    public static byte[] marshall(final ResultPayload resultPayload) {
        final Parcel obtain = Parcel.obtain();
        resultPayload.writeToParcel(obtain, 0);
        final byte[] marshall = obtain.marshall();
        obtain.recycle();
        return marshall;
    }
    
    private static Parcel unmarshall(final byte[] array) {
        final Parcel obtain = Parcel.obtain();
        obtain.unmarshall(array, 0, array.length);
        obtain.setDataPosition(0);
        return obtain;
    }
    
    public static <T> T unmarshall(final byte[] array, final Parcelable.Creator<T> parcelable$Creator) {
        final Parcel unmarshall = unmarshall(array);
        final Object fromParcel = parcelable$Creator.createFromParcel(unmarshall);
        unmarshall.recycle();
        return (T)fromParcel;
    }
}
