package com.android.settings;

import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.KeyEvent;
import android.content.res.Resources$NotFoundException;
import android.os.Bundle;
import android.text.Editable;
import android.text.format.DateUtils;
import android.widget.Button;
import android.provider.Settings;
import android.os.AsyncTask;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings;
import android.view.View.OnClickListener;
import android.content.Intent;
import java.util.Iterator;
import android.view.inputmethod.InputMethodSubtype;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.text.TextUtils;
import com.android.internal.widget.LockPatternView$DisplayMode;
import android.telephony.TelephonyManager;
import android.telecom.TelecomManager;
import android.os.IBinder;
import android.os.storage.IStorageManager$Stub;
import android.os.ServiceManager;
import android.widget.ProgressBar;
import android.os.PowerManager;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.ComponentName;
import android.content.Context;
import android.widget.TextView;
import android.os.storage.IStorageManager;
import android.view.View;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternView$Cell;
import java.util.List;
import android.os.Message;
import android.os.PowerManager$WakeLock;
import android.app.StatusBarManager;
import com.android.settings.widget.ImeAwareEditText;
import com.android.internal.widget.LockPatternView;
import android.os.Handler;
import com.android.internal.widget.LockPatternView$OnPatternListener;
import android.media.AudioManager;
import android.widget.TextView$OnEditorActionListener;
import android.view.View.OnTouchListener;
import android.view.View$OnKeyListener;
import android.text.TextWatcher;
import android.app.Activity;

public class CryptKeeper extends Activity implements TextWatcher, View$OnKeyListener, View.OnTouchListener, TextView$OnEditorActionListener
{
    private AudioManager mAudioManager;
    protected LockPatternView$OnPatternListener mChooseNewLockPatternListener;
    private final Runnable mClearPatternRunnable;
    private boolean mCooldown;
    private boolean mCorrupt;
    private boolean mEncryptionGoneBad;
    private final Runnable mFakeUnlockAttemptRunnable;
    private final Handler mHandler;
    private LockPatternView mLockPatternView;
    private int mNotificationCountdown;
    private ImeAwareEditText mPasswordEntry;
    private int mReleaseWakeLockCountdown;
    private StatusBarManager mStatusBar;
    private int mStatusString;
    private boolean mValidationComplete;
    private boolean mValidationRequested;
    PowerManager$WakeLock mWakeLock;
    
    public CryptKeeper() {
        this.mCooldown = false;
        this.mNotificationCountdown = 0;
        this.mReleaseWakeLockCountdown = 0;
        this.mStatusString = 2131887560;
        this.mFakeUnlockAttemptRunnable = new Runnable() {
            @Override
            public void run() {
                CryptKeeper.this.handleBadAttempt(1);
            }
        };
        this.mClearPatternRunnable = new Runnable() {
            @Override
            public void run() {
                CryptKeeper.this.mLockPatternView.clearPattern();
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case 2: {
                        CryptKeeper.this.notifyUser();
                        break;
                    }
                    case 1: {
                        CryptKeeper.this.updateProgress();
                        break;
                    }
                }
            }
        };
        this.mChooseNewLockPatternListener = (LockPatternView$OnPatternListener)new LockPatternView$OnPatternListener() {
            public void onPatternCellAdded(final List<LockPatternView$Cell> list) {
            }
            
            public void onPatternCleared() {
            }
            
            public void onPatternDetected(final List<LockPatternView$Cell> list) {
                CryptKeeper.this.mLockPatternView.setEnabled(false);
                if (list.size() >= 4) {
                    new DecryptTask().execute((Object[])new String[] { LockPatternUtils.patternToString((List)list) });
                }
                else {
                    CryptKeeper.this.fakeUnlockAttempt((View)CryptKeeper.this.mLockPatternView);
                }
            }
            
            public void onPatternStart() {
                CryptKeeper.this.mLockPatternView.removeCallbacks(CryptKeeper.this.mClearPatternRunnable);
            }
        };
    }
    
    private void beginAttempt() {
        ((TextView)this.findViewById(2131362645)).setText(2131887008);
    }
    
    private void cooldown() {
        if (this.mPasswordEntry != null) {
            this.mPasswordEntry.setEnabled(false);
        }
        if (this.mLockPatternView != null) {
            this.mLockPatternView.setEnabled(false);
        }
        ((TextView)this.findViewById(2131362645)).setText(2131887175);
    }
    
    private void delayAudioNotification() {
        this.mNotificationCountdown = 20;
    }
    
    private static void disableCryptKeeperComponent(final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        final ComponentName componentName = new ComponentName(context, (Class)CryptKeeper.class);
        final StringBuilder sb = new StringBuilder();
        sb.append("Disabling component ");
        sb.append(componentName);
        Log.d("CryptKeeper", sb.toString());
        packageManager.setComponentEnabledSetting(componentName, 2, 1);
    }
    
    private void encryptionProgressInit() {
        Log.d("CryptKeeper", "Encryption progress screen initializing.");
        if (this.mWakeLock == null) {
            Log.d("CryptKeeper", "Acquiring wakelock.");
            (this.mWakeLock = ((PowerManager)this.getSystemService("power")).newWakeLock(26, "CryptKeeper")).acquire();
        }
        ((ProgressBar)this.findViewById(2131362484)).setIndeterminate(true);
        this.setBackFunctionality(false);
        this.updateProgress();
    }
    
    private void fakeUnlockAttempt(final View view) {
        this.beginAttempt();
        view.postDelayed(this.mFakeUnlockAttemptRunnable, 1000L);
    }
    
    private IStorageManager getStorageManager() {
        final IBinder service = ServiceManager.getService("mount");
        if (service != null) {
            return IStorageManager$Stub.asInterface(service);
        }
        return null;
    }
    
    private TelecomManager getTelecomManager() {
        return (TelecomManager)this.getSystemService("telecom");
    }
    
    private TelephonyManager getTelephonyManager() {
        return (TelephonyManager)this.getSystemService("phone");
    }
    
    private void handleBadAttempt(final Integer n) {
        if (this.mLockPatternView != null) {
            this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Wrong);
            this.mLockPatternView.removeCallbacks(this.mClearPatternRunnable);
            this.mLockPatternView.postDelayed(this.mClearPatternRunnable, 1500L);
        }
        if (n % 10 == 0) {
            this.mCooldown = true;
            this.cooldown();
        }
        else {
            final TextView textView = (TextView)this.findViewById(2131362645);
            final int n2 = 30 - n;
            if (n2 < 10) {
                textView.setText(TextUtils.expandTemplate(this.getText(2131887183), new CharSequence[] { Integer.toString(n2) }));
            }
            else {
                final boolean b = false;
                int passwordType;
                try {
                    passwordType = this.getStorageManager().getPasswordType();
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Error calling mount service ");
                    sb.append(ex);
                    Log.e("CryptKeeper", sb.toString());
                    passwordType = (b ? 1 : 0);
                }
                if (passwordType == 2) {
                    textView.setText(2131887188);
                }
                else if (passwordType == 3) {
                    textView.setText(2131887187);
                }
                else {
                    textView.setText(2131887186);
                }
            }
            if (this.mLockPatternView != null) {
                this.mLockPatternView.setDisplayMode(LockPatternView$DisplayMode.Wrong);
                this.mLockPatternView.setEnabled(true);
            }
            if (this.mPasswordEntry != null) {
                this.mPasswordEntry.setEnabled(true);
                this.mPasswordEntry.scheduleShowSoftInput();
                this.setBackFunctionality(true);
            }
        }
    }
    
    private boolean hasMultipleEnabledIMEsOrSubtypes(final InputMethodManager inputMethodManager, final boolean b) {
        final List enabledInputMethodList = inputMethodManager.getEnabledInputMethodList();
        int n = 0;
        for (final InputMethodInfo inputMethodInfo : enabledInputMethodList) {
            if (n > 1) {
                return true;
            }
            final List enabledInputMethodSubtypeList = inputMethodManager.getEnabledInputMethodSubtypeList(inputMethodInfo, true);
            if (enabledInputMethodSubtypeList.isEmpty()) {
                ++n;
            }
            else {
                int n2 = 0;
                final Iterator<InputMethodSubtype> iterator2 = enabledInputMethodSubtypeList.iterator();
                while (iterator2.hasNext()) {
                    int n3 = n2;
                    if (iterator2.next().isAuxiliary()) {
                        n3 = n2 + 1;
                    }
                    n2 = n3;
                }
                if (enabledInputMethodSubtypeList.size() - n2 <= 0 && (!b || n2 <= 1)) {
                    continue;
                }
                ++n;
            }
        }
        boolean b2 = false;
        if (n > 1 || inputMethodManager.getEnabledInputMethodSubtypeList((InputMethodInfo)null, false).size() > 1) {
            b2 = true;
        }
        return b2;
    }
    
    private boolean isDebugView() {
        return this.getIntent().hasExtra("com.android.settings.CryptKeeper.DEBUG_FORCE_VIEW");
    }
    
    private boolean isDebugView(final String s) {
        return s.equals(this.getIntent().getStringExtra("com.android.settings.CryptKeeper.DEBUG_FORCE_VIEW"));
    }
    
    private boolean isEmergencyCallCapable() {
        return this.getResources().getBoolean(17957073);
    }
    
    private void launchEmergencyDialer() {
        final Intent intent = new Intent("com.android.phone.EmergencyDialer.DIAL");
        intent.setFlags(276824064);
        this.setBackFunctionality(true);
        this.startActivity(intent);
    }
    
    private void notifyUser() {
        if (this.mNotificationCountdown > 0) {
            --this.mNotificationCountdown;
        }
        else if (this.mAudioManager != null) {
            try {
                this.mAudioManager.playSoundEffect(5, 100);
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("notifyUser: Exception while playing sound: ");
                sb.append(ex);
                Log.w("CryptKeeper", sb.toString());
            }
        }
        this.mHandler.removeMessages(2);
        this.mHandler.sendEmptyMessageDelayed(2, 5000L);
        if (this.mWakeLock.isHeld()) {
            if (this.mReleaseWakeLockCountdown > 0) {
                --this.mReleaseWakeLockCountdown;
            }
            else {
                this.mWakeLock.release();
            }
        }
    }
    
    private void passwordEntryInit() {
        this.mPasswordEntry = (ImeAwareEditText)this.findViewById(2131362437);
        if (this.mPasswordEntry != null) {
            this.mPasswordEntry.setOnEditorActionListener((TextView$OnEditorActionListener)this);
            this.mPasswordEntry.requestFocus();
            this.mPasswordEntry.setOnKeyListener((View$OnKeyListener)this);
            this.mPasswordEntry.setOnTouchListener((View.OnTouchListener)this);
            this.mPasswordEntry.addTextChangedListener((TextWatcher)this);
        }
        this.mLockPatternView = (LockPatternView)this.findViewById(2131362345);
        if (this.mLockPatternView != null) {
            this.mLockPatternView.setOnPatternListener(this.mChooseNewLockPatternListener);
        }
        if (!this.getTelephonyManager().isVoiceCapable()) {
            final View viewById = this.findViewById(2131362102);
            if (viewById != null) {
                Log.d("CryptKeeper", "Removing the emergency Call button");
                viewById.setVisibility(8);
            }
        }
        final View viewById2 = this.findViewById(2131362707);
        final InputMethodManager inputMethodManager = (InputMethodManager)this.getSystemService("input_method");
        if (viewById2 != null && this.hasMultipleEnabledIMEsOrSubtypes(inputMethodManager, false)) {
            viewById2.setVisibility(0);
            viewById2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    inputMethodManager.showInputMethodPicker(false);
                }
            });
        }
        if (this.mWakeLock == null) {
            Log.d("CryptKeeper", "Acquiring wakelock.");
            final PowerManager powerManager = (PowerManager)this.getSystemService("power");
            if (powerManager != null) {
                (this.mWakeLock = powerManager.newWakeLock(26, "CryptKeeper")).acquire();
                this.mReleaseWakeLockCountdown = 96;
            }
        }
        if (this.mLockPatternView == null && !this.mCooldown) {
            this.getWindow().setSoftInputMode(5);
            if (this.mPasswordEntry != null) {
                this.mPasswordEntry.scheduleShowSoftInput();
            }
        }
        this.updateEmergencyCallButtonState();
        this.mHandler.removeMessages(2);
        this.mHandler.sendEmptyMessageDelayed(2, 120000L);
        this.getWindow().addFlags(4718592);
    }
    
    private final void setAirplaneModeIfNecessary() {
        if (this.getTelephonyManager().getLteOnCdmaMode() != 1) {
            Log.d("CryptKeeper", "Going into airplane mode.");
            Settings.Global.putInt(this.getContentResolver(), "airplane_mode_on", 1);
            final Intent intent = new Intent("android.intent.action.AIRPLANE_MODE");
            intent.putExtra("state", true);
            this.sendBroadcastAsUser(intent, UserHandle.ALL);
        }
    }
    
    private final void setBackFunctionality(final boolean b) {
        if (b) {
            this.mStatusBar.disable(52887552);
        }
        else {
            this.mStatusBar.disable(57081856);
        }
    }
    
    private void setupUi() {
        if (!this.mEncryptionGoneBad && !this.isDebugView("error")) {
            if ("".equals(SystemProperties.get("vold.encrypt_progress")) && !this.isDebugView("progress")) {
                if (!this.mValidationComplete && !this.isDebugView("password")) {
                    if (!this.mValidationRequested) {
                        new ValidationTask().execute((Object[])null);
                        this.mValidationRequested = true;
                    }
                }
                else {
                    new AsyncTask<Void, Void, Void>() {
                        String owner_info;
                        int passwordType = 0;
                        boolean password_visible;
                        boolean pattern_visible;
                        
                        public Void doInBackground(final Void... array) {
                            try {
                                final IStorageManager access$300 = CryptKeeper.this.getStorageManager();
                                this.passwordType = access$300.getPasswordType();
                                this.owner_info = access$300.getField("OwnerInfo");
                                this.pattern_visible = ("0".equals(access$300.getField("PatternVisible")) ^ true);
                                this.password_visible = ("0".equals(access$300.getField("PasswordVisible")) ^ true);
                            }
                            catch (Exception ex) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Error calling mount service ");
                                sb.append(ex);
                                Log.e("CryptKeeper", sb.toString());
                            }
                            return null;
                        }
                        
                        public void onPostExecute(final Void void1) {
                            Settings.System.putInt(CryptKeeper.this.getContentResolver(), "show_password", (int)(this.password_visible ? 1 : 0));
                            if (this.passwordType == 2) {
                                CryptKeeper.this.setContentView(2131558515);
                                CryptKeeper.this.mStatusString = 2131887562;
                            }
                            else if (this.passwordType == 3) {
                                CryptKeeper.this.setContentView(2131558513);
                                CryptKeeper.this.setBackFunctionality(false);
                                CryptKeeper.this.mStatusString = 2131887561;
                            }
                            else {
                                CryptKeeper.this.setContentView(2131558511);
                                CryptKeeper.this.mStatusString = 2131887560;
                            }
                            ((TextView)CryptKeeper.this.findViewById(2131362645)).setText(CryptKeeper.this.mStatusString);
                            final TextView textView = (TextView)CryptKeeper.this.findViewById(2131362428);
                            textView.setText((CharSequence)this.owner_info);
                            textView.setSelected(true);
                            CryptKeeper.this.passwordEntryInit();
                            CryptKeeper.this.findViewById(16908290).setSystemUiVisibility(4194304);
                            if (CryptKeeper.this.mLockPatternView != null) {
                                CryptKeeper.this.mLockPatternView.setInStealthMode(true ^ this.pattern_visible);
                            }
                            if (CryptKeeper.this.mCooldown) {
                                CryptKeeper.this.setBackFunctionality(false);
                                CryptKeeper.this.cooldown();
                            }
                        }
                    }.execute((Object[])new Void[0]);
                }
            }
            else {
                this.setContentView(2131558517);
                this.encryptionProgressInit();
            }
            return;
        }
        this.setContentView(2131558517);
        this.showFactoryReset(this.mCorrupt);
    }
    
    private void showFactoryReset(final boolean b) {
        this.findViewById(2131362105).setVisibility(8);
        final Button button = (Button)this.findViewById(2131362140);
        button.setVisibility(0);
        button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final Intent intent = new Intent("android.intent.action.FACTORY_RESET");
                intent.setPackage("android");
                intent.addFlags(268435456);
                final StringBuilder sb = new StringBuilder();
                sb.append("CryptKeeper.showFactoryReset() corrupt=");
                sb.append(b);
                intent.putExtra("android.intent.extra.REASON", sb.toString());
                CryptKeeper.this.sendBroadcast(intent);
            }
        });
        if (b) {
            ((TextView)this.findViewById(R.id.title)).setText(2131887165);
            ((TextView)this.findViewById(2131362645)).setText(2131887164);
        }
        else {
            ((TextView)this.findViewById(R.id.title)).setText(2131887173);
            ((TextView)this.findViewById(2131362645)).setText(2131887172);
        }
        final View viewById = this.findViewById(2131361917);
        if (viewById != null) {
            viewById.setVisibility(0);
        }
    }
    
    private void takeEmergencyCallAction() {
        final TelecomManager telecomManager = this.getTelecomManager();
        if (telecomManager.isInCall()) {
            telecomManager.showInCallScreen(false);
        }
        else {
            this.launchEmergencyDialer();
        }
    }
    
    private void updateEmergencyCallButtonState() {
        final Button button = (Button)this.findViewById(2131362102);
        if (button == null) {
            return;
        }
        if (this.isEmergencyCallCapable()) {
            button.setVisibility(0);
            button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    CryptKeeper.this.takeEmergencyCallAction();
                }
            });
            int text;
            if (this.getTelecomManager().isInCall()) {
                text = 2131887185;
            }
            else {
                text = 2131887184;
            }
            button.setText(text);
            return;
        }
        button.setVisibility(8);
    }
    
    private void updateProgress() {
        final String value = SystemProperties.get("vold.encrypt_progress");
        if ("error_partially_encrypted".equals(value)) {
            this.showFactoryReset(false);
            return;
        }
        CharSequence charSequence = this.getText(2131887178);
        int int1 = 0;
        try {
            if (this.isDebugView()) {
                int1 = 50;
            }
            else {
                int1 = Integer.parseInt(value);
            }
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error parsing progress: ");
            sb.append(ex.toString());
            Log.w("CryptKeeper", sb.toString());
        }
        final String string = Integer.toString(int1);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Encryption progress: ");
        sb2.append(string);
        Log.v("CryptKeeper", sb2.toString());
        String formatElapsedTime = string;
        CharSequence charSequence2;
        try {
            final int int2 = Integer.parseInt(SystemProperties.get("vold.encrypt_time_remaining"));
            charSequence2 = charSequence;
            formatElapsedTime = string;
            if (int2 >= 0) {
                formatElapsedTime = string;
                formatElapsedTime = DateUtils.formatElapsedTime((long)((int2 + 9) / 10 * 10));
                charSequence = this.getText(2131887179);
                formatElapsedTime = formatElapsedTime;
                charSequence2 = charSequence;
            }
        }
        catch (Exception ex2) {
            charSequence2 = charSequence;
        }
        final TextView textView = (TextView)this.findViewById(2131362645);
        if (textView != null) {
            textView.setText(TextUtils.expandTemplate(charSequence2, new CharSequence[] { formatElapsedTime }));
        }
        this.mHandler.removeMessages(1);
        this.mHandler.sendEmptyMessageDelayed(1, 1000L);
    }
    
    public void afterTextChanged(final Editable editable) {
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    public void onBackPressed() {
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String value = SystemProperties.get("vold.decrypt");
        if (!this.isDebugView() && ("".equals(value) || "trigger_restart_framework".equals(value))) {
            disableCryptKeeperComponent((Context)this);
            this.finish();
            return;
        }
        try {
            if (this.getResources().getBoolean(2131034182)) {
                this.setRequestedOrientation(-1);
            }
        }
        catch (Resources$NotFoundException ex) {}
        (this.mStatusBar = (StatusBarManager)this.getSystemService("statusbar")).disable(52887552);
        if (bundle != null) {
            this.mCooldown = bundle.getBoolean("cooldown");
        }
        this.setAirplaneModeIfNecessary();
        this.mAudioManager = (AudioManager)this.getSystemService("audio");
        final Object lastNonConfigurationInstance = this.getLastNonConfigurationInstance();
        if (lastNonConfigurationInstance instanceof NonConfigurationInstanceState) {
            this.mWakeLock = ((NonConfigurationInstanceState)lastNonConfigurationInstance).wakelock;
            Log.d("CryptKeeper", "Restoring wakelock from NonConfigurationInstanceState");
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        if (this.mWakeLock != null) {
            Log.d("CryptKeeper", "Releasing and destroying wakelock");
            this.mWakeLock.release();
            this.mWakeLock = null;
        }
    }
    
    public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
        if (n != 0 && n != 6) {
            return false;
        }
        final String string = textView.getText().toString();
        if (TextUtils.isEmpty((CharSequence)string)) {
            return true;
        }
        textView.setText((CharSequence)null);
        this.mPasswordEntry.setEnabled(false);
        this.setBackFunctionality(false);
        if (string.length() >= 4) {
            new DecryptTask().execute((Object[])new String[] { string });
        }
        else {
            this.fakeUnlockAttempt((View)this.mPasswordEntry);
        }
        return true;
    }
    
    public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
        this.delayAudioNotification();
        return false;
    }
    
    public Object onRetainNonConfigurationInstance() {
        final NonConfigurationInstanceState nonConfigurationInstanceState = new NonConfigurationInstanceState(this.mWakeLock);
        Log.d("CryptKeeper", "Handing wakelock off to NonConfigurationInstanceState");
        this.mWakeLock = null;
        return nonConfigurationInstanceState;
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putBoolean("cooldown", this.mCooldown);
    }
    
    public void onStart() {
        super.onStart();
        this.setupUi();
    }
    
    public void onStop() {
        super.onStop();
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
        this.delayAudioNotification();
    }
    
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        this.delayAudioNotification();
        return false;
    }
    
    private class DecryptTask extends AsyncTask<String, Void, Integer>
    {
        private void hide(final int n) {
            final View viewById = CryptKeeper.this.findViewById(n);
            if (viewById != null) {
                viewById.setVisibility(8);
            }
        }
        
        protected Integer doInBackground(final String... array) {
            final IStorageManager access$300 = CryptKeeper.this.getStorageManager();
            try {
                return access$300.decryptStorage(array[0]);
            }
            catch (Exception ex) {
                Log.e("CryptKeeper", "Error while decrypting...", (Throwable)ex);
                return -1;
            }
        }
        
        protected void onPostExecute(final Integer n) {
            if (n == 0) {
                if (CryptKeeper.this.mLockPatternView != null) {
                    CryptKeeper.this.mLockPatternView.removeCallbacks(CryptKeeper.this.mClearPatternRunnable);
                    CryptKeeper.this.mLockPatternView.postDelayed(CryptKeeper.this.mClearPatternRunnable, 500L);
                }
                ((TextView)CryptKeeper.this.findViewById(2131362645)).setText(2131889221);
                this.hide(2131362437);
                this.hide(2131362707);
                this.hide(2131362345);
                this.hide(2131362428);
                this.hide(2131362102);
            }
            else if (n == 30) {
                final Intent intent = new Intent("android.intent.action.FACTORY_RESET");
                intent.setPackage("android");
                intent.addFlags(268435456);
                intent.putExtra("android.intent.extra.REASON", "CryptKeeper.MAX_FAILED_ATTEMPTS");
                CryptKeeper.this.sendBroadcast(intent);
            }
            else {
                if (n == -1) {
                    CryptKeeper.this.setContentView(2131558517);
                    CryptKeeper.this.showFactoryReset(true);
                    return;
                }
                CryptKeeper.this.handleBadAttempt(n);
            }
        }
        
        protected void onPreExecute() {
            super.onPreExecute();
            CryptKeeper.this.beginAttempt();
        }
    }
    
    private static class NonConfigurationInstanceState
    {
        final PowerManager$WakeLock wakelock;
        
        NonConfigurationInstanceState(final PowerManager$WakeLock wakelock) {
            this.wakelock = wakelock;
        }
    }
    
    private class ValidationTask extends AsyncTask<Void, Void, Boolean>
    {
        int state;
        
        protected Boolean doInBackground(final Void... array) {
            final IStorageManager access$300 = CryptKeeper.this.getStorageManager();
            try {
                Log.d("CryptKeeper", "Validating encryption state.");
                this.state = access$300.getEncryptionState();
                if (this.state == 1) {
                    Log.w("CryptKeeper", "Unexpectedly in CryptKeeper even though there is no encryption.");
                    return true;
                }
                return this.state == 0;
            }
            catch (RemoteException ex) {
                Log.w("CryptKeeper", "Unable to get encryption state properly");
                return true;
            }
        }
        
        protected void onPostExecute(final Boolean b) {
            final CryptKeeper this$0 = CryptKeeper.this;
            boolean b2 = true;
            this$0.mValidationComplete = true;
            if (Boolean.FALSE.equals(b)) {
                Log.w("CryptKeeper", "Incomplete, or corrupted encryption detected. Prompting user to wipe.");
                CryptKeeper.this.mEncryptionGoneBad = true;
                final CryptKeeper this$2 = CryptKeeper.this;
                if (this.state != -4) {
                    b2 = false;
                }
                this$2.mCorrupt = b2;
            }
            else {
                Log.d("CryptKeeper", "Encryption state validated. Proceeding to configure UI");
            }
            CryptKeeper.this.setupUi();
        }
    }
}
