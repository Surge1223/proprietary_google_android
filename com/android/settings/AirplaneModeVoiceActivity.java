package com.android.settings;

import android.util.Log;
import android.net.ConnectivityManager;
import android.content.Intent;
import com.android.settings.utils.VoiceSettingsActivity;

public class AirplaneModeVoiceActivity extends VoiceSettingsActivity
{
    @Override
    protected boolean onVoiceSettingInteraction(final Intent intent) {
        if (intent.hasExtra("airplane_mode_enabled")) {
            ((ConnectivityManager)this.getSystemService("connectivity")).setAirplaneMode(intent.getBooleanExtra("airplane_mode_enabled", false));
        }
        else {
            Log.v("AirplaneModeVoiceActivity", "Missing airplane mode extra");
        }
        return true;
    }
}
