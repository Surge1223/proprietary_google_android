package com.android.settings.datetime;

import android.content.Context;

public interface UpdateTimeAndDateCallback
{
    void updateTimeAndDateDisplay(final Context p0);
}
