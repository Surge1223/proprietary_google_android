package com.android.settings.datetime;

import com.android.settingslib.RestrictedSwitchPreference;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settingslib.RestrictedLockUtils;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class AutoTimePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final UpdateTimeAndDateCallback mCallback;
    
    public AutoTimePreferenceController(final Context context, final UpdateTimeAndDateCallback mCallback) {
        super(context);
        this.mCallback = mCallback;
    }
    
    private RestrictedLockUtils.EnforcedAdmin getEnforcedAdminProperty() {
        return RestrictedLockUtils.checkIfAutoTimeRequired(this.mContext);
    }
    
    @Override
    public String getPreferenceKey() {
        return "auto_time";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public boolean isEnabled() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "auto_time", 0) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "auto_time", (int)(((boolean)o) ? 1 : 0));
        this.mCallback.updateTimeAndDateDisplay(this.mContext);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof RestrictedSwitchPreference)) {
            return;
        }
        if (!((RestrictedSwitchPreference)preference).isDisabledByAdmin()) {
            ((RestrictedSwitchPreference)preference).setDisabledByAdmin(this.getEnforcedAdminProperty());
        }
        ((RestrictedSwitchPreference)preference).setChecked(this.isEnabled());
    }
}
