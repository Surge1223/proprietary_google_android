package com.android.settings.datetime;

import com.android.settingslib.RestrictedPreference;
import android.app.AlarmManager;
import android.widget.TimePicker;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.text.format.DateFormat;
import java.util.Calendar;
import android.app.TimePickerDialog;
import android.app.Activity;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.app.TimePickerDialog$OnTimeSetListener;
import com.android.settingslib.core.AbstractPreferenceController;

public class TimePreferenceController extends AbstractPreferenceController implements TimePickerDialog$OnTimeSetListener, PreferenceControllerMixin
{
    private final AutoTimePreferenceController mAutoTimePreferenceController;
    private final TimePreferenceHost mHost;
    
    public TimePreferenceController(final Context context, final TimePreferenceHost mHost, final AutoTimePreferenceController mAutoTimePreferenceController) {
        super(context);
        this.mHost = mHost;
        this.mAutoTimePreferenceController = mAutoTimePreferenceController;
    }
    
    public TimePickerDialog buildTimePicker(final Activity activity) {
        final Calendar instance = Calendar.getInstance();
        return new TimePickerDialog((Context)activity, (TimePickerDialog$OnTimeSetListener)this, instance.get(11), instance.get(12), DateFormat.is24HourFormat((Context)activity));
    }
    
    @Override
    public String getPreferenceKey() {
        return "time";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)"time", (CharSequence)preference.getKey())) {
            return false;
        }
        this.mHost.showTimePicker();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void onTimeSet(final TimePicker timePicker, final int n, final int n2) {
        if (this.mContext != null) {
            this.setTime(n, n2);
            this.mHost.updateTimeAndDateDisplay(this.mContext);
        }
    }
    
    void setTime(final int n, final int n2) {
        final Calendar instance = Calendar.getInstance();
        instance.set(11, n);
        instance.set(12, n2);
        instance.set(13, 0);
        instance.set(14, 0);
        final long max = Math.max(instance.getTimeInMillis(), 1194220800000L);
        if (max / 1000L < 2147483647L) {
            ((AlarmManager)this.mContext.getSystemService("alarm")).setTime(max);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof RestrictedPreference)) {
            return;
        }
        preference.setSummary(DateFormat.getTimeFormat(this.mContext).format(Calendar.getInstance().getTime()));
        if (!((RestrictedPreference)preference).isDisabledByAdmin()) {
            preference.setEnabled(this.mAutoTimePreferenceController.isEnabled() ^ true);
        }
    }
    
    public interface TimePreferenceHost extends UpdateTimeAndDateCallback
    {
        void showTimePicker();
    }
}
