package com.android.settings.datetime;

import android.text.format.DateFormat;
import com.android.settingslib.RestrictedPreference;
import android.app.AlarmManager;
import android.widget.DatePicker;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import java.util.Calendar;
import android.app.DatePickerDialog;
import android.app.Activity;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.app.DatePickerDialog$OnDateSetListener;
import com.android.settingslib.core.AbstractPreferenceController;

public class DatePreferenceController extends AbstractPreferenceController implements DatePickerDialog$OnDateSetListener, PreferenceControllerMixin
{
    private final AutoTimePreferenceController mAutoTimePreferenceController;
    private final DatePreferenceHost mHost;
    
    public DatePreferenceController(final Context context, final DatePreferenceHost mHost, final AutoTimePreferenceController mAutoTimePreferenceController) {
        super(context);
        this.mHost = mHost;
        this.mAutoTimePreferenceController = mAutoTimePreferenceController;
    }
    
    public DatePickerDialog buildDatePicker(final Activity activity) {
        final Calendar instance = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog((Context)activity, (DatePickerDialog$OnDateSetListener)this, instance.get(1), instance.get(2), instance.get(5));
        instance.clear();
        instance.set(2007, 0, 1);
        datePickerDialog.getDatePicker().setMinDate(instance.getTimeInMillis());
        instance.clear();
        instance.set(2037, 11, 31);
        datePickerDialog.getDatePicker().setMaxDate(instance.getTimeInMillis());
        return datePickerDialog;
    }
    
    @Override
    public String getPreferenceKey() {
        return "date";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"date")) {
            return false;
        }
        this.mHost.showDatePicker();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void onDateSet(final DatePicker datePicker, final int n, final int n2, final int n3) {
        this.setDate(n, n2, n3);
        this.mHost.updateTimeAndDateDisplay(this.mContext);
    }
    
    void setDate(final int n, final int n2, final int n3) {
        final Calendar instance = Calendar.getInstance();
        instance.set(1, n);
        instance.set(2, n2);
        instance.set(5, n3);
        final long max = Math.max(instance.getTimeInMillis(), 1194220800000L);
        if (max / 1000L < 2147483647L) {
            ((AlarmManager)this.mContext.getSystemService("alarm")).setTime(max);
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof RestrictedPreference)) {
            return;
        }
        preference.setSummary(DateFormat.getLongDateFormat(this.mContext).format(Calendar.getInstance().getTime()));
        if (!((RestrictedPreference)preference).isDisabledByAdmin()) {
            preference.setEnabled(this.mAutoTimePreferenceController.isEnabled() ^ true);
        }
    }
    
    public interface DatePreferenceHost extends UpdateTimeAndDateCallback
    {
        void showDatePicker();
    }
}
