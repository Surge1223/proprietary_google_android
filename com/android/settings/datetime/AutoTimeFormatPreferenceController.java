package com.android.settings.datetime;

import android.text.format.DateFormat;
import java.util.Locale;
import android.support.v14.preference.SwitchPreference;
import android.text.TextUtils;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.Preference;
import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class AutoTimeFormatPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    public AutoTimeFormatPreferenceController(final Context context, final UpdateTimeAndDateCallback updateTimeAndDateCallback) {
        super(context);
    }
    
    static boolean isAutoTimeFormatSelection(final Context context) {
        return Settings.System.getString(context.getContentResolver(), "time_12_24") == null;
    }
    
    @Override
    public String getPreferenceKey() {
        return "auto_24hour";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (preference instanceof TwoStatePreference && TextUtils.equals((CharSequence)"auto_24hour", (CharSequence)preference.getKey())) {
            Boolean value;
            if (((SwitchPreference)preference).isChecked()) {
                value = null;
            }
            else {
                value = this.is24HourLocale(this.mContext.getResources().getConfiguration().locale);
            }
            TimeFormatPreferenceController.update24HourFormat(this.mContext, value);
            return true;
        }
        return false;
    }
    
    boolean is24HourLocale(final Locale locale) {
        return DateFormat.is24HourLocale(locale);
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof SwitchPreference)) {
            return;
        }
        ((SwitchPreference)preference).setChecked(isAutoTimeFormatSelection(this.mContext));
    }
}
