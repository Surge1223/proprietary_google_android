package com.android.settings.datetime;

import android.support.v14.preference.SwitchPreference;
import android.provider.Settings;
import com.android.settingslib.Utils;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class AutoTimeZonePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final UpdateTimeAndDateCallback mCallback;
    private final boolean mIsFromSUW;
    
    public AutoTimeZonePreferenceController(final Context context, final UpdateTimeAndDateCallback mCallback, final boolean mIsFromSUW) {
        super(context);
        this.mCallback = mCallback;
        this.mIsFromSUW = mIsFromSUW;
    }
    
    @Override
    public String getPreferenceKey() {
        return "auto_zone";
    }
    
    @Override
    public boolean isAvailable() {
        return !Utils.isWifiOnly(this.mContext) && !this.mIsFromSUW;
    }
    
    public boolean isEnabled() {
        final boolean available = this.isAvailable();
        boolean b = false;
        if (available) {
            b = b;
            if (Settings.Global.getInt(this.mContext.getContentResolver(), "auto_time_zone", 0) > 0) {
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "auto_time_zone", (int)(((boolean)o) ? 1 : 0));
        this.mCallback.updateTimeAndDateDisplay(this.mContext);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof SwitchPreference)) {
            return;
        }
        ((SwitchPreference)preference).setChecked(this.isEnabled());
    }
}
