package com.android.settings.datetime;

import android.os.Handler;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.content.BroadcastReceiver;

public class TimeChangeListenerMixin extends BroadcastReceiver implements LifecycleObserver, OnPause, OnResume
{
    private final UpdateTimeAndDateCallback mCallback;
    private final Context mContext;
    
    public TimeChangeListenerMixin(final Context mContext, final UpdateTimeAndDateCallback mCallback) {
        this.mContext = mContext;
        this.mCallback = mCallback;
    }
    
    public void onPause() {
        this.mContext.unregisterReceiver((BroadcastReceiver)this);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (this.mCallback != null) {
            this.mCallback.updateTimeAndDateDisplay(this.mContext);
        }
    }
    
    public void onResume() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.TIME_TICK");
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter, (String)null, (Handler)null);
    }
}
