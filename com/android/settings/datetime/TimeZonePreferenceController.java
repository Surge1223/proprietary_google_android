package com.android.settings.datetime;

import com.android.settings.datetime.timezone.TimeZoneSettings;
import com.android.settingslib.RestrictedPreference;
import android.support.v7.preference.Preference;
import com.android.settingslib.datetime.ZoneGetter;
import java.util.Calendar;
import android.util.FeatureFlagUtils;
import android.content.Context;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class TimeZonePreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final AutoTimeZonePreferenceController mAutoTimeZonePreferenceController;
    private final boolean mZonePickerV2;
    
    public TimeZonePreferenceController(final Context context, final AutoTimeZonePreferenceController mAutoTimeZonePreferenceController) {
        super(context);
        this.mAutoTimeZonePreferenceController = mAutoTimeZonePreferenceController;
        this.mZonePickerV2 = FeatureFlagUtils.isEnabled(this.mContext, "settings_zone_picker_v2");
    }
    
    @Override
    public String getPreferenceKey() {
        return "timezone";
    }
    
    CharSequence getTimeZoneOffsetAndName() {
        final Calendar instance = Calendar.getInstance();
        return ZoneGetter.getTimeZoneOffsetAndName(this.mContext, instance.getTimeZone(), instance.getTime());
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof RestrictedPreference)) {
            return;
        }
        if (this.mZonePickerV2) {
            preference.setFragment(TimeZoneSettings.class.getName());
        }
        preference.setSummary(this.getTimeZoneOffsetAndName());
        if (!((RestrictedPreference)preference).isDisabledByAdmin()) {
            preference.setEnabled(this.mAutoTimeZonePreferenceController.isEnabled() ^ true);
        }
    }
}
