package com.android.settings.datetime.timezone;

import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.datetime.timezone.model.TimeZoneDataLoader;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.TextView;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import java.util.Locale;
import com.android.settings.datetime.timezone.model.TimeZoneData;
import android.widget.SearchView;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView$OnQueryTextListener;
import com.android.settings.core.InstrumentedFragment;

public abstract class BaseTimeZonePicker extends InstrumentedFragment implements SearchView$OnQueryTextListener
{
    private BaseTimeZoneAdapter mAdapter;
    private final boolean mDefaultExpandSearch;
    private RecyclerView mRecyclerView;
    private final boolean mSearchEnabled;
    private final int mSearchHintResId;
    private SearchView mSearchView;
    private TimeZoneData mTimeZoneData;
    private final int mTitleResId;
    
    protected BaseTimeZonePicker(final int mTitleResId, final int mSearchHintResId, final boolean mSearchEnabled, final boolean mDefaultExpandSearch) {
        this.mTitleResId = mTitleResId;
        this.mSearchHintResId = mSearchHintResId;
        this.mSearchEnabled = mSearchEnabled;
        this.mDefaultExpandSearch = mDefaultExpandSearch;
    }
    
    protected abstract BaseTimeZoneAdapter createAdapter(final TimeZoneData p0);
    
    protected Locale getLocale() {
        return this.getContext().getResources().getConfiguration().getLocales().get(0);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setHasOptionsMenu(true);
        this.getActivity().setTitle(this.mTitleResId);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        if (this.mSearchEnabled) {
            menuInflater.inflate(2131623942, menu);
            final MenuItem item = menu.findItem(2131362736);
            (this.mSearchView = (SearchView)item.getActionView()).setQueryHint(this.getText(this.mSearchHintResId));
            this.mSearchView.setOnQueryTextListener((SearchView$OnQueryTextListener)this);
            if (this.mDefaultExpandSearch) {
                item.expandActionView();
                this.mSearchView.setIconified(false);
                this.mSearchView.setActivated(true);
                this.mSearchView.setQuery((CharSequence)"", true);
            }
            final TextView textView = (TextView)this.mSearchView.findViewById(16909268);
            textView.setPadding(0, textView.getPaddingTop(), 0, textView.getPaddingBottom());
            final View viewById = this.mSearchView.findViewById(16909264);
            final LinearLayout$LayoutParams layoutParams = (LinearLayout$LayoutParams)viewById.getLayoutParams();
            layoutParams.setMarginStart(0);
            layoutParams.setMarginEnd(0);
            viewById.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        }
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558847, viewGroup, false);
        (this.mRecyclerView = (RecyclerView)inflate.findViewById(2131362510)).setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(this.getContext(), 1, false));
        this.mRecyclerView.setAdapter((RecyclerView.Adapter)this.mAdapter);
        this.getLoaderManager().initLoader(0, (Bundle)null, (LoaderManager$LoaderCallbacks)new TimeZoneDataLoader.LoaderCreator(this.getContext(), new _$$Lambda$MBKbnic3yruONZHLQGUj0vAB5hk(this)));
        return inflate;
    }
    
    public boolean onQueryTextChange(final String s) {
        if (this.mAdapter != null) {
            this.mAdapter.getFilter().filter((CharSequence)s);
        }
        return false;
    }
    
    public boolean onQueryTextSubmit(final String s) {
        return false;
    }
    
    public void onTimeZoneDataReady(final TimeZoneData mTimeZoneData) {
        if (this.mTimeZoneData == null && mTimeZoneData != null) {
            this.mTimeZoneData = mTimeZoneData;
            this.mAdapter = this.createAdapter(this.mTimeZoneData);
            if (this.mRecyclerView != null) {
                this.mRecyclerView.setAdapter((RecyclerView.Adapter)this.mAdapter);
            }
        }
    }
    
    public interface OnListItemClickListener<T extends BaseTimeZoneAdapter.AdapterItem>
    {
        void onListItemClick(final T p0);
    }
}
