package com.android.settings.datetime.timezone;

import java.util.Locale;
import java.util.Formatter;
import android.text.SpannableStringBuilder;
import android.text.Spannable;
import android.content.res.Resources;

public class SpannableUtil
{
    public static Spannable getResourcesText(final Resources resources, final int n, final Object... array) {
        final Locale value = resources.getConfiguration().getLocales().get(0);
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        new Formatter((Appendable)spannableStringBuilder, value).format(resources.getString(n), array);
        return (Spannable)spannableStringBuilder;
    }
}
