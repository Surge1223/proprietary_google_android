package com.android.settings.datetime.timezone;

import android.support.v7.preference.Preference;
import android.icu.util.Calendar;
import android.icu.impl.OlsonTimeZone;
import android.icu.util.TimeZoneTransition;
import android.icu.util.TimeZone;
import android.icu.text.DisplayContext;
import android.content.Context;
import android.icu.text.DateFormat;
import java.util.Date;

public class TimeZoneInfoPreferenceController extends BaseTimeZonePreferenceController
{
    private static final String PREFERENCE_KEY = "footer_preference";
    Date mDate;
    private final DateFormat mDateFormat;
    private TimeZoneInfo mTimeZoneInfo;
    
    public TimeZoneInfoPreferenceController(final Context context) {
        super(context, "footer_preference");
        (this.mDateFormat = DateFormat.getDateInstance(1)).setContext(DisplayContext.CAPITALIZATION_NONE);
        this.mDate = new Date();
    }
    
    private TimeZoneTransition findNextDstTransition(final TimeZone timeZone) {
        if (!(timeZone instanceof OlsonTimeZone)) {
            return null;
        }
        final OlsonTimeZone olsonTimeZone = (OlsonTimeZone)timeZone;
        TimeZoneTransition nextTransition = olsonTimeZone.getNextTransition(this.mDate.getTime(), false);
        while (nextTransition.getTo().getDSTSavings() == nextTransition.getFrom().getDSTSavings()) {
            final TimeZoneTransition nextTransition2 = olsonTimeZone.getNextTransition(nextTransition.getTime(), false);
            if ((nextTransition = nextTransition2) == null) {
                nextTransition = nextTransition2;
                return nextTransition;
            }
        }
        return nextTransition;
    }
    
    private CharSequence formatInfo(final TimeZoneInfo timeZoneInfo) {
        final CharSequence formatOffsetAndName = this.formatOffsetAndName(timeZoneInfo);
        final TimeZone timeZone = timeZoneInfo.getTimeZone();
        if (!timeZone.observesDaylightTime()) {
            return this.mContext.getString(2131890432, new Object[] { formatOffsetAndName });
        }
        final TimeZoneTransition nextDstTransition = this.findNextDstTransition(timeZone);
        if (nextDstTransition == null) {
            return null;
        }
        final boolean b = nextDstTransition.getTo().getDSTSavings() != 0;
        String s;
        if (b) {
            s = timeZoneInfo.getDaylightName();
        }
        else {
            s = timeZoneInfo.getStandardName();
        }
        String s2 = s;
        if (s == null) {
            String s3;
            if (b) {
                s3 = this.mContext.getString(2131890438);
            }
            else {
                s3 = this.mContext.getString(2131890439);
            }
            s2 = s3;
        }
        final Calendar instance = Calendar.getInstance(timeZone);
        instance.setTimeInMillis(nextDstTransition.getTime());
        return (CharSequence)SpannableUtil.getResourcesText(this.mContext.getResources(), 2131890431, formatOffsetAndName, s2, this.mDateFormat.format((Object)instance));
    }
    
    private CharSequence formatOffsetAndName(final TimeZoneInfo timeZoneInfo) {
        String s;
        if ((s = timeZoneInfo.getGenericName()) == null) {
            if (timeZoneInfo.getTimeZone().inDaylightTime(this.mDate)) {
                s = timeZoneInfo.getDaylightName();
            }
            else {
                s = timeZoneInfo.getStandardName();
            }
        }
        if (s == null) {
            return timeZoneInfo.getGmtOffset().toString();
        }
        return (CharSequence)SpannableUtil.getResourcesText(this.mContext.getResources(), 2131890433, timeZoneInfo.getGmtOffset(), s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    public TimeZoneInfo getTimeZoneInfo() {
        return this.mTimeZoneInfo;
    }
    
    public void setTimeZoneInfo(final TimeZoneInfo mTimeZoneInfo) {
        this.mTimeZoneInfo = mTimeZoneInfo;
    }
    
    @Override
    public void updateState(final Preference preference) {
        CharSequence formatInfo;
        if (this.mTimeZoneInfo == null) {
            formatInfo = "";
        }
        else {
            formatInfo = this.formatInfo(this.mTimeZoneInfo);
        }
        preference.setTitle(formatInfo);
        preference.setVisible(this.mTimeZoneInfo != null);
    }
}
