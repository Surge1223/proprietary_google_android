package com.android.settings.datetime.timezone.model;

import java.util.Collections;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Predicate;
import java.util.List;
import libcore.util.CountryTimeZones;
import libcore.util.CountryTimeZones$TimeZoneMapping;
import java.util.function.Function;

public final class _$$Lambda$FilteredCountryTimeZones$ISUVeCzEqV6U2C82Sgby5UdDf3Y implements Function
{
    @Override
    public final Object apply(final Object o) {
        return ((CountryTimeZones$TimeZoneMapping)o).timeZoneId;
    }
}
