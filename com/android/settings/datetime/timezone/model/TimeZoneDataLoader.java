package com.android.settings.datetime.timezone.model;

import android.content.Loader;
import android.os.Bundle;
import android.app.LoaderManager$LoaderCallbacks;
import android.content.Context;
import com.android.settingslib.utils.AsyncLoader;

public class TimeZoneDataLoader extends AsyncLoader<TimeZoneData>
{
    public TimeZoneDataLoader(final Context context) {
        super(context);
    }
    
    public TimeZoneData loadInBackground() {
        return TimeZoneData.getInstance();
    }
    
    @Override
    protected void onDiscardResult(final TimeZoneData timeZoneData) {
    }
    
    public static class LoaderCreator implements LoaderManager$LoaderCallbacks<TimeZoneData>
    {
        private final OnDataReadyCallback mCallback;
        private final Context mContext;
        
        public LoaderCreator(final Context mContext, final OnDataReadyCallback mCallback) {
            this.mContext = mContext;
            this.mCallback = mCallback;
        }
        
        public Loader onCreateLoader(final int n, final Bundle bundle) {
            return (Loader)new TimeZoneDataLoader(this.mContext);
        }
        
        public void onLoadFinished(final Loader<TimeZoneData> loader, final TimeZoneData timeZoneData) {
            if (this.mCallback != null) {
                this.mCallback.onTimeZoneDataReady(timeZoneData);
            }
        }
        
        public void onLoaderReset(final Loader<TimeZoneData> loader) {
        }
    }
    
    public interface OnDataReadyCallback
    {
        void onTimeZoneDataReady(final TimeZoneData p0);
    }
}
