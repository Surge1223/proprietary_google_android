package com.android.settings.datetime.timezone.model;

import java.util.Collections;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.List;
import libcore.util.CountryTimeZones;
import libcore.util.CountryTimeZones$TimeZoneMapping;
import java.util.function.Predicate;

public final class _$$Lambda$FilteredCountryTimeZones$4MxYnMuZMfSQu2iAD_J0AM_CAoE implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return ((CountryTimeZones$TimeZoneMapping)o).showInPicker && (((CountryTimeZones$TimeZoneMapping)o).notUsedAfter == null || ((CountryTimeZones$TimeZoneMapping)o).notUsedAfter.longValue() >= 1514764800000L);
    }
}
