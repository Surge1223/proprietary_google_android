package com.android.settings.datetime.timezone.model;

import libcore.util.CountryTimeZones;
import android.support.v4.util.ArraySet;
import java.util.Locale;
import java.util.Iterator;
import java.util.Collections;
import java.util.HashSet;
import libcore.util.TimeZoneFinder;
import java.util.List;
import java.util.Set;
import libcore.util.CountryZonesFinder;
import java.lang.ref.WeakReference;

public class TimeZoneData
{
    private static WeakReference<TimeZoneData> sCache;
    private final CountryZonesFinder mCountryZonesFinder;
    private final Set<String> mRegionIds;
    
    static {
        TimeZoneData.sCache = null;
    }
    
    public TimeZoneData(final CountryZonesFinder mCountryZonesFinder) {
        this.mCountryZonesFinder = mCountryZonesFinder;
        this.mRegionIds = getNormalizedRegionIds(this.mCountryZonesFinder.lookupAllCountryIsoCodes());
    }
    
    public static TimeZoneData getInstance() {
        synchronized (TimeZoneData.class) {
            TimeZoneData timeZoneData;
            if (TimeZoneData.sCache == null) {
                timeZoneData = null;
            }
            else {
                timeZoneData = TimeZoneData.sCache.get();
            }
            if (timeZoneData != null) {
                return timeZoneData;
            }
            final TimeZoneData timeZoneData2 = new TimeZoneData(TimeZoneFinder.getInstance().getCountryZonesFinder());
            TimeZoneData.sCache = new WeakReference<TimeZoneData>(timeZoneData2);
            return timeZoneData2;
        }
    }
    
    private static Set<String> getNormalizedRegionIds(final List<String> list) {
        final HashSet<String> set = new HashSet<String>(list.size());
        final Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            set.add(normalizeRegionId(iterator.next()));
        }
        return (Set<String>)Collections.unmodifiableSet((Set<?>)set);
    }
    
    public static String normalizeRegionId(String upperCase) {
        if (upperCase == null) {
            upperCase = null;
        }
        else {
            upperCase = upperCase.toUpperCase(Locale.US);
        }
        return upperCase;
    }
    
    public Set<String> getRegionIds() {
        return this.mRegionIds;
    }
    
    public Set<String> lookupCountryCodesForZoneId(final String s) {
        if (s == null) {
            return Collections.emptySet();
        }
        final List lookupCountryTimeZonesForZoneId = this.mCountryZonesFinder.lookupCountryTimeZonesForZoneId(s);
        final ArraySet<String> set = new ArraySet<String>();
        final Iterator<CountryTimeZones> iterator = lookupCountryTimeZonesForZoneId.iterator();
        while (iterator.hasNext()) {
            final FilteredCountryTimeZones filteredCountryTimeZones = new FilteredCountryTimeZones(iterator.next());
            if (filteredCountryTimeZones.getTimeZoneIds().contains(s)) {
                set.add(filteredCountryTimeZones.getRegionId());
            }
        }
        return set;
    }
    
    public FilteredCountryTimeZones lookupCountryTimeZones(final String s) {
        final FilteredCountryTimeZones filteredCountryTimeZones = null;
        CountryTimeZones lookupCountryTimeZones;
        if (s == null) {
            lookupCountryTimeZones = null;
        }
        else {
            lookupCountryTimeZones = this.mCountryZonesFinder.lookupCountryTimeZones(s);
        }
        FilteredCountryTimeZones filteredCountryTimeZones2;
        if (lookupCountryTimeZones == null) {
            filteredCountryTimeZones2 = filteredCountryTimeZones;
        }
        else {
            filteredCountryTimeZones2 = new FilteredCountryTimeZones(lookupCountryTimeZones);
        }
        return filteredCountryTimeZones2;
    }
}
