package com.android.settings.datetime.timezone.model;

import libcore.util.CountryTimeZones$TimeZoneMapping;
import java.util.Collections;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.List;
import libcore.util.CountryTimeZones;

public class FilteredCountryTimeZones
{
    private final CountryTimeZones mCountryTimeZones;
    private final List<String> mTimeZoneIds;
    
    public FilteredCountryTimeZones(final CountryTimeZones mCountryTimeZones) {
        this.mCountryTimeZones = mCountryTimeZones;
        this.mTimeZoneIds = Collections.unmodifiableList((List<? extends String>)mCountryTimeZones.getTimeZoneMappings().stream().filter((Predicate)_$$Lambda$FilteredCountryTimeZones$4MxYnMuZMfSQu2iAD_J0AM_CAoE.INSTANCE).map((Function)_$$Lambda$FilteredCountryTimeZones$ISUVeCzEqV6U2C82Sgby5UdDf3Y.INSTANCE).collect(Collectors.toList()));
    }
    
    public String getRegionId() {
        return TimeZoneData.normalizeRegionId(this.mCountryTimeZones.getCountryIso());
    }
    
    public List<String> getTimeZoneIds() {
        return this.mTimeZoneIds;
    }
}
