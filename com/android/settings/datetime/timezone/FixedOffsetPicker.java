package com.android.settings.datetime.timezone;

import com.android.settings.datetime.timezone.model.TimeZoneData;
import java.util.Collections;
import java.util.Locale;
import android.icu.util.TimeZone;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FixedOffsetPicker extends BaseTimeZoneInfoPicker
{
    public FixedOffsetPicker() {
        super(2131887320, 2131888971, false, false);
    }
    
    private List<TimeZoneInfo> loadFixedOffsets() {
        final TimeZoneInfo.Formatter formatter = new TimeZoneInfo.Formatter(this.getLocale(), new Date());
        final ArrayList<TimeZoneInfo> list = new ArrayList<TimeZoneInfo>();
        list.add(formatter.format(TimeZone.getFrozenTimeZone("Etc/UTC")));
        for (int i = 12; i >= -14; --i) {
            if (i != 0) {
                list.add(formatter.format(TimeZone.getFrozenTimeZone(String.format(Locale.US, "Etc/GMT%+d", i))));
            }
        }
        return (List<TimeZoneInfo>)Collections.unmodifiableList((List<?>)list);
    }
    
    @Override
    public List<TimeZoneInfo> getAllTimeZoneInfos(final TimeZoneData timeZoneData) {
        return this.loadFixedOffsets();
    }
    
    public int getMetricsCategory() {
        return 1357;
    }
}
