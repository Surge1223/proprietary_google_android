package com.android.settings.datetime.timezone;

import android.support.v7.preference.Preference;
import android.content.Context;

public class RegionZonePreferenceController extends BaseTimeZonePreferenceController
{
    private static final String PREFERENCE_KEY = "region_zone";
    private boolean mIsClickable;
    private TimeZoneInfo mTimeZoneInfo;
    
    public RegionZonePreferenceController(final Context context) {
        super(context, "region_zone");
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public CharSequence getSummary() {
        Object resourcesText;
        if (this.mTimeZoneInfo == null) {
            resourcesText = "";
        }
        else {
            resourcesText = SpannableUtil.getResourcesText(this.mContext.getResources(), 2131890430, this.mTimeZoneInfo.getExemplarLocation(), this.mTimeZoneInfo.getGmtOffset());
        }
        return (CharSequence)resourcesText;
    }
    
    public TimeZoneInfo getTimeZoneInfo() {
        return this.mTimeZoneInfo;
    }
    
    public boolean isClickable() {
        return this.mIsClickable;
    }
    
    public void setClickable(final boolean mIsClickable) {
        this.mIsClickable = mIsClickable;
    }
    
    public void setTimeZoneInfo(final TimeZoneInfo mTimeZoneInfo) {
        this.mTimeZoneInfo = mTimeZoneInfo;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        preference.setEnabled(this.isClickable());
    }
}
