package com.android.settings.datetime.timezone;

import com.android.settings.datetime.timezone.model.TimeZoneData;
import com.android.settings.datetime.timezone.model.TimeZoneDataLoader;

public final class _$$Lambda$MBKbnic3yruONZHLQGUj0vAB5hk implements OnDataReadyCallback
{
    @Override
    public final void onTimeZoneDataReady(final TimeZoneData timeZoneData) {
        this.f$0.onTimeZoneDataReady(timeZoneData);
    }
}
