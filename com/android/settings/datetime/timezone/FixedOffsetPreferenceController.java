package com.android.settings.datetime.timezone;

import android.content.Context;

public class FixedOffsetPreferenceController extends BaseTimeZonePreferenceController
{
    private static final String PREFERENCE_KEY = "fixed_offset";
    private TimeZoneInfo mTimeZoneInfo;
    
    public FixedOffsetPreferenceController(final Context context) {
        super(context, "fixed_offset");
    }
    
    @Override
    public CharSequence getSummary() {
        if (this.mTimeZoneInfo == null) {
            return "";
        }
        final String standardName = this.mTimeZoneInfo.getStandardName();
        if (standardName == null) {
            return this.mTimeZoneInfo.getGmtOffset();
        }
        return (CharSequence)SpannableUtil.getResourcesText(this.mContext.getResources(), 2131890433, this.mTimeZoneInfo.getGmtOffset(), standardName);
    }
    
    public TimeZoneInfo getTimeZoneInfo() {
        return this.mTimeZoneInfo;
    }
    
    public void setTimeZoneInfo(final TimeZoneInfo mTimeZoneInfo) {
        this.mTimeZoneInfo = mTimeZoneInfo;
    }
}
