package com.android.settings.datetime.timezone;

import android.view.View.OnClickListener;
import android.widget.TextView;
import java.util.Iterator;
import java.util.ArrayList;
import android.text.TextUtils;
import android.widget.Filter$FilterResults;
import android.icu.text.BreakIterator;
import android.widget.Filter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.Locale;
import java.util.List;
import com.android.settings.datetime.timezone.BaseTimeZoneAdapter$com.android.settings.datetime.timezone.BaseTimeZoneAdapter$ArrayFilter;
import android.support.v7.widget.RecyclerView;

public class BaseTimeZoneAdapter<T extends AdapterItem> extends Adapter<ViewHolder>
{
    static final int TYPE_HEADER = 0;
    static final int TYPE_ITEM = 1;
    private BaseTimeZoneAdapter$ArrayFilter mFilter;
    private final CharSequence mHeaderText;
    private List<T> mItems;
    private final Locale mLocale;
    private final BaseTimeZonePicker.OnListItemClickListener<T> mOnListItemClickListener;
    private final List<T> mOriginalItems;
    private final boolean mShowHeader;
    private final boolean mShowItemSummary;
    
    public BaseTimeZoneAdapter(final List<T> list, final BaseTimeZonePicker.OnListItemClickListener<T> mOnListItemClickListener, final Locale mLocale, final boolean mShowItemSummary, final CharSequence mHeaderText) {
        this.mOriginalItems = list;
        this.mItems = list;
        this.mOnListItemClickListener = mOnListItemClickListener;
        this.mLocale = mLocale;
        this.mShowItemSummary = mShowItemSummary;
        this.mShowHeader = (mHeaderText != null);
        this.mHeaderText = mHeaderText;
        this.setHasStableIds(true);
    }
    
    private int getHeaderCount() {
        return this.mShowHeader ? 1 : 0;
    }
    
    private boolean isPositionHeader(final int n) {
        return this.mShowHeader && n == 0;
    }
    
    public T getDataItem(final int n) {
        return this.mItems.get(n - this.getHeaderCount());
    }
    
    public BaseTimeZoneAdapter$ArrayFilter getFilter() {
        if (this.mFilter == null) {
            this.mFilter = new ArrayFilter();
        }
        return (BaseTimeZoneAdapter$ArrayFilter)this.mFilter;
    }
    
    @Override
    public int getItemCount() {
        return this.mItems.size() + this.getHeaderCount();
    }
    
    @Override
    public long getItemId(final int n) {
        long itemId;
        if (this.isPositionHeader(n)) {
            itemId = -1L;
        }
        else {
            itemId = this.getDataItem(n).getItemId();
        }
        return itemId;
    }
    
    @Override
    public int getItemViewType(final int n) {
        return (this.isPositionHeader(n) ^ true) ? 1 : 0;
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int visibility) {
        if (viewHolder instanceof HeaderViewHolder) {
            ((HeaderViewHolder)viewHolder).setText(this.mHeaderText);
        }
        else if (viewHolder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder)viewHolder;
            itemViewHolder.setAdapterItem(this.getDataItem(visibility));
            final View mSummaryFrame = itemViewHolder.mSummaryFrame;
            if (this.mShowItemSummary) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            mSummaryFrame.setVisibility(visibility);
        }
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        final LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected viewType: ");
                sb.append(n);
                throw new IllegalArgumentException(sb.toString());
            }
            case 1: {
                return new ItemViewHolder<Object>(from.inflate(2131558849, viewGroup, false), (BaseTimeZonePicker.OnListItemClickListener<AdapterItem>)this.mOnListItemClickListener);
            }
            case 0: {
                return new HeaderViewHolder(from.inflate(2131558650, viewGroup, false));
            }
        }
    }
    
    @Override
    public final void setHasStableIds(final boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
    
    public interface AdapterItem
    {
        String getCurrentTime();
        
        String getIconText();
        
        long getItemId();
        
        String[] getSearchKeys();
        
        CharSequence getSummary();
        
        CharSequence getTitle();
    }
    
    public class ArrayFilter extends Filter
    {
        private BreakIterator mBreakIterator;
        
        public ArrayFilter() {
            this.mBreakIterator = BreakIterator.getWordInstance(BaseTimeZoneAdapter.this.mLocale);
        }
        
        protected Filter$FilterResults performFiltering(final CharSequence charSequence) {
            List<AdapterItem> access$100;
            if (TextUtils.isEmpty(charSequence)) {
                access$100 = (List<AdapterItem>)BaseTimeZoneAdapter.this.mOriginalItems;
            }
            else {
                final String lowerCase = charSequence.toString().toLowerCase(BaseTimeZoneAdapter.this.mLocale);
                access$100 = new ArrayList<AdapterItem>();
                for (final AdapterItem adapterItem : BaseTimeZoneAdapter.this.mOriginalItems) {
                    final String[] searchKeys = adapterItem.getSearchKeys();
                Label_0224:
                    for (int length = searchKeys.length, i = 0; i < length; ++i) {
                        final String lowerCase2 = searchKeys[i].toLowerCase(BaseTimeZoneAdapter.this.mLocale);
                        if (lowerCase2.startsWith(lowerCase)) {
                            access$100.add(adapterItem);
                            break;
                        }
                        this.mBreakIterator.setText(lowerCase2);
                        int n = 0;
                        int next;
                        for (int j = this.mBreakIterator.next(); j != -1; j = next) {
                            if (this.mBreakIterator.getRuleStatus() != 0 && lowerCase2.startsWith(lowerCase, n)) {
                                access$100.add(adapterItem);
                                break Label_0224;
                            }
                            next = this.mBreakIterator.next();
                            n = j;
                        }
                    }
                }
            }
            final Filter$FilterResults filter$FilterResults = new Filter$FilterResults();
            filter$FilterResults.values = access$100;
            filter$FilterResults.count = access$100.size();
            return filter$FilterResults;
        }
        
        public void publishResults(final CharSequence charSequence, final Filter$FilterResults filter$FilterResults) {
            BaseTimeZoneAdapter.this.mItems = (List<T>)filter$FilterResults.values;
            BaseTimeZoneAdapter.this.notifyDataSetChanged();
        }
    }
    
    private static class HeaderViewHolder extends ViewHolder
    {
        private final TextView mTextView;
        
        public HeaderViewHolder(final View view) {
            super(view);
            this.mTextView = (TextView)view.findViewById(16908310);
        }
        
        public void setText(final CharSequence text) {
            this.mTextView.setText(text);
        }
    }
    
    public static class ItemViewHolder<T extends AdapterItem> extends ViewHolder implements View.OnClickListener
    {
        final TextView mIconTextView;
        private T mItem;
        final BaseTimeZonePicker.OnListItemClickListener<T> mOnListItemClickListener;
        final View mSummaryFrame;
        final TextView mSummaryView;
        final TextView mTimeView;
        final TextView mTitleView;
        
        public ItemViewHolder(final View view, final BaseTimeZonePicker.OnListItemClickListener<T> mOnListItemClickListener) {
            super(view);
            view.setOnClickListener((View.OnClickListener)this);
            this.mSummaryFrame = view.findViewById(2131362672);
            this.mTitleView = (TextView)view.findViewById(16908310);
            this.mIconTextView = (TextView)view.findViewById(2131362241);
            this.mSummaryView = (TextView)view.findViewById(16908304);
            this.mTimeView = (TextView)view.findViewById(2131362029);
            this.mOnListItemClickListener = mOnListItemClickListener;
        }
        
        public void onClick(final View view) {
            this.mOnListItemClickListener.onListItemClick(this.mItem);
        }
        
        public void setAdapterItem(final T mItem) {
            this.mItem = mItem;
            this.mTitleView.setText(((AdapterItem)mItem).getTitle());
            this.mIconTextView.setText((CharSequence)((AdapterItem)mItem).getIconText());
            this.mSummaryView.setText(((AdapterItem)mItem).getSummary());
            this.mTimeView.setText((CharSequence)((AdapterItem)mItem).getCurrentTime());
        }
    }
}
