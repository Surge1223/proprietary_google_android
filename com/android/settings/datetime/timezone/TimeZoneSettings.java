package com.android.settings.datetime.timezone;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.datetime.timezone.model.TimeZoneDataLoader;
import java.util.Objects;
import android.content.Intent;
import java.util.Set;
import java.util.ArrayList;
import java.util.Date;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.app.Fragment;
import com.android.settings.core.SubSettingLauncher;
import android.icu.util.TimeZone;
import android.support.v7.preference.PreferenceCategory;
import android.content.SharedPreferences$Editor;
import android.app.AlarmManager;
import android.os.Bundle;
import com.android.settings.datetime.timezone.model.FilteredCountryTimeZones;
import android.util.Log;
import com.android.settings.datetime.timezone.model.TimeZoneData;
import java.util.Locale;
import com.android.settings.dashboard.DashboardFragment;

public class TimeZoneSettings extends DashboardFragment
{
    private Locale mLocale;
    private boolean mSelectByRegion;
    private String mSelectedTimeZoneId;
    private TimeZoneData mTimeZoneData;
    private TimeZoneInfo.Formatter mTimeZoneInfoFormatter;
    
    private String findRegionIdForTzId(final String s) {
        return this.findRegionIdForTzId(s, this.getPreferenceManager().getSharedPreferences().getString("time_zone_region", (String)null), this.getLocaleRegionId());
    }
    
    private String getLocaleRegionId() {
        return this.mLocale.getCountry().toUpperCase(Locale.US);
    }
    
    private static boolean isFixedOffset(final String s) {
        return s.startsWith("Etc/GMT") || s.equals("Etc/UTC");
    }
    
    private void onFixedOffsetZoneChanged(final String mSelectedTimeZoneId) {
        this.setDisplayedFixedOffsetTimeZoneInfo(this.mSelectedTimeZoneId = mSelectedTimeZoneId);
        this.saveTimeZone(null, this.mSelectedTimeZoneId);
        this.setSelectByRegion(false);
    }
    
    private void onRegionZoneChanged(final String displayedRegion, final String mSelectedTimeZoneId) {
        final FilteredCountryTimeZones lookupCountryTimeZones = this.mTimeZoneData.lookupCountryTimeZones(displayedRegion);
        if (lookupCountryTimeZones != null && lookupCountryTimeZones.getTimeZoneIds().contains(mSelectedTimeZoneId)) {
            this.mSelectedTimeZoneId = mSelectedTimeZoneId;
            this.setDisplayedRegion(displayedRegion);
            this.setDisplayedTimeZoneInfo(displayedRegion, this.mSelectedTimeZoneId);
            this.saveTimeZone(displayedRegion, this.mSelectedTimeZoneId);
            this.setSelectByRegion(true);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown time zone id is selected: ");
        sb.append(mSelectedTimeZoneId);
        Log.e("TimeZoneSettings", sb.toString());
    }
    
    private void onRegionZonePreferenceClicked() {
        final Bundle bundle = new Bundle();
        bundle.putString("com.android.settings.datetime.timezone.region_id", this.use(RegionPreferenceController.class).getRegionId());
        this.startPickerFragment(RegionZonePicker.class, bundle, 2);
    }
    
    private void onTimeZoneDataReady(final TimeZoneData mTimeZoneData) {
        if (this.mTimeZoneData == null && mTimeZoneData != null) {
            this.mTimeZoneData = mTimeZoneData;
            this.setupForCurrentTimeZone();
            this.getActivity().invalidateOptionsMenu();
        }
    }
    
    private void saveTimeZone(final String s, final String timeZone) {
        final SharedPreferences$Editor edit = this.getPreferenceManager().getSharedPreferences().edit();
        if (s == null) {
            edit.remove("time_zone_region");
        }
        else {
            edit.putString("time_zone_region", s);
        }
        edit.apply();
        ((AlarmManager)this.getActivity().getSystemService((Class)AlarmManager.class)).setTimeZone(timeZone);
    }
    
    private void setDisplayedFixedOffsetTimeZoneInfo(final String s) {
        if (isFixedOffset(s)) {
            this.use(FixedOffsetPreferenceController.class).setTimeZoneInfo(this.mTimeZoneInfoFormatter.format(s));
        }
        else {
            this.use(FixedOffsetPreferenceController.class).setTimeZoneInfo(null);
        }
        this.updatePreferenceStates();
    }
    
    private void setDisplayedRegion(final String regionId) {
        this.use(RegionPreferenceController.class).setRegionId(regionId);
        this.updatePreferenceStates();
    }
    
    private void setDisplayedTimeZoneInfo(final String s, final String s2) {
        TimeZoneInfo format;
        if (s2 == null) {
            format = null;
        }
        else {
            format = this.mTimeZoneInfoFormatter.format(s2);
        }
        final FilteredCountryTimeZones lookupCountryTimeZones = this.mTimeZoneData.lookupCountryTimeZones(s);
        this.use(RegionZonePreferenceController.class).setTimeZoneInfo(format);
        final RegionZonePreferenceController regionZonePreferenceController = this.use(RegionZonePreferenceController.class);
        boolean clickable = true;
        if (format != null) {
            clickable = (lookupCountryTimeZones != null && lookupCountryTimeZones.getTimeZoneIds().size() > 1 && clickable);
        }
        regionZonePreferenceController.setClickable(clickable);
        this.use(TimeZoneInfoPreferenceController.class).setTimeZoneInfo(format);
        this.updatePreferenceStates();
    }
    
    private void setPreferenceCategoryVisible(final PreferenceCategory preferenceCategory, final boolean b) {
        preferenceCategory.setVisible(b);
        for (int i = 0; i < preferenceCategory.getPreferenceCount(); ++i) {
            preferenceCategory.getPreference(i).setVisible(b);
        }
    }
    
    private void setSelectByRegion(final boolean mSelectByRegion) {
        this.mSelectByRegion = mSelectByRegion;
        this.setPreferenceCategoryVisible((PreferenceCategory)this.findPreference("time_zone_region_preference_category"), mSelectByRegion);
        this.setPreferenceCategoryVisible((PreferenceCategory)this.findPreference("time_zone_fixed_offset_preference_category"), mSelectByRegion ^ true);
        String localeRegionId = this.getLocaleRegionId();
        if (!this.mTimeZoneData.getRegionIds().contains(localeRegionId)) {
            localeRegionId = null;
        }
        this.setDisplayedRegion(localeRegionId);
        this.setDisplayedTimeZoneInfo(localeRegionId, null);
        if (!this.mSelectByRegion) {
            this.setDisplayedFixedOffsetTimeZoneInfo(this.mSelectedTimeZoneId);
            return;
        }
        final String regionIdForTzId = this.findRegionIdForTzId(this.mSelectedTimeZoneId);
        if (regionIdForTzId != null) {
            this.setDisplayedRegion(regionIdForTzId);
            this.setDisplayedTimeZoneInfo(regionIdForTzId, this.mSelectedTimeZoneId);
        }
    }
    
    private void setupForCurrentTimeZone() {
        this.mSelectedTimeZoneId = TimeZone.getDefault().getID();
        this.setSelectByRegion(isFixedOffset(this.mSelectedTimeZoneId) ^ true);
    }
    
    private void startFixedOffsetPicker() {
        this.startPickerFragment(FixedOffsetPicker.class, new Bundle(), 3);
    }
    
    private void startPickerFragment(final Class<? extends BaseTimeZonePicker> clazz, final Bundle arguments, final int n) {
        new SubSettingLauncher(this.getContext()).setDestination(clazz.getCanonicalName()).setArguments(arguments).setSourceMetricsCategory(this.getMetricsCategory()).setResultListener(this, n).launch();
    }
    
    private void startRegionPicker() {
        this.startPickerFragment(RegionSearchPicker.class, new Bundle(), 1);
    }
    
    public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        this.mLocale = context.getResources().getConfiguration().getLocales().get(0);
        this.mTimeZoneInfoFormatter = new TimeZoneInfo.Formatter(this.mLocale, new Date());
        final ArrayList<RegionPreferenceController> list = (ArrayList<RegionPreferenceController>)new ArrayList<AbstractPreferenceController>();
        final RegionPreferenceController regionPreferenceController = new RegionPreferenceController(context);
        regionPreferenceController.setOnClickListener(new _$$Lambda$TimeZoneSettings$vqMeoCUXFQsF8oLE4z3Gn5iFYMM(this));
        final RegionZonePreferenceController regionZonePreferenceController = new RegionZonePreferenceController(context);
        regionZonePreferenceController.setOnClickListener(new _$$Lambda$TimeZoneSettings$fBefFKEAVxzXT5oriz7X9NJj6a0(this));
        final FixedOffsetPreferenceController fixedOffsetPreferenceController = new FixedOffsetPreferenceController(context);
        fixedOffsetPreferenceController.setOnClickListener(new _$$Lambda$TimeZoneSettings$Ah3tL_2LTanl7tTAw64r8xCK07o(this));
        list.add(regionPreferenceController);
        list.add((RegionPreferenceController)regionZonePreferenceController);
        list.add((RegionPreferenceController)fixedOffsetPreferenceController);
        return (List<AbstractPreferenceController>)list;
    }
    
    String findRegionIdForTzId(final String s, final String s2, final String s3) {
        final Set<String> lookupCountryCodesForZoneId = this.mTimeZoneData.lookupCountryCodesForZoneId(s);
        if (lookupCountryCodesForZoneId.size() == 0) {
            return null;
        }
        if (s2 != null && lookupCountryCodesForZoneId.contains(s2)) {
            return s2;
        }
        if (s3 != null && lookupCountryCodesForZoneId.contains(s3)) {
            return s3;
        }
        return lookupCountryCodesForZoneId.toArray(new String[lookupCountryCodesForZoneId.size()])[0];
    }
    
    @Override
    protected String getLogTag() {
        return "TimeZoneSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 515;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082845;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n2 == -1 && intent != null) {
            switch (n) {
                case 3: {
                    final String stringExtra = intent.getStringExtra("com.android.settings.datetime.timezone.result_time_zone_id");
                    if (stringExtra != null && !stringExtra.equals(this.mSelectedTimeZoneId)) {
                        this.onFixedOffsetZoneChanged(stringExtra);
                        break;
                    }
                    break;
                }
                case 1:
                case 2: {
                    final String stringExtra2 = intent.getStringExtra("com.android.settings.datetime.timezone.result_region_id");
                    final String stringExtra3 = intent.getStringExtra("com.android.settings.datetime.timezone.result_time_zone_id");
                    if (!Objects.equals(stringExtra2, this.use(RegionPreferenceController.class).getRegionId()) || !Objects.equals(stringExtra3, this.mSelectedTimeZoneId)) {
                        this.onRegionZoneChanged(stringExtra2, stringExtra3);
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setPreferenceCategoryVisible((PreferenceCategory)this.findPreference("time_zone_region_preference_category"), false);
        this.setPreferenceCategoryVisible((PreferenceCategory)this.findPreference("time_zone_fixed_offset_preference_category"), false);
        this.getLoaderManager().initLoader(0, (Bundle)null, (LoaderManager$LoaderCallbacks)new TimeZoneDataLoader.LoaderCreator(this.getContext(), new _$$Lambda$TimeZoneSettings$CFHMJtb3KFCwNTuhyOFedUZcT20(this)));
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        menu.add(0, 1, 0, 2131890437);
        menu.add(0, 2, 0, 2131890436);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return false;
            }
            case 2: {
                this.startFixedOffsetPicker();
                return true;
            }
            case 1: {
                this.startRegionPicker();
                return true;
            }
        }
    }
    
    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        final boolean b = true;
        menu.findItem(1).setVisible(this.mTimeZoneData != null && !this.mSelectByRegion);
        menu.findItem(2).setVisible(this.mTimeZoneData != null && this.mSelectByRegion && b);
    }
    
    void setTimeZoneData(final TimeZoneData mTimeZoneData) {
        this.mTimeZoneData = mTimeZoneData;
    }
}
