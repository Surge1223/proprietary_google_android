package com.android.settings.datetime.timezone;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.datetime.timezone.model.TimeZoneDataLoader;
import java.util.Objects;
import android.content.Intent;
import java.util.Set;
import java.util.ArrayList;
import java.util.Date;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import android.app.Fragment;
import com.android.settings.core.SubSettingLauncher;
import android.icu.util.TimeZone;
import android.support.v7.preference.PreferenceCategory;
import android.content.SharedPreferences$Editor;
import android.app.AlarmManager;
import android.os.Bundle;
import com.android.settings.datetime.timezone.model.FilteredCountryTimeZones;
import android.util.Log;
import com.android.settings.datetime.timezone.model.TimeZoneData;
import java.util.Locale;
import com.android.settings.dashboard.DashboardFragment;

public final class _$$Lambda$TimeZoneSettings$vqMeoCUXFQsF8oLE4z3Gn5iFYMM implements OnPreferenceClickListener
{
    @Override
    public final void onClick() {
        this.f$0.startRegionPicker();
    }
}
