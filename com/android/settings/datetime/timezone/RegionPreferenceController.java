package com.android.settings.datetime.timezone;

import android.content.Context;
import android.icu.text.LocaleDisplayNames;

public class RegionPreferenceController extends BaseTimeZonePreferenceController
{
    private static final String PREFERENCE_KEY = "region";
    private final LocaleDisplayNames mLocaleDisplayNames;
    private String mRegionId;
    
    public RegionPreferenceController(final Context context) {
        super(context, "region");
        this.mRegionId = "";
        this.mLocaleDisplayNames = LocaleDisplayNames.getInstance(context.getResources().getConfiguration().getLocales().get(0));
    }
    
    public String getRegionId() {
        return this.mRegionId;
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mLocaleDisplayNames.regionDisplayName(this.mRegionId);
    }
    
    public void setRegionId(final String mRegionId) {
        this.mRegionId = mRegionId;
    }
}
