package com.android.settings.datetime.timezone;

import android.app.Activity;
import com.android.settings.datetime.timezone.model.FilteredCountryTimeZones;
import android.util.Log;
import android.app.Fragment;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.content.Intent;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import android.icu.text.LocaleDisplayNames;
import java.util.Comparator;
import java.util.TreeSet;
import android.icu.text.Collator;
import java.util.List;
import java.util.Set;
import com.android.settings.datetime.timezone.model.TimeZoneData;

public final class _$$Lambda$RegionSearchPicker$DOJaHroZb7JziN_vdZ6PwdoM4gg implements OnListItemClickListener
{
    @Override
    public final void onListItemClick(final BaseTimeZoneAdapter.AdapterItem adapterItem) {
        this.f$0.onListItemClick((RegionSearchPicker.RegionItem)adapterItem);
    }
}
