package com.android.settings.datetime.timezone;

import java.util.Iterator;
import java.util.ArrayList;
import android.icu.text.SimpleDateFormat;
import java.util.Locale;
import android.content.Context;
import android.icu.util.Calendar;
import java.util.Date;
import android.icu.text.DateFormat;
import android.content.res.Resources;
import android.content.Intent;
import java.util.List;
import com.android.settings.datetime.timezone.model.TimeZoneData;

public abstract class BaseTimeZoneInfoPicker extends BaseTimeZonePicker
{
    protected ZoneAdapter mAdapter;
    
    protected BaseTimeZoneInfoPicker(final int n, final int n2, final boolean b, final boolean b2) {
        super(n, n2, b, b2);
    }
    
    private void onListItemClick(final TimeZoneInfoItem timeZoneInfoItem) {
        this.getActivity().setResult(-1, this.prepareResultData(timeZoneInfoItem.mTimeZoneInfo));
        this.getActivity().finish();
    }
    
    @Override
    protected BaseTimeZoneAdapter createAdapter(final TimeZoneData timeZoneData) {
        return this.mAdapter = new ZoneAdapter(this.getContext(), this.getAllTimeZoneInfos(timeZoneData), new _$$Lambda$BaseTimeZoneInfoPicker$rmIiAzryW5v4Oz5tFaKKhXINMbA(this), this.getLocale(), this.getHeaderText());
    }
    
    public abstract List<TimeZoneInfo> getAllTimeZoneInfos(final TimeZoneData p0);
    
    protected CharSequence getHeaderText() {
        return null;
    }
    
    protected Intent prepareResultData(final TimeZoneInfo timeZoneInfo) {
        return new Intent().putExtra("com.android.settings.datetime.timezone.result_time_zone_id", timeZoneInfo.getId());
    }
    
    private static class TimeZoneInfoItem implements AdapterItem
    {
        private final long mItemId;
        private final Resources mResources;
        private final String[] mSearchKeys;
        private final DateFormat mTimeFormat;
        private final TimeZoneInfo mTimeZoneInfo;
        private final String mTitle;
        
        private TimeZoneInfoItem(final long mItemId, final TimeZoneInfo mTimeZoneInfo, final Resources mResources, final DateFormat mTimeFormat) {
            this.mItemId = mItemId;
            this.mTimeZoneInfo = mTimeZoneInfo;
            this.mResources = mResources;
            this.mTimeFormat = mTimeFormat;
            this.mTitle = createTitle(mTimeZoneInfo);
            this.mSearchKeys = new String[] { this.mTitle };
        }
        
        private static String createTitle(final TimeZoneInfo timeZoneInfo) {
            String s;
            if ((s = timeZoneInfo.getExemplarLocation()) == null) {
                s = timeZoneInfo.getGenericName();
            }
            String daylightName;
            if ((daylightName = s) == null) {
                daylightName = s;
                if (timeZoneInfo.getTimeZone().inDaylightTime(new Date())) {
                    daylightName = timeZoneInfo.getDaylightName();
                }
            }
            String standardName;
            if ((standardName = daylightName) == null) {
                standardName = timeZoneInfo.getStandardName();
            }
            String value;
            if ((value = standardName) == null) {
                value = String.valueOf(timeZoneInfo.getGmtOffset());
            }
            return value;
        }
        
        @Override
        public String getCurrentTime() {
            return this.mTimeFormat.format((Object)Calendar.getInstance(this.mTimeZoneInfo.getTimeZone()));
        }
        
        @Override
        public String getIconText() {
            return null;
        }
        
        @Override
        public long getItemId() {
            return this.mItemId;
        }
        
        @Override
        public String[] getSearchKeys() {
            return this.mSearchKeys;
        }
        
        @Override
        public CharSequence getSummary() {
            String s;
            if ((s = this.mTimeZoneInfo.getGenericName()) == null) {
                if (this.mTimeZoneInfo.getTimeZone().inDaylightTime(new Date())) {
                    s = this.mTimeZoneInfo.getDaylightName();
                }
                else {
                    s = this.mTimeZoneInfo.getStandardName();
                }
            }
            if (s != null && !s.equals(this.mTitle)) {
                return (CharSequence)SpannableUtil.getResourcesText(this.mResources, 2131890433, this.mTimeZoneInfo.getGmtOffset(), s);
            }
            CharSequence gmtOffset = this.mTimeZoneInfo.getGmtOffset();
            if (gmtOffset == null || gmtOffset.toString().equals(this.mTitle)) {
                gmtOffset = "";
            }
            return gmtOffset;
        }
        
        @Override
        public CharSequence getTitle() {
            return this.mTitle;
        }
    }
    
    protected static class ZoneAdapter extends BaseTimeZoneAdapter<TimeZoneInfoItem>
    {
        public ZoneAdapter(final Context context, final List<TimeZoneInfo> list, final OnListItemClickListener<TimeZoneInfoItem> onListItemClickListener, final Locale locale, final CharSequence charSequence) {
            super(createTimeZoneInfoItems(context, list, locale), onListItemClickListener, locale, true, charSequence);
        }
        
        private static List<TimeZoneInfoItem> createTimeZoneInfoItems(final Context context, final List<TimeZoneInfo> list, final Locale locale) {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(android.text.format.DateFormat.getTimeFormatString(context), locale);
            final ArrayList<TimeZoneInfoItem> list2 = new ArrayList<TimeZoneInfoItem>(list.size());
            final Resources resources = context.getResources();
            final Iterator<TimeZoneInfo> iterator = list.iterator();
            long n = 0L;
            while (iterator.hasNext()) {
                list2.add(new TimeZoneInfoItem(n, (TimeZoneInfo)iterator.next(), resources, (DateFormat)simpleDateFormat));
                ++n;
            }
            return list2;
        }
    }
}
