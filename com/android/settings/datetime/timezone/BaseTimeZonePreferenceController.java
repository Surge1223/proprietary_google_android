package com.android.settings.datetime.timezone;

import com.google.common.base.Objects;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public abstract class BaseTimeZonePreferenceController extends BasePreferenceController
{
    private OnPreferenceClickListener mOnClickListener;
    
    protected BaseTimeZonePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.mOnClickListener != null && Objects.equal(this.getPreferenceKey(), preference.getKey())) {
            this.mOnClickListener.onClick();
            return true;
        }
        return false;
    }
    
    public void setOnClickListener(final OnPreferenceClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }
}
