package com.android.settings.datetime.timezone;

import android.app.Activity;
import com.android.settings.datetime.timezone.model.FilteredCountryTimeZones;
import android.util.Log;
import android.app.Fragment;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.content.Intent;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import android.icu.text.LocaleDisplayNames;
import java.util.Comparator;
import java.util.TreeSet;
import android.icu.text.Collator;
import java.util.List;
import java.util.Set;
import com.android.settings.datetime.timezone.model.TimeZoneData;

public class RegionSearchPicker extends BaseTimeZonePicker
{
    private BaseTimeZoneAdapter<RegionItem> mAdapter;
    private TimeZoneData mTimeZoneData;
    
    public RegionSearchPicker() {
        super(2131887321, 2131887319, true, true);
    }
    
    private List<RegionItem> createAdapterItem(final Set<String> set) {
        final TreeSet<RegionItem> set2 = new TreeSet<RegionItem>((Comparator<? super RegionItem>)new RegionInfoComparator(Collator.getInstance(this.getLocale())));
        final LocaleDisplayNames instance = LocaleDisplayNames.getInstance(this.getLocale());
        long n = 0L;
        for (final String s : set) {
            set2.add(new RegionItem(n, s, instance.regionDisplayName(s)));
            ++n;
        }
        return new ArrayList<RegionItem>(set2);
    }
    
    private void onListItemClick(final RegionItem regionItem) {
        final String id = regionItem.getId();
        final FilteredCountryTimeZones lookupCountryTimeZones = this.mTimeZoneData.lookupCountryTimeZones(id);
        final Activity activity = this.getActivity();
        if (lookupCountryTimeZones != null && !lookupCountryTimeZones.getTimeZoneIds().isEmpty()) {
            final List<String> timeZoneIds = lookupCountryTimeZones.getTimeZoneIds();
            if (timeZoneIds.size() == 1) {
                this.getActivity().setResult(-1, new Intent().putExtra("com.android.settings.datetime.timezone.result_region_id", id).putExtra("com.android.settings.datetime.timezone.result_time_zone_id", (String)timeZoneIds.get(0)));
                this.getActivity().finish();
            }
            else {
                final Bundle arguments = new Bundle();
                arguments.putString("com.android.settings.datetime.timezone.region_id", id);
                new SubSettingLauncher(this.getContext()).setDestination(RegionZonePicker.class.getCanonicalName()).setArguments(arguments).setSourceMetricsCategory(this.getMetricsCategory()).setResultListener(this, 1).launch();
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Region has no time zones: ");
        sb.append(id);
        Log.e("RegionSearchPicker", sb.toString());
        activity.setResult(0);
        activity.finish();
    }
    
    @Override
    protected BaseTimeZoneAdapter createAdapter(final TimeZoneData mTimeZoneData) {
        this.mTimeZoneData = mTimeZoneData;
        return this.mAdapter = new BaseTimeZoneAdapter<RegionItem>(this.createAdapterItem(mTimeZoneData.getRegionIds()), new _$$Lambda$RegionSearchPicker$DOJaHroZb7JziN_vdZ6PwdoM4gg(this), this.getLocale(), false, null);
    }
    
    public int getMetricsCategory() {
        return 1355;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1) {
            if (n2 == -1) {
                this.getActivity().setResult(-1, intent);
            }
            this.getActivity().finish();
        }
    }
    
    private static class RegionInfoComparator implements Comparator<RegionItem>
    {
        private final Collator mCollator;
        
        RegionInfoComparator(final Collator mCollator) {
            this.mCollator = mCollator;
        }
        
        @Override
        public int compare(final RegionItem regionItem, final RegionItem regionItem2) {
            return this.mCollator.compare((Object)regionItem.getTitle(), (Object)regionItem2.getTitle());
        }
    }
    
    static class RegionItem implements AdapterItem
    {
        private final String mId;
        private final long mItemId;
        private final String mName;
        private final String[] mSearchKeys;
        
        RegionItem(final long mItemId, final String mId, final String mName) {
            this.mId = mId;
            this.mName = mName;
            this.mItemId = mItemId;
            this.mSearchKeys = new String[] { this.mId, this.mName };
        }
        
        @Override
        public String getCurrentTime() {
            return null;
        }
        
        @Override
        public String getIconText() {
            return null;
        }
        
        public String getId() {
            return this.mId;
        }
        
        @Override
        public long getItemId() {
            return this.mItemId;
        }
        
        @Override
        public String[] getSearchKeys() {
            return this.mSearchKeys;
        }
        
        @Override
        public CharSequence getSummary() {
            return null;
        }
        
        @Override
        public CharSequence getTitle() {
            return this.mName;
        }
    }
}
