package com.android.settings.datetime.timezone;

import android.icu.text.TimeZoneNames;
import com.android.settingslib.datetime.ZoneGetter;
import android.icu.text.TimeZoneNames$NameType;
import android.icu.text.TimeZoneFormat;
import java.util.Date;
import java.util.Locale;
import android.text.TextUtils;
import android.icu.util.TimeZone;

public class TimeZoneInfo
{
    private final String mDaylightName;
    private final String mExemplarLocation;
    private final String mGenericName;
    private final CharSequence mGmtOffset;
    private final String mId;
    private final String mStandardName;
    private final TimeZone mTimeZone;
    
    public TimeZoneInfo(final Builder builder) {
        this.mTimeZone = builder.mTimeZone;
        this.mId = this.mTimeZone.getID();
        this.mGenericName = builder.mGenericName;
        this.mStandardName = builder.mStandardName;
        this.mDaylightName = builder.mDaylightName;
        this.mExemplarLocation = builder.mExemplarLocation;
        this.mGmtOffset = builder.mGmtOffset;
    }
    
    public String getDaylightName() {
        return this.mDaylightName;
    }
    
    public String getExemplarLocation() {
        return this.mExemplarLocation;
    }
    
    public String getGenericName() {
        return this.mGenericName;
    }
    
    public CharSequence getGmtOffset() {
        return this.mGmtOffset;
    }
    
    public String getId() {
        return this.mId;
    }
    
    public String getStandardName() {
        return this.mStandardName;
    }
    
    public TimeZone getTimeZone() {
        return this.mTimeZone;
    }
    
    public static class Builder
    {
        private String mDaylightName;
        private String mExemplarLocation;
        private String mGenericName;
        private CharSequence mGmtOffset;
        private String mStandardName;
        private final TimeZone mTimeZone;
        
        public Builder(final TimeZone mTimeZone) {
            if (mTimeZone != null) {
                this.mTimeZone = mTimeZone;
                return;
            }
            throw new IllegalArgumentException("TimeZone must not be null!");
        }
        
        public TimeZoneInfo build() {
            if (!TextUtils.isEmpty(this.mGmtOffset)) {
                return new TimeZoneInfo(this);
            }
            throw new IllegalStateException("gmtOffset must not be empty!");
        }
        
        public Builder setDaylightName(final String mDaylightName) {
            this.mDaylightName = mDaylightName;
            return this;
        }
        
        public Builder setExemplarLocation(final String mExemplarLocation) {
            this.mExemplarLocation = mExemplarLocation;
            return this;
        }
        
        public Builder setGenericName(final String mGenericName) {
            this.mGenericName = mGenericName;
            return this;
        }
        
        public Builder setGmtOffset(final CharSequence mGmtOffset) {
            this.mGmtOffset = mGmtOffset;
            return this;
        }
        
        public Builder setStandardName(final String mStandardName) {
            this.mStandardName = mStandardName;
            return this;
        }
    }
    
    public static class Formatter
    {
        private final Locale mLocale;
        private final Date mNow;
        private final TimeZoneFormat mTimeZoneFormat;
        
        public Formatter(final Locale mLocale, final Date mNow) {
            this.mLocale = mLocale;
            this.mNow = mNow;
            this.mTimeZoneFormat = TimeZoneFormat.getInstance(mLocale);
        }
        
        public TimeZoneInfo format(final TimeZone timeZone) {
            final String id = timeZone.getID();
            final TimeZoneNames timeZoneNames = this.mTimeZoneFormat.getTimeZoneNames();
            return new Builder(timeZone).setGenericName(timeZoneNames.getDisplayName(id, TimeZoneNames$NameType.LONG_GENERIC, this.mNow.getTime())).setStandardName(timeZoneNames.getDisplayName(id, TimeZoneNames$NameType.LONG_STANDARD, this.mNow.getTime())).setDaylightName(timeZoneNames.getDisplayName(id, TimeZoneNames$NameType.LONG_DAYLIGHT, this.mNow.getTime())).setExemplarLocation(timeZoneNames.getExemplarLocationName(id)).setGmtOffset(ZoneGetter.getGmtOffsetText(this.mTimeZoneFormat, this.mLocale, java.util.TimeZone.getTimeZone(id), this.mNow)).build();
        }
        
        public TimeZoneInfo format(final String s) {
            return this.format(TimeZone.getFrozenTimeZone(s));
        }
    }
}
