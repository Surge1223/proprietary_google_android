package com.android.settings.datetime.timezone;

import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import java.util.Iterator;
import java.util.ArrayList;
import android.icu.util.TimeZone;
import java.util.Comparator;
import java.util.TreeSet;
import android.icu.text.Collator;
import java.util.Date;
import com.android.settings.datetime.timezone.model.FilteredCountryTimeZones;
import java.util.Collection;
import java.util.Collections;
import android.util.Log;
import java.util.List;
import com.android.settings.datetime.timezone.model.TimeZoneData;

public class RegionZonePicker extends BaseTimeZoneInfoPicker
{
    private String mRegionName;
    
    public RegionZonePicker() {
        super(2131887329, 2131888971, true, false);
    }
    
    @Override
    public List<TimeZoneInfo> getAllTimeZoneInfos(final TimeZoneData timeZoneData) {
        if (this.getArguments() == null) {
            Log.e("RegionZoneSearchPicker", "getArguments() == null");
            this.getActivity().finish();
            return Collections.emptyList();
        }
        final String string = this.getArguments().getString("com.android.settings.datetime.timezone.region_id");
        final FilteredCountryTimeZones lookupCountryTimeZones = timeZoneData.lookupCountryTimeZones(string);
        if (lookupCountryTimeZones == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("region id is not valid: ");
            sb.append(string);
            Log.e("RegionZoneSearchPicker", sb.toString());
            this.getActivity().finish();
            return Collections.emptyList();
        }
        return this.getRegionTimeZoneInfo(lookupCountryTimeZones.getTimeZoneIds());
    }
    
    @Override
    protected CharSequence getHeaderText() {
        return this.mRegionName;
    }
    
    public int getMetricsCategory() {
        return 1356;
    }
    
    public List<TimeZoneInfo> getRegionTimeZoneInfo(final Collection<String> collection) {
        final TimeZoneInfo.Formatter formatter = new TimeZoneInfo.Formatter(this.getLocale(), new Date());
        final TreeSet<TimeZoneInfo> set = new TreeSet<TimeZoneInfo>(new TimeZoneInfoComparator(Collator.getInstance(this.getLocale()), new Date()));
        final Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext()) {
            final TimeZone frozenTimeZone = TimeZone.getFrozenTimeZone((String)iterator.next());
            if (frozenTimeZone.getID().equals("Etc/Unknown")) {
                continue;
            }
            set.add(formatter.format(frozenTimeZone));
        }
        return (List<TimeZoneInfo>)Collections.unmodifiableList((List<?>)new ArrayList<Object>(set));
    }
    
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        final LocaleDisplayNames instance = LocaleDisplayNames.getInstance(this.getLocale());
        arguments = this.getArguments();
        final String s = null;
        String string;
        if (arguments == null) {
            string = null;
        }
        else {
            string = this.getArguments().getString("com.android.settings.datetime.timezone.region_id");
        }
        String regionDisplayName;
        if (string == null) {
            regionDisplayName = s;
        }
        else {
            regionDisplayName = instance.regionDisplayName(string);
        }
        this.mRegionName = regionDisplayName;
    }
    
    @Override
    protected Intent prepareResultData(final TimeZoneInfo timeZoneInfo) {
        final Intent prepareResultData = super.prepareResultData(timeZoneInfo);
        prepareResultData.putExtra("com.android.settings.datetime.timezone.result_region_id", this.getArguments().getString("com.android.settings.datetime.timezone.region_id"));
        return prepareResultData;
    }
    
    static class TimeZoneInfoComparator implements Comparator<TimeZoneInfo>
    {
        private Collator mCollator;
        private final Date mNow;
        
        TimeZoneInfoComparator(final Collator mCollator, final Date mNow) {
            this.mCollator = mCollator;
            this.mNow = mNow;
        }
        
        @Override
        public int compare(final TimeZoneInfo timeZoneInfo, final TimeZoneInfo timeZoneInfo2) {
            int n;
            if ((n = Integer.compare(timeZoneInfo.getTimeZone().getOffset(this.mNow.getTime()), timeZoneInfo2.getTimeZone().getOffset(this.mNow.getTime()))) == 0) {
                n = Integer.compare(timeZoneInfo.getTimeZone().getRawOffset(), timeZoneInfo2.getTimeZone().getRawOffset());
            }
            int compare;
            if ((compare = n) == 0) {
                compare = this.mCollator.compare(timeZoneInfo.getExemplarLocation(), timeZoneInfo2.getExemplarLocation());
            }
            int compare2;
            if ((compare2 = compare) == 0) {
                compare2 = compare;
                if (timeZoneInfo.getGenericName() != null) {
                    compare2 = compare;
                    if (timeZoneInfo2.getGenericName() != null) {
                        compare2 = this.mCollator.compare(timeZoneInfo.getGenericName(), timeZoneInfo2.getGenericName());
                    }
                }
            }
            return compare2;
        }
    }
}
