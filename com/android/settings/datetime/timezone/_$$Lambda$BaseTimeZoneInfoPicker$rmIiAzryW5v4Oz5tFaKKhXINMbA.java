package com.android.settings.datetime.timezone;

import java.util.Iterator;
import java.util.ArrayList;
import android.icu.text.SimpleDateFormat;
import java.util.Locale;
import android.content.Context;
import android.icu.util.Calendar;
import java.util.Date;
import android.icu.text.DateFormat;
import android.content.res.Resources;
import android.content.Intent;
import java.util.List;
import com.android.settings.datetime.timezone.model.TimeZoneData;

public final class _$$Lambda$BaseTimeZoneInfoPicker$rmIiAzryW5v4Oz5tFaKKhXINMbA implements OnListItemClickListener
{
    @Override
    public final void onListItemClick(final BaseTimeZoneAdapter.AdapterItem adapterItem) {
        this.f$0.onListItemClick((BaseTimeZoneInfoPicker.TimeZoneInfoItem)adapterItem);
    }
}
