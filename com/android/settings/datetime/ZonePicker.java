package com.android.settings.datetime;

import android.widget.TextView;
import java.text.Collator;
import android.view.MenuItem;
import android.app.AlarmManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settings.overlay.FeatureFactory;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ListView;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.Map;
import android.widget.SimpleAdapter$ViewBinder;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import com.android.settingslib.datetime.ZoneGetter;
import android.content.Context;
import com.android.settingslib.core.instrumentation.VisibilityLoggerMixin;
import android.widget.SimpleAdapter;
import com.android.settingslib.core.instrumentation.Instrumentable;
import android.app.ListFragment;

public class ZonePicker extends ListFragment implements Instrumentable
{
    private SimpleAdapter mAlphabeticalAdapter;
    private boolean mSortedByTimezone;
    private SimpleAdapter mTimezoneSortedAdapter;
    private VisibilityLoggerMixin mVisibilityLoggerMixin;
    
    public static SimpleAdapter constructTimezoneAdapter(final Context context, final boolean b) {
        return constructTimezoneAdapter(context, b, 2131558534);
    }
    
    public static SimpleAdapter constructTimezoneAdapter(final Context context, final boolean b, final int n) {
        String s;
        if (b) {
            s = "display_label";
        }
        else {
            s = "offset";
        }
        final MyComparator myComparator = new MyComparator(s);
        final List<Map<String, Object>> zonesList = ZoneGetter.getZonesList(context);
        Collections.sort((List<Object>)zonesList, (Comparator<? super Object>)myComparator);
        final SimpleAdapter simpleAdapter = new SimpleAdapter(context, (List)zonesList, n, new String[] { "display_label", "offset_label" }, new int[] { 16908308, 16908309 });
        simpleAdapter.setViewBinder((SimpleAdapter$ViewBinder)new TimeZoneViewBinder());
        return simpleAdapter;
    }
    
    public static int getTimeZoneIndex(final SimpleAdapter simpleAdapter, final TimeZone timeZone) {
        final String id = timeZone.getID();
        for (int count = simpleAdapter.getCount(), i = 0; i < count; ++i) {
            if (id.equals(((HashMap)simpleAdapter.getItem(i)).get("id"))) {
                return i;
            }
        }
        return -1;
    }
    
    static void prepareCustomPreferencesList(final ListView listView) {
        listView.setScrollBarStyle(33554432);
        listView.setClipToPadding(false);
        listView.setDivider((Drawable)null);
    }
    
    private void setSorting(final boolean mSortedByTimezone) {
        SimpleAdapter listAdapter;
        if (mSortedByTimezone) {
            listAdapter = this.mTimezoneSortedAdapter;
        }
        else {
            listAdapter = this.mAlphabeticalAdapter;
        }
        this.setListAdapter((ListAdapter)listAdapter);
        this.mSortedByTimezone = mSortedByTimezone;
        final int timeZoneIndex = getTimeZoneIndex(listAdapter, TimeZone.getDefault());
        if (timeZoneIndex >= 0) {
            this.setSelection(timeZoneIndex);
        }
    }
    
    public int getMetricsCategory() {
        return 515;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final Activity activity = this.getActivity();
        this.mTimezoneSortedAdapter = constructTimezoneAdapter((Context)activity, false);
        this.mAlphabeticalAdapter = constructTimezoneAdapter((Context)activity, true);
        this.setSorting(true);
        this.setHasOptionsMenu(true);
        activity.setTitle(2131887327);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mVisibilityLoggerMixin = new VisibilityLoggerMixin(this.getMetricsCategory(), FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider());
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        menu.add(0, 1, 0, 2131890434).setIcon(17301660);
        menu.add(0, 2, 0, 2131890435).setIcon(2131231063);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        prepareCustomPreferencesList((ListView)onCreateView.findViewById(16908298));
        return onCreateView;
    }
    
    public void onListItemClick(final ListView listView, final View view, final int n, final long n2) {
        if (!this.isResumed()) {
            return;
        }
        ((AlarmManager)this.getActivity().getSystemService("alarm")).setTimeZone((String)((Map)listView.getItemAtPosition(n)).get("id"));
        this.getActivity().onBackPressed();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return false;
            }
            case 2: {
                this.setSorting(true);
                return true;
            }
            case 1: {
                this.setSorting(false);
                return true;
            }
        }
    }
    
    public void onPause() {
        super.onPause();
        this.mVisibilityLoggerMixin.onPause();
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        if (this.mSortedByTimezone) {
            menu.findItem(2).setVisible(false);
            menu.findItem(1).setVisible(true);
        }
        else {
            menu.findItem(2).setVisible(true);
            menu.findItem(1).setVisible(false);
        }
    }
    
    public void onResume() {
        super.onResume();
        this.mVisibilityLoggerMixin.onResume();
    }
    
    static class MyComparator implements Comparator<Map<?, ?>>
    {
        private final Collator mCollator;
        private boolean mSortedByName;
        private String mSortingKey;
        
        public MyComparator(final String mSortingKey) {
            this.mCollator = Collator.getInstance();
            this.mSortingKey = mSortingKey;
            this.mSortedByName = "display_label".equals(mSortingKey);
        }
        
        private boolean isComparable(final Object o) {
            return o != null && o instanceof Comparable;
        }
        
        @Override
        public int compare(final Map<?, ?> map, final Map<?, ?> map2) {
            final Object value = map.get(this.mSortingKey);
            final Object value2 = map2.get(this.mSortingKey);
            if (!this.isComparable(value)) {
                return this.isComparable(value2) ? 1 : 0;
            }
            if (!this.isComparable(value2)) {
                return -1;
            }
            if (this.mSortedByName) {
                return this.mCollator.compare(value, value2);
            }
            return ((Comparable<Object>)value).compareTo(value2);
        }
    }
    
    private static class TimeZoneViewBinder implements SimpleAdapter$ViewBinder
    {
        public boolean setViewValue(final View view, final Object o, final String s) {
            ((TextView)view).setText((CharSequence)o);
            return true;
        }
    }
}
