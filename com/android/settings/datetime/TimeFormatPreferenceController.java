package com.android.settings.datetime;

import android.support.v14.preference.SwitchPreference;
import android.text.TextUtils;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.Preference;
import android.content.Intent;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.content.Context;
import java.util.Calendar;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class TimeFormatPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final Calendar mDummyDate;
    private final boolean mIsFromSUW;
    private final UpdateTimeAndDateCallback mUpdateTimeAndDateCallback;
    
    public TimeFormatPreferenceController(final Context context, final UpdateTimeAndDateCallback mUpdateTimeAndDateCallback, final boolean mIsFromSUW) {
        super(context);
        this.mIsFromSUW = mIsFromSUW;
        this.mDummyDate = Calendar.getInstance();
        this.mUpdateTimeAndDateCallback = mUpdateTimeAndDateCallback;
    }
    
    private boolean is24Hour() {
        return DateFormat.is24HourFormat(this.mContext);
    }
    
    static void set24Hour(final Context context, final Boolean b) {
        String s;
        if (b == null) {
            s = null;
        }
        else if (b) {
            s = "24";
        }
        else {
            s = "12";
        }
        Settings.System.putString(context.getContentResolver(), "time_12_24", s);
    }
    
    static void timeUpdated(final Context context, final Boolean b) {
        final Intent intent = new Intent("android.intent.action.TIME_SET");
        intent.addFlags(16777216);
        int n;
        if (b == null) {
            n = 2;
        }
        else if (b) {
            n = 1;
        }
        else {
            n = 0;
        }
        intent.putExtra("android.intent.extra.TIME_PREF_24_HOUR_FORMAT", n);
        context.sendBroadcast(intent);
    }
    
    static void update24HourFormat(final Context context, final Boolean b) {
        set24Hour(context, b);
        timeUpdated(context, b);
    }
    
    @Override
    public String getPreferenceKey() {
        return "24 hour";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (preference instanceof TwoStatePreference && TextUtils.equals((CharSequence)"24 hour", (CharSequence)preference.getKey())) {
            update24HourFormat(this.mContext, ((SwitchPreference)preference).isChecked());
            this.mUpdateTimeAndDateCallback.updateTimeAndDateDisplay(this.mContext);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mIsFromSUW ^ true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (!(preference instanceof TwoStatePreference)) {
            return;
        }
        preference.setEnabled(AutoTimeFormatPreferenceController.isAutoTimeFormatSelection(this.mContext) ^ true);
        ((TwoStatePreference)preference).setChecked(this.is24Hour());
        final Calendar instance = Calendar.getInstance();
        this.mDummyDate.setTimeZone(instance.getTimeZone());
        this.mDummyDate.set(instance.get(1), 11, 31, 13, 0, 0);
        preference.setSummary(DateFormat.getTimeFormat(this.mContext).format(this.mDummyDate.getTime()));
    }
}
