package com.android.settings;

import android.text.TextUtils;
import android.content.res.Resources$NotFoundException;
import android.text.TextUtils$TruncateAt;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import java.util.ArrayList;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.RemoteCallback;
import android.os.RemoteCallback$OnResultListener;
import com.android.settings.users.UserDialogs;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Process;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.view.Display;
import android.view.WindowManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.app.ActivityManager;
import android.util.Log;
import com.android.settings.fuelgauge.BatteryUtils;
import android.util.EventLog;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import java.util.List;
import java.util.Optional;
import java.util.Iterator;
import android.widget.AppSecurityPermissions;
import android.app.admin.DeviceAdminInfo$PolicyInfo;
import android.content.Context;
import android.os.UserManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Handler;
import android.app.admin.DeviceAdminInfo;
import android.app.admin.DevicePolicyManager;
import android.app.AppOpsManager;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.app.Activity;
import android.content.ComponentName;
import java.util.function.Predicate;

public final class _$$Lambda$DeviceAdminAdd$3kbf0VppdPbIFmWVVpDZ5dj27E4 implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return DeviceAdminAdd.lambda$findAdminWithPackageName$0(this.f$0, (ComponentName)o);
    }
}
