package com.android.settings;

import android.content.Context;
import java.io.Serializable;
import android.content.DialogInterface;
import android.content.DialogInterface$OnCancelListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.util.Log;
import android.content.DialogInterface$OnDismissListener;
import com.android.settings.widget.LoadingViewController;
import java.util.Iterator;
import android.support.v14.preference.PreferenceDialogFragment;
import android.app.Fragment;
import com.android.settingslib.CustomEditTextPreference;
import com.android.settingslib.CustomDialogPreference;
import java.util.UUID;
import android.view.LayoutInflater;
import android.app.Dialog;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.support.actionbar.HelpMenuController;
import com.android.settingslib.core.lifecycle.ObservablePreferenceFragment;
import com.android.settings.search.actionbar.SearchMenuController;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.widget.Button;
import android.content.Intent;
import android.app.Activity;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceGroup;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.Preference;
import android.util.ArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import com.android.settings.applications.LayoutPreference;
import com.android.settingslib.widget.FooterPreferenceMixin;
import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.content.ContentResolver;
import android.view.ViewGroup;
import com.android.settings.widget.HighlightablePreferenceGroupAdapter;
import com.android.settings.support.actionbar.HelpResourceProvider;
import com.android.settings.core.InstrumentedPreferenceFragment;

public abstract class SettingsPreferenceFragment extends InstrumentedPreferenceFragment implements DialogCreatable, HelpResourceProvider
{
    public HighlightablePreferenceGroupAdapter mAdapter;
    private boolean mAnimationAllowed;
    private ViewGroup mButtonBar;
    private ContentResolver mContentResolver;
    private RecyclerView.Adapter mCurrentRootAdapter;
    private RecyclerView.AdapterDataObserver mDataSetObserver;
    private SettingsDialogFragment mDialogFragment;
    private View mEmptyView;
    protected final FooterPreferenceMixin mFooterPreferenceMixin;
    private LayoutPreference mHeader;
    private boolean mIsDataSetObserverRegistered;
    private LinearLayoutManager mLayoutManager;
    private ViewGroup mPinnedHeaderFrameLayout;
    private ArrayMap<String, Preference> mPreferenceCache;
    public boolean mPreferenceHighlighted;
    
    public SettingsPreferenceFragment() {
        this.mFooterPreferenceMixin = new FooterPreferenceMixin(this, this.getLifecycle());
        this.mIsDataSetObserverRegistered = false;
        this.mDataSetObserver = new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                SettingsPreferenceFragment.this.onDataSetChanged();
            }
            
            @Override
            public void onItemRangeChanged(final int n, final int n2) {
                SettingsPreferenceFragment.this.onDataSetChanged();
            }
            
            @Override
            public void onItemRangeChanged(final int n, final int n2, final Object o) {
                SettingsPreferenceFragment.this.onDataSetChanged();
            }
            
            @Override
            public void onItemRangeInserted(final int n, final int n2) {
                SettingsPreferenceFragment.this.onDataSetChanged();
            }
            
            @Override
            public void onItemRangeMoved(final int n, final int n2, final int n3) {
                SettingsPreferenceFragment.this.onDataSetChanged();
            }
            
            @Override
            public void onItemRangeRemoved(final int n, final int n2) {
                SettingsPreferenceFragment.this.onDataSetChanged();
            }
        };
        this.mPreferenceHighlighted = false;
    }
    
    private void addPreferenceToTop(final LayoutPreference layoutPreference) {
        layoutPreference.setOrder(-1);
        if (this.getPreferenceScreen() != null) {
            this.getPreferenceScreen().addPreference(layoutPreference);
        }
    }
    
    private void checkAvailablePrefs(final PreferenceGroup preferenceGroup) {
        if (preferenceGroup == null) {
            return;
        }
        for (int i = 0; i < preferenceGroup.getPreferenceCount(); ++i) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (preference instanceof SelfAvailablePreference && !((SelfAvailablePreference)preference).isAvailable(this.getContext())) {
                preferenceGroup.removePreference(preference);
            }
            else if (preference instanceof PreferenceGroup) {
                this.checkAvailablePrefs((PreferenceGroup)preference);
            }
        }
    }
    
    @Override
    public void addPreferencesFromResource(final int n) {
        super.addPreferencesFromResource(n);
        this.checkAvailablePrefs(this.getPreferenceScreen());
    }
    
    protected void cacheRemoveAllPrefs(final PreferenceGroup preferenceGroup) {
        this.mPreferenceCache = (ArrayMap<String, Preference>)new ArrayMap();
        for (int preferenceCount = preferenceGroup.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (!TextUtils.isEmpty((CharSequence)preference.getKey())) {
                this.mPreferenceCache.put((Object)preference.getKey(), (Object)preference);
            }
        }
    }
    
    public void finish() {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        if (this.getFragmentManager().getBackStackEntryCount() > 0) {
            this.getFragmentManager().popBackStack();
        }
        else {
            activity.finish();
        }
    }
    
    public final void finishFragment() {
        this.getActivity().onBackPressed();
    }
    
    public ViewGroup getButtonBar() {
        return this.mButtonBar;
    }
    
    protected Preference getCachedPreference(final String s) {
        Preference preference;
        if (this.mPreferenceCache != null) {
            preference = (Preference)this.mPreferenceCache.remove((Object)s);
        }
        else {
            preference = null;
        }
        return preference;
    }
    
    protected ContentResolver getContentResolver() {
        final Activity activity = this.getActivity();
        if (activity != null) {
            this.mContentResolver = ((Context)activity).getContentResolver();
        }
        return this.mContentResolver;
    }
    
    @Override
    public int getDialogMetricsCategory(final int n) {
        return 0;
    }
    
    public LayoutPreference getHeaderView() {
        return this.mHeader;
    }
    
    public int getInitialExpandedChildCount() {
        return 0;
    }
    
    protected Intent getIntent() {
        if (this.getActivity() == null) {
            return null;
        }
        return this.getActivity().getIntent();
    }
    
    protected Button getNextButton() {
        return ((ButtonBarHandler)this.getActivity()).getNextButton();
    }
    
    protected PackageManager getPackageManager() {
        return this.getActivity().getPackageManager();
    }
    
    protected Object getSystemService(final String s) {
        return this.getActivity().getSystemService(s);
    }
    
    protected boolean hasNextButton() {
        return ((ButtonBarHandler)this.getActivity()).hasNextButton();
    }
    
    public void highlightPreferenceIfNeeded() {
        if (!this.isAdded()) {
            return;
        }
        if (this.mAdapter != null) {
            this.mAdapter.requestHighlight(this.getView(), this.getListView());
        }
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.setHasOptionsMenu(true);
    }
    
    @Override
    protected void onBindPreferences() {
        this.registerObserverIfNeeded();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        SearchMenuController.init(this);
        HelpMenuController.init(this);
        if (bundle != null) {
            this.mPreferenceHighlighted = bundle.getBoolean("android:preference_highlighted");
        }
        HighlightablePreferenceGroupAdapter.adjustInitialExpandedChildCount(this);
    }
    
    @Override
    protected RecyclerView.Adapter onCreateAdapter(final PreferenceScreen preferenceScreen) {
        final Bundle arguments = this.getArguments();
        String string;
        if (arguments == null) {
            string = null;
        }
        else {
            string = arguments.getString(":settings:fragment_args_key");
        }
        return this.mAdapter = new HighlightablePreferenceGroupAdapter(preferenceScreen, string, this.mPreferenceHighlighted);
    }
    
    @Override
    public Dialog onCreateDialog(final int n) {
        return null;
    }
    
    @Override
    public RecyclerView.LayoutManager onCreateLayoutManager() {
        return this.mLayoutManager = new LinearLayoutManager(this.getContext());
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.mPinnedHeaderFrameLayout = (ViewGroup)onCreateView.findViewById(2131362453);
        this.mButtonBar = (ViewGroup)onCreateView.findViewById(2131361950);
        return onCreateView;
    }
    
    protected void onDataSetChanged() {
        this.highlightPreferenceIfNeeded();
        this.updateEmptyView();
    }
    
    public void onDetach() {
        if (this.isRemoving() && this.mDialogFragment != null) {
            this.mDialogFragment.dismiss();
            this.mDialogFragment = null;
        }
        super.onDetach();
    }
    
    public void onDialogShowing() {
    }
    
    @Override
    public void onDisplayPreferenceDialog(final Preference preference) {
        if (preference.getKey() == null) {
            preference.setKey(UUID.randomUUID().toString());
        }
        PreferenceDialogFragment preferenceDialogFragment;
        if (preference instanceof RestrictedListPreference) {
            preferenceDialogFragment = RestrictedListPreference.RestrictedListPreferenceDialogFragment.newInstance(preference.getKey());
        }
        else if (preference instanceof CustomListPreference) {
            preferenceDialogFragment = CustomListPreference.CustomListPreferenceDialogFragment.newInstance(preference.getKey());
        }
        else if (preference instanceof CustomDialogPreference) {
            preferenceDialogFragment = CustomDialogPreference.CustomPreferenceDialogFragment.newInstance(preference.getKey());
        }
        else {
            if (!(preference instanceof CustomEditTextPreference)) {
                super.onDisplayPreferenceDialog(preference);
                return;
            }
            preferenceDialogFragment = CustomEditTextPreference.CustomPreferenceDialogFragment.newInstance(preference.getKey());
        }
        preferenceDialogFragment.setTargetFragment((Fragment)this, 0);
        preferenceDialogFragment.show(this.getFragmentManager(), "dialog_preference");
        this.onDialogShowing();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.highlightPreferenceIfNeeded();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mAdapter != null) {
            bundle.putBoolean("android:preference_highlighted", this.mAdapter.isHighlightRequested());
        }
    }
    
    @Override
    protected void onUnbindPreferences() {
        this.unregisterObserverIfNeeded();
    }
    
    public void registerObserverIfNeeded() {
        if (!this.mIsDataSetObserverRegistered) {
            if (this.mCurrentRootAdapter != null) {
                this.mCurrentRootAdapter.unregisterAdapterDataObserver(this.mDataSetObserver);
            }
            (this.mCurrentRootAdapter = this.getListView().getAdapter()).registerAdapterDataObserver(this.mDataSetObserver);
            this.mIsDataSetObserverRegistered = true;
            this.onDataSetChanged();
        }
    }
    
    protected void removeCachedPrefs(final PreferenceGroup preferenceGroup) {
        final Iterator<Preference> iterator = this.mPreferenceCache.values().iterator();
        while (iterator.hasNext()) {
            preferenceGroup.removePreference(iterator.next());
        }
        this.mPreferenceCache = null;
    }
    
    protected void removeDialog(final int n) {
        if (this.mDialogFragment != null && this.mDialogFragment.getDialogId() == n) {
            this.mDialogFragment.dismissAllowingStateLoss();
        }
        this.mDialogFragment = null;
    }
    
    boolean removePreference(final PreferenceGroup preferenceGroup, final String s) {
        for (int preferenceCount = preferenceGroup.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = preferenceGroup.getPreference(i);
            if (TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)s)) {
                return preferenceGroup.removePreference(preference);
            }
            if (preference instanceof PreferenceGroup && this.removePreference((PreferenceGroup)preference, s)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean removePreference(final String s) {
        return this.removePreference(this.getPreferenceScreen(), s);
    }
    
    protected void setAnimationAllowed(final boolean mAnimationAllowed) {
        this.mAnimationAllowed = mAnimationAllowed;
    }
    
    public void setEmptyView(final View mEmptyView) {
        if (this.mEmptyView != null) {
            this.mEmptyView.setVisibility(8);
        }
        this.mEmptyView = mEmptyView;
        this.updateEmptyView();
    }
    
    protected void setHeaderView(final int n) {
        this.addPreferenceToTop(this.mHeader = new LayoutPreference(this.getPrefContext(), n));
    }
    
    public void setLoading(final boolean b, final boolean b2) {
        LoadingViewController.handleLoadingContainer(this.getView().findViewById(2131362342), (View)this.getListView(), b ^ true, b2);
    }
    
    protected void setOnDismissListener(final DialogInterface$OnDismissListener dialogInterface$OnDismissListener) {
        if (this.mDialogFragment != null) {
            this.mDialogFragment.mOnDismissListener = dialogInterface$OnDismissListener;
        }
    }
    
    public View setPinnedHeaderView(final int n) {
        final View inflate = this.getActivity().getLayoutInflater().inflate(n, this.mPinnedHeaderFrameLayout, false);
        this.setPinnedHeaderView(inflate);
        return inflate;
    }
    
    public void setPinnedHeaderView(final View view) {
        this.mPinnedHeaderFrameLayout.addView(view);
        this.mPinnedHeaderFrameLayout.setVisibility(0);
    }
    
    @Override
    public void setPreferenceScreen(final PreferenceScreen preferenceScreen) {
        if (preferenceScreen != null && !preferenceScreen.isAttached()) {
            preferenceScreen.setShouldUseGeneratedIds(this.mAnimationAllowed);
        }
        super.setPreferenceScreen(preferenceScreen);
        if (preferenceScreen != null && this.mHeader != null) {
            preferenceScreen.addPreference(this.mHeader);
        }
    }
    
    protected void setResult(final int result) {
        if (this.getActivity() == null) {
            return;
        }
        this.getActivity().setResult(result);
    }
    
    protected void setResult(final int n, final Intent intent) {
        if (this.getActivity() == null) {
            return;
        }
        this.getActivity().setResult(n, intent);
    }
    
    protected void showDialog(final int n) {
        if (this.mDialogFragment != null) {
            Log.e("SettingsPreference", "Old dialog fragment not null!");
        }
        (this.mDialogFragment = new SettingsDialogFragment(this, n)).show(this.getChildFragmentManager(), Integer.toString(n));
    }
    
    public void unregisterObserverIfNeeded() {
        if (this.mIsDataSetObserverRegistered) {
            if (this.mCurrentRootAdapter != null) {
                this.mCurrentRootAdapter.unregisterAdapterDataObserver(this.mDataSetObserver);
                this.mCurrentRootAdapter = null;
            }
            this.mIsDataSetObserverRegistered = false;
        }
    }
    
    void updateEmptyView() {
        if (this.mEmptyView == null) {
            return;
        }
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        final boolean b = false;
        if (preferenceScreen != null) {
            final View viewById = this.getActivity().findViewById(16908351);
            final int preferenceCount = this.getPreferenceScreen().getPreferenceCount();
            final LayoutPreference mHeader = this.mHeader;
            final boolean b2 = true;
            int n;
            if (mHeader != null) {
                n = 1;
            }
            else {
                n = 0;
            }
            boolean b3 = b2;
            if (preferenceCount - n - (this.mFooterPreferenceMixin.hasFooter() ? 1 : 0) > 0) {
                b3 = (viewById != null && viewById.getVisibility() != 0 && b2);
            }
            final View mEmptyView = this.mEmptyView;
            int visibility;
            if (b3) {
                visibility = (b ? 1 : 0);
            }
            else {
                visibility = 8;
            }
            mEmptyView.setVisibility(visibility);
        }
        else {
            this.mEmptyView.setVisibility(0);
        }
    }
    
    public static class SettingsDialogFragment extends InstrumentedDialogFragment
    {
        private DialogInterface$OnCancelListener mOnCancelListener;
        private DialogInterface$OnDismissListener mOnDismissListener;
        private Fragment mParentFragment;
        
        public SettingsDialogFragment() {
        }
        
        public SettingsDialogFragment(final DialogCreatable dialogCreatable, final int n) {
            super(dialogCreatable, n);
            if (dialogCreatable instanceof Fragment) {
                this.mParentFragment = (Fragment)dialogCreatable;
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("fragment argument must be an instance of ");
            sb.append(Fragment.class.getName());
            throw new IllegalArgumentException(sb.toString());
        }
        
        public int getDialogId() {
            return this.mDialogId;
        }
        
        @Override
        public int getMetricsCategory() {
            if (this.mDialogCreatable == null) {
                return 0;
            }
            final int dialogMetricsCategory = this.mDialogCreatable.getDialogMetricsCategory(this.mDialogId);
            if (dialogMetricsCategory > 0) {
                return dialogMetricsCategory;
            }
            throw new IllegalStateException("Dialog must provide a metrics category");
        }
        
        public void onCancel(final DialogInterface dialogInterface) {
            super.onCancel(dialogInterface);
            if (this.mOnCancelListener != null) {
                this.mOnCancelListener.onCancel(dialogInterface);
            }
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            if (bundle != null) {
                this.mDialogId = bundle.getInt("key_dialog_id", 0);
                this.mParentFragment = this.getParentFragment();
                final int int1 = bundle.getInt("key_parent_fragment_id", -1);
                if (this.mParentFragment == null) {
                    this.mParentFragment = this.getFragmentManager().findFragmentById(int1);
                }
                if (!(this.mParentFragment instanceof DialogCreatable)) {
                    final StringBuilder sb = new StringBuilder();
                    Serializable s;
                    if (this.mParentFragment != null) {
                        s = this.mParentFragment.getClass().getName();
                    }
                    else {
                        s = int1;
                    }
                    sb.append(s);
                    sb.append(" must implement ");
                    sb.append(DialogCreatable.class.getName());
                    throw new IllegalArgumentException(sb.toString());
                }
                if (this.mParentFragment instanceof SettingsPreferenceFragment) {
                    ((SettingsPreferenceFragment)this.mParentFragment).mDialogFragment = this;
                }
            }
            return ((DialogCreatable)this.mParentFragment).onCreateDialog(this.mDialogId);
        }
        
        public void onDetach() {
            super.onDetach();
            if (this.mParentFragment instanceof SettingsPreferenceFragment && ((SettingsPreferenceFragment)this.mParentFragment).mDialogFragment == this) {
                ((SettingsPreferenceFragment)this.mParentFragment).mDialogFragment = null;
            }
        }
        
        public void onDismiss(final DialogInterface dialogInterface) {
            super.onDismiss(dialogInterface);
            if (this.mOnDismissListener != null) {
                this.mOnDismissListener.onDismiss(dialogInterface);
            }
        }
        
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            if (this.mParentFragment != null) {
                bundle.putInt("key_dialog_id", this.mDialogId);
                bundle.putInt("key_parent_fragment_id", this.mParentFragment.getId());
            }
        }
        
        @Override
        public void onStart() {
            super.onStart();
            if (this.mParentFragment != null && this.mParentFragment instanceof SettingsPreferenceFragment) {
                ((SettingsPreferenceFragment)this.mParentFragment).onDialogShowing();
            }
        }
    }
}
