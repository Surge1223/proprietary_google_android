package com.android.settings.wrapper;

import android.os.RemoteException;
import android.content.om.OverlayInfo;
import java.util.ArrayList;
import java.util.List;
import android.content.om.IOverlayManager$Stub;
import android.os.ServiceManager;
import android.content.om.IOverlayManager;

public class OverlayManagerWrapper
{
    private final IOverlayManager mOverlayManager;
    
    public OverlayManagerWrapper() {
        this(IOverlayManager$Stub.asInterface(ServiceManager.getService("overlay")));
    }
    
    public OverlayManagerWrapper(final IOverlayManager mOverlayManager) {
        this.mOverlayManager = mOverlayManager;
    }
    
    public List<OverlayInfo> getOverlayInfosForTarget(final String s, int i) {
        if (this.mOverlayManager == null) {
            return new ArrayList<OverlayInfo>();
        }
        try {
            final List overlayInfosForTarget = this.mOverlayManager.getOverlayInfosForTarget(s, i);
            final ArrayList list = new ArrayList<OverlayInfo>(overlayInfosForTarget.size());
            for (i = 0; i < overlayInfosForTarget.size(); ++i) {
                list.add(new OverlayInfo(overlayInfosForTarget.get(i)));
            }
            return (List<OverlayInfo>)list;
        }
        catch (RemoteException ex) {
            throw ex.rethrowFromSystemServer();
        }
    }
    
    public boolean setEnabled(final String s, final boolean b, final int n) {
        if (this.mOverlayManager == null) {
            return false;
        }
        try {
            return this.mOverlayManager.setEnabled(s, b, n);
        }
        catch (RemoteException ex) {
            throw ex.rethrowFromSystemServer();
        }
    }
    
    public boolean setEnabledExclusiveInCategory(final String s, final int n) {
        if (this.mOverlayManager == null) {
            return false;
        }
        try {
            return this.mOverlayManager.setEnabledExclusiveInCategory(s, n);
        }
        catch (RemoteException ex) {
            throw ex.rethrowFromSystemServer();
        }
    }
    
    public static class OverlayInfo
    {
        public final String category;
        private final boolean mEnabled;
        public final String packageName;
        public final int priority;
        
        public OverlayInfo(final android.content.om.OverlayInfo overlayInfo) {
            this.mEnabled = overlayInfo.isEnabled();
            this.category = overlayInfo.category;
            this.packageName = overlayInfo.packageName;
            this.priority = overlayInfo.priority;
        }
        
        public boolean isEnabled() {
            return this.mEnabled;
        }
    }
}
