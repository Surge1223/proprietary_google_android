package com.android.settings;

import android.app.ActivityManager$TaskDescription;
import android.content.IntentFilter;
import com.android.settingslib.core.instrumentation.Instrumentable;
import com.android.settings.core.SubSettingLauncher;
import android.support.v7.preference.Preference;
import android.support.v4.content.LocalBroadcastManager;
import android.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.Activity;
import android.widget.Toolbar;
import java.util.Collection;
import com.android.settings.dashboard.DashboardSummary;
import com.android.settingslib.core.instrumentation.SharedPreferencesLogger;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap$Config;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settings.overlay.FeatureFactory;
import android.app.FragmentTransaction;
import android.transition.TransitionManager;
import android.app.Fragment;
import android.os.Bundle;
import android.os.UserHandle;
import android.app.FragmentManager$BackStackEntry;
import com.android.internal.util.ArrayUtils;
import com.android.settings.core.gateway.SettingsGateway;
import android.content.ComponentName;
import com.android.settings.search.DeviceIndexFeatureProvider;
import com.android.settings.applications.manageapplications.ManageApplications;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import com.android.settings.widget.SwitchBar;
import android.widget.Button;
import com.android.settings.dashboard.DashboardFeatureProvider;
import android.view.ViewGroup;
import com.android.settingslib.drawer.DashboardCategory;
import java.util.ArrayList;
import android.content.BroadcastReceiver;
import android.support.v7.preference.PreferenceManager;
import android.support.v14.preference.PreferenceFragment;
import android.app.FragmentManager$OnBackStackChangedListener;
import com.android.settingslib.drawer.SettingsDrawerActivity;

public class SettingsActivity extends SettingsDrawerActivity implements FragmentManager$OnBackStackChangedListener, OnPreferenceStartFragmentCallback, OnPreferenceTreeClickListener, ButtonBarHandler
{
    private BroadcastReceiver mBatteryInfoReceiver;
    private boolean mBatteryPresent;
    private ArrayList<DashboardCategory> mCategories;
    private ViewGroup mContent;
    private DashboardFeatureProvider mDashboardFeatureProvider;
    private BroadcastReceiver mDevelopmentSettingsListener;
    private String mFragmentClass;
    private CharSequence mInitialTitle;
    private int mInitialTitleResId;
    private boolean mIsShowingDashboard;
    private Button mNextButton;
    private SwitchBar mSwitchBar;
    
    public SettingsActivity() {
        this.mBatteryPresent = true;
        this.mBatteryInfoReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                    final boolean batteryPresent = Utils.isBatteryPresent(intent);
                    if (SettingsActivity.this.mBatteryPresent != batteryPresent) {
                        SettingsActivity.this.mBatteryPresent = batteryPresent;
                        SettingsActivity.this.updateTilesList();
                    }
                }
            }
        };
        this.mCategories = new ArrayList<DashboardCategory>();
    }
    
    private void doUpdateTilesList() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   com/android/settings/SettingsActivity.getPackageManager:()Landroid/content/pm/PackageManager;
        //     4: astore_1       
        //     5: aload_0        
        //     6: invokestatic    android/os/UserManager.get:(Landroid/content/Context;)Landroid/os/UserManager;
        //     9: astore_2       
        //    10: aload_2        
        //    11: invokevirtual   android/os/UserManager.isAdminUser:()Z
        //    14: istore_3       
        //    15: aload_0        
        //    16: invokestatic    com/android/settings/overlay/FeatureFactory.getFactory:(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;
        //    19: astore          4
        //    21: aload_0        
        //    22: invokevirtual   com/android/settings/SettingsActivity.getPackageName:()Ljava/lang/String;
        //    25: astore          5
        //    27: new             Ljava/lang/StringBuilder;
        //    30: dup            
        //    31: invokespecial   java/lang/StringBuilder.<init>:()V
        //    34: astore          6
        //    36: aload_0        
        //    37: aload           6
        //    39: new             Landroid/content/ComponentName;
        //    42: dup            
        //    43: aload           5
        //    45: ldc             Lcom/android/settings/Settings$WifiSettingsActivity;.class
        //    47: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //    50: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    53: aload_1        
        //    54: ldc             "android.hardware.wifi"
        //    56: invokevirtual   android/content/pm/PackageManager.hasSystemFeature:(Ljava/lang/String;)Z
        //    59: iload_3        
        //    60: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //    63: ifne            79
        //    66: iconst_0       
        //    67: ifeq            73
        //    70: goto            79
        //    73: iconst_0       
        //    74: istore          7
        //    76: goto            82
        //    79: iconst_1       
        //    80: istore          7
        //    82: aload_0        
        //    83: aload           6
        //    85: new             Landroid/content/ComponentName;
        //    88: dup            
        //    89: aload           5
        //    91: ldc             Lcom/android/settings/Settings$BluetoothSettingsActivity;.class
        //    93: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //    96: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    99: aload_1        
        //   100: ldc             "android.hardware.bluetooth"
        //   102: invokevirtual   android/content/pm/PackageManager.hasSystemFeature:(Ljava/lang/String;)Z
        //   105: iload_3        
        //   106: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   109: ifne            126
        //   112: iload           7
        //   114: ifeq            120
        //   117: goto            126
        //   120: iconst_0       
        //   121: istore          7
        //   123: goto            129
        //   126: iconst_1       
        //   127: istore          7
        //   129: aload_0        
        //   130: aload           6
        //   132: new             Landroid/content/ComponentName;
        //   135: dup            
        //   136: aload           5
        //   138: ldc             Lcom/android/settings/Settings$DataUsageSummaryActivity;.class
        //   140: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   143: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   146: invokestatic    com/android/settings/Utils.isBandwidthControlEnabled:()Z
        //   149: iload_3        
        //   150: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   153: ifne            170
        //   156: iload           7
        //   158: ifeq            164
        //   161: goto            170
        //   164: iconst_0       
        //   165: istore          7
        //   167: goto            173
        //   170: iconst_1       
        //   171: istore          7
        //   173: aload_0        
        //   174: aload           6
        //   176: new             Landroid/content/ComponentName;
        //   179: dup            
        //   180: aload           5
        //   182: ldc             Lcom/android/settings/Settings$ConnectedDeviceDashboardActivity;.class
        //   184: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   187: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   190: aload_0        
        //   191: invokestatic    android/os/UserManager.isDeviceInDemoMode:(Landroid/content/Context;)Z
        //   194: iconst_1       
        //   195: ixor           
        //   196: iload_3        
        //   197: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   200: ifne            217
        //   203: iload           7
        //   205: ifeq            211
        //   208: goto            217
        //   211: iconst_0       
        //   212: istore          7
        //   214: goto            220
        //   217: iconst_1       
        //   218: istore          7
        //   220: aload_0        
        //   221: aload           6
        //   223: new             Landroid/content/ComponentName;
        //   226: dup            
        //   227: aload           5
        //   229: ldc             Lcom/android/settings/Settings$SimSettingsActivity;.class
        //   231: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   234: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   237: aload_0        
        //   238: invokestatic    com/android/settings/Utils.showSimCardTile:(Landroid/content/Context;)Z
        //   241: iload_3        
        //   242: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   245: ifne            262
        //   248: iload           7
        //   250: ifeq            256
        //   253: goto            262
        //   256: iconst_0       
        //   257: istore          7
        //   259: goto            265
        //   262: iconst_1       
        //   263: istore          7
        //   265: aload_0        
        //   266: aload           6
        //   268: new             Landroid/content/ComponentName;
        //   271: dup            
        //   272: aload           5
        //   274: ldc             Lcom/android/settings/Settings$PowerUsageSummaryActivity;.class
        //   276: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   279: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   282: aload_0        
        //   283: getfield        com/android/settings/SettingsActivity.mBatteryPresent:Z
        //   286: iload_3        
        //   287: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   290: ifne            307
        //   293: iload           7
        //   295: ifeq            301
        //   298: goto            307
        //   301: iconst_0       
        //   302: istore          7
        //   304: goto            310
        //   307: iconst_1       
        //   308: istore          7
        //   310: aload_0        
        //   311: ldc             "settings_data_usage_v2"
        //   313: invokestatic    android/util/FeatureFlagUtils.isEnabled:(Landroid/content/Context;Ljava/lang/String;)Z
        //   316: istore          8
        //   318: new             Landroid/content/ComponentName;
        //   321: dup            
        //   322: aload           5
        //   324: ldc             Lcom/android/settings/Settings$DataUsageSummaryActivity;.class
        //   326: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   329: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   332: astore          9
        //   334: invokestatic    com/android/settings/Utils.isBandwidthControlEnabled:()Z
        //   337: ifeq            351
        //   340: iload           8
        //   342: ifeq            351
        //   345: iconst_1       
        //   346: istore          10
        //   348: goto            354
        //   351: iconst_0       
        //   352: istore          10
        //   354: aload_0        
        //   355: aload           6
        //   357: aload           9
        //   359: iload           10
        //   361: iload_3        
        //   362: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   365: ifne            382
        //   368: iload           7
        //   370: ifeq            376
        //   373: goto            382
        //   376: iconst_0       
        //   377: istore          7
        //   379: goto            385
        //   382: iconst_1       
        //   383: istore          7
        //   385: new             Landroid/content/ComponentName;
        //   388: dup            
        //   389: aload           5
        //   391: ldc             Lcom/android/settings/Settings$DataUsageSummaryLegacyActivity;.class
        //   393: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   396: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   399: astore          9
        //   401: invokestatic    com/android/settings/Utils.isBandwidthControlEnabled:()Z
        //   404: ifeq            418
        //   407: iload           8
        //   409: ifne            418
        //   412: iconst_1       
        //   413: istore          10
        //   415: goto            421
        //   418: iconst_0       
        //   419: istore          10
        //   421: aload_0        
        //   422: aload           6
        //   424: aload           9
        //   426: iload           10
        //   428: iload_3        
        //   429: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   432: ifne            449
        //   435: iload           7
        //   437: ifeq            443
        //   440: goto            449
        //   443: iconst_0       
        //   444: istore          7
        //   446: goto            452
        //   449: iconst_1       
        //   450: istore          7
        //   452: new             Landroid/content/ComponentName;
        //   455: dup            
        //   456: aload           5
        //   458: ldc             Lcom/android/settings/Settings$UserSettingsActivity;.class
        //   460: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   463: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   466: astore          9
        //   468: invokestatic    android/os/UserManager.supportsMultipleUsers:()Z
        //   471: ifeq            486
        //   474: invokestatic    com/android/settings/Utils.isMonkeyRunning:()Z
        //   477: ifne            486
        //   480: iconst_1       
        //   481: istore          10
        //   483: goto            489
        //   486: iconst_0       
        //   487: istore          10
        //   489: aload_0        
        //   490: aload           6
        //   492: aload           9
        //   494: iload           10
        //   496: iload_3        
        //   497: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   500: ifne            517
        //   503: iload           7
        //   505: ifeq            511
        //   508: goto            517
        //   511: iconst_0       
        //   512: istore          7
        //   514: goto            520
        //   517: iconst_1       
        //   518: istore          7
        //   520: aload_0        
        //   521: aload           6
        //   523: new             Landroid/content/ComponentName;
        //   526: dup            
        //   527: aload           5
        //   529: ldc             Lcom/android/settings/Settings$NetworkDashboardActivity;.class
        //   531: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   534: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   537: aload_0        
        //   538: invokestatic    android/os/UserManager.isDeviceInDemoMode:(Landroid/content/Context;)Z
        //   541: iconst_1       
        //   542: ixor           
        //   543: iload_3        
        //   544: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   547: ifne            564
        //   550: iload           7
        //   552: ifeq            558
        //   555: goto            564
        //   558: iconst_0       
        //   559: istore          7
        //   561: goto            567
        //   564: iconst_1       
        //   565: istore          7
        //   567: aload_0        
        //   568: aload           6
        //   570: new             Landroid/content/ComponentName;
        //   573: dup            
        //   574: aload           5
        //   576: ldc             Lcom/android/settings/Settings$DateTimeSettingsActivity;.class
        //   578: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   581: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   584: aload_0        
        //   585: invokestatic    android/os/UserManager.isDeviceInDemoMode:(Landroid/content/Context;)Z
        //   588: iconst_1       
        //   589: ixor           
        //   590: iload_3        
        //   591: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   594: ifne            611
        //   597: iload           7
        //   599: ifeq            605
        //   602: goto            611
        //   605: iconst_0       
        //   606: istore          7
        //   608: goto            614
        //   611: iconst_1       
        //   612: istore          7
        //   614: aload_0        
        //   615: invokestatic    com/android/settingslib/development/DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled:(Landroid/content/Context;)Z
        //   618: ifeq            633
        //   621: invokestatic    com/android/settings/Utils.isMonkeyRunning:()Z
        //   624: ifne            633
        //   627: iconst_1       
        //   628: istore          8
        //   630: goto            636
        //   633: iconst_0       
        //   634: istore          8
        //   636: aload_2        
        //   637: invokevirtual   android/os/UserManager.isAdminUser:()Z
        //   640: ifne            659
        //   643: aload_2        
        //   644: invokevirtual   android/os/UserManager.isDemoUser:()Z
        //   647: ifeq            653
        //   650: goto            659
        //   653: iconst_0       
        //   654: istore          10
        //   656: goto            662
        //   659: iconst_1       
        //   660: istore          10
        //   662: aload_0        
        //   663: aload           6
        //   665: new             Landroid/content/ComponentName;
        //   668: dup            
        //   669: aload           5
        //   671: ldc             Lcom/android/settings/Settings$DevelopmentSettingsDashboardActivity;.class
        //   673: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   676: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   679: iload           8
        //   681: iload           10
        //   683: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   686: ifne            703
        //   689: iload           7
        //   691: ifeq            697
        //   694: goto            703
        //   697: iconst_0       
        //   698: istore          7
        //   700: goto            706
        //   703: iconst_1       
        //   704: istore          7
        //   706: aload_0        
        //   707: aload           6
        //   709: new             Landroid/content/ComponentName;
        //   712: dup            
        //   713: aload           5
        //   715: ldc             Lcom/android/settings/backup/BackupSettingsActivity;.class
        //   717: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   720: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   723: iconst_1       
        //   724: iload_3        
        //   725: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   728: ifne            745
        //   731: iload           7
        //   733: ifeq            739
        //   736: goto            745
        //   739: iconst_0       
        //   740: istore          7
        //   742: goto            748
        //   745: iconst_1       
        //   746: istore          7
        //   748: aload_0        
        //   749: aload           6
        //   751: new             Landroid/content/ComponentName;
        //   754: dup            
        //   755: aload           5
        //   757: ldc             Lcom/android/settings/Settings$WifiDisplaySettingsActivity;.class
        //   759: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   762: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   765: aload_0        
        //   766: invokestatic    com/android/settings/wfd/WifiDisplaySettings.isAvailable:(Landroid/content/Context;)Z
        //   769: iload_3        
        //   770: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   773: ifne            790
        //   776: iload           7
        //   778: ifeq            784
        //   781: goto            790
        //   784: iconst_0       
        //   785: istore          7
        //   787: goto            793
        //   790: iconst_1       
        //   791: istore          7
        //   793: aload           4
        //   795: invokevirtual   com/android/settings/overlay/FeatureFactory.getAccountFeatureProvider:()Lcom/android/settings/accounts/AccountFeatureProvider;
        //   798: aload_0        
        //   799: invokeinterface com/android/settings/accounts/AccountFeatureProvider.isAboutPhoneV2Enabled:(Landroid/content/Context;)Z
        //   804: istore          8
        //   806: aload_0        
        //   807: aload           6
        //   809: new             Landroid/content/ComponentName;
        //   812: dup            
        //   813: aload           5
        //   815: ldc             Lcom/android/settings/Settings$MyDeviceInfoActivity;.class
        //   817: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   820: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   823: iload           8
        //   825: iload_3        
        //   826: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   829: ifne            846
        //   832: iload           7
        //   834: ifeq            840
        //   837: goto            846
        //   840: iconst_0       
        //   841: istore          7
        //   843: goto            849
        //   846: iconst_1       
        //   847: istore          7
        //   849: aload_0        
        //   850: aload           6
        //   852: new             Landroid/content/ComponentName;
        //   855: dup            
        //   856: aload           5
        //   858: ldc             Lcom/android/settings/Settings$DeviceInfoSettingsActivity;.class
        //   860: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   863: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   866: iload           8
        //   868: iconst_1       
        //   869: ixor           
        //   870: iload_3        
        //   871: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //   874: ifne            891
        //   877: iload           7
        //   879: ifeq            885
        //   882: goto            891
        //   885: iconst_0       
        //   886: istore          7
        //   888: goto            894
        //   891: iconst_1       
        //   892: istore          7
        //   894: iload_3        
        //   895: ifne            1143
        //   898: aload_0        
        //   899: getfield        com/android/settings/SettingsActivity.mDashboardFeatureProvider:Lcom/android/settings/dashboard/DashboardFeatureProvider;
        //   902: invokeinterface com/android/settings/dashboard/DashboardFeatureProvider.getAllCategories:()Ljava/util/List;
        //   907: astore          11
        //   909: aload           11
        //   911: monitorenter   
        //   912: aload_1        
        //   913: astore          9
        //   915: aload_2        
        //   916: astore          9
        //   918: aload           11
        //   920: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   925: astore          4
        //   927: aload_1        
        //   928: astore          9
        //   930: aload_2        
        //   931: astore          9
        //   933: aload           4
        //   935: invokeinterface java/util/Iterator.hasNext:()Z
        //   940: ifeq            1127
        //   943: aload_1        
        //   944: astore          9
        //   946: aload_2        
        //   947: astore          9
        //   949: aload           4
        //   951: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   956: checkcast       Lcom/android/settingslib/drawer/DashboardCategory;
        //   959: astore          12
        //   961: aload_1        
        //   962: astore          9
        //   964: aload_2        
        //   965: astore          9
        //   967: aload           12
        //   969: invokevirtual   com/android/settingslib/drawer/DashboardCategory.getTilesCount:()I
        //   972: istore          13
        //   974: iconst_0       
        //   975: istore          14
        //   977: iload           14
        //   979: iload           13
        //   981: if_icmpge       1124
        //   984: aload           12
        //   986: iload           14
        //   988: invokevirtual   com/android/settingslib/drawer/DashboardCategory.getTile:(I)Lcom/android/settingslib/drawer/Tile;
        //   991: getfield        com/android/settingslib/drawer/Tile.intent:Landroid/content/Intent;
        //   994: invokevirtual   android/content/Intent.getComponent:()Landroid/content/ComponentName;
        //   997: astore          15
        //   999: aload           15
        //  1001: invokevirtual   android/content/ComponentName.getClassName:()Ljava/lang/String;
        //  1004: astore          16
        //  1006: getstatic       com/android/settings/core/gateway/SettingsGateway.SETTINGS_FOR_RESTRICTED:[Ljava/lang/String;
        //  1009: astore          9
        //  1011: aload           9
        //  1013: aload           16
        //  1015: invokestatic    com/android/internal/util/ArrayUtils.contains:([Ljava/lang/Object;Ljava/lang/Object;)Z
        //  1018: ifne            1048
        //  1021: iload           10
        //  1023: ifeq            1042
        //  1026: ldc             Lcom/android/settings/Settings$DevelopmentSettingsDashboardActivity;.class
        //  1028: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //  1031: aload           16
        //  1033: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1036: ifeq            1042
        //  1039: goto            1048
        //  1042: iconst_0       
        //  1043: istore          17
        //  1045: goto            1051
        //  1048: iconst_1       
        //  1049: istore          17
        //  1051: aload           5
        //  1053: aload           15
        //  1055: invokevirtual   android/content/ComponentName.getPackageName:()Ljava/lang/String;
        //  1058: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1061: ifeq            1106
        //  1064: iload           17
        //  1066: ifne            1106
        //  1069: aload_0        
        //  1070: aload           6
        //  1072: aload           15
        //  1074: iconst_0       
        //  1075: iload_3        
        //  1076: invokespecial   com/android/settings/SettingsActivity.setTileEnabled:(Ljava/lang/StringBuilder;Landroid/content/ComponentName;ZZ)Z
        //  1079: istore          8
        //  1081: iload           8
        //  1083: ifne            1100
        //  1086: iload           7
        //  1088: ifeq            1094
        //  1091: goto            1100
        //  1094: iconst_0       
        //  1095: istore          7
        //  1097: goto            1103
        //  1100: iconst_1       
        //  1101: istore          7
        //  1103: goto            1106
        //  1106: iinc            14, 1
        //  1109: goto            977
        //  1112: astore_2       
        //  1113: goto            1134
        //  1116: astore_2       
        //  1117: goto            1134
        //  1120: astore_2       
        //  1121: goto            1134
        //  1124: goto            927
        //  1127: aload           11
        //  1129: monitorexit    
        //  1130: goto            1143
        //  1133: astore_2       
        //  1134: aload           11
        //  1136: monitorexit    
        //  1137: aload_2        
        //  1138: athrow         
        //  1139: astore_2       
        //  1140: goto            1134
        //  1143: iload           7
        //  1145: ifeq            1192
        //  1148: new             Ljava/lang/StringBuilder;
        //  1151: dup            
        //  1152: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1155: astore_2       
        //  1156: aload_2        
        //  1157: ldc_w           "Enabled state changed for some tiles, reloading all categories "
        //  1160: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1163: pop            
        //  1164: aload_2        
        //  1165: aload           6
        //  1167: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1170: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1173: pop            
        //  1174: ldc_w           "SettingsActivity"
        //  1177: aload_2        
        //  1178: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1181: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //  1184: pop            
        //  1185: aload_0        
        //  1186: invokevirtual   com/android/settings/SettingsActivity.updateCategories:()V
        //  1189: goto            1202
        //  1192: ldc_w           "SettingsActivity"
        //  1195: ldc_w           "No enabled state changed, skipping updateCategory call"
        //  1198: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //  1201: pop            
        //  1202: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  918    927    1133   1134   Any
        //  933    943    1133   1134   Any
        //  949    961    1133   1134   Any
        //  967    974    1133   1134   Any
        //  984    1006   1120   1124   Any
        //  1006   1011   1116   1120   Any
        //  1011   1021   1112   1116   Any
        //  1026   1039   1112   1116   Any
        //  1051   1064   1112   1116   Any
        //  1069   1081   1112   1116   Any
        //  1127   1130   1139   1143   Any
        //  1134   1137   1139   1143   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_1127:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Thread.java:745)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void getMetaData() {
        try {
            final ActivityInfo activityInfo = this.getPackageManager().getActivityInfo(this.getComponentName(), 128);
            if (activityInfo == null || activityInfo.metaData == null) {
                return;
            }
            this.mFragmentClass = activityInfo.metaData.getString("com.android.settings.FRAGMENT_CLASS");
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot get Metadata for: ");
            sb.append(this.getComponentName().toString());
            Log.d("SettingsActivity", sb.toString());
        }
    }
    
    private String getMetricsTag() {
        String s2;
        final String s = s2 = this.getClass().getName();
        if (this.getIntent() != null) {
            s2 = s;
            if (this.getIntent().hasExtra(":settings:show_fragment")) {
                s2 = this.getIntent().getStringExtra(":settings:show_fragment");
            }
        }
        String replace = s2;
        if (s2.startsWith("com.android.settings.")) {
            replace = s2.replace("com.android.settings.", "");
        }
        return replace;
    }
    
    private String getStartingFragmentClass(final Intent intent) {
        if (this.mFragmentClass != null) {
            return this.mFragmentClass;
        }
        final String className = intent.getComponent().getClassName();
        if (className.equals(this.getClass().getName())) {
            return null;
        }
        if (!"com.android.settings.RunningServices".equals(className)) {
            final String name = className;
            if (!"com.android.settings.applications.StorageUse".equals(className)) {
                return name;
            }
        }
        return ManageApplications.class.getName();
    }
    
    private boolean setTileEnabled(final StringBuilder sb, final ComponentName componentName, final boolean b, final boolean b2) {
        boolean b3 = b;
        if (!b2) {
            b3 = b;
            if (this.getPackageName().equals(componentName.getPackageName())) {
                b3 = b;
                if (!ArrayUtils.contains((Object[])SettingsGateway.SETTINGS_FOR_RESTRICTED, (Object)componentName.getClassName())) {
                    b3 = false;
                }
            }
        }
        final boolean setTileEnabled = this.setTileEnabled(componentName, b3);
        if (setTileEnabled) {
            sb.append(componentName.toShortString());
            sb.append(",");
        }
        return setTileEnabled;
    }
    
    private void setTitleFromBackStack() {
        final int backStackEntryCount = this.getFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 0) {
            if (this.mInitialTitleResId > 0) {
                this.setTitle(this.mInitialTitleResId);
            }
            else {
                this.setTitle(this.mInitialTitle);
            }
            return;
        }
        this.setTitleFromBackStackEntry(this.getFragmentManager().getBackStackEntryAt(backStackEntryCount - 1));
    }
    
    private void setTitleFromBackStackEntry(final FragmentManager$BackStackEntry fragmentManager$BackStackEntry) {
        final int breadCrumbTitleRes = fragmentManager$BackStackEntry.getBreadCrumbTitleRes();
        CharSequence title;
        if (breadCrumbTitleRes > 0) {
            title = this.getText(breadCrumbTitleRes);
        }
        else {
            title = fragmentManager$BackStackEntry.getBreadCrumbTitle();
        }
        if (title != null) {
            this.setTitle(title);
        }
    }
    
    private void setTitleFromIntent(Intent stringExtra) {
        Log.d("SettingsActivity", "Starting to set activity title");
        final int intExtra = stringExtra.getIntExtra(":settings:show_fragment_title_resid", -1);
        if (intExtra > 0) {
            this.mInitialTitle = null;
            this.mInitialTitleResId = intExtra;
            stringExtra = (Intent)stringExtra.getStringExtra(":settings:show_fragment_title_res_package_name");
            Label_0136: {
                if (stringExtra != null) {
                    try {
                        this.setTitle(this.mInitialTitle = this.createPackageContextAsUser((String)stringExtra, 0, new UserHandle(UserHandle.myUserId())).getResources().getText(this.mInitialTitleResId));
                        this.mInitialTitleResId = -1;
                        return;
                    }
                    catch (PackageManager$NameNotFoundException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Could not find package");
                        sb.append((String)stringExtra);
                        Log.w("SettingsActivity", sb.toString());
                        break Label_0136;
                    }
                }
                this.setTitle(this.mInitialTitleResId);
            }
        }
        else {
            this.mInitialTitleResId = -1;
            CharSequence mInitialTitle = stringExtra.getStringExtra(":settings:show_fragment_title");
            if (mInitialTitle == null) {
                mInitialTitle = this.getTitle();
            }
            this.setTitle(this.mInitialTitle = mInitialTitle);
        }
        Log.d("SettingsActivity", "Done setting title");
    }
    
    private Fragment switchToFragment(final String s, final Bundle bundle, final boolean b, final boolean b2, final int breadCrumbTitle, final CharSequence breadCrumbTitle2, final boolean b3) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Switching to fragment ");
        sb.append(s);
        Log.d("SettingsActivity", sb.toString());
        if (b && !this.isValidFragment(s)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid fragment for this activity: ");
            sb2.append(s);
            throw new IllegalArgumentException(sb2.toString());
        }
        final Fragment instantiate = Fragment.instantiate((Context)this, s, bundle);
        final FragmentTransaction beginTransaction = this.getFragmentManager().beginTransaction();
        beginTransaction.replace(2131362353, instantiate);
        if (b3) {
            TransitionManager.beginDelayedTransition(this.mContent);
        }
        if (b2) {
            beginTransaction.addToBackStack(":settings:prefs");
        }
        if (breadCrumbTitle > 0) {
            beginTransaction.setBreadCrumbTitle(breadCrumbTitle);
        }
        else if (breadCrumbTitle2 != null) {
            beginTransaction.setBreadCrumbTitle(breadCrumbTitle2);
        }
        beginTransaction.commitAllowingStateLoss();
        this.getFragmentManager().executePendingTransactions();
        Log.d("SettingsActivity", "Executed frag manager pendingTransactions");
        return instantiate;
    }
    
    private void updateDeviceIndex() {
        ThreadUtils.postOnBackgroundThread(new _$$Lambda$SettingsActivity$HXMcoLHGNmdxTucTgqvnp3fY_K8(this, FeatureFactory.getFactory((Context)this).getDeviceIndexFeatureProvider()));
    }
    
    private void updateTilesList() {
        AsyncTask.execute((Runnable)new Runnable() {
            @Override
            public void run() {
                SettingsActivity.this.doUpdateTilesList();
            }
        });
    }
    
    public void finishPreferencePanel(final int n, final Intent intent) {
        this.setResult(n, intent);
        this.finish();
    }
    
    Bitmap getBitmapFromXmlResource(final int n) {
        final Drawable drawable = this.getResources().getDrawable(n, this.getTheme());
        final Canvas canvas = new Canvas();
        final Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap$Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    
    public Intent getIntent() {
        final Intent intent = super.getIntent();
        final String startingFragmentClass = this.getStartingFragmentClass(intent);
        if (startingFragmentClass != null) {
            final Intent intent2 = new Intent(intent);
            intent2.putExtra(":settings:show_fragment", startingFragmentClass);
            final Bundle bundleExtra = intent.getBundleExtra(":settings:show_fragment_args");
            Bundle bundle;
            if (bundleExtra != null) {
                bundle = new Bundle(bundleExtra);
            }
            else {
                bundle = new Bundle();
            }
            bundle.putParcelable("intent", (Parcelable)intent);
            intent2.putExtra(":settings:show_fragment_args", bundle);
            return intent2;
        }
        return intent;
    }
    
    public Button getNextButton() {
        return this.mNextButton;
    }
    
    public SharedPreferences getSharedPreferences(final String s, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getPackageName());
        sb.append("_preferences");
        if (s.equals(sb.toString())) {
            return (SharedPreferences)new SharedPreferencesLogger((Context)this, this.getMetricsTag(), FeatureFactory.getFactory((Context)this).getMetricsFeatureProvider());
        }
        return super.getSharedPreferences(s, n);
    }
    
    public SwitchBar getSwitchBar() {
        return this.mSwitchBar;
    }
    
    public boolean hasNextButton() {
        return this.mNextButton != null;
    }
    
    protected boolean isValidFragment(final String s) {
        for (int i = 0; i < SettingsGateway.ENTRY_FRAGMENTS.length; ++i) {
            if (SettingsGateway.ENTRY_FRAGMENTS[i].equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    void launchSettingFragment(final String s, final boolean b, final Intent titleFromIntent) {
        if (!this.mIsShowingDashboard && s != null) {
            this.setTitleFromIntent(titleFromIntent);
            this.switchToFragment(s, titleFromIntent.getBundleExtra(":settings:show_fragment_args"), true, false, this.mInitialTitleResId, this.mInitialTitle, false);
        }
        else {
            this.mInitialTitleResId = 2131887202;
            this.switchToFragment(DashboardSummary.class.getName(), null, false, false, this.mInitialTitleResId, this.mInitialTitle, false);
        }
    }
    
    public void onBackStackChanged() {
        this.setTitleFromBackStack();
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        Log.d("SettingsActivity", "Starting onCreate");
        System.currentTimeMillis();
        this.mDashboardFeatureProvider = FeatureFactory.getFactory((Context)this).getDashboardFeatureProvider((Context)this);
        this.getMetaData();
        final Intent intent = this.getIntent();
        if (intent.hasExtra("settings:ui_options")) {
            this.getWindow().setUiOptions(intent.getIntExtra("settings:ui_options", 0));
        }
        final String stringExtra = intent.getStringExtra(":settings:show_fragment");
        this.mIsShowingDashboard = intent.getComponent().getClassName().equals(Settings.class.getName());
        final boolean b = this instanceof SubSettings || intent.getBooleanExtra(":settings:show_fragment_as_subsetting", false);
        if (b) {
            this.setTheme(2131952105);
        }
        int contentView;
        if (this.mIsShowingDashboard) {
            contentView = 2131558744;
        }
        else {
            contentView = 2131558745;
        }
        this.setContentView(contentView);
        this.mContent = (ViewGroup)this.findViewById(2131362353);
        this.getFragmentManager().addOnBackStackChangedListener((FragmentManager$OnBackStackChangedListener)this);
        if (bundle != null) {
            this.setTitleFromIntent(intent);
            final ArrayList parcelableArrayList = bundle.getParcelableArrayList(":settings:categories");
            if (parcelableArrayList != null) {
                this.mCategories.clear();
                this.mCategories.addAll(parcelableArrayList);
                this.setTitleFromBackStack();
            }
        }
        else {
            this.launchSettingFragment(stringExtra, b, intent);
        }
        final boolean deviceProvisioned = Utils.isDeviceProvisioned((Context)this);
        if (this.mIsShowingDashboard) {
            final View viewById = this.findViewById(2131362558);
            int visibility;
            if (deviceProvisioned) {
                visibility = 0;
            }
            else {
                visibility = 4;
            }
            viewById.setVisibility(visibility);
            this.findViewById(R.id.action_bar).setVisibility(8);
            final Toolbar actionBar = (Toolbar)this.findViewById(2131362555);
            FeatureFactory.getFactory((Context)this).getSearchFeatureProvider().initSearchToolbar(this, actionBar);
            this.setActionBar(actionBar);
            final View navigationView = actionBar.getNavigationView();
            navigationView.setClickable(false);
            navigationView.setImportantForAccessibility(2);
            navigationView.setBackground((Drawable)null);
        }
        final ActionBar actionBar2 = this.getActionBar();
        if (actionBar2 != null) {
            actionBar2.setDisplayHomeAsUpEnabled(deviceProvisioned);
            actionBar2.setHomeButtonEnabled(deviceProvisioned);
            actionBar2.setDisplayShowTitleEnabled(true ^ this.mIsShowingDashboard);
        }
        this.mSwitchBar = (SwitchBar)this.findViewById(2131362706);
        if (this.mSwitchBar != null) {
            this.mSwitchBar.setMetricsTag(this.getMetricsTag());
        }
        if (intent.getBooleanExtra("extra_prefs_show_button_bar", false)) {
            final View viewById2 = this.findViewById(2131361950);
            if (viewById2 != null) {
                viewById2.setVisibility(0);
                final Button button = (Button)this.findViewById(2131361895);
                button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        SettingsActivity.this.setResult(0, (Intent)null);
                        SettingsActivity.this.finish();
                    }
                });
                final Button button2 = (Button)this.findViewById(2131362617);
                button2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        SettingsActivity.this.setResult(-1, (Intent)null);
                        SettingsActivity.this.finish();
                    }
                });
                (this.mNextButton = (Button)this.findViewById(2131362389)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        SettingsActivity.this.setResult(-1, (Intent)null);
                        SettingsActivity.this.finish();
                    }
                });
                if (intent.hasExtra("extra_prefs_set_next_text")) {
                    final String stringExtra2 = intent.getStringExtra("extra_prefs_set_next_text");
                    if (TextUtils.isEmpty((CharSequence)stringExtra2)) {
                        this.mNextButton.setVisibility(8);
                    }
                    else {
                        this.mNextButton.setText((CharSequence)stringExtra2);
                    }
                }
                if (intent.hasExtra("extra_prefs_set_back_text")) {
                    final String stringExtra3 = intent.getStringExtra("extra_prefs_set_back_text");
                    if (TextUtils.isEmpty((CharSequence)stringExtra3)) {
                        button.setVisibility(8);
                    }
                    else {
                        button.setText((CharSequence)stringExtra3);
                    }
                }
                if (intent.getBooleanExtra("extra_prefs_show_skip", false)) {
                    button2.setVisibility(0);
                }
            }
        }
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance((Context)this).unregisterReceiver(this.mDevelopmentSettingsListener);
        this.mDevelopmentSettingsListener = null;
        this.unregisterReceiver(this.mBatteryInfoReceiver);
    }
    
    public boolean onPreferenceStartFragment(final PreferenceFragment preferenceFragment, final Preference preference) {
        final SubSettingLauncher setArguments = new SubSettingLauncher((Context)this).setDestination(preference.getFragment()).setArguments(preference.getExtras());
        int metricsCategory;
        if (preferenceFragment instanceof Instrumentable) {
            metricsCategory = ((Instrumentable)preferenceFragment).getMetricsCategory();
        }
        else {
            metricsCategory = 0;
        }
        setArguments.setSourceMetricsCategory(metricsCategory).setTitle(-1).launch();
        return true;
    }
    
    public boolean onPreferenceTreeClick(final Preference preference) {
        return false;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        this.mDevelopmentSettingsListener = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                SettingsActivity.this.updateTilesList();
            }
        };
        LocalBroadcastManager.getInstance((Context)this).registerReceiver(this.mDevelopmentSettingsListener, new IntentFilter("com.android.settingslib.development.DevelopmentSettingsEnabler.SETTINGS_CHANGED"));
        this.registerReceiver(this.mBatteryInfoReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        this.updateTilesList();
        this.updateDeviceIndex();
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.saveState(bundle);
    }
    
    void saveState(final Bundle bundle) {
        if (this.mCategories.size() > 0) {
            bundle.putParcelableArrayList(":settings:categories", (ArrayList)this.mCategories);
        }
    }
    
    public void setTaskDescription(final ActivityManager$TaskDescription taskDescription) {
        taskDescription.setIcon(this.getBitmapFromXmlResource(2131231051));
        super.setTaskDescription(taskDescription);
    }
}
