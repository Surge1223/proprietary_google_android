package com.android.settings;

import java.util.ArrayList;
import java.lang.ref.WeakReference;
import android.content.IntentFilter;
import android.os.Environment;
import android.app.Activity;
import android.os.Bundle;
import android.net.ConnectivityManager$OnStartTetheringCallback;
import android.bluetooth.BluetoothAdapter;
import com.android.settingslib.TetherUtil;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.content.Context;
import android.bluetooth.BluetoothProfile;
import com.android.settings.wifi.tether.WifiTetherPreferenceController;
import android.content.BroadcastReceiver;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.os.Handler;
import android.support.v7.preference.Preference;
import android.net.ConnectivityManager;
import android.support.v14.preference.SwitchPreference;
import android.bluetooth.BluetoothPan;
import java.util.concurrent.atomic.AtomicReference;
import com.android.settings.datausage.DataSaverBackend;

public class TetherSettings extends RestrictedSettingsFragment implements Listener
{
    private boolean mBluetoothEnableForTether;
    private AtomicReference<BluetoothPan> mBluetoothPan;
    private String[] mBluetoothRegexs;
    private SwitchPreference mBluetoothTether;
    private ConnectivityManager mCm;
    private DataSaverBackend mDataSaverBackend;
    private boolean mDataSaverEnabled;
    private Preference mDataSaverFooter;
    private Handler mHandler;
    private boolean mMassStorageActive;
    private BluetoothProfile$ServiceListener mProfileServiceListener;
    private OnStartTetheringCallback mStartTetheringCallback;
    private BroadcastReceiver mTetherChangeReceiver;
    private boolean mUnavailable;
    private boolean mUsbConnected;
    private String[] mUsbRegexs;
    private SwitchPreference mUsbTether;
    private WifiTetherPreferenceController mWifiTetherPreferenceController;
    
    public TetherSettings() {
        super("no_config_tethering");
        this.mBluetoothPan = new AtomicReference<BluetoothPan>();
        this.mHandler = new Handler();
        this.mProfileServiceListener = (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
            public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                TetherSettings.this.mBluetoothPan.set(bluetoothProfile);
            }
            
            public void onServiceDisconnected(final int n) {
                TetherSettings.this.mBluetoothPan.set(null);
            }
        };
    }
    
    private static boolean isIntentAvailable(final Context context) {
        final String[] stringArray = context.getResources().getStringArray(17236021);
        final int length = stringArray.length;
        boolean b = false;
        if (length < 2) {
            return false;
        }
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.setClassName(stringArray[0], stringArray[1]);
        if (packageManager.queryIntentActivities(intent, 65536).size() > 0) {
            b = true;
        }
        return b;
    }
    
    public static boolean isProvisioningNeededButUnavailable(final Context context) {
        return TetherUtil.isProvisioningNeeded(context) && !isIntentAvailable(context);
    }
    
    private void startTethering(final int n) {
        if (n == 2) {
            final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            if (defaultAdapter.getState() == 10) {
                this.mBluetoothEnableForTether = true;
                defaultAdapter.enable();
                this.mBluetoothTether.setEnabled(false);
                return;
            }
        }
        this.mCm.startTethering(n, true, (ConnectivityManager$OnStartTetheringCallback)this.mStartTetheringCallback, this.mHandler);
    }
    
    private void updateBluetoothState() {
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter == null) {
            return;
        }
        final int state = defaultAdapter.getState();
        if (state == 13) {
            this.mBluetoothTether.setEnabled(false);
        }
        else if (state == 11) {
            this.mBluetoothTether.setEnabled(false);
        }
        else {
            final BluetoothPan bluetoothPan = this.mBluetoothPan.get();
            if (state == 12 && bluetoothPan != null && bluetoothPan.isTetheringOn()) {
                this.mBluetoothTether.setChecked(true);
                this.mBluetoothTether.setEnabled(this.mDataSaverEnabled ^ true);
            }
            else {
                this.mBluetoothTether.setEnabled(true ^ this.mDataSaverEnabled);
                this.mBluetoothTether.setChecked(false);
            }
        }
    }
    
    private void updateState() {
        this.updateState(this.mCm.getTetherableIfaces(), this.mCm.getTetheredIfaces(), this.mCm.getTetheringErroredIfaces());
    }
    
    private void updateState(final String[] array, final String[] array2, final String[] array3) {
        this.updateUsbState(array, array2, array3);
        this.updateBluetoothState();
    }
    
    private void updateUsbState(String[] mUsbRegexs, String[] mUsbRegexs2, final String[] array) {
        final boolean b = this.mUsbConnected && !this.mMassStorageActive;
        final int length = mUsbRegexs.length;
        int n = 0;
        for (final String s : mUsbRegexs) {
            final String[] mUsbRegexs3 = this.mUsbRegexs;
            int lastTetherError;
            for (int length2 = mUsbRegexs3.length, j = 0; j < length2; ++j, n = lastTetherError) {
                lastTetherError = n;
                if (s.matches(mUsbRegexs3[j]) && (lastTetherError = n) == 0) {
                    lastTetherError = this.mCm.getLastTetherError(s);
                }
            }
        }
        final int length3 = mUsbRegexs2.length;
        boolean b2 = false;
        for (final String s2 : mUsbRegexs2) {
            mUsbRegexs = this.mUsbRegexs;
            for (int length4 = mUsbRegexs.length, l = 0; l < length4; ++l) {
                if (s2.matches(mUsbRegexs[l])) {
                    b2 = true;
                }
            }
        }
        for (final String s3 : array) {
            mUsbRegexs2 = this.mUsbRegexs;
            for (int length6 = mUsbRegexs2.length, n3 = 0; n3 < length6; ++n3) {
                if (s3.matches(mUsbRegexs2[n3])) {}
            }
        }
        if (b2) {
            this.mUsbTether.setEnabled(this.mDataSaverEnabled ^ true);
            this.mUsbTether.setChecked(true);
        }
        else if (b) {
            this.mUsbTether.setEnabled(true ^ this.mDataSaverEnabled);
            this.mUsbTether.setChecked(false);
        }
        else {
            this.mUsbTether.setEnabled(false);
            this.mUsbTether.setChecked(false);
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887832;
    }
    
    @Override
    public int getMetricsCategory() {
        return 90;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mWifiTetherPreferenceController = new WifiTetherPreferenceController(context, this.getLifecycle());
    }
    
    @Override
    public void onBlacklistStatusChanged(final int n, final boolean b) {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082844);
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131889472);
        this.mDataSaverBackend = new DataSaverBackend(this.getContext());
        this.mDataSaverEnabled = this.mDataSaverBackend.isDataSaverEnabled();
        this.mDataSaverFooter = this.findPreference("disabled_on_data_saver");
        this.setIfOnlyAvailableForAdmins(true);
        if (this.isUiRestricted()) {
            this.mUnavailable = true;
            this.getPreferenceScreen().removeAll();
            return;
        }
        final Activity activity = this.getActivity();
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            defaultAdapter.getProfileProxy(activity.getApplicationContext(), this.mProfileServiceListener, 5);
        }
        this.mUsbTether = (SwitchPreference)this.findPreference("usb_tether_settings");
        this.mBluetoothTether = (SwitchPreference)this.findPreference("enable_bluetooth_tethering");
        this.mDataSaverBackend.addListener((DataSaverBackend.Listener)this);
        this.mCm = (ConnectivityManager)this.getSystemService("connectivity");
        this.mUsbRegexs = this.mCm.getTetherableUsbRegexs();
        this.mBluetoothRegexs = this.mCm.getTetherableBluetoothRegexs();
        final boolean b = this.mUsbRegexs.length != 0;
        final boolean b2 = this.mBluetoothRegexs.length != 0;
        if (!b || Utils.isMonkeyRunning()) {
            this.getPreferenceScreen().removePreference(this.mUsbTether);
        }
        this.mWifiTetherPreferenceController.displayPreference(this.getPreferenceScreen());
        if (!b2) {
            this.getPreferenceScreen().removePreference(this.mBluetoothTether);
        }
        else {
            final BluetoothPan bluetoothPan = this.mBluetoothPan.get();
            if (bluetoothPan != null && bluetoothPan.isTetheringOn()) {
                this.mBluetoothTether.setChecked(true);
            }
            else {
                this.mBluetoothTether.setChecked(false);
            }
        }
        this.onDataSaverChanged(this.mDataSaverBackend.isDataSaverEnabled());
    }
    
    @Override
    public void onDataSaverChanged(final boolean mDataSaverEnabled) {
        this.mDataSaverEnabled = mDataSaverEnabled;
        this.mUsbTether.setEnabled(this.mDataSaverEnabled ^ true);
        this.mBluetoothTether.setEnabled(this.mDataSaverEnabled ^ true);
        this.mDataSaverFooter.setVisible(this.mDataSaverEnabled);
    }
    
    @Override
    public void onDestroy() {
        this.mDataSaverBackend.remListener((DataSaverBackend.Listener)this);
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        final BluetoothProfile bluetoothProfile = (BluetoothProfile)this.mBluetoothPan.getAndSet(null);
        if (bluetoothProfile != null && defaultAdapter != null) {
            defaultAdapter.closeProfileProxy(5, bluetoothProfile);
        }
        super.onDestroy();
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference == this.mUsbTether) {
            if (this.mUsbTether.isChecked()) {
                this.startTethering(1);
            }
            else {
                this.mCm.stopTethering(1);
            }
        }
        else if (preference == this.mBluetoothTether) {
            if (this.mBluetoothTether.isChecked()) {
                this.startTethering(2);
            }
            else {
                this.mCm.stopTethering(2);
                this.updateState();
            }
        }
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (this.mUnavailable) {
            if (!this.isUiRestrictedByOnlyAdmin()) {
                this.getEmptyTextView().setText(2131889476);
            }
            this.getPreferenceScreen().removeAll();
            return;
        }
        final Activity activity = this.getActivity();
        this.mStartTetheringCallback = new OnStartTetheringCallback(this);
        this.mMassStorageActive = "shared".equals(Environment.getExternalStorageState());
        this.mTetherChangeReceiver = new TetherChangeReceiver();
        final Intent registerReceiver = activity.registerReceiver(this.mTetherChangeReceiver, new IntentFilter("android.net.conn.TETHER_STATE_CHANGED"));
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.hardware.usb.action.USB_STATE");
        activity.registerReceiver(this.mTetherChangeReceiver, intentFilter);
        final IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.intent.action.MEDIA_SHARED");
        intentFilter2.addAction("android.intent.action.MEDIA_UNSHARED");
        intentFilter2.addDataScheme("file");
        activity.registerReceiver(this.mTetherChangeReceiver, intentFilter2);
        final IntentFilter intentFilter3 = new IntentFilter();
        intentFilter3.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        activity.registerReceiver(this.mTetherChangeReceiver, intentFilter3);
        if (registerReceiver != null) {
            this.mTetherChangeReceiver.onReceive((Context)activity, registerReceiver);
        }
        this.updateState();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (this.mUnavailable) {
            return;
        }
        this.getActivity().unregisterReceiver(this.mTetherChangeReceiver);
        this.mTetherChangeReceiver = null;
        this.mStartTetheringCallback = null;
    }
    
    @Override
    public void onWhitelistStatusChanged(final int n, final boolean b) {
    }
    
    private static final class OnStartTetheringCallback extends ConnectivityManager$OnStartTetheringCallback
    {
        final WeakReference<TetherSettings> mTetherSettings;
        
        OnStartTetheringCallback(final TetherSettings tetherSettings) {
            this.mTetherSettings = new WeakReference<TetherSettings>(tetherSettings);
        }
        
        private void update() {
            final TetherSettings tetherSettings = this.mTetherSettings.get();
            if (tetherSettings != null) {
                tetherSettings.updateState();
            }
        }
        
        public void onTetheringFailed() {
            this.update();
        }
        
        public void onTetheringStarted() {
            this.update();
        }
    }
    
    private class TetherChangeReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if (action.equals("android.net.conn.TETHER_STATE_CHANGED")) {
                final ArrayList stringArrayListExtra = intent.getStringArrayListExtra("availableArray");
                final ArrayList stringArrayListExtra2 = intent.getStringArrayListExtra("tetherArray");
                final ArrayList stringArrayListExtra3 = intent.getStringArrayListExtra("erroredArray");
                TetherSettings.this.updateState(stringArrayListExtra.toArray(new String[stringArrayListExtra.size()]), stringArrayListExtra2.toArray(new String[stringArrayListExtra2.size()]), stringArrayListExtra3.toArray(new String[stringArrayListExtra3.size()]));
            }
            else if (action.equals("android.intent.action.MEDIA_SHARED")) {
                TetherSettings.this.mMassStorageActive = true;
                TetherSettings.this.updateState();
            }
            else if (action.equals("android.intent.action.MEDIA_UNSHARED")) {
                TetherSettings.this.mMassStorageActive = false;
                TetherSettings.this.updateState();
            }
            else if (action.equals("android.hardware.usb.action.USB_STATE")) {
                TetherSettings.this.mUsbConnected = intent.getBooleanExtra("connected", false);
                TetherSettings.this.updateState();
            }
            else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                if (TetherSettings.this.mBluetoothEnableForTether) {
                    final int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                    if (intExtra != Integer.MIN_VALUE && intExtra != 10) {
                        if (intExtra == 12) {
                            TetherSettings.this.startTethering(2);
                            TetherSettings.this.mBluetoothEnableForTether = false;
                        }
                    }
                    else {
                        TetherSettings.this.mBluetoothEnableForTether = false;
                    }
                }
                TetherSettings.this.updateState();
            }
        }
    }
}
