package com.android.settings.localepicker;

import android.os.Bundle;
import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.widget.LinearLayoutManager;

public class LocaleLinearLayoutManager extends LinearLayoutManager
{
    private final AccessibilityNodeInfoCompat.AccessibilityActionCompat mActionMoveBottom;
    private final AccessibilityNodeInfoCompat.AccessibilityActionCompat mActionMoveDown;
    private final AccessibilityNodeInfoCompat.AccessibilityActionCompat mActionMoveTop;
    private final AccessibilityNodeInfoCompat.AccessibilityActionCompat mActionMoveUp;
    private final AccessibilityNodeInfoCompat.AccessibilityActionCompat mActionRemove;
    private final LocaleDragAndDropAdapter mAdapter;
    private final Context mContext;
    
    public LocaleLinearLayoutManager(final Context mContext, final LocaleDragAndDropAdapter mAdapter) {
        super(mContext);
        this.mContext = mContext;
        this.mAdapter = mAdapter;
        this.mActionMoveUp = new AccessibilityNodeInfoCompat.AccessibilityActionCompat(2131361822, this.mContext.getString(2131886243));
        this.mActionMoveDown = new AccessibilityNodeInfoCompat.AccessibilityActionCompat(2131361820, this.mContext.getString(2131886241));
        this.mActionMoveTop = new AccessibilityNodeInfoCompat.AccessibilityActionCompat(2131361821, this.mContext.getString(2131886242));
        this.mActionMoveBottom = new AccessibilityNodeInfoCompat.AccessibilityActionCompat(2131361819, this.mContext.getString(2131886240));
        this.mActionRemove = new AccessibilityNodeInfoCompat.AccessibilityActionCompat(2131361823, this.mContext.getString(2131886244));
    }
    
    @Override
    public void onInitializeAccessibilityNodeInfoForItem(final Recycler recycler, final State state, final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.onInitializeAccessibilityNodeInfoForItem(recycler, state, view, accessibilityNodeInfoCompat);
        final int itemCount = ((RecyclerView.LayoutManager)this).getItemCount();
        final int position = ((RecyclerView.LayoutManager)this).getPosition(view);
        final LocaleDragCell localeDragCell = (LocaleDragCell)view;
        final StringBuilder sb = new StringBuilder();
        sb.append(position + 1);
        sb.append(", ");
        sb.append((Object)localeDragCell.getCheckbox().getContentDescription());
        accessibilityNodeInfoCompat.setContentDescription(sb.toString());
        if (this.mAdapter.isRemoveMode()) {
            return;
        }
        if (position > 0) {
            accessibilityNodeInfoCompat.addAction(this.mActionMoveUp);
            accessibilityNodeInfoCompat.addAction(this.mActionMoveTop);
        }
        if (position + 1 < itemCount) {
            accessibilityNodeInfoCompat.addAction(this.mActionMoveDown);
            accessibilityNodeInfoCompat.addAction(this.mActionMoveBottom);
        }
        if (itemCount > 1) {
            accessibilityNodeInfoCompat.addAction(this.mActionRemove);
        }
    }
    
    @Override
    public boolean performAccessibilityActionForItem(final Recycler recycler, final State state, final View view, final int n, final Bundle bundle) {
        final int itemCount = ((RecyclerView.LayoutManager)this).getItemCount();
        final int position = ((RecyclerView.LayoutManager)this).getPosition(view);
        boolean b = false;
        switch (n) {
            default: {
                return super.performAccessibilityActionForItem(recycler, state, view, n, bundle);
            }
            case 2131361823: {
                if (itemCount > 1) {
                    this.mAdapter.removeItem(position);
                    b = true;
                    break;
                }
                break;
            }
            case 2131361822: {
                if (position > 0) {
                    this.mAdapter.onItemMove(position, position - 1);
                    b = true;
                    break;
                }
                break;
            }
            case 2131361821: {
                if (position != 0) {
                    this.mAdapter.onItemMove(position, 0);
                    b = true;
                    break;
                }
                break;
            }
            case 2131361820: {
                if (position + 1 < itemCount) {
                    this.mAdapter.onItemMove(position, position + 1);
                    b = true;
                    break;
                }
                break;
            }
            case 2131361819: {
                if (position != itemCount - 1) {
                    this.mAdapter.onItemMove(position, itemCount - 1);
                    b = true;
                    break;
                }
                break;
            }
        }
        if (b) {
            this.mAdapter.doTheUpdate();
        }
        return b;
    }
}
