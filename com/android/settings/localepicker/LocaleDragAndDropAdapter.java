package com.android.settings.localepicker;

import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import com.android.settings.shortcut.CreateShortcut;
import com.android.internal.app.LocalePicker;
import java.util.ArrayList;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import java.util.Iterator;
import java.util.Locale;
import android.view.View;
import android.graphics.Canvas;
import android.util.TypedValue;
import java.text.NumberFormat;
import android.os.LocaleList;
import android.support.v7.widget.helper.ItemTouchHelper;
import com.android.internal.app.LocaleStore$LocaleInfo;
import java.util.List;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

class LocaleDragAndDropAdapter extends Adapter<CustomViewHolder>
{
    private final Context mContext;
    private boolean mDragEnabled;
    private final List<LocaleStore$LocaleInfo> mFeedItemList;
    private final ItemTouchHelper mItemTouchHelper;
    private LocaleList mLocalesSetLast;
    private LocaleList mLocalesToSetNext;
    private NumberFormat mNumberFormatter;
    private RecyclerView mParentView;
    private boolean mRemoveMode;
    
    public LocaleDragAndDropAdapter(final Context mContext, final List<LocaleStore$LocaleInfo> mFeedItemList) {
        this.mParentView = null;
        this.mRemoveMode = false;
        this.mDragEnabled = true;
        this.mNumberFormatter = NumberFormat.getNumberInstance();
        this.mLocalesToSetNext = null;
        this.mLocalesSetLast = null;
        this.mFeedItemList = mFeedItemList;
        this.mContext = mContext;
        this.mItemTouchHelper = new ItemTouchHelper((ItemTouchHelper.Callback)new ItemTouchHelper.SimpleCallback(3, 0) {
            private int mSelectionStatus = -1;
            final /* synthetic */ float val$dragElevation = TypedValue.applyDimension(1, 8.0f, mContext.getResources().getDisplayMetrics());
            
            @Override
            public void onChildDraw(final Canvas canvas, final RecyclerView recyclerView, final ViewHolder viewHolder, float val$dragElevation, final float n, final int n2, final boolean b) {
                super.onChildDraw(canvas, recyclerView, viewHolder, val$dragElevation, n, n2, b);
                if (this.mSelectionStatus != -1) {
                    final View itemView = viewHolder.itemView;
                    if (this.mSelectionStatus == 1) {
                        val$dragElevation = this.val$dragElevation;
                    }
                    else {
                        val$dragElevation = 0.0f;
                    }
                    itemView.setElevation(val$dragElevation);
                    this.mSelectionStatus = -1;
                }
            }
            
            @Override
            public boolean onMove(final RecyclerView recyclerView, final ViewHolder viewHolder, final ViewHolder viewHolder2) {
                LocaleDragAndDropAdapter.this.onItemMove(viewHolder.getAdapterPosition(), viewHolder2.getAdapterPosition());
                return true;
            }
            
            @Override
            public void onSelectedChanged(final ViewHolder viewHolder, final int n) {
                super.onSelectedChanged(viewHolder, n);
                if (n == 2) {
                    this.mSelectionStatus = 1;
                }
                else if (n == 0) {
                    this.mSelectionStatus = 0;
                }
            }
            
            @Override
            public void onSwiped(final ViewHolder viewHolder, final int n) {
            }
        });
    }
    
    private void setDragEnabled(final boolean mDragEnabled) {
        this.mDragEnabled = mDragEnabled;
    }
    
    void addLocale(final LocaleStore$LocaleInfo localeStore$LocaleInfo) {
        this.mFeedItemList.add(localeStore$LocaleInfo);
        ((RecyclerView.Adapter)this).notifyItemInserted(this.mFeedItemList.size() - 1);
        this.doTheUpdate();
    }
    
    public void doTheUpdate() {
        final int size = this.mFeedItemList.size();
        final Locale[] array = new Locale[size];
        for (int i = 0; i < size; ++i) {
            array[i] = this.mFeedItemList.get(i).getLocale();
        }
        this.updateLocalesWhenAnimationStops(new LocaleList(array));
    }
    
    int getCheckedCount() {
        int n = 0;
        final Iterator<LocaleStore$LocaleInfo> iterator = this.mFeedItemList.iterator();
        while (iterator.hasNext()) {
            int n2 = n;
            if (iterator.next().getChecked()) {
                n2 = n + 1;
            }
            n = n2;
        }
        return n;
    }
    
    @Override
    public int getItemCount() {
        int size;
        if (this.mFeedItemList != null) {
            size = this.mFeedItemList.size();
        }
        else {
            size = 0;
        }
        if (size >= 2 && !this.mRemoveMode) {
            this.setDragEnabled(true);
        }
        else {
            this.setDragEnabled(false);
        }
        return size;
    }
    
    boolean isRemoveMode() {
        return this.mRemoveMode;
    }
    
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int n) {
        final LocaleStore$LocaleInfo tag = this.mFeedItemList.get(n);
        final LocaleDragCell localeDragCell = customViewHolder.getLocaleDragCell();
        localeDragCell.setLabelAndDescription(tag.getFullNameNative(), tag.getFullNameInUiLanguage());
        localeDragCell.setLocalized(tag.isTranslated());
        localeDragCell.setMiniLabel(this.mNumberFormatter.format(n + 1));
        localeDragCell.setShowCheckbox(this.mRemoveMode);
        final boolean mRemoveMode = this.mRemoveMode;
        boolean showHandle = true;
        localeDragCell.setShowMiniLabel(mRemoveMode ^ true);
        final boolean mRemoveMode2 = this.mRemoveMode;
        final boolean b = false;
        if (mRemoveMode2 || !this.mDragEnabled) {
            showHandle = false;
        }
        localeDragCell.setShowHandle(showHandle);
        boolean checked = b;
        if (this.mRemoveMode) {
            checked = tag.getChecked();
        }
        localeDragCell.setChecked(checked);
        localeDragCell.setTag((Object)tag);
        localeDragCell.getCheckbox().setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean checked) {
                ((LocaleStore$LocaleInfo)localeDragCell.getTag()).setChecked(checked);
            }
        });
    }
    
    public CustomViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        return new CustomViewHolder((LocaleDragCell)LayoutInflater.from(this.mContext).inflate(2131558601, viewGroup, false));
    }
    
    void onItemMove(final int n, final int n2) {
        if (n >= 0 && n2 >= 0) {
            final LocaleStore$LocaleInfo localeStore$LocaleInfo = this.mFeedItemList.get(n);
            this.mFeedItemList.remove(n);
            this.mFeedItemList.add(n2, localeStore$LocaleInfo);
        }
        else {
            Log.e("LocaleDragAndDropAdapter", String.format(Locale.US, "Negative position in onItemMove %d -> %d", n, n2));
        }
        ((RecyclerView.Adapter)this).notifyItemChanged(n);
        ((RecyclerView.Adapter)this).notifyItemChanged(n2);
        ((RecyclerView.Adapter)this).notifyItemMoved(n, n2);
    }
    
    void removeChecked() {
        for (int i = this.mFeedItemList.size() - 1; i >= 0; --i) {
            if (this.mFeedItemList.get(i).getChecked()) {
                this.mFeedItemList.remove(i);
            }
        }
        ((RecyclerView.Adapter)this).notifyDataSetChanged();
        this.doTheUpdate();
    }
    
    void removeItem(final int n) {
        final int size = this.mFeedItemList.size();
        if (size <= 1) {
            return;
        }
        if (n >= 0 && n < size) {
            this.mFeedItemList.remove(n);
            ((RecyclerView.Adapter)this).notifyDataSetChanged();
        }
    }
    
    public void restoreState(final Bundle bundle) {
        if (bundle != null && this.mRemoveMode) {
            final ArrayList stringArrayList = bundle.getStringArrayList("selectedLocales");
            if (stringArrayList == null || stringArrayList.isEmpty()) {
                return;
            }
            for (final LocaleStore$LocaleInfo localeStore$LocaleInfo : this.mFeedItemList) {
                localeStore$LocaleInfo.setChecked(stringArrayList.contains(localeStore$LocaleInfo.getId()));
            }
            ((RecyclerView.Adapter)this).notifyItemRangeChanged(0, this.mFeedItemList.size());
        }
    }
    
    public void saveState(final Bundle bundle) {
        if (bundle != null) {
            final ArrayList<String> list = new ArrayList<String>();
            for (final LocaleStore$LocaleInfo localeStore$LocaleInfo : this.mFeedItemList) {
                if (localeStore$LocaleInfo.getChecked()) {
                    list.add(localeStore$LocaleInfo.getId());
                }
            }
            bundle.putStringArrayList("selectedLocales", (ArrayList)list);
        }
    }
    
    public void setRecyclerView(final RecyclerView mParentView) {
        this.mParentView = mParentView;
        this.mItemTouchHelper.attachToRecyclerView(mParentView);
    }
    
    void setRemoveMode(final boolean mRemoveMode) {
        this.mRemoveMode = mRemoveMode;
        for (int size = this.mFeedItemList.size(), i = 0; i < size; ++i) {
            this.mFeedItemList.get(i).setChecked(false);
            ((RecyclerView.Adapter)this).notifyItemChanged(i);
        }
    }
    
    public void updateLocalesWhenAnimationStops(final LocaleList list) {
        if (list.equals((Object)this.mLocalesToSetNext)) {
            return;
        }
        LocaleList.setDefault(list);
        this.mLocalesToSetNext = list;
        this.mParentView.getItemAnimator().isRunning((RecyclerView.ItemAnimator.ItemAnimatorFinishedListener)new ItemAnimatorFinishedListener() {
            @Override
            public void onAnimationsFinished() {
                if (LocaleDragAndDropAdapter.this.mLocalesToSetNext != null && !LocaleDragAndDropAdapter.this.mLocalesToSetNext.equals((Object)LocaleDragAndDropAdapter.this.mLocalesSetLast)) {
                    LocalePicker.updateLocales(LocaleDragAndDropAdapter.this.mLocalesToSetNext);
                    LocaleDragAndDropAdapter.this.mLocalesSetLast = LocaleDragAndDropAdapter.this.mLocalesToSetNext;
                    new CreateShortcut.ShortcutsUpdateTask(LocaleDragAndDropAdapter.this.mContext).execute((Object[])new Void[0]);
                    LocaleDragAndDropAdapter.this.mLocalesToSetNext = null;
                    LocaleDragAndDropAdapter.this.mNumberFormatter = NumberFormat.getNumberInstance(Locale.getDefault());
                }
            }
        });
    }
    
    class CustomViewHolder extends ViewHolder implements View.OnTouchListener
    {
        private final LocaleDragCell mLocaleDragCell;
        
        public CustomViewHolder(final LocaleDragCell mLocaleDragCell) {
            super((View)mLocaleDragCell);
            this.mLocaleDragCell = mLocaleDragCell;
            this.mLocaleDragCell.getDragHandle().setOnTouchListener((View.OnTouchListener)this);
        }
        
        public LocaleDragCell getLocaleDragCell() {
            return this.mLocaleDragCell;
        }
        
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            if (LocaleDragAndDropAdapter.this.mDragEnabled) {
                if (MotionEventCompat.getActionMasked(motionEvent) == 0) {
                    LocaleDragAndDropAdapter.this.mItemTouchHelper.startDrag(this);
                }
            }
            return false;
        }
    }
}
