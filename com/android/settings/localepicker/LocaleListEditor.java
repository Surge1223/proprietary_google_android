package com.android.settings.localepicker;

import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.os.Bundle;
import android.view.MenuItem;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.os.LocaleList;
import com.android.internal.app.LocaleStore;
import com.android.internal.app.LocalePicker;
import java.util.ArrayList;
import com.android.internal.app.LocaleStore$LocaleInfo;
import java.util.List;
import android.app.Fragment;
import com.android.internal.app.LocalePickerWithRegion;
import android.view.View.OnClickListener;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import com.android.internal.app.LocalePickerWithRegion$LocaleSelectedListener;
import com.android.settings.RestrictedSettingsFragment;

public class LocaleListEditor extends RestrictedSettingsFragment implements LocalePickerWithRegion$LocaleSelectedListener
{
    private LocaleDragAndDropAdapter mAdapter;
    private View mAddLanguage;
    private boolean mIsUiRestricted;
    private Menu mMenu;
    private boolean mRemoveMode;
    private boolean mShowingRemoveDialog;
    
    public LocaleListEditor() {
        super("no_config_locale");
    }
    
    private void configureDragAndDrop(final View view) {
        final RecyclerView recyclerView = (RecyclerView)view.findViewById(2131362091);
        final LocaleLinearLayoutManager layoutManager = new LocaleLinearLayoutManager(this.getContext(), this.mAdapter);
        ((RecyclerView.LayoutManager)layoutManager).setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager((RecyclerView.LayoutManager)layoutManager);
        recyclerView.setHasFixedSize(true);
        this.mAdapter.setRecyclerView(recyclerView);
        recyclerView.setAdapter((RecyclerView.Adapter)this.mAdapter);
        (this.mAddLanguage = view.findViewById(2131361837)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                LocaleListEditor.this.getFragmentManager().beginTransaction().setTransition(4097).replace(LocaleListEditor.this.getId(), (Fragment)LocalePickerWithRegion.createLanguagePicker(LocaleListEditor.this.getContext(), (LocalePickerWithRegion$LocaleSelectedListener)LocaleListEditor.this, false)).addToBackStack("localeListEditor").commit();
            }
        });
    }
    
    private List<LocaleStore$LocaleInfo> getUserLocaleList() {
        final ArrayList<LocaleStore$LocaleInfo> list = new ArrayList<LocaleStore$LocaleInfo>();
        final LocaleList locales = LocalePicker.getLocales();
        for (int i = 0; i < locales.size(); ++i) {
            list.add(LocaleStore.getLocaleInfo(locales.get(i)));
        }
        return list;
    }
    
    private void setRemoveMode(final boolean b) {
        this.mRemoveMode = b;
        this.mAdapter.setRemoveMode(b);
        final View mAddLanguage = this.mAddLanguage;
        int visibility;
        if (b) {
            visibility = 4;
        }
        else {
            visibility = 0;
        }
        mAddLanguage.setVisibility(visibility);
        this.updateVisibilityOfRemoveMenu();
    }
    
    private void showRemoveLocaleWarningDialog() {
        final int checkedCount = this.mAdapter.getCheckedCount();
        if (checkedCount == 0) {
            this.setRemoveMode(true ^ this.mRemoveMode);
            return;
        }
        if (checkedCount == this.mAdapter.getItemCount()) {
            this.mShowingRemoveDialog = true;
            new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131887462).setMessage(2131887461).setPositiveButton(17039379, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                }
            }).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    LocaleListEditor.this.mShowingRemoveDialog = false;
                }
            }).create().show();
            return;
        }
        final String quantityString = this.getResources().getQuantityString(2131755029, checkedCount);
        this.mShowingRemoveDialog = true;
        new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)quantityString).setMessage(2131887463).setNegativeButton(17039369, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                LocaleListEditor.this.setRemoveMode(false);
            }
        }).setPositiveButton(17039379, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                LocaleListEditor.this.mRemoveMode = false;
                LocaleListEditor.this.mShowingRemoveDialog = false;
                LocaleListEditor.this.mAdapter.removeChecked();
                LocaleListEditor.this.setRemoveMode(false);
            }
        }).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                LocaleListEditor.this.mShowingRemoveDialog = false;
            }
        }).create().show();
    }
    
    private void updateVisibilityOfRemoveMenu() {
        if (this.mMenu == null) {
            return;
        }
        final Menu mMenu = this.mMenu;
        int showAsAction = 2;
        final MenuItem item = mMenu.findItem(2);
        if (item != null) {
            if (!this.mRemoveMode) {
                showAsAction = 0;
            }
            item.setShowAsAction(showAsAction);
            final int itemCount = this.mAdapter.getItemCount();
            boolean visible = true;
            if (itemCount <= 1 || this.mIsUiRestricted) {
                visible = false;
            }
            item.setVisible(visible);
        }
    }
    
    public int getMetricsCategory() {
        return 344;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setHasOptionsMenu(true);
        LocaleStore.fillCache(this.getContext());
        this.mAdapter = new LocaleDragAndDropAdapter(this.getContext(), this.getUserLocaleList());
    }
    
    public void onCreateOptionsMenu(final Menu mMenu, final MenuInflater menuInflater) {
        final MenuItem add = mMenu.add(0, 2, 0, 2131888037);
        add.setShowAsAction(4);
        add.setIcon(2131230999);
        super.onCreateOptionsMenu(mMenu, menuInflater);
        this.mMenu = mMenu;
        this.updateVisibilityOfRemoveMenu();
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.configureDragAndDrop(layoutInflater.inflate(2131558602, (ViewGroup)onCreateView));
        return onCreateView;
    }
    
    public void onLocaleSelected(final LocaleStore$LocaleInfo localeStore$LocaleInfo) {
        this.mAdapter.addLocale(localeStore$LocaleInfo);
        this.updateVisibilityOfRemoveMenu();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId != 2) {
            if (itemId == 16908332) {
                if (this.mRemoveMode) {
                    this.setRemoveMode(false);
                    return true;
                }
            }
            return super.onOptionsItemSelected(menuItem);
        }
        if (this.mRemoveMode) {
            this.showRemoveLocaleWarningDialog();
        }
        else {
            this.setRemoveMode(true);
        }
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final boolean mIsUiRestricted = this.mIsUiRestricted;
        this.mIsUiRestricted = this.isUiRestricted();
        final TextView emptyTextView = this.getEmptyTextView();
        if (this.mIsUiRestricted && !mIsUiRestricted) {
            emptyTextView.setText(2131888006);
            emptyTextView.setVisibility(0);
            this.updateVisibilityOfRemoveMenu();
        }
        else if (!this.mIsUiRestricted && mIsUiRestricted) {
            emptyTextView.setVisibility(8);
            this.updateVisibilityOfRemoveMenu();
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("localeRemoveMode", this.mRemoveMode);
        bundle.putBoolean("showingLocaleRemoveDialog", this.mShowingRemoveDialog);
        this.mAdapter.saveState(bundle);
    }
    
    public void onViewStateRestored(final Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null) {
            this.mRemoveMode = bundle.getBoolean("localeRemoveMode", false);
            this.mShowingRemoveDialog = bundle.getBoolean("showingLocaleRemoveDialog", false);
        }
        this.setRemoveMode(this.mRemoveMode);
        this.mAdapter.restoreState(bundle);
        if (this.mShowingRemoveDialog) {
            this.showRemoveLocaleWarningDialog();
        }
    }
}
