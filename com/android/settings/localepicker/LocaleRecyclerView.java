package com.android.settings.localepicker;

import android.view.MotionEvent;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

class LocaleRecyclerView extends RecyclerView
{
    public LocaleRecyclerView(final Context context) {
        super(context);
    }
    
    public LocaleRecyclerView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public LocaleRecyclerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            final LocaleDragAndDropAdapter localeDragAndDropAdapter = (LocaleDragAndDropAdapter)this.getAdapter();
            if (localeDragAndDropAdapter != null) {
                localeDragAndDropAdapter.doTheUpdate();
            }
        }
        return super.onTouchEvent(motionEvent);
    }
}
