package com.android.settings.localepicker;

import android.os.LocaleList;
import com.android.internal.app.LocaleHelper;
import java.util.Locale;
import com.android.internal.app.LocalePicker;

public class LocaleFeatureProviderImpl implements LocaleFeatureProvider
{
    @Override
    public String getLocaleNames() {
        final LocaleList locales = LocalePicker.getLocales();
        final Locale default1 = Locale.getDefault();
        return LocaleHelper.toSentenceCase(LocaleHelper.getDisplayLocaleList(locales, default1, 2), default1);
    }
}
