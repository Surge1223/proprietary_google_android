package com.android.settings.localepicker;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

class LocaleDragCell extends RelativeLayout
{
    private CheckBox mCheckbox;
    private ImageView mDragHandle;
    private TextView mLabel;
    private TextView mLocalized;
    private TextView mMiniLabel;
    
    public LocaleDragCell(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public CheckBox getCheckbox() {
        return this.mCheckbox;
    }
    
    public ImageView getDragHandle() {
        return this.mDragHandle;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabel = (TextView)this.findViewById(2131362311);
        this.mLocalized = (TextView)this.findViewById(2131362300);
        this.mMiniLabel = (TextView)this.findViewById(2131362375);
        this.mCheckbox = (CheckBox)this.findViewById(2131361981);
        this.mDragHandle = (ImageView)this.findViewById(2131362090);
    }
    
    public void setChecked(final boolean checked) {
        this.mCheckbox.setChecked(checked);
    }
    
    public void setLabelAndDescription(final String s, final String s2) {
        this.mLabel.setText((CharSequence)s);
        this.mCheckbox.setText((CharSequence)s);
        this.mLabel.setContentDescription((CharSequence)s2);
        this.mCheckbox.setContentDescription((CharSequence)s2);
        this.invalidate();
    }
    
    public void setLocalized(final boolean b) {
        final TextView mLocalized = this.mLocalized;
        int visibility;
        if (b) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        mLocalized.setVisibility(visibility);
        this.invalidate();
    }
    
    public void setMiniLabel(final String text) {
        this.mMiniLabel.setText((CharSequence)text);
        this.invalidate();
    }
    
    public void setShowCheckbox(final boolean b) {
        if (b) {
            this.mCheckbox.setVisibility(0);
            this.mLabel.setVisibility(4);
        }
        else {
            this.mCheckbox.setVisibility(4);
            this.mLabel.setVisibility(0);
        }
        this.invalidate();
        this.requestLayout();
    }
    
    public void setShowHandle(final boolean b) {
        final ImageView mDragHandle = this.mDragHandle;
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        mDragHandle.setVisibility(visibility);
        this.invalidate();
        this.requestLayout();
    }
    
    public void setShowMiniLabel(final boolean b) {
        final TextView mMiniLabel = this.mMiniLabel;
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        mMiniLabel.setVisibility(visibility);
        this.invalidate();
        this.requestLayout();
    }
}
