package com.android.settings.applications;

import android.content.Context;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateWriteSettingsBridge extends AppStateAppOpsBridge
{
    public static final AppFilter FILTER_WRITE_SETTINGS;
    private static final String[] PM_PERMISSIONS;
    
    static {
        PM_PERMISSIONS = new String[] { "android.permission.WRITE_SETTINGS" };
        FILTER_WRITE_SETTINGS = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.extraInfo != null;
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    public AppStateWriteSettingsBridge(final Context context, final ApplicationsState applicationsState, final Callback callback) {
        super(context, applicationsState, callback, 23, AppStateWriteSettingsBridge.PM_PERMISSIONS);
    }
    
    public WriteSettingsState getWriteSettingsInfo(final String s, final int n) {
        return new WriteSettingsState(super.getPermissionInfo(s, n));
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = this.getWriteSettingsInfo(s, n);
    }
    
    public static class WriteSettingsState extends PermissionState
    {
        public WriteSettingsState(final PermissionState permissionState) {
            super(permissionState.packageName, permissionState.userHandle);
            this.packageInfo = permissionState.packageInfo;
            this.appOpMode = permissionState.appOpMode;
            this.permissionDeclared = permissionState.permissionDeclared;
            this.staticPermissionGranted = permissionState.staticPermissionGranted;
        }
    }
}
