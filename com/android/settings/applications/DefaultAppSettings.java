package com.android.settings.applications;

import android.text.TextUtils;
import com.android.settings.applications.defaultapps.DefaultHomePreferenceController;
import com.android.settings.applications.defaultapps.DefaultEmergencyPreferenceController;
import com.android.settings.applications.defaultapps.DefaultSmsPreferenceController;
import com.android.settings.applications.defaultapps.DefaultPhonePreferenceController;
import com.android.settings.applications.defaultapps.DefaultBrowserPreferenceController;
import com.android.settings.applications.assist.DefaultAssistPreferenceController;
import com.android.settings.widget.PreferenceCategoryController;
import java.util.Collection;
import com.android.settings.applications.defaultapps.DefaultWorkBrowserPreferenceController;
import com.android.settings.applications.defaultapps.DefaultWorkPhonePreferenceController;
import com.android.settings.applications.defaultapps.DefaultPaymentSettingsPreferenceController;
import java.util.ArrayList;
import android.app.Activity;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class DefaultAppSettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("assist_and_voice_input");
                nonIndexableKeys.add("work_default_phone_app");
                nonIndexableKeys.add("work_default_browser");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082708;
                return Arrays.asList(searchIndexableResource);
            }
        };
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new DefaultAppSettings.SummaryProvider((Context)activity, summaryLoader);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context) {
        final ArrayList<DefaultPaymentSettingsPreferenceController> list = (ArrayList<DefaultPaymentSettingsPreferenceController>)new ArrayList<AbstractPreferenceController>();
        final ArrayList<DefaultPaymentSettingsPreferenceController> children = new ArrayList<DefaultPaymentSettingsPreferenceController>();
        children.add(new DefaultWorkPhonePreferenceController(context));
        children.add((DefaultWorkPhonePreferenceController)new DefaultWorkBrowserPreferenceController(context));
        list.addAll((Collection<? extends AbstractPreferenceController>)children);
        list.add(new PreferenceCategoryController(context, "work_app_defaults").setChildren((List<AbstractPreferenceController>)children));
        list.add(new DefaultAssistPreferenceController(context, "assist_and_voice_input", false));
        list.add(new DefaultBrowserPreferenceController(context));
        list.add(new DefaultPhonePreferenceController(context));
        list.add(new DefaultSmsPreferenceController(context));
        list.add(new DefaultEmergencyPreferenceController(context));
        list.add(new DefaultHomePreferenceController(context));
        list.add(new DefaultPaymentSettingsPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context);
    }
    
    @Override
    protected String getLogTag() {
        return "DefaultAppSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 130;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082708;
    }
    
    static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final DefaultBrowserPreferenceController mDefaultBrowserPreferenceController;
        private final DefaultPhonePreferenceController mDefaultPhonePreferenceController;
        private final DefaultSmsPreferenceController mDefaultSmsPreferenceController;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
            this.mDefaultSmsPreferenceController = new DefaultSmsPreferenceController(this.mContext);
            this.mDefaultBrowserPreferenceController = new DefaultBrowserPreferenceController(this.mContext);
            this.mDefaultPhonePreferenceController = new DefaultPhonePreferenceController(this.mContext);
        }
        
        private CharSequence concatSummaryText(final CharSequence charSequence, final CharSequence charSequence2) {
            if (TextUtils.isEmpty(charSequence)) {
                return charSequence2;
            }
            if (TextUtils.isEmpty(charSequence2)) {
                return charSequence;
            }
            return this.mContext.getString(2131887916, new Object[] { charSequence, charSequence2 });
        }
        
        @Override
        public void setListening(final boolean b) {
            if (!b) {
                return;
            }
            final CharSequence concatSummaryText = this.concatSummaryText(this.concatSummaryText(this.mDefaultBrowserPreferenceController.getDefaultAppLabel(), this.mDefaultPhonePreferenceController.getDefaultAppLabel()), this.mDefaultSmsPreferenceController.getDefaultAppLabel());
            if (!TextUtils.isEmpty(concatSummaryText)) {
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, concatSummaryText);
            }
        }
    }
}
