package com.android.settings.applications.appinfo;

import android.support.v7.preference.Preference;
import com.android.settings.notification.AppNotificationSettings;
import com.android.settings.SettingsPreferenceFragment;
import android.os.Bundle;
import com.android.settingslib.applications.ApplicationsState;
import android.content.Context;
import com.android.settings.notification.NotificationBackend;

public class AppNotificationPreferenceController extends AppInfoPreferenceControllerBase
{
    private final NotificationBackend mBackend;
    private String mChannelId;
    
    public AppNotificationPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mChannelId = null;
        this.mBackend = new NotificationBackend();
    }
    
    public static CharSequence getNotificationSummary(final NotificationBackend.AppRow appRow, final Context context) {
        if (appRow == null) {
            return "";
        }
        if (appRow.banned) {
            return context.getText(2131888509);
        }
        if (appRow.channelCount == 0) {
            return context.getText(2131888510);
        }
        if (appRow.channelCount == appRow.blockedChannelCount) {
            return context.getText(2131888509);
        }
        if (appRow.blockedChannelCount == 0) {
            return context.getText(2131888510);
        }
        return context.getString(2131888511, new Object[] { context.getResources().getQuantityString(2131755049, appRow.blockedChannelCount, new Object[] { appRow.blockedChannelCount }) });
    }
    
    private CharSequence getNotificationSummary(final AppEntry appEntry, final Context context, final NotificationBackend notificationBackend) {
        return getNotificationSummary(notificationBackend.loadAppRow(context, context.getPackageManager(), appEntry.info), context);
    }
    
    @Override
    protected Bundle getArguments() {
        Bundle bundle = null;
        if (this.mChannelId != null) {
            bundle = new Bundle();
            bundle.putString(":settings:fragment_args_key", this.mChannelId);
        }
        return bundle;
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return AppNotificationSettings.class;
    }
    
    @Override
    public void setParentFragment(final AppInfoDashboardFragment parentFragment) {
        super.setParentFragment(parentFragment);
        if (parentFragment != null && parentFragment.getActivity() != null && parentFragment.getActivity().getIntent() != null) {
            this.mChannelId = parentFragment.getActivity().getIntent().getStringExtra(":settings:fragment_args_key");
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(this.getNotificationSummary(this.mParent.getAppEntry(), this.mContext, this.mBackend));
    }
}
