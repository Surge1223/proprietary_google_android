package com.android.settings.applications.appinfo;

import com.android.settings.SettingsPreferenceFragment;
import android.content.pm.PackageInfo;
import android.os.UserManager;
import android.content.Context;

public class DrawOverlayDetailPreferenceController extends AppInfoPreferenceControllerBase
{
    public DrawOverlayDetailPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (UserManager.get(this.mContext).isManagedProfile()) {
            return 3;
        }
        final PackageInfo packageInfo = this.mParent.getPackageInfo();
        if (packageInfo != null && packageInfo.requestedPermissions != null) {
            for (int i = 0; i < packageInfo.requestedPermissions.length; ++i) {
                if (packageInfo.requestedPermissions[i].equals("android.permission.SYSTEM_ALERT_WINDOW")) {
                    return 0;
                }
            }
            return 3;
        }
        return 3;
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return DrawOverlayDetails.class;
    }
    
    @Override
    public CharSequence getSummary() {
        return DrawOverlayDetails.getSummary(this.mContext, this.mParent.getAppEntry());
    }
}
