package com.android.settings.applications.appinfo;

import android.support.v7.preference.Preference;
import android.content.pm.PackageInfo;
import com.android.settings.applications.AppStateBaseBridge;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.AppStateInstallAppsBridge;
import com.android.settings.SettingsPreferenceFragment;
import android.os.UserManager;
import android.content.Context;

public class ExternalSourceDetailPreferenceController extends AppInfoPreferenceControllerBase
{
    private String mPackageName;
    
    public ExternalSourceDetailPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        final boolean managedProfile = UserManager.get(this.mContext).isManagedProfile();
        int n = 3;
        if (managedProfile) {
            return 3;
        }
        if (this.isPotentialAppSource()) {
            n = 0;
        }
        return n;
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return ExternalSourcesDetails.class;
    }
    
    CharSequence getPreferenceSummary() {
        return ExternalSourcesDetails.getPreferenceSummary(this.mContext, this.mParent.getAppEntry());
    }
    
    boolean isPotentialAppSource() {
        final PackageInfo packageInfo = this.mParent.getPackageInfo();
        return packageInfo != null && new AppStateInstallAppsBridge(this.mContext, null, null).createInstallAppsStateFor(this.mPackageName, packageInfo.applicationInfo.uid).isPotentialAppSource();
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(this.getPreferenceSummary());
    }
}
