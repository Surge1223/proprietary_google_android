package com.android.settings.applications.appinfo;

import android.os.UserHandle;
import com.android.settings.applications.defaultapps.DefaultBrowserPreferenceController;
import android.content.Context;

public class DefaultBrowserShortcutPreferenceController extends DefaultAppShortcutPreferenceControllerBase
{
    private static final String KEY = "default_browser";
    
    public DefaultBrowserShortcutPreferenceController(final Context context, final String s) {
        super(context, "default_browser", s);
    }
    
    @Override
    protected boolean hasAppCapability() {
        return DefaultBrowserPreferenceController.hasBrowserPreference(this.mPackageName, this.mContext);
    }
    
    @Override
    protected boolean isDefaultApp() {
        return new DefaultBrowserPreferenceController(this.mContext).isBrowserDefault(this.mPackageName, UserHandle.myUserId());
    }
}
