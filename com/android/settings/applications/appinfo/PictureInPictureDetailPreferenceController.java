package com.android.settings.applications.appinfo;

import android.support.v7.preference.Preference;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.UserHandle;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;
import android.content.pm.PackageManager;

public class PictureInPictureDetailPreferenceController extends AppInfoPreferenceControllerBase
{
    private static final String TAG = "PicInPicDetailControl";
    private final PackageManager mPackageManager;
    private String mPackageName;
    
    public PictureInPictureDetailPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mPackageManager = context.getPackageManager();
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.hasPictureInPictureActivites()) {
            n = 0;
        }
        else {
            n = 3;
        }
        return n;
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return PictureInPictureDetails.class;
    }
    
    int getPreferenceSummary() {
        return PictureInPictureDetails.getPreferenceSummary(this.mContext, this.mParent.getPackageInfo().applicationInfo.uid, this.mPackageName);
    }
    
    boolean hasPictureInPictureActivites() {
        PackageInfo packageInfoAsUser = null;
        boolean b = true;
        try {
            packageInfoAsUser = this.mPackageManager.getPackageInfoAsUser(this.mPackageName, 1, UserHandle.myUserId());
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception while retrieving the package info of ");
            sb.append(this.mPackageName);
            Log.e("PicInPicDetailControl", sb.toString(), (Throwable)ex);
        }
        if (packageInfoAsUser == null || !PictureInPictureSettings.checkPackageHasPictureInPictureActivities(packageInfoAsUser.packageName, packageInfoAsUser.activities)) {
            b = false;
        }
        return b;
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(this.getPreferenceSummary());
    }
}
