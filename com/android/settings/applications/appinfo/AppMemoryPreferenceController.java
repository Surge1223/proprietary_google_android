package com.android.settings.applications.appinfo;

import android.text.format.Formatter;
import java.util.Iterator;
import android.content.pm.PackageInfo;
import android.app.Activity;
import com.android.settings.applications.ProcStatsEntry;
import android.os.AsyncTask;
import com.android.settings.applications.ProcessStatsBase;
import com.android.settings.SettingsActivity;
import com.android.settingslib.development.DevelopmentSettingsEnabler;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.applications.ProcStatsData;
import com.android.settings.applications.ProcStatsPackageEntry;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class AppMemoryPreferenceController extends BasePreferenceController implements LifecycleObserver, OnResume
{
    private static final String KEY_MEMORY = "memory";
    private final AppInfoDashboardFragment mParent;
    private Preference mPreference;
    private ProcStatsPackageEntry mStats;
    private ProcStatsData mStatsManager;
    
    public AppMemoryPreferenceController(final Context context, final AppInfoDashboardFragment mParent, final Lifecycle lifecycle) {
        super(context, "memory");
        this.mParent = mParent;
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (!this.mContext.getResources().getBoolean(2131034134)) {
            return 2;
        }
        return DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(this.mContext) ? 0 : 1;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if ("memory".equals(preference.getKey())) {
            ProcessStatsBase.launchMemoryDetail((SettingsActivity)this.mParent.getActivity(), this.mStatsManager.getMemInfo(), this.mStats, false);
            return true;
        }
        return false;
    }
    
    @Override
    public void onResume() {
        if (this.isAvailable()) {
            new MemoryUpdater().execute((Object[])new Void[0]);
        }
    }
    
    private class MemoryUpdater extends AsyncTask<Void, Void, ProcStatsPackageEntry>
    {
        protected ProcStatsPackageEntry doInBackground(final Void... array) {
            final Activity activity = AppMemoryPreferenceController.this.mParent.getActivity();
            if (activity == null) {
                return null;
            }
            final PackageInfo packageInfo = AppMemoryPreferenceController.this.mParent.getPackageInfo();
            if (packageInfo == null) {
                return null;
            }
            if (AppMemoryPreferenceController.this.mStatsManager == null) {
                AppMemoryPreferenceController.this.mStatsManager = new ProcStatsData((Context)activity, false);
                AppMemoryPreferenceController.this.mStatsManager.setDuration(ProcessStatsBase.sDurations[0]);
            }
            AppMemoryPreferenceController.this.mStatsManager.refreshStats(true);
            for (final ProcStatsPackageEntry procStatsPackageEntry : AppMemoryPreferenceController.this.mStatsManager.getEntries()) {
                final Iterator<ProcStatsEntry> iterator2 = procStatsPackageEntry.getEntries().iterator();
                while (iterator2.hasNext()) {
                    if (iterator2.next().getUid() == packageInfo.applicationInfo.uid) {
                        procStatsPackageEntry.updateMetrics();
                        return procStatsPackageEntry;
                    }
                }
            }
            return null;
        }
        
        protected void onPostExecute(final ProcStatsPackageEntry procStatsPackageEntry) {
            if (AppMemoryPreferenceController.this.mParent.getActivity() == null) {
                return;
            }
            if (procStatsPackageEntry != null) {
                AppMemoryPreferenceController.this.mStats = procStatsPackageEntry;
                AppMemoryPreferenceController.this.mPreference.setEnabled(true);
                AppMemoryPreferenceController.this.mPreference.setSummary(AppMemoryPreferenceController.this.mContext.getString(2131888288, new Object[] { Formatter.formatShortFileSize(AppMemoryPreferenceController.this.mContext, (long)(Math.max(procStatsPackageEntry.getRunWeight(), procStatsPackageEntry.getBgWeight()) * AppMemoryPreferenceController.this.mStatsManager.getMemInfo().getWeightToRam())) }));
            }
            else {
                AppMemoryPreferenceController.this.mPreference.setEnabled(false);
                AppMemoryPreferenceController.this.mPreference.setSummary(AppMemoryPreferenceController.this.mContext.getString(2131888419));
            }
        }
    }
}
