package com.android.settings.applications.appinfo;

import android.content.pm.ResolveInfo;
import android.content.ComponentName;
import android.os.RemoteException;
import android.webkit.IWebViewUpdateService$Stub;
import android.os.ServiceManager;
import java.util.List;
import java.util.ArrayList;
import com.android.settingslib.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.os.Handler;
import android.net.Uri;
import com.android.settingslib.applications.AppUtils;
import android.content.pm.PackageInfo;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.overlay.FeatureFactory;
import android.os.UserHandle;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.content.pm.PackageManager;
import java.util.HashSet;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settings.core.BasePreferenceController;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$AppActionButtonPreferenceController$Ww2IUjWxdICZ6sY_1SuD__XEpOY implements View.OnClickListener
{
    public final void onClick(final View view) {
        AppActionButtonPreferenceController.lambda$initUninstallButtons$0(this.f$0, view);
    }
}
