package com.android.settings.applications.appinfo;

import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import com.android.settings.applications.AppInfoWithHeader;

public class PictureInPictureDetails extends AppInfoWithHeader implements OnPreferenceChangeListener
{
    private SwitchPreference mSwitchPref;
    
    static boolean getEnterPipStateForPackage(final Context context, final int n, final String s) {
        return ((AppOpsManager)context.getSystemService((Class)AppOpsManager.class)).checkOpNoThrow(67, n, s) == 0;
    }
    
    public static int getPreferenceSummary(final Context context, int n, final String s) {
        if (getEnterPipStateForPackage(context, n, s)) {
            n = 2131886392;
        }
        else {
            n = 2131886393;
        }
        return n;
    }
    
    static void setEnterPipStateForPackage(final Context context, final int n, final String s, final boolean b) {
        final AppOpsManager appOpsManager = (AppOpsManager)context.getSystemService((Class)AppOpsManager.class);
        int n2;
        if (b) {
            n2 = 0;
        }
        else {
            n2 = 2;
        }
        appOpsManager.setMode(67, n, s, n2);
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 812;
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 813;
        }
        else {
            n = 814;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082803);
        (this.mSwitchPref = (SwitchPreference)this.findPreference("app_ops_settings_switch")).setTitle(2131888569);
        this.mSwitchPref.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mSwitchPref) {
            this.logSpecialPermissionChange((boolean)o, this.mPackageName);
            setEnterPipStateForPackage((Context)this.getActivity(), this.mPackageInfo.applicationInfo.uid, this.mPackageName, (boolean)o);
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean refreshUi() {
        this.mSwitchPref.setChecked(getEnterPipStateForPackage((Context)this.getActivity(), this.mPackageInfo.applicationInfo.uid, this.mPackageName));
        return true;
    }
}
