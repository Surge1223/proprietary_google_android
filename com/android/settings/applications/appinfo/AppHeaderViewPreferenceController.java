package com.android.settings.applications.appinfo;

import android.app.Fragment;
import android.support.v7.preference.PreferenceScreen;
import android.app.Activity;
import com.android.settings.Utils;
import com.android.settingslib.applications.AppUtils;
import com.android.settingslib.applications.ApplicationsState;
import android.content.pm.PackageInfo;
import android.content.Context;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.applications.LayoutPreference;
import com.android.settings.widget.EntityHeaderController;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class AppHeaderViewPreferenceController extends BasePreferenceController implements Callback, LifecycleObserver, OnStart
{
    private static final String KEY_HEADER = "header_view";
    private EntityHeaderController mEntityHeaderController;
    private LayoutPreference mHeader;
    private final Lifecycle mLifecycle;
    private final String mPackageName;
    private final AppInfoDashboardFragment mParent;
    
    public AppHeaderViewPreferenceController(final Context context, final AppInfoDashboardFragment mParent, final String mPackageName, final Lifecycle mLifecycle) {
        super(context, "header_view");
        this.mParent = mParent;
        this.mPackageName = mPackageName;
        this.mLifecycle = mLifecycle;
        if (this.mLifecycle != null) {
            this.mLifecycle.addObserver(this);
        }
    }
    
    private void setAppLabelAndIcon(final PackageInfo packageInfo, final AppEntry appEntry) {
        final Activity activity = this.mParent.getActivity();
        final boolean instant = AppUtils.isInstant(packageInfo.applicationInfo);
        CharSequence string;
        if (instant) {
            string = null;
        }
        else {
            string = this.mContext.getString(Utils.getInstallationStatus(appEntry.info));
        }
        this.mEntityHeaderController.setLabel(appEntry).setIcon(appEntry).setSummary(string).setIsInstantApp(instant).done(activity, false);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mHeader = (LayoutPreference)preferenceScreen.findPreference("header_view");
        this.mEntityHeaderController = EntityHeaderController.newInstance(this.mParent.getActivity(), this.mParent, this.mHeader.findViewById(2131362112)).setPackageName(this.mPackageName).setButtonActions(0, 0).bindHeaderButtons();
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public void onStart() {
        this.mEntityHeaderController.setRecyclerView(this.mParent.getListView(), this.mLifecycle).styleActionBar(this.mParent.getActivity());
    }
    
    @Override
    public void refreshUi() {
        this.setAppLabelAndIcon(this.mParent.getPackageInfo(), this.mParent.getAppEntry());
    }
}
