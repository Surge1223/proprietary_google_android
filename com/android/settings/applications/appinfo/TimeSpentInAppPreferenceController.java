package com.android.settings.applications.appinfo;

import java.util.Iterator;
import java.util.List;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.content.pm.ResolveInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.Intent;
import com.android.settings.core.BasePreferenceController;

public class TimeSpentInAppPreferenceController extends BasePreferenceController
{
    static final Intent SEE_TIME_IN_APP_TEMPLATE;
    private Intent mIntent;
    private final PackageManager mPackageManager;
    private String mPackageName;
    
    static {
        SEE_TIME_IN_APP_TEMPLATE = new Intent("com.android.settings.action.TIME_SPENT_IN_APP");
    }
    
    public TimeSpentInAppPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mPackageManager = context.getPackageManager();
    }
    
    private boolean isSystemApp(final ResolveInfo resolveInfo) {
        boolean b = true;
        if (resolveInfo == null || resolveInfo.activityInfo == null || resolveInfo.activityInfo.applicationInfo == null || (resolveInfo.activityInfo.applicationInfo.flags & 0x1) == 0x0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        if (preference != null) {
            preference.setIntent(this.mIntent);
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (TextUtils.isEmpty((CharSequence)this.mPackageName)) {
            return 2;
        }
        final List queryIntentActivities = this.mPackageManager.queryIntentActivities(this.mIntent, 0);
        if (queryIntentActivities != null && !queryIntentActivities.isEmpty()) {
            final Iterator<ResolveInfo> iterator = queryIntentActivities.iterator();
            while (iterator.hasNext()) {
                if (this.isSystemApp(iterator.next())) {
                    return 0;
                }
            }
            return 2;
        }
        return 2;
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
        this.mIntent = new Intent(TimeSpentInAppPreferenceController.SEE_TIME_IN_APP_TEMPLATE).putExtra("android.intent.extra.PACKAGE_NAME", this.mPackageName);
    }
}
