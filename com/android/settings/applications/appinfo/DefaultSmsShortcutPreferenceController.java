package com.android.settings.applications.appinfo;

import com.android.settings.applications.defaultapps.DefaultSmsPreferenceController;
import android.content.Context;

public class DefaultSmsShortcutPreferenceController extends DefaultAppShortcutPreferenceControllerBase
{
    private static final String KEY = "default_sms_app";
    
    public DefaultSmsShortcutPreferenceController(final Context context, final String s) {
        super(context, "default_sms_app", s);
    }
    
    @Override
    protected boolean hasAppCapability() {
        return DefaultSmsPreferenceController.hasSmsPreference(this.mPackageName, this.mContext);
    }
    
    @Override
    protected boolean isDefaultApp() {
        return DefaultSmsPreferenceController.isSmsDefault(this.mPackageName, this.mContext);
    }
}
