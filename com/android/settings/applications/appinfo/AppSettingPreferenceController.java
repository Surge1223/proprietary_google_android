package com.android.settings.applications.appinfo;

import com.android.settings.overlay.FeatureFactory;
import android.support.v7.preference.Preference;
import android.text.TextUtils;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.Context;

public class AppSettingPreferenceController extends AppInfoPreferenceControllerBase
{
    private String mPackageName;
    
    public AppSettingPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    private Intent resolveIntent(final Intent intent) {
        final ResolveInfo resolveActivity = this.mContext.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity != null) {
            return new Intent(intent.getAction()).setClassName(resolveActivity.activityInfo.packageName, resolveActivity.activityInfo.name);
        }
        return null;
    }
    
    @Override
    public int getAvailabilityStatus() {
        final boolean empty = TextUtils.isEmpty((CharSequence)this.mPackageName);
        int n = 1;
        if (!empty && this.mParent != null) {
            if (this.resolveIntent(new Intent("android.intent.action.APPLICATION_PREFERENCES").setPackage(this.mPackageName)) != null) {
                n = 0;
            }
            return n;
        }
        return 1;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return false;
        }
        final Intent resolveIntent = this.resolveIntent(new Intent("android.intent.action.APPLICATION_PREFERENCES").setPackage(this.mPackageName));
        if (resolveIntent == null) {
            return false;
        }
        FeatureFactory.getFactory(this.mContext).getMetricsFeatureProvider().actionWithSource(this.mContext, this.mParent.getMetricsCategory(), 1017);
        this.mContext.startActivity(resolveIntent);
        return true;
    }
    
    public AppSettingPreferenceController setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
        return this;
    }
}
