package com.android.settings.applications.appinfo;

import android.content.Context;
import com.android.settings.widget.PreferenceCategoryController;

public class AppInstallerPreferenceCategoryController extends PreferenceCategoryController
{
    public AppInstallerPreferenceCategoryController(final Context context, final String s) {
        super(context, s);
    }
}
