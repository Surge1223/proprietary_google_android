package com.android.settings.applications.appinfo;

import android.view.WindowManager.LayoutParams;
import android.view.Window;
import android.app.Activity;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.app.AlertDialog;
import com.android.settings.applications.AppStateBaseBridge;
import com.android.settings.applications.AppStateAppOpsBridge;
import com.android.settingslib.applications.ApplicationsState;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import android.content.Intent;
import com.android.settings.applications.AppStateOverlayBridge;
import android.app.AppOpsManager;
import android.support.v7.preference.Preference;
import com.android.settings.applications.AppInfoWithHeader;

public class DrawOverlayDetails extends AppInfoWithHeader implements OnPreferenceChangeListener, OnPreferenceClickListener
{
    private static final int[] APP_OPS_OP_CODE;
    private AppOpsManager mAppOpsManager;
    private AppStateOverlayBridge mOverlayBridge;
    private AppStateOverlayBridge.OverlayState mOverlayState;
    private Intent mSettingsIntent;
    private SwitchPreference mSwitchPref;
    
    static {
        APP_OPS_OP_CODE = new int[] { 24 };
    }
    
    public static CharSequence getSummary(final Context context, final AppStateOverlayBridge.OverlayState overlayState) {
        int n;
        if (((AppStateAppOpsBridge.PermissionState)overlayState).isPermissible()) {
            n = 2131886392;
        }
        else {
            n = 2131886393;
        }
        return context.getString(n);
    }
    
    public static CharSequence getSummary(final Context context, final AppEntry appEntry) {
        AppStateAppOpsBridge.PermissionState overlayInfo;
        if (appEntry.extraInfo instanceof AppStateOverlayBridge.OverlayState) {
            overlayInfo = (AppStateOverlayBridge.OverlayState)appEntry.extraInfo;
        }
        else if (appEntry.extraInfo instanceof AppStateAppOpsBridge.PermissionState) {
            overlayInfo = new AppStateOverlayBridge.OverlayState((AppStateAppOpsBridge.PermissionState)appEntry.extraInfo);
        }
        else {
            overlayInfo = new AppStateOverlayBridge(context, null, null).getOverlayInfo(appEntry.info.packageName, appEntry.info.uid);
        }
        return getSummary(context, (AppStateOverlayBridge.OverlayState)overlayInfo);
    }
    
    private void setCanDrawOverlay(final boolean b) {
        this.logSpecialPermissionChange(b, this.mPackageName);
        final AppOpsManager mAppOpsManager = this.mAppOpsManager;
        final int uid = this.mPackageInfo.applicationInfo.uid;
        final String mPackageName = this.mPackageName;
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 2;
        }
        mAppOpsManager.setMode(24, uid, mPackageName, n);
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 221;
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 770;
        }
        else {
            n = 771;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mOverlayBridge = new AppStateOverlayBridge((Context)activity, this.mState, null);
        this.mAppOpsManager = (AppOpsManager)((Context)activity).getSystemService("appops");
        this.addPreferencesFromResource(2132082768);
        (this.mSwitchPref = (SwitchPreference)this.findPreference("app_ops_settings_switch")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mSettingsIntent = new Intent("android.intent.action.MAIN").setAction("android.settings.action.MANAGE_OVERLAY_PERMISSION");
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mOverlayBridge.release();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        final Window window = this.getActivity().getWindow();
        final WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.privateFlags &= 0xFFF7FFFF;
        window.setAttributes(attributes);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mSwitchPref) {
            if (this.mOverlayState != null && (boolean)o != ((AppStateAppOpsBridge.PermissionState)this.mOverlayState).isPermissible()) {
                this.setCanDrawOverlay(((AppStateAppOpsBridge.PermissionState)this.mOverlayState).isPermissible() ^ true);
                this.refreshUi();
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.getActivity().getWindow().addPrivateFlags(524288);
    }
    
    @Override
    protected boolean refreshUi() {
        this.mOverlayState = this.mOverlayBridge.getOverlayInfo(this.mPackageName, this.mPackageInfo.applicationInfo.uid);
        this.mSwitchPref.setChecked(((AppStateAppOpsBridge.PermissionState)this.mOverlayState).isPermissible());
        this.mSwitchPref.setEnabled(this.mOverlayState.permissionDeclared && this.mOverlayState.controlEnabled);
        this.mPm.resolveActivityAsUser(this.mSettingsIntent, 128, this.mUserId);
        return true;
    }
}
