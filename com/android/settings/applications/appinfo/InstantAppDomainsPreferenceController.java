package com.android.settings.applications.appinfo;

import java.util.Set;
import android.util.ArraySet;
import com.android.settings.Utils;
import com.android.settings.applications.AppDomainsPreference;
import android.support.v7.preference.Preference;
import com.android.settingslib.applications.AppUtils;
import android.content.Context;
import android.content.pm.PackageManager;

public class InstantAppDomainsPreferenceController extends AppInfoPreferenceControllerBase
{
    private PackageManager mPackageManager;
    
    public InstantAppDomainsPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mPackageManager = this.mContext.getPackageManager();
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (AppUtils.isInstant(this.mParent.getPackageInfo().applicationInfo)) {
            n = 0;
        }
        else {
            n = 3;
        }
        return n;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final AppDomainsPreference appDomainsPreference = (AppDomainsPreference)preference;
        final ArraySet<String> handledDomains = Utils.getHandledDomains(this.mPackageManager, this.mParent.getPackageInfo().packageName);
        final String[] titles = ((Set<String>)handledDomains).toArray(new String[((Set)handledDomains).size()]);
        appDomainsPreference.setTitles(titles);
        appDomainsPreference.setValues(new int[titles.length]);
    }
}
