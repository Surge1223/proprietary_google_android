package com.android.settings.applications.appinfo;

import java.text.Collator;
import android.view.View;
import android.content.pm.PackageManager;
import android.support.v7.preference.PreferenceScreen;
import android.app.Fragment;
import com.android.settings.applications.AppInfoBase;
import android.support.v7.preference.Preference;
import com.android.settings.widget.AppPreference;
import java.util.Comparator;
import java.util.Collections;
import android.os.UserHandle;
import android.os.Bundle;
import java.util.Iterator;
import android.content.pm.PackageInfo;
import android.content.pm.UserInfo;
import android.content.pm.ApplicationInfo;
import android.util.Pair;
import android.content.pm.ActivityInfo;
import java.util.ArrayList;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.util.IconDrawableFactory;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import java.util.List;
import com.android.settings.notification.EmptyTextSettings;

public class PictureInPictureSettings extends EmptyTextSettings
{
    @VisibleForTesting
    static final List<String> IGNORE_PACKAGE_LIST;
    private static final String TAG;
    private Context mContext;
    private IconDrawableFactory mIconDrawableFactory;
    private PackageManagerWrapper mPackageManager;
    private UserManager mUserManager;
    
    static {
        TAG = PictureInPictureSettings.class.getSimpleName();
        (IGNORE_PACKAGE_LIST = new ArrayList<String>()).add("com.android.systemui");
    }
    
    public static boolean checkPackageHasPictureInPictureActivities(final String s, final ActivityInfo[] array) {
        if (PictureInPictureSettings.IGNORE_PACKAGE_LIST.contains(s)) {
            return false;
        }
        if (array != null) {
            for (int i = array.length - 1; i >= 0; --i) {
                if (array[i].supportsPictureInPicture()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    ArrayList<Pair<ApplicationInfo, Integer>> collectPipApps(int intValue) {
        final ArrayList<Pair> list = (ArrayList<Pair>)new ArrayList<Pair<ApplicationInfo, Integer>>();
        final ArrayList<Integer> list2 = new ArrayList<Integer>();
        final Iterator<UserInfo> iterator = this.mUserManager.getProfiles(intValue).iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next().id);
        }
        final Iterator<Integer> iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            intValue = iterator2.next();
            for (final PackageInfo packageInfo : this.mPackageManager.getInstalledPackagesAsUser(1, intValue)) {
                if (checkPackageHasPictureInPictureActivities(packageInfo.packageName, packageInfo.activities)) {
                    list.add(new Pair((Object)packageInfo.applicationInfo, (Object)intValue));
                }
            }
        }
        return (ArrayList<Pair<ApplicationInfo, Integer>>)list;
    }
    
    @Override
    public int getMetricsCategory() {
        return 812;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082804;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Context)this.getActivity();
        this.mPackageManager = new PackageManagerWrapper(this.mContext.getPackageManager());
        this.mUserManager = (UserManager)this.mContext.getSystemService("user");
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(this.mContext);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        final PackageManager packageManager = this.mPackageManager.getPackageManager();
        final ArrayList<Pair<ApplicationInfo, Integer>> collectPipApps = this.collectPipApps(UserHandle.myUserId());
        Collections.sort((List<Object>)collectPipApps, (Comparator<? super Object>)new AppComparator(packageManager));
        final Context prefContext = this.getPrefContext();
        for (final Pair<ApplicationInfo, Integer> pair : collectPipApps) {
            final ApplicationInfo applicationInfo = (ApplicationInfo)pair.first;
            final int intValue = (int)pair.second;
            final UserHandle of = UserHandle.of(intValue);
            final String packageName = applicationInfo.packageName;
            final CharSequence loadLabel = applicationInfo.loadLabel(packageManager);
            final AppPreference appPreference = new AppPreference(prefContext);
            appPreference.setIcon(this.mIconDrawableFactory.getBadgedIcon(applicationInfo, intValue));
            appPreference.setTitle(packageManager.getUserBadgedLabel(loadLabel, of));
            appPreference.setSummary(PictureInPictureDetails.getPreferenceSummary(prefContext, applicationInfo.uid, packageName));
            appPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(final Preference preference) {
                    AppInfoBase.startAppInfoFragment(PictureInPictureDetails.class, 2131888570, packageName, applicationInfo.uid, PictureInPictureSettings.this, -1, PictureInPictureSettings.this.getMetricsCategory());
                    return true;
                }
            });
            preferenceScreen.addPreference(appPreference);
        }
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.setEmptyText(2131888571);
    }
    
    static class AppComparator implements Comparator<Pair<ApplicationInfo, Integer>>
    {
        private final Collator mCollator;
        private final PackageManager mPm;
        
        public AppComparator(final PackageManager mPm) {
            this.mCollator = Collator.getInstance();
            this.mPm = mPm;
        }
        
        @Override
        public final int compare(final Pair<ApplicationInfo, Integer> pair, final Pair<ApplicationInfo, Integer> pair2) {
            CharSequence charSequence;
            if ((charSequence = ((ApplicationInfo)pair.first).loadLabel(this.mPm)) == null) {
                charSequence = ((ApplicationInfo)pair.first).name;
            }
            CharSequence charSequence2;
            if ((charSequence2 = ((ApplicationInfo)pair2.first).loadLabel(this.mPm)) == null) {
                charSequence2 = ((ApplicationInfo)pair2.first).name;
            }
            final int compare = this.mCollator.compare(charSequence.toString(), charSequence2.toString());
            if (compare != 0) {
                return compare;
            }
            return (int)pair.second - (int)pair2.second;
        }
    }
}
