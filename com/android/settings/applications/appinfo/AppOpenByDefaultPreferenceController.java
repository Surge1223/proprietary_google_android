package com.android.settings.applications.appinfo;

import android.content.pm.PackageInfo;
import com.android.settingslib.applications.AppUtils;
import android.support.v7.preference.Preference;
import com.android.settings.applications.AppLaunchSettings;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settingslib.applications.ApplicationsState;
import android.support.v7.preference.PreferenceScreen;
import android.hardware.usb.IUsbManager$Stub;
import android.os.ServiceManager;
import android.content.Context;
import android.hardware.usb.IUsbManager;
import android.content.pm.PackageManager;

public class AppOpenByDefaultPreferenceController extends AppInfoPreferenceControllerBase
{
    private PackageManager mPackageManager;
    private IUsbManager mUsbManager;
    
    public AppOpenByDefaultPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mUsbManager = IUsbManager$Stub.asInterface(ServiceManager.getService("usb"));
        this.mPackageManager = context.getPackageManager();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final AppEntry appEntry = this.mParent.getAppEntry();
        if (appEntry != null && appEntry.info != null) {
            if ((appEntry.info.flags & 0x800000) == 0x0 || !appEntry.info.enabled) {
                this.mPreference.setEnabled(false);
            }
        }
        else {
            this.mPreference.setEnabled(false);
        }
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return AppLaunchSettings.class;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final PackageInfo packageInfo = this.mParent.getPackageInfo();
        if (packageInfo != null && !AppUtils.isInstant(packageInfo.applicationInfo)) {
            preference.setVisible(true);
            preference.setSummary(AppUtils.getLaunchByDefaultSummary(this.mParent.getAppEntry(), this.mUsbManager, this.mPackageManager, this.mContext));
        }
        else {
            preference.setVisible(false);
        }
    }
}
