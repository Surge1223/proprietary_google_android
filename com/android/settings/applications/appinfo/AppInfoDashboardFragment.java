package com.android.settings.applications.appinfo;

import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import java.lang.ref.WeakReference;
import android.os.AsyncTask;
import android.content.IntentFilter;
import android.app.FragmentManager;
import com.android.settingslib.applications.AppUtils;
import android.content.pm.ApplicationInfo;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import java.util.Arrays;
import com.android.settings.DeviceAdminAdd;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.app.Activity;
import java.util.Iterator;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.AlertDialog;
import android.net.Uri;
import android.app.Fragment;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.os.UserHandle;
import android.util.Log;
import android.app.ActivityManager;
import android.util.Pair;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import android.text.TextUtils;
import android.content.Intent;
import android.content.Context;
import java.util.ArrayList;
import android.os.UserManager;
import android.content.pm.PackageManager;
import android.content.BroadcastReceiver;
import android.content.pm.PackageInfo;
import android.app.admin.DevicePolicyManager;
import java.util.List;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.dashboard.DashboardFragment;

public class AppInfoDashboardFragment extends DashboardFragment implements Callbacks
{
    static final int REQUEST_UNINSTALL = 0;
    static final int UNINSTALL_ALL_USERS_MENU = 1;
    static final int UNINSTALL_UPDATES = 2;
    private AppActionButtonPreferenceController mAppActionButtonPreferenceController;
    private AppEntry mAppEntry;
    private RestrictedLockUtils.EnforcedAdmin mAppsControlDisallowedAdmin;
    private boolean mAppsControlDisallowedBySystem;
    private List<Callback> mCallbacks;
    private boolean mDisableAfterUninstall;
    private DevicePolicyManager mDpm;
    private boolean mFinishing;
    private boolean mInitialized;
    private InstantAppButtonsPreferenceController mInstantAppButtonPreferenceController;
    private boolean mListeningToPackageRemove;
    private PackageInfo mPackageInfo;
    private String mPackageName;
    final BroadcastReceiver mPackageRemovedReceiver;
    private PackageManager mPm;
    private Session mSession;
    private boolean mShowUninstalled;
    private ApplicationsState mState;
    private boolean mUpdatedSysApp;
    private int mUserId;
    private UserManager mUserManager;
    
    public AppInfoDashboardFragment() {
        this.mUpdatedSysApp = false;
        this.mCallbacks = new ArrayList<Callback>();
        this.mPackageRemovedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
                if (!AppInfoDashboardFragment.this.mFinishing && (AppInfoDashboardFragment.this.mAppEntry == null || AppInfoDashboardFragment.this.mAppEntry.info == null || TextUtils.equals((CharSequence)AppInfoDashboardFragment.this.mAppEntry.info.packageName, (CharSequence)schemeSpecificPart))) {
                    AppInfoDashboardFragment.this.onPackageRemoved();
                }
            }
        };
    }
    
    private void forceStopPackage(final String s) {
        this.mMetricsFeatureProvider.action(this.getContext(), 807, s, (Pair<Integer, Object>[])new Pair[0]);
        final ActivityManager activityManager = (ActivityManager)this.getActivity().getSystemService("activity");
        final StringBuilder sb = new StringBuilder();
        sb.append("Stopping package ");
        sb.append(s);
        Log.d("AppInfoDashboard", sb.toString());
        activityManager.forceStopPackage(s);
        final int userId = UserHandle.getUserId(this.mAppEntry.info.uid);
        this.mState.invalidatePackage(s, userId);
        final ApplicationsState.AppEntry entry = this.mState.getEntry(s, userId);
        if (entry != null) {
            this.mAppEntry = entry;
        }
        this.mAppActionButtonPreferenceController.checkForceStop(this.mAppEntry, this.mPackageInfo);
    }
    
    private String getPackageName() {
        if (this.mPackageName != null) {
            return this.mPackageName;
        }
        final Bundle arguments = this.getArguments();
        String string;
        if (arguments != null) {
            string = arguments.getString("package");
        }
        else {
            string = null;
        }
        this.mPackageName = string;
        if (this.mPackageName == null) {
            Intent intent;
            if (arguments == null) {
                intent = this.getActivity().getIntent();
            }
            else {
                intent = (Intent)arguments.getParcelable("intent");
            }
            if (intent != null) {
                this.mPackageName = intent.getData().getSchemeSpecificPart();
            }
        }
        return this.mPackageName;
    }
    
    private boolean isDisabledUntilUsed() {
        return this.mAppEntry.info.enabledSetting == 4;
    }
    
    private boolean isSingleUser() {
        final int userCount = this.mUserManager.getUserCount();
        boolean b = true;
        if (userCount != 1) {
            final UserManager mUserManager = this.mUserManager;
            b = (UserManager.isSplitSystemUser() && userCount == 2 && b);
        }
        return b;
    }
    
    private void onPackageRemoved() {
        this.getActivity().finishActivity(1);
        this.getActivity().finishAndRemoveTask();
    }
    
    private void setIntentAndFinish(final boolean b, final boolean b2) {
        final Intent intent = new Intent();
        intent.putExtra("chg", b2);
        ((SettingsActivity)this.getActivity()).finishPreferencePanel(-1, intent);
        this.mFinishing = true;
    }
    
    public static void startAppInfoFragment(final Class<?> clazz, final int title, final Bundle bundle, final SettingsPreferenceFragment settingsPreferenceFragment, final AppEntry appEntry) {
        Bundle arguments = bundle;
        if (bundle == null) {
            arguments = new Bundle();
        }
        arguments.putString("package", appEntry.info.packageName);
        arguments.putInt("uid", appEntry.info.uid);
        new SubSettingLauncher(settingsPreferenceFragment.getContext()).setDestination(clazz.getName()).setArguments(arguments).setTitle(title).setResultListener(settingsPreferenceFragment, 1).setSourceMetricsCategory(settingsPreferenceFragment.getMetricsCategory()).launch();
    }
    
    private void stopListeningToPackageRemove() {
        if (!this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = false;
        this.getContext().unregisterReceiver(this.mPackageRemovedReceiver);
    }
    
    private void uninstallPkg(final String s, final boolean b, final boolean mDisableAfterUninstall) {
        this.stopListeningToPackageRemove();
        final StringBuilder sb = new StringBuilder();
        sb.append("package:");
        sb.append(s);
        final Intent intent = new Intent("android.intent.action.UNINSTALL_PACKAGE", Uri.parse(sb.toString()));
        intent.putExtra("android.intent.extra.UNINSTALL_ALL_USERS", b);
        this.mMetricsFeatureProvider.action(this.getContext(), 872, (Pair<Integer, Object>[])new Pair[0]);
        this.startActivityForResult(intent, 0);
        this.mDisableAfterUninstall = mDisableAfterUninstall;
    }
    
    void addToCallbackList(final Callback callback) {
        if (callback != null) {
            this.mCallbacks.add(callback);
        }
    }
    
    AlertDialog createDialog(final int n, final int n2) {
        switch (n) {
            default: {
                return this.mInstantAppButtonPreferenceController.createDialog(n);
            }
            case 3: {
                return new AlertDialog$Builder((Context)this.getActivity()).setMessage(this.getActivity().getText(2131886347)).setPositiveButton(2131886346, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        AppInfoDashboardFragment.this.mMetricsFeatureProvider.action(AppInfoDashboardFragment.this.getContext(), 874, (Pair<Integer, Object>[])new Pair[0]);
                        AppInfoDashboardFragment.this.uninstallPkg(AppInfoDashboardFragment.this.mAppEntry.info.packageName, false, true);
                    }
                }).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
            case 2: {
                return new AlertDialog$Builder((Context)this.getActivity()).setMessage(this.getActivity().getText(2131886347)).setPositiveButton(2131886346, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        AppInfoDashboardFragment.this.mMetricsFeatureProvider.action(AppInfoDashboardFragment.this.getContext(), 874, (Pair<Integer, Object>[])new Pair[0]);
                        new DisableChanger(AppInfoDashboardFragment.this, AppInfoDashboardFragment.this.mAppEntry.info, 3).execute(new Object[] { null });
                    }
                }).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
            case 1: {
                return new AlertDialog$Builder((Context)this.getActivity()).setTitle(this.getActivity().getText(2131887696)).setMessage(this.getActivity().getText(2131887695)).setPositiveButton(2131887460, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        AppInfoDashboardFragment.this.forceStopPackage(AppInfoDashboardFragment.this.mAppEntry.info.packageName);
                    }
                }).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
        }
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        this.retrieveAppEntry();
        if (this.mPackageInfo == null) {
            return null;
        }
        final String packageName = this.getPackageName();
        final ArrayList<AppHeaderViewPreferenceController> list = new ArrayList<AppHeaderViewPreferenceController>();
        final Lifecycle lifecycle = this.getLifecycle();
        list.add(new AppHeaderViewPreferenceController(context, this, packageName, lifecycle));
        list.add(this.mAppActionButtonPreferenceController = new AppActionButtonPreferenceController(context, this, packageName));
        final Iterator<AbstractPreferenceController> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.mCallbacks.add((Callback)iterator.next());
        }
        list.add(this.mInstantAppButtonPreferenceController = new InstantAppButtonsPreferenceController(context, this, packageName, lifecycle));
        list.add(new AppBatteryPreferenceController(context, this, packageName, lifecycle));
        list.add(new AppMemoryPreferenceController(context, this, lifecycle));
        list.add(new DefaultHomeShortcutPreferenceController(context, packageName));
        list.add(new DefaultBrowserShortcutPreferenceController(context, packageName));
        list.add(new DefaultPhoneShortcutPreferenceController(context, packageName));
        list.add(new DefaultEmergencyShortcutPreferenceController(context, packageName));
        list.add(new DefaultSmsShortcutPreferenceController(context, packageName));
        return (List<AbstractPreferenceController>)list;
    }
    
    boolean ensurePackageInfoAvailable(final Activity activity) {
        if (this.mPackageInfo == null) {
            this.mFinishing = true;
            Log.w("AppInfoDashboard", "Package info not available. Is this package already uninstalled?");
            activity.finishAndRemoveTask();
            return false;
        }
        return true;
    }
    
    AppEntry getAppEntry() {
        return this.mAppEntry;
    }
    
    @Override
    protected String getLogTag() {
        return "AppInfoDashboard";
    }
    
    @Override
    public int getMetricsCategory() {
        return 20;
    }
    
    int getNumberOfUserWithPackageInstalled(final String s) {
        final List users = this.mUserManager.getUsers(true);
        int n = 0;
        for (final UserInfo userInfo : users) {
            try {
                final int flags = this.mPm.getApplicationInfoAsUser(s, 128, userInfo.id).flags;
                int n2 = n;
                if ((flags & 0x800000) != 0x0) {
                    n2 = n + 1;
                }
                n = n2;
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Package: ");
                sb.append(s);
                sb.append(" not found for user: ");
                sb.append(userInfo.id);
                Log.e("AppInfoDashboard", sb.toString());
            }
        }
        return n;
    }
    
    PackageInfo getPackageInfo() {
        return this.mPackageInfo;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082709;
    }
    
    void handleForceStopButtonClick() {
        if (this.mAppEntry == null) {
            this.setIntentAndFinish(true, true);
            return;
        }
        if (this.mAppsControlDisallowedAdmin != null && !this.mAppsControlDisallowedBySystem) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this.getActivity(), this.mAppsControlDisallowedAdmin);
        }
        else {
            this.showDialogInner(1, 0);
        }
    }
    
    void handleUninstallButtonClick() {
        if (this.mAppEntry == null) {
            this.setIntentAndFinish(true, true);
            return;
        }
        final String packageName = this.mAppEntry.info.packageName;
        if (this.mDpm.packageHasActiveAdmins(this.mPackageInfo.packageName)) {
            this.stopListeningToPackageRemove();
            final Activity activity = this.getActivity();
            final Intent intent = new Intent((Context)activity, (Class)DeviceAdminAdd.class);
            intent.putExtra("android.app.extra.DEVICE_ADMIN_PACKAGE_NAME", this.mPackageName);
            this.mMetricsFeatureProvider.action((Context)activity, 873, (Pair<Integer, Object>[])new Pair[0]);
            activity.startActivityForResult(intent, 1);
            return;
        }
        final RestrictedLockUtils.EnforcedAdmin checkIfUninstallBlocked = RestrictedLockUtils.checkIfUninstallBlocked((Context)this.getActivity(), packageName, this.mUserId);
        final boolean b = this.mAppsControlDisallowedBySystem || RestrictedLockUtils.hasBaseUserRestriction((Context)this.getActivity(), packageName, this.mUserId);
        if (checkIfUninstallBlocked != null && !b) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this.getActivity(), checkIfUninstallBlocked);
        }
        else if ((this.mAppEntry.info.flags & 0x1) != 0x0) {
            if (this.mAppEntry.info.enabled && !this.isDisabledUntilUsed()) {
                if (this.mUpdatedSysApp && this.isSingleUser()) {
                    this.showDialogInner(3, 0);
                }
                else {
                    this.showDialogInner(2, 0);
                }
            }
            else {
                this.mMetricsFeatureProvider.action((Context)this.getActivity(), 875, (Pair<Integer, Object>[])new Pair[0]);
                new DisableChanger(this, this.mAppEntry.info, 1).execute(new Object[] { null });
            }
        }
        else if ((this.mAppEntry.info.flags & 0x800000) == 0x0) {
            this.uninstallPkg(packageName, true, false);
        }
        else {
            this.uninstallPkg(packageName, false, false);
        }
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        switch (n) {
            case 1: {
                if (!this.refreshUi()) {
                    this.setIntentAndFinish(true, true);
                    break;
                }
                this.startListeningToPackageRemove();
                break;
            }
            case 0: {
                this.getActivity().invalidateOptionsMenu();
                if (this.mDisableAfterUninstall) {
                    this.mDisableAfterUninstall = false;
                    new DisableChanger(this, this.mAppEntry.info, 3).execute(new Object[] { null });
                }
                if (!this.refreshUi()) {
                    this.onPackageRemoved();
                    break;
                }
                this.startListeningToPackageRemove();
                break;
            }
        }
    }
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        final String packageName = this.getPackageName();
        this.use(TimeSpentInAppPreferenceController.class).setPackageName(packageName);
        this.use(AppDataUsagePreferenceController.class).setParentFragment(this);
        final AppInstallerInfoPreferenceController appInstallerInfoPreferenceController = this.use(AppInstallerInfoPreferenceController.class);
        appInstallerInfoPreferenceController.setPackageName(packageName);
        appInstallerInfoPreferenceController.setParentFragment(this);
        this.use(AppInstallerPreferenceCategoryController.class).setChildren(Arrays.asList(appInstallerInfoPreferenceController));
        this.use(AppNotificationPreferenceController.class).setParentFragment(this);
        this.use(AppOpenByDefaultPreferenceController.class).setParentFragment(this);
        this.use(AppPermissionPreferenceController.class).setParentFragment(this);
        this.use(AppPermissionPreferenceController.class).setPackageName(packageName);
        this.use(AppSettingPreferenceController.class).setPackageName(packageName).setParentFragment(this);
        this.use(AppStoragePreferenceController.class).setParentFragment(this);
        this.use(AppVersionPreferenceController.class).setParentFragment(this);
        this.use(InstantAppDomainsPreferenceController.class).setParentFragment(this);
        final WriteSystemSettingsPreferenceController writeSystemSettingsPreferenceController = this.use(WriteSystemSettingsPreferenceController.class);
        writeSystemSettingsPreferenceController.setParentFragment(this);
        final DrawOverlayDetailPreferenceController drawOverlayDetailPreferenceController = this.use(DrawOverlayDetailPreferenceController.class);
        drawOverlayDetailPreferenceController.setParentFragment(this);
        final PictureInPictureDetailPreferenceController pictureInPictureDetailPreferenceController = this.use(PictureInPictureDetailPreferenceController.class);
        pictureInPictureDetailPreferenceController.setPackageName(packageName);
        pictureInPictureDetailPreferenceController.setParentFragment(this);
        final ExternalSourceDetailPreferenceController externalSourceDetailPreferenceController = this.use(ExternalSourceDetailPreferenceController.class);
        externalSourceDetailPreferenceController.setPackageName(packageName);
        externalSourceDetailPreferenceController.setParentFragment(this);
        this.use(AdvancedAppInfoPreferenceCategoryController.class).setChildren(Arrays.asList(writeSystemSettingsPreferenceController, drawOverlayDetailPreferenceController, pictureInPictureDetailPreferenceController, externalSourceDetailPreferenceController));
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFinishing = false;
        final Activity activity = this.getActivity();
        this.mDpm = (DevicePolicyManager)activity.getSystemService("device_policy");
        this.mUserManager = (UserManager)activity.getSystemService("user");
        this.mPm = activity.getPackageManager();
        if (!this.ensurePackageInfoAvailable(activity)) {
            return;
        }
        this.startListeningToPackageRemove();
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menu.add(0, 2, 0, 2131886351).setShowAsAction(0);
        menu.add(0, 1, 1, 2131889542).setShowAsAction(0);
    }
    
    @Override
    public void onDestroy() {
        this.stopListeningToPackageRemove();
        super.onDestroy();
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2: {
                this.uninstallPkg(this.mAppEntry.info.packageName, false, false);
                return true;
            }
            case 1: {
                this.uninstallPkg(this.mAppEntry.info.packageName, true, false);
                return true;
            }
        }
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
        if (!this.refreshUi()) {
            this.setIntentAndFinish(true, true);
        }
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
        if (!TextUtils.equals((CharSequence)s, (CharSequence)this.mPackageName)) {
            Log.d("AppInfoDashboard", "Package change irrelevant, skipping");
            return;
        }
        this.refreshUi();
    }
    
    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        if (this.mFinishing) {
            return;
        }
        super.onPrepareOptionsMenu(menu);
        final boolean b = true;
        menu.findItem(1).setVisible(this.shouldShowUninstallForAll(this.mAppEntry));
        this.mUpdatedSysApp = ((this.mAppEntry.info.flags & 0x80) != 0x0);
        final MenuItem item = menu.findItem(2);
        final boolean boolean1 = this.getContext().getResources().getBoolean(2131034118);
        item.setVisible(this.mUpdatedSysApp && !this.mAppsControlDisallowedBySystem && !boolean1 && b);
        if (item.isVisible()) {
            RestrictedLockUtils.setMenuItemAsDisabledByAdmin((Context)this.getActivity(), item, this.mAppsControlDisallowedAdmin);
        }
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final Activity activity = this.getActivity();
        this.mAppsControlDisallowedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced((Context)activity, "no_control_apps", this.mUserId);
        this.mAppsControlDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction((Context)activity, "no_control_apps", this.mUserId);
        if (!this.refreshUi()) {
            this.setIntentAndFinish(true, true);
        }
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    boolean refreshUi() {
        this.retrieveAppEntry();
        final AppEntry mAppEntry = this.mAppEntry;
        final boolean b = false;
        boolean mShowUninstalled = false;
        if (mAppEntry == null) {
            return false;
        }
        if (this.mPackageInfo == null) {
            return false;
        }
        this.mState.ensureIcon(this.mAppEntry);
        final Iterator<Callback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().refreshUi();
        }
        if (!this.mInitialized) {
            this.mInitialized = true;
            if ((this.mAppEntry.info.flags & 0x800000) == 0x0) {
                mShowUninstalled = true;
            }
            this.mShowUninstalled = mShowUninstalled;
            return true;
        }
        try {
            final ApplicationInfo applicationInfo = this.getActivity().getPackageManager().getApplicationInfo(this.mAppEntry.info.packageName, 4194816);
            if (!this.mShowUninstalled) {
                final int flags = applicationInfo.flags;
                boolean b2 = b;
                if ((0x800000 & flags) != 0x0) {
                    b2 = true;
                }
                return b2;
            }
            return true;
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    void retrieveAppEntry() {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        if (this.mState == null) {
            this.mState = ApplicationsState.getInstance(activity.getApplication());
            this.mSession = this.mState.newSession((ApplicationsState.Callbacks)this, this.getLifecycle());
        }
        this.mUserId = UserHandle.myUserId();
        this.mAppEntry = this.mState.getEntry(this.getPackageName(), UserHandle.myUserId());
        if (this.mAppEntry != null) {
            try {
                this.mPackageInfo = activity.getPackageManager().getPackageInfo(this.mAppEntry.info.packageName, 4198976);
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Exception when retrieving package:");
                sb.append(this.mAppEntry.info.packageName);
                Log.e("AppInfoDashboard", sb.toString(), (Throwable)ex);
            }
        }
        else {
            Log.w("AppInfoDashboard", "Missing AppEntry; maybe reinstalling?");
            this.mPackageInfo = null;
        }
    }
    
    boolean shouldShowUninstallForAll(final AppEntry appEntry) {
        boolean b = true;
        if (this.mUpdatedSysApp) {
            b = false;
        }
        else if (appEntry == null) {
            b = false;
        }
        else if ((appEntry.info.flags & 0x1) != 0x0) {
            b = false;
        }
        else if (this.mPackageInfo != null && !this.mDpm.packageHasActiveAdmins(this.mPackageInfo.packageName)) {
            if (UserHandle.myUserId() != 0) {
                b = false;
            }
            else if (this.mUserManager.getUsers().size() < 2) {
                b = false;
            }
            else if (this.getNumberOfUserWithPackageInstalled(this.mPackageName) < 2 && (appEntry.info.flags & 0x800000) != 0x0) {
                b = false;
            }
            else if (AppUtils.isInstant(appEntry.info)) {
                b = false;
            }
        }
        else {
            b = false;
        }
        return b;
    }
    
    void showDialogInner(final int n, final int n2) {
        final MyAlertDialogFragment instance = MyAlertDialogFragment.newInstance(n, n2);
        instance.setTargetFragment((Fragment)this, 0);
        final FragmentManager fragmentManager = this.getFragmentManager();
        final StringBuilder sb = new StringBuilder();
        sb.append("dialog ");
        sb.append(n);
        instance.show(fragmentManager, sb.toString());
    }
    
    void startListeningToPackageRemove() {
        if (this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = true;
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.getContext().registerReceiver(this.mPackageRemovedReceiver, intentFilter);
    }
    
    public interface Callback
    {
        void refreshUi();
    }
    
    private static class DisableChanger extends AsyncTask<Object, Object, Object>
    {
        final WeakReference<AppInfoDashboardFragment> mActivity;
        final ApplicationInfo mInfo;
        final PackageManager mPm;
        final int mState;
        
        DisableChanger(final AppInfoDashboardFragment appInfoDashboardFragment, final ApplicationInfo mInfo, final int mState) {
            this.mPm = appInfoDashboardFragment.mPm;
            this.mActivity = new WeakReference<AppInfoDashboardFragment>(appInfoDashboardFragment);
            this.mInfo = mInfo;
            this.mState = mState;
        }
        
        protected Object doInBackground(final Object... array) {
            this.mPm.setApplicationEnabledSetting(this.mInfo.packageName, this.mState, 0);
            return null;
        }
    }
    
    public static class MyAlertDialogFragment extends InstrumentedDialogFragment
    {
        public static MyAlertDialogFragment newInstance(final int n, final int n2) {
            final MyAlertDialogFragment myAlertDialogFragment = new MyAlertDialogFragment();
            final Bundle arguments = new Bundle();
            arguments.putInt("id", n);
            arguments.putInt("moveError", n2);
            myAlertDialogFragment.setArguments(arguments);
            return myAlertDialogFragment;
        }
        
        @Override
        public int getMetricsCategory() {
            return 558;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final int int1 = this.getArguments().getInt("id");
            final AlertDialog dialog = ((AppInfoDashboardFragment)this.getTargetFragment()).createDialog(int1, this.getArguments().getInt("moveError"));
            if (dialog != null) {
                return (Dialog)dialog;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("unknown id ");
            sb.append(int1);
            throw new IllegalArgumentException(sb.toString());
        }
    }
}
