package com.android.settings.applications.appinfo;

import android.content.pm.ResolveInfo;
import android.content.ComponentName;
import android.os.RemoteException;
import android.webkit.IWebViewUpdateService$Stub;
import android.os.ServiceManager;
import java.util.List;
import java.util.ArrayList;
import com.android.settingslib.Utils;
import android.support.v7.preference.PreferenceScreen;
import android.os.Bundle;
import android.os.Handler;
import android.net.Uri;
import com.android.settingslib.applications.AppUtils;
import android.content.pm.PackageInfo;
import com.android.settingslib.applications.ApplicationsState;
import android.view.View.OnClickListener;
import com.android.settingslib.RestrictedLockUtils;
import android.view.View;
import com.android.settings.overlay.FeatureFactory;
import android.os.UserHandle;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.content.pm.PackageManager;
import java.util.HashSet;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settings.core.BasePreferenceController;

public class AppActionButtonPreferenceController extends BasePreferenceController implements Callback
{
    private static final String KEY_ACTION_BUTTONS = "action_buttons";
    private static final String TAG = "AppActionButtonControl";
    ActionButtonPreference mActionButtons;
    private final ApplicationFeatureProvider mApplicationFeatureProvider;
    private final BroadcastReceiver mCheckKillProcessesReceiver;
    private DevicePolicyManager mDpm;
    private final HashSet<String> mHomePackages;
    private final String mPackageName;
    private final AppInfoDashboardFragment mParent;
    private PackageManager mPm;
    private int mUserId;
    private UserManager mUserManager;
    
    public AppActionButtonPreferenceController(final Context context, final AppInfoDashboardFragment mParent, final String mPackageName) {
        super(context, "action_buttons");
        this.mHomePackages = new HashSet<String>();
        this.mCheckKillProcessesReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final boolean b = this.getResultCode() != 0;
                final StringBuilder sb = new StringBuilder();
                sb.append("Got broadcast response: Restart status for ");
                sb.append(AppActionButtonPreferenceController.this.mParent.getAppEntry().info.packageName);
                sb.append(" ");
                sb.append(b);
                Log.d("AppActionButtonControl", sb.toString());
                AppActionButtonPreferenceController.this.updateForceStopButton(b);
            }
        };
        this.mParent = mParent;
        this.mPackageName = mPackageName;
        this.mUserId = UserHandle.myUserId();
        this.mApplicationFeatureProvider = FeatureFactory.getFactory(context).getApplicationFeatureProvider(context);
    }
    
    private boolean signaturesMatch(final String s, final String s2) {
        boolean b = false;
        if (s != null && s2 != null) {
            try {
                if (this.mPm.checkSignatures(s, s2) >= 0) {
                    b = true;
                }
                return b;
            }
            catch (Exception ex) {}
        }
        return false;
    }
    
    private void updateForceStopButton(boolean button2Enabled) {
        final boolean hasBaseUserRestriction = RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_control_apps", this.mUserId);
        final ActionButtonPreference mActionButtons = this.mActionButtons;
        if (hasBaseUserRestriction) {
            button2Enabled = false;
        }
        final ActionButtonPreference setButton2Enabled = mActionButtons.setButton2Enabled(button2Enabled);
        Object button2OnClickListener;
        if (hasBaseUserRestriction) {
            button2OnClickListener = null;
        }
        else {
            button2OnClickListener = new _$$Lambda$AppActionButtonPreferenceController$oIXjjHquqzr1XuPAGEk55khGTJ0(this);
        }
        setButton2Enabled.setButton2OnClickListener((View.OnClickListener)button2OnClickListener);
    }
    
    void checkForceStop(final AppEntry appEntry, final PackageInfo packageInfo) {
        if (this.mDpm.packageHasActiveAdmins(packageInfo.packageName)) {
            Log.w("AppActionButtonControl", "User can't force stop device admin");
            this.updateForceStopButton(false);
        }
        else if (this.mPm.isPackageStateProtected(packageInfo.packageName, UserHandle.getUserId(appEntry.info.uid))) {
            Log.w("AppActionButtonControl", "User can't force stop protected packages");
            this.updateForceStopButton(false);
        }
        else if (AppUtils.isInstant(packageInfo.applicationInfo)) {
            this.updateForceStopButton(false);
            this.mActionButtons.setButton2Visible(false);
        }
        else if ((appEntry.info.flags & 0x200000) == 0x0) {
            Log.w("AppActionButtonControl", "App is not explicitly stopped");
            this.updateForceStopButton(true);
        }
        else {
            final Intent intent = new Intent("android.intent.action.QUERY_PACKAGE_RESTART", Uri.fromParts("package", appEntry.info.packageName, (String)null));
            intent.putExtra("android.intent.extra.PACKAGES", new String[] { appEntry.info.packageName });
            intent.putExtra("android.intent.extra.UID", appEntry.info.uid);
            intent.putExtra("android.intent.extra.user_handle", UserHandle.getUserId(appEntry.info.uid));
            final StringBuilder sb = new StringBuilder();
            sb.append("Sending broadcast to query restart status for ");
            sb.append(appEntry.info.packageName);
            Log.d("AppActionButtonControl", sb.toString());
            this.mContext.sendOrderedBroadcastAsUser(intent, UserHandle.CURRENT, (String)null, this.mCheckKillProcessesReceiver, (Handler)null, 0, (String)null, (Bundle)null);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mActionButtons = ((ActionButtonPreference)preferenceScreen.findPreference("action_buttons")).setButton2Text(2131887694).setButton2Positive(false).setButton2Enabled(false);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (AppUtils.isInstant(this.mParent.getPackageInfo().applicationInfo)) {
            n = 3;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    boolean handleDisableable(final AppEntry appEntry, final PackageInfo packageInfo) {
        boolean b = false;
        if (!this.mHomePackages.contains(appEntry.info.packageName) && !Utils.isSystemPackage(this.mContext.getResources(), this.mPm, packageInfo)) {
            if (appEntry.info.enabled && appEntry.info.enabledSetting != 4) {
                this.mActionButtons.setButton1Text(2131887421).setButton1Positive(false);
                b = (this.mApplicationFeatureProvider.getKeepEnabledPackages().contains(appEntry.info.packageName) ^ true);
            }
            else {
                this.mActionButtons.setButton1Text(2131887538).setButton1Positive(true);
                b = true;
            }
        }
        else {
            this.mActionButtons.setButton1Text(2131887421).setButton1Positive(false);
        }
        return b;
    }
    
    boolean initUninstallButtonForUserApp() {
        boolean b = true;
        final PackageInfo packageInfo = this.mParent.getPackageInfo();
        if ((packageInfo.applicationInfo.flags & 0x800000) == 0x0 && this.mUserManager.getUsers().size() >= 2) {
            b = false;
        }
        else if (AppUtils.isInstant(packageInfo.applicationInfo)) {
            b = false;
            this.mActionButtons.setButton1Visible(false);
        }
        this.mActionButtons.setButton1Text(2131889544).setButton1Positive(false);
        return b;
    }
    
    void initUninstallButtons(final AppEntry appEntry, final PackageInfo packageInfo) {
        final int flags = appEntry.info.flags;
        final boolean b = true;
        final boolean b2 = (flags & 0x1) != 0x0;
        boolean b3;
        if (b2) {
            b3 = this.handleDisableable(appEntry, packageInfo);
        }
        else {
            b3 = this.initUninstallButtonForUserApp();
        }
        boolean b4 = b3;
        if (b2) {
            b4 = b3;
            if (this.mDpm.packageHasActiveAdmins(packageInfo.packageName)) {
                b4 = false;
            }
        }
        if (com.android.settings.Utils.isProfileOrDeviceOwner(this.mUserManager, this.mDpm, packageInfo.packageName)) {
            b4 = false;
        }
        boolean b5 = b4;
        if (Utils.isDeviceProvisioningPackage(this.mContext.getResources(), appEntry.info.packageName)) {
            b5 = false;
        }
        if (this.mDpm.isUninstallInQueue(this.mPackageName)) {
            b5 = false;
        }
        boolean button1Enabled = b5;
        if (b5) {
            button1Enabled = b5;
            if (this.mHomePackages.contains(packageInfo.packageName)) {
                if (b2) {
                    button1Enabled = false;
                }
                else {
                    final ComponentName homeActivities = this.mPm.getHomeActivities((List)new ArrayList());
                    if (homeActivities == null) {
                        button1Enabled = (this.mHomePackages.size() > 1 && b);
                    }
                    else {
                        button1Enabled = (true ^ packageInfo.packageName.equals(homeActivities.getPackageName()));
                    }
                }
            }
        }
        if (RestrictedLockUtils.hasBaseUserRestriction(this.mContext, "no_control_apps", this.mUserId)) {
            button1Enabled = false;
        }
        try {
            if (IWebViewUpdateService$Stub.asInterface(ServiceManager.getService("webviewupdate")).isFallbackPackage(appEntry.info.packageName)) {
                button1Enabled = false;
            }
            this.mActionButtons.setButton1Enabled(button1Enabled);
            if (button1Enabled) {
                this.mActionButtons.setButton1OnClickListener((View.OnClickListener)new _$$Lambda$AppActionButtonPreferenceController$Ww2IUjWxdICZ6sY_1SuD__XEpOY(this));
            }
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    @Override
    public void refreshUi() {
        if (this.mPm == null) {
            this.mPm = this.mContext.getPackageManager();
        }
        if (this.mDpm == null) {
            this.mDpm = (DevicePolicyManager)this.mContext.getSystemService("device_policy");
        }
        if (this.mUserManager == null) {
            this.mUserManager = (UserManager)this.mContext.getSystemService("user");
        }
        final AppEntry appEntry = this.mParent.getAppEntry();
        final PackageInfo packageInfo = this.mParent.getPackageInfo();
        final ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>)new ArrayList<Object>();
        this.mPm.getHomeActivities((List)list);
        this.mHomePackages.clear();
        for (int i = 0; i < list.size(); ++i) {
            final ResolveInfo resolveInfo = list.get(i);
            final String packageName = resolveInfo.activityInfo.packageName;
            this.mHomePackages.add(packageName);
            final Bundle metaData = resolveInfo.activityInfo.metaData;
            if (metaData != null) {
                final String string = metaData.getString("android.app.home.alternate");
                if (this.signaturesMatch(string, packageName)) {
                    this.mHomePackages.add(string);
                }
            }
        }
        this.checkForceStop(appEntry, packageInfo);
        this.initUninstallButtons(appEntry, packageInfo);
    }
}
