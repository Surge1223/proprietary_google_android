package com.android.settings.applications.appinfo;

import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.applications.defaultapps.DefaultHomePreferenceController;
import android.content.Context;

public class DefaultHomeShortcutPreferenceController extends DefaultAppShortcutPreferenceControllerBase
{
    private static final String KEY = "default_home";
    
    public DefaultHomeShortcutPreferenceController(final Context context, final String s) {
        super(context, "default_home", s);
    }
    
    @Override
    protected boolean hasAppCapability() {
        return DefaultHomePreferenceController.hasHomePreference(this.mPackageName, this.mContext);
    }
    
    @Override
    protected boolean isDefaultApp() {
        return DefaultHomePreferenceController.isHomeDefault(this.mPackageName, new PackageManagerWrapper(this.mContext.getPackageManager()));
    }
}
