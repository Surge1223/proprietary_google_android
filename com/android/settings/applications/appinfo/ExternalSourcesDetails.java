package com.android.settings.applications.appinfo;

import com.android.settings.Settings;
import android.app.Activity;
import android.os.Bundle;
import android.app.AlertDialog;
import com.android.settings.applications.AppStateBaseBridge;
import android.os.UserHandle;
import com.android.settingslib.applications.ApplicationsState;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.RestrictedSwitchPreference;
import android.app.AppOpsManager;
import com.android.settings.applications.AppStateInstallAppsBridge;
import android.support.v7.preference.Preference;
import com.android.settings.applications.AppInfoWithHeader;

public class ExternalSourcesDetails extends AppInfoWithHeader implements OnPreferenceChangeListener
{
    private AppStateInstallAppsBridge mAppBridge;
    private AppOpsManager mAppOpsManager;
    private AppStateInstallAppsBridge.InstallAppsState mInstallAppsState;
    private RestrictedSwitchPreference mSwitchPref;
    private UserManager mUserManager;
    
    public static CharSequence getPreferenceSummary(final Context context, final AppEntry appEntry) {
        final int userRestrictionSource = UserManager.get(context).getUserRestrictionSource("no_install_unknown_sources", UserHandle.getUserHandleForUid(appEntry.info.uid));
        if (userRestrictionSource != 4) {
            switch (userRestrictionSource) {
                default: {
                    int n;
                    if (new AppStateInstallAppsBridge(context, null, null).createInstallAppsStateFor(appEntry.info.packageName, appEntry.info.uid).canInstallApps()) {
                        n = 2131886392;
                    }
                    else {
                        n = 2131886393;
                    }
                    return context.getString(n);
                }
                case 1: {
                    return context.getString(2131887422);
                }
                case 2: {
                    break;
                }
            }
        }
        return context.getString(R.string.disabled_by_admin);
    }
    
    private void setCanInstallApps(final boolean b) {
        final AppOpsManager mAppOpsManager = this.mAppOpsManager;
        final int uid = this.mPackageInfo.applicationInfo.uid;
        final String mPackageName = this.mPackageName;
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 2;
        }
        mAppOpsManager.setMode(66, uid, mPackageName, n);
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 808;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mAppBridge = new AppStateInstallAppsBridge((Context)activity, this.mState, null);
        this.mAppOpsManager = (AppOpsManager)((Context)activity).getSystemService("appops");
        this.mUserManager = UserManager.get((Context)activity);
        this.addPreferencesFromResource(2132082773);
        (this.mSwitchPref = (RestrictedSwitchPreference)this.findPreference("external_sources_settings_switch")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean booleanValue = (boolean)o;
        final RestrictedSwitchPreference mSwitchPref = this.mSwitchPref;
        int result = 0;
        if (preference == mSwitchPref) {
            if (this.mInstallAppsState != null && booleanValue != this.mInstallAppsState.canInstallApps()) {
                if (Settings.ManageAppExternalSourcesActivity.class.getName().equals(this.getIntent().getComponent().getClassName())) {
                    if (booleanValue) {
                        result = -1;
                    }
                    this.setResult(result);
                }
                this.setCanInstallApps(booleanValue);
                this.refreshUi();
            }
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean refreshUi() {
        if (this.mPackageInfo == null || this.mPackageInfo.applicationInfo == null) {
            return false;
        }
        if (this.mUserManager.hasBaseUserRestriction("no_install_unknown_sources", UserHandle.of(UserHandle.myUserId()))) {
            this.mSwitchPref.setChecked(false);
            this.mSwitchPref.setSummary(2131887422);
            this.mSwitchPref.setEnabled(false);
            return true;
        }
        this.mSwitchPref.checkRestrictionAndSetDisabled("no_install_unknown_sources");
        if (this.mSwitchPref.isDisabledByAdmin()) {
            return true;
        }
        this.mInstallAppsState = this.mAppBridge.createInstallAppsStateFor(this.mPackageName, this.mPackageInfo.applicationInfo.uid);
        if (!this.mInstallAppsState.isPotentialAppSource()) {
            this.mSwitchPref.setEnabled(false);
            return true;
        }
        this.mSwitchPref.setChecked(this.mInstallAppsState.canInstallApps());
        return true;
    }
}
