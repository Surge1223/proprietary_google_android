package com.android.settings.applications.appinfo;

import com.android.settings.applications.DefaultAppSettings;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.os.UserManager;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public abstract class DefaultAppShortcutPreferenceControllerBase extends BasePreferenceController
{
    protected final String mPackageName;
    
    public DefaultAppShortcutPreferenceControllerBase(final Context context, final String s, final String mPackageName) {
        super(context, s);
        this.mPackageName = mPackageName;
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (UserManager.get(this.mContext).isManagedProfile()) {
            return 3;
        }
        int n;
        if (this.hasAppCapability()) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        int n;
        if (this.isDefaultApp()) {
            n = 2131890232;
        }
        else {
            n = 2131888406;
        }
        return this.mContext.getText(n);
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (TextUtils.equals((CharSequence)this.mPreferenceKey, (CharSequence)preference.getKey())) {
            final Bundle arguments = new Bundle();
            arguments.putString(":settings:fragment_args_key", this.mPreferenceKey);
            new SubSettingLauncher(this.mContext).setDestination(DefaultAppSettings.class.getName()).setArguments(arguments).setTitle(2131887102).setSourceMetricsCategory(0).launch();
            return true;
        }
        return false;
    }
    
    protected abstract boolean hasAppCapability();
    
    protected abstract boolean isDefaultApp();
}
