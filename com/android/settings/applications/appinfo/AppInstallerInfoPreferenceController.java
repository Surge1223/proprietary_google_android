package com.android.settings.applications.appinfo;

import android.content.Intent;
import com.android.settingslib.applications.AppUtils;
import android.support.v7.preference.Preference;
import com.android.settings.Utils;
import com.android.settings.applications.AppStoreUtil;
import android.os.UserManager;
import android.content.Context;

public class AppInstallerInfoPreferenceController extends AppInfoPreferenceControllerBase
{
    private CharSequence mInstallerLabel;
    private String mInstallerPackage;
    private String mPackageName;
    
    public AppInstallerInfoPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public int getAvailabilityStatus() {
        final boolean managedProfile = UserManager.get(this.mContext).isManagedProfile();
        int n = 3;
        if (managedProfile) {
            return 3;
        }
        if (this.mInstallerLabel != null) {
            n = 0;
        }
        return n;
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
        this.mInstallerPackage = AppStoreUtil.getInstallerPackageName(this.mContext, this.mPackageName);
        this.mInstallerLabel = Utils.getApplicationLabel(this.mContext, this.mInstallerPackage);
    }
    
    @Override
    public void updateState(final Preference preference) {
        int n;
        if (AppUtils.isInstant(this.mParent.getPackageInfo().applicationInfo)) {
            n = 2131887898;
        }
        else {
            n = 2131886355;
        }
        preference.setSummary(this.mContext.getString(n, new Object[] { this.mInstallerLabel }));
        final Intent appStoreLink = AppStoreUtil.getAppStoreLink(this.mContext, this.mInstallerPackage, this.mPackageName);
        if (appStoreLink != null) {
            preference.setIntent(appStoreLink);
        }
        else {
            preference.setEnabled(false);
        }
    }
}
