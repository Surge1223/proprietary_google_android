package com.android.settings.applications.appinfo;

import android.support.v7.preference.Preference;
import android.app.LoaderManager;
import com.android.settings.applications.FetchPackageStorageAsyncLoader;
import android.os.UserHandle;
import android.content.Loader;
import android.os.Bundle;
import android.text.format.Formatter;
import com.android.settings.applications.AppStorageSettings;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.applications.StorageStatsSource;
import android.app.LoaderManager$LoaderCallbacks;

public class AppStoragePreferenceController extends AppInfoPreferenceControllerBase implements LoaderManager$LoaderCallbacks<StorageStatsSource.AppStorageStats>, LifecycleObserver, OnPause, OnResume
{
    private StorageStatsSource.AppStorageStats mLastResult;
    
    public AppStoragePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return AppStorageSettings.class;
    }
    
    CharSequence getStorageSummary(final StorageStatsSource.AppStorageStats appStorageStats, final boolean b) {
        if (appStorageStats == null) {
            return this.mContext.getText(2131887067);
        }
        final Context mContext = this.mContext;
        int n;
        if (b) {
            n = 2131889315;
        }
        else {
            n = 2131889316;
        }
        return this.mContext.getString(2131889312, new Object[] { Formatter.formatFileSize(this.mContext, appStorageStats.getTotalBytes()), mContext.getString(n).toString().toLowerCase() });
    }
    
    public Loader<StorageStatsSource.AppStorageStats> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<StorageStatsSource.AppStorageStats>)new FetchPackageStorageAsyncLoader(this.mContext, new StorageStatsSource(this.mContext), this.mParent.getAppEntry().info, UserHandle.of(UserHandle.myUserId()));
    }
    
    public void onLoadFinished(final Loader<StorageStatsSource.AppStorageStats> loader, final StorageStatsSource.AppStorageStats mLastResult) {
        this.mLastResult = mLastResult;
        this.updateState(this.mPreference);
    }
    
    public void onLoaderReset(final Loader<StorageStatsSource.AppStorageStats> loader) {
    }
    
    public void onPause() {
        final LoaderManager loaderManager = this.mParent.getLoaderManager();
        final AppInfoDashboardFragment mParent = this.mParent;
        loaderManager.destroyLoader(3);
    }
    
    public void onResume() {
        final LoaderManager loaderManager = this.mParent.getLoaderManager();
        final AppInfoDashboardFragment mParent = this.mParent;
        loaderManager.restartLoader(3, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
    }
    
    public void updateState(final Preference preference) {
        preference.setSummary(this.getStorageSummary(this.mLastResult, (this.mParent.getAppEntry().info.flags & 0x40000) != 0x0));
    }
}
