package com.android.settings.applications.appinfo;

import android.view.MenuInflater;
import android.view.Menu;
import android.content.pm.IPackageDeleteObserver;
import android.os.UserHandle;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.content.DialogInterface;
import com.android.settingslib.applications.AppUtils;
import android.support.v7.preference.PreferenceScreen;
import android.app.AlertDialog$Builder;
import android.app.AlertDialog;
import com.android.settings.applications.AppStoreUtil;
import android.view.View.OnClickListener;
import android.net.Uri;
import android.widget.Button;
import android.view.View;
import android.os.Bundle;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.applications.LayoutPreference;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.view.MenuItem;
import com.android.settingslib.core.lifecycle.events.OnPrepareOptionsMenu;
import com.android.settingslib.core.lifecycle.events.OnOptionsItemSelected;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.BasePreferenceController;

public class InstantAppButtonsPreferenceController extends BasePreferenceController implements DialogInterface$OnClickListener, LifecycleObserver, OnCreateOptionsMenu, OnOptionsItemSelected, OnPrepareOptionsMenu
{
    private static final String KEY_INSTANT_APP_BUTTONS = "instant_app_buttons";
    private static final String META_DATA_DEFAULT_URI = "default-url";
    private MenuItem mInstallMenu;
    private String mLaunchUri;
    private final PackageManagerWrapper mPackageManagerWrapper;
    private final String mPackageName;
    private final AppInfoDashboardFragment mParent;
    private LayoutPreference mPreference;
    
    public InstantAppButtonsPreferenceController(final Context context, final AppInfoDashboardFragment mParent, final String mPackageName, final Lifecycle lifecycle) {
        super(context, "instant_app_buttons");
        this.mParent = mParent;
        this.mPackageName = mPackageName;
        this.mPackageManagerWrapper = new PackageManagerWrapper(context.getPackageManager());
        this.mLaunchUri = this.getDefaultLaunchUri();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private String getDefaultLaunchUri() {
        final PackageManager packageManager = this.mContext.getPackageManager();
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setPackage(this.mPackageName);
        final Iterator iterator = packageManager.queryIntentActivities(intent, 8388736).iterator();
        while (iterator.hasNext()) {
            final Bundle metaData = iterator.next().activityInfo.metaData;
            if (metaData != null) {
                final String string = metaData.getString("default-url");
                if (!TextUtils.isEmpty((CharSequence)string)) {
                    return string;
                }
                continue;
            }
        }
        return null;
    }
    
    private void initButtons(final View view) {
        final Button button = (Button)view.findViewById(2131362273);
        final Button button2 = (Button)view.findViewById(2131361988);
        final Button button3 = (Button)view.findViewById(2131362325);
        if (!TextUtils.isEmpty((CharSequence)this.mLaunchUri)) {
            button.setVisibility(8);
            final Intent intent = new Intent("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.BROWSABLE");
            intent.setPackage(this.mPackageName);
            intent.setData(Uri.parse(this.mLaunchUri));
            intent.addFlags(268435456);
            button3.setOnClickListener((View.OnClickListener)new _$$Lambda$InstantAppButtonsPreferenceController$2vM5nla3CEsaIUNVk7alr9UEbBA(this, intent));
        }
        else {
            button3.setVisibility(8);
            final Intent appStoreLink = AppStoreUtil.getAppStoreLink(this.mContext, this.mPackageName);
            if (appStoreLink != null) {
                button.setOnClickListener((View.OnClickListener)new _$$Lambda$InstantAppButtonsPreferenceController$oBWjqqdf33bi3sDY5lE6TGLlFJM(this, appStoreLink));
            }
            else {
                button.setEnabled(false);
            }
        }
        button2.setOnClickListener((View.OnClickListener)new _$$Lambda$InstantAppButtonsPreferenceController$f8slAx9lBDdGAmwfjMjp59JCarA(this));
    }
    
    AlertDialog createDialog(final int n) {
        if (n == 4) {
            return new AlertDialog$Builder(this.mContext).setPositiveButton(2131887020, (DialogInterface$OnClickListener)this).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setTitle(2131887020).setMessage((CharSequence)this.mContext.getString(2131887019)).create();
        }
        return null;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (LayoutPreference)preferenceScreen.findPreference("instant_app_buttons");
        this.initButtons(this.mPreference.findViewById(2131362275));
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (AppUtils.isInstant(this.mParent.getPackageInfo().applicationInfo)) {
            n = 0;
        }
        else {
            n = 3;
        }
        return n;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        FeatureFactory.getFactory(this.mContext).getMetricsFeatureProvider().action(this.mContext, 923, this.mPackageName, (Pair<Integer, Object>[])new Pair[0]);
        this.mPackageManagerWrapper.deletePackageAsUser(this.mPackageName, null, 0, UserHandle.myUserId());
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        if (!TextUtils.isEmpty((CharSequence)this.mLaunchUri)) {
            menu.add(0, 3, 2, 2131887894).setShowAsAction(0);
        }
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == 3) {
            final Intent appStoreLink = AppStoreUtil.getAppStoreLink(this.mContext, this.mPackageName);
            if (appStoreLink != null) {
                this.mParent.startActivity(appStoreLink);
            }
            return true;
        }
        return false;
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        this.mInstallMenu = menu.findItem(3);
        if (this.mInstallMenu != null && AppStoreUtil.getAppStoreLink(this.mContext, this.mPackageName) == null) {
            this.mInstallMenu.setEnabled(false);
        }
    }
}
