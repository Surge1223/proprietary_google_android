package com.android.settings.applications.appinfo;

import com.android.settings.applications.defaultapps.DefaultPhonePreferenceController;
import android.content.Context;

public class DefaultPhoneShortcutPreferenceController extends DefaultAppShortcutPreferenceControllerBase
{
    private static final String KEY = "default_phone_app";
    
    public DefaultPhoneShortcutPreferenceController(final Context context, final String s) {
        super(context, "default_phone_app", s);
    }
    
    @Override
    protected boolean hasAppCapability() {
        return DefaultPhonePreferenceController.hasPhonePreference(this.mPackageName, this.mContext);
    }
    
    @Override
    protected boolean isDefaultApp() {
        return DefaultPhonePreferenceController.isPhoneDefault(this.mPackageName, this.mContext);
    }
}
