package com.android.settings.applications.appinfo;

import android.support.v7.preference.Preference;
import com.android.settingslib.AppItem;
import android.app.LoaderManager;
import com.android.settingslib.net.ChartDataLoader;
import android.content.Loader;
import android.os.Bundle;
import com.android.settings.Utils;
import com.android.settings.datausage.AppDataUsage;
import com.android.settings.SettingsPreferenceFragment;
import android.net.INetworkStatsService;
import android.os.RemoteException;
import android.net.INetworkStatsService$Stub;
import android.os.ServiceManager;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.datausage.DataUsageUtils;
import com.android.settings.datausage.DataUsageList;
import android.net.NetworkTemplate;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.content.Context;
import android.net.INetworkStatsSession;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.net.ChartData;
import android.app.LoaderManager$LoaderCallbacks;

public class AppDataUsagePreferenceController extends AppInfoPreferenceControllerBase implements LoaderManager$LoaderCallbacks<ChartData>, LifecycleObserver, OnPause, OnResume
{
    private ChartData mChartData;
    private INetworkStatsSession mStatsSession;
    
    public AppDataUsagePreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    private CharSequence getDataSummary() {
        if (this.mChartData == null) {
            return this.mContext.getString(2131887067);
        }
        final long totalBytes = this.mChartData.detail.getTotalBytes();
        if (totalBytes == 0L) {
            return this.mContext.getString(2131888413);
        }
        return this.mContext.getString(2131887212, new Object[] { Formatter.formatFileSize(this.mContext, totalBytes), DateUtils.formatDateTime(this.mContext, this.mChartData.detail.getStart(), 65552) });
    }
    
    private static NetworkTemplate getTemplate(final Context context) {
        if (DataUsageList.hasReadyMobileRadio(context)) {
            return NetworkTemplate.buildTemplateMobileWildcard();
        }
        if (DataUsageUtils.hasWifiRadio(context)) {
            return NetworkTemplate.buildTemplateWifiWildcard();
        }
        return NetworkTemplate.buildTemplateEthernet();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            final INetworkStatsService interface1 = INetworkStatsService$Stub.asInterface(ServiceManager.getService("netstats"));
            try {
                this.mStatsSession = interface1.openSession();
            }
            catch (RemoteException ex) {
                throw new RuntimeException((Throwable)ex);
            }
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        return (this.isBandwidthControlEnabled() ^ true) ? 1 : 0;
    }
    
    @Override
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return AppDataUsage.class;
    }
    
    boolean isBandwidthControlEnabled() {
        return Utils.isBandwidthControlEnabled();
    }
    
    public Loader<ChartData> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<ChartData>)new ChartDataLoader(this.mContext, this.mStatsSession, bundle);
    }
    
    public void onLoadFinished(final Loader<ChartData> loader, final ChartData mChartData) {
        this.mChartData = mChartData;
        this.updateState(this.mPreference);
    }
    
    public void onLoaderReset(final Loader<ChartData> loader) {
    }
    
    public void onPause() {
        final LoaderManager loaderManager = this.mParent.getLoaderManager();
        final AppInfoDashboardFragment mParent = this.mParent;
        loaderManager.destroyLoader(2);
    }
    
    public void onResume() {
        if (this.mStatsSession != null) {
            final int uid = this.mParent.getAppEntry().info.uid;
            final AppItem appItem = new AppItem(uid);
            appItem.addUid(uid);
            final LoaderManager loaderManager = this.mParent.getLoaderManager();
            final AppInfoDashboardFragment mParent = this.mParent;
            loaderManager.restartLoader(2, ChartDataLoader.buildArgs(getTemplate(this.mContext), appItem), (LoaderManager$LoaderCallbacks)this);
        }
    }
    
    public void updateState(final Preference preference) {
        preference.setSummary(this.getDataSummary());
    }
}
