package com.android.settings.applications.appinfo;

import android.support.v7.preference.Preference;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.content.Intent;
import android.content.res.Resources;
import android.icu.text.ListFormatter;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import com.android.settingslib.applications.PermissionsSummaryHelper;

public class AppPermissionPreferenceController extends AppInfoPreferenceControllerBase
{
    private static final String EXTRA_HIDE_INFO_BUTTON = "hideInfoButton";
    private static final String TAG = "PermissionPrefControl";
    private String mPackageName;
    final PermissionsSummaryHelper.PermissionsResultCallback mPermissionCallback;
    
    public AppPermissionPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mPermissionCallback = new PermissionsSummaryHelper.PermissionsResultCallback() {
            @Override
            public void onPermissionSummaryResult(final int n, final int n2, final int n3, final List<CharSequence> list) {
                if (AppPermissionPreferenceController.this.mParent.getActivity() == null) {
                    return;
                }
                final Resources resources = AppPermissionPreferenceController.this.mContext.getResources();
                String summary;
                if (n2 == 0) {
                    summary = resources.getString(2131888887);
                    AppPermissionPreferenceController.this.mPreference.setEnabled(false);
                }
                else {
                    final ArrayList<String> list2 = new ArrayList<String>((Collection<? extends String>)list);
                    if (n3 > 0) {
                        list2.add(resources.getQuantityString(2131755057, n3, new Object[] { n3 }));
                    }
                    if (list2.size() == 0) {
                        summary = resources.getString(2131888886);
                    }
                    else {
                        summary = ListFormatter.getInstance().format((Collection)list2);
                    }
                    AppPermissionPreferenceController.this.mPreference.setEnabled(true);
                }
                AppPermissionPreferenceController.this.mPreference.setSummary(summary);
            }
        };
    }
    
    private void startManagePermissionsActivity() {
        final Intent intent = new Intent("android.intent.action.MANAGE_APP_PERMISSIONS");
        intent.putExtra("android.intent.extra.PACKAGE_NAME", this.mParent.getAppEntry().info.packageName);
        intent.putExtra("hideInfoButton", true);
        try {
            final Activity activity = this.mParent.getActivity();
            final AppInfoDashboardFragment mParent = this.mParent;
            activity.startActivityForResult(intent, 1);
        }
        catch (ActivityNotFoundException ex) {
            Log.w("PermissionPrefControl", "No app can handle android.intent.action.MANAGE_APP_PERMISSIONS");
        }
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.getPreferenceKey().equals(preference.getKey())) {
            this.startManagePermissionsActivity();
            return true;
        }
        return false;
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
    }
    
    @Override
    public void updateState(final Preference preference) {
        PermissionsSummaryHelper.getPermissionSummary(this.mContext, this.mPackageName, this.mPermissionCallback);
    }
}
