package com.android.settings.applications.appinfo;

import android.view.MenuInflater;
import android.view.Menu;
import android.content.pm.IPackageDeleteObserver;
import android.os.UserHandle;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.content.DialogInterface;
import com.android.settingslib.applications.AppUtils;
import android.support.v7.preference.PreferenceScreen;
import android.app.AlertDialog$Builder;
import android.app.AlertDialog;
import com.android.settings.applications.AppStoreUtil;
import android.net.Uri;
import android.widget.Button;
import android.os.Bundle;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.content.pm.ResolveInfo;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.applications.LayoutPreference;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.view.MenuItem;
import com.android.settingslib.core.lifecycle.events.OnPrepareOptionsMenu;
import com.android.settingslib.core.lifecycle.events.OnOptionsItemSelected;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.BasePreferenceController;
import android.view.View;
import android.content.Intent;
import android.view.View.OnClickListener;

public final class _$$Lambda$InstantAppButtonsPreferenceController$oBWjqqdf33bi3sDY5lE6TGLlFJM implements View.OnClickListener
{
    public final void onClick(final View view) {
        InstantAppButtonsPreferenceController.lambda$initButtons$1(this.f$0, this.f$1, view);
    }
}
