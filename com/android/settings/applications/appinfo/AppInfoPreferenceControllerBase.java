package com.android.settings.applications.appinfo;

import android.text.TextUtils;
import android.os.Bundle;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.core.BasePreferenceController;

public abstract class AppInfoPreferenceControllerBase extends BasePreferenceController implements Callback
{
    private final Class<? extends SettingsPreferenceFragment> mDetailFragmentClass;
    protected AppInfoDashboardFragment mParent;
    protected Preference mPreference;
    
    public AppInfoPreferenceControllerBase(final Context context, final String s) {
        super(context, s);
        this.mDetailFragmentClass = this.getDetailFragmentClass();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    protected Bundle getArguments() {
        return null;
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    protected Class<? extends SettingsPreferenceFragment> getDetailFragmentClass() {
        return null;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.mPreferenceKey) && this.mDetailFragmentClass != null) {
            AppInfoDashboardFragment.startAppInfoFragment(this.mDetailFragmentClass, -1, this.getArguments(), this.mParent, this.mParent.getAppEntry());
            return true;
        }
        return false;
    }
    
    @Override
    public void refreshUi() {
        this.updateState(this.mPreference);
    }
    
    public void setParentFragment(final AppInfoDashboardFragment mParent) {
        (this.mParent = mParent).addToCallbackList((AppInfoDashboardFragment.Callback)this);
    }
}
