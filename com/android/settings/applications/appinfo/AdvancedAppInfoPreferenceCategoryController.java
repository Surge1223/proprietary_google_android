package com.android.settings.applications.appinfo;

import android.content.Context;
import com.android.settings.widget.PreferenceCategoryController;

public class AdvancedAppInfoPreferenceCategoryController extends PreferenceCategoryController
{
    public AdvancedAppInfoPreferenceCategoryController(final Context context, final String s) {
        super(context, s);
    }
}
