package com.android.settings.applications.appinfo;

import android.text.BidiFormatter;
import android.content.Context;

public class AppVersionPreferenceController extends AppInfoPreferenceControllerBase
{
    public AppVersionPreferenceController(final Context context, final String s) {
        super(context, s);
    }
    
    @Override
    public CharSequence getSummary() {
        return this.mContext.getString(2131889770, new Object[] { BidiFormatter.getInstance().unicodeWrap(this.mParent.getPackageInfo().versionName) });
    }
}
