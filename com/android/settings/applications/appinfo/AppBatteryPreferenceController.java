package com.android.settings.applications.appinfo;

import com.android.settingslib.Utils;
import java.util.Collection;
import java.util.ArrayList;
import android.app.LoaderManager;
import android.content.pm.PackageInfo;
import com.android.settings.fuelgauge.BatteryStatsHelperLoader;
import android.content.Loader;
import android.os.Bundle;
import com.android.settings.fuelgauge.anomaly.Anomaly;
import com.android.settings.core.InstrumentedPreferenceFragment;
import android.app.Activity;
import com.android.settings.fuelgauge.AdvancedPowerUsageDetail;
import com.android.settings.SettingsActivity;
import android.os.Handler;
import com.android.settings.fuelgauge.BatteryEntry;
import android.os.UserManager;
import java.util.List;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.internal.os.BatterySipper;
import android.support.v7.preference.Preference;
import com.android.settings.fuelgauge.BatteryUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.internal.os.BatteryStatsHelper;
import android.app.LoaderManager$LoaderCallbacks;
import com.android.settings.core.BasePreferenceController;

public class AppBatteryPreferenceController extends BasePreferenceController implements LoaderManager$LoaderCallbacks<BatteryStatsHelper>, LifecycleObserver, OnPause, OnResume
{
    private static final String KEY_BATTERY = "battery";
    BatteryStatsHelper mBatteryHelper;
    private String mBatteryPercent;
    BatteryUtils mBatteryUtils;
    private final String mPackageName;
    private final AppInfoDashboardFragment mParent;
    private Preference mPreference;
    BatterySipper mSipper;
    
    public AppBatteryPreferenceController(final Context context, final AppInfoDashboardFragment mParent, final String mPackageName, final Lifecycle lifecycle) {
        super(context, "battery");
        this.mParent = mParent;
        this.mBatteryUtils = BatteryUtils.getInstance(this.mContext);
        this.mPackageName = mPackageName;
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        (this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey())).setEnabled(false);
    }
    
    BatterySipper findTargetSipper(final BatteryStatsHelper batteryStatsHelper, final int n) {
        final List usageList = batteryStatsHelper.getUsageList();
        for (int i = 0; i < usageList.size(); ++i) {
            final BatterySipper batterySipper = usageList.get(i);
            if (batterySipper.getUid() == n) {
                return batterySipper;
            }
        }
        return null;
    }
    
    @Override
    public int getAvailabilityStatus() {
        return this.mContext.getResources().getBoolean(2131034133) ? 0 : 1;
    }
    
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!"battery".equals(preference.getKey())) {
            return false;
        }
        if (this.isBatteryStatsAvailable()) {
            final BatteryEntry batteryEntry = new BatteryEntry(this.mContext, null, (UserManager)this.mContext.getSystemService("user"), this.mSipper);
            batteryEntry.defaultPackageName = this.mPackageName;
            AdvancedPowerUsageDetail.startBatteryDetailPage(this.mParent.getActivity(), this.mParent, this.mBatteryHelper, 0, batteryEntry, this.mBatteryPercent, null);
        }
        else {
            AdvancedPowerUsageDetail.startBatteryDetailPage(this.mParent.getActivity(), this.mParent, this.mPackageName);
        }
        return true;
    }
    
    boolean isBatteryStatsAvailable() {
        return this.mBatteryHelper != null && this.mSipper != null;
    }
    
    public Loader<BatteryStatsHelper> onCreateLoader(final int n, final Bundle bundle) {
        return (Loader<BatteryStatsHelper>)new BatteryStatsHelperLoader(this.mContext);
    }
    
    public void onLoadFinished(final Loader<BatteryStatsHelper> loader, final BatteryStatsHelper mBatteryHelper) {
        this.mBatteryHelper = mBatteryHelper;
        final PackageInfo packageInfo = this.mParent.getPackageInfo();
        if (packageInfo != null) {
            this.mSipper = this.findTargetSipper(mBatteryHelper, packageInfo.applicationInfo.uid);
            if (this.mParent.getActivity() != null) {
                this.updateBattery();
            }
        }
    }
    
    public void onLoaderReset(final Loader<BatteryStatsHelper> loader) {
    }
    
    public void onPause() {
        final LoaderManager loaderManager = this.mParent.getLoaderManager();
        final AppInfoDashboardFragment mParent = this.mParent;
        loaderManager.destroyLoader(4);
    }
    
    public void onResume() {
        final LoaderManager loaderManager = this.mParent.getLoaderManager();
        final AppInfoDashboardFragment mParent = this.mParent;
        loaderManager.restartLoader(4, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
    }
    
    void updateBattery() {
        this.mPreference.setEnabled(true);
        if (this.isBatteryStatsAvailable()) {
            this.mBatteryPercent = Utils.formatPercentage((int)this.mBatteryUtils.calculateBatteryPercent(this.mSipper.totalPowerMah, this.mBatteryHelper.getTotalPower(), this.mBatteryUtils.removeHiddenBatterySippers(new ArrayList<BatterySipper>(this.mBatteryHelper.getUsageList())), this.mBatteryHelper.getStats().getDischargeAmount(0)));
            this.mPreference.setSummary(this.mContext.getString(2131886655, new Object[] { this.mBatteryPercent }));
        }
        else {
            this.mPreference.setSummary(this.mContext.getString(2131888409));
        }
    }
}
