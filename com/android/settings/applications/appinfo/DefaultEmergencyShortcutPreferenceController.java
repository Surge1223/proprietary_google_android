package com.android.settings.applications.appinfo;

import com.android.settings.applications.defaultapps.DefaultEmergencyPreferenceController;
import android.content.Context;

public class DefaultEmergencyShortcutPreferenceController extends DefaultAppShortcutPreferenceControllerBase
{
    private static final String KEY = "default_emergency_app";
    
    public DefaultEmergencyShortcutPreferenceController(final Context context, final String s) {
        super(context, "default_emergency_app", s);
    }
    
    @Override
    protected boolean hasAppCapability() {
        return DefaultEmergencyPreferenceController.hasEmergencyPreference(this.mPackageName, this.mContext);
    }
    
    @Override
    protected boolean isDefaultApp() {
        return DefaultEmergencyPreferenceController.isEmergencyDefault(this.mPackageName, this.mContext);
    }
}
