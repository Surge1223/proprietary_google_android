package com.android.settings.applications.appinfo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.app.AlertDialog;
import com.android.settings.applications.AppStateBaseBridge;
import com.android.settings.applications.AppStateAppOpsBridge;
import com.android.settingslib.applications.ApplicationsState;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import android.content.Intent;
import android.app.AppOpsManager;
import com.android.settings.applications.AppStateWriteSettingsBridge;
import android.support.v7.preference.Preference;
import com.android.settings.applications.AppInfoWithHeader;

public class WriteSettingsDetails extends AppInfoWithHeader implements OnPreferenceChangeListener, OnPreferenceClickListener
{
    private static final int[] APP_OPS_OP_CODE;
    private AppStateWriteSettingsBridge mAppBridge;
    private AppOpsManager mAppOpsManager;
    private Intent mSettingsIntent;
    private SwitchPreference mSwitchPref;
    private AppStateWriteSettingsBridge.WriteSettingsState mWriteSettingsState;
    
    static {
        APP_OPS_OP_CODE = new int[] { 23 };
    }
    
    public static CharSequence getSummary(final Context context, final AppStateWriteSettingsBridge.WriteSettingsState writeSettingsState) {
        int n;
        if (((AppStateAppOpsBridge.PermissionState)writeSettingsState).isPermissible()) {
            n = 2131886392;
        }
        else {
            n = 2131886393;
        }
        return context.getString(n);
    }
    
    public static CharSequence getSummary(final Context context, final AppEntry appEntry) {
        AppStateAppOpsBridge.PermissionState writeSettingsInfo;
        if (appEntry.extraInfo instanceof AppStateWriteSettingsBridge.WriteSettingsState) {
            writeSettingsInfo = (AppStateWriteSettingsBridge.WriteSettingsState)appEntry.extraInfo;
        }
        else if (appEntry.extraInfo instanceof AppStateAppOpsBridge.PermissionState) {
            writeSettingsInfo = new AppStateWriteSettingsBridge.WriteSettingsState((AppStateAppOpsBridge.PermissionState)appEntry.extraInfo);
        }
        else {
            writeSettingsInfo = new AppStateWriteSettingsBridge(context, null, null).getWriteSettingsInfo(appEntry.info.packageName, appEntry.info.uid);
        }
        return getSummary(context, (AppStateWriteSettingsBridge.WriteSettingsState)writeSettingsInfo);
    }
    
    private void setCanWriteSettings(final boolean b) {
        this.logSpecialPermissionChange(b, this.mPackageName);
        final AppOpsManager mAppOpsManager = this.mAppOpsManager;
        final int uid = this.mPackageInfo.applicationInfo.uid;
        final String mPackageName = this.mPackageName;
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 2;
        }
        mAppOpsManager.setMode(23, uid, mPackageName, n);
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 221;
    }
    
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 774;
        }
        else {
            n = 775;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mAppBridge = new AppStateWriteSettingsBridge((Context)activity, this.mState, null);
        this.mAppOpsManager = (AppOpsManager)((Context)activity).getSystemService("appops");
        this.addPreferencesFromResource(2132082875);
        (this.mSwitchPref = (SwitchPreference)this.findPreference("app_ops_settings_switch")).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mSettingsIntent = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.USAGE_ACCESS_CONFIG").setPackage(this.mPackageName);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mSwitchPref) {
            if (this.mWriteSettingsState != null && (boolean)o != ((AppStateAppOpsBridge.PermissionState)this.mWriteSettingsState).isPermissible()) {
                this.setCanWriteSettings(((AppStateAppOpsBridge.PermissionState)this.mWriteSettingsState).isPermissible() ^ true);
                this.refreshUi();
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        return false;
    }
    
    @Override
    protected boolean refreshUi() {
        this.mWriteSettingsState = this.mAppBridge.getWriteSettingsInfo(this.mPackageName, this.mPackageInfo.applicationInfo.uid);
        this.mSwitchPref.setChecked(((AppStateAppOpsBridge.PermissionState)this.mWriteSettingsState).isPermissible());
        this.mSwitchPref.setEnabled(this.mWriteSettingsState.permissionDeclared);
        this.mPm.resolveActivityAsUser(this.mSettingsIntent, 128, this.mUserId);
        return true;
    }
}
