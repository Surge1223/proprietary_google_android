package com.android.settings.applications;

import android.content.pm.PackageManager;
import android.widget.ImageView;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import android.widget.BaseAdapter;
import android.text.format.DateUtils;
import android.os.SystemClock;
import java.util.Iterator;
import android.text.format.Formatter;
import android.text.BidiFormatter;
import android.widget.AdapterView;
import android.app.ActivityManager.MemoryInfo;
import android.graphics.PorterDuff.Mode;
import com.android.settingslib.Utils;
import android.content.res.ColorStateList;
import android.widget.ListAdapter;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.SettingsPreferenceFragment;
import com.android.internal.util.MemInfoReader;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.ActivityManager;
import android.view.View;
import java.util.HashMap;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.AbsListView$RecyclerListener;
import android.widget.FrameLayout;

public class RunningProcessesView extends FrameLayout implements AbsListView$RecyclerListener, AdapterView$OnItemClickListener, OnRefreshUiListener
{
    long SECONDARY_SERVER_MEM;
    final HashMap<View, ActiveItem> mActiveItems;
    ServiceListAdapter mAdapter;
    ActivityManager mAm;
    TextView mAppsProcessPrefix;
    TextView mAppsProcessText;
    TextView mBackgroundProcessPrefix;
    TextView mBackgroundProcessText;
    StringBuilder mBuilder;
    ProgressBar mColorBar;
    long mCurHighRam;
    long mCurLowRam;
    long mCurMedRam;
    BaseItem mCurSelected;
    boolean mCurShowCached;
    long mCurTotalRam;
    Runnable mDataAvail;
    TextView mForegroundProcessPrefix;
    TextView mForegroundProcessText;
    View mHeader;
    ListView mListView;
    MemInfoReader mMemInfoReader;
    final int mMyUserId;
    SettingsPreferenceFragment mOwner;
    RunningState mState;
    
    public RunningProcessesView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mActiveItems = new HashMap<View, ActiveItem>();
        this.mBuilder = new StringBuilder(128);
        this.mCurTotalRam = -1L;
        this.mCurHighRam = -1L;
        this.mCurMedRam = -1L;
        this.mCurLowRam = -1L;
        this.mCurShowCached = false;
        this.mMemInfoReader = new MemInfoReader();
        this.mMyUserId = UserHandle.myUserId();
    }
    
    private void startServiceDetailsActivity(final MergedItem mergedItem) {
        if (this.mOwner != null && mergedItem != null) {
            final Bundle arguments = new Bundle();
            if (mergedItem.mProcess != null) {
                arguments.putInt("uid", mergedItem.mProcess.mUid);
                arguments.putString("process", mergedItem.mProcess.mProcessName);
            }
            arguments.putInt("user_id", mergedItem.mUserId);
            arguments.putBoolean("background", this.mAdapter.mShowBackground);
            new SubSettingLauncher(this.getContext()).setDestination(RunningServiceDetails.class.getName()).setArguments(arguments).setTitle(2131888881).setSourceMetricsCategory(this.mOwner.getMetricsCategory()).launch();
        }
    }
    
    public void doCreate() {
        this.mAm = (ActivityManager)this.getContext().getSystemService("activity");
        this.mState = RunningState.getInstance(this.getContext());
        final LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService("layout_inflater");
        layoutInflater.inflate(2131558722, (ViewGroup)this);
        this.mListView = (ListView)this.findViewById(16908298);
        final View viewById = this.findViewById(16908292);
        if (viewById != null) {
            this.mListView.setEmptyView(viewById);
        }
        this.mListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        this.mListView.setRecyclerListener((AbsListView$RecyclerListener)this);
        this.mAdapter = new ServiceListAdapter(this.mState);
        this.mListView.setAdapter((ListAdapter)this.mAdapter);
        this.mHeader = layoutInflater.inflate(2131558720, (ViewGroup)null);
        this.mListView.addHeaderView(this.mHeader, (Object)null, false);
        this.mColorBar = (ProgressBar)this.mHeader.findViewById(2131361995);
        final Context context = this.getContext();
        this.mColorBar.setProgressTintList(ColorStateList.valueOf(context.getColor(2131099831)));
        this.mColorBar.setSecondaryProgressTintList(ColorStateList.valueOf(Utils.getColorAccent(context)));
        this.mColorBar.setSecondaryProgressTintMode(PorterDuff.Mode.SRC);
        this.mColorBar.setProgressBackgroundTintList(ColorStateList.valueOf(context.getColor(2131099830)));
        this.mColorBar.setProgressBackgroundTintMode(PorterDuff.Mode.SRC);
        this.mBackgroundProcessPrefix = (TextView)this.mHeader.findViewById(2131362172);
        this.mAppsProcessPrefix = (TextView)this.mHeader.findViewById(2131361886);
        this.mForegroundProcessPrefix = (TextView)this.mHeader.findViewById(2131362714);
        this.mBackgroundProcessText = (TextView)this.mHeader.findViewById(2131362171);
        this.mAppsProcessText = (TextView)this.mHeader.findViewById(2131361885);
        this.mForegroundProcessText = (TextView)this.mHeader.findViewById(2131362713);
        final ActivityManager.MemoryInfo activityManager$MemoryInfo = new ActivityManager.MemoryInfo();
        this.mAm.getMemoryInfo(activityManager$MemoryInfo);
        this.SECONDARY_SERVER_MEM = activityManager$MemoryInfo.secondaryServerThreshold;
    }
    
    public void doPause() {
        this.mState.pause();
        this.mDataAvail = null;
        this.mOwner = null;
    }
    
    public boolean doResume(final SettingsPreferenceFragment mOwner, final Runnable mDataAvail) {
        this.mOwner = mOwner;
        this.mState.resume((RunningState.OnRefreshUiListener)this);
        if (this.mState.hasData()) {
            this.refreshUi(true);
            return true;
        }
        this.mDataAvail = mDataAvail;
        return false;
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        this.startServiceDetailsActivity((MergedItem)(this.mCurSelected = (MergedItem)((ListView)adapterView).getAdapter().getItem(n)));
    }
    
    public void onMovedToScrapHeap(final View view) {
        this.mActiveItems.remove(view);
    }
    
    public void onRefreshUi(final int n) {
        switch (n) {
            case 2: {
                this.refreshUi(true);
                this.updateTimes();
                break;
            }
            case 1: {
                this.refreshUi(false);
                this.updateTimes();
                break;
            }
            case 0: {
                this.updateTimes();
                break;
            }
        }
    }
    
    void refreshUi(final boolean b) {
        if (b) {
            final ServiceListAdapter mAdapter = this.mAdapter;
            mAdapter.refreshItems();
            mAdapter.notifyDataSetChanged();
        }
        if (this.mDataAvail != null) {
            this.mDataAvail.run();
            this.mDataAvail = null;
        }
        this.mMemInfoReader.readMemInfo();
        synchronized (this.mState.mLock) {
            if (this.mCurShowCached != this.mAdapter.mShowBackground) {
                this.mCurShowCached = this.mAdapter.mShowBackground;
                if (this.mCurShowCached) {
                    this.mForegroundProcessPrefix.setText(this.getResources().getText(2131888874));
                    this.mAppsProcessPrefix.setText(this.getResources().getText(2131888868));
                }
                else {
                    this.mForegroundProcessPrefix.setText(this.getResources().getText(2131888872));
                    this.mAppsProcessPrefix.setText(this.getResources().getText(2131888867));
                }
            }
            final long totalSize = this.mMemInfoReader.getTotalSize();
            long mCurLowRam;
            long mCurMedRam;
            if (this.mCurShowCached) {
                mCurLowRam = this.mMemInfoReader.getFreeSize() + this.mMemInfoReader.getCachedSize();
                mCurMedRam = this.mState.mBackgroundProcessMemory;
            }
            else {
                mCurLowRam = this.mMemInfoReader.getFreeSize() + this.mMemInfoReader.getCachedSize() + this.mState.mBackgroundProcessMemory;
                mCurMedRam = this.mState.mServiceProcessMemory;
            }
            final long mCurHighRam = totalSize - mCurMedRam - mCurLowRam;
            if (this.mCurTotalRam != totalSize || this.mCurHighRam != mCurHighRam || this.mCurMedRam != mCurMedRam || this.mCurLowRam != mCurLowRam) {
                this.mCurTotalRam = totalSize;
                this.mCurHighRam = mCurHighRam;
                this.mCurMedRam = mCurMedRam;
                this.mCurLowRam = mCurLowRam;
                final BidiFormatter instance = BidiFormatter.getInstance();
                this.mBackgroundProcessText.setText((CharSequence)this.getResources().getString(2131888871, new Object[] { instance.unicodeWrap(Formatter.formatShortFileSize(this.getContext(), mCurLowRam)) }));
                this.mAppsProcessText.setText((CharSequence)this.getResources().getString(2131888871, new Object[] { instance.unicodeWrap(Formatter.formatShortFileSize(this.getContext(), mCurMedRam)) }));
                this.mForegroundProcessText.setText((CharSequence)this.getResources().getString(2131888871, new Object[] { instance.unicodeWrap(Formatter.formatShortFileSize(this.getContext(), mCurHighRam)) }));
                final int progress = (int)(mCurHighRam / totalSize * 100.0f);
                this.mColorBar.setProgress(progress);
                this.mColorBar.setSecondaryProgress((int)(mCurMedRam / totalSize * 100.0f) + progress);
            }
        }
    }
    
    void updateTimes() {
        final Iterator<ActiveItem> iterator = this.mActiveItems.values().iterator();
        while (iterator.hasNext()) {
            final ActiveItem activeItem = iterator.next();
            if (activeItem.mRootView.getWindowToken() == null) {
                iterator.remove();
            }
            else {
                activeItem.updateTime(this.getContext(), this.mBuilder);
            }
        }
    }
    
    public static class ActiveItem
    {
        long mFirstRunTime;
        ViewHolder mHolder;
        BaseItem mItem;
        View mRootView;
        boolean mSetBackground;
        
        void updateTime(final Context context, final StringBuilder sb) {
            final TextView textView = null;
            TextView textView2;
            if (this.mItem instanceof ServiceItem) {
                textView2 = this.mHolder.size;
            }
            else {
                String mSizeStr;
                if (this.mItem.mSizeStr != null) {
                    mSizeStr = this.mItem.mSizeStr;
                }
                else {
                    mSizeStr = "";
                }
                if (!mSizeStr.equals(this.mItem.mCurSizeStr)) {
                    this.mItem.mCurSizeStr = mSizeStr;
                    this.mHolder.size.setText((CharSequence)mSizeStr);
                }
                if (this.mItem.mBackground) {
                    textView2 = textView;
                    if (!this.mSetBackground) {
                        this.mSetBackground = true;
                        this.mHolder.uptime.setText((CharSequence)"");
                        textView2 = textView;
                    }
                }
                else {
                    textView2 = textView;
                    if (this.mItem instanceof MergedItem) {
                        textView2 = this.mHolder.uptime;
                    }
                }
            }
            if (textView2 != null) {
                final boolean b = false;
                this.mSetBackground = false;
                if (this.mFirstRunTime >= 0L) {
                    textView2.setText((CharSequence)DateUtils.formatElapsedTime(sb, (SystemClock.elapsedRealtime() - this.mFirstRunTime) / 1000L));
                }
                else {
                    boolean b2 = false;
                    if (this.mItem instanceof MergedItem) {
                        b2 = b;
                        if (((MergedItem)this.mItem).mServices.size() > 0) {
                            b2 = true;
                        }
                    }
                    if (b2) {
                        textView2.setText(context.getResources().getText(2131889044));
                    }
                    else {
                        textView2.setText((CharSequence)"");
                    }
                }
            }
        }
    }
    
    class ServiceListAdapter extends BaseAdapter
    {
        final LayoutInflater mInflater;
        final ArrayList<MergedItem> mItems;
        ArrayList<MergedItem> mOrigItems;
        boolean mShowBackground;
        final RunningState mState;
        
        ServiceListAdapter(final RunningState mState) {
            this.mItems = new ArrayList<MergedItem>();
            this.mState = mState;
            this.mInflater = (LayoutInflater)RunningProcessesView.this.getContext().getSystemService("layout_inflater");
            this.refreshItems();
        }
        
        public boolean areAllItemsEnabled() {
            return false;
        }
        
        public void bindView(final View view, final int n) {
            synchronized (this.mState.mLock) {
                if (n >= this.mItems.size()) {
                    return;
                }
                RunningProcessesView.this.mActiveItems.put(view, ((ViewHolder)view.getTag()).bind(this.mState, this.mItems.get(n), RunningProcessesView.this.mBuilder));
            }
        }
        
        public int getCount() {
            return this.mItems.size();
        }
        
        public Object getItem(final int n) {
            return this.mItems.get(n);
        }
        
        public long getItemId(final int n) {
            return this.mItems.get(n).hashCode();
        }
        
        boolean getShowBackground() {
            return this.mShowBackground;
        }
        
        public View getView(final int n, View view, final ViewGroup viewGroup) {
            if (view == null) {
                view = this.newView(viewGroup);
            }
            this.bindView(view, n);
            return view;
        }
        
        public boolean hasStableIds() {
            return true;
        }
        
        public boolean isEmpty() {
            return this.mState.hasData() && this.mItems.size() == 0;
        }
        
        public boolean isEnabled(final int n) {
            return this.mItems.get(n).mIsProcess ^ true;
        }
        
        public View newView(final ViewGroup viewGroup) {
            final View inflate = this.mInflater.inflate(2131558721, viewGroup, false);
            new ViewHolder(inflate);
            return inflate;
        }
        
        void refreshItems() {
            ArrayList<RunningState.MergedItem> mOrigItems;
            if (this.mShowBackground) {
                mOrigItems = this.mState.getCurrentBackgroundItems();
            }
            else {
                mOrigItems = this.mState.getCurrentMergedItems();
            }
            if (this.mOrigItems != mOrigItems) {
                if ((this.mOrigItems = mOrigItems) == null) {
                    this.mItems.clear();
                }
                else {
                    this.mItems.clear();
                    this.mItems.addAll((Collection<? extends MergedItem>)mOrigItems);
                    if (this.mShowBackground) {
                        Collections.sort(this.mItems, (Comparator<? super MergedItem>)this.mState.mBackgroundComparator);
                    }
                }
            }
        }
        
        void setShowBackground(final boolean b) {
            if (this.mShowBackground != b) {
                this.mShowBackground = b;
                this.mState.setWatchingBackgroundItems(b);
                this.refreshItems();
                RunningProcessesView.this.refreshUi(true);
            }
        }
    }
    
    public static class ViewHolder
    {
        public TextView description;
        public ImageView icon;
        public TextView name;
        public View rootView;
        public TextView size;
        public TextView uptime;
        
        public ViewHolder(final View rootView) {
            this.rootView = rootView;
            this.icon = (ImageView)rootView.findViewById(R.id.icon);
            this.name = (TextView)rootView.findViewById(2131362382);
            this.description = (TextView)rootView.findViewById(2131362067);
            this.size = (TextView)rootView.findViewById(2131362615);
            this.uptime = (TextView)rootView.findViewById(2131362778);
            rootView.setTag((Object)this);
        }
        
        public ActiveItem bind(final RunningState runningState, final BaseItem mItem, final StringBuilder sb) {
            synchronized (runningState.mLock) {
                final PackageManager packageManager = this.rootView.getContext().getPackageManager();
                if (mItem.mPackageInfo == null && mItem instanceof MergedItem && ((MergedItem)mItem).mProcess != null) {
                    ((MergedItem)mItem).mProcess.ensureLabel(packageManager);
                    mItem.mPackageInfo = ((MergedItem)mItem).mProcess.mPackageInfo;
                    mItem.mDisplayLabel = ((MergedItem)mItem).mProcess.mDisplayLabel;
                }
                this.name.setText(mItem.mDisplayLabel);
                final ActiveItem activeItem = new ActiveItem();
                activeItem.mRootView = this.rootView;
                activeItem.mItem = mItem;
                activeItem.mHolder = this;
                activeItem.mFirstRunTime = mItem.mActiveSince;
                if (mItem.mBackground) {
                    this.description.setText(this.rootView.getContext().getText(2131886935));
                }
                else {
                    this.description.setText((CharSequence)mItem.mDescription);
                }
                mItem.mCurSizeStr = null;
                this.icon.setImageDrawable(mItem.loadIcon(this.rootView.getContext(), runningState));
                this.icon.setVisibility(0);
                activeItem.updateTime(this.rootView.getContext(), sb);
                return activeItem;
            }
        }
    }
}
