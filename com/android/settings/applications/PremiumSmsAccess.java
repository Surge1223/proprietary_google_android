package com.android.settings.applications;

import android.support.v7.preference.PreferenceViewHolder;
import android.content.Context;
import android.support.v7.preference.DropDownPreference;
import android.view.View;
import android.arch.lifecycle.Lifecycle;
import android.app.Application;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.widget.FooterPreference;
import java.util.ArrayList;
import com.android.settingslib.applications.ApplicationsState;
import android.support.v7.preference.Preference;
import com.android.settings.notification.EmptyTextSettings;

public class PremiumSmsAccess extends EmptyTextSettings implements OnPreferenceChangeListener, Callback, Callbacks
{
    private ApplicationsState mApplicationsState;
    private Session mSession;
    private AppStateSmsPremBridge mSmsBackend;
    
    private void update() {
        this.updatePrefs(this.mSession.rebuild(AppStateSmsPremBridge.FILTER_APP_PREMIUM_SMS, ApplicationsState.ALPHA_COMPARATOR));
    }
    
    private void updatePrefs(final ArrayList<AppEntry> list) {
        if (list == null) {
            return;
        }
        this.setEmptyText(2131888631);
        int i = 0;
        this.setLoading(false, true);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        preferenceScreen.setOrderingAsAdded(true);
        while (i < list.size()) {
            final PremiumSmsPreference premiumSmsPreference = new PremiumSmsPreference(list.get(i), this.getPrefContext());
            premiumSmsPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            preferenceScreen.addPreference(premiumSmsPreference);
            ++i;
        }
        if (list.size() != 0) {
            final FooterPreference footerPreference = new FooterPreference(this.getPrefContext());
            footerPreference.setTitle(2131888632);
            preferenceScreen.addPreference(footerPreference);
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 388;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082811;
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(int n, final String s) {
        final int n2 = 0;
        switch (n) {
            default: {
                n = n2;
                break;
            }
            case 3: {
                n = 780;
                break;
            }
            case 2: {
                n = 779;
                break;
            }
            case 1: {
                n = 778;
                break;
            }
        }
        if (n != 0) {
            FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
        }
    }
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mApplicationsState = ApplicationsState.getInstance((Application)this.getContext().getApplicationContext());
        this.mSession = this.mApplicationsState.newSession((ApplicationsState.Callbacks)this, this.getLifecycle());
        this.mSmsBackend = new AppStateSmsPremBridge(this.getContext(), this.mApplicationsState, this);
    }
    
    @Override
    public void onDestroy() {
        this.mSmsBackend.release();
        super.onDestroy();
    }
    
    @Override
    public void onExtraInfoUpdated() {
        this.update();
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public void onPause() {
        this.mSmsBackend.pause();
        super.onPause();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final PremiumSmsPreference premiumSmsPreference = (PremiumSmsPreference)preference;
        final int int1 = Integer.parseInt((String)o);
        this.logSpecialPermissionChange(int1, premiumSmsPreference.mAppEntry.info.packageName);
        this.mSmsBackend.setSmsState(premiumSmsPreference.mAppEntry.info.packageName, int1);
        return true;
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
        this.updatePrefs(list);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mSmsBackend.resume();
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.setLoading(true, false);
    }
    
    private class PremiumSmsPreference extends DropDownPreference
    {
        private final AppEntry mAppEntry;
        
        public PremiumSmsPreference(final AppEntry mAppEntry, final Context context) {
            super(context);
            (this.mAppEntry = mAppEntry).ensureLabel(context);
            this.setTitle(this.mAppEntry.label);
            if (this.mAppEntry.icon != null) {
                this.setIcon(this.mAppEntry.icon);
            }
            this.setEntries(2130903140);
            this.setEntryValues(new CharSequence[] { String.valueOf(1), String.valueOf(2), String.valueOf(3) });
            this.setValue(String.valueOf(this.getCurrentValue()));
            this.setSummary("%s");
        }
        
        private int getCurrentValue() {
            int smsState;
            if (this.mAppEntry.extraInfo instanceof AppStateSmsPremBridge.SmsState) {
                smsState = ((AppStateSmsPremBridge.SmsState)this.mAppEntry.extraInfo).smsState;
            }
            else {
                smsState = 0;
            }
            return smsState;
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            if (this.getIcon() == null) {
                preferenceViewHolder.itemView.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        PremiumSmsAccess.this.mApplicationsState.ensureIcon(PremiumSmsPreference.this.mAppEntry);
                        PremiumSmsPreference.this.setIcon(PremiumSmsPreference.this.mAppEntry.icon);
                    }
                });
            }
            super.onBindViewHolder(preferenceViewHolder);
        }
    }
}
