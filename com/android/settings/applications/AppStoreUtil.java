package com.android.settings.applications;

import android.content.pm.ResolveInfo;
import android.util.Log;
import android.content.Intent;
import android.content.Context;

public class AppStoreUtil
{
    public static Intent getAppStoreLink(final Context context, final String s) {
        return getAppStoreLink(context, getInstallerPackageName(context, s), s);
    }
    
    public static Intent getAppStoreLink(final Context context, final String package1, final String s) {
        final Intent resolveIntent = resolveIntent(context, new Intent("android.intent.action.SHOW_APP_INFO").setPackage(package1));
        if (resolveIntent != null) {
            resolveIntent.putExtra("android.intent.extra.PACKAGE_NAME", s);
            return resolveIntent;
        }
        return null;
    }
    
    public static String getInstallerPackageName(final Context context, final String s) {
        final String s2 = null;
        String installerPackageName;
        try {
            installerPackageName = context.getPackageManager().getInstallerPackageName(s);
        }
        catch (IllegalArgumentException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception while retrieving the package installer of ");
            sb.append(s);
            Log.e("AppStoreUtil", sb.toString(), (Throwable)ex);
            installerPackageName = s2;
        }
        return installerPackageName;
    }
    
    private static Intent resolveIntent(final Context context, final Intent intent) {
        final ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        Intent setClassName;
        if (resolveActivity != null) {
            setClassName = new Intent(intent.getAction()).setClassName(resolveActivity.activityInfo.packageName, resolveActivity.activityInfo.name);
        }
        else {
            setClassName = null;
        }
        return setClassName;
    }
}
