package com.android.settings.applications;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class DataSaverController extends BasePreferenceController
{
    static final String KEY_DATA_SAVER = "data_saver";
    
    public DataSaverController(final Context context) {
        super(context, "data_saver");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034139)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
