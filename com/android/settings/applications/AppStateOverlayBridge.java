package com.android.settings.applications;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateOverlayBridge extends AppStateAppOpsBridge
{
    public static final AppFilter FILTER_SYSTEM_ALERT_WINDOW;
    private static final String[] PM_PERMISSION;
    
    static {
        PM_PERMISSION = new String[] { "android.permission.SYSTEM_ALERT_WINDOW" };
        FILTER_SYSTEM_ALERT_WINDOW = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.extraInfo != null;
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    public AppStateOverlayBridge(final Context context, final ApplicationsState applicationsState, final Callback callback) {
        super(context, applicationsState, callback, 24, AppStateOverlayBridge.PM_PERMISSION);
    }
    
    public OverlayState getOverlayInfo(final String s, final int n) {
        return new OverlayState(super.getPermissionInfo(s, n));
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = this.getOverlayInfo(s, n);
    }
    
    public static class OverlayState extends PermissionState
    {
        private static final List<String> DISABLE_PACKAGE_LIST;
        public final boolean controlEnabled;
        
        static {
            (DISABLE_PACKAGE_LIST = new ArrayList<String>()).add("com.android.systemui");
        }
        
        public OverlayState(final PermissionState permissionState) {
            super(permissionState.packageName, permissionState.userHandle);
            this.packageInfo = permissionState.packageInfo;
            this.appOpMode = permissionState.appOpMode;
            this.permissionDeclared = permissionState.permissionDeclared;
            this.staticPermissionGranted = permissionState.staticPermissionGranted;
            this.controlEnabled = (OverlayState.DISABLE_PACKAGE_LIST.contains(permissionState.packageName) ^ true);
        }
    }
}
