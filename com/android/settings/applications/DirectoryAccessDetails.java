package com.android.settings.applications;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import android.support.v7.preference.PreferenceScreen;
import java.util.function.Consumer;
import java.util.HashSet;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume$ScopedAccessProviderContract;
import android.net.Uri.Builder;
import java.util.HashMap;
import android.app.Activity;
import com.android.settingslib.applications.AppUtils;
import android.util.IconDrawableFactory;
import android.view.View;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.util.Pair;
import android.support.v7.preference.PreferenceCategory;
import java.util.Iterator;
import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import android.support.v7.preference.Preference;
import java.util.Set;
import android.net.Uri;
import android.content.Context;

public class DirectoryAccessDetails extends AppInfoBase
{
    private boolean mCreated;
    
    private SwitchPreference newPreference(final Context context, final String title, final Uri uri, final String s, final String s2, final boolean checked, final Set<SwitchPreference> set) {
        final SwitchPreference switchPreference = new SwitchPreference(context);
        switchPreference.setKey(String.format("%s:%s", s, s2));
        switchPreference.setTitle(title);
        switchPreference.setChecked(checked);
        switchPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new _$$Lambda$DirectoryAccessDetails$lMkU9x3CDhpq6XQS106C__FREgc(this, context, uri, s, s2, set));
        return switchPreference;
    }
    
    private void resetDoNotAskAgain(final Context context, final boolean b, final Uri uri, final String s, final String s2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Asking ");
        sb.append(uri);
        sb.append(" to update ");
        sb.append(s);
        sb.append("/");
        sb.append(s2);
        sb.append(" to ");
        sb.append(b);
        Log.d("DirectoryAccessDetails", sb.toString());
        final ContentValues contentValues = new ContentValues(1);
        contentValues.put("granted", b);
        final int update = context.getContentResolver().update(uri, contentValues, (String)null, new String[] { this.mPackageName, s, s2 });
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Updated ");
        sb2.append(update);
        sb2.append(" entries for ");
        sb2.append(s);
        sb2.append("/");
        sb2.append(s2);
        Log.d("DirectoryAccessDetails", sb2.toString());
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 1284;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        if (this.mCreated) {
            Log.w("DirectoryAccessDetails", "onActivityCreated(): ignoring duplicate call");
            return;
        }
        this.mCreated = true;
        if (this.mPackageInfo == null) {
            Log.w("DirectoryAccessDetails", "onActivityCreated(): no package info");
            return;
        }
        final Activity activity = this.getActivity();
        this.getPreferenceScreen().addPreference(EntityHeaderController.newInstance(activity, this, null).setRecyclerView(this.getListView(), this.getLifecycle()).setIcon(IconDrawableFactory.newInstance(this.getPrefContext()).getBadgedIcon(this.mPackageInfo.applicationInfo)).setLabel(this.mPackageInfo.applicationInfo.loadLabel(this.mPm)).setIsInstantApp(AppUtils.isInstant(this.mPackageInfo.applicationInfo)).setPackageName(this.mPackageName).setUid(this.mPackageInfo.applicationInfo.uid).setHasAppInfoLink(false).setButtonActions(0, 0).done(activity, this.getPrefContext()));
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082763);
    }
    
    @Override
    protected boolean refreshUi() {
        final Context prefContext = this.getPrefContext();
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        preferenceScreen.removeAll();
        final HashMap<Object, ExternalVolume> hashMap = (HashMap<Object, ExternalVolume>)new HashMap<Object, Throwable>();
        final Uri build = new Uri.Builder().scheme("content").authority("com.android.documentsui.scopedAccess").appendPath("permissions").appendPath("*").build();
        Object o = prefContext.getContentResolver().query(build, StorageVolume$ScopedAccessProviderContract.TABLE_PERMISSIONS_COLUMNS, (String)null, new String[] { this.mPackageName }, (String)null);
        Object volumes = null;
        Object o2 = null;
        Object o3 = null;
        Label_1148: {
            if (o == null) {
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Didn't get cursor for ");
                    sb.append(this.mPackageName);
                    Log.w("DirectoryAccessDetails", sb.toString());
                    if (o != null) {
                        ((Cursor)o).close();
                    }
                    return true;
                }
                catch (Throwable t3) {
                    break Label_1148;
                }
                finally {
                    o2 = volumes;
                    break Label_1148;
                }
            }
            try {
                if (((Cursor)o).getCount() == 0) {
                    o3 = new StringBuilder();
                    ((StringBuilder)o3).append("No permissions for ");
                    ((StringBuilder)o3).append(this.mPackageName);
                    Log.w("DirectoryAccessDetails", ((StringBuilder)o3).toString());
                    if (o != null) {
                        ((Cursor)o).close();
                    }
                    return true;
                }
                while (((Cursor)o).moveToNext()) {
                    o3 = ((Cursor)o).getString(0);
                    final String string = ((Cursor)o).getString(1);
                    final String string2 = ((Cursor)o).getString(2);
                    final boolean granted = ((Cursor)o).getInt(3) == 1;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Pkg:");
                    sb2.append((String)o3);
                    sb2.append(" uuid: ");
                    sb2.append(string);
                    sb2.append(" dir: ");
                    sb2.append(string2);
                    sb2.append(" granted:");
                    sb2.append(granted);
                    Log.v("DirectoryAccessDetails", sb2.toString());
                    if (!this.mPackageName.equals(o3)) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Ignoring ");
                        sb3.append(string);
                        sb3.append("/");
                        sb3.append(string2);
                        sb3.append(" due to package mismatch: expected ");
                        sb3.append(this.mPackageName);
                        sb3.append(", got ");
                        sb3.append((String)o3);
                        Log.w("DirectoryAccessDetails", sb3.toString());
                    }
                    else if (string == null) {
                        if (string2 == null) {
                            Log.wtf("DirectoryAccessDetails", "Ignoring permission on primary storage root");
                        }
                        else {
                            preferenceScreen.addPreference(this.newPreference(prefContext, string2, build, null, string2, granted, null));
                        }
                    }
                    else {
                        if ((o3 = hashMap.get(string)) == null) {
                            o3 = new ExternalVolume(string);
                            hashMap.put(string, (Throwable)o3);
                        }
                        if (string2 == null) {
                            ((ExternalVolume)o3).granted = granted;
                        }
                        else {
                            final List<Pair<String, Boolean>> children = ((ExternalVolume)o3).children;
                            o3 = new Pair((Object)string2, (Object)granted);
                            children.add((Pair<String, Boolean>)o3);
                        }
                    }
                }
                if (o != null) {
                    ((Cursor)o).close();
                }
                o3 = new StringBuilder();
                ((StringBuilder)o3).append("external volumes: ");
                ((StringBuilder)o3).append(hashMap);
                Log.v("DirectoryAccessDetails", ((StringBuilder)o3).toString());
                if (hashMap.isEmpty()) {
                    return true;
                }
                final StorageManager storageManager = (StorageManager)prefContext.getSystemService((Class)StorageManager.class);
                volumes = storageManager.getVolumes();
                if (((List)volumes).isEmpty()) {
                    Log.w("DirectoryAccessDetails", "StorageManager returned no secondary volumes");
                    return true;
                }
                final HashMap hashMap2 = new HashMap<Object, Object>(((List)volumes).size());
                for (final VolumeInfo volumeInfo : volumes) {
                    o = volumeInfo.getFsUuid();
                    if (o == null) {
                        continue;
                    }
                    if ((o3 = storageManager.getBestVolumeDescription(volumeInfo)) == null) {
                        o3 = new StringBuilder();
                        ((StringBuilder)o3).append("No description for ");
                        ((StringBuilder)o3).append(volumeInfo);
                        ((StringBuilder)o3).append("; using uuid instead: ");
                        ((StringBuilder)o3).append((String)o);
                        Log.w("DirectoryAccessDetails", ((StringBuilder)o3).toString());
                        o3 = o;
                    }
                    hashMap2.put((Throwable)o, (String)o3);
                }
                o3 = new StringBuilder();
                ((StringBuilder)o3).append("UUID -> name mapping: ");
                ((StringBuilder)o3).append(hashMap2);
                Log.v("DirectoryAccessDetails", ((StringBuilder)o3).toString());
                o = hashMap.values().iterator();
                o3 = prefContext;
                final Throwable t = (Throwable)o;
                while (true) {
                    o = this;
                    if (!((Iterator)t).hasNext()) {
                        break;
                    }
                    final ExternalVolume externalVolume = ((Iterator<ExternalVolume>)t).next();
                    final String s = hashMap2.get(externalVolume.uuid);
                    if (s == null) {
                        o = new StringBuilder();
                        ((StringBuilder)o).append("Ignoring entry for invalid UUID: ");
                        ((StringBuilder)o).append(externalVolume.uuid);
                        Log.w("DirectoryAccessDetails", ((StringBuilder)o).toString());
                    }
                    else {
                        final PreferenceCategory preferenceCategory = new PreferenceCategory((Context)o3);
                        preferenceScreen.addPreference(preferenceCategory);
                        final HashSet set = new HashSet<SwitchPreference>(externalVolume.children.size());
                        preferenceCategory.addPreference(((DirectoryAccessDetails)o).newPreference((Context)o3, s, build, externalVolume.uuid, (String)null, externalVolume.granted, (Set<SwitchPreference>)set));
                        externalVolume.children.forEach(new _$$Lambda$DirectoryAccessDetails$K0N0BhiTAIxLxuaXU9qwR_rLnAY((DirectoryAccessDetails)o, (Context)o3, s, build, externalVolume, preferenceCategory, set));
                    }
                }
                return true;
            }
            catch (Throwable t4) {}
            finally {
                o2 = volumes;
                break Label_1148;
            }
            try {
                throw o3;
            }
            finally {
                o2 = o3;
                final Context context;
                o3 = context;
            }
        }
        if (o != null) {
            if (o2 != null) {
                try {
                    ((Cursor)o).close();
                }
                catch (Throwable t2) {
                    ((Throwable)o2).addSuppressed(t2);
                }
            }
            else {
                ((Cursor)o).close();
            }
        }
        throw o3;
    }
    
    private static class ExternalVolume
    {
        final List<Pair<String, Boolean>> children;
        boolean granted;
        final String uuid;
        
        ExternalVolume(final String uuid) {
            this.children = new ArrayList<Pair<String, Boolean>>();
            this.uuid = uuid;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ExternalVolume: [uuid=");
            sb.append(this.uuid);
            sb.append(", granted=");
            sb.append(this.granted);
            sb.append(", children=");
            sb.append(this.children);
            sb.append("]");
            return sb.toString();
        }
    }
}
