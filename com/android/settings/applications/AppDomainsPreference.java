package com.android.settings.applications;

import android.widget.TextView;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.accessibility.ListDialogPreference;

public class AppDomainsPreference extends ListDialogPreference
{
    private int mNumEntries;
    
    public AppDomainsPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.setDialogLayoutResource(2131558446);
        this.setListItemLayoutResource(2131558447);
    }
    
    @Override
    public CharSequence getSummary() {
        final Context context = this.getContext();
        if (this.mNumEntries == 0) {
            return context.getString(2131887489);
        }
        final CharSequence summary = super.getSummary();
        int n;
        if (this.mNumEntries == 1) {
            n = 2131887490;
        }
        else {
            n = 2131887491;
        }
        return context.getString(n, new Object[] { summary });
    }
    
    @Override
    protected void onBindListItem(final View view, final int n) {
        final CharSequence title = this.getTitleAt(n);
        if (title != null) {
            ((TextView)view.findViewById(2131362087)).setText(title);
        }
    }
    
    @Override
    public void setTitles(final CharSequence[] titles) {
        int length;
        if (titles != null) {
            length = titles.length;
        }
        else {
            length = 0;
        }
        this.mNumEntries = length;
        super.setTitles(titles);
    }
}
