package com.android.settings.applications;

import android.os.Message;
import android.os.Handler;
import java.util.ArrayList;
import android.arch.lifecycle.LifecycleObserver;
import android.os.Looper;
import com.android.settingslib.applications.ApplicationsState;

public abstract class AppStateBaseBridge implements Callbacks
{
    protected final Session mAppSession;
    protected final ApplicationsState mAppState;
    protected final Callback mCallback;
    protected final BackgroundHandler mHandler;
    protected final MainHandler mMainHandler;
    
    public AppStateBaseBridge(final ApplicationsState mAppState, final Callback mCallback) {
        this.mAppState = mAppState;
        LifecycleObserver session;
        if (this.mAppState != null) {
            session = this.mAppState.newSession((ApplicationsState.Callbacks)this);
        }
        else {
            session = null;
        }
        this.mAppSession = (Session)session;
        this.mCallback = mCallback;
        Looper looper;
        if (this.mAppState != null) {
            looper = this.mAppState.getBackgroundLooper();
        }
        else {
            looper = Looper.getMainLooper();
        }
        this.mHandler = new BackgroundHandler(looper);
        this.mMainHandler = new MainHandler();
    }
    
    public void forceUpdate(final String s, final int n) {
        this.mHandler.obtainMessage(2, n, 0, (Object)s).sendToTarget();
    }
    
    protected abstract void loadAllExtraInfo();
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
        this.mHandler.sendEmptyMessage(1);
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
        this.mHandler.sendEmptyMessage(1);
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    public void pause() {
        this.mAppSession.onPause();
    }
    
    public void release() {
        this.mAppSession.onDestroy();
    }
    
    public void resume() {
        this.mHandler.sendEmptyMessage(1);
        this.mAppSession.onResume();
    }
    
    protected abstract void updateExtraInfo(final AppEntry p0, final String p1, final int p2);
    
    private class BackgroundHandler extends Handler
    {
        public BackgroundHandler(final Looper looper) {
            super(looper);
        }
        
        public void handleMessage(final Message message) {
            switch (message.what) {
                case 2: {
                    final ArrayList<AppEntry> allApps = AppStateBaseBridge.this.mAppSession.getAllApps();
                    final int size = allApps.size();
                    final String s = (String)message.obj;
                    final int arg1 = message.arg1;
                    for (int i = 0; i < size; ++i) {
                        final AppEntry appEntry = allApps.get(i);
                        if (appEntry.info.uid == arg1 && s.equals(appEntry.info.packageName)) {
                            AppStateBaseBridge.this.updateExtraInfo(appEntry, s, arg1);
                        }
                    }
                    AppStateBaseBridge.this.mMainHandler.sendEmptyMessage(1);
                    break;
                }
                case 1: {
                    AppStateBaseBridge.this.loadAllExtraInfo();
                    AppStateBaseBridge.this.mMainHandler.sendEmptyMessage(1);
                    break;
                }
            }
        }
    }
    
    public interface Callback
    {
        void onExtraInfoUpdated();
    }
    
    private class MainHandler extends Handler
    {
        public void handleMessage(final Message message) {
            if (message.what == 1) {
                AppStateBaseBridge.this.mCallback.onExtraInfoUpdated();
            }
        }
    }
}
