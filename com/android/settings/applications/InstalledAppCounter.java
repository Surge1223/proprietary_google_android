package com.android.settings.applications;

import android.content.pm.ResolveInfo;
import java.util.List;
import android.net.Uri;
import android.content.Intent;
import android.os.UserHandle;
import android.content.pm.ApplicationInfo;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;

public abstract class InstalledAppCounter extends AppCounter
{
    private final int mInstallReason;
    
    public InstalledAppCounter(final Context context, final int mInstallReason, final PackageManagerWrapper packageManagerWrapper) {
        super(context, packageManagerWrapper);
        this.mInstallReason = mInstallReason;
    }
    
    public static boolean includeInCount(final int n, final PackageManagerWrapper packageManagerWrapper, final ApplicationInfo applicationInfo) {
        final int userId = UserHandle.getUserId(applicationInfo.uid);
        final boolean b = false;
        if (n != -1 && packageManagerWrapper.getInstallReason(applicationInfo.packageName, new UserHandle(userId)) != n) {
            return false;
        }
        if ((applicationInfo.flags & 0x80) != 0x0) {
            return true;
        }
        if ((applicationInfo.flags & 0x1) == 0x0) {
            return true;
        }
        final List<ResolveInfo> queryIntentActivitiesAsUser = packageManagerWrapper.queryIntentActivitiesAsUser(new Intent("android.intent.action.MAIN", (Uri)null).addCategory("android.intent.category.LAUNCHER").setPackage(applicationInfo.packageName), 786944, userId);
        boolean b2 = b;
        if (queryIntentActivitiesAsUser != null) {
            b2 = b;
            if (queryIntentActivitiesAsUser.size() != 0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    protected boolean includeInCount(final ApplicationInfo applicationInfo) {
        return includeInCount(this.mInstallReason, this.mPm, applicationInfo);
    }
}
