package com.android.settings.applications;

import android.view.ViewGroup.LayoutParams;
import android.support.v7.preference.PreferenceViewHolder;
import android.content.res.TypedArray;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class SpacePreference extends Preference
{
    private int mHeight;
    
    public SpacePreference(final Context context, final AttributeSet set) {
        this(context, set, TypedArrayUtils.getAttr(context, R.attr.preferenceStyle, 16842894));
    }
    
    public SpacePreference(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public SpacePreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setLayoutResource(2131558757);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, new int[] { 16842997 }, n, n2);
        this.mHeight = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        obtainStyledAttributes.recycle();
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        preferenceViewHolder.itemView.setLayoutParams(new ViewGroup.LayoutParams(-1, this.mHeight));
    }
}
