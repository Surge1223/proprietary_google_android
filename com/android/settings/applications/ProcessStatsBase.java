package com.android.settings.applications;

import android.widget.SpinnerAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.content.Context;
import com.android.settings.core.SubSettingLauncher;
import android.os.Parcelable;
import android.os.Bundle;
import com.android.settings.SettingsActivity;
import com.android.internal.app.procstats.ProcessStats;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView$OnItemSelectedListener;
import com.android.settings.SettingsPreferenceFragment;

public abstract class ProcessStatsBase extends SettingsPreferenceFragment implements AdapterView$OnItemSelectedListener
{
    private static final long DURATION_QUANTUM;
    protected static int[] sDurationLabels;
    public static long[] sDurations;
    protected int mDurationIndex;
    private ArrayAdapter<String> mFilterAdapter;
    private Spinner mFilterSpinner;
    private ViewGroup mSpinnerHeader;
    protected ProcStatsData mStatsManager;
    
    static {
        DURATION_QUANTUM = ProcessStats.COMMIT_PERIOD;
        ProcessStatsBase.sDurations = new long[] { 10800000L - ProcessStatsBase.DURATION_QUANTUM / 2L, 21600000L - ProcessStatsBase.DURATION_QUANTUM / 2L, 43200000L - ProcessStatsBase.DURATION_QUANTUM / 2L, 86400000L - ProcessStatsBase.DURATION_QUANTUM / 2L };
        ProcessStatsBase.sDurationLabels = new int[] { 2131888293, 2131888294, 2131888291, 2131888292 };
    }
    
    public static void launchMemoryDetail(final SettingsActivity settingsActivity, final ProcStatsData.MemInfo memInfo, final ProcStatsPackageEntry procStatsPackageEntry, final boolean b) {
        final Bundle arguments = new Bundle();
        arguments.putParcelable("package_entry", (Parcelable)procStatsPackageEntry);
        arguments.putDouble("weight_to_ram", memInfo.weightToRam);
        arguments.putLong("total_time", memInfo.memTotalTime);
        arguments.putDouble("max_memory_usage", memInfo.usedWeight * memInfo.weightToRam);
        arguments.putDouble("total_scale", memInfo.totalScale);
        new SubSettingLauncher((Context)settingsActivity).setDestination(ProcessStatsDetail.class.getName()).setTitle(2131888285).setArguments(arguments).setSourceMetricsCategory(0).launch();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Bundle arguments = this.getArguments();
        this.mStatsManager = new ProcStatsData((Context)this.getActivity(), bundle != null || (arguments != null && arguments.getBoolean("transfer_stats", false)));
        int mDurationIndex;
        if (bundle != null) {
            mDurationIndex = bundle.getInt("duration_index");
        }
        else if (arguments != null) {
            mDurationIndex = arguments.getInt("duration_index");
        }
        else {
            mDurationIndex = 0;
        }
        this.mDurationIndex = mDurationIndex;
        final ProcStatsData mStatsManager = this.mStatsManager;
        long long1;
        if (bundle != null) {
            long1 = bundle.getLong("duration", ProcessStatsBase.sDurations[0]);
        }
        else {
            long1 = ProcessStatsBase.sDurations[0];
        }
        mStatsManager.setDuration(long1);
    }
    
    public void onDestroy() {
        super.onDestroy();
        if (this.getActivity().isChangingConfigurations()) {
            this.mStatsManager.xferStats();
        }
    }
    
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int mDurationIndex, final long n) {
        this.mDurationIndex = mDurationIndex;
        this.mStatsManager.setDuration(ProcessStatsBase.sDurations[mDurationIndex]);
        this.refreshUi();
    }
    
    public void onNothingSelected(final AdapterView<?> adapterView) {
        this.mFilterSpinner.setSelection(0);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mStatsManager.refreshStats(false);
        this.refreshUi();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putLong("duration", this.mStatsManager.getDuration());
        bundle.putInt("duration_index", this.mDurationIndex);
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.mSpinnerHeader = (ViewGroup)this.setPinnedHeaderView(2131558453);
        this.mFilterSpinner = (Spinner)this.mSpinnerHeader.findViewById(2131362145);
        (this.mFilterAdapter = (ArrayAdapter<String>)new ArrayAdapter(this.mFilterSpinner.getContext(), 2131558555)).setDropDownViewResource(17367049);
        for (int i = 0; i < 4; ++i) {
            this.mFilterAdapter.add((Object)this.getString(ProcessStatsBase.sDurationLabels[i]));
        }
        this.mFilterSpinner.setAdapter((SpinnerAdapter)this.mFilterAdapter);
        this.mFilterSpinner.setSelection(this.mDurationIndex);
        this.mFilterSpinner.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
    }
    
    public abstract void refreshUi();
}
