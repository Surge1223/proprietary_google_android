package com.android.settings.applications;

import android.content.Intent;
import com.android.settings.SettingsActivity;

public class InstalledAppOpenByDefaultPage extends SettingsActivity
{
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", AppLaunchSettings.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return AppLaunchSettings.class.getName().equals(s);
    }
}
