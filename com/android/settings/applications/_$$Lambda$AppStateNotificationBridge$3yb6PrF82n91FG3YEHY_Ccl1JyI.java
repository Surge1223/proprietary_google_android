package com.android.settings.applications;

import android.os.UserHandle;
import java.util.Iterator;
import android.util.ArrayMap;
import java.util.Map;
import android.app.usage.UsageEvents;
import android.app.usage.UsageEvents$Event;
import android.os.RemoteException;
import android.view.ViewGroup;
import android.widget.Switch;
import com.android.settingslib.utils.StringUtil;
import com.android.settings.Utils;
import java.util.ArrayList;
import android.os.UserManager;
import java.util.List;
import android.app.usage.IUsageStatsManager;
import android.content.Context;
import com.android.settings.notification.NotificationBackend;
import java.util.Comparator;
import android.view.View;
import com.android.settingslib.applications.ApplicationsState;
import android.view.View.OnClickListener;

public final class _$$Lambda$AppStateNotificationBridge$3yb6PrF82n91FG3YEHY_Ccl1JyI implements View.OnClickListener
{
    public final void onClick(final View view) {
        AppStateNotificationBridge.lambda$getSwitchOnClickListener$0(this.f$0, this.f$1, view);
    }
}
