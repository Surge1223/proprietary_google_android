package com.android.settings.applications;

import android.support.v7.preference.PreferenceViewHolder;
import android.util.ArraySet;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.settings.widget.AppPreference;
import android.view.View;
import android.content.ComponentName;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceCategory;
import android.provider.Settings;
import android.app.Fragment;
import android.provider.Settings;
import android.arch.lifecycle.Lifecycle;
import android.app.Application;
import android.os.Bundle;
import java.util.ArrayList;
import android.content.Intent;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.applications.ApplicationsState;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class ManageDomainUrls extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnPreferenceClickListener, Callbacks
{
    private ApplicationsState mApplicationsState;
    private PreferenceGroup mDomainAppList;
    private Preference mInstantAppAccountPreference;
    private Session mSession;
    private SwitchPreference mWebAction;
    
    private void rebuild() {
        final ArrayList<AppEntry> rebuild = this.mSession.rebuild(ApplicationsState.FILTER_WITH_DOMAIN_URLS, ApplicationsState.ALPHA_COMPARATOR);
        if (rebuild != null) {
            this.onRebuildComplete(rebuild);
        }
    }
    
    private void rebuildAppList(final PreferenceGroup preferenceGroup, final ArrayList<AppEntry> list) {
        this.cacheRemoveAllPrefs(preferenceGroup);
        for (int size = list.size(), i = 0; i < size; ++i) {
            final AppEntry appEntry = list.get(i);
            final StringBuilder sb = new StringBuilder();
            sb.append(appEntry.info.packageName);
            sb.append("|");
            sb.append(appEntry.info.uid);
            final String string = sb.toString();
            DomainAppPreference domainAppPreference = (DomainAppPreference)this.getCachedPreference(string);
            if (domainAppPreference == null) {
                domainAppPreference = new DomainAppPreference(this.getPrefContext(), this.mApplicationsState, appEntry);
                domainAppPreference.setKey(string);
                domainAppPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
                preferenceGroup.addPreference(domainAppPreference);
            }
            else {
                domainAppPreference.reuse();
            }
            domainAppPreference.setOrder(i);
        }
        this.removeCachedPrefs(preferenceGroup);
    }
    
    @Override
    public int getMetricsCategory() {
        return 143;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082788;
    }
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setAnimationAllowed(true);
        this.mApplicationsState = ApplicationsState.getInstance((Application)this.getContext().getApplicationContext());
        this.mSession = this.mApplicationsState.newSession((ApplicationsState.Callbacks)this, this.getLifecycle());
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
        this.rebuild();
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mWebAction) {
            Settings.Secure.putInt(this.getContentResolver(), "instant_apps_enabled", (int)(((boolean)o) ? 1 : 0));
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (preference.getClass() == DomainAppPreference.class) {
            final AppEntry access$000 = ((DomainAppPreference)preference).mEntry;
            AppInfoBase.startAppInfoFragment(AppLaunchSettings.class, 2131886490, access$000.info.packageName, access$000.info.uid, this, 1, this.getMetricsCategory());
            return true;
        }
        return false;
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
        if (this.getContext() == null) {
            return;
        }
        final int int1 = Settings.Global.getInt(this.getContext().getContentResolver(), "enable_ephemeral_feature", 1);
        boolean checked = false;
        if (int1 == 0) {
            this.mDomainAppList = this.getPreferenceScreen();
        }
        else {
            final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
            if (preferenceScreen.getPreferenceCount() == 0) {
                final PreferenceCategory preferenceCategory = new PreferenceCategory(this.getPrefContext());
                preferenceCategory.setTitle(2131889881);
                preferenceScreen.addPreference(preferenceCategory);
                (this.mWebAction = new SwitchPreference(this.getPrefContext())).setTitle(2131889880);
                this.mWebAction.setSummary(2131889879);
                final SwitchPreference mWebAction = this.mWebAction;
                if (Settings.Secure.getInt(this.getContentResolver(), "instant_apps_enabled", 1) != 0) {
                    checked = true;
                }
                mWebAction.setChecked(checked);
                this.mWebAction.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
                preferenceCategory.addPreference(this.mWebAction);
                final ComponentName instantAppResolverSettingsComponent = this.getActivity().getPackageManager().getInstantAppResolverSettingsComponent();
                Intent setComponent = null;
                if (instantAppResolverSettingsComponent != null) {
                    setComponent = new Intent().setComponent(instantAppResolverSettingsComponent);
                }
                if (setComponent != null) {
                    (this.mInstantAppAccountPreference = new Preference(this.getPrefContext())).setTitle(2131887899);
                    this.mInstantAppAccountPreference.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$ManageDomainUrls$agHbI5vf9m7UaPnJCYH2ithkZhk(this, setComponent));
                    preferenceCategory.addPreference(this.mInstantAppAccountPreference);
                }
                (this.mDomainAppList = new PreferenceCategory(this.getPrefContext())).setTitle(2131887487);
                preferenceScreen.addPreference(this.mDomainAppList);
            }
        }
        this.rebuildAppList(this.mDomainAppList, list);
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
    }
    
    static class DomainAppPreference extends AppPreference
    {
        private final ApplicationsState mApplicationsState;
        private final AppEntry mEntry;
        private final PackageManager mPm;
        
        public DomainAppPreference(final Context context, final ApplicationsState mApplicationsState, final AppEntry mEntry) {
            super(context);
            this.mApplicationsState = mApplicationsState;
            this.mPm = context.getPackageManager();
            (this.mEntry = mEntry).ensureLabel(this.getContext());
            this.setState();
            if (this.mEntry.icon != null) {
                this.setIcon(this.mEntry.icon);
            }
        }
        
        private CharSequence getDomainsSummary(final String s) {
            if (this.mPm.getIntentVerificationStatusAsUser(s, UserHandle.myUserId()) == 3) {
                return this.getContext().getString(2131887489);
            }
            final ArraySet<String> handledDomains = Utils.getHandledDomains(this.mPm, s);
            if (handledDomains.size() == 0) {
                return this.getContext().getString(2131887489);
            }
            if (handledDomains.size() == 1) {
                return this.getContext().getString(2131887490, new Object[] { handledDomains.valueAt(0) });
            }
            return this.getContext().getString(2131887491, new Object[] { handledDomains.valueAt(0) });
        }
        
        private void setState() {
            this.setTitle(this.mEntry.label);
            this.setSummary(this.getDomainsSummary(this.mEntry.info.packageName));
        }
        
        @Override
        public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
            if (this.mEntry.icon == null) {
                preferenceViewHolder.itemView.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        DomainAppPreference.this.mApplicationsState.ensureIcon(DomainAppPreference.this.mEntry);
                        DomainAppPreference.this.setIcon(DomainAppPreference.this.mEntry.icon);
                    }
                });
            }
            super.onBindViewHolder(preferenceViewHolder);
        }
        
        public void reuse() {
            this.setState();
            this.notifyChanged();
        }
    }
}
