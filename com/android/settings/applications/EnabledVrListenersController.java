package com.android.settings.applications;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class EnabledVrListenersController extends BasePreferenceController
{
    static final String KEY_ENABLED_VR_LISTENERS = "enabled_vr_listeners";
    
    public EnabledVrListenersController(final Context context) {
        super(context, "enabled_vr_listeners");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034143)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
