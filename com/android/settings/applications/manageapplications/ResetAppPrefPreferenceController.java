package com.android.settings.applications.manageapplications;

import android.os.Bundle;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ResetAppPrefPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnCreate, OnSaveInstanceState
{
    private ResetAppsHelper mResetAppsHelper;
    
    public ResetAppPrefPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mResetAppsHelper = new ResetAppsHelper(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "reset_app_prefs";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return false;
        }
        this.mResetAppsHelper.buildResetDialog();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.mResetAppsHelper.onRestoreInstanceState(bundle);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        this.mResetAppsHelper.onSaveInstanceState(bundle);
    }
}
