package com.android.settings.applications.manageapplications;

import android.os.Bundle;
import android.os.AsyncTask;
import java.util.List;
import android.app.ActivityManager;
import android.os.UserHandle;
import android.content.pm.ApplicationInfo;
import android.content.DialogInterface;
import android.app.AlertDialog$Builder;
import android.os.RemoteException;
import android.webkit.IWebViewUpdateService$Stub;
import android.app.INotificationManager$Stub;
import android.content.pm.IPackageManager$Stub;
import android.os.ServiceManager;
import android.webkit.IWebViewUpdateService;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.net.NetworkPolicyManager;
import android.app.INotificationManager;
import android.content.pm.IPackageManager;
import android.content.Context;
import android.app.AppOpsManager;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;

public class ResetAppsHelper implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    private final AppOpsManager mAom;
    private final Context mContext;
    private final IPackageManager mIPm;
    private final INotificationManager mNm;
    private final NetworkPolicyManager mNpm;
    private final PackageManager mPm;
    private AlertDialog mResetDialog;
    private final IWebViewUpdateService mWvus;
    
    public ResetAppsHelper(final Context mContext) {
        this.mContext = mContext;
        this.mPm = mContext.getPackageManager();
        this.mIPm = IPackageManager$Stub.asInterface(ServiceManager.getService("package"));
        this.mNm = INotificationManager$Stub.asInterface(ServiceManager.getService("notification"));
        this.mWvus = IWebViewUpdateService$Stub.asInterface(ServiceManager.getService("webviewupdate"));
        this.mNpm = NetworkPolicyManager.from(mContext);
        this.mAom = (AppOpsManager)mContext.getSystemService("appops");
    }
    
    private boolean isNonEnableableFallback(final String s) {
        try {
            return this.mWvus.isFallbackPackage(s);
        }
        catch (RemoteException ex) {
            throw new RuntimeException((Throwable)ex);
        }
    }
    
    void buildResetDialog() {
        if (this.mResetDialog == null) {
            this.mResetDialog = new AlertDialog$Builder(this.mContext).setTitle(2131888810).setMessage(2131888809).setPositiveButton(2131888808, (DialogInterface$OnClickListener)this).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setOnDismissListener((DialogInterface$OnDismissListener)this).show();
        }
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        if (this.mResetDialog != dialogInterface) {
            return;
        }
        AsyncTask.execute((Runnable)new Runnable() {
            @Override
            public void run() {
                final List installedApplications = ResetAppsHelper.this.mPm.getInstalledApplications(512);
                for (int i = 0; i < installedApplications.size(); ++i) {
                    final ApplicationInfo applicationInfo = installedApplications.get(i);
                    try {
                        ResetAppsHelper.this.mNm.setNotificationsEnabledForPackage(applicationInfo.packageName, applicationInfo.uid, true);
                    }
                    catch (RemoteException ex) {}
                    if (!applicationInfo.enabled && ResetAppsHelper.this.mPm.getApplicationEnabledSetting(applicationInfo.packageName) == 3 && !ResetAppsHelper.this.isNonEnableableFallback(applicationInfo.packageName)) {
                        ResetAppsHelper.this.mPm.setApplicationEnabledSetting(applicationInfo.packageName, 0, 1);
                    }
                }
                try {
                    ResetAppsHelper.this.mIPm.resetApplicationPreferences(UserHandle.myUserId());
                }
                catch (RemoteException ex2) {}
                ResetAppsHelper.this.mAom.resetAllModes();
                final int[] uidsWithPolicy = ResetAppsHelper.this.mNpm.getUidsWithPolicy(1);
                final int currentUser = ActivityManager.getCurrentUser();
                for (final int n : uidsWithPolicy) {
                    if (UserHandle.getUserId(n) == currentUser) {
                        ResetAppsHelper.this.mNpm.setUidPolicy(n, 0);
                    }
                }
            }
        });
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        if (this.mResetDialog == dialogInterface) {
            this.mResetDialog = null;
        }
    }
    
    public void onRestoreInstanceState(final Bundle bundle) {
        if (bundle != null && bundle.getBoolean("resetDialog")) {
            this.buildResetDialog();
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        if (this.mResetDialog != null) {
            bundle.putBoolean("resetDialog", true);
        }
    }
    
    public void stop() {
        if (this.mResetDialog != null) {
            this.mResetDialog.dismiss();
            this.mResetDialog = null;
        }
    }
}
