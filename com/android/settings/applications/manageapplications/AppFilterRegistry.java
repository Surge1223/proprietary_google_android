package com.android.settings.applications.manageapplications;

import com.android.settings.wifi.AppStateChangeWifiStateBridge;
import com.android.settings.applications.AppStateDirectoryAccessBridge;
import com.android.settings.applications.AppStateInstallAppsBridge;
import com.android.settings.applications.AppStateWriteSettingsBridge;
import com.android.settings.applications.AppStateOverlayBridge;
import com.android.settings.applications.AppStateUsageBridge;
import com.android.settings.applications.AppStateNotificationBridge;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.AppStatePowerBridge;

public class AppFilterRegistry
{
    private static AppFilterRegistry sRegistry;
    private final AppFilterItem[] mFilters;
    
    private AppFilterRegistry() {
        (this.mFilters = new AppFilterItem[16])[0] = new AppFilterItem(new ApplicationsState.CompoundFilter(AppStatePowerBridge.FILTER_POWER_WHITELISTED, ApplicationsState.FILTER_ALL_ENABLED), 0, 2131887846);
        this.mFilters[1] = new AppFilterItem(new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_WITHOUT_DISABLED_UNTIL_USED, ApplicationsState.FILTER_ALL_ENABLED), 1, 2131887627);
        this.mFilters[2] = new AppFilterItem(ApplicationsState.FILTER_EVERYTHING, 2, 2131887627);
        this.mFilters[3] = new AppFilterItem(ApplicationsState.FILTER_ALL_ENABLED, 3, 2131887634);
        this.mFilters[5] = new AppFilterItem(ApplicationsState.FILTER_DISABLED, 5, 2131887629);
        this.mFilters[4] = new AppFilterItem(ApplicationsState.FILTER_INSTANT, 4, 2131887636);
        this.mFilters[6] = new AppFilterItem(AppStateNotificationBridge.FILTER_APP_NOTIFICATION_RECENCY, 6, 2131889191);
        this.mFilters[7] = new AppFilterItem(AppStateNotificationBridge.FILTER_APP_NOTIFICATION_FREQUENCY, 7, 2131889190);
        this.mFilters[8] = new AppFilterItem(ApplicationsState.FILTER_PERSONAL, 8, 2131887644);
        this.mFilters[9] = new AppFilterItem(ApplicationsState.FILTER_WORK, 9, 2131887645);
        this.mFilters[10] = new AppFilterItem(AppStateUsageBridge.FILTER_APP_USAGE, 10, 2131887627);
        this.mFilters[11] = new AppFilterItem(AppStateOverlayBridge.FILTER_SYSTEM_ALERT_WINDOW, 11, 2131887643);
        this.mFilters[12] = new AppFilterItem(AppStateWriteSettingsBridge.FILTER_WRITE_SETTINGS, 12, 2131887646);
        this.mFilters[13] = new AppFilterItem(AppStateInstallAppsBridge.FILTER_APP_SOURCES, 13, 2131887635);
        this.mFilters[14] = new AppFilterItem(AppStateDirectoryAccessBridge.FILTER_APP_HAS_DIRECTORY_ACCESS, 14, 2131887635);
        this.mFilters[15] = new AppFilterItem(AppStateChangeWifiStateBridge.FILTER_CHANGE_WIFI_STATE, 15, 2131887646);
    }
    
    public static AppFilterRegistry getInstance() {
        if (AppFilterRegistry.sRegistry == null) {
            AppFilterRegistry.sRegistry = new AppFilterRegistry();
        }
        return AppFilterRegistry.sRegistry;
    }
    
    public AppFilterItem get(final int n) {
        return this.mFilters[n];
    }
    
    public int getDefaultFilterType(final int n) {
        if (n == 1) {
            return 6;
        }
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        return 2;
                    }
                    case 13: {
                        return 15;
                    }
                    case 12: {
                        return 14;
                    }
                }
                break;
            }
            case 8: {
                return 13;
            }
            case 7: {
                return 12;
            }
            case 6: {
                return 11;
            }
            case 5: {
                return 0;
            }
            case 4: {
                return 10;
            }
        }
    }
}
