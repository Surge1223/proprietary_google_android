package com.android.settings.applications.manageapplications;

import android.text.format.Formatter;
import java.io.IOException;
import android.util.Log;
import com.android.settings.Utils;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.content.Intent;
import android.app.Fragment;
import android.os.UserHandle;
import com.android.settingslib.applications.StorageStatsSource;
import android.content.Context;

public class MusicViewHolderController implements FileViewHolderController
{
    private Context mContext;
    private long mMusicSize;
    private StorageStatsSource mSource;
    private UserHandle mUser;
    private String mVolumeUuid;
    
    public MusicViewHolderController(final Context mContext, final StorageStatsSource mSource, final String mVolumeUuid, final UserHandle mUser) {
        this.mContext = mContext;
        this.mSource = mSource;
        this.mVolumeUuid = mVolumeUuid;
        this.mUser = mUser;
    }
    
    @Override
    public void onClick(final Fragment fragment) {
        final Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(DocumentsContract.buildRootUri("com.android.providers.media.documents", "audio_root"), "vnd.android.document/root");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.putExtra("android.intent.extra.USER_ID", (Parcelable)this.mUser);
        Utils.launchIntent(fragment, intent);
    }
    
    @Override
    public void queryStats() {
        try {
            this.mMusicSize = this.mSource.getExternalStorageStats(this.mVolumeUuid, this.mUser).audioBytes;
        }
        catch (IOException ex) {
            this.mMusicSize = 0L;
            Log.w("MusicViewHolderCtrl", (Throwable)ex);
        }
    }
    
    @Override
    public void setupView(final ApplicationViewHolder applicationViewHolder) {
        applicationViewHolder.setIcon(2131231028);
        applicationViewHolder.setTitle(this.mContext.getText(2131886465));
        applicationViewHolder.setSummary(Formatter.formatFileSize(this.mContext, this.mMusicSize));
    }
    
    @Override
    public boolean shouldShow() {
        return true;
    }
}
