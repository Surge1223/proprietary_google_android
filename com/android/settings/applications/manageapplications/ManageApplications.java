package com.android.settings.applications.manageapplications;

import com.android.settings.applications.InstalledAppCounter;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import java.util.List;
import java.util.Collections;
import android.widget.ArrayAdapter;
import android.os.Environment;
import android.text.TextUtils;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import com.android.settings.applications.AppStateAppOpsBridge;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.utils.ThreadUtils;
import java.util.Comparator;
import com.android.settings.wifi.AppStateChangeWifiStateBridge;
import com.android.settings.applications.AppStateDirectoryAccessBridge;
import com.android.settings.applications.AppStateInstallAppsBridge;
import com.android.settings.applications.AppStateWriteSettingsBridge;
import com.android.settings.applications.AppStateOverlayBridge;
import com.android.settings.applications.AppStatePowerBridge;
import com.android.settings.applications.AppStateUsageBridge;
import com.android.settings.applications.AppStateNotificationBridge;
import com.android.settings.widget.LoadingViewController;
import java.util.ArrayList;
import com.android.settings.applications.AppStateBaseBridge;
import com.android.settings.applications.DefaultAppSettings;
import com.android.settings.notification.ConfigureNotificationSettings;
import com.android.settings.core.SubSettingLauncher;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.preference.PreferenceFrameLayout$LayoutParams;
import android.preference.PreferenceFrameLayout;
import android.support.v7.widget.LinearLayoutManager;
import com.android.settingslib.applications.StorageStatsSource;
import android.view.LayoutInflater;
import com.android.settingslib.HelpUtils;
import android.view.MenuInflater;
import android.app.usage.IUsageStatsManager$Stub;
import android.os.ServiceManager;
import com.android.settings.Settings;
import android.os.Bundle;
import android.os.UserHandle;
import android.content.Intent;
import android.widget.SpinnerAdapter;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.android.settings.notification.AppNotificationSettings;
import com.android.settings.applications.UsageAccessDetails;
import com.android.settings.fuelgauge.HighPowerDetail;
import com.android.settings.applications.appinfo.DrawOverlayDetails;
import com.android.settings.applications.appinfo.WriteSettingsDetails;
import com.android.settings.applications.appinfo.ExternalSourcesDetails;
import com.android.settings.applications.AppStorageSettings;
import com.android.settings.applications.DirectoryAccessDetails;
import com.android.settings.wifi.ChangeWifiStateDetails;
import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import android.app.Fragment;
import com.android.settings.applications.AppInfoBase;
import android.content.Context;
import android.app.Activity;
import java.util.Collection;
import android.util.ArraySet;
import java.util.Arrays;
import android.util.Log;
import android.os.UserManager;
import android.app.usage.IUsageStatsManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import com.android.settings.notification.NotificationBackend;
import android.widget.Spinner;
import android.view.View;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.dashboard.SummaryLoader;
import java.util.Set;
import android.widget.AdapterView$OnItemSelectedListener;
import android.view.View.OnClickListener;
import com.android.settings.core.InstrumentedFragment;

public class ManageApplications extends InstrumentedFragment implements View.OnClickListener, AdapterView$OnItemSelectedListener
{
    static final boolean DEBUG;
    public static final Set<Integer> LIST_TYPES_WITH_INSTANT;
    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private ApplicationsAdapter mApplications;
    private ApplicationsState mApplicationsState;
    private String mCurrentPkgName;
    private int mCurrentUid;
    private View mEmptyView;
    private AppFilterItem mFilter;
    private FilterSpinnerAdapter mFilterAdapter;
    private Spinner mFilterSpinner;
    CharSequence mInvalidSizeStr;
    private boolean mIsWorkOnly;
    private View mListContainer;
    public int mListType;
    private View mLoadingContainer;
    private NotificationBackend mNotificationBackend;
    private Menu mOptionsMenu;
    private RecyclerView mRecyclerView;
    private ResetAppsHelper mResetAppsHelper;
    private View mRootView;
    private boolean mShowSystem;
    int mSortOrder;
    private View mSpinnerHeader;
    private int mStorageType;
    private IUsageStatsManager mUsageStatsManager;
    private UserManager mUserManager;
    private String mVolumeUuid;
    private int mWorkUserId;
    
    static {
        DEBUG = Log.isLoggable("ManageApplications", 3);
        LIST_TYPES_WITH_INSTANT = (Set)new ArraySet((Collection)Arrays.asList(0, 3));
        SUMMARY_PROVIDER_FACTORY = new SummaryLoader.SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new ManageApplications.SummaryProvider((Context)activity, summaryLoader);
            }
        };
    }
    
    public ManageApplications() {
        this.mSortOrder = 2131362624;
    }
    
    static ApplicationsState.AppFilter getCompositeFilter(final int n, final int n2, final String s) {
        final ApplicationsState.VolumeFilter volumeFilter = new ApplicationsState.VolumeFilter(s);
        if (n == 3) {
            ApplicationsState.AppFilter appFilter;
            if (n2 == 1) {
                appFilter = new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_AUDIO, volumeFilter);
            }
            else {
                appFilter = volumeFilter;
                if (n2 == 0) {
                    appFilter = new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_OTHER_APPS, volumeFilter);
                }
            }
            return appFilter;
        }
        if (n == 9) {
            return new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_GAMES, volumeFilter);
        }
        if (n == 10) {
            return new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_MOVIES, volumeFilter);
        }
        if (n == 11) {
            return new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_PHOTOS, volumeFilter);
        }
        return null;
    }
    
    private void startAppInfoFragment(final Class<?> clazz, final int n) {
        AppInfoBase.startAppInfoFragment(clazz, n, this.mCurrentPkgName, this.mCurrentUid, this, 1, this.getMetricsCategory());
    }
    
    private void startApplicationDetailsActivity() {
        final int mListType = this.mListType;
        if (mListType != 1) {
            switch (mListType) {
                default: {
                    this.startAppInfoFragment(AppInfoDashboardFragment.class, 2131886405);
                    break;
                }
                case 13: {
                    this.startAppInfoFragment(ChangeWifiStateDetails.class, 2131887002);
                    break;
                }
                case 12: {
                    this.startAppInfoFragment(DirectoryAccessDetails.class, 2131887414);
                    break;
                }
                case 11: {
                    this.startAppInfoFragment(AppStorageSettings.class, 2131889304);
                    break;
                }
                case 10: {
                    this.startAppInfoFragment(AppStorageSettings.class, 2131889299);
                    break;
                }
                case 9: {
                    this.startAppInfoFragment(AppStorageSettings.class, 2131887720);
                    break;
                }
                case 8: {
                    this.startAppInfoFragment(ExternalSourcesDetails.class, 2131887893);
                    break;
                }
                case 7: {
                    this.startAppInfoFragment(WriteSettingsDetails.class, 2131890229);
                    break;
                }
                case 6: {
                    this.startAppInfoFragment(DrawOverlayDetails.class, 2131888545);
                    break;
                }
                case 5: {
                    HighPowerDetail.show(this, this.mCurrentUid, this.mCurrentPkgName, 1);
                    break;
                }
                case 4: {
                    this.startAppInfoFragment(UsageAccessDetails.class, 2131889602);
                    break;
                }
                case 3: {
                    this.startAppInfoFragment(AppStorageSettings.class, 2131889306);
                    break;
                }
            }
        }
        else {
            this.startAppInfoFragment(AppNotificationSettings.class, 2131888522);
        }
    }
    
    void createHeader() {
        final Activity activity = this.getActivity();
        final FrameLayout frameLayout = (FrameLayout)this.mRootView.findViewById(2131362453);
        this.mSpinnerHeader = activity.getLayoutInflater().inflate(2131558453, (ViewGroup)frameLayout, false);
        this.mFilterSpinner = (Spinner)this.mSpinnerHeader.findViewById(2131362145);
        this.mFilterAdapter = new FilterSpinnerAdapter(this);
        this.mFilterSpinner.setAdapter((SpinnerAdapter)this.mFilterAdapter);
        this.mFilterSpinner.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        frameLayout.addView(this.mSpinnerHeader, 0);
        final AppFilterRegistry instance = AppFilterRegistry.getInstance();
        this.mFilterAdapter.enableFilter(instance.getDefaultFilterType(this.mListType));
        if (this.mListType == 0 && UserManager.get((Context)this.getActivity()).getUserProfiles().size() > 1) {
            this.mFilterAdapter.enableFilter(8);
            this.mFilterAdapter.enableFilter(9);
        }
        if (this.mListType == 1) {
            this.mFilterAdapter.enableFilter(6);
            this.mFilterAdapter.enableFilter(7);
            this.mFilterAdapter.disableFilter(2);
        }
        if (this.mListType == 5) {
            this.mFilterAdapter.enableFilter(1);
        }
        ApplicationsState.AppFilter compositeFilter;
        final ApplicationsState.AppFilter appFilter = compositeFilter = getCompositeFilter(this.mListType, this.mStorageType, this.mVolumeUuid);
        if (this.mIsWorkOnly) {
            compositeFilter = new ApplicationsState.CompoundFilter(appFilter, instance.get(9).getFilter());
        }
        if (compositeFilter != null) {
            this.mApplications.setCompositeFilter(compositeFilter);
        }
    }
    
    int getHelpResource() {
        if (this.mListType == 0) {
            return 2131887772;
        }
        if (this.mListType == 4) {
            return 2131887836;
        }
        return 2131887778;
    }
    
    public int getMetricsCategory() {
        switch (this.mListType) {
            default: {
                return 0;
            }
            case 13: {
                return 338;
            }
            case 12: {
                return 1283;
            }
            case 11: {
                return 1092;
            }
            case 10: {
                return 935;
            }
            case 9: {
                return 838;
            }
            case 8: {
                return 808;
            }
            case 7: {
                return 221;
            }
            case 6: {
                return 221;
            }
            case 5: {
                return 184;
            }
            case 4: {
                return 95;
            }
            case 3: {
                if (this.mStorageType == 1) {
                    return 839;
                }
                return 182;
            }
            case 1: {
                return 133;
            }
            case 0: {
                return 65;
            }
        }
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 1 && this.mCurrentPkgName != null) {
            if (this.mListType == 1) {
                this.mApplications.mExtraInfoBridge.forceUpdate(this.mCurrentPkgName, this.mCurrentUid);
            }
            else if (this.mListType != 5 && this.mListType != 6 && this.mListType != 7) {
                this.mApplicationsState.requestSize(this.mCurrentPkgName, UserHandle.getUserId(this.mCurrentUid));
            }
            else {
                this.mApplications.mExtraInfoBridge.forceUpdate(this.mCurrentPkgName, this.mCurrentUid);
            }
        }
    }
    
    public void onClick(final View view) {
        if (this.mApplications == null) {
            return;
        }
        final int childAdapterPosition = this.mRecyclerView.getChildAdapterPosition(view);
        if (childAdapterPosition == -1) {
            Log.w("ManageApplications", "Cannot find position for child, skipping onClick handling");
            return;
        }
        if (this.mApplications.getApplicationCount() > childAdapterPosition) {
            final ApplicationsState.AppEntry appEntry = this.mApplications.getAppEntry(childAdapterPosition);
            this.mCurrentPkgName = appEntry.info.packageName;
            this.mCurrentUid = appEntry.info.uid;
            this.startApplicationDetailsActivity();
        }
        else {
            this.mApplications.mExtraViewController.onClick(this);
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setHasOptionsMenu(true);
        final Activity activity = this.getActivity();
        this.mApplicationsState = ApplicationsState.getInstance(activity.getApplication());
        final Intent intent = activity.getIntent();
        final Bundle arguments = this.getArguments();
        final int intExtra = intent.getIntExtra(":settings:show_fragment_title_resid", 2131886405);
        String string;
        if (arguments != null) {
            string = arguments.getString("classname");
        }
        else {
            string = null;
        }
        String className = string;
        if (string == null) {
            className = intent.getComponent().getClassName();
        }
        final boolean equals = className.equals(Settings.StorageUseActivity.class.getName());
        final int n = -1;
        boolean boolean1 = false;
        int title;
        if (equals) {
            if (arguments != null && arguments.containsKey("volumeUuid")) {
                this.mVolumeUuid = arguments.getString("volumeUuid");
                this.mStorageType = arguments.getInt("storageType", 0);
                this.mListType = 3;
            }
            else {
                this.mListType = 0;
            }
            this.mSortOrder = 2131362627;
            title = intExtra;
        }
        else if (className.equals(Settings.UsageAccessSettingsActivity.class.getName())) {
            this.mListType = 4;
            title = 2131889602;
        }
        else if (className.equals(Settings.HighPowerApplicationsActivity.class.getName())) {
            this.mListType = 5;
            this.mShowSystem = true;
            title = 2131887844;
        }
        else if (className.equals(Settings.OverlaySettingsActivity.class.getName())) {
            this.mListType = 6;
            title = 2131889440;
        }
        else if (className.equals(Settings.WriteSettingsActivity.class.getName())) {
            this.mListType = 7;
            title = 2131890223;
        }
        else if (className.equals(Settings.ManageExternalSourcesActivity.class.getName())) {
            this.mListType = 8;
            title = 2131887893;
        }
        else if (className.equals(Settings.GamesStorageActivity.class.getName())) {
            this.mListType = 9;
            this.mSortOrder = 2131362627;
            title = intExtra;
        }
        else if (className.equals(Settings.MoviesStorageActivity.class.getName())) {
            this.mListType = 10;
            this.mSortOrder = 2131362627;
            title = intExtra;
        }
        else if (className.equals(Settings.PhotosStorageActivity.class.getName())) {
            this.mListType = 11;
            this.mSortOrder = 2131362627;
            this.mStorageType = arguments.getInt("storageType", 0);
            title = intExtra;
        }
        else if (className.equals(Settings.DirectoryAccessSettingsActivity.class.getName())) {
            this.mListType = 12;
            title = 2131887414;
        }
        else if (className.equals(Settings.ChangeWifiStateActivity.class.getName())) {
            this.mListType = 13;
            title = 2131887002;
        }
        else if (className.equals(Settings.NotificationAppListActivity.class.getName())) {
            this.mListType = 1;
            this.mUsageStatsManager = IUsageStatsManager$Stub.asInterface(ServiceManager.getService("usagestats"));
            this.mUserManager = UserManager.get(this.getContext());
            this.mNotificationBackend = new NotificationBackend();
            this.mSortOrder = 2131362626;
            title = 2131886389;
        }
        else {
            if ((title = intExtra) == -1) {
                title = 2131886405;
            }
            this.mListType = 0;
        }
        final AppFilterRegistry instance = AppFilterRegistry.getInstance();
        this.mFilter = instance.get(instance.getDefaultFilterType(this.mListType));
        if (arguments != null) {
            boolean1 = arguments.getBoolean("workProfileOnly");
        }
        this.mIsWorkOnly = boolean1;
        int int1 = n;
        if (arguments != null) {
            int1 = arguments.getInt("workId");
        }
        this.mWorkUserId = int1;
        if (bundle != null) {
            this.mSortOrder = bundle.getInt("sortOrder", this.mSortOrder);
            this.mShowSystem = bundle.getBoolean("showSystem", this.mShowSystem);
        }
        this.mInvalidSizeStr = activity.getText(2131887912);
        this.mResetAppsHelper = new ResetAppsHelper((Context)activity);
        if (title > 0) {
            activity.setTitle(title);
        }
    }
    
    public void onCreateOptionsMenu(final Menu mOptionsMenu, final MenuInflater menuInflater) {
        final Activity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        HelpUtils.prepareHelpMenuItem(activity, mOptionsMenu, this.getHelpResource(), this.getClass().getName());
        menuInflater.inflate(2131623938, this.mOptionsMenu = mOptionsMenu);
        this.updateOptionsMenu();
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2131558607, (ViewGroup)null);
        this.mLoadingContainer = this.mRootView.findViewById(2131362342);
        this.mListContainer = this.mRootView.findViewById(2131362340);
        if (this.mListContainer != null) {
            this.mEmptyView = this.mListContainer.findViewById(16908292);
            this.mApplications = new ApplicationsAdapter(this.mApplicationsState, this, this.mFilter, bundle);
            if (bundle != null) {
                this.mApplications.mHasReceivedLoadEntries = bundle.getBoolean("hasEntries", false);
                this.mApplications.mHasReceivedBridgeCallback = bundle.getBoolean("hasBridge", false);
            }
            int n;
            if (this.mIsWorkOnly) {
                n = this.mWorkUserId;
            }
            else {
                n = UserHandle.getUserId(this.mCurrentUid);
            }
            if (this.mStorageType == 1) {
                final Context context = this.getContext();
                this.mApplications.setExtraViewController(new MusicViewHolderController(context, new StorageStatsSource(context), this.mVolumeUuid, UserHandle.of(n)));
            }
            else if (this.mStorageType == 3) {
                final Context context2 = this.getContext();
                this.mApplications.setExtraViewController(new PhotosViewHolderController(context2, new StorageStatsSource(context2), this.mVolumeUuid, UserHandle.of(n)));
            }
            (this.mRecyclerView = (RecyclerView)this.mListContainer.findViewById(2131361887)).setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(this.getContext(), 1, false));
            this.mRecyclerView.setAdapter((RecyclerView.Adapter)this.mApplications);
        }
        if (viewGroup instanceof PreferenceFrameLayout) {
            ((PreferenceFrameLayout$LayoutParams)this.mRootView.getLayoutParams()).removeBorders = true;
        }
        this.createHeader();
        this.mResetAppsHelper.onRestoreInstanceState(bundle);
        return this.mRootView;
    }
    
    public void onDestroyOptionsMenu() {
        this.mOptionsMenu = null;
    }
    
    public void onDestroyView() {
        super.onDestroyView();
        if (this.mApplications != null) {
            this.mApplications.release();
        }
        this.mRootView = null;
    }
    
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        this.mFilter = this.mFilterAdapter.getFilter(n);
        this.mApplications.setFilter(this.mFilter);
        if (ManageApplications.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Selecting filter ");
            sb.append(this.mFilter);
            Log.d("ManageApplications", sb.toString());
        }
    }
    
    public void onNothingSelected(final AdapterView<?> adapterView) {
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        switch (menuItem.getItemId()) {
            default: {
                return false;
            }
            case 2131362624:
            case 2131362627: {
                if (this.mApplications != null) {
                    this.mApplications.rebuild(itemId);
                    break;
                }
                break;
            }
            case 2131362521: {
                this.mResetAppsHelper.buildResetDialog();
                return true;
            }
            case 2131362226:
            case 2131362608: {
                this.mShowSystem ^= true;
                this.mApplications.rebuild();
                break;
            }
            case 2131361856: {
                if (this.mListType == 1) {
                    new SubSettingLauncher(this.getContext()).setDestination(ConfigureNotificationSettings.class.getName()).setTitle(2131887104).setSourceMetricsCategory(this.getMetricsCategory()).setResultListener(this, 2).launch();
                }
                else {
                    new SubSettingLauncher(this.getContext()).setDestination(DefaultAppSettings.class.getName()).setTitle(2131887102).setSourceMetricsCategory(this.getMetricsCategory()).setResultListener(this, 2).launch();
                }
                return true;
            }
        }
        this.updateOptionsMenu();
        return true;
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        this.updateOptionsMenu();
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mResetAppsHelper.onSaveInstanceState(bundle);
        bundle.putInt("sortOrder", this.mSortOrder);
        bundle.putBoolean("showSystem", this.mShowSystem);
        bundle.putBoolean("hasEntries", this.mApplications.mHasReceivedLoadEntries);
        bundle.putBoolean("hasBridge", this.mApplications.mHasReceivedBridgeCallback);
        if (this.mApplications != null) {
            this.mApplications.onSaveInstanceState(bundle);
        }
    }
    
    public void onStart() {
        super.onStart();
        this.updateView();
        if (this.mApplications != null) {
            this.mApplications.resume(this.mSortOrder);
            this.mApplications.updateLoading();
        }
    }
    
    public void onStop() {
        super.onStop();
        if (this.mApplications != null) {
            this.mApplications.pause();
        }
        this.mResetAppsHelper.stop();
    }
    
    public void setHasDisabled(final boolean b) {
        if (this.mListType != 0) {
            return;
        }
        this.mFilterAdapter.setFilterEnabled(3, b);
        this.mFilterAdapter.setFilterEnabled(5, b);
    }
    
    public void setHasInstant(final boolean b) {
        if (ManageApplications.LIST_TYPES_WITH_INSTANT.contains(this.mListType)) {
            this.mFilterAdapter.setFilterEnabled(4, b);
        }
    }
    
    void updateOptionsMenu() {
        if (this.mOptionsMenu == null) {
            return;
        }
        this.mOptionsMenu.findItem(2131361856).setVisible(false);
        final MenuItem item = this.mOptionsMenu.findItem(2131362624);
        final int mListType = this.mListType;
        final boolean b = true;
        item.setVisible(mListType == 3 && this.mSortOrder != 2131362624);
        this.mOptionsMenu.findItem(2131362627).setVisible(this.mListType == 3 && this.mSortOrder != 2131362627);
        this.mOptionsMenu.findItem(2131362608).setVisible(!this.mShowSystem && this.mListType != 5);
        this.mOptionsMenu.findItem(2131362226).setVisible(this.mShowSystem && this.mListType != 5);
        this.mOptionsMenu.findItem(2131362521).setVisible(this.mListType == 0 && b);
        this.mOptionsMenu.findItem(2131362626).setVisible(false);
        this.mOptionsMenu.findItem(2131362625).setVisible(false);
    }
    
    public void updateView() {
        this.updateOptionsMenu();
        final Activity activity = this.getActivity();
        if (activity != null) {
            activity.invalidateOptionsMenu();
        }
    }
    
    static class ApplicationsAdapter extends Adapter<ApplicationViewHolder> implements Callback, Callbacks
    {
        private AppFilterItem mAppFilter;
        private AppFilter mCompositeFilter;
        private final Context mContext;
        private ArrayList<AppEntry> mEntries;
        private final AppStateBaseBridge mExtraInfoBridge;
        private FileViewHolderController mExtraViewController;
        private boolean mHasReceivedBridgeCallback;
        private boolean mHasReceivedLoadEntries;
        private int mLastIndex;
        private int mLastSortMode;
        private final LoadingViewController mLoadingViewController;
        private final ManageApplications mManageApplications;
        OnScrollListener mOnScrollListener;
        private RecyclerView mRecyclerView;
        private boolean mResumed;
        private final Session mSession;
        private final ApplicationsState mState;
        private int mWhichSize;
        
        public ApplicationsAdapter(final ApplicationsState mState, final ManageApplications mManageApplications, final AppFilterItem mAppFilter, final Bundle bundle) {
            this.mLastSortMode = -1;
            this.mWhichSize = 0;
            this.mLastIndex = -1;
            ((RecyclerView.Adapter)this).setHasStableIds(true);
            this.mState = mState;
            this.mSession = mState.newSession((ApplicationsState.Callbacks)this);
            this.mManageApplications = mManageApplications;
            this.mLoadingViewController = new LoadingViewController(this.mManageApplications.mLoadingContainer, this.mManageApplications.mListContainer);
            this.mContext = (Context)mManageApplications.getActivity();
            this.mAppFilter = mAppFilter;
            if (this.mManageApplications.mListType == 1) {
                this.mExtraInfoBridge = new AppStateNotificationBridge(this.mContext, this.mState, this, mManageApplications.mUsageStatsManager, mManageApplications.mUserManager, mManageApplications.mNotificationBackend);
            }
            else if (this.mManageApplications.mListType == 4) {
                this.mExtraInfoBridge = new AppStateUsageBridge(this.mContext, this.mState, this);
            }
            else if (this.mManageApplications.mListType == 5) {
                this.mExtraInfoBridge = new AppStatePowerBridge(this.mContext, this.mState, this);
            }
            else if (this.mManageApplications.mListType == 6) {
                this.mExtraInfoBridge = new AppStateOverlayBridge(this.mContext, this.mState, this);
            }
            else if (this.mManageApplications.mListType == 7) {
                this.mExtraInfoBridge = new AppStateWriteSettingsBridge(this.mContext, this.mState, this);
            }
            else if (this.mManageApplications.mListType == 8) {
                this.mExtraInfoBridge = new AppStateInstallAppsBridge(this.mContext, this.mState, this);
            }
            else if (this.mManageApplications.mListType == 12) {
                this.mExtraInfoBridge = new AppStateDirectoryAccessBridge(this.mState, this);
            }
            else if (this.mManageApplications.mListType == 13) {
                this.mExtraInfoBridge = new AppStateChangeWifiStateBridge(this.mContext, this.mState, this);
            }
            else {
                this.mExtraInfoBridge = null;
            }
            if (bundle != null) {
                this.mLastIndex = bundle.getInt("state_last_scroll_index");
            }
        }
        
        private boolean hasExtraView() {
            return this.mExtraViewController != null && this.mExtraViewController.shouldShow();
        }
        
        private static boolean packageNameEquals(final PackageItemInfo packageItemInfo, final PackageItemInfo packageItemInfo2) {
            return packageItemInfo != null && packageItemInfo2 != null && (packageItemInfo.packageName != null && packageItemInfo2.packageName != null) && packageItemInfo.packageName.equals(packageItemInfo2.packageName);
        }
        
        private ArrayList<AppEntry> removeDuplicateIgnoringUser(final ArrayList<AppEntry> list) {
            final int size = list.size();
            final ArrayList list2 = new ArrayList<AppEntry>(size);
            PackageItemInfo packageItemInfo = null;
            for (int i = 0; i < size; ++i) {
                final AppEntry appEntry = list.get(i);
                final ApplicationInfo info = appEntry.info;
                if (!packageNameEquals(packageItemInfo, (PackageItemInfo)appEntry.info)) {
                    list2.add(appEntry);
                }
                packageItemInfo = (PackageItemInfo)info;
            }
            list2.trimToSize();
            return (ArrayList<AppEntry>)list2;
        }
        
        static boolean shouldUseStableItemHeight(final int n) {
            return n != 1;
        }
        
        private void updateSummary(final ApplicationViewHolder applicationViewHolder, final AppEntry appEntry) {
            final int mListType = this.mManageApplications.mListType;
            boolean b = true;
            Label_0289: {
                if (mListType != 1) {
                    switch (mListType) {
                        default: {
                            switch (mListType) {
                                default: {
                                    applicationViewHolder.updateSizeText(appEntry, this.mManageApplications.mInvalidSizeStr, this.mWhichSize);
                                    break Label_0289;
                                }
                                case 13: {
                                    applicationViewHolder.setSummary(ChangeWifiStateDetails.getSummary(this.mContext, appEntry));
                                    break Label_0289;
                                }
                                case 12: {
                                    applicationViewHolder.setSummary(null);
                                    break Label_0289;
                                }
                            }
                            break;
                        }
                        case 8: {
                            applicationViewHolder.setSummary(ExternalSourcesDetails.getPreferenceSummary(this.mContext, appEntry));
                            break;
                        }
                        case 7: {
                            applicationViewHolder.setSummary(WriteSettingsDetails.getSummary(this.mContext, appEntry));
                            break;
                        }
                        case 6: {
                            applicationViewHolder.setSummary(DrawOverlayDetails.getSummary(this.mContext, appEntry));
                            break;
                        }
                        case 5: {
                            applicationViewHolder.setSummary(HighPowerDetail.getSummary(this.mContext, appEntry));
                            break;
                        }
                        case 4: {
                            if (appEntry.extraInfo != null) {
                                int summary;
                                if (((AppStateAppOpsBridge.PermissionState)new AppStateUsageBridge.UsageState((AppStateAppOpsBridge.PermissionState)appEntry.extraInfo)).isPermissible()) {
                                    summary = 2131886392;
                                }
                                else {
                                    summary = 2131886393;
                                }
                                applicationViewHolder.setSummary(summary);
                                break;
                            }
                            applicationViewHolder.setSummary(null);
                            break;
                        }
                    }
                }
                else if (appEntry.extraInfo != null) {
                    final Context mContext = this.mContext;
                    final AppStateNotificationBridge.NotificationsSentState notificationsSentState = (AppStateNotificationBridge.NotificationsSentState)appEntry.extraInfo;
                    if (this.mLastSortMode != 2131362626) {
                        b = false;
                    }
                    applicationViewHolder.setSummary(AppStateNotificationBridge.getSummary(mContext, notificationsSentState, b));
                }
                else {
                    applicationViewHolder.setSummary(null);
                }
            }
        }
        
        private void updateSwitch(final ApplicationViewHolder applicationViewHolder, final AppEntry appEntry) {
            final int mListType = this.mManageApplications.mListType;
            boolean b = true;
            if (mListType == 1) {
                applicationViewHolder.updateSwitch(((AppStateNotificationBridge)this.mExtraInfoBridge).getSwitchOnClickListener(appEntry), AppStateNotificationBridge.enableSwitch(appEntry), AppStateNotificationBridge.checkSwitch(appEntry));
                if (appEntry.extraInfo != null) {
                    final Context mContext = this.mContext;
                    final AppStateNotificationBridge.NotificationsSentState notificationsSentState = (AppStateNotificationBridge.NotificationsSentState)appEntry.extraInfo;
                    if (this.mLastSortMode != 2131362626) {
                        b = false;
                    }
                    applicationViewHolder.setSummary(AppStateNotificationBridge.getSummary(mContext, notificationsSentState, b));
                }
                else {
                    applicationViewHolder.setSummary(null);
                }
            }
        }
        
        public AppEntry getAppEntry(final int n) {
            return this.mEntries.get(n);
        }
        
        public int getApplicationCount() {
            int size;
            if (this.mEntries != null) {
                size = this.mEntries.size();
            }
            else {
                size = 0;
            }
            return size;
        }
        
        @Override
        public int getItemCount() {
            if (this.mEntries == null) {
                return 0;
            }
            return this.mEntries.size() + (this.hasExtraView() ? 1 : 0);
        }
        
        @Override
        public long getItemId(final int n) {
            if (n == this.mEntries.size()) {
                return -1L;
            }
            return this.mEntries.get(n).id;
        }
        
        @Override
        public int getItemViewType(int n) {
            final int itemCount = this.getItemCount();
            final int n2 = 1;
            if (itemCount - 1 == n) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (this.hasExtraView() && n != 0) {
                n = n2;
            }
            else {
                n = 0;
            }
            return n;
        }
        
        public boolean isEnabled(final int n) {
            return this.getItemViewType(n) == 1 || this.mManageApplications.mListType != 5 || (true ^ PowerWhitelistBackend.getInstance(this.mContext).isSysWhitelisted(this.mEntries.get(n).info.packageName));
        }
        
        @Override
        public void onAllSizesComputed() {
            if (this.mLastSortMode == 2131362627) {
                this.rebuild();
            }
        }
        
        @Override
        public void onAttachedToRecyclerView(final RecyclerView mRecyclerView) {
            super.onAttachedToRecyclerView(mRecyclerView);
            this.mRecyclerView = mRecyclerView;
            this.mOnScrollListener = new OnScrollListener(this);
            this.mRecyclerView.addOnScrollListener((RecyclerView.OnScrollListener)this.mOnScrollListener);
        }
        
        public void onBindViewHolder(final ApplicationViewHolder applicationViewHolder, final int n) {
            Label_0107: {
                if (this.mEntries != null && this.mExtraViewController != null && n == this.mEntries.size()) {
                    this.mExtraViewController.setupView(applicationViewHolder);
                    break Label_0107;
                }
                final AppEntry appEntry = this.mEntries.get(n);
                synchronized (appEntry) {
                    applicationViewHolder.setTitle(appEntry.label);
                    this.mState.ensureIcon(appEntry);
                    applicationViewHolder.setIcon(appEntry.icon);
                    this.updateSummary(applicationViewHolder, appEntry);
                    this.updateSwitch(applicationViewHolder, appEntry);
                    applicationViewHolder.updateDisableView(appEntry.info);
                    // monitorexit(appEntry)
                    applicationViewHolder.setEnabled(this.isEnabled(n));
                    applicationViewHolder.itemView.setOnClickListener((View.OnClickListener)this.mManageApplications);
                }
            }
        }
        
        public ApplicationViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
            View view;
            if (this.mManageApplications.mListType == 1) {
                view = ApplicationViewHolder.newView(viewGroup, true);
            }
            else {
                view = ApplicationViewHolder.newView(viewGroup, false);
            }
            return new ApplicationViewHolder(view, shouldUseStableItemHeight(this.mManageApplications.mListType));
        }
        
        @Override
        public void onDetachedFromRecyclerView(final RecyclerView recyclerView) {
            super.onDetachedFromRecyclerView(recyclerView);
            this.mRecyclerView.removeOnScrollListener((RecyclerView.OnScrollListener)this.mOnScrollListener);
            this.mOnScrollListener = null;
            this.mRecyclerView = null;
        }
        
        @Override
        public void onExtraInfoUpdated() {
            this.mHasReceivedBridgeCallback = true;
            this.rebuild();
        }
        
        public void onExtraViewCompleted() {
            if (!this.hasExtraView()) {
                return;
            }
            ((RecyclerView.Adapter)this).notifyItemChanged(this.getItemCount() - 1);
        }
        
        @Override
        public void onLauncherInfoChanged() {
            if (!this.mManageApplications.mShowSystem) {
                this.rebuild();
            }
        }
        
        @Override
        public void onLoadEntriesCompleted() {
            this.mHasReceivedLoadEntries = true;
            this.rebuild();
        }
        
        @Override
        public void onPackageIconChanged() {
        }
        
        @Override
        public void onPackageListChanged() {
            this.rebuild();
        }
        
        @Override
        public void onPackageSizeChanged(final String s) {
            if (this.mEntries == null) {
                return;
            }
            for (int size = this.mEntries.size(), i = 0; i < size; ++i) {
                final ApplicationInfo info = this.mEntries.get(i).info;
                if (info != null || TextUtils.equals((CharSequence)s, (CharSequence)info.packageName)) {
                    if (TextUtils.equals((CharSequence)this.mManageApplications.mCurrentPkgName, (CharSequence)info.packageName)) {
                        this.rebuild();
                        return;
                    }
                    this.mOnScrollListener.postNotifyItemChange(i);
                }
            }
        }
        
        @Override
        public void onRebuildComplete(final ArrayList<AppEntry> list) {
            final int filterType = this.mAppFilter.getFilterType();
            ArrayList<AppEntry> removeDuplicateIgnoringUser = null;
            Label_0025: {
                if (filterType != 0) {
                    removeDuplicateIgnoringUser = list;
                    if (filterType != 1) {
                        break Label_0025;
                    }
                }
                removeDuplicateIgnoringUser = this.removeDuplicateIgnoringUser(list);
            }
            this.mEntries = removeDuplicateIgnoringUser;
            ((RecyclerView.Adapter)this).notifyDataSetChanged();
            if (this.getItemCount() == 0) {
                this.mManageApplications.mRecyclerView.setVisibility(8);
                this.mManageApplications.mEmptyView.setVisibility(0);
            }
            else {
                this.mManageApplications.mEmptyView.setVisibility(8);
                this.mManageApplications.mRecyclerView.setVisibility(0);
            }
            if (this.mLastIndex != -1 && this.getItemCount() > this.mLastIndex) {
                this.mManageApplications.mRecyclerView.getLayoutManager().scrollToPosition(this.mLastIndex);
                this.mLastIndex = -1;
            }
            if (this.mSession.getAllApps().size() != 0 && this.mManageApplications.mListContainer.getVisibility() != 0) {
                this.mLoadingViewController.showContent(true);
            }
            if (this.mManageApplications.mListType == 4) {
                return;
            }
            this.mManageApplications.setHasDisabled(this.mState.haveDisabledApps());
            this.mManageApplications.setHasInstant(this.mState.haveInstantApps());
        }
        
        @Override
        public void onRunningStateChanged(final boolean progressBarIndeterminateVisibility) {
            this.mManageApplications.getActivity().setProgressBarIndeterminateVisibility(progressBarIndeterminateVisibility);
        }
        
        public void onSaveInstanceState(final Bundle bundle) {
            bundle.putInt("state_last_scroll_index", ((LinearLayoutManager)this.mManageApplications.mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition());
        }
        
        public void pause() {
            if (this.mResumed) {
                this.mResumed = false;
                this.mSession.onPause();
                if (this.mExtraInfoBridge != null) {
                    this.mExtraInfoBridge.pause();
                }
            }
        }
        
        public void rebuild() {
            if (this.mHasReceivedLoadEntries && (this.mExtraInfoBridge == null || this.mHasReceivedBridgeCallback)) {
                if (Environment.isExternalStorageEmulated()) {
                    this.mWhichSize = 0;
                }
                else {
                    this.mWhichSize = 1;
                }
                AppFilter filter;
                final AppFilter appFilter = filter = this.mAppFilter.getFilter();
                if (this.mCompositeFilter != null) {
                    filter = new ApplicationsState.CompoundFilter(appFilter, this.mCompositeFilter);
                }
                AppFilter appFilter2 = filter;
                if (!this.mManageApplications.mShowSystem) {
                    if (ManageApplications.LIST_TYPES_WITH_INSTANT.contains(this.mManageApplications.mListType)) {
                        appFilter2 = new ApplicationsState.CompoundFilter(filter, ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER_AND_INSTANT);
                    }
                    else {
                        appFilter2 = new ApplicationsState.CompoundFilter(filter, ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER);
                    }
                }
                Comparator<ApplicationsState.AppEntry> comparator = null;
                Label_0228: {
                    switch (this.mLastSortMode) {
                        default: {
                            comparator = ApplicationsState.ALPHA_COMPARATOR;
                            break;
                        }
                        case 2131362627: {
                            switch (this.mWhichSize) {
                                default: {
                                    comparator = ApplicationsState.SIZE_COMPARATOR;
                                    break Label_0228;
                                }
                                case 2: {
                                    comparator = ApplicationsState.EXTERNAL_SIZE_COMPARATOR;
                                    break Label_0228;
                                }
                                case 1: {
                                    comparator = ApplicationsState.INTERNAL_SIZE_COMPARATOR;
                                    break Label_0228;
                                }
                            }
                            break;
                        }
                        case 2131362626: {
                            comparator = AppStateNotificationBridge.RECENT_NOTIFICATION_COMPARATOR;
                            break;
                        }
                        case 2131362625: {
                            comparator = AppStateNotificationBridge.FREQUENCY_NOTIFICATION_COMPARATOR;
                            break;
                        }
                    }
                }
                ThreadUtils.postOnBackgroundThread(new _$$Lambda$ManageApplications$ApplicationsAdapter$z53WdtAYQ69qQ4PsDaqCwHe1hfA(this, new ApplicationsState.CompoundFilter(appFilter2, ApplicationsState.FILTER_NOT_HIDE), comparator));
            }
        }
        
        public void rebuild(final int n) {
            if (n == this.mLastSortMode) {
                return;
            }
            this.mManageApplications.mSortOrder = n;
            this.mLastSortMode = n;
            this.rebuild();
        }
        
        public void release() {
            this.mSession.onDestroy();
            if (this.mExtraInfoBridge != null) {
                this.mExtraInfoBridge.release();
            }
        }
        
        public void resume(final int mLastSortMode) {
            if (ManageApplications.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Resume!  mResumed=");
                sb.append(this.mResumed);
                Log.i("ManageApplications", sb.toString());
            }
            if (!this.mResumed) {
                this.mResumed = true;
                this.mSession.onResume();
                this.mLastSortMode = mLastSortMode;
                if (this.mExtraInfoBridge != null) {
                    this.mExtraInfoBridge.resume();
                }
                this.rebuild();
            }
            else {
                this.rebuild(mLastSortMode);
            }
        }
        
        public void setCompositeFilter(final AppFilter mCompositeFilter) {
            this.mCompositeFilter = mCompositeFilter;
            this.rebuild();
        }
        
        public void setExtraViewController(final FileViewHolderController mExtraViewController) {
            this.mExtraViewController = mExtraViewController;
            ThreadUtils.postOnBackgroundThread(new _$$Lambda$ManageApplications$ApplicationsAdapter$qMEtWjKuRu1RgrWKYhF_ScJDD7E(this));
        }
        
        public void setFilter(final AppFilterItem mAppFilter) {
            this.mAppFilter = mAppFilter;
            if (7 == mAppFilter.getFilterType()) {
                this.rebuild(2131362625);
            }
            else if (6 == mAppFilter.getFilterType()) {
                this.rebuild(2131362626);
            }
            else {
                this.rebuild();
            }
        }
        
        void updateLoading() {
            if (this.mHasReceivedLoadEntries && this.mSession.getAllApps().size() != 0) {
                this.mLoadingViewController.showContent(false);
            }
            else {
                this.mLoadingViewController.showLoadingViewDelayed();
            }
        }
        
        public static class OnScrollListener extends RecyclerView.OnScrollListener
        {
            private ApplicationsAdapter mAdapter;
            private boolean mDelayNotifyDataChange;
            private int mScrollState;
            
            public OnScrollListener(final ApplicationsAdapter mAdapter) {
                this.mScrollState = 0;
                this.mAdapter = mAdapter;
            }
            
            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, final int mScrollState) {
                this.mScrollState = mScrollState;
                if (this.mScrollState == 0 && this.mDelayNotifyDataChange) {
                    this.mDelayNotifyDataChange = false;
                    ((RecyclerView.Adapter)this.mAdapter).notifyDataSetChanged();
                }
            }
            
            public void postNotifyItemChange(final int n) {
                if (this.mScrollState == 0) {
                    ((RecyclerView.Adapter)this.mAdapter).notifyItemChanged(n);
                }
                else {
                    this.mDelayNotifyDataChange = true;
                }
            }
        }
    }
    
    static class FilterSpinnerAdapter extends ArrayAdapter<CharSequence>
    {
        private final Context mContext;
        private final ArrayList<AppFilterItem> mFilterOptions;
        private final ManageApplications mManageApplications;
        
        public FilterSpinnerAdapter(final ManageApplications mManageApplications) {
            super(mManageApplications.getContext(), 2131558555);
            this.mFilterOptions = new ArrayList<AppFilterItem>();
            this.mContext = mManageApplications.getContext();
            this.mManageApplications = mManageApplications;
            this.setDropDownViewResource(17367049);
        }
        
        public void disableFilter(int visibility) {
            final AppFilterItem value = AppFilterRegistry.getInstance().get(visibility);
            if (!this.mFilterOptions.remove(value)) {
                return;
            }
            if (ManageApplications.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Disabling filter ");
                sb.append(value);
                Log.d("ManageApplications", sb.toString());
            }
            Collections.sort(this.mFilterOptions);
            final View access$400 = this.mManageApplications.mSpinnerHeader;
            if (this.mFilterOptions.size() > 1) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            access$400.setVisibility(visibility);
            this.notifyDataSetChanged();
            if (this.mManageApplications.mFilter == value && this.mFilterOptions.size() > 0) {
                if (ManageApplications.DEBUG) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Auto selecting filter ");
                    sb2.append(this.mFilterOptions.get(0));
                    Log.d("ManageApplications", sb2.toString());
                }
                this.mManageApplications.mFilterSpinner.setSelection(0);
                this.mManageApplications.onItemSelected(null, null, 0, 0L);
            }
        }
        
        public void enableFilter(int visibility) {
            final AppFilterItem value = AppFilterRegistry.getInstance().get(visibility);
            if (this.mFilterOptions.contains(value)) {
                return;
            }
            if (ManageApplications.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Enabling filter ");
                sb.append(value);
                Log.d("ManageApplications", sb.toString());
            }
            this.mFilterOptions.add(value);
            Collections.sort(this.mFilterOptions);
            final View access$400 = this.mManageApplications.mSpinnerHeader;
            if (this.mFilterOptions.size() > 1) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            access$400.setVisibility(visibility);
            this.notifyDataSetChanged();
            if (this.mFilterOptions.size() == 1) {
                if (ManageApplications.DEBUG) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Auto selecting filter ");
                    sb2.append(value);
                    Log.d("ManageApplications", sb2.toString());
                }
                this.mManageApplications.mFilterSpinner.setSelection(0);
                this.mManageApplications.onItemSelected(null, null, 0, 0L);
            }
        }
        
        public int getCount() {
            return this.mFilterOptions.size();
        }
        
        public AppFilterItem getFilter(final int n) {
            return this.mFilterOptions.get(n);
        }
        
        public CharSequence getItem(final int n) {
            return this.mContext.getText(this.mFilterOptions.get(n).getTitle());
        }
        
        public void setFilterEnabled(final int n, final boolean b) {
            if (b) {
                this.enableFilter(n);
            }
            else {
                this.disableFilter(n);
            }
        }
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mLoader;
        
        private SummaryProvider(final Context mContext, final SummaryLoader mLoader) {
            this.mContext = mContext;
            this.mLoader = mLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                new InstalledAppCounter(this.mContext, -1, new PackageManagerWrapper(this.mContext.getPackageManager())) {
                    @Override
                    protected void onCountComplete(final int n) {
                        SummaryProvider.this.mLoader.setSummary((SummaryLoader.SummaryProvider)SummaryProvider.this, SummaryProvider.this.mContext.getString(2131886413, new Object[] { n }));
                    }
                }.execute((Object[])new Void[0]);
            }
        }
    }
}
