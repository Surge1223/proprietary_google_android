package com.android.settings.applications.manageapplications;

import android.util.Log;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import com.android.settings.applications.AppStateAppOpsBridge;
import com.android.settings.fuelgauge.HighPowerDetail;
import com.android.settings.applications.appinfo.DrawOverlayDetails;
import com.android.settings.applications.appinfo.WriteSettingsDetails;
import com.android.settings.applications.appinfo.ExternalSourcesDetails;
import com.android.settings.wifi.ChangeWifiStateDetails;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.utils.ThreadUtils;
import java.util.Comparator;
import com.android.settings.wifi.AppStateChangeWifiStateBridge;
import com.android.settings.applications.AppStateDirectoryAccessBridge;
import com.android.settings.applications.AppStateInstallAppsBridge;
import com.android.settings.applications.AppStateWriteSettingsBridge;
import com.android.settings.applications.AppStateOverlayBridge;
import com.android.settings.applications.AppStatePowerBridge;
import com.android.settings.applications.AppStateUsageBridge;
import com.android.settings.applications.AppStateNotificationBridge;
import android.os.Bundle;
import com.android.settings.widget.LoadingViewController;
import android.content.Context;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.applications.AppStateBaseBridge;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

public final class _$$Lambda$ManageApplications$ApplicationsAdapter$u5dJjouyXSv_EkAyCVJkIO0SEV0 implements Runnable
{
    @Override
    public final void run() {
        this.f$0.onRebuildComplete(this.f$1);
    }
}
