package com.android.settings.applications.manageapplications;

import android.text.format.Formatter;
import java.io.IOException;
import android.util.Log;
import com.android.settings.Utils;
import android.content.Intent;
import android.app.Fragment;
import android.os.UserHandle;
import com.android.settingslib.applications.StorageStatsSource;
import android.content.Context;

public class PhotosViewHolderController implements FileViewHolderController
{
    private Context mContext;
    private long mFilesSize;
    private StorageStatsSource mSource;
    private UserHandle mUser;
    private String mVolumeUuid;
    
    public PhotosViewHolderController(final Context mContext, final StorageStatsSource mSource, final String mVolumeUuid, final UserHandle mUser) {
        this.mContext = mContext;
        this.mSource = mSource;
        this.mVolumeUuid = mVolumeUuid;
        this.mUser = mUser;
    }
    
    @Override
    public void onClick(final Fragment fragment) {
        final Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(524288);
        intent.setType("image/*");
        intent.putExtra("android.intent.extra.FROM_STORAGE", true);
        Utils.launchIntent(fragment, intent);
    }
    
    @Override
    public void queryStats() {
        try {
            final StorageStatsSource.ExternalStorageStats externalStorageStats = this.mSource.getExternalStorageStats(this.mVolumeUuid, this.mUser);
            this.mFilesSize = externalStorageStats.imageBytes + externalStorageStats.videoBytes;
        }
        catch (IOException ex) {
            this.mFilesSize = 0L;
            Log.w("PhotosViewHolderCtrl", (Throwable)ex);
        }
    }
    
    @Override
    public void setupView(final ApplicationViewHolder applicationViewHolder) {
        applicationViewHolder.setIcon(2131231094);
        applicationViewHolder.setTitle(this.mContext.getText(2131889261));
        applicationViewHolder.setSummary(Formatter.formatFileSize(this.mContext, this.mFilesSize));
    }
    
    @Override
    public boolean shouldShow() {
        return true;
    }
}
