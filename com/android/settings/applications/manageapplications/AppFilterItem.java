package com.android.settings.applications.manageapplications;

import java.util.Objects;
import com.android.settingslib.applications.ApplicationsState;

public class AppFilterItem implements Comparable<AppFilterItem>
{
    private final ApplicationsState.AppFilter mFilter;
    private final int mFilterType;
    private final int mTitle;
    
    public AppFilterItem(final ApplicationsState.AppFilter mFilter, final int mFilterType, final int mTitle) {
        this.mTitle = mTitle;
        this.mFilterType = mFilterType;
        this.mFilter = mFilter;
    }
    
    @Override
    public int compareTo(final AppFilterItem appFilterItem) {
        if (appFilterItem == null) {
            return 1;
        }
        if (this == appFilterItem) {
            return 0;
        }
        return this.mFilterType - appFilterItem.mFilterType;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        if (o == null || !(o instanceof AppFilterItem)) {
            return false;
        }
        if (this == o) {
            return true;
        }
        final AppFilterItem appFilterItem = (AppFilterItem)o;
        boolean b2 = b;
        if (this.mTitle == appFilterItem.mTitle) {
            b2 = b;
            if (this.mFilterType == appFilterItem.mFilterType) {
                b2 = b;
                if (this.mFilter == appFilterItem.mFilter) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public ApplicationsState.AppFilter getFilter() {
        return this.mFilter;
    }
    
    public int getFilterType() {
        return this.mFilterType;
    }
    
    public int getTitle() {
        return this.mTitle;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.mFilter, this.mTitle, this.mFilterType);
    }
}
