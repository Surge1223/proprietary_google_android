package com.android.settings.applications.manageapplications;

import android.view.View.OnClickListener;
import android.text.TextUtils;
import android.util.Log;
import com.android.settingslib.applications.ApplicationsState;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Switch;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.support.v7.widget.RecyclerView;

public class ApplicationViewHolder extends ViewHolder
{
    private final ImageView mAppIcon;
    private final TextView mAppName;
    final TextView mDisabled;
    private final boolean mKeepStableHeight;
    final TextView mSummary;
    View mSummaryContainer;
    final Switch mSwitch;
    final ViewGroup mWidgetContainer;
    
    ApplicationViewHolder(final View view, final boolean mKeepStableHeight) {
        super(view);
        this.mAppName = (TextView)view.findViewById(16908310);
        this.mAppIcon = (ImageView)view.findViewById(16908294);
        this.mSummaryContainer = view.findViewById(2131362671);
        this.mSummary = (TextView)view.findViewById(16908304);
        this.mDisabled = (TextView)view.findViewById(2131361883);
        this.mKeepStableHeight = mKeepStableHeight;
        this.mSwitch = (Switch)view.findViewById(R.id.switchWidget);
        this.mWidgetContainer = (ViewGroup)view.findViewById(16908312);
    }
    
    static View newView(final ViewGroup viewGroup, final boolean b) {
        final ViewGroup viewGroup2 = (ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(2131558644, viewGroup, false);
        if (b) {
            final ViewGroup viewGroup3 = (ViewGroup)viewGroup2.findViewById(16908312);
            if (viewGroup3 != null) {
                LayoutInflater.from(viewGroup.getContext()).inflate(2131558683, viewGroup3, true);
                viewGroup2.addView(LayoutInflater.from(viewGroup.getContext()).inflate(2131558674, viewGroup2, false), viewGroup2.getChildCount() - 1);
            }
        }
        return (View)viewGroup2;
    }
    
    void setEnabled(final boolean enabled) {
        this.itemView.setEnabled(enabled);
    }
    
    void setIcon(final int imageResource) {
        this.mAppIcon.setImageResource(imageResource);
    }
    
    void setIcon(final Drawable imageDrawable) {
        if (imageDrawable == null) {
            return;
        }
        this.mAppIcon.setImageDrawable(imageDrawable);
    }
    
    void setSummary(final int text) {
        this.mSummary.setText(text);
        this.updateSummaryContainer();
    }
    
    void setSummary(final CharSequence text) {
        this.mSummary.setText(text);
        this.updateSummaryContainer();
    }
    
    void setTitle(final CharSequence text) {
        if (text == null) {
            return;
        }
        this.mAppName.setText(text);
    }
    
    void updateDisableView(final ApplicationInfo applicationInfo) {
        if ((applicationInfo.flags & 0x800000) == 0x0) {
            this.mDisabled.setVisibility(0);
            this.mDisabled.setText(2131888426);
        }
        else if (applicationInfo.enabled && applicationInfo.enabledSetting != 4) {
            this.mDisabled.setVisibility(8);
        }
        else {
            this.mDisabled.setVisibility(0);
            this.mDisabled.setText(2131887422);
        }
        this.updateSummaryContainer();
    }
    
    void updateSizeText(final ApplicationsState.AppEntry appEntry, final CharSequence summary, final int n) {
        if (ManageApplications.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("updateSizeText of ");
            sb.append(appEntry.label);
            sb.append(" ");
            sb.append(appEntry);
            sb.append(": ");
            sb.append(appEntry.sizeStr);
            Log.d("ManageApplications", sb.toString());
        }
        if (appEntry.sizeStr != null) {
            switch (n) {
                default: {
                    this.setSummary(appEntry.sizeStr);
                    break;
                }
                case 2: {
                    this.setSummary(appEntry.externalSizeStr);
                    break;
                }
                case 1: {
                    this.setSummary(appEntry.internalSizeStr);
                    break;
                }
            }
        }
        else if (appEntry.size == -2L) {
            this.setSummary(summary);
        }
    }
    
    void updateSummaryContainer() {
        final boolean mKeepStableHeight = this.mKeepStableHeight;
        final boolean b = false;
        if (mKeepStableHeight) {
            this.mSummaryContainer.setVisibility(0);
            return;
        }
        final boolean b2 = !TextUtils.isEmpty(this.mDisabled.getText()) || !TextUtils.isEmpty(this.mSummary.getText());
        final View mSummaryContainer = this.mSummaryContainer;
        int visibility;
        if (b2) {
            visibility = (b ? 1 : 0);
        }
        else {
            visibility = 8;
        }
        mSummaryContainer.setVisibility(visibility);
    }
    
    void updateSwitch(final View.OnClickListener onClickListener, final boolean enabled, final boolean checked) {
        if (this.mSwitch != null && this.mWidgetContainer != null) {
            this.mWidgetContainer.setOnClickListener(onClickListener);
            this.mSwitch.setChecked(checked);
            this.mSwitch.setEnabled(enabled);
        }
    }
}
