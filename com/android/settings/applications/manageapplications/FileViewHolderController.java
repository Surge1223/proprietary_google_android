package com.android.settings.applications.manageapplications;

import android.app.Fragment;

public interface FileViewHolderController
{
    void onClick(final Fragment p0);
    
    void queryStats();
    
    void setupView(final ApplicationViewHolder p0);
    
    boolean shouldShow();
}
