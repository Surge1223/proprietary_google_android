package com.android.settings.applications;

import android.app.Activity;
import com.android.settings.notification.EmergencyBroadcastPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import android.app.Application;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class AppAndNotificationDashboardFragment extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add(new SpecialAppAccessPreferenceController(context).getPreferenceKey());
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082706;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Application application, final Fragment fragment) {
        final ArrayList<SpecialAppAccessPreferenceController> list = (ArrayList<SpecialAppAccessPreferenceController>)new ArrayList<RecentAppsPreferenceController>();
        list.add((RecentAppsPreferenceController)new EmergencyBroadcastPreferenceController(context, "app_and_notif_cell_broadcast_settings"));
        list.add((RecentAppsPreferenceController)new SpecialAppAccessPreferenceController(context));
        list.add(new RecentAppsPreferenceController(context, application, fragment));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final Activity activity = this.getActivity();
        Application application;
        if (activity != null) {
            application = activity.getApplication();
        }
        else {
            application = null;
        }
        return buildPreferenceControllers(context, application, this);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887794;
    }
    
    @Override
    protected String getLogTag() {
        return "AppAndNotifDashboard";
    }
    
    @Override
    public int getMetricsCategory() {
        return 748;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082706;
    }
}
