package com.android.settings.applications;

import android.content.pm.ApplicationInfo;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;

public abstract class InstalledAppLister extends AppLister
{
    public InstalledAppLister(final PackageManagerWrapper packageManagerWrapper, final UserManager userManager) {
        super(packageManagerWrapper, userManager);
    }
    
    @Override
    protected boolean includeInCount(final ApplicationInfo applicationInfo) {
        return InstalledAppCounter.includeInCount(1, this.mPm, applicationInfo);
    }
}
