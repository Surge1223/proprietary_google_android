package com.android.settings.applications;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class DeviceAdministratorsController extends BasePreferenceController
{
    static final String KEY_DEVICE_ADMIN = "device_administrators";
    
    public DeviceAdministratorsController(final Context context) {
        super(context, "device_administrators");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034141)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
