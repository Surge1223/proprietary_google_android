package com.android.settings.applications.assist;

import com.android.settings.gestures.AssistGestureSettingsPreferenceController;
import java.util.ArrayList;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class ManageAssist extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null);
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("gesture_assist_application");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082787;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<AssistContextPreferenceController> list = (ArrayList<AssistContextPreferenceController>)new ArrayList<DefaultVoiceInputPreferenceController>();
        list.add((DefaultVoiceInputPreferenceController)new DefaultAssistPreferenceController(context, "default_assist", true));
        list.add((DefaultVoiceInputPreferenceController)new AssistContextPreferenceController(context, lifecycle));
        list.add((DefaultVoiceInputPreferenceController)new AssistScreenshotPreferenceController(context, lifecycle));
        list.add((DefaultVoiceInputPreferenceController)new AssistFlashScreenPreferenceController(context, lifecycle));
        list.add(new DefaultVoiceInputPreferenceController(context, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    protected String getLogTag() {
        return "ManageAssist";
    }
    
    @Override
    public int getMetricsCategory() {
        return 201;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082787;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.use(AssistGestureSettingsPreferenceController.class).setAssistOnly(true);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mFooterPreferenceMixin.createFooterPreference().setTitle(2131886425);
    }
}
