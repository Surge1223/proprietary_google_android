package com.android.settings.applications.assist;

import android.content.Context;
import com.android.settingslib.widget.CandidateInfo;
import com.android.settingslib.applications.DefaultAppInfo;
import android.provider.Settings;
import android.util.Log;
import android.text.TextUtils;
import android.content.pm.PackageManager;
import android.service.voice.VoiceInteractionServiceInfo;
import java.util.Iterator;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import java.util.ArrayList;
import java.util.List;
import com.android.internal.app.AssistUtils;
import android.content.Intent;
import com.android.settings.applications.defaultapps.DefaultAppPickerFragment;

public class DefaultAssistPicker extends DefaultAppPickerFragment
{
    private static final Intent ASSIST_ACTIVITY_PROBE;
    private static final Intent ASSIST_SERVICE_PROBE;
    private AssistUtils mAssistUtils;
    private final List<Info> mAvailableAssistants;
    
    static {
        ASSIST_SERVICE_PROBE = new Intent("android.service.voice.VoiceInteractionService");
        ASSIST_ACTIVITY_PROBE = new Intent("android.intent.action.ASSIST");
    }
    
    public DefaultAssistPicker() {
        this.mAvailableAssistants = new ArrayList<Info>();
    }
    
    private void addAssistActivities() {
        for (final ResolveInfo resolveInfo : this.mPm.getPackageManager().queryIntentActivities(DefaultAssistPicker.ASSIST_ACTIVITY_PROBE, 65536)) {
            this.mAvailableAssistants.add(new Info(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name)));
        }
    }
    
    private void addAssistServices() {
        final PackageManager packageManager = this.mPm.getPackageManager();
        for (final ResolveInfo resolveInfo : packageManager.queryIntentServices(DefaultAssistPicker.ASSIST_SERVICE_PROBE, 128)) {
            final VoiceInteractionServiceInfo voiceInteractionServiceInfo = new VoiceInteractionServiceInfo(packageManager, resolveInfo.serviceInfo);
            if (!voiceInteractionServiceInfo.getSupportsAssist()) {
                continue;
            }
            this.mAvailableAssistants.add(new Info(new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name), voiceInteractionServiceInfo));
        }
    }
    
    private Info findAssistantByPackageName(final String s) {
        for (final Info info : this.mAvailableAssistants) {
            if (TextUtils.equals((CharSequence)info.component.getPackageName(), (CharSequence)s)) {
                return info;
            }
        }
        return null;
    }
    
    private String getDefaultRecognizer() {
        final ResolveInfo resolveService = this.mPm.getPackageManager().resolveService(new Intent("android.speech.RecognitionService"), 128);
        if (resolveService != null && resolveService.serviceInfo != null) {
            return new ComponentName(resolveService.serviceInfo.packageName, resolveService.serviceInfo.name).flattenToShortString();
        }
        Log.w("DefaultAssistPicker", "Unable to resolve default voice recognition service.");
        return "";
    }
    
    private void setAssistActivity(final Info info) {
        Settings.Secure.putString(this.getContext().getContentResolver(), "assistant", info.component.flattenToShortString());
        Settings.Secure.putString(this.getContext().getContentResolver(), "voice_interaction_service", "");
        Settings.Secure.putString(this.getContext().getContentResolver(), "voice_recognition_service", this.getDefaultRecognizer());
    }
    
    private void setAssistNone() {
        Settings.Secure.putString(this.getContext().getContentResolver(), "assistant", "");
        Settings.Secure.putString(this.getContext().getContentResolver(), "voice_interaction_service", "");
        Settings.Secure.putString(this.getContext().getContentResolver(), "voice_recognition_service", this.getDefaultRecognizer());
    }
    
    private void setAssistService(final Info info) {
        final String flattenToShortString = info.component.flattenToShortString();
        final String flattenToShortString2 = new ComponentName(info.component.getPackageName(), info.voiceInteractionServiceInfo.getRecognitionService()).flattenToShortString();
        Settings.Secure.putString(this.getContext().getContentResolver(), "assistant", flattenToShortString);
        Settings.Secure.putString(this.getContext().getContentResolver(), "voice_interaction_service", flattenToShortString);
        Settings.Secure.putString(this.getContext().getContentResolver(), "voice_recognition_service", flattenToShortString2);
    }
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        this.mAvailableAssistants.clear();
        this.addAssistServices();
        this.addAssistActivities();
        final ArrayList<String> list = new ArrayList<String>();
        final ArrayList<DefaultAppInfo> list2 = new ArrayList<DefaultAppInfo>();
        for (final Info info : this.mAvailableAssistants) {
            final String packageName = info.component.getPackageName();
            if (list.contains(packageName)) {
                continue;
            }
            list.add(packageName);
            list2.add(new DefaultAppInfo(this.getContext(), this.mPm, this.mUserId, info.component));
        }
        return list2;
    }
    
    @Override
    protected String getConfirmationMessage(final CandidateInfo candidateInfo) {
        if (candidateInfo == null) {
            return null;
        }
        return this.getContext().getString(2131886457, new Object[] { candidateInfo.loadLabel() });
    }
    
    public ComponentName getCurrentAssist() {
        return this.mAssistUtils.getAssistComponentForUser(this.mUserId);
    }
    
    @Override
    protected String getDefaultKey() {
        final ComponentName currentAssist = this.getCurrentAssist();
        if (currentAssist != null) {
            return new DefaultAppInfo(this.getContext(), this.mPm, this.mUserId, currentAssist).getKey();
        }
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 843;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082747;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mAssistUtils = new AssistUtils(context);
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            this.setAssistNone();
            return true;
        }
        final Info assistantByPackageName = this.findAssistantByPackageName(ComponentName.unflattenFromString(s).getPackageName());
        if (assistantByPackageName == null) {
            this.setAssistNone();
            return true;
        }
        if (assistantByPackageName.isVoiceInteractionService()) {
            this.setAssistService(assistantByPackageName);
        }
        else {
            this.setAssistActivity(assistantByPackageName);
        }
        return true;
    }
    
    @Override
    protected boolean shouldShowItemNone() {
        return true;
    }
    
    static class Info
    {
        public final ComponentName component;
        public final VoiceInteractionServiceInfo voiceInteractionServiceInfo;
        
        Info(final ComponentName component) {
            this.component = component;
            this.voiceInteractionServiceInfo = null;
        }
        
        Info(final ComponentName component, final VoiceInteractionServiceInfo voiceInteractionServiceInfo) {
            this.component = component;
            this.voiceInteractionServiceInfo = voiceInteractionServiceInfo;
        }
        
        public boolean isVoiceInteractionService() {
            return this.voiceInteractionServiceInfo != null;
        }
    }
}
