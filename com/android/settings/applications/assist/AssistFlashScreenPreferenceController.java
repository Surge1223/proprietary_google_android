package com.android.settings.applications.assist;

import java.util.Arrays;
import java.util.List;
import android.net.Uri;
import android.provider.Settings;
import com.android.internal.annotations.VisibleForTesting;
import android.support.v7.preference.TwoStatePreference;
import android.os.UserHandle;
import android.content.ComponentName;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.PreferenceScreen;
import com.android.internal.app.AssistUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class AssistFlashScreenPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin, LifecycleObserver, OnPause, OnResume
{
    private final AssistUtils mAssistUtils;
    private Preference mPreference;
    private PreferenceScreen mScreen;
    private final SettingObserver mSettingObserver;
    
    public AssistFlashScreenPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mAssistUtils = new AssistUtils(context);
        this.mSettingObserver = new SettingObserver();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private ComponentName getCurrentAssist() {
        return this.mAssistUtils.getAssistComponentForUser(UserHandle.myUserId());
    }
    
    private void updatePreference() {
        if (this.mPreference != null && this.mPreference instanceof TwoStatePreference) {
            if (this.isAvailable()) {
                if (this.mScreen.findPreference(this.getPreferenceKey()) == null) {
                    this.mScreen.addPreference(this.mPreference);
                }
            }
            else {
                this.mScreen.removePreference(this.mPreference);
            }
            final ComponentName currentAssist = this.getCurrentAssist();
            this.mPreference.setEnabled(AssistContextPreferenceController.isChecked(this.mContext) && this.isPreInstalledAssistant(currentAssist));
            ((TwoStatePreference)this.mPreference).setChecked(this.willShowFlash(currentAssist));
        }
    }
    
    @VisibleForTesting
    boolean allowDisablingAssistDisclosure() {
        return AssistUtils.allowDisablingAssistDisclosure(this.mContext);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        this.mScreen = mScreen;
        this.mPreference = mScreen.findPreference(this.getPreferenceKey());
        super.displayPreference(mScreen);
    }
    
    @Override
    public String getPreferenceKey() {
        return "flash";
    }
    
    @Override
    public boolean isAvailable() {
        return this.getCurrentAssist() != null && this.allowDisablingAssistDisclosure();
    }
    
    @VisibleForTesting
    boolean isPreInstalledAssistant(final ComponentName componentName) {
        return AssistUtils.isPreinstalledAssistant(this.mContext, componentName);
    }
    
    @Override
    public void onPause() {
        this.mSettingObserver.register(this.mContext.getContentResolver(), false);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "assist_disclosure_enabled", (int)(((boolean)o) ? 1 : 0));
        return true;
    }
    
    @Override
    public void onResume() {
        this.mSettingObserver.register(this.mContext.getContentResolver(), true);
        this.updatePreference();
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updatePreference();
    }
    
    @VisibleForTesting
    boolean willShowFlash(final ComponentName componentName) {
        return AssistUtils.shouldDisclose(this.mContext, componentName);
    }
    
    class SettingObserver extends AssistSettingObserver
    {
        private final Uri CONTEXT_URI;
        private final Uri URI;
        
        SettingObserver() {
            this.URI = Settings.Secure.getUriFor("assist_disclosure_enabled");
            this.CONTEXT_URI = Settings.Secure.getUriFor("assist_structure_enabled");
        }
        
        @Override
        protected List<Uri> getSettingUris() {
            return Arrays.asList(this.URI, this.CONTEXT_URI);
        }
        
        @Override
        public void onSettingChange() {
            AssistFlashScreenPreferenceController.this.updatePreference();
        }
    }
}
