package com.android.settings.applications.assist;

import java.util.Iterator;
import android.content.ContentResolver;
import com.android.settingslib.utils.ThreadUtils;
import java.util.List;
import android.provider.Settings;
import android.os.Handler;
import android.net.Uri;
import android.database.ContentObserver;

public abstract class AssistSettingObserver extends ContentObserver
{
    private final Uri ASSIST_URI;
    
    public AssistSettingObserver() {
        super((Handler)null);
        this.ASSIST_URI = Settings.Secure.getUriFor("assistant");
    }
    
    protected abstract List<Uri> getSettingUris();
    
    public void onChange(final boolean b, final Uri uri) {
        super.onChange(b, uri);
        final boolean b2 = false;
        final List<Uri> settingUris = this.getSettingUris();
        boolean b3 = false;
        Label_0050: {
            if (!this.ASSIST_URI.equals((Object)uri)) {
                b3 = b2;
                if (settingUris == null) {
                    break Label_0050;
                }
                b3 = b2;
                if (!settingUris.contains(uri)) {
                    break Label_0050;
                }
            }
            b3 = true;
        }
        if (b3) {
            ThreadUtils.postOnMainThread(new _$$Lambda$AssistSettingObserver$iBFvDXS30QMXzEK_zAgHqcs78mE(this));
        }
    }
    
    public abstract void onSettingChange();
    
    public void register(final ContentResolver contentResolver, final boolean b) {
        if (b) {
            contentResolver.registerContentObserver(this.ASSIST_URI, false, (ContentObserver)this);
            final List<Uri> settingUris = this.getSettingUris();
            if (settingUris != null) {
                final Iterator<Uri> iterator = settingUris.iterator();
                while (iterator.hasNext()) {
                    contentResolver.registerContentObserver((Uri)iterator.next(), false, (ContentObserver)this);
                }
            }
        }
        else {
            contentResolver.unregisterContentObserver((ContentObserver)this);
        }
    }
}
