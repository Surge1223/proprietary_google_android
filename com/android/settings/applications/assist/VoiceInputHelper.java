package com.android.settings.applications.assist;

import android.content.res.XmlResourceParser;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.res.Resources;
import android.content.pm.ServiceInfo;
import java.io.IOException;
import android.content.pm.PackageManager;
import com.android.internal.R$styleable;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParserException;
import java.util.Collections;
import android.util.Log;
import android.service.voice.VoiceInteractionServiceInfo;
import android.util.ArraySet;
import android.provider.Settings;
import android.content.Intent;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ResolveInfo;
import java.util.List;
import java.util.ArrayList;

public final class VoiceInputHelper
{
    final ArrayList<InteractionInfo> mAvailableInteractionInfos;
    final List<ResolveInfo> mAvailableRecognition;
    final ArrayList<RecognizerInfo> mAvailableRecognizerInfos;
    final List<ResolveInfo> mAvailableVoiceInteractions;
    final Context mContext;
    ComponentName mCurrentRecognizer;
    ComponentName mCurrentVoiceInteraction;
    
    public VoiceInputHelper(final Context mContext) {
        this.mAvailableInteractionInfos = new ArrayList<InteractionInfo>();
        this.mAvailableRecognizerInfos = new ArrayList<RecognizerInfo>();
        this.mContext = mContext;
        this.mAvailableVoiceInteractions = (List<ResolveInfo>)this.mContext.getPackageManager().queryIntentServices(new Intent("android.service.voice.VoiceInteractionService"), 128);
        this.mAvailableRecognition = (List<ResolveInfo>)this.mContext.getPackageManager().queryIntentServices(new Intent("android.speech.RecognitionService"), 128);
    }
    
    public void buildUi() {
        final String string = Settings.Secure.getString(this.mContext.getContentResolver(), "voice_interaction_service");
        if (string != null && !string.isEmpty()) {
            this.mCurrentVoiceInteraction = ComponentName.unflattenFromString(string);
        }
        else {
            this.mCurrentVoiceInteraction = null;
        }
        final ArraySet set = new ArraySet();
        for (int size = this.mAvailableVoiceInteractions.size(), i = 0; i < size; ++i) {
            final ResolveInfo resolveInfo = this.mAvailableVoiceInteractions.get(i);
            final VoiceInteractionServiceInfo voiceInteractionServiceInfo = new VoiceInteractionServiceInfo(this.mContext.getPackageManager(), resolveInfo.serviceInfo);
            if (voiceInteractionServiceInfo.getParseError() != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error in VoiceInteractionService ");
                sb.append(resolveInfo.serviceInfo.packageName);
                sb.append("/");
                sb.append(resolveInfo.serviceInfo.name);
                sb.append(": ");
                sb.append(voiceInteractionServiceInfo.getParseError());
                Log.w("VoiceInteractionService", sb.toString());
            }
            else {
                this.mAvailableInteractionInfos.add(new InteractionInfo(this.mContext.getPackageManager(), voiceInteractionServiceInfo));
                set.add((Object)new ComponentName(resolveInfo.serviceInfo.packageName, voiceInteractionServiceInfo.getRecognitionService()));
            }
        }
        Collections.sort(this.mAvailableInteractionInfos);
        final String string2 = Settings.Secure.getString(this.mContext.getContentResolver(), "voice_recognition_service");
        if (string2 != null && !string2.isEmpty()) {
            this.mCurrentRecognizer = ComponentName.unflattenFromString(string2);
        }
        else {
            this.mCurrentRecognizer = null;
        }
        final int size2 = this.mAvailableRecognition.size();
        int j = 0;
    Label_0703_Outer:
        while (j < size2) {
            final ResolveInfo resolveInfo2 = this.mAvailableRecognition.get(j);
            set.contains((Object)new ComponentName(resolveInfo2.serviceInfo.packageName, resolveInfo2.serviceInfo.name));
            final ServiceInfo serviceInfo = resolveInfo2.serviceInfo;
            Object o = null;
            Object o2 = null;
            Object o3 = null;
            Object o4 = null;
            final String s = null;
            final String s2 = null;
            String s4;
            final String s3 = s4 = null;
            String s5 = s;
            String s6 = s2;
            while (true) {
                Label_1149: {
                    try {
                        final Object loadXmlMetaData = serviceInfo.loadXmlMetaData(this.mContext.getPackageManager(), "android.speech");
                        if (loadXmlMetaData == null) {
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            final StringBuilder sb2 = new StringBuilder();
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            sb2.append("No android.speech meta-data for ");
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            sb2.append(serviceInfo.packageName);
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            final XmlPullParserException ex = new XmlPullParserException(sb2.toString());
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            throw ex;
                        }
                        o4 = loadXmlMetaData;
                        o = loadXmlMetaData;
                        s4 = s3;
                        o2 = loadXmlMetaData;
                        s5 = s;
                        o3 = loadXmlMetaData;
                        s6 = s2;
                        final Resources resourcesForApplication = this.mContext.getPackageManager().getResourcesForApplication(serviceInfo.applicationInfo);
                        o4 = loadXmlMetaData;
                        o = loadXmlMetaData;
                        s4 = s3;
                        o2 = loadXmlMetaData;
                        s5 = s;
                        o3 = loadXmlMetaData;
                        s6 = s2;
                        final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)loadXmlMetaData);
                        int next;
                        do {
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            next = ((XmlResourceParser)loadXmlMetaData).next();
                        } while (next != 1 && next != 2);
                        o4 = loadXmlMetaData;
                        o = loadXmlMetaData;
                        s4 = s3;
                        o2 = loadXmlMetaData;
                        s5 = s;
                        o3 = loadXmlMetaData;
                        s6 = s2;
                        if (!"recognition-service".equals(((XmlResourceParser)loadXmlMetaData).getName())) {
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            final XmlPullParserException ex2 = new XmlPullParserException("Meta-data does not start with recognition-service tag");
                            o4 = loadXmlMetaData;
                            o = loadXmlMetaData;
                            s4 = s3;
                            o2 = loadXmlMetaData;
                            s5 = s;
                            o3 = loadXmlMetaData;
                            s6 = s2;
                            throw ex2;
                        }
                        o4 = loadXmlMetaData;
                        o = loadXmlMetaData;
                        s4 = s3;
                        o2 = loadXmlMetaData;
                        s5 = s;
                        o3 = loadXmlMetaData;
                        s6 = s2;
                        final TypedArray obtainAttributes = resourcesForApplication.obtainAttributes(attributeSet, R$styleable.RecognitionService);
                        o4 = loadXmlMetaData;
                        o = loadXmlMetaData;
                        s4 = s3;
                        o2 = loadXmlMetaData;
                        s5 = s;
                        o3 = loadXmlMetaData;
                        s6 = s2;
                        final String string3 = obtainAttributes.getString(0);
                        o4 = loadXmlMetaData;
                        o = loadXmlMetaData;
                        s4 = string3;
                        o2 = loadXmlMetaData;
                        s5 = string3;
                        o3 = loadXmlMetaData;
                        s6 = string3;
                        obtainAttributes.recycle();
                        o4 = string3;
                        if (loadXmlMetaData != null) {
                            o4 = string3;
                            ((XmlResourceParser)loadXmlMetaData).close();
                        }
                        break Label_1149;
                    }
                    catch (PackageManager$NameNotFoundException ex3) {}
                    catch (IOException ex4) {}
                    catch (XmlPullParserException ex5) {}
                    finally {
                        if (o4 != null) {
                            ((XmlResourceParser)o4).close();
                        }
                        Object loadXmlMetaData = o;
                        o4 = s4;
                        continue;
                        loadXmlMetaData = o3;
                        o4 = s6;
                        continue;
                        this.mAvailableRecognizerInfos.add(new RecognizerInfo(this.mContext.getPackageManager(), resolveInfo2.serviceInfo, (String)o4));
                        ++j;
                        continue Label_0703_Outer;
                        loadXmlMetaData = o2;
                        o4 = s5;
                        continue;
                    }
                }
                break;
            }
            break;
        }
        Collections.sort(this.mAvailableRecognizerInfos);
    }
    
    public static class BaseInfo implements Comparable
    {
        public final CharSequence appLabel;
        public final ComponentName componentName;
        public final String key;
        public final CharSequence label;
        public final String labelStr;
        public final ServiceInfo service;
        public final ComponentName settings;
        
        public BaseInfo(final PackageManager packageManager, final ServiceInfo service, final String s) {
            this.service = service;
            this.componentName = new ComponentName(service.packageName, service.name);
            this.key = this.componentName.flattenToShortString();
            ComponentName settings;
            if (s != null) {
                settings = new ComponentName(service.packageName, s);
            }
            else {
                settings = null;
            }
            this.settings = settings;
            this.label = service.loadLabel(packageManager);
            this.labelStr = this.label.toString();
            this.appLabel = service.applicationInfo.loadLabel(packageManager);
        }
        
        @Override
        public int compareTo(final Object o) {
            return this.labelStr.compareTo(((BaseInfo)o).labelStr);
        }
    }
    
    public static class InteractionInfo extends BaseInfo
    {
        public final VoiceInteractionServiceInfo serviceInfo;
        
        public InteractionInfo(final PackageManager packageManager, final VoiceInteractionServiceInfo serviceInfo) {
            super(packageManager, serviceInfo.getServiceInfo(), serviceInfo.getSettingsActivity());
            this.serviceInfo = serviceInfo;
        }
    }
    
    public static class RecognizerInfo extends BaseInfo
    {
        public RecognizerInfo(final PackageManager packageManager, final ServiceInfo serviceInfo, final String s) {
            super(packageManager, serviceInfo, s);
        }
    }
}
