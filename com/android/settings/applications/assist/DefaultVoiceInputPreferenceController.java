package com.android.settings.applications.assist;

import android.net.Uri;
import java.util.List;
import android.content.Intent;
import java.util.Iterator;
import android.text.TextUtils;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.ComponentName;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import com.android.internal.app.AssistUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.applications.defaultapps.DefaultAppPreferenceController;

public class DefaultVoiceInputPreferenceController extends DefaultAppPreferenceController implements LifecycleObserver, OnPause, OnResume
{
    private AssistUtils mAssistUtils;
    private VoiceInputHelper mHelper;
    private Preference mPreference;
    private PreferenceScreen mScreen;
    private SettingObserver mSettingObserver;
    
    public DefaultVoiceInputPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mSettingObserver = new SettingObserver();
        this.mAssistUtils = new AssistUtils(context);
        (this.mHelper = new VoiceInputHelper(context)).buildUi();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private String getDefaultAppKey() {
        final ComponentName currentService = DefaultVoiceInputPicker.getCurrentService(this.mHelper);
        if (currentService == null) {
            return null;
        }
        return currentService.flattenToShortString();
    }
    
    private void updatePreference() {
        if (this.mPreference == null) {
            return;
        }
        this.mHelper.buildUi();
        if (this.isAvailable()) {
            if (this.mScreen.findPreference(this.getPreferenceKey()) == null) {
                this.mScreen.addPreference(this.mPreference);
            }
        }
        else {
            this.mScreen.removePreference(this.mPreference);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen mScreen) {
        this.mScreen = mScreen;
        this.mPreference = mScreen.findPreference(this.getPreferenceKey());
        super.displayPreference(mScreen);
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        final String defaultAppKey = this.getDefaultAppKey();
        if (defaultAppKey == null) {
            return null;
        }
        for (final VoiceInputHelper.InteractionInfo interactionInfo : this.mHelper.mAvailableInteractionInfos) {
            if (TextUtils.equals((CharSequence)defaultAppKey, (CharSequence)interactionInfo.key)) {
                return new DefaultVoiceInputPicker.VoiceInputDefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, interactionInfo, true);
            }
        }
        for (final VoiceInputHelper.RecognizerInfo recognizerInfo : this.mHelper.mAvailableRecognizerInfos) {
            if (TextUtils.equals((CharSequence)defaultAppKey, (CharSequence)recognizerInfo.key)) {
                return new DefaultVoiceInputPicker.VoiceInputDefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, recognizerInfo, true);
            }
        }
        return null;
    }
    
    @Override
    public String getPreferenceKey() {
        return "voice_input_settings";
    }
    
    @Override
    protected Intent getSettingIntent(DefaultAppInfo defaultAppInfo) {
        defaultAppInfo = this.getDefaultAppInfo();
        if (defaultAppInfo != null && defaultAppInfo instanceof DefaultVoiceInputPicker.VoiceInputDefaultAppInfo) {
            return ((DefaultVoiceInputPicker.VoiceInputDefaultAppInfo)defaultAppInfo).getSettingIntent();
        }
        return null;
    }
    
    @Override
    public boolean isAvailable() {
        return DefaultVoiceInputPicker.isCurrentAssistVoiceService(this.mAssistUtils.getAssistComponentForUser(this.mUserId), DefaultVoiceInputPicker.getCurrentService(this.mHelper)) ^ true;
    }
    
    @Override
    public void onPause() {
        this.mSettingObserver.register(this.mContext.getContentResolver(), false);
    }
    
    @Override
    public void onResume() {
        this.mSettingObserver.register(this.mContext.getContentResolver(), true);
        this.updatePreference();
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(this.mPreference);
        this.updatePreference();
    }
    
    class SettingObserver extends AssistSettingObserver
    {
        @Override
        protected List<Uri> getSettingUris() {
            return null;
        }
        
        @Override
        public void onSettingChange() {
            DefaultVoiceInputPreferenceController.this.updatePreference();
        }
    }
}
