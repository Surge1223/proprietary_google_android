package com.android.settings.applications.assist;

import java.util.List;
import android.content.Intent;
import com.android.settingslib.applications.DefaultAppInfo;
import android.service.voice.VoiceInteractionServiceInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.ComponentName;
import android.content.Context;
import com.android.internal.app.AssistUtils;
import com.android.settings.applications.defaultapps.DefaultAppPreferenceController;

public class DefaultAssistPreferenceController extends DefaultAppPreferenceController
{
    private final AssistUtils mAssistUtils;
    private final String mPrefKey;
    private final boolean mShowSetting;
    
    public DefaultAssistPreferenceController(final Context context, final String mPrefKey, final boolean mShowSetting) {
        super(context);
        this.mPrefKey = mPrefKey;
        this.mShowSetting = mShowSetting;
        this.mAssistUtils = new AssistUtils(context);
    }
    
    String getAssistSettingsActivity(final ComponentName componentName, final ResolveInfo resolveInfo, final PackageManager packageManager) {
        final VoiceInteractionServiceInfo voiceInteractionServiceInfo = new VoiceInteractionServiceInfo(packageManager, resolveInfo.serviceInfo);
        if (!voiceInteractionServiceInfo.getSupportsAssist()) {
            return null;
        }
        return voiceInteractionServiceInfo.getSettingsActivity();
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        final ComponentName assistComponentForUser = this.mAssistUtils.getAssistComponentForUser(this.mUserId);
        if (assistComponentForUser == null) {
            return null;
        }
        return new DefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, assistComponentForUser);
    }
    
    @Override
    public String getPreferenceKey() {
        return this.mPrefKey;
    }
    
    @Override
    protected Intent getSettingIntent(final DefaultAppInfo defaultAppInfo) {
        if (!this.mShowSetting) {
            return null;
        }
        final ComponentName assistComponentForUser = this.mAssistUtils.getAssistComponentForUser(this.mUserId);
        if (assistComponentForUser == null) {
            return null;
        }
        final Intent setPackage = new Intent("android.service.voice.VoiceInteractionService").setPackage(assistComponentForUser.getPackageName());
        final PackageManager packageManager = this.mPackageManager.getPackageManager();
        final List queryIntentServices = packageManager.queryIntentServices(setPackage, 128);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            return null;
        }
        final String assistSettingsActivity = this.getAssistSettingsActivity(assistComponentForUser, queryIntentServices.get(0), packageManager);
        if (assistSettingsActivity == null) {
            return null;
        }
        return new Intent("android.intent.action.MAIN").setComponent(new ComponentName(assistComponentForUser.getPackageName(), assistSettingsActivity));
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034135);
    }
}
