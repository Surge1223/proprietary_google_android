package com.android.settings.applications.assist;

import android.content.Intent;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settingslib.applications.DefaultAppInfo;
import android.provider.Settings;
import java.util.Iterator;
import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import android.content.ComponentName;
import com.android.internal.app.AssistUtils;
import com.android.settings.applications.defaultapps.DefaultAppPickerFragment;

public class DefaultVoiceInputPicker extends DefaultAppPickerFragment
{
    private String mAssistRestrict;
    private AssistUtils mAssistUtils;
    private VoiceInputHelper mHelper;
    
    private ComponentName getCurrentAssist() {
        return this.mAssistUtils.getAssistComponentForUser(this.mUserId);
    }
    
    public static ComponentName getCurrentService(final VoiceInputHelper voiceInputHelper) {
        if (voiceInputHelper.mCurrentVoiceInteraction != null) {
            return voiceInputHelper.mCurrentVoiceInteraction;
        }
        if (voiceInputHelper.mCurrentRecognizer != null) {
            return voiceInputHelper.mCurrentRecognizer;
        }
        return null;
    }
    
    public static boolean isCurrentAssistVoiceService(final ComponentName componentName, final ComponentName componentName2) {
        return (componentName == null && componentName2 == null) || (componentName != null && componentName.equals((Object)componentName2));
    }
    
    @Override
    protected List<VoiceInputDefaultAppInfo> getCandidates() {
        final ArrayList<VoiceInputDefaultAppInfo> list = new ArrayList<VoiceInputDefaultAppInfo>();
        final Context context = this.getContext();
        final Iterator<VoiceInputHelper.InteractionInfo> iterator = this.mHelper.mAvailableInteractionInfos.iterator();
        boolean b = true;
        while (iterator.hasNext()) {
            final VoiceInputHelper.InteractionInfo interactionInfo = iterator.next();
            final boolean equals = TextUtils.equals((CharSequence)interactionInfo.key, (CharSequence)this.mAssistRestrict);
            b |= equals;
            list.add(new VoiceInputDefaultAppInfo(context, this.mPm, this.mUserId, interactionInfo, equals));
        }
        final boolean b2 = !b;
        final Iterator<VoiceInputHelper.RecognizerInfo> iterator2 = this.mHelper.mAvailableRecognizerInfos.iterator();
        while (iterator2.hasNext()) {
            list.add(new VoiceInputDefaultAppInfo(context, this.mPm, this.mUserId, iterator2.next(), !b2));
        }
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        final ComponentName currentService = getCurrentService(this.mHelper);
        if (currentService == null) {
            return null;
        }
        return currentService.flattenToShortString();
    }
    
    @Override
    public int getMetricsCategory() {
        return 844;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082754;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mAssistUtils = new AssistUtils(context);
        (this.mHelper = new VoiceInputHelper(context)).buildUi();
        final ComponentName currentAssist = this.getCurrentAssist();
        if (isCurrentAssistVoiceService(currentAssist, getCurrentService(this.mHelper))) {
            this.mAssistRestrict = currentAssist.flattenToShortString();
        }
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        for (final VoiceInputHelper.InteractionInfo interactionInfo : this.mHelper.mAvailableInteractionInfos) {
            if (TextUtils.equals((CharSequence)s, (CharSequence)interactionInfo.key)) {
                Settings.Secure.putString(this.getContext().getContentResolver(), "voice_interaction_service", s);
                Settings.Secure.putString(this.getContext().getContentResolver(), "voice_recognition_service", new ComponentName(interactionInfo.service.packageName, interactionInfo.serviceInfo.getRecognitionService()).flattenToShortString());
                return true;
            }
        }
        final Iterator<VoiceInputHelper.RecognizerInfo> iterator2 = this.mHelper.mAvailableRecognizerInfos.iterator();
        while (iterator2.hasNext()) {
            if (TextUtils.equals((CharSequence)s, (CharSequence)((VoiceInputHelper.RecognizerInfo)iterator2.next()).key)) {
                Settings.Secure.putString(this.getContext().getContentResolver(), "voice_interaction_service", "");
                Settings.Secure.putString(this.getContext().getContentResolver(), "voice_recognition_service", s);
                return true;
            }
        }
        return true;
    }
    
    public static class VoiceInputDefaultAppInfo extends DefaultAppInfo
    {
        public VoiceInputHelper.BaseInfo mInfo;
        
        public VoiceInputDefaultAppInfo(final Context context, final PackageManagerWrapper packageManagerWrapper, final int n, final VoiceInputHelper.BaseInfo mInfo, final boolean b) {
            super(context, packageManagerWrapper, n, mInfo.componentName, null, b);
            this.mInfo = mInfo;
        }
        
        @Override
        public String getKey() {
            return this.mInfo.key;
        }
        
        public Intent getSettingIntent() {
            if (this.mInfo.settings == null) {
                return null;
            }
            return new Intent("android.intent.action.MAIN").setComponent(this.mInfo.settings);
        }
        
        @Override
        public CharSequence loadLabel() {
            if (this.mInfo instanceof VoiceInputHelper.InteractionInfo) {
                return this.mInfo.appLabel;
            }
            return this.mInfo.label;
        }
    }
}
