package com.android.settings.applications;

import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Parcelable;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.Intent;
import android.app.PendingIntent;
import com.android.settings.Utils;
import android.os.Bundle;
import android.content.Context;
import android.app.ApplicationErrorReport;
import android.provider.Settings;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import android.os.UserHandle;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import android.app.Fragment;
import android.app.Activity;
import com.android.settingslib.utils.ThreadUtils;
import android.content.ComponentName;
import android.view.View;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.app.ActivityManager;
import android.view.ViewGroup;
import java.util.ArrayList;
import com.android.settings.core.InstrumentedFragment;

public final class _$$Lambda$RunningServiceDetails$YTkFZYBIB00Mbz3Oy26GxrtuRF0 implements Runnable
{
    @Override
    public final void run() {
        RunningServiceDetails.lambda$finish$0(this.f$0);
    }
}
