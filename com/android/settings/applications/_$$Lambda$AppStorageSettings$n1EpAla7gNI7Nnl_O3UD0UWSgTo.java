package com.android.settings.applications;

import android.content.pm.IPackageDataObserver$Stub;
import android.content.Loader;
import com.android.settings.deviceinfo.StorageWizardMoveConfirm;
import com.android.settings.Utils;
import com.android.settingslib.RestrictedLockUtils;
import android.content.DialogInterface;
import android.content.Context;
import android.app.AlertDialog;
import android.os.Bundle;
import android.content.pm.PackageManager;
import java.util.Iterator;
import android.content.pm.PackageManager;
import java.util.Map;
import android.util.MutableInt;
import android.app.GrantedUriPermission;
import java.util.TreeMap;
import android.os.RemoteException;
import android.os.UserHandle;
import android.app.AppGlobals;
import android.content.pm.IPackageDataObserver;
import android.util.Pair;
import java.util.Objects;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import android.os.storage.StorageManager;
import android.content.Intent;
import android.net.Uri;
import android.app.Activity;
import android.util.Log;
import android.net.Uri.Builder;
import android.app.ActivityManager;
import android.os.Message;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.app.AlertDialog$Builder;
import android.widget.Button;
import android.os.storage.VolumeInfo;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settingslib.applications.ApplicationsState;
import android.content.DialogInterface$OnClickListener;
import com.android.settingslib.applications.StorageStatsSource;
import android.app.LoaderManager$LoaderCallbacks;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$AppStorageSettings$n1EpAla7gNI7Nnl_O3UD0UWSgTo implements View.OnClickListener
{
    public final void onClick(final View view) {
        AppStorageSettings.lambda$updateUiWithSize$1(this.f$0, view);
    }
}
