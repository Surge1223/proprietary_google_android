package com.android.settings.applications;

import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.content.Intent;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.view.View;
import com.android.settings.core.SubSettingLauncher;
import com.android.settings.core.InstrumentedFragment;

public class ConvertToFbe extends InstrumentedFragment
{
    private void convert() {
        new SubSettingLauncher(this.getContext()).setDestination(ConfirmConvertToFbe.class.getName()).setTitle(2131887133).setSourceMetricsCategory(this.getMetricsCategory()).launch();
    }
    
    private boolean runKeyguardConfirmation(final int n) {
        return new ChooseLockSettingsHelper(this.getActivity(), this).launchConfirmationActivity(n, this.getActivity().getResources().getText(2131887133));
    }
    
    @Override
    public int getMetricsCategory() {
        return 402;
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n != 55) {
            return;
        }
        if (n2 == -1) {
            this.convert();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131887133);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558506, (ViewGroup)null);
        ((Button)inflate.findViewById(2131361952)).setOnClickListener((View.OnClickListener)new _$$Lambda$ConvertToFbe$cKWuNkHe_dkbg8HKJCoDk07_9og(this));
        return inflate;
    }
}
