package com.android.settings.applications;

import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.os.Bundle;
import android.view.Menu;
import com.android.settings.widget.LoadingViewController;
import android.view.View;
import com.android.settings.SettingsPreferenceFragment;

public class RunningServices extends SettingsPreferenceFragment
{
    private View mLoadingContainer;
    private LoadingViewController mLoadingViewController;
    private Menu mOptionsMenu;
    private final Runnable mRunningProcessesAvail;
    private RunningProcessesView mRunningProcessesView;
    
    public RunningServices() {
        this.mRunningProcessesAvail = new Runnable() {
            @Override
            public void run() {
                RunningServices.this.mLoadingViewController.showContent(true);
            }
        };
    }
    
    private void updateOptionsMenu() {
        final boolean showBackground = this.mRunningProcessesView.mAdapter.getShowBackground();
        this.mOptionsMenu.findItem(1).setVisible(showBackground);
        this.mOptionsMenu.findItem(2).setVisible(showBackground ^ true);
    }
    
    @Override
    public int getMetricsCategory() {
        return 404;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131888885);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu mOptionsMenu, final MenuInflater menuInflater) {
        this.mOptionsMenu = mOptionsMenu;
        mOptionsMenu.add(0, 1, 1, 2131889108).setShowAsAction(0);
        mOptionsMenu.add(0, 2, 2, 2131889088).setShowAsAction(0);
        this.updateOptionsMenu();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558608, (ViewGroup)null);
        (this.mRunningProcessesView = (RunningProcessesView)inflate.findViewById(2131362542)).doCreate();
        this.mLoadingContainer = inflate.findViewById(2131362342);
        this.mLoadingViewController = new LoadingViewController(this.mLoadingContainer, (View)this.mRunningProcessesView);
        return inflate;
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return false;
            }
            case 2: {
                this.mRunningProcessesView.mAdapter.setShowBackground(true);
                break;
            }
            case 1: {
                this.mRunningProcessesView.mAdapter.setShowBackground(false);
                break;
            }
        }
        this.updateOptionsMenu();
        return true;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mRunningProcessesView.doPause();
    }
    
    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        this.updateOptionsMenu();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mLoadingViewController.handleLoadingContainer(this.mRunningProcessesView.doResume(this, this.mRunningProcessesAvail), false);
    }
}
