package com.android.settings.applications;

import android.text.format.Formatter$BytesResult;
import com.android.settingslib.Utils;
import android.text.format.Formatter;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.content.Context;
import android.app.Activity;
import com.android.settings.SummaryPreference;
import com.android.settings.dashboard.SummaryLoader;
import android.support.v7.preference.Preference;

public class ProcessStatsSummary extends ProcessStatsBase implements OnPreferenceClickListener
{
    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    private Preference mAppListPreference;
    private Preference mAverageUsed;
    private Preference mFree;
    private Preference mPerformance;
    private SummaryPreference mSummaryPref;
    private Preference mTotalMemory;
    
    static {
        SUMMARY_PROVIDER_FACTORY = new SummaryLoader.SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new ProcessStatsSummary.SummaryProvider((Context)activity, summaryLoader);
            }
        };
    }
    
    public int getHelpResource() {
        return 2131887784;
    }
    
    public int getMetricsCategory() {
        return 202;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082817);
        this.mSummaryPref = (SummaryPreference)this.findPreference("status_header");
        this.mPerformance = this.findPreference("performance");
        this.mTotalMemory = this.findPreference("total_memory");
        this.mAverageUsed = this.findPreference("average_used");
        this.mFree = this.findPreference("free");
        (this.mAppListPreference = this.findPreference("apps_list")).setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        if (preference == this.mAppListPreference) {
            final Bundle arguments = new Bundle();
            arguments.putBoolean("transfer_stats", true);
            arguments.putInt("duration_index", this.mDurationIndex);
            this.mStatsManager.xferStats();
            new SubSettingLauncher(this.getContext()).setDestination(ProcessStatsUi.class.getName()).setTitle(2131888286).setArguments(arguments).setSourceMetricsCategory(this.getMetricsCategory()).launch();
            return true;
        }
        return false;
    }
    
    @Override
    public void refreshUi() {
        final Context context = this.getContext();
        final ProcStatsData.MemInfo memInfo = this.mStatsManager.getMemInfo();
        final double realUsedRam = memInfo.realUsedRam;
        final double realTotalRam = memInfo.realTotalRam;
        final double realFreeRam = memInfo.realFreeRam;
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(context.getResources(), (long)realUsedRam, 1);
        final String formatShortFileSize = Formatter.formatShortFileSize(context, (long)realTotalRam);
        final String formatShortFileSize2 = Formatter.formatShortFileSize(context, (long)realFreeRam);
        final CharSequence[] textArray = this.getResources().getTextArray(2130903137);
        final int memState = this.mStatsManager.getMemState();
        CharSequence summary;
        if (memState >= 0 && memState < textArray.length - 1) {
            summary = textArray[memState];
        }
        else {
            summary = textArray[textArray.length - 1];
        }
        this.mSummaryPref.setAmount(formatBytes.value);
        this.mSummaryPref.setUnits(formatBytes.units);
        final float n = (float)(realUsedRam / (realFreeRam + realUsedRam));
        this.mSummaryPref.setRatios(n, 0.0f, 1.0f - n);
        this.mPerformance.setSummary(summary);
        this.mTotalMemory.setSummary(formatShortFileSize);
        this.mAverageUsed.setSummary(Utils.formatPercentage((long)realUsedRam, (long)realTotalRam));
        this.mFree.setSummary(formatShortFileSize2);
        final String string = this.getString(ProcessStatsSummary.sDurationLabels[this.mDurationIndex]);
        final int size = this.mStatsManager.getEntries().size();
        this.mAppListPreference.setSummary(this.getResources().getQuantityString(2131755044, size, new Object[] { size, string }));
    }
    
    private static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                final ProcStatsData procStatsData = new ProcStatsData(this.mContext, false);
                procStatsData.setDuration(ProcessStatsBase.sDurations[0]);
                final ProcStatsData.MemInfo memInfo = procStatsData.getMemInfo();
                this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getString(2131888284, new Object[] { Formatter.formatShortFileSize(this.mContext, (long)memInfo.realUsedRam), Formatter.formatShortFileSize(this.mContext, (long)memInfo.realTotalRam) }));
            }
        }
    }
}
