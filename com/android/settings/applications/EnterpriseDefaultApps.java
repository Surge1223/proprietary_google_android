package com.android.settings.applications;

import android.net.Uri;
import android.content.Intent;

public enum EnterpriseDefaultApps
{
    BROWSER(new Intent[] { buildIntent("android.intent.action.VIEW", "android.intent.category.BROWSABLE", "http:", null) }), 
    CALENDAR(new Intent[] { buildIntent("android.intent.action.INSERT", null, null, "vnd.android.cursor.dir/event") }), 
    CAMERA(new Intent[] { new Intent("android.media.action.IMAGE_CAPTURE"), new Intent("android.media.action.VIDEO_CAPTURE") }), 
    CONTACTS(new Intent[] { buildIntent("android.intent.action.PICK", null, null, "vnd.android.cursor.dir/contact") }), 
    EMAIL(new Intent[] { new Intent("android.intent.action.SENDTO"), new Intent("android.intent.action.SEND"), new Intent("android.intent.action.SEND_MULTIPLE") }), 
    MAP(new Intent[] { buildIntent("android.intent.action.VIEW", null, "geo:", null) }), 
    PHONE(new Intent[] { new Intent("android.intent.action.DIAL"), new Intent("android.intent.action.CALL") });
    
    private final Intent[] mIntents;
    
    private EnterpriseDefaultApps(final Intent[] mIntents) {
        this.mIntents = mIntents;
    }
    
    private static Intent buildIntent(final String s, final String s2, final String s3, final String type) {
        final Intent intent = new Intent(s);
        if (s2 != null) {
            intent.addCategory(s2);
        }
        if (s3 != null) {
            intent.setData(Uri.parse(s3));
        }
        if (type != null) {
            intent.setType(type);
        }
        return intent;
    }
    
    public Intent[] getIntents() {
        return this.mIntents;
    }
}
