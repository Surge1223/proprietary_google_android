package com.android.settings.applications;

import java.util.Iterator;
import android.content.pm.ApplicationInfo;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import java.util.ArrayList;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import java.util.List;
import android.os.AsyncTask;

public abstract class AppLister extends AsyncTask<Void, Void, List<UserAppInfo>>
{
    protected final PackageManagerWrapper mPm;
    protected final UserManager mUm;
    
    public AppLister(final PackageManagerWrapper mPm, final UserManager mUm) {
        this.mPm = mPm;
        this.mUm = mUm;
    }
    
    protected List<UserAppInfo> doInBackground(final Void... array) {
        final ArrayList<UserAppInfo> list = new ArrayList<UserAppInfo>();
        for (final UserInfo userInfo : this.mUm.getProfiles(UserHandle.myUserId())) {
            final PackageManagerWrapper mPm = this.mPm;
            int n;
            if (userInfo.isAdmin()) {
                n = 4194304;
            }
            else {
                n = 0;
            }
            for (final ApplicationInfo applicationInfo : mPm.getInstalledApplicationsAsUser(0x8200 | n, userInfo.id)) {
                if (this.includeInCount(applicationInfo)) {
                    list.add(new UserAppInfo(userInfo, applicationInfo));
                }
            }
        }
        return list;
    }
    
    protected abstract boolean includeInCount(final ApplicationInfo p0);
    
    protected abstract void onAppListBuilt(final List<UserAppInfo> p0);
    
    protected void onPostExecute(final List<UserAppInfo> list) {
        this.onAppListBuilt(list);
    }
}
