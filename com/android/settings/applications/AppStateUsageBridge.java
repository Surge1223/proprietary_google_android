package com.android.settings.applications;

import android.content.Context;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateUsageBridge extends AppStateAppOpsBridge
{
    public static final AppFilter FILTER_APP_USAGE;
    private static final String[] PM_PERMISSION;
    
    static {
        PM_PERMISSION = new String[] { "android.permission.PACKAGE_USAGE_STATS" };
        FILTER_APP_USAGE = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.extraInfo != null;
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    public AppStateUsageBridge(final Context context, final ApplicationsState applicationsState, final Callback callback) {
        super(context, applicationsState, callback, 43, AppStateUsageBridge.PM_PERMISSION);
    }
    
    public UsageState getUsageInfo(final String s, final int n) {
        return new UsageState(super.getPermissionInfo(s, n));
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = this.getUsageInfo(s, n);
    }
    
    public static class UsageState extends PermissionState
    {
        public UsageState(final PermissionState permissionState) {
            super(permissionState.packageName, permissionState.userHandle);
            this.packageInfo = permissionState.packageInfo;
            this.appOpMode = permissionState.appOpMode;
            this.permissionDeclared = permissionState.permissionDeclared;
            this.staticPermissionGranted = permissionState.staticPermissionGranted;
        }
    }
}
