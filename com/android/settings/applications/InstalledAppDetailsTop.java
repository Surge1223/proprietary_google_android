package com.android.settings.applications;

import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import android.content.Intent;
import com.android.settings.SettingsActivity;

public class InstalledAppDetailsTop extends SettingsActivity
{
    @Override
    public Intent getIntent() {
        final Intent intent = new Intent(super.getIntent());
        intent.putExtra(":settings:show_fragment", AppInfoDashboardFragment.class.getName());
        return intent;
    }
    
    @Override
    protected boolean isValidFragment(final String s) {
        return AppInfoDashboardFragment.class.getName().equals(s);
    }
}
