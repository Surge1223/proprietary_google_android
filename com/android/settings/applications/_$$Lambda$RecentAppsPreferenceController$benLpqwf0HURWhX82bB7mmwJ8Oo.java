package com.android.settings.applications;

import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.applications.AppUtils;
import android.content.Intent;
import android.util.Log;
import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import com.android.settingslib.utils.StringUtil;
import com.android.settings.widget.AppPreference;
import android.util.ArrayMap;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.app.Application;
import android.content.Context;
import java.util.Collection;
import java.util.Arrays;
import android.util.ArraySet;
import android.app.usage.UsageStatsManager;
import java.util.List;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.Fragment;
import android.support.v7.preference.PreferenceCategory;
import java.util.Calendar;
import java.util.Set;
import android.app.usage.UsageStats;
import java.util.Comparator;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settingslib.applications.ApplicationsState;
import android.support.v7.preference.Preference;

public final class _$$Lambda$RecentAppsPreferenceController$benLpqwf0HURWhX82bB7mmwJ8Oo implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return RecentAppsPreferenceController.lambda$displayRecentApps$0(this.f$0, this.f$1, this.f$2, preference);
    }
}
