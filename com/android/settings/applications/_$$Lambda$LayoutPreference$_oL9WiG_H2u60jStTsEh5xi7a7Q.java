package com.android.settings.applications;

import android.widget.FrameLayout;
import android.support.v7.preference.PreferenceViewHolder;
import com.android.settings.Utils;
import android.content.res.TypedArray;
import com.android.internal.R$styleable;
import android.support.v4.content.res.TypedArrayUtils;
import com.android.settings.R;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.view.View;
import android.view.View.OnClickListener;

public final class _$$Lambda$LayoutPreference$_oL9WiG_H2u60jStTsEh5xi7a7Q implements View.OnClickListener
{
    public final void onClick(final View view) {
        this.f$0.performClick(view);
    }
}
