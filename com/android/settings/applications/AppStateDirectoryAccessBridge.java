package com.android.settings.applications;

import android.database.Cursor;
import android.net.Uri;
import android.util.ArraySet;
import android.util.Log;
import android.os.CancellationSignal;
import android.os.Bundle;
import android.os.storage.StorageVolume$ScopedAccessProviderContract;
import android.net.Uri.Builder;
import android.content.Context;
import java.util.Set;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateDirectoryAccessBridge extends AppStateBaseBridge
{
    public static final AppFilter FILTER_APP_HAS_DIRECTORY_ACCESS;
    
    static {
        FILTER_APP_HAS_DIRECTORY_ACCESS = new AppFilter() {
            private Set<String> mPackages;
            
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return this.mPackages != null && this.mPackages.contains(appEntry.info.packageName);
            }
            
            @Override
            public void init() {
                throw new UnsupportedOperationException("Need to call constructor that takes context");
            }
            
            @Override
            public void init(Context context) {
                final Context context2 = null;
                this.mPackages = null;
                final Uri build = new Uri.Builder().scheme("content").authority("com.android.documentsui.scopedAccess").appendPath("packages").appendPath("*").build();
                final Cursor query = context.getContentResolver().query(build, StorageVolume$ScopedAccessProviderContract.TABLE_PACKAGES_COLUMNS, (Bundle)null, (CancellationSignal)null);
                StringBuilder sb;
                Cursor cursor;
                int count;
                StringBuilder sb2;
                StringBuilder sb3;
                Block_11_Outer:Block_12_Outer:
                while (true) {
                    Label_0125: {
                        if (query != null) {
                            break Label_0125;
                        }
                        context = context2;
                        try {
                            try {
                                context = context2;
                                sb = new StringBuilder();
                                context = context2;
                                sb.append("Didn't get cursor for ");
                                context = context2;
                                sb.append(build);
                                context = context2;
                                Log.w("DirectoryAccessBridge", sb.toString());
                                if (query != null) {
                                    query.close();
                                }
                                return;
                            }
                            finally {
                                if (query != null) {
                                    if (context != null) {
                                        cursor = query;
                                        cursor.close();
                                    }
                                    else {
                                        query.close();
                                    }
                                }
                                query.close();
                                return;
                                // iftrue(Label_0207:, count != 0)
                                // iftrue(Label_0263:, !query.moveToNext())
                                // iftrue(Label_0206:, query == null)
                                // iftrue(Label_0319:, query == null)
                                while (true) {
                                    Block_9: {
                                        while (true) {
                                            this.mPackages.add(query.getString(0));
                                            Label_0228: {
                                                break Label_0228;
                                                query.close();
                                                return;
                                                count = query.getCount();
                                                break Block_9;
                                            }
                                            continue Block_12_Outer;
                                        }
                                        Label_0206: {
                                            return;
                                        }
                                        Label_0319:
                                        return;
                                    }
                                    sb2 = new StringBuilder();
                                    sb2.append("No packages anymore (was ");
                                    sb2.append(this.mPackages);
                                    sb2.append(")");
                                    Log.d("DirectoryAccessBridge", sb2.toString());
                                    continue Block_11_Outer;
                                    Label_0263: {
                                        sb3 = new StringBuilder();
                                    }
                                    sb3.append("init(): ");
                                    sb3.append(this.mPackages);
                                    Log.d("DirectoryAccessBridge", sb3.toString());
                                    continue;
                                }
                                Label_0207: {
                                    this.mPackages = (Set<String>)new ArraySet(count);
                                }
                            }
                        }
                        catch (Throwable t) {}
                    }
                    try {
                        cursor = query;
                        cursor.close();
                        continue;
                    }
                    catch (Throwable t2) {}
                    break;
                }
            }
        };
    }
    
    public AppStateDirectoryAccessBridge(final ApplicationsState applicationsState, final Callback callback) {
        super(applicationsState, callback);
    }
    
    @Override
    protected void loadAllExtraInfo() {
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
    }
}
