package com.android.settings.applications;

import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Parcelable;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.Intent;
import android.app.PendingIntent;
import com.android.settings.Utils;
import android.os.Bundle;
import android.content.Context;
import android.app.ApplicationErrorReport;
import android.provider.Settings;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import android.os.UserHandle;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import android.app.Fragment;
import android.app.Activity;
import com.android.settingslib.utils.ThreadUtils;
import android.content.ComponentName;
import android.view.View;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.app.ActivityManager;
import android.view.ViewGroup;
import java.util.ArrayList;
import com.android.settings.core.InstrumentedFragment;

public class RunningServiceDetails extends InstrumentedFragment implements OnRefreshUiListener
{
    final ArrayList<ActiveDetail> mActiveDetails;
    ViewGroup mAllDetails;
    ActivityManager mAm;
    StringBuilder mBuilder;
    boolean mHaveData;
    LayoutInflater mInflater;
    MergedItem mMergedItem;
    int mNumProcesses;
    int mNumServices;
    String mProcessName;
    TextView mProcessesHeader;
    View mRootView;
    TextView mServicesHeader;
    boolean mShowBackground;
    ViewGroup mSnippet;
    RunningProcessesView.ActiveItem mSnippetActiveItem;
    RunningProcessesView.ViewHolder mSnippetViewHolder;
    RunningState mState;
    int mUid;
    int mUserId;
    
    public RunningServiceDetails() {
        this.mActiveDetails = new ArrayList<ActiveDetail>();
        this.mBuilder = new StringBuilder(128);
    }
    
    private void finish() {
        ThreadUtils.postOnMainThread(new _$$Lambda$RunningServiceDetails$YTkFZYBIB00Mbz3Oy26GxrtuRF0(this));
    }
    
    private void showConfirmStopDialog(final ComponentName componentName) {
        final MyAlertDialogFragment confirmStop = MyAlertDialogFragment.newConfirmStop(1, componentName);
        confirmStop.setTargetFragment((Fragment)this, 0);
        confirmStop.show(this.getFragmentManager(), "confirmstop");
    }
    
    ActiveDetail activeDetailForService(final ComponentName componentName) {
        for (int i = 0; i < this.mActiveDetails.size(); ++i) {
            final ActiveDetail activeDetail = this.mActiveDetails.get(i);
            if (activeDetail.mServiceItem != null && activeDetail.mServiceItem.mRunningService != null && componentName.equals((Object)activeDetail.mServiceItem.mRunningService.service)) {
                return activeDetail;
            }
        }
        return null;
    }
    
    void addDetailViews() {
        for (int i = this.mActiveDetails.size() - 1; i >= 0; --i) {
            this.mAllDetails.removeView(this.mActiveDetails.get(i).mRootView);
        }
        this.mActiveDetails.clear();
        if (this.mServicesHeader != null) {
            this.mAllDetails.removeView((View)this.mServicesHeader);
            this.mServicesHeader = null;
        }
        if (this.mProcessesHeader != null) {
            this.mAllDetails.removeView((View)this.mProcessesHeader);
            this.mProcessesHeader = null;
        }
        this.mNumProcesses = 0;
        this.mNumServices = 0;
        if (this.mMergedItem != null) {
            if (this.mMergedItem.mUser != null) {
                ArrayList<MergedItem> mChildren;
                if (this.mShowBackground) {
                    mChildren = new ArrayList<MergedItem>((Collection<? extends T>)this.mMergedItem.mChildren);
                    Collections.sort((List<Object>)mChildren, (Comparator<? super Object>)this.mState.mBackgroundComparator);
                }
                else {
                    mChildren = this.mMergedItem.mChildren;
                }
                for (int j = 0; j < mChildren.size(); ++j) {
                    this.addDetailsViews(mChildren.get(j), true, false);
                }
                for (int k = 0; k < mChildren.size(); ++k) {
                    this.addDetailsViews(mChildren.get(k), false, true);
                }
            }
            else {
                this.addDetailsViews(this.mMergedItem, true, true);
            }
        }
    }
    
    void addDetailsViews(final MergedItem mergedItem, final boolean b, final boolean b2) {
        if (mergedItem != null) {
            final boolean b3 = true;
            if (b) {
                for (int i = 0; i < mergedItem.mServices.size(); ++i) {
                    this.addServiceDetailsView(mergedItem.mServices.get(i), mergedItem, true, true);
                }
            }
            if (b2) {
                if (mergedItem.mServices.size() <= 0) {
                    this.addServiceDetailsView(null, mergedItem, false, mergedItem.mUserId != UserHandle.myUserId() && b3);
                }
                else {
                    for (int j = -1; j < mergedItem.mOtherProcesses.size(); ++j) {
                        ProcessItem mProcess;
                        if (j < 0) {
                            mProcess = mergedItem.mProcess;
                        }
                        else {
                            mProcess = mergedItem.mOtherProcesses.get(j);
                        }
                        if (mProcess == null || mProcess.mPid > 0) {
                            this.addProcessDetailsView(mProcess, j < 0);
                        }
                    }
                }
            }
        }
    }
    
    void addProcessDetailsView(final ProcessItem processItem, final boolean b) {
        this.addProcessesHeader();
        final ActiveDetail activeDetail = new ActiveDetail();
        final View inflate = this.mInflater.inflate(2131558724, this.mAllDetails, false);
        this.mAllDetails.addView(inflate);
        activeDetail.mRootView = inflate;
        activeDetail.mViewHolder = new RunningProcessesView.ViewHolder(inflate);
        activeDetail.mActiveItem = activeDetail.mViewHolder.bind(this.mState, processItem, this.mBuilder);
        final TextView textView = (TextView)inflate.findViewById(2131362000);
        if (processItem.mUserId != UserHandle.myUserId()) {
            textView.setVisibility(8);
        }
        else if (b) {
            textView.setText(2131888211);
        }
        else {
            int n = 0;
            final Object o = null;
            final Object o2 = null;
            final ActivityManager$RunningAppProcessInfo mRunningProcessInfo = processItem.mRunningProcessInfo;
            final ComponentName importanceReasonComponent = mRunningProcessInfo.importanceReasonComponent;
            Object o3 = null;
            switch (mRunningProcessInfo.importanceReasonCode) {
                default: {
                    o3 = o;
                    break;
                }
                case 2: {
                    final int n2 = n = 2131888680;
                    o3 = o;
                    if (mRunningProcessInfo.importanceReasonComponent != null) {
                        try {
                            final ServiceInfo serviceInfo = this.getActivity().getPackageManager().getServiceInfo(mRunningProcessInfo.importanceReasonComponent, 0);
                            o3 = RunningState.makeLabel(this.getActivity().getPackageManager(), serviceInfo.name, (PackageItemInfo)serviceInfo);
                            n = n2;
                        }
                        catch (PackageManager$NameNotFoundException ex) {
                            n = n2;
                            o3 = o;
                        }
                        break;
                    }
                    break;
                }
                case 1: {
                    final int n3 = n = 2131888679;
                    o3 = o;
                    if (mRunningProcessInfo.importanceReasonComponent != null) {
                        while (true) {
                            try {
                                final ProviderInfo providerInfo = this.getActivity().getPackageManager().getProviderInfo(mRunningProcessInfo.importanceReasonComponent, 0);
                                o3 = RunningState.makeLabel(this.getActivity().getPackageManager(), providerInfo.name, (PackageItemInfo)providerInfo);
                                n = n3;
                            }
                            catch (PackageManager$NameNotFoundException ex2) {
                                o3 = o2;
                                continue;
                            }
                            break;
                        }
                        break;
                    }
                    break;
                }
            }
            if (n != 0 && o3 != null) {
                textView.setText((CharSequence)this.getActivity().getString(n, new Object[] { o3 }));
            }
        }
        this.mActiveDetails.add(activeDetail);
    }
    
    void addProcessesHeader() {
        if (this.mNumProcesses == 0) {
            (this.mProcessesHeader = (TextView)this.mInflater.inflate(2131558739, this.mAllDetails, false)).setText(2131888879);
            this.mAllDetails.addView((View)this.mProcessesHeader);
        }
        ++this.mNumProcesses;
    }
    
    void addServiceDetailsView(final ServiceItem mServiceItem, final MergedItem mergedItem, boolean enabled, final boolean b) {
        if (enabled) {
            this.addServicesHeader();
        }
        else if (mergedItem.mUserId != UserHandle.myUserId()) {
            this.addProcessesHeader();
        }
        BaseItem baseItem;
        if (mServiceItem != null) {
            baseItem = mServiceItem;
        }
        else {
            baseItem = mergedItem;
        }
        final ActiveDetail activeDetail = new ActiveDetail();
        final View inflate = this.mInflater.inflate(2131558725, this.mAllDetails, false);
        this.mAllDetails.addView(inflate);
        activeDetail.mRootView = inflate;
        activeDetail.mServiceItem = mServiceItem;
        activeDetail.mViewHolder = new RunningProcessesView.ViewHolder(inflate);
        activeDetail.mActiveItem = activeDetail.mViewHolder.bind(this.mState, baseItem, this.mBuilder);
        if (!b) {
            inflate.findViewById(2131362590).setVisibility(8);
        }
        if (mServiceItem != null && mServiceItem.mRunningService.clientLabel != 0) {
            activeDetail.mManageIntent = this.mAm.getRunningServiceControlPanel(mServiceItem.mRunningService.service);
        }
        final TextView textView = (TextView)inflate.findViewById(2131362000);
        activeDetail.mStopButton = (Button)inflate.findViewById(2131362331);
        activeDetail.mReportButton = (Button)inflate.findViewById(2131362531);
        if (enabled && mergedItem.mUserId != UserHandle.myUserId()) {
            textView.setVisibility(8);
            inflate.findViewById(2131362021).setVisibility(8);
        }
        else {
            enabled = true;
            if (mServiceItem != null && mServiceItem.mServiceInfo.descriptionRes != 0) {
                textView.setText(this.getActivity().getPackageManager().getText(mServiceItem.mServiceInfo.packageName, mServiceItem.mServiceInfo.descriptionRes, mServiceItem.mServiceInfo.applicationInfo));
            }
            else if (mergedItem.mBackground) {
                textView.setText(2131886534);
            }
            else if (activeDetail.mManageIntent != null) {
                try {
                    textView.setText((CharSequence)this.getActivity().getString(2131889042, new Object[] { this.getActivity().getPackageManager().getResourcesForApplication(mServiceItem.mRunningService.clientPackage).getString(mServiceItem.mRunningService.clientLabel) }));
                }
                catch (PackageManager$NameNotFoundException ex) {}
            }
            else {
                final Activity activity = this.getActivity();
                int n;
                if (mServiceItem != null) {
                    n = 2131889047;
                }
                else {
                    n = 2131887766;
                }
                textView.setText(activity.getText(n));
            }
            activeDetail.mStopButton.setOnClickListener((View.OnClickListener)activeDetail);
            final Button mStopButton = activeDetail.mStopButton;
            final Activity activity2 = this.getActivity();
            int n2;
            if (activeDetail.mManageIntent != null) {
                n2 = 2131889041;
            }
            else {
                n2 = 2131889046;
            }
            mStopButton.setText(activity2.getText(n2));
            activeDetail.mReportButton.setOnClickListener((View.OnClickListener)activeDetail);
            activeDetail.mReportButton.setText(17040771);
            if (Settings.Global.getInt(this.getActivity().getContentResolver(), "send_action_app_error", 0) != 0 && mServiceItem != null) {
                activeDetail.mInstaller = ApplicationErrorReport.getErrorReportReceiver((Context)this.getActivity(), mServiceItem.mServiceInfo.packageName, mServiceItem.mServiceInfo.applicationInfo.flags);
                final Button mReportButton = activeDetail.mReportButton;
                if (activeDetail.mInstaller == null) {
                    enabled = false;
                }
                mReportButton.setEnabled(enabled);
            }
            else {
                activeDetail.mReportButton.setEnabled(false);
            }
        }
        this.mActiveDetails.add(activeDetail);
    }
    
    void addServicesHeader() {
        if (this.mNumServices == 0) {
            (this.mServicesHeader = (TextView)this.mInflater.inflate(2131558739, this.mAllDetails, false)).setText(2131888880);
            this.mAllDetails.addView((View)this.mServicesHeader);
        }
        ++this.mNumServices;
    }
    
    void ensureData() {
        if (!this.mHaveData) {
            this.mHaveData = true;
            this.mState.resume((RunningState.OnRefreshUiListener)this);
            this.mState.waitForData();
            this.refreshUi(true);
        }
    }
    
    boolean findMergedItem() {
        final MergedItem mergedItem = null;
        ArrayList<RunningState.MergedItem> list;
        if (this.mShowBackground) {
            list = this.mState.getCurrentBackgroundItems();
        }
        else {
            list = this.mState.getCurrentMergedItems();
        }
        MergedItem mMergedItem = mergedItem;
        if (list != null) {
            int n = 0;
            while (true) {
                mMergedItem = mergedItem;
                if (n >= list.size()) {
                    break;
                }
                mMergedItem = list.get(n);
                if (mMergedItem.mUserId == this.mUserId) {
                    if (this.mUid < 0 || mMergedItem.mProcess == null || mMergedItem.mProcess.mUid == this.mUid) {
                        if (this.mProcessName == null) {
                            break;
                        }
                        if (mMergedItem.mProcess != null && this.mProcessName.equals(mMergedItem.mProcess.mProcessName)) {
                            break;
                        }
                    }
                }
                ++n;
            }
        }
        if (this.mMergedItem != mMergedItem) {
            this.mMergedItem = mMergedItem;
            return true;
        }
        return false;
    }
    
    @Override
    public int getMetricsCategory() {
        return 85;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setHasOptionsMenu(true);
        this.mUid = this.getArguments().getInt("uid", -1);
        this.mUserId = this.getArguments().getInt("user_id", 0);
        this.mProcessName = this.getArguments().getString("process", (String)null);
        this.mShowBackground = this.getArguments().getBoolean("background", false);
        this.mAm = (ActivityManager)this.getActivity().getSystemService("activity");
        this.mInflater = (LayoutInflater)this.getActivity().getSystemService("layout_inflater");
        this.mState = RunningState.getInstance((Context)this.getActivity());
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131558723, viewGroup, false);
        Utils.prepareCustomPreferencesList(viewGroup, inflate, inflate, false);
        this.mRootView = inflate;
        this.mAllDetails = (ViewGroup)inflate.findViewById(2131361860);
        this.mSnippet = (ViewGroup)inflate.findViewById(2131362623);
        this.mSnippetViewHolder = new RunningProcessesView.ViewHolder((View)this.mSnippet);
        this.ensureData();
        return inflate;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mHaveData = false;
        this.mState.pause();
    }
    
    @Override
    public void onRefreshUi(final int n) {
        if (this.getActivity() == null) {
            return;
        }
        switch (n) {
            case 2: {
                this.refreshUi(true);
                this.updateTimes();
                break;
            }
            case 1: {
                this.refreshUi(false);
                this.updateTimes();
                break;
            }
            case 0: {
                this.updateTimes();
                break;
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.ensureData();
    }
    
    void refreshUi(boolean b) {
        if (this.findMergedItem()) {
            b = true;
        }
        if (b) {
            if (this.mMergedItem != null) {
                this.mSnippetActiveItem = this.mSnippetViewHolder.bind(this.mState, this.mMergedItem, this.mBuilder);
            }
            else {
                if (this.mSnippetActiveItem == null) {
                    this.finish();
                    return;
                }
                this.mSnippetActiveItem.mHolder.size.setText((CharSequence)"");
                this.mSnippetActiveItem.mHolder.uptime.setText((CharSequence)"");
                this.mSnippetActiveItem.mHolder.description.setText(2131888422);
            }
            this.addDetailViews();
        }
    }
    
    void updateTimes() {
        if (this.mSnippetActiveItem != null) {
            this.mSnippetActiveItem.updateTime((Context)this.getActivity(), this.mBuilder);
        }
        for (int i = 0; i < this.mActiveDetails.size(); ++i) {
            this.mActiveDetails.get(i).mActiveItem.updateTime((Context)this.getActivity(), this.mBuilder);
        }
    }
    
    class ActiveDetail implements View.OnClickListener
    {
        RunningProcessesView.ActiveItem mActiveItem;
        ComponentName mInstaller;
        PendingIntent mManageIntent;
        Button mReportButton;
        View mRootView;
        ServiceItem mServiceItem;
        Button mStopButton;
        RunningProcessesView.ViewHolder mViewHolder;
        
        public void onClick(final View p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: aload_0        
            //     2: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mReportButton:Landroid/widget/Button;
            //     5: if_acmpne       655
            //     8: new             Landroid/app/ApplicationErrorReport;
            //    11: dup            
            //    12: invokespecial   android/app/ApplicationErrorReport.<init>:()V
            //    15: astore_2       
            //    16: aload_2        
            //    17: iconst_5       
            //    18: putfield        android/app/ApplicationErrorReport.type:I
            //    21: aload_2        
            //    22: aload_0        
            //    23: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;
            //    26: getfield        com/android/settings/applications/RunningState$ServiceItem.mServiceInfo:Landroid/content/pm/ServiceInfo;
            //    29: getfield        android/content/pm/ServiceInfo.packageName:Ljava/lang/String;
            //    32: putfield        android/app/ApplicationErrorReport.packageName:Ljava/lang/String;
            //    35: aload_2        
            //    36: aload_0        
            //    37: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mInstaller:Landroid/content/ComponentName;
            //    40: invokevirtual   android/content/ComponentName.getPackageName:()Ljava/lang/String;
            //    43: putfield        android/app/ApplicationErrorReport.installerPackageName:Ljava/lang/String;
            //    46: aload_2        
            //    47: aload_0        
            //    48: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;
            //    51: getfield        com/android/settings/applications/RunningState$ServiceItem.mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;
            //    54: getfield        android/app/ActivityManager$RunningServiceInfo.process:Ljava/lang/String;
            //    57: putfield        android/app/ApplicationErrorReport.processName:Ljava/lang/String;
            //    60: aload_2        
            //    61: invokestatic    java/lang/System.currentTimeMillis:()J
            //    64: putfield        android/app/ApplicationErrorReport.time:J
            //    67: aload_0        
            //    68: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;
            //    71: getfield        com/android/settings/applications/RunningState$ServiceItem.mServiceInfo:Landroid/content/pm/ServiceInfo;
            //    74: getfield        android/content/pm/ServiceInfo.applicationInfo:Landroid/content/pm/ApplicationInfo;
            //    77: getfield        android/content/pm/ApplicationInfo.flags:I
            //    80: iconst_1       
            //    81: iand           
            //    82: ifeq            90
            //    85: iconst_1       
            //    86: istore_3       
            //    87: goto            92
            //    90: iconst_0       
            //    91: istore_3       
            //    92: aload_2        
            //    93: iload_3        
            //    94: putfield        android/app/ApplicationErrorReport.systemApp:Z
            //    97: new             Landroid/app/ApplicationErrorReport$RunningServiceInfo;
            //   100: dup            
            //   101: invokespecial   android/app/ApplicationErrorReport$RunningServiceInfo.<init>:()V
            //   104: astore          4
            //   106: aload_0        
            //   107: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
            //   110: getfield        com/android/settings/applications/RunningProcessesView$ActiveItem.mFirstRunTime:J
            //   113: lconst_0       
            //   114: lcmp           
            //   115: iflt            137
            //   118: aload           4
            //   120: invokestatic    android/os/SystemClock.elapsedRealtime:()J
            //   123: aload_0        
            //   124: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
            //   127: getfield        com/android/settings/applications/RunningProcessesView$ActiveItem.mFirstRunTime:J
            //   130: lsub           
            //   131: putfield        android/app/ApplicationErrorReport$RunningServiceInfo.durationMillis:J
            //   134: goto            145
            //   137: aload           4
            //   139: ldc2_w          -1
            //   142: putfield        android/app/ApplicationErrorReport$RunningServiceInfo.durationMillis:J
            //   145: new             Landroid/content/ComponentName;
            //   148: dup            
            //   149: aload_0        
            //   150: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;
            //   153: getfield        com/android/settings/applications/RunningState$ServiceItem.mServiceInfo:Landroid/content/pm/ServiceInfo;
            //   156: getfield        android/content/pm/ServiceInfo.packageName:Ljava/lang/String;
            //   159: aload_0        
            //   160: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;
            //   163: getfield        com/android/settings/applications/RunningState$ServiceItem.mServiceInfo:Landroid/content/pm/ServiceInfo;
            //   166: getfield        android/content/pm/ServiceInfo.name:Ljava/lang/String;
            //   169: invokespecial   android/content/ComponentName.<init>:(Ljava/lang/String;Ljava/lang/String;)V
            //   172: astore          5
            //   174: aload_0        
            //   175: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   178: invokevirtual   com/android/settings/applications/RunningServiceDetails.getActivity:()Landroid/app/Activity;
            //   181: ldc             "service_dump.txt"
            //   183: invokevirtual   android/app/Activity.getFileStreamPath:(Ljava/lang/String;)Ljava/io/File;
            //   186: astore          6
            //   188: aconst_null    
            //   189: astore          7
            //   191: aconst_null    
            //   192: astore          8
            //   194: aload           8
            //   196: astore          9
            //   198: aload           7
            //   200: astore_1       
            //   201: new             Ljava/io/FileOutputStream;
            //   204: astore          10
            //   206: aload           8
            //   208: astore          9
            //   210: aload           7
            //   212: astore_1       
            //   213: aload           10
            //   215: aload           6
            //   217: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
            //   220: aload           10
            //   222: astore          9
            //   224: aload           10
            //   226: astore_1       
            //   227: ldc             "activity"
            //   229: aload           10
            //   231: invokevirtual   java/io/FileOutputStream.getFD:()Ljava/io/FileDescriptor;
            //   234: iconst_3       
            //   235: anewarray       Ljava/lang/String;
            //   238: dup            
            //   239: iconst_0       
            //   240: ldc             "-a"
            //   242: aastore        
            //   243: dup            
            //   244: iconst_1       
            //   245: ldc             "service"
            //   247: aastore        
            //   248: dup            
            //   249: iconst_2       
            //   250: aload           5
            //   252: invokevirtual   android/content/ComponentName.flattenToString:()Ljava/lang/String;
            //   255: aastore        
            //   256: invokestatic    android/os/Debug.dumpService:(Ljava/lang/String;Ljava/io/FileDescriptor;[Ljava/lang/String;)Z
            //   259: pop            
            //   260: aload           10
            //   262: astore_1       
            //   263: aload_1        
            //   264: invokevirtual   java/io/FileOutputStream.close:()V
            //   267: goto            341
            //   270: astore_1       
            //   271: goto            341
            //   274: astore_1       
            //   275: goto            638
            //   278: astore          10
            //   280: aload_1        
            //   281: astore          9
            //   283: new             Ljava/lang/StringBuilder;
            //   286: astore          8
            //   288: aload_1        
            //   289: astore          9
            //   291: aload           8
            //   293: invokespecial   java/lang/StringBuilder.<init>:()V
            //   296: aload_1        
            //   297: astore          9
            //   299: aload           8
            //   301: ldc             "Can't dump service: "
            //   303: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   306: pop            
            //   307: aload_1        
            //   308: astore          9
            //   310: aload           8
            //   312: aload           5
            //   314: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //   317: pop            
            //   318: aload_1        
            //   319: astore          9
            //   321: ldc             "RunningServicesDetails"
            //   323: aload           8
            //   325: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   328: aload           10
            //   330: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   333: pop            
            //   334: aload_1        
            //   335: ifnull          341
            //   338: goto            263
            //   341: aconst_null    
            //   342: astore          7
            //   344: aconst_null    
            //   345: astore          10
            //   347: aload           10
            //   349: astore          9
            //   351: aload           7
            //   353: astore_1       
            //   354: new             Ljava/io/FileInputStream;
            //   357: astore          8
            //   359: aload           10
            //   361: astore          9
            //   363: aload           7
            //   365: astore_1       
            //   366: aload           8
            //   368: aload           6
            //   370: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
            //   373: aload           8
            //   375: astore          10
            //   377: aload           10
            //   379: astore          9
            //   381: aload           10
            //   383: astore_1       
            //   384: aload           6
            //   386: invokevirtual   java/io/File.length:()J
            //   389: l2i            
            //   390: newarray        B
            //   392: astore          8
            //   394: aload           10
            //   396: astore          9
            //   398: aload           10
            //   400: astore_1       
            //   401: aload           10
            //   403: aload           8
            //   405: invokevirtual   java/io/FileInputStream.read:([B)I
            //   408: pop            
            //   409: aload           10
            //   411: astore          9
            //   413: aload           10
            //   415: astore_1       
            //   416: new             Ljava/lang/String;
            //   419: astore          7
            //   421: aload           10
            //   423: astore          9
            //   425: aload           10
            //   427: astore_1       
            //   428: aload           7
            //   430: aload           8
            //   432: invokespecial   java/lang/String.<init>:([B)V
            //   435: aload           10
            //   437: astore          9
            //   439: aload           10
            //   441: astore_1       
            //   442: aload           4
            //   444: aload           7
            //   446: putfield        android/app/ApplicationErrorReport$RunningServiceInfo.serviceDetails:Ljava/lang/String;
            //   449: aload           10
            //   451: astore_1       
            //   452: aload_1        
            //   453: invokevirtual   java/io/FileInputStream.close:()V
            //   456: goto            530
            //   459: astore_1       
            //   460: goto            530
            //   463: astore_1       
            //   464: goto            621
            //   467: astore          10
            //   469: aload_1        
            //   470: astore          9
            //   472: new             Ljava/lang/StringBuilder;
            //   475: astore          8
            //   477: aload_1        
            //   478: astore          9
            //   480: aload           8
            //   482: invokespecial   java/lang/StringBuilder.<init>:()V
            //   485: aload_1        
            //   486: astore          9
            //   488: aload           8
            //   490: ldc             "Can't read service dump: "
            //   492: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   495: pop            
            //   496: aload_1        
            //   497: astore          9
            //   499: aload           8
            //   501: aload           5
            //   503: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //   506: pop            
            //   507: aload_1        
            //   508: astore          9
            //   510: ldc             "RunningServicesDetails"
            //   512: aload           8
            //   514: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   517: aload           10
            //   519: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   522: pop            
            //   523: aload_1        
            //   524: ifnull          530
            //   527: goto            452
            //   530: aload           6
            //   532: invokevirtual   java/io/File.delete:()Z
            //   535: pop            
            //   536: new             Ljava/lang/StringBuilder;
            //   539: dup            
            //   540: invokespecial   java/lang/StringBuilder.<init>:()V
            //   543: astore_1       
            //   544: aload_1        
            //   545: ldc             "Details: "
            //   547: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   550: pop            
            //   551: aload_1        
            //   552: aload           4
            //   554: getfield        android/app/ApplicationErrorReport$RunningServiceInfo.serviceDetails:Ljava/lang/String;
            //   557: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   560: pop            
            //   561: ldc             "RunningServicesDetails"
            //   563: aload_1        
            //   564: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   567: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
            //   570: pop            
            //   571: aload_2        
            //   572: aload           4
            //   574: putfield        android/app/ApplicationErrorReport.runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;
            //   577: new             Landroid/content/Intent;
            //   580: dup            
            //   581: ldc             "android.intent.action.APP_ERROR"
            //   583: invokespecial   android/content/Intent.<init>:(Ljava/lang/String;)V
            //   586: astore_1       
            //   587: aload_1        
            //   588: aload_0        
            //   589: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mInstaller:Landroid/content/ComponentName;
            //   592: invokevirtual   android/content/Intent.setComponent:(Landroid/content/ComponentName;)Landroid/content/Intent;
            //   595: pop            
            //   596: aload_1        
            //   597: ldc             "android.intent.extra.BUG_REPORT"
            //   599: aload_2        
            //   600: invokevirtual   android/content/Intent.putExtra:(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
            //   603: pop            
            //   604: aload_1        
            //   605: ldc_w           268435456
            //   608: invokevirtual   android/content/Intent.addFlags:(I)Landroid/content/Intent;
            //   611: pop            
            //   612: aload_0        
            //   613: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   616: aload_1        
            //   617: invokevirtual   com/android/settings/applications/RunningServiceDetails.startActivity:(Landroid/content/Intent;)V
            //   620: return         
            //   621: aload           9
            //   623: ifnull          636
            //   626: aload           9
            //   628: invokevirtual   java/io/FileInputStream.close:()V
            //   631: goto            636
            //   634: astore          10
            //   636: aload_1        
            //   637: athrow         
            //   638: aload           9
            //   640: ifnull          653
            //   643: aload           9
            //   645: invokevirtual   java/io/FileOutputStream.close:()V
            //   648: goto            653
            //   651: astore          10
            //   653: aload_1        
            //   654: athrow         
            //   655: aload_0        
            //   656: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mManageIntent:Landroid/app/PendingIntent;
            //   659: ifnull          723
            //   662: aload_0        
            //   663: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   666: invokevirtual   com/android/settings/applications/RunningServiceDetails.getActivity:()Landroid/app/Activity;
            //   669: aload_0        
            //   670: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mManageIntent:Landroid/app/PendingIntent;
            //   673: invokevirtual   android/app/PendingIntent.getIntentSender:()Landroid/content/IntentSender;
            //   676: aconst_null    
            //   677: ldc_w           268959744
            //   680: ldc_w           524288
            //   683: iconst_0       
            //   684: invokevirtual   android/app/Activity.startIntentSender:(Landroid/content/IntentSender;Landroid/content/Intent;III)V
            //   687: goto            720
            //   690: astore_1       
            //   691: ldc             "RunningServicesDetails"
            //   693: aload_1        
            //   694: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/Throwable;)I
            //   697: pop            
            //   698: goto            720
            //   701: astore_1       
            //   702: ldc             "RunningServicesDetails"
            //   704: aload_1        
            //   705: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/Throwable;)I
            //   708: pop            
            //   709: goto            720
            //   712: astore_1       
            //   713: ldc             "RunningServicesDetails"
            //   715: aload_1        
            //   716: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/Throwable;)I
            //   719: pop            
            //   720: goto            814
            //   723: aload_0        
            //   724: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;
            //   727: ifnull          738
            //   730: aload_0        
            //   731: iconst_0       
            //   732: invokevirtual   com/android/settings/applications/RunningServiceDetails$ActiveDetail.stopActiveService:(Z)V
            //   735: goto            814
            //   738: aload_0        
            //   739: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
            //   742: getfield        com/android/settings/applications/RunningProcessesView$ActiveItem.mItem:Lcom/android/settings/applications/RunningState$BaseItem;
            //   745: getfield        com/android/settings/applications/RunningState$BaseItem.mBackground:Z
            //   748: ifeq            784
            //   751: aload_0        
            //   752: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   755: getfield        com/android/settings/applications/RunningServiceDetails.mAm:Landroid/app/ActivityManager;
            //   758: aload_0        
            //   759: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
            //   762: getfield        com/android/settings/applications/RunningProcessesView$ActiveItem.mItem:Lcom/android/settings/applications/RunningState$BaseItem;
            //   765: getfield        com/android/settings/applications/RunningState$BaseItem.mPackageInfo:Landroid/content/pm/PackageItemInfo;
            //   768: getfield        android/content/pm/PackageItemInfo.packageName:Ljava/lang/String;
            //   771: invokevirtual   android/app/ActivityManager.killBackgroundProcesses:(Ljava/lang/String;)V
            //   774: aload_0        
            //   775: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   778: invokestatic    com/android/settings/applications/RunningServiceDetails.access$100:(Lcom/android/settings/applications/RunningServiceDetails;)V
            //   781: goto            814
            //   784: aload_0        
            //   785: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   788: getfield        com/android/settings/applications/RunningServiceDetails.mAm:Landroid/app/ActivityManager;
            //   791: aload_0        
            //   792: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
            //   795: getfield        com/android/settings/applications/RunningProcessesView$ActiveItem.mItem:Lcom/android/settings/applications/RunningState$BaseItem;
            //   798: getfield        com/android/settings/applications/RunningState$BaseItem.mPackageInfo:Landroid/content/pm/PackageItemInfo;
            //   801: getfield        android/content/pm/PackageItemInfo.packageName:Ljava/lang/String;
            //   804: invokevirtual   android/app/ActivityManager.forceStopPackage:(Ljava/lang/String;)V
            //   807: aload_0        
            //   808: getfield        com/android/settings/applications/RunningServiceDetails$ActiveDetail.this$0:Lcom/android/settings/applications/RunningServiceDetails;
            //   811: invokestatic    com/android/settings/applications/RunningServiceDetails.access$100:(Lcom/android/settings/applications/RunningServiceDetails;)V
            //   814: return         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                              
            //  -----  -----  -----  -----  --------------------------------------------------
            //  201    206    278    341    Ljava/io/IOException;
            //  201    206    274    655    Any
            //  213    220    278    341    Ljava/io/IOException;
            //  213    220    274    655    Any
            //  227    260    278    341    Ljava/io/IOException;
            //  227    260    274    655    Any
            //  263    267    270    274    Ljava/io/IOException;
            //  283    288    274    655    Any
            //  291    296    274    655    Any
            //  299    307    274    655    Any
            //  310    318    274    655    Any
            //  321    334    274    655    Any
            //  354    359    467    530    Ljava/io/IOException;
            //  354    359    463    638    Any
            //  366    373    467    530    Ljava/io/IOException;
            //  366    373    463    638    Any
            //  384    394    467    530    Ljava/io/IOException;
            //  384    394    463    638    Any
            //  401    409    467    530    Ljava/io/IOException;
            //  401    409    463    638    Any
            //  416    421    467    530    Ljava/io/IOException;
            //  416    421    463    638    Any
            //  428    435    467    530    Ljava/io/IOException;
            //  428    435    463    638    Any
            //  442    449    467    530    Ljava/io/IOException;
            //  442    449    463    638    Any
            //  452    456    459    463    Ljava/io/IOException;
            //  472    477    463    638    Any
            //  480    485    463    638    Any
            //  488    496    463    638    Any
            //  499    507    463    638    Any
            //  510    523    463    638    Any
            //  626    631    634    636    Ljava/io/IOException;
            //  643    648    651    653    Ljava/io/IOException;
            //  662    687    712    720    Landroid/content/IntentSender$SendIntentException;
            //  662    687    701    712    Ljava/lang/IllegalArgumentException;
            //  662    687    690    701    Landroid/content/ActivityNotFoundException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index: 400, Size: 400
            //     at java.util.ArrayList.rangeCheck(ArrayList.java:653)
            //     at java.util.ArrayList.get(ArrayList.java:429)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Thread.java:745)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        void stopActiveService(final boolean b) {
            final ServiceItem mServiceItem = this.mServiceItem;
            if (!b && (mServiceItem.mServiceInfo.applicationInfo.flags & 0x1) != 0x0) {
                RunningServiceDetails.this.showConfirmStopDialog(mServiceItem.mRunningService.service);
                return;
            }
            RunningServiceDetails.this.getActivity().stopService(new Intent().setComponent(mServiceItem.mRunningService.service));
            if (RunningServiceDetails.this.mMergedItem == null) {
                RunningServiceDetails.this.mState.updateNow();
                RunningServiceDetails.this.finish();
            }
            else if (!RunningServiceDetails.this.mShowBackground && RunningServiceDetails.this.mMergedItem.mServices.size() <= 1) {
                RunningServiceDetails.this.mState.updateNow();
                RunningServiceDetails.this.finish();
            }
            else {
                RunningServiceDetails.this.mState.updateNow();
            }
        }
    }
    
    public static class MyAlertDialogFragment extends InstrumentedDialogFragment
    {
        public static MyAlertDialogFragment newConfirmStop(final int n, final ComponentName componentName) {
            final MyAlertDialogFragment myAlertDialogFragment = new MyAlertDialogFragment();
            final Bundle arguments = new Bundle();
            arguments.putInt("id", n);
            arguments.putParcelable("comp", (Parcelable)componentName);
            myAlertDialogFragment.setArguments(arguments);
            return myAlertDialogFragment;
        }
        
        @Override
        public int getMetricsCategory() {
            return 536;
        }
        
        RunningServiceDetails getOwner() {
            return (RunningServiceDetails)this.getTargetFragment();
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final int int1 = this.getArguments().getInt("id");
            if (int1 != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown id ");
                sb.append(int1);
                throw new IllegalArgumentException(sb.toString());
            }
            final ComponentName componentName = (ComponentName)this.getArguments().getParcelable("comp");
            if (this.getOwner().activeDetailForService(componentName) == null) {
                return null;
            }
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle((CharSequence)this.getActivity().getString(2131888883)).setMessage((CharSequence)this.getActivity().getString(2131888882)).setPositiveButton(2131887460, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    final ActiveDetail activeDetailForService = MyAlertDialogFragment.this.getOwner().activeDetailForService(componentName);
                    if (activeDetailForService != null) {
                        activeDetailForService.stopActiveService(true);
                    }
                }
            }).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
        }
    }
}
