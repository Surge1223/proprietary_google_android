package com.android.settings.applications;

import android.os.UserHandle;
import android.view.View.OnClickListener;
import java.util.Iterator;
import android.util.ArrayMap;
import java.util.Map;
import android.app.usage.UsageEvents;
import android.app.usage.UsageEvents$Event;
import android.os.RemoteException;
import android.view.ViewGroup;
import android.widget.Switch;
import android.view.View;
import com.android.settingslib.utils.StringUtil;
import com.android.settings.Utils;
import java.util.ArrayList;
import android.os.UserManager;
import java.util.List;
import android.app.usage.IUsageStatsManager;
import android.content.Context;
import com.android.settings.notification.NotificationBackend;
import java.util.Comparator;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateNotificationBridge extends AppStateBaseBridge
{
    public static final AppFilter FILTER_APP_NOTIFICATION_FREQUENCY;
    public static final AppFilter FILTER_APP_NOTIFICATION_RECENCY;
    public static final Comparator<AppEntry> FREQUENCY_NOTIFICATION_COMPARATOR;
    public static final Comparator<AppEntry> RECENT_NOTIFICATION_COMPARATOR;
    private NotificationBackend mBackend;
    private final Context mContext;
    private IUsageStatsManager mUsageStatsManager;
    protected List<Integer> mUserIds;
    
    static {
        FILTER_APP_NOTIFICATION_RECENCY = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                final NotificationsSentState access$000 = getNotificationsSentState(appEntry);
                boolean b = false;
                if (access$000 != null) {
                    if (access$000.lastSent != 0L) {
                        b = true;
                    }
                    return b;
                }
                return false;
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_APP_NOTIFICATION_FREQUENCY = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                final NotificationsSentState access$000 = getNotificationsSentState(appEntry);
                boolean b = false;
                if (access$000 != null) {
                    if (access$000.sentCount != 0) {
                        b = true;
                    }
                    return b;
                }
                return false;
            }
            
            @Override
            public void init() {
            }
        };
        RECENT_NOTIFICATION_COMPARATOR = new Comparator<AppEntry>() {
            @Override
            public int compare(final AppEntry appEntry, final AppEntry appEntry2) {
                final NotificationsSentState access$000 = getNotificationsSentState(appEntry);
                final NotificationsSentState access$2 = getNotificationsSentState(appEntry2);
                if (access$000 == null && access$2 != null) {
                    return -1;
                }
                if (access$000 != null && access$2 == null) {
                    return 1;
                }
                if (access$000 != null && access$2 != null) {
                    if (access$000.lastSent < access$2.lastSent) {
                        return 1;
                    }
                    if (access$000.lastSent > access$2.lastSent) {
                        return -1;
                    }
                }
                return ApplicationsState.ALPHA_COMPARATOR.compare(appEntry, appEntry2);
            }
        };
        FREQUENCY_NOTIFICATION_COMPARATOR = new Comparator<AppEntry>() {
            @Override
            public int compare(final AppEntry appEntry, final AppEntry appEntry2) {
                final NotificationsSentState access$000 = getNotificationsSentState(appEntry);
                final NotificationsSentState access$2 = getNotificationsSentState(appEntry2);
                if (access$000 == null && access$2 != null) {
                    return -1;
                }
                if (access$000 != null && access$2 == null) {
                    return 1;
                }
                if (access$000 != null && access$2 != null) {
                    if (access$000.sentCount < access$2.sentCount) {
                        return 1;
                    }
                    if (access$000.sentCount > access$2.sentCount) {
                        return -1;
                    }
                }
                return ApplicationsState.ALPHA_COMPARATOR.compare(appEntry, appEntry2);
            }
        };
    }
    
    public AppStateNotificationBridge(final Context mContext, final ApplicationsState applicationsState, final Callback callback, final IUsageStatsManager mUsageStatsManager, final UserManager userManager, final NotificationBackend mBackend) {
        super(applicationsState, callback);
        this.mContext = mContext;
        this.mUsageStatsManager = mUsageStatsManager;
        this.mBackend = mBackend;
        (this.mUserIds = new ArrayList<Integer>()).add(this.mContext.getUserId());
        final int managedProfileId = Utils.getManagedProfileId(userManager, this.mContext.getUserId());
        if (managedProfileId != -10000) {
            this.mUserIds.add(managedProfileId);
        }
    }
    
    private void addBlockStatus(final AppEntry appEntry, final NotificationsSentState notificationsSentState) {
        if (notificationsSentState != null) {
            notificationsSentState.blocked = this.mBackend.getNotificationsBanned(appEntry.info.packageName, appEntry.info.uid);
            notificationsSentState.systemApp = this.mBackend.isSystemApp(this.mContext, appEntry.info);
            notificationsSentState.blockable = (!notificationsSentState.systemApp || (notificationsSentState.systemApp && notificationsSentState.blocked));
        }
    }
    
    private void calculateAvgSentCounts(final NotificationsSentState notificationsSentState) {
        if (notificationsSentState != null) {
            notificationsSentState.avgSentDaily = Math.round(notificationsSentState.sentCount / 7.0f);
            if (notificationsSentState.sentCount < 7) {
                notificationsSentState.avgSentWeekly = notificationsSentState.sentCount;
            }
        }
    }
    
    public static final boolean checkSwitch(final AppEntry appEntry) {
        final NotificationsSentState notificationsSentState = getNotificationsSentState(appEntry);
        return notificationsSentState != null && (notificationsSentState.blocked ^ true);
    }
    
    public static final boolean enableSwitch(final AppEntry appEntry) {
        final NotificationsSentState notificationsSentState = getNotificationsSentState(appEntry);
        return notificationsSentState != null && notificationsSentState.blockable;
    }
    
    protected static String getKey(final int n, final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append("|");
        sb.append(s);
        return sb.toString();
    }
    
    private static NotificationsSentState getNotificationsSentState(final AppEntry appEntry) {
        if (appEntry == null || appEntry.extraInfo == null) {
            return null;
        }
        if (appEntry.extraInfo instanceof NotificationsSentState) {
            return (NotificationsSentState)appEntry.extraInfo;
        }
        return null;
    }
    
    public static CharSequence getSummary(final Context context, final NotificationsSentState notificationsSentState, final boolean b) {
        if (b) {
            if (notificationsSentState.lastSent == 0L) {
                return context.getString(2131888518);
            }
            return StringUtil.formatRelativeTime(context, System.currentTimeMillis() - notificationsSentState.lastSent, true);
        }
        else {
            if (notificationsSentState.avgSentWeekly > 0) {
                return context.getString(2131888519, new Object[] { notificationsSentState.avgSentWeekly });
            }
            return context.getString(2131888517, new Object[] { notificationsSentState.avgSentDaily });
        }
    }
    
    protected NotificationsSentState getAggregatedUsageEvents(final int n, final String s) {
        NotificationsSentState notificationsSentState = null;
        final NotificationsSentState notificationsSentState2 = null;
        final long currentTimeMillis = System.currentTimeMillis();
        UsageEvents queryEventsForPackageForUser = null;
        try {
            queryEventsForPackageForUser = this.mUsageStatsManager.queryEventsForPackageForUser(currentTimeMillis - 604800000L, currentTimeMillis, n, s, this.mContext.getPackageName());
        }
        catch (RemoteException ex) {
            ex.printStackTrace();
        }
        if (queryEventsForPackageForUser != null) {
            final UsageEvents$Event usageEvents$Event = new UsageEvents$Event();
            NotificationsSentState notificationsSentState3 = notificationsSentState2;
            while (true) {
                notificationsSentState = notificationsSentState3;
                if (!queryEventsForPackageForUser.hasNextEvent()) {
                    break;
                }
                queryEventsForPackageForUser.getNextEvent(usageEvents$Event);
                if (usageEvents$Event.getEventType() != 12) {
                    continue;
                }
                NotificationsSentState notificationsSentState4;
                if ((notificationsSentState4 = notificationsSentState3) == null) {
                    notificationsSentState4 = new NotificationsSentState();
                }
                if (usageEvents$Event.getTimeStamp() > notificationsSentState4.lastSent) {
                    notificationsSentState4.lastSent = usageEvents$Event.getTimeStamp();
                }
                ++notificationsSentState4.sentCount;
                notificationsSentState3 = notificationsSentState4;
            }
        }
        return notificationsSentState;
    }
    
    protected Map<String, NotificationsSentState> getAggregatedUsageEvents() {
        final ArrayMap arrayMap = new ArrayMap();
        final long currentTimeMillis = System.currentTimeMillis();
        for (final int intValue : this.mUserIds) {
            UsageEvents queryEventsForUser = null;
            try {
                queryEventsForUser = this.mUsageStatsManager.queryEventsForUser(currentTimeMillis - 604800000L, currentTimeMillis, intValue, this.mContext.getPackageName());
            }
            catch (RemoteException ex) {
                ex.printStackTrace();
            }
            if (queryEventsForUser != null) {
                final UsageEvents$Event usageEvents$Event = new UsageEvents$Event();
                while (queryEventsForUser.hasNextEvent()) {
                    queryEventsForUser.getNextEvent(usageEvents$Event);
                    NotificationsSentState notificationsSentState;
                    if ((notificationsSentState = (NotificationsSentState)arrayMap.get((Object)getKey(intValue, usageEvents$Event.getPackageName()))) == null) {
                        notificationsSentState = new NotificationsSentState();
                        arrayMap.put((Object)getKey(intValue, usageEvents$Event.getPackageName()), (Object)notificationsSentState);
                    }
                    if (usageEvents$Event.getEventType() == 12) {
                        if (usageEvents$Event.getTimeStamp() > notificationsSentState.lastSent) {
                            notificationsSentState.lastSent = usageEvents$Event.getTimeStamp();
                        }
                        ++notificationsSentState.sentCount;
                    }
                }
            }
        }
        return (Map<String, NotificationsSentState>)arrayMap;
    }
    
    public View.OnClickListener getSwitchOnClickListener(final AppEntry appEntry) {
        if (appEntry != null) {
            return (View.OnClickListener)new _$$Lambda$AppStateNotificationBridge$3yb6PrF82n91FG3YEHY_Ccl1JyI(this, appEntry);
        }
        return null;
    }
    
    @Override
    protected void loadAllExtraInfo() {
        final ArrayList<AppEntry> allApps = this.mAppSession.getAllApps();
        if (allApps == null) {
            return;
        }
        final Map<String, NotificationsSentState> aggregatedUsageEvents = this.getAggregatedUsageEvents();
        for (final AppEntry appEntry : allApps) {
            final NotificationsSentState extraInfo = aggregatedUsageEvents.get(getKey(UserHandle.getUserId(appEntry.info.uid), appEntry.info.packageName));
            this.calculateAvgSentCounts(extraInfo);
            this.addBlockStatus(appEntry, extraInfo);
            appEntry.extraInfo = extraInfo;
        }
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        final NotificationsSentState aggregatedUsageEvents = this.getAggregatedUsageEvents(UserHandle.getUserId(appEntry.info.uid), appEntry.info.packageName);
        this.calculateAvgSentCounts(aggregatedUsageEvents);
        this.addBlockStatus(appEntry, aggregatedUsageEvents);
        appEntry.extraInfo = aggregatedUsageEvents;
    }
    
    public static class NotificationsSentState
    {
        public int avgSentDaily;
        public int avgSentWeekly;
        public boolean blockable;
        public boolean blocked;
        public long lastSent;
        public int sentCount;
        public boolean systemApp;
        
        public NotificationsSentState() {
            this.avgSentDaily = 0;
            this.avgSentWeekly = 0;
            this.lastSent = 0L;
            this.sentCount = 0;
        }
    }
}
