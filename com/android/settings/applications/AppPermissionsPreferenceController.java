package com.android.settings.applications;

import android.content.pm.PackageInfo;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.pm.PermissionInfo;
import android.util.ArraySet;
import java.util.Set;
import android.text.TextUtils;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.settings.core.BasePreferenceController;

public class AppPermissionsPreferenceController extends BasePreferenceController
{
    private static final String KEY_APP_PERMISSION_GROUPS = "manage_perms";
    private static final int NUM_PERMISSION_TO_USE = 3;
    private static final String[] PERMISSION_GROUPS;
    private static final String TAG = "AppPermissionPrefCtrl";
    private final PackageManager mPackageManager;
    
    static {
        PERMISSION_GROUPS = new String[] { "android.permission-group.LOCATION", "android.permission-group.MICROPHONE", "android.permission-group.CAMERA", "android.permission-group.SMS", "android.permission-group.CONTACTS", "android.permission-group.PHONE" };
    }
    
    public AppPermissionsPreferenceController(final Context context) {
        super(context, "manage_perms");
        this.mPackageManager = context.getPackageManager();
    }
    
    private CharSequence concatSummaryText(final CharSequence charSequence, String lowerCase) {
        lowerCase = this.getPermissionGroupLabel(lowerCase).toString().toLowerCase();
        if (TextUtils.isEmpty(charSequence)) {
            return lowerCase;
        }
        return this.mContext.getString(2131887916, new Object[] { charSequence, lowerCase });
    }
    
    private Set<String> getAllPermissionsInGroups() {
        final ArraySet set = new ArraySet();
        for (final String s : AppPermissionsPreferenceController.PERMISSION_GROUPS) {
            try {
                final Iterator iterator = this.mPackageManager.queryPermissionsByGroup(s, 0).iterator();
                while (iterator.hasNext()) {
                    set.add((Object)iterator.next().name);
                }
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error getting permissions in group ");
                sb.append(s);
                Log.e("AppPermissionPrefCtrl", sb.toString(), (Throwable)ex);
            }
        }
        return (Set<String>)set;
    }
    
    private Set<String> getGrantedPermissionGroups(final Set<String> set) {
        final ArraySet set2 = new ArraySet();
        for (final PackageInfo packageInfo : this.mPackageManager.getInstalledPackages(4096)) {
            if (packageInfo.permissions == null) {
                continue;
            }
            for (final PermissionInfo permissionInfo : packageInfo.permissions) {
                if (set.contains(permissionInfo.name) && !set2.contains((Object)permissionInfo.group)) {
                    set2.add((Object)permissionInfo.group);
                }
            }
        }
        return (Set<String>)set2;
    }
    
    private CharSequence getPermissionGroupLabel(final String s) {
        try {
            return this.mPackageManager.getPermissionGroupInfo(s, 0).loadLabel(this.mPackageManager);
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e("AppPermissionPrefCtrl", "Error getting permissions label.", (Throwable)ex);
            return s;
        }
    }
    
    @Override
    public int getAvailabilityStatus() {
        return 0;
    }
    
    @Override
    public CharSequence getSummary() {
        final Set<String> grantedPermissionGroups = this.getGrantedPermissionGroups(this.getAllPermissionsInGroups());
        final String[] permission_GROUPS = AppPermissionsPreferenceController.PERMISSION_GROUPS;
        final int length = permission_GROUPS.length;
        int n = 0;
        CharSequence charSequence = null;
        int n2 = 0;
        CharSequence concatSummaryText;
        int n3;
        while (true) {
            concatSummaryText = charSequence;
            n3 = n;
            if (n2 >= length) {
                break;
            }
            final String s = permission_GROUPS[n2];
            if (grantedPermissionGroups.contains(s)) {
                concatSummaryText = this.concatSummaryText(charSequence, s);
                n3 = n + 1;
                charSequence = concatSummaryText;
                if ((n = n3) >= 3) {
                    break;
                }
            }
            ++n2;
        }
        String string;
        if (n3 > 0) {
            string = this.mContext.getString(2131886395, new Object[] { concatSummaryText });
        }
        else {
            string = null;
        }
        return string;
    }
}
