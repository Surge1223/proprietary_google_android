package com.android.settings.applications;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class PremiumSmsController extends BasePreferenceController
{
    static final String KEY_PREMIUM_SMS = "premium_sms";
    
    public PremiumSmsController(final Context context) {
        super(context, "premium_sms");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034158)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
