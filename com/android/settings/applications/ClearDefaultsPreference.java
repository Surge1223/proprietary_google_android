package com.android.settings.applications;

import android.text.TextUtils;
import android.text.style.BulletSpan;
import android.text.SpannableString;
import com.android.settingslib.applications.AppUtils;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.PreferenceViewHolder;
import android.os.UserHandle;
import android.widget.TextView;
import android.hardware.usb.IUsbManager$Stub;
import android.os.ServiceManager;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.AttributeSet;
import android.content.Context;
import android.hardware.usb.IUsbManager;
import android.content.pm.PackageManager;
import android.appwidget.AppWidgetManager;
import com.android.settingslib.applications.ApplicationsState;
import android.widget.Button;
import android.support.v7.preference.Preference;

public class ClearDefaultsPreference extends Preference
{
    protected static final String TAG;
    private Button mActivitiesButton;
    protected ApplicationsState.AppEntry mAppEntry;
    private AppWidgetManager mAppWidgetManager;
    private String mPackageName;
    private PackageManager mPm;
    private IUsbManager mUsbManager;
    
    static {
        TAG = ClearDefaultsPreference.class.getSimpleName();
    }
    
    public ClearDefaultsPreference(final Context context) {
        this(context, null);
    }
    
    public ClearDefaultsPreference(final Context context, final AttributeSet set) {
        this(context, set, TypedArrayUtils.getAttr(context, R.attr.preferenceStyle, 16842894));
    }
    
    public ClearDefaultsPreference(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public ClearDefaultsPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.setLayoutResource(2131558452);
        this.mAppWidgetManager = AppWidgetManager.getInstance(context);
        this.mPm = context.getPackageManager();
        this.mUsbManager = IUsbManager$Stub.asInterface(ServiceManager.getService("usb"));
    }
    
    private boolean isDefaultBrowser(final String s) {
        return s.equals(this.mPm.getDefaultBrowserPackageNameAsUser(UserHandle.myUserId()));
    }
    
    private void resetLaunchDefaultsUi(final TextView textView) {
        textView.setText(2131886488);
        this.mActivitiesButton.setEnabled(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        (this.mActivitiesButton = (Button)preferenceViewHolder.findViewById(2131361986)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (ClearDefaultsPreference.this.mUsbManager != null) {
                    final int myUserId = UserHandle.myUserId();
                    ClearDefaultsPreference.this.mPm.clearPackagePreferredActivities(ClearDefaultsPreference.this.mPackageName);
                    if (ClearDefaultsPreference.this.isDefaultBrowser(ClearDefaultsPreference.this.mPackageName)) {
                        ClearDefaultsPreference.this.mPm.setDefaultBrowserPackageNameAsUser((String)null, myUserId);
                    }
                    try {
                        ClearDefaultsPreference.this.mUsbManager.clearDefaults(ClearDefaultsPreference.this.mPackageName, myUserId);
                    }
                    catch (RemoteException ex) {
                        Log.e(ClearDefaultsPreference.TAG, "mUsbManager.clearDefaults", (Throwable)ex);
                    }
                    ClearDefaultsPreference.this.mAppWidgetManager.setBindAppWidgetPermission(ClearDefaultsPreference.this.mPackageName, false);
                    ClearDefaultsPreference.this.resetLaunchDefaultsUi((TextView)preferenceViewHolder.findViewById(2131361894));
                }
            }
        });
        this.updateUI(preferenceViewHolder);
    }
    
    public void setAppEntry(final ApplicationsState.AppEntry mAppEntry) {
        this.mAppEntry = mAppEntry;
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
    }
    
    public boolean updateUI(final PreferenceViewHolder preferenceViewHolder) {
        final boolean hasBindAppWidgetPermission = this.mAppWidgetManager.hasBindAppWidgetPermission(this.mAppEntry.info.packageName);
        final TextView textView = (TextView)preferenceViewHolder.findViewById(2131361894);
        final boolean b = AppUtils.hasPreferredActivities(this.mPm, this.mPackageName) || this.isDefaultBrowser(this.mPackageName) || AppUtils.hasUsbDefaults(this.mUsbManager, this.mPackageName);
        if (!b && !hasBindAppWidgetPermission) {
            this.resetLaunchDefaultsUi(textView);
        }
        else {
            final boolean b2 = hasBindAppWidgetPermission && b;
            if (hasBindAppWidgetPermission) {
                textView.setText(2131886491);
            }
            else {
                textView.setText(2131886490);
            }
            final Context context = this.getContext();
            CharSequence charSequence = null;
            final int dimensionPixelSize = context.getResources().getDimensionPixelSize(2131165469);
            if (b) {
                final CharSequence text = context.getText(2131886489);
                final SpannableString spannableString = new SpannableString(text);
                if (b2) {
                    spannableString.setSpan((Object)new BulletSpan(dimensionPixelSize), 0, text.length(), 0);
                }
                if (!false) {
                    charSequence = TextUtils.concat(new CharSequence[] { spannableString, "\n" });
                }
                else {
                    charSequence = TextUtils.concat(new CharSequence[] { null, "\n", spannableString, "\n" });
                }
            }
            CharSequence text2 = charSequence;
            if (hasBindAppWidgetPermission) {
                final CharSequence text3 = context.getText(2131886295);
                final SpannableString spannableString2 = new SpannableString(text3);
                if (b2) {
                    spannableString2.setSpan((Object)new BulletSpan(dimensionPixelSize), 0, text3.length(), 0);
                }
                CharSequence charSequence2;
                if (charSequence == null) {
                    charSequence2 = TextUtils.concat(new CharSequence[] { spannableString2, "\n" });
                }
                else {
                    charSequence2 = TextUtils.concat(new CharSequence[] { charSequence, "\n", spannableString2, "\n" });
                }
                text2 = charSequence2;
            }
            textView.setText(text2);
            this.mActivitiesButton.setEnabled(true);
        }
        return true;
    }
}
