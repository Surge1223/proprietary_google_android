package com.android.settings.applications;

import java.util.Objects;
import android.text.TextUtils;
import android.content.pm.UserInfo;
import android.content.pm.ApplicationInfo;

public class UserAppInfo
{
    public final ApplicationInfo appInfo;
    public final UserInfo userInfo;
    
    public UserAppInfo(final UserInfo userInfo, final ApplicationInfo appInfo) {
        this.userInfo = userInfo;
        this.appInfo = appInfo;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final UserAppInfo userAppInfo = (UserAppInfo)o;
            if (userAppInfo.userInfo.id != this.userInfo.id || !TextUtils.equals((CharSequence)userAppInfo.appInfo.packageName, (CharSequence)this.appInfo.packageName)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.userInfo.id, this.appInfo.packageName);
    }
}
