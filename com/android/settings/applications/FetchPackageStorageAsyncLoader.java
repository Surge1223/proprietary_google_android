package com.android.settings.applications;

import java.io.IOException;
import android.content.pm.PackageManager;
import android.util.Log;
import com.android.internal.util.Preconditions;
import android.content.Context;
import android.os.UserHandle;
import android.content.pm.ApplicationInfo;
import com.android.settingslib.applications.StorageStatsSource;
import com.android.settingslib.utils.AsyncLoader;

public class FetchPackageStorageAsyncLoader extends AsyncLoader<StorageStatsSource.AppStorageStats>
{
    private final ApplicationInfo mInfo;
    private final StorageStatsSource mSource;
    private final UserHandle mUser;
    
    public FetchPackageStorageAsyncLoader(final Context context, final StorageStatsSource storageStatsSource, final ApplicationInfo mInfo, final UserHandle mUser) {
        super(context);
        this.mSource = (StorageStatsSource)Preconditions.checkNotNull((Object)storageStatsSource);
        this.mInfo = mInfo;
        this.mUser = mUser;
    }
    
    public StorageStatsSource.AppStorageStats loadInBackground() {
        Object statsForPackage = null;
        try {
            statsForPackage = this.mSource.getStatsForPackage(this.mInfo.volumeUuid, this.mInfo.packageName, this.mUser);
        }
        catch (PackageManager$NameNotFoundException | IOException ex) {
            final Throwable t;
            Log.w("FetchPackageStorage", "Package may have been removed during query, failing gracefully", t);
        }
        return (StorageStatsSource.AppStorageStats)statsForPackage;
    }
    
    @Override
    protected void onDiscardResult(final StorageStatsSource.AppStorageStats appStorageStats) {
    }
}
