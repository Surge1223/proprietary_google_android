package com.android.settings.applications;

import java.util.Set;
import java.util.List;
import android.content.Intent;

public interface ApplicationFeatureProvider
{
    void calculateNumberOfAppsWithAdminGrantedPermissions(final String[] p0, final boolean p1, final NumberOfAppsCallback p2);
    
    void calculateNumberOfPolicyInstalledApps(final boolean p0, final NumberOfAppsCallback p1);
    
    List<UserAppInfo> findPersistentPreferredActivities(final int p0, final Intent[] p1);
    
    Set<String> getKeepEnabledPackages();
    
    void listAppsWithAdminGrantedPermissions(final String[] p0, final ListOfAppsCallback p1);
    
    void listPolicyInstalledApps(final ListOfAppsCallback p0);
    
    public interface ListOfAppsCallback
    {
        void onListOfAppsResult(final List<UserAppInfo> p0);
    }
    
    public interface NumberOfAppsCallback
    {
        void onNumberOfAppsResult(final int p0);
    }
}
