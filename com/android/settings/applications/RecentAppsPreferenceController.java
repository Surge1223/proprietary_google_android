package com.android.settings.applications;

import java.util.Map;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.applications.AppUtils;
import android.content.Intent;
import android.util.Log;
import com.android.settings.applications.appinfo.AppInfoDashboardFragment;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import com.android.settingslib.utils.StringUtil;
import com.android.settings.widget.AppPreference;
import android.util.ArrayMap;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.app.Application;
import android.content.Context;
import java.util.Collection;
import java.util.Arrays;
import android.util.ArraySet;
import android.app.usage.UsageStatsManager;
import java.util.List;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import java.util.Calendar;
import com.android.settingslib.applications.ApplicationsState;
import java.util.Set;
import android.app.usage.UsageStats;
import java.util.Comparator;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class RecentAppsPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, Comparator<UsageStats>
{
    static final String KEY_DIVIDER = "all_app_info_divider";
    static final String KEY_SEE_ALL = "all_app_info";
    private static final Set<String> SKIP_SYSTEM_PACKAGES;
    private final ApplicationsState mApplicationsState;
    private Calendar mCal;
    private PreferenceCategory mCategory;
    private Preference mDivider;
    private boolean mHasRecentApps;
    private final Fragment mHost;
    private final IconDrawableFactory mIconDrawableFactory;
    private final PackageManager mPm;
    private Preference mSeeAllPref;
    private List<UsageStats> mStats;
    private final UsageStatsManager mUsageStatsManager;
    private final int mUserId;
    
    static {
        (SKIP_SYSTEM_PACKAGES = (Set)new ArraySet()).addAll(Arrays.asList("android", "com.android.phone", "com.android.settings", "com.android.systemui", "com.android.providers.calendar", "com.android.providers.media"));
    }
    
    public RecentAppsPreferenceController(final Context context, final Application application, final Fragment fragment) {
        ApplicationsState instance;
        if (application == null) {
            instance = null;
        }
        else {
            instance = ApplicationsState.getInstance(application);
        }
        this(context, instance, fragment);
    }
    
    RecentAppsPreferenceController(final Context context, final ApplicationsState mApplicationsState, final Fragment mHost) {
        super(context);
        this.mIconDrawableFactory = IconDrawableFactory.newInstance(context);
        this.mUserId = UserHandle.myUserId();
        this.mPm = context.getPackageManager();
        this.mHost = mHost;
        this.mUsageStatsManager = (UsageStatsManager)context.getSystemService("usagestats");
        this.mApplicationsState = mApplicationsState;
    }
    
    private void displayOnlyAppInfo() {
        this.mCategory.setTitle(null);
        this.mDivider.setVisible(false);
        this.mSeeAllPref.setTitle(2131886408);
        this.mSeeAllPref.setIcon(null);
        for (int i = this.mCategory.getPreferenceCount() - 1; i >= 0; --i) {
            final Preference preference = this.mCategory.getPreference(i);
            if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)"all_app_info")) {
                this.mCategory.removePreference(preference);
            }
        }
    }
    
    private void displayRecentApps(final Context context, final List<UsageStats> list) {
        this.mCategory.setTitle(2131888794);
        this.mDivider.setVisible(true);
        this.mSeeAllPref.setSummary(null);
        this.mSeeAllPref.setIcon(2131230994);
        final ArrayMap arrayMap = new ArrayMap();
        for (int preferenceCount = this.mCategory.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = this.mCategory.getPreference(i);
            final String key = preference.getKey();
            if (!TextUtils.equals((CharSequence)key, (CharSequence)"all_app_info")) {
                ((Map<String, Preference>)arrayMap).put(key, preference);
            }
        }
        for (int size = list.size(), j = 0; j < size; ++j) {
            final UsageStats usageStats = list.get(j);
            final String packageName = usageStats.getPackageName();
            final ApplicationsState.AppEntry entry = this.mApplicationsState.getEntry(packageName, this.mUserId);
            if (entry != null) {
                boolean b = true;
                Preference preference2 = ((Map<String, Preference>)arrayMap).remove(packageName);
                if (preference2 == null) {
                    preference2 = new AppPreference(context);
                    b = false;
                }
                preference2.setKey(packageName);
                preference2.setTitle(entry.label);
                preference2.setIcon(this.mIconDrawableFactory.getBadgedIcon(entry.info));
                preference2.setSummary(StringUtil.formatRelativeTime(this.mContext, System.currentTimeMillis() - usageStats.getLastTimeUsed(), false));
                preference2.setOrder(j);
                preference2.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new _$$Lambda$RecentAppsPreferenceController$benLpqwf0HURWhX82bB7mmwJ8Oo(this, packageName, entry));
                if (!b) {
                    this.mCategory.addPreference(preference2);
                }
            }
        }
        final Iterator<Preference> iterator = ((Map<String, Preference>)arrayMap).values().iterator();
        while (iterator.hasNext()) {
            this.mCategory.removePreference(iterator.next());
        }
    }
    
    private List<UsageStats> getDisplayableRecentAppList() {
        final ArrayList<UsageStats> list = new ArrayList<UsageStats>();
        final ArrayMap arrayMap = new ArrayMap();
        for (int size = this.mStats.size(), i = 0; i < size; ++i) {
            final UsageStats usageStats = this.mStats.get(i);
            if (this.shouldIncludePkgInRecents(usageStats)) {
                final String packageName = usageStats.getPackageName();
                final UsageStats usageStats2 = ((Map<String, UsageStats>)arrayMap).get(packageName);
                if (usageStats2 == null) {
                    ((Map<String, UsageStats>)arrayMap).put(packageName, usageStats);
                }
                else {
                    usageStats2.add(usageStats);
                }
            }
        }
        final ArrayList<UsageStats> list2 = new ArrayList<UsageStats>();
        list2.addAll((Collection<?>)((Map<String, UsageStats>)arrayMap).values());
        Collections.sort((List<Object>)list2, (Comparator<? super Object>)this);
        int n = 0;
        for (final UsageStats usageStats3 : list2) {
            if (this.mApplicationsState.getEntry(usageStats3.getPackageName(), this.mUserId) == null) {
                continue;
            }
            list.add(usageStats3);
            if (++n >= 5) {
                break;
            }
        }
        return list;
    }
    
    private boolean shouldIncludePkgInRecents(final UsageStats usageStats) {
        final String packageName = usageStats.getPackageName();
        if (usageStats.getLastTimeUsed() < this.mCal.getTimeInMillis()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid timestamp, skipping ");
            sb.append(packageName);
            Log.d("RecentAppsCtrl", sb.toString());
            return false;
        }
        if (RecentAppsPreferenceController.SKIP_SYSTEM_PACKAGES.contains(packageName)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("System package, skipping ");
            sb2.append(packageName);
            Log.d("RecentAppsCtrl", sb2.toString());
            return false;
        }
        if (this.mPm.resolveActivity(new Intent().addCategory("android.intent.category.LAUNCHER").setPackage(packageName), 0) == null) {
            final ApplicationsState.AppEntry entry = this.mApplicationsState.getEntry(packageName, this.mUserId);
            if (entry == null || entry.info == null || !AppUtils.isInstant(entry.info)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Not a user visible or instant app, skipping ");
                sb3.append(packageName);
                Log.d("RecentAppsCtrl", sb3.toString());
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final int compare(final UsageStats usageStats, final UsageStats usageStats2) {
        return Long.compare(usageStats2.getLastTimeUsed(), usageStats.getLastTimeUsed());
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mCategory = (PreferenceCategory)preferenceScreen.findPreference(this.getPreferenceKey());
        this.mSeeAllPref = preferenceScreen.findPreference("all_app_info");
        this.mDivider = preferenceScreen.findPreference("all_app_info_divider");
        super.displayPreference(preferenceScreen);
        this.refreshUi(this.mCategory.getContext());
    }
    
    @Override
    public String getPreferenceKey() {
        return "recent_apps_category";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    void refreshUi(final Context context) {
        this.reloadData();
        final List<UsageStats> displayableRecentAppList = this.getDisplayableRecentAppList();
        if (displayableRecentAppList != null && !displayableRecentAppList.isEmpty()) {
            this.mHasRecentApps = true;
            this.displayRecentApps(context, displayableRecentAppList);
        }
        else {
            this.mHasRecentApps = false;
            this.displayOnlyAppInfo();
        }
    }
    
    void reloadData() {
        (this.mCal = Calendar.getInstance()).add(6, -1);
        this.mStats = (List<UsageStats>)this.mUsageStatsManager.queryUsageStats(4, this.mCal.getTimeInMillis(), System.currentTimeMillis());
    }
    
    @Override
    public void updateNonIndexableKeys(final List<String> list) {
        super.updateNonIndexableKeys(list);
        list.add("recent_apps_category");
        list.add("all_app_info_divider");
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        this.refreshUi(this.mCategory.getContext());
        new InstalledAppCounter(this.mContext, -1, new PackageManagerWrapper(this.mContext.getPackageManager())) {
            @Override
            protected void onCountComplete(final int n) {
                if (RecentAppsPreferenceController.this.mHasRecentApps) {
                    RecentAppsPreferenceController.this.mSeeAllPref.setTitle(RecentAppsPreferenceController.this.mContext.getString(2131889020, new Object[] { n }));
                }
                else {
                    RecentAppsPreferenceController.this.mSeeAllPref.setSummary(RecentAppsPreferenceController.this.mContext.getString(2131886413, new Object[] { n }));
                }
            }
        }.execute((Object[])new Void[0]);
    }
}
