package com.android.settings.applications;

import android.content.ComponentName;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import com.android.settings.utils.ManagedServiceSettings;

public class VrListenerSettings extends ManagedServiceSettings
{
    private static final Config CONFIG;
    private static final String TAG;
    
    static {
        TAG = VrListenerSettings.class.getSimpleName();
        CONFIG = new Config.Builder().setTag(VrListenerSettings.TAG).setSetting("enabled_vr_listeners").setIntentAction("android.service.vr.VrListenerService").setPermission("android.permission.BIND_VR_LISTENER_SERVICE").setNoun("vr listener").setWarningDialogTitle(2131889866).setWarningDialogSummary(2131889865).setEmptyText(2131888424).build();
    }
    
    @Override
    protected Config getConfig() {
        return VrListenerSettings.CONFIG;
    }
    
    @Override
    public int getMetricsCategory() {
        return 334;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082861;
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 772;
        }
        else {
            n = 773;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    protected boolean setEnabled(final ComponentName componentName, final String s, final boolean b) {
        this.logSpecialPermissionChange(b, componentName.getPackageName());
        return super.setEnabled(componentName, s, b);
    }
}
