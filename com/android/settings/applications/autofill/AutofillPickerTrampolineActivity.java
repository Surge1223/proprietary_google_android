package com.android.settings.applications.autofill;

import android.content.Intent;
import android.view.autofill.AutofillManager;
import android.content.Context;
import com.android.settings.applications.defaultapps.DefaultAutofillPicker;
import android.os.Bundle;
import android.app.Activity;

public class AutofillPickerTrampolineActivity extends Activity
{
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent intent = this.getIntent();
        final String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
        final String defaultKey = DefaultAutofillPicker.getDefaultKey((Context)this);
        if (defaultKey != null && defaultKey.startsWith(schemeSpecificPart)) {
            this.setResult(-1);
            this.finish();
            return;
        }
        final AutofillManager autofillManager = (AutofillManager)this.getSystemService((Class)AutofillManager.class);
        if (autofillManager != null && autofillManager.hasAutofillFeature() && autofillManager.isAutofillSupported()) {
            this.startActivity(new Intent((Context)this, (Class)AutofillPickerActivity.class).setFlags(33554432).setData(intent.getData()));
            this.finish();
            return;
        }
        this.setResult(0);
        this.finish();
    }
}
