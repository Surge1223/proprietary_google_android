package com.android.settings.applications.autofill;

import android.content.Intent;
import android.os.Bundle;
import com.android.settings.applications.defaultapps.DefaultAutofillPicker;
import com.android.settings.SettingsActivity;

public class AutofillPickerActivity extends SettingsActivity
{
    @Override
    protected boolean isValidFragment(final String s) {
        return super.isValidFragment(s) || DefaultAutofillPicker.class.getName().equals(s);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        final Intent intent = this.getIntent();
        final String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
        intent.putExtra(":settings:show_fragment", DefaultAutofillPicker.class.getName());
        intent.putExtra(":settings:show_fragment_title_resid", 2131886502);
        intent.putExtra("package_name", schemeSpecificPart);
        super.onCreate(bundle);
    }
}
