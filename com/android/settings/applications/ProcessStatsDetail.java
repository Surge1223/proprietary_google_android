package com.android.settings.applications;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.util.IconDrawableFactory;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.view.View;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import android.app.ActivityManager$RunningServiceInfo;
import android.util.Log;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.ActivityManager;
import android.support.v7.preference.Preference;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import android.content.Intent;
import android.text.format.Formatter$BytesResult;
import android.app.Activity;
import android.content.Context;
import android.text.format.Formatter;
import com.android.settings.SummaryPreference;
import android.content.pm.PackageManager;
import com.android.settings.CancellablePreference;
import android.content.ComponentName;
import android.util.ArrayMap;
import android.support.v7.preference.PreferenceCategory;
import android.content.pm.PackageManager;
import android.view.MenuItem;
import android.app.admin.DevicePolicyManager;
import java.util.Comparator;
import com.android.settings.SettingsPreferenceFragment;

public class ProcessStatsDetail extends SettingsPreferenceFragment
{
    static final Comparator<ProcStatsEntry> sEntryCompare;
    static final Comparator<ProcStatsEntry.Service> sServiceCompare;
    static final Comparator<PkgService> sServicePkgCompare;
    private ProcStatsPackageEntry mApp;
    private DevicePolicyManager mDpm;
    private MenuItem mForceStop;
    private double mMaxMemoryUsage;
    private long mOnePercentTime;
    private PackageManager mPm;
    private PreferenceCategory mProcGroup;
    private final ArrayMap<ComponentName, CancellablePreference> mServiceMap;
    private double mTotalScale;
    private long mTotalTime;
    private double mWeightToRam;
    
    static {
        sEntryCompare = new Comparator<ProcStatsEntry>() {
            @Override
            public int compare(final ProcStatsEntry procStatsEntry, final ProcStatsEntry procStatsEntry2) {
                if (procStatsEntry.mRunWeight < procStatsEntry2.mRunWeight) {
                    return 1;
                }
                if (procStatsEntry.mRunWeight > procStatsEntry2.mRunWeight) {
                    return -1;
                }
                return 0;
            }
        };
        sServiceCompare = new Comparator<ProcStatsEntry.Service>() {
            @Override
            public int compare(final ProcStatsEntry.Service service, final ProcStatsEntry.Service service2) {
                if (service.mDuration < service2.mDuration) {
                    return 1;
                }
                if (service.mDuration > service2.mDuration) {
                    return -1;
                }
                return 0;
            }
        };
        sServicePkgCompare = new Comparator<PkgService>() {
            @Override
            public int compare(final PkgService pkgService, final PkgService pkgService2) {
                if (pkgService.mDuration < pkgService2.mDuration) {
                    return 1;
                }
                if (pkgService.mDuration > pkgService2.mDuration) {
                    return -1;
                }
                return 0;
            }
        };
    }
    
    public ProcessStatsDetail() {
        this.mServiceMap = (ArrayMap<ComponentName, CancellablePreference>)new ArrayMap();
    }
    
    private static String capitalize(final String s) {
        final char char1 = s.charAt(0);
        if (!Character.isLowerCase(char1)) {
            return s;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(Character.toUpperCase(char1));
        sb.append(s.substring(1));
        return sb.toString();
    }
    
    private void checkForceStop() {
        if (this.mForceStop == null) {
            return;
        }
        if (this.mApp.mEntries.get(0).mUid < 10000) {
            this.mForceStop.setVisible(false);
            return;
        }
        boolean b = false;
        for (int i = 0; i < this.mApp.mEntries.size(); ++i) {
            final ProcStatsEntry procStatsEntry = this.mApp.mEntries.get(i);
            for (int j = 0; j < procStatsEntry.mPackages.size(); ++j) {
                final String s = procStatsEntry.mPackages.get(j);
                if (this.mDpm.packageHasActiveAdmins(s)) {
                    this.mForceStop.setEnabled(false);
                    return;
                }
                try {
                    if ((this.mPm.getApplicationInfo(s, 0).flags & 0x200000) == 0x0) {
                        b = true;
                    }
                }
                catch (PackageManager$NameNotFoundException ex) {}
            }
        }
        if (b) {
            this.mForceStop.setVisible(true);
        }
    }
    
    private void createDetails() {
        this.addPreferencesFromResource(2132082711);
        this.mProcGroup = (PreferenceCategory)this.findPreference("processes");
        this.fillProcessesSection();
        final SummaryPreference summaryPreference = (SummaryPreference)this.findPreference("status_header");
        double n;
        if (this.mApp.mRunWeight > this.mApp.mBgWeight) {
            n = this.mApp.mRunWeight;
        }
        else {
            n = this.mApp.mBgWeight;
        }
        final double n2 = n * this.mWeightToRam;
        final float n3 = (float)(n2 / this.mMaxMemoryUsage);
        final Activity activity = this.getActivity();
        summaryPreference.setRatios(n3, 0.0f, 1.0f - n3);
        final Formatter$BytesResult formatBytes = Formatter.formatBytes(((Context)activity).getResources(), (long)n2, 1);
        summaryPreference.setAmount(formatBytes.value);
        summaryPreference.setUnits(formatBytes.units);
        this.findPreference("frequency").setSummary(ProcStatsPackageEntry.getFrequency(Math.max(this.mApp.mRunDuration, this.mApp.mBgDuration) / this.mTotalTime, (Context)this.getActivity()));
        this.findPreference("max_usage").setSummary(Formatter.formatShortFileSize(this.getContext(), (long)(Math.max(this.mApp.mMaxBgMem, this.mApp.mMaxRunMem) * this.mTotalScale * 1024.0)));
    }
    
    private void doStopService(final String s, final String s2) {
        this.getActivity().stopService(new Intent().setClassName(s, s2));
        this.updateRunningServices();
    }
    
    private void fillProcessesSection() {
        this.mProcGroup.removeAll();
        final ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0; i < this.mApp.mEntries.size(); ++i) {
            final ProcStatsEntry procStatsEntry = this.mApp.mEntries.get(i);
            if (procStatsEntry.mPackage.equals("os")) {
                procStatsEntry.mLabel = procStatsEntry.mName;
            }
            else {
                procStatsEntry.mLabel = getProcessName(this.mApp.mUiLabel, procStatsEntry);
            }
            list.add(procStatsEntry);
        }
        Collections.sort(list, (Comparator<? super Object>)ProcessStatsDetail.sEntryCompare);
        for (int j = 0; j < list.size(); ++j) {
            final ProcStatsEntry procStatsEntry2 = list.get(j);
            final Preference preference = new Preference(this.getPrefContext());
            preference.setTitle(procStatsEntry2.mLabel);
            preference.setSelectable(false);
            preference.setSummary(this.getString(2131888287, new Object[] { Formatter.formatShortFileSize((Context)this.getActivity(), Math.max((long)(procStatsEntry2.mRunWeight * this.mWeightToRam), (long)(procStatsEntry2.mBgWeight * this.mWeightToRam))), ProcStatsPackageEntry.getFrequency(Math.max(procStatsEntry2.mRunDuration, procStatsEntry2.mBgDuration) / this.mTotalTime, (Context)this.getActivity()) }));
            this.mProcGroup.addPreference(preference);
        }
        if (this.mProcGroup.getPreferenceCount() < 2) {
            this.getPreferenceScreen().removePreference(this.mProcGroup);
        }
    }
    
    private static String getProcessName(final String s, final ProcStatsEntry procStatsEntry) {
        final String mName = procStatsEntry.mName;
        if (mName.contains(":")) {
            return capitalize(mName.substring(mName.lastIndexOf(58) + 1));
        }
        if (!mName.startsWith(procStatsEntry.mPackage)) {
            return mName;
        }
        if (mName.length() == procStatsEntry.mPackage.length()) {
            return s;
        }
        int length;
        final int n = length = procStatsEntry.mPackage.length();
        if (mName.charAt(n) == '.') {
            length = n + 1;
        }
        return capitalize(mName.substring(length));
    }
    
    private void killProcesses() {
        final ActivityManager activityManager = (ActivityManager)this.getActivity().getSystemService("activity");
        for (int i = 0; i < this.mApp.mEntries.size(); ++i) {
            final ProcStatsEntry procStatsEntry = this.mApp.mEntries.get(i);
            for (int j = 0; j < procStatsEntry.mPackages.size(); ++j) {
                activityManager.forceStopPackage((String)procStatsEntry.mPackages.get(j));
            }
        }
    }
    
    private void showStopServiceDialog(final String s, final String s2) {
        new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888883).setMessage(2131888882).setPositiveButton(2131887460, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                ProcessStatsDetail.this.doStopService(s, s2);
            }
        }).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).show();
    }
    
    private void stopService(final String s, final String s2) {
        try {
            if ((this.getActivity().getPackageManager().getApplicationInfo(s, 0).flags & 0x1) != 0x0) {
                this.showStopServiceDialog(s, s2);
                return;
            }
            this.doStopService(s, s2);
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Can't find app ");
            sb.append(s);
            Log.e("ProcessStatsDetail", sb.toString(), (Throwable)ex);
        }
    }
    
    private void updateRunningServices() {
        final List runningServices = ((ActivityManager)this.getActivity().getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
        final int size = this.mServiceMap.size();
        final int n = 0;
        for (int i = 0; i < size; ++i) {
            ((CancellablePreference)this.mServiceMap.valueAt(i)).setCancellable(false);
        }
        for (int size2 = runningServices.size(), j = n; j < size2; ++j) {
            final ActivityManager$RunningServiceInfo activityManager$RunningServiceInfo = runningServices.get(j);
            if (activityManager$RunningServiceInfo.started || activityManager$RunningServiceInfo.clientLabel != 0) {
                if ((activityManager$RunningServiceInfo.flags & 0x8) == 0x0) {
                    final ComponentName service = activityManager$RunningServiceInfo.service;
                    final CancellablePreference cancellablePreference = (CancellablePreference)this.mServiceMap.get((Object)service);
                    if (cancellablePreference != null) {
                        cancellablePreference.setOnCancelListener((CancellablePreference.OnCancelListener)new CancellablePreference.OnCancelListener() {
                            @Override
                            public void onCancel(final CancellablePreference cancellablePreference) {
                                ProcessStatsDetail.this.stopService(service.getPackageName(), service.getClassName());
                            }
                        });
                        cancellablePreference.setCancellable(true);
                    }
                }
            }
        }
    }
    
    @Override
    public int getMetricsCategory() {
        return 21;
    }
    
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        this.mPm = this.getActivity().getPackageManager();
        this.mDpm = (DevicePolicyManager)this.getActivity().getSystemService("device_policy");
        arguments = this.getArguments();
        (this.mApp = (ProcStatsPackageEntry)arguments.getParcelable("package_entry")).retrieveUiData((Context)this.getActivity(), this.mPm);
        this.mWeightToRam = arguments.getDouble("weight_to_ram");
        this.mTotalTime = arguments.getLong("total_time");
        this.mMaxMemoryUsage = arguments.getDouble("max_memory_usage");
        this.mTotalScale = arguments.getDouble("total_scale");
        this.mOnePercentTime = this.mTotalTime / 100L;
        this.mServiceMap.clear();
        this.createDetails();
        this.setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        this.mForceStop = menu.add(0, 1, 0, 2131887694);
        this.checkForceStop();
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != 1) {
            return false;
        }
        this.killProcesses();
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.checkForceStop();
        this.updateRunningServices();
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (this.mApp.mUiTargetApp == null) {
            this.finish();
            return;
        }
        final Activity activity = this.getActivity();
        final EntityHeaderController setRecyclerView = EntityHeaderController.newInstance(activity, this, null).setRecyclerView(this.getListView(), this.getLifecycle());
        Object badgedIcon;
        if (this.mApp.mUiTargetApp != null) {
            badgedIcon = IconDrawableFactory.newInstance((Context)activity).getBadgedIcon(this.mApp.mUiTargetApp);
        }
        else {
            badgedIcon = new ColorDrawable(0);
        }
        final EntityHeaderController setPackageName = setRecyclerView.setIcon((Drawable)badgedIcon).setLabel(this.mApp.mUiLabel).setPackageName(this.mApp.mPackage);
        int uid;
        if (this.mApp.mUiTargetApp != null) {
            uid = this.mApp.mUiTargetApp.uid;
        }
        else {
            uid = -10000;
        }
        this.getPreferenceScreen().addPreference(setPackageName.setUid(uid).setHasAppInfoLink(true).setButtonActions(0, 0).done(activity, this.getPrefContext()));
    }
    
    static class PkgService
    {
        long mDuration;
        final ArrayList<ProcStatsEntry.Service> mServices;
        
        PkgService() {
            this.mServices = new ArrayList<ProcStatsEntry.Service>();
        }
    }
}
