package com.android.settings.applications;

import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class HighPowerAppsController extends BasePreferenceController
{
    static final String KEY_HIGH_POWER_APPS = "high_power_apps";
    
    public HighPowerAppsController(final Context context) {
        super(context, "high_power_apps");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034145)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
}
