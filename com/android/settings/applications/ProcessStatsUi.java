package com.android.settings.applications;

import android.app.Activity;
import android.content.Context;
import java.util.List;
import java.util.Collections;
import com.android.settings.SettingsActivity;
import android.support.v7.preference.Preference;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.view.MenuItem;
import android.support.v7.preference.PreferenceGroup;
import java.util.Comparator;

public class ProcessStatsUi extends ProcessStatsBase
{
    public static final int[] BACKGROUND_AND_SYSTEM_PROC_STATES;
    public static final int[] CACHED_PROC_STATES;
    public static final int[] FOREGROUND_PROC_STATES;
    static final Comparator<ProcStatsPackageEntry> sMaxPackageEntryCompare;
    static final Comparator<ProcStatsPackageEntry> sPackageEntryCompare;
    private PreferenceGroup mAppListGroup;
    private MenuItem mMenuAvg;
    private MenuItem mMenuMax;
    private PackageManager mPm;
    private boolean mShowMax;
    
    static {
        BACKGROUND_AND_SYSTEM_PROC_STATES = new int[] { 0, 2, 3, 4, 8, 5, 6, 7, 9 };
        FOREGROUND_PROC_STATES = new int[] { 1 };
        CACHED_PROC_STATES = new int[] { 11, 12, 13 };
        sPackageEntryCompare = new Comparator<ProcStatsPackageEntry>() {
            @Override
            public int compare(final ProcStatsPackageEntry procStatsPackageEntry, final ProcStatsPackageEntry procStatsPackageEntry2) {
                final double max = Math.max(procStatsPackageEntry2.mRunWeight, procStatsPackageEntry2.mBgWeight);
                final double max2 = Math.max(procStatsPackageEntry.mRunWeight, procStatsPackageEntry.mBgWeight);
                if (max2 == max) {
                    return 0;
                }
                int n;
                if (max2 < max) {
                    n = 1;
                }
                else {
                    n = -1;
                }
                return n;
            }
        };
        sMaxPackageEntryCompare = new Comparator<ProcStatsPackageEntry>() {
            @Override
            public int compare(final ProcStatsPackageEntry procStatsPackageEntry, final ProcStatsPackageEntry procStatsPackageEntry2) {
                final double n = Math.max(procStatsPackageEntry2.mMaxBgMem, procStatsPackageEntry2.mMaxRunMem);
                final double n2 = Math.max(procStatsPackageEntry.mMaxBgMem, procStatsPackageEntry.mMaxRunMem);
                if (n2 == n) {
                    return 0;
                }
                int n3;
                if (n2 < n) {
                    n3 = 1;
                }
                else {
                    n3 = -1;
                }
                return n3;
            }
        };
    }
    
    private void updateMenu() {
        this.mMenuMax.setVisible(this.mShowMax ^ true);
        this.mMenuAvg.setVisible(this.mShowMax);
    }
    
    public int getHelpResource() {
        return 2131887783;
    }
    
    public int getMetricsCategory() {
        return 23;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mPm = this.getActivity().getPackageManager();
        this.addPreferencesFromResource(2132082818);
        this.mAppListGroup = (PreferenceGroup)this.findPreference("app_list");
        this.setHasOptionsMenu(true);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        this.mMenuAvg = menu.add(0, 1, 0, 2131889187);
        this.mMenuMax = menu.add(0, 2, 0, 2131889188);
        this.updateMenu();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 1:
            case 2: {
                this.mShowMax ^= true;
                this.refreshUi();
                this.updateMenu();
                return true;
            }
        }
    }
    
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (!(preference instanceof ProcessStatsPreference)) {
            return false;
        }
        ProcessStatsBase.launchMemoryDetail((SettingsActivity)this.getActivity(), this.mStatsManager.getMemInfo(), ((ProcessStatsPreference)preference).getEntry(), true);
        return super.onPreferenceTreeClick(preference);
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
    
    @Override
    public void refreshUi() {
        this.mAppListGroup.removeAll();
        final PreferenceGroup mAppListGroup = this.mAppListGroup;
        final int n = 0;
        mAppListGroup.setOrderingAsAdded(false);
        final PreferenceGroup mAppListGroup2 = this.mAppListGroup;
        int title;
        if (this.mShowMax) {
            title = 2131888248;
        }
        else {
            title = 2131886518;
        }
        mAppListGroup2.setTitle(title);
        final Activity activity = this.getActivity();
        final ProcStatsData.MemInfo memInfo = this.mStatsManager.getMemInfo();
        final List<ProcStatsPackageEntry> entries = this.mStatsManager.getEntries();
        for (int i = 0; i < entries.size(); ++i) {
            entries.get(i).updateMetrics();
        }
        Comparator<ProcStatsPackageEntry> comparator;
        if (this.mShowMax) {
            comparator = ProcessStatsUi.sMaxPackageEntryCompare;
        }
        else {
            comparator = ProcessStatsUi.sPackageEntryCompare;
        }
        Collections.sort((List<Object>)entries, (Comparator<? super Object>)comparator);
        double realTotalRam;
        if (this.mShowMax) {
            realTotalRam = memInfo.realTotalRam;
        }
        else {
            realTotalRam = memInfo.usedWeight * memInfo.weightToRam;
        }
        for (int j = n; j < entries.size(); ++j) {
            final ProcStatsPackageEntry procStatsPackageEntry = entries.get(j);
            final ProcessStatsPreference processStatsPreference = new ProcessStatsPreference(this.getPrefContext());
            procStatsPackageEntry.retrieveUiData((Context)activity, this.mPm);
            processStatsPreference.init(procStatsPackageEntry, this.mPm, realTotalRam, memInfo.weightToRam, memInfo.totalScale, this.mShowMax ^ true);
            processStatsPreference.setOrder(j);
            this.mAppListGroup.addPreference(processStatsPreference);
        }
    }
}
