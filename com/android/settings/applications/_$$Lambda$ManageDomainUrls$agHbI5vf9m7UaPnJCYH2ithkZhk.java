package com.android.settings.applications;

import android.support.v7.preference.PreferenceViewHolder;
import android.util.ArraySet;
import com.android.settings.Utils;
import android.os.UserHandle;
import android.content.Context;
import android.content.pm.PackageManager;
import com.android.settings.widget.AppPreference;
import android.view.View;
import android.content.ComponentName;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceCategory;
import android.provider.Settings;
import android.app.Fragment;
import android.provider.Settings;
import android.arch.lifecycle.Lifecycle;
import android.app.Application;
import android.os.Bundle;
import java.util.ArrayList;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceGroup;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.SettingsPreferenceFragment;
import android.content.Intent;
import android.support.v7.preference.Preference;

public final class _$$Lambda$ManageDomainUrls$agHbI5vf9m7UaPnJCYH2ithkZhk implements OnPreferenceClickListener
{
    @Override
    public final boolean onPreferenceClick(final Preference preference) {
        return ManageDomainUrls.lambda$onRebuildComplete$0(this.f$0, this.f$1, preference);
    }
}
