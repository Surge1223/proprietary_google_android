package com.android.settings.applications;

import android.text.format.Formatter;
import android.util.Log;
import android.text.TextUtils;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settings.widget.AppPreference;

public class ProcessStatsPreference extends AppPreference
{
    private ProcStatsPackageEntry mEntry;
    
    public ProcessStatsPreference(final Context context) {
        super(context, null);
    }
    
    public ProcStatsPackageEntry getEntry() {
        return this.mEntry;
    }
    
    public void init(final ProcStatsPackageEntry mEntry, final PackageManager packageManager, final double n, double n2, double n3, final boolean b) {
        this.mEntry = mEntry;
        String title;
        if (TextUtils.isEmpty((CharSequence)mEntry.mUiLabel)) {
            title = mEntry.mPackage;
        }
        else {
            title = mEntry.mUiLabel;
        }
        this.setTitle(title);
        if (TextUtils.isEmpty((CharSequence)title)) {
            Log.d("ProcessStatsPreference", "PackageEntry contained no package name or uiLabel");
        }
        if (mEntry.mUiTargetApp != null) {
            this.setIcon(mEntry.mUiTargetApp.loadIcon(packageManager));
        }
        else {
            this.setIcon(packageManager.getDefaultActivityIcon());
        }
        final boolean b2 = mEntry.mRunWeight > mEntry.mBgWeight;
        if (b) {
            if (b2) {
                n3 = mEntry.mRunWeight;
            }
            else {
                n3 = mEntry.mBgWeight;
            }
            n2 *= n3;
        }
        else {
            long n4;
            if (b2) {
                n4 = mEntry.mMaxRunMem;
            }
            else {
                n4 = mEntry.mMaxBgMem;
            }
            n2 = n4 * n3 * 1024.0;
        }
        this.setSummary(Formatter.formatShortFileSize(this.getContext(), (long)n2));
        this.setProgress((int)(100.0 * n2 / n));
    }
}
