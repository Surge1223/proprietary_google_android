package com.android.settings.applications;

import java.util.ArrayList;
import android.os.RemoteException;
import com.android.internal.telephony.ISms$Stub;
import android.os.ServiceManager;
import com.android.internal.telephony.ISms;
import android.content.Context;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateSmsPremBridge extends AppStateBaseBridge
{
    public static final AppFilter FILTER_APP_PREMIUM_SMS;
    private final Context mContext;
    private final ISms mSmsManager;
    
    static {
        FILTER_APP_PREMIUM_SMS = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.extraInfo instanceof SmsState && ((SmsState)appEntry.extraInfo).smsState != 0;
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    public AppStateSmsPremBridge(final Context mContext, final ApplicationsState applicationsState, final Callback callback) {
        super(applicationsState, callback);
        this.mContext = mContext;
        this.mSmsManager = ISms$Stub.asInterface(ServiceManager.getService("isms"));
    }
    
    private int getSmsState(final String s) {
        try {
            return this.mSmsManager.getPremiumSmsPermission(s);
        }
        catch (RemoteException ex) {
            return 0;
        }
    }
    
    public SmsState getState(final String s) {
        final SmsState smsState = new SmsState();
        smsState.smsState = this.getSmsState(s);
        return smsState;
    }
    
    @Override
    protected void loadAllExtraInfo() {
        final ArrayList<AppEntry> allApps = this.mAppSession.getAllApps();
        for (int size = allApps.size(), i = 0; i < size; ++i) {
            final AppEntry appEntry = allApps.get(i);
            this.updateExtraInfo(appEntry, appEntry.info.packageName, appEntry.info.uid);
        }
    }
    
    public void setSmsState(final String s, final int n) {
        try {
            this.mSmsManager.setPremiumSmsPermission(s, n);
        }
        catch (RemoteException ex) {}
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = this.getState(s);
    }
    
    public static class SmsState
    {
        public int smsState;
    }
}
