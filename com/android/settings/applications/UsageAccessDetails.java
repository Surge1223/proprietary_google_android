package com.android.settings.applications;

import android.content.pm.ResolveInfo;
import android.content.ComponentName;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.android.internal.annotations.VisibleForTesting;
import android.util.Pair;
import com.android.settings.overlay.FeatureFactory;
import android.app.AlertDialog;
import android.support.v14.preference.SwitchPreference;
import android.content.Intent;
import android.app.admin.DevicePolicyManager;
import android.app.AppOpsManager;
import android.support.v7.preference.Preference;

public class UsageAccessDetails extends AppInfoWithHeader implements OnPreferenceChangeListener, OnPreferenceClickListener
{
    private AppOpsManager mAppOpsManager;
    private DevicePolicyManager mDpm;
    private Intent mSettingsIntent;
    private SwitchPreference mSwitchPref;
    private AppStateUsageBridge mUsageBridge;
    private Preference mUsageDesc;
    private AppStateUsageBridge.UsageState mUsageState;
    
    private void setHasAccess(final boolean b) {
        this.logSpecialPermissionChange(b, this.mPackageName);
        this.mAppOpsManager.setMode(43, this.mPackageInfo.applicationInfo.uid, this.mPackageName, (int)((b ^ true) ? 1 : 0));
    }
    
    @Override
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 183;
    }
    
    @VisibleForTesting
    void logSpecialPermissionChange(final boolean b, final String s) {
        int n;
        if (b) {
            n = 783;
        }
        else {
            n = 784;
        }
        FeatureFactory.getFactory(this.getContext()).getMetricsFeatureProvider().action(this.getContext(), n, s, (Pair<Integer, Object>[])new Pair[0]);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        this.mUsageBridge = new AppStateUsageBridge((Context)activity, this.mState, null);
        this.mAppOpsManager = (AppOpsManager)((Context)activity).getSystemService("appops");
        this.mDpm = (DevicePolicyManager)((Context)activity).getSystemService((Class)DevicePolicyManager.class);
        this.addPreferencesFromResource(2132082713);
        this.mSwitchPref = (SwitchPreference)this.findPreference("app_ops_settings_switch");
        this.mUsageDesc = this.findPreference("app_ops_settings_description");
        this.getPreferenceScreen().setTitle(2131889602);
        this.mSwitchPref.setTitle(2131888558);
        this.mUsageDesc.setSummary(2131889603);
        this.mSwitchPref.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        this.mSettingsIntent = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.USAGE_ACCESS_CONFIG").setPackage(this.mPackageName);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mSwitchPref) {
            if (this.mUsageState != null && (boolean)o != ((AppStateAppOpsBridge.PermissionState)this.mUsageState).isPermissible()) {
                if (((AppStateAppOpsBridge.PermissionState)this.mUsageState).isPermissible() && this.mDpm.isProfileOwnerApp(this.mPackageName)) {
                    new AlertDialog$Builder(this.getContext()).setIcon(17302338).setTitle(17039380).setMessage(2131890214).setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)null).show();
                }
                this.setHasAccess(((AppStateAppOpsBridge.PermissionState)this.mUsageState).isPermissible() ^ true);
                this.refreshUi();
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(final Preference preference) {
        return false;
    }
    
    @Override
    protected boolean refreshUi() {
        this.retrieveAppEntry();
        if (this.mAppEntry == null) {
            return false;
        }
        if (this.mPackageInfo == null) {
            return false;
        }
        this.mUsageState = this.mUsageBridge.getUsageInfo(this.mPackageName, this.mPackageInfo.applicationInfo.uid);
        this.mSwitchPref.setChecked(((AppStateAppOpsBridge.PermissionState)this.mUsageState).isPermissible());
        this.mSwitchPref.setEnabled(this.mUsageState.permissionDeclared);
        final ResolveInfo resolveActivityAsUser = this.mPm.resolveActivityAsUser(this.mSettingsIntent, 128, this.mUserId);
        if (resolveActivityAsUser != null) {
            final Bundle metaData = resolveActivityAsUser.activityInfo.metaData;
            this.mSettingsIntent.setComponent(new ComponentName(resolveActivityAsUser.activityInfo.packageName, resolveActivityAsUser.activityInfo.name));
            if (metaData != null && metaData.containsKey("android.settings.metadata.USAGE_ACCESS_REASON")) {
                this.mSwitchPref.setSummary(metaData.getString("android.settings.metadata.USAGE_ACCESS_REASON"));
            }
        }
        return true;
    }
}
