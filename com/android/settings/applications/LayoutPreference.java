package com.android.settings.applications;

import android.widget.FrameLayout;
import android.support.v7.preference.PreferenceViewHolder;
import com.android.settings.Utils;
import android.content.res.TypedArray;
import com.android.internal.R$styleable;
import android.support.v4.content.res.TypedArrayUtils;
import com.android.settings.R;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;

public class LayoutPreference extends Preference
{
    private boolean mAllowDividerAbove;
    private boolean mAllowDividerBelow;
    private final View.OnClickListener mClickListener;
    View mRootView;
    
    public LayoutPreference(final Context context, final int n) {
        this(context, LayoutInflater.from(context).inflate(n, (ViewGroup)null, false));
    }
    
    public LayoutPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mClickListener = (View.OnClickListener)new _$$Lambda$LayoutPreference$_oL9WiG_H2u60jStTsEh5xi7a7Q(this);
        this.init(context, set, 0);
    }
    
    public LayoutPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mClickListener = (View.OnClickListener)new _$$Lambda$LayoutPreference$_oL9WiG_H2u60jStTsEh5xi7a7Q(this);
        this.init(context, set, n);
    }
    
    public LayoutPreference(final Context context, final View view) {
        super(context);
        this.mClickListener = (View.OnClickListener)new _$$Lambda$LayoutPreference$_oL9WiG_H2u60jStTsEh5xi7a7Q(this);
        this.setView(view);
    }
    
    private void init(final Context context, final AttributeSet set, int resourceId) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Preference);
        this.mAllowDividerAbove = TypedArrayUtils.getBoolean(obtainStyledAttributes, 16, 16, false);
        this.mAllowDividerBelow = TypedArrayUtils.getBoolean(obtainStyledAttributes, 17, 17, false);
        obtainStyledAttributes.recycle();
        final TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(set, R$styleable.Preference, resourceId, 0);
        resourceId = obtainStyledAttributes2.getResourceId(3, 0);
        if (resourceId != 0) {
            obtainStyledAttributes2.recycle();
            this.setView(LayoutInflater.from(this.getContext()).inflate(resourceId, (ViewGroup)null, false));
            return;
        }
        throw new IllegalArgumentException("LayoutPreference requires a layout to be defined");
    }
    
    private void setView(final View mRootView) {
        this.setLayoutResource(2131558599);
        final ViewGroup viewGroup = (ViewGroup)mRootView.findViewById(2131361860);
        if (viewGroup != null) {
            Utils.forceCustomPadding((View)viewGroup, true);
        }
        this.mRootView = mRootView;
        this.setShouldDisableView(false);
    }
    
    public <T extends View> T findViewById(final int n) {
        return (T)this.mRootView.findViewById(n);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        preferenceViewHolder.itemView.setOnClickListener(this.mClickListener);
        final boolean selectable = this.isSelectable();
        preferenceViewHolder.itemView.setFocusable(selectable);
        preferenceViewHolder.itemView.setClickable(selectable);
        preferenceViewHolder.setDividerAllowedAbove(this.mAllowDividerAbove);
        preferenceViewHolder.setDividerAllowedBelow(this.mAllowDividerBelow);
        final FrameLayout frameLayout = (FrameLayout)preferenceViewHolder.itemView;
        frameLayout.removeAllViews();
        final ViewGroup viewGroup = (ViewGroup)this.mRootView.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this.mRootView);
        }
        frameLayout.addView(this.mRootView);
    }
}
