package com.android.settings.applications;

import java.util.ArrayList;
import java.util.List;
import android.support.v7.preference.PreferenceScreen;
import java.util.HashSet;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume$ScopedAccessProviderContract;
import android.net.Uri.Builder;
import java.util.HashMap;
import android.app.Activity;
import com.android.settingslib.applications.AppUtils;
import android.util.IconDrawableFactory;
import android.view.View;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.ContentValues;
import java.util.Iterator;
import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import android.support.v7.preference.Preference;
import android.util.Pair;
import java.util.Set;
import android.support.v7.preference.PreferenceCategory;
import android.net.Uri;
import android.content.Context;
import java.util.function.Consumer;

public final class _$$Lambda$DirectoryAccessDetails$K0N0BhiTAIxLxuaXU9qwR_rLnAY implements Consumer
{
    @Override
    public final void accept(final Object o) {
        DirectoryAccessDetails.lambda$refreshUi$0(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (Pair)o);
    }
}
