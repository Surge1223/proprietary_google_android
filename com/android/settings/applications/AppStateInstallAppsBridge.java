package com.android.settings.applications;

import java.util.ArrayList;
import com.android.internal.util.ArrayUtils;
import android.os.RemoteException;
import android.util.Log;
import android.app.AppGlobals;
import android.content.Context;
import android.content.pm.IPackageManager;
import android.app.AppOpsManager;
import com.android.settingslib.applications.ApplicationsState;

public class AppStateInstallAppsBridge extends AppStateBaseBridge
{
    public static final AppFilter FILTER_APP_SOURCES;
    private static final String TAG;
    private final AppOpsManager mAppOpsManager;
    private final IPackageManager mIpm;
    
    static {
        TAG = AppStateInstallAppsBridge.class.getSimpleName();
        FILTER_APP_SOURCES = new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.extraInfo != null && appEntry.extraInfo instanceof InstallAppsState && ((InstallAppsState)appEntry.extraInfo).isPotentialAppSource();
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    public AppStateInstallAppsBridge(final Context context, final ApplicationsState applicationsState, final Callback callback) {
        super(applicationsState, callback);
        this.mIpm = AppGlobals.getPackageManager();
        this.mAppOpsManager = (AppOpsManager)context.getSystemService("appops");
    }
    
    private int getAppOpMode(final int n, final int n2, final String s) {
        return this.mAppOpsManager.checkOpNoThrow(n, n2, s);
    }
    
    private boolean hasPermission(final String s, int checkUidPermission) {
        boolean b = false;
        try {
            checkUidPermission = this.mIpm.checkUidPermission(s, checkUidPermission);
            if (checkUidPermission == 0) {
                b = true;
            }
            return b;
        }
        catch (RemoteException ex) {
            Log.e(AppStateInstallAppsBridge.TAG, "PackageManager dead. Cannot get permission info");
            return false;
        }
    }
    
    private boolean hasRequestedAppOpPermission(final String s, final String s2) {
        try {
            return ArrayUtils.contains((Object[])this.mIpm.getAppOpPermissionPackages(s), (Object)s2);
        }
        catch (RemoteException ex) {
            Log.e(AppStateInstallAppsBridge.TAG, "PackageManager dead. Cannot get permission info");
            return false;
        }
    }
    
    public InstallAppsState createInstallAppsStateFor(final String s, final int n) {
        final InstallAppsState installAppsState = new InstallAppsState();
        installAppsState.permissionRequested = this.hasRequestedAppOpPermission("android.permission.REQUEST_INSTALL_PACKAGES", s);
        installAppsState.permissionGranted = this.hasPermission("android.permission.REQUEST_INSTALL_PACKAGES", n);
        installAppsState.appOpMode = this.getAppOpMode(66, n, s);
        return installAppsState;
    }
    
    @Override
    protected void loadAllExtraInfo() {
        final ArrayList<AppEntry> allApps = this.mAppSession.getAllApps();
        for (int i = 0; i < allApps.size(); ++i) {
            final AppEntry appEntry = allApps.get(i);
            this.updateExtraInfo(appEntry, appEntry.info.packageName, appEntry.info.uid);
        }
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        appEntry.extraInfo = this.createInstallAppsStateFor(s, n);
    }
    
    public static class InstallAppsState
    {
        int appOpMode;
        boolean permissionGranted;
        boolean permissionRequested;
        
        public InstallAppsState() {
            this.appOpMode = 3;
        }
        
        public boolean canInstallApps() {
            if (this.appOpMode == 3) {
                return this.permissionGranted;
            }
            return this.appOpMode == 0;
        }
        
        public boolean isPotentialAppSource() {
            return this.appOpMode != 3 || this.permissionRequested;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[permissionGranted: ");
            sb.append(this.permissionGranted);
            final StringBuilder sb2 = new StringBuilder(sb.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(", permissionRequested: ");
            sb3.append(this.permissionRequested);
            sb2.append(sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(", appOpMode: ");
            sb4.append(this.appOpMode);
            sb2.append(sb4.toString());
            sb2.append("]");
            return sb2.toString();
        }
    }
}
