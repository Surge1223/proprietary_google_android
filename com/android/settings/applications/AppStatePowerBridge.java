package com.android.settings.applications;

import java.util.ArrayList;
import android.content.Context;
import com.android.settingslib.fuelgauge.PowerWhitelistBackend;
import com.android.settingslib.applications.ApplicationsState;

public class AppStatePowerBridge extends AppStateBaseBridge
{
    public static final AppFilter FILTER_POWER_WHITELISTED;
    private final PowerWhitelistBackend mBackend;
    
    static {
        FILTER_POWER_WHITELISTED = new ApplicationsState.CompoundFilter(ApplicationsState.FILTER_WITHOUT_DISABLED_UNTIL_USED, new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.extraInfo == Boolean.TRUE;
            }
            
            @Override
            public void init() {
            }
        });
    }
    
    public AppStatePowerBridge(final Context context, final ApplicationsState applicationsState, final Callback callback) {
        super(applicationsState, callback);
        this.mBackend = PowerWhitelistBackend.getInstance(context);
    }
    
    @Override
    protected void loadAllExtraInfo() {
        final ArrayList<AppEntry> allApps = this.mAppSession.getAllApps();
        for (int size = allApps.size(), i = 0; i < size; ++i) {
            final AppEntry appEntry = allApps.get(i);
            Boolean extraInfo;
            if (this.mBackend.isWhitelisted(appEntry.info.packageName)) {
                extraInfo = Boolean.TRUE;
            }
            else {
                extraInfo = Boolean.FALSE;
            }
            appEntry.extraInfo = extraInfo;
        }
    }
    
    @Override
    protected void updateExtraInfo(final AppEntry appEntry, final String s, final int n) {
        Boolean extraInfo;
        if (this.mBackend.isWhitelisted(s)) {
            extraInfo = Boolean.TRUE;
        }
        else {
            extraInfo = Boolean.FALSE;
        }
        appEntry.extraInfo = extraInfo;
    }
}
