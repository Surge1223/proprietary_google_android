package com.android.settings.applications;

import android.app.Dialog;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.IntentFilter;
import android.app.FragmentManager;
import com.android.settings.SettingsActivity;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.ArrayList;
import android.app.Activity;
import android.hardware.usb.IUsbManager$Stub;
import android.os.ServiceManager;
import android.arch.lifecycle.Lifecycle;
import com.android.settings.overlay.FeatureFactory;
import android.app.AlertDialog;
import android.os.UserHandle;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.content.Intent;
import android.content.Context;
import android.os.UserManager;
import android.hardware.usb.IUsbManager;
import android.content.pm.PackageManager;
import android.content.BroadcastReceiver;
import android.content.pm.PackageInfo;
import android.app.admin.DevicePolicyManager;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settings.SettingsPreferenceFragment;

public abstract class AppInfoBase extends SettingsPreferenceFragment implements Callbacks
{
    protected static final String TAG;
    protected AppEntry mAppEntry;
    protected ApplicationFeatureProvider mApplicationFeatureProvider;
    protected RestrictedLockUtils.EnforcedAdmin mAppsControlDisallowedAdmin;
    protected boolean mAppsControlDisallowedBySystem;
    protected DevicePolicyManager mDpm;
    protected boolean mFinishing;
    protected boolean mListeningToPackageRemove;
    protected PackageInfo mPackageInfo;
    protected String mPackageName;
    protected final BroadcastReceiver mPackageRemovedReceiver;
    protected PackageManager mPm;
    protected Session mSession;
    protected ApplicationsState mState;
    protected IUsbManager mUsbManager;
    protected int mUserId;
    protected UserManager mUserManager;
    
    static {
        TAG = AppInfoBase.class.getSimpleName();
    }
    
    public AppInfoBase() {
        this.mPackageRemovedReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
                if (!AppInfoBase.this.mFinishing && (AppInfoBase.this.mAppEntry == null || AppInfoBase.this.mAppEntry.info == null || TextUtils.equals((CharSequence)AppInfoBase.this.mAppEntry.info.packageName, (CharSequence)schemeSpecificPart))) {
                    AppInfoBase.this.onPackageRemoved();
                }
            }
        };
    }
    
    public static void startAppInfoFragment(final Class<?> clazz, final int title, final String s, final int n, final Fragment fragment, final int n2, final int sourceMetricsCategory) {
        final Bundle arguments = new Bundle();
        arguments.putString("package", s);
        arguments.putInt("uid", n);
        new SubSettingLauncher(fragment.getContext()).setDestination(clazz.getName()).setSourceMetricsCategory(sourceMetricsCategory).setTitle(title).setArguments(arguments).setUserHandle(new UserHandle(UserHandle.getUserId(n))).setResultListener(fragment, n2).launch();
    }
    
    protected abstract AlertDialog createDialog(final int p0, final int p1);
    
    @Override
    public void onAllSizesComputed() {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFinishing = false;
        final Activity activity = this.getActivity();
        this.mApplicationFeatureProvider = FeatureFactory.getFactory((Context)activity).getApplicationFeatureProvider((Context)activity);
        this.mState = ApplicationsState.getInstance(activity.getApplication());
        this.mSession = this.mState.newSession((ApplicationsState.Callbacks)this, this.getLifecycle());
        this.mDpm = (DevicePolicyManager)activity.getSystemService("device_policy");
        this.mUserManager = (UserManager)activity.getSystemService("user");
        this.mPm = activity.getPackageManager();
        this.mUsbManager = IUsbManager$Stub.asInterface(ServiceManager.getService("usb"));
        this.retrieveAppEntry();
        this.startListeningToPackageRemove();
    }
    
    @Override
    public void onDestroy() {
        this.stopListeningToPackageRemove();
        super.onDestroy();
    }
    
    @Override
    public void onLauncherInfoChanged() {
    }
    
    @Override
    public void onLoadEntriesCompleted() {
    }
    
    @Override
    public void onPackageIconChanged() {
    }
    
    @Override
    public void onPackageListChanged() {
        if (!this.refreshUi()) {
            this.setIntentAndFinish(true, true);
        }
    }
    
    protected void onPackageRemoved() {
        this.getActivity().finishAndRemoveTask();
    }
    
    @Override
    public void onPackageSizeChanged(final String s) {
    }
    
    @Override
    public void onRebuildComplete(final ArrayList<AppEntry> list) {
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mAppsControlDisallowedAdmin = RestrictedLockUtils.checkIfRestrictionEnforced((Context)this.getActivity(), "no_control_apps", this.mUserId);
        this.mAppsControlDisallowedBySystem = RestrictedLockUtils.hasBaseUserRestriction((Context)this.getActivity(), "no_control_apps", this.mUserId);
        if (!this.refreshUi()) {
            this.setIntentAndFinish(true, true);
        }
    }
    
    @Override
    public void onRunningStateChanged(final boolean b) {
    }
    
    protected abstract boolean refreshUi();
    
    protected String retrieveAppEntry() {
        final Bundle arguments = this.getArguments();
        String string;
        if (arguments != null) {
            string = arguments.getString("package");
        }
        else {
            string = null;
        }
        this.mPackageName = string;
        Intent intent;
        if (arguments == null) {
            intent = this.getIntent();
        }
        else {
            intent = (Intent)arguments.getParcelable("intent");
        }
        if (this.mPackageName == null && intent != null && intent.getData() != null) {
            this.mPackageName = intent.getData().getSchemeSpecificPart();
        }
        if (intent != null && intent.hasExtra("android.intent.extra.user_handle")) {
            this.mUserId = ((UserHandle)intent.getParcelableExtra("android.intent.extra.user_handle")).getIdentifier();
        }
        else {
            this.mUserId = UserHandle.myUserId();
        }
        this.mAppEntry = this.mState.getEntry(this.mPackageName, this.mUserId);
        if (this.mAppEntry != null) {
            try {
                this.mPackageInfo = this.mPm.getPackageInfoAsUser(this.mAppEntry.info.packageName, 134222336, this.mUserId);
            }
            catch (PackageManager$NameNotFoundException ex) {
                final String tag = AppInfoBase.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Exception when retrieving package:");
                sb.append(this.mAppEntry.info.packageName);
                Log.e(tag, sb.toString(), (Throwable)ex);
            }
        }
        else {
            Log.w(AppInfoBase.TAG, "Missing AppEntry; maybe reinstalling?");
            this.mPackageInfo = null;
        }
        return this.mPackageName;
    }
    
    protected void setIntentAndFinish(final boolean b, final boolean b2) {
        final Intent intent = new Intent();
        intent.putExtra("chg", b2);
        ((SettingsActivity)this.getActivity()).finishPreferencePanel(-1, intent);
        this.mFinishing = true;
    }
    
    protected void showDialogInner(final int n, final int n2) {
        final MyAlertDialogFragment instance = MyAlertDialogFragment.newInstance(n, n2);
        instance.setTargetFragment((Fragment)this, 0);
        final FragmentManager fragmentManager = this.getFragmentManager();
        final StringBuilder sb = new StringBuilder();
        sb.append("dialog ");
        sb.append(n);
        instance.show(fragmentManager, sb.toString());
    }
    
    protected void startListeningToPackageRemove() {
        if (this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = true;
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.getContext().registerReceiver(this.mPackageRemovedReceiver, intentFilter);
    }
    
    protected void stopListeningToPackageRemove() {
        if (!this.mListeningToPackageRemove) {
            return;
        }
        this.mListeningToPackageRemove = false;
        this.getContext().unregisterReceiver(this.mPackageRemovedReceiver);
    }
    
    public static class MyAlertDialogFragment extends InstrumentedDialogFragment
    {
        public static MyAlertDialogFragment newInstance(final int n, final int n2) {
            final MyAlertDialogFragment myAlertDialogFragment = new MyAlertDialogFragment();
            final Bundle arguments = new Bundle();
            arguments.putInt("id", n);
            arguments.putInt("moveError", n2);
            myAlertDialogFragment.setArguments(arguments);
            return myAlertDialogFragment;
        }
        
        @Override
        public int getMetricsCategory() {
            return 558;
        }
        
        public Dialog onCreateDialog(final Bundle bundle) {
            final int int1 = this.getArguments().getInt("id");
            final AlertDialog dialog = ((AppInfoBase)this.getTargetFragment()).createDialog(int1, this.getArguments().getInt("moveError"));
            if (dialog != null) {
                return (Dialog)dialog;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("unknown id ");
            sb.append(int1);
            throw new IllegalArgumentException(sb.toString());
        }
    }
}
