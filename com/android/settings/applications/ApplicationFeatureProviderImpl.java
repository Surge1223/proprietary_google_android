package com.android.settings.applications;

import android.content.pm.ComponentInfo;
import android.content.ComponentName;
import com.android.internal.telephony.SmsApplication;
import android.text.TextUtils;
import android.telecom.DefaultDialerManager;
import java.util.Set;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.os.RemoteException;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.List;
import android.content.Intent;
import android.os.UserManager;
import android.content.pm.IPackageManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.app.admin.DevicePolicyManager;
import android.content.Context;

public class ApplicationFeatureProviderImpl implements ApplicationFeatureProvider
{
    private final Context mContext;
    private final DevicePolicyManager mDpm;
    private final PackageManagerWrapper mPm;
    private final IPackageManager mPms;
    private final UserManager mUm;
    
    public ApplicationFeatureProviderImpl(final Context context, final PackageManagerWrapper mPm, final IPackageManager mPms, final DevicePolicyManager mDpm) {
        this.mContext = context.getApplicationContext();
        this.mPm = mPm;
        this.mPms = mPms;
        this.mDpm = mDpm;
        this.mUm = UserManager.get(this.mContext);
    }
    
    @Override
    public void calculateNumberOfAppsWithAdminGrantedPermissions(final String[] array, final boolean b, final NumberOfAppsCallback numberOfAppsCallback) {
        final CurrentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter currentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter = new CurrentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter(this.mContext, array, this.mPm, this.mPms, this.mDpm, numberOfAppsCallback);
        if (b) {
            currentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter.execute((Object[])new Void[0]);
        }
        else {
            currentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter.executeInForeground();
        }
    }
    
    @Override
    public void calculateNumberOfPolicyInstalledApps(final boolean b, final NumberOfAppsCallback numberOfAppsCallback) {
        final CurrentUserAndManagedProfilePolicyInstalledAppCounter currentUserAndManagedProfilePolicyInstalledAppCounter = new CurrentUserAndManagedProfilePolicyInstalledAppCounter(this.mContext, this.mPm, numberOfAppsCallback);
        if (b) {
            currentUserAndManagedProfilePolicyInstalledAppCounter.execute((Object[])new Void[0]);
        }
        else {
            currentUserAndManagedProfilePolicyInstalledAppCounter.executeInForeground();
        }
    }
    
    @Override
    public List<UserAppInfo> findPersistentPreferredActivities(final int n, final Intent[] array) {
        final ArrayList<UserAppInfo> list = new ArrayList<UserAppInfo>();
        final ArraySet set = new ArraySet();
        final UserInfo userInfo = this.mUm.getUserInfo(n);
        for (final Intent intent : array) {
            try {
                final ResolveInfo persistentPreferredActivity = this.mPms.findPersistentPreferredActivity(intent, n);
                if (persistentPreferredActivity != null) {
                    Object o = null;
                    if (persistentPreferredActivity.activityInfo != null) {
                        o = persistentPreferredActivity.activityInfo;
                    }
                    else if (persistentPreferredActivity.serviceInfo != null) {
                        o = persistentPreferredActivity.serviceInfo;
                    }
                    else if (persistentPreferredActivity.providerInfo != null) {
                        o = persistentPreferredActivity.providerInfo;
                    }
                    if (o != null) {
                        final UserAppInfo userAppInfo = new UserAppInfo(userInfo, ((ComponentInfo)o).applicationInfo);
                        if (((Set<UserAppInfo>)set).add(userAppInfo)) {
                            list.add(userAppInfo);
                        }
                    }
                }
            }
            catch (RemoteException ex) {}
        }
        return list;
    }
    
    @Override
    public Set<String> getKeepEnabledPackages() {
        final ArraySet set = new ArraySet();
        final String defaultDialerApplication = DefaultDialerManager.getDefaultDialerApplication(this.mContext);
        if (!TextUtils.isEmpty((CharSequence)defaultDialerApplication)) {
            ((Set<String>)set).add(defaultDialerApplication);
        }
        final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication(this.mContext, true);
        if (defaultSmsApplication != null) {
            ((Set<String>)set).add(defaultSmsApplication.getPackageName());
        }
        return (Set<String>)set;
    }
    
    @Override
    public void listAppsWithAdminGrantedPermissions(final String[] array, final ListOfAppsCallback listOfAppsCallback) {
        new CurrentUserAppWithAdminGrantedPermissionsLister(array, this.mPm, this.mPms, this.mDpm, this.mUm, listOfAppsCallback).execute((Object[])new Void[0]);
    }
    
    @Override
    public void listPolicyInstalledApps(final ListOfAppsCallback listOfAppsCallback) {
        new CurrentUserPolicyInstalledAppLister(this.mPm, this.mUm, listOfAppsCallback).execute((Object[])new Void[0]);
    }
    
    private static class CurrentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter extends AppWithAdminGrantedPermissionsCounter
    {
        private NumberOfAppsCallback mCallback;
        
        CurrentUserAndManagedProfileAppWithAdminGrantedPermissionsCounter(final Context context, final String[] array, final PackageManagerWrapper packageManagerWrapper, final IPackageManager packageManager, final DevicePolicyManager devicePolicyManager, final NumberOfAppsCallback mCallback) {
            super(context, array, packageManagerWrapper, packageManager, devicePolicyManager);
            this.mCallback = mCallback;
        }
        
        @Override
        protected void onCountComplete(final int n) {
            this.mCallback.onNumberOfAppsResult(n);
        }
    }
    
    private static class CurrentUserAndManagedProfilePolicyInstalledAppCounter extends InstalledAppCounter
    {
        private NumberOfAppsCallback mCallback;
        
        CurrentUserAndManagedProfilePolicyInstalledAppCounter(final Context context, final PackageManagerWrapper packageManagerWrapper, final NumberOfAppsCallback mCallback) {
            super(context, 1, packageManagerWrapper);
            this.mCallback = mCallback;
        }
        
        @Override
        protected void onCountComplete(final int n) {
            this.mCallback.onNumberOfAppsResult(n);
        }
    }
    
    private static class CurrentUserAppWithAdminGrantedPermissionsLister extends AppWithAdminGrantedPermissionsLister
    {
        private ListOfAppsCallback mCallback;
        
        CurrentUserAppWithAdminGrantedPermissionsLister(final String[] array, final PackageManagerWrapper packageManagerWrapper, final IPackageManager packageManager, final DevicePolicyManager devicePolicyManager, final UserManager userManager, final ListOfAppsCallback mCallback) {
            super(array, packageManagerWrapper, packageManager, devicePolicyManager, userManager);
            this.mCallback = mCallback;
        }
        
        @Override
        protected void onAppListBuilt(final List<UserAppInfo> list) {
            this.mCallback.onListOfAppsResult(list);
        }
    }
    
    private static class CurrentUserPolicyInstalledAppLister extends InstalledAppLister
    {
        private ListOfAppsCallback mCallback;
        
        CurrentUserPolicyInstalledAppLister(final PackageManagerWrapper packageManagerWrapper, final UserManager userManager, final ListOfAppsCallback mCallback) {
            super(packageManagerWrapper, userManager);
            this.mCallback = mCallback;
        }
        
        @Override
        protected void onAppListBuilt(final List<UserAppInfo> list) {
            this.mCallback.onListOfAppsResult(list);
        }
    }
}
