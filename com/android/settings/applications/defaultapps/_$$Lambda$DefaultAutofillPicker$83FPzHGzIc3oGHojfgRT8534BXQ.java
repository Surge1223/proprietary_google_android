package com.android.settings.applications.defaultapps;

import android.content.pm.ServiceInfo;
import android.service.autofill.AutofillServiceInfo;
import android.os.Bundle;
import android.text.Html;
import com.android.settingslib.widget.CandidateInfo;
import java.util.Iterator;
import android.util.Log;
import android.content.pm.ResolveInfo;
import java.util.ArrayList;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;
import android.net.Uri;
import android.text.TextUtils;
import android.content.ComponentName;
import android.provider.Settings;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settingslib.utils.ThreadUtils;
import com.android.internal.content.PackageMonitor;
import android.content.Intent;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$DefaultAutofillPicker$83FPzHGzIc3oGHojfgRT8534BXQ implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        DefaultAutofillPicker.lambda$onCreate$0(this.f$0, dialogInterface, n);
    }
}
