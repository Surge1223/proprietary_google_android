package com.android.settings.applications.defaultapps;

import android.content.Intent;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.ComponentName;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import java.util.Iterator;
import java.util.ArrayList;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.content.Context;
import android.content.IntentFilter;

public class DefaultHomePreferenceController extends DefaultAppPreferenceController
{
    static final IntentFilter HOME_FILTER;
    private final String mPackageName;
    
    static {
        (HOME_FILTER = new IntentFilter("android.intent.action.MAIN")).addCategory("android.intent.category.HOME");
        DefaultHomePreferenceController.HOME_FILTER.addCategory("android.intent.category.DEFAULT");
    }
    
    public DefaultHomePreferenceController(final Context context) {
        super(context);
        this.mPackageName = this.mContext.getPackageName();
    }
    
    private ActivityInfo getOnlyAppInfo(final List<ResolveInfo> list) {
        final ArrayList<ActivityInfo> list2 = new ArrayList<ActivityInfo>();
        this.mPackageManager.getHomeActivities(list);
        final Iterator<ResolveInfo> iterator = list.iterator();
        while (iterator.hasNext()) {
            final ActivityInfo activityInfo = iterator.next().activityInfo;
            if (activityInfo.packageName.equals(this.mPackageName)) {
                continue;
            }
            list2.add(activityInfo);
        }
        ActivityInfo activityInfo2;
        if (list2.size() == 1) {
            activityInfo2 = list2.get(0);
        }
        else {
            activityInfo2 = null;
        }
        return activityInfo2;
    }
    
    public static boolean hasHomePreference(final String s, final Context context) {
        final ArrayList<ResolveInfo> list = new ArrayList<ResolveInfo>();
        context.getPackageManager().getHomeActivities((List)list);
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i).activityInfo.packageName.equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isHomeDefault(final String s, final PackageManagerWrapper packageManagerWrapper) {
        final ComponentName homeActivities = packageManagerWrapper.getHomeActivities(new ArrayList<ResolveInfo>());
        return homeActivities == null || homeActivities.getPackageName().equals(s);
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        final ArrayList<ResolveInfo> list = new ArrayList<ResolveInfo>();
        final ComponentName homeActivities = this.mPackageManager.getHomeActivities(list);
        if (homeActivities != null) {
            return new DefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, homeActivities);
        }
        final ActivityInfo onlyAppInfo = this.getOnlyAppInfo(list);
        if (onlyAppInfo != null) {
            return new DefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, onlyAppInfo.getComponentName());
        }
        return null;
    }
    
    @Override
    public String getPreferenceKey() {
        return "default_home";
    }
    
    @Override
    protected Intent getSettingIntent(final DefaultAppInfo defaultAppInfo) {
        final Intent intent = null;
        if (defaultAppInfo == null) {
            return null;
        }
        String package1;
        if (defaultAppInfo.componentName != null) {
            package1 = defaultAppInfo.componentName.getPackageName();
        }
        else {
            if (defaultAppInfo.packageItemInfo == null) {
                return null;
            }
            package1 = defaultAppInfo.packageItemInfo.packageName;
        }
        final Intent addFlags = new Intent("android.intent.action.APPLICATION_PREFERENCES").setPackage(package1).addFlags(268468224);
        Intent intent2 = intent;
        if (this.mPackageManager.queryIntentActivities(addFlags, 0).size() == 1) {
            intent2 = addFlags;
        }
        return intent2;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034140);
    }
}
