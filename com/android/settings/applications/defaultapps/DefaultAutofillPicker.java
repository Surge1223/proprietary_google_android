package com.android.settings.applications.defaultapps;

import android.content.pm.ServiceInfo;
import android.service.autofill.AutofillServiceInfo;
import android.os.Bundle;
import android.text.Html;
import com.android.settingslib.widget.CandidateInfo;
import java.util.Iterator;
import android.util.Log;
import android.content.pm.ResolveInfo;
import java.util.ArrayList;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;
import android.net.Uri;
import android.text.TextUtils;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.ComponentName;
import android.provider.Settings;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settingslib.utils.ThreadUtils;
import com.android.internal.content.PackageMonitor;
import android.content.DialogInterface$OnClickListener;
import android.content.Intent;

public class DefaultAutofillPicker extends DefaultAppPickerFragment
{
    static final Intent AUTOFILL_PROBE;
    private DialogInterface$OnClickListener mCancelListener;
    private final PackageMonitor mSettingsPackageMonitor;
    
    static {
        AUTOFILL_PROBE = new Intent("android.service.autofill.AutofillService");
    }
    
    public DefaultAutofillPicker() {
        this.mSettingsPackageMonitor = new PackageMonitor() {
            public void onPackageAdded(final String s, final int n) {
                ThreadUtils.postOnMainThread(new _$$Lambda$DefaultAutofillPicker$1$FkWp_TdrMINB6fYhO2TMWiQylcc(this));
            }
            
            public void onPackageModified(final String s) {
                ThreadUtils.postOnMainThread(new _$$Lambda$DefaultAutofillPicker$1$25IAggSj280QPpgEn1surevHwi4(this));
            }
            
            public void onPackageRemoved(final String s, final int n) {
                ThreadUtils.postOnMainThread(new _$$Lambda$DefaultAutofillPicker$1$wTLnu3hVgtYHDTidiWNsKDdM5mo(this));
            }
        };
    }
    
    private void addAddServicePreference() {
        final Preference addServicePreferenceOrNull = this.newAddServicePreferenceOrNull();
        if (addServicePreferenceOrNull != null) {
            this.getPreferenceScreen().addPreference(addServicePreferenceOrNull);
        }
    }
    
    public static String getDefaultKey(final Context context) {
        final String string = Settings.Secure.getString(context.getContentResolver(), "autofill_service");
        if (string != null) {
            final ComponentName unflattenFromString = ComponentName.unflattenFromString(string);
            if (unflattenFromString != null) {
                return unflattenFromString.flattenToString();
            }
        }
        return null;
    }
    
    private Preference newAddServicePreferenceOrNull() {
        final String string = Settings.Secure.getString(this.getActivity().getContentResolver(), "autofill_service_search_uri");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        final Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(string));
        final Preference preference = new Preference(this.getPrefContext());
        preference.setTitle(2131888651);
        preference.setIcon(2131231064);
        preference.setOrder(2147483646);
        preference.setIntent(intent);
        preference.setPersistent(false);
        return preference;
    }
    
    private void update() {
        this.updateCandidates();
        this.addAddServicePreference();
    }
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final ArrayList<DefaultAppInfo> list = new ArrayList<DefaultAppInfo>();
        final List<ResolveInfo> queryIntentServices = this.mPm.queryIntentServices(DefaultAutofillPicker.AUTOFILL_PROBE, 128);
        final Context context = this.getContext();
        for (final ResolveInfo resolveInfo : queryIntentServices) {
            final String permission = resolveInfo.serviceInfo.permission;
            if ("android.permission.BIND_AUTOFILL_SERVICE".equals(permission)) {
                list.add(new DefaultAppInfo(context, this.mPm, this.mUserId, new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name)));
            }
            if ("android.permission.BIND_AUTOFILL".equals(permission)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("AutofillService from '");
                sb.append(resolveInfo.serviceInfo.packageName);
                sb.append("' uses unsupported permission ");
                sb.append("android.permission.BIND_AUTOFILL");
                sb.append(". It works for now, but might not be supported on future releases");
                Log.w("DefaultAutofillPicker", sb.toString());
                list.add(new DefaultAppInfo(context, this.mPm, this.mUserId, new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name)));
            }
        }
        return list;
    }
    
    @Override
    protected CharSequence getConfirmationMessage(final CandidateInfo candidateInfo) {
        if (candidateInfo == null) {
            return null;
        }
        return (CharSequence)Html.fromHtml(this.getContext().getString(2131886503, new Object[] { candidateInfo.loadLabel() }));
    }
    
    @Override
    protected String getDefaultKey() {
        return getDefaultKey(this.getContext());
    }
    
    @Override
    public int getMetricsCategory() {
        return 792;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082748;
    }
    
    @Override
    protected ConfirmationDialogFragment newConfirmationDialogFragment(final String s, final CharSequence charSequence) {
        final AutofillPickerConfirmationDialogFragment autofillPickerConfirmationDialogFragment = new AutofillPickerConfirmationDialogFragment();
        ((ConfirmationDialogFragment)autofillPickerConfirmationDialogFragment).init(this, s, charSequence);
        return autofillPickerConfirmationDialogFragment;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Activity activity = this.getActivity();
        if (activity != null && activity.getIntent().getStringExtra("package_name") != null) {
            this.mCancelListener = (DialogInterface$OnClickListener)new _$$Lambda$DefaultAutofillPicker$83FPzHGzIc3oGHojfgRT8534BXQ(activity);
        }
        this.mSettingsPackageMonitor.register((Context)activity, activity.getMainLooper(), false);
        this.update();
    }
    
    @Override
    public void onDestroy() {
        this.mSettingsPackageMonitor.unregister();
        super.onDestroy();
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        Settings.Secure.putString(this.getContext().getContentResolver(), "autofill_service", s);
        final Activity activity = this.getActivity();
        if (activity != null) {
            final String stringExtra = activity.getIntent().getStringExtra("package_name");
            if (stringExtra != null) {
                int result;
                if (s != null && s.startsWith(stringExtra)) {
                    result = -1;
                }
                else {
                    result = 0;
                }
                activity.setResult(result);
                activity.finish();
            }
        }
        return true;
    }
    
    @Override
    protected boolean shouldShowItemNone() {
        return true;
    }
    
    public static class AutofillPickerConfirmationDialogFragment extends ConfirmationDialogFragment
    {
        public void onCreate(final Bundle bundle) {
            ((ConfirmationDialogFragment)this).setCancelListener(((DefaultAutofillPicker)this.getTargetFragment()).mCancelListener);
            super.onCreate(bundle);
        }
    }
    
    static final class AutofillSettingIntentProvider
    {
        private final Context mContext;
        private final String mSelectedKey;
        
        public AutofillSettingIntentProvider(final Context mContext, final String mSelectedKey) {
            this.mSelectedKey = mSelectedKey;
            this.mContext = mContext;
        }
        
        public Intent getIntent() {
            final Iterator<ResolveInfo> iterator = this.mContext.getPackageManager().queryIntentServices(DefaultAutofillPicker.AUTOFILL_PROBE, 128).iterator();
            while (iterator.hasNext()) {
                final ServiceInfo serviceInfo = iterator.next().serviceInfo;
                if (TextUtils.equals((CharSequence)this.mSelectedKey, (CharSequence)new ComponentName(serviceInfo.packageName, serviceInfo.name).flattenToString())) {
                    try {
                        final String settingsActivity = new AutofillServiceInfo(this.mContext, serviceInfo).getSettingsActivity();
                        if (TextUtils.isEmpty((CharSequence)settingsActivity)) {
                            return null;
                        }
                        return new Intent("android.intent.action.MAIN").setComponent(new ComponentName(serviceInfo.packageName, settingsActivity));
                    }
                    catch (SecurityException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Error getting info for ");
                        sb.append(serviceInfo);
                        sb.append(": ");
                        sb.append(ex);
                        Log.w("DefaultAutofillPicker", sb.toString());
                        return null;
                    }
                }
            }
            return null;
        }
    }
}
