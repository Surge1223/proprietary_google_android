package com.android.settings.applications.defaultapps;

import com.android.settings.Utils;
import android.content.Context;
import android.os.UserHandle;

public class DefaultWorkPhonePreferenceController extends DefaultPhonePreferenceController
{
    private final UserHandle mUserHandle;
    
    public DefaultWorkPhonePreferenceController(final Context context) {
        super(context);
        this.mUserHandle = Utils.getManagedProfile(this.mUserManager);
        if (this.mUserHandle != null) {
            this.mUserId = this.mUserHandle.getIdentifier();
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "work_default_phone_app";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUserHandle != null && super.isAvailable();
    }
}
