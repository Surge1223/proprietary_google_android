package com.android.settings.applications.defaultapps;

import android.content.ComponentName;
import android.text.TextUtils;
import com.android.settings.Utils;
import com.android.settingslib.widget.CandidateInfo;
import java.util.Iterator;
import java.util.Collection;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import com.android.internal.telephony.SmsApplication$SmsApplicationData;
import java.util.ArrayList;
import com.android.internal.telephony.SmsApplication;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;

public class DefaultSmsPicker extends DefaultAppPickerFragment
{
    private DefaultKeyUpdater mDefaultKeyUpdater;
    
    public DefaultSmsPicker() {
        this.mDefaultKeyUpdater = new DefaultKeyUpdater();
    }
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final Context context = this.getContext();
        final Collection applicationCollection = SmsApplication.getApplicationCollection(context);
        final ArrayList list = new ArrayList<DefaultAppInfo>(applicationCollection.size());
        for (final SmsApplication$SmsApplicationData smsApplication$SmsApplicationData : applicationCollection) {
            try {
                list.add(new DefaultAppInfo(context, this.mPm, (PackageItemInfo)this.mPm.getApplicationInfoAsUser(smsApplication$SmsApplicationData.mPackageName, 0, this.mUserId)));
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        return (List<DefaultAppInfo>)list;
    }
    
    @Override
    protected String getConfirmationMessage(final CandidateInfo candidateInfo) {
        String string;
        if (Utils.isPackageDirectBootAware(this.getContext(), candidateInfo.getKey())) {
            string = null;
        }
        else {
            string = this.getContext().getString(R.string.direct_boot_unaware_dialog_message);
        }
        return string;
    }
    
    @Override
    protected String getDefaultKey() {
        return this.mDefaultKeyUpdater.getDefaultApplication(this.getContext());
    }
    
    @Override
    public int getMetricsCategory() {
        return 789;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082753;
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s) && !TextUtils.equals((CharSequence)s, (CharSequence)this.getDefaultKey())) {
            this.mDefaultKeyUpdater.setDefaultApplication(this.getContext(), s);
            return true;
        }
        return false;
    }
    
    static class DefaultKeyUpdater
    {
        public String getDefaultApplication(final Context context) {
            final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication(context, true);
            if (defaultSmsApplication != null) {
                return defaultSmsApplication.getPackageName();
            }
            return null;
        }
        
        public void setDefaultApplication(final Context context, final String s) {
            SmsApplication.setDefaultApplication(s, context);
        }
    }
}
