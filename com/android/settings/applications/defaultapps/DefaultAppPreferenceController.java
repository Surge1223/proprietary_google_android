package com.android.settings.applications.defaultapps;

import android.util.Log;
import com.android.settings.Utils;
import android.text.TextUtils;
import com.android.settingslib.TwoTargetPreference;
import android.graphics.drawable.Drawable;
import android.support.v7.preference.Preference;
import com.android.settingslib.applications.DefaultAppInfo;
import com.android.settings.widget.GearPreference;
import android.content.Intent;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class DefaultAppPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    protected final PackageManagerWrapper mPackageManager;
    protected int mUserId;
    protected final UserManager mUserManager;
    
    public DefaultAppPreferenceController(final Context context) {
        super(context);
        this.mPackageManager = new PackageManagerWrapper(context.getPackageManager());
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mUserId = UserHandle.myUserId();
    }
    
    private void mayUpdateGearIcon(final DefaultAppInfo defaultAppInfo, final Preference preference) {
        if (!(preference instanceof GearPreference)) {
            return;
        }
        final Intent settingIntent = this.getSettingIntent(defaultAppInfo);
        if (settingIntent != null) {
            ((GearPreference)preference).setOnGearClickListener((GearPreference.OnGearClickListener)new _$$Lambda$DefaultAppPreferenceController$P93yGe3NhKzPqeqQwHkMaXpVB1M(this, settingIntent));
        }
        else {
            ((GearPreference)preference).setOnGearClickListener(null);
        }
    }
    
    public Drawable getDefaultAppIcon() {
        if (!this.isAvailable()) {
            return null;
        }
        final DefaultAppInfo defaultAppInfo = this.getDefaultAppInfo();
        if (defaultAppInfo != null) {
            return defaultAppInfo.loadIcon();
        }
        return null;
    }
    
    protected abstract DefaultAppInfo getDefaultAppInfo();
    
    public CharSequence getDefaultAppLabel() {
        if (!this.isAvailable()) {
            return null;
        }
        final DefaultAppInfo defaultAppInfo = this.getDefaultAppInfo();
        if (defaultAppInfo != null) {
            return defaultAppInfo.loadLabel();
        }
        return null;
    }
    
    protected Intent getSettingIntent(final DefaultAppInfo defaultAppInfo) {
        return null;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final DefaultAppInfo defaultAppInfo = this.getDefaultAppInfo();
        final CharSequence defaultAppLabel = this.getDefaultAppLabel();
        if (preference instanceof TwoTargetPreference) {
            ((TwoTargetPreference)preference).setIconSize(1);
        }
        if (!TextUtils.isEmpty(defaultAppLabel)) {
            preference.setSummary(defaultAppLabel);
            Utils.setSafeIcon(preference, this.getDefaultAppIcon());
        }
        else {
            Log.d("DefaultAppPrefControl", "No default app");
            preference.setSummary(2131886368);
            preference.setIcon(null);
        }
        this.mayUpdateGearIcon(defaultAppInfo, preference);
    }
}
