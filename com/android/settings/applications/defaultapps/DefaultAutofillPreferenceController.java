package com.android.settings.applications.defaultapps;

import android.content.Intent;
import android.content.ComponentName;
import android.text.TextUtils;
import android.provider.Settings;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.Context;
import android.view.autofill.AutofillManager;

public class DefaultAutofillPreferenceController extends DefaultAppPreferenceController
{
    private final AutofillManager mAutofillManager;
    
    public DefaultAutofillPreferenceController(final Context context) {
        super(context);
        this.mAutofillManager = (AutofillManager)this.mContext.getSystemService((Class)AutofillManager.class);
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        final String string = Settings.Secure.getString(this.mContext.getContentResolver(), "autofill_service");
        if (!TextUtils.isEmpty((CharSequence)string)) {
            return new DefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, ComponentName.unflattenFromString(string));
        }
        return null;
    }
    
    @Override
    public String getPreferenceKey() {
        return "default_autofill";
    }
    
    @Override
    protected Intent getSettingIntent(final DefaultAppInfo defaultAppInfo) {
        if (defaultAppInfo == null) {
            return null;
        }
        return new DefaultAutofillPicker.AutofillSettingIntentProvider(this.mContext, defaultAppInfo.getKey()).getIntent();
    }
    
    @Override
    public boolean isAvailable() {
        return this.mAutofillManager != null && this.mAutofillManager.hasAutofillFeature() && this.mAutofillManager.isAutofillSupported();
    }
}
