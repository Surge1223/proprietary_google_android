package com.android.settings.applications.defaultapps;

import android.telephony.TelephonyManager;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.ComponentName;
import java.util.Iterator;
import com.android.internal.telephony.SmsApplication$SmsApplicationData;
import com.android.internal.telephony.SmsApplication;
import android.content.Context;

public class DefaultSmsPreferenceController extends DefaultAppPreferenceController
{
    public DefaultSmsPreferenceController(final Context context) {
        super(context);
    }
    
    public static boolean hasSmsPreference(final String s, final Context context) {
        final Iterator<SmsApplication$SmsApplicationData> iterator = SmsApplication.getApplicationCollection(context).iterator();
        while (iterator.hasNext()) {
            if (iterator.next().mPackageName.equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isSmsDefault(final String s, final Context context) {
        boolean b = true;
        final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication(context, true);
        if (defaultSmsApplication == null || !defaultSmsApplication.getPackageName().equals(s)) {
            b = false;
        }
        return b;
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication(this.mContext, true);
        if (defaultSmsApplication != null) {
            return new DefaultAppInfo(this.mContext, this.mPackageManager, this.mUserId, defaultSmsApplication);
        }
        return null;
    }
    
    @Override
    public String getPreferenceKey() {
        return "default_sms_app";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean restricted = this.mUserManager.getUserInfo(this.mUserId).isRestricted();
        final TelephonyManager telephonyManager = (TelephonyManager)this.mContext.getSystemService("phone");
        return !restricted && telephonyManager.isSmsCapable();
    }
}
