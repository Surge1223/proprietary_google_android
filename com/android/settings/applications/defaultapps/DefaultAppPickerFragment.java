package com.android.settings.applications.defaultapps;

import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.app.Fragment;
import android.os.Bundle;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.util.Pair;
import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.android.settingslib.applications.DefaultAppInfo;
import com.android.settingslib.widget.CandidateInfo;
import com.android.settings.widget.RadioButtonPreference;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.widget.RadioButtonPickerFragment;

public abstract class DefaultAppPickerFragment extends RadioButtonPickerFragment
{
    protected PackageManagerWrapper mPm;
    
    @Override
    public void bindPreferenceExtra(final RadioButtonPreference radioButtonPreference, final String s, final CandidateInfo candidateInfo, final String s2, final String s3) {
        if (!(candidateInfo instanceof DefaultAppInfo)) {
            return;
        }
        if (TextUtils.equals((CharSequence)s3, (CharSequence)s)) {
            radioButtonPreference.setSummary(2131889442);
        }
        else if (!TextUtils.isEmpty((CharSequence)((DefaultAppInfo)candidateInfo).summary)) {
            radioButtonPreference.setSummary(((DefaultAppInfo)candidateInfo).summary);
        }
    }
    
    protected CharSequence getConfirmationMessage(final CandidateInfo candidateInfo) {
        return null;
    }
    
    protected ConfirmationDialogFragment newConfirmationDialogFragment(final String s, final CharSequence charSequence) {
        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
        confirmationDialogFragment.init(this, s, charSequence);
        return confirmationDialogFragment;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mPm = new PackageManagerWrapper(context.getPackageManager());
    }
    
    @Override
    public void onRadioButtonClicked(final RadioButtonPreference radioButtonPreference) {
        final String key = radioButtonPreference.getKey();
        final CharSequence confirmationMessage = this.getConfirmationMessage(this.getCandidate(key));
        final Activity activity = this.getActivity();
        if (TextUtils.isEmpty(confirmationMessage)) {
            super.onRadioButtonClicked(radioButtonPreference);
        }
        else if (activity != null) {
            this.newConfirmationDialogFragment(key, confirmationMessage).show(activity.getFragmentManager(), "DefaultAppConfirm");
        }
    }
    
    @Override
    protected void onRadioButtonConfirmed(final String s) {
        this.mMetricsFeatureProvider.action(this.getContext(), 1000, s, Pair.create((Object)833, (Object)this.getMetricsCategory()));
        super.onRadioButtonConfirmed(s);
    }
    
    public static class ConfirmationDialogFragment extends InstrumentedDialogFragment implements DialogInterface$OnClickListener
    {
        private DialogInterface$OnClickListener mCancelListener;
        
        public int getMetricsCategory() {
            return 791;
        }
        
        public void init(final DefaultAppPickerFragment defaultAppPickerFragment, final String s, final CharSequence charSequence) {
            final Bundle arguments = new Bundle();
            arguments.putString("extra_key", s);
            arguments.putCharSequence("extra_message", charSequence);
            this.setArguments(arguments);
            this.setTargetFragment((Fragment)defaultAppPickerFragment, 0);
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            final Fragment targetFragment = this.getTargetFragment();
            if (targetFragment instanceof DefaultAppPickerFragment) {
                ((DefaultAppPickerFragment)targetFragment).onRadioButtonConfirmed(this.getArguments().getString("extra_key"));
            }
        }
        
        public Dialog onCreateDialog(Bundle arguments) {
            arguments = this.getArguments();
            return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setMessage(arguments.getCharSequence("extra_message")).setPositiveButton(17039370, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, this.mCancelListener).create();
        }
        
        public void setCancelListener(final DialogInterface$OnClickListener mCancelListener) {
            this.mCancelListener = mCancelListener;
        }
    }
}
