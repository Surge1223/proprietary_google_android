package com.android.settings.applications.defaultapps;

import android.support.v7.preference.Preference;
import com.android.settingslib.applications.DefaultAppInfo;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import android.util.IconDrawableFactory;
import android.util.Log;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.content.Context;
import android.net.Uri;
import android.content.Intent;

public class DefaultBrowserPreferenceController extends DefaultAppPreferenceController
{
    static final Intent BROWSE_PROBE;
    
    static {
        BROWSE_PROBE = new Intent().setAction("android.intent.action.VIEW").addCategory("android.intent.category.BROWSABLE").setData(Uri.parse("http:"));
    }
    
    public DefaultBrowserPreferenceController(final Context context) {
        super(context);
    }
    
    private List<ResolveInfo> getCandidates() {
        return this.mPackageManager.queryIntentActivitiesAsUser(DefaultBrowserPreferenceController.BROWSE_PROBE, 131072, this.mUserId);
    }
    
    private Drawable getOnlyAppIcon() {
        final List<ResolveInfo> candidates = this.getCandidates();
        if (candidates != null && candidates.size() == 1) {
            final ComponentInfo componentInfo = candidates.get(0).getComponentInfo();
            String packageName;
            if (componentInfo == null) {
                packageName = null;
            }
            else {
                packageName = componentInfo.packageName;
            }
            if (TextUtils.isEmpty((CharSequence)packageName)) {
                return null;
            }
            try {
                final ApplicationInfo applicationInfo = this.mPackageManager.getPackageManager().getApplicationInfo(packageName, 0);
                final StringBuilder sb = new StringBuilder();
                sb.append("Getting icon for the only browser app: ");
                sb.append(packageName);
                Log.d("BrowserPrefCtrl", sb.toString());
                return IconDrawableFactory.newInstance(this.mContext).getBadgedIcon((PackageItemInfo)componentInfo, applicationInfo, this.mUserId);
            }
            catch (PackageManager$NameNotFoundException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Error getting app info for ");
                sb2.append(packageName);
                Log.w("BrowserPrefCtrl", sb2.toString());
                return null;
            }
        }
        return null;
    }
    
    private String getOnlyAppLabel() {
        final List<ResolveInfo> candidates = this.getCandidates();
        String packageName = null;
        if (candidates != null && candidates.size() == 1) {
            final ResolveInfo resolveInfo = candidates.get(0);
            final String string = resolveInfo.loadLabel(this.mPackageManager.getPackageManager()).toString();
            final ComponentInfo componentInfo = resolveInfo.getComponentInfo();
            if (componentInfo != null) {
                packageName = componentInfo.packageName;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Getting label for the only browser app: ");
            sb.append(packageName);
            sb.append(string);
            Log.d("BrowserPrefCtrl", sb.toString());
            return string;
        }
        return null;
    }
    
    public static boolean hasBrowserPreference(final String package1, final Context context) {
        final Intent intent = new Intent(DefaultBrowserPreferenceController.BROWSE_PROBE);
        intent.setPackage(package1);
        final PackageManager packageManager = context.getPackageManager();
        final boolean b = false;
        final List queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        boolean b2 = b;
        if (queryIntentActivities != null) {
            b2 = b;
            if (queryIntentActivities.size() != 0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public Drawable getDefaultAppIcon() {
        if (!this.isAvailable()) {
            return null;
        }
        final DefaultAppInfo defaultAppInfo = this.getDefaultAppInfo();
        if (defaultAppInfo != null) {
            return defaultAppInfo.loadIcon();
        }
        return this.getOnlyAppIcon();
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        try {
            final String defaultBrowserPackageNameAsUser = this.mPackageManager.getDefaultBrowserPackageNameAsUser(this.mUserId);
            final StringBuilder sb = new StringBuilder();
            sb.append("Get default browser package: ");
            sb.append(defaultBrowserPackageNameAsUser);
            Log.d("BrowserPrefCtrl", sb.toString());
            return new DefaultAppInfo(this.mContext, this.mPackageManager, (PackageItemInfo)this.mPackageManager.getPackageManager().getApplicationInfo(defaultBrowserPackageNameAsUser, 0));
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @Override
    public CharSequence getDefaultAppLabel() {
        final boolean available = this.isAvailable();
        CharSequence loadLabel = null;
        if (!available) {
            return null;
        }
        final DefaultAppInfo defaultAppInfo = this.getDefaultAppInfo();
        if (defaultAppInfo != null) {
            loadLabel = defaultAppInfo.loadLabel();
        }
        if (!TextUtils.isEmpty(loadLabel)) {
            return loadLabel;
        }
        return this.getOnlyAppLabel();
    }
    
    @Override
    public String getPreferenceKey() {
        return "default_browser";
    }
    
    @Override
    public boolean isAvailable() {
        final List<ResolveInfo> candidates = this.getCandidates();
        return candidates != null && !candidates.isEmpty();
    }
    
    public boolean isBrowserDefault(final String s, final int n) {
        final String defaultBrowserPackageNameAsUser = this.mPackageManager.getDefaultBrowserPackageNameAsUser(n);
        if (defaultBrowserPackageNameAsUser != null) {
            return defaultBrowserPackageNameAsUser.equals(s);
        }
        final List<ResolveInfo> queryIntentActivitiesAsUser = this.mPackageManager.queryIntentActivitiesAsUser(DefaultBrowserPreferenceController.BROWSE_PROBE, 131072, n);
        boolean b = true;
        if (queryIntentActivitiesAsUser == null || queryIntentActivitiesAsUser.size() != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void updateState(final Preference preference) {
        super.updateState(preference);
        final CharSequence defaultAppLabel = this.getDefaultAppLabel();
        if (!TextUtils.isEmpty(defaultAppLabel)) {
            preference.setSummary(defaultAppLabel);
        }
    }
}
