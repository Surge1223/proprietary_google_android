package com.android.settings.applications.defaultapps;

import java.util.Set;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import android.content.pm.ResolveInfo;
import android.util.ArraySet;
import java.util.ArrayList;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;

public class DefaultBrowserPicker extends DefaultAppPickerFragment
{
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final ArrayList<DefaultAppInfo> list = new ArrayList<DefaultAppInfo>();
        final Context context = this.getContext();
        final List<ResolveInfo> queryIntentActivitiesAsUser = this.mPm.queryIntentActivitiesAsUser(DefaultBrowserPreferenceController.BROWSE_PROBE, 131072, this.mUserId);
        final int size = queryIntentActivitiesAsUser.size();
        final ArraySet set = new ArraySet();
        for (int i = 0; i < size; ++i) {
            final ResolveInfo resolveInfo = queryIntentActivitiesAsUser.get(i);
            if (resolveInfo.activityInfo != null) {
                if (resolveInfo.handleAllWebDataURI) {
                    final String packageName = resolveInfo.activityInfo.packageName;
                    if (!((Set)set).contains(packageName)) {
                        try {
                            list.add(new DefaultAppInfo(context, this.mPm, (PackageItemInfo)this.mPm.getApplicationInfoAsUser(packageName, 0, this.mUserId)));
                            ((Set<String>)set).add(packageName);
                        }
                        catch (PackageManager$NameNotFoundException ex) {}
                    }
                }
            }
        }
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        return this.mPm.getDefaultBrowserPackageNameAsUser(this.mUserId);
    }
    
    @Override
    public int getMetricsCategory() {
        return 785;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082749;
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        return this.mPm.setDefaultBrowserPackageNameAsUser(s, this.mUserId);
    }
}
