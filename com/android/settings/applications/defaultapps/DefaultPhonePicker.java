package com.android.settings.applications.defaultapps;

import android.text.TextUtils;
import android.telecom.TelecomManager;
import java.util.Iterator;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import android.telecom.DefaultDialerManager;
import java.util.ArrayList;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;

public class DefaultPhonePicker extends DefaultAppPickerFragment
{
    private DefaultKeyUpdater mDefaultKeyUpdater;
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final ArrayList<DefaultAppInfo> list = new ArrayList<DefaultAppInfo>();
        final List installedDialerApplications = DefaultDialerManager.getInstalledDialerApplications(this.getContext(), this.mUserId);
        final Context context = this.getContext();
        for (final String s : installedDialerApplications) {
            try {
                list.add(new DefaultAppInfo(context, this.mPm, (PackageItemInfo)this.mPm.getApplicationInfoAsUser(s, 0, this.mUserId)));
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        return this.mDefaultKeyUpdater.getDefaultDialerApplication(this.getContext(), this.mUserId);
    }
    
    @Override
    public int getMetricsCategory() {
        return 788;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082752;
    }
    
    @Override
    protected String getSystemDefaultKey() {
        return this.mDefaultKeyUpdater.getSystemDialerPackage();
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mDefaultKeyUpdater = new DefaultKeyUpdater((TelecomManager)context.getSystemService("telecom"));
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        return !TextUtils.isEmpty((CharSequence)s) && !TextUtils.equals((CharSequence)s, (CharSequence)this.getDefaultKey()) && this.mDefaultKeyUpdater.setDefaultDialerApplication(this.getContext(), s, this.mUserId);
    }
    
    static class DefaultKeyUpdater
    {
        private final TelecomManager mTelecomManager;
        
        public DefaultKeyUpdater(final TelecomManager mTelecomManager) {
            this.mTelecomManager = mTelecomManager;
        }
        
        public String getDefaultDialerApplication(final Context context, final int n) {
            return DefaultDialerManager.getDefaultDialerApplication(context, n);
        }
        
        public String getSystemDialerPackage() {
            return this.mTelecomManager.getSystemDialerPackage();
        }
        
        public boolean setDefaultDialerApplication(final Context context, final String s, final int n) {
            return DefaultDialerManager.setDefaultDialerApplication(context, s, n);
        }
    }
}
