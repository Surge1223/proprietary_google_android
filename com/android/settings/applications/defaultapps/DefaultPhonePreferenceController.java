package com.android.settings.applications.defaultapps;

import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.applications.DefaultAppInfo;
import android.os.UserHandle;
import android.telecom.DefaultDialerManager;
import java.util.List;
import android.content.Context;

public class DefaultPhonePreferenceController extends DefaultAppPreferenceController
{
    public DefaultPhonePreferenceController(final Context context) {
        super(context);
    }
    
    private List<String> getCandidates() {
        return (List<String>)DefaultDialerManager.getInstalledDialerApplications(this.mContext, this.mUserId);
    }
    
    public static boolean hasPhonePreference(final String s, final Context context) {
        return DefaultDialerManager.getInstalledDialerApplications(context, UserHandle.myUserId()).contains(s);
    }
    
    public static boolean isPhoneDefault(final String s, final Context context) {
        final String defaultDialerApplication = DefaultDialerManager.getDefaultDialerApplication(context, UserHandle.myUserId());
        return defaultDialerApplication != null && defaultDialerApplication.equals(s);
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        try {
            return new DefaultAppInfo(this.mContext, this.mPackageManager, (PackageItemInfo)this.mPackageManager.getPackageManager().getApplicationInfo(DefaultDialerManager.getDefaultDialerApplication(this.mContext, this.mUserId), 0));
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "default_phone_app";
    }
    
    @Override
    public boolean isAvailable() {
        final boolean voiceCapable = ((TelephonyManager)this.mContext.getSystemService("phone")).isVoiceCapable();
        final boolean b = false;
        if (!voiceCapable) {
            return false;
        }
        if (((UserManager)this.mContext.getSystemService("user")).hasUserRestriction("no_outgoing_calls")) {
            return false;
        }
        final List<String> candidates = this.getCandidates();
        boolean b2 = b;
        if (candidates != null) {
            b2 = b;
            if (!candidates.isEmpty()) {
                b2 = true;
            }
        }
        return b2;
    }
}
