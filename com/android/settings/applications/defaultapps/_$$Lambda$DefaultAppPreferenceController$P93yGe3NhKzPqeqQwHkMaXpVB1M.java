package com.android.settings.applications.defaultapps;

import android.util.Log;
import com.android.settings.Utils;
import android.text.TextUtils;
import com.android.settingslib.TwoTargetPreference;
import android.graphics.drawable.Drawable;
import android.support.v7.preference.Preference;
import com.android.settingslib.applications.DefaultAppInfo;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;
import android.content.Intent;
import com.android.settings.widget.GearPreference;

public final class _$$Lambda$DefaultAppPreferenceController$P93yGe3NhKzPqeqQwHkMaXpVB1M implements OnGearClickListener
{
    @Override
    public final void onGearClick(final GearPreference gearPreference) {
        DefaultAppPreferenceController.lambda$mayUpdateGearIcon$0(this.f$0, this.f$1, gearPreference);
    }
}
