package com.android.settings.applications.defaultapps;

import com.android.settingslib.applications.DefaultAppInfo;
import android.provider.Settings;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.Intent;

public class DefaultEmergencyPreferenceController extends DefaultAppPreferenceController
{
    public static final Intent QUERY_INTENT;
    
    static {
        QUERY_INTENT = new Intent("android.telephony.action.EMERGENCY_ASSISTANCE");
    }
    
    public DefaultEmergencyPreferenceController(final Context context) {
        super(context);
    }
    
    public static boolean hasEmergencyPreference(final String package1, final Context context) {
        final Intent intent = new Intent(DefaultEmergencyPreferenceController.QUERY_INTENT);
        intent.setPackage(package1);
        final PackageManager packageManager = context.getPackageManager();
        final boolean b = false;
        final List queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        boolean b2 = b;
        if (queryIntentActivities != null) {
            b2 = b;
            if (queryIntentActivities.size() != 0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean isEmergencyDefault(final String s, final Context context) {
        final String string = Settings.Secure.getString(context.getContentResolver(), "emergency_assistance_application");
        return string != null && string.equals(s);
    }
    
    @Override
    protected DefaultAppInfo getDefaultAppInfo() {
        return null;
    }
    
    @Override
    public String getPreferenceKey() {
        return "default_emergency_app";
    }
    
    @Override
    public boolean isAvailable() {
        return false;
    }
}
