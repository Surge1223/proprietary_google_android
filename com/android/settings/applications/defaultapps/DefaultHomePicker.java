package com.android.settings.applications.defaultapps;

import android.content.Intent;
import android.text.TextUtils;
import android.content.pm.ActivityInfo;
import android.content.Context;
import android.content.ComponentName;
import java.util.ArrayList;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import java.util.Iterator;
import android.content.pm.UserInfo;

public class DefaultHomePicker extends DefaultAppPickerFragment
{
    private String mPackageName;
    
    private boolean hasManagedProfile() {
        final Iterator<UserInfo> iterator = this.mUserManager.getProfiles(this.getContext().getUserId()).iterator();
        while (iterator.hasNext()) {
            if (iterator.next().isManagedProfile()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean launcherHasManagedProfilesFeature(final ResolveInfo resolveInfo) {
        try {
            return this.versionNumberAtLeastL(this.mPm.getPackageManager().getApplicationInfo(resolveInfo.activityInfo.packageName, 0).targetSdkVersion);
        }
        catch (PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    private boolean versionNumberAtLeastL(final int n) {
        return n >= 21;
    }
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final boolean hasManagedProfile = this.hasManagedProfile();
        final ArrayList<DefaultAppInfo> list = new ArrayList<DefaultAppInfo>();
        final ArrayList<ResolveInfo> list2 = new ArrayList<ResolveInfo>();
        final Context context = this.getContext();
        this.mPm.getHomeActivities(list2);
        for (final ResolveInfo resolveInfo : list2) {
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            final ComponentName componentName = new ComponentName(activityInfo.packageName, activityInfo.name);
            if (activityInfo.packageName.equals(this.mPackageName)) {
                continue;
            }
            boolean b = true;
            String string;
            if (hasManagedProfile && !this.launcherHasManagedProfilesFeature(resolveInfo)) {
                string = this.getContext().getString(2131887855);
                b = false;
            }
            else {
                string = null;
            }
            list.add(new DefaultAppInfo(context, this.mPm, this.mUserId, componentName, string, b));
        }
        return list;
    }
    
    @Override
    protected String getDefaultKey() {
        final ComponentName homeActivities = this.mPm.getHomeActivities(new ArrayList<ResolveInfo>());
        if (homeActivities != null) {
            return homeActivities.flattenToString();
        }
        return null;
    }
    
    @Override
    public int getMetricsCategory() {
        return 787;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082751;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mPackageName = context.getPackageName();
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final ComponentName unflattenFromString = ComponentName.unflattenFromString(s);
            final ArrayList<ResolveInfo> list = new ArrayList<ResolveInfo>();
            this.mPm.getHomeActivities(list);
            final ArrayList<ComponentName> list2 = new ArrayList<ComponentName>();
            final Iterator<Object> iterator = list.iterator();
            while (iterator.hasNext()) {
                final ActivityInfo activityInfo = iterator.next().activityInfo;
                list2.add(new ComponentName(activityInfo.packageName, activityInfo.name));
            }
            this.mPm.replacePreferredActivity(DefaultHomePreferenceController.HOME_FILTER, 1048576, list2.toArray(new ComponentName[0]), unflattenFromString);
            final Context context = this.getContext();
            final Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            intent.setFlags(268435456);
            context.startActivity(intent);
            return true;
        }
        return false;
    }
}
