package com.android.settings.applications.defaultapps;

import com.android.settings.Utils;
import android.content.Context;
import android.os.UserHandle;

public class DefaultWorkBrowserPreferenceController extends DefaultBrowserPreferenceController
{
    private final UserHandle mUserHandle;
    
    public DefaultWorkBrowserPreferenceController(final Context context) {
        super(context);
        this.mUserHandle = Utils.getManagedProfile(this.mUserManager);
        if (this.mUserHandle != null) {
            this.mUserId = this.mUserHandle.getIdentifier();
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "work_default_browser";
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUserHandle != null && super.isAvailable();
    }
}
