package com.android.settings.applications.defaultapps;

import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.Utils;
import com.android.settingslib.widget.CandidateInfo;
import java.util.Iterator;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import android.content.pm.PackageManager;
import android.content.pm.PackageItemInfo;
import android.content.pm.ResolveInfo;
import java.util.ArrayList;
import com.android.settingslib.applications.DefaultAppInfo;
import java.util.List;
import android.content.pm.ApplicationInfo;

public class DefaultEmergencyPicker extends DefaultAppPickerFragment
{
    private boolean isSystemApp(final ApplicationInfo applicationInfo) {
        boolean b = true;
        if (applicationInfo == null || (applicationInfo.flags & 0x1) == 0x0) {
            b = false;
        }
        return b;
    }
    
    @Override
    protected List<DefaultAppInfo> getCandidates() {
        final ArrayList<DefaultAppInfo> list = new ArrayList<DefaultAppInfo>();
        final List queryIntentActivities = this.mPm.getPackageManager().queryIntentActivities(DefaultEmergencyPreferenceController.QUERY_INTENT, 0);
        PackageInfo packageInfo = null;
        final Context context = this.getContext();
        for (final ResolveInfo resolveInfo : queryIntentActivities) {
            try {
                final PackageInfo packageInfo2 = this.mPm.getPackageManager().getPackageInfo(resolveInfo.activityInfo.packageName, 0);
                final ApplicationInfo applicationInfo = packageInfo2.applicationInfo;
                list.add(new DefaultAppInfo(context, this.mPm, (PackageItemInfo)applicationInfo));
                PackageInfo packageInfo3 = packageInfo;
                Label_0152: {
                    if (this.isSystemApp(applicationInfo)) {
                        if (packageInfo != null) {
                            final long firstInstallTime = packageInfo.firstInstallTime;
                            final long firstInstallTime2 = packageInfo2.firstInstallTime;
                            packageInfo3 = packageInfo;
                            if (firstInstallTime <= firstInstallTime2) {
                                break Label_0152;
                            }
                        }
                        packageInfo3 = packageInfo2;
                    }
                }
                packageInfo = packageInfo3;
            }
            catch (PackageManager$NameNotFoundException ex) {}
            if (packageInfo != null && TextUtils.isEmpty((CharSequence)this.getDefaultKey())) {
                this.setDefaultKey(packageInfo.packageName);
            }
        }
        return list;
    }
    
    @Override
    protected String getConfirmationMessage(final CandidateInfo candidateInfo) {
        String string;
        if (Utils.isPackageDirectBootAware(this.getContext(), candidateInfo.getKey())) {
            string = null;
        }
        else {
            string = this.getContext().getString(R.string.direct_boot_unaware_dialog_message);
        }
        return string;
    }
    
    @Override
    protected String getDefaultKey() {
        return Settings.Secure.getString(this.getContext().getContentResolver(), "emergency_assistance_application");
    }
    
    @Override
    public int getMetricsCategory() {
        return 786;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082750;
    }
    
    @Override
    protected boolean setDefaultKey(final String s) {
        final ContentResolver contentResolver = this.getContext().getContentResolver();
        final String string = Settings.Secure.getString(contentResolver, "emergency_assistance_application");
        if (!TextUtils.isEmpty((CharSequence)s) && !TextUtils.equals((CharSequence)s, (CharSequence)string)) {
            Settings.Secure.putString(contentResolver, "emergency_assistance_application", s);
            return true;
        }
        return false;
    }
}
