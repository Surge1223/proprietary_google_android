package com.android.settings.applications;

import java.util.Iterator;
import android.content.pm.ApplicationInfo;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.os.AsyncTask;

public abstract class AppCounter extends AsyncTask<Void, Void, Integer>
{
    protected final PackageManagerWrapper mPm;
    protected final UserManager mUm;
    
    public AppCounter(final Context context, final PackageManagerWrapper mPm) {
        this.mPm = mPm;
        this.mUm = (UserManager)context.getSystemService("user");
    }
    
    protected Integer doInBackground(final Void... array) {
        int n = 0;
        for (final UserInfo userInfo : this.mUm.getProfiles(UserHandle.myUserId())) {
            final PackageManagerWrapper mPm = this.mPm;
            int n2;
            if (userInfo.isAdmin()) {
                n2 = 4194304;
            }
            else {
                n2 = 0;
            }
            final Iterator<ApplicationInfo> iterator2 = mPm.getInstalledApplicationsAsUser(0x8200 | n2, userInfo.id).iterator();
            while (iterator2.hasNext()) {
                int n3 = n;
                if (this.includeInCount(iterator2.next())) {
                    n3 = n + 1;
                }
                n = n3;
            }
        }
        return n;
    }
    
    void executeInForeground() {
        this.onPostExecute(this.doInBackground(new Void[0]));
    }
    
    protected abstract boolean includeInCount(final ApplicationInfo p0);
    
    protected abstract void onCountComplete(final int p0);
    
    protected void onPostExecute(final Integer n) {
        this.onCountComplete(n);
    }
}
