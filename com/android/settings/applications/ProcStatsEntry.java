package com.android.settings.applications;

import java.io.PrintWriter;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import android.content.pm.PackageManager;
import java.util.Collections;
import com.android.internal.app.procstats.ProcessStats$PackageState;
import android.util.LongSparseArray;
import java.util.Comparator;
import com.android.internal.app.procstats.ProcessStats;
import android.content.pm.PackageManager;
import com.android.internal.app.procstats.ServiceState;
import android.util.Log;
import com.android.internal.app.procstats.ProcessStats$ProcessDataCollection;
import com.android.internal.app.procstats.ProcessState;
import java.util.List;
import android.os.Parcel;
import android.util.ArrayMap;
import java.util.ArrayList;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public final class ProcStatsEntry implements Parcelable
{
    public static final Parcelable.Creator<ProcStatsEntry> CREATOR;
    private static boolean DEBUG;
    final long mAvgBgMem;
    final long mAvgRunMem;
    String mBestTargetPackage;
    final long mBgDuration;
    final double mBgWeight;
    public CharSequence mLabel;
    final long mMaxBgMem;
    final long mMaxRunMem;
    final String mName;
    final String mPackage;
    final ArrayList<String> mPackages;
    final long mRunDuration;
    final double mRunWeight;
    ArrayMap<String, ArrayList<Service>> mServices;
    final int mUid;
    
    static {
        ProcStatsEntry.DEBUG = false;
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<ProcStatsEntry>() {
            public ProcStatsEntry createFromParcel(final Parcel parcel) {
                return new ProcStatsEntry(parcel);
            }
            
            public ProcStatsEntry[] newArray(final int n) {
                return new ProcStatsEntry[n];
            }
        };
    }
    
    public ProcStatsEntry(final Parcel parcel) {
        this.mPackages = new ArrayList<String>();
        this.mServices = (ArrayMap<String, ArrayList<Service>>)new ArrayMap(1);
        this.mPackage = parcel.readString();
        this.mUid = parcel.readInt();
        this.mName = parcel.readString();
        parcel.readStringList((List)this.mPackages);
        this.mBgDuration = parcel.readLong();
        this.mAvgBgMem = parcel.readLong();
        this.mMaxBgMem = parcel.readLong();
        this.mBgWeight = parcel.readDouble();
        this.mRunDuration = parcel.readLong();
        this.mAvgRunMem = parcel.readLong();
        this.mMaxRunMem = parcel.readLong();
        this.mRunWeight = parcel.readDouble();
        this.mBestTargetPackage = parcel.readString();
        final int int1 = parcel.readInt();
        if (int1 > 0) {
            this.mServices.ensureCapacity(int1);
            for (int i = 0; i < int1; ++i) {
                final String string = parcel.readString();
                final ArrayList list = new ArrayList();
                parcel.readTypedList((List)list, (Parcelable.Creator)Service.CREATOR);
                this.mServices.append((Object)string, (Object)list);
            }
        }
    }
    
    public ProcStatsEntry(final ProcessState processState, final String s, final ProcessStats$ProcessDataCollection collection, final ProcessStats$ProcessDataCollection collection2, final boolean b) {
        this.mPackages = new ArrayList<String>();
        this.mServices = (ArrayMap<String, ArrayList<Service>>)new ArrayMap(1);
        processState.computeProcessData(collection, 0L);
        processState.computeProcessData(collection2, 0L);
        this.mPackage = processState.getPackage();
        this.mUid = processState.getUid();
        this.mName = processState.getName();
        this.mPackages.add(s);
        this.mBgDuration = collection.totalTime;
        long mAvgBgMem;
        if (b) {
            mAvgBgMem = collection.avgUss;
        }
        else {
            mAvgBgMem = collection.avgPss;
        }
        this.mAvgBgMem = mAvgBgMem;
        long mMaxBgMem;
        if (b) {
            mMaxBgMem = collection.maxUss;
        }
        else {
            mMaxBgMem = collection.maxPss;
        }
        this.mMaxBgMem = mMaxBgMem;
        this.mBgWeight = this.mAvgBgMem * this.mBgDuration;
        this.mRunDuration = collection2.totalTime;
        long mAvgRunMem;
        if (b) {
            mAvgRunMem = collection2.avgUss;
        }
        else {
            mAvgRunMem = collection2.avgPss;
        }
        this.mAvgRunMem = mAvgRunMem;
        long mMaxRunMem;
        if (b) {
            mMaxRunMem = collection2.maxUss;
        }
        else {
            mMaxRunMem = collection2.maxPss;
        }
        this.mMaxRunMem = mMaxRunMem;
        this.mRunWeight = this.mAvgRunMem * this.mRunDuration;
        if (ProcStatsEntry.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("New proc entry ");
            sb.append(processState.getName());
            sb.append(": dur=");
            sb.append(this.mBgDuration);
            sb.append(" avgpss=");
            sb.append(this.mAvgBgMem);
            sb.append(" weight=");
            sb.append(this.mBgWeight);
            Log.d("ProcStatsEntry", sb.toString());
        }
    }
    
    public ProcStatsEntry(final String mPackage, final int mUid, final String mName, final long n, final long n2, final long n3) {
        this.mPackages = new ArrayList<String>();
        this.mServices = (ArrayMap<String, ArrayList<Service>>)new ArrayMap(1);
        this.mPackage = mPackage;
        this.mUid = mUid;
        this.mName = mName;
        this.mRunDuration = n;
        this.mBgDuration = n;
        this.mMaxRunMem = n2;
        this.mAvgRunMem = n2;
        this.mMaxBgMem = n2;
        this.mAvgBgMem = n2;
        final double n4 = n3 * n2;
        this.mRunWeight = n4;
        this.mBgWeight = n4;
        if (ProcStatsEntry.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("New proc entry ");
            sb.append(mName);
            sb.append(": dur=");
            sb.append(this.mBgDuration);
            sb.append(" avgpss=");
            sb.append(this.mAvgBgMem);
            sb.append(" weight=");
            sb.append(this.mBgWeight);
            Log.d("ProcStatsEntry", sb.toString());
        }
    }
    
    public void addPackage(final String s) {
        this.mPackages.add(s);
    }
    
    public void addService(final ServiceState serviceState) {
        ArrayList<Service> list;
        if ((list = (ArrayList<Service>)this.mServices.get((Object)serviceState.getPackage())) == null) {
            list = new ArrayList<Service>();
            this.mServices.put((Object)serviceState.getPackage(), (Object)list);
        }
        list.add(new Service(serviceState));
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void evaluateTargetPackage(final PackageManager packageManager, final ProcessStats processStats, final ProcessStats$ProcessDataCollection collection, ProcessStats$ProcessDataCollection collection2, final Comparator<ProcStatsEntry> comparator, final boolean b) {
        this.mBestTargetPackage = null;
        if (this.mPackages.size() == 1) {
            if (ProcStatsEntry.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Eval pkg of ");
                sb.append(this.mName);
                sb.append(": single pkg ");
                sb.append(this.mPackages.get(0));
                Log.d("ProcStatsEntry", sb.toString());
            }
            this.mBestTargetPackage = this.mPackages.get(0);
            return;
        }
        for (int i = 0; i < this.mPackages.size(); ++i) {
            if ("android".equals(this.mPackages.get(i))) {
                this.mBestTargetPackage = this.mPackages.get(i);
                return;
            }
        }
        final ArrayList<Object> list = new ArrayList<Object>();
        for (int j = 0; j < this.mPackages.size(); ++j) {
            final LongSparseArray longSparseArray = (LongSparseArray)processStats.mPackages.get((String)this.mPackages.get(j), this.mUid);
            for (int k = 0; k < longSparseArray.size(); ++k) {
                final ProcessStats$PackageState processStats$PackageState = (ProcessStats$PackageState)longSparseArray.valueAt(k);
                if (ProcStatsEntry.DEBUG) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Eval pkg of ");
                    sb2.append(this.mName);
                    sb2.append(", pkg ");
                    sb2.append(processStats$PackageState);
                    sb2.append(":");
                    Log.d("ProcStatsEntry", sb2.toString());
                }
                if (processStats$PackageState == null) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("No package state found for ");
                    sb3.append(this.mPackages.get(j));
                    sb3.append("/");
                    sb3.append(this.mUid);
                    sb3.append(" in process ");
                    sb3.append(this.mName);
                    Log.w("ProcStatsEntry", sb3.toString());
                }
                else {
                    final ProcessState processState = (ProcessState)processStats$PackageState.mProcesses.get((Object)this.mName);
                    if (processState == null) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("No process ");
                        sb4.append(this.mName);
                        sb4.append(" found in package state ");
                        sb4.append(this.mPackages.get(j));
                        sb4.append("/");
                        sb4.append(this.mUid);
                        Log.w("ProcStatsEntry", sb4.toString());
                    }
                    else {
                        list.add(new ProcStatsEntry(processState, processStats$PackageState.mPackageName, collection, collection2, b));
                    }
                }
            }
        }
        if (list.size() > 1) {
            Collections.sort(list, (Comparator<? super Object>)comparator);
            if (((ProcStatsEntry)list.get(0)).mRunWeight > ((ProcStatsEntry)list.get(1)).mRunWeight * 3.0) {
                if (ProcStatsEntry.DEBUG) {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("Eval pkg of ");
                    sb5.append(this.mName);
                    sb5.append(": best pkg ");
                    sb5.append(list.get(0).mPackage);
                    sb5.append(" weight ");
                    sb5.append(list.get(0).mRunWeight);
                    sb5.append(" better than ");
                    sb5.append(list.get(1).mPackage);
                    sb5.append(" weight ");
                    sb5.append(list.get(1).mRunWeight);
                    Log.d("ProcStatsEntry", sb5.toString());
                }
                this.mBestTargetPackage = ((ProcStatsEntry)list.get(0)).mPackage;
                return;
            }
            double mRunWeight = list.get(0).mRunWeight;
            long n = -1L;
            int n2 = 0;
            int n3;
            long n4;
            for (int l = 0; l < list.size(); ++l, n2 = n3, n = n4) {
                collection2 = (ProcessStats$ProcessDataCollection)list.get(l);
                Label_1228: {
                    if (((ProcStatsEntry)collection2).mRunWeight < mRunWeight / 2.0) {
                        n3 = n2;
                        n4 = n;
                        if (ProcStatsEntry.DEBUG) {
                            final StringBuilder sb6 = new StringBuilder();
                            sb6.append("Eval pkg of ");
                            sb6.append(this.mName);
                            sb6.append(": pkg ");
                            sb6.append(((ProcStatsEntry)collection2).mPackage);
                            sb6.append(" weight ");
                            sb6.append(((ProcStatsEntry)collection2).mRunWeight);
                            sb6.append(" too small");
                            Log.d("ProcStatsEntry", sb6.toString());
                            n3 = n2;
                            n4 = n;
                            break Label_1228;
                        }
                        break Label_1228;
                    }
                    try {
                        final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(((ProcStatsEntry)collection2).mPackage, 0);
                        if (applicationInfo.icon == 0) {
                            try {
                                if (ProcStatsEntry.DEBUG) {
                                    final StringBuilder sb7 = new StringBuilder();
                                    sb7.append("Eval pkg of ");
                                    sb7.append(this.mName);
                                    sb7.append(": pkg ");
                                    sb7.append(((ProcStatsEntry)collection2).mPackage);
                                    sb7.append(" has no icon");
                                    Log.d("ProcStatsEntry", sb7.toString());
                                }
                                n3 = n2;
                                n4 = n;
                                break Label_1228;
                            }
                            catch (PackageManager$NameNotFoundException ex) {
                                break Label_1228;
                            }
                        }
                        if ((applicationInfo.flags & 0x8) != 0x0) {
                            final long mRunDuration = ((ProcStatsEntry)collection2).mRunDuration;
                            if (n2 != 0 && mRunDuration <= n) {
                                if (ProcStatsEntry.DEBUG) {
                                    final StringBuilder sb8 = new StringBuilder();
                                    sb8.append("Eval pkg of ");
                                    sb8.append(this.mName);
                                    sb8.append(": pkg ");
                                    sb8.append(((ProcStatsEntry)collection2).mPackage);
                                    sb8.append(" pers run time ");
                                    sb8.append(mRunDuration);
                                    sb8.append(" not as good as last ");
                                    sb8.append(n);
                                    Log.d("ProcStatsEntry", sb8.toString());
                                }
                            }
                            else {
                                if (ProcStatsEntry.DEBUG) {
                                    final StringBuilder sb9 = new StringBuilder();
                                    sb9.append("Eval pkg of ");
                                    sb9.append(this.mName);
                                    sb9.append(": pkg ");
                                    sb9.append(((ProcStatsEntry)collection2).mPackage);
                                    sb9.append(" new best pers run time ");
                                    sb9.append(mRunDuration);
                                    Log.d("ProcStatsEntry", sb9.toString());
                                }
                                n = mRunDuration;
                                n2 = 1;
                            }
                            n4 = n;
                            n3 = n2;
                        }
                        else if (n2 != 0) {
                            if (ProcStatsEntry.DEBUG) {
                                final StringBuilder sb10 = new StringBuilder();
                                sb10.append("Eval pkg of ");
                                sb10.append(this.mName);
                                sb10.append(": pkg ");
                                sb10.append(((ProcStatsEntry)collection2).mPackage);
                                sb10.append(" is not persistent");
                                Log.d("ProcStatsEntry", sb10.toString());
                            }
                            n3 = n2;
                            n4 = n;
                        }
                        else {
                            ArrayList<Service> list2 = null;
                            for (int n5 = 0; n5 < this.mServices.size(); ++n5) {
                                final ArrayList list3 = (ArrayList)this.mServices.valueAt(n5);
                                if (list3.get(0).mPackage.equals(((ProcStatsEntry)collection2).mPackage)) {
                                    list2 = (ArrayList<Service>)list3;
                                    break;
                                }
                            }
                            double n6 = mRunWeight;
                            long mDuration = 0L;
                            Label_1548: {
                                if (list2 != null) {
                                    int n7 = 0;
                                    final int size = list2.size();
                                    while (true) {
                                        n6 = mRunWeight;
                                        if (n7 >= size) {
                                            break;
                                        }
                                        final Service service = list2.get(n7);
                                        if (service.mDuration > 0L) {
                                            if (ProcStatsEntry.DEBUG) {
                                                final StringBuilder sb11 = new StringBuilder();
                                                sb11.append("Eval pkg of ");
                                                sb11.append(this.mName);
                                                sb11.append(": pkg ");
                                                sb11.append(((ProcStatsEntry)collection2).mPackage);
                                                sb11.append(" service ");
                                                sb11.append(service.mName);
                                                sb11.append(" run time is ");
                                                sb11.append(service.mDuration);
                                                Log.d("ProcStatsEntry", sb11.toString());
                                            }
                                            mDuration = service.mDuration;
                                            n6 = mRunWeight;
                                            break Label_1548;
                                        }
                                        ++n7;
                                    }
                                }
                                mDuration = 0L;
                            }
                            if (mDuration > n) {
                                if (ProcStatsEntry.DEBUG) {
                                    final StringBuilder sb12 = new StringBuilder();
                                    sb12.append("Eval pkg of ");
                                    sb12.append(this.mName);
                                    sb12.append(": pkg ");
                                    sb12.append(((ProcStatsEntry)collection2).mPackage);
                                    sb12.append(" new best run time ");
                                    sb12.append(mDuration);
                                    Log.d("ProcStatsEntry", sb12.toString());
                                }
                                this.mBestTargetPackage = ((ProcStatsEntry)collection2).mPackage;
                                n4 = mDuration;
                                n3 = n2;
                                mRunWeight = n6;
                            }
                            else {
                                n3 = n2;
                                n4 = n;
                                mRunWeight = n6;
                                if (ProcStatsEntry.DEBUG) {
                                    final StringBuilder sb13 = new StringBuilder();
                                    sb13.append("Eval pkg of ");
                                    sb13.append(this.mName);
                                    sb13.append(": pkg ");
                                    sb13.append(((ProcStatsEntry)collection2).mPackage);
                                    sb13.append(" run time ");
                                    sb13.append(mDuration);
                                    sb13.append(" not as good as last ");
                                    sb13.append(n);
                                    Log.d("ProcStatsEntry", sb13.toString());
                                    n3 = n2;
                                    n4 = n;
                                    mRunWeight = n6;
                                }
                            }
                        }
                        continue;
                    }
                    catch (PackageManager$NameNotFoundException ex2) {}
                }
                if (ProcStatsEntry.DEBUG) {
                    final StringBuilder sb14 = new StringBuilder();
                    sb14.append("Eval pkg of ");
                    sb14.append(this.mName);
                    sb14.append(": pkg ");
                    sb14.append(((ProcStatsEntry)collection2).mPackage);
                    sb14.append(" failed finding app info");
                    Log.d("ProcStatsEntry", sb14.toString());
                }
                n4 = n;
                n3 = n2;
            }
            if (TextUtils.isEmpty((CharSequence)this.mBestTargetPackage)) {
                this.mBestTargetPackage = ((ProcStatsEntry)list.get(0)).mPackage;
            }
        }
        else if (list.size() == 1) {
            this.mBestTargetPackage = ((ProcStatsEntry)list.get(0)).mPackage;
        }
    }
    
    public int getUid() {
        return this.mUid;
    }
    
    public void writeToParcel(final Parcel parcel, int i) {
        parcel.writeString(this.mPackage);
        parcel.writeInt(this.mUid);
        parcel.writeString(this.mName);
        parcel.writeStringList((List)this.mPackages);
        parcel.writeLong(this.mBgDuration);
        parcel.writeLong(this.mAvgBgMem);
        parcel.writeLong(this.mMaxBgMem);
        parcel.writeDouble(this.mBgWeight);
        parcel.writeLong(this.mRunDuration);
        parcel.writeLong(this.mAvgRunMem);
        parcel.writeLong(this.mMaxRunMem);
        parcel.writeDouble(this.mRunWeight);
        parcel.writeString(this.mBestTargetPackage);
        final int size = this.mServices.size();
        parcel.writeInt(size);
        for (i = 0; i < size; ++i) {
            parcel.writeString((String)this.mServices.keyAt(i));
            parcel.writeTypedList((List)this.mServices.valueAt(i));
        }
    }
    
    public static final class Service implements Parcelable
    {
        public static final Parcelable.Creator<Service> CREATOR;
        final long mDuration;
        final String mName;
        final String mPackage;
        final String mProcess;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<Service>() {
                public Service createFromParcel(final Parcel parcel) {
                    return new Service(parcel);
                }
                
                public Service[] newArray(final int n) {
                    return new Service[n];
                }
            };
        }
        
        public Service(final Parcel parcel) {
            this.mPackage = parcel.readString();
            this.mName = parcel.readString();
            this.mProcess = parcel.readString();
            this.mDuration = parcel.readLong();
        }
        
        public Service(final ServiceState serviceState) {
            this.mPackage = serviceState.getPackage();
            this.mName = serviceState.getName();
            this.mProcess = serviceState.getProcessName();
            this.mDuration = serviceState.dumpTime((PrintWriter)null, (String)null, 0, -1, 0L, 0L);
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeString(this.mPackage);
            parcel.writeString(this.mName);
            parcel.writeString(this.mProcess);
            parcel.writeLong(this.mDuration);
        }
    }
}
