package com.android.settings.applications;

import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settings.datausage.DataSaverBackend;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class SpecialAppAccessPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private DataSaverBackend mDataSaverBackend;
    
    public SpecialAppAccessPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public String getPreferenceKey() {
        return "special_access";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mDataSaverBackend == null) {
            this.mDataSaverBackend = new DataSaverBackend(this.mContext);
        }
        final int whitelistedCount = this.mDataSaverBackend.getWhitelistedCount();
        preference.setSummary(this.mContext.getResources().getQuantityString(2131755064, whitelistedCount, new Object[] { whitelistedCount }));
    }
}
