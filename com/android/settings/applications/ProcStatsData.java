package com.android.settings.applications;

import com.android.internal.util.MemInfoReader;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import java.io.PrintWriter;
import com.android.internal.app.procstats.DumpUtils;
import android.os.SystemClock;
import java.util.List;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.IOException;
import java.io.InputStream;
import android.os.ParcelFileDescriptor$AutoCloseInputStream;
import com.android.internal.app.procstats.ServiceState;
import android.util.Log;
import com.android.internal.app.procstats.ProcessState;
import com.android.internal.app.procstats.ProcessStats$PackageState;
import android.util.LongSparseArray;
import android.util.SparseArray;
import com.android.internal.app.ProcessMap;
import android.util.ArrayMap;
import com.android.internal.app.procstats.ProcessStats$TotalMemoryUseCollection;
import com.android.internal.app.procstats.ProcessStats$ProcessDataCollection;
import com.android.internal.app.procstats.IProcessStats$Stub;
import android.os.ServiceManager;
import java.util.ArrayList;
import com.android.internal.app.procstats.IProcessStats;
import android.content.pm.PackageManager;
import android.content.Context;
import com.android.internal.app.procstats.ProcessStats;
import java.util.Comparator;

public class ProcStatsData
{
    static final Comparator<ProcStatsEntry> sEntryCompare;
    private static ProcessStats sStatsXfer;
    private Context mContext;
    private long mDuration;
    private MemInfo mMemInfo;
    private int[] mMemStates;
    private PackageManager mPm;
    private IProcessStats mProcessStats;
    private int[] mStates;
    private ProcessStats mStats;
    private boolean mUseUss;
    private long memTotalTime;
    private ArrayList<ProcStatsPackageEntry> pkgEntries;
    
    static {
        sEntryCompare = new Comparator<ProcStatsEntry>() {
            @Override
            public int compare(final ProcStatsEntry procStatsEntry, final ProcStatsEntry procStatsEntry2) {
                if (procStatsEntry.mRunWeight < procStatsEntry2.mRunWeight) {
                    return 1;
                }
                if (procStatsEntry.mRunWeight > procStatsEntry2.mRunWeight) {
                    return -1;
                }
                if (procStatsEntry.mRunDuration < procStatsEntry2.mRunDuration) {
                    return 1;
                }
                if (procStatsEntry.mRunDuration > procStatsEntry2.mRunDuration) {
                    return -1;
                }
                return 0;
            }
        };
    }
    
    public ProcStatsData(final Context mContext, final boolean b) {
        this.mContext = mContext;
        this.mPm = mContext.getPackageManager();
        this.mProcessStats = IProcessStats$Stub.asInterface(ServiceManager.getService("procstats"));
        this.mMemStates = ProcessStats.ALL_MEM_ADJ;
        this.mStates = ProcessStats.BACKGROUND_PROC_STATES;
        if (b) {
            this.mStats = ProcStatsData.sStatsXfer;
        }
    }
    
    private ProcStatsPackageEntry createOsEntry(final ProcessStats$ProcessDataCollection collection, final ProcessStats$ProcessDataCollection collection2, final ProcessStats$TotalMemoryUseCollection collection3, final long n) {
        final ProcStatsPackageEntry procStatsPackageEntry = new ProcStatsPackageEntry("os", this.memTotalTime);
        if (collection3.sysMemNativeWeight > 0.0) {
            final ProcStatsEntry procStatsEntry = new ProcStatsEntry("os", 0, this.mContext.getString(2131888685), this.memTotalTime, (long)(collection3.sysMemNativeWeight / this.memTotalTime), this.memTotalTime);
            procStatsEntry.evaluateTargetPackage(this.mPm, this.mStats, collection, collection2, ProcStatsData.sEntryCompare, this.mUseUss);
            procStatsPackageEntry.addEntry(procStatsEntry);
        }
        if (collection3.sysMemKernelWeight > 0.0) {
            final ProcStatsEntry procStatsEntry2 = new ProcStatsEntry("os", 0, this.mContext.getString(2131888683), this.memTotalTime, (long)(collection3.sysMemKernelWeight / this.memTotalTime), this.memTotalTime);
            procStatsEntry2.evaluateTargetPackage(this.mPm, this.mStats, collection, collection2, ProcStatsData.sEntryCompare, this.mUseUss);
            procStatsPackageEntry.addEntry(procStatsEntry2);
        }
        if (n > 0L) {
            final ProcStatsEntry procStatsEntry3 = new ProcStatsEntry("os", 0, this.mContext.getString(2131888682), this.memTotalTime, n / 1024L, this.memTotalTime);
            procStatsEntry3.evaluateTargetPackage(this.mPm, this.mStats, collection, collection2, ProcStatsData.sEntryCompare, this.mUseUss);
            procStatsPackageEntry.addEntry(procStatsEntry3);
        }
        return procStatsPackageEntry;
    }
    
    private void createPkgMap(final ArrayList<ProcStatsEntry> list, final ProcessStats$ProcessDataCollection collection, final ProcessStats$ProcessDataCollection collection2) {
        final ArrayMap arrayMap = new ArrayMap();
        for (int i = list.size() - 1; i >= 0; --i) {
            final ProcStatsEntry procStatsEntry = list.get(i);
            procStatsEntry.evaluateTargetPackage(this.mPm, this.mStats, collection, collection2, ProcStatsData.sEntryCompare, this.mUseUss);
            ProcStatsPackageEntry procStatsPackageEntry;
            if ((procStatsPackageEntry = (ProcStatsPackageEntry)arrayMap.get((Object)procStatsEntry.mBestTargetPackage)) == null) {
                procStatsPackageEntry = new ProcStatsPackageEntry(procStatsEntry.mBestTargetPackage, this.memTotalTime);
                arrayMap.put((Object)procStatsEntry.mBestTargetPackage, (Object)procStatsPackageEntry);
                this.pkgEntries.add(procStatsPackageEntry);
            }
            procStatsPackageEntry.addEntry(procStatsEntry);
        }
    }
    
    private void distributeZRam(final double n) {
        long n2 = (long)(n / this.memTotalTime);
        long n3 = 0L;
        for (int i = this.pkgEntries.size() - 1; i >= 0; --i) {
            final ProcStatsPackageEntry procStatsPackageEntry = this.pkgEntries.get(i);
            for (int j = procStatsPackageEntry.mEntries.size() - 1; j >= 0; --j) {
                n3 += procStatsPackageEntry.mEntries.get(j).mRunDuration;
            }
        }
        long n8;
        long n9;
        for (int n4 = this.pkgEntries.size() - 1; n4 >= 0 && n3 > 0L; --n4, n2 = n8, n3 = n9) {
            final ProcStatsPackageEntry procStatsPackageEntry2 = this.pkgEntries.get(n4);
            long n5 = 0L;
            long n6 = 0L;
            long mRunDuration;
            for (int k = procStatsPackageEntry2.mEntries.size() - 1; k >= 0; --k, n6 = mRunDuration) {
                final ProcStatsEntry procStatsEntry = procStatsPackageEntry2.mEntries.get(k);
                n5 += procStatsEntry.mRunDuration;
                mRunDuration = n6;
                if (procStatsEntry.mRunDuration > n6) {
                    mRunDuration = procStatsEntry.mRunDuration;
                }
            }
            final long n7 = n2 * n5 / n3;
            n8 = n2;
            n9 = n3;
            if (n7 > 0L) {
                n9 = n3 - n5;
                final ProcStatsEntry procStatsEntry2 = new ProcStatsEntry(procStatsPackageEntry2.mPackage, 0, this.mContext.getString(2131888686), n6, n7, this.memTotalTime);
                procStatsEntry2.evaluateTargetPackage(this.mPm, this.mStats, null, null, ProcStatsData.sEntryCompare, this.mUseUss);
                procStatsPackageEntry2.addEntry(procStatsEntry2);
                n8 = n2 - n7;
            }
        }
    }
    
    private ArrayList<ProcStatsEntry> getProcs(final ProcessStats$ProcessDataCollection collection, final ProcessStats$ProcessDataCollection collection2) {
        final ArrayList<ProcStatsEntry> list = new ArrayList<ProcStatsEntry>();
        final ProcessMap processMap = new ProcessMap();
        for (int i = 0; i < this.mStats.mPackages.getMap().size(); ++i) {
            final SparseArray sparseArray = (SparseArray)this.mStats.mPackages.getMap().valueAt(i);
            for (int j = 0; j < sparseArray.size(); ++j) {
                final LongSparseArray longSparseArray = (LongSparseArray)sparseArray.valueAt(j);
                for (int k = 0; k < longSparseArray.size(); ++k) {
                    final ProcessStats$PackageState processStats$PackageState = (ProcessStats$PackageState)longSparseArray.valueAt(k);
                    for (int l = 0; l < processStats$PackageState.mProcesses.size(); ++l) {
                        final ProcessState processState = (ProcessState)processStats$PackageState.mProcesses.valueAt(l);
                        final ProcessState processState2 = (ProcessState)this.mStats.mProcesses.get(processState.getName(), processState.getUid());
                        if (processState2 == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No process found for pkg ");
                            sb.append(processStats$PackageState.mPackageName);
                            sb.append("/");
                            sb.append(processStats$PackageState.mUid);
                            sb.append(" proc name ");
                            sb.append(processState.getName());
                            Log.w("ProcStatsManager", sb.toString());
                        }
                        else {
                            final ProcStatsEntry procStatsEntry = (ProcStatsEntry)processMap.get(processState2.getName(), processState2.getUid());
                            if (procStatsEntry == null) {
                                final ProcStatsEntry procStatsEntry2 = new ProcStatsEntry(processState2, processStats$PackageState.mPackageName, collection, collection2, this.mUseUss);
                                if (procStatsEntry2.mRunWeight > 0.0) {
                                    processMap.put(processState2.getName(), processState2.getUid(), (Object)procStatsEntry2);
                                    list.add(procStatsEntry2);
                                }
                            }
                            else {
                                procStatsEntry.addPackage(processStats$PackageState.mPackageName);
                            }
                        }
                    }
                }
            }
        }
        for (int n = 0; n < this.mStats.mPackages.getMap().size(); ++n) {
            final SparseArray sparseArray2 = (SparseArray)this.mStats.mPackages.getMap().valueAt(n);
            for (int n2 = 0; n2 < sparseArray2.size(); ++n2) {
                final LongSparseArray longSparseArray2 = (LongSparseArray)sparseArray2.valueAt(n2);
                for (int n3 = 0; n3 < longSparseArray2.size(); ++n3) {
                    final ProcessStats$PackageState processStats$PackageState2 = (ProcessStats$PackageState)longSparseArray2.valueAt(n3);
                    for (int n4 = 0; n4 < processStats$PackageState2.mServices.size(); ++n4) {
                        final ServiceState serviceState = (ServiceState)processStats$PackageState2.mServices.valueAt(n4);
                        if (serviceState.getProcessName() != null) {
                            final ProcStatsEntry procStatsEntry3 = (ProcStatsEntry)processMap.get(serviceState.getProcessName(), sparseArray2.keyAt(n2));
                            if (procStatsEntry3 != null) {
                                procStatsEntry3.addService(serviceState);
                            }
                            else {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("No process ");
                                sb2.append(serviceState.getProcessName());
                                sb2.append("/");
                                sb2.append(sparseArray2.keyAt(n2));
                                sb2.append(" for service ");
                                sb2.append(serviceState.getName());
                                Log.w("ProcStatsManager", sb2.toString());
                            }
                        }
                    }
                }
            }
        }
        return list;
    }
    
    private void load() {
        try {
            final ParcelFileDescriptor statsOverTime = this.mProcessStats.getStatsOverTime(this.mDuration);
            this.mStats = new ProcessStats(false);
            final ParcelFileDescriptor$AutoCloseInputStream parcelFileDescriptor$AutoCloseInputStream = new ParcelFileDescriptor$AutoCloseInputStream(statsOverTime);
            this.mStats.read((InputStream)parcelFileDescriptor$AutoCloseInputStream);
            try {
                ((InputStream)parcelFileDescriptor$AutoCloseInputStream).close();
            }
            catch (IOException ex2) {}
            if (this.mStats.mReadError != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failure reading process stats: ");
                sb.append(this.mStats.mReadError);
                Log.w("ProcStatsManager", sb.toString());
            }
        }
        catch (RemoteException ex) {
            Log.e("ProcStatsManager", "RemoteException:", (Throwable)ex);
        }
    }
    
    public long getDuration() {
        return this.mDuration;
    }
    
    public List<ProcStatsPackageEntry> getEntries() {
        return this.pkgEntries;
    }
    
    public MemInfo getMemInfo() {
        return this.mMemInfo;
    }
    
    public int getMemState() {
        final int mMemFactor = this.mStats.mMemFactor;
        if (mMemFactor == -1) {
            return 0;
        }
        int n;
        if ((n = mMemFactor) >= 4) {
            n = mMemFactor - 4;
        }
        return n;
    }
    
    public void refreshStats(final boolean b) {
        if (this.mStats == null || b) {
            this.load();
        }
        this.pkgEntries = new ArrayList<ProcStatsPackageEntry>();
        final long uptimeMillis = SystemClock.uptimeMillis();
        this.memTotalTime = DumpUtils.dumpSingleTime((PrintWriter)null, (String)null, this.mStats.mMemFactorDurations, this.mStats.mMemFactor, this.mStats.mStartTime, uptimeMillis);
        final ProcessStats$TotalMemoryUseCollection collection = new ProcessStats$TotalMemoryUseCollection(ProcessStats.ALL_SCREEN_ADJ, this.mMemStates);
        this.mStats.computeTotalMemoryUse(collection, uptimeMillis);
        this.mMemInfo = new MemInfo(this.mContext, collection, this.memTotalTime);
        final ProcessStats$ProcessDataCollection collection2 = new ProcessStats$ProcessDataCollection(ProcessStats.ALL_SCREEN_ADJ, this.mMemStates, this.mStates);
        final ProcessStats$ProcessDataCollection collection3 = new ProcessStats$ProcessDataCollection(ProcessStats.ALL_SCREEN_ADJ, this.mMemStates, ProcessStats.NON_CACHED_PROC_STATES);
        this.createPkgMap(this.getProcs(collection2, collection3), collection2, collection3);
        if (collection.sysMemZRamWeight > 0.0 && !collection.hasSwappedOutPss) {
            this.distributeZRam(collection.sysMemZRamWeight);
        }
        this.pkgEntries.add(this.createOsEntry(collection2, collection3, collection, this.mMemInfo.baseCacheRam));
    }
    
    public void setDuration(final long mDuration) {
        if (mDuration != this.mDuration) {
            this.mDuration = mDuration;
            this.refreshStats(true);
        }
    }
    
    public void xferStats() {
        ProcStatsData.sStatsXfer = this.mStats;
    }
    
    public static class MemInfo
    {
        long baseCacheRam;
        double freeWeight;
        double[] mMemStateWeights;
        long memTotalTime;
        public double realFreeRam;
        public double realTotalRam;
        public double realUsedRam;
        double totalRam;
        double totalScale;
        double usedWeight;
        double weightToRam;
        
        private MemInfo(final Context context, final ProcessStats$TotalMemoryUseCollection collection, final long memTotalTime) {
            this.mMemStateWeights = new double[14];
            this.calculateWeightInfo(context, collection, this.memTotalTime = memTotalTime);
            final double n = this.usedWeight * 1024.0 / memTotalTime;
            final double realUsedRam = this.freeWeight * 1024.0 / memTotalTime;
            this.totalRam = n + realUsedRam;
            this.totalScale = this.realTotalRam / this.totalRam;
            this.weightToRam = this.totalScale / memTotalTime * 1024.0;
            this.realUsedRam = this.totalScale * n;
            this.realFreeRam = this.totalScale * realUsedRam;
            final ActivityManager.MemoryInfo activityManager$MemoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager)context.getSystemService("activity")).getMemoryInfo(activityManager$MemoryInfo);
            if (activityManager$MemoryInfo.hiddenAppThreshold >= this.realFreeRam) {
                this.realUsedRam = realUsedRam;
                this.realFreeRam = 0.0;
                this.baseCacheRam = (long)this.realFreeRam;
            }
            else {
                this.realUsedRam += activityManager$MemoryInfo.hiddenAppThreshold;
                this.realFreeRam -= activityManager$MemoryInfo.hiddenAppThreshold;
                this.baseCacheRam = activityManager$MemoryInfo.hiddenAppThreshold;
            }
        }
        
        private void calculateWeightInfo(final Context context, final ProcessStats$TotalMemoryUseCollection collection, final long n) {
            final MemInfoReader memInfoReader = new MemInfoReader();
            memInfoReader.readMemInfo();
            this.realTotalRam = memInfoReader.getTotalSize();
            this.freeWeight = collection.sysMemFreeWeight + collection.sysMemCachedWeight;
            this.usedWeight = collection.sysMemKernelWeight + collection.sysMemNativeWeight;
            if (!collection.hasSwappedOutPss) {
                this.usedWeight += collection.sysMemZRamWeight;
            }
            for (int i = 0; i < 14; ++i) {
                if (i == 6) {
                    this.mMemStateWeights[i] = 0.0;
                }
                else {
                    this.mMemStateWeights[i] = collection.processStateWeight[i];
                    if (i >= 9) {
                        this.freeWeight += collection.processStateWeight[i];
                    }
                    else {
                        this.usedWeight += collection.processStateWeight[i];
                    }
                }
            }
        }
        
        public double getWeightToRam() {
            return this.weightToRam;
        }
    }
}
