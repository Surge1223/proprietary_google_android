package com.android.settings.applications;

import android.app.Activity;
import android.support.v7.preference.Preference;
import com.android.settingslib.applications.AppUtils;
import android.util.IconDrawableFactory;
import android.view.View;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.util.Log;
import android.os.Bundle;

public abstract class AppInfoWithHeader extends AppInfoBase
{
    private boolean mCreated;
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        if (this.mCreated) {
            Log.w(AppInfoWithHeader.TAG, "onActivityCreated: ignoring duplicate call");
            return;
        }
        this.mCreated = true;
        if (this.mPackageInfo == null) {
            return;
        }
        final Activity activity = this.getActivity();
        this.getPreferenceScreen().addPreference(EntityHeaderController.newInstance(activity, this, null).setRecyclerView(this.getListView(), this.getLifecycle()).setIcon(IconDrawableFactory.newInstance(this.getContext()).getBadgedIcon(this.mPackageInfo.applicationInfo)).setLabel(this.mPackageInfo.applicationInfo.loadLabel(this.mPm)).setSummary(this.mPackageInfo).setIsInstantApp(AppUtils.isInstant(this.mPackageInfo.applicationInfo)).setPackageName(this.mPackageName).setUid(this.mPackageInfo.applicationInfo.uid).setHasAppInfoLink(true).setButtonActions(0, 0).done(activity, this.getPrefContext()));
    }
}
