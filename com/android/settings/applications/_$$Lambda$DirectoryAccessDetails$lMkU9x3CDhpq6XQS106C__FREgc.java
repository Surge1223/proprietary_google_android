package com.android.settings.applications;

import java.util.ArrayList;
import java.util.List;
import android.support.v7.preference.PreferenceScreen;
import java.util.function.Consumer;
import java.util.HashSet;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume$ScopedAccessProviderContract;
import android.net.Uri.Builder;
import java.util.HashMap;
import android.app.Activity;
import com.android.settingslib.applications.AppUtils;
import android.util.IconDrawableFactory;
import android.view.View;
import android.app.Fragment;
import com.android.settings.widget.EntityHeaderController;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.util.Pair;
import android.support.v7.preference.PreferenceCategory;
import java.util.Iterator;
import android.support.v14.preference.SwitchPreference;
import android.util.Log;
import java.util.Set;
import android.net.Uri;
import android.content.Context;
import android.support.v7.preference.Preference;

public final class _$$Lambda$DirectoryAccessDetails$lMkU9x3CDhpq6XQS106C__FREgc implements OnPreferenceChangeListener
{
    @Override
    public final boolean onPreferenceChange(final Preference preference, final Object o) {
        return DirectoryAccessDetails.lambda$newPreference$1(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, preference, o);
    }
}
