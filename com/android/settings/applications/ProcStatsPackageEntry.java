package com.android.settings.applications;

import android.content.pm.PackageManager;
import android.util.Log;
import android.content.pm.PackageManager;
import com.android.settingslib.Utils;
import android.content.Context;
import java.util.List;
import android.os.Parcel;
import android.content.pm.ApplicationInfo;
import java.util.ArrayList;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class ProcStatsPackageEntry implements Parcelable
{
    public static final Parcelable.Creator<ProcStatsPackageEntry> CREATOR;
    private static boolean DEBUG;
    long mAvgBgMem;
    long mAvgRunMem;
    long mBgDuration;
    double mBgWeight;
    final ArrayList<ProcStatsEntry> mEntries;
    long mMaxBgMem;
    long mMaxRunMem;
    final String mPackage;
    long mRunDuration;
    double mRunWeight;
    public String mUiLabel;
    public ApplicationInfo mUiTargetApp;
    private long mWindowLength;
    
    static {
        ProcStatsPackageEntry.DEBUG = false;
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<ProcStatsPackageEntry>() {
            public ProcStatsPackageEntry createFromParcel(final Parcel parcel) {
                return new ProcStatsPackageEntry(parcel);
            }
            
            public ProcStatsPackageEntry[] newArray(final int n) {
                return new ProcStatsPackageEntry[n];
            }
        };
    }
    
    public ProcStatsPackageEntry(final Parcel parcel) {
        this.mEntries = new ArrayList<ProcStatsEntry>();
        this.mPackage = parcel.readString();
        parcel.readTypedList((List)this.mEntries, (Parcelable.Creator)ProcStatsEntry.CREATOR);
        this.mBgDuration = parcel.readLong();
        this.mAvgBgMem = parcel.readLong();
        this.mMaxBgMem = parcel.readLong();
        this.mBgWeight = parcel.readDouble();
        this.mRunDuration = parcel.readLong();
        this.mAvgRunMem = parcel.readLong();
        this.mMaxRunMem = parcel.readLong();
        this.mRunWeight = parcel.readDouble();
    }
    
    public ProcStatsPackageEntry(final String mPackage, final long mWindowLength) {
        this.mEntries = new ArrayList<ProcStatsEntry>();
        this.mPackage = mPackage;
        this.mWindowLength = mWindowLength;
    }
    
    public static CharSequence getFrequency(final float n, final Context context) {
        if (n > 0.95f) {
            return context.getString(2131886296, new Object[] { Utils.formatPercentage((int)(100.0f * n)) });
        }
        if (n > 0.25f) {
            return context.getString(2131889186, new Object[] { Utils.formatPercentage((int)(100.0f * n)) });
        }
        return context.getString(2131888791, new Object[] { Utils.formatPercentage((int)(100.0f * n)) });
    }
    
    public void addEntry(final ProcStatsEntry procStatsEntry) {
        this.mEntries.add(procStatsEntry);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public double getBgWeight() {
        return this.mBgWeight;
    }
    
    public ArrayList<ProcStatsEntry> getEntries() {
        return this.mEntries;
    }
    
    public double getRunWeight() {
        return this.mRunWeight;
    }
    
    public void retrieveUiData(final Context context, final PackageManager packageManager) {
        this.mUiTargetApp = null;
        this.mUiLabel = this.mPackage;
        try {
            if ("os".equals(this.mPackage)) {
                this.mUiTargetApp = packageManager.getApplicationInfo("android", 4227584);
                this.mUiLabel = context.getString(2131888684);
            }
            else {
                this.mUiTargetApp = packageManager.getApplicationInfo(this.mPackage, 4227584);
                this.mUiLabel = this.mUiTargetApp.loadLabel(packageManager).toString();
            }
        }
        catch (PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("could not find package: ");
            sb.append(this.mPackage);
            Log.d("ProcStatsEntry", sb.toString());
        }
    }
    
    public void updateMetrics() {
        this.mMaxBgMem = 0L;
        this.mAvgBgMem = 0L;
        this.mBgDuration = 0L;
        this.mBgWeight = 0.0;
        this.mMaxRunMem = 0L;
        this.mAvgRunMem = 0L;
        this.mRunDuration = 0L;
        this.mRunWeight = 0.0;
        final int size = this.mEntries.size();
        for (int i = 0; i < size; ++i) {
            final ProcStatsEntry procStatsEntry = this.mEntries.get(i);
            this.mBgDuration = Math.max(procStatsEntry.mBgDuration, this.mBgDuration);
            this.mAvgBgMem += procStatsEntry.mAvgBgMem;
            this.mBgWeight += procStatsEntry.mBgWeight;
            this.mRunDuration = Math.max(procStatsEntry.mRunDuration, this.mRunDuration);
            this.mAvgRunMem += procStatsEntry.mAvgRunMem;
            this.mRunWeight += procStatsEntry.mRunWeight;
            this.mMaxBgMem += procStatsEntry.mMaxBgMem;
            this.mMaxRunMem += procStatsEntry.mMaxRunMem;
        }
        this.mAvgBgMem /= size;
        this.mAvgRunMem /= size;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.mPackage);
        parcel.writeTypedList((List)this.mEntries);
        parcel.writeLong(this.mBgDuration);
        parcel.writeLong(this.mAvgBgMem);
        parcel.writeLong(this.mMaxBgMem);
        parcel.writeDouble(this.mBgWeight);
        parcel.writeLong(this.mRunDuration);
        parcel.writeLong(this.mAvgRunMem);
        parcel.writeLong(this.mMaxRunMem);
        parcel.writeDouble(this.mRunWeight);
    }
}
