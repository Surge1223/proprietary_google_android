package com.android.settings.applications;

import android.os.Bundle;
import android.view.View;
import android.app.AlertDialog;
import android.util.Log;
import android.content.pm.ResolveInfo;
import android.util.ArraySet;
import com.android.settings.Utils;
import android.content.IntentFilter;
import android.content.pm.IntentFilterVerificationInfo;
import java.util.List;
import android.os.UserHandle;
import android.net.Uri;
import android.content.pm.PackageManager;
import android.support.v7.preference.DropDownPreference;
import android.content.Intent;
import android.view.View.OnClickListener;
import android.support.v7.preference.Preference;

public class AppLaunchSettings extends AppInfoWithHeader implements OnPreferenceChangeListener, View.OnClickListener
{
    private static final Intent sBrowserIntent;
    private AppDomainsPreference mAppDomainUrls;
    private DropDownPreference mAppLinkState;
    private ClearDefaultsPreference mClearDefaultsPreference;
    private boolean mHasDomainUrls;
    private boolean mIsBrowser;
    private PackageManager mPm;
    
    static {
        sBrowserIntent = new Intent().setAction("android.intent.action.VIEW").addCategory("android.intent.category.BROWSABLE").setData(Uri.parse("http:"));
    }
    
    private void buildStateDropDown() {
        if (this.mIsBrowser) {
            this.mAppLinkState.setShouldDisableView(true);
            this.mAppLinkState.setEnabled(false);
            this.mAppDomainUrls.setShouldDisableView(true);
            this.mAppDomainUrls.setEnabled(false);
        }
        else {
            this.mAppLinkState.setEntries(new CharSequence[] { this.getString(2131886364), this.getString(2131886365), this.getString(2131886366) });
            final DropDownPreference mAppLinkState = this.mAppLinkState;
            final String string = Integer.toString(2);
            final int n = 4;
            mAppLinkState.setEntryValues(new CharSequence[] { string, Integer.toString(4), Integer.toString(3) });
            this.mAppLinkState.setEnabled(this.mHasDomainUrls);
            if (this.mHasDomainUrls) {
                int intentVerificationStatusAsUser = this.mPm.getIntentVerificationStatusAsUser(this.mPackageName, UserHandle.myUserId());
                final DropDownPreference mAppLinkState2 = this.mAppLinkState;
                if (intentVerificationStatusAsUser == 0) {
                    intentVerificationStatusAsUser = n;
                }
                mAppLinkState2.setValue(Integer.toString(intentVerificationStatusAsUser));
                this.mAppLinkState.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(final Preference preference, final Object o) {
                        return AppLaunchSettings.this.updateAppLinkState(Integer.parseInt((String)o));
                    }
                });
            }
        }
    }
    
    private CharSequence[] getEntries(final String s, final List<IntentFilterVerificationInfo> list, final List<IntentFilter> list2) {
        final ArraySet<String> handledDomains = Utils.getHandledDomains(this.mPm, s);
        return (CharSequence[])handledDomains.toArray((Object[])new CharSequence[handledDomains.size()]);
    }
    
    private boolean isBrowserApp(final String package1) {
        AppLaunchSettings.sBrowserIntent.setPackage(package1);
        final List queryIntentActivitiesAsUser = this.mPm.queryIntentActivitiesAsUser(AppLaunchSettings.sBrowserIntent, 131072, UserHandle.myUserId());
        for (int size = queryIntentActivitiesAsUser.size(), i = 0; i < size; ++i) {
            final ResolveInfo resolveInfo = queryIntentActivitiesAsUser.get(i);
            if (resolveInfo.activityInfo != null && resolveInfo.handleAllWebDataURI) {
                return true;
            }
        }
        return false;
    }
    
    private boolean updateAppLinkState(final int n) {
        final boolean mIsBrowser = this.mIsBrowser;
        boolean b = false;
        if (mIsBrowser) {
            return false;
        }
        final int myUserId = UserHandle.myUserId();
        if (this.mPm.getIntentVerificationStatusAsUser(this.mPackageName, myUserId) == n) {
            return false;
        }
        final boolean updateIntentVerificationStatusAsUser = this.mPm.updateIntentVerificationStatusAsUser(this.mPackageName, n, myUserId);
        if (updateIntentVerificationStatusAsUser) {
            if (n == this.mPm.getIntentVerificationStatusAsUser(this.mPackageName, myUserId)) {
                b = true;
            }
        }
        else {
            Log.e("AppLaunchSettings", "Couldn't update intent verification status!");
            b = updateIntentVerificationStatusAsUser;
        }
        return b;
    }
    
    protected AlertDialog createDialog(final int n, final int n2) {
        return null;
    }
    
    public int getMetricsCategory() {
        return 17;
    }
    
    public void onClick(final View view) {
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.addPreferencesFromResource(2132082782);
        this.mAppDomainUrls = (AppDomainsPreference)this.findPreference("app_launch_supported_domain_urls");
        this.mClearDefaultsPreference = (ClearDefaultsPreference)this.findPreference("app_launch_clear_defaults");
        this.mAppLinkState = (DropDownPreference)this.findPreference("app_link_state");
        this.mPm = this.getActivity().getPackageManager();
        this.mIsBrowser = this.isBrowserApp(this.mPackageName);
        this.mHasDomainUrls = ((this.mAppEntry.info.privateFlags & 0x10) != 0x0);
        if (!this.mIsBrowser) {
            final CharSequence[] entries = this.getEntries(this.mPackageName, this.mPm.getIntentFilterVerifications(this.mPackageName), this.mPm.getAllIntentFilters(this.mPackageName));
            this.mAppDomainUrls.setTitles(entries);
            this.mAppDomainUrls.setValues(new int[entries.length]);
        }
        this.buildStateDropDown();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        return true;
    }
    
    protected boolean refreshUi() {
        this.mClearDefaultsPreference.setPackageName(this.mPackageName);
        this.mClearDefaultsPreference.setAppEntry(this.mAppEntry);
        return true;
    }
}
