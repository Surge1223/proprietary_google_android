package com.android.settings.applications;

import android.app.ActivityManager;
import android.os.Bundle;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class SpecialAccessSettings extends DashboardFragment
{
    private static final String[] DISABLED_FEATURES_LOW_RAM;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        DISABLED_FEATURES_LOW_RAM = new String[] { "notification_access", "zen_access", "enabled_vr_listeners", "picture_in_picture" };
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082834;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context) {
        final ArrayList<DeviceAdministratorsController> list = (ArrayList<DeviceAdministratorsController>)new ArrayList<EnabledVrListenersController>();
        list.add((EnabledVrListenersController)new HighPowerAppsController(context));
        list.add((EnabledVrListenersController)new DeviceAdministratorsController(context));
        list.add((EnabledVrListenersController)new PremiumSmsController(context));
        list.add((EnabledVrListenersController)new DataSaverController(context));
        list.add(new EnabledVrListenersController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context);
    }
    
    @Override
    protected String getLogTag() {
        return "SpecialAccessSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 351;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082834;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (ActivityManager.isLowRamDeviceStatic()) {
            for (final String s : SpecialAccessSettings.DISABLED_FEATURES_LOW_RAM) {
                if (this.findPreference(s) != null) {
                    this.removePreference(s);
                }
            }
        }
    }
}
