package com.android.settings.applications;

import android.content.pm.ApplicationInfo;
import android.os.UserManager;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.pm.IPackageManager;
import android.app.admin.DevicePolicyManager;

public abstract class AppWithAdminGrantedPermissionsLister extends AppLister
{
    private final DevicePolicyManager mDevicePolicyManager;
    private final IPackageManager mPackageManagerService;
    private final String[] mPermissions;
    
    public AppWithAdminGrantedPermissionsLister(final String[] mPermissions, final PackageManagerWrapper packageManagerWrapper, final IPackageManager mPackageManagerService, final DevicePolicyManager mDevicePolicyManager, final UserManager userManager) {
        super(packageManagerWrapper, userManager);
        this.mPermissions = mPermissions;
        this.mPackageManagerService = mPackageManagerService;
        this.mDevicePolicyManager = mDevicePolicyManager;
    }
    
    @Override
    protected boolean includeInCount(final ApplicationInfo applicationInfo) {
        return AppWithAdminGrantedPermissionsCounter.includeInCount(this.mPermissions, this.mDevicePolicyManager, this.mPm, this.mPackageManagerService, applicationInfo);
    }
}
