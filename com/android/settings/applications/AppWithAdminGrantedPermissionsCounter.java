package com.android.settings.applications;

import android.os.RemoteException;
import android.os.UserHandle;
import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;
import android.content.pm.IPackageManager;
import android.app.admin.DevicePolicyManager;

public abstract class AppWithAdminGrantedPermissionsCounter extends AppCounter
{
    private final DevicePolicyManager mDevicePolicyManager;
    private final IPackageManager mPackageManagerService;
    private final String[] mPermissions;
    
    public AppWithAdminGrantedPermissionsCounter(final Context context, final String[] mPermissions, final PackageManagerWrapper packageManagerWrapper, final IPackageManager mPackageManagerService, final DevicePolicyManager mDevicePolicyManager) {
        super(context, packageManagerWrapper);
        this.mPermissions = mPermissions;
        this.mPackageManagerService = mPackageManagerService;
        this.mDevicePolicyManager = mDevicePolicyManager;
    }
    
    public static boolean includeInCount(final String[] array, final DevicePolicyManager devicePolicyManager, final PackageManagerWrapper packageManagerWrapper, final IPackageManager packageManager, final ApplicationInfo applicationInfo) {
        if (applicationInfo.targetSdkVersion >= 23) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (devicePolicyManager.getPermissionGrantState((ComponentName)null, applicationInfo.packageName, array[i]) == 1) {
                    return true;
                }
            }
            return false;
        }
        if (packageManagerWrapper.getInstallReason(applicationInfo.packageName, new UserHandle(UserHandle.getUserId(applicationInfo.uid))) != 1) {
            return false;
        }
        try {
            for (int length2 = array.length, j = 0; j < length2; ++j) {
                if (packageManager.checkUidPermission(array[j], applicationInfo.uid) == 0) {
                    return true;
                }
            }
        }
        catch (RemoteException ex) {}
        return false;
    }
    
    @Override
    protected boolean includeInCount(final ApplicationInfo applicationInfo) {
        return includeInCount(this.mPermissions, this.mDevicePolicyManager, this.mPm, this.mPackageManagerService, applicationInfo);
    }
}
