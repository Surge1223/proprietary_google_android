package com.android.settings.applications;

import android.content.pm.IPackageDataObserver$Stub;
import android.content.Loader;
import com.android.settings.deviceinfo.StorageWizardMoveConfirm;
import com.android.settings.Utils;
import com.android.settingslib.RestrictedLockUtils;
import android.content.DialogInterface;
import android.content.Context;
import android.app.AlertDialog;
import android.os.Bundle;
import android.content.pm.PackageManager;
import java.util.Iterator;
import android.content.pm.PackageManager;
import java.util.Map;
import android.util.MutableInt;
import android.app.GrantedUriPermission;
import java.util.TreeMap;
import android.view.View;
import android.os.RemoteException;
import android.os.UserHandle;
import android.app.AppGlobals;
import android.content.pm.IPackageDataObserver;
import android.util.Pair;
import java.util.Objects;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import android.os.storage.StorageManager;
import android.content.Intent;
import android.net.Uri;
import android.app.Activity;
import android.util.Log;
import android.net.Uri.Builder;
import android.app.ActivityManager;
import android.os.Message;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.Preference;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.app.AlertDialog$Builder;
import android.widget.Button;
import android.os.storage.VolumeInfo;
import com.android.settings.widget.ActionButtonPreference;
import com.android.settingslib.applications.ApplicationsState;
import android.view.View.OnClickListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settingslib.applications.StorageStatsSource;
import android.app.LoaderManager$LoaderCallbacks;

public class AppStorageSettings extends AppInfoWithHeader implements LoaderManager$LoaderCallbacks<StorageStatsSource.AppStorageStats>, DialogInterface$OnClickListener, View.OnClickListener, Callbacks
{
    private static final String TAG;
    ActionButtonPreference mButtonsPref;
    private boolean mCacheCleared;
    private boolean mCanClearData;
    private VolumeInfo[] mCandidates;
    private Button mChangeStorageButton;
    private ClearCacheObserver mClearCacheObserver;
    private ClearUserDataObserver mClearDataObserver;
    private LayoutPreference mClearUri;
    private Button mClearUriButton;
    private boolean mDataCleared;
    private AlertDialog$Builder mDialogBuilder;
    private final Handler mHandler;
    private ApplicationInfo mInfo;
    AppStorageSizesController mSizeController;
    private Preference mStorageUsed;
    private PreferenceCategory mUri;
    
    static {
        TAG = AppStorageSettings.class.getSimpleName();
    }
    
    public AppStorageSettings() {
        this.mCanClearData = true;
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                if (AppStorageSettings.this.getView() == null) {
                    return;
                }
                final int what = message.what;
                if (what != 1) {
                    if (what == 3) {
                        AppStorageSettings.this.mCacheCleared = true;
                        AppStorageSettings.this.updateSize();
                    }
                }
                else {
                    AppStorageSettings.this.mDataCleared = true;
                    AppStorageSettings.this.mCacheCleared = true;
                    AppStorageSettings.this.processClearMsg(message);
                }
            }
        };
    }
    
    private void clearUriPermissions() {
        final Activity activity = this.getActivity();
        final String packageName = this.mAppEntry.info.packageName;
        ((ActivityManager)((Context)activity).getSystemService("activity")).clearGrantedUriPermissions(packageName);
        final Uri build = new Uri.Builder().scheme("content").authority("com.android.documentsui.scopedAccess").appendPath("permissions").appendPath("*").build();
        final String tag = AppStorageSettings.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Asking ");
        sb.append(build);
        sb.append(" to delete permissions for ");
        sb.append(packageName);
        Log.v(tag, sb.toString());
        final int delete = ((Context)activity).getContentResolver().delete(build, (String)null, new String[] { packageName });
        final String tag2 = AppStorageSettings.TAG;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Deleted ");
        sb2.append(delete);
        sb2.append(" entries for package ");
        sb2.append(packageName);
        Log.d(tag2, sb2.toString());
        this.refreshGrantedUriPermissions();
    }
    
    private void initDataButtons() {
        final String manageSpaceActivityName = this.mAppEntry.info.manageSpaceActivityName;
        boolean b = true;
        final boolean b2 = manageSpaceActivityName != null;
        final boolean packageHasActiveAdmins = this.mDpm.packageHasActiveAdmins(this.mPackageName);
        final boolean b3 = (this.mAppEntry.info.flags & 0x41) == 0x1 || packageHasActiveAdmins;
        final Intent intent = new Intent("android.intent.action.VIEW");
        if (b2) {
            intent.setClassName(this.mAppEntry.info.packageName, this.mAppEntry.info.manageSpaceActivityName);
        }
        if (this.getPackageManager().resolveActivity(intent, 0) == null) {
            b = false;
        }
        if ((!b2 && b3) || !b) {
            this.mButtonsPref.setButton1Text(2131887022).setButton1Enabled(false);
            this.mCanClearData = false;
        }
        else {
            if (b2) {
                this.mButtonsPref.setButton1Text(2131888216);
            }
            else {
                this.mButtonsPref.setButton1Text(2131887022);
            }
            this.mButtonsPref.setButton1Text(2131887022).setButton1OnClickListener((View.OnClickListener)new _$$Lambda$AppStorageSettings$uXyfUeZFqT2Ct1euRP3fPo2Es3o(this));
        }
        if (this.mAppsControlDisallowedBySystem) {
            this.mButtonsPref.setButton1Enabled(false);
        }
    }
    
    private void initMoveDialog() {
        final Activity activity = this.getActivity();
        final StorageManager storageManager = (StorageManager)((Context)activity).getSystemService((Class)StorageManager.class);
        final List packageCandidateVolumes = ((Context)activity).getPackageManager().getPackageCandidateVolumes(this.mAppEntry.info);
        if (packageCandidateVolumes.size() > 1) {
            Collections.sort((List<Object>)packageCandidateVolumes, VolumeInfo.getDescriptionComparator());
            final CharSequence[] array = new CharSequence[packageCandidateVolumes.size()];
            int n = -1;
            for (int i = 0; i < packageCandidateVolumes.size(); ++i) {
                final String bestVolumeDescription = storageManager.getBestVolumeDescription((VolumeInfo)packageCandidateVolumes.get(i));
                if (Objects.equals(bestVolumeDescription, this.mStorageUsed.getSummary())) {
                    n = i;
                }
                array[i] = bestVolumeDescription;
            }
            this.mCandidates = packageCandidateVolumes.toArray(new VolumeInfo[packageCandidateVolumes.size()]);
            this.mDialogBuilder = new AlertDialog$Builder(this.getContext()).setTitle(2131886998).setSingleChoiceItems(array, n, (DialogInterface$OnClickListener)this).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null);
        }
        else {
            this.removePreference("storage_used");
            this.removePreference("change_storage_button");
            this.removePreference("storage_space");
        }
    }
    
    private void initiateClearUserData() {
        this.mMetricsFeatureProvider.action(this.getContext(), 876, (Pair<Integer, Object>[])new Pair[0]);
        this.mButtonsPref.setButton1Enabled(false);
        final String packageName = this.mAppEntry.info.packageName;
        final String tag = AppStorageSettings.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Clearing user data for package : ");
        sb.append(packageName);
        Log.i(tag, sb.toString());
        if (this.mClearDataObserver == null) {
            this.mClearDataObserver = new ClearUserDataObserver();
        }
        if (!((ActivityManager)this.getActivity().getSystemService("activity")).clearApplicationUserData(packageName, (IPackageDataObserver)this.mClearDataObserver)) {
            final String tag2 = AppStorageSettings.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Couldn't clear application user data for package:");
            sb2.append(packageName);
            Log.i(tag2, sb2.toString());
            this.showDialogInner(2, 0);
        }
        else {
            this.mButtonsPref.setButton1Text(2131888797);
        }
    }
    
    private boolean isMoveInProgress() {
        try {
            AppGlobals.getPackageManager().checkPackageStartable(this.mPackageName, UserHandle.myUserId());
            return false;
        }
        catch (RemoteException | SecurityException ex) {
            return true;
        }
    }
    
    private void processClearMsg(final Message message) {
        final int arg1 = message.arg1;
        final String packageName = this.mAppEntry.info.packageName;
        this.mButtonsPref.setButton1Text(2131887022);
        if (arg1 == 1) {
            final String tag = AppStorageSettings.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Cleared user data for package : ");
            sb.append(packageName);
            Log.i(tag, sb.toString());
            this.updateSize();
        }
        else {
            this.mButtonsPref.setButton1Enabled(true);
        }
    }
    
    private void refreshButtons() {
        this.initMoveDialog();
        this.initDataButtons();
    }
    
    private void refreshGrantedUriPermissions() {
        this.removeUriPermissionsFromUi();
        final List list = ((ActivityManager)this.getActivity().getSystemService("activity")).getGrantedUriPermissions(this.mAppEntry.info.packageName).getList();
        if (list.isEmpty()) {
            this.mClearUriButton.setVisibility(8);
            return;
        }
        final PackageManager packageManager = this.getActivity().getPackageManager();
        final TreeMap<Object, MutableInt> treeMap = (TreeMap<Object, MutableInt>)new TreeMap<Object, Object>();
        final Iterator<GrantedUriPermission> iterator = list.iterator();
        while (iterator.hasNext()) {
            final CharSequence loadLabel = packageManager.resolveContentProvider(iterator.next().uri.getAuthority(), 0).applicationInfo.loadLabel(packageManager);
            final MutableInt mutableInt = treeMap.get(loadLabel);
            if (mutableInt == null) {
                treeMap.put(loadLabel, new MutableInt(1));
            }
            else {
                ++mutableInt.value;
            }
        }
        for (final Map.Entry<CharSequence, MutableInt> entry : treeMap.entrySet()) {
            final int value = entry.getValue().value;
            final Preference preference = new Preference(this.getPrefContext());
            preference.setTitle(entry.getKey());
            preference.setSummary(this.getPrefContext().getResources().getQuantityString(2131755071, value, new Object[] { value }));
            preference.setSelectable(false);
            preference.setLayoutResource(2131558590);
            preference.setOrder(0);
            final String tag = AppStorageSettings.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Adding preference '");
            sb.append(preference);
            sb.append("' at order ");
            sb.append(0);
            Log.v(tag, sb.toString());
            this.mUri.addPreference(preference);
        }
        if (this.mAppsControlDisallowedBySystem) {
            this.mClearUriButton.setEnabled(false);
        }
        this.mClearUri.setOrder(0);
        this.mClearUriButton.setVisibility(0);
    }
    
    private void removeUriPermissionsFromUi() {
        for (int i = this.mUri.getPreferenceCount() - 1; i >= 0; --i) {
            final Preference preference = this.mUri.getPreference(i);
            if (preference != this.mClearUri) {
                this.mUri.removePreference(preference);
            }
        }
    }
    
    private void setupViews() {
        this.mSizeController = new AppStorageSizesController.Builder().setTotalSizePreference(this.findPreference("total_size")).setAppSizePreference(this.findPreference("app_size")).setDataSizePreference(this.findPreference("data_size")).setCacheSizePreference(this.findPreference("cache_size")).setComputingString(2131887067).setErrorString(2131887912).build();
        this.mButtonsPref = ((ActionButtonPreference)this.findPreference("header_view")).setButton1Positive(false).setButton2Positive(false);
        this.mStorageUsed = this.findPreference("storage_used");
        (this.mChangeStorageButton = ((LayoutPreference)this.findPreference("change_storage_button")).findViewById(2131361942)).setText(2131886997);
        this.mChangeStorageButton.setOnClickListener((View.OnClickListener)this);
        this.mButtonsPref.setButton2Text(2131887015);
        this.mUri = (PreferenceCategory)this.findPreference("uri_category");
        this.mClearUri = (LayoutPreference)this.mUri.findPreference("clear_uri_button");
        (this.mClearUriButton = this.mClearUri.findViewById(2131361942)).setText(2131887021);
        this.mClearUriButton.setOnClickListener((View.OnClickListener)this);
    }
    
    private void updateSize() {
        final PackageManager packageManager = this.getPackageManager();
        try {
            this.mInfo = packageManager.getApplicationInfo(this.mPackageName, 0);
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.e(AppStorageSettings.TAG, "Could not find package", (Throwable)ex);
        }
        if (this.mInfo == null) {
            return;
        }
        this.getLoaderManager().restartLoader(1, Bundle.EMPTY, (LoaderManager$LoaderCallbacks)this);
    }
    
    protected AlertDialog createDialog(final int n, final int n2) {
        switch (n) {
            default: {
                return null;
            }
            case 2: {
                return new AlertDialog$Builder((Context)this.getActivity()).setTitle(this.getActivity().getText(2131887022)).setMessage(this.getActivity().getText(2131887018)).setNeutralButton(2131887460, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        AppStorageSettings.this.mButtonsPref.setButton1Enabled(false);
                        AppStorageSettings.this.setIntentAndFinish(false, false);
                    }
                }).create();
            }
            case 1: {
                return new AlertDialog$Builder((Context)this.getActivity()).setTitle(this.getActivity().getText(2131887017)).setMessage(this.getActivity().getText(2131887016)).setPositiveButton(2131887460, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        AppStorageSettings.this.initiateClearUserData();
                    }
                }).setNegativeButton(2131887454, (DialogInterface$OnClickListener)null).create();
            }
        }
    }
    
    public int getMetricsCategory() {
        return 19;
    }
    
    void handleClearCacheClick() {
        if (this.mAppsControlDisallowedAdmin != null && !this.mAppsControlDisallowedBySystem) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this.getActivity(), this.mAppsControlDisallowedAdmin);
            return;
        }
        if (this.mClearCacheObserver == null) {
            this.mClearCacheObserver = new ClearCacheObserver();
        }
        this.mMetricsFeatureProvider.action(this.getContext(), 877, (Pair<Integer, Object>[])new Pair[0]);
        this.mPm.deleteApplicationCacheFiles(this.mPackageName, (IPackageDataObserver)this.mClearCacheObserver);
    }
    
    void handleClearDataClick() {
        if (this.mAppsControlDisallowedAdmin != null && !this.mAppsControlDisallowedBySystem) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this.getActivity(), this.mAppsControlDisallowedAdmin);
        }
        else if (this.mAppEntry.info.manageSpaceActivityName != null) {
            if (!Utils.isMonkeyRunning()) {
                final Intent intent = new Intent("android.intent.action.VIEW");
                intent.setClassName(this.mAppEntry.info.packageName, this.mAppEntry.info.manageSpaceActivityName);
                this.startActivityForResult(intent, 2);
            }
        }
        else {
            this.showDialogInner(1, 0);
        }
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final Activity activity = this.getActivity();
        final VolumeInfo volumeInfo = this.mCandidates[n];
        if (!Objects.equals(volumeInfo, ((Context)activity).getPackageManager().getPackageCurrentVolume(this.mAppEntry.info))) {
            final Intent intent = new Intent((Context)activity, (Class)StorageWizardMoveConfirm.class);
            intent.putExtra("android.os.storage.extra.VOLUME_ID", volumeInfo.getId());
            intent.putExtra("android.intent.extra.PACKAGE_NAME", this.mAppEntry.info.packageName);
            this.startActivity(intent);
        }
        dialogInterface.dismiss();
    }
    
    public void onClick(final View view) {
        if (view == this.mChangeStorageButton && this.mDialogBuilder != null && !this.isMoveInProgress()) {
            this.mDialogBuilder.show();
        }
        else if (view == this.mClearUriButton) {
            if (this.mAppsControlDisallowedAdmin != null && !this.mAppsControlDisallowedBySystem) {
                RestrictedLockUtils.sendShowAdminSupportDetailsIntent((Context)this.getActivity(), this.mAppsControlDisallowedAdmin);
            }
            else {
                this.clearUriPermissions();
            }
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            boolean mCacheCleared = false;
            this.mCacheCleared = bundle.getBoolean("cache_cleared", false);
            this.mDataCleared = bundle.getBoolean("data_cleared", false);
            if (this.mCacheCleared || this.mDataCleared) {
                mCacheCleared = true;
            }
            this.mCacheCleared = mCacheCleared;
        }
        this.addPreferencesFromResource(2132082715);
        this.setupViews();
        this.initMoveDialog();
    }
    
    public Loader<StorageStatsSource.AppStorageStats> onCreateLoader(final int n, final Bundle bundle) {
        final Context context = this.getContext();
        return (Loader<StorageStatsSource.AppStorageStats>)new FetchPackageStorageAsyncLoader(context, new StorageStatsSource(context), this.mInfo, UserHandle.of(this.mUserId));
    }
    
    public void onLoadFinished(final Loader<StorageStatsSource.AppStorageStats> loader, final StorageStatsSource.AppStorageStats result) {
        this.mSizeController.setResult(result);
        this.updateUiWithSize(result);
    }
    
    public void onLoaderReset(final Loader<StorageStatsSource.AppStorageStats> loader) {
    }
    
    public void onPackageSizeChanged(final String s) {
    }
    
    public void onResume() {
        super.onResume();
        this.updateSize();
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("cache_cleared", this.mCacheCleared);
        bundle.putBoolean("data_cleared", this.mDataCleared);
    }
    
    protected boolean refreshUi() {
        this.retrieveAppEntry();
        if (this.mAppEntry == null) {
            return false;
        }
        this.updateUiWithSize(this.mSizeController.getLastResult());
        this.refreshGrantedUriPermissions();
        this.mStorageUsed.setSummary(((StorageManager)this.getContext().getSystemService((Class)StorageManager.class)).getBestVolumeDescription(this.getActivity().getPackageManager().getPackageCurrentVolume(this.mAppEntry.info)));
        this.refreshButtons();
        return true;
    }
    
    void updateUiWithSize(final StorageStatsSource.AppStorageStats appStorageStats) {
        if (this.mCacheCleared) {
            this.mSizeController.setCacheCleared(true);
        }
        if (this.mDataCleared) {
            this.mSizeController.setDataCleared(true);
        }
        this.mSizeController.updateUi(this.getContext());
        if (appStorageStats == null) {
            this.mButtonsPref.setButton1Enabled(false).setButton2Enabled(false);
        }
        else {
            final long cacheBytes = appStorageStats.getCacheBytes();
            if (appStorageStats.getDataBytes() - cacheBytes > 0L && this.mCanClearData && !this.mDataCleared) {
                this.mButtonsPref.setButton1Enabled(true).setButton1OnClickListener((View.OnClickListener)new _$$Lambda$AppStorageSettings$n1EpAla7gNI7Nnl_O3UD0UWSgTo(this));
            }
            else {
                this.mButtonsPref.setButton1Enabled(false);
            }
            if (cacheBytes > 0L && !this.mCacheCleared) {
                this.mButtonsPref.setButton2Enabled(true).setButton2OnClickListener((View.OnClickListener)new _$$Lambda$AppStorageSettings$DjRyx_XFfzsxe3o1nZS2usao_fc(this));
            }
            else {
                this.mButtonsPref.setButton2Enabled(false);
            }
        }
        if (this.mAppsControlDisallowedBySystem) {
            this.mButtonsPref.setButton1Enabled(false).setButton2Enabled(false);
        }
    }
    
    class ClearCacheObserver extends IPackageDataObserver$Stub
    {
        public void onRemoveCompleted(final String s, final boolean b) {
            final Message obtainMessage = AppStorageSettings.this.mHandler.obtainMessage(3);
            int arg1;
            if (b) {
                arg1 = 1;
            }
            else {
                arg1 = 2;
            }
            obtainMessage.arg1 = arg1;
            AppStorageSettings.this.mHandler.sendMessage(obtainMessage);
        }
    }
    
    class ClearUserDataObserver extends IPackageDataObserver$Stub
    {
        public void onRemoveCompleted(final String s, final boolean b) {
            final Handler access$500 = AppStorageSettings.this.mHandler;
            int arg1 = 1;
            final Message obtainMessage = access$500.obtainMessage(1);
            if (!b) {
                arg1 = 2;
            }
            obtainMessage.arg1 = arg1;
            AppStorageSettings.this.mHandler.sendMessage(obtainMessage);
        }
    }
}
