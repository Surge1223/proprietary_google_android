package com.android.settings.applications;

import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.pm.ServiceInfo;
import android.os.RemoteException;
import android.util.Log;
import android.app.ActivityThread;
import android.app.ActivityManager$RunningServiceInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.text.format.Formatter;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.Utils;
import android.content.pm.UserInfo;
import android.os.UserHandle;
import android.os.Message;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.os.UserManager;
import java.util.HashMap;
import android.content.pm.PackageManager;
import android.util.SparseArray;
import com.android.settingslib.applications.InterestingConfigChanges;
import android.os.Handler;
import android.os.HandlerThread;
import java.util.Comparator;
import android.content.Context;
import android.app.ActivityManager;
import java.util.ArrayList;

public class RunningState
{
    static Object sGlobalLock;
    static RunningState sInstance;
    final ArrayList<ProcessItem> mAllProcessItems;
    final ActivityManager mAm;
    final Context mApplicationContext;
    final Comparator<MergedItem> mBackgroundComparator;
    final BackgroundHandler mBackgroundHandler;
    ArrayList<MergedItem> mBackgroundItems;
    long mBackgroundProcessMemory;
    final HandlerThread mBackgroundThread;
    long mForegroundProcessMemory;
    final Handler mHandler;
    boolean mHaveData;
    final boolean mHideManagedProfiles;
    final InterestingConfigChanges mInterestingConfigChanges;
    final ArrayList<ProcessItem> mInterestingProcesses;
    ArrayList<BaseItem> mItems;
    final Object mLock;
    ArrayList<MergedItem> mMergedItems;
    final int mMyUserId;
    int mNumBackgroundProcesses;
    int mNumForegroundProcesses;
    int mNumServiceProcesses;
    final SparseArray<MergedItem> mOtherUserBackgroundItems;
    final SparseArray<MergedItem> mOtherUserMergedItems;
    final PackageManager mPm;
    final ArrayList<ProcessItem> mProcessItems;
    OnRefreshUiListener mRefreshUiListener;
    boolean mResumed;
    final SparseArray<ProcessItem> mRunningProcesses;
    int mSequence;
    final ServiceProcessComparator mServiceProcessComparator;
    long mServiceProcessMemory;
    final SparseArray<HashMap<String, ProcessItem>> mServiceProcessesByName;
    final SparseArray<ProcessItem> mServiceProcessesByPid;
    final SparseArray<AppProcessInfo> mTmpAppProcesses;
    final UserManager mUm;
    private final UserManagerBroadcastReceiver mUmBroadcastReceiver;
    ArrayList<MergedItem> mUserBackgroundItems;
    boolean mWatchingBackgroundItems;
    
    static {
        RunningState.sGlobalLock = new Object();
    }
    
    private RunningState(final Context context) {
        this.mInterestingConfigChanges = new InterestingConfigChanges();
        this.mServiceProcessesByName = (SparseArray<HashMap<String, ProcessItem>>)new SparseArray();
        this.mServiceProcessesByPid = (SparseArray<ProcessItem>)new SparseArray();
        this.mServiceProcessComparator = new ServiceProcessComparator();
        this.mInterestingProcesses = new ArrayList<ProcessItem>();
        this.mRunningProcesses = (SparseArray<ProcessItem>)new SparseArray();
        this.mProcessItems = new ArrayList<ProcessItem>();
        this.mAllProcessItems = new ArrayList<ProcessItem>();
        this.mOtherUserMergedItems = (SparseArray<MergedItem>)new SparseArray();
        this.mOtherUserBackgroundItems = (SparseArray<MergedItem>)new SparseArray();
        this.mTmpAppProcesses = (SparseArray<AppProcessInfo>)new SparseArray();
        this.mSequence = 0;
        this.mBackgroundComparator = new Comparator<MergedItem>() {
            @Override
            public int compare(final MergedItem mergedItem, final MergedItem mergedItem2) {
                final int mUserId = mergedItem.mUserId;
                final int mUserId2 = mergedItem2.mUserId;
                int compareTo = -1;
                if (mUserId != mUserId2) {
                    if (mergedItem.mUserId == RunningState.this.mMyUserId) {
                        return -1;
                    }
                    if (mergedItem2.mUserId == RunningState.this.mMyUserId) {
                        return 1;
                    }
                    if (mergedItem.mUserId >= mergedItem2.mUserId) {
                        compareTo = 1;
                    }
                    return compareTo;
                }
                else if (mergedItem.mProcess == mergedItem2.mProcess) {
                    if (mergedItem.mLabel == mergedItem2.mLabel) {
                        return 0;
                    }
                    if (mergedItem.mLabel != null) {
                        compareTo = mergedItem.mLabel.compareTo(mergedItem2.mLabel);
                    }
                    return compareTo;
                }
                else {
                    if (mergedItem.mProcess == null) {
                        return -1;
                    }
                    if (mergedItem2.mProcess == null) {
                        return 1;
                    }
                    final ActivityManager$RunningAppProcessInfo mRunningProcessInfo = mergedItem.mProcess.mRunningProcessInfo;
                    final ActivityManager$RunningAppProcessInfo mRunningProcessInfo2 = mergedItem2.mProcess.mRunningProcessInfo;
                    final boolean b = mRunningProcessInfo.importance >= 400;
                    if (b != mRunningProcessInfo2.importance >= 400) {
                        if (b) {
                            compareTo = 1;
                        }
                        return compareTo;
                    }
                    final boolean b2 = (mRunningProcessInfo.flags & 0x4) != 0x0;
                    if (b2 != ((mRunningProcessInfo2.flags & 0x4) != 0x0)) {
                        if (!b2) {
                            compareTo = 1;
                        }
                        return compareTo;
                    }
                    if (mRunningProcessInfo.lru != mRunningProcessInfo2.lru) {
                        if (mRunningProcessInfo.lru >= mRunningProcessInfo2.lru) {
                            compareTo = 1;
                        }
                        return compareTo;
                    }
                    if (mergedItem.mProcess.mLabel == mergedItem2.mProcess.mLabel) {
                        return 0;
                    }
                    if (mergedItem.mProcess.mLabel == null) {
                        return 1;
                    }
                    if (mergedItem2.mProcess.mLabel == null) {
                        return -1;
                    }
                    return mergedItem.mProcess.mLabel.compareTo(mergedItem2.mProcess.mLabel);
                }
            }
        };
        this.mLock = new Object();
        this.mItems = new ArrayList<BaseItem>();
        this.mMergedItems = new ArrayList<MergedItem>();
        this.mBackgroundItems = new ArrayList<MergedItem>();
        this.mUserBackgroundItems = new ArrayList<MergedItem>();
        this.mHandler = new Handler() {
            int mNextUpdate = 0;
            
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case 4: {
                        synchronized (RunningState.this.mLock) {
                            if (!RunningState.this.mResumed) {
                                return;
                            }
                            // monitorexit(this.this$0.mLock)
                            this.removeMessages(4);
                            this.sendMessageDelayed(this.obtainMessage(4), 1000L);
                            if (RunningState.this.mRefreshUiListener != null) {
                                RunningState.this.mRefreshUiListener.onRefreshUi(this.mNextUpdate);
                                this.mNextUpdate = 0;
                                break;
                            }
                            break;
                        }
                    }
                    case 3: {
                        int mNextUpdate;
                        if (message.arg1 != 0) {
                            mNextUpdate = 2;
                        }
                        else {
                            mNextUpdate = 1;
                        }
                        this.mNextUpdate = mNextUpdate;
                        break;
                    }
                }
            }
        };
        this.mUmBroadcastReceiver = new UserManagerBroadcastReceiver();
        this.mApplicationContext = context.getApplicationContext();
        this.mAm = (ActivityManager)this.mApplicationContext.getSystemService("activity");
        this.mPm = this.mApplicationContext.getPackageManager();
        this.mUm = (UserManager)this.mApplicationContext.getSystemService("user");
        this.mMyUserId = UserHandle.myUserId();
        final UserInfo userInfo = this.mUm.getUserInfo(this.mMyUserId);
        this.mHideManagedProfiles = (userInfo == null || !userInfo.canHaveProfile());
        this.mResumed = false;
        (this.mBackgroundThread = new HandlerThread("RunningState:Background")).start();
        this.mBackgroundHandler = new BackgroundHandler(this.mBackgroundThread.getLooper());
        this.mUmBroadcastReceiver.register(this.mApplicationContext);
    }
    
    private void addOtherUserItem(final Context context, final ArrayList<MergedItem> list, final SparseArray<MergedItem> sparseArray, final MergedItem mergedItem) {
        final MergedItem mergedItem2 = (MergedItem)sparseArray.get(mergedItem.mUserId);
        final boolean b = mergedItem2 == null || mergedItem2.mCurSeq != this.mSequence;
        MergedItem mergedItem3 = mergedItem2;
        if (b) {
            final UserInfo userInfo = this.mUm.getUserInfo(mergedItem.mUserId);
            if (userInfo == null) {
                return;
            }
            if (this.mHideManagedProfiles && userInfo.isManagedProfile()) {
                return;
            }
            MergedItem mergedItem5;
            if (mergedItem2 == null) {
                final MergedItem mergedItem4 = new MergedItem(mergedItem.mUserId);
                sparseArray.put(mergedItem.mUserId, (Object)mergedItem4);
                mergedItem5 = mergedItem4;
            }
            else {
                mergedItem2.mChildren.clear();
                mergedItem5 = mergedItem2;
            }
            mergedItem5.mCurSeq = this.mSequence;
            mergedItem5.mUser = new UserState();
            mergedItem5.mUser.mInfo = userInfo;
            mergedItem5.mUser.mIcon = Utils.getUserIcon(context, this.mUm, userInfo);
            mergedItem5.mUser.mLabel = Utils.getUserLabel(context, userInfo);
            list.add(mergedItem5);
            mergedItem3 = mergedItem5;
        }
        mergedItem3.mChildren.add(mergedItem);
    }
    
    static RunningState getInstance(final Context context) {
        synchronized (RunningState.sGlobalLock) {
            if (RunningState.sInstance == null) {
                RunningState.sInstance = new RunningState(context);
            }
            return RunningState.sInstance;
        }
    }
    
    private boolean isInterestingProcess(final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo) {
        return (activityManager$RunningAppProcessInfo.flags & 0x1) != 0x0 || ((activityManager$RunningAppProcessInfo.flags & 0x2) == 0x0 && activityManager$RunningAppProcessInfo.importance >= 100 && activityManager$RunningAppProcessInfo.importance < 350 && activityManager$RunningAppProcessInfo.importanceReasonCode == 0);
    }
    
    static CharSequence makeLabel(final PackageManager packageManager, final String s, final PackageItemInfo packageItemInfo) {
        if (packageItemInfo != null && (packageItemInfo.labelRes != 0 || packageItemInfo.nonLocalizedLabel != null)) {
            final CharSequence loadLabel = packageItemInfo.loadLabel(packageManager);
            if (loadLabel != null) {
                return loadLabel;
            }
        }
        final int lastIndex = s.lastIndexOf(46);
        String substring = s;
        if (lastIndex >= 0) {
            substring = s.substring(lastIndex + 1, s.length());
        }
        return substring;
    }
    
    private void reset() {
        this.mServiceProcessesByName.clear();
        this.mServiceProcessesByPid.clear();
        this.mInterestingProcesses.clear();
        this.mRunningProcesses.clear();
        this.mProcessItems.clear();
        this.mAllProcessItems.clear();
    }
    
    private boolean update(final Context p0, final ActivityManager p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //     4: astore_3       
        //     5: aload_0        
        //     6: aload_0        
        //     7: getfield        com/android/settings/applications/RunningState.mSequence:I
        //    10: iconst_1       
        //    11: iadd           
        //    12: putfield        com/android/settings/applications/RunningState.mSequence:I
        //    15: aload_2        
        //    16: bipush          100
        //    18: invokevirtual   android/app/ActivityManager.getRunningServices:(I)Ljava/util/List;
        //    21: astore          4
        //    23: aload           4
        //    25: ifnull          40
        //    28: aload           4
        //    30: invokeinterface java/util/List.size:()I
        //    35: istore          5
        //    37: goto            43
        //    40: iconst_0       
        //    41: istore          5
        //    43: iconst_0       
        //    44: istore          6
        //    46: iload           6
        //    48: iload           5
        //    50: if_icmpge       162
        //    53: aload           4
        //    55: iload           6
        //    57: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    62: checkcast       Landroid/app/ActivityManager$RunningServiceInfo;
        //    65: astore          7
        //    67: aload           7
        //    69: getfield        android/app/ActivityManager$RunningServiceInfo.started:Z
        //    72: ifne            108
        //    75: aload           7
        //    77: getfield        android/app/ActivityManager$RunningServiceInfo.clientLabel:I
        //    80: ifne            108
        //    83: aload           4
        //    85: iload           6
        //    87: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
        //    92: pop            
        //    93: iload           6
        //    95: iconst_1       
        //    96: isub           
        //    97: istore          8
        //    99: iload           5
        //   101: iconst_1       
        //   102: isub           
        //   103: istore          9
        //   105: goto            149
        //   108: iload           6
        //   110: istore          8
        //   112: iload           5
        //   114: istore          9
        //   116: aload           7
        //   118: getfield        android/app/ActivityManager$RunningServiceInfo.flags:I
        //   121: bipush          8
        //   123: iand           
        //   124: ifeq            149
        //   127: aload           4
        //   129: iload           6
        //   131: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
        //   136: pop            
        //   137: iload           6
        //   139: iconst_1       
        //   140: isub           
        //   141: istore          8
        //   143: iload           5
        //   145: iconst_1       
        //   146: isub           
        //   147: istore          9
        //   149: iload           8
        //   151: iconst_1       
        //   152: iadd           
        //   153: istore          6
        //   155: iload           9
        //   157: istore          5
        //   159: goto            46
        //   162: aload_2        
        //   163: invokevirtual   android/app/ActivityManager.getRunningAppProcesses:()Ljava/util/List;
        //   166: astore_2       
        //   167: aload_2        
        //   168: ifnull          182
        //   171: aload_2        
        //   172: invokeinterface java/util/List.size:()I
        //   177: istore          9
        //   179: goto            185
        //   182: iconst_0       
        //   183: istore          9
        //   185: aload_0        
        //   186: getfield        com/android/settings/applications/RunningState.mTmpAppProcesses:Landroid/util/SparseArray;
        //   189: invokevirtual   android/util/SparseArray.clear:()V
        //   192: iconst_0       
        //   193: istore          8
        //   195: iload           8
        //   197: iload           9
        //   199: if_icmpge       242
        //   202: aload_2        
        //   203: iload           8
        //   205: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   210: checkcast       Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   213: astore          7
        //   215: aload_0        
        //   216: getfield        com/android/settings/applications/RunningState.mTmpAppProcesses:Landroid/util/SparseArray;
        //   219: aload           7
        //   221: getfield        android/app/ActivityManager$RunningAppProcessInfo.pid:I
        //   224: new             Lcom/android/settings/applications/RunningState$AppProcessInfo;
        //   227: dup            
        //   228: aload           7
        //   230: invokespecial   com/android/settings/applications/RunningState$AppProcessInfo.<init>:(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
        //   233: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   236: iinc            8, 1
        //   239: goto            195
        //   242: iconst_0       
        //   243: istore          8
        //   245: iload           8
        //   247: iload           5
        //   249: if_icmpge       332
        //   252: aload           4
        //   254: iload           8
        //   256: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   261: checkcast       Landroid/app/ActivityManager$RunningServiceInfo;
        //   264: astore          10
        //   266: aload           10
        //   268: getfield        android/app/ActivityManager$RunningServiceInfo.restarting:J
        //   271: lconst_0       
        //   272: lcmp           
        //   273: ifne            326
        //   276: aload           10
        //   278: getfield        android/app/ActivityManager$RunningServiceInfo.pid:I
        //   281: ifle            326
        //   284: aload_0        
        //   285: getfield        com/android/settings/applications/RunningState.mTmpAppProcesses:Landroid/util/SparseArray;
        //   288: aload           10
        //   290: getfield        android/app/ActivityManager$RunningServiceInfo.pid:I
        //   293: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   296: checkcast       Lcom/android/settings/applications/RunningState$AppProcessInfo;
        //   299: astore          7
        //   301: aload           7
        //   303: ifnull          326
        //   306: aload           7
        //   308: iconst_1       
        //   309: putfield        com/android/settings/applications/RunningState$AppProcessInfo.hasServices:Z
        //   312: aload           10
        //   314: getfield        android/app/ActivityManager$RunningServiceInfo.foreground:Z
        //   317: ifeq            326
        //   320: aload           7
        //   322: iconst_1       
        //   323: putfield        com/android/settings/applications/RunningState$AppProcessInfo.hasForegroundServices:Z
        //   326: iinc            8, 1
        //   329: goto            245
        //   332: iconst_0       
        //   333: istore          11
        //   335: iconst_0       
        //   336: istore          8
        //   338: iload           8
        //   340: iload           5
        //   342: if_icmpge       772
        //   345: aload           4
        //   347: iload           8
        //   349: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   354: checkcast       Landroid/app/ActivityManager$RunningServiceInfo;
        //   357: astore          12
        //   359: aload           12
        //   361: getfield        android/app/ActivityManager$RunningServiceInfo.restarting:J
        //   364: lconst_0       
        //   365: lcmp           
        //   366: ifne            510
        //   369: aload           12
        //   371: getfield        android/app/ActivityManager$RunningServiceInfo.pid:I
        //   374: ifle            510
        //   377: aload_0        
        //   378: getfield        com/android/settings/applications/RunningState.mTmpAppProcesses:Landroid/util/SparseArray;
        //   381: aload           12
        //   383: getfield        android/app/ActivityManager$RunningServiceInfo.pid:I
        //   386: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   389: checkcast       Lcom/android/settings/applications/RunningState$AppProcessInfo;
        //   392: astore          7
        //   394: aload           7
        //   396: ifnull          510
        //   399: aload           7
        //   401: getfield        com/android/settings/applications/RunningState$AppProcessInfo.hasForegroundServices:Z
        //   404: ifne            510
        //   407: aload           7
        //   409: getfield        com/android/settings/applications/RunningState$AppProcessInfo.info:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   412: getfield        android/app/ActivityManager$RunningAppProcessInfo.importance:I
        //   415: sipush          300
        //   418: if_icmpge       510
        //   421: iconst_0       
        //   422: istore          13
        //   424: aload_0        
        //   425: getfield        com/android/settings/applications/RunningState.mTmpAppProcesses:Landroid/util/SparseArray;
        //   428: aload           7
        //   430: getfield        com/android/settings/applications/RunningState$AppProcessInfo.info:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   433: getfield        android/app/ActivityManager$RunningAppProcessInfo.importanceReasonPid:I
        //   436: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   439: checkcast       Lcom/android/settings/applications/RunningState$AppProcessInfo;
        //   442: astore          7
        //   444: iload           13
        //   446: istore          6
        //   448: aload           7
        //   450: ifnull          502
        //   453: aload           7
        //   455: getfield        com/android/settings/applications/RunningState$AppProcessInfo.hasServices:Z
        //   458: ifne            499
        //   461: aload_0        
        //   462: aload           7
        //   464: getfield        com/android/settings/applications/RunningState$AppProcessInfo.info:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   467: invokespecial   com/android/settings/applications/RunningState.isInterestingProcess:(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z
        //   470: ifeq            476
        //   473: goto            499
        //   476: aload_0        
        //   477: getfield        com/android/settings/applications/RunningState.mTmpAppProcesses:Landroid/util/SparseArray;
        //   480: aload           7
        //   482: getfield        com/android/settings/applications/RunningState$AppProcessInfo.info:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   485: getfield        android/app/ActivityManager$RunningAppProcessInfo.importanceReasonPid:I
        //   488: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   491: checkcast       Lcom/android/settings/applications/RunningState$AppProcessInfo;
        //   494: astore          7
        //   496: goto            444
        //   499: iconst_1       
        //   500: istore          6
        //   502: iload           6
        //   504: ifeq            510
        //   507: goto            766
        //   510: aload_0        
        //   511: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //   514: aload           12
        //   516: getfield        android/app/ActivityManager$RunningServiceInfo.uid:I
        //   519: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   522: checkcast       Ljava/util/HashMap;
        //   525: astore          10
        //   527: aload           10
        //   529: astore          7
        //   531: aload           10
        //   533: ifnonnull       559
        //   536: new             Ljava/util/HashMap;
        //   539: dup            
        //   540: invokespecial   java/util/HashMap.<init>:()V
        //   543: astore          7
        //   545: aload_0        
        //   546: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //   549: aload           12
        //   551: getfield        android/app/ActivityManager$RunningServiceInfo.uid:I
        //   554: aload           7
        //   556: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   559: aload           7
        //   561: aload           12
        //   563: getfield        android/app/ActivityManager$RunningServiceInfo.process:Ljava/lang/String;
        //   566: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   569: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //   572: astore          14
        //   574: aload           14
        //   576: astore          10
        //   578: aload           14
        //   580: ifnonnull       619
        //   583: iconst_1       
        //   584: istore          11
        //   586: new             Lcom/android/settings/applications/RunningState$ProcessItem;
        //   589: dup            
        //   590: aload_1        
        //   591: aload           12
        //   593: getfield        android/app/ActivityManager$RunningServiceInfo.uid:I
        //   596: aload           12
        //   598: getfield        android/app/ActivityManager$RunningServiceInfo.process:Ljava/lang/String;
        //   601: invokespecial   com/android/settings/applications/RunningState$ProcessItem.<init>:(Landroid/content/Context;ILjava/lang/String;)V
        //   604: astore          10
        //   606: aload           7
        //   608: aload           12
        //   610: getfield        android/app/ActivityManager$RunningServiceInfo.process:Ljava/lang/String;
        //   613: aload           10
        //   615: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   618: pop            
        //   619: iload           11
        //   621: istore          15
        //   623: aload           10
        //   625: getfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //   628: aload_0        
        //   629: getfield        com/android/settings/applications/RunningState.mSequence:I
        //   632: if_icmpeq       753
        //   635: aload           12
        //   637: getfield        android/app/ActivityManager$RunningServiceInfo.restarting:J
        //   640: lconst_0       
        //   641: lcmp           
        //   642: ifne            655
        //   645: aload           12
        //   647: getfield        android/app/ActivityManager$RunningServiceInfo.pid:I
        //   650: istore          6
        //   652: goto            658
        //   655: iconst_0       
        //   656: istore          6
        //   658: iload           6
        //   660: aload           10
        //   662: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //   665: if_icmpeq       732
        //   668: iconst_1       
        //   669: istore          15
        //   671: iload           15
        //   673: istore          11
        //   675: aload           10
        //   677: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //   680: iload           6
        //   682: if_icmpeq       732
        //   685: aload           10
        //   687: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //   690: ifeq            705
        //   693: aload_0        
        //   694: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //   697: aload           10
        //   699: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //   702: invokevirtual   android/util/SparseArray.remove:(I)V
        //   705: iload           6
        //   707: ifeq            721
        //   710: aload_0        
        //   711: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //   714: iload           6
        //   716: aload           10
        //   718: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   721: aload           10
        //   723: iload           6
        //   725: putfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //   728: iload           15
        //   730: istore          11
        //   732: aload           10
        //   734: getfield        com/android/settings/applications/RunningState$ProcessItem.mDependentProcesses:Landroid/util/SparseArray;
        //   737: invokevirtual   android/util/SparseArray.clear:()V
        //   740: aload           10
        //   742: aload_0        
        //   743: getfield        com/android/settings/applications/RunningState.mSequence:I
        //   746: putfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //   749: iload           11
        //   751: istore          15
        //   753: iload           15
        //   755: aload           10
        //   757: aload_1        
        //   758: aload           12
        //   760: invokevirtual   com/android/settings/applications/RunningState$ProcessItem.updateService:(Landroid/content/Context;Landroid/app/ActivityManager$RunningServiceInfo;)Z
        //   763: ior            
        //   764: istore          11
        //   766: iinc            8, 1
        //   769: goto            338
        //   772: iconst_0       
        //   773: istore          8
        //   775: iload           8
        //   777: iload           9
        //   779: if_icmpge       1004
        //   782: aload_2        
        //   783: iload           8
        //   785: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   790: checkcast       Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   793: astore          14
        //   795: aload_0        
        //   796: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //   799: aload           14
        //   801: getfield        android/app/ActivityManager$RunningAppProcessInfo.pid:I
        //   804: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   807: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //   810: astore          10
        //   812: iload           11
        //   814: istore          15
        //   816: aload           10
        //   818: astore          7
        //   820: aload           10
        //   822: ifnonnull       910
        //   825: aload_0        
        //   826: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //   829: aload           14
        //   831: getfield        android/app/ActivityManager$RunningAppProcessInfo.pid:I
        //   834: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   837: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //   840: astore          10
        //   842: aload           10
        //   844: astore          7
        //   846: aload           10
        //   848: ifnonnull       898
        //   851: iconst_1       
        //   852: istore          11
        //   854: new             Lcom/android/settings/applications/RunningState$ProcessItem;
        //   857: dup            
        //   858: aload_1        
        //   859: aload           14
        //   861: getfield        android/app/ActivityManager$RunningAppProcessInfo.uid:I
        //   864: aload           14
        //   866: getfield        android/app/ActivityManager$RunningAppProcessInfo.processName:Ljava/lang/String;
        //   869: invokespecial   com/android/settings/applications/RunningState$ProcessItem.<init>:(Landroid/content/Context;ILjava/lang/String;)V
        //   872: astore          7
        //   874: aload           7
        //   876: aload           14
        //   878: getfield        android/app/ActivityManager$RunningAppProcessInfo.pid:I
        //   881: putfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //   884: aload_0        
        //   885: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //   888: aload           14
        //   890: getfield        android/app/ActivityManager$RunningAppProcessInfo.pid:I
        //   893: aload           7
        //   895: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   898: aload           7
        //   900: getfield        com/android/settings/applications/RunningState$ProcessItem.mDependentProcesses:Landroid/util/SparseArray;
        //   903: invokevirtual   android/util/SparseArray.clear:()V
        //   906: iload           11
        //   908: istore          15
        //   910: aload_0        
        //   911: aload           14
        //   913: invokespecial   com/android/settings/applications/RunningState.isInterestingProcess:(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z
        //   916: ifeq            972
        //   919: aload_0        
        //   920: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //   923: aload           7
        //   925: invokevirtual   java/util/ArrayList.contains:(Ljava/lang/Object;)Z
        //   928: ifne            944
        //   931: iconst_1       
        //   932: istore          15
        //   934: aload_0        
        //   935: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //   938: aload           7
        //   940: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   943: pop            
        //   944: aload           7
        //   946: aload_0        
        //   947: getfield        com/android/settings/applications/RunningState.mSequence:I
        //   950: putfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //   953: aload           7
        //   955: iconst_1       
        //   956: putfield        com/android/settings/applications/RunningState$ProcessItem.mInteresting:Z
        //   959: aload           7
        //   961: aload_3        
        //   962: invokevirtual   com/android/settings/applications/RunningState$ProcessItem.ensureLabel:(Landroid/content/pm/PackageManager;)V
        //   965: iload           15
        //   967: istore          11
        //   969: goto            982
        //   972: aload           7
        //   974: iconst_0       
        //   975: putfield        com/android/settings/applications/RunningState$ProcessItem.mInteresting:Z
        //   978: iload           15
        //   980: istore          11
        //   982: aload           7
        //   984: aload_0        
        //   985: getfield        com/android/settings/applications/RunningState.mSequence:I
        //   988: putfield        com/android/settings/applications/RunningState$ProcessItem.mRunningSeq:I
        //   991: aload           7
        //   993: aload           14
        //   995: putfield        com/android/settings/applications/RunningState$ProcessItem.mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //   998: iinc            8, 1
        //  1001: goto            775
        //  1004: aload_0        
        //  1005: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  1008: invokevirtual   android/util/SparseArray.size:()I
        //  1011: istore          8
        //  1013: iconst_0       
        //  1014: istore          6
        //  1016: iload           6
        //  1018: iload           8
        //  1020: if_icmpge       1161
        //  1023: aload_0        
        //  1024: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  1027: iload           6
        //  1029: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  1032: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1035: astore          14
        //  1037: aload           14
        //  1039: getfield        com/android/settings/applications/RunningState$ProcessItem.mRunningSeq:I
        //  1042: aload_0        
        //  1043: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  1046: if_icmpne       1136
        //  1049: aload           14
        //  1051: getfield        com/android/settings/applications/RunningState$ProcessItem.mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //  1054: getfield        android/app/ActivityManager$RunningAppProcessInfo.importanceReasonPid:I
        //  1057: istore          13
        //  1059: iload           13
        //  1061: ifeq            1124
        //  1064: aload_0        
        //  1065: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //  1068: iload           13
        //  1070: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //  1073: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1076: astore          10
        //  1078: aload           10
        //  1080: astore          7
        //  1082: aload           10
        //  1084: ifnonnull       1101
        //  1087: aload_0        
        //  1088: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  1091: iload           13
        //  1093: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //  1096: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1099: astore          7
        //  1101: aload           7
        //  1103: ifnull          1121
        //  1106: aload           7
        //  1108: getfield        com/android/settings/applications/RunningState$ProcessItem.mDependentProcesses:Landroid/util/SparseArray;
        //  1111: aload           14
        //  1113: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  1116: aload           14
        //  1118: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //  1121: goto            1130
        //  1124: aload           14
        //  1126: aconst_null    
        //  1127: putfield        com/android/settings/applications/RunningState$ProcessItem.mClient:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1130: iinc            6, 1
        //  1133: goto            1158
        //  1136: iconst_1       
        //  1137: istore          11
        //  1139: aload_0        
        //  1140: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  1143: aload_0        
        //  1144: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  1147: iload           6
        //  1149: invokevirtual   android/util/SparseArray.keyAt:(I)I
        //  1152: invokevirtual   android/util/SparseArray.remove:(I)V
        //  1155: iinc            8, -1
        //  1158: goto            1016
        //  1161: aload_0        
        //  1162: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //  1165: invokevirtual   java/util/ArrayList.size:()I
        //  1168: istore          6
        //  1170: iconst_0       
        //  1171: istore          13
        //  1173: iload           13
        //  1175: iload           6
        //  1177: if_icmpge       1263
        //  1180: aload_0        
        //  1181: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //  1184: iload           13
        //  1186: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  1189: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1192: astore          7
        //  1194: aload           7
        //  1196: getfield        com/android/settings/applications/RunningState$ProcessItem.mInteresting:Z
        //  1199: ifeq            1225
        //  1202: iload           13
        //  1204: istore          16
        //  1206: iload           6
        //  1208: istore          17
        //  1210: aload_0        
        //  1211: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  1214: aload           7
        //  1216: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  1219: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //  1222: ifnonnull       1250
        //  1225: iconst_1       
        //  1226: istore          11
        //  1228: aload_0        
        //  1229: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //  1232: iload           13
        //  1234: invokevirtual   java/util/ArrayList.remove:(I)Ljava/lang/Object;
        //  1237: pop            
        //  1238: iload           13
        //  1240: iconst_1       
        //  1241: isub           
        //  1242: istore          16
        //  1244: iload           6
        //  1246: iconst_1       
        //  1247: isub           
        //  1248: istore          17
        //  1250: iload           16
        //  1252: iconst_1       
        //  1253: iadd           
        //  1254: istore          13
        //  1256: iload           17
        //  1258: istore          6
        //  1260: goto            1173
        //  1263: aload_0        
        //  1264: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //  1267: invokevirtual   android/util/SparseArray.size:()I
        //  1270: istore          13
        //  1272: iconst_0       
        //  1273: istore          17
        //  1275: iload           17
        //  1277: iload           13
        //  1279: if_icmpge       1338
        //  1282: aload_0        
        //  1283: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //  1286: iload           17
        //  1288: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  1291: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1294: astore          7
        //  1296: iload           11
        //  1298: istore          15
        //  1300: aload           7
        //  1302: getfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //  1305: aload_0        
        //  1306: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  1309: if_icmpne       1328
        //  1312: iload           11
        //  1314: aload           7
        //  1316: aload_1        
        //  1317: aload_3        
        //  1318: aload_0        
        //  1319: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  1322: invokevirtual   com/android/settings/applications/RunningState$ProcessItem.buildDependencyChain:(Landroid/content/Context;Landroid/content/pm/PackageManager;I)Z
        //  1325: ior            
        //  1326: istore          15
        //  1328: iinc            17, 1
        //  1331: iload           15
        //  1333: istore          11
        //  1335: goto            1275
        //  1338: aconst_null    
        //  1339: astore          7
        //  1341: iconst_0       
        //  1342: istore          17
        //  1344: aload_2        
        //  1345: astore          14
        //  1347: aload_3        
        //  1348: astore          10
        //  1350: iload           17
        //  1352: aload_0        
        //  1353: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //  1356: invokevirtual   android/util/SparseArray.size:()I
        //  1359: if_icmpge       1604
        //  1362: aload_0        
        //  1363: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //  1366: iload           17
        //  1368: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  1371: checkcast       Ljava/util/HashMap;
        //  1374: astore_3       
        //  1375: aload_3        
        //  1376: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //  1379: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //  1384: astore          12
        //  1386: aload           7
        //  1388: astore_2       
        //  1389: aload           10
        //  1391: astore          7
        //  1393: aload           12
        //  1395: invokeinterface java/util/Iterator.hasNext:()Z
        //  1400: ifeq            1591
        //  1403: aload           12
        //  1405: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1410: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1413: astore          18
        //  1415: aload           18
        //  1417: getfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //  1420: aload_0        
        //  1421: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  1424: if_icmpne       1511
        //  1427: aload           18
        //  1429: aload           7
        //  1431: invokevirtual   com/android/settings/applications/RunningState$ProcessItem.ensureLabel:(Landroid/content/pm/PackageManager;)V
        //  1434: aload           18
        //  1436: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  1439: ifne            1450
        //  1442: aload           18
        //  1444: getfield        com/android/settings/applications/RunningState$ProcessItem.mDependentProcesses:Landroid/util/SparseArray;
        //  1447: invokevirtual   android/util/SparseArray.clear:()V
        //  1450: aload           18
        //  1452: getfield        com/android/settings/applications/RunningState$ProcessItem.mServices:Ljava/util/HashMap;
        //  1455: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //  1458: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //  1463: astore          10
        //  1465: aload           10
        //  1467: invokeinterface java/util/Iterator.hasNext:()Z
        //  1472: ifeq            1508
        //  1475: aload           10
        //  1477: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1482: checkcast       Lcom/android/settings/applications/RunningState$ServiceItem;
        //  1485: getfield        com/android/settings/applications/RunningState$ServiceItem.mCurSeq:I
        //  1488: aload_0        
        //  1489: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  1492: if_icmpeq       1505
        //  1495: aload           10
        //  1497: invokeinterface java/util/Iterator.remove:()V
        //  1502: iconst_1       
        //  1503: istore          11
        //  1505: goto            1465
        //  1508: goto            1588
        //  1511: iconst_1       
        //  1512: istore          11
        //  1514: aload           12
        //  1516: invokeinterface java/util/Iterator.remove:()V
        //  1521: aload_2        
        //  1522: astore          10
        //  1524: aload_3        
        //  1525: invokevirtual   java/util/HashMap.size:()I
        //  1528: ifne            1565
        //  1531: aload_2        
        //  1532: astore          10
        //  1534: aload_2        
        //  1535: ifnonnull       1547
        //  1538: new             Ljava/util/ArrayList;
        //  1541: dup            
        //  1542: invokespecial   java/util/ArrayList.<init>:()V
        //  1545: astore          10
        //  1547: aload           10
        //  1549: aload_0        
        //  1550: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //  1553: iload           17
        //  1555: invokevirtual   android/util/SparseArray.keyAt:(I)I
        //  1558: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //  1561: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  1564: pop            
        //  1565: aload           18
        //  1567: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  1570: ifeq            1585
        //  1573: aload_0        
        //  1574: getfield        com/android/settings/applications/RunningState.mServiceProcessesByPid:Landroid/util/SparseArray;
        //  1577: aload           18
        //  1579: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  1582: invokevirtual   android/util/SparseArray.remove:(I)V
        //  1585: aload           10
        //  1587: astore_2       
        //  1588: goto            1393
        //  1591: iinc            17, 1
        //  1594: aload           7
        //  1596: astore          10
        //  1598: aload_2        
        //  1599: astore          7
        //  1601: goto            1350
        //  1604: aload           7
        //  1606: ifnull          1652
        //  1609: iconst_0       
        //  1610: istore          17
        //  1612: iload           17
        //  1614: aload           7
        //  1616: invokevirtual   java/util/ArrayList.size:()I
        //  1619: if_icmpge       1652
        //  1622: aload           7
        //  1624: iload           17
        //  1626: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  1629: checkcast       Ljava/lang/Integer;
        //  1632: invokevirtual   java/lang/Integer.intValue:()I
        //  1635: istore          16
        //  1637: aload_0        
        //  1638: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //  1641: iload           16
        //  1643: invokevirtual   android/util/SparseArray.remove:(I)V
        //  1646: iinc            17, 1
        //  1649: goto            1612
        //  1652: iload           11
        //  1654: ifeq            2582
        //  1657: new             Ljava/util/ArrayList;
        //  1660: dup            
        //  1661: invokespecial   java/util/ArrayList.<init>:()V
        //  1664: astore          4
        //  1666: iconst_0       
        //  1667: istore          16
        //  1669: iload           5
        //  1671: istore          17
        //  1673: iload           16
        //  1675: istore          5
        //  1677: iload           5
        //  1679: aload_0        
        //  1680: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //  1683: invokevirtual   android/util/SparseArray.size:()I
        //  1686: if_icmpge       1907
        //  1689: aload_0        
        //  1690: getfield        com/android/settings/applications/RunningState.mServiceProcessesByName:Landroid/util/SparseArray;
        //  1693: iload           5
        //  1695: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  1698: checkcast       Ljava/util/HashMap;
        //  1701: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //  1704: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //  1709: astore          10
        //  1711: iload           8
        //  1713: istore          16
        //  1715: aload           10
        //  1717: invokeinterface java/util/Iterator.hasNext:()Z
        //  1722: ifeq            1897
        //  1725: aload           10
        //  1727: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1732: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1735: astore          14
        //  1737: aload           14
        //  1739: iconst_0       
        //  1740: putfield        com/android/settings/applications/RunningState$ProcessItem.mIsSystem:Z
        //  1743: aload           14
        //  1745: iconst_1       
        //  1746: putfield        com/android/settings/applications/RunningState$ProcessItem.mIsStarted:Z
        //  1749: aload           14
        //  1751: ldc2_w          9223372036854775807
        //  1754: putfield        com/android/settings/applications/RunningState$ProcessItem.mActiveSince:J
        //  1757: aload           14
        //  1759: getfield        com/android/settings/applications/RunningState$ProcessItem.mServices:Ljava/util/HashMap;
        //  1762: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //  1765: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //  1770: astore_2       
        //  1771: iload           6
        //  1773: istore          8
        //  1775: aload_2        
        //  1776: invokeinterface java/util/Iterator.hasNext:()Z
        //  1781: ifeq            1882
        //  1784: aload_2        
        //  1785: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1790: checkcast       Lcom/android/settings/applications/RunningState$ServiceItem;
        //  1793: astore_3       
        //  1794: aload_3        
        //  1795: getfield        com/android/settings/applications/RunningState$ServiceItem.mServiceInfo:Landroid/content/pm/ServiceInfo;
        //  1798: ifnull          1825
        //  1801: aload_3        
        //  1802: getfield        com/android/settings/applications/RunningState$ServiceItem.mServiceInfo:Landroid/content/pm/ServiceInfo;
        //  1805: getfield        android/content/pm/ServiceInfo.applicationInfo:Landroid/content/pm/ApplicationInfo;
        //  1808: getfield        android/content/pm/ApplicationInfo.flags:I
        //  1811: iconst_1       
        //  1812: iand           
        //  1813: ifeq            1825
        //  1816: aload           14
        //  1818: iconst_1       
        //  1819: putfield        com/android/settings/applications/RunningState$ProcessItem.mIsSystem:Z
        //  1822: goto            1825
        //  1825: aload_3        
        //  1826: getfield        com/android/settings/applications/RunningState$ServiceItem.mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;
        //  1829: ifnull          1879
        //  1832: aload_3        
        //  1833: getfield        com/android/settings/applications/RunningState$ServiceItem.mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;
        //  1836: getfield        android/app/ActivityManager$RunningServiceInfo.clientLabel:I
        //  1839: ifeq            1879
        //  1842: aload           14
        //  1844: iconst_0       
        //  1845: putfield        com/android/settings/applications/RunningState$ProcessItem.mIsStarted:Z
        //  1848: aload           14
        //  1850: getfield        com/android/settings/applications/RunningState$ProcessItem.mActiveSince:J
        //  1853: aload_3        
        //  1854: getfield        com/android/settings/applications/RunningState$ServiceItem.mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;
        //  1857: getfield        android/app/ActivityManager$RunningServiceInfo.activeSince:J
        //  1860: lcmp           
        //  1861: ifle            1879
        //  1864: aload           14
        //  1866: aload_3        
        //  1867: getfield        com/android/settings/applications/RunningState$ServiceItem.mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;
        //  1870: getfield        android/app/ActivityManager$RunningServiceInfo.activeSince:J
        //  1873: putfield        com/android/settings/applications/RunningState$ProcessItem.mActiveSince:J
        //  1876: goto            1879
        //  1879: goto            1775
        //  1882: aload           4
        //  1884: aload           14
        //  1886: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  1889: pop            
        //  1890: iload           8
        //  1892: istore          6
        //  1894: goto            1715
        //  1897: iinc            5, 1
        //  1900: iload           16
        //  1902: istore          8
        //  1904: goto            1677
        //  1907: aload           4
        //  1909: aload_0        
        //  1910: getfield        com/android/settings/applications/RunningState.mServiceProcessComparator:Lcom/android/settings/applications/RunningState$ServiceProcessComparator;
        //  1913: invokestatic    java/util/Collections.sort:(Ljava/util/List;Ljava/util/Comparator;)V
        //  1916: new             Ljava/util/ArrayList;
        //  1919: dup            
        //  1920: invokespecial   java/util/ArrayList.<init>:()V
        //  1923: astore_3       
        //  1924: new             Ljava/util/ArrayList;
        //  1927: dup            
        //  1928: invokespecial   java/util/ArrayList.<init>:()V
        //  1931: astore          14
        //  1933: aload_0        
        //  1934: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  1937: invokevirtual   java/util/ArrayList.clear:()V
        //  1940: iconst_0       
        //  1941: istore          8
        //  1943: aload           4
        //  1945: astore_2       
        //  1946: iload           8
        //  1948: aload_2        
        //  1949: invokevirtual   java/util/ArrayList.size:()I
        //  1952: if_icmpge       2352
        //  1955: aload_2        
        //  1956: iload           8
        //  1958: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  1961: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  1964: astore          12
        //  1966: aload           12
        //  1968: iconst_0       
        //  1969: putfield        com/android/settings/applications/RunningState$ProcessItem.mNeedDivider:Z
        //  1972: aload_0        
        //  1973: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  1976: invokevirtual   java/util/ArrayList.size:()I
        //  1979: istore          5
        //  1981: aload           12
        //  1983: aload_3        
        //  1984: aload_0        
        //  1985: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  1988: invokevirtual   com/android/settings/applications/RunningState$ProcessItem.addDependentProcesses:(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
        //  1991: aload_3        
        //  1992: aload           12
        //  1994: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  1997: pop            
        //  1998: aload           12
        //  2000: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  2003: ifle            2016
        //  2006: aload_0        
        //  2007: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  2010: aload           12
        //  2012: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2015: pop            
        //  2016: aconst_null    
        //  2017: astore          10
        //  2019: iconst_0       
        //  2020: istore          15
        //  2022: iconst_0       
        //  2023: istore          19
        //  2025: aload           12
        //  2027: getfield        com/android/settings/applications/RunningState$ProcessItem.mServices:Ljava/util/HashMap;
        //  2030: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //  2033: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //  2038: astore          4
        //  2040: aload           4
        //  2042: invokeinterface java/util/Iterator.hasNext:()Z
        //  2047: ifeq            2137
        //  2050: aload           4
        //  2052: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2057: checkcast       Lcom/android/settings/applications/RunningState$ServiceItem;
        //  2060: astore          18
        //  2062: aload           18
        //  2064: iload           19
        //  2066: putfield        com/android/settings/applications/RunningState$ServiceItem.mNeedDivider:Z
        //  2069: iconst_1       
        //  2070: istore          20
        //  2072: aload_3        
        //  2073: aload           18
        //  2075: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2078: pop            
        //  2079: aload           18
        //  2081: getfield        com/android/settings/applications/RunningState$ServiceItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2084: ifnull          2127
        //  2087: iload           15
        //  2089: istore          19
        //  2091: aload           10
        //  2093: ifnull          2113
        //  2096: iload           15
        //  2098: istore          19
        //  2100: aload           10
        //  2102: aload           18
        //  2104: getfield        com/android/settings/applications/RunningState$ServiceItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2107: if_acmpeq       2113
        //  2110: iconst_0       
        //  2111: istore          19
        //  2113: aload           18
        //  2115: getfield        com/android/settings/applications/RunningState$ServiceItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2118: astore          10
        //  2120: iload           19
        //  2122: istore          15
        //  2124: goto            2130
        //  2127: iconst_0       
        //  2128: istore          15
        //  2130: iload           20
        //  2132: istore          19
        //  2134: goto            2040
        //  2137: iload           15
        //  2139: ifeq            2170
        //  2142: aload           10
        //  2144: ifnull          2170
        //  2147: aload           10
        //  2149: astore          4
        //  2151: aload           10
        //  2153: getfield        com/android/settings/applications/RunningState$MergedItem.mServices:Ljava/util/ArrayList;
        //  2156: invokevirtual   java/util/ArrayList.size:()I
        //  2159: aload           12
        //  2161: getfield        com/android/settings/applications/RunningState$ProcessItem.mServices:Ljava/util/HashMap;
        //  2164: invokevirtual   java/util/HashMap.size:()I
        //  2167: if_icmpeq       2302
        //  2170: new             Lcom/android/settings/applications/RunningState$MergedItem;
        //  2173: dup            
        //  2174: aload           12
        //  2176: getfield        com/android/settings/applications/RunningState$ProcessItem.mUserId:I
        //  2179: invokespecial   com/android/settings/applications/RunningState$MergedItem.<init>:(I)V
        //  2182: astore          10
        //  2184: aload           12
        //  2186: getfield        com/android/settings/applications/RunningState$ProcessItem.mServices:Ljava/util/HashMap;
        //  2189: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //  2192: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //  2197: astore          18
        //  2199: aload           18
        //  2201: invokeinterface java/util/Iterator.hasNext:()Z
        //  2206: ifeq            2242
        //  2209: aload           18
        //  2211: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2216: checkcast       Lcom/android/settings/applications/RunningState$ServiceItem;
        //  2219: astore          4
        //  2221: aload           10
        //  2223: getfield        com/android/settings/applications/RunningState$MergedItem.mServices:Ljava/util/ArrayList;
        //  2226: aload           4
        //  2228: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2231: pop            
        //  2232: aload           4
        //  2234: aload           10
        //  2236: putfield        com/android/settings/applications/RunningState$ServiceItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2239: goto            2199
        //  2242: aload           10
        //  2244: aload           12
        //  2246: putfield        com/android/settings/applications/RunningState$MergedItem.mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2249: aload           10
        //  2251: getfield        com/android/settings/applications/RunningState$MergedItem.mOtherProcesses:Ljava/util/ArrayList;
        //  2254: invokevirtual   java/util/ArrayList.clear:()V
        //  2257: aload           10
        //  2259: astore          4
        //  2261: iload           5
        //  2263: aload_0        
        //  2264: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  2267: invokevirtual   java/util/ArrayList.size:()I
        //  2270: iconst_1       
        //  2271: isub           
        //  2272: if_icmpge       2302
        //  2275: aload           10
        //  2277: getfield        com/android/settings/applications/RunningState$MergedItem.mOtherProcesses:Ljava/util/ArrayList;
        //  2280: aload_0        
        //  2281: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  2284: iload           5
        //  2286: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  2289: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2292: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2295: pop            
        //  2296: iinc            5, 1
        //  2299: goto            2257
        //  2302: aload           4
        //  2304: aload_1        
        //  2305: iconst_0       
        //  2306: invokevirtual   com/android/settings/applications/RunningState$MergedItem.update:(Landroid/content/Context;Z)Z
        //  2309: pop            
        //  2310: aload           4
        //  2312: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  2315: aload_0        
        //  2316: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  2319: if_icmpeq       2338
        //  2322: aload_0        
        //  2323: aload_1        
        //  2324: aload           14
        //  2326: aload_0        
        //  2327: getfield        com/android/settings/applications/RunningState.mOtherUserMergedItems:Landroid/util/SparseArray;
        //  2330: aload           4
        //  2332: invokespecial   com/android/settings/applications/RunningState.addOtherUserItem:(Landroid/content/Context;Ljava/util/ArrayList;Landroid/util/SparseArray;Lcom/android/settings/applications/RunningState$MergedItem;)V
        //  2335: goto            2346
        //  2338: aload           14
        //  2340: aload           4
        //  2342: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2345: pop            
        //  2346: iinc            8, 1
        //  2349: goto            1946
        //  2352: aload_0        
        //  2353: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //  2356: invokevirtual   java/util/ArrayList.size:()I
        //  2359: istore          8
        //  2361: iconst_0       
        //  2362: istore          5
        //  2364: iload           5
        //  2366: iload           8
        //  2368: if_icmpge       2498
        //  2371: aload_0        
        //  2372: getfield        com/android/settings/applications/RunningState.mInterestingProcesses:Ljava/util/ArrayList;
        //  2375: iload           5
        //  2377: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  2380: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2383: astore_2       
        //  2384: aload_2        
        //  2385: getfield        com/android/settings/applications/RunningState$ProcessItem.mClient:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2388: ifnonnull       2492
        //  2391: aload_2        
        //  2392: getfield        com/android/settings/applications/RunningState$ProcessItem.mServices:Ljava/util/HashMap;
        //  2395: invokevirtual   java/util/HashMap.size:()I
        //  2398: ifgt            2492
        //  2401: aload_2        
        //  2402: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2405: ifnonnull       2431
        //  2408: aload_2        
        //  2409: new             Lcom/android/settings/applications/RunningState$MergedItem;
        //  2412: dup            
        //  2413: aload_2        
        //  2414: getfield        com/android/settings/applications/RunningState$ProcessItem.mUserId:I
        //  2417: invokespecial   com/android/settings/applications/RunningState$MergedItem.<init>:(I)V
        //  2420: putfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2423: aload_2        
        //  2424: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2427: aload_2        
        //  2428: putfield        com/android/settings/applications/RunningState$MergedItem.mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2431: aload_2        
        //  2432: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2435: aload_1        
        //  2436: iconst_0       
        //  2437: invokevirtual   com/android/settings/applications/RunningState$MergedItem.update:(Landroid/content/Context;Z)Z
        //  2440: pop            
        //  2441: aload_2        
        //  2442: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2445: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  2448: aload_0        
        //  2449: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  2452: if_icmpeq       2473
        //  2455: aload_0        
        //  2456: aload_1        
        //  2457: aload           14
        //  2459: aload_0        
        //  2460: getfield        com/android/settings/applications/RunningState.mOtherUserMergedItems:Landroid/util/SparseArray;
        //  2463: aload_2        
        //  2464: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2467: invokespecial   com/android/settings/applications/RunningState.addOtherUserItem:(Landroid/content/Context;Ljava/util/ArrayList;Landroid/util/SparseArray;Lcom/android/settings/applications/RunningState$MergedItem;)V
        //  2470: goto            2483
        //  2473: aload           14
        //  2475: iconst_0       
        //  2476: aload_2        
        //  2477: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  2480: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //  2483: aload_0        
        //  2484: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  2487: aload_2        
        //  2488: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2491: pop            
        //  2492: iinc            5, 1
        //  2495: goto            2364
        //  2498: aload_0        
        //  2499: getfield        com/android/settings/applications/RunningState.mOtherUserMergedItems:Landroid/util/SparseArray;
        //  2502: invokevirtual   android/util/SparseArray.size:()I
        //  2505: istore          8
        //  2507: iconst_0       
        //  2508: istore          5
        //  2510: iload           5
        //  2512: iload           8
        //  2514: if_icmpge       2554
        //  2517: aload_0        
        //  2518: getfield        com/android/settings/applications/RunningState.mOtherUserMergedItems:Landroid/util/SparseArray;
        //  2521: iload           5
        //  2523: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  2526: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  2529: astore_2       
        //  2530: aload_2        
        //  2531: getfield        com/android/settings/applications/RunningState$MergedItem.mCurSeq:I
        //  2534: aload_0        
        //  2535: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  2538: if_icmpne       2548
        //  2541: aload_2        
        //  2542: aload_1        
        //  2543: iconst_0       
        //  2544: invokevirtual   com/android/settings/applications/RunningState$MergedItem.update:(Landroid/content/Context;Z)Z
        //  2547: pop            
        //  2548: iinc            5, 1
        //  2551: goto            2510
        //  2554: aload_0        
        //  2555: getfield        com/android/settings/applications/RunningState.mLock:Ljava/lang/Object;
        //  2558: astore_2       
        //  2559: aload_2        
        //  2560: monitorenter   
        //  2561: aload_0        
        //  2562: aload_3        
        //  2563: putfield        com/android/settings/applications/RunningState.mItems:Ljava/util/ArrayList;
        //  2566: aload_0        
        //  2567: aload           14
        //  2569: putfield        com/android/settings/applications/RunningState.mMergedItems:Ljava/util/ArrayList;
        //  2572: aload_2        
        //  2573: monitorexit    
        //  2574: goto            2582
        //  2577: astore_1       
        //  2578: aload_2        
        //  2579: monitorexit    
        //  2580: aload_1        
        //  2581: athrow         
        //  2582: aload_0        
        //  2583: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2586: invokevirtual   java/util/ArrayList.clear:()V
        //  2589: aload_0        
        //  2590: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2593: aload_0        
        //  2594: getfield        com/android/settings/applications/RunningState.mProcessItems:Ljava/util/ArrayList;
        //  2597: invokevirtual   java/util/ArrayList.addAll:(Ljava/util/Collection;)Z
        //  2600: pop            
        //  2601: iconst_0       
        //  2602: istore          17
        //  2604: aload_0        
        //  2605: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  2608: invokevirtual   android/util/SparseArray.size:()I
        //  2611: istore          21
        //  2613: iconst_0       
        //  2614: istore          6
        //  2616: iconst_0       
        //  2617: istore          16
        //  2619: iconst_0       
        //  2620: istore          5
        //  2622: iload           5
        //  2624: iload           21
        //  2626: if_icmpge       2782
        //  2629: aload_0        
        //  2630: getfield        com/android/settings/applications/RunningState.mRunningProcesses:Landroid/util/SparseArray;
        //  2633: iload           5
        //  2635: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  2638: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2641: astore          10
        //  2643: aload           10
        //  2645: getfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //  2648: aload_0        
        //  2649: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  2652: if_icmpeq       2773
        //  2655: aload           10
        //  2657: getfield        com/android/settings/applications/RunningState$ProcessItem.mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //  2660: getfield        android/app/ActivityManager$RunningAppProcessInfo.importance:I
        //  2663: sipush          400
        //  2666: if_icmplt       2685
        //  2669: iinc            16, 1
        //  2672: aload_0        
        //  2673: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2676: aload           10
        //  2678: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2681: pop            
        //  2682: goto            2776
        //  2685: aload           10
        //  2687: getfield        com/android/settings/applications/RunningState$ProcessItem.mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //  2690: getfield        android/app/ActivityManager$RunningAppProcessInfo.importance:I
        //  2693: sipush          200
        //  2696: if_icmpgt       2715
        //  2699: iinc            6, 1
        //  2702: aload_0        
        //  2703: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2706: aload           10
        //  2708: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2711: pop            
        //  2712: goto            2776
        //  2715: new             Ljava/lang/StringBuilder;
        //  2718: dup            
        //  2719: invokespecial   java/lang/StringBuilder.<init>:()V
        //  2722: astore_2       
        //  2723: aload_2        
        //  2724: ldc_w           "Unknown non-service process: "
        //  2727: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  2730: pop            
        //  2731: aload_2        
        //  2732: aload           10
        //  2734: getfield        com/android/settings/applications/RunningState$ProcessItem.mProcessName:Ljava/lang/String;
        //  2737: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  2740: pop            
        //  2741: aload_2        
        //  2742: ldc_w           " #"
        //  2745: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  2748: pop            
        //  2749: aload_2        
        //  2750: aload           10
        //  2752: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  2755: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  2758: pop            
        //  2759: ldc_w           "RunningState"
        //  2762: aload_2        
        //  2763: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  2766: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //  2769: pop            
        //  2770: goto            2776
        //  2773: iinc            17, 1
        //  2776: iinc            5, 1
        //  2779: goto            2622
        //  2782: lconst_0       
        //  2783: lstore          22
        //  2785: lconst_0       
        //  2786: lstore          24
        //  2788: lconst_0       
        //  2789: lstore          26
        //  2791: iconst_0       
        //  2792: istore          28
        //  2794: iconst_0       
        //  2795: istore          29
        //  2797: iconst_0       
        //  2798: istore          30
        //  2800: aload_0        
        //  2801: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2804: invokevirtual   java/util/ArrayList.size:()I
        //  2807: istore          5
        //  2809: iload           5
        //  2811: newarray        I
        //  2813: astore          10
        //  2815: iconst_0       
        //  2816: istore          8
        //  2818: iload           8
        //  2820: iload           5
        //  2822: if_icmpge       2862
        //  2825: aload           10
        //  2827: iload           8
        //  2829: aload_0        
        //  2830: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2833: iload           8
        //  2835: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  2838: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2841: getfield        com/android/settings/applications/RunningState$ProcessItem.mPid:I
        //  2844: iastore        
        //  2845: iinc            8, 1
        //  2848: goto            2818
        //  2851: astore_2       
        //  2852: aconst_null    
        //  2853: astore          7
        //  2855: iload           29
        //  2857: istore          5
        //  2859: goto            3787
        //  2862: invokestatic    android/app/ActivityManager.getService:()Landroid/app/IActivityManager;
        //  2865: aload           10
        //  2867: invokeinterface android/app/IActivityManager.getProcessPss:([I)[J
        //  2872: astore          14
        //  2874: aconst_null    
        //  2875: astore_2       
        //  2876: iconst_0       
        //  2877: istore          8
        //  2879: iconst_0       
        //  2880: istore          29
        //  2882: iload           30
        //  2884: istore          5
        //  2886: aload           7
        //  2888: astore          4
        //  2890: iload           9
        //  2892: istore          30
        //  2894: aload           10
        //  2896: astore          7
        //  2898: iload           29
        //  2900: aload           7
        //  2902: arraylength    
        //  2903: if_icmpge       3765
        //  2906: aload_0        
        //  2907: getfield        com/android/settings/applications/RunningState.mAllProcessItems:Ljava/util/ArrayList;
        //  2910: iload           29
        //  2912: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  2915: checkcast       Lcom/android/settings/applications/RunningState$ProcessItem;
        //  2918: astore          18
        //  2920: aload_2        
        //  2921: astore          10
        //  2923: lload           22
        //  2925: lstore          31
        //  2927: iload           5
        //  2929: istore          9
        //  2931: iload           11
        //  2933: istore          15
        //  2935: iload           11
        //  2937: aload           18
        //  2939: aload_1        
        //  2940: aload           14
        //  2942: iload           29
        //  2944: laload         
        //  2945: aload_0        
        //  2946: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  2949: invokevirtual   com/android/settings/applications/RunningState$ProcessItem.updateSize:(Landroid/content/Context;JI)Z
        //  2952: ior            
        //  2953: istore          11
        //  2955: aload_2        
        //  2956: astore          10
        //  2958: lload           22
        //  2960: lstore          31
        //  2962: iload           5
        //  2964: istore          9
        //  2966: iload           11
        //  2968: istore          15
        //  2970: aload           18
        //  2972: getfield        com/android/settings/applications/RunningState$ProcessItem.mCurSeq:I
        //  2975: aload_0        
        //  2976: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  2979: if_icmpne       3036
        //  2982: aload_2        
        //  2983: astore          10
        //  2985: lload           22
        //  2987: lstore          31
        //  2989: iload           5
        //  2991: istore          9
        //  2993: iload           11
        //  2995: istore          15
        //  2997: lload           26
        //  2999: aload           18
        //  3001: getfield        com/android/settings/applications/RunningState$ProcessItem.mSize:J
        //  3004: ladd           
        //  3005: lstore          33
        //  3007: lload           33
        //  3009: lstore          26
        //  3011: iload           8
        //  3013: istore          35
        //  3015: aload_2        
        //  3016: astore_3       
        //  3017: lload           22
        //  3019: lstore          33
        //  3021: lload           24
        //  3023: lstore          36
        //  3025: lload           26
        //  3027: lstore          38
        //  3029: iload           5
        //  3031: istore          40
        //  3033: goto            3717
        //  3036: aload_2        
        //  3037: astore          10
        //  3039: lload           22
        //  3041: lstore          31
        //  3043: iload           5
        //  3045: istore          9
        //  3047: iload           11
        //  3049: istore          15
        //  3051: aload           18
        //  3053: getfield        com/android/settings/applications/RunningState$ProcessItem.mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //  3056: getfield        android/app/ActivityManager$RunningAppProcessInfo.importance:I
        //  3059: sipush          400
        //  3062: if_icmplt       3607
        //  3065: aload_2        
        //  3066: astore          10
        //  3068: lload           22
        //  3070: lstore          31
        //  3072: iload           5
        //  3074: istore          9
        //  3076: iload           11
        //  3078: istore          15
        //  3080: lload           22
        //  3082: aload           18
        //  3084: getfield        com/android/settings/applications/RunningState$ProcessItem.mSize:J
        //  3087: ladd           
        //  3088: lstore          22
        //  3090: aload_2        
        //  3091: ifnull          3255
        //  3094: aload_2        
        //  3095: astore          10
        //  3097: lload           22
        //  3099: lstore          31
        //  3101: iload           5
        //  3103: istore          9
        //  3105: iload           11
        //  3107: istore          15
        //  3109: new             Lcom/android/settings/applications/RunningState$MergedItem;
        //  3112: astore_3       
        //  3113: aload_2        
        //  3114: astore          10
        //  3116: lload           22
        //  3118: lstore          31
        //  3120: iload           5
        //  3122: istore          9
        //  3124: iload           11
        //  3126: istore          15
        //  3128: aload_3        
        //  3129: aload           18
        //  3131: getfield        com/android/settings/applications/RunningState$ProcessItem.mUserId:I
        //  3134: invokespecial   com/android/settings/applications/RunningState$MergedItem.<init>:(I)V
        //  3137: aload_2        
        //  3138: astore          10
        //  3140: lload           22
        //  3142: lstore          31
        //  3144: iload           5
        //  3146: istore          9
        //  3148: iload           11
        //  3150: istore          15
        //  3152: aload           18
        //  3154: aload_3        
        //  3155: putfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  3158: aload_2        
        //  3159: astore          10
        //  3161: lload           22
        //  3163: lstore          31
        //  3165: iload           5
        //  3167: istore          9
        //  3169: iload           11
        //  3171: istore          15
        //  3173: aload           18
        //  3175: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  3178: aload           18
        //  3180: putfield        com/android/settings/applications/RunningState$MergedItem.mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  3183: aload_2        
        //  3184: astore          10
        //  3186: lload           22
        //  3188: lstore          31
        //  3190: iload           5
        //  3192: istore          9
        //  3194: iload           11
        //  3196: istore          15
        //  3198: aload_3        
        //  3199: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  3202: aload_0        
        //  3203: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  3206: if_icmpeq       3215
        //  3209: iconst_1       
        //  3210: istore          9
        //  3212: goto            3218
        //  3215: iconst_0       
        //  3216: istore          9
        //  3218: iload           5
        //  3220: iload           9
        //  3222: ior            
        //  3223: istore          5
        //  3225: aload_2        
        //  3226: astore          10
        //  3228: lload           22
        //  3230: lstore          31
        //  3232: iload           5
        //  3234: istore          9
        //  3236: iload           11
        //  3238: istore          15
        //  3240: aload_2        
        //  3241: aload_3        
        //  3242: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  3245: pop            
        //  3246: aload_3        
        //  3247: astore          10
        //  3249: aload           10
        //  3251: astore_3       
        //  3252: goto            3558
        //  3255: aload_2        
        //  3256: astore          10
        //  3258: lload           22
        //  3260: lstore          31
        //  3262: iload           5
        //  3264: istore          9
        //  3266: iload           11
        //  3268: istore          15
        //  3270: iload           8
        //  3272: aload_0        
        //  3273: getfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  3276: invokevirtual   java/util/ArrayList.size:()I
        //  3279: if_icmpge       3354
        //  3282: aload_2        
        //  3283: astore          10
        //  3285: lload           22
        //  3287: lstore          31
        //  3289: iload           5
        //  3291: istore          9
        //  3293: iload           11
        //  3295: istore          15
        //  3297: aload_0        
        //  3298: getfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  3301: iload           8
        //  3303: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  3306: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  3309: getfield        com/android/settings/applications/RunningState$MergedItem.mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  3312: aload           18
        //  3314: if_acmpeq       3320
        //  3317: goto            3354
        //  3320: aload_2        
        //  3321: astore          10
        //  3323: lload           22
        //  3325: lstore          31
        //  3327: iload           5
        //  3329: istore          9
        //  3331: iload           11
        //  3333: istore          15
        //  3335: aload_0        
        //  3336: getfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  3339: iload           8
        //  3341: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  3344: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  3347: astore_3       
        //  3348: aload_3        
        //  3349: astore          10
        //  3351: goto            3249
        //  3354: aload_2        
        //  3355: astore          10
        //  3357: lload           22
        //  3359: lstore          31
        //  3361: iload           5
        //  3363: istore          9
        //  3365: iload           11
        //  3367: istore          15
        //  3369: new             Ljava/util/ArrayList;
        //  3372: dup            
        //  3373: iload           16
        //  3375: invokespecial   java/util/ArrayList.<init>:(I)V
        //  3378: astore_2       
        //  3379: iconst_0       
        //  3380: istore          35
        //  3382: iload           5
        //  3384: istore          9
        //  3386: iload           35
        //  3388: iload           8
        //  3390: if_icmpge       3468
        //  3393: iload           9
        //  3395: istore          5
        //  3397: aload_0        
        //  3398: getfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  3401: iload           35
        //  3403: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  3406: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  3409: astore          10
        //  3411: iload           9
        //  3413: istore          5
        //  3415: aload           10
        //  3417: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  3420: aload_0        
        //  3421: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  3424: if_icmpeq       3433
        //  3427: iconst_1       
        //  3428: istore          5
        //  3430: goto            3436
        //  3433: iconst_0       
        //  3434: istore          5
        //  3436: iload           9
        //  3438: iload           5
        //  3440: ior            
        //  3441: istore          9
        //  3443: iload           9
        //  3445: istore          5
        //  3447: aload_2        
        //  3448: aload           10
        //  3450: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  3453: pop            
        //  3454: iinc            35, 1
        //  3457: goto            3386
        //  3460: astore          7
        //  3462: aload_2        
        //  3463: astore          7
        //  3465: goto            3787
        //  3468: iload           9
        //  3470: istore          5
        //  3472: new             Lcom/android/settings/applications/RunningState$MergedItem;
        //  3475: astore_3       
        //  3476: iload           9
        //  3478: istore          5
        //  3480: aload_3        
        //  3481: aload           18
        //  3483: getfield        com/android/settings/applications/RunningState$ProcessItem.mUserId:I
        //  3486: invokespecial   com/android/settings/applications/RunningState$MergedItem.<init>:(I)V
        //  3489: iload           9
        //  3491: istore          5
        //  3493: aload           18
        //  3495: aload_3        
        //  3496: putfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  3499: iload           9
        //  3501: istore          5
        //  3503: aload           18
        //  3505: getfield        com/android/settings/applications/RunningState$ProcessItem.mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;
        //  3508: aload           18
        //  3510: putfield        com/android/settings/applications/RunningState$MergedItem.mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;
        //  3513: iload           9
        //  3515: istore          5
        //  3517: aload_3        
        //  3518: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  3521: aload_0        
        //  3522: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  3525: if_icmpeq       3534
        //  3528: iconst_1       
        //  3529: istore          5
        //  3531: goto            3537
        //  3534: iconst_0       
        //  3535: istore          5
        //  3537: iload           9
        //  3539: iload           5
        //  3541: ior            
        //  3542: istore          9
        //  3544: iload           9
        //  3546: istore          5
        //  3548: aload_2        
        //  3549: aload_3        
        //  3550: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  3553: pop            
        //  3554: iload           9
        //  3556: istore          5
        //  3558: aload_2        
        //  3559: astore          10
        //  3561: lload           22
        //  3563: lstore          31
        //  3565: iload           5
        //  3567: istore          9
        //  3569: iload           11
        //  3571: istore          15
        //  3573: aload_3        
        //  3574: aload_1        
        //  3575: iconst_1       
        //  3576: invokevirtual   com/android/settings/applications/RunningState$MergedItem.update:(Landroid/content/Context;Z)Z
        //  3579: pop            
        //  3580: aload_2        
        //  3581: astore          10
        //  3583: lload           22
        //  3585: lstore          31
        //  3587: iload           5
        //  3589: istore          9
        //  3591: iload           11
        //  3593: istore          15
        //  3595: aload_3        
        //  3596: aload_1        
        //  3597: invokevirtual   com/android/settings/applications/RunningState$MergedItem.updateSize:(Landroid/content/Context;)Z
        //  3600: pop            
        //  3601: iinc            8, 1
        //  3604: goto            3011
        //  3607: aload           7
        //  3609: astore          12
        //  3611: iload           8
        //  3613: istore          35
        //  3615: aload_2        
        //  3616: astore_3       
        //  3617: lload           22
        //  3619: lstore          33
        //  3621: lload           24
        //  3623: lstore          36
        //  3625: lload           26
        //  3627: lstore          38
        //  3629: iload           5
        //  3631: istore          40
        //  3633: aload           12
        //  3635: astore          7
        //  3637: aload_2        
        //  3638: astore          10
        //  3640: lload           22
        //  3642: lstore          31
        //  3644: iload           5
        //  3646: istore          9
        //  3648: iload           11
        //  3650: istore          15
        //  3652: aload           18
        //  3654: getfield        com/android/settings/applications/RunningState$ProcessItem.mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;
        //  3657: getfield        android/app/ActivityManager$RunningAppProcessInfo.importance:I
        //  3660: sipush          200
        //  3663: if_icmpgt       3717
        //  3666: aload_2        
        //  3667: astore          10
        //  3669: lload           22
        //  3671: lstore          31
        //  3673: iload           5
        //  3675: istore          9
        //  3677: iload           11
        //  3679: istore          15
        //  3681: aload           18
        //  3683: getfield        com/android/settings/applications/RunningState$ProcessItem.mSize:J
        //  3686: lstore          33
        //  3688: lload           24
        //  3690: lload           33
        //  3692: ladd           
        //  3693: lstore          36
        //  3695: aload           12
        //  3697: astore          7
        //  3699: iload           5
        //  3701: istore          40
        //  3703: lload           26
        //  3705: lstore          38
        //  3707: lload           22
        //  3709: lstore          33
        //  3711: aload_2        
        //  3712: astore_3       
        //  3713: iload           8
        //  3715: istore          35
        //  3717: iinc            29, 1
        //  3720: iload           35
        //  3722: istore          8
        //  3724: aload_3        
        //  3725: astore_2       
        //  3726: lload           33
        //  3728: lstore          22
        //  3730: lload           36
        //  3732: lstore          24
        //  3734: lload           38
        //  3736: lstore          26
        //  3738: iload           40
        //  3740: istore          5
        //  3742: goto            2898
        //  3745: astore_2       
        //  3746: aload           10
        //  3748: astore          7
        //  3750: lload           31
        //  3752: lstore          22
        //  3754: iload           9
        //  3756: istore          5
        //  3758: iload           15
        //  3760: istore          11
        //  3762: goto            3787
        //  3765: aload_2        
        //  3766: astore          7
        //  3768: goto            3787
        //  3771: astore          7
        //  3773: aload_2        
        //  3774: astore          7
        //  3776: goto            3787
        //  3779: astore_2       
        //  3780: aconst_null    
        //  3781: astore          7
        //  3783: iload           29
        //  3785: istore          5
        //  3787: iload           13
        //  3789: istore          30
        //  3791: aload           7
        //  3793: astore_2       
        //  3794: iload           5
        //  3796: istore          30
        //  3798: aload           7
        //  3800: ifnonnull       3919
        //  3803: iload           13
        //  3805: istore          30
        //  3807: aload           7
        //  3809: astore_2       
        //  3810: iload           5
        //  3812: istore          30
        //  3814: aload_0        
        //  3815: getfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  3818: invokevirtual   java/util/ArrayList.size:()I
        //  3821: iload           16
        //  3823: if_icmple       3919
        //  3826: new             Ljava/util/ArrayList;
        //  3829: dup            
        //  3830: iload           16
        //  3832: invokespecial   java/util/ArrayList.<init>:(I)V
        //  3835: astore          7
        //  3837: iconst_0       
        //  3838: istore          8
        //  3840: iload           5
        //  3842: istore          9
        //  3844: iload           13
        //  3846: istore          5
        //  3848: iload           5
        //  3850: istore          30
        //  3852: aload           7
        //  3854: astore_2       
        //  3855: iload           9
        //  3857: istore          30
        //  3859: iload           8
        //  3861: iload           16
        //  3863: if_icmpge       3919
        //  3866: aload_0        
        //  3867: getfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  3870: iload           8
        //  3872: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  3875: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  3878: astore_2       
        //  3879: aload_2        
        //  3880: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  3883: aload_0        
        //  3884: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  3887: if_icmpeq       3896
        //  3890: iconst_1       
        //  3891: istore          13
        //  3893: goto            3899
        //  3896: iconst_0       
        //  3897: istore          13
        //  3899: iload           9
        //  3901: iload           13
        //  3903: ior            
        //  3904: istore          9
        //  3906: aload           7
        //  3908: aload_2        
        //  3909: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  3912: pop            
        //  3913: iinc            8, 1
        //  3916: goto            3848
        //  3919: aload_2        
        //  3920: ifnull          4095
        //  3923: iload           30
        //  3925: ifne            3938
        //  3928: aload_2        
        //  3929: astore          7
        //  3931: iload           28
        //  3933: istore          5
        //  3935: goto            4108
        //  3938: new             Ljava/util/ArrayList;
        //  3941: dup            
        //  3942: invokespecial   java/util/ArrayList.<init>:()V
        //  3945: astore          10
        //  3947: aload_2        
        //  3948: invokevirtual   java/util/ArrayList.size:()I
        //  3951: istore          5
        //  3953: iconst_0       
        //  3954: istore          9
        //  3956: iload           9
        //  3958: iload           5
        //  3960: if_icmpge       4016
        //  3963: aload_2        
        //  3964: iload           9
        //  3966: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  3969: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  3972: astore          7
        //  3974: aload           7
        //  3976: getfield        com/android/settings/applications/RunningState$MergedItem.mUserId:I
        //  3979: aload_0        
        //  3980: getfield        com/android/settings/applications/RunningState.mMyUserId:I
        //  3983: if_icmpeq       4002
        //  3986: aload_0        
        //  3987: aload_1        
        //  3988: aload           10
        //  3990: aload_0        
        //  3991: getfield        com/android/settings/applications/RunningState.mOtherUserBackgroundItems:Landroid/util/SparseArray;
        //  3994: aload           7
        //  3996: invokespecial   com/android/settings/applications/RunningState.addOtherUserItem:(Landroid/content/Context;Ljava/util/ArrayList;Landroid/util/SparseArray;Lcom/android/settings/applications/RunningState$MergedItem;)V
        //  3999: goto            4010
        //  4002: aload           10
        //  4004: aload           7
        //  4006: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  4009: pop            
        //  4010: iinc            9, 1
        //  4013: goto            3956
        //  4016: aload_2        
        //  4017: astore          7
        //  4019: aload_0        
        //  4020: getfield        com/android/settings/applications/RunningState.mOtherUserBackgroundItems:Landroid/util/SparseArray;
        //  4023: invokevirtual   android/util/SparseArray.size:()I
        //  4026: istore          9
        //  4028: iconst_0       
        //  4029: istore          5
        //  4031: aload           10
        //  4033: astore_2       
        //  4034: iload           5
        //  4036: iload           9
        //  4038: if_icmpge       4088
        //  4041: aload_0        
        //  4042: getfield        com/android/settings/applications/RunningState.mOtherUserBackgroundItems:Landroid/util/SparseArray;
        //  4045: iload           5
        //  4047: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
        //  4050: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  4053: astore          10
        //  4055: aload           10
        //  4057: getfield        com/android/settings/applications/RunningState$MergedItem.mCurSeq:I
        //  4060: aload_0        
        //  4061: getfield        com/android/settings/applications/RunningState.mSequence:I
        //  4064: if_icmpne       4082
        //  4067: aload           10
        //  4069: aload_1        
        //  4070: iconst_1       
        //  4071: invokevirtual   com/android/settings/applications/RunningState$MergedItem.update:(Landroid/content/Context;Z)Z
        //  4074: pop            
        //  4075: aload           10
        //  4077: aload_1        
        //  4078: invokevirtual   com/android/settings/applications/RunningState$MergedItem.updateSize:(Landroid/content/Context;)Z
        //  4081: pop            
        //  4082: iinc            5, 1
        //  4085: goto            4034
        //  4088: iload           28
        //  4090: istore          5
        //  4092: goto            4108
        //  4095: aconst_null    
        //  4096: astore          10
        //  4098: aload_2        
        //  4099: astore          7
        //  4101: iload           28
        //  4103: istore          5
        //  4105: aload           10
        //  4107: astore_2       
        //  4108: iload           5
        //  4110: aload_0        
        //  4111: getfield        com/android/settings/applications/RunningState.mMergedItems:Ljava/util/ArrayList;
        //  4114: invokevirtual   java/util/ArrayList.size:()I
        //  4117: if_icmpge       4143
        //  4120: aload_0        
        //  4121: getfield        com/android/settings/applications/RunningState.mMergedItems:Ljava/util/ArrayList;
        //  4124: iload           5
        //  4126: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
        //  4129: checkcast       Lcom/android/settings/applications/RunningState$MergedItem;
        //  4132: aload_1        
        //  4133: invokevirtual   com/android/settings/applications/RunningState$MergedItem.updateSize:(Landroid/content/Context;)Z
        //  4136: pop            
        //  4137: iinc            5, 1
        //  4140: goto            4108
        //  4143: aload_0        
        //  4144: getfield        com/android/settings/applications/RunningState.mLock:Ljava/lang/Object;
        //  4147: astore          10
        //  4149: aload           10
        //  4151: monitorenter   
        //  4152: aload_0        
        //  4153: iload           16
        //  4155: putfield        com/android/settings/applications/RunningState.mNumBackgroundProcesses:I
        //  4158: aload_0        
        //  4159: iload           6
        //  4161: putfield        com/android/settings/applications/RunningState.mNumForegroundProcesses:I
        //  4164: aload_0        
        //  4165: iload           17
        //  4167: putfield        com/android/settings/applications/RunningState.mNumServiceProcesses:I
        //  4170: aload_0        
        //  4171: lload           22
        //  4173: putfield        com/android/settings/applications/RunningState.mBackgroundProcessMemory:J
        //  4176: aload_0        
        //  4177: lload           24
        //  4179: putfield        com/android/settings/applications/RunningState.mForegroundProcessMemory:J
        //  4182: aload_0        
        //  4183: lload           26
        //  4185: putfield        com/android/settings/applications/RunningState.mServiceProcessMemory:J
        //  4188: aload           7
        //  4190: ifnull          4217
        //  4193: aload_0        
        //  4194: aload           7
        //  4196: putfield        com/android/settings/applications/RunningState.mBackgroundItems:Ljava/util/ArrayList;
        //  4199: aload_0        
        //  4200: aload_2        
        //  4201: putfield        com/android/settings/applications/RunningState.mUserBackgroundItems:Ljava/util/ArrayList;
        //  4204: aload_0        
        //  4205: getfield        com/android/settings/applications/RunningState.mWatchingBackgroundItems:Z
        //  4208: ifeq            4217
        //  4211: iconst_1       
        //  4212: istore          11
        //  4214: goto            4217
        //  4217: aload_0        
        //  4218: getfield        com/android/settings/applications/RunningState.mHaveData:Z
        //  4221: ifne            4236
        //  4224: aload_0        
        //  4225: iconst_1       
        //  4226: putfield        com/android/settings/applications/RunningState.mHaveData:Z
        //  4229: aload_0        
        //  4230: getfield        com/android/settings/applications/RunningState.mLock:Ljava/lang/Object;
        //  4233: invokevirtual   java/lang/Object.notifyAll:()V
        //  4236: aload           10
        //  4238: monitorexit    
        //  4239: iload           11
        //  4241: ireturn        
        //  4242: astore_1       
        //  4243: aload           10
        //  4245: monitorexit    
        //  4246: aload_1        
        //  4247: athrow         
        //  4248: astore_1       
        //  4249: goto            4243
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  2561   2574   2577   2582   Any
        //  2578   2580   2577   2582   Any
        //  2800   2815   3779   3787   Landroid/os/RemoteException;
        //  2825   2845   2851   2862   Landroid/os/RemoteException;
        //  2862   2874   3779   3787   Landroid/os/RemoteException;
        //  2898   2920   3771   3779   Landroid/os/RemoteException;
        //  2935   2955   3745   3765   Landroid/os/RemoteException;
        //  2970   2982   3745   3765   Landroid/os/RemoteException;
        //  2997   3007   3745   3765   Landroid/os/RemoteException;
        //  3051   3065   3745   3765   Landroid/os/RemoteException;
        //  3080   3090   3745   3765   Landroid/os/RemoteException;
        //  3109   3113   3745   3765   Landroid/os/RemoteException;
        //  3128   3137   3745   3765   Landroid/os/RemoteException;
        //  3152   3158   3745   3765   Landroid/os/RemoteException;
        //  3173   3183   3745   3765   Landroid/os/RemoteException;
        //  3198   3209   3745   3765   Landroid/os/RemoteException;
        //  3240   3246   3745   3765   Landroid/os/RemoteException;
        //  3270   3282   3745   3765   Landroid/os/RemoteException;
        //  3297   3317   3745   3765   Landroid/os/RemoteException;
        //  3335   3348   3745   3765   Landroid/os/RemoteException;
        //  3369   3379   3745   3765   Landroid/os/RemoteException;
        //  3397   3411   3460   3468   Landroid/os/RemoteException;
        //  3415   3427   3460   3468   Landroid/os/RemoteException;
        //  3447   3454   3460   3468   Landroid/os/RemoteException;
        //  3472   3476   3460   3468   Landroid/os/RemoteException;
        //  3480   3489   3460   3468   Landroid/os/RemoteException;
        //  3493   3499   3460   3468   Landroid/os/RemoteException;
        //  3503   3513   3460   3468   Landroid/os/RemoteException;
        //  3517   3528   3460   3468   Landroid/os/RemoteException;
        //  3548   3554   3460   3468   Landroid/os/RemoteException;
        //  3573   3580   3745   3765   Landroid/os/RemoteException;
        //  3595   3601   3745   3765   Landroid/os/RemoteException;
        //  3652   3666   3745   3765   Landroid/os/RemoteException;
        //  3681   3688   3745   3765   Landroid/os/RemoteException;
        //  4152   4188   4242   4243   Any
        //  4193   4204   4248   4252   Any
        //  4204   4211   4248   4252   Any
        //  4217   4236   4248   4252   Any
        //  4236   4239   4248   4252   Any
        //  4243   4246   4248   4252   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_4217:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Thread.java:745)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    ArrayList<MergedItem> getCurrentBackgroundItems() {
        synchronized (this.mLock) {
            return this.mUserBackgroundItems;
        }
    }
    
    ArrayList<MergedItem> getCurrentMergedItems() {
        synchronized (this.mLock) {
            return this.mMergedItems;
        }
    }
    
    boolean hasData() {
        synchronized (this.mLock) {
            return this.mHaveData;
        }
    }
    
    void pause() {
        synchronized (this.mLock) {
            this.mResumed = false;
            this.mRefreshUiListener = null;
            this.mHandler.removeMessages(4);
        }
    }
    
    void resume(final OnRefreshUiListener mRefreshUiListener) {
        synchronized (this.mLock) {
            this.mResumed = true;
            this.mRefreshUiListener = mRefreshUiListener;
            final boolean checkUsersChangedLocked = this.mUmBroadcastReceiver.checkUsersChangedLocked();
            final boolean applyNewConfig = this.mInterestingConfigChanges.applyNewConfig(this.mApplicationContext.getResources());
            if (checkUsersChangedLocked || applyNewConfig) {
                this.mHaveData = false;
                this.mBackgroundHandler.removeMessages(1);
                this.mBackgroundHandler.removeMessages(2);
                this.mBackgroundHandler.sendEmptyMessage(1);
            }
            if (!this.mBackgroundHandler.hasMessages(2)) {
                this.mBackgroundHandler.sendEmptyMessage(2);
            }
            this.mHandler.sendEmptyMessage(4);
        }
    }
    
    void setWatchingBackgroundItems(final boolean mWatchingBackgroundItems) {
        synchronized (this.mLock) {
            this.mWatchingBackgroundItems = mWatchingBackgroundItems;
        }
    }
    
    void updateNow() {
        synchronized (this.mLock) {
            this.mBackgroundHandler.removeMessages(2);
            this.mBackgroundHandler.sendEmptyMessage(2);
        }
    }
    
    void waitForData() {
        synchronized (this.mLock) {
            while (!this.mHaveData) {
                try {
                    this.mLock.wait(0L);
                }
                catch (InterruptedException ex) {}
            }
        }
    }
    
    static class AppProcessInfo
    {
        boolean hasForegroundServices;
        boolean hasServices;
        final ActivityManager$RunningAppProcessInfo info;
        
        AppProcessInfo(final ActivityManager$RunningAppProcessInfo info) {
            this.info = info;
        }
    }
    
    final class BackgroundHandler extends Handler
    {
        public BackgroundHandler(final Looper looper) {
            super(looper);
        }
        
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2: {
                    message = (Message)RunningState.this.mLock;
                    synchronized (message) {
                        if (!RunningState.this.mResumed) {
                            return;
                        }
                        // monitorexit(message)
                        message = RunningState.this.mHandler.obtainMessage(3);
                        message.arg1 = (RunningState.this.update(RunningState.this.mApplicationContext, RunningState.this.mAm) ? 1 : 0);
                        RunningState.this.mHandler.sendMessage(message);
                        this.removeMessages(2);
                        this.sendMessageDelayed(this.obtainMessage(2), 2000L);
                        break;
                    }
                }
                case 1: {
                    RunningState.this.reset();
                    break;
                }
            }
        }
    }
    
    static class BaseItem
    {
        long mActiveSince;
        boolean mBackground;
        int mCurSeq;
        String mCurSizeStr;
        String mDescription;
        CharSequence mDisplayLabel;
        final boolean mIsProcess;
        String mLabel;
        boolean mNeedDivider;
        PackageItemInfo mPackageInfo;
        long mSize;
        String mSizeStr;
        final int mUserId;
        
        public BaseItem(final boolean mIsProcess, final int mUserId) {
            this.mIsProcess = mIsProcess;
            this.mUserId = mUserId;
        }
        
        public Drawable loadIcon(final Context context, final RunningState runningState) {
            if (this.mPackageInfo != null) {
                return runningState.mPm.getUserBadgedIcon(this.mPackageInfo.loadUnbadgedIcon(runningState.mPm), new UserHandle(this.mUserId));
            }
            return null;
        }
    }
    
    static class MergedItem extends BaseItem
    {
        final ArrayList<MergedItem> mChildren;
        private int mLastNumProcesses;
        private int mLastNumServices;
        final ArrayList<ProcessItem> mOtherProcesses;
        ProcessItem mProcess;
        final ArrayList<ServiceItem> mServices;
        UserState mUser;
        
        MergedItem(final int n) {
            super(false, n);
            this.mOtherProcesses = new ArrayList<ProcessItem>();
            this.mServices = new ArrayList<ServiceItem>();
            this.mChildren = new ArrayList<MergedItem>();
            this.mLastNumProcesses = -1;
            this.mLastNumServices = -1;
        }
        
        private void setDescription(final Context context, final int mLastNumProcesses, final int mLastNumServices) {
            if (this.mLastNumProcesses != mLastNumProcesses || this.mLastNumServices != mLastNumServices) {
                this.mLastNumProcesses = mLastNumProcesses;
                this.mLastNumServices = mLastNumServices;
                int n = 2131888878;
                if (mLastNumProcesses != 1) {
                    if (mLastNumServices != 1) {
                        n = 2131888875;
                    }
                    else {
                        n = 2131888876;
                    }
                }
                else if (mLastNumServices != 1) {
                    n = 2131888877;
                }
                this.mDescription = context.getResources().getString(n, new Object[] { mLastNumProcesses, mLastNumServices });
            }
        }
        
        @Override
        public Drawable loadIcon(final Context context, final RunningState runningState) {
            if (this.mUser == null) {
                return super.loadIcon(context, runningState);
            }
            if (this.mUser.mIcon == null) {
                return context.getDrawable(17302625);
            }
            final Drawable$ConstantState constantState = this.mUser.mIcon.getConstantState();
            if (constantState == null) {
                return this.mUser.mIcon;
            }
            return constantState.newDrawable();
        }
        
        boolean update(final Context context, final boolean mBackground) {
            this.mBackground = mBackground;
            if (this.mUser != null) {
                this.mPackageInfo = this.mChildren.get(0).mProcess.mPackageInfo;
                String mLabel;
                if (this.mUser != null) {
                    mLabel = this.mUser.mLabel;
                }
                else {
                    mLabel = null;
                }
                this.mLabel = mLabel;
                this.mDisplayLabel = this.mLabel;
                int n = 0;
                int n2 = 0;
                this.mActiveSince = -1L;
                for (int i = 0; i < this.mChildren.size(); ++i) {
                    final MergedItem mergedItem = this.mChildren.get(i);
                    n += mergedItem.mLastNumProcesses;
                    n2 += mergedItem.mLastNumServices;
                    if (mergedItem.mActiveSince >= 0L && this.mActiveSince < mergedItem.mActiveSince) {
                        this.mActiveSince = mergedItem.mActiveSince;
                    }
                }
                if (!this.mBackground) {
                    this.setDescription(context, n, n2);
                }
            }
            else {
                this.mPackageInfo = this.mProcess.mPackageInfo;
                this.mDisplayLabel = this.mProcess.mDisplayLabel;
                this.mLabel = this.mProcess.mLabel;
                if (!this.mBackground) {
                    int n3;
                    if (this.mProcess.mPid > 0) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    this.setDescription(context, n3 + this.mOtherProcesses.size(), this.mServices.size());
                }
                this.mActiveSince = -1L;
                for (int j = 0; j < this.mServices.size(); ++j) {
                    final ServiceItem serviceItem = this.mServices.get(j);
                    if (serviceItem.mActiveSince >= 0L && this.mActiveSince < serviceItem.mActiveSince) {
                        this.mActiveSince = serviceItem.mActiveSince;
                    }
                }
            }
            return false;
        }
        
        boolean updateSize(final Context context) {
            if (this.mUser != null) {
                this.mSize = 0L;
                for (int i = 0; i < this.mChildren.size(); ++i) {
                    final MergedItem mergedItem = this.mChildren.get(i);
                    mergedItem.updateSize(context);
                    this.mSize += mergedItem.mSize;
                }
            }
            else {
                this.mSize = this.mProcess.mSize;
                for (int j = 0; j < this.mOtherProcesses.size(); ++j) {
                    this.mSize += this.mOtherProcesses.get(j).mSize;
                }
            }
            final String formatShortFileSize = Formatter.formatShortFileSize(context, this.mSize);
            if (!formatShortFileSize.equals(this.mSizeStr)) {
                this.mSizeStr = formatShortFileSize;
                return false;
            }
            return false;
        }
    }
    
    interface OnRefreshUiListener
    {
        void onRefreshUi(final int p0);
    }
    
    static class ProcessItem extends BaseItem
    {
        long mActiveSince;
        ProcessItem mClient;
        final SparseArray<ProcessItem> mDependentProcesses;
        boolean mInteresting;
        boolean mIsStarted;
        boolean mIsSystem;
        int mLastNumDependentProcesses;
        MergedItem mMergedItem;
        int mPid;
        final String mProcessName;
        ActivityManager$RunningAppProcessInfo mRunningProcessInfo;
        int mRunningSeq;
        final HashMap<ComponentName, ServiceItem> mServices;
        final int mUid;
        
        public ProcessItem(final Context context, final int mUid, final String mProcessName) {
            super(true, UserHandle.getUserId(mUid));
            this.mServices = new HashMap<ComponentName, ServiceItem>();
            this.mDependentProcesses = (SparseArray<ProcessItem>)new SparseArray();
            this.mDescription = context.getResources().getString(2131889043, new Object[] { mProcessName });
            this.mUid = mUid;
            this.mProcessName = mProcessName;
        }
        
        void addDependentProcesses(final ArrayList<BaseItem> list, final ArrayList<ProcessItem> list2) {
            for (int size = this.mDependentProcesses.size(), i = 0; i < size; ++i) {
                final ProcessItem processItem = (ProcessItem)this.mDependentProcesses.valueAt(i);
                processItem.addDependentProcesses(list, list2);
                list.add(processItem);
                if (processItem.mPid > 0) {
                    list2.add(processItem);
                }
            }
        }
        
        boolean buildDependencyChain(final Context context, final PackageManager packageManager, final int mCurSeq) {
            final int size = this.mDependentProcesses.size();
            boolean b = false;
            for (int i = 0; i < size; ++i) {
                final ProcessItem processItem = (ProcessItem)this.mDependentProcesses.valueAt(i);
                if (processItem.mClient != this) {
                    b = true;
                    processItem.mClient = this;
                }
                processItem.mCurSeq = mCurSeq;
                processItem.ensureLabel(packageManager);
                b |= processItem.buildDependencyChain(context, packageManager, mCurSeq);
            }
            if (this.mLastNumDependentProcesses != this.mDependentProcesses.size()) {
                b = true;
                this.mLastNumDependentProcesses = this.mDependentProcesses.size();
            }
            return b;
        }
        
        void ensureLabel(final PackageManager packageManager) {
            if (this.mLabel != null) {
                return;
            }
            try {
                final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.mProcessName, 4194304);
                if (applicationInfo.uid == this.mUid) {
                    this.mDisplayLabel = applicationInfo.loadLabel(packageManager);
                    this.mLabel = this.mDisplayLabel.toString();
                    this.mPackageInfo = (PackageItemInfo)applicationInfo;
                    return;
                }
            }
            catch (PackageManager$NameNotFoundException ex) {}
            final String[] packagesForUid = packageManager.getPackagesForUid(this.mUid);
            if (packagesForUid.length == 1) {
                try {
                    final ApplicationInfo applicationInfo2 = packageManager.getApplicationInfo(packagesForUid[0], 4194304);
                    this.mDisplayLabel = applicationInfo2.loadLabel(packageManager);
                    this.mLabel = this.mDisplayLabel.toString();
                    this.mPackageInfo = (PackageItemInfo)applicationInfo2;
                    return;
                }
                catch (PackageManager$NameNotFoundException ex2) {}
            }
            for (final String s : packagesForUid) {
                try {
                    final PackageInfo packageInfo = packageManager.getPackageInfo(s, 0);
                    if (packageInfo.sharedUserLabel != 0) {
                        final CharSequence text = packageManager.getText(s, packageInfo.sharedUserLabel, packageInfo.applicationInfo);
                        if (text != null) {
                            this.mDisplayLabel = text;
                            this.mLabel = text.toString();
                            this.mPackageInfo = (PackageItemInfo)packageInfo.applicationInfo;
                            return;
                        }
                    }
                }
                catch (PackageManager$NameNotFoundException ex3) {}
            }
            if (this.mServices.size() > 0) {
                this.mPackageInfo = (PackageItemInfo)this.mServices.values().iterator().next().mServiceInfo.applicationInfo;
                this.mDisplayLabel = this.mPackageInfo.loadLabel(packageManager);
                this.mLabel = this.mDisplayLabel.toString();
                return;
            }
            try {
                final ApplicationInfo applicationInfo3 = packageManager.getApplicationInfo(packagesForUid[0], 4194304);
                this.mDisplayLabel = applicationInfo3.loadLabel(packageManager);
                this.mLabel = this.mDisplayLabel.toString();
                this.mPackageInfo = (PackageItemInfo)applicationInfo3;
            }
            catch (PackageManager$NameNotFoundException ex4) {}
        }
        
        boolean updateService(final Context context, final ActivityManager$RunningServiceInfo activityManager$RunningServiceInfo) {
            final PackageManager packageManager = context.getPackageManager();
            boolean b = false;
            ServiceItem serviceItem;
            if ((serviceItem = this.mServices.get(activityManager$RunningServiceInfo.service)) == null) {
                b = true;
                final ServiceItem serviceItem2 = new ServiceItem(this.mUserId);
                serviceItem2.mRunningService = activityManager$RunningServiceInfo;
                try {
                    serviceItem2.mServiceInfo = ActivityThread.getPackageManager().getServiceInfo(activityManager$RunningServiceInfo.service, 4194304, UserHandle.getUserId(activityManager$RunningServiceInfo.uid));
                    if (serviceItem2.mServiceInfo == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("getServiceInfo returned null for: ");
                        sb.append(activityManager$RunningServiceInfo.service);
                        Log.d("RunningService", sb.toString());
                        return false;
                    }
                }
                catch (RemoteException ex) {}
                serviceItem2.mDisplayLabel = RunningState.makeLabel(packageManager, serviceItem2.mRunningService.service.getClassName(), (PackageItemInfo)serviceItem2.mServiceInfo);
                String string;
                if (this.mDisplayLabel != null) {
                    string = this.mDisplayLabel.toString();
                }
                else {
                    string = null;
                }
                this.mLabel = string;
                serviceItem2.mPackageInfo = (PackageItemInfo)serviceItem2.mServiceInfo.applicationInfo;
                this.mServices.put(activityManager$RunningServiceInfo.service, serviceItem2);
                serviceItem = serviceItem2;
            }
            serviceItem.mCurSeq = this.mCurSeq;
            serviceItem.mRunningService = activityManager$RunningServiceInfo;
            long activeSince;
            if (activityManager$RunningServiceInfo.restarting == 0L) {
                activeSince = activityManager$RunningServiceInfo.activeSince;
            }
            else {
                activeSince = -1L;
            }
            if (serviceItem.mActiveSince != activeSince) {
                serviceItem.mActiveSince = activeSince;
                b = true;
            }
            if (activityManager$RunningServiceInfo.clientPackage != null && activityManager$RunningServiceInfo.clientLabel != 0) {
                if (serviceItem.mShownAsStarted) {
                    serviceItem.mShownAsStarted = false;
                    b = true;
                }
                try {
                    serviceItem.mDescription = context.getResources().getString(2131889039, new Object[] { packageManager.getResourcesForApplication(activityManager$RunningServiceInfo.clientPackage).getString(activityManager$RunningServiceInfo.clientLabel) });
                }
                catch (PackageManager$NameNotFoundException ex2) {
                    serviceItem.mDescription = null;
                }
            }
            else {
                if (!serviceItem.mShownAsStarted) {
                    serviceItem.mShownAsStarted = true;
                    b = true;
                }
                serviceItem.mDescription = context.getResources().getString(2131889045);
            }
            return b;
        }
        
        boolean updateSize(final Context context, final long n, final int n2) {
            this.mSize = 1024L * n;
            if (this.mCurSeq == n2) {
                final String formatShortFileSize = Formatter.formatShortFileSize(context, this.mSize);
                if (!formatShortFileSize.equals(this.mSizeStr)) {
                    this.mSizeStr = formatShortFileSize;
                    return false;
                }
            }
            return false;
        }
    }
    
    static class ServiceItem extends BaseItem
    {
        MergedItem mMergedItem;
        ActivityManager$RunningServiceInfo mRunningService;
        ServiceInfo mServiceInfo;
        boolean mShownAsStarted;
        
        public ServiceItem(final int n) {
            super(false, n);
        }
    }
    
    class ServiceProcessComparator implements Comparator<ProcessItem>
    {
        @Override
        public int compare(final ProcessItem processItem, final ProcessItem processItem2) {
            final int mUserId = processItem.mUserId;
            final int mUserId2 = processItem2.mUserId;
            final boolean b = true;
            final boolean b2 = true;
            final boolean b3 = true;
            int n = 1;
            if (mUserId != mUserId2) {
                if (processItem.mUserId == RunningState.this.mMyUserId) {
                    return -1;
                }
                if (processItem2.mUserId == RunningState.this.mMyUserId) {
                    return 1;
                }
                if (processItem.mUserId < processItem2.mUserId) {
                    n = -1;
                }
                return n;
            }
            else {
                if (processItem.mIsStarted != processItem2.mIsStarted) {
                    int n2 = b ? 1 : 0;
                    if (processItem.mIsStarted) {
                        n2 = -1;
                    }
                    return n2;
                }
                if (processItem.mIsSystem != processItem2.mIsSystem) {
                    int n3;
                    if (processItem.mIsSystem) {
                        n3 = (b2 ? 1 : 0);
                    }
                    else {
                        n3 = -1;
                    }
                    return n3;
                }
                if (processItem.mActiveSince != processItem2.mActiveSince) {
                    int n4 = b3 ? 1 : 0;
                    if (processItem.mActiveSince > processItem2.mActiveSince) {
                        n4 = -1;
                    }
                    return n4;
                }
                return 0;
            }
        }
    }
    
    private final class UserManagerBroadcastReceiver extends BroadcastReceiver
    {
        private volatile boolean usersChanged;
        
        public boolean checkUsersChangedLocked() {
            final boolean usersChanged = this.usersChanged;
            this.usersChanged = false;
            return usersChanged;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            synchronized (RunningState.this.mLock) {
                if (RunningState.this.mResumed) {
                    RunningState.this.mHaveData = false;
                    RunningState.this.mBackgroundHandler.removeMessages(1);
                    RunningState.this.mBackgroundHandler.sendEmptyMessage(1);
                    RunningState.this.mBackgroundHandler.removeMessages(2);
                    RunningState.this.mBackgroundHandler.sendEmptyMessage(2);
                }
                else {
                    this.usersChanged = true;
                }
            }
        }
        
        void register(final Context context) {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.USER_STOPPED");
            intentFilter.addAction("android.intent.action.USER_STARTED");
            intentFilter.addAction("android.intent.action.USER_INFO_CHANGED");
            context.registerReceiverAsUser((BroadcastReceiver)this, UserHandle.ALL, intentFilter, (String)null, (Handler)null);
        }
    }
    
    static class UserState
    {
        Drawable mIcon;
        UserInfo mInfo;
        String mLabel;
    }
}
