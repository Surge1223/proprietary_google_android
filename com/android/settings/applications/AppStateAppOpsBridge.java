package com.android.settings.applications;

import java.util.ArrayList;
import android.content.pm.PackageInfo;
import android.app.AppOpsManager$OpEntry;
import android.app.AppOpsManager$PackageOps;
import java.util.Iterator;
import android.os.RemoteException;
import android.util.Log;
import java.util.Collection;
import java.util.Arrays;
import java.util.HashSet;
import android.util.ArrayMap;
import android.util.SparseArray;
import android.app.AppGlobals;
import com.android.settingslib.applications.ApplicationsState;
import android.os.UserManager;
import android.os.UserHandle;
import java.util.List;
import android.content.pm.IPackageManager;
import android.content.Context;
import android.app.AppOpsManager;

public abstract class AppStateAppOpsBridge extends AppStateBaseBridge
{
    private final AppOpsManager mAppOpsManager;
    private final int[] mAppOpsOpCodes;
    private final Context mContext;
    private final IPackageManager mIPackageManager;
    private final String[] mPermissions;
    private final List<UserHandle> mProfiles;
    private final UserManager mUserManager;
    
    public AppStateAppOpsBridge(final Context context, final ApplicationsState applicationsState, final Callback callback, final int n, final String[] array) {
        this(context, applicationsState, callback, n, array, AppGlobals.getPackageManager());
    }
    
    AppStateAppOpsBridge(final Context mContext, final ApplicationsState applicationsState, final Callback callback, final int n, final String[] mPermissions, final IPackageManager miPackageManager) {
        super(applicationsState, callback);
        this.mContext = mContext;
        this.mIPackageManager = miPackageManager;
        this.mUserManager = UserManager.get(mContext);
        this.mProfiles = (List<UserHandle>)this.mUserManager.getUserProfiles();
        this.mAppOpsManager = (AppOpsManager)mContext.getSystemService("appops");
        this.mAppOpsOpCodes = new int[] { n };
        this.mPermissions = mPermissions;
    }
    
    private boolean doesAnyPermissionMatch(final String s, final String[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (s.equals(array[i])) {
                return true;
            }
        }
        return false;
    }
    
    private SparseArray<ArrayMap<String, PermissionState>> getEntries() {
        try {
            final HashSet<String> set = new HashSet<String>();
            final String[] mPermissions = this.mPermissions;
            for (int length = mPermissions.length, i = 0; i < length; ++i) {
                final String[] appOpPermissionPackages = this.mIPackageManager.getAppOpPermissionPackages(mPermissions[i]);
                if (appOpPermissionPackages != null) {
                    set.addAll((Collection<?>)Arrays.asList(appOpPermissionPackages));
                }
            }
            if (set.isEmpty()) {
                return null;
            }
            final SparseArray sparseArray = new SparseArray();
            for (final UserHandle userHandle : this.mProfiles) {
                final ArrayMap arrayMap = new ArrayMap();
                final int identifier = userHandle.getIdentifier();
                sparseArray.put(identifier, (Object)arrayMap);
                for (final String s : set) {
                    final boolean packageAvailable = this.mIPackageManager.isPackageAvailable(s, identifier);
                    if (!this.shouldIgnorePackage(s) && packageAvailable) {
                        arrayMap.put((Object)s, (Object)new PermissionState(s, userHandle));
                    }
                }
            }
            return (SparseArray<ArrayMap<String, PermissionState>>)sparseArray;
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("PackageManager is dead. Can't get list of packages requesting ");
            sb.append(this.mPermissions[0]);
            Log.w("AppStateAppOpsBridge", sb.toString(), (Throwable)ex);
            return null;
        }
    }
    
    private boolean isThisUserAProfileOfCurrentUser(final int n) {
        for (int size = this.mProfiles.size(), i = 0; i < size; ++i) {
            if (this.mProfiles.get(i).getIdentifier() == n) {
                return true;
            }
        }
        return false;
    }
    
    private void loadAppOpsStates(final SparseArray<ArrayMap<String, PermissionState>> sparseArray) {
        final List packagesForOps = this.mAppOpsManager.getPackagesForOps(this.mAppOpsOpCodes);
        int size;
        if (packagesForOps != null) {
            size = packagesForOps.size();
        }
        else {
            size = 0;
        }
        for (int i = 0; i < size; ++i) {
            final AppOpsManager$PackageOps appOpsManager$PackageOps = packagesForOps.get(i);
            final int userId = UserHandle.getUserId(appOpsManager$PackageOps.getUid());
            if (this.isThisUserAProfileOfCurrentUser(userId)) {
                final ArrayMap arrayMap = (ArrayMap)sparseArray.get(userId);
                if (arrayMap != null) {
                    final PermissionState permissionState = (PermissionState)arrayMap.get((Object)appOpsManager$PackageOps.getPackageName());
                    if (permissionState == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("AppOp permission exists for package ");
                        sb.append(appOpsManager$PackageOps.getPackageName());
                        sb.append(" of user ");
                        sb.append(userId);
                        sb.append(" but package doesn't exist or did not request ");
                        sb.append(this.mPermissions);
                        sb.append(" access");
                        Log.w("AppStateAppOpsBridge", sb.toString());
                    }
                    else if (appOpsManager$PackageOps.getOps().size() < 1) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("No AppOps permission exists for package ");
                        sb2.append(appOpsManager$PackageOps.getPackageName());
                        Log.w("AppStateAppOpsBridge", sb2.toString());
                    }
                    else {
                        permissionState.appOpMode = appOpsManager$PackageOps.getOps().get(0).getMode();
                    }
                }
            }
        }
    }
    
    private void loadPermissionsStates(final SparseArray<ArrayMap<String, PermissionState>> sparseArray) {
        if (sparseArray == null) {
            return;
        }
        try {
            final Iterator<UserHandle> iterator = this.mProfiles.iterator();
            while (iterator.hasNext()) {
                final int identifier = iterator.next().getIdentifier();
                final ArrayMap arrayMap = (ArrayMap)sparseArray.get(identifier);
                if (arrayMap == null) {
                    continue;
                }
                final IPackageManager miPackageManager = this.mIPackageManager;
                final String[] mPermissions = this.mPermissions;
                int i = 0;
                final List list = miPackageManager.getPackagesHoldingPermissions(mPermissions, 0, identifier).getList();
                int size;
                if (list != null) {
                    size = list.size();
                }
                else {
                    size = 0;
                }
                while (i < size) {
                    final PackageInfo packageInfo = list.get(i);
                    final PermissionState permissionState = (PermissionState)arrayMap.get((Object)packageInfo.packageName);
                    if (permissionState != null) {
                        permissionState.packageInfo = packageInfo;
                        permissionState.staticPermissionGranted = true;
                    }
                    ++i;
                }
            }
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("PackageManager is dead. Can't get list of packages granted ");
            sb.append(this.mPermissions);
            Log.w("AppStateAppOpsBridge", sb.toString(), (Throwable)ex);
        }
    }
    
    private boolean shouldIgnorePackage(final String s) {
        return s.equals("android") || s.equals(this.mContext.getPackageName());
    }
    
    public PermissionState getPermissionInfo(final String s, final int n) {
        final PermissionState permissionState = new PermissionState(s, new UserHandle(UserHandle.getUserId(n)));
        try {
            permissionState.packageInfo = this.mIPackageManager.getPackageInfo(s, 4198400, permissionState.userHandle.getIdentifier());
            if (permissionState.packageInfo != null) {
                final String[] requestedPermissions = permissionState.packageInfo.requestedPermissions;
                final int[] requestedPermissionsFlags = permissionState.packageInfo.requestedPermissionsFlags;
                if (requestedPermissions != null) {
                    for (int i = 0; i < requestedPermissions.length; ++i) {
                        if (this.doesAnyPermissionMatch(requestedPermissions[i], this.mPermissions)) {
                            permissionState.permissionDeclared = true;
                            if ((requestedPermissionsFlags[i] & 0x2) != 0x0) {
                                permissionState.staticPermissionGranted = true;
                                break;
                            }
                        }
                    }
                }
            }
            final List opsForPackage = this.mAppOpsManager.getOpsForPackage(n, s, this.mAppOpsOpCodes);
            if (opsForPackage != null && opsForPackage.size() > 0 && opsForPackage.get(0).getOps().size() > 0) {
                permissionState.appOpMode = ((AppOpsManager$OpEntry)opsForPackage.get(0).getOps().get(0)).getMode();
            }
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("PackageManager is dead. Can't get package info ");
            sb.append(s);
            Log.w("AppStateAppOpsBridge", sb.toString(), (Throwable)ex);
        }
        return permissionState;
    }
    
    @Override
    protected void loadAllExtraInfo() {
        final SparseArray<ArrayMap<String, PermissionState>> entries = this.getEntries();
        this.loadPermissionsStates(entries);
        this.loadAppOpsStates(entries);
        final ArrayList<AppEntry> allApps = this.mAppSession.getAllApps();
        for (int size = allApps.size(), i = 0; i < size; ++i) {
            final AppEntry appEntry = allApps.get(i);
            final ArrayMap arrayMap = (ArrayMap)entries.get(UserHandle.getUserId(appEntry.info.uid));
            Object value;
            if (arrayMap != null) {
                value = arrayMap.get((Object)appEntry.info.packageName);
            }
            else {
                value = null;
            }
            appEntry.extraInfo = value;
        }
    }
    
    @Override
    protected abstract void updateExtraInfo(final AppEntry p0, final String p1, final int p2);
    
    public static class PermissionState
    {
        public int appOpMode;
        public PackageInfo packageInfo;
        public final String packageName;
        public boolean permissionDeclared;
        public boolean staticPermissionGranted;
        public final UserHandle userHandle;
        
        public PermissionState(final String packageName, final UserHandle userHandle) {
            this.packageName = packageName;
            this.appOpMode = 3;
            this.userHandle = userHandle;
        }
        
        public boolean isPermissible() {
            if (this.appOpMode == 3) {
                return this.staticPermissionGranted;
            }
            return this.appOpMode == 0;
        }
    }
}
