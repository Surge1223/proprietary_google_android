package com.android.settings.applications;

import com.android.internal.util.Preconditions;
import android.text.format.Formatter;
import android.content.Context;
import com.android.settingslib.applications.StorageStatsSource;
import android.support.v7.preference.Preference;

public class AppStorageSizesController
{
    private final Preference mAppSize;
    private final Preference mCacheSize;
    private boolean mCachedCleared;
    private final int mComputing;
    private boolean mDataCleared;
    private final Preference mDataSize;
    private final int mError;
    private long mLastCacheSize;
    private long mLastCodeSize;
    private long mLastDataSize;
    private StorageStatsSource.AppStorageStats mLastResult;
    private boolean mLastResultFailed;
    private long mLastTotalSize;
    private final Preference mTotalSize;
    
    private AppStorageSizesController(final Preference mTotalSize, final Preference mAppSize, final Preference mDataSize, final Preference mCacheSize, final int mComputing, final int mError) {
        this.mLastCodeSize = -1L;
        this.mLastDataSize = -1L;
        this.mLastCacheSize = -1L;
        this.mLastTotalSize = -1L;
        this.mTotalSize = mTotalSize;
        this.mAppSize = mAppSize;
        this.mDataSize = mDataSize;
        this.mCacheSize = mCacheSize;
        this.mComputing = mComputing;
        this.mError = mError;
    }
    
    private String getSizeStr(final Context context, final long n) {
        return Formatter.formatFileSize(context, n);
    }
    
    public StorageStatsSource.AppStorageStats getLastResult() {
        return this.mLastResult;
    }
    
    public void setCacheCleared(final boolean mCachedCleared) {
        this.mCachedCleared = mCachedCleared;
    }
    
    public void setDataCleared(final boolean mDataCleared) {
        this.mDataCleared = mDataCleared;
    }
    
    public void setResult(final StorageStatsSource.AppStorageStats mLastResult) {
        this.mLastResult = mLastResult;
        this.mLastResultFailed = (mLastResult == null);
    }
    
    public void updateUi(final Context context) {
        if (this.mLastResult == null) {
            int n;
            if (this.mLastResultFailed) {
                n = this.mError;
            }
            else {
                n = this.mComputing;
            }
            this.mAppSize.setSummary(n);
            this.mDataSize.setSummary(n);
            this.mCacheSize.setSummary(n);
            this.mTotalSize.setSummary(n);
        }
        else {
            final long codeBytes = this.mLastResult.getCodeBytes();
            final boolean mDataCleared = this.mDataCleared;
            final long n2 = 0L;
            long mLastDataSize;
            if (mDataCleared) {
                mLastDataSize = 0L;
            }
            else {
                mLastDataSize = this.mLastResult.getDataBytes() - this.mLastResult.getCacheBytes();
            }
            if (this.mLastCodeSize != codeBytes) {
                this.mLastCodeSize = codeBytes;
                this.mAppSize.setSummary(this.getSizeStr(context, codeBytes));
            }
            if (this.mLastDataSize != mLastDataSize) {
                this.mLastDataSize = mLastDataSize;
                this.mDataSize.setSummary(this.getSizeStr(context, mLastDataSize));
            }
            long cacheBytes = n2;
            if (!this.mDataCleared) {
                if (this.mCachedCleared) {
                    cacheBytes = n2;
                }
                else {
                    cacheBytes = this.mLastResult.getCacheBytes();
                }
            }
            if (this.mLastCacheSize != cacheBytes) {
                this.mLastCacheSize = cacheBytes;
                this.mCacheSize.setSummary(this.getSizeStr(context, cacheBytes));
            }
            final long mLastTotalSize = codeBytes + mLastDataSize + cacheBytes;
            if (this.mLastTotalSize != mLastTotalSize) {
                this.mLastTotalSize = mLastTotalSize;
                this.mTotalSize.setSummary(this.getSizeStr(context, mLastTotalSize));
            }
        }
    }
    
    public static class Builder
    {
        private Preference mAppSize;
        private Preference mCacheSize;
        private int mComputing;
        private Preference mDataSize;
        private int mError;
        private Preference mTotalSize;
        
        public AppStorageSizesController build() {
            return new AppStorageSizesController((Preference)Preconditions.checkNotNull((Object)this.mTotalSize), (Preference)Preconditions.checkNotNull((Object)this.mAppSize), (Preference)Preconditions.checkNotNull((Object)this.mDataSize), (Preference)Preconditions.checkNotNull((Object)this.mCacheSize), this.mComputing, this.mError, null);
        }
        
        public Builder setAppSizePreference(final Preference mAppSize) {
            this.mAppSize = mAppSize;
            return this;
        }
        
        public Builder setCacheSizePreference(final Preference mCacheSize) {
            this.mCacheSize = mCacheSize;
            return this;
        }
        
        public Builder setComputingString(final int mComputing) {
            this.mComputing = mComputing;
            return this;
        }
        
        public Builder setDataSizePreference(final Preference mDataSize) {
            this.mDataSize = mDataSize;
            return this;
        }
        
        public Builder setErrorString(final int mError) {
            this.mError = mError;
            return this;
        }
        
        public Builder setTotalSizePreference(final Preference mTotalSize) {
            this.mTotalSize = mTotalSize;
            return this;
        }
    }
}
