package com.android.settings.applications.appops;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;
import android.text.format.DateUtils;
import android.content.res.Resources;
import android.util.SparseArray;
import android.graphics.drawable.Drawable;
import java.io.File;
import java.util.Collections;
import android.content.pm.PackageInfo;
import java.util.ArrayList;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.pm.ApplicationInfo;
import java.util.HashMap;
import android.app.AppOpsManager$OpEntry;
import android.app.AppOpsManager$PackageOps;
import java.util.List;
import java.text.Collator;
import android.content.pm.PackageManager;
import android.content.Context;
import android.app.AppOpsManager;
import java.util.Comparator;

public class AppOpsState
{
    public static final OpsTemplate[] ALL_TEMPLATES;
    public static final OpsTemplate DEVICE_TEMPLATE;
    public static final Comparator<AppOpEntry> LABEL_COMPARATOR;
    public static final OpsTemplate LOCATION_TEMPLATE;
    public static final OpsTemplate MEDIA_TEMPLATE;
    public static final OpsTemplate MESSAGING_TEMPLATE;
    public static final OpsTemplate PERSONAL_TEMPLATE;
    public static final Comparator<AppOpEntry> RECENCY_COMPARATOR;
    public static final OpsTemplate RUN_IN_BACKGROUND_TEMPLATE;
    final AppOpsManager mAppOps;
    final Context mContext;
    final CharSequence[] mOpLabels;
    final CharSequence[] mOpSummaries;
    final PackageManager mPm;
    
    static {
        LOCATION_TEMPLATE = new OpsTemplate(new int[] { 0, 1, 2, 10, 12, 41, 42 }, new boolean[] { true, true, false, false, false, false, false });
        PERSONAL_TEMPLATE = new OpsTemplate(new int[] { 4, 5, 6, 7, 8, 9, 29, 30 }, new boolean[] { true, true, true, true, true, true, false, false });
        MESSAGING_TEMPLATE = new OpsTemplate(new int[] { 14, 16, 17, 18, 19, 15, 20, 21, 22 }, new boolean[] { true, true, true, true, true, true, true, true, true });
        MEDIA_TEMPLATE = new OpsTemplate(new int[] { 3, 26, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 64, 44 }, new boolean[] { false, true, true, false, false, false, false, false, false, false, false, false, false, false });
        DEVICE_TEMPLATE = new OpsTemplate(new int[] { 11, 25, 13, 23, 24, 40, 46, 47, 49, 50 }, new boolean[] { false, true, true, true, true, true, false, false, false, false });
        RUN_IN_BACKGROUND_TEMPLATE = new OpsTemplate(new int[] { 63 }, new boolean[] { false });
        ALL_TEMPLATES = new OpsTemplate[] { AppOpsState.LOCATION_TEMPLATE, AppOpsState.PERSONAL_TEMPLATE, AppOpsState.MESSAGING_TEMPLATE, AppOpsState.MEDIA_TEMPLATE, AppOpsState.DEVICE_TEMPLATE, AppOpsState.RUN_IN_BACKGROUND_TEMPLATE };
        RECENCY_COMPARATOR = new Comparator<AppOpEntry>() {
            private final Collator sCollator = Collator.getInstance();
            
            @Override
            public int compare(final AppOpEntry appOpEntry, final AppOpEntry appOpEntry2) {
                final int switchOrder = appOpEntry.getSwitchOrder();
                final int switchOrder2 = appOpEntry2.getSwitchOrder();
                final boolean b = true;
                final boolean b2 = true;
                int n = 1;
                if (switchOrder != switchOrder2) {
                    if (appOpEntry.getSwitchOrder() < appOpEntry2.getSwitchOrder()) {
                        n = -1;
                    }
                    return n;
                }
                if (appOpEntry.isRunning() != appOpEntry2.isRunning()) {
                    int n2 = b ? 1 : 0;
                    if (appOpEntry.isRunning()) {
                        n2 = -1;
                    }
                    return n2;
                }
                if (appOpEntry.getTime() != appOpEntry2.getTime()) {
                    int n3 = b2 ? 1 : 0;
                    if (appOpEntry.getTime() > appOpEntry2.getTime()) {
                        n3 = -1;
                    }
                    return n3;
                }
                return this.sCollator.compare(appOpEntry.getAppEntry().getLabel(), appOpEntry2.getAppEntry().getLabel());
            }
        };
        LABEL_COMPARATOR = new Comparator<AppOpEntry>() {
            private final Collator sCollator = Collator.getInstance();
            
            @Override
            public int compare(final AppOpEntry appOpEntry, final AppOpEntry appOpEntry2) {
                return this.sCollator.compare(appOpEntry.getAppEntry().getLabel(), appOpEntry2.getAppEntry().getLabel());
            }
        };
    }
    
    public AppOpsState(final Context mContext) {
        this.mContext = mContext;
        this.mAppOps = (AppOpsManager)mContext.getSystemService("appops");
        this.mPm = mContext.getPackageManager();
        this.mOpSummaries = mContext.getResources().getTextArray(2130903050);
        this.mOpLabels = mContext.getResources().getTextArray(2130903049);
    }
    
    private void addOp(final List<AppOpEntry> list, final AppOpsManager$PackageOps appOpsManager$PackageOps, final AppEntry appEntry, final AppOpsManager$OpEntry appOpsManager$OpEntry, final boolean b, final int n) {
        if (b && list.size() > 0) {
            final int size = list.size();
            int n2 = true ? 1 : 0;
            final AppOpEntry appOpEntry = list.get(size - 1);
            if (appOpEntry.getAppEntry() == appEntry) {
                final boolean b2 = appOpEntry.getTime() != 0L;
                if (appOpsManager$OpEntry.getTime() == 0L) {
                    n2 = (false ? 1 : 0);
                }
                if ((b2 ? 1 : 0) == n2) {
                    appOpEntry.addOp(appOpsManager$OpEntry);
                    return;
                }
            }
        }
        final AppOpEntry opSwitch = appEntry.getOpSwitch(appOpsManager$OpEntry.getOp());
        if (opSwitch != null) {
            opSwitch.addOp(appOpsManager$OpEntry);
            return;
        }
        list.add(new AppOpEntry(appOpsManager$PackageOps, appOpsManager$OpEntry, appEntry, n));
    }
    
    private AppEntry getAppEntry(final Context context, final HashMap<String, AppEntry> hashMap, final String s, final ApplicationInfo applicationInfo) {
        AppEntry appEntry;
        if ((appEntry = hashMap.get(s)) == null) {
            ApplicationInfo applicationInfo2;
            if ((applicationInfo2 = applicationInfo) == null) {
                try {
                    applicationInfo2 = this.mPm.getApplicationInfo(s, 4194816);
                }
                catch (PackageManager$NameNotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to find info for package ");
                    sb.append(s);
                    Log.w("AppOpsState", sb.toString());
                    return null;
                }
            }
            appEntry = new AppEntry(this, applicationInfo2);
            appEntry.loadLabel(context);
            hashMap.put(s, appEntry);
        }
        return appEntry;
    }
    
    public List<AppOpEntry> buildState(OpsTemplate packagesHoldingPermissions, int n, final String s, final Comparator<AppOpEntry> comparator) {
        final Context mContext = this.mContext;
        final HashMap<String, AppEntry> hashMap = new HashMap<String, AppEntry>();
        final ArrayList<Object> list = new ArrayList<Object>();
        final ArrayList<String> list2 = new ArrayList<String>();
        final ArrayList<Integer> list3 = new ArrayList<Integer>();
        final int[] array = new int[78];
        final boolean b = false;
        final int n2 = 0;
        for (int i = 0; i < packagesHoldingPermissions.ops.length; ++i) {
            if (packagesHoldingPermissions.showPerms[i]) {
                final String opToPermission = AppOpsManager.opToPermission(packagesHoldingPermissions.ops[i]);
                if (opToPermission != null && !list2.contains(opToPermission)) {
                    list2.add(opToPermission);
                    list3.add(packagesHoldingPermissions.ops[i]);
                    array[packagesHoldingPermissions.ops[i]] = i;
                }
            }
        }
        List<AppOpsManager$PackageOps> list4;
        if (s != null) {
            list4 = (List<AppOpsManager$PackageOps>)this.mAppOps.getOpsForPackage(n, s, packagesHoldingPermissions.ops);
        }
        else {
            list4 = (List<AppOpsManager$PackageOps>)this.mAppOps.getPackagesForOps(packagesHoldingPermissions.ops);
        }
        int n3 = b ? 1 : 0;
        int[] array2 = array;
        if (list4 != null) {
            final int n4 = 0;
            int[] array3 = array;
            n = n2;
            int n5 = n4;
            while (true) {
                n3 = n;
                array2 = array3;
                if (n5 >= list4.size()) {
                    break;
                }
                final AppOpsManager$PackageOps appOpsManager$PackageOps = list4.get(n5);
                final AppEntry appEntry = this.getAppEntry(mContext, hashMap, appOpsManager$PackageOps.getPackageName(), null);
                int[] array4;
                List<AppOpsManager$PackageOps> list6;
                if (appEntry == null) {
                    final List<AppOpsManager$PackageOps> list5 = list4;
                    array4 = array3;
                    list6 = list5;
                }
                else {
                    for (int j = n; j < appOpsManager$PackageOps.getOps().size(); ++j) {
                        final AppOpsManager$OpEntry appOpsManager$OpEntry = appOpsManager$PackageOps.getOps().get(j);
                        final boolean b2 = s == null || n;
                        int n6;
                        if (s == null) {
                            n6 = n;
                        }
                        else {
                            n6 = array3[appOpsManager$OpEntry.getOp()];
                        }
                        this.addOp((List<AppOpEntry>)list, appOpsManager$PackageOps, appEntry, appOpsManager$OpEntry, b2, n6);
                    }
                    final List<AppOpsManager$PackageOps> list7 = list4;
                    array4 = array3;
                    list6 = list7;
                }
                ++n5;
                final int[] array5 = array4;
                list4 = list6;
                array3 = array5;
            }
        }
        n = n3;
        if (s != null) {
            packagesHoldingPermissions = (OpsTemplate)new ArrayList();
            try {
                ((List<PackageInfo>)packagesHoldingPermissions).add(this.mPm.getPackageInfo(s, 4096));
            }
            catch (PackageManager$NameNotFoundException ex) {}
        }
        else {
            final String[] array6 = new String[list2.size()];
            list2.toArray(array6);
            packagesHoldingPermissions = (OpsTemplate)this.mPm.getPackagesHoldingPermissions(array6, n);
        }
        int n7;
        for (int k = n; k < ((List)packagesHoldingPermissions).size(); k = n7 + 1, n = 0) {
            PackageInfo packageInfo = ((List<PackageInfo>)packagesHoldingPermissions).get(k);
            AppEntry appEntry2 = this.getAppEntry(mContext, hashMap, packageInfo.packageName, packageInfo.applicationInfo);
            if (appEntry2 == null) {
                n7 = k;
            }
            else {
                OpsTemplate opsTemplate = packagesHoldingPermissions;
                n7 = k;
                if (packageInfo.requestedPermissions != null) {
                    AppOpsManager$PackageOps appOpsManager$PackageOps2 = null;
                    List<AppOpsManager$OpEntry> list8 = null;
                    final int n8 = n;
                    int l = n;
                    n = k;
                    int n9 = n8;
                    while (true) {
                        opsTemplate = packagesHoldingPermissions;
                        n7 = n;
                        if (n9 >= packageInfo.requestedPermissions.length) {
                            break;
                        }
                        AppEntry appEntry3;
                        List<AppOpsManager$OpEntry> list9;
                        AppOpsManager$PackageOps appOpsManager$PackageOps3;
                        OpsTemplate opsTemplate2;
                        PackageInfo packageInfo3;
                        if (packageInfo.requestedPermissionsFlags != null && (packageInfo.requestedPermissionsFlags[n9] & 0x2) == 0x0) {
                            appEntry3 = appEntry2;
                            final PackageInfo packageInfo2 = packageInfo;
                            list9 = list8;
                            appOpsManager$PackageOps3 = appOpsManager$PackageOps2;
                            opsTemplate2 = packagesHoldingPermissions;
                            packageInfo3 = packageInfo2;
                        }
                        else {
                            final PackageInfo packageInfo4 = packageInfo;
                            appEntry3 = appEntry2;
                            appOpsManager$PackageOps3 = appOpsManager$PackageOps2;
                            list9 = list8;
                            while (l < list2.size()) {
                                if (list2.get(l).equals(packageInfo4.requestedPermissions[n9])) {
                                    if (!appEntry3.hasOp(list3.get(l))) {
                                        if (list9 == null) {
                                            list9 = new ArrayList<AppOpsManager$OpEntry>();
                                            appOpsManager$PackageOps3 = new AppOpsManager$PackageOps(packageInfo4.packageName, packageInfo4.applicationInfo.uid, (List)list9);
                                        }
                                        final AppOpsManager$OpEntry appOpsManager$OpEntry2 = new AppOpsManager$OpEntry((int)list3.get(l), 0, 0L, 0L, 0, -1, (String)null);
                                        list9.add(appOpsManager$OpEntry2);
                                        final boolean b3 = s == null;
                                        int n10;
                                        if (s == null) {
                                            n10 = 0;
                                        }
                                        else {
                                            n10 = array2[appOpsManager$OpEntry2.getOp()];
                                        }
                                        this.addOp((List<AppOpEntry>)list, appOpsManager$PackageOps3, appEntry3, appOpsManager$OpEntry2, b3, n10);
                                    }
                                }
                                ++l;
                            }
                            final OpsTemplate opsTemplate3 = packagesHoldingPermissions;
                            packageInfo3 = packageInfo4;
                            opsTemplate2 = opsTemplate3;
                        }
                        ++n9;
                        final OpsTemplate opsTemplate4 = opsTemplate2;
                        final PackageInfo packageInfo5 = packageInfo3;
                        l = 0;
                        packagesHoldingPermissions = opsTemplate4;
                        list8 = list9;
                        appOpsManager$PackageOps2 = appOpsManager$PackageOps3;
                        appEntry2 = appEntry3;
                        packageInfo = packageInfo5;
                    }
                }
                packagesHoldingPermissions = opsTemplate;
            }
        }
        Collections.sort(list, (Comparator<? super Object>)comparator);
        return (List<AppOpEntry>)list;
    }
    
    public AppOpsManager getAppOpsManager() {
        return this.mAppOps;
    }
    
    public static class AppEntry
    {
        private final File mApkFile;
        private Drawable mIcon;
        private final ApplicationInfo mInfo;
        private String mLabel;
        private boolean mMounted;
        private final SparseArray<AppOpEntry> mOpSwitches;
        private final SparseArray<AppOpsManager$OpEntry> mOps;
        private final AppOpsState mState;
        
        public AppEntry(final AppOpsState mState, final ApplicationInfo mInfo) {
            this.mOps = (SparseArray<AppOpsManager$OpEntry>)new SparseArray();
            this.mOpSwitches = (SparseArray<AppOpEntry>)new SparseArray();
            this.mState = mState;
            this.mInfo = mInfo;
            this.mApkFile = new File(mInfo.sourceDir);
        }
        
        public void addOp(final AppOpEntry appOpEntry, final AppOpsManager$OpEntry appOpsManager$OpEntry) {
            this.mOps.put(appOpsManager$OpEntry.getOp(), (Object)appOpsManager$OpEntry);
            this.mOpSwitches.put(AppOpsManager.opToSwitch(appOpsManager$OpEntry.getOp()), (Object)appOpEntry);
        }
        
        public ApplicationInfo getApplicationInfo() {
            return this.mInfo;
        }
        
        public Drawable getIcon() {
            if (this.mIcon == null) {
                if (this.mApkFile.exists()) {
                    return this.mIcon = this.mInfo.loadIcon(this.mState.mPm);
                }
                this.mMounted = false;
            }
            else {
                if (this.mMounted) {
                    return this.mIcon;
                }
                if (this.mApkFile.exists()) {
                    this.mMounted = true;
                    return this.mIcon = this.mInfo.loadIcon(this.mState.mPm);
                }
            }
            return this.mState.mContext.getDrawable(17301651);
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public AppOpEntry getOpSwitch(final int n) {
            return (AppOpEntry)this.mOpSwitches.get(AppOpsManager.opToSwitch(n));
        }
        
        public boolean hasOp(final int n) {
            return this.mOps.indexOfKey(n) >= 0;
        }
        
        void loadLabel(final Context context) {
            if (this.mLabel == null || !this.mMounted) {
                if (!this.mApkFile.exists()) {
                    this.mMounted = false;
                    this.mLabel = this.mInfo.packageName;
                }
                else {
                    this.mMounted = true;
                    final CharSequence loadLabel = this.mInfo.loadLabel(context.getPackageManager());
                    String mLabel;
                    if (loadLabel != null) {
                        mLabel = loadLabel.toString();
                    }
                    else {
                        mLabel = this.mInfo.packageName;
                    }
                    this.mLabel = mLabel;
                }
            }
        }
        
        @Override
        public String toString() {
            return this.mLabel;
        }
    }
    
    public static class AppOpEntry
    {
        private final AppEntry mApp;
        private final ArrayList<AppOpsManager$OpEntry> mOps;
        private int mOverriddenPrimaryMode;
        private final AppOpsManager$PackageOps mPkgOps;
        private final ArrayList<AppOpsManager$OpEntry> mSwitchOps;
        private final int mSwitchOrder;
        
        public AppOpEntry(final AppOpsManager$PackageOps mPkgOps, final AppOpsManager$OpEntry appOpsManager$OpEntry, final AppEntry mApp, final int mSwitchOrder) {
            this.mOps = new ArrayList<AppOpsManager$OpEntry>();
            this.mSwitchOps = new ArrayList<AppOpsManager$OpEntry>();
            this.mOverriddenPrimaryMode = -1;
            this.mPkgOps = mPkgOps;
            this.mApp = mApp;
            this.mSwitchOrder = mSwitchOrder;
            this.mApp.addOp(this, appOpsManager$OpEntry);
            this.mOps.add(appOpsManager$OpEntry);
            this.mSwitchOps.add(appOpsManager$OpEntry);
        }
        
        private static void addOp(final ArrayList<AppOpsManager$OpEntry> list, final AppOpsManager$OpEntry appOpsManager$OpEntry) {
            for (int i = 0; i < list.size(); ++i) {
                final AppOpsManager$OpEntry appOpsManager$OpEntry2 = list.get(i);
                if (appOpsManager$OpEntry2.isRunning() != appOpsManager$OpEntry.isRunning()) {
                    if (appOpsManager$OpEntry.isRunning()) {
                        list.add(i, appOpsManager$OpEntry);
                        return;
                    }
                }
                else if (appOpsManager$OpEntry2.getTime() < appOpsManager$OpEntry.getTime()) {
                    list.add(i, appOpsManager$OpEntry);
                    return;
                }
            }
            list.add(appOpsManager$OpEntry);
        }
        
        public void addOp(final AppOpsManager$OpEntry appOpsManager$OpEntry) {
            this.mApp.addOp(this, appOpsManager$OpEntry);
            addOp(this.mOps, appOpsManager$OpEntry);
            if (this.mApp.getOpSwitch(AppOpsManager.opToSwitch(appOpsManager$OpEntry.getOp())) == null) {
                addOp(this.mSwitchOps, appOpsManager$OpEntry);
            }
        }
        
        public AppEntry getAppEntry() {
            return this.mApp;
        }
        
        public AppOpsManager$OpEntry getOpEntry(final int n) {
            return this.mOps.get(n);
        }
        
        public int getPrimaryOpMode() {
            int n;
            if (this.mOverriddenPrimaryMode >= 0) {
                n = this.mOverriddenPrimaryMode;
            }
            else {
                n = this.mOps.get(0).getMode();
            }
            return n;
        }
        
        public int getSwitchOrder() {
            return this.mSwitchOrder;
        }
        
        public long getTime() {
            return this.mOps.get(0).getTime();
        }
        
        public CharSequence getTimeText(final Resources resources, final boolean b) {
            if (this.isRunning()) {
                return resources.getText(2131886391);
            }
            if (this.getTime() > 0L) {
                return DateUtils.getRelativeTimeSpanString(this.getTime(), System.currentTimeMillis(), 60000L, 262144);
            }
            CharSequence text;
            if (b) {
                text = resources.getText(2131886390);
            }
            else {
                text = "";
            }
            return text;
        }
        
        public boolean isRunning() {
            return this.mOps.get(0).isRunning();
        }
        
        public void overridePrimaryOpMode(final int mOverriddenPrimaryMode) {
            this.mOverriddenPrimaryMode = mOverriddenPrimaryMode;
        }
        
        @Override
        public String toString() {
            return this.mApp.getLabel();
        }
    }
    
    public static class OpsTemplate implements Parcelable
    {
        public static final Parcelable.Creator<OpsTemplate> CREATOR;
        public final int[] ops;
        public final boolean[] showPerms;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<OpsTemplate>() {
                public OpsTemplate createFromParcel(final Parcel parcel) {
                    return new OpsTemplate(parcel);
                }
                
                public OpsTemplate[] newArray(final int n) {
                    return new OpsTemplate[n];
                }
            };
        }
        
        OpsTemplate(final Parcel parcel) {
            this.ops = parcel.createIntArray();
            this.showPerms = parcel.createBooleanArray();
        }
        
        public OpsTemplate(final int[] ops, final boolean[] showPerms) {
            this.ops = ops;
            this.showPerms = showPerms;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeIntArray(this.ops);
            parcel.writeBooleanArray(this.showPerms);
        }
    }
}
