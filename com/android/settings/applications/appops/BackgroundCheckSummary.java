package com.android.settings.applications.appops;

import android.app.FragmentTransaction;
import android.app.Fragment;
import android.preference.PreferenceFrameLayout$LayoutParams;
import android.preference.PreferenceFrameLayout;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.android.settings.core.InstrumentedPreferenceFragment;

public class BackgroundCheckSummary extends InstrumentedPreferenceFragment
{
    private LayoutInflater mInflater;
    
    @Override
    public int getMetricsCategory() {
        return 258;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.getActivity().setTitle(2131886528);
    }
    
    @Override
    public View onCreateView(final LayoutInflater mInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mInflater = mInflater;
        final View inflate = this.mInflater.inflate(2131558465, viewGroup, false);
        if (viewGroup instanceof PreferenceFrameLayout) {
            ((PreferenceFrameLayout$LayoutParams)inflate.getLayoutParams()).removeBorders = true;
        }
        final FragmentTransaction beginTransaction = this.getChildFragmentManager().beginTransaction();
        beginTransaction.add(2131361884, (Fragment)new AppOpsCategory(AppOpsState.RUN_IN_BACKGROUND_TEMPLATE), "appops");
        beginTransaction.commitAllowingStateLoss();
        return inflate;
    }
}
