package com.android.settings.applications.appops;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.BroadcastReceiver;
import android.content.AsyncTaskLoader;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.app.AppOpsManager$OpEntry;
import android.widget.Switch;
import android.view.View;
import android.widget.ListView;
import android.content.Loader;
import android.widget.ListAdapter;
import android.content.Context;
import android.os.Parcelable;
import android.os.Bundle;
import java.util.List;
import android.app.LoaderManager$LoaderCallbacks;
import android.app.ListFragment;

public class AppOpsCategory extends ListFragment implements LoaderManager$LoaderCallbacks<List<AppOpsState.AppOpEntry>>
{
    AppListAdapter mAdapter;
    AppOpsState mState;
    
    public AppOpsCategory() {
    }
    
    public AppOpsCategory(final AppOpsState.OpsTemplate opsTemplate) {
        final Bundle arguments = new Bundle();
        arguments.putParcelable("template", (Parcelable)opsTemplate);
        this.setArguments(arguments);
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.setEmptyText((CharSequence)"No applications");
        this.setHasOptionsMenu(true);
        this.setListAdapter((ListAdapter)(this.mAdapter = new AppListAdapter((Context)this.getActivity(), this.mState)));
        this.setListShown(false);
        this.getLoaderManager().initLoader(0, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mState = new AppOpsState((Context)this.getActivity());
    }
    
    public Loader<List<AppOpsState.AppOpEntry>> onCreateLoader(final int n, final Bundle bundle) {
        final Bundle arguments = this.getArguments();
        AppOpsState.OpsTemplate opsTemplate = null;
        if (arguments != null) {
            opsTemplate = (AppOpsState.OpsTemplate)arguments.getParcelable("template");
        }
        return (Loader<List<AppOpsState.AppOpEntry>>)new AppListLoader((Context)this.getActivity(), this.mState, opsTemplate);
    }
    
    public void onListItemClick(final ListView listView, final View view, int n, final long n2) {
        final AppOpsState.AppOpEntry item = this.mAdapter.getItem(n);
        if (item != null) {
            final Switch switch1 = (Switch)view.findViewById(2131362421);
            final boolean checked = switch1.isChecked();
            n = 1;
            final boolean checked2 = checked ^ true;
            switch1.setChecked(checked2);
            final AppOpsManager$OpEntry opEntry = item.getOpEntry(0);
            if (checked2) {
                n = 0;
            }
            this.mState.getAppOpsManager().setMode(opEntry.getOp(), item.getAppEntry().getApplicationInfo().uid, item.getAppEntry().getApplicationInfo().packageName, n);
            item.overridePrimaryOpMode(n);
        }
    }
    
    public void onLoadFinished(final Loader<List<AppOpsState.AppOpEntry>> loader, final List<AppOpsState.AppOpEntry> data) {
        this.mAdapter.setData(data);
        if (this.isResumed()) {
            this.setListShown(true);
        }
        else {
            this.setListShownNoAnimation(true);
        }
    }
    
    public void onLoaderReset(final Loader<List<AppOpsState.AppOpEntry>> loader) {
        this.mAdapter.setData(null);
    }
    
    public static class AppListAdapter extends BaseAdapter
    {
        private final LayoutInflater mInflater;
        List<AppOpsState.AppOpEntry> mList;
        private final Resources mResources;
        private final AppOpsState mState;
        
        public AppListAdapter(final Context context, final AppOpsState mState) {
            this.mResources = context.getResources();
            this.mInflater = (LayoutInflater)context.getSystemService("layout_inflater");
            this.mState = mState;
        }
        
        public int getCount() {
            int size;
            if (this.mList != null) {
                size = this.mList.size();
            }
            else {
                size = 0;
            }
            return size;
        }
        
        public AppOpsState.AppOpEntry getItem(final int n) {
            return this.mList.get(n);
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, View inflate, final ViewGroup viewGroup) {
            boolean checked = false;
            if (inflate == null) {
                inflate = this.mInflater.inflate(2131558450, viewGroup, false);
            }
            final AppOpsState.AppOpEntry item = this.getItem(n);
            ((ImageView)inflate.findViewById(2131361874)).setImageDrawable(item.getAppEntry().getIcon());
            ((TextView)inflate.findViewById(2131361875)).setText((CharSequence)item.getAppEntry().getLabel());
            ((TextView)inflate.findViewById(2131362420)).setText(item.getTimeText(this.mResources, false));
            inflate.findViewById(2131362422).setVisibility(8);
            final Switch switch1 = (Switch)inflate.findViewById(2131362421);
            if (item.getPrimaryOpMode() == 0) {
                checked = true;
            }
            switch1.setChecked(checked);
            return inflate;
        }
        
        public void setData(final List<AppOpsState.AppOpEntry> mList) {
            this.mList = mList;
            this.notifyDataSetChanged();
        }
    }
    
    public static class AppListLoader extends AsyncTaskLoader<List<AppOpsState.AppOpEntry>>
    {
        List<AppOpsState.AppOpEntry> mApps;
        final InterestingConfigChanges mLastConfig;
        PackageIntentReceiver mPackageObserver;
        final AppOpsState mState;
        final AppOpsState.OpsTemplate mTemplate;
        
        public AppListLoader(final Context context, final AppOpsState mState, final AppOpsState.OpsTemplate mTemplate) {
            super(context);
            this.mLastConfig = new InterestingConfigChanges();
            this.mState = mState;
            this.mTemplate = mTemplate;
        }
        
        public void deliverResult(final List<AppOpsState.AppOpEntry> mApps) {
            if (this.isReset() && mApps != null) {
                this.onReleaseResources(mApps);
            }
            this.mApps = mApps;
            if (this.isStarted()) {
                super.deliverResult((Object)mApps);
            }
            if (mApps != null) {
                this.onReleaseResources(mApps);
            }
        }
        
        public List<AppOpsState.AppOpEntry> loadInBackground() {
            return this.mState.buildState(this.mTemplate, 0, null, AppOpsState.LABEL_COMPARATOR);
        }
        
        public void onCanceled(final List<AppOpsState.AppOpEntry> list) {
            super.onCanceled((Object)list);
            this.onReleaseResources(list);
        }
        
        protected void onReleaseResources(final List<AppOpsState.AppOpEntry> list) {
        }
        
        protected void onReset() {
            super.onReset();
            this.onStopLoading();
            if (this.mApps != null) {
                this.onReleaseResources(this.mApps);
                this.mApps = null;
            }
            if (this.mPackageObserver != null) {
                this.getContext().unregisterReceiver((BroadcastReceiver)this.mPackageObserver);
                this.mPackageObserver = null;
            }
        }
        
        protected void onStartLoading() {
            this.onContentChanged();
            if (this.mApps != null) {
                this.deliverResult(this.mApps);
            }
            if (this.mPackageObserver == null) {
                this.mPackageObserver = new PackageIntentReceiver(this);
            }
            final boolean applyNewConfig = this.mLastConfig.applyNewConfig(this.getContext().getResources());
            if (this.takeContentChanged() || this.mApps == null || applyNewConfig) {
                this.forceLoad();
            }
        }
        
        protected void onStopLoading() {
            this.cancelLoad();
        }
    }
    
    public static class InterestingConfigChanges
    {
        final Configuration mLastConfiguration;
        int mLastDensity;
        
        public InterestingConfigChanges() {
            this.mLastConfiguration = new Configuration();
        }
        
        boolean applyNewConfig(final Resources resources) {
            final int update = this.mLastConfiguration.updateFrom(resources.getConfiguration());
            if (this.mLastDensity == resources.getDisplayMetrics().densityDpi && (update & 0x304) == 0x0) {
                return false;
            }
            this.mLastDensity = resources.getDisplayMetrics().densityDpi;
            return true;
        }
    }
    
    public static class PackageIntentReceiver extends BroadcastReceiver
    {
        final AppListLoader mLoader;
        
        public PackageIntentReceiver(final AppListLoader mLoader) {
            this.mLoader = mLoader;
            final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
            intentFilter.addDataScheme("package");
            this.mLoader.getContext().registerReceiver((BroadcastReceiver)this, intentFilter);
            final IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
            intentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
            this.mLoader.getContext().registerReceiver((BroadcastReceiver)this, intentFilter2);
        }
        
        public void onReceive(final Context context, final Intent intent) {
            this.mLoader.onContentChanged();
        }
    }
}
