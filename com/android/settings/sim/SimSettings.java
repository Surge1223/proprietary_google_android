package com.android.settings.sim;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemProperties;
import android.telecom.PhoneAccountHandle;
import android.support.v7.preference.Preference;
import android.telecom.TelecomManager;
import android.util.Log;
import android.telephony.TelephonyManager;
import com.android.settings.Utils;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settings.search.BaseSearchIndexProvider;
import android.telephony.SubscriptionManager;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager$OnSubscriptionsChangedListener;
import android.content.Context;
import android.telephony.SubscriptionInfo;
import java.util.List;
import com.android.settings.search.Indexable;
import com.android.settings.RestrictedSettingsFragment;

public class SimSettings extends RestrictedSettingsFragment implements Indexable
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private List<SubscriptionInfo> mAvailableSubInfos;
    private int[] mCallState;
    private Context mContext;
    private int mNumSlots;
    private final SubscriptionManager$OnSubscriptionsChangedListener mOnSubscriptionsChangeListener;
    private int mPhoneCount;
    private PhoneStateListener[] mPhoneStateListener;
    private List<SubscriptionInfo> mSelectableSubInfos;
    private PreferenceScreen mSimCards;
    private List<SubscriptionInfo> mSubInfoList;
    private SubscriptionManager mSubscriptionManager;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                if (Utils.showSimCardTile(context)) {
                    final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                    searchIndexableResource.xmlResId = 2132082831;
                    list.add(searchIndexableResource);
                }
                return list;
            }
        };
    }
    
    public SimSettings() {
        super("no_config_sim");
        this.mAvailableSubInfos = null;
        this.mSubInfoList = null;
        this.mSelectableSubInfos = null;
        this.mSimCards = null;
        this.mPhoneCount = TelephonyManager.getDefault().getPhoneCount();
        this.mCallState = new int[this.mPhoneCount];
        this.mPhoneStateListener = new PhoneStateListener[this.mPhoneCount];
        this.mOnSubscriptionsChangeListener = new SubscriptionManager$OnSubscriptionsChangedListener() {
            public void onSubscriptionsChanged() {
                SimSettings.this.updateSubscriptions();
            }
        };
    }
    
    private String getPhoneNumber(final SubscriptionInfo subscriptionInfo) {
        return ((TelephonyManager)this.mContext.getSystemService("phone")).getLine1Number(subscriptionInfo.getSubscriptionId());
    }
    
    private PhoneStateListener getPhoneStateListener(final int n, final int n2) {
        return this.mPhoneStateListener[n] = new PhoneStateListener(n2) {
            public void onCallStateChanged(final int n, final String s) {
                SimSettings.this.mCallState[n] = n;
                SimSettings.this.updateCellularDataValues();
            }
        };
    }
    
    private boolean isCallStateIdle() {
        boolean b = true;
        for (int i = 0; i < this.mCallState.length; ++i) {
            if (this.mCallState[i] != 0) {
                b = false;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("isCallStateIdle ");
        sb.append(b);
        Log.d("SimSettings", sb.toString());
        return b;
    }
    
    private void updateActivitesCategory() {
        this.updateCellularDataValues();
        this.updateCallValues();
        this.updateSmsValues();
    }
    
    private void updateAllOptions() {
        this.updateSimSlotValues();
        this.updateActivitesCategory();
    }
    
    private void updateCallValues() {
        final Preference preference = this.findPreference("sim_calls");
        final TelecomManager from = TelecomManager.from(this.mContext);
        final PhoneAccountHandle userSelectedOutgoingPhoneAccount = from.getUserSelectedOutgoingPhoneAccount();
        final List callCapablePhoneAccounts = from.getCallCapablePhoneAccounts();
        preference.setTitle(2131886943);
        String string;
        if (userSelectedOutgoingPhoneAccount == null) {
            string = this.mContext.getResources().getString(2131889116);
        }
        else {
            string = (String)from.getPhoneAccount(userSelectedOutgoingPhoneAccount).getLabel();
        }
        preference.setSummary(string);
        final int size = callCapablePhoneAccounts.size();
        boolean enabled = true;
        if (size <= 1) {
            enabled = false;
        }
        preference.setEnabled(enabled);
    }
    
    private void updateCellularDataValues() {
        final Preference preference = this.findPreference("sim_cellular_data");
        final SubscriptionInfo defaultDataSubscriptionInfo = this.mSubscriptionManager.getDefaultDataSubscriptionInfo();
        preference.setTitle(2131886994);
        final boolean callStateIdle = this.isCallStateIdle();
        final boolean b = false;
        final boolean b2 = false;
        final boolean boolean1 = SystemProperties.getBoolean("ril.cdma.inecmmode", false);
        if (defaultDataSubscriptionInfo != null) {
            preference.setSummary(defaultDataSubscriptionInfo.getDisplayName());
            boolean enabled = b2;
            if (this.mSelectableSubInfos.size() > 1) {
                enabled = b2;
                if (callStateIdle) {
                    enabled = b2;
                    if (!boolean1) {
                        enabled = true;
                    }
                }
            }
            preference.setEnabled(enabled);
        }
        else if (defaultDataSubscriptionInfo == null) {
            preference.setSummary(2131889165);
            boolean enabled2 = b;
            if (this.mSelectableSubInfos.size() >= 1) {
                enabled2 = b;
                if (callStateIdle) {
                    enabled2 = b;
                    if (!boolean1) {
                        enabled2 = true;
                    }
                }
            }
            preference.setEnabled(enabled2);
        }
    }
    
    private void updateSimSlotValues() {
        for (int preferenceCount = this.mSimCards.getPreferenceCount(), i = 0; i < preferenceCount; ++i) {
            final Preference preference = this.mSimCards.getPreference(i);
            if (preference instanceof SimPreference) {
                ((SimPreference)preference).update();
            }
        }
    }
    
    private void updateSmsValues() {
        final Preference preference = this.findPreference("sim_sms");
        final SubscriptionInfo defaultSmsSubscriptionInfo = this.mSubscriptionManager.getDefaultSmsSubscriptionInfo();
        preference.setTitle(2131889185);
        final boolean b = false;
        boolean enabled = false;
        if (defaultSmsSubscriptionInfo != null) {
            preference.setSummary(defaultSmsSubscriptionInfo.getDisplayName());
            if (this.mSelectableSubInfos.size() > 1) {
                enabled = true;
            }
            preference.setEnabled(enabled);
        }
        else if (defaultSmsSubscriptionInfo == null) {
            preference.setSummary(2131889165);
            boolean enabled2 = b;
            if (this.mSelectableSubInfos.size() >= 1) {
                enabled2 = true;
            }
            preference.setEnabled(enabled2);
        }
    }
    
    private void updateSubscriptions() {
        this.mSubInfoList = (List<SubscriptionInfo>)this.mSubscriptionManager.getActiveSubscriptionInfoList();
        final int n = 0;
        for (int i = 0; i < this.mNumSlots; ++i) {
            final PreferenceScreen mSimCards = this.mSimCards;
            final StringBuilder sb = new StringBuilder();
            sb.append("sim");
            sb.append(i);
            final Preference preference = mSimCards.findPreference(sb.toString());
            if (preference instanceof SimPreference) {
                this.mSimCards.removePreference(preference);
            }
        }
        this.mAvailableSubInfos.clear();
        this.mSelectableSubInfos.clear();
        for (int j = n; j < this.mNumSlots; ++j) {
            final SubscriptionInfo activeSubscriptionInfoForSimSlotIndex = this.mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(j);
            final SimPreference simPreference = new SimPreference(this.getPrefContext(), activeSubscriptionInfoForSimSlotIndex, j);
            simPreference.setOrder(j - this.mNumSlots);
            this.mSimCards.addPreference(simPreference);
            this.mAvailableSubInfos.add(activeSubscriptionInfoForSimSlotIndex);
            if (activeSubscriptionInfoForSimSlotIndex != null) {
                this.mSelectableSubInfos.add(activeSubscriptionInfoForSimSlotIndex);
            }
        }
        this.updateAllOptions();
    }
    
    @Override
    public int getMetricsCategory() {
        return 88;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Context)this.getActivity();
        this.mSubscriptionManager = SubscriptionManager.from((Context)this.getActivity());
        final TelephonyManager telephonyManager = (TelephonyManager)this.getActivity().getSystemService("phone");
        this.addPreferencesFromResource(2132082831);
        this.mNumSlots = telephonyManager.getSimCount();
        this.mSimCards = (PreferenceScreen)this.findPreference("sim_cards");
        this.mAvailableSubInfos = new ArrayList<SubscriptionInfo>(this.mNumSlots);
        this.mSelectableSubInfos = new ArrayList<SubscriptionInfo>();
        SimSelectNotification.cancelNotification((Context)this.getActivity());
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mSubscriptionManager.removeOnSubscriptionsChangedListener(this.mOnSubscriptionsChangeListener);
        final TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService("phone");
        for (int i = 0; i < this.mPhoneCount; ++i) {
            if (this.mPhoneStateListener[i] != null) {
                telephonyManager.listen(this.mPhoneStateListener[i], 0);
                this.mPhoneStateListener[i] = null;
            }
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        final Context mContext = this.mContext;
        final Intent intent = new Intent(mContext, (Class)SimDialogActivity.class);
        intent.addFlags(268435456);
        if (preference instanceof SimPreference) {
            final Intent intent2 = new Intent(mContext, (Class)SimPreferenceDialog.class);
            intent2.putExtra("slot_id", ((SimPreference)preference).getSlotId());
            this.startActivity(intent2);
        }
        else if (this.findPreference("sim_cellular_data") == preference) {
            intent.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, 0);
            mContext.startActivity(intent);
        }
        else if (this.findPreference("sim_calls") == preference) {
            intent.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, 1);
            mContext.startActivity(intent);
        }
        else if (this.findPreference("sim_sms") == preference) {
            intent.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, 2);
            mContext.startActivity(intent);
        }
        return true;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mSubscriptionManager.addOnSubscriptionsChangedListener(this.mOnSubscriptionsChangeListener);
        this.updateSubscriptions();
        final TelephonyManager telephonyManager = (TelephonyManager)this.getActivity().getSystemService("phone");
        if (this.mSelectableSubInfos.size() > 1) {
            Log.d("SimSettings", "Register for call state change");
            for (int i = 0; i < this.mPhoneCount; ++i) {
                telephonyManager.listen(this.getPhoneStateListener(i, this.mSelectableSubInfos.get(i).getSubscriptionId()), 32);
            }
        }
    }
    
    private class SimPreference extends Preference
    {
        Context mContext;
        private int mSlotId;
        private SubscriptionInfo mSubInfoRecord;
        
        public SimPreference(final Context mContext, final SubscriptionInfo mSubInfoRecord, final int mSlotId) {
            super(mContext);
            this.mContext = mContext;
            this.mSubInfoRecord = mSubInfoRecord;
            this.mSlotId = mSlotId;
            final StringBuilder sb = new StringBuilder();
            sb.append("sim");
            sb.append(this.mSlotId);
            this.setKey(sb.toString());
            this.update();
        }
        
        private int getSlotId() {
            return this.mSlotId;
        }
        
        public void update() {
            final Resources resources = this.mContext.getResources();
            this.setTitle(String.format(this.mContext.getResources().getString(2131889133), this.mSlotId + 1));
            if (this.mSubInfoRecord != null) {
                if (TextUtils.isEmpty((CharSequence)SimSettings.this.getPhoneNumber(this.mSubInfoRecord))) {
                    this.setSummary(this.mSubInfoRecord.getDisplayName());
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append((Object)this.mSubInfoRecord.getDisplayName());
                    sb.append(" - ");
                    sb.append((Object)PhoneNumberUtils.createTtsSpannable((CharSequence)SimSettings.this.getPhoneNumber(this.mSubInfoRecord)));
                    this.setSummary(sb.toString());
                    this.setEnabled(true);
                }
                this.setIcon((Drawable)new BitmapDrawable(resources, this.mSubInfoRecord.createIconBitmap(this.mContext)));
            }
            else {
                this.setSummary(2131889170);
                this.setFragment(null);
                this.setEnabled(false);
            }
        }
    }
}
