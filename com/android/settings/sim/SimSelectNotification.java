package com.android.settings.sim;

import java.util.List;
import android.telephony.SubscriptionInfo;
import android.util.Log;
import com.android.settings.Utils;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.content.res.Resources;
import android.app.PendingIntent;
import android.content.Intent;
import com.android.settings.Settings;
import android.support.v4.app.NotificationCompat;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.BroadcastReceiver;

public class SimSelectNotification extends BroadcastReceiver
{
    public static void cancelNotification(final Context context) {
        ((NotificationManager)context.getSystemService("notification")).cancel(1);
    }
    
    private void createNotification(final Context context) {
        final Resources resources = context.getResources();
        final NotificationChannel notificationChannel = new NotificationChannel("sim_select_notification_channel", (CharSequence)resources.getString(2131889164), 2);
        final NotificationCompat.Builder setContentText = new NotificationCompat.Builder(context, "sim_select_notification_channel").setSmallIcon(2131231153).setColor(context.getColor(2131099844)).setContentTitle(resources.getString(2131889153)).setContentText(resources.getString(2131889152));
        final Intent intent = new Intent(context, (Class)Settings.SimSettingsActivity.class);
        intent.addFlags(268435456);
        setContentText.setContentIntent(PendingIntent.getActivity(context, 0, intent, 268435456));
        final NotificationManager notificationManager = (NotificationManager)context.getSystemService("notification");
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(1, setContentText.build());
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
        final SubscriptionManager from = SubscriptionManager.from(context);
        final int simCount = telephonyManager.getSimCount();
        if (simCount < 2 || !Utils.isDeviceProvisioned(context)) {
            return;
        }
        cancelNotification(context);
        final String stringExtra = intent.getStringExtra("ss");
        if (!"ABSENT".equals(stringExtra) && !"LOADED".equals(stringExtra)) {
            Log.d("SimSelectNotification", "sim state is not Absent or Loaded");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("simstatus = ");
        sb.append(stringExtra);
        Log.d("SimSelectNotification", sb.toString());
        for (int i = 0; i < simCount; ++i) {
            final int simState = telephonyManager.getSimState(i);
            if (simState != 1 && simState != 5 && simState != 0) {
                Log.d("SimSelectNotification", "All sims not in valid state yet");
                return;
            }
        }
        final List activeSubscriptionInfoList = from.getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList == null || activeSubscriptionInfoList.size() < 1) {
            Log.d("SimSelectNotification", "Subscription list is empty");
            return;
        }
        from.clearDefaultsForInactiveSubIds();
        final boolean usableSubIdValue = SubscriptionManager.isUsableSubIdValue(SubscriptionManager.getDefaultDataSubscriptionId());
        final boolean usableSubIdValue2 = SubscriptionManager.isUsableSubIdValue(SubscriptionManager.getDefaultSmsSubscriptionId());
        if (usableSubIdValue && usableSubIdValue2) {
            Log.d("SimSelectNotification", "Data & SMS default sims are selected. No notification");
            return;
        }
        this.createNotification(context);
        if (activeSubscriptionInfoList.size() == 1) {
            final Intent intent2 = new Intent(context, (Class)SimDialogActivity.class);
            intent2.addFlags(268435456);
            intent2.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, 3);
            intent2.putExtra(SimDialogActivity.PREFERRED_SIM, activeSubscriptionInfoList.get(0).getSimSlotIndex());
            context.startActivity(intent2);
        }
        else if (!usableSubIdValue) {
            final Intent intent3 = new Intent(context, (Class)SimDialogActivity.class);
            intent3.addFlags(268435456);
            intent3.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, 0);
            context.startActivity(intent3);
        }
    }
}
