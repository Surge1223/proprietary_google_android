package com.android.settings.sim;

import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.ArrayAdapter;
import android.os.Bundle;
import android.app.AlertDialog;
import android.telecom.PhoneAccount;
import android.content.DialogInterface$OnCancelListener;
import android.widget.ListAdapter;
import android.view.KeyEvent;
import android.content.DialogInterface$OnKeyListener;
import java.util.List;
import java.util.ArrayList;
import android.app.Dialog;
import java.util.ListIterator;
import android.telephony.TelephonyManager;
import android.telecom.TelecomManager;
import android.widget.Toast;
import android.content.res.Resources;
import android.content.DialogInterface;
import android.telephony.SubscriptionInfo;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.telephony.SubscriptionManager;
import android.content.Context;
import android.telecom.PhoneAccountHandle;
import android.app.Activity;

public class SimDialogActivity extends Activity
{
    public static String DIALOG_TYPE_KEY;
    public static String PREFERRED_SIM;
    private static String TAG;
    
    static {
        SimDialogActivity.TAG = "SimDialogActivity";
        SimDialogActivity.PREFERRED_SIM = "preferred_sim";
        SimDialogActivity.DIALOG_TYPE_KEY = "dialog_type";
    }
    
    private void displayPreferredDialog(final int n) {
        final Resources resources = this.getResources();
        final Context applicationContext = this.getApplicationContext();
        final SubscriptionInfo activeSubscriptionInfoForSimSlotIndex = SubscriptionManager.from(applicationContext).getActiveSubscriptionInfoForSimSlotIndex(n);
        if (activeSubscriptionInfoForSimSlotIndex != null) {
            final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this);
            alertDialog$Builder.setTitle(2131889161);
            alertDialog$Builder.setMessage((CharSequence)resources.getString(2131889160, new Object[] { activeSubscriptionInfoForSimSlotIndex.getDisplayName() }));
            alertDialog$Builder.setPositiveButton(2131890232, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, int subscriptionId) {
                    subscriptionId = activeSubscriptionInfoForSimSlotIndex.getSubscriptionId();
                    final PhoneAccountHandle access$000 = SimDialogActivity.this.subscriptionIdToPhoneAccountHandle(subscriptionId);
                    setDefaultDataSubId(applicationContext, subscriptionId);
                    setDefaultSmsSubId(applicationContext, subscriptionId);
                    SimDialogActivity.this.setUserSelectedOutgoingPhoneAccount(access$000);
                    SimDialogActivity.this.finish();
                }
            });
            alertDialog$Builder.setNegativeButton(2131888406, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    SimDialogActivity.this.finish();
                }
            });
            alertDialog$Builder.create().show();
        }
        else {
            this.finish();
        }
    }
    
    private static void setDefaultDataSubId(final Context context, final int defaultDataSubId) {
        SubscriptionManager.from(context).setDefaultDataSubId(defaultDataSubId);
        Toast.makeText(context, 2131887213, 1).show();
    }
    
    private static void setDefaultSmsSubId(final Context context, final int defaultSmsSubId) {
        SubscriptionManager.from(context).setDefaultSmsSubId(defaultSmsSubId);
    }
    
    private void setUserSelectedOutgoingPhoneAccount(final PhoneAccountHandle userSelectedOutgoingPhoneAccount) {
        TelecomManager.from((Context)this).setUserSelectedOutgoingPhoneAccount(userSelectedOutgoingPhoneAccount);
    }
    
    private PhoneAccountHandle subscriptionIdToPhoneAccountHandle(final int n) {
        final TelecomManager from = TelecomManager.from((Context)this);
        final TelephonyManager from2 = TelephonyManager.from((Context)this);
        final ListIterator listIterator = from.getCallCapablePhoneAccounts().listIterator();
        while (listIterator.hasNext()) {
            final PhoneAccountHandle phoneAccountHandle = listIterator.next();
            if (n == from2.getSubIdForPhoneAccount(from.getPhoneAccount(phoneAccountHandle))) {
                return phoneAccountHandle;
            }
        }
        return null;
    }
    
    public Dialog createDialog(final Context context, final int n) {
        final ArrayList<String> list = new ArrayList<String>();
        final List activeSubscriptionInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        int size;
        if (activeSubscriptionInfoList == null) {
            size = 0;
        }
        else {
            size = activeSubscriptionInfoList.size();
        }
        final DialogInterface$OnClickListener dialogInterface$OnClickListener = (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                switch (n) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid dialog type ");
                        sb.append(n);
                        sb.append(" in SIM dialog.");
                        throw new IllegalArgumentException(sb.toString());
                    }
                    case 2: {
                        setDefaultSmsSubId(context, activeSubscriptionInfoList.get(n).getSubscriptionId());
                        break;
                    }
                    case 1: {
                        final List callCapablePhoneAccounts = TelecomManager.from(context).getCallCapablePhoneAccounts();
                        final SimDialogActivity this$0 = SimDialogActivity.this;
                        PhoneAccountHandle phoneAccountHandle;
                        if (n < 1) {
                            phoneAccountHandle = null;
                        }
                        else {
                            phoneAccountHandle = callCapablePhoneAccounts.get(n - 1);
                        }
                        this$0.setUserSelectedOutgoingPhoneAccount(phoneAccountHandle);
                        break;
                    }
                    case 0: {
                        setDefaultDataSubId(context, activeSubscriptionInfoList.get(n).getSubscriptionId());
                        break;
                    }
                }
                SimDialogActivity.this.finish();
            }
        };
        final DialogInterface$OnKeyListener onKeyListener = (DialogInterface$OnKeyListener)new DialogInterface$OnKeyListener() {
            public boolean onKey(final DialogInterface dialogInterface, final int n, final KeyEvent keyEvent) {
                if (n == 4) {
                    SimDialogActivity.this.finish();
                }
                return true;
            }
        };
        final ArrayList<SubscriptionInfo> list2 = new ArrayList<SubscriptionInfo>();
        if (n == 1) {
            final TelecomManager from = TelecomManager.from(context);
            final TelephonyManager from2 = TelephonyManager.from(context);
            final ListIterator listIterator = from.getCallCapablePhoneAccounts().listIterator();
            list.add(this.getResources().getString(2131889116));
            list2.add(null);
            while (listIterator.hasNext()) {
                final PhoneAccount phoneAccount = from.getPhoneAccount((PhoneAccountHandle)listIterator.next());
                list.add((String)phoneAccount.getLabel());
                final int subIdForPhoneAccount = from2.getSubIdForPhoneAccount(phoneAccount);
                if (subIdForPhoneAccount != -1) {
                    list2.add(SubscriptionManager.from(context).getActiveSubscriptionInfo(subIdForPhoneAccount));
                }
                else {
                    list2.add(null);
                }
            }
        }
        else {
            for (int i = 0; i < size; ++i) {
                CharSequence displayName;
                if ((displayName = activeSubscriptionInfoList.get(i).getDisplayName()) == null) {
                    displayName = "";
                }
                list.add(displayName.toString());
            }
        }
        final String[] array = list.toArray(new String[0]);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(context);
        List<SubscriptionInfo> list3;
        if (n == 1) {
            list3 = list2;
        }
        else {
            list3 = (List<SubscriptionInfo>)activeSubscriptionInfoList;
        }
        final SelectAccountListAdapter selectAccountListAdapter = new SelectAccountListAdapter(list3, alertDialog$Builder.getContext(), 2131558735, array, n);
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid dialog type ");
                sb.append(n);
                sb.append(" in SIM dialog.");
                throw new IllegalArgumentException(sb.toString());
            }
            case 2: {
                alertDialog$Builder.setTitle(2131889118);
                break;
            }
            case 1: {
                alertDialog$Builder.setTitle(2131889030);
                break;
            }
            case 0: {
                alertDialog$Builder.setTitle(2131889031);
                break;
            }
        }
        final AlertDialog create = alertDialog$Builder.setAdapter((ListAdapter)selectAccountListAdapter, (DialogInterface$OnClickListener)dialogInterface$OnClickListener).create();
        ((Dialog)create).setOnKeyListener((DialogInterface$OnKeyListener)onKeyListener);
        ((Dialog)create).setOnCancelListener((DialogInterface$OnCancelListener)new DialogInterface$OnCancelListener() {
            public void onCancel(final DialogInterface dialogInterface) {
                SimDialogActivity.this.finish();
            }
        });
        return (Dialog)create;
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final int intExtra = this.getIntent().getIntExtra(SimDialogActivity.DIALOG_TYPE_KEY, -1);
        switch (intExtra) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid dialog type ");
                sb.append(intExtra);
                sb.append(" sent.");
                throw new IllegalArgumentException(sb.toString());
            }
            case 3: {
                this.displayPreferredDialog(this.getIntent().getIntExtra(SimDialogActivity.PREFERRED_SIM, 0));
                break;
            }
            case 0:
            case 1:
            case 2: {
                this.createDialog((Context)this, intExtra).show();
                break;
            }
        }
    }
    
    private class SelectAccountListAdapter extends ArrayAdapter<String>
    {
        private final float OPACITY;
        private Context mContext;
        private int mDialogId;
        private int mResId;
        private List<SubscriptionInfo> mSubInfoList;
        
        public SelectAccountListAdapter(final List<SubscriptionInfo> mSubInfoList, final Context mContext, final int mResId, final String[] array, final int mDialogId) {
            super(mContext, mResId, (Object[])array);
            this.OPACITY = 0.54f;
            this.mContext = mContext;
            this.mResId = mResId;
            this.mDialogId = mDialogId;
            this.mSubInfoList = mSubInfoList;
        }
        
        public View getView(final int n, View inflate, final ViewGroup viewGroup) {
            final LayoutInflater layoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
            ViewHolder tag;
            if (inflate == null) {
                inflate = layoutInflater.inflate(this.mResId, (ViewGroup)null);
                tag = new ViewHolder();
                tag.title = (TextView)inflate.findViewById(R.id.title);
                tag.summary = (TextView)inflate.findViewById(2131362668);
                tag.icon = (ImageView)inflate.findViewById(R.id.icon);
                inflate.setTag((Object)tag);
            }
            else {
                tag = (ViewHolder)inflate.getTag();
            }
            final SubscriptionInfo subscriptionInfo = this.mSubInfoList.get(n);
            if (subscriptionInfo == null) {
                tag.title.setText((CharSequence)this.getItem(n));
                tag.summary.setText((CharSequence)"");
                tag.icon.setImageDrawable(SimDialogActivity.this.getResources().getDrawable(2131231054));
                tag.icon.setAlpha(0.54f);
            }
            else {
                tag.title.setText(subscriptionInfo.getDisplayName());
                tag.summary.setText((CharSequence)subscriptionInfo.getNumber());
                tag.icon.setImageBitmap(subscriptionInfo.createIconBitmap(this.mContext));
            }
            return inflate;
        }
        
        private class ViewHolder
        {
            ImageView icon;
            TextView summary;
            TextView title;
        }
    }
}
