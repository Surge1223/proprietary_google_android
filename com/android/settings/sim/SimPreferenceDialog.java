package com.android.settings.sim;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.graphics.drawable.shapes.Shape;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.Paint.Style;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.res.Resources;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.widget.TextView;
import android.telephony.TelephonyManager;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import com.android.settings.Utils;
import android.widget.EditText;
import android.os.Bundle;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionInfo;
import android.view.View;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Activity;

public class SimPreferenceDialog extends Activity
{
    private final String SIM_NAME;
    private final String TINT_POS;
    AlertDialog$Builder mBuilder;
    private String[] mColorStrings;
    private Context mContext;
    View mDialogLayout;
    private int mSlotId;
    private SubscriptionInfo mSubInfoRecord;
    private SubscriptionManager mSubscriptionManager;
    private int[] mTintArr;
    private int mTintSelectorPos;
    
    public SimPreferenceDialog() {
        this.SIM_NAME = "sim_name";
        this.TINT_POS = "tint_pos";
    }
    
    private void createEditDialog(final Bundle bundle) {
        final Resources resources = this.mContext.getResources();
        final EditText editTextCursorPosition = (EditText)this.mDialogLayout.findViewById(2131362613);
        editTextCursorPosition.setText(this.mSubInfoRecord.getDisplayName());
        Utils.setEditTextCursorPosition(editTextCursorPosition);
        final Spinner spinner = (Spinner)this.mDialogLayout.findViewById(R.id.spinner);
        final SelectColorAdapter adapter = new SelectColorAdapter(this.mContext, 2131558741, this.mColorStrings);
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter)adapter);
        for (int i = 0; i < this.mTintArr.length; ++i) {
            if (this.mTintArr[i] == this.mSubInfoRecord.getIconTint()) {
                spinner.setSelection(i);
                this.mTintSelectorPos = i;
                break;
            }
        }
        spinner.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int selection, final long n) {
                spinner.setSelection(selection);
                SimPreferenceDialog.this.mTintSelectorPos = selection;
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        });
        final TelephonyManager telephonyManager = (TelephonyManager)this.mContext.getSystemService("phone");
        final TextView textView = (TextView)this.mDialogLayout.findViewById(2131362409);
        final String line1Number = telephonyManager.getLine1Number(this.mSubInfoRecord.getSubscriptionId());
        if (TextUtils.isEmpty((CharSequence)line1Number)) {
            textView.setText((CharSequence)resources.getString(17039374));
        }
        else {
            textView.setText((CharSequence)PhoneNumberUtils.formatNumber(line1Number));
        }
        String text = telephonyManager.getSimOperatorName(this.mSubInfoRecord.getSubscriptionId());
        final TextView textView2 = (TextView)this.mDialogLayout.findViewById(2131361965);
        if (TextUtils.isEmpty((CharSequence)text)) {
            text = this.mContext.getString(17039374);
        }
        textView2.setText((CharSequence)text);
        this.mBuilder.setTitle((CharSequence)String.format(resources.getString(2131889133), this.mSubInfoRecord.getSimSlotIndex() + 1));
        this.mBuilder.setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, int n) {
                final EditText editTextCursorPosition = (EditText)SimPreferenceDialog.this.mDialogLayout.findViewById(2131362613);
                Utils.setEditTextCursorPosition(editTextCursorPosition);
                final String string = editTextCursorPosition.getText().toString();
                n = SimPreferenceDialog.this.mSubInfoRecord.getSubscriptionId();
                SimPreferenceDialog.this.mSubInfoRecord.setDisplayName((CharSequence)string);
                SimPreferenceDialog.this.mSubscriptionManager.setDisplayName(string, n, 2L);
                final int selectedItemPosition = spinner.getSelectedItemPosition();
                n = SimPreferenceDialog.this.mSubInfoRecord.getSubscriptionId();
                final int iconTint = SimPreferenceDialog.this.mTintArr[selectedItemPosition];
                SimPreferenceDialog.this.mSubInfoRecord.setIconTint(iconTint);
                SimPreferenceDialog.this.mSubscriptionManager.setIconTint(iconTint, n);
                dialogInterface.dismiss();
            }
        });
        this.mBuilder.setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        this.mBuilder.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                SimPreferenceDialog.this.finish();
            }
        });
        this.mBuilder.create().show();
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Context)this;
        this.mSlotId = this.getIntent().getExtras().getInt("slot_id", -1);
        this.mSubscriptionManager = SubscriptionManager.from(this.mContext);
        this.mSubInfoRecord = this.mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(this.mSlotId);
        this.mTintArr = this.mContext.getResources().getIntArray(17236081);
        this.mColorStrings = this.mContext.getResources().getStringArray(2130903100);
        this.mTintSelectorPos = 0;
        this.mBuilder = new AlertDialog$Builder(this.mContext);
        this.mDialogLayout = ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(2131558613, (ViewGroup)null);
        this.mBuilder.setView(this.mDialogLayout);
        this.createEditDialog(bundle);
    }
    
    public void onRestoreInstanceState(final Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        final int int1 = bundle.getInt("tint_pos");
        ((Spinner)this.mDialogLayout.findViewById(R.id.spinner)).setSelection(int1);
        this.mTintSelectorPos = int1;
        final EditText editTextCursorPosition = (EditText)this.mDialogLayout.findViewById(2131362613);
        editTextCursorPosition.setText((CharSequence)bundle.getString("sim_name"));
        Utils.setEditTextCursorPosition(editTextCursorPosition);
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putInt("tint_pos", this.mTintSelectorPos);
        bundle.putString("sim_name", ((EditText)this.mDialogLayout.findViewById(2131362613)).getText().toString());
        super.onSaveInstanceState(bundle);
    }
    
    private class SelectColorAdapter extends ArrayAdapter<CharSequence>
    {
        private Context mContext;
        private int mResId;
        
        public SelectColorAdapter(final Context mContext, final int mResId, final String[] array) {
            super(mContext, mResId, (Object[])array);
            this.mContext = mContext;
            this.mResId = mResId;
        }
        
        public View getDropDownView(final int n, final View view, final ViewGroup viewGroup) {
            final View view2 = this.getView(n, view, viewGroup);
            final ViewHolder viewHolder = (ViewHolder)view2.getTag();
            if (SimPreferenceDialog.this.mTintSelectorPos == n) {
                viewHolder.swatch.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
            }
            else {
                viewHolder.swatch.getPaint().setStyle(Paint.Style.STROKE);
            }
            viewHolder.icon.setVisibility(0);
            return view2;
        }
        
        public View getView(final int n, View inflate, final ViewGroup viewGroup) {
            final LayoutInflater layoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
            final Resources resources = this.mContext.getResources();
            final int dimensionPixelSize = resources.getDimensionPixelSize(2131165334);
            final int dimensionPixelSize2 = resources.getDimensionPixelSize(2131165335);
            ViewHolder tag;
            if (inflate == null) {
                inflate = layoutInflater.inflate(this.mResId, (ViewGroup)null);
                tag = new ViewHolder();
                final ShapeDrawable swatch = new ShapeDrawable((Shape)new OvalShape());
                swatch.setIntrinsicHeight(dimensionPixelSize);
                swatch.setIntrinsicWidth(dimensionPixelSize);
                swatch.getPaint().setStrokeWidth((float)dimensionPixelSize2);
                tag.label = (TextView)inflate.findViewById(2131361999);
                tag.icon = (ImageView)inflate.findViewById(2131361996);
                tag.swatch = swatch;
                inflate.setTag((Object)tag);
            }
            else {
                tag = (ViewHolder)inflate.getTag();
            }
            tag.label.setText((CharSequence)this.getItem(n));
            tag.swatch.getPaint().setColor(SimPreferenceDialog.this.mTintArr[n]);
            tag.swatch.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
            tag.icon.setVisibility(0);
            tag.icon.setImageDrawable((Drawable)tag.swatch);
            return inflate;
        }
        
        private class ViewHolder
        {
            ImageView icon;
            TextView label;
            ShapeDrawable swatch;
        }
    }
}
