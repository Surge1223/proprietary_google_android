package com.android.settings;

import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Intent;
import android.os.AsyncTask;
import android.service.oemlock.OemLockManager;
import android.service.persistentdata.PersistentDataBlockManager;
import android.content.Context;
import android.app.ProgressDialog;
import android.view.View.OnClickListener;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;

public final class _$$Lambda$MasterClearConfirm$weRgiuD2TQnm7jx9NX__qHWwsHU implements DialogInterface$OnDismissListener
{
    public final void onDismiss(final DialogInterface dialogInterface) {
        MasterClearConfirm.lambda$onCreateView$0(this.f$0, dialogInterface);
    }
}
