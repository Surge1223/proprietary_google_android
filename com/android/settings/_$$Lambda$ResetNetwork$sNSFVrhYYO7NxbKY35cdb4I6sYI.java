package com.android.settings;

import com.android.settings.enterprise.ActionDisabledByAdminDialogHelper;
import com.android.settingslib.RestrictedLockUtils;
import android.os.UserHandle;
import android.os.UserManager;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Intent;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.content.ContentResolver;
import android.provider.Settings;
import android.telephony.euicc.EuiccManager;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import java.util.Iterator;
import android.widget.TextView;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.text.TextUtils;
import java.util.ArrayList;
import android.content.Context;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionInfo;
import java.util.List;
import android.widget.Spinner;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.view.View;
import com.android.settings.core.InstrumentedFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;

public final class _$$Lambda$ResetNetwork$sNSFVrhYYO7NxbKY35cdb4I6sYI implements DialogInterface$OnDismissListener
{
    public final void onDismiss(final DialogInterface dialogInterface) {
        ResetNetwork.lambda$onCreateView$0(this.f$0, dialogInterface);
    }
}
