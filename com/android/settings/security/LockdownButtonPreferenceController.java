package com.android.settings.security;

import android.content.ContentResolver;
import android.provider.Settings;
import android.os.UserHandle;
import android.content.Context;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.TogglePreferenceController;

public class LockdownButtonPreferenceController extends TogglePreferenceController
{
    private static final String KEY_LOCKDOWN_ENALBED = "security_setting_lockdown_enabled";
    private final LockPatternUtils mLockPatternUtils;
    
    public LockdownButtonPreferenceController(final Context context) {
        super(context, "security_setting_lockdown_enabled");
        this.mLockPatternUtils = new LockPatternUtils(context);
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (this.mLockPatternUtils.isSecure(UserHandle.myUserId())) {
            return 0;
        }
        return 3;
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Secure.getInt(contentResolver, "lockdown_in_power_menu", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), "lockdown_in_power_menu", (int)(b ? 1 : 0));
        return true;
    }
}
