package com.android.settings.security;

import com.android.settingslib.RestrictedLockUtils;
import android.content.Intent;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.password.ChooseLockGeneric;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import com.android.settings.Utils;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.UserHandle;
import com.android.settingslib.RestrictedSwitchPreference;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class LockUnificationPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private static final int MY_USER_ID;
    private String mCurrentDevicePassword;
    private String mCurrentProfilePassword;
    private final SecuritySettings mHost;
    private final LockPatternUtils mLockPatternUtils;
    private final int mProfileChallengeUserId;
    private final UserManager mUm;
    private RestrictedSwitchPreference mUnifyProfile;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public LockUnificationPreferenceController(final Context context, final SecuritySettings mHost) {
        super(context);
        this.mHost = mHost;
        this.mUm = (UserManager)context.getSystemService("user");
        this.mLockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
        this.mProfileChallengeUserId = Utils.getManagedProfileId(this.mUm, LockUnificationPreferenceController.MY_USER_ID);
    }
    
    private void launchConfirmProfileLockForUnification() {
        if (!new ChooseLockSettingsHelper(this.mHost.getActivity(), this.mHost).launchConfirmationActivity(129, this.mContext.getString(2131889579), true, this.mProfileChallengeUserId)) {
            this.unifyLocks();
        }
    }
    
    private void unifyLocks() {
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mProfileChallengeUserId);
        if (keyguardStoredPasswordQuality == 65536) {
            this.mLockPatternUtils.saveLockPattern(LockPatternUtils.stringToPattern(this.mCurrentProfilePassword), this.mCurrentDevicePassword, LockUnificationPreferenceController.MY_USER_ID);
        }
        else {
            this.mLockPatternUtils.saveLockPassword(this.mCurrentProfilePassword, this.mCurrentDevicePassword, keyguardStoredPasswordQuality, LockUnificationPreferenceController.MY_USER_ID);
        }
        this.mLockPatternUtils.setSeparateProfileChallengeEnabled(this.mProfileChallengeUserId, false, this.mCurrentProfilePassword);
        this.mLockPatternUtils.setVisiblePatternEnabled(this.mLockPatternUtils.isVisiblePatternEnabled(this.mProfileChallengeUserId), LockUnificationPreferenceController.MY_USER_ID);
        this.mCurrentDevicePassword = null;
        this.mCurrentProfilePassword = null;
    }
    
    private void ununifyLocks() {
        final Bundle arguments = new Bundle();
        arguments.putInt("android.intent.extra.USER_ID", this.mProfileChallengeUserId);
        new SubSettingLauncher(this.mContext).setDestination(ChooseLockGeneric.ChooseLockGenericFragment.class.getName()).setTitle(2131888098).setSourceMetricsCategory(this.mHost.getMetricsCategory()).setArguments(arguments).launch();
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mUnifyProfile = (RestrictedSwitchPreference)preferenceScreen.findPreference("unification");
    }
    
    @Override
    public String getPreferenceKey() {
        return "unification";
    }
    
    public boolean handleActivityResult(final int n, final int n2, final Intent intent) {
        if (n == 130 && n2 == -1) {
            this.ununifyLocks();
            return true;
        }
        if (n == 128 && n2 == -1) {
            this.mCurrentDevicePassword = intent.getStringExtra("password");
            this.launchConfirmProfileLockForUnification();
            return true;
        }
        if (n == 129 && n2 == -1) {
            this.mCurrentProfilePassword = intent.getStringExtra("password");
            this.unifyLocks();
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mProfileChallengeUserId != -10000 && this.mLockPatternUtils.isSeparateProfileChallengeAllowed(this.mProfileChallengeUserId);
    }
    
    void launchConfirmDeviceLockForUnification() {
        if (!new ChooseLockSettingsHelper(this.mHost.getActivity(), this.mHost).launchConfirmationActivity(128, this.mContext.getString(2131889578), true, LockUnificationPreferenceController.MY_USER_ID)) {
            this.launchConfirmProfileLockForUnification();
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        final boolean startQuietModeDialogIfNecessary = Utils.startQuietModeDialogIfNecessary(this.mContext, this.mUm, this.mProfileChallengeUserId);
        final boolean b = false;
        if (startQuietModeDialogIfNecessary) {
            return false;
        }
        if (o) {
            boolean b2 = b;
            if (this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mProfileChallengeUserId) >= 65536) {
                b2 = b;
                if (this.mLockPatternUtils.isSeparateProfileChallengeAllowedToUnify(this.mProfileChallengeUserId)) {
                    b2 = true;
                }
            }
            UnificationConfirmationDialog.newInstance(b2).show(this.mHost);
        }
        else if (!new ChooseLockSettingsHelper(this.mHost.getActivity(), this.mHost).launchConfirmationActivity(130, this.mContext.getString(2131889578), true, LockUnificationPreferenceController.MY_USER_ID)) {
            this.ununifyLocks();
        }
        return true;
    }
    
    void unifyUncompliantLocks() {
        this.mLockPatternUtils.setSeparateProfileChallengeEnabled(this.mProfileChallengeUserId, false, this.mCurrentProfilePassword);
        new SubSettingLauncher(this.mContext).setDestination(ChooseLockGeneric.ChooseLockGenericFragment.class.getName()).setTitle(2131888097).setSourceMetricsCategory(this.mHost.getMetricsCategory()).launch();
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mUnifyProfile != null) {
            final boolean separateProfileChallengeEnabled = this.mLockPatternUtils.isSeparateProfileChallengeEnabled(this.mProfileChallengeUserId);
            this.mUnifyProfile.setChecked(separateProfileChallengeEnabled ^ true);
            if (separateProfileChallengeEnabled) {
                this.mUnifyProfile.setDisabledByAdmin(RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, "no_unified_password", this.mProfileChallengeUserId));
            }
        }
    }
}
