package com.android.settings.security;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.text.TextUtils;
import android.app.Activity;
import android.os.Bundle;
import android.support.v14.preference.PreferenceFragment;
import com.android.settings.SettingsActivity;
import com.android.settings.CryptKeeperConfirm;
import android.content.res.Resources;
import android.os.UserHandle;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.app.admin.DevicePolicyManager;
import android.support.v7.preference.Preference;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.View;
import com.android.settings.core.InstrumentedPreferenceFragment;

public class CryptKeeperSettings extends InstrumentedPreferenceFragment
{
    private View mBatteryWarning;
    private View mContentView;
    private Button mInitiateButton;
    private View.OnClickListener mInitiateListener;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mIntentReceiver;
    private View mPowerWarning;
    
    public CryptKeeperSettings() {
        this.mIntentReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
                    final boolean b = false;
                    final int intExtra = intent.getIntExtra("level", 0);
                    final int intExtra2 = intent.getIntExtra("plugged", 0);
                    final int intExtra3 = intent.getIntExtra("invalid_charger", 0);
                    boolean enabled = true;
                    final boolean b2 = intExtra >= 80;
                    final boolean b3 = (intExtra2 & 0x7) != 0x0 && intExtra3 == 0;
                    final Button access$000 = CryptKeeperSettings.this.mInitiateButton;
                    if (!b2 || !b3) {
                        enabled = false;
                    }
                    access$000.setEnabled(enabled);
                    final View access$2 = CryptKeeperSettings.this.mPowerWarning;
                    int visibility;
                    if (b3) {
                        visibility = 8;
                    }
                    else {
                        visibility = 0;
                    }
                    access$2.setVisibility(visibility);
                    final View access$3 = CryptKeeperSettings.this.mBatteryWarning;
                    int visibility2 = b ? 1 : 0;
                    if (b2) {
                        visibility2 = 8;
                    }
                    access$3.setVisibility(visibility2);
                }
            }
        };
        this.mInitiateListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (!CryptKeeperSettings.this.runKeyguardConfirmation(55)) {
                    new AlertDialog$Builder((Context)CryptKeeperSettings.this.getActivity()).setTitle(2131887168).setMessage(2131887167).setPositiveButton(17039370, (DialogInterface$OnClickListener)null).create().show();
                }
            }
        };
    }
    
    private void addEncryptionInfoToPreference(final Preference preference, final int n, final String s) {
        if (((DevicePolicyManager)this.getActivity().getSystemService("device_policy")).getDoNotAskCredentialsOnBoot()) {
            preference.getExtras().putInt("type", 1);
            preference.getExtras().putString("password", "");
        }
        else {
            preference.getExtras().putInt("type", n);
            preference.getExtras().putString("password", s);
        }
    }
    
    private boolean runKeyguardConfirmation(final int n) {
        final Resources resources = this.getActivity().getResources();
        final ChooseLockSettingsHelper chooseLockSettingsHelper = new ChooseLockSettingsHelper(this.getActivity(), this);
        if (chooseLockSettingsHelper.utils().getKeyguardStoredPasswordQuality(UserHandle.myUserId()) == 0) {
            this.showFinalConfirmation(1, "");
            return true;
        }
        return chooseLockSettingsHelper.launchConfirmationActivity(n, resources.getText(2131887169), true);
    }
    
    private void showFinalConfirmation(final int n, final String s) {
        final Preference preference = new Preference(this.getPreferenceManager().getContext());
        preference.setFragment(CryptKeeperConfirm.class.getName());
        preference.setTitle(2131887163);
        this.addEncryptionInfoToPreference(preference, n, s);
        ((SettingsActivity)this.getActivity()).onPreferenceStartFragment(null, preference);
    }
    
    @Override
    public int getMetricsCategory() {
        return 32;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final Activity activity = this.getActivity();
        if ("android.app.action.START_ENCRYPTION".equals(activity.getIntent().getAction())) {
            final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)activity.getSystemService("device_policy");
            if (devicePolicyManager != null && devicePolicyManager.getStorageEncryptionStatus() != 1) {
                activity.finish();
            }
        }
        activity.setTitle(2131887169);
    }
    
    public void onActivityResult(int intExtra, final int n, final Intent intent) {
        super.onActivityResult(intExtra, n, intent);
        if (intExtra != 55) {
            return;
        }
        if (n == -1 && intent != null) {
            intExtra = intent.getIntExtra("type", -1);
            final String stringExtra = intent.getStringExtra("password");
            if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
                this.showFinalConfirmation(intExtra, stringExtra);
            }
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mContentView = layoutInflater.inflate(2131558518, (ViewGroup)null);
        (this.mIntentFilter = new IntentFilter()).addAction("android.intent.action.BATTERY_CHANGED");
        (this.mInitiateButton = (Button)this.mContentView.findViewById(2131362270)).setOnClickListener(this.mInitiateListener);
        this.mInitiateButton.setEnabled(false);
        this.mPowerWarning = this.mContentView.findViewById(2131362816);
        this.mBatteryWarning = this.mContentView.findViewById(2131362815);
        return this.mContentView;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.getActivity().unregisterReceiver(this.mIntentReceiver);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.getActivity().registerReceiver(this.mIntentReceiver, this.mIntentFilter);
    }
}
