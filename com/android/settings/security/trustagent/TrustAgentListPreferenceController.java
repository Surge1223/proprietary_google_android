package com.android.settings.security.trustagent;

import android.os.Parcelable;
import android.os.Bundle;
import android.app.Fragment;
import com.android.settings.password.ChooseLockSettingsHelper;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import java.util.Iterator;
import java.util.List;
import android.support.v7.preference.Preference;
import com.android.settingslib.RestrictedPreference;
import com.android.settings.security.SecurityFeatureProvider;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserHandle;
import android.content.Intent;
import android.support.v7.preference.PreferenceCategory;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.security.SecuritySettings;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class TrustAgentListPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnCreate, OnResume, OnSaveInstanceState
{
    private static final int MY_USER_ID;
    static final String PREF_KEY_SECURITY_CATEGORY = "security_category";
    static final String PREF_KEY_TRUST_AGENT = "trust_agent";
    private final SecuritySettings mHost;
    private final LockPatternUtils mLockPatternUtils;
    private PreferenceCategory mSecurityCategory;
    private Intent mTrustAgentClickIntent;
    private final TrustAgentManager mTrustAgentManager;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public TrustAgentListPreferenceController(final Context context, final SecuritySettings mHost, final Lifecycle lifecycle) {
        super(context);
        final SecurityFeatureProvider securityFeatureProvider = FeatureFactory.getFactory(context).getSecurityFeatureProvider();
        this.mHost = mHost;
        this.mLockPatternUtils = securityFeatureProvider.getLockPatternUtils(context);
        this.mTrustAgentManager = securityFeatureProvider.getTrustAgentManager();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private void updateTrustAgents() {
        if (this.mSecurityCategory == null) {
            return;
        }
        while (true) {
            final Preference preference = this.mSecurityCategory.findPreference("trust_agent");
            if (preference == null) {
                break;
            }
            this.mSecurityCategory.removePreference(preference);
        }
        if (!this.isAvailable()) {
            return;
        }
        final boolean secure = this.mLockPatternUtils.isSecure(TrustAgentListPreferenceController.MY_USER_ID);
        final List<TrustAgentManager.TrustAgentComponentInfo> activeTrustAgents = this.mTrustAgentManager.getActiveTrustAgents(this.mContext, this.mLockPatternUtils);
        if (activeTrustAgents == null) {
            return;
        }
        for (final TrustAgentManager.TrustAgentComponentInfo trustAgentComponentInfo : activeTrustAgents) {
            final RestrictedPreference restrictedPreference = new RestrictedPreference(this.mSecurityCategory.getContext());
            restrictedPreference.setKey("trust_agent");
            restrictedPreference.setTitle(trustAgentComponentInfo.title);
            restrictedPreference.setSummary(trustAgentComponentInfo.summary);
            restrictedPreference.setIntent(new Intent("android.intent.action.MAIN").setComponent(trustAgentComponentInfo.componentName));
            restrictedPreference.setDisabledByAdmin(trustAgentComponentInfo.admin);
            if (!restrictedPreference.isDisabledByAdmin() && !secure) {
                restrictedPreference.setEnabled(false);
                restrictedPreference.setSummary(2131887423);
            }
            this.mSecurityCategory.addPreference(restrictedPreference);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mSecurityCategory = (PreferenceCategory)preferenceScreen.findPreference("security_category");
        this.updateTrustAgents();
    }
    
    @Override
    public String getPreferenceKey() {
        return "trust_agent";
    }
    
    public boolean handleActivityResult(final int n, final int n2) {
        if (n == 126 && n2 == -1) {
            if (this.mTrustAgentClickIntent != null) {
                this.mHost.startActivity(this.mTrustAgentClickIntent);
                this.mTrustAgentClickIntent = null;
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return super.handlePreferenceTreeClick(preference);
        }
        final ChooseLockSettingsHelper chooseLockSettingsHelper = new ChooseLockSettingsHelper(this.mHost.getActivity(), this.mHost);
        this.mTrustAgentClickIntent = preference.getIntent();
        if (!chooseLockSettingsHelper.launchConfirmationActivity(126, preference.getTitle()) && this.mTrustAgentClickIntent != null) {
            this.mHost.startActivity(this.mTrustAgentClickIntent);
            this.mTrustAgentClickIntent = null;
        }
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034168);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        if (bundle != null && bundle.containsKey("trust_agent_click_intent")) {
            this.mTrustAgentClickIntent = (Intent)bundle.getParcelable("trust_agent_click_intent");
        }
    }
    
    @Override
    public void onResume() {
        this.updateTrustAgents();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        if (this.mTrustAgentClickIntent != null) {
            bundle.putParcelable("trust_agent_click_intent", (Parcelable)this.mTrustAgentClickIntent);
        }
    }
}
