package com.android.settings.security.trustagent;

import android.support.v7.preference.Preference;
import com.android.settings.security.SecurityFeatureProvider;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.UserHandle;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.BasePreferenceController;

public class ManageTrustAgentsPreferenceController extends BasePreferenceController
{
    static final String KEY_MANAGE_TRUST_AGENTS = "manage_trust_agents";
    private static final int MY_USER_ID;
    private final LockPatternUtils mLockPatternUtils;
    private TrustAgentManager mTrustAgentManager;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public ManageTrustAgentsPreferenceController(final Context context) {
        super(context, "manage_trust_agents");
        final SecurityFeatureProvider securityFeatureProvider = FeatureFactory.getFactory(context).getSecurityFeatureProvider();
        this.mLockPatternUtils = securityFeatureProvider.getLockPatternUtils(context);
        this.mTrustAgentManager = securityFeatureProvider.getTrustAgentManager();
    }
    
    private int getTrustAgentCount() {
        return this.mTrustAgentManager.getActiveTrustAgents(this.mContext, this.mLockPatternUtils).size();
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034149)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public void updateState(final Preference preference) {
        final int trustAgentCount = this.getTrustAgentCount();
        if (!this.mLockPatternUtils.isSecure(ManageTrustAgentsPreferenceController.MY_USER_ID)) {
            preference.setEnabled(false);
            preference.setSummary(2131887423);
        }
        else if (trustAgentCount > 0) {
            preference.setEnabled(true);
            preference.setSummary(this.mContext.getResources().getQuantityString(2131755043, trustAgentCount, new Object[] { trustAgentCount }));
        }
        else {
            preference.setEnabled(true);
            preference.setSummary(2131888218);
        }
    }
}
