package com.android.settings.security.trustagent;

import android.graphics.drawable.Drawable;
import android.support.v14.preference.SwitchPreference;
import com.android.settings.overlay.FeatureFactory;
import android.os.Bundle;
import android.app.Activity;
import com.android.settingslib.RestrictedSwitchPreference;
import com.android.settingslib.RestrictedLockUtils;
import android.support.v7.preference.PreferenceGroup;
import android.content.Context;
import java.util.Collection;
import android.os.UserHandle;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import com.android.internal.widget.LockPatternUtils;
import android.app.admin.DevicePolicyManager;
import android.util.ArrayMap;
import android.content.ComponentName;
import android.util.ArraySet;
import android.support.v7.preference.Preference;
import com.android.settings.SettingsPreferenceFragment;

public class TrustAgentSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener
{
    private final ArraySet<ComponentName> mActiveAgents;
    private ArrayMap<ComponentName, AgentInfo> mAvailableAgents;
    private DevicePolicyManager mDpm;
    private LockPatternUtils mLockPatternUtils;
    private TrustAgentManager mTrustAgentManager;
    
    public TrustAgentSettings() {
        this.mActiveAgents = (ArraySet<ComponentName>)new ArraySet();
    }
    
    private ArrayMap<ComponentName, AgentInfo> findAvailableTrustAgents() {
        final PackageManager packageManager = this.getActivity().getPackageManager();
        final List queryIntentServices = packageManager.queryIntentServices(new Intent("android.service.trust.TrustAgentService"), 128);
        final ArrayMap arrayMap = new ArrayMap();
        final int size = queryIntentServices.size();
        arrayMap.ensureCapacity(size);
        for (int i = 0; i < size; ++i) {
            final ResolveInfo resolveInfo = queryIntentServices.get(i);
            if (resolveInfo.serviceInfo != null) {
                if (this.mTrustAgentManager.shouldProvideTrust(resolveInfo, packageManager)) {
                    final ComponentName componentName = this.mTrustAgentManager.getComponentName(resolveInfo);
                    final AgentInfo agentInfo = new AgentInfo();
                    agentInfo.label = resolveInfo.loadLabel(packageManager);
                    agentInfo.icon = resolveInfo.loadIcon(packageManager);
                    arrayMap.put((Object)(agentInfo.component = componentName), (Object)agentInfo);
                }
            }
        }
        return (ArrayMap<ComponentName, AgentInfo>)arrayMap;
    }
    
    private void loadActiveAgents() {
        final List enabledTrustAgents = this.mLockPatternUtils.getEnabledTrustAgents(UserHandle.myUserId());
        if (enabledTrustAgents != null) {
            this.mActiveAgents.addAll((Collection)enabledTrustAgents);
        }
    }
    
    private void saveActiveAgents() {
        this.mLockPatternUtils.setEnabledTrustAgents((Collection)this.mActiveAgents, UserHandle.myUserId());
    }
    
    private void updateAgents() {
        final Activity activity = this.getActivity();
        if (this.mAvailableAgents == null) {
            this.mAvailableAgents = this.findAvailableTrustAgents();
        }
        if (this.mLockPatternUtils == null) {
            this.mLockPatternUtils = new LockPatternUtils((Context)this.getActivity());
        }
        this.loadActiveAgents();
        final PreferenceGroup preferenceGroup = (PreferenceGroup)this.getPreferenceScreen().findPreference("trust_agents");
        preferenceGroup.removeAll();
        final RestrictedLockUtils.EnforcedAdmin checkIfKeyguardFeaturesDisabled = RestrictedLockUtils.checkIfKeyguardFeaturesDisabled((Context)activity, 16, UserHandle.myUserId());
        for (int size = this.mAvailableAgents.size(), i = 0; i < size; ++i) {
            final AgentInfo agentInfo = (AgentInfo)this.mAvailableAgents.valueAt(i);
            final RestrictedSwitchPreference preference = new RestrictedSwitchPreference(this.getPrefContext());
            preference.useAdminDisabledSummary(true);
            (agentInfo.preference = preference).setPersistent(false);
            preference.setTitle(agentInfo.label);
            preference.setIcon(agentInfo.icon);
            preference.setPersistent(false);
            preference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            preference.setChecked(this.mActiveAgents.contains((Object)agentInfo.component));
            if (checkIfKeyguardFeaturesDisabled != null && this.mDpm.getTrustAgentConfiguration((ComponentName)null, agentInfo.component) == null) {
                preference.setChecked(false);
                preference.setDisabledByAdmin(checkIfKeyguardFeaturesDisabled);
            }
            preferenceGroup.addPreference(agentInfo.preference);
        }
    }
    
    @Override
    public int getHelpResource() {
        return 2131887833;
    }
    
    @Override
    public int getMetricsCategory() {
        return 91;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mDpm = (DevicePolicyManager)this.getActivity().getSystemService((Class)DevicePolicyManager.class);
        this.mTrustAgentManager = FeatureFactory.getFactory((Context)this.getActivity()).getSecurityFeatureProvider().getTrustAgentManager();
        this.addPreferencesFromResource(2132082847);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference instanceof SwitchPreference) {
            for (int size = this.mAvailableAgents.size(), i = 0; i < size; ++i) {
                final AgentInfo agentInfo = (AgentInfo)this.mAvailableAgents.valueAt(i);
                if (agentInfo.preference == preference) {
                    if (o) {
                        if (!this.mActiveAgents.contains((Object)agentInfo.component)) {
                            this.mActiveAgents.add((Object)agentInfo.component);
                        }
                    }
                    else {
                        this.mActiveAgents.remove((Object)agentInfo.component);
                    }
                    this.saveActiveAgents();
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.removePreference("dummy_preference");
        this.updateAgents();
    }
    
    public static final class AgentInfo
    {
        ComponentName component;
        public Drawable icon;
        CharSequence label;
        SwitchPreference preference;
        
        @Override
        public boolean equals(final Object o) {
            return !(o instanceof AgentInfo) || this.component.equals((Object)((AgentInfo)o).component);
        }
    }
}
