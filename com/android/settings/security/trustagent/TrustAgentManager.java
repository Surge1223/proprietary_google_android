package com.android.settings.security.trustagent;

import android.content.res.XmlResourceParser;
import android.util.Log;
import java.util.Iterator;
import android.text.TextUtils;
import com.android.settingslib.RestrictedLockUtils;
import java.util.ArrayList;
import android.app.admin.DevicePolicyManager;
import android.os.UserHandle;
import java.util.List;
import com.android.internal.widget.LockPatternUtils;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.res.Resources;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import com.android.internal.R$styleable;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.util.Slog;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import android.content.Intent;

public class TrustAgentManager
{
    static final String PERMISSION_PROVIDE_AGENT = "android.permission.PROVIDE_TRUST_AGENT";
    private static final Intent TRUST_AGENT_INTENT;
    
    static {
        TRUST_AGENT_INTENT = new Intent("android.service.trust.TrustAgentService");
    }
    
    private TrustAgentComponentInfo getSettingsComponent(final PackageManager packageManager, final ResolveInfo resolveInfo) {
        final ComponentName componentName = null;
        if (resolveInfo == null || resolveInfo.serviceInfo == null || resolveInfo.serviceInfo.metaData == null) {
            return null;
        }
        final Object o = null;
        final Object o2 = null;
        final Object o3 = null;
        final TrustAgentComponentInfo trustAgentComponentInfo = new TrustAgentComponentInfo();
        Object o4 = null;
        Object o5 = null;
        Object o6 = null;
        Object o7 = null;
        final Throwable t = null;
        final Throwable t2 = null;
        while (true) {
            try {
                final Object loadXmlMetaData = resolveInfo.serviceInfo.loadXmlMetaData(packageManager, "android.service.trust.trustagent");
                if (loadXmlMetaData == null) {
                    o7 = loadXmlMetaData;
                    o4 = loadXmlMetaData;
                    o5 = loadXmlMetaData;
                    o6 = loadXmlMetaData;
                    Slog.w("TrustAgentManager", "Can't find android.service.trust.trustagent meta-data");
                    return null;
                }
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                final Resources resourcesForApplication = packageManager.getResourcesForApplication(resolveInfo.serviceInfo.applicationInfo);
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)loadXmlMetaData);
                int next;
                do {
                    o7 = loadXmlMetaData;
                    o4 = loadXmlMetaData;
                    o5 = loadXmlMetaData;
                    o6 = loadXmlMetaData;
                    next = ((XmlResourceParser)loadXmlMetaData).next();
                } while (next != 1 && next != 2);
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                if (!"trust-agent".equals(((XmlResourceParser)loadXmlMetaData).getName())) {
                    o7 = loadXmlMetaData;
                    o4 = loadXmlMetaData;
                    o5 = loadXmlMetaData;
                    o6 = loadXmlMetaData;
                    Slog.w("TrustAgentManager", "Meta-data does not start with trust-agent tag");
                    return null;
                }
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                final TypedArray obtainAttributes = resourcesForApplication.obtainAttributes(attributeSet, R$styleable.TrustAgent);
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                trustAgentComponentInfo.summary = obtainAttributes.getString(1);
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                trustAgentComponentInfo.title = obtainAttributes.getString(0);
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                final String string = obtainAttributes.getString(2);
                o7 = loadXmlMetaData;
                o4 = loadXmlMetaData;
                o5 = loadXmlMetaData;
                o6 = loadXmlMetaData;
                obtainAttributes.recycle();
                o7 = t;
                if (loadXmlMetaData != null) {
                    o7 = t2;
                    ((XmlResourceParser)loadXmlMetaData).close();
                }
            }
            catch (XmlPullParserException ex) {
                o7 = ex;
                if (o4 != null) {
                    final Object loadXmlMetaData = o4;
                    o7 = ex;
                    continue;
                }
            }
            catch (IOException ex2) {
                o7 = ex2;
                if (o5 != null) {
                    final Object loadXmlMetaData = o5;
                    o7 = ex2;
                    continue;
                }
            }
            catch (PackageManager$NameNotFoundException ex3) {
                o7 = ex3;
                if (o6 != null) {
                    final Object loadXmlMetaData = o6;
                    o7 = ex3;
                    continue;
                }
            }
            finally {
                if (o7 != null) {
                    ((XmlResourceParser)o7).close();
                }
            }
            break;
        }
        if (o7 != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error parsing : ");
            sb.append(resolveInfo.serviceInfo.packageName);
            Slog.w("TrustAgentManager", sb.toString(), (Throwable)o7);
            return null;
        }
        final String s;
        String string2;
        if ((string2 = s) != null) {
            string2 = s;
            if (s.indexOf(47) < 0) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(resolveInfo.serviceInfo.packageName);
                sb2.append("/");
                sb2.append(s);
                string2 = sb2.toString();
            }
        }
        ComponentName unflattenFromString;
        if (string2 == null) {
            unflattenFromString = componentName;
        }
        else {
            unflattenFromString = ComponentName.unflattenFromString(string2);
        }
        trustAgentComponentInfo.componentName = unflattenFromString;
        return trustAgentComponentInfo;
    }
    
    public CharSequence getActiveTrustAgentLabel(final Context context, final LockPatternUtils lockPatternUtils) {
        final List<TrustAgentComponentInfo> activeTrustAgents = this.getActiveTrustAgents(context, lockPatternUtils);
        CharSequence title;
        if (activeTrustAgents.isEmpty()) {
            title = null;
        }
        else {
            title = activeTrustAgents.get(0).title;
        }
        return title;
    }
    
    public List<TrustAgentComponentInfo> getActiveTrustAgents(final Context context, final LockPatternUtils lockPatternUtils) {
        final int myUserId = UserHandle.myUserId();
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService((Class)DevicePolicyManager.class);
        final PackageManager packageManager = context.getPackageManager();
        final ArrayList<TrustAgentComponentInfo> list = new ArrayList<TrustAgentComponentInfo>();
        final List queryIntentServices = packageManager.queryIntentServices(TrustAgentManager.TRUST_AGENT_INTENT, 128);
        final List enabledTrustAgents = lockPatternUtils.getEnabledTrustAgents(myUserId);
        final RestrictedLockUtils.EnforcedAdmin checkIfKeyguardFeaturesDisabled = RestrictedLockUtils.checkIfKeyguardFeaturesDisabled(context, 16, myUserId);
        if (enabledTrustAgents != null && !enabledTrustAgents.isEmpty()) {
            for (final ResolveInfo resolveInfo : queryIntentServices) {
                if (resolveInfo.serviceInfo != null) {
                    if (!this.shouldProvideTrust(resolveInfo, packageManager)) {
                        continue;
                    }
                    final TrustAgentComponentInfo settingsComponent = this.getSettingsComponent(packageManager, resolveInfo);
                    if (settingsComponent.componentName == null || !enabledTrustAgents.contains(this.getComponentName(resolveInfo))) {
                        continue;
                    }
                    if (TextUtils.isEmpty((CharSequence)settingsComponent.title)) {
                        continue;
                    }
                    if (checkIfKeyguardFeaturesDisabled != null && devicePolicyManager.getTrustAgentConfiguration((ComponentName)null, this.getComponentName(resolveInfo)) == null) {
                        settingsComponent.admin = checkIfKeyguardFeaturesDisabled;
                    }
                    list.add(settingsComponent);
                    break;
                }
            }
        }
        return list;
    }
    
    public ComponentName getComponentName(final ResolveInfo resolveInfo) {
        if (resolveInfo != null && resolveInfo.serviceInfo != null) {
            return new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
        }
        return null;
    }
    
    public boolean shouldProvideTrust(final ResolveInfo resolveInfo, final PackageManager packageManager) {
        final String packageName = resolveInfo.serviceInfo.packageName;
        if (packageManager.checkPermission("android.permission.PROVIDE_TRUST_AGENT", packageName) != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Skipping agent because package ");
            sb.append(packageName);
            sb.append(" does not have permission ");
            sb.append("android.permission.PROVIDE_TRUST_AGENT");
            sb.append(".");
            Log.w("TrustAgentManager", sb.toString());
            return false;
        }
        return true;
    }
    
    public static class TrustAgentComponentInfo
    {
        public RestrictedLockUtils.EnforcedAdmin admin;
        public ComponentName componentName;
        public String summary;
        public String title;
        
        public TrustAgentComponentInfo() {
            this.admin = null;
        }
    }
}
