package com.android.settings.security;

import android.support.v7.preference.Preference;
import android.content.Context;
import android.security.KeyStore;

public class CredentialStoragePreferenceController extends RestrictedEncryptionPreferenceController
{
    private final KeyStore mKeyStore;
    
    public CredentialStoragePreferenceController(final Context context) {
        super(context, "no_config_credentials");
        this.mKeyStore = KeyStore.getInstance();
    }
    
    @Override
    public String getPreferenceKey() {
        return "credential_storage_type";
    }
    
    @Override
    public void updateState(final Preference preference) {
        int summary;
        if (this.mKeyStore.isHardwareBacked()) {
            summary = 2131887142;
        }
        else {
            summary = 2131887143;
        }
        preference.setSummary(summary);
    }
}
