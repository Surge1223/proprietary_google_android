package com.android.settings.security;

import android.content.ContentResolver;
import android.provider.Settings;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import android.os.UserHandle;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.TogglePreferenceController;

public class ShowPasswordPreferenceController extends TogglePreferenceController
{
    private static final String KEY_SHOW_PASSWORD = "show_password";
    private static final int MY_USER_ID;
    private final LockPatternUtils mLockPatternUtils;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public ShowPasswordPreferenceController(final Context context) {
        super(context, "show_password");
        this.mLockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034163)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public boolean isChecked() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = true;
        if (Settings.System.getInt(contentResolver, "show_password", 1) == 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        Settings.System.putInt(this.mContext.getContentResolver(), "show_password", (int)(b ? 1 : 0));
        this.mLockPatternUtils.setVisiblePasswordEnabled(b, ShowPasswordPreferenceController.MY_USER_ID);
        return true;
    }
}
