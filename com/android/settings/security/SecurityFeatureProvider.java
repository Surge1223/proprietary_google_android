package com.android.settings.security;

import com.android.settings.security.trustagent.TrustAgentManager;
import com.android.internal.widget.LockPatternUtils;
import android.content.Context;

public interface SecurityFeatureProvider
{
    LockPatternUtils getLockPatternUtils(final Context p0);
    
    TrustAgentManager getTrustAgentManager();
}
