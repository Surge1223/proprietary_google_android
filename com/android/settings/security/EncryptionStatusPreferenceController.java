package com.android.settings.security;

import com.android.internal.widget.LockPatternUtils;
import android.support.v7.preference.Preference;
import android.text.TextUtils;
import android.content.Context;
import android.os.UserManager;
import com.android.settings.core.BasePreferenceController;

public class EncryptionStatusPreferenceController extends BasePreferenceController
{
    static final String PREF_KEY_ENCRYPTION_DETAIL_PAGE = "encryption_and_credentials_encryption_status";
    static final String PREF_KEY_ENCRYPTION_SECURITY_PAGE = "encryption_and_credential";
    private final UserManager mUserManager;
    
    public EncryptionStatusPreferenceController(final Context context, final String s) {
        super(context, s);
        this.mUserManager = (UserManager)context.getSystemService("user");
    }
    
    @Override
    public int getAvailabilityStatus() {
        if (TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"encryption_and_credentials_encryption_status") && !this.mContext.getResources().getBoolean(2131034144)) {
            return 2;
        }
        int n;
        if (this.mUserManager.isAdminUser()) {
            n = 0;
        }
        else {
            n = 3;
        }
        return n;
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (LockPatternUtils.isDeviceEncryptionEnabled()) {
            if (TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"encryption_and_credentials_encryption_status")) {
                preference.setFragment(null);
            }
            preference.setSummary(2131887170);
        }
        else {
            if (TextUtils.equals((CharSequence)this.getPreferenceKey(), (CharSequence)"encryption_and_credentials_encryption_status")) {
                preference.setFragment(CryptKeeperSettings.class.getName());
            }
            preference.setSummary(2131887346);
        }
    }
}
