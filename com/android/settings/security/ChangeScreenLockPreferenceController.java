package com.android.settings.security;

import com.android.settings.security.screenlock.ScreenLockSettings;
import com.android.settings.password.ChooseLockGeneric;
import com.android.settings.core.SubSettingLauncher;
import android.os.storage.StorageManager;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settings.Utils;
import com.android.settings.overlay.FeatureFactory;
import android.os.UserHandle;
import android.content.Context;
import android.os.UserManager;
import com.android.settingslib.RestrictedPreference;
import com.android.internal.widget.LockPatternUtils;
import android.app.admin.DevicePolicyManager;
import com.android.settings.widget.GearPreference;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class ChangeScreenLockPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, OnGearClickListener
{
    protected final DevicePolicyManager mDPM;
    protected final SecuritySettings mHost;
    protected final LockPatternUtils mLockPatternUtils;
    protected RestrictedPreference mPreference;
    protected final int mProfileChallengeUserId;
    protected final UserManager mUm;
    protected final int mUserId;
    
    public ChangeScreenLockPreferenceController(final Context context, final SecuritySettings mHost) {
        super(context);
        this.mUserId = UserHandle.myUserId();
        this.mUm = (UserManager)context.getSystemService("user");
        this.mDPM = (DevicePolicyManager)context.getSystemService("device_policy");
        this.mLockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
        this.mHost = mHost;
        this.mProfileChallengeUserId = Utils.getManagedProfileId(this.mUm, this.mUserId);
    }
    
    void disableIfPasswordQualityManaged(final int n) {
        final RestrictedLockUtils.EnforcedAdmin checkIfPasswordQualityIsSet = RestrictedLockUtils.checkIfPasswordQualityIsSet(this.mContext, n);
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)this.mContext.getSystemService("device_policy");
        if (checkIfPasswordQualityIsSet != null && devicePolicyManager.getPasswordQuality(checkIfPasswordQualityIsSet.component, n) == 524288) {
            this.mPreference.setDisabledByAdmin(checkIfPasswordQualityIsSet);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (RestrictedPreference)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "unlock_set_or_change";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return super.handlePreferenceTreeClick(preference);
        }
        if (this.mProfileChallengeUserId != -10000 && !this.mLockPatternUtils.isSeparateProfileChallengeEnabled(this.mProfileChallengeUserId) && StorageManager.isFileEncryptedNativeOnly() && Utils.startQuietModeDialogIfNecessary(this.mContext, this.mUm, this.mProfileChallengeUserId)) {
            return false;
        }
        new SubSettingLauncher(this.mContext).setDestination(ChooseLockGeneric.ChooseLockGenericFragment.class.getName()).setTitle(2131888097).setSourceMetricsCategory(this.mHost.getMetricsCategory()).launch();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mContext.getResources().getBoolean(2131034170);
    }
    
    @Override
    public void onGearClick(final GearPreference gearPreference) {
        if (TextUtils.equals((CharSequence)gearPreference.getKey(), (CharSequence)this.getPreferenceKey())) {
            new SubSettingLauncher(this.mContext).setDestination(ScreenLockSettings.class.getName()).setSourceMetricsCategory(this.mHost.getMetricsCategory()).launch();
        }
    }
    
    @Override
    public void updateState(final Preference preference) {
        if (this.mPreference != null && this.mPreference instanceof GearPreference) {
            if (!this.mLockPatternUtils.isSecure(this.mUserId) && this.mLockPatternUtils.isLockScreenDisabled(this.mUserId)) {
                ((GearPreference)this.mPreference).setOnGearClickListener(null);
            }
            else {
                ((GearPreference)this.mPreference).setOnGearClickListener((GearPreference.OnGearClickListener)this);
            }
        }
        this.updateSummary(preference, this.mUserId);
        this.disableIfPasswordQualityManaged(this.mUserId);
        if (!this.mLockPatternUtils.isSeparateProfileChallengeEnabled(this.mProfileChallengeUserId)) {
            this.disableIfPasswordQualityManaged(this.mProfileChallengeUserId);
        }
    }
    
    protected void updateSummary(final Preference preference, int keyguardStoredPasswordQuality) {
        if (!this.mLockPatternUtils.isSecure(keyguardStoredPasswordQuality)) {
            if (keyguardStoredPasswordQuality != this.mProfileChallengeUserId && !this.mLockPatternUtils.isLockScreenDisabled(keyguardStoredPasswordQuality)) {
                preference.setSummary(2131889580);
            }
            else {
                preference.setSummary(2131889581);
            }
        }
        else {
            keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(keyguardStoredPasswordQuality);
            if (keyguardStoredPasswordQuality != 65536) {
                if (keyguardStoredPasswordQuality != 131072 && keyguardStoredPasswordQuality != 196608) {
                    if (keyguardStoredPasswordQuality == 262144 || keyguardStoredPasswordQuality == 327680 || keyguardStoredPasswordQuality == 393216 || keyguardStoredPasswordQuality == 524288) {
                        preference.setSummary(2131889582);
                    }
                }
                else {
                    preference.setSummary(2131889584);
                }
            }
            else {
                preference.setSummary(2131889583);
            }
        }
        this.mPreference.setEnabled(true);
    }
}
