package com.android.settings.security;

import android.arch.lifecycle.LifecycleObserver;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import android.app.Fragment;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settings.users.AddUserWhenLockedPreferenceController;
import com.android.settings.notification.LockScreenNotificationPreferenceController;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class LockscreenDashboardFragment extends DashboardFragment implements OwnerInfoCallback
{
    static final String KEY_ADD_USER_FROM_LOCK_SCREEN = "security_lockscreen_add_users_when_locked";
    static final String KEY_LOCK_SCREEN_NOTIFICATON = "security_setting_lock_screen_notif";
    static final String KEY_LOCK_SCREEN_NOTIFICATON_WORK_PROFILE = "security_setting_lock_screen_notif_work";
    static final String KEY_LOCK_SCREEN_NOTIFICATON_WORK_PROFILE_HEADER = "security_setting_lock_screen_notif_work_header";
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private OwnerInfoPreferenceController mOwnerInfoPreferenceController;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                final ArrayList<AddUserWhenLockedPreferenceController> list = (ArrayList<AddUserWhenLockedPreferenceController>)new ArrayList<LockdownButtonPreferenceController>();
                list.add((LockdownButtonPreferenceController)new LockScreenNotificationPreferenceController(context));
                list.add((LockdownButtonPreferenceController)new AddUserWhenLockedPreferenceController(context, "security_lockscreen_add_users_when_locked", null));
                list.add((LockdownButtonPreferenceController)new OwnerInfoPreferenceController(context, null, null));
                list.add(new LockdownButtonPreferenceController(context));
                return (List<AbstractPreferenceController>)list;
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("security_lockscreen_add_users_when_locked");
                nonIndexableKeys.add("security_setting_lock_screen_notif_work");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082825;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        final ArrayList<AddUserWhenLockedPreferenceController> list = (ArrayList<AddUserWhenLockedPreferenceController>)new ArrayList<LockdownButtonPreferenceController>();
        final Lifecycle lifecycle = this.getLifecycle();
        final LockScreenNotificationPreferenceController lockScreenNotificationPreferenceController = new LockScreenNotificationPreferenceController(context, "security_setting_lock_screen_notif", "security_setting_lock_screen_notif_work_header", "security_setting_lock_screen_notif_work");
        lifecycle.addObserver(lockScreenNotificationPreferenceController);
        list.add((LockdownButtonPreferenceController)lockScreenNotificationPreferenceController);
        list.add((LockdownButtonPreferenceController)new AddUserWhenLockedPreferenceController(context, "security_lockscreen_add_users_when_locked", lifecycle));
        list.add((LockdownButtonPreferenceController)(this.mOwnerInfoPreferenceController = new OwnerInfoPreferenceController(context, this, lifecycle)));
        list.add(new LockdownButtonPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887816;
    }
    
    @Override
    protected String getLogTag() {
        return "LockscreenDashboardFragment";
    }
    
    @Override
    public int getMetricsCategory() {
        return 882;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082825;
    }
    
    @Override
    public void onOwnerInfoUpdated() {
        if (this.mOwnerInfoPreferenceController != null) {
            this.mOwnerInfoPreferenceController.updateSummary();
        }
    }
}
