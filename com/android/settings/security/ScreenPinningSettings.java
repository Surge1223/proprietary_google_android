package com.android.settings.security;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.Switch;
import com.android.settings.SettingsActivity;
import android.os.Bundle;
import android.content.Intent;
import android.provider.Settings;
import android.content.ContentResolver;
import android.provider.Settings;
import android.os.UserHandle;
import java.util.Arrays;
import android.provider.SearchIndexableResource;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.support.v14.preference.SwitchPreference;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.widget.SwitchBar;
import com.android.settings.search.Indexable;
import com.android.settings.SettingsPreferenceFragment;

public class ScreenPinningSettings extends SettingsPreferenceFragment implements Indexable, OnSwitchChangeListener
{
    private static final CharSequence KEY_USE_SCREEN_LOCK;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private LockPatternUtils mLockPatternUtils;
    private SwitchBar mSwitchBar;
    private SwitchPreference mUseScreenLock;
    
    static {
        KEY_USE_SCREEN_LOCK = "use_screen_lock";
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082823;
                return Arrays.asList(searchIndexableResource);
            }
        };
    }
    
    private int getCurrentSecurityTitle() {
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(UserHandle.myUserId());
        if (keyguardStoredPasswordQuality != 65536) {
            if (keyguardStoredPasswordQuality == 131072 || keyguardStoredPasswordQuality == 196608) {
                return 2131888900;
            }
            if (keyguardStoredPasswordQuality == 262144 || keyguardStoredPasswordQuality == 327680 || keyguardStoredPasswordQuality == 393216 || keyguardStoredPasswordQuality == 524288) {
                return 2131888898;
            }
        }
        else if (this.mLockPatternUtils.isLockPatternEnabled(UserHandle.myUserId())) {
            return 2131888899;
        }
        return 2131888897;
    }
    
    private static boolean isLockToAppEnabled(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = false;
        if (Settings.System.getInt(contentResolver, "lock_to_app_enabled", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    private boolean isScreenLockUsed() {
        final int currentSecurityTitle = this.getCurrentSecurityTitle();
        boolean b = false;
        int n;
        if (currentSecurityTitle != 2131888897) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (Settings.Secure.getInt(this.getContentResolver(), "lock_to_app_exit_locked", n) != 0) {
            b = true;
        }
        return b;
    }
    
    private void setLockToAppEnabled(final boolean b) {
        Settings.System.putInt(this.getContentResolver(), "lock_to_app_enabled", (int)(b ? 1 : 0));
        if ((b ? 1 : 0) != 0) {
            this.setScreenLockUsedSetting(this.isScreenLockUsed());
        }
    }
    
    private boolean setScreenLockUsed(final boolean screenLockUsedSetting) {
        if (screenLockUsedSetting && new LockPatternUtils((Context)this.getActivity()).getKeyguardStoredPasswordQuality(UserHandle.myUserId()) == 0) {
            final Intent intent = new Intent("android.app.action.SET_NEW_PASSWORD");
            intent.putExtra("minimum_quality", 65536);
            this.startActivityForResult(intent, 43);
            return false;
        }
        this.setScreenLockUsedSetting(screenLockUsedSetting);
        return true;
    }
    
    private void setScreenLockUsedSetting(final boolean b) {
        Settings.Secure.putInt(this.getContentResolver(), "lock_to_app_exit_locked", (int)(b ? 1 : 0));
    }
    
    @Override
    public int getHelpResource() {
        return 2131887826;
    }
    
    @Override
    public int getMetricsCategory() {
        return 86;
    }
    
    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        final SettingsActivity settingsActivity = (SettingsActivity)this.getActivity();
        settingsActivity.setTitle(2131888896);
        this.mLockPatternUtils = new LockPatternUtils((Context)settingsActivity);
        (this.mSwitchBar = settingsActivity.getSwitchBar()).addOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        this.mSwitchBar.show();
        this.mSwitchBar.setChecked(isLockToAppEnabled((Context)this.getActivity()));
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 43) {
            final boolean b = new LockPatternUtils((Context)this.getActivity()).getKeyguardStoredPasswordQuality(UserHandle.myUserId()) != 0;
            this.setScreenLockUsed(b);
            this.mUseScreenLock.setChecked(b);
        }
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.mSwitchBar.removeOnSwitchChangeListener((SwitchBar.OnSwitchChangeListener)this);
        this.mSwitchBar.hide();
    }
    
    @Override
    public void onSwitchChanged(final Switch switch1, final boolean lockToAppEnabled) {
        this.setLockToAppEnabled(lockToAppEnabled);
        this.updateDisplay();
    }
    
    @Override
    public void onViewCreated(View inflate, final Bundle bundle) {
        super.onViewCreated(inflate, bundle);
        final ViewGroup viewGroup = (ViewGroup)inflate.findViewById(16908351);
        inflate = LayoutInflater.from(this.getContext()).inflate(2131558726, viewGroup, false);
        viewGroup.addView(inflate);
        this.setEmptyView(inflate);
    }
    
    public void updateDisplay() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (preferenceScreen != null) {
            preferenceScreen.removeAll();
        }
        if (isLockToAppEnabled((Context)this.getActivity())) {
            this.addPreferencesFromResource(2132082823);
            (this.mUseScreenLock = (SwitchPreference)this.getPreferenceScreen().findPreference(ScreenPinningSettings.KEY_USE_SCREEN_LOCK)).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(final Preference preference, final Object o) {
                    return ScreenPinningSettings.this.setScreenLockUsed((boolean)o);
                }
            });
            this.mUseScreenLock.setChecked(this.isScreenLockUsed());
            this.mUseScreenLock.setTitle(this.getCurrentSecurityTitle());
        }
    }
}
