package com.android.settings.security;

import android.content.Intent;
import android.app.Activity;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class ConfigureKeyGuardDialog extends InstrumentedDialogFragment implements DialogInterface$OnClickListener, DialogInterface$OnDismissListener
{
    private boolean mConfigureConfirmed;
    
    public int getMetricsCategory() {
        return 1010;
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        this.mConfigureConfirmed = (n == -1);
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        return (Dialog)new AlertDialog$Builder((Context)this.getActivity()).setTitle(17039380).setMessage(2131887145).setPositiveButton(2131887144, (DialogInterface$OnClickListener)this).setNegativeButton(17039360, (DialogInterface$OnClickListener)this).create();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        if (this.mConfigureConfirmed) {
            this.mConfigureConfirmed = false;
            this.startPasswordSetup();
            return;
        }
        final Activity activity = this.getActivity();
        if (activity != null) {
            activity.finish();
        }
    }
    
    void startPasswordSetup() {
        final Intent intent = new Intent("android.app.action.SET_NEW_PASSWORD");
        intent.putExtra("minimum_quality", 65536);
        this.startActivity(intent);
    }
}
