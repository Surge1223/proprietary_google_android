package com.android.settings.security;

import android.os.UserManager;
import android.provider.SearchIndexableResource;
import com.android.settings.search.BaseSearchIndexProvider;
import java.util.Arrays;
import com.android.settings.widget.PreferenceCategoryController;
import java.util.ArrayList;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class EncryptionAndCredential extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new SecuritySearchIndexProvider();
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle) {
        final ArrayList<PreferenceCategoryController> list = (ArrayList<PreferenceCategoryController>)new ArrayList<InstallCredentialsPreferenceController>();
        final EncryptionStatusPreferenceController encryptionStatusPreferenceController = new EncryptionStatusPreferenceController(context, "encryption_and_credentials_encryption_status");
        list.add((InstallCredentialsPreferenceController)encryptionStatusPreferenceController);
        list.add((InstallCredentialsPreferenceController)new PreferenceCategoryController(context, "encryption_and_credentials_status_category").setChildren(Arrays.asList(encryptionStatusPreferenceController)));
        list.add((InstallCredentialsPreferenceController)new CredentialStoragePreferenceController(context));
        list.add((InstallCredentialsPreferenceController)new UserCredentialsPreferenceController(context));
        list.add((InstallCredentialsPreferenceController)new ResetCredentialsPreferenceController(context, lifecycle));
        list.add(new InstallCredentialsPreferenceController(context));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle());
    }
    
    @Override
    public int getHelpResource() {
        return 2131887810;
    }
    
    @Override
    protected String getLogTag() {
        return "EncryptionAndCredential";
    }
    
    @Override
    public int getMetricsCategory() {
        return 846;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082770;
    }
    
    private static class SecuritySearchIndexProvider extends BaseSearchIndexProvider
    {
        @Override
        public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
            return buildPreferenceControllers(context, null);
        }
        
        @Override
        public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
            final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
            searchIndexableResource.xmlResId = 2132082770;
            return Arrays.asList(searchIndexableResource);
        }
        
        @Override
        protected boolean isPageSearchEnabled(final Context context) {
            return ((UserManager)context.getSystemService("user")).isAdminUser();
        }
    }
}
