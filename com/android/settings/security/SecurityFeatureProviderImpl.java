package com.android.settings.security;

import android.content.Context;
import com.android.settings.security.trustagent.TrustAgentManager;
import com.android.internal.widget.LockPatternUtils;

public class SecurityFeatureProviderImpl implements SecurityFeatureProvider
{
    private LockPatternUtils mLockPatternUtils;
    private TrustAgentManager mTrustAgentManager;
    
    @Override
    public LockPatternUtils getLockPatternUtils(final Context context) {
        if (this.mLockPatternUtils == null) {
            this.mLockPatternUtils = new LockPatternUtils(context.getApplicationContext());
        }
        return this.mLockPatternUtils;
    }
    
    @Override
    public TrustAgentManager getTrustAgentManager() {
        if (this.mTrustAgentManager == null) {
            this.mTrustAgentManager = new TrustAgentManager();
        }
        return this.mTrustAgentManager;
    }
}
