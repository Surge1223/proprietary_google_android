package com.android.settings.security;

import com.android.settings.users.OwnerInfoSettings;
import android.support.v7.preference.Preference;
import com.android.settingslib.RestrictedLockUtils;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserHandle;
import android.app.Fragment;
import com.android.settingslib.RestrictedPreference;
import com.android.internal.widget.LockPatternUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public class OwnerInfoPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin, LifecycleObserver, OnResume
{
    private static final int MY_USER_ID;
    private final LockPatternUtils mLockPatternUtils;
    private RestrictedPreference mOwnerInfoPref;
    private final Fragment mParent;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public OwnerInfoPreferenceController(final Context context, final Fragment mParent, final Lifecycle lifecycle) {
        super(context);
        this.mParent = mParent;
        this.mLockPatternUtils = new LockPatternUtils(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        this.mOwnerInfoPref = (RestrictedPreference)preferenceScreen.findPreference("owner_info_settings");
    }
    
    RestrictedLockUtils.EnforcedAdmin getDeviceOwner() {
        return RestrictedLockUtils.getDeviceOwner(this.mContext);
    }
    
    String getDeviceOwnerInfo() {
        return this.mLockPatternUtils.getDeviceOwnerInfo();
    }
    
    String getOwnerInfo() {
        return this.mLockPatternUtils.getOwnerInfo(OwnerInfoPreferenceController.MY_USER_ID);
    }
    
    @Override
    public String getPreferenceKey() {
        return "owner_info_settings";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    boolean isDeviceOwnerInfoEnabled() {
        return this.mLockPatternUtils.isDeviceOwnerInfoEnabled();
    }
    
    boolean isOwnerInfoEnabled() {
        return this.mLockPatternUtils.isOwnerInfoEnabled(OwnerInfoPreferenceController.MY_USER_ID);
    }
    
    @Override
    public void onResume() {
        this.updateEnableState();
        this.updateSummary();
    }
    
    public void updateEnableState() {
        if (this.mOwnerInfoPref == null) {
            return;
        }
        if (this.isDeviceOwnerInfoEnabled()) {
            this.mOwnerInfoPref.setDisabledByAdmin(this.getDeviceOwner());
        }
        else {
            this.mOwnerInfoPref.setDisabledByAdmin(null);
            this.mOwnerInfoPref.setEnabled(this.mLockPatternUtils.isLockScreenDisabled(OwnerInfoPreferenceController.MY_USER_ID) ^ true);
            if (this.mOwnerInfoPref.isEnabled()) {
                this.mOwnerInfoPref.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(final Preference preference) {
                        OwnerInfoSettings.show(OwnerInfoPreferenceController.this.mParent);
                        return true;
                    }
                });
            }
        }
    }
    
    public void updateSummary() {
        if (this.mOwnerInfoPref != null) {
            if (this.isDeviceOwnerInfoEnabled()) {
                this.mOwnerInfoPref.setSummary(this.getDeviceOwnerInfo());
            }
            else {
                final RestrictedPreference mOwnerInfoPref = this.mOwnerInfoPref;
                String summary;
                if (this.isOwnerInfoEnabled()) {
                    summary = this.getOwnerInfo();
                }
                else {
                    summary = this.mContext.getString(2131888548);
                }
                mOwnerInfoPref.setSummary(summary);
            }
        }
    }
    
    public interface OwnerInfoCallback
    {
        void onOwnerInfoUpdated();
    }
}
